﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadUserBranch : System.Web.UI.Page
    {                
        bool suucess = false;
        bool suucessSave = false;
        bool CheckListsuucess = false;
        bool CheckListsuucessSave = false;
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                BindCustomersList(userID);
            }
        }
        protected void lnkSampleFormat_Click(object sender, EventArgs e)
        {
            try
            {                
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                string filePath = Server.MapPath("~/ExcelFormat/SampleUserBranchUploadFormat.xlsx");

                if (!string.IsNullOrEmpty(filePath))
                {
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.AddHeader("Content-Disposition", "attachment;filename=SampleUserBranchUploadFormat.xlsx");
                    byte[] data = req.DownloadData(filePath);
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    response.BinaryWrite(data);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "No Format Available for Download, Please Contact Admin for more detail";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void ProcessUserData(ExcelPackage xlWorkbook)
        {
            try
            {
                int CustomerID = 0;
                bool saveSuccess = false;
                int uploadedContractCount = 0;
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    if (ddlCustomerList.SelectedValue != "-1")
                    {
                        CustomerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    }
                }
                if (CustomerID != 0)
                {
                    #region Excel
                    ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["UserList"];
                    if (xlWorksheet != null)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            long newContractID = 0;
                            int count = 0;
                            int xlrow2 = xlWorksheet.Dimension.End.Row;
                            List<long> lstDepartmentIDs = new List<long>();
                            List<string> errorMessage = new List<string>();                         
                            string FirstName = string.Empty;
                            string LastName = string.Empty;
                            string Email = string.Empty;
                            string ContactNumber = string.Empty;
                            string CustomerbranchName = string.Empty;
                            string IsDepartmentHead = string.Empty;
                            string DepartmentCommaseparated = string.Empty;
                            string ContractOwner = string.Empty;
                            string RoleCode = string.Empty;
                            string DepartmetNames = string.Empty;
                            string Designation = string.Empty;

                            //Fetch List from database of Master Data                            
                            var masterlstrolecode = entities.Roles.ToList();
                            var masterlstCustomerBranches = entities.CustomerBranches.Where(row => row.CustomerID == CustomerID).ToList();
                            var masterlstUsers = entities.Users.Where(row => row.CustomerID == CustomerID).ToList();                            
                            var masterlstDepts = entities.Departments.Where(row => row.CustomerID == CustomerID).ToList();

                            #region Validations
                            for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                            {
                                count = count + 1;
                                FirstName = string.Empty;
                                LastName = string.Empty;
                                Email = string.Empty;
                                ContactNumber = string.Empty;
                                RoleCode = string.Empty;
                                CustomerbranchName = string.Empty;
                                IsDepartmentHead = string.Empty;
                                DepartmentCommaseparated = string.Empty;
                                Designation = string.Empty;
                               

                                if (lstDepartmentIDs.Count>0)
                                {
                                    lstDepartmentIDs.Clear();
                                
                                }
                                #region 1 First Name
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                                {
                                    FirstName = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();                                    
                                }
                                if (String.IsNullOrEmpty(FirstName))
                                {
                                    errorMessage.Add("Required First Name at row number-" + rowNum);
                                }
                                #endregion
                                #region 2 Last Name
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                                {
                                    LastName = Convert.ToString(xlWorksheet.Cells[rowNum, 2].Text).Trim();
                                }
                                if (String.IsNullOrEmpty(LastName))
                                {
                                    errorMessage.Add("Required Last Name at row number-" + rowNum);
                                }
                                #endregion

                                #region 3 Email                                
                                long OwnerID = -1;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                                {
                                    Email = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                                    if (!Email.Contains('@'))
                                    {
                                        errorMessage.Add("Please Correct the User(Email) at row number-" + rowNum);
                                    }
                                    else
                                    {
                                        OwnerID = UsersUpload.GetOwnerUserIDByEmail(masterlstUsers, CustomerID, Email.ToString().Trim());
                                        if (OwnerID>0)
                                        {
                                            errorMessage.Add("Please Correct the User(Email) or User (" + Email + ") alreay defined in the User Master, Correct at row number-" + rowNum);                                            
                                        }
                                    }

                                    //bool emailExists1;
                                    //var Data= UserManagement.ExistsEmailID(Email);
                                    //if (Data !=null)
                                    //{
                                    //    errorMessage.Add("Please Correct the User(Email) or User (" + Email + ") alreay defined in the User Master, Correct at row number-" + rowNum);
                                    //} 
                                }
                                else
                                {
                                    errorMessage.Add("Required User (Email) at row number-" + rowNum);
                                }
                                #endregion

                                #region 4 Contact number
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                                {
                                    bool ValidatePhone = UsersUpload.IsValidPhoneNumber(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim());
                                    if (ValidatePhone == false)
                                    {
                                        errorMessage.Add("Contact Number Not Proper Format at row number - " + (count + 1) + "");
                                    }
                                }
                                else
                                 {
                                    errorMessage.Add("Contact Number required at row number - " + (count + 1) + "");

                                }
                                
                                #endregion
                                #region 5 RoleCode
                                int roleid = 0;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                {
                                    RoleCode = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                    roleid = UsersUpload.GetRoleIDByCode(masterlstrolecode, RoleCode);
                                    if (roleid == 0 || roleid == -1)
                                    {
                                        errorMessage.Add("Please Correct the Role Code (" + RoleCode + ") not Defined in the Masters, Correct at row number-" + rowNum);
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Required Role Code at row number-" + rowNum);
                                }
                                #endregion

                                #region 6 Location
                                    long branchid = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                                    {
                                        CustomerbranchName = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                                        branchid = UsersUpload.GetBranchIDByName(masterlstCustomerBranches, CustomerID, CustomerbranchName);
                                        if (branchid == 0 || branchid == -1)
                                        {
                                            errorMessage.Add("Please Correct the Location or Location not defined in the Unit Master, Correct at row number- " + rowNum);
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Location associated with user, Correct at row number-" + rowNum);
                                    }
                                    #endregion
                                

                                #region 7 Is Department Head
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                                {
                                    IsDepartmentHead = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text).Trim();                                    
                                }
                                
                                if (IsDepartmentHead.Trim().ToUpper() == "YES")
                                {
                                    int deptid = 0;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                    {
                                        DepartmetNames = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                                        string[] split = DepartmetNames.Split(',');
                                        if (split.Length > 0)
                                        {
                                            for (int rs = 0; rs < split.Length; rs++)
                                            {
                                                deptid = UsersUpload.GetDepartmentIDByName(masterlstDepts, split[rs].ToString().Trim(), CustomerID);                                                
                                                if (deptid <= 0)
                                                {
                                                    errorMessage.Add("Please Correct the Department Name or Department (" + split[rs].ToString().Trim() + ") not defined in the Department Master, Correct at row number- " + rowNum);
                                                }
                                                else
                                                {
                                                    lstDepartmentIDs.Add(deptid);
                                                }                                                
                                            }
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Department Name at row number-" + rowNum);
                                    }
                                } 
                                else
                                {
                                    int deptid = 0;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                    {
                                        DepartmetNames = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                                        string[] split = DepartmetNames.Split(',');
                                        if (split.Length > 0)
                                        {
                                            for (int rs = 0; rs < split.Length; rs++)
                                            {
                                                deptid = UsersUpload.GetDepartmentIDByName(masterlstDepts, split[rs].ToString().Trim(), CustomerID);
                                                if (deptid <= 0)
                                                {
                                                    errorMessage.Add("Please Correct the Department Name or Department (" + split[rs].ToString().Trim() + ") not defined in the Department Master, Correct at row number- " + rowNum);
                                                }
                                                else
                                                {
                                                    lstDepartmentIDs.Add(deptid);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        errorMessage.Add("Required Department Name at row number-" + rowNum);
                                    }
                                }

                                #endregion

                                #region 8 Designation
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString().Trim()))
                                {
                                    Designation = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text).Trim();
                                }
                                if (String.IsNullOrEmpty(Designation))
                                {
                                    errorMessage.Add("Required Designation at row number-" + rowNum);
                                }
                                #endregion

                            }
                           



                            #endregion

                            if (errorMessage.Count > 0)
                            {
                                ErrorMessages(errorMessage);
                            }
                            else
                            {
                                #region Save 
                                List<DepartmentMapping> lstVendorMapping_ToSave = new List<DepartmentMapping>();
                                List<User> cUsers = new List<User>();
                                List<mst_User> AUsers = new List<mst_User>();

                                for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                                {
                                    FirstName = string.Empty;
                                    LastName = string.Empty;
                                    Email = string.Empty;

                                    RoleCode = string.Empty;
                                    CustomerbranchName = string.Empty;
                                    IsDepartmentHead = string.Empty;
                                    DepartmentCommaseparated = string.Empty;
                                    Designation = string.Empty;

                                    if (lstDepartmentIDs.Count > 0)
                                    {
                                        lstDepartmentIDs.Clear();
                                    }

                                    #region 1 First Name
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                                    {
                                        FirstName = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();
                                    }
                                    #endregion

                                    #region 2 Last Name
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                                    {
                                        LastName = Convert.ToString(xlWorksheet.Cells[rowNum, 2].Text).Trim();
                                    }
                                    #endregion

                                    #region 3 Email
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                                    {
                                        Email = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                                    }
                                    #endregion


                                    #region 4 Role Code
                                    int roleid = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                    {
                                        RoleCode = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                        roleid = UsersUpload.GetRoleIDByCode(masterlstrolecode, RoleCode);
                                    }
                                    #endregion

                                    #region 5 Location
                                    int branchid = 0;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                                    {
                                        CustomerbranchName = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                                        branchid = UsersUpload.GetBranchIDByName(masterlstCustomerBranches, CustomerID, CustomerbranchName);

                                    }
                                    #endregion

                                    #region 6 IsDepartmentHead
                                    bool ishead = false;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                                    {
                                        IsDepartmentHead = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text).Trim();
                                        if (IsDepartmentHead.Trim().ToUpper() == "YES")
                                        {
                                            ishead = true;

                                            int deptid = 0;
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                            {
                                                DepartmetNames = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                                                string[] split = DepartmetNames.Split(',');
                                                if (split.Length > 0)
                                                {
                                                    for (int rs = 0; rs < split.Length; rs++)
                                                    {
                                                        deptid = UsersUpload.GetDepartmentIDByName(masterlstDepts, split[rs].ToString().Trim(), CustomerID);
                                                        lstDepartmentIDs.Add(deptid);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ishead = false;
                                            }

                                        }
                                        else
                                        {
                                            ishead = false;

                                            int deptid = 0;
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                            {
                                                DepartmetNames = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                                                string[] split = DepartmetNames.Split(',');
                                                if (split.Length > 0)
                                                {
                                                    for (int rs = 0; rs < split.Length; rs++)
                                                    {
                                                        deptid = UsersUpload.GetDepartmentIDByName(masterlstDepts, split[rs].ToString().Trim(), CustomerID);
                                                        lstDepartmentIDs.Add(deptid);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ishead = true;
                                            }

                                        }
                                    }
                                    #endregion

                                    #region 7 Last Name
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString().Trim()))
                                    {
                                        Designation = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text).Trim();
                                    }
                                    #endregion

                                    #region Compliance User
                                    User user = new User()
                                    {
                                        FirstName = FirstName,
                                        LastName = LastName,
                                        Email = Email,
                                        ContactNumber = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim(),
                                        CustomerBranchID = branchid,
                                        IsHead = ishead,
                                        RoleID = roleid,
                                        IsExternal = false,
                                        CustomerID = CustomerID,
                                        IsAuditHeadOrMgr = null,
                                        Designation = Designation,
                                        CreatedFrom = 1,
                                        CreatedBy = AuthenticationHelper.UserID
                                    };
                                    #endregion

                                    #region Risk User

                                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                                    {
                                        FirstName = FirstName,
                                        LastName = LastName,
                                        Email = Email,
                                        ContactNumber = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim(),
                                        CustomerBranchID = branchid,
                                        IsHead = ishead,
                                        RoleID = roleid,
                                        IsExternal = false,
                                        CustomerID = CustomerID,
                                        IsAuditHeadOrMgr = null,
                                        Designation = Designation,
                                        CreatedFrom = 1,
                                        CreatedBy = AuthenticationHelper.UserID
                                    };

                                    #endregion

                                    //if role is company admin
                                    if (roleid == 2)
                                    {

                                        user.VendorRoleID = 2;
                                        mstUser.VendorRoleID = 2;

                                        var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(CustomerID));
                                        //Litigation Management
                                        if (ProductMappingDetails.Contains(2))
                                        {
                                            if (user.LitigationRoleID == null)
                                            {
                                                user.LitigationRoleID = 2;
                                                mstUser.LitigationRoleID = 2;
                                            }
                                        }
                                        //Contract Management
                                        if (ProductMappingDetails.Contains(5))
                                        {
                                            if (user.ContractRoleID == null)
                                            {
                                                user.ContractRoleID = 2;
                                                mstUser.ContractRoleID = 2;
                                            }
                                        }
                                        //License Management
                                        if (ProductMappingDetails.Contains(6))
                                        {
                                            if (user.LicenseRoleID == null)
                                            {
                                                user.LicenseRoleID = 2;
                                                mstUser.LicenseRoleID = 2;
                                            }
                                        }
                                    }
                                    string passwordText = Util.CreateRandomPassword(10);

                                    user.CreatedBy = AuthenticationHelper.UserID;
                                    user.CreatedByText = AuthenticationHelper.User;
                                    //user.Password = Util.CalculateMD5Hash(passwordText);
                                    user.Password = Util.CalculateAESHash(passwordText);
                                    //string message = SendNotificationEmail(user, passwordText);

                                    mstUser.CreatedBy = AuthenticationHelper.UserID;
                                    mstUser.CreatedByText = AuthenticationHelper.User;
                                    //mstUser.Password = Util.CalculateMD5Hash(passwordText);
                                    mstUser.Password = Util.CalculateAESHash(passwordText);
                                    bool result = false;

                                    var Data = UserManagement.ExistsEmailID(Email);
                                    if (Data == null)
                                    {
                                        int resultValue = 0;
                                        resultValue = UsersUpload.CreateCompliance(user);

                                        if (resultValue > 0)
                                        {

                                            result = UsersUpload.CreateAuditUser(mstUser);
                                            if (result == false)
                                            {

                                                UserManagement.deleteUser(resultValue);

                                            }
                                            else
                                            {

                                                widget swid = new widget()
                                                {
                                                    UserId = (Int32)user.ID,
                                                    Performer = true,
                                                    Reviewer = true,
                                                    PerformerLocation = true,
                                                    ReviewerLocation = true,
                                                    DailyUpdate = true,
                                                    NewsLetter = true,
                                                    ComplianceSummary = true,
                                                    FunctionSummary = true,
                                                    RiskCriteria = true,
                                                    EventOwner = true,
                                                    PenaltySummary = true,
                                                    TaskSummary = true,
                                                    ReviewerTaskSummary = true,
                                                    CustomWidget = false,
                                                    Department = true,
                                                };
                                                result = UserManagement.Create(swid);

                                                //try
                                                //{
                                                //    string message = SendNotificationEmail(user, passwordText);
                                                //    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Account Created.", message);
                                                //}
                                                //catch (Exception)
                                                //{

                                                //}

                                                uploadedContractCount++;
                                                saveSuccess = true;
                                            }
                                            #region Department

                                            List<DepartmentMapping> objDepartmentMapping = new List<DepartmentMapping>();

                                            if (lstDepartmentIDs.Count > 0)
                                            {
                                                objDepartmentMapping.Clear();

                                                lstDepartmentIDs.ForEach(Eachdept =>
                                                {
                                                    DepartmentMapping DeptMapping = new DepartmentMapping()
                                                    {
                                                        UserID = resultValue,
                                                        DepartmentID = Convert.ToInt32(Eachdept),
                                                        IsActive = true,
                                                        CustomerID = CustomerID,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                    };
                                                    objDepartmentMapping.Add(DeptMapping);
                                                });
                                                if (objDepartmentMapping.Count > 0)
                                                {
                                                    UsersUpload.CreateDepartmentMapping(objDepartmentMapping);
                                                }
                                                lstDepartmentIDs.Clear();
                                                objDepartmentMapping.Clear();
                                            }

                                            #endregion
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (saveSuccess)
                            {
                                if (uploadedContractCount > 0)
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = uploadedContractCount + " User(s) Details Uploaded Successfully";
                                    cvUploadUtilityPage.CssClass = "alert alert-success";
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                                }
                            }

                            masterlstCustomerBranches = null;
                            masterlstUsers = null;
                            masterlstrolecode = null;
                            masterlstDepts = null;

                            
                        }// END DB Context
                    }
                    #endregion
                }//Customerid=0 end
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string accessURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(user.CustomerID));
                if (Urloutput != null)
                {
                    accessURL = Urloutput.URL;
                }
                else
                {
                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                      .Replace("@Username", user.Email)
                                      .Replace("@User", username)
                                      .Replace("@PortalURL", Convert.ToString(accessURL))
                                      .Replace("@Password", passwordText)
                                      .Replace("@From", ReplyEmailAddressName)
                                      .Replace("@URL", Convert.ToString(accessURL));

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }


        private void ProcessBranchData(ExcelPackage xlWorkbook)
        {
            try
            {
                int CustomerID = 0;
                bool saveSuccess = false;
                int uploadedContractCount = 0;
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    if (ddlCustomerList.SelectedValue != "-1")
                    {
                        CustomerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    }
                }
                if (CustomerID != 0)
                {
                    #region Excel
                    ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["BranchList"];
                    if (xlWorksheet != null)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            long newContractID = 0;
                            int xlrow2 = xlWorksheet.Dimension.End.Row;
                            string ParentName = string.Empty;
                            string Location = string.Empty;
                            string SubEntityType = string.Empty;
                            string Type = string.Empty;
                            string IndustryCommaseparated = string.Empty;
                            string Address = string.Empty;
                            string Address2 = string.Empty;
                            string State = string.Empty;
                            string City = string.Empty;
                            string pincode = string.Empty;
                            string IndustryNames = string.Empty;
                            // long newContractID = 0;
                            List<long> lstIndustryIDs = new List<long>();                            
                            List<string> errorMessage = new List<string>();
                            string ContactPerson = string.Empty;
                            string landlineno = string.Empty;
                            string mobno = string.Empty;
                            string EmailID = string.Empty;
                            string LegalEntityType = string.Empty;

                            //Fetch List from database of Master Data  

                            var masterlstMLegalEntityType = entities.M_LegalEntityType.ToList();
                            var masterlstCompanyType = entities.CompanyTypes.ToList();
                            var masterlstNodeType = entities.NodeTypes.ToList();
                            var masterlstState = entities.States.ToList();
                            var masterlstCity = entities.Cities.ToList();
                            var masterlstIndustry = entities.Industries.ToList();
                            var masterlstUsers = entities.Users.Where(row => row.CustomerID == CustomerID).ToList();
                            //var masterlstCustomerBranches = entities.CustomerBranches.Where(row => row.CustomerID == CustomerID).ToList();
                            var masterlstLegalEntityType = entities.M_LegalEntityType.ToList();

                            #region Validations
                            for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                            {
                                ParentName = string.Empty;
                                Location = string.Empty;
                                SubEntityType = string.Empty;
                                Type = string.Empty;
                                Address = string.Empty;
                                Address2 = string.Empty;
                                State = string.Empty;
                                City = string.Empty;
                                pincode = string.Empty;
                                ContactPerson = string.Empty;
                                IndustryCommaseparated = string.Empty;
                                EmailID = string.Empty;
                                landlineno = string.Empty;
                                mobno = string.Empty;
                                LegalEntityType = string.Empty;

                                #region 1 Parent Name
                                //int parentid = 0;
                                //int? parentid = null;
                                //if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                                //{
                                //    ParentName = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();
                                //    parentid = UsersUpload.GetParentID(masterlstCustomerBranches, ParentName);
                                //}
                                //if (String.IsNullOrEmpty(ParentName))
                                //{
                                //    if (parentid == 0)
                                //    {
                                //        parentid = null;
                                //    }
                                //}
                                #endregion

                                #region 2 Location
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                                {
                                    Location = Convert.ToString(xlWorksheet.Cells[rowNum, 2].Text).Trim();
                                    if (UsersUpload.Exists(Location, CustomerID))
                                    {                                        
                                        errorMessage.Add("Customer Location name already exists (" + Location + "), Correct at row number- " + rowNum);
                                    }
                                }
                                if (String.IsNullOrEmpty(Location))
                                {
                                    errorMessage.Add("Required Location name at row number-" + rowNum);
                                }
                                #endregion

                                #region 3 Sub Unit Type
                                int Subid = 0;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                                {
                                    SubEntityType = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                                    Subid = UsersUpload.GetSubEntityType(masterlstNodeType, SubEntityType);

                                    if (Subid ==0 || Subid == -1)
                                    {
                                        errorMessage.Add("Please Correct the Sub Unit Type (" + SubEntityType + ") not Defined in the Masters, Correct at row number-" + rowNum);
                                    }                                    
                                }
                                if (String.IsNullOrEmpty(SubEntityType))
                                {
                                    errorMessage.Add("Required Sub Unit Type at row number-" + rowNum);
                                }
                                #endregion

                                #region 4  Type
                                int Typeid = 0;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                                {
                                    Type = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim();
                                    Typeid = UsersUpload.GetType(masterlstCompanyType, Type);
                                    if (Typeid == 1 || Typeid == 2 || Typeid == 3 ||Typeid == 4)
                                    {
                                    }
                                    else
                                    {
                                        errorMessage.Add("Please Correct the Type (" + Type + ") not Defined in the Masters, Correct at row number-" + rowNum);
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Required Type name at row number-" + rowNum);
                                }                             
                                #endregion

                                #region 5 Industry separated
                                //if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                //{
                                //    IndustryCommaseparated = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                //}

                                //if (IndustryCommaseparated.Trim().ToUpper() == "")
                                //{
                                   
                                int IndustryID = 0;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                {
                                    IndustryNames = Regex.Replace(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim(), @"\t|\n|\r", "");
                                    //Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                    string[] split = IndustryNames.Split(',');
                                    if (split.Length > 0)
                                    {
                                        for (int rs = 0; rs < split.Length; rs++)
                                        {
                                            IndustryID = UsersUpload.GetIndustryIDByName(masterlstIndustry, split[rs].ToString().Trim(), CustomerID);
                                            if (IndustryID <= 0)
                                            {
                                                errorMessage.Add("Please Correct the Industry Name or Industry (" + split[rs].ToString().Trim() + ") not defined in the Industry Master, Correct at row number- " + rowNum);
                                            }
                                            else
                                            {
                                                lstIndustryIDs.Add(IndustryID);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Required Industry Name at row number-" + rowNum);
                                }
                                //}

                                #endregion

                                #region 6 Address
                               
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                                {
                                    Address = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                                }
                                if (String.IsNullOrEmpty(Address))
                                {
                                    errorMessage.Add("Required Address at row number-" + rowNum);
                                }
                                #endregion

                                #region 7 State
                               
                                int stateid = 0;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                {
                                    State = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                                    stateid = UsersUpload.GetStateID(masterlstState, State);
                                    if (stateid == 0 || stateid == -1)
                                    {
                                        errorMessage.Add("Please Correct the State (" + State + ") not Defined in the Masters, Correct at row number-" + rowNum);
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Required State at row number-" + rowNum);
                                }
                                #endregion

                                #region 8 City

                                int Cityid = 0;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString().Trim()))
                                {
                                    City = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text).Trim();
                                    Cityid = UsersUpload.GetCityID(masterlstCity, City);
                                    if (Cityid == 0 || Cityid == -1)
                                    {
                                        errorMessage.Add("Please Correct the City (" + City + ") not Defined in the Masters, Correct at row number-" + rowNum);
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Required City at row number-" + rowNum);
                                }

                                #endregion

                                #region 9 Contact Person
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 11].Text.ToString().Trim()))
                                {
                                    ContactPerson = Convert.ToString(xlWorksheet.Cells[rowNum, 11].Text).Trim();
                                }
                                if (String.IsNullOrEmpty(ContactPerson))
                                {
                                    errorMessage.Add("Required Contact Person Name at row number-" + rowNum);
                                }
                                #endregion

                                #region 10 Email                                
                                long OwnerID = -1;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 14].Text.ToString().Trim()))
                                {
                                    EmailID = Convert.ToString(xlWorksheet.Cells[rowNum, 14].Text).Trim();
                                    if (!EmailID.Contains('@'))
                                    {
                                        errorMessage.Add("Please Correct the User(Email) at row number-" + rowNum);
                                    }
                                    else
                                    {
                                        OwnerID = UsersUpload.GetOwnerUserIDByEmail(masterlstUsers, CustomerID, EmailID.ToString().Trim());
                                        if (OwnerID < 0)
                                        {
                                            errorMessage.Add("Please Correct the User(Email) or User (" + EmailID + ") alreay defined in the User Master, Correct at row number-" + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    errorMessage.Add("Required User (EmailID) at row number-" + rowNum);
                                }
                                #endregion
                           }

                           #endregion

                            if (errorMessage.Count > 0)
                            {
                                ErrorMessages(errorMessage);
                            }
                            else
                            {
                                #region Save 
                                List<Business.Data.CustomerBranchIndustryMapping> lstIndustryMapping_ToSave = new List<Business.Data.CustomerBranchIndustryMapping>();
                                List<CustomerBranch> cUsers = new List<CustomerBranch>();
                                List<mst_CustomerBranch> AUsers = new List<mst_CustomerBranch>();

                                for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                                {
                                    if (lstIndustryIDs.Count > 0)
                                    {
                                        lstIndustryIDs.Clear();
                                    }
                                    ParentName = string.Empty;
                                    Location = string.Empty;
                                    SubEntityType = string.Empty;
                                    Type = string.Empty;
                                    Address = string.Empty;
                                    Address2 = string.Empty;
                                    State = string.Empty;
                                    City = string.Empty;
                                    ContactPerson = string.Empty;
                                    IndustryCommaseparated = string.Empty;
                                    landlineno = string.Empty;
                                    mobno = string.Empty;
                                    LegalEntityType = string.Empty;
                                    pincode = string.Empty;



                                    #region 1 Parent Name
                                    int ?parentid = null;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                                    {
                                        ParentName = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();
                                        parentid = UsersUpload.GetParentID(CustomerID, ParentName);
                                        if (parentid == 0)
                                        {
                                            parentid = null;
                                        }
                                    }
                                    #endregion

                                    #region 2 Location

                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                                    {
                                        Location = Convert.ToString(xlWorksheet.Cells[rowNum, 2].Text).Trim();

                                    }

                                    #endregion

                                    #region 3 SubEntityType
                                    int Subid = 0;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                                    {
                                        SubEntityType = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                                        Subid = UsersUpload.GetSubEntityType(masterlstNodeType, SubEntityType);

                                    }
                                    #endregion

                                    #region 4 Type

                                    int Typeid = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                                    {
                                        Type = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim();
                                        Typeid = UsersUpload.GetType(masterlstCompanyType, Type);
                                    }
                                    #endregion

                                    #region 5 Industry Commaseparated
                                    bool ishead = false;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                    {
                                        //IndustryCommaseparated = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();

                                        int IndustryID = 0;
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                        {
                                            IndustryNames = Regex.Replace(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim(), @"\t|\n|\r", "");
                                            //IndustryNames = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                                            string[] split = IndustryNames.Split(',');
                                            if (split.Length > 0)
                                            {
                                                for (int rs = 0; rs < split.Length; rs++)
                                                {
                                                    IndustryID = UsersUpload.GetIndustryIDByName(masterlstIndustry, split[rs].ToString().Trim(), CustomerID);
                                                    lstIndustryIDs.Add(IndustryID);
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region 6 Address
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                                    {
                                        Address = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                                    }
                                    #endregion

                                    #region 7 Address2
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                                    {
                                        Address2 = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text).Trim();
                                    }
                                    #endregion

                                    #region 8 State
                                    int stateid = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                    {
                                        State = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                                        stateid = UsersUpload.GetStateID(masterlstState, State);
                                    }
                                    #endregion

                                    #region 9 City
                                    int Cityid = -1;
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 9].Text.ToString().Trim()))
                                    {
                                        City = Convert.ToString(xlWorksheet.Cells[rowNum, 9].Text).Trim();
                                        Cityid = UsersUpload.GetCityID(masterlstCity, City);
                                    }
                                    #endregion

                                    #region 10 pincode

                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 10].Text.ToString().Trim()))
                                    {
                                        pincode = Convert.ToString(xlWorksheet.Cells[rowNum, 10].Text).Trim();
                                       
                                    }
                                    #endregion

                                    #region 11 Contact Person
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 11].Text.ToString().Trim()))
                                    {
                                        ContactPerson = Convert.ToString(xlWorksheet.Cells[rowNum, 11].Text).Trim();
                                    }
                                    #endregion

                                    #region 12 landlineno
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 12].Text.ToString().Trim()))
                                    {
                                        landlineno = Convert.ToString(xlWorksheet.Cells[rowNum, 12].Text).Trim();
                                    }
                                    #endregion

                                    #region 13 mobno
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 13].Text.ToString().Trim()))
                                    {
                                        mobno = Convert.ToString(xlWorksheet.Cells[rowNum, 13].Text).Trim();
                                    }
                                    #endregion

                                    #region 14 Email
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 14].Text.ToString().Trim()))
                                    {
                                        EmailID = Convert.ToString(xlWorksheet.Cells[rowNum, 14].Text).Trim();
                                    }
                                    #endregion

                                    #region 15 Unit Type
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 15].Text.ToString().Trim()))
                                    {
                                        LegalEntityType = Convert.ToString(xlWorksheet.Cells[rowNum, 15].Text).Trim();
                                    }
                                    #endregion

                                    #region Compliance User Branch


                                    CustomerBranch customerBranch = new CustomerBranch()
                                    {
                                        Name = Location.Trim(),
                                        Type = Convert.ToByte(Subid),
                                        ComType = Convert.ToByte(Typeid),
                                        AddressLine1 = Address.Trim(),
                                        AddressLine2 = Address2.Trim(),
                                        StateID = Convert.ToInt32(stateid),
                                        CityID = Convert.ToInt32(Cityid),
                                        Others = "",
                                        PinCode = pincode.Trim(),
                                        ContactPerson = ContactPerson.Trim(),
                                        Landline = landlineno.Trim(),
                                        Mobile = mobno.Trim(),
                                        EmailID = EmailID,
                                        CustomerID = Convert.ToInt32(ddlCustomerList.SelectedValue),
                                        ParentID = parentid,
                                        Status = 0,
                                        AuditPR = false,
                                        LegalRelationShipID = null,
                                        CreatedBy =AuthenticationHelper.UserID,
                                        CreatedFrom=1

                                };


                                   #endregion

                                    #region Risk Customer Branch

                                    com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch mstCustomerBranch = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
                                    {
                                        Name = Location.Trim(),
                                        Type = Convert.ToByte(Subid),
                                        ComType = Convert.ToByte(Typeid),
                                        AddressLine1 = Address.Trim(),
                                        AddressLine2 = Address2.Trim(),
                                        StateID = Convert.ToInt32(stateid),
                                        CityID = Convert.ToInt32(Cityid),
                                        Others = "",
                                        PinCode = pincode.Trim(),
                                        ContactPerson = ContactPerson.Trim(),
                                        Landline = landlineno.Trim(),
                                        Mobile = mobno.Trim(),
                                        EmailID = EmailID,
                                        CustomerID = Convert.ToInt32(ddlCustomerList.SelectedValue),
                                        ParentID = parentid,
                                        Status = 0,
                                        AuditPR = false,
                                        LegalRelationShipID = null,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedFrom=1
                                    };

                                    #endregion

                                    bool result = false;
                                    int resultValue = 0;
                                    resultValue = UsersUpload.CreateBranchCompliance(customerBranch);

                                    if (resultValue > 0)
                                    {

                                        result = UsersUpload.CreateBranchAuditUser(mstCustomerBranch);
                                        if (result == false)
                                        {
                                            UserManagement.deleteUser(resultValue);
                                        }
                                        else
                                        {
                                            uploadedContractCount++;
                                            saveSuccess = true;
                                        }

                                        #region Industry

                                        List<Business.Data.IndustryMapping> objIndustryMapping = new List<Business.Data.IndustryMapping>();

                                        if (lstIndustryIDs.Count > 0)
                                        {
                                            lstIndustryMapping_ToSave.Clear();

                                            lstIndustryIDs.ForEach(Eachdept =>
                                            {
                                                Business.Data.CustomerBranchIndustryMapping IndMapping = new Business.Data.CustomerBranchIndustryMapping()
                                                {

                                                    CustomerBranchID = resultValue,
                                                    IndustryID = Convert.ToInt32(Eachdept),         
                                                    IsActive = true,
                                                    EditedBy = AuthenticationHelper.UserID,
                                                    EditedDate = DateTime.Now,
                                            };
                                                lstIndustryMapping_ToSave.Add(IndMapping);
                                            });
                                            if (lstIndustryMapping_ToSave.Count > 0)
                                            {
                                                UsersUpload.CreateIndustryMapping(lstIndustryMapping_ToSave);
                                            }                                            
                                            lstIndustryMapping_ToSave.Clear();
                                            lstIndustryIDs.Clear();
                                        }

                                        #endregion
                                    }                                  
                                }
                                #endregion
                                if (saveSuccess)
                                {
                                    if (uploadedContractCount > 0)
                                    {
                                        cvUploadUtilityPage.IsValid = false;
                                        cvUploadUtilityPage.ErrorMessage = uploadedContractCount + " Branch(s) Details Uploaded Successfully";
                                        cvUploadUtilityPage.CssClass = "alert alert-success";
                                    }
                                    else
                                    {
                                        cvUploadUtilityPage.IsValid = false;
                                        cvUploadUtilityPage.ErrorMessage = "No Data Uploaded from Excel Document";
                                    }
                                }

                                //masterlstCustomerBranches = null;
                                masterlstMLegalEntityType = null;
                                masterlstCompanyType = null;
                                masterlstNodeType = null;
                                masterlstState = null;
                                masterlstIndustry = null;
                                masterlstCity = null;


                            }
                        }// END DB Context

                    }//xlWorksheet //Customerid=0 end
                    #endregion
                }//Customerid=0 end

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public void ErrorMessages(List<string> emsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvUploadUtilityPage.IsValid = false;
            cvUploadUtilityPage.ErrorMessage = finalErrMsg;
        }      
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private void BindCustomersList(int userID)
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";
                //  ddlCustomerList.DataSource = UsersUpload.GetClient();
                ddlCustomerList.DataSource = UsersUpload.GetAllCustomer(userID);
                ddlCustomerList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadUtilityPage.IsValid = false;
                cvUploadUtilityPage.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }                
        public static bool checkSheetExist(ExcelPackage xlWorkbook, string sheetNameUploaded)
        {
            try
            {
                bool matchFlag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (sheet.Name.Trim().Equals(sheetNameUploaded))
                    {
                        matchFlag = true;
                    }
                } //End ForEach
                return matchFlag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoBtn_User.Checked)
                            {
                                bool matchSuccess = checkSheetExist(xlWorkbook, "UserList");
                                if (matchSuccess)
                                {
                                    ProcessUserData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'UserList'.";
                                }
                            }
                            else if (rdoBtn_Branch.Checked)
                            {
                                bool matchSuccess = checkSheetExist(xlWorkbook, "BranchList");
                                if (matchSuccess)
                                {
                                    ProcessBranchData(xlWorkbook);
                                }
                                else
                                {
                                    cvUploadUtilityPage.IsValid = false;
                                    cvUploadUtilityPage.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'User Branch list'.";
                                }
                            }
                            else
                            {
                                cvUploadUtilityPage.IsValid = false;
                                cvUploadUtilityPage.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                        }
                    }
                    else
                    {
                        cvUploadUtilityPage.IsValid = false;
                        cvUploadUtilityPage.ErrorMessage = "Error Uploading Excel Document. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadUtilityPage.IsValid = false;
                    cvUploadUtilityPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }

        }
        protected void rdoBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoBtn_Branch.Checked || rdoBtn_User.Checked)
            {
                MasterFileUpload.Visible = true;
                btnUploadFile.Visible = true;
            }
            else
            {
                MasterFileUpload.Visible = false;
                btnUploadFile.Visible = false;
            }
        }
        
    }

}

