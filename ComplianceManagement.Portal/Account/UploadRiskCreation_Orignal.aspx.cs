﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class UploadRiskCreation_Orignal : System.Web.UI.Page
    {
        bool suucess = false;
        bool suucessProcess = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnUploadFile1.Visible = false;
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;
                Tab1_Click1(sender, e);
                BindCustomer();
                GetPageDisplaySummary();
                BindLegalEntityData();
                //BindIndustry();
                //BindAssertions();
                ViewState["Flagvalue"] = null;
            }
        }
        public void BindLegalEntityData()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            //  ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {

        }

        #region  Import Risk Register and Risk Control Matrix      
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoCompliance.Checked)
                            {
                                bool flag = RiskTransactionSheetsExitsts(xlWorkbook, "RiskControlMatrixCreation");

                                if (flag == true)
                                {
                                    if (CheckSubProcessScore(xlWorkbook))
                                        ProcessRiskCategoryDataNew1(xlWorkbook);
                                    if (suucess == true)//commented by Manisha
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'RiskControlMatrixCreation'.";
                                }
                            }
                            else if (rdoSubProcess.Checked)
                            {
                                bool flag = ProcessSheetsExitsts(xlWorkbook, "ProcessSubProcess");
                                if (flag == true)
                                {
                                    ProcessData(xlWorkbook);
                                    if (suucessProcess == true)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ProcessSubProcess'.";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please select type of data wants to be upload.";
                            }
                           
                           
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool RiskSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("RiskRegistorCreation"))
                    {
                        // if (sheet.Name.Trim().Equals("Act") || sheet.Name.Trim().Equals("Compliance Categories") || sheet.Name.Trim().Equals("Compliance Types"))
                        if (sheet.Name.Trim().Equals("RiskRegistorCreation") || sheet.Name.Trim().Equals("RiskRegistorCreation") || sheet.Name.Trim().Equals("RiskRegistorCreation"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private bool RiskTransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("RiskControlMatrixCreation"))
                    {
                        if (sheet.Name.Trim().Equals("RiskControlMatrixCreation") || sheet.Name.Trim().Equals("RiskControlMatrixCreation") || sheet.Name.Trim().Equals("RiskControlMatrixCreation"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                MasterFileUpload = null;
                //lblMessage1.Text = "";
                //MasterFileUpload1 = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static int GetRiskControlRatingID(string Flag, string Name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                         where row.Name.ToUpper() == Name.ToUpper() && row.IsActive == false
                                         && row.IsRiskControl == Flag
                                         select row.Value).FirstOrDefault();
                return transactionsQuery;
            }
        }

        private bool CheckSubProcessScore(ExcelPackage xlWorkbook)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                bool CheckScoreSuccess = false;

                List<Tuple<int, int, decimal>> ExcelProcesSubProcessList = new List<Tuple<int, int, decimal>>();

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RiskControlMatrixCreation"];

                if (xlWorksheet != null)
                {
                    int count = 0;

                    int xlrow2 = xlWorksheet.Dimension.End.Row;

                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;

                        int processid = -1;
                        int subprocessid = -1;
                        Decimal SubProcessScore = 0;

                   
                        //2 Process 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            processid = ProcessManagement.GetProcessIDByName(xlWorksheet.Cells[i, 2].Text.ToString().Trim(),customerID);
                        }

                        if (processid == 0 || processid == -1)
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process at Row - " + (count + 1) + " or Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            CheckScoreSuccess = true;
                        }

                        //4 Sub Process                    
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            subprocessid = ProcessManagement.GetSubProcessIDByName(xlWorksheet.Cells[i, 4].Text.ToString().Trim(), processid);
                        }

                        if (subprocessid == 0 || subprocessid == -1)
                        {
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process at Row - " + (count + 1) + " or Sub Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            CheckScoreSuccess = true;
                        }

                        //33 SubProcess Score
                        try
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                                SubProcessScore = Convert.ToDecimal(xlWorksheet.Cells[i, 33].Text.Trim());
                            else
                                SubProcessScore = 0;
                        }
                        catch (Exception ex)
                        {
                            SubProcessScore = 0;
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Score at Row - " + (count + 1) + " or Sub Process Score not Defined in the System.";
                            break;
                        }

                        if (CheckScoreSuccess)
                        {
                            ExcelProcesSubProcessList.Add(new Tuple<int, int, decimal>(processid, subprocessid, SubProcessScore));
                        }
                    }

                    var NewList = ExcelProcesSubProcessList.GroupBy(s => new { s.Item1, s.Item2 })
                        .Select(g => new ProcessSubProcessScore
                        {
                            ProcessID = g.Key.Item1,
                            SubProcessID = g.Key.Item2,
                            SubProcessScore = g.Sum(x => x.Item3)
                        }).ToList();

                   
                    foreach (ProcessSubProcessScore EachEntry in NewList)
                    {
                        if (EachEntry.SubProcessScore != ProcessManagement.GetSubProcessScore(EachEntry.ProcessID, EachEntry.SubProcessID))
                        {                           
                            CheckScoreSuccess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Check Sub Process Score of Process -" + ProcessManagement.GetProcessName(EachEntry.ProcessID, customerID) + " and Sub Process -" + ProcessManagement.GetSubProcessName(EachEntry.SubProcessID) + "";
                            break;
                        }
                        else
                            CheckScoreSuccess = true;
                    }                  
                }

                return CheckScoreSuccess;
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }

        private void ProcessRiskCategoryDataNew1(ExcelPackage xlWorkbook)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                List<string> RiskIdProcess = new List<string>();
                List<string> IndustryIdProcess = new List<string>();
                List<string> BranchVerticalID = new List<string>();
                //List<string> BranchIdProcessTest = new List<string>();
                List<string> AssertionsIdProcess = new List<string>();
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RiskControlMatrixCreation"];
                if (xlWorksheet != null)
                {
                    int count = 0;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<RiskCategoryCreation> riskcategorycreationlistTest = new List<RiskCategoryCreation>();
                    List<RiskCategoryCreation> riskcategorycreationlistTest1 = new List<RiskCategoryCreation>();
                    List<RiskCategoryCreation> riskcategorycreationlist = new List<RiskCategoryCreation>();
                    List<RiskCategoryCreation> riskcategorycreationlist1 = new List<RiskCategoryCreation>();
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;
                        string ControlDescription = string.Empty;
                        string ControlNo = "";
                        int processid = -1;
                        int subprocessid = -1;
                        string ActivityDescription = "";
                        string ControlObjective = "";
                        //1 Control No                      
                      
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            ControlNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                        }
                        //2 Process 
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            processid = ProcessManagement.GetProcessIDByName(xlWorksheet.Cells[i, 2].Text.ToString().Trim(),customerID);
                        }
                        if (processid == 0 || processid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process Name at row number - " + (count + 1) + " or Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //4 Sub Process                    
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            subprocessid = ProcessManagement.GetSubProcessIDByName(xlWorksheet.Cells[i, 4].Text.ToString().Trim(), processid);
                        }
                        if (subprocessid == 0 || subprocessid == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Name at row number - " + (count + 1) + " or Sub Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //5 Risk Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                        {
                            ActivityDescription = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                        }
                        //6 Control Objective
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                        {
                            ControlObjective = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                        }

                        #region 7 Riskcatrgory Mapping add to List                       
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                        {
                            string risk = xlWorksheet.Cells[i, 7].Text.ToString().Trim();
                            string[] split = risk.Split(',');
                            if (split.Length > 0)
                            {
                                string finalriskcategoryid = "";
                                string riskcategoryid = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    riskcategoryid = Convert.ToString(RiskCategoryManagement.GetriskcategoryIDByName(split[rs].ToString().Trim()));

                                    if (riskcategoryid == "0" || riskcategoryid == "-1")
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Correct the Risk catrgory Name at row number - " + (count + 1) + " or Risk catrgory Name not Defined in the System.";
                                        break;
                                    }
                                    else
                                    {
                                        suucess = true;
                                    }
                                    if (split.Length > 1)
                                    {
                                        finalriskcategoryid += riskcategoryid + ",";
                                    }
                                    else
                                    {
                                        finalriskcategoryid += riskcategoryid;
                                    }
                                }
                                if (suucess)
                                {
                                    RiskIdProcess.Add(finalriskcategoryid.Trim(','));
                                }                               
                            }
                        }
                        else
                        {
                            RiskIdProcess.Add("0");
                        }
                        if (suucess==false)
                        {
                            break;
                        }
                        #endregion
                        //8 Customer Branch
                        int Customerbranchidcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        {
                            Customerbranchidcheck = RiskCategoryManagement.GetBranchIdByName(xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                        }
                        if (Customerbranchidcheck == 0 || Customerbranchidcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Customer Branch Name at row number - " + (count + 1) + " or Customer Branch not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }

                        #region 9 Verticals Mapping add to List                       
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            string verticals = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                            string[] split = verticals.Split(',');
                            if (split.Length > 0)
                            {
                                string finalverticalsid = "";
                                string verticalsid = "";
                                for (int rs = 0; rs < split.Length; rs++)
                                {
                                    verticalsid = Convert.ToString(RiskCategoryManagement.GetBranchVerticalIDByName(customerID, split[rs].ToString().Trim()));
                                    if (verticalsid == "0" || verticalsid == "-1")
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Correct the Verticals Name at row number - " + (count + 1) + " or Verticals Name not Defined in the System.";
                                        break;
                                    }
                                    else
                                    {
                                        suucess = true;
                                    }
                                    if (split.Length > 1)
                                    {
                                        finalverticalsid += verticalsid + ",";
                                    }
                                    else
                                    {
                                        finalverticalsid += verticalsid;
                                    }
                                }
                                if (suucess)
                                {
                                    BranchVerticalID.Add(finalverticalsid.Trim(','));
                                }                                
                            }
                        }
                        else
                        {
                            BranchVerticalID.Add("0");
                        }
                        #endregion
                        if (suucess == false)
                        {
                            break;
                        }
                        #region 10 Assertions Mapping   add to List
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                        {
                            string Assertions = xlWorksheet.Cells[i, 10].Text.ToString().Trim();
                            string[] splitAssertions = Assertions.Split(',');
                            if (splitAssertions.Length > 0)
                            {
                                string finalAssertionsmappigid = "";
                                string Assertionsmappigid = "";
                                for (int rs = 0; rs < splitAssertions.Length; rs++)
                                {
                                    Assertionsmappigid = Convert.ToString(RiskCategoryManagement.GetAssertionsIdByName(splitAssertions[rs].ToString().Trim()));
                                    if (Assertionsmappigid == "0" || Assertionsmappigid == "-1")
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Correct the Assertions at row number - " + (count + 1) + " or Assertions not Defined in the System.";
                                        break;
                                    }
                                    else
                                    {
                                        suucess = true;
                                    }

                                    if (splitAssertions.Length > 1)
                                    {
                                        finalAssertionsmappigid += Assertionsmappigid + ",";
                                    }
                                    else
                                    {
                                        finalAssertionsmappigid += Assertionsmappigid;
                                    }
                                }
                                if (suucess)
                                {
                                    AssertionsIdProcess.Add(finalAssertionsmappigid.Trim(','));
                                }
                                
                            }
                        }
                        else
                        {
                            AssertionsIdProcess.Add("0");
                        }
                        #endregion
                        if (suucess == false)
                        {
                            break;
                        }
                        //11 Control Description
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                        {
                            ControlDescription = Convert.ToString(xlWorksheet.Cells[i, 11].Text).Trim();
                        }
                        //13 Person Responsible
                        int personresponsibleIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                            {
                                //14 Email
                                personresponsibleIDcheck = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 14].Text.ToString().Trim());
                            }
                        }
                        if (personresponsibleIDcheck == 0 || personresponsibleIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Person Responsible Name at row number - " + (count + 1) + " or Person Responsible not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //16 Key
                        int KeyIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                        {
                            KeyIDcheck = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                        }
                        if (KeyIDcheck == 0 || KeyIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Key/Non Key at row number - " + (count + 1) + " or Key/Non Key not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //17 Preventive Control
                        int PrevationControlIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                        {
                            PrevationControlIDcheck = RiskCategoryManagement.GetPrevationControlIdByName(xlWorksheet.Cells[i, 17].Text.ToString().Trim());
                        }
                        if (PrevationControlIDcheck == 0 || PrevationControlIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Preventive/Detective Control Name at row number - " + (count + 1) + " or Preventive/Detective not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //18 Automated Control
                        int AutomatedControlIDcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                        {
                            AutomatedControlIDcheck = RiskCategoryManagement.GetAutomatedControlIDByName(xlWorksheet.Cells[i, 18].Text.ToString().Trim());
                        }
                        if (AutomatedControlIDcheck == 0 || AutomatedControlIDcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Automated/Manual Control Name at row number - " + (count + 1) + " or Automated/Manual Control not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //19 Frequency
                        int Frequencycheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                        {
                            Frequencycheck = RiskCategoryManagement.GetFrequencyIDByName(xlWorksheet.Cells[i, 19].Text.ToString().Trim());
                        }
                        if (Frequencycheck == 0 || Frequencycheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Frquency Name at row number - " + (count + 1) + " or Frquency not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //29 Industry
                        #region 29 Industry Mapping   add to List
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text.ToString().Trim()))
                        {
                            string industry = xlWorksheet.Cells[i, 29].Text.ToString().Trim();
                            string[] splitindustry = industry.Split(',');
                            if (splitindustry.Length > 0)
                            {
                                string finalindustrymappigid = "";
                                string industrymappigid = "";
                                for (int rs = 0; rs < splitindustry.Length; rs++)
                                {
                                    industrymappigid = Convert.ToString(RiskCategoryManagement.GetIndustryIdByName(splitindustry[rs].ToString().Trim()));

                                    if (industrymappigid == "0" || industrymappigid == "-1")
                                    {
                                        suucess = false;
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Please Correct the Industry at row number - " + (count + 1) + " or Industry not Defined in the System.";
                                        break;
                                    }
                                    else
                                    {
                                        suucess = true;
                                    }

                                    if (splitindustry.Length > 1)
                                    {
                                        finalindustrymappigid += industrymappigid + ",";
                                    }
                                    else
                                    {
                                        finalindustrymappigid += industrymappigid;
                                    }
                                }
                                IndustryIdProcess.Add(finalindustrymappigid.Trim(','));
                            }
                        }
                        else
                        {
                            IndustryIdProcess.Add("0");
                        }
                        #endregion

                        //30 Location Type
                        int LocationType = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                        {
                            LocationType = ProcessManagement.GetLocationTypeID(Convert.ToString(xlWorksheet.Cells[i, 30].Text).Trim());
                        }
                        if (LocationType == 0 || LocationType == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Location Type at row number - " + (count + 1) + " or Location Type not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //31 Risk Rating
                        int RiskRatingcheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                        {
                            RiskRatingcheck = GetRiskControlRatingID("R", Convert.ToString(xlWorksheet.Cells[i, 31].Text).Trim());
                        }
                        if (RiskRatingcheck == 0 || RiskRatingcheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Risk Rating at row number - " + (count + 1) + " or Risk Rating not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //32 Control Rating
                        int ControlRatingCheck = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                        {
                            ControlRatingCheck = GetRiskControlRatingID("C", Convert.ToString(xlWorksheet.Cells[i, 31].Text).Trim());
                        }
                        if (ControlRatingCheck == 0 || ControlRatingCheck == -1)
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Control Rating at row number - " + (count + 1) + " or Control Rating not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucess = true;
                        }
                        //33 Process Scores
                        int processscores = -1;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()))
                        {
                            processscores = Convert.ToInt32(xlWorksheet.Cells[i, 33].Text);
                        }



                        if (!(RiskCategoryManagement.ExistsNew(Convert.ToString(ActivityDescription), Convert.ToString(ControlObjective), Convert.ToInt32(processid), Convert.ToInt32(LocationType), customerID, Customerbranchidcheck, Convert.ToInt32(subprocessid),ControlNo)))
                        {
                            if (!riskcategorycreationlist.Any(x => x.ActivityDescription.ToUpper().Trim() == ActivityDescription.ToUpper().Trim()
                            && x.ControlObjective.ToUpper().Trim() == ControlObjective.ToUpper().Trim() && x.ProcessId == processid
                            && x.LocationType == LocationType && x.CustomerId == customerID 
                            && x.CustomerBranchId == Customerbranchidcheck  && x.ControlNo.ToUpper().Trim()==ControlNo.ToUpper().Trim()))
                            {
                                RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation();
                                riskcategorycreation.ControlNo = ControlNo;
                                riskcategorycreation.ProcessId = processid;
                                riskcategorycreation.SubProcessId = subprocessid;
                                riskcategorycreation.ActivityDescription = ActivityDescription;
                                riskcategorycreation.ControlObjective = ControlObjective;
                                riskcategorycreation.IsDeleted = false;
                                riskcategorycreation.LocationType = LocationType;
                                riskcategorycreation.IsInternalAudit = "N";
                                riskcategorycreation.CustomerId = customerID;
                                riskcategorycreation.CustomerBranchId = Customerbranchidcheck;
                                riskcategorycreationlist.Add(riskcategorycreation);
                            }
                        }//exists end
                        else
                        {
                            //New Condition Added by rahul on 23 SEP 2016
                            if (!riskcategorycreationlistTest.Any(x => x.ActivityDescription == ActivityDescription && x.ControlObjective == ControlObjective
                                && x.ProcessId == processid && x.LocationType == LocationType && x.CustomerId == customerID && x.CustomerBranchId == Customerbranchidcheck))
                            {
                                RiskCategoryCreation riskcategorycreationtest = new RiskCategoryCreation();
                                riskcategorycreationtest.ControlNo = ControlNo;
                                riskcategorycreationtest.ProcessId = processid;
                                riskcategorycreationtest.SubProcessId = subprocessid;
                                riskcategorycreationtest.ActivityDescription = ActivityDescription;
                                riskcategorycreationtest.ControlObjective = ControlObjective;
                                riskcategorycreationtest.IsDeleted = false;
                                riskcategorycreationtest.LocationType = LocationType;
                                riskcategorycreationtest.IsInternalAudit = "N";
                                riskcategorycreationtest.CustomerId = customerID;
                                riskcategorycreationtest.CustomerBranchId = Customerbranchidcheck;
                                riskcategorycreationlistTest.Add(riskcategorycreationtest);
                            }
                        }
                    }
                    if (suucess)
                    {
                        riskcategorycreationlist1 = riskcategorycreationlist.Where(entry => entry.ProcessId == 0 && entry.SubProcessId == 0).Distinct().ToList();
                        if (riskcategorycreationlist1.Count == 0)
                        {
                            suucess = RiskCategoryManagement.CreateExcel(riskcategorycreationlist);
                            #region Riskcatrgory Mapping Save
                            if (RiskIdProcess.Count > 0)
                            {
                                riskcategorycreationlist.ForEach(entry =>
                                {
                                    for (int i = 0; i < RiskIdProcess.Count; i++)
                                    {
                                        string aItem = RiskIdProcess[i].ToString();
                                        if (aItem.ToString().Contains(","))
                                        {
                                            string[] split = aItem.ToString().Split(',');
                                            if (split.Length > 0)
                                            {
                                                for (int rs = 0; rs < split.Length; rs++)
                                                {
                                                    RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                                    {
                                                        RiskCategoryCreationId = entry.Id,
                                                        RiskCategoryId = Convert.ToInt32(split[rs]),
                                                        ProcessId = entry.ProcessId,
                                                        IsActive = true,
                                                        SubProcessId = entry.SubProcessId,
                                                    };
                                                    RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                                }
                                                RiskIdProcess.Remove(aItem);
                                                break;
                                            }

                                        }
                                        else
                                        {
                                            if (aItem != "0")
                                            {
                                                RiskCategoryMapping riskcategorymapping = new RiskCategoryMapping()
                                                {
                                                    RiskCategoryCreationId = entry.Id,
                                                    RiskCategoryId = Convert.ToInt32(aItem),
                                                    ProcessId = entry.ProcessId,
                                                    IsActive = true,
                                                    SubProcessId = entry.SubProcessId,
                                                };
                                                RiskCategoryManagement.CreateRiskCategoryMapping(riskcategorymapping);
                                            }

                                            RiskIdProcess.Remove(aItem);
                                            break;
                                        }
                                    }
                                });

                                RiskIdProcess.Clear();
                            }
                            #endregion
                            #region Industry Mapping Save
                            if (IndustryIdProcess.Count > 0)
                            {
                                riskcategorycreationlist.ForEach(entry =>
                                {
                                    for (int i = 0; i < IndustryIdProcess.Count; i++)
                                    {
                                        string aItem = IndustryIdProcess[i].ToString();
                                        if (aItem.ToString().Contains(","))
                                        {
                                            string[] split = aItem.ToString().Split(',');
                                            if (split.Length > 0)
                                            {
                                                for (int rs = 0; rs < split.Length; rs++)
                                                {
                                                    IndustryMapping IndustryMapping = new IndustryMapping()
                                                    {
                                                        RiskCategoryCreationId = entry.Id,
                                                        IndustryID = Convert.ToInt32(split[rs]),
                                                        IsActive = true,
                                                        EditedDate = DateTime.Now,
                                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                                        ProcessId = entry.ProcessId,
                                                        SubProcessId = entry.SubProcessId,
                                                    };
                                                    RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                                }
                                                IndustryIdProcess.Remove(aItem);
                                                break;
                                            }

                                        }
                                        else
                                        {
                                            if (aItem != "0")
                                            {
                                                IndustryMapping IndustryMapping = new IndustryMapping()
                                                {
                                                    RiskCategoryCreationId = entry.Id,
                                                    IndustryID = Convert.ToInt32(aItem),
                                                    IsActive = true,
                                                    EditedDate = DateTime.Now,
                                                    EditedBy = Convert.ToInt32(Session["userID"]),
                                                    ProcessId = entry.ProcessId,
                                                    SubProcessId = entry.SubProcessId,
                                                };
                                                RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                            }
                                            IndustryIdProcess.Remove(aItem);
                                            break;
                                        }
                                    }
                                });

                                IndustryIdProcess.Clear();
                            }
                            #endregion

                            #region Assertions Mapping Save
                            if (AssertionsIdProcess.Count > 0)
                            {
                                riskcategorycreationlist.ForEach(entry =>
                                {
                                    for (int i = 0; i < AssertionsIdProcess.Count; i++)
                                    {
                                        string aItem = AssertionsIdProcess[i].ToString();
                                        if (aItem.ToString().Contains(","))
                                        {
                                            string[] split = aItem.ToString().Split(',');
                                            if (split.Length > 0)
                                            {
                                                for (int rs = 0; rs < split.Length; rs++)
                                                {
                                                    AssertionsMapping assertionsmapping = new AssertionsMapping()
                                                    {
                                                        RiskCategoryCreationId = entry.Id,
                                                        AssertionId = Convert.ToInt32(split[rs]),
                                                        ProcessId = entry.ProcessId,
                                                        IsActive = true,
                                                        SubProcessId = entry.SubProcessId,
                                                    };
                                                    RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                                }
                                                AssertionsIdProcess.Remove(aItem);
                                                break;
                                            }

                                        }
                                        else
                                        {
                                            if (aItem != "0")
                                            {
                                                AssertionsMapping assertionsmapping = new AssertionsMapping()
                                                {
                                                    RiskCategoryCreationId = entry.Id,
                                                    AssertionId = Convert.ToInt32(aItem),
                                                    ProcessId = entry.ProcessId,
                                                    IsActive = true,
                                                    SubProcessId = entry.SubProcessId,
                                                };
                                                RiskCategoryManagement.CreateAssertionsMapping(assertionsmapping);
                                            }
                                            AssertionsIdProcess.Remove(aItem);
                                            break;
                                        }
                                    }
                                });

                                AssertionsIdProcess.Clear();
                            }
                            #endregion

                            List<RiskActivityTransaction> riskactivitytransactionlist = new List<RiskActivityTransaction>();
                            List<RiskActivityTransaction> riskactivitytransactionlist1 = new List<RiskActivityTransaction>();
                            for (int i = 2; i <= xlrow2; i++)
                            {
                                string ControlNo = "";
                                int processid = -1;
                                int subprocessid = -1;
                                string ActivityDescription = "";
                                string ControlObjective = "";
                                string ControlDescription = string.Empty;
                                string MControlDescription = string.Empty;
                                int personresponsibleID = 0;
                                int KeyID = 0;
                                int PrevationControlID = 0;
                                int AutomatedControlID = 0;
                                int Frequency = 0;
                                string GapDescription = "";
                                string Recommendations = "";
                                string ActionRemediationplan = "";
                                string IPE = "";
                                string ERPsystem = "";
                                string FRC = "";
                                string UniqueReferred = "";
                                string TestStrategy = "";
                                string DocumentsExamined = "";                                
                                int Customerbranchid = -1;
                                int LocationType = -1;
                                int riskcreationId = -1;                                
                                //1 Control No
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                                {
                                    ControlNo = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                                }
                                //2 ProcessName	
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                                {
                                    processid = ProcessManagement.GetProcessIDByName(xlWorksheet.Cells[i, 2].Text.ToString().Trim(),customerID);
                                }
                                //4 SubProcessName
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                                {
                                    subprocessid = ProcessManagement.GetSubProcessIDByName(xlWorksheet.Cells[i, 4].Text.ToString().Trim(), processid);
                                }
                                //5 Risk Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                                {
                                    ActivityDescription = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                                }
                                //6 Control Objective
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                                {
                                    ControlObjective = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                }
                                //8 Location
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString()))
                                {
                                    Customerbranchid = RiskCategoryManagement.GetBranchIdByName(xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                                }
                                //11 Control Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                {
                                    ControlDescription = Convert.ToString(xlWorksheet.Cells[i, 11].Text).Trim();
                                }
                                //12 Mitigating Control Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                {
                                    MControlDescription = Convert.ToString(xlWorksheet.Cells[i, 12].Text).Trim();
                                }
                                //13 Person Responsible
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                    {
                                        personresponsibleID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), customerID, xlWorksheet.Cells[i, 14].Text.ToString().Trim());
                                    }
                                }
                                //15 Effective Date	                          
                                DateTime dt = new DateTime();
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                                {
                                    string c = Convert.ToString(xlWorksheet.Cells[i, 15].Text).Trim();
                                    DateTime? aaaa = CleanDateField(c);
                                    DateTime dt1 = Convert.ToDateTime(aaaa);
                                    dt = GetDate(dt1.ToString("dd/MM/yyyy"));
                                }
                                //16 Key
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                                {
                                    KeyID = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 16].Text.ToString().Trim());
                                }
                                //17 Preventive Control
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                                {
                                    PrevationControlID = RiskCategoryManagement.GetPrevationControlIdByName(xlWorksheet.Cells[i, 17].Text.ToString().Trim());
                                }
                                //18 Automated Control
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                                {
                                    AutomatedControlID = RiskCategoryManagement.GetAutomatedControlIDByName(xlWorksheet.Cells[i, 18].Text.ToString().Trim());
                                }
                                //19 Frequency
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                                {
                                    Frequency = RiskCategoryManagement.GetFrequencyIDByName(xlWorksheet.Cells[i, 19].Text.ToString().Trim());
                                }
                                //20 Gap Description
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                                {
                                    GapDescription = Convert.ToString(xlWorksheet.Cells[i, 20].Text.ToString().Trim());
                                }
                                //21 Recommendations
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                                {
                                    Recommendations = Convert.ToString(xlWorksheet.Cells[i, 21].Text.ToString().Trim());
                                }
                                //22 Action Remediation Plan
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()))
                                {
                                    ActionRemediationplan = Convert.ToString(xlWorksheet.Cells[i, 22].Text.ToString().Trim());
                                }
                                //23 Information Produced by Entity
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()))
                                {
                                    IPE = Convert.ToString(xlWorksheet.Cells[i, 23].Text.ToString().Trim());
                                }
                                //24 ERP System	
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                                {
                                    ERPsystem = Convert.ToString(xlWorksheet.Cells[i, 24].Text.ToString().Trim());
                                }
                                //25 Fraud Risk Control	
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                                {
                                    FRC = Convert.ToString(xlWorksheet.Cells[i, 25].Text.ToString().Trim());
                                }
                                //26 Unique Referred
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim().Trim()))
                                {
                                    UniqueReferred = Convert.ToString(xlWorksheet.Cells[i, 26].Text.ToString().Trim());
                                }
                                //27 Test Strategy
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim().Trim()))
                                {
                                    TestStrategy = Convert.ToString(xlWorksheet.Cells[i, 27].Text.ToString().Trim());
                                }
                                //28 Documents Examined
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                                {
                                    DocumentsExamined = Convert.ToString(xlWorksheet.Cells[i, 28].Text.ToString().Trim());
                                }
                                //30 Location Type
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                                {
                                    LocationType = ProcessManagement.GetLocationTypeID(Convert.ToString(xlWorksheet.Cells[i, 30].Text).Trim());
                                }
                                //31 RiskRating
                                int RiskRating = -1;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                                {
                                    RiskRating = GetRiskControlRatingID("R", Convert.ToString(xlWorksheet.Cells[i, 31].Text).Trim());
                                }
                                //32 ControlRating
                                int ControlRating = -1;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                                {
                                    ControlRating = GetRiskControlRatingID("C", Convert.ToString(xlWorksheet.Cells[i, 32].Text).Trim());
                                }
                                int processscores = -1;
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()))
                                {
                                    processscores = Convert.ToInt32(xlWorksheet.Cells[i, 33].Text);
                                }
                                riskcreationId = RiskCategoryManagement.GetRiskCreationIdNew(Convert.ToString(ActivityDescription), Convert.ToString(ControlObjective), Convert.ToInt32(processid), Convert.ToInt32(LocationType), customerID, Customerbranchid,subprocessid, ControlNo);
                                if (!(RiskCategoryManagement.Exists(processid, subprocessid, riskcreationId, Convert.ToInt32(Frequency), Customerbranchid)))
                                {
                                    if (BranchVerticalID.Count > 0)
                                    {
                                        for (int BV = 0; BV < BranchVerticalID.Count; BV++)
                                        {
                                            string aItem = BranchVerticalID[BV].ToString();
                                            if (aItem.ToString().Contains(","))
                                            {
                                                string[] split = aItem.ToString().Split(',');
                                                if (split.Length > 0)
                                                {
                                                    for (int rs = 0; rs < split.Length; rs++)
                                                    {
                                                        RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                        riskactivitytransaction.ProcessId = processid;
                                                        riskactivitytransaction.SubProcessId = subprocessid;
                                                        riskactivitytransaction.RiskCreationId = riskcreationId;
                                                        riskactivitytransaction.CustomerBranchId = Customerbranchid;
                                                        riskactivitytransaction.ControlDescription = ControlDescription;
                                                        riskactivitytransaction.MControlDescription = MControlDescription;
                                                        riskactivitytransaction.PersonResponsible = personresponsibleID;
                                                        riskactivitytransaction.EffectiveDate = dt.Date;
                                                        riskactivitytransaction.Key_Value = KeyID;
                                                        riskactivitytransaction.PrevationControl = PrevationControlID;
                                                        riskactivitytransaction.AutomatedControl = AutomatedControlID;
                                                        riskactivitytransaction.Frequency = Frequency;
                                                        riskactivitytransaction.IsDeleted = false;
                                                        riskactivitytransaction.GapDescription = GapDescription;
                                                        riskactivitytransaction.Recommendations = Recommendations;
                                                        riskactivitytransaction.ActionRemediationplan = ActionRemediationplan;
                                                        riskactivitytransaction.IPE = IPE;
                                                        riskactivitytransaction.ERPsystem = ERPsystem;
                                                        riskactivitytransaction.FRC = FRC;
                                                        riskactivitytransaction.UniqueReferred = UniqueReferred;
                                                        riskactivitytransaction.TestStrategy = TestStrategy;
                                                        riskactivitytransaction.RiskRating = RiskRating;
                                                        riskactivitytransaction.ControlRating = ControlRating;
                                                        riskactivitytransaction.ProcessScore = processscores;
                                                        riskactivitytransaction.VerticalsId = Convert.ToInt32(split[rs]);
                                                        riskactivitytransaction.ISICFR_Close = true;
                                                        riskactivitytransactionlist.Add(riskactivitytransaction);
                                                    }
                                                    BranchVerticalID.Remove(aItem);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (aItem != "0")
                                                {

                                                    RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                                                    riskactivitytransaction.ProcessId = processid;
                                                    riskactivitytransaction.SubProcessId = subprocessid;
                                                    riskactivitytransaction.RiskCreationId = riskcreationId;
                                                    riskactivitytransaction.CustomerBranchId = Customerbranchid;
                                                    riskactivitytransaction.ControlDescription = ControlDescription;
                                                    riskactivitytransaction.MControlDescription = MControlDescription;
                                                    riskactivitytransaction.PersonResponsible = personresponsibleID;
                                                    riskactivitytransaction.EffectiveDate = dt.Date;
                                                    riskactivitytransaction.Key_Value = KeyID;
                                                    riskactivitytransaction.PrevationControl = PrevationControlID;
                                                    riskactivitytransaction.AutomatedControl = AutomatedControlID;
                                                    riskactivitytransaction.Frequency = Frequency;
                                                    riskactivitytransaction.IsDeleted = false;
                                                    riskactivitytransaction.GapDescription = GapDescription;
                                                    riskactivitytransaction.Recommendations = Recommendations;
                                                    riskactivitytransaction.ActionRemediationplan = ActionRemediationplan;
                                                    riskactivitytransaction.IPE = IPE;
                                                    riskactivitytransaction.ERPsystem = ERPsystem;
                                                    riskactivitytransaction.FRC = FRC;
                                                    riskactivitytransaction.UniqueReferred = UniqueReferred;
                                                    riskactivitytransaction.TestStrategy = TestStrategy;
                                                    riskactivitytransaction.RiskRating = RiskRating;
                                                    riskactivitytransaction.ControlRating = ControlRating;
                                                    riskactivitytransaction.ProcessScore = processscores;
                                                    riskactivitytransaction.VerticalsId = Convert.ToInt32(aItem);
                                                    riskactivitytransaction.ISICFR_Close = true;
                                                    riskactivitytransactionlist.Add(riskactivitytransaction);
                                                }
                                                BranchVerticalID.Remove(aItem);
                                                break;
                                            }
                                        }
                                    }
                                }//exists end                                  
                            }//End Of Second For  Loop  
                            //BranchVerticalID.Clear();
                            riskactivitytransactionlist1 = riskactivitytransactionlist.Where(entry => entry.ProcessId == 0 && entry.RiskCreationId == 0).ToList();
                            if (riskactivitytransactionlist1.Count == 0)
                            {
                                suucess = RiskCategoryManagement.CreateExcelTransaction(riskactivitytransactionlist);
                                suucess = true;
                            }
                            else
                            {
                                suucess = false;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "File Upload Error!!. Please Upload the Excel File in Appropriate Format.";
                            }
                        }
                        else
                        {
                            suucess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "File Upload Error!!. Please Upload the Excel File in Appropriate Format.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }
        private void ProcessRiskCategoryTransactionData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["RiskControlMatrixCreation"];
                if (xlWorksheet != null)
                {

                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<RiskActivityTransaction> riskcategorycreationlist = new List<RiskActivityTransaction>();
                    List<RiskActivityTransaction> riskcategorycreationlist1 = new List<RiskActivityTransaction>();
                    int customerID = -1;
                    customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        string ControlNo = "";
                        int riskcreationId = 0;
                        int processid = -1;
                        int subprocessid = -1;
                        int customerbranchid = 0;
                        int assertionid = 0;
                        int personresponsibleID = 0;
                        int KeyID = 0;
                        int PrevationControlID = 0;
                        int AutomatedControlID = 0;
                        int Frequency = 0;
                        string GapDescription = "";
                        string Recommendations = "";
                        string ActionRemediationplan = "";
                        string IPE = "";
                        string ERPsystem = "";
                        string FRC = "";
                        string UniqueReferred = "";
                        string TestStrategy = "";
                        string DocumentsExamined = "";

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Value.ToString()))
                        {
                            ControlNo = Convert.ToString(xlWorksheet.Cells[i, 1].Value);
                        }

                        if (xlWorksheet.Cells[i, 2].Value != null)
                        {
                            riskcreationId = Convert.ToInt32(xlWorksheet.Cells[i, 2].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 3].Value != null)
                        {
                            processid = ProcessManagement.GetProcessIDByName(xlWorksheet.Cells[i, 3].Value.ToString(),customerID);
                        }
                        if (xlWorksheet.Cells[i, 4].Value != null)
                        {
                            subprocessid = ProcessManagement.GetSubProcessIDByName(xlWorksheet.Cells[i, 4].Value.ToString(), processid);
                        }
                        if (xlWorksheet.Cells[i, 8].Value != null)
                        {
                            customerbranchid = RiskCategoryManagement.GetCustomerBranchIDByName(xlWorksheet.Cells[i, 8].Value.ToString());
                        }

                        if (xlWorksheet.Cells[i, 12].Value != null)
                        {
                            if (xlWorksheet.Cells[i, 13].Value != null)
                            {
                                personresponsibleID = RiskCategoryManagement.GetUserIDByName(xlWorksheet.Cells[i, 12].Value.ToString(), customerID, xlWorksheet.Cells[i, 13].Value.ToString());
                            }
                        }

                        if (xlWorksheet.Cells[i, 15].Value != null)
                        {
                            KeyID = RiskCategoryManagement.GetKeyIdByName(xlWorksheet.Cells[i, 15].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 16].Value != null)
                        {
                            PrevationControlID = RiskCategoryManagement.GetPrevationControlIdByName(xlWorksheet.Cells[i, 16].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 17].Value != null)
                        {
                            AutomatedControlID = RiskCategoryManagement.GetAutomatedControlIDByName(xlWorksheet.Cells[i, 17].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 18].Value != null)
                        {
                            Frequency = RiskCategoryManagement.GetFrequencyIDByName(xlWorksheet.Cells[i, 18].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 19].Value != null)
                        {
                            GapDescription = Convert.ToString(xlWorksheet.Cells[i, 19].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 20].Value != null)
                        {
                            Recommendations = Convert.ToString(xlWorksheet.Cells[i, 20].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 21].Value != null)
                        {
                            ActionRemediationplan = Convert.ToString(xlWorksheet.Cells[i, 21].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 22].Value != null)
                        {
                            IPE = Convert.ToString(xlWorksheet.Cells[i, 22].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 23].Value != null)
                        {
                            ERPsystem = Convert.ToString(xlWorksheet.Cells[i, 23].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 24].Value != null)
                        {
                            FRC = Convert.ToString(xlWorksheet.Cells[i, 24].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 25].Value != null)
                        {
                            UniqueReferred = Convert.ToString(xlWorksheet.Cells[i, 25].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 26].Value != null)
                        {
                            TestStrategy = Convert.ToString(xlWorksheet.Cells[i, 26].Value.ToString());
                        }
                        if (xlWorksheet.Cells[i, 27].Value != null)
                        {
                            DocumentsExamined = Convert.ToString(xlWorksheet.Cells[i, 27].Value.ToString());
                        }

                        DateTime dt = new DateTime();
                        if (xlWorksheet.Cells[i, 14].Value != null)
                        {
                            string c = Convert.ToString(xlWorksheet.Cells[i, 14].Value);
                            DateTime? aaaa = CleanDateField(c);
                            DateTime dt1 = Convert.ToDateTime(aaaa);
                            dt = GetDate(dt1.ToString("dd/MM/yyyy"));
                        }
                        if (!(RiskCategoryManagement.Exists(processid, subprocessid, riskcreationId, Convert.ToInt32(Frequency), customerbranchid)))
                        {
                            RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                            riskactivitytransaction.ProcessId = processid;
                            riskactivitytransaction.SubProcessId = subprocessid;
                            riskactivitytransaction.RiskCreationId = riskcreationId;
                            riskactivitytransaction.CustomerBranchId = customerbranchid;
                            //riskactivitytransaction.Assertion = assertionid;
                            riskactivitytransaction.ControlDescription = Convert.ToString(xlWorksheet.Cells[i, 10].Value);
                            riskactivitytransaction.MControlDescription = Convert.ToString(xlWorksheet.Cells[i, 11].Value);
                            riskactivitytransaction.PersonResponsible = personresponsibleID;
                            riskactivitytransaction.EffectiveDate = dt.Date;
                            riskactivitytransaction.Key_Value = KeyID;
                            riskactivitytransaction.PrevationControl = PrevationControlID;
                            riskactivitytransaction.AutomatedControl = AutomatedControlID;
                            riskactivitytransaction.Frequency = Frequency;
                            riskactivitytransaction.IsDeleted = false;
                            riskactivitytransaction.GapDescription = GapDescription;
                            riskactivitytransaction.Recommendations = Recommendations;
                            riskactivitytransaction.ActionRemediationplan = ActionRemediationplan;
                            riskactivitytransaction.IPE = IPE;
                            riskactivitytransaction.ERPsystem = ERPsystem;
                            riskactivitytransaction.FRC = FRC;
                            riskactivitytransaction.UniqueReferred = UniqueReferred;
                            riskactivitytransaction.TestStrategy = TestStrategy;
                            riskactivitytransaction.DocumentsExamined = DocumentsExamined;
                            riskcategorycreationlist.Add(riskactivitytransaction);
                        }//exists end
                    }
                    riskcategorycreationlist1 = riskcategorycreationlist.Where(entry => entry.ProcessId == 0).ToList();

                    if (riskcategorycreationlist1.Count > 1)
                    {
                        suucess = false;
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "File Upload Error!!. Please Upload the Excel File in Appropriate Format.";
                    }
                    else
                    {
                        suucess = RiskCategoryManagement.CreateExcelTransaction(riskcategorycreationlist);
                        suucess = true;
                    }

                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        #region Export  Risk Control Matrix
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
        protected void btnUploadFile1_Click(object sender, EventArgs e)
        {
            try
            {

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RiskControlMatrixCreation.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                using (StringWriter sw = new StringWriter())
                {
                    HtmlTextWriter hw = new HtmlTextWriter(sw);
                    //To Export all pages
                    GridExportExcel.AllowPaging = false;
                    this.BindDataExport(ViewState["Flagvalue"].ToString());
                    GridExportExcel.HeaderRow.BackColor = Color.CornflowerBlue;
                    //GridExportExcel.BorderStyle = BorderStyle.Solid;
                    //GridExportExcel.BorderColor = Color.Black;
                    GridExportExcel.HeaderStyle.BorderStyle = BorderStyle.Solid;
                    GridExportExcel.HeaderStyle.BorderColor = Color.Black;
                    foreach (TableCell cell in GridExportExcel.HeaderRow.Cells)
                    {
                        cell.BackColor = GridExportExcel.HeaderStyle.BackColor;
                    }
                    GridExportExcel.RenderControl(hw);
                    string style = @"<style> .textmode { } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindCustomer()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("Select Location", "-1"));
        }

        public void BindProcess(int branchid)
        {

            ddlProcess.DataTextField = "Name";
            ddlProcess.DataValueField = "ID";
            ddlProcess.Items.Clear();
            ddlProcess.DataSource = UserManagementRisk.FillCustomerCreationProcess(branchid);
            ddlProcess.DataBind();
            ddlProcess.Items.Insert(0, new ListItem("Select Process", "-1"));
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.SelectedValue != null)
            {
                if (ddlFilterLocation.SelectedValue != "Select Location")
                {
                    BindProcess(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                    ViewState["Flagvalue"] = null;
                    ViewState["Flagvalue"] = "L";
                    BindDataExport("L");
                    // bindGridData();
                }
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {


        }


        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProcess.SelectedValue != null)
            {
                if (ddlProcess.SelectedValue != "Select Process")
                {
                    ViewState["Flagvalue"] = null;
                    ViewState["Flagvalue"] = "P";
                    BindDataExport("P");
                    // bindGridData();
                }
            }
        }
        public string ShowLocationType(int locationtypeid)
        {
            string LocationType = "";
            LocationType = ProcessManagement.GetLocationTypeName(locationtypeid);
            return LocationType;
        }

        public string ShowProcessName(long processId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetProcessName(processId,customerID);
            return processnonprocess;
        }
        public string ShowSubProcessName(long processId, long SubprocessId)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetSubProcessName(processId, SubprocessId);
            return processnonprocess;
        }
        public string ShowRiskCategoryName(long categoryid)
        {
            List<RiskCategoryName_Result> a = new List<RiskCategoryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetRiskCategoryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowIndustryName(long categoryid)
        {
            List<IndustryName_Result> a = new List<IndustryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetIndustryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowAssertionsName(long categoryid)
        {
            List<AssertionsName_Result> a = new List<AssertionsName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetAssertionsNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowCustomerBranchName(long categoryid, long branchid)
        {
            List<CustomerBranchNameLocationWise_Result> a = new List<CustomerBranchNameLocationWise_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedureLocationWise(Convert.ToInt32(categoryid), Convert.ToInt32(branchid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }

        protected void GridExportExcel_DataBound(object sender, EventArgs e)
        {
            int rowCount = GridExportExcel.Rows.Count;

            if (rowCount == 0)
                btnUploadFile1.Visible = false;
            else
                btnUploadFile1.Visible = true;
        }

        private void BindDataExport(string Flag)
        {
            try
            {
                int branchid = -1;
                if (ddlFilterLocation.SelectedValue != "")
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                int processid = -1;
                if (ddlProcess.SelectedValue != "")
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlProcess.SelectedValue);
                    }
                }
                if (Flag == "L")
                {
                    var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskCategoryCreationExportBranchWise(branchid);
                    GridExportExcel.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    GridExportExcel.DataBind();
                }
                else if (Flag == "P")
                {
                    var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskCategoryCreationExportProcessWise(branchid, processid);
                    GridExportExcel.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;
                    GridExportExcel.DataBind();
                }
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        private void ProcessData(ExcelPackage xlWorkbook)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["ProcessSubProcess"];
                if (xlWorksheet != null)
                {
                    int count = 0;
                    int processid = -1;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<mst_Subprocess> mstSubprocesslist = new List<mst_Subprocess>();
                    List<mst_Subprocess> mstSubprocesslist1 = new List<mst_Subprocess>();
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        count = count + 1;                        
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                        {
                            processid = ProcessManagement.GetProcessIDByName(xlWorksheet.Cells[i, 1].Text.ToString(),customerID);
                        }
                        if (processid == 0 || processid == -1)
                        {
                            suucessProcess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Process Name at row number - " + count + " or Process not Defined in the System.";
                            break;
                        }
                        else
                        {
                            suucessProcess = true;
                        }                       
                        string subprocessname = "";
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            subprocessname = Convert.ToString(xlWorksheet.Cells[i, 2].Text.ToString().Trim());
                        }
                        if (subprocessname=="" || subprocessname==null)
                        {
                            suucessProcess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Name at row number - " + count + " or Sub Process Cannot Blank.";
                            break;
                        }
                        else
                        {
                            suucessProcess = true;
                        }
                        decimal subprocessScore = 0;
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                        {
                            subprocessScore = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text.ToString().Trim());
                        }
                        if (subprocessScore==0)
                        {
                            suucessProcess = false;
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Correct the Sub Process Score at row number - " + count + " or Sub Process Cannot Blank.";
                            break;
                        }
                        else
                        {
                            suucessProcess = true;
                        }
                        string isprocessnonprocess = "";
                        if (xlWorksheet.Cells[i, 4].Text != null)
                        {
                            if (xlWorksheet.Cells[i, 4].Text.ToString().Trim() == "P")
                            {
                                isprocessnonprocess = "P";
                            }
                            else
                            {
                                isprocessnonprocess = "N";
                            }
                        }
                        if (!(ProcessManagement.ExistsProcess(processid, Convert.ToString(subprocessname))))
                        {
                            mst_Subprocess mstSubprocess = new mst_Subprocess();
                            mstSubprocess.Name = subprocessname;
                            mstSubprocess.ProcessId = processid;
                            mstSubprocess.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            mstSubprocess.IsDeleted = false;
                            mstSubprocess.CreatedOn = DateTime.Now;
                            mstSubprocess.IsProcessNonProcess = isprocessnonprocess;
                            mstSubprocess.Scores = subprocessScore;
                            mstSubprocesslist.Add(mstSubprocess);
                        }//exists end
                    }
                    if (suucessProcess)
                    {
                        mstSubprocesslist1 = mstSubprocesslist.Where(entry => entry.ProcessId == 0).ToList();
                        if (mstSubprocesslist1.Count == 0)
                        {
                            if (processid != 0)
                            {
                                if (processid != -1)
                                {
                                    suucessProcess = ProcessManagement.CreateExcelProcess(mstSubprocesslist);
                                    suucessProcess = true;
                                }
                            }
                        }
                        else
                        {
                            suucessProcess = false;
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                suucessProcess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool ProcessSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("ProcessSubProcess"))
                    {
                        if (sheet.Name.Trim().Equals("ProcessSubProcess") || sheet.Name.Trim().Equals("ProcessSubProcess") || sheet.Name.Trim().Equals("ProcessSubProcess"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }


        //protected void rdoAct_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rdoAct.Checked == true)
        //        MasterFileUpload.Visible = true;
        //}

        protected void rdoCompliance_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoCompliance.Checked == true)
                MasterFileUpload.Visible = true;
        }

        protected void rdoSubProcess_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSubProcess.Checked == true)
                MasterFileUpload.Visible = true;
        }

        protected void Tab1_Click1(object sender, EventArgs e)
        {
            //Tab1.CssClass = "Clicked";
            //Tab2.CssClass = "Initial";
            //MainView.ActiveViewIndex = 0;
            try
            {
                performerdocuments.Visible = true;
                reviewerdocuments.Visible = false;

                Tab1.Attributes.Add("class", "active");
                Tab2.Attributes.Add("class", "");

                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Tab2_Click1(object sender, EventArgs e)
        {
            //Tab1.CssClass = "Initial";
            //Tab2.CssClass = "Clicked";
            //MainView.ActiveViewIndex = 1;
            try
            {
                performerdocuments.Visible = false;
                reviewerdocuments.Visible = true;
                Tab1.Attributes.Add("class", "");
                Tab2.Attributes.Add("class", "active");

                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane active");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    GridExportExcel.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridExportExcel.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                bindGridData();
                GetPageDisplaySummary();

            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    GridExportExcel.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridExportExcel.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                bindGridData();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    GridExportExcel.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridExportExcel.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                //Reload the Grid
                bindGridData();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void bindGridData()
        {
            if (ddlFilterLocation.SelectedValue != null)
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    if (ddlProcess.SelectedValue != null)
                    {
                        if (ddlProcess.SelectedValue != "-1")
                        {
                            ViewState["Flagvalue"] = null;
                            ViewState["Flagvalue"] = "P";
                            BindDataExport("P");
                        }

                        else
                        {
                            BindProcess(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                            ViewState["Flagvalue"] = null;
                            ViewState["Flagvalue"] = "L";
                            BindDataExport("L");
                        }
                    }
                }
            }
        }
    }
}