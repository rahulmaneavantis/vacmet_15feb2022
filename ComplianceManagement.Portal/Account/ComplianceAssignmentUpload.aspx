﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceAssignmentUpload.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.ComplianceAssignmentUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails_Load">
        <ContentTemplate>

            <table width="100%" align="left">
                <tr>
                    <td>
                        <asp:Button Text="Compliance Assignment" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                            OnClick="Tab1_Click" />
                        <asp:Button Text="Existing Compliance Assign Owner Officer" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                            OnClick="Tab3_Click" />
                        <asp:Button Text="Download Excel Format" BorderStyle="None" ID="btnExcelFormat" CssClass="Initial" runat="server"
                            OnClick="btnExcelFormat_Click" />
                        <asp:MultiView ID="MainView" runat="server">
                            <div style="width: 100%; float: left; margin-bottom: 15px">
                                <asp:View ID="View1" runat="server">
                                    <div style="margin-top: 32px; width: auto">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                        <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>
                                    </div>
                                    <table align="left" cellpadding="2" style="margin-left: 55px;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Select Customer :"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                    CssClass="txtbox" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; padding-left: 80px;" colspan="2">
                                                <div style="display: none;">
                                                    <asp:RadioButton ID="rdoAssignCompliance" runat="server" Checked="true" AutoPostBack="false" Text="Assign Compliance" GroupName="uploadContentGroup" /><br />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 31px;">
                                                <asp:Label ID="lblUploadFile" runat="server" Text="Upload File :"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="MasterFileUpload" runat="server" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                                    runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; padding-left: 35px;" colspan="2">
                                                <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                                    Style="margin-left: 75px;"
                                                    OnClick="btnUploadFile_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="display: none;">
                                                <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                </asp:View>
                                <asp:View ID="View3" runat="server">
                                       <div style="margin-top: 32px; width: auto">
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup1" />
                                        <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="cvDuplicateEntry2" runat="server" EnableClientScript="False"
                                                ValidationGroup="oplValidationGroup1" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>
                                    </div>
                                    <table align="left" cellpadding="2" style="margin-left: 55px;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="Select Customer :"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlECustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                    CssClass="txtbox" />
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                 
                                            </td>
                                            <td>
                                                <asp:UpdatePanel ID="uppanal" runat="server" >
                                                    <ContentTemplate>
                                                   <asp:Button ID="BtnDownloadAssignment" runat="server" Text="Download" ValidationGroup="oplValidationGroup1"
                                                    Style="margin-left: 75px;"
                                                    OnClick="BtnDownloadAssignment_Click" />
                                                         </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="BtnDownloadAssignment" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                            <td style="padding-left: 31px;">
                                                <asp:Label ID="Label5" runat="server" Text="Upload File :"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="EMasterFileUpload" runat="server" />
                                            <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Select File." ControlToValidate="EMasterFileUpload"
                                                    runat="server" Display="None" ValidationGroup="oplValidationGroup1" /> --%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 31px;">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <td style="text-align: left; padding-left: 35px;" colspan="2">
                                                        <asp:Button ID="btnEUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup1"
                                                            Style="margin-left: 75px;"
                                                            OnClick="btnEUploadFile_Click" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnEUploadFile" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="display: none;">
                                                 
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />                               
                                </asp:View>
                                </div>
                        </asp:MultiView>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="btnExcelFormat" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
