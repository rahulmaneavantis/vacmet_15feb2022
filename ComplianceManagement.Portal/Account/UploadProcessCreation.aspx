﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="UploadProcessCreation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.UploadProcessCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
       
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');
            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });            
        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <table width="100%" align="left">
                <tr>
                    <td>
                        <asp:Button Text="Import SubProcess" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                            OnClick="Tab1_Click" />
                        
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">

                                <div style="width: 100%; float: left; margin-bottom: 15px">
                                    <div style="margin-bottom: 4px; width: auto">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="oplValidationGroup" />
                                        <div align="center" style="margin-top: 30px; font-family: Arial; font-size: 10pt">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="LblErormessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                                        </div>
                                    </div>
                                    <table align="left" cellpadding="2" style="margin-left: 55px;">
                                        <tr>
                                            <td style="text-align: left; padding-left: 80px;" colspan="2">
                                                <div>
                                                    <asp:RadioButton ID="rdoProcess" runat="server" AutoPostBack="false" Text="Upload Sub Process" GroupName="uploadContentGroup" /><br />
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblUploadFile" runat="server" Text="Upload File :"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="MasterFileUpload" runat="server" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Select File." ControlToValidate="MasterFileUpload"
                                                    runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; padding-left: 8px;" colspan="2">
                                                <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup"
                                                    Style="margin-left: 75px;"
                                                    OnClick="btnUploadFile_Click" />
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </asp:View>

                        </asp:MultiView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
