﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Account
{
    public partial class DumpMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (User.Identity.IsAuthenticated)
                {
                    BindUserList();
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                }
                else
                    FormsAuthentication.RedirectToLoginPage();               
            }
        }

        public void BindUserList()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {                   
                    var user = (from row in entities.Users
                                where row.IsActive == true
                                && row.IsDeleted == false
                                && (row.RoleID == 12 || row.RoleID == 24 || row.RoleID == 25 || row.RoleID == 26)
                                select new UserName()
                                {
                                    ID = row.ID,
                                    Name = row.FirstName + " "+ row.LastName
                                }).ToList();

                    drpUserlst.DataTextField = "Name";
                    drpUserlst.DataValueField = "ID";
                    drpUserlst.Items.Clear();
                    drpUserlst.DataSource = user;
                    drpUserlst.DataBind();
                    drpUserlst.Items.Insert(0, new ListItem("Select User", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public class UserName
        {
            public long ID { get; set; }
            public string Name { get; set; }
        }

        protected void btnexcel_Click(object sender, EventArgs e)
        {
            try
            {

                if (drpUserlst.SelectedValue != "-1")
                {

                    bool flagOutput = false;
                    List<int> actslst = new List<int>();
                    List<long> Compliancelst = new List<long>();
                    string actids = txtactid.Text;

                    if (!string.IsNullOrEmpty(actids))
                    {
                        string[] values = actids.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (Convert.ToInt32(values[i].Trim()) > 0)
                                actslst.Add(Convert.ToInt32(values[i].Trim()));
                        }
                    }

                    actslst = actslst.Distinct().ToList();

                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            List<ComplianceViewExport> table = new List<ComplianceViewExport>();

                            table = (from row in entities.ComplianceViewExports
                                     select row).ToList();

                            var RemainningEventComplianceDump = table.ToList();

                            if (table.Count > 0)
                            {
                                if (actslst.Count > 0)
                                    table = table.Where(x => actslst.Contains(x.ActID)).ToList();

                            }
                            table = table.Distinct().ToList();
                            if (table.Count > 0)
                            {
                                
                                //Event List  
                                List<string> EventIDList = new List<string>();
                                foreach (var item in table)
                                {
                                    if(Convert.ToString(item.EventID) !="")
                                    {
                                        string[] IDList = item.EventID.ToString().Trim(',').Split(',');

                                        foreach (var item1 in IDList)
                                        {
                                            EventIDList.Add(item1);
                                        }
                                    }
                                }

                                List<string> distinctEventIDList = EventIDList.Distinct().ToList();

                                //Compliance List
                                List<long> ComplianceList = new List<long>();
                                foreach (var item in table)
                                {
                                    ComplianceList.Add(Convert.ToInt64(item.ID));
                                }

                                List<long> distinctComplianceList = ComplianceList.Distinct().ToList();

                                //State List
                                List<string> StateList = new List<string>();
                                foreach (var item in table)
                                {
                                    if (item.State != "")
                                    {
                                        StateList.Add(item.State);
                                    }
                                }

                                List<string> distinctState = StateList.Distinct().ToList();


                                #region Data Dump sheet 1
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Data Dump List");
                                System.Data.DataTable ExcelData1 = null;
                                DataView view1 = new System.Data.DataView((table as List<ComplianceViewExport>).ToDataTable());
                                ExcelData1 = view1.ToTable("Selected", false, "ID", "ActID", "ActName", "Sections", "ShortDescription", "Description", "PenaltyDescription", "ReferenceMaterialText", "Frequency", "DueDate", "RiskType", "ComplianceType", "NonComplianceType", "State", "RequiredForms", "NatureOfCompliance", "ComplianceCategory", "Authority", "ActionableOrInformative", "Ctype", "ModeofCompliance", "EventName", "ScheduleType");

                                exWorkSheet1.Cells["A1"].LoadFromDataTable(ExcelData1, true);

                                exWorkSheet1.Cells["A1"].Value = "Compliance ID";
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["A1"].AutoFitColumns(10);

                                exWorkSheet1.Cells["B1"].Value = "ActID";
                                exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["B1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["B1"].AutoFitColumns(10);

                                exWorkSheet1.Cells["C1"].Value = "ActName";
                                exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["C1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["C1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["D1"].Value = "Sections";
                                exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["D1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["D1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["E1"].Value = "ShortDescription";
                                exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["E1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["E1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["F1"].Value = "Description";
                                exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["F1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["F1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["G1"].Value = "PenaltyDescription";
                                exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["G1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["G1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["H1"].Value = "AdditionalText";
                                exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["H1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["H1"].AutoFitColumns(50);                                

                                exWorkSheet1.Cells["I1"].Value = "Frequency";
                                exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["I1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["I1"].AutoFitColumns(10);

                                exWorkSheet1.Cells["J1"].Value = "DueDate";
                                exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["J1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["J1"].AutoFitColumns(10);                                

                                exWorkSheet1.Cells["K1"].Value = "RiskType";
                                exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["K1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["K1"].AutoFitColumns(10);

                                exWorkSheet1.Cells["L1"].Value = "ComplianceType";
                                exWorkSheet1.Cells["L1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["L1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["L1"].AutoFitColumns(20);

                                exWorkSheet1.Cells["M1"].Value = "NonComplianceType";
                                exWorkSheet1.Cells["M1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["M1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["M1"].AutoFitColumns(20);                                

                                exWorkSheet1.Cells["N1"].Value = "State";
                                exWorkSheet1.Cells["N1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["N1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["N1"].AutoFitColumns(20);

                                exWorkSheet1.Cells["O1"].Value = "RequiredForms";
                                exWorkSheet1.Cells["O1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["O1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["O1"].AutoFitColumns(20);

                                exWorkSheet1.Cells["P1"].Value = "NatureOfCompliance";
                                exWorkSheet1.Cells["P1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["P1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["P1"].AutoFitColumns(20);
                                
                                exWorkSheet1.Cells["Q1"].Value = "ComplianceCategory";
                                exWorkSheet1.Cells["Q1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["Q1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["Q1"].AutoFitColumns(50);

                                exWorkSheet1.Cells["R1"].Value = "Authority";
                                exWorkSheet1.Cells["R1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["R1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["R1"].AutoFitColumns(30);
                                
                                exWorkSheet1.Cells["S1"].Value = "Actionable/Informative";
                                exWorkSheet1.Cells["S1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["S1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["S1"].AutoFitColumns(20);

                                exWorkSheet1.Cells["T1"].Value = "IsEventBased";
                                exWorkSheet1.Cells["T1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["T1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["T1"].AutoFitColumns(20);

                                exWorkSheet1.Cells["U1"].Value = "ModeofCompliance";
                                exWorkSheet1.Cells["U1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["U1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["U1"].AutoFitColumns(20);
                                                                    
                                exWorkSheet1.Cells["V1"].Value = "Event Name";
                                exWorkSheet1.Cells["V1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["V1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["V1"].AutoFitColumns(40);

                                exWorkSheet1.Cells["W1"].Value = "ScheduleType";
                                exWorkSheet1.Cells["W1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["W1"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["W1"].AutoFitColumns(30);

                                using (ExcelRange col = exWorkSheet1.Cells[2, 1, 2 + ExcelData1.Rows.Count, 23])
                                {
                                    col.Style.WrapText = true;
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                }

                                #endregion

                                #region Data Dump sheet 2

                                //Event
                                if (distinctEventIDList.Count() >= 1)
                                {
                                    ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("Remaining Data Dump List");
                                    System.Data.DataTable ExcelData2 = null;

                                    var stringEventIDList = distinctEventIDList.Select(i => i.ToString(CultureInfo.InvariantCulture)).Aggregate((s1, s2) => s1 + ", " + s2);

                                    var longEventList= distinctEventIDList.Select(long.Parse).ToList();

                                    var Remevent = (from row in entities.sp_GetEventDump()
                                                    select row).ToList();
                                    
                                    //Compliance
                                    if (distinctComplianceList.Count() >= 1)
                                    {
                                        Remevent = Remevent.Where(entry => longEventList.Contains((long)entry.EventID)).ToList();
                                    }

                                    //State
                                    if (distinctState.Count() >= 1)
                                    {
                                        Remevent = Remevent.Where(entry => (distinctState.Contains((string)entry.State) || entry.State == null)).ToList();
                                    }

                                    //Compliance
                                    if (distinctComplianceList.Count() >= 1)
                                    {
                                        Remevent.RemoveAll(a => distinctComplianceList.Contains((long)a.ComplianceID));
                                    }

                                    DataView view2 = new System.Data.DataView((Remevent as List<sp_GetEventDump_Result>).ToDataTable());
                                    ExcelData2 = view2.ToTable("Selected", false, "EventID", "EventName", "ComplianceID", "ActID", "Type", "ActName", "ShortDescription", "Sections", "Description", "ComplianceCategory", "ComplianceType", "Frequency", "State", "Ctype", "ActionableOrInformative", "ScheduleType");

                                    exWorkSheet2.Cells["A1"].LoadFromDataTable(ExcelData2, true);

                                    exWorkSheet2.Cells["A1"].Value = "Event ID";
                                    exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["A1"].AutoFitColumns(10);

                                    exWorkSheet2.Cells["B1"].Value = "EventName";
                                    exWorkSheet2.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["B1"].AutoFitColumns(50);

                                    exWorkSheet2.Cells["C1"].Value = "ComplianceID";
                                    exWorkSheet2.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["C1"].AutoFitColumns(10);

                                    exWorkSheet2.Cells["D1"].Value = "ActID";
                                    exWorkSheet2.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["D1"].AutoFitColumns(10);

                                    exWorkSheet2.Cells["E1"].Value = "Type";
                                    exWorkSheet2.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["E1"].AutoFitColumns(20);

                                    exWorkSheet2.Cells["F1"].Value = "ActName";
                                    exWorkSheet2.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["F1"].AutoFitColumns(50);

                                    exWorkSheet2.Cells["G1"].Value = "ShortDescription";
                                    exWorkSheet2.Cells["G1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["G1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["G1"].AutoFitColumns(50);

                                    exWorkSheet2.Cells["H1"].Value = "Sections";
                                    exWorkSheet2.Cells["H1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["H1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["H1"].AutoFitColumns(30);

                                    exWorkSheet2.Cells["I1"].Value = "Description";
                                    exWorkSheet2.Cells["I1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["I1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["I1"].AutoFitColumns(50);

                                    exWorkSheet2.Cells["J1"].Value = "ComplianceCategory";
                                    exWorkSheet2.Cells["J1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["J1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["J1"].AutoFitColumns(30);

                                    exWorkSheet2.Cells["K1"].Value = "ComplianceType";
                                    exWorkSheet2.Cells["K1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["K1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["K1"].AutoFitColumns(20);

                                    exWorkSheet2.Cells["L1"].Value = "Frequency";
                                    exWorkSheet2.Cells["L1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["L1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["L1"].AutoFitColumns(20);

                                    exWorkSheet2.Cells["M1"].Value = "State";
                                    exWorkSheet2.Cells["M1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["M1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["M1"].AutoFitColumns(40);

                                    exWorkSheet2.Cells["N1"].Value = "Ctype";
                                    exWorkSheet2.Cells["N1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["N1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["N1"].AutoFitColumns(20); 

                                    exWorkSheet2.Cells["O1"].Value = "ActionableOrInformative";
                                    exWorkSheet2.Cells["O1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["O1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["O1"].AutoFitColumns(40);

                                    exWorkSheet2.Cells["P1"].Value = "ScheduleType";
                                    exWorkSheet2.Cells["P1"].Style.Font.Bold = true;
                                    exWorkSheet2.Cells["P1"].Style.Font.Size = 12;
                                    exWorkSheet2.Cells["P1"].AutoFitColumns(30);

                                    using (ExcelRange col = exWorkSheet2.Cells[2, 1, 2 + ExcelData1.Rows.Count, 16])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }
                                }

                                #endregion

                                #region Data Dump sheet 3

                                //Event
                                if (distinctEventIDList.Count() >= 1)
                                {
                                    ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("Event List");
                                    System.Data.DataTable ExcelData3 = null;

                                    var stringEventIDList = distinctEventIDList.Select(i => i.ToString(CultureInfo.InvariantCulture)).Aggregate((s1, s2) => s1 + ", " + s2);

                                    var longEventList = distinctEventIDList.Select(long.Parse).ToList();

                                    var eventList = (from row in entities.SP_GetParentEventList()
                                                     where longEventList.Contains(row.ID)
                                                     select row).Distinct().ToList();

                                    //Compliance
                                    if (eventList.Count() >= 1)
                                    {
                                        eventList = eventList.Where(entry => longEventList.Contains((long)entry.ID)).ToList();
                                    }
                                    
                                    DataView view3 = new System.Data.DataView((eventList as List<SP_GetParentEventList_Result>).ToDataTable());

                                    ExcelData3 = view3.ToTable("Selected", false, "ID", "Name", "Type");

                                    //ExcelData2.Columns.Remove("ComplianceID");
                                    //ExcelData2.Columns.Remove("ActID");

                                    //DataView view = new DataView(ExcelData3);
                                    //DataTable distinctValues = new DataTable();
                                    //distinctValues = view.ToTable(true, "EventID");

                                    exWorkSheet3.Cells["A1"].LoadFromDataTable(ExcelData3, true);

                                    exWorkSheet3.Cells["A1"].Value = "Event ID";
                                    exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["A1"].AutoFitColumns(10);

                                    exWorkSheet3.Cells["B1"].Value = "EventName";
                                    exWorkSheet3.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["B1"].AutoFitColumns(50);

                                    exWorkSheet3.Cells["C1"].Value = "Type";
                                    exWorkSheet3.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet3.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet3.Cells["C1"].AutoFitColumns(30);

                                    using (ExcelRange col = exWorkSheet3.Cells[2, 1, 2 + ExcelData3.Rows.Count, 3])
                                    {
                                        col.Style.WrapText = true;
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                        // Assign borders
                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }
                                }

                                #endregion


                                Byte[] fileBytes = exportPackge.GetAsByteArray();
                                Response.ClearContent();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=DumpReport.xlsx");
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.ms-excel";
                                StringWriter sw = new StringWriter();
                                Response.BinaryWrite(fileBytes);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                flagOutput = true;

                            }

                            if (flagOutput)
                            {
                                DataDumpLog obj = new DataDumpLog();

                                obj.UserID = Convert.ToInt32(drpUserlst.SelectedValue);
                                obj.ActID = txtactid.Text.Trim();
                                obj.CreatedBy = AuthenticationHelper.UserID;
                                obj.CreatedDate = DateTime.Now;

                                entities.DataDumpLogs.Add(obj);
                                entities.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Please Select User.')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}