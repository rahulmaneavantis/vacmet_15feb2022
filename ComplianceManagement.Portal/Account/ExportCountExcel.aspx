﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ExportCountExcel.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Account.ExportCountExcel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">



     <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upActList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%" >
                <tr align="center">
                    <td>
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select User:
                    </label>
                    <asp:DropDownList runat="server" ID="ddlUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged">
                    </asp:DropDownList>
                    </td>

                    <td>
                        <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                            From Date :
                        </label>
                        <asp:TextBox ID="txtFromDate" AutoPostBack="true" OnTextChanged ="txtFromDate_TextChanged" CssClass="StartDate" Width="100px" runat="server"></asp:TextBox>
                    </td>
                   <td>
                        <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                            To Date :
                        </label>
                        <asp:TextBox ID="txtToDate" AutoPostBack="true" OnTextChanged="txtToDate_TextChanged"  CssClass="StartDate" Width="100px" runat="server"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:Button ID="btnExport" runat="server" Text="Export to Excel"
                            OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            <div id="divAct" runat="server">
                <asp:Panel ID ="pnlgrid" runat ="server" Width ="600px" Height ="500px">
                    <br />
                    <br />
                    <table width="100%">
                        <tr>
                        </tr>
                        <tr align="center">
                            <td>
                                <asp:GridView ID="grdAct" AutoGenerateColumns="false" CellPadding="5" runat="server">
                                    <Columns>
                                        <asp:BoundField HeaderText="Name" ItemStyle-HorizontalAlign="Center" DataField="Name" />
                                        <asp:BoundField HeaderText="Act Count" ItemStyle-HorizontalAlign="Center" DataField="ActCount" />
                                        <asp:BoundField HeaderText="Compliance Count" ItemStyle-HorizontalAlign="Center" DataField="ComplianceCount" />
                                        <asp:BoundField HeaderText="Checklist Count" ItemStyle-HorizontalAlign="Center" DataField="ChecklistCount" />
                                    </Columns>
                                    <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel> 
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
    </asp:UpdatePanel>

      <script type="text/javascript">
       
        function initializeCombobox() {
             $("#<%= ddlUsers.ClientID %>").combobox();
         }

         function initializeDatePicker(date) {

             var startDate = new Date();
             $(".StartDate").datepicker({
                 dateFormat: 'dd-mm-yy',
                 setDate: startDate,
                 numberOfMonths: 1
             });
         }

         function setDate() {
             $(".StartDate").datepicker();
         }
    </script>
</asp:Content>
