﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="InsiderReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.InsiderReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">
    .k-i-calendar{
        margin-top:5px !important;
    }
    .k-i-arrow-60-down{
         margin-top:5px !important;
    }
    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
    <style>
        body{
            color:#666;
            font-weight:300;
        }
        .form-group.required .control-label:after { 
            content:"*";
            color:red;
        }
        html .k-grid tr:hover {
            background: #E4F7FB;
        }
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
         }
         .k-calendar td.k-state-focused.k-state-selected{
               background-color:blue;
         }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="col-sm-12">
                     <div class="col-sm-12" style="padding:0">
                <div class="toolbar">  
                   
                     <div class="col-sm-3" style="padding:0">
                <label class=" control-label" style="font-weight:450;padding-top: 5px;" for="sel1">From: &nbsp; </label>
                <input id="datepicker" value="10/10/2020" title="datepicker" style="width: 70%" onchange="refreshreport()" />
            </div>
                    <div class="col-sm-3" style="padding:0">
                <label class=" control-label" style="font-weight:450;padding-top: 5px;" for="sel1">To: &nbsp; </label>
                <input id="datepicker1" value="20/12/2020" title="datepicker" style="width: 70%" onchange="refreshreport()" />
            </div>
                    <label class="control-label" style="font-weight:450;padding-top: 5px;" for="sel1">Select Company: &nbsp; </label>
                    <input id="dropdownlistComplianceType" data-placeholder="Type" style="width:172px;">
                 <a  onclick="DownloadExcl()" style="float:right;" class="btn btn-default" download><img src="../../Images/Excel _icon.png"/></a> 
                </div>
                 
                        
            </div>
        <div class="col-sm-12" style="margin-bottom:2%;" >
            
        </div>

    <div class="col-sm-12" id="grid"></div>
  </div>
        <script>
            $("#pagetype").text("My Reports");
            $("#datepicker").kendoDatePicker({
                format: "dd/MM/yyyy"
            });
            $("#datepicker1").kendoDatePicker({
                format: "dd/MM/yyyy"
            });
            var xx = $('#dropdownlistComplianceType').val();
            $("#grid").kendoGrid({

                dataSource: {
                    transport: {
                        read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getdetails/?user_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>+'&customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + xx
                        },
                    schema: {
                        data: function (response) {
                            return response.data;
                        },
                        total: function (response) {
                            return response.total;
                        },
                        model: {
                            fields: {
                                name: { type: "string" },
                                upsitype: { type: "string" },
                                InformationSharedDate: { type: "date" },
                                InformationSharedTime: { type: "string" },
                                ConcernedName: { type: "string" },
                                FirmName: { type: "string" },
                                ConcernedDesignation: { type: "string" },
                                EmployeeID: { type: "string" },
                                Reason: { type: "string" },
                                IdType: { type: "string" },
                            }
                        }
                    },
                    pageSize: 7
                },
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                filterable: true,
                //height: 550,
                //toolbar: ["search"],
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [{
                    template: "<div class='customer-name'>#: name #</div>",
                    field: "name",
                    title: "Name",
                    locked: true,
                    width: 140
                }, {
                    field: "upsitype",
                    title: "UPSI Type",
                    locked: true,
                    width: 140
                }, {
                    field: "InformationType",
                    title: "Information Type",
                    width: 140
                }, {

                    field: "InformationSharedDate",
                    title: "Shared Date",
                    format: "{0:dd/MM/yyyy}" ,
                    width: 150

                }, {

                    field: "InformationSharedTime",
                    title: "Shared Time",
                    width: 140

                }, {

                    field: "ConcernedName",
                    title: "Shared With",
                    width: 150

                    }, {

                        field: "EmployeeID",
                        title: "PAN",
                        width: 150

                    }, {

                        field: "ConcernedDesignation",
                        title: "Designation",
                        width: 150

                    }, {

                    field: "FirmName",
                    title: "Company/Firm",
                    width: 150

                }, {

                    field: "Reason",
                    title: "Remark",
                    width: 150

                }]
            });

            $("#dropdownlistComplianceType").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "ID",
                dataSource: {
                    transport: {
                        read: {
                            url: "<%=ConfigurationManager.AppSettings["insiderapi"]%>getcompanyall/?customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>,
                        }
                    }
                },
                change: function (e) {
                    var value = this.value();
                    var ff = formatDate($('#datepicker').val().split("/").reverse().join("-"));
                    var tt = formatDate($('#datepicker1').val().split("/").reverse().join("-"));

                    var user_id = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>";
                    user_id = user_id.trim();

                    var dataSource = new kendo.data.DataSource({

                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getdetails/?user_id=' +user_id+'&customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + this.value() + '&todate=' + tt + '&fromdate=' + ff
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    name: { type: "string" },
                                    upsitype: { type: "string" },
                                    InformationSharedDate: { type: "date" },
                                    ConcernedName: { type: "string" },
                                    FirmName: { type: "string" },
                                    ConcernedDesignation: { type: "string" },
                                    EmployeeID: { type: "string" },
                                    Reason: { type: "string" },
                                    IdType: { type: "string" },
                                }
                            }
                        },
                        pageSize:7


                    });
                     var grid = $("#grid").data("kendoGrid");
                     grid.setDataSource(dataSource);
                     // Use the value of the widget
                 }
            });

    </script>
    <script>
        function DownloadExcl() {
           // http://localhost:8000/api/downloadexcel/?customer_id=1167
            var xx = $('#dropdownlistComplianceType').val();
            var ff = formatDate($('#datepicker').val().split("/").reverse().join("-"));
            var tt = formatDate($('#datepicker1').val().split("/").reverse().join("-"));
            
            var user_id = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>";
            user_id = user_id.trim();    
                
            window.location = "<%=ConfigurationManager.AppSettings["insiderapi"]%>downloadexcel/?user_id=" +user_id+"&customer_id=" +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + xx + '&todate=' + tt + '&fromdate=' + ff;
        }
        
    </script>

    <script>
        function refreshreport() {
            var xx = $('#dropdownlistComplianceType').val();
            var ff = formatDate($('#datepicker').val().split("/").reverse().join("-"));
            var tt = formatDate($('#datepicker1').val().split("/").reverse().join("-"));

            var user_id = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>";
            user_id = user_id.trim();
            var dataSource = new kendo.data.DataSource({

                transport: {
                    read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getdetails/?user_id=' +user_id+'&customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>+'&fcompanyid=' + xx + '&todate=' + tt + '&fromdate=' + ff
                },
                schema: {
                    data: function (response) {
                        return response.data;
                    },
                    total: function (response) {
                        return response.total;
                    },
                    model: {
                        fields: {
                            name: { type: "string" },
                            upsitype: { type: "string" },
                            InformationSharedDate: { type: "date" },
                            ConcernedName: { type: "string" },
                            FirmName: { type: "string" },
                            ConcernedDesignation: { type: "string" },
                            EmployeeID: { type: "string" },
                            Reason: { type: "string" },
                            IdType: {type:"string"},
                        }
                    }
                },
                pageSize: 7


            });
            var grid = $("#grid").data("kendoGrid");
            grid.setDataSource(dataSource);
                     // Use the value of the widget

        }
    </script>
    <script>
        function formatDate(date) {
            var d = new Date(date),
                day = '' + d.getDate(),
                month = '' + (d.getMonth() + 1),
                year = d.getFullYear();
            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }
    </script>
</asp:Content>
