﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controllers
{
    public class AuditTrasactionController : ApiController
    {
        public class ErrorDetails
        {
            public string ErrorMessage { get; set; }
            public string ErrorDetailMessage { get; set; }
        }
        public class UpdateNotification
        {
            public string Message { get; set; }

            public string MessageType { get; set; }
        }

        public class StatusListDetails
        {
            public string ID { get; set; }
            public string Name { get; set; }
        }

        //[Route("AuditStatus/GetGroupLevelObj")]
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //public HttpResponseMessage GetGroupLevelObj(int CustomerId)
        //{
        //    try
        //    {
        //        using (AuditControlEntities entities = new AuditControlEntities())
        //        {
        //            var query = (from row in entities.Mst_GroupObservationCategory
        //                         where row.CustomerID == CustomerId && row.IsActive == false
        //                         select row).Distinct();

        //            var users = (from row in query
        //                         select new { ID = row.ID, Name = row.Grp_Observetion_Cat_Name }).OrderBy(entry => entry.Name).ToList<object>();

        //            return new HttpResponseMessage()
        //            {
        //                Content = new StringContent(JArray.FromObject(users).ToString(), Encoding.UTF8, "application/json")
        //            };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        List<ErrorDetails> errordetail = new List<ErrorDetails>();
        //        errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
        //        return new HttpResponseMessage()
        //        {
        //            Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
        //        };
        //    }
        //}

        [Route("AuditStatus/GetPersonResponsible")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetPersonResponsible(int CustomerId)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.mst_User
                                 where row.CustomerID == CustomerId
                                 && row.IsDeleted == false && row.IsActive == true
                                 select row);
                    var users = (from row in query
                                 select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(users).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }
        
        //[Route("AuditStatus/GetObservationCategory")]
        [Route("AuditStatus/GetObservationCategory")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetObservationCategory(int CustomerId)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.Mst_ObservationCategory
                                 where row.CustomerID == CustomerId && row.IsActive == false
                                 select row).Distinct();

                    var users = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(users).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //[Route("AuditStatus/GetSubObservationCategory")]
        [Route("AuditStatus/GetSubObservationCategory")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetSubObservationCategory(int CustomerId, int CategoryID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var query = (from row in entities.Mst_ObservationSubCategory
                                 join row1 in entities.Mst_ObservationCategory
                                 on row.ObscatID equals row1.ID
                                 where row.IsActive == false
                                 && row.ObscatID == CategoryID
                                 && row1.CustomerID == CustomerId
                                 && row1.IsActive == false
                                 select row).Distinct();

                    var users = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(users).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/BindWorkingFileGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindWorkingFileGrid(int AuditID, int ATBDID)
        {
            try
            {
                List<GetInternalAuditDocumentsView> objFiles = new List<GetInternalAuditDocumentsView>();
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    objFiles = (from row in entities.GetInternalAuditDocumentsViews
                                where row.ATBDId == ATBDID
                                && row.AuditID == AuditID
                                select row).ToList();

                    if (objFiles.Count > 0)
                    {
                        objFiles = objFiles.Where(entry => entry.Version == "1.0").ToList();
                        objFiles = objFiles.Where(e => e.TypeOfFile != "OB").ToList();
                    }

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(objFiles).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/DeleteWorkingFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DeleteWorkingFile(int FileID)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>(); ;
                bool result = DashboardManagementRisk.DeleteFile(null, FileID);
                if (result)
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                }
                else
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        //Working upload File One
        [Route("AuditStatus/UpladWorkingFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpladWorkingFile(MultipartDataMediaFormatter.Infrastructure.FormData files, int AuditID, int ATBDID, int CustomerId, int UserID, int ScheduleOnID, int InstanceID, int ProcessID, string UserName)
        {
            int AlreadyEXistFile = 0;
            int SaveFile = 0;
            string Result = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                AuditClosureDetail AuditDatail = UserManagementRisk.GetAuditDatail(AuditID);
                DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                InternalAuditTransaction transaction = new InternalAuditTransaction()
                {
                    StatusId = null,
                    CreatedByText = UserName,
                    AuditScheduleOnID = ScheduleOnID,
                    FinancialYear = AuditDatail.FinancialYear,
                    CustomerBranchId = AuditDatail.BranchId,
                    InternalAuditInstance = InstanceID,
                    ProcessId = ProcessID,
                    ForPeriod = AuditDatail.ForMonth,
                    ATBDId = ATBDID,
                    RoleID = 3,
                    UserID = UserID,
                    VerticalID = (int)AuditDatail.VerticalId,
                    AuditID = AuditID,
                    StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                };

                InternalAuditTransaction TrasactionDetails = UserManagementRisk.GetTrasactionID(Convert.ToInt32(AuditID), Convert.ToInt32(ATBDID));
                if (TrasactionDetails == null)
                {
                    RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                }

                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                List<InternalFileData_Risk> Objfiles = new List<InternalFileData_Risk>();
                var InstanceData = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuditID, ATBDID);
                AuditClosureDetail AuditDetails = RiskCategoryManagement.GetAuditDetailsByAuditID(AuditID);
                string directoryPath = "";
                //directoryPath = System.Web.HttpContext.Current.Server.MapPath("~/InternalControlWorkingDocument/" + customerID + "/" + AuditDetails.BranchId.ToString() + "/" + AuditDetails.VerticalId.ToString() + "/" + AuditDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ID + "/1.0");
                directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + AuditDetails.BranchId.ToString() + "/" + AuditDetails.VerticalId.ToString() + "/" + AuditDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ID + "/1.0");
                if (!File.Exists(directoryPath))
                {
                    DocumentManagement.CreateDirectory(directoryPath);
                }
                for (int i = 0; i < files.Files.Count; i++)
                {
                    string fileName = "";
                    Guid fileKey = Guid.NewGuid();
                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(files.Files[0].Value.FileName));
                    Byte[] bytes = files.Files[0].Value.Buffer;
                    fileName = files.Files[0].Value.FileName;
                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                    InternalFileData_Risk file = new InternalFileData_Risk()
                    {
                        Name = fileName,
                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                        FileKey = fileKey.ToString(),
                        Version = "1.0",
                        VersionDate = DateTime.UtcNow,
                        ProcessID = Convert.ToInt32(InstanceData.ProcessId),
                        CustomerBranchId = AuditDetails.BranchId,
                        FinancialYear = AuditDetails.FinancialYear,
                        IsDeleted = false,
                        ATBDId = ATBDID,
                        VerticalID = (int)AuditDetails.VerticalId,
                        AuditID = AuditID,
                        UpdatedBy = UserID,
                        UpdatedOn = DateTime.Now,
                    };
                    if (UserManagementRisk.CheckWorkingFileExist(file))
                    {
                        Objfiles.Add(file);
                        SaveFile = 1;
                    }
                    else
                    {
                        AlreadyEXistFile = 2;
                    }
                }
                if (fileList != null)
                {
                    SaveDocFiles(fileList);
                    InternalAuditTransaction TrasactionDetailsData = UserManagementRisk.GetTrasactionID(AuditID, ATBDID);
                    bool flag = UserManagementRisk.UpdateTransactionNew(TrasactionDetailsData, Objfiles, UserID);
                    if (flag)
                    {
                        Result = "" + SaveFile; // save File only --1
                    }
                    else
                    {
                        if (AlreadyEXistFile == 2)
                        {
                            Result = "" + AlreadyEXistFile; //Exist file only -- 2
                        }
                    }
                    if (SaveFile == 1 && AlreadyEXistFile == 1)
                    {
                        Result = "3";//Both Save and exist file -- 3 
                    }
                }
                fileList.Clear();
            }
            string jsonstring = JsonConvert.SerializeObject(Result);
            return new HttpResponseMessage()
            {
                Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
            };
        }


        //Working upload File One
        [Route("AuditStatus/UploadWorkingDocumentTwo")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UploadWorkingDocumentTwo(MultipartDataMediaFormatter.Infrastructure.FormData files, int AuditID, int ATBDID, int CustomerId, int UserID)
        {
            string Result = "";
            int AlreadyEXistFile = 0;
            int SaveFile = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                List<InternalFileData_Risk> Objfiles = new List<InternalFileData_Risk>();
                var InstanceData = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuditID, ATBDID);
                AuditClosureDetail AuditDetails = RiskCategoryManagement.GetAuditDetailsByAuditID(AuditID);
                string directoryPath = "";
                //directoryPath = System.Web.HttpContext.Current.Server.MapPath("~/InternalControlWorkingDocument/" + customerID + "/" + AuditDetails.BranchId.ToString() + "/" + AuditDetails.VerticalId.ToString() + "/" + AuditDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ID + "/1.0");
                directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + AuditDetails.BranchId.ToString() + "/" + AuditDetails.VerticalId.ToString() + "/" + AuditDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ID + "/1.0");
                if (!File.Exists(directoryPath))
                {
                    DocumentManagement.CreateDirectory(directoryPath);
                }
                for (int i = 0; i < files.Files.Count; i++)
                {
                    string fileName = "";
                    Guid fileKey = Guid.NewGuid();
                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(files.Files[0].Value.FileName));
                    Byte[] bytes = files.Files[0].Value.Buffer;
                    fileName = files.Files[0].Value.FileName;
                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                    InternalFileData_Risk file = new InternalFileData_Risk()
                    {
                        Name = fileName,
                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                        FileKey = fileKey.ToString(),
                        Version = "1.0",
                        VersionDate = DateTime.UtcNow,
                        ProcessID = Convert.ToInt32(InstanceData.ProcessId),
                        CustomerBranchId = AuditDetails.BranchId,
                        FinancialYear = AuditDetails.FinancialYear,
                        IsDeleted = false,
                        ATBDId = ATBDID,
                        VerticalID = (int)AuditDetails.VerticalId,
                        AuditID = AuditID,
                        UpdatedBy = UserID,
                        UpdatedOn = DateTime.Now,
                        TypeOfFile = "OB"
                    };
                    if (UserManagementRisk.CheckWorkingFileTwoExist(file))
                    {
                        Objfiles.Add(file);
                        SaveFile = 1;
                    }
                    else
                    {
                        AlreadyEXistFile = 2;
                    }
                }
                if (fileList != null)
                {
                    SaveDocFiles(fileList);
                    InternalAuditTransaction TrasactionDetails = UserManagementRisk.GetTrasactionID(AuditID, ATBDID);
                    bool flag = UserManagementRisk.UpdateTransactionNew(TrasactionDetails, Objfiles, UserID);
                    if (flag)
                    {
                        Result = "" + SaveFile; // save File only --1
                    }
                    else
                    {
                        if (AlreadyEXistFile == 2)
                        {
                            Result = "" + AlreadyEXistFile; //Exist file only -- 2
                        }
                    }
                    if (SaveFile == 1 && AlreadyEXistFile == 1)
                    {
                        Result = "3";//Both Save and exist file -- 3 
                    }
                }
                fileList.Clear();
            }
            string jsonstring = JsonConvert.SerializeObject(Result);
            return new HttpResponseMessage()
            {
                Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
            };
        }

        [Route("AuditStatus/UpladAnnexureFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UpladAnnexureFile(MultipartDataMediaFormatter.Infrastructure.FormData files, int AuditID, int ATBDID, int CustomerId, int UserID)
        {
            int AlreadyEXistFile = 0;
            int SaveFile = 0;
            string Result = "";
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                List<Tran_AnnxetureUpload> AnnxetureList = new List<Tran_AnnxetureUpload>();
                var InstanceData = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuditID, ATBDID);
                AuditClosureDetail AuditDetails = RiskCategoryManagement.GetAuditDetailsByAuditID(AuditID);
                string directoryPath = "";
                directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/AuditClosureAnnuxureDocument/" + CustomerId + "/"
                                    + AuditDetails.BranchId + "/" + AuditDetails.VerticalId + "/" + InstanceData.ProcessId + "/"
                                    + AuditDetails.FinancialYear + "/" + AuditDetails.ForMonth + "/" + AuditID + "/" + ATBDID + "/1.0");

                //directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + AuditDetails.BranchId.ToString() + "/" + AuditDetails.VerticalId.ToString() + "/" + AuditDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ID + "/1.0");
                if (!File.Exists(directoryPath))
                {
                    DocumentManagement.CreateDirectory(directoryPath);
                }
                for (int i = 0; i < files.Files.Count; i++)
                {
                    string fileName = "";
                    Guid fileKey = Guid.NewGuid();
                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(files.Files[0].Value.FileName));
                    Byte[] bytes = files.Files[0].Value.Buffer;
                    fileName = files.Files[0].Value.FileName;
                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                    Tran_AnnxetureUpload AnnxetureUpload = new Tran_AnnxetureUpload()
                    {
                        ProcessId = (int)InstanceData.ProcessId,
                        ATBDId = ATBDID,
                        CustomerBranchId = AuditDetails.BranchId,
                        FinancialYear = AuditDetails.FinancialYear,
                        Period = AuditDetails.ForMonth,
                        VerticalID = (int)AuditDetails.VerticalId,
                        IsDeleted = false,
                        FileName = fileName,
                        FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                        FileKey = fileKey.ToString(),
                        Version = "1.0",
                        VersionDate = DateTime.Now,
                        CreatedDate = DateTime.Today.Date,
                        CreatedBy = UserID,
                        AuditID = AuditID,
                    };
                    AnnxetureList.Add(AnnxetureUpload);
                }
                if (fileList != null)
                {
                    SaveDocFiles(fileList);
                    bool Success1 = false;
                    foreach (var item in AnnxetureList)
                    {
                        if (!RiskCategoryManagement.Tran_AnnxetureUploadResultExists(item))
                        {
                            Success1 = RiskCategoryManagement.CreateTran_AnnxetureUploadResult(item);
                            SaveFile = 1;
                            Result = "1";
                        }
                        else
                        {
                            AlreadyEXistFile = 2;
                            Result = "2";
                        }
                    }
                    if (SaveFile == 1 && AlreadyEXistFile == 1)
                    {
                        Result = "3";//Both Save and exist file -- 3 
                    }
                }
                fileList.Clear();
            }
            string jsonstring = JsonConvert.SerializeObject(Result);
            return new HttpResponseMessage()
            {
                Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
            };
        }

        [Route("AuditStatus/UploadImageFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UploadImageFile(MultipartDataMediaFormatter.Infrastructure.FormData files, int AuditID, int ATBDID, int CustomerId, int UserID)
        {
            string Result = "";
            bool Success = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                var InstanceData = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuditID, ATBDID);
                AuditClosureDetail AuditDetails = RiskCategoryManagement.GetAuditDetailsByAuditID(AuditID);
                string directoryPath = "";
                directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ObservationImages/" + CustomerId + "/"
                                    + AuditDetails.BranchId + "/" + AuditDetails.VerticalId + "/" + InstanceData.ProcessId + "/"
                                    + AuditDetails.FinancialYear + "/" + AuditDetails.ForMonth + "/" + AuditID + "/" + ATBDID + "/1.0");

                //directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/InternalControlWorkingDocument/" + CustomerId + "/" + AuditDetails.BranchId.ToString() + "/" + AuditDetails.VerticalId.ToString() + "/" + AuditDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ID + "/1.0");
                if (!File.Exists(directoryPath))
                {
                    DocumentManagement.CreateDirectory(directoryPath);
                }
                for (int i = 0; i < files.Files.Count; i++)
                {
                    string fileName = "";
                    Guid fileKey = Guid.NewGuid();
                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(files.Files[0].Value.FileName));
                    Byte[] bytes = files.Files[0].Value.Buffer;
                    fileName = files.Files[0].Value.FileName;
                    fileList.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                    ObservationImage ObjImg = new ObservationImage()
                    {
                        AuditID = AuditID,
                        ATBTID = ATBDID,
                        ImageName = fileName,
                        ImagePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                        FileKey = fileKey.ToString(),
                        IsActive = true,
                    };

                    if (RiskCategoryManagement.CheckObservationImageExist(ObjImg))
                    {
                        ObjImg.UpdatedBy = UserID;
                        ObjImg.UpdatedOn = DateTime.Now;
                        Success = RiskCategoryManagement.UpdateObservationImg(ObjImg);
                    }
                    else
                    {
                        ObjImg.CreatedBy = UserID;
                        ObjImg.CreatedOn = DateTime.Now;
                        Success = RiskCategoryManagement.CreateObservationImg(ObjImg);
                    }
                    //string finalPath1 = Path.Combine(directoryPath, ObjImg.ImageName);
                    //SaveDocFiles(finalPath1);

                }
                if (fileList != null)
                {
                    if (Success)
                    {
                        SaveDocFiles(fileList);
                        Result = "success";
                    }
                }
                fileList.Clear();
            }
            string jsonstring = JsonConvert.SerializeObject(Result);
            return new HttpResponseMessage()
            {
                Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
            };
        }

        //Working upload File One
        [Route("AuditStatus/UploadReviewHistoryDocument")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage UploadReviewHistoryDocument(MultipartDataMediaFormatter.Infrastructure.FormData files, int AuditID, int ATBDID, int CustomerId, int UserID, int InstanceID, string TestRemark)
        {
            bool Flag = false;
            string Result = "";
            if (string.IsNullOrEmpty(TestRemark))
            {
                TestRemark = "NA";
            }
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                string UserName = UserManagementRisk.GetUserName(UserID);
                List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                List<InternalFileData_Risk> Objfiles = new List<InternalFileData_Risk>();
                var InstanceData = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuditID, ATBDID);
                AuditClosureDetail AuditDetails = RiskCategoryManagement.GetAuditDetailsByAuditID(AuditID);
                string directoryPath = "";
                directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/InternalTestingAdditionalDocument/" + CustomerId + "/" + AuditDetails.BranchId.ToString() + "/" + AuditDetails.VerticalId.ToString() + "/" + AuditDetails.FinancialYear + "/" + InstanceData.ProcessId.ToString() + "/" + InstanceData.ID + "/1.0");
                if (!File.Exists(directoryPath))
                {
                    DocumentManagement.CreateDirectory(directoryPath);
                }
                if (files.Files.Count > 0)
                {
                    for (int i = 0; i < files.Files.Count; i++)
                    {
                        string fileName = "";
                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(files.Files[0].Value.FileName));
                        Byte[] bytes = files.Files[0].Value.Buffer;
                        fileName = files.Files[0].Value.FileName;
                        fileList.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                        InternalReviewHistory RH = new InternalReviewHistory()
                        {
                            ATBDId = ATBDID,
                            ProcessId = InstanceData.ProcessId,
                            InternalAuditInstance = InstanceID,
                            CreatedBy = UserID,
                            CreatedByText = UserName,
                            Dated = DateTime.Now,
                            Remarks = TestRemark,
                            AuditScheduleOnID = InstanceData.ID,
                            FinancialYear = AuditDetails.FinancialYear,
                            CustomerBranchId = AuditDetails.BranchId,
                            Name = fileName,
                            FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                            FileKey = fileKey.ToString(),
                            Version = "1.0",
                            VersionDate = DateTime.UtcNow,
                            FixRemark = "NA",
                            VerticalID = (int)AuditDetails.VerticalId,
                            CreatedOn = DateTime.Now,
                            AuditID = AuditID,
                            //UniqueFlag = UniqueFlag,
                        };
                        Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                    }
                    SaveDocFiles(fileList);
                }
                else
                {
                    InternalReviewHistory RH = new InternalReviewHistory()
                    {
                        ProcessId = InstanceData.ProcessId,
                        InternalAuditInstance = InstanceID,
                        CreatedBy = UserID,
                        CreatedByText = UserName,
                        Dated = DateTime.Now,
                        Remarks = TestRemark,
                        AuditScheduleOnID = InstanceData.ID,
                        FinancialYear = AuditDetails.FinancialYear,
                        CustomerBranchId = AuditDetails.BranchId,
                        ATBDId = ATBDID,
                        FixRemark = "NA",
                        VerticalID = (int)AuditDetails.VerticalId,
                        AuditID = AuditID,
                        //  UniqueFlag= UniqueFlag,
                    };
                    Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                }
                fileList.Clear();
            }
            if (Flag)
            {
                Result = "Successfully Added";
            }
            string jsonstring = JsonConvert.SerializeObject(Result);
            return new HttpResponseMessage()
            {
                Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
            };
        }

        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(System.IO.File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }

        #region Performer Save
        [Route("AuditStatus/ObservationTabOne")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ObservationTabOne(JObject data)
        {
            try
            {
                dynamic json = data;
                string result = null;
                string ErrorMessage = string.Empty;
                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (Convert.ToString(json.PersonResponsible) == "-1" || Convert.ToString(json.PersonResponsible) == "")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (Convert.ToString(json.Owner) == "-1" || Convert.ToString(json.Owner) == "")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion
                if (!string.IsNullOrEmpty(Convert.ToString(json.ATBDID)) || !string.IsNullOrEmpty(Convert.ToString(json.AuditID)))
                {
                    long roleid = 3;
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = json.ScheduleOnID,
                        ProcessId = json.ProcessID,
                        FinancialYear = json.FinYear,
                        ForPerid = json.Period,
                        CustomerBranchId = Convert.ToInt32(json.BranchID),
                        IsDeleted = false,
                        UserID = AuthenticationHelper.UserID,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = json.InstanceID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        StatusId = null,
                        CreatedByText = AuthenticationHelper.User,
                        StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                        AuditScheduleOnID = json.ScheduleOnID,
                        FinancialYear = json.FinYear,
                        CustomerBranchId = json.BranchID,
                        InternalAuditInstance = json.InstanceID,
                        ProcessId = json.ProcessID,
                        SubProcessId = -1,
                        ForPeriod = json.Period,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        UserID = AuthenticationHelper.UserID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                    {
                        MstRiskResult.AuditObjective = null;
                    }
                    else
                        MstRiskResult.AuditObjective = Convert.ToString(json.AuditObjective).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = Convert.ToString(json.AuditSteps).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = Convert.ToString(json.AnalysisToBePerofrmed).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = Convert.ToString(json.ProcessWalkthrough).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = Convert.ToString(json.ActivityToBeDone).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = Convert.ToString(json.Population).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = Convert.ToString(json.Sample).Trim();

                    MstRiskResult.ObservationNumber = "";

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = Convert.ToString(json.ObservationTitle).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        MstRiskResult.AnnexueTitle = Convert.ToString(json.AnnexueTitle).Trim();
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                    }

                    if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                        MstRiskResult.Observation = "";
                    else
                    {
                        MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                    }

                    if (string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                        MstRiskResult.BriefObservation = "";
                    else
                        MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                        MstRiskResult.ObjBackground = "";
                    else
                        MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = Convert.ToString(json.Recomendation).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");

                    int checkStatusId = -1;

                    if (Convert.ToInt32(json.StatusID) == 4)
                    {
                        checkStatusId = 4;
                    }
                    else if (Convert.ToInt32(json.StatusID) == 5)
                    {
                        checkStatusId = 5;
                    }
                    else
                    {
                        checkStatusId = 2;
                    }

                    if (checkStatusId != -1)
                    {
                        MstRiskResult.AStatusId = checkStatusId;
                    }

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.TimeLine);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.TimeLine = dt2.Date;
                        }
                        catch (Exception)
                        {
                            string TL = Convert.ToString(json.TimeLine);
                            dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.TimeLine = dt.Date;
                        }
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    DateTime responseDueDate = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.ResponseDueDate);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.ResponseDueDate = dt2;
                        }
                        catch (Exception)
                        {
                            string RD = Convert.ToString(json.ResponseDueDate);
                            responseDueDate = DateTime.ParseExact(RD, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.ResponseDueDate = responseDueDate.Date;
                        }
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (json.PersonResponsible == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (json.Owner == "-1")
                        {
                            MstRiskResult.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(json.Owner);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        if (json.ObservatioRating == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        if (json.ObservationCategory == "-1")
                        {
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                    {
                        if (json.ObservationSubCategory == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                        }
                    }
                    //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                    //{
                    //    if (json.GroupObservationCategoryID == "-1")
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = null;
                    //    }
                    //    else
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                    //    }
                    //}
                    bool Success = false;
                    InternalAuditTransaction TrasactionDetails = UserManagementRisk.GetTrasactionID(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                    if (TrasactionDetails == null)
                    {
                        RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                    }
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        if (Convert.ToInt32(json.StatusID) == 4)
                        {
                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            MstRiskResult.AStatusId = 4;
                            Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                        else if (Convert.ToInt32(json.StatusID) == 5)
                        {
                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            MstRiskResult.AStatusId = 5;
                            Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                        else
                        {
                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        result = "success";
                    }

                    if (!string.IsNullOrEmpty(result))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(json.AudioVideoLink)))
                        {
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                            if (observationAudioVideoList.Count > 0)
                            {
                                string CheckCommaValOne = Convert.ToString(json.AudioVideoLink);
                                string[] audioVideoLinks = CheckCommaValOne.Split(',');
                                var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                if (filtered.Count > 0)
                                {
                                    foreach (var item in filtered)
                                    {
                                        RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                    }
                                }
                            }

                            ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                            {
                                AuditId = Convert.ToInt32(json.AuditID),
                                ATBTID = Convert.ToInt32(json.ATBDID),
                                IsActive = true,
                            };

                            string CheckCommaVal = Convert.ToString(json.AudioVideoLink);
                            if (CheckCommaVal.Contains(',')) //checking entered single value or multiple values
                            {
                                string[] audioVideoLinks = (Convert.ToString(json.AudioVideoLink)).Split(',');
                                int lenght = audioVideoLinks.Length;
                                string link = string.Empty;
                                for (int i = 0; i < audioVideoLinks.Length; i++)
                                {
                                    link = audioVideoLinks[i].ToString();
                                    objObservationAudioVideo.AudioVideoLink = link.Trim();
                                    if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                    {
                                        objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                        RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                    }
                                }
                            }
                            else
                            {
                                objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                {
                                    objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                }
                                else
                                {
                                    objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                    objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                }
                            }
                        }
                        else
                        {
                            // when user want delete all existing link then we will get null from txtMultilineVideolink
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                {
                                    RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                }
                            }
                        }
                    }
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/PerformerSave")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage PerformerSave(JObject dataValues)
        {
            try
            {
                dynamic json = dataValues;
                string ErrorMessage = string.Empty;
                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (Convert.ToString(json.PersonResponsible) == "-1" || Convert.ToString(json.PersonResponsible) == "")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (Convert.ToString(json.Owner) == "-1" || Convert.ToString(json.Owner) == "")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                #region
                if (json.StatusID != -1)
                {
                    bool Flag = false;
                    long roleid = 3;
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuthenticationHelper.UserID, 3, Convert.ToString(json.FinYear), Convert.ToString(json.Period), Convert.ToInt32(json.BranchID), Convert.ToInt32(json.ATBDID), Convert.ToInt32(json.VerticalID), Convert.ToInt32(json.AuditID));
                    if (getScheduleonDetails != null)
                    {
                        #region
                        string AuditeeResponse = string.Empty;
                        string remark = string.Empty;
                        int checkStatusId = -1;

                        if (Convert.ToInt32(json.StatusID) == 4)
                        {
                            AuditeeResponse = "PT";
                            checkStatusId = 4;
                            remark = "Audit Steps Under Team Review";
                        }
                        else if (Convert.ToInt32(json.StatusID) == 5)
                        {
                            checkStatusId = 5;
                            remark = "Audit Steps Under Final Review.";
                        }
                        else
                        {
                            AuditeeResponse = "PS";
                            checkStatusId = 2;
                            remark = "Audit Steps Submited.";
                        }
                        InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                        {
                            AuditScheduleOnID = getScheduleonDetails.ID,
                            ProcessId = getScheduleonDetails.ProcessId,
                            FinancialYear = Convert.ToString(json.FinYear),
                            ForPerid = Convert.ToString(json.Period),
                            CustomerBranchId = Convert.ToInt32(json.BranchID),
                            IsDeleted = false,
                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            ATBDId = Convert.ToInt32(json.ATBDID),
                            RoleID = roleid,
                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                            AStatusId = checkStatusId,
                            VerticalID = Convert.ToInt32(json.VerticalID),
                            AuditID = Convert.ToInt32(json.AuditID),
                            AuditeeResponse = AuditeeResponse,
                            AnnexueTitle = Convert.ToString(json.AnnexueTitle)
                        };

                        InternalAuditTransaction transaction = new InternalAuditTransaction()
                        {
                            StatusId = checkStatusId,
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                            AuditScheduleOnID = getScheduleonDetails.ID,
                            FinancialYear = Convert.ToString(json.FinYear),
                            CustomerBranchId = Convert.ToInt32(json.BranchID),
                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                            ProcessId = getScheduleonDetails.ProcessId,
                            SubProcessId = -1,
                            ForPeriod = Convert.ToString(json.Period),
                            ATBDId = Convert.ToInt32(json.ATBDID),
                            RoleID = roleid,
                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            VerticalID = Convert.ToInt32(json.VerticalID),
                            AuditID = Convert.ToInt32(json.AuditID),
                        };
                        if (Convert.ToString(json.Population).Length > 200)
                        {
                            ErrorMessage = "Population should not greater than 200 characters.";
                        }

                        ObservationHistory objHistory = new ObservationHistory()
                        {
                            UserID = AuthenticationHelper.UserID,
                            RoleID = roleid,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            ATBTID = Convert.ToInt32(json.ATBDID)
                        };
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                        {
                            MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                            objHistory.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                        }
                        objHistory.ObservationOld = json.hidObservation;
                        objHistory.ProcessWalkthroughOld = json.hidObservation;
                        objHistory.ActualWorkDoneOld = json.hidActualWorkDone;
                        objHistory.PopulationOld = json.hidpopulation;
                        objHistory.SampleOld = json.hidSample;
                        objHistory.ObservationTitleOld = json.hidObservationTitle;
                        objHistory.RiskOld = json.hidRisk;
                        objHistory.RootCauseOld = json.hidRootcost;
                        objHistory.FinancialImpactOld = json.hidfinancialImpact;
                        objHistory.RecommendationOld = json.hidRecommendation;
                        objHistory.ManagementResponseOld = json.hidMgtResponse;
                        objHistory.RemarksOld = json.hidRemarks;
                        objHistory.ScoreOld = json.hidAuditStepScore;
                        //objHistory.AnnexueTitleOld = json.hidAnnaxureTitle;
                        //objHistory.AnnexueTitle = json.AnnexueTitle;


                        if (!string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                        {
                            MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                            transaction.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                        }
                        else
                        {
                            MstRiskResult.BriefObservation = null;
                            transaction.BriefObservation = null;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                        {
                            MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                            transaction.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                        }
                        else
                        {
                            MstRiskResult.ObjBackground = null;
                            transaction.ObjBackground = null;
                        }
                        //if (!string.IsNullOrEmpty(DefeciencyType))
                        //{
                        //    MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        //    transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        //}
                        //else
                        //{
                        //    MstRiskResult.DefeciencyType = null;
                        //    transaction.DefeciencyType = null;
                        //}
                        objHistory.OldBodyContent = Convert.ToString(json.hidBodyContent);
                        objHistory.OldUHComment = Convert.ToString(json.hidUHComment);
                        objHistory.OldPRESIDENTComment = Convert.ToString(json.hidPRESIDENTComment);

                        if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                        {
                            MstRiskResult.BodyContent = "";
                            objHistory.BodyContent = "";
                            transaction.BodyContent = "";
                        }
                        else
                        {
                            MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();
                            objHistory.BodyContent = Convert.ToString(json.BodyContent).Trim();
                            transaction.BodyContent = Convert.ToString(json.BodyContent).Trim();
                        }


                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidPersonResponsible)))
                        {
                            objHistory.PersonResponsibleOld = Convert.ToInt32(json.hidPersonResponsible);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationRating)))
                        {
                            objHistory.ObservationRatingOld = Convert.ToInt32(json.hidObservationRating);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationCategory)))
                        {
                            objHistory.ObservationCategoryOld = Convert.ToInt32(json.hidObservationCategory);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationSubCategory)))
                        {
                            objHistory.ObservationSubCategoryOld = Convert.ToInt32(json.hidObservationSubCategory);
                        }
                        //if (!string.IsNullOrEmpty(Convert.ToString(json.hidGroupObservationCategory)))
                        //{
                        //    objHistory.GroupObservationCategoryID = Convert.ToInt32(json.hidGroupObservationCategory);
                        //}
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidOwner)))
                        {
                            objHistory.OwnerOld = Convert.ToInt32(json.hidOwner);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidUserID)))
                        {
                            objHistory.UserOld = Convert.ToInt32(json.hidUserID);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                        {
                            objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                        {
                            objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                        }
                        objHistory.AuditId = Convert.ToInt32(json.AuditID);
                        objHistory.Remarks = remark;

                        DateTime dt5 = new DateTime();
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidResponseDueDate)))
                        {
                            try
                            {
                                DateTime dt15 = Convert.ToDateTime(json.hidResponseDueDate);
                                DateTime dt25 = GetDate(dt15.ToString("dd/MM/yyyy"));
                                objHistory.ResponseDueDateOld = dt25.Date;
                            }
                            catch (Exception)
                            {
                                string TL = Convert.ToString(json.hidResponseDueDate);
                                dt5 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.TimeLine = dt5.Date;
                            }
                        }
                        DateTime dt3 = new DateTime();
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidTimeLine)))
                        {
                            try
                            {
                                DateTime dt14 = Convert.ToDateTime(json.hidTimeLine);
                                DateTime dt24 = GetDate(dt14.ToString("dd/MM/yyyy"));
                                objHistory.TimeLineOld = dt24.Date;
                            }
                            catch (Exception)
                            {
                                string TL = Convert.ToString(json.hidTimeLine);
                                dt3 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                objHistory.TimeLineOld = dt3.Date;
                            }
                        }

                        DateTime dt1 = new DateTime();
                        if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                        {
                            try
                            {
                                DateTime dt13 = Convert.ToDateTime(json.TimeLine);
                                DateTime dt23 = GetDate(dt13.ToString("dd/MM/yyyy"));
                                MstRiskResult.TimeLine = dt23.Date;
                                objHistory.TimeLine = dt23;
                            }
                            catch (Exception)
                            {
                                string TL = Convert.ToString(json.TimeLine);
                                dt1 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.TimeLine = dt1.Date;
                                objHistory.TimeLine = dt1;
                            }
                        }
                        else
                        {
                            MstRiskResult.TimeLine = null;
                            objHistory.TimeLine = null;
                        }

                        DateTime dt6 = new DateTime();
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                        {
                            try
                            {
                                DateTime dt16 = Convert.ToDateTime(json.ResponseDueDate);
                                DateTime dt26 = GetDate(dt16.ToString("dd/MM/yyyy"));
                                MstRiskResult.ResponseDueDate = dt26.Date;
                                transaction.ResponseDueDate = dt26.Date;
                                objHistory.ResponseDueDate = dt26;
                            }
                            catch (Exception)
                            {
                                string TL = Convert.ToString(json.ResponseDueDate);
                                dt6 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.ResponseDueDate = dt6.Date;
                                transaction.ResponseDueDate = dt6.Date;
                                objHistory.ResponseDueDate = dt6.Date;
                            }
                        }
                        else
                        {
                            MstRiskResult.ResponseDueDate = null;
                            transaction.ResponseDueDate = null;
                            objHistory.ResponseDueDate = null;
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                            MstRiskResult.AuditObjective = null;
                        else
                            MstRiskResult.AuditObjective = Regex.Replace(Convert.ToString(json.AuditObjective).Trim(), @"\t|\n|\r", "");

                        if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                            MstRiskResult.AuditSteps = "";
                        else
                            MstRiskResult.AuditSteps = Regex.Replace(Convert.ToString(json.AuditSteps).Trim(), @"\t|\n|\r", "");

                        if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                            MstRiskResult.AnalysisToBePerofrmed = "";
                        else
                            MstRiskResult.AnalysisToBePerofrmed = Regex.Replace(Convert.ToString(json.AnalysisToBePerofrmed).Trim(), @"\t|\n|\r", "");

                        if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                        {
                            MstRiskResult.ProcessWalkthrough = "";
                            objHistory.ProcessWalkthrough = "";
                        }
                        else
                        {
                            MstRiskResult.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");
                            objHistory.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                        {
                            MstRiskResult.ActivityToBeDone = "";
                            objHistory.ActualWorkDone = "";
                        }
                        else
                        {
                            MstRiskResult.ActivityToBeDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");
                            objHistory.ActualWorkDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                        {
                            MstRiskResult.Population = "";
                            objHistory.Population = "";
                        }
                        else
                        {
                            MstRiskResult.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");
                            objHistory.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                        {
                            MstRiskResult.Sample = "";
                            objHistory.Sample = "";
                        }
                        else
                        {
                            MstRiskResult.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");
                            objHistory.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                        {
                            MstRiskResult.ObservationTitle = "";
                            objHistory.ObservationTitle = "";
                        }
                        else
                        {
                            MstRiskResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                            objHistory.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                        {
                            MstRiskResult.AnnexueTitle = "";
                        }
                        else
                        {
                            MstRiskResult.AnnexueTitle = Regex.Replace(Convert.ToString(json.AnnexueTitle).Trim(), @"\t|\n|\r", "");
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                        {
                            MstRiskResult.Observation = "";
                            objHistory.Observation = "";
                        }
                        else
                        {
                            MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                            objHistory.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                        {
                            MstRiskResult.Risk = "";
                            objHistory.Risk = "";
                        }
                        else
                        {
                            MstRiskResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                            objHistory.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                        {
                            MstRiskResult.RootCost = null;
                            objHistory.RootCause = null;
                        }
                        else
                        {
                            MstRiskResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                            objHistory.RootCause = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                        {
                            MstRiskResult.FinancialImpact = null;
                            objHistory.FinancialImpact = null;
                        }
                        else
                        {
                            MstRiskResult.FinancialImpact = Regex.Replace(Convert.ToString(json.FinancialImpact).Trim(), @"\t|\n|\r", "");
                            objHistory.FinancialImpact = Regex.Replace(Convert.ToString(json.FinancialImpact).Trim(), @"\t|\n|\r", "");
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                        {
                            MstRiskResult.Recomendation = "";
                            objHistory.Recommendation = "";
                        }
                        else
                        {
                            MstRiskResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                            objHistory.Recommendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                        {
                            MstRiskResult.ManagementResponse = "";
                            objHistory.ManagementResponse = "";
                        }
                        else
                        {
                            MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                            objHistory.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                            MstRiskResult.FixRemark = "";
                        else
                            MstRiskResult.FixRemark = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");


                        if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                        {
                            if (Convert.ToString(json.PersonResponsible) == "-1")
                            {
                                MstRiskResult.PersonResponsible = null;
                                transaction.PersonResponsible = null;
                                objHistory.PersonResponsible = null;
                            }
                            else
                            {
                                MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                transaction.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                objHistory.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                        {
                            if (Convert.ToString(json.Owner) == "-1")
                            {
                                MstRiskResult.Owner = null;
                                transaction.Owner = null;
                                objHistory.Owner = null;
                            }
                            else
                            {
                                MstRiskResult.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                transaction.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                objHistory.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                        {
                            if (Convert.ToString(json.ObservatioRating) == "-1")
                            {
                                transaction.ObservatioRating = null;
                                MstRiskResult.ObservationRating = null;
                                objHistory.ObservationRating = null;
                            }
                            else
                            {
                                transaction.ObservatioRating = Convert.ToInt32(json.ObservatioRating);
                                MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                objHistory.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                        {
                            if (Convert.ToString(json.ObservationCategory) == "-1")
                            {
                                transaction.ObservationCategory = null;
                                MstRiskResult.ObservationCategory = null;
                                objHistory.ObservationCategory = null;
                            }
                            else
                            {
                                transaction.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                objHistory.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                            }
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                        {
                            if (Convert.ToString(json.ObservationSubCategory) == "-1")
                            {
                                transaction.ObservationSubCategory = null;
                                MstRiskResult.ObservationSubCategory = null;
                                objHistory.ObservationSubCategory = null;
                            }
                            else
                            {
                                transaction.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                objHistory.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                            }
                        }
                        //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                        //{
                        //    if (Convert.ToString(json.GroupObservationCategoryID) == "-1")
                        //    {
                        //        transaction.GroupObservationCategoryID = null;
                        //        MstRiskResult.GroupObservationCategoryID = null;
                        //        objHistory.GroupObservationCategoryID = null;
                        //    }
                        //    else
                        //    {
                        //        transaction.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                        //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                        //        objHistory.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                        //    }
                        //}
                        transaction.Remarks = remark.Trim();
                        bool Success1 = false;
                        bool Success2 = false;

                        if (RiskCategoryManagement.InternalControlResultExistsWithStatus(MstRiskResult))
                        {
                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                        }
                        else
                        {
                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        }

                        if (RiskCategoryManagement.InternalAuditTxnExists(transaction) && Convert.ToInt32(json.StatusID) != 4)
                        {
                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatus(transaction);
                        }
                        else
                        {
                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                        }
                        string Testremark = "";
                        if (string.IsNullOrEmpty(Convert.ToString(json.txtRemark)))
                        {
                            Testremark = "NA";
                        }
                        else
                        {
                            Testremark = Convert.ToString(json.txtRemark).Trim();
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(objHistory.Observation))
                        {
                            if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                            {
                                RiskCategoryManagement.AddObservationHistory(objHistory);
                            }
                        }

                        #region

                        InternalReviewHistory RH = new InternalReviewHistory()
                        {
                            ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            Dated = DateTime.Now,
                            Remarks = Testremark,
                            AuditScheduleOnID = getScheduleonDetails.ID,
                            FinancialYear = Convert.ToString(json.FinYear),
                            CustomerBranchId = Convert.ToInt32(json.BranchID),
                            ATBDId = Convert.ToInt32(json.ATBDID),
                            FixRemark = "NA",
                            VerticalID = Convert.ToInt32(json.VerticalID),
                            AuditID = Convert.ToInt32(json.AuditID),
                            //  UniqueFlag= UniqueFlag,
                        };
                        if (RiskCategoryManagement.CheckRecordExist(RH))
                        {
                            Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                        }

                        #endregion

                        if (Success1)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(json.AudioVideoLink)))
                            {
                                List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                                if (observationAudioVideoList.Count > 0)
                                {
                                    string CheckCommaValOne = Convert.ToString(json.AudioVideoLink);
                                    string[] audioVideoLinks = CheckCommaValOne.Split(',');
                                    var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                    if (filtered.Count > 0)
                                    {
                                        foreach (var item in filtered)
                                        {
                                            RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                        }
                                    }
                                }

                                ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                                {
                                    AuditId = Convert.ToInt32(json.AuditID),
                                    ATBTID = Convert.ToInt32(json.ATBDID),
                                    IsActive = true,
                                };

                                string CheckCommaVal = Convert.ToString(json.AudioVideoLink);
                                if (CheckCommaVal.Contains(',')) //checking entered single value or multiple values
                                {
                                    string[] audioVideoLinks = (Convert.ToString(json.AudioVideoLink)).Split(',');
                                    int lenght = audioVideoLinks.Length;
                                    string link = string.Empty;
                                    for (int i = 0; i < audioVideoLinks.Length; i++)
                                    {
                                        link = audioVideoLinks[i].ToString();
                                        objObservationAudioVideo.AudioVideoLink = link.Trim();
                                        if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                        {
                                            objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                            objObservationAudioVideo.CreatedOn = DateTime.Now;
                                            RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                        }
                                        else
                                        {
                                            objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                            objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                            RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                        }
                                    }
                                }
                                else
                                {
                                    objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                    if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                    {
                                        objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                        objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                        RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                    }
                                }
                            }
                            else
                            {
                                // when user want delete all existing link then we will get null from txtMultilineVideolink
                                List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                                if (observationAudioVideoList.Count > 0)
                                {
                                    foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                    {
                                        RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                    }
                                }
                            }
                        }
                        else
                        {
                            ErrorMessage = "Server Error Occured. Please try again.";
                        }
                    }//getScheduleonDetails
                }//ddlFilterStatus  Null
                #endregion
                string result = string.Empty;
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    result = ErrorMessage;
                }
                else
                {
                    result = "success";
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string jsonstring = JsonConvert.SerializeObject("Something went wrong");
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion

        #region Reviewer Save
        [Route("AuditStatus/ReviewerObservationTabOne")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ReviewerObservationTabOne(JObject data)
        {
            try
            {
                dynamic json = data;
                string result = null;
                string ErrorMessage = string.Empty;
                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (Convert.ToString(json.PersonResponsible) == "-1" || Convert.ToString(json.PersonResponsible) == "")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (Convert.ToString(json.Owner) == "-1" || Convert.ToString(json.Owner) == "")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                if (!string.IsNullOrEmpty(Convert.ToString(json.ATBDID)) || !string.IsNullOrEmpty(Convert.ToString(json.AuditID)))
                {
                    long roleid = 4;
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = json.ScheduleOnID,
                        ProcessId = json.ProcessID,
                        FinancialYear = json.FinYear,
                        ForPerid = json.Period,
                        CustomerBranchId = Convert.ToInt32(json.BranchID),
                        IsDeleted = false,
                        UserID = AuthenticationHelper.UserID,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = json.InstanceID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        StatusId = null,
                        CreatedByText = AuthenticationHelper.User,
                        StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                        AuditScheduleOnID = json.ScheduleOnID,
                        FinancialYear = json.FinYear,
                        CustomerBranchId = json.BranchID,
                        InternalAuditInstance = json.InstanceID,
                        ProcessId = json.ProcessID,
                        SubProcessId = -1,
                        ForPeriod = json.Period,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        UserID = AuthenticationHelper.UserID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                    {
                        MstRiskResult.AuditObjective = null;
                    }
                    else
                        MstRiskResult.AuditObjective = Regex.Replace(Convert.ToString(json.AuditObjective).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = Regex.Replace(Convert.ToString(json.AuditSteps).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = Regex.Replace(Convert.ToString(json.AnalysisToBePerofrmed).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");

                    MstRiskResult.ObservationNumber = "";

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        MstRiskResult.AnnexueTitle = Regex.Replace(Convert.ToString(json.AnnexueTitle).Trim(), @"\t|\n|\r", "");
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                    }

                    if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                        MstRiskResult.BriefObservation = "";
                    else
                        MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                        MstRiskResult.ObjBackground = "";
                    else
                        MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");

                    int checkStatusId = -1;

                    if (Convert.ToInt32(json.StatusID) == 4)
                    {
                        checkStatusId = 4;
                    }
                    else if (Convert.ToInt32(json.StatusID) == 5)
                    {
                        checkStatusId = 5;
                    }
                    else
                    {
                        checkStatusId = 2;
                    }

                    if (checkStatusId != -1)
                    {
                        MstRiskResult.AStatusId = checkStatusId;
                    }

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.TimeLine);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.TimeLine = dt2.Date;
                        }
                        catch (Exception)
                        {
                            string TL = Convert.ToString(json.TimeLine);
                            dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.TimeLine = dt.Date;
                        }
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    DateTime dt14 = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                    {
                        try
                        {
                            DateTime dt13 = Convert.ToDateTime(json.ResponseDueDate);
                            DateTime dt23 = GetDate(dt13.ToString("dd/MM/yyyy"));
                            MstRiskResult.ResponseDueDate = dt23.Date;
                        }
                        catch (Exception)
                        {
                            string TL = Convert.ToString(json.ResponseDueDate);
                            dt14 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.ResponseDueDate = dt14.Date;
                        }
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (json.PersonResponsible == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (json.Owner == "-1")
                        {
                            MstRiskResult.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(json.Owner);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        if (json.ObservatioRating == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        if (json.ObservationCategory == "-1")
                        {
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                    {
                        if (json.ObservationSubCategory == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                        }
                    }
                    //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                    //{
                    //    if (json.GroupObservationCategoryID == "-1")
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = null;
                    //    }
                    //    else
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                    //    }
                    //}
                    bool Success = false;
                    InternalAuditTransaction TrasactionDetails = UserManagementRisk.GetTrasactionID(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                    if (TrasactionDetails == null)
                    {
                        RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                    }
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        if (Convert.ToInt32(json.StatusID) == 4)
                        {
                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            MstRiskResult.AStatusId = 4;
                            MstRiskResult.AuditeeResponse = AuditeeResponse;
                            Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                        else if (Convert.ToInt32(json.StatusID) == 5)
                        {
                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            MstRiskResult.AStatusId = 5;
                            MstRiskResult.AuditeeResponse = AuditeeResponse;
                            Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                        else if (Convert.ToInt32(json.StatusID) == 6)
                        {
                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            MstRiskResult.AStatusId = 6;
                            MstRiskResult.AuditeeResponse = AuditeeResponse;
                            Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                        else
                        {
                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        result = "success";
                    }

                    if (!string.IsNullOrEmpty(result))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(json.AudioVideoLink)))
                        {
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                            if (observationAudioVideoList.Count > 0)
                            {
                                string CheckCommaValOne = Convert.ToString(json.AudioVideoLink);
                                string[] audioVideoLinks = CheckCommaValOne.Split(',');
                                var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                if (filtered.Count > 0)
                                {
                                    foreach (var item in filtered)
                                    {
                                        RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                    }
                                }
                            }

                            ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                            {
                                AuditId = Convert.ToInt32(json.AuditID),
                                ATBTID = Convert.ToInt32(json.ATBDID),
                                IsActive = true,
                            };

                            string CheckCommaVal = Convert.ToString(json.AudioVideoLink);
                            if (CheckCommaVal.Contains(',')) //checking entered single value or multiple values
                            {
                                string[] audioVideoLinks = (Convert.ToString(json.AudioVideoLink)).Split(',');
                                int lenght = audioVideoLinks.Length;
                                string link = string.Empty;
                                for (int i = 0; i < audioVideoLinks.Length; i++)
                                {
                                    link = audioVideoLinks[i].ToString();
                                    objObservationAudioVideo.AudioVideoLink = link.Trim();
                                    if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                    {
                                        objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                        RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                    }
                                }
                            }
                            else
                            {
                                objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                {
                                    objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                }
                                else
                                {
                                    objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                    objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                }
                            }
                        }
                        else
                        {
                            // when user want delete all existing link then we will get null from txtMultilineVideolink
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                {
                                    RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                }
                            }
                        }
                    }
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/ReviewerSave")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage ReviewerSave(JObject dataValues)
        {
            try
            {
                string result = null;
                dynamic json = dataValues;
                string ErrorMessage = string.Empty;
                string CurrentStatus = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(json.CurrentStatusID)))
                {
                    CurrentStatus = UserManagementRisk.GetstatusbyID(Convert.ToInt32(json.CurrentStatusID));
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (Convert.ToString(json.PersonResponsible) == "-1" || Convert.ToString(json.PersonResponsible) == "")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (Convert.ToString(json.Owner) == "-1" || Convert.ToString(json.Owner) == "")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                    if ((Convert.ToString(CurrentStatus)) == "Closed")
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                        {
                            ErrorMessage = "Please enter Timeline.";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                #region
                if (Convert.ToInt32(json.StatusID) != -1)
                {
                    bool SubmitValue = false;
                    bool Flag = false;
                    long roleid = 4;
                    int LogedInUserRole = UserManagementRisk.CheckAssignedRoleOfUser(Convert.ToInt32(json.AuditID), AuthenticationHelper.UserID);
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    int customerID = -1;
                    List<long> ProcessList = new List<long>();
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuthenticationHelper.UserID, 3, Convert.ToString(json.FinYear), Convert.ToString(json.Period), Convert.ToInt32(json.BranchID), Convert.ToInt32(json.ATBDID), Convert.ToInt32(json.VerticalID), Convert.ToInt32(json.AuditID));
                    if (getScheduleonDetails != null)
                    {
                        ProcessList.Add(getScheduleonDetails.ProcessId);
                        var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(Convert.ToInt32(json.AuditID), AuthenticationHelper.UserID, ProcessList);
                        List<int> RoleListInAudit = CustomerManagementRisk.GetAuditListRole(Convert.ToInt32(json.AuditID), ProcessList);
                        int rowcount = RoleListInAudit.Count();
                        if (!string.IsNullOrEmpty(CurrentStatus))
                        {
                            //string Currentstatus = UserManagementRisk.GetstatusbyID(Convert.ToInt32(json.StatusID));
                            if (rowcount == 3 && CurrentStatus == "Closed" && (Convert.ToString(json.StatusID) == "2" || Convert.ToString(json.StatusID) == "4" || Convert.ToString(json.StatusID) == "6"))
                            {
                                SubmitValue = false;
                            }
                            else if (rowcount == 2 && CurrentStatus == "Final Review")
                            {
                                SubmitValue = false;
                            }
                            else
                            {
                                SubmitValue = true;
                            }
                            if (SubmitValue)
                            {
                                #region
                                InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                {
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    ForPerid = Convert.ToString(json.Period),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    IsDeleted = false,
                                    UserID = AuthenticationHelper.UserID,
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    RoleID = roleid,
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    AStatusId = Convert.ToInt32(json.CurrentStatusID),
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    AnnexueTitle = Convert.ToString(json.AnnexueTitle)
                                };

                                InternalAuditTransaction transaction = new InternalAuditTransaction()
                                {
                                    StatusId = Convert.ToInt32(json.CurrentStatusID),
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    SubProcessId = -1,
                                    ForPeriod = Convert.ToString(json.Period),
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    RoleID = roleid,
                                    UserID = AuthenticationHelper.UserID,
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                };
                                //AuditeeStepOneGoStatu objAuditee = new AuditeeStepOneGoStatu()
                                //{
                                //    AuditID = Convert.ToInt32(json.AuditID),
                                //    ATBDID = Convert.ToInt32(json.ATBDID),
                                //    Status = "Assigned", //Assigned && Pendig For Review are two status used
                                //    CreatedOn = DateTime.Now,
                                //    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                //    IsSentAuditee = false
                                //};
                                AuditClosure AuditClosureResult = new AuditClosure()
                                {
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    ForPeriod = Convert.ToString(json.Period),
                                    CustomerbranchId = Convert.ToInt32(json.BranchID),
                                    ACStatus = 1,
                                    AuditCommiteRemark = "",
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditCommiteFlag = 0,
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    AnnexueTitle = Convert.ToString(json.AnnexueTitle)
                                };
                                ObservationHistory objHistory = new ObservationHistory()
                                {
                                    UserID = AuthenticationHelper.UserID,
                                    RoleID = roleid,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    ATBTID = Convert.ToInt32(json.ATBDID)
                                };

                                if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                                {
                                    MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                    objHistory.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                    AuditClosureResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                }
                                objHistory.ObservationOld = json.hidObservation;
                                objHistory.ProcessWalkthroughOld = json.hidObservation;
                                objHistory.ActualWorkDoneOld = json.hidActualWorkDone;
                                objHistory.PopulationOld = json.hidpopulation;
                                objHistory.SampleOld = json.hidSample;
                                objHistory.ObservationTitleOld = json.hidObservationTitle;
                                objHistory.RiskOld = json.hidRisk;
                                objHistory.RootCauseOld = json.hidRootcost;
                                objHistory.FinancialImpactOld = json.hidfinancialImpact;
                                objHistory.RecommendationOld = json.hidRecommendation;
                                objHistory.ManagementResponseOld = json.hidMgtResponse;
                                objHistory.RemarksOld = json.hidRemarks;
                                objHistory.ScoreOld = json.hidAuditStepScore;
                                //objHistory.AnnexueTitleOld = json.hidAnnaxureTitle;
                                //objHistory.AnnexueTitle = json.AnnexueTitle;

                                if (!string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                                {
                                    MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                    transaction.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                }
                                else
                                {
                                    MstRiskResult.BriefObservation = null;
                                    transaction.BriefObservation = null;
                                    AuditClosureResult.BriefObservation = null;
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                                {
                                    MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                    transaction.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                }
                                else
                                {
                                    MstRiskResult.ObjBackground = null;
                                    transaction.ObjBackground = null;
                                    AuditClosureResult.ObjBackground = null;
                                }
                                //if (!string.IsNullOrEmpty(DefeciencyType))
                                //{
                                //    MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                                //    transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                                //}
                                //else
                                //{
                                //    MstRiskResult.DefeciencyType = null;
                                //    transaction.DefeciencyType = null;
                                //}
                                objHistory.OldBodyContent = Convert.ToString(json.hidBodyContent);
                                objHistory.OldUHComment = Convert.ToString(json.hidUHComment);
                                objHistory.OldPRESIDENTComment = Convert.ToString(json.hidPRESIDENTComment);

                                if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                                {
                                    MstRiskResult.BodyContent = "";
                                    objHistory.BodyContent = "";
                                    transaction.BodyContent = "";
                                    AuditClosureResult.BodyContent = "";
                                }
                                else
                                {
                                    MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    objHistory.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    transaction.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    AuditClosureResult.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                }


                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidPersonResponsible)))
                                {
                                    objHistory.PersonResponsibleOld = Convert.ToInt32(json.hidPersonResponsible);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationRating)))
                                {
                                    objHistory.ObservationRatingOld = Convert.ToInt32(json.hidObservationRating);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationCategory)))
                                {
                                    objHistory.ObservationCategoryOld = Convert.ToInt32(json.hidObservationCategory);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationSubCategory)))
                                {
                                    objHistory.ObservationSubCategoryOld = Convert.ToInt32(json.hidObservationSubCategory);
                                }
                                //if (!string.IsNullOrEmpty(Convert.ToString(json.hidGroupObservationCategory)))
                                //{
                                //    objHistory.GroupObservationCategoryID = Convert.ToInt32(json.hidGroupObservationCategory);
                                //}
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidOwner)))
                                {
                                    objHistory.OwnerOld = Convert.ToInt32(json.hidOwner);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidUserID)))
                                {
                                    objHistory.UserOld = Convert.ToInt32(json.hidUserID);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                                {
                                    objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                                {
                                    objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                                }

                                objHistory.AuditId = Convert.ToInt32(json.AuditID);

                                if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                                    MstRiskResult.AuditObjective = null;
                                else
                                    MstRiskResult.AuditObjective = Regex.Replace(Convert.ToString(json.AuditObjective).Trim(), @"\t|\n|\r", "");

                                if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                                    MstRiskResult.AuditSteps = "";
                                else
                                    MstRiskResult.AuditSteps = Regex.Replace(Convert.ToString(json.AuditSteps).Trim(), @"\t|\n|\r", "");

                                if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                                    MstRiskResult.AnalysisToBePerofrmed = "";
                                else
                                    MstRiskResult.AnalysisToBePerofrmed = Regex.Replace(Convert.ToString(json.AnalysisToBePerofrmed).Trim(), @"\t|\n|\r", "");

                                if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                                {
                                    MstRiskResult.ProcessWalkthrough = "";
                                    objHistory.ProcessWalkthrough = "";
                                }
                                else
                                {
                                    MstRiskResult.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");
                                    objHistory.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                                {
                                    MstRiskResult.ActivityToBeDone = "";
                                    objHistory.ActualWorkDone = "";
                                }
                                else
                                {
                                    MstRiskResult.ActivityToBeDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");
                                    objHistory.ActualWorkDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                                {
                                    MstRiskResult.Population = "";
                                    objHistory.Population = "";
                                }
                                else
                                {
                                    MstRiskResult.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");
                                    objHistory.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                                {
                                    MstRiskResult.Sample = "";
                                    objHistory.Sample = "";
                                }
                                else
                                {
                                    MstRiskResult.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");
                                    objHistory.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                                {
                                    MstRiskResult.ObservationTitle = "";
                                    objHistory.ObservationTitle = "";
                                    AuditClosureResult.ObservationTitle = "";
                                }
                                else
                                {
                                    MstRiskResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                                    objHistory.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                                {
                                    MstRiskResult.AnnexueTitle = "";
                                }
                                else
                                {
                                    MstRiskResult.AnnexueTitle = Regex.Replace(Convert.ToString(json.AnnexueTitle).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                                {
                                    MstRiskResult.Observation = "";
                                    objHistory.Observation = "";
                                    AuditClosureResult.Observation = "";
                                }
                                else
                                {
                                    MstRiskResult.Observation = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                    objHistory.Observation = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Observation = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                                {
                                    MstRiskResult.Risk = "";
                                    objHistory.Risk = "";
                                    AuditClosureResult.Risk = "";
                                }
                                else
                                {
                                    MstRiskResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                    objHistory.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                                {
                                    MstRiskResult.RootCost = null;
                                    objHistory.RootCause = null;
                                    AuditClosureResult.RootCost = null;
                                }
                                else
                                {
                                    MstRiskResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                    objHistory.RootCause = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                                {
                                    MstRiskResult.FinancialImpact = null;
                                    objHistory.FinancialImpact = null;
                                    AuditClosureResult.FinancialImpact = null;
                                }
                                else
                                {
                                    MstRiskResult.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();
                                    objHistory.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();
                                    AuditClosureResult.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                                {
                                    MstRiskResult.Recomendation = "";
                                    objHistory.Recommendation = "";
                                    AuditClosureResult.Recomendation = "";
                                }
                                else
                                {
                                    MstRiskResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                    objHistory.Recommendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                                {
                                    MstRiskResult.ManagementResponse = "";
                                    objHistory.ManagementResponse = "";
                                    AuditClosureResult.ManagementResponse = "";
                                }
                                else
                                {
                                    MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                    objHistory.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                                    MstRiskResult.FixRemark = "";
                                else
                                    MstRiskResult.FixRemark = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");

                                DateTime dt5 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidResponseDueDate)))
                                {
                                    try
                                    {
                                        DateTime dt15 = Convert.ToDateTime(json.hidResponseDueDate);
                                        DateTime dt25 = GetDate(dt15.ToString("dd/MM/yyyy"));
                                        objHistory.ResponseDueDateOld = dt25.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.hidResponseDueDate);
                                        dt5 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.TimeLine = dt5.Date;
                                    }
                                }
                                DateTime dt = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidTimeLine)))
                                {
                                    try
                                    {
                                        DateTime dt1 = Convert.ToDateTime(json.hidTimeLine);
                                        DateTime dt21 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                        objHistory.TimeLineOld = dt21.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.hidTimeLine);
                                        dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        objHistory.TimeLineOld = dt.Date;
                                    }
                                }
                                DateTime dt2 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                                {
                                    try
                                    {
                                        DateTime dt11 = Convert.ToDateTime(json.TimeLine);
                                        DateTime dt21 = GetDate(dt11.ToString("dd/MM/yyyy"));
                                        MstRiskResult.TimeLine = dt21.Date;
                                        objHistory.TimeLine = dt21.Date;
                                        AuditClosureResult.TimeLine = dt21.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.TimeLine);
                                        dt2 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.TimeLine = dt2.Date;
                                        objHistory.TimeLine = dt2.Date;
                                        AuditClosureResult.TimeLine = dt2.Date;
                                    }
                                }
                                else
                                {
                                    MstRiskResult.TimeLine = null;
                                    objHistory.TimeLine = null;
                                    AuditClosureResult.TimeLine = null;
                                }

                                DateTime dt3 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                                {
                                    try
                                    {
                                        DateTime dt13 = Convert.ToDateTime(json.ResponseDueDate);
                                        DateTime dt23 = GetDate(dt13.ToString("dd/MM/yyyy"));
                                        MstRiskResult.ResponseDueDate = dt23.Date;
                                        transaction.ResponseDueDate = dt23.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.ResponseDueDate);
                                        dt3 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.ResponseDueDate = dt3.Date;
                                        transaction.ResponseDueDate = dt3.Date;
                                    }
                                }
                                else
                                {
                                    MstRiskResult.ResponseDueDate = null;
                                    transaction.ResponseDueDate = null;
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                                {
                                    if (Convert.ToString(json.PersonResponsible) == "-1")
                                    {
                                        MstRiskResult.PersonResponsible = null;
                                        transaction.PersonResponsible = null;
                                        objHistory.PersonResponsible = null;
                                        AuditClosureResult.PersonResponsible = null;
                                    }
                                    else
                                    {
                                        MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        transaction.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        objHistory.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        AuditClosureResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                                {
                                    if (Convert.ToString(json.Owner) == "-1")
                                    {
                                        MstRiskResult.Owner = null;
                                        transaction.Owner = null;
                                        objHistory.Owner = null;
                                        AuditClosureResult.Owner = null;
                                    }
                                    else
                                    {
                                        MstRiskResult.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        transaction.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        objHistory.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        AuditClosureResult.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                                {
                                    if (Convert.ToString(json.ObservatioRating) == "-1")
                                    {
                                        transaction.ObservatioRating = null;
                                        MstRiskResult.ObservationRating = null;
                                        objHistory.ObservationRating = null;
                                        AuditClosureResult.ObservationRating = null;
                                    }
                                    else
                                    {
                                        transaction.ObservatioRating = Convert.ToInt32(json.ObservatioRating);
                                        MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                        objHistory.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                        AuditClosureResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                                {
                                    if (Convert.ToString(json.ObservationCategory) == "-1")
                                    {
                                        transaction.ObservationCategory = null;
                                        MstRiskResult.ObservationCategory = null;
                                        objHistory.ObservationCategory = null;
                                        AuditClosureResult.ObservationCategory = null;
                                    }
                                    else
                                    {
                                        transaction.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        objHistory.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        AuditClosureResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                    }
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                                {
                                    if (Convert.ToString(json.ObservationSubCategory) == "-1")
                                    {
                                        transaction.ObservationSubCategory = null;
                                        MstRiskResult.ObservationSubCategory = null;
                                        objHistory.ObservationSubCategory = null;
                                        AuditClosureResult.ObservationSubCategory = null;
                                    }
                                    else
                                    {
                                        transaction.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        objHistory.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        AuditClosureResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                    }
                                }
                                //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                                //{
                                //    if (Convert.ToString(json.GroupObservationCategoryID) == "-1")
                                //    {
                                //        transaction.GroupObservationCategoryID = null;
                                //        MstRiskResult.GroupObservationCategoryID = null;
                                //        objHistory.GroupObservationCategoryID = null;
                                //        AuditClosureResult.GroupObservationCategoryID = null;
                                //    }
                                //    else
                                //    {
                                //        transaction.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        objHistory.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        AuditClosureResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //    }
                                //}

                                string remark = string.Empty;
                                if ((Convert.ToString(CurrentStatus)) == "Closed")
                                {
                                    transaction.StatusId = 3;
                                    MstRiskResult.AStatusId = 3;
                                    remark = "Audit Steps Closed.";
                                    MstRiskResult.AuditeeResponse = "AC";
                                    transaction.Remarks = remark.Trim();
                                }
                                else if ((Convert.ToString(CurrentStatus)) == "Team Review")
                                {
                                    transaction.StatusId = 4;
                                    MstRiskResult.AStatusId = 4;
                                    remark = "Audit Steps Under Team Review.";
                                    MstRiskResult.AuditeeResponse = "RT";
                                    transaction.Remarks = remark.Trim();
                                }
                                else if ((Convert.ToString(CurrentStatus)) == "Final Review")
                                {
                                    transaction.StatusId = 5;
                                    MstRiskResult.AStatusId = 5;
                                    remark = "Audit Steps Under Final Review.";
                                    if (LogedInUserRole == 4)
                                    {
                                        MstRiskResult.AuditeeResponse = "RF";
                                    }
                                    else if (LogedInUserRole == 5)
                                    {
                                        MstRiskResult.AuditeeResponse = "HF";
                                    }
                                    transaction.Remarks = remark.Trim();
                                }
                                else if ((Convert.ToString(CurrentStatus)) == "Auditee Review")
                                {
                                    if (MstRiskResult != null)
                                    {
                                        var AuditteeStatusID = RiskCategoryManagement.GetLatestStatusOfAudittee(MstRiskResult);
                                        if (AuditteeStatusID == 6)
                                        {
                                            var AuditteeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                                            if (AuditteeResponse == "RS")
                                            {
                                                MstRiskResult.AuditeeResponse = "RS";
                                            }
                                            else
                                            {
                                                MstRiskResult.AuditeeResponse = "RA";
                                            }
                                        }
                                        else
                                        {
                                            MstRiskResult.AuditeeResponse = "RS";
                                        }
                                    }
                                    transaction.StatusId = 6;
                                    MstRiskResult.AStatusId = 6;
                                    remark = "Audit Steps Under Auditee Review.";
                                    transaction.Remarks = remark.Trim();
                                }
                                objHistory.Remarks = remark;

                                bool Success1 = false;
                                bool Success2 = false;
                                if ((Convert.ToString(CurrentStatus)) == "Auditee Review")
                                {
                                    if (string.IsNullOrEmpty(Convert.ToString(json.Observation).Trim()))
                                    {
                                        ErrorMessage = "There is no observation, please select the other status.";
                                        if (!string.IsNullOrEmpty(ErrorMessage))
                                        {
                                            string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                            return new HttpResponseMessage()
                                            {
                                                Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                            };
                                        }
                                    }
                                }
                                if (RiskCategoryManagement.InternalControlResultExistsCheckLastestNullWithStatus(MstRiskResult))
                                {
                                    //if Exists With Status
                                    // Submitted
                                    if (Convert.ToInt32(json.StatusID) == 2 && Convert.ToString(CurrentStatus) == "Team Review")
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 2 && Convert.ToString(CurrentStatus) == "Auditee Review")
                                    {
                                        if (MstRiskResult.AStatusId == 6)
                                        {
                                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                        }
                                        else
                                        {
                                            if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                            {
                                                MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                            }
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 2 && Convert.ToString(CurrentStatus) == "Final Review")
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }// Team Review
                                    else if (Convert.ToInt32(json.StatusID) == 4 && (Convert.ToString(CurrentStatus) == "Auditee Review"))
                                    {
                                        if (MstRiskResult.AStatusId == 6)
                                        {
                                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                        }
                                        else
                                        {
                                            if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                            {
                                                MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                            }
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 4 && (Convert.ToString(CurrentStatus) == "Final Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }//Auditee Review
                                    else if (Convert.ToInt32(json.StatusID) == 6 && (Convert.ToString(CurrentStatus) == "Final Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 6 && (Convert.ToString(CurrentStatus) == "Auditee Review"))
                                    {
                                        if (MstRiskResult.AStatusId == 6)
                                        {
                                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                        }
                                        else
                                        {
                                            if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                            {
                                                MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                                MstRiskResult.UpdatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExistsWithStatus(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                        else
                                        {
                                            MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                        }
                                        //Code added by Sushant
                                        if (Convert.ToString(CurrentStatus) == "Closed")
                                        {
                                            if (RiskCategoryManagement.InternalControlResultClosedCount(MstRiskResult) == 1)
                                            {
                                                #region  AuditClosure 
                                                using (AuditControlEntities entities = new AuditControlEntities())
                                                {
                                                    var CheckClosure = (from row in entities.AuditClosures
                                                                        where row.AuditID == MstRiskResult.AuditID
                                                                        && row.ATBDId == MstRiskResult.ATBDId
                                                                        select row).FirstOrDefault();
                                                    if (CheckClosure == null)
                                                    {
                                                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                                        RiskCategoryManagement.AuditClosureDetailsExists(customerID,
                                                        Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID,
                                                        MstRiskResult.ForPerid, MstRiskResult.FinancialYear, Convert.ToInt32(json.AuditID));

                                                        var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                              where row.ProcessId == MstRiskResult.ProcessId
                                                                              && row.FinancialYear == MstRiskResult.FinancialYear
                                                                              && row.ForPerid == MstRiskResult.ForPerid
                                                                              && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                              && row.UserID == MstRiskResult.UserID
                                                                              && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                              && row.VerticalID == MstRiskResult.VerticalID
                                                                              && row.AuditID == MstRiskResult.AuditID
                                                                              select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                                        if (RecordtoUpdate != null)
                                                        {
                                                            AuditClosureResult.ResultID = RecordtoUpdate;
                                                            AuditClosureResult.CreatedBy = AuthenticationHelper.UserID;
                                                            AuditClosureResult.CreatedOn = DateTime.Now;
                                                            RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    MstRiskResult.CreatedBy = AuthenticationHelper.UserID;
                                    MstRiskResult.CreatedOn = DateTime.Now;
                                    Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                    //Code added by Sushant
                                    if (Convert.ToString(CurrentStatus) == "Closed")
                                    {
                                        if (RiskCategoryManagement.InternalControlResultClosedCount(MstRiskResult) == 1)
                                        {
                                            #region  AuditClosure 
                                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                            using (AuditControlEntities entities = new AuditControlEntities())
                                            {
                                                var CheckClosure = (from row in entities.AuditClosures
                                                                    where row.AuditID == MstRiskResult.AuditID
                                                                    && row.ATBDId == MstRiskResult.ATBDId
                                                                    select row).FirstOrDefault();
                                                if (CheckClosure == null)
                                                {
                                                    RiskCategoryManagement.AuditClosureDetailsExists(customerID,
                                                        Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID,
                                                        MstRiskResult.ForPerid, MstRiskResult.FinancialYear, Convert.ToInt32(json.AuditID));

                                                    var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                          where row.ProcessId == MstRiskResult.ProcessId
                                                                          && row.FinancialYear == MstRiskResult.FinancialYear
                                                                          && row.ForPerid == MstRiskResult.ForPerid
                                                                          && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                          && row.UserID == MstRiskResult.UserID
                                                                          && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                          && row.VerticalID == MstRiskResult.VerticalID
                                                                          && row.AuditID == MstRiskResult.AuditID
                                                                          select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                                    if (RecordtoUpdate != null)
                                                    {
                                                        AuditClosureResult.ResultID = RecordtoUpdate;
                                                        AuditClosureResult.CreatedBy = AuthenticationHelper.UserID;
                                                        AuditClosureResult.CreatedOn = DateTime.Now;
                                                        RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(objHistory.Observation))
                                {
                                    if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                    {
                                        objHistory.CreatedBy = AuthenticationHelper.UserID;
                                        objHistory.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.AddObservationHistory(objHistory);
                                    }
                                }

                                if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                {
                                    if (Convert.ToInt32(json.StatusID) == 4 || Convert.ToInt32(json.StatusID) == 5 || Convert.ToInt32(json.StatusID) == 6)
                                    {
                                        transaction.CreatedBy = AuthenticationHelper.UserID;
                                        transaction.CreatedOn = DateTime.Now;
                                        Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                    }
                                    else
                                    {
                                        transaction.UpdatedBy = AuthenticationHelper.UserID;
                                        transaction.UpdatedOn = DateTime.Now;
                                        Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatusReviewer(transaction);
                                    }
                                }
                                else
                                {
                                    transaction.CreatedBy = AuthenticationHelper.UserID;
                                    transaction.CreatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                }
                                string Testremark = "";
                                if (string.IsNullOrEmpty(Convert.ToString(json.txtRemark)))
                                {
                                    Testremark = "NA";
                                }
                                else
                                {
                                    Testremark = Convert.ToString(json.txtRemark).Trim();
                                }
                                #endregion

                                if (!string.IsNullOrEmpty(objHistory.Observation))
                                {
                                    if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                    {
                                        RiskCategoryManagement.AddObservationHistory(objHistory);
                                    }
                                }

                                #region

                                InternalReviewHistory RH = new InternalReviewHistory()
                                {
                                    ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    Dated = DateTime.Now,
                                    Remarks = Testremark,
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    FixRemark = "NA",
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    //  UniqueFlag= UniqueFlag,
                                };
                                if (RiskCategoryManagement.CheckRecordExist(RH))
                                {
                                    Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                                }

                                #endregion

                                if (Success1 == true && Success2 == true)
                                {
                                    //if (Convert.ToString(CurrentStatus) == "Auditee Review")
                                    //{
                                    //    UserManagementRisk.AddAuditeeStatusRemark(objAuditee);
                                    //}

                                    #region Audit Close 
                                    if (Convert.ToString(CurrentStatus) == "Closed")
                                    {
                                        if (RiskCategoryManagement.CheckAllStepClosed(Convert.ToInt32(json.AuditID)))
                                        {
                                            AuditClosureClose auditclosureclose = new AuditClosureClose();
                                            auditclosureclose.CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                                            auditclosureclose.CustomerBranchId = Convert.ToInt32(json.BranchID);
                                            auditclosureclose.FinancialYear = Convert.ToString(json.FinYear);
                                            auditclosureclose.ForPeriod = Convert.ToString(json.Period);
                                            auditclosureclose.CreatedBy = AuthenticationHelper.UserID;
                                            auditclosureclose.CreatedDate = DateTime.Today.Date;
                                            auditclosureclose.ACCStatus = 1;
                                            auditclosureclose.VerticalID = Convert.ToInt32(json.VerticalID);
                                            auditclosureclose.AuditId = Convert.ToInt32(json.AuditID);
                                            if (!RiskCategoryManagement.AuditClosureCloseExists(auditclosureclose.CustomerBranchId, (int)auditclosureclose.VerticalID, auditclosureclose.FinancialYear, auditclosureclose.ForPeriod, Convert.ToInt32(json.AuditID)))
                                            {
                                                RiskCategoryManagement.CreateAuditClosureClosed(auditclosureclose);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    ErrorMessage = "Something went wrong.";
                                    if (!string.IsNullOrEmpty(ErrorMessage))
                                    {
                                        string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                        return new HttpResponseMessage()
                                        {
                                            Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                        };
                                    }
                                }

                                #region Audio Vedeo save
                                if (Success1)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(json.AudioVideoLink)))
                                    {
                                        List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                                        if (observationAudioVideoList.Count > 0)
                                        {
                                            string CheckCommaValOne = Convert.ToString(json.AudioVideoLink);
                                            string[] audioVideoLinks = CheckCommaValOne.Split(',');
                                            var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                            if (filtered.Count > 0)
                                            {
                                                foreach (var item in filtered)
                                                {
                                                    RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                                }
                                            }
                                        }

                                        ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                                        {
                                            AuditId = Convert.ToInt32(json.AuditID),
                                            ATBTID = Convert.ToInt32(json.ATBDID),
                                            IsActive = true,
                                        };

                                        string CheckCommaVal = Convert.ToString(json.AudioVideoLink);
                                        if (CheckCommaVal.Contains(',')) //checking entered single value or multiple values
                                        {
                                            string[] audioVideoLinks = (Convert.ToString(json.AudioVideoLink)).Split(',');
                                            int lenght = audioVideoLinks.Length;
                                            string link = string.Empty;
                                            for (int i = 0; i < audioVideoLinks.Length; i++)
                                            {
                                                link = audioVideoLinks[i].ToString();
                                                objObservationAudioVideo.AudioVideoLink = link.Trim();
                                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                                {
                                                    objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                                }
                                                else
                                                {
                                                    objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                            if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                            {
                                                objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                                objObservationAudioVideo.CreatedOn = DateTime.Now;
                                                RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                            }
                                            else
                                            {
                                                objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                                objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                                objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                                RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // when user want delete all existing link then we will get null from txtMultilineVideolink
                                        List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                                        if (observationAudioVideoList.Count > 0)
                                        {
                                            foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                            {
                                                RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessage = "Server Error Occured. Please try again.";
                                }
                                #endregion
                            }
                            else
                            {
                                ErrorMessage = "You don't have access to change status into " + Convert.ToString(CurrentStatus) + ".";
                                if (!string.IsNullOrEmpty(ErrorMessage))
                                {
                                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                    return new HttpResponseMessage()
                                    {
                                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                    };
                                }
                            }
                        }
                        else
                        {
                            ErrorMessage = "Please Select Status";
                            if (!string.IsNullOrEmpty(ErrorMessage))
                            {
                                string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                return new HttpResponseMessage()
                                {
                                    Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                };
                            }
                        }
                    }//getScheduleonDetails
                }//ddlFilterStatus  Null
                #endregion
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    result = ErrorMessage;
                }
                else
                {
                    result = "success";
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string jsonstring = JsonConvert.SerializeObject("Something went wrong");
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion

        #region PersonResponsible Save
        [Route("AuditStatus/PersonResponsibleObservationTabOne")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage PersonResponsibleObservationTabOne(JObject data)
        {
            try
            {
                string ErrorMessage = string.Empty;
                dynamic json = data;
                string result = null;
                if (!string.IsNullOrEmpty(Convert.ToString(json.ATBDID)) || !string.IsNullOrEmpty(Convert.ToString(json.AuditID)))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                        {
                            ErrorMessage = "Please Enter Management Response.";
                        }
                        else if (string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                        {
                            ErrorMessage = "Please Enter TimeLine.";
                        }

                        if (!string.IsNullOrEmpty(ErrorMessage))
                        {
                            string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                            };
                        }
                    }
                    long roleid = 4;
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = json.ScheduleOnID,
                        ProcessId = json.ProcessID,
                        FinancialYear = json.FinYear,
                        ForPerid = json.Period,
                        CustomerBranchId = Convert.ToInt32(json.BranchID),
                        IsDeleted = false,
                        UserID = AuthenticationHelper.UserID,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = json.InstanceID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                        AuditeeResponse = "AS",
                    };
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        StatusId = 6,
                        CreatedByText = AuthenticationHelper.User,
                        StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                        AuditScheduleOnID = json.ScheduleOnID,
                        FinancialYear = json.FinYear,
                        CustomerBranchId = json.BranchID,
                        InternalAuditInstance = json.InstanceID,
                        ProcessId = json.ProcessID,
                        SubProcessId = -1,
                        ForPeriod = json.Period,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        UserID = AuthenticationHelper.UserID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                    {
                        MstRiskResult.AuditObjective = null;
                    }
                    else
                        MstRiskResult.AuditObjective = Regex.Replace(Convert.ToString(json.AuditObjective).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = Regex.Replace(Convert.ToString(json.AuditSteps).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = Regex.Replace(Convert.ToString(json.AnalysisToBePerofrmed).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");

                    MstRiskResult.ObservationNumber = "";

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        MstRiskResult.AnnexueTitle = Regex.Replace(Convert.ToString(json.AnnexueTitle).Trim(), @"\t|\n|\r", "");
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                    }

                    if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                        MstRiskResult.BriefObservation = "";
                    else
                        MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                        MstRiskResult.ObjBackground = "";
                    else
                        MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.TimeLine);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.TimeLine = dt2.Date;
                        }
                        catch (Exception)
                        {
                            string TL = Convert.ToString(json.TimeLine);
                            dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.TimeLine = dt.Date;
                        }
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    DateTime responseDueDate = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.ResponseDueDate);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.ResponseDueDate = dt2;
                        }
                        catch (Exception)
                        {
                            string RD = Convert.ToString(json.ResponseDueDate);
                            responseDueDate = DateTime.ParseExact(RD, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.ResponseDueDate = responseDueDate.Date;
                        }
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (json.PersonResponsible == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (json.Owner == "-1")
                        {
                            MstRiskResult.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(json.Owner);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        if (json.ObservatioRating == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        if (json.ObservationCategory == "-1")
                        {
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                    {
                        if (json.ObservationSubCategory == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                        }
                    }
                    //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                    //{
                    //    if (json.GroupObservationCategoryID == "-1")
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = null;
                    //    }
                    //    else
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                    //    }
                    //}
                    bool Success = false;
                    InternalAuditTransaction TrasactionDetails = UserManagementRisk.GetTrasactionID(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                    if (TrasactionDetails == null)
                    {
                        RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                    }
                    MstRiskResult.AStatusId = 6;
                    bool Success1 = false;
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    MstRiskResult.AuditeeResponse = AuditeeResponse;
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        if (RiskCategoryManagement.InternalControlResultExistsCheckLastestNullWithStatus(MstRiskResult))
                        {
                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.UpdatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                        else
                        {
                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        result = "success";
                    }
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/PersonResponsibleSave")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage PersonResponsibleSave(JObject dataValues)
        {
            try
            {
                string result = "";
                dynamic json = dataValues;
                string ErrorMessage = string.Empty;
                #region

                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                    {
                        ErrorMessage = "Please Enter Management Response.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                    {
                        ErrorMessage = "Please Enter TimeLine.";
                    }

                    if (!string.IsNullOrEmpty(ErrorMessage))
                    {
                        string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                        return new HttpResponseMessage()
                        {
                            Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                        };
                    }
                }
                #endregion

                #region
                if (Convert.ToInt32(json.StatusID) == 6)
                {
                    bool Flag = false;
                    long roleid = 4;
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    int customerID = -1;
                    List<long> ProcessList = new List<long>();
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuthenticationHelper.UserID, 3, Convert.ToString(json.FinYear), Convert.ToString(json.Period), Convert.ToInt32(json.BranchID), Convert.ToInt32(json.ATBDID), Convert.ToInt32(json.VerticalID), Convert.ToInt32(json.AuditID));
                    if (getScheduleonDetails != null)
                    {
                        ProcessList.Add(getScheduleonDetails.ProcessId);
                        var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(Convert.ToInt32(json.AuditID), AuthenticationHelper.UserID, ProcessList);
                        List<int> RoleListInAudit = CustomerManagementRisk.GetAuditListRole(Convert.ToInt32(json.AuditID), ProcessList);
                        int rowcount = RoleListInAudit.Count();

                        #region
                        InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                        {
                            AuditScheduleOnID = getScheduleonDetails.ID,
                            ProcessId = getScheduleonDetails.ProcessId,
                            FinancialYear = Convert.ToString(json.FinYear),
                            ForPerid = Convert.ToString(json.Period),
                            CustomerBranchId = Convert.ToInt32(json.BranchID),
                            IsDeleted = false,
                            UserID = AuthenticationHelper.UserID,
                            ATBDId = Convert.ToInt32(json.ATBDID),
                            RoleID = roleid,
                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                            AStatusId = 6,
                            VerticalID = Convert.ToInt32(json.VerticalID),
                            AuditID = Convert.ToInt32(json.AuditID),
                            AnnexueTitle = Convert.ToString(json.AnnexueTitle),
                            AuditeeResponse = "AS"
                        };

                        InternalAuditTransaction transaction = new InternalAuditTransaction()
                        {
                            StatusId = 6,
                            CreatedByText = AuthenticationHelper.User,
                            StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                            AuditScheduleOnID = getScheduleonDetails.ID,
                            FinancialYear = Convert.ToString(json.FinYear),
                            CustomerBranchId = Convert.ToInt32(json.BranchID),
                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                            ProcessId = getScheduleonDetails.ProcessId,
                            SubProcessId = -1,
                            ForPeriod = Convert.ToString(json.Period),
                            ATBDId = Convert.ToInt32(json.ATBDID),
                            RoleID = roleid,
                            UserID = AuthenticationHelper.UserID,
                            VerticalID = Convert.ToInt32(json.VerticalID),
                            AuditID = Convert.ToInt32(json.AuditID),
                        };
                        ObservationHistory objHistory = new ObservationHistory()
                        {
                            UserID = AuthenticationHelper.UserID,
                            RoleID = roleid,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            ATBTID = Convert.ToInt32(json.ATBDID)
                        };

                        if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                        {
                            MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                            objHistory.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                        }
                        objHistory.ObservationOld = json.hidObservation;
                        objHistory.ProcessWalkthroughOld = json.hidObservation;
                        objHistory.ActualWorkDoneOld = json.hidActualWorkDone;
                        objHistory.PopulationOld = json.hidpopulation;
                        objHistory.SampleOld = json.hidSample;
                        objHistory.ObservationTitleOld = json.hidObservationTitle;
                        objHistory.RiskOld = json.hidRisk;
                        objHistory.RootCauseOld = json.hidRootcost;
                        objHistory.FinancialImpactOld = json.hidfinancialImpact;
                        objHistory.RecommendationOld = json.hidRecommendation;
                        objHistory.ManagementResponseOld = json.hidMgtResponse;
                        objHistory.RemarksOld = json.hidRemarks;
                        objHistory.ScoreOld = json.hidAuditStepScore;
                        //objHistory.AnnexueTitleOld = json.hidAnnaxureTitle;
                        //objHistory.AnnexueTitle = json.AnnexueTitle;
                        DateTime dt5 = new DateTime();
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidResponseDueDate)))
                        {
                            try
                            {
                                DateTime dt15 = Convert.ToDateTime(json.hidResponseDueDate);
                                DateTime dt25 = GetDate(dt15.ToString("dd/MM/yyyy"));
                                objHistory.ResponseDueDateOld = dt25.Date;
                            }
                            catch (Exception)
                            {
                                string TL = Convert.ToString(json.hidResponseDueDate);
                                dt5 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.TimeLine = dt5.Date;
                            }
                        }
                        DateTime dt = new DateTime();
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidTimeLine)))
                        {
                            try
                            {
                                DateTime dt1 = Convert.ToDateTime(json.hidTimeLine);
                                DateTime dt21 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                objHistory.TimeLineOld = dt21.Date;
                            }
                            catch (Exception)
                            {
                                string TL = Convert.ToString(json.hidTimeLine);
                                dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                objHistory.TimeLineOld = dt.Date;
                            }
                        }
                        DateTime dt2 = new DateTime();
                        if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                        {
                            try
                            {
                                DateTime dt11 = Convert.ToDateTime(json.TimeLine);
                                DateTime dt21 = GetDate(dt11.ToString("dd/MM/yyyy"));
                                MstRiskResult.TimeLine = dt21.Date;
                                objHistory.TimeLine = dt21.Date;
                            }
                            catch (Exception)
                            {
                                string TL = Convert.ToString(json.TimeLine);
                                dt2 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.TimeLine = dt2.Date;
                                objHistory.TimeLine = dt2.Date;
                            }
                        }
                        else
                        {
                            MstRiskResult.TimeLine = null;
                            objHistory.TimeLine = null;
                        }

                        DateTime dt3 = new DateTime();
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                        {
                            try
                            {
                                DateTime dt13 = Convert.ToDateTime(json.ResponseDueDate);
                                DateTime dt23 = GetDate(dt13.ToString("dd/MM/yyyy"));
                                MstRiskResult.ResponseDueDate = dt23.Date;
                                transaction.ResponseDueDate = dt23.Date;
                            }
                            catch (Exception)
                            {
                                string TL = Convert.ToString(json.ResponseDueDate);
                                dt3 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                MstRiskResult.ResponseDueDate = dt3.Date;
                                transaction.ResponseDueDate = dt3.Date;
                            }
                        }
                        else
                        {
                            MstRiskResult.ResponseDueDate = null;
                            transaction.ResponseDueDate = null;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                        {
                            MstRiskResult.BriefObservation = Convert.ToString(json.BriefObservation).Trim();
                            transaction.BriefObservation = Convert.ToString(json.BriefObservation).Trim();
                        }
                        else
                        {
                            MstRiskResult.BriefObservation = null;
                            transaction.BriefObservation = null;
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                        {
                            MstRiskResult.ObjBackground = Convert.ToString(json.ObjBackground).Trim();
                            transaction.ObjBackground = Convert.ToString(json.ObjBackground).Trim();
                        }
                        else
                        {
                            MstRiskResult.ObjBackground = null;
                            transaction.ObjBackground = null;
                        }
                        //if (!string.IsNullOrEmpty(DefeciencyType))
                        //{
                        //    MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        //    transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                        //}
                        //else
                        //{
                        //    MstRiskResult.DefeciencyType = null;
                        //    transaction.DefeciencyType = null;
                        //}
                        objHistory.OldBodyContent = Convert.ToString(json.hidBodyContent);
                        objHistory.OldUHComment = Convert.ToString(json.hidUHComment);
                        objHistory.OldPRESIDENTComment = Convert.ToString(json.hidPRESIDENTComment);

                        if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                        {
                            MstRiskResult.BodyContent = "";
                            objHistory.BodyContent = "";
                            transaction.BodyContent = "";
                        }
                        else
                        {
                            MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();
                            objHistory.BodyContent = Convert.ToString(json.BodyContent).Trim();
                            transaction.BodyContent = Convert.ToString(json.BodyContent).Trim();
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidPersonResponsible)))
                        {
                            objHistory.PersonResponsibleOld = Convert.ToInt32(json.hidPersonResponsible);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationRating)))
                        {
                            objHistory.ObservationRatingOld = Convert.ToInt32(json.hidObservationRating);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationCategory)))
                        {
                            objHistory.ObservationCategoryOld = Convert.ToInt32(json.hidObservationCategory);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationSubCategory)))
                        {
                            objHistory.ObservationSubCategoryOld = Convert.ToInt32(json.hidObservationSubCategory);
                        }
                        //if (!string.IsNullOrEmpty(Convert.ToString(json.hidGroupObservationCategory)))
                        //{
                        //    objHistory.GroupObservationCategoryID = Convert.ToInt32(json.hidGroupObservationCategory);
                        //}
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidOwner)))
                        {
                            objHistory.OwnerOld = Convert.ToInt32(json.hidOwner);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidUserID)))
                        {
                            objHistory.UserOld = Convert.ToInt32(json.hidUserID);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                        {
                            objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                        {
                            objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                        }

                        objHistory.AuditId = Convert.ToInt32(json.AuditID);

                        if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                            MstRiskResult.AuditObjective = null;
                        else
                            MstRiskResult.AuditObjective = Convert.ToString(json.AuditObjective).Trim();

                        if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                            MstRiskResult.AuditSteps = "";
                        else
                            MstRiskResult.AuditSteps = Convert.ToString(json.AuditSteps).Trim();

                        if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                            MstRiskResult.AnalysisToBePerofrmed = "";
                        else
                            MstRiskResult.AnalysisToBePerofrmed = Convert.ToString(json.AnalysisToBePerofrmed).Trim();

                        if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                        {
                            MstRiskResult.ProcessWalkthrough = "";
                            objHistory.ProcessWalkthrough = "";
                        }
                        else
                        {
                            MstRiskResult.ProcessWalkthrough = Convert.ToString(json.ProcessWalkthrough).Trim();
                            objHistory.ProcessWalkthrough = Convert.ToString(json.ProcessWalkthrough).Trim();
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                        {
                            MstRiskResult.ActivityToBeDone = "";
                            objHistory.ActualWorkDone = "";
                        }
                        else
                        {
                            MstRiskResult.ActivityToBeDone = Convert.ToString(json.ActivityToBeDone).Trim();
                            objHistory.ActualWorkDone = Convert.ToString(json.ActivityToBeDone).Trim();
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                        {
                            MstRiskResult.Population = "";
                            objHistory.Population = "";
                        }
                        else
                        {
                            MstRiskResult.Population = Convert.ToString(json.Population).Trim();
                            objHistory.Population = Convert.ToString(json.Population).Trim();
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                        {
                            MstRiskResult.Sample = "";
                            objHistory.Sample = "";
                        }
                        else
                        {
                            MstRiskResult.Sample = Convert.ToString(json.Sample).Trim();
                            objHistory.Sample = Convert.ToString(json.Sample).Trim();
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                        {
                            MstRiskResult.ObservationTitle = "";
                            objHistory.ObservationTitle = "";
                        }
                        else
                        {
                            MstRiskResult.ObservationTitle = Convert.ToString(json.ObservationTitle).Trim();
                            objHistory.ObservationTitle = Convert.ToString(json.ObservationTitle).Trim();
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                        {
                            MstRiskResult.AnnexueTitle = "";
                        }
                        else
                        {
                            MstRiskResult.AnnexueTitle = Convert.ToString(json.AnnexueTitle).Trim();
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                        {
                            MstRiskResult.Observation = "";
                            objHistory.Observation = "";
                        }
                        else
                        {
                            MstRiskResult.Observation = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                            objHistory.Observation = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                        {
                            MstRiskResult.Risk = "";
                            objHistory.Risk = "";
                        }
                        else
                        {
                            MstRiskResult.Risk = Convert.ToString(json.Risk).Trim();
                            objHistory.Risk = Convert.ToString(json.Risk).Trim();
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                        {
                            MstRiskResult.RootCost = null;
                            objHistory.RootCause = null;
                        }
                        else
                        {
                            MstRiskResult.RootCost = Convert.ToString(json.RootCost).Trim();
                            objHistory.RootCause = Convert.ToString(json.RootCost).Trim();
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                        {
                            MstRiskResult.FinancialImpact = null;
                            objHistory.FinancialImpact = null;
                        }
                        else
                        {
                            MstRiskResult.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();
                            objHistory.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                        {
                            MstRiskResult.Recomendation = "";
                            objHistory.Recommendation = "";
                        }
                        else
                        {
                            MstRiskResult.Recomendation = Convert.ToString(json.Recomendation).Trim();
                            objHistory.Recommendation = Convert.ToString(json.Recomendation).Trim();
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                        {
                            MstRiskResult.ManagementResponse = "";
                            objHistory.ManagementResponse = "";
                        }
                        else
                        {
                            MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                            objHistory.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                        }

                        if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                            MstRiskResult.FixRemark = "";
                        else
                            MstRiskResult.FixRemark = Convert.ToString(json.FixRemark).Trim();

                        if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                        {
                            if (Convert.ToString(json.PersonResponsible) == "-1")
                            {
                                MstRiskResult.PersonResponsible = null;
                                transaction.PersonResponsible = null;
                                objHistory.PersonResponsible = null;
                            }
                            else
                            {
                                MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                transaction.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                objHistory.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                        {
                            if (Convert.ToString(json.Owner) == "-1")
                            {
                                MstRiskResult.Owner = null;
                                transaction.Owner = null;
                                objHistory.Owner = null;
                            }
                            else
                            {
                                MstRiskResult.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                transaction.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                objHistory.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                        {
                            if (Convert.ToString(json.ObservatioRating) == "-1")
                            {
                                transaction.ObservatioRating = null;
                                MstRiskResult.ObservationRating = null;
                                objHistory.ObservationRating = null;
                            }
                            else
                            {
                                transaction.ObservatioRating = Convert.ToInt32(json.ObservatioRating);
                                MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                objHistory.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                            }
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                        {
                            if (Convert.ToString(json.ObservationCategory) == "-1")
                            {
                                transaction.ObservationCategory = null;
                                MstRiskResult.ObservationCategory = null;
                                objHistory.ObservationCategory = null;
                            }
                            else
                            {
                                transaction.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                objHistory.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                            }
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                        {
                            if (Convert.ToString(json.ObservationSubCategory) == "-1")
                            {
                                transaction.ObservationSubCategory = null;
                                MstRiskResult.ObservationSubCategory = null;
                                objHistory.ObservationSubCategory = null;
                            }
                            else
                            {
                                transaction.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                objHistory.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                            }
                        }
                        //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                        //{
                        //    if (Convert.ToString(json.GroupObservationCategoryID) == "-1")
                        //    {
                        //        transaction.GroupObservationCategoryID = null;
                        //        MstRiskResult.GroupObservationCategoryID = null;
                        //        objHistory.GroupObservationCategoryID = null;
                        //    }
                        //    else
                        //    {
                        //        transaction.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                        //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                        //        objHistory.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                        //    }
                        //}

                        string remark = string.Empty;
                        remark = "Audit Steps Under Auditee Review.";
                        MstRiskResult.AuditeeResponse = "RA";
                        objHistory.Remarks = remark;
                        transaction.Remarks = remark.Trim();
                        bool Success1 = false;
                        bool Success2 = false;
                        if (RiskCategoryManagement.InternalControlResultExistsCheckLastestNullWithStatus(MstRiskResult))
                        {
                            if (MstRiskResult.AStatusId == 6)
                            {
                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                MstRiskResult.CreatedOn = DateTime.Now;
                                Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                result = "success";
                            }
                            else
                            {
                                if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                {
                                    MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    MstRiskResult.UpdatedOn = DateTime.Now;
                                    Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                    result = "success";
                                }
                            }
                        }
                        else
                        {
                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            MstRiskResult.CreatedOn = DateTime.Now;
                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                            result = "success";
                        }
                        //if (Success1)
                        //{
                        //    UserManagementRisk.UpdateAuditeeStatusOnGoStatus(MstRiskResult.AuditID, MstRiskResult.ATBDId);
                        //}

                        if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.CreatedOn = DateTime.Now;
                            Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                            result = "success";
                        }
                        else
                        {
                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                            transaction.CreatedOn = DateTime.Now;
                            Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                            result = "success";
                        }

                        if (!string.IsNullOrEmpty(objHistory.Observation))
                        {
                            if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                            {
                                objHistory.CreatedBy = AuthenticationHelper.UserID;
                                objHistory.CreatedOn = DateTime.Now;
                                RiskCategoryManagement.AddObservationHistory(objHistory);
                            }
                        }

                        string Testremark = "";
                        if (string.IsNullOrEmpty(Convert.ToString(json.txtRemark)))
                        {
                            Testremark = "NA";
                        }
                        else
                        {
                            Testremark = Convert.ToString(json.txtRemark).Trim();
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(objHistory.Observation))
                        {
                            if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                            {
                                RiskCategoryManagement.AddObservationHistory(objHistory);
                            }
                        }

                        #region

                        InternalReviewHistory RH = new InternalReviewHistory()
                        {
                            ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                            InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                            Dated = DateTime.Now,
                            Remarks = Testremark,
                            AuditScheduleOnID = getScheduleonDetails.ID,
                            FinancialYear = Convert.ToString(json.FinYear),
                            CustomerBranchId = Convert.ToInt32(json.BranchID),
                            ATBDId = Convert.ToInt32(json.ATBDID),
                            FixRemark = "NA",
                            VerticalID = Convert.ToInt32(json.VerticalID),
                            AuditID = Convert.ToInt32(json.AuditID),
                            //  UniqueFlag= UniqueFlag,
                        };
                        if (RiskCategoryManagement.CheckRecordExist(RH))
                        {
                            Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                        }
                        #endregion

                        if (Success1 == true && Success2 == true)
                        {
                            result = "success";
                        }
                        else
                        {
                            ErrorMessage = "Something went wrong.";
                            if (!string.IsNullOrEmpty(ErrorMessage))
                            {
                                string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                return new HttpResponseMessage()
                                {
                                    Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                };
                            }
                        }
                    }//getScheduleonDetails
                }//ddlFilterStatus  Null
                #endregion
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    result = ErrorMessage;
                }
                else
                {
                    result = "success";
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string jsonstring = JsonConvert.SerializeObject("Something went wrong");
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion

        #region AuditHead Save
        [Route("AuditStatus/AuditHeadObservationTabOne")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AuditHeadObservationTabOne(JObject data)
        {
            try
            {
                dynamic json = data;
                string result = null;
                string ErrorMessage = string.Empty;
                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (Convert.ToString(json.PersonResponsible) == "-1" || Convert.ToString(json.PersonResponsible) == "")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (Convert.ToString(json.Owner) == "-1" || Convert.ToString(json.Owner) == "")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                if (!string.IsNullOrEmpty(Convert.ToString(json.ATBDID)) || !string.IsNullOrEmpty(Convert.ToString(json.AuditID)))
                {
                    long roleid = 4;
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = json.ScheduleOnID,
                        ProcessId = json.ProcessID,
                        FinancialYear = json.FinYear,
                        ForPerid = json.Period,
                        CustomerBranchId = Convert.ToInt32(json.BranchID),
                        IsDeleted = false,
                        UserID = AuthenticationHelper.UserID,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = json.InstanceID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        StatusId = null,
                        CreatedByText = AuthenticationHelper.User,
                        StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                        AuditScheduleOnID = json.ScheduleOnID,
                        FinancialYear = json.FinYear,
                        CustomerBranchId = json.BranchID,
                        InternalAuditInstance = json.InstanceID,
                        ProcessId = json.ProcessID,
                        SubProcessId = -1,
                        ForPeriod = json.Period,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        UserID = AuthenticationHelper.UserID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                    {
                        MstRiskResult.AuditObjective = null;
                    }
                    else
                        MstRiskResult.AuditObjective = Regex.Replace(Convert.ToString(json.AuditObjective).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = Regex.Replace(Convert.ToString(json.AuditSteps).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = Regex.Replace(Convert.ToString(json.AnalysisToBePerofrmed).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");

                    MstRiskResult.ObservationNumber = "";

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        MstRiskResult.AnnexueTitle = Regex.Replace(Convert.ToString(json.AnnexueTitle).Trim(), @"\t|\n|\r", "");
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                    }

                    if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                        MstRiskResult.Observation = "";
                    else
                       MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                        MstRiskResult.BriefObservation = "";
                    else
                        MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                        MstRiskResult.ObjBackground = "";
                    else
                        MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.TimeLine);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.TimeLine = dt2.Date;
                        }
                        catch (Exception)
                        {
                            string TL = Convert.ToString(json.TimeLine);
                            dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.TimeLine = dt.Date;
                        }
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    DateTime responseDueDate = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.ResponseDueDate);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.ResponseDueDate = dt2;
                        }
                        catch (Exception)
                        {
                            string RD = Convert.ToString(json.ResponseDueDate);
                            responseDueDate = DateTime.ParseExact(RD, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.ResponseDueDate = responseDueDate.Date;
                        }
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (json.PersonResponsible == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (json.Owner == "-1")
                        {
                            MstRiskResult.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(json.Owner);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        if (json.ObservatioRating == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        if (json.ObservationCategory == "-1")
                        {
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                    {
                        if (json.ObservationSubCategory == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                        }
                    }
                    //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                    //{
                    //    if (json.GroupObservationCategoryID == "-1")
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = null;
                    //    }
                    //    else
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                    //    }
                    //}
                    bool Success = false;
                    InternalAuditTransaction TrasactionDetails = UserManagementRisk.GetTrasactionID(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                    if (TrasactionDetails == null)
                    {
                        RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                    }

                    MstRiskResult.AStatusId = 5;
                    bool Success1 = false;
                    //bool Success2 = false;
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    MstRiskResult.AuditeeResponse = AuditeeResponse;
                    if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                    {
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                        result = "success";
                    }
                    else
                    {
                        MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.CreatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                        result = "success";
                    }

                    if (!string.IsNullOrEmpty(result))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(json.AudioVideoLink)))
                        {
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                            if (observationAudioVideoList.Count > 0)
                            {
                                string CheckCommaValOne = Convert.ToString(json.AudioVideoLink);
                                string[] audioVideoLinks = CheckCommaValOne.Split(',');
                                var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                if (filtered.Count > 0)
                                {
                                    foreach (var item in filtered)
                                    {
                                        RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                    }
                                }
                            }

                            ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                            {
                                AuditId = Convert.ToInt32(json.AuditID),
                                ATBTID = Convert.ToInt32(json.ATBDID),
                                IsActive = true,
                            };

                            string CheckCommaVal = Convert.ToString(json.AudioVideoLink);
                            if (CheckCommaVal.Contains(',')) //checking entered single value or multiple values
                            {
                                string[] audioVideoLinks = (Convert.ToString(json.AudioVideoLink)).Split(',');
                                int lenght = audioVideoLinks.Length;
                                string link = string.Empty;
                                for (int i = 0; i < audioVideoLinks.Length; i++)
                                {
                                    link = audioVideoLinks[i].ToString();
                                    objObservationAudioVideo.AudioVideoLink = link.Trim();
                                    if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                    {
                                        objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                        RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                    }
                                }
                            }
                            else
                            {
                                objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                {
                                    objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                }
                                else
                                {
                                    objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                    objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                }
                            }
                        }
                        else
                        {
                            // when user want delete all existing link then we will get null from txtMultilineVideolink
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                {
                                    RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                }
                            }
                        }
                    }
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/AuditHeadSave")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AuditHeadSave(JObject dataValues)
        {
            try
            {
                dynamic json = dataValues;
                string ErrorMessage = string.Empty;
                string CurrentStatus = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(json.CurrentStatusID)))
                {
                    CurrentStatus = UserManagementRisk.GetstatusbyID(Convert.ToInt32(json.CurrentStatusID));
                }
                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (Convert.ToString(json.PersonResponsible) == "-1" || Convert.ToString(json.PersonResponsible) == "")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (Convert.ToString(json.Owner) == "-1" || Convert.ToString(json.Owner) == "")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                    if ((Convert.ToString(CurrentStatus)) == "Closed")
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                        {
                            ErrorMessage = "Please enter Timeline.";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (json.PersonResponsible == "-1")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (json.Owner == "-1")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                #region
                if (Convert.ToInt32(json.StatusID) != -1)
                {
                    bool SubmitValue = false;
                    bool Flag = false;
                    long roleid = 4;
                    int LogedInUserRole = UserManagementRisk.CheckAssignedRoleOfUser(Convert.ToInt32(json.AuditID), AuthenticationHelper.UserID);
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    int customerID = -1;
                    List<long> ProcessList = new List<long>();
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuthenticationHelper.UserID, 3, Convert.ToString(json.FinYear), Convert.ToString(json.Period), Convert.ToInt32(json.BranchID), Convert.ToInt32(json.ATBDID), Convert.ToInt32(json.VerticalID), Convert.ToInt32(json.AuditID));
                    if (getScheduleonDetails != null)
                    {
                        ProcessList.Add(getScheduleonDetails.ProcessId);
                        var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(Convert.ToInt32(json.AuditID), AuthenticationHelper.UserID, ProcessList);
                        List<int> RoleListInAudit = CustomerManagementRisk.GetAuditListRole(Convert.ToInt32(json.AuditID), ProcessList);
                        int rowcount = RoleListInAudit.Count();
                        if (!string.IsNullOrEmpty(CurrentStatus))
                        {
                            //string Currentstatus = UserManagementRisk.GetstatusbyID(Convert.ToInt32(json.StatusID));
                            if (rowcount == 3 && CurrentStatus == "Closed" && (Convert.ToString(json.StatusID) == "2" || Convert.ToString(json.StatusID) == "4" || Convert.ToString(json.StatusID) == "6"))
                            {
                                SubmitValue = false;
                            }
                            else if (rowcount == 2 && CurrentStatus == "Final Review")
                            {
                                SubmitValue = false;
                            }
                            else
                            {
                                SubmitValue = true;
                            }
                            if (SubmitValue)
                            {
                                #region
                                InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                {
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    ForPerid = Convert.ToString(json.Period),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    IsDeleted = false,
                                    UserID = AuthenticationHelper.UserID,
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    RoleID = roleid,
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    AStatusId = Convert.ToInt32(json.CurrentStatusID),
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    AnnexueTitle = Convert.ToString(json.AnnexueTitle)
                                };

                                InternalAuditTransaction transaction = new InternalAuditTransaction()
                                {
                                    StatusId = Convert.ToInt32(json.CurrentStatusID),
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    SubProcessId = -1,
                                    ForPeriod = Convert.ToString(json.Period),
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    RoleID = roleid,
                                    UserID = AuthenticationHelper.UserID,
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                };
                                //AuditeeStepOneGoStatu objAuditee = new AuditeeStepOneGoStatu()
                                //{
                                //    AuditID = Convert.ToInt32(json.AuditID),
                                //    ATBDID = Convert.ToInt32(json.ATBDID),
                                //    Status = "Assigned", //Assigned && Pendig For Review are two status used
                                //    CreatedOn = DateTime.Now,
                                //    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                //    IsSentAuditee = false
                                //};
                                AuditClosure AuditClosureResult = new AuditClosure()
                                {
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    ForPeriod = Convert.ToString(json.Period),
                                    CustomerbranchId = Convert.ToInt32(json.BranchID),
                                    ACStatus = 1,
                                    AuditCommiteRemark = "",
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditCommiteFlag = 0,
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    AnnexueTitle = Convert.ToString(json.AnnexueTitle)
                                };
                                ObservationHistory objHistory = new ObservationHistory()
                                {
                                    UserID = AuthenticationHelper.UserID,
                                    RoleID = roleid,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    ATBTID = Convert.ToInt32(json.ATBDID)
                                };

                                if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                                {
                                    MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                    objHistory.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                    AuditClosureResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                }
                                objHistory.ObservationOld = json.hidObservation;
                                objHistory.ProcessWalkthroughOld = json.hidObservation;
                                objHistory.ActualWorkDoneOld = json.hidActualWorkDone;
                                objHistory.PopulationOld = json.hidpopulation;
                                objHistory.SampleOld = json.hidSample;
                                objHistory.ObservationTitleOld = json.hidObservationTitle;
                                objHistory.RiskOld = json.hidRisk;
                                objHistory.RootCauseOld = json.hidRootcost;
                                objHistory.FinancialImpactOld = json.hidfinancialImpact;
                                objHistory.RecommendationOld = json.hidRecommendation;
                                objHistory.ManagementResponseOld = json.hidMgtResponse;
                                objHistory.RemarksOld = json.hidRemarks;
                                objHistory.ScoreOld = json.hidAuditStepScore;
                                //objHistory.AnnexueTitleOld = json.hidAnnaxureTitle;
                                //objHistory.AnnexueTitle = json.AnnexueTitle;
                                DateTime dt5 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidResponseDueDate)))
                                {
                                    try
                                    {
                                        DateTime dt15 = Convert.ToDateTime(json.hidResponseDueDate);
                                        DateTime dt25 = GetDate(dt15.ToString("dd/MM/yyyy"));
                                        objHistory.ResponseDueDateOld = dt25.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.hidResponseDueDate);
                                        dt5 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.TimeLine = dt5.Date;
                                    }
                                }
                                DateTime dt = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidTimeLine)))
                                {
                                    try
                                    {
                                        DateTime dt1 = Convert.ToDateTime(json.hidTimeLine);
                                        DateTime dt21 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                        objHistory.TimeLineOld = dt21.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.hidTimeLine);
                                        dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        objHistory.TimeLineOld = dt.Date;
                                    }
                                }
                                DateTime dt2 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                                {
                                    try
                                    {
                                        DateTime dt11 = Convert.ToDateTime(json.TimeLine);
                                        DateTime dt21 = GetDate(dt11.ToString("dd/MM/yyyy"));
                                        MstRiskResult.TimeLine = dt21.Date;
                                        objHistory.TimeLine = dt21.Date;
                                        AuditClosureResult.TimeLine = dt21.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.TimeLine);
                                        dt2 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.TimeLine = dt2.Date;
                                        objHistory.TimeLine = dt2.Date;
                                        AuditClosureResult.TimeLine = dt2.Date;
                                    }
                                }
                                else
                                {
                                    MstRiskResult.TimeLine = null;
                                    objHistory.TimeLine = null;
                                    AuditClosureResult.TimeLine = null;
                                }

                                DateTime dt3 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                                {
                                    try
                                    {
                                        DateTime dt13 = Convert.ToDateTime(json.ResponseDueDate);
                                        DateTime dt23 = GetDate(dt13.ToString("dd/MM/yyyy"));
                                        MstRiskResult.ResponseDueDate = dt23.Date;
                                        transaction.ResponseDueDate = dt23.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.ResponseDueDate);
                                        dt3 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.ResponseDueDate = dt3.Date;
                                        transaction.ResponseDueDate = dt3.Date;
                                    }
                                }
                                else
                                {
                                    MstRiskResult.ResponseDueDate = null;
                                    transaction.ResponseDueDate = null;
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                                {
                                    MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                    transaction.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                }
                                else
                                {
                                    MstRiskResult.BriefObservation = null;
                                    transaction.BriefObservation = null;
                                    AuditClosureResult.BriefObservation = null;
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                                {
                                    MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                    transaction.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                }
                                else
                                {
                                    MstRiskResult.ObjBackground = null;
                                    transaction.ObjBackground = null;
                                    AuditClosureResult.ObjBackground = null;
                                }
                                //if (!string.IsNullOrEmpty(DefeciencyType))
                                //{
                                //    MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                                //    transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                                //}
                                //else
                                //{
                                //    MstRiskResult.DefeciencyType = null;
                                //    transaction.DefeciencyType = null;
                                //}
                                objHistory.OldBodyContent = Convert.ToString(json.hidBodyContent);
                                objHistory.OldUHComment = Convert.ToString(json.hidUHComment);
                                objHistory.OldPRESIDENTComment = Convert.ToString(json.hidPRESIDENTComment);

                                if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                                {
                                    MstRiskResult.BodyContent = "";
                                    objHistory.BodyContent = "";
                                    transaction.BodyContent = "";
                                    AuditClosureResult.BodyContent = "";
                                }
                                else
                                {
                                    MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    objHistory.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    transaction.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    AuditClosureResult.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidPersonResponsible)))
                                {
                                    objHistory.PersonResponsibleOld = Convert.ToInt32(json.hidPersonResponsible);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationRating)))
                                {
                                    objHistory.ObservationRatingOld = Convert.ToInt32(json.hidObservationRating);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationCategory)))
                                {
                                    objHistory.ObservationCategoryOld = Convert.ToInt32(json.hidObservationCategory);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationSubCategory)))
                                {
                                    objHistory.ObservationSubCategoryOld = Convert.ToInt32(json.hidObservationSubCategory);
                                }
                                //if (!string.IsNullOrEmpty(Convert.ToString(json.hidGroupObservationCategory)))
                                //{
                                //    objHistory.GroupObservationCategoryID = Convert.ToInt32(json.hidGroupObservationCategory);
                                //}
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidOwner)))
                                {
                                    objHistory.OwnerOld = Convert.ToInt32(json.hidOwner);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidUserID)))
                                {
                                    objHistory.UserOld = Convert.ToInt32(json.hidUserID);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                                {
                                    objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                                {
                                    objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                                }

                                objHistory.AuditId = Convert.ToInt32(json.AuditID);


                                if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                                    MstRiskResult.AuditObjective = null;
                                else
                                    MstRiskResult.AuditObjective = Regex.Replace(Convert.ToString(json.AuditObjective).Trim(), @"\t|\n|\r", "");

                                if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                                    MstRiskResult.AuditSteps = "";
                                else
                                    MstRiskResult.AuditSteps = Regex.Replace(Convert.ToString(json.AuditSteps).Trim(), @"\t|\n|\r", "");

                                if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                                    MstRiskResult.AnalysisToBePerofrmed = "";
                                else
                                    MstRiskResult.AnalysisToBePerofrmed = Regex.Replace(Convert.ToString(json.AnalysisToBePerofrmed).Trim(), @"\t|\n|\r", "");

                                if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                                {
                                    MstRiskResult.ProcessWalkthrough = "";
                                    objHistory.ProcessWalkthrough = "";
                                }
                                else
                                {
                                    MstRiskResult.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");
                                    objHistory.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                                {
                                    MstRiskResult.ActivityToBeDone = "";
                                    objHistory.ActualWorkDone = "";
                                }
                                else
                                {
                                    MstRiskResult.ActivityToBeDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");
                                    objHistory.ActualWorkDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                                {
                                    MstRiskResult.Population = "";
                                    objHistory.Population = "";
                                }
                                else
                                {
                                    MstRiskResult.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");
                                    objHistory.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                                {
                                    MstRiskResult.Sample = "";
                                    objHistory.Sample = "";
                                }
                                else
                                {
                                    MstRiskResult.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");
                                    objHistory.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                                {
                                    MstRiskResult.ObservationTitle = "";
                                    objHistory.ObservationTitle = "";
                                    AuditClosureResult.ObservationTitle = "";
                                }
                                else
                                {
                                    MstRiskResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                                    objHistory.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                                {
                                    MstRiskResult.AnnexueTitle = "";
                                }
                                else
                                {
                                    MstRiskResult.AnnexueTitle = Regex.Replace(Convert.ToString(json.AnnexueTitle).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                                {
                                    MstRiskResult.Observation = "";
                                    objHistory.Observation = "";
                                    AuditClosureResult.Observation = "";
                                }
                                else
                                {
                                    MstRiskResult.Observation = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                    objHistory.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                                {
                                    MstRiskResult.Risk = "";
                                    objHistory.Risk = "";
                                    AuditClosureResult.Risk = "";
                                }
                                else
                                {
                                    MstRiskResult.Risk = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                    objHistory.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                                {
                                    MstRiskResult.RootCost = null;
                                    objHistory.RootCause = null;
                                    AuditClosureResult.RootCost = null;
                                }
                                else
                                {
                                    MstRiskResult.RootCost = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                    objHistory.RootCause = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                                {
                                    MstRiskResult.FinancialImpact = null;
                                    objHistory.FinancialImpact = null;
                                    AuditClosureResult.FinancialImpact = null;
                                }
                                else
                                {
                                    MstRiskResult.FinancialImpact = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.FinancialImpact).Trim(), @"\t|\n|\r", "");
                                    objHistory.FinancialImpact = Regex.Replace(Convert.ToString(json.FinancialImpact).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.FinancialImpact = Regex.Replace(Convert.ToString(json.FinancialImpact).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                                {
                                    MstRiskResult.Recomendation = "";
                                    objHistory.Recommendation = "";
                                    AuditClosureResult.Recomendation = "";
                                }
                                else
                                {
                                    MstRiskResult.Recomendation = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                    objHistory.Recommendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                                {
                                    MstRiskResult.ManagementResponse = "";
                                    objHistory.ManagementResponse = "";
                                    AuditClosureResult.ManagementResponse = "";
                                }
                                else
                                {
                                    MstRiskResult.ManagementResponse = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                    objHistory.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                                    MstRiskResult.FixRemark = "";
                                else
                                    MstRiskResult.FixRemark = MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");

                                if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                                {
                                    if (Convert.ToString(json.PersonResponsible) == "-1")
                                    {
                                        MstRiskResult.PersonResponsible = null;
                                        transaction.PersonResponsible = null;
                                        objHistory.PersonResponsible = null;
                                        AuditClosureResult.PersonResponsible = null;
                                    }
                                    else
                                    {
                                        MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        transaction.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        objHistory.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        AuditClosureResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                                {
                                    if (Convert.ToString(json.Owner) == "-1")
                                    {
                                        MstRiskResult.Owner = null;
                                        transaction.Owner = null;
                                        objHistory.Owner = null;
                                        AuditClosureResult.Owner = null;
                                    }
                                    else
                                    {
                                        MstRiskResult.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        transaction.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        objHistory.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        AuditClosureResult.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                                {
                                    if (Convert.ToString(json.ObservatioRating) == "-1")
                                    {
                                        transaction.ObservatioRating = null;
                                        MstRiskResult.ObservationRating = null;
                                        objHistory.ObservationRating = null;
                                        AuditClosureResult.ObservationRating = null;
                                    }
                                    else
                                    {
                                        transaction.ObservatioRating = Convert.ToInt32(json.ObservatioRating);
                                        MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                        objHistory.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                        AuditClosureResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                                {
                                    if (Convert.ToString(json.ObservationCategory) == "-1")
                                    {
                                        transaction.ObservationCategory = null;
                                        MstRiskResult.ObservationCategory = null;
                                        objHistory.ObservationCategory = null;
                                        AuditClosureResult.ObservationCategory = null;
                                    }
                                    else
                                    {
                                        transaction.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        objHistory.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        AuditClosureResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                    }
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                                {
                                    if (Convert.ToString(json.ObservationSubCategory) == "-1")
                                    {
                                        transaction.ObservationSubCategory = null;
                                        MstRiskResult.ObservationSubCategory = null;
                                        objHistory.ObservationSubCategory = null;
                                        AuditClosureResult.ObservationSubCategory = null;
                                    }
                                    else
                                    {
                                        transaction.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        objHistory.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        AuditClosureResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                    }
                                }
                                //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                                //{
                                //    if (Convert.ToString(json.GroupObservationCategoryID) == "-1")
                                //    {
                                //        transaction.GroupObservationCategoryID = null;
                                //        MstRiskResult.GroupObservationCategoryID = null;
                                //        objHistory.GroupObservationCategoryID = null;
                                //        AuditClosureResult.GroupObservationCategoryID = null;
                                //    }
                                //    else
                                //    {
                                //        transaction.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        objHistory.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        AuditClosureResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //    }
                                //}

                                string remark = string.Empty;
                                if (CurrentStatus == "Closed")
                                {
                                    transaction.StatusId = 3;
                                    MstRiskResult.AStatusId = 3;
                                    remark = "Audit Steps Closed.";
                                    MstRiskResult.AuditeeResponse = "HC";
                                    MstRiskResult.AuditeeResponse = "C";
                                    transaction.Remarks = remark.Trim();
                                }
                                else if (CurrentStatus == "Team Review")
                                {
                                    transaction.StatusId = 4;
                                    MstRiskResult.AStatusId = 4;
                                    remark = "Audit Steps Under Team Review.";
                                    MstRiskResult.AuditeeResponse = "HT";
                                    transaction.Remarks = remark.Trim();
                                }
                                else if (CurrentStatus == "Final Review")
                                {
                                    transaction.StatusId = 5;
                                    MstRiskResult.AStatusId = 5;
                                    remark = "Audit Steps Under Final Review.";
                                    MstRiskResult.AuditeeResponse = "HF";
                                    transaction.Remarks = remark.Trim();
                                }
                                else if (CurrentStatus == "Auditee Review")
                                {
                                    transaction.StatusId = 6;
                                    MstRiskResult.AStatusId = 6;
                                    remark = "Audit Steps Under Auditee Review.";
                                    MstRiskResult.AuditeeResponse = "RA";
                                    transaction.Remarks = remark.Trim();
                                }
                                objHistory.Remarks = remark;

                                bool Success1 = false;
                                bool Success2 = false;
                                if (RiskCategoryManagement.InternalControlResultExistsCheckLastestNullWithStatus(MstRiskResult))
                                {
                                    if (Convert.ToInt32(json.StatusID) == 2 && (CurrentStatus == "Team Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 2 && (CurrentStatus == "Auditee Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 2 && (CurrentStatus == "Final Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 2 && (CurrentStatus == "Closed"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }//Team Review                        
                                    else if (Convert.ToInt32(json.StatusID) == 4 && (CurrentStatus == "Auditee Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 4 && (CurrentStatus == "Final Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 4 && (CurrentStatus == "Closed"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }//Auditee Review
                                    else if (Convert.ToInt32(json.StatusID) == 6 && (CurrentStatus == "Final Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 6 && (CurrentStatus == "Auditee Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 6 && (CurrentStatus == "Closed"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }//Final Review
                                    else if (Convert.ToInt32(json.StatusID) == 5 && (CurrentStatus == "Final Review"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else if (Convert.ToInt32(json.StatusID) == 5 && (CurrentStatus == "Closed"))
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                    else
                                    {
                                        if (RiskCategoryManagement.InternalControlResultExistsWithStatus(MstRiskResult))
                                        {
                                            MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                        }
                                        else
                                        {
                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                        }
                                    }
                                }
                                else
                                {
                                    MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    MstRiskResult.CreatedOn = DateTime.Now;
                                    Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                }
                                if (Convert.ToString(CurrentStatus) == "Closed")
                                {
                                    if (RiskCategoryManagement.InternalControlResultClosedCount(MstRiskResult) == 1)
                                    {
                                        #region  AuditClosure 
                                        using (AuditControlEntities entities = new AuditControlEntities())
                                        {
                                            var CheckClosure = (from row in entities.AuditClosures
                                                                where row.AuditID == MstRiskResult.AuditID
                                                                && row.ATBDId == MstRiskResult.ATBDId
                                                                select row).FirstOrDefault();
                                            if (CheckClosure == null)
                                            {
                                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                                RiskCategoryManagement.AuditClosureDetailsExists(customerID,
                                                Convert.ToInt32(MstRiskResult.CustomerBranchId), (long)MstRiskResult.VerticalID,
                                                MstRiskResult.ForPerid, MstRiskResult.FinancialYear, Convert.ToInt32(json.AuditID));

                                                var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                                      where row.ProcessId == MstRiskResult.ProcessId
                                                                      && row.FinancialYear == MstRiskResult.FinancialYear
                                                                      && row.ForPerid == MstRiskResult.ForPerid
                                                                      && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                                      && row.UserID == MstRiskResult.UserID
                                                                      && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                                      && row.VerticalID == MstRiskResult.VerticalID
                                                                      && row.AuditID == MstRiskResult.AuditID
                                                                      select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                                if (RecordtoUpdate != null)
                                                {
                                                    AuditClosureResult.ResultID = RecordtoUpdate;
                                                    AuditClosureResult.CreatedBy = AuthenticationHelper.UserID;
                                                    AuditClosureResult.CreatedOn = DateTime.Now;
                                                    RiskCategoryManagement.CreateAuditClosureResult(AuditClosureResult);
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                if (!string.IsNullOrEmpty(objHistory.Observation))
                                {
                                    if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                    {
                                        objHistory.CreatedBy = AuthenticationHelper.UserID;
                                        objHistory.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.AddObservationHistory(objHistory);
                                    }
                                }

                                if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                {
                                    if (Convert.ToInt32(json.StatusID) == 4 || Convert.ToInt32(json.StatusID) == 5 || Convert.ToInt32(json.StatusID) == 6)
                                    {
                                        transaction.CreatedBy = AuthenticationHelper.UserID;
                                        transaction.CreatedOn = DateTime.Now;
                                        Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                    }
                                    else
                                    {
                                        transaction.UpdatedBy = AuthenticationHelper.UserID;
                                        transaction.UpdatedOn = DateTime.Now;
                                        Success2 = RiskCategoryManagement.UpdateInternalAuditTxnStatusReviewer(transaction);
                                    }
                                }
                                else
                                {
                                    transaction.CreatedBy = AuthenticationHelper.UserID;
                                    transaction.CreatedOn = DateTime.Now;
                                    Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                }
                                string Testremark = "";
                                if (string.IsNullOrEmpty(Convert.ToString(json.txtRemark)))
                                {
                                    Testremark = "NA";
                                }
                                else
                                {
                                    Testremark = Convert.ToString(json.txtRemark).Trim();
                                }
                                #endregion

                                if (!string.IsNullOrEmpty(objHistory.Observation))
                                {
                                    if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                    {
                                        RiskCategoryManagement.AddObservationHistory(objHistory);
                                    }
                                }

                                #region

                                InternalReviewHistory RH = new InternalReviewHistory()
                                {
                                    ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    Dated = DateTime.Now,
                                    Remarks = Testremark,
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    FixRemark = "NA",
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    //  UniqueFlag= UniqueFlag,
                                };
                                if (RiskCategoryManagement.CheckRecordExist(RH))
                                {
                                    Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                                }

                                #endregion

                                if (Success1 == true && Success2 == true && Flag == true)
                                {
                                    //if (Convert.ToString(CurrentStatus) == "Auditee Review")
                                    //{
                                    //    UserManagementRisk.AddAuditeeStatusRemark(objAuditee);
                                    //}

                                    #region Audit Close 
                                    if (Convert.ToString(CurrentStatus) == "Closed")
                                    {
                                        if (RiskCategoryManagement.CheckAllStepClosed(Convert.ToInt32(json.AuditID)))
                                        {
                                            AuditClosureClose auditclosureclose = new AuditClosureClose();
                                            auditclosureclose.CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                                            auditclosureclose.CustomerBranchId = Convert.ToInt32(json.BranchID);
                                            auditclosureclose.FinancialYear = Convert.ToString(json.FinYear);
                                            auditclosureclose.ForPeriod = Convert.ToString(json.Period);
                                            auditclosureclose.CreatedBy = AuthenticationHelper.UserID;
                                            auditclosureclose.CreatedDate = DateTime.Today.Date;
                                            auditclosureclose.ACCStatus = 1;
                                            auditclosureclose.VerticalID = Convert.ToInt32(json.VerticalID);
                                            auditclosureclose.AuditId = Convert.ToInt32(json.AuditID);
                                            if (!RiskCategoryManagement.AuditClosureCloseExists(auditclosureclose.CustomerBranchId, (int)auditclosureclose.VerticalID, auditclosureclose.FinancialYear, auditclosureclose.ForPeriod, Convert.ToInt32(json.AuditID)))
                                            {
                                                RiskCategoryManagement.CreateAuditClosureClosed(auditclosureclose);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    ErrorMessage = "Something went wrong.";
                                    if (!string.IsNullOrEmpty(ErrorMessage))
                                    {
                                        string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                        return new HttpResponseMessage()
                                        {
                                            Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                        };
                                    }
                                }

                                #region Audio Vedeo save
                                if (Success1)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(json.AudioVideoLink)))
                                    {
                                        List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                                        if (observationAudioVideoList.Count > 0)
                                        {
                                            string CheckCommaValOne = Convert.ToString(json.AudioVideoLink);
                                            string[] audioVideoLinks = CheckCommaValOne.Split(',');
                                            var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                            if (filtered.Count > 0)
                                            {
                                                foreach (var item in filtered)
                                                {
                                                    RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                                }
                                            }
                                        }

                                        ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                                        {
                                            AuditId = Convert.ToInt32(json.AuditID),
                                            ATBTID = Convert.ToInt32(json.ATBDID),
                                            IsActive = true,
                                        };

                                        string CheckCommaVal = Convert.ToString(json.AudioVideoLink);
                                        if (CheckCommaVal.Contains(',')) //checking entered single value or multiple values
                                        {
                                            string[] audioVideoLinks = (Convert.ToString(json.AudioVideoLink)).Split(',');
                                            int lenght = audioVideoLinks.Length;
                                            string link = string.Empty;
                                            for (int i = 0; i < audioVideoLinks.Length; i++)
                                            {
                                                link = audioVideoLinks[i].ToString();
                                                objObservationAudioVideo.AudioVideoLink = link.Trim();
                                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                                {
                                                    objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                                }
                                                else
                                                {
                                                    objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                            if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                            {
                                                objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                                objObservationAudioVideo.CreatedOn = DateTime.Now;
                                                RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                            }
                                            else
                                            {
                                                objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                                objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                                objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                                RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // when user want delete all existing link then we will get null from txtMultilineVideolink
                                        List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                                        if (observationAudioVideoList.Count > 0)
                                        {
                                            foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                            {
                                                RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessage = "Server Error Occured. Please try again.";
                                }
                                #endregion
                            }
                            else
                            {
                                ErrorMessage = "You don't have access to change status into " + Convert.ToString(CurrentStatus) + ".";
                                if (!string.IsNullOrEmpty(ErrorMessage))
                                {
                                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                    return new HttpResponseMessage()
                                    {
                                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                    };
                                }
                            }
                        }
                        else
                        {
                            ErrorMessage = "Please Select Status";
                            if (!string.IsNullOrEmpty(ErrorMessage))
                            {
                                string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                return new HttpResponseMessage()
                                {
                                    Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                };
                            }
                        }
                    }//getScheduleonDetails
                }//ddlFilterStatus  Null
                #endregion
                string result = string.Empty;
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    result = ErrorMessage;
                }
                else
                {
                    result = "success";
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string jsonstring = JsonConvert.SerializeObject("Something went wrong");
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion

        #region AuditHead Observation Listing Save
        [Route("AuditStatus/AuditHeadObservationListingTabOne")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AuditHeadObservationListingTabOne(JObject data)
        {
            try
            {
                dynamic json = data;
                string result = null;
                string ErrorMessage = string.Empty;
                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (Convert.ToString(json.PersonResponsible) == "-1" || Convert.ToString(json.PersonResponsible) == "")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (Convert.ToString(json.Owner) == "-1" || Convert.ToString(json.Owner) == "")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                if (!string.IsNullOrEmpty(Convert.ToString(json.ATBDID)) || !string.IsNullOrEmpty(Convert.ToString(json.AuditID)))
                {
                    long roleid = 4;
                    InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                    {
                        AuditScheduleOnID = json.ScheduleOnID,
                        ProcessId = json.ProcessID,
                        FinancialYear = json.FinYear,
                        ForPerid = json.Period,
                        CustomerBranchId = Convert.ToInt32(json.BranchID),
                        IsDeleted = false,
                        UserID = AuthenticationHelper.UserID,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        InternalAuditInstance = json.InstanceID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    InternalAuditTransaction transaction = new InternalAuditTransaction()
                    {
                        StatusId = null,
                        CreatedByText = AuthenticationHelper.User,
                        StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                        AuditScheduleOnID = json.ScheduleOnID,
                        FinancialYear = json.FinYear,
                        CustomerBranchId = json.BranchID,
                        InternalAuditInstance = json.InstanceID,
                        ProcessId = json.ProcessID,
                        SubProcessId = -1,
                        ForPeriod = json.Period,
                        ATBDId = json.ATBDID,
                        RoleID = roleid,
                        UserID = AuthenticationHelper.UserID,
                        VerticalID = json.VerticalID,
                        AuditID = json.AuditID,
                    };
                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                    {
                        MstRiskResult.AuditObjective = null;
                    }
                    else
                        MstRiskResult.AuditObjective = Convert.ToString(json.AuditObjective).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                        MstRiskResult.AuditSteps = "";
                    else
                        MstRiskResult.AuditSteps = Regex.Replace(Convert.ToString(json.AuditSteps).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                        MstRiskResult.AnalysisToBePerofrmed = "";
                    else
                        MstRiskResult.AnalysisToBePerofrmed = Regex.Replace(Convert.ToString(json.AnalysisToBePerofrmed).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                        MstRiskResult.ProcessWalkthrough = "";
                    else
                        MstRiskResult.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                        MstRiskResult.ActivityToBeDone = "";
                    else
                        MstRiskResult.ActivityToBeDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                        MstRiskResult.Population = "";
                    else
                        MstRiskResult.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                        MstRiskResult.Sample = "";
                    else
                        MstRiskResult.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");

                    MstRiskResult.ObservationNumber = "";

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                        MstRiskResult.ObservationTitle = "";
                    else
                        MstRiskResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                    {
                        MstRiskResult.AnnexueTitle = "";
                    }
                    else
                    {
                        MstRiskResult.AnnexueTitle = Regex.Replace(Convert.ToString(json.AnnexueTitle).Trim(), @"\t|\n|\r", "");
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                    {
                        MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                    }

                    if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                        MstRiskResult.Observation = "";
                    else
                        MstRiskResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                        MstRiskResult.BriefObservation = "";
                    else
                        MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                        MstRiskResult.ObjBackground = "";
                    else
                        MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                        MstRiskResult.BodyContent = "";
                    else
                        MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                        MstRiskResult.Risk = "";
                    else
                        MstRiskResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                        MstRiskResult.RootCost = null;
                    else
                        MstRiskResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                        MstRiskResult.FinancialImpact = null;
                    else
                        MstRiskResult.FinancialImpact = Convert.ToString(json.FinancialImpact).Trim();

                    if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                        MstRiskResult.Recomendation = "";
                    else
                        MstRiskResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                        MstRiskResult.ManagementResponse = "";
                    else
                        MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");

                    if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                        MstRiskResult.FixRemark = "";
                    else
                        MstRiskResult.FixRemark = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");

                    DateTime dt = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.TimeLine);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.TimeLine = dt2.Date;
                        }
                        catch (Exception)
                        {
                            string TL = Convert.ToString(json.TimeLine);
                            dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.TimeLine = dt.Date;
                        }
                    }
                    else
                        MstRiskResult.TimeLine = null;

                    DateTime responseDueDate = new DateTime();
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                    {
                        try
                        {
                            DateTime dt1 = Convert.ToDateTime(json.ResponseDueDate);
                            DateTime dt2 = GetDate(dt1.ToString("dd/MM/yyyy"));
                            MstRiskResult.ResponseDueDate = dt2;
                        }
                        catch (Exception)
                        {
                            string RD = Convert.ToString(json.ResponseDueDate);
                            responseDueDate = DateTime.ParseExact(RD, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            MstRiskResult.ResponseDueDate = responseDueDate.Date;
                        }
                    }
                    else
                    {
                        MstRiskResult.ResponseDueDate = null;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (json.PersonResponsible == "-1")
                        {
                            MstRiskResult.PersonResponsible = null;
                        }
                        else
                        {
                            MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (json.Owner == "-1")
                        {
                            MstRiskResult.Owner = null;
                        }
                        else
                        {
                            MstRiskResult.Owner = Convert.ToInt32(json.Owner);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        if (json.ObservatioRating == "-1")
                        {
                            MstRiskResult.ObservationRating = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        if (json.ObservationCategory == "-1")
                        {
                            MstRiskResult.ObservationCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                    {
                        if (json.ObservationSubCategory == "-1")
                        {
                            MstRiskResult.ObservationSubCategory = null;
                        }
                        else
                        {
                            MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                        }
                    }
                    //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                    //{
                    //    if (json.GroupObservationCategoryID == "-1")
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = null;
                    //    }
                    //    else
                    //    {
                    //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                    //    }
                    //}
                    bool Success = false;
                    InternalAuditTransaction TrasactionDetails = UserManagementRisk.GetTrasactionID(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                    if (TrasactionDetails == null)
                    {
                        RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                    }

                    MstRiskResult.AStatusId = 5;
                    bool Success1 = false;
                    //bool Success2 = false;
                    var AuditeeResponse = RiskCategoryManagement.GetLatestStatusOfAuditteeResponse(MstRiskResult);
                    MstRiskResult.AuditeeResponse = AuditeeResponse;
                    if (RiskCategoryManagement.InternalControlResultExists_ObservationList(MstRiskResult))
                    {
                        MstRiskResult.AStatusId = 3;
                        MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        MstRiskResult.UpdatedOn = DateTime.Now;
                        Success1 = RiskCategoryManagement.UpdateInternalControlResult_ObaservationList(MstRiskResult);
                        result = "success";
                    }

                    if (!string.IsNullOrEmpty(result))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(json.AudioVideoLink)))
                        {
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                            if (observationAudioVideoList.Count > 0)
                            {
                                string CheckCommaValOne = Convert.ToString(json.AudioVideoLink);
                                string[] audioVideoLinks = CheckCommaValOne.Split(',');
                                var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                if (filtered.Count > 0)
                                {
                                    foreach (var item in filtered)
                                    {
                                        RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                    }
                                }
                            }

                            ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                            {
                                AuditId = Convert.ToInt32(json.AuditID),
                                ATBTID = Convert.ToInt32(json.ATBDID),
                                IsActive = true,
                            };

                            string CheckCommaVal = Convert.ToString(json.AudioVideoLink);
                            if (CheckCommaVal.Contains(',')) //checking entered single value or multiple values
                            {
                                string[] audioVideoLinks = (Convert.ToString(json.AudioVideoLink)).Split(',');
                                int lenght = audioVideoLinks.Length;
                                string link = string.Empty;
                                for (int i = 0; i < audioVideoLinks.Length; i++)
                                {
                                    link = audioVideoLinks[i].ToString();
                                    objObservationAudioVideo.AudioVideoLink = link.Trim();
                                    if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                    {
                                        objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                    }
                                    else
                                    {
                                        objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                        objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                        RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                    }
                                }
                            }
                            else
                            {
                                objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                {
                                    objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                }
                                else
                                {
                                    objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                    objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                }
                            }
                        }
                        else
                        {
                            // when user want delete all existing link then we will get null from txtMultilineVideolink
                            List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                            if (observationAudioVideoList.Count > 0)
                            {
                                foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                {
                                    RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                }
                            }
                        }
                    }
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = "" });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/AuditHeadObservationListingSave")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AuditHeadObservationListingSave(JObject dataValues)
        {
            try
            {
                dynamic json = dataValues;
                string ErrorMessage = string.Empty;
                string CurrentStatus = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(json.CurrentStatusID)))
                {
                    CurrentStatus = UserManagementRisk.GetstatusbyID(Convert.ToInt32(json.CurrentStatusID));
                }
                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (Convert.ToString(json.PersonResponsible) == "-1" || Convert.ToString(json.PersonResponsible) == "")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (Convert.ToString(json.Owner) == "-1" || Convert.ToString(json.Owner) == "")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                    if ((Convert.ToString(CurrentStatus)) == "Closed")
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                        {
                            ErrorMessage = "Please enter Timeline.";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                #region
                if (Convert.ToString(json.Population).Length > 200)
                {
                    ErrorMessage = "Population should not greater than 200 characters.";
                }
                if (!string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                {
                    if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                    {
                        ErrorMessage = "Please Enter Observation Title.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                    {
                        ErrorMessage = "Please Enter Business Implication.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                    {
                        ErrorMessage = "Please Enter Root Cause.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                    {
                        ErrorMessage = "Please Enter Financial Impact.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                    {
                        ErrorMessage = "Please Enter Recommendation.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                    {
                        if (json.PersonResponsible == "-1")
                        {
                            ErrorMessage = "Please Select Person Responsible.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                    {
                        if (json.Owner == "-1")
                        {
                            ErrorMessage = "Please Select Owner.";
                        }
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                    {
                        ErrorMessage = "Please Select Observation Rating.";
                    }
                    else if (string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                    {
                        ErrorMessage = "Please Select Observation Category.";
                    }
                }
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                    };
                }
                #endregion

                #region
                if (Convert.ToInt32(json.StatusID) != -1)
                {
                    bool SubmitValue = false;
                    bool Flag = false;
                    long roleid = 4;
                    int LogedInUserRole = UserManagementRisk.CheckAssignedRoleOfUser(Convert.ToInt32(json.AuditID), AuthenticationHelper.UserID);
                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    int customerID = -1;
                    List<long> ProcessList = new List<long>();
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var getScheduleonDetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(AuthenticationHelper.UserID, 3, Convert.ToString(json.FinYear), Convert.ToString(json.Period), Convert.ToInt32(json.BranchID), Convert.ToInt32(json.ATBDID), Convert.ToInt32(json.VerticalID), Convert.ToInt32(json.AuditID));
                    if (getScheduleonDetails != null)
                    {
                        ProcessList.Add(getScheduleonDetails.ProcessId);
                        var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(Convert.ToInt32(json.AuditID), AuthenticationHelper.UserID, ProcessList);
                        List<int> RoleListInAudit = CustomerManagementRisk.GetAuditListRole(Convert.ToInt32(json.AuditID), ProcessList);
                        int rowcount = RoleListInAudit.Count();
                        if (!string.IsNullOrEmpty(CurrentStatus))
                        {
                            //string Currentstatus = UserManagementRisk.GetstatusbyID(Convert.ToInt32(json.StatusID));
                            if (rowcount == 3 && CurrentStatus == "Closed" && (Convert.ToString(json.StatusID) == "2" || Convert.ToString(json.StatusID) == "4" || Convert.ToString(json.StatusID) == "6"))
                            {
                                SubmitValue = false;
                            }
                            else if (rowcount == 2 && CurrentStatus == "Final Review")
                            {
                                SubmitValue = false;
                            }
                            else
                            {
                                SubmitValue = true;
                            }
                            if (SubmitValue)
                            {
                                #region
                                InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                {
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    ForPerid = Convert.ToString(json.Period),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    IsDeleted = false,
                                    UserID = AuthenticationHelper.UserID,
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    RoleID = roleid,
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    AStatusId = Convert.ToInt32(json.CurrentStatusID),
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    AnnexueTitle = Convert.ToString(json.AnnexueTitle)
                                };

                                InternalAuditTransaction transaction = new InternalAuditTransaction()
                                {
                                    StatusId = Convert.ToInt32(json.CurrentStatusID),
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusChangedOn = GetDate(b.ToString("dd-MM-yyyy")),
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    SubProcessId = -1,
                                    ForPeriod = Convert.ToString(json.Period),
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    RoleID = roleid,
                                    UserID = AuthenticationHelper.UserID,
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                };
                                //AuditeeStepOneGoStatu objAuditee = new AuditeeStepOneGoStatu()
                                //{
                                //    AuditID = Convert.ToInt32(json.AuditID),
                                //    ATBDID = Convert.ToInt32(json.ATBDID),
                                //    Status = "Assigned", //Assigned && Pendig For Review are two status used
                                //    CreatedOn = DateTime.Now,
                                //    CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                //    IsSentAuditee = false
                                //};
                                AuditClosure AuditClosureResult = new AuditClosure()
                                {
                                    ProcessId = getScheduleonDetails.ProcessId,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    ForPeriod = Convert.ToString(json.Period),
                                    CustomerbranchId = Convert.ToInt32(json.BranchID),
                                    ACStatus = 1,
                                    AuditCommiteRemark = "",
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditCommiteFlag = 0,
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    AnnexueTitle = Convert.ToString(json.AnnexueTitle)
                                };
                                ObservationHistory objHistory = new ObservationHistory()
                                {
                                    UserID = AuthenticationHelper.UserID,
                                    RoleID = roleid,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    ATBTID = Convert.ToInt32(json.ATBDID)
                                };

                                if (!string.IsNullOrEmpty(Convert.ToString(json.ISACPORMIS)))
                                {
                                    MstRiskResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                    objHistory.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                    AuditClosureResult.ISACPORMIS = Convert.ToInt16(json.ISACPORMIS);
                                }
                                objHistory.ObservationOld = json.hidObservation;
                                objHistory.ProcessWalkthroughOld = json.hidObservation;
                                objHistory.ActualWorkDoneOld = json.hidActualWorkDone;
                                objHistory.PopulationOld = json.hidpopulation;
                                objHistory.SampleOld = json.hidSample;
                                objHistory.ObservationTitleOld = json.hidObservationTitle;
                                objHistory.RiskOld = json.hidRisk;
                                objHistory.RootCauseOld = json.hidRootcost;
                                objHistory.FinancialImpactOld = json.hidfinancialImpact;
                                objHistory.RecommendationOld = json.hidRecommendation;
                                objHistory.ManagementResponseOld = json.hidMgtResponse;
                                objHistory.RemarksOld = json.hidRemarks;
                                objHistory.ScoreOld = json.hidAuditStepScore;
                                //objHistory.AnnexueTitleOld = json.hidAnnaxureTitle;
                                //objHistory.AnnexueTitle = json.AnnexueTitle;
                                DateTime dt5 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidResponseDueDate)))
                                {
                                    try
                                    {
                                        DateTime dt15 = Convert.ToDateTime(json.hidResponseDueDate);
                                        DateTime dt25 = GetDate(dt15.ToString("dd/MM/yyyy"));
                                        objHistory.ResponseDueDateOld = dt25.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.hidResponseDueDate);
                                        dt5 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.TimeLine = dt5.Date;
                                    }
                                }
                                DateTime dt = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidTimeLine)))
                                {
                                    try
                                    {
                                        DateTime dt1 = Convert.ToDateTime(json.hidTimeLine);
                                        DateTime dt21 = GetDate(dt1.ToString("dd/MM/yyyy"));
                                        objHistory.TimeLineOld = dt21.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.hidTimeLine);
                                        dt = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        objHistory.TimeLineOld = dt.Date;
                                    }
                                }
                                DateTime dt2 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.TimeLine)))
                                {
                                    try
                                    {
                                        DateTime dt11 = Convert.ToDateTime(json.TimeLine);
                                        DateTime dt21 = GetDate(dt11.ToString("dd/MM/yyyy"));
                                        MstRiskResult.TimeLine = dt21.Date;
                                        objHistory.TimeLine = dt21.Date;
                                        AuditClosureResult.TimeLine = dt21.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.TimeLine);
                                        dt2 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.TimeLine = dt2.Date;
                                        objHistory.TimeLine = dt2.Date;
                                        AuditClosureResult.TimeLine = dt2.Date;
                                    }
                                }
                                else
                                {
                                    MstRiskResult.TimeLine = null;
                                    objHistory.TimeLine = null;
                                    AuditClosureResult.TimeLine = null;
                                }

                                DateTime dt3 = new DateTime();
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ResponseDueDate)))
                                {
                                    try
                                    {
                                        DateTime dt13 = Convert.ToDateTime(json.ResponseDueDate);
                                        DateTime dt23 = GetDate(dt13.ToString("dd/MM/yyyy"));
                                        MstRiskResult.ResponseDueDate = dt23.Date;
                                        transaction.ResponseDueDate = dt23.Date;
                                    }
                                    catch (Exception)
                                    {
                                        string TL = Convert.ToString(json.ResponseDueDate);
                                        dt3 = DateTime.ParseExact(TL, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        MstRiskResult.ResponseDueDate = dt3.Date;
                                        transaction.ResponseDueDate = dt3.Date;
                                    }
                                }
                                else
                                {
                                    MstRiskResult.ResponseDueDate = null;
                                    transaction.ResponseDueDate = null;
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(json.BriefObservation)))
                                {
                                    MstRiskResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                    transaction.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.BriefObservation = Regex.Replace(Convert.ToString(json.BriefObservation).Trim(), @"\t|\n|\r", "");
                                }
                                else
                                {
                                    MstRiskResult.BriefObservation = null;
                                    transaction.BriefObservation = null;
                                    AuditClosureResult.BriefObservation = null;
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObjBackground)))
                                {
                                    MstRiskResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                    transaction.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ObjBackground = Regex.Replace(Convert.ToString(json.ObjBackground).Trim(), @"\t|\n|\r", "");
                                }
                                else
                                {
                                    MstRiskResult.ObjBackground = null;
                                    transaction.ObjBackground = null;
                                    AuditClosureResult.ObjBackground = null;
                                }
                                //if (!string.IsNullOrEmpty(DefeciencyType))
                                //{
                                //    MstRiskResult.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                                //    transaction.DefeciencyType = Convert.ToInt32(ddlDefeciencyType.SelectedValue);
                                //}
                                //else
                                //{
                                //    MstRiskResult.DefeciencyType = null;
                                //    transaction.DefeciencyType = null;
                                //}
                                objHistory.OldBodyContent = Convert.ToString(json.hidBodyContent);
                                objHistory.OldUHComment = Convert.ToString(json.hidUHComment);
                                objHistory.OldPRESIDENTComment = Convert.ToString(json.hidPRESIDENTComment);

                                if (string.IsNullOrEmpty(Convert.ToString(json.BodyContent)))
                                {
                                    MstRiskResult.BodyContent = "";
                                    objHistory.BodyContent = "";
                                    transaction.BodyContent = "";
                                    AuditClosureResult.BodyContent = "";
                                }
                                else
                                {
                                    MstRiskResult.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    objHistory.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    transaction.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                    AuditClosureResult.BodyContent = Convert.ToString(json.BodyContent).Trim();
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidPersonResponsible)))
                                {
                                    objHistory.PersonResponsibleOld = Convert.ToInt32(json.hidPersonResponsible);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationRating)))
                                {
                                    objHistory.ObservationRatingOld = Convert.ToInt32(json.hidObservationRating);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationCategory)))
                                {
                                    objHistory.ObservationCategoryOld = Convert.ToInt32(json.hidObservationCategory);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidObservationSubCategory)))
                                {
                                    objHistory.ObservationSubCategoryOld = Convert.ToInt32(json.hidObservationSubCategory);
                                }
                                //if (!string.IsNullOrEmpty(Convert.ToString(json.hidGroupObservationCategory)))
                                //{
                                //    objHistory.GroupObservationCategoryID = Convert.ToInt32(json.hidGroupObservationCategory);
                                //}
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidOwner)))
                                {
                                    objHistory.OwnerOld = Convert.ToInt32(json.hidOwner);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidUserID)))
                                {
                                    objHistory.UserOld = Convert.ToInt32(json.hidUserID);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                                {
                                    objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.hidISACPORMIS)))
                                {
                                    objHistory.ISACPORMISOld = Convert.ToInt32(json.hidISACPORMIS);
                                }

                                objHistory.AuditId = Convert.ToInt32(json.AuditID);


                                if (string.IsNullOrEmpty(Convert.ToString(json.AuditObjective)))
                                    MstRiskResult.AuditObjective = null;
                                else
                                    MstRiskResult.AuditObjective = Regex.Replace(Convert.ToString(json.AuditObjective).Trim(), @"\t|\n|\r", "");


                                if (string.IsNullOrEmpty(Convert.ToString(json.AuditSteps)))
                                    MstRiskResult.AuditSteps = "";
                                else
                                    MstRiskResult.AuditSteps = Regex.Replace(Convert.ToString(json.AuditSteps).Trim(), @"\t|\n|\r", "");

                                if (string.IsNullOrEmpty(Convert.ToString(json.AnalysisToBePerofrmed)))
                                    MstRiskResult.AnalysisToBePerofrmed = "";
                                else
                                    MstRiskResult.AnalysisToBePerofrmed = Regex.Replace(Convert.ToString(json.AnalysisToBePerofrmed).Trim(), @"\t|\n|\r", "");

                                if (string.IsNullOrEmpty(Convert.ToString(json.ProcessWalkthrough)))
                                {
                                    MstRiskResult.ProcessWalkthrough = "";
                                    objHistory.ProcessWalkthrough = "";
                                }
                                else
                                {
                                    MstRiskResult.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");
                                    objHistory.ProcessWalkthrough = Regex.Replace(Convert.ToString(json.ProcessWalkthrough).Trim(), @"\t|\n|\r", "");                                    
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.ActivityToBeDone)))
                                {
                                    MstRiskResult.ActivityToBeDone = "";
                                    objHistory.ActualWorkDone = "";
                                }
                                else
                                {
                                    MstRiskResult.ActivityToBeDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");
                                    objHistory.ActualWorkDone = Regex.Replace(Convert.ToString(json.ActivityToBeDone).Trim(), @"\t|\n|\r", "");                                    
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Population)))
                                {
                                    MstRiskResult.Population = "";
                                    objHistory.Population = "";
                                }
                                else
                                {
                                    MstRiskResult.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");
                                    objHistory.Population = Regex.Replace(Convert.ToString(json.Population).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Sample)))
                                {
                                    MstRiskResult.Sample = "";
                                    objHistory.Sample = "";
                                }
                                else
                                {
                                    MstRiskResult.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");
                                    objHistory.Sample = Regex.Replace(Convert.ToString(json.Sample).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.ObservationTitle)))
                                {
                                    MstRiskResult.ObservationTitle = "";
                                    objHistory.ObservationTitle = "";
                                    AuditClosureResult.ObservationTitle = "";
                                }
                                else
                                {
                                    MstRiskResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                                    objHistory.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ObservationTitle = Regex.Replace(Convert.ToString(json.ObservationTitle).Trim(), @"\t|\n|\r", "");                                    
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.AnnexueTitle)))
                                {
                                    MstRiskResult.AnnexueTitle = "";
                                }
                                else
                                {
                                    MstRiskResult.AnnexueTitle = Regex.Replace(Convert.ToString(json.AnnexueTitle).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.Observation)))
                                {
                                    MstRiskResult.Observation = "";
                                    objHistory.Observation = "";
                                    AuditClosureResult.Observation = "";
                                }
                                else
                                {
                                    MstRiskResult.Observation  = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                    objHistory.Observation  = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Observation = Regex.Replace(Convert.ToString(json.Observation).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.Risk)))
                                {
                                    MstRiskResult.Risk = "";
                                    objHistory.Risk = "";
                                    AuditClosureResult.Risk = "";
                                }
                                else
                                {
                                    MstRiskResult.Risk =Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                    objHistory.Risk =  Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Risk = Regex.Replace(Convert.ToString(json.Risk).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.RootCost)))
                                {
                                    MstRiskResult.RootCost = null;
                                    objHistory.RootCause = null;
                                    AuditClosureResult.RootCost = null;
                                }
                                else
                                {
                                    MstRiskResult.RootCost = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                    objHistory.RootCause = Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.RootCost=Regex.Replace(Convert.ToString(json.RootCost).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.FinancialImpact)))
                                {
                                    MstRiskResult.FinancialImpact = null;
                                    objHistory.FinancialImpact = null;
                                    AuditClosureResult.FinancialImpact = null;
                                }
                                else
                                {
                                    MstRiskResult.FinancialImpact = Regex.Replace(Convert.ToString(json.FinancialImpact).Trim(), @"\t|\n|\r", "");
                                    objHistory.FinancialImpact = Regex.Replace(Convert.ToString(json.FinancialImpact).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.FinancialImpact = Regex.Replace(Convert.ToString(json.FinancialImpact).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.Recomendation)))
                                {
                                    MstRiskResult.Recomendation = "";
                                    objHistory.Recommendation = "";
                                    AuditClosureResult.Recomendation = "";
                                }
                                else
                                {
                                    MstRiskResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                    objHistory.Recommendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.Recomendation = Regex.Replace(Convert.ToString(json.Recomendation).Trim(), @"\t|\n|\r", "");
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(json.ManagementResponse)))
                                {
                                    MstRiskResult.ManagementResponse = "";
                                    objHistory.ManagementResponse = "";
                                    AuditClosureResult.ManagementResponse = "";
                                }
                                else
                                {
                                    MstRiskResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                    objHistory.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                    AuditClosureResult.ManagementResponse = Regex.Replace(Convert.ToString(json.ManagementResponse).Trim(), @"\t|\n|\r", "");
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(json.FixRemark)))
                                    MstRiskResult.FixRemark = "";
                                else
                                    MstRiskResult.FixRemark = Regex.Replace(Convert.ToString(json.FixRemark).Trim(), @"\t|\n|\r", "");

                                if (!string.IsNullOrEmpty(Convert.ToString(json.PersonResponsible)))
                                {
                                    if (Convert.ToString(json.PersonResponsible) == "-1")
                                    {
                                        MstRiskResult.PersonResponsible = null;
                                        transaction.PersonResponsible = null;
                                        objHistory.PersonResponsible = null;
                                        AuditClosureResult.PersonResponsible = null;
                                    }
                                    else
                                    {
                                        MstRiskResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        transaction.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        objHistory.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                        AuditClosureResult.PersonResponsible = Convert.ToInt32(json.PersonResponsible);
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.Owner)))
                                {
                                    if (Convert.ToString(json.Owner) == "-1")
                                    {
                                        MstRiskResult.Owner = null;
                                        transaction.Owner = null;
                                        objHistory.Owner = null;
                                        AuditClosureResult.Owner = null;
                                    }
                                    else
                                    {
                                        MstRiskResult.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        transaction.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        objHistory.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                        AuditClosureResult.Owner = Convert.ToInt32(Convert.ToString(json.Owner));
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservatioRating)))
                                {
                                    if (Convert.ToString(json.ObservatioRating) == "-1")
                                    {
                                        transaction.ObservatioRating = null;
                                        MstRiskResult.ObservationRating = null;
                                        objHistory.ObservationRating = null;
                                        AuditClosureResult.ObservationRating = null;
                                    }
                                    else
                                    {
                                        transaction.ObservatioRating = Convert.ToInt32(json.ObservatioRating);
                                        MstRiskResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                        objHistory.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                        AuditClosureResult.ObservationRating = Convert.ToInt32(json.ObservatioRating);
                                    }
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationCategory)))
                                {
                                    if (Convert.ToString(json.ObservationCategory) == "-1")
                                    {
                                        transaction.ObservationCategory = null;
                                        MstRiskResult.ObservationCategory = null;
                                        objHistory.ObservationCategory = null;
                                        AuditClosureResult.ObservationCategory = null;
                                    }
                                    else
                                    {
                                        transaction.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        MstRiskResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        objHistory.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                        AuditClosureResult.ObservationCategory = Convert.ToInt32(json.ObservationCategory);
                                    }
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(json.ObservationSubCategory)))
                                {
                                    if (Convert.ToString(json.ObservationSubCategory) == "-1")
                                    {
                                        transaction.ObservationSubCategory = null;
                                        MstRiskResult.ObservationSubCategory = null;
                                        objHistory.ObservationSubCategory = null;
                                        AuditClosureResult.ObservationSubCategory = null;
                                    }
                                    else
                                    {
                                        transaction.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        MstRiskResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        objHistory.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                        AuditClosureResult.ObservationSubCategory = Convert.ToInt32(json.ObservationSubCategory);
                                    }
                                }
                                //if (!string.IsNullOrEmpty(Convert.ToString(json.GroupObservationCategoryID)))
                                //{
                                //    if (Convert.ToString(json.GroupObservationCategoryID) == "-1")
                                //    {
                                //        transaction.GroupObservationCategoryID = null;
                                //        MstRiskResult.GroupObservationCategoryID = null;
                                //        objHistory.GroupObservationCategoryID = null;
                                //        AuditClosureResult.GroupObservationCategoryID = null;
                                //    }
                                //    else
                                //    {
                                //        transaction.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        MstRiskResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        objHistory.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //        AuditClosureResult.GroupObservationCategoryID = Convert.ToInt32(json.GroupObservationCategoryID);
                                //    }
                                //}

                                string remark = string.Empty;
                                if (CurrentStatus == "Closed")
                                {
                                    transaction.StatusId = 3;
                                    MstRiskResult.AStatusId = 3;
                                    remark = "Audit Steps Closed.";
                                    MstRiskResult.AuditeeResponse = "HC";
                                    MstRiskResult.AuditeeResponse = "C";
                                    transaction.Remarks = remark.Trim();
                                }
                                else if (CurrentStatus == "Team Review")
                                {
                                    transaction.StatusId = 4;
                                    MstRiskResult.AStatusId = 4;
                                    remark = "Audit Steps Under Team Review.";
                                    MstRiskResult.AuditeeResponse = "HT";
                                    transaction.Remarks = remark.Trim();
                                }
                                else if (CurrentStatus == "Final Review")
                                {
                                    transaction.StatusId = 5;
                                    MstRiskResult.AStatusId = 5;
                                    remark = "Audit Steps Under Final Review.";
                                    MstRiskResult.AuditeeResponse = "HF";
                                    transaction.Remarks = remark.Trim();
                                }
                                else if (CurrentStatus == "Auditee Review")
                                {
                                    transaction.StatusId = 6;
                                    MstRiskResult.AStatusId = 6;
                                    remark = "Audit Steps Under Auditee Review.";
                                    MstRiskResult.AuditeeResponse = "RA";
                                    transaction.Remarks = remark.Trim();
                                }
                                objHistory.Remarks = remark;

                                bool Success1 = false;
                                bool Success2 = false;
                                if (RiskCategoryManagement.InternalControlResultExists_ObservationList(MstRiskResult))
                                {
                                    MstRiskResult.AStatusId = 3;
                                    MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    MstRiskResult.UpdatedOn = DateTime.Now;
                                    Success1 = RiskCategoryManagement.UpdateInternalControlResult_ObaservationList(MstRiskResult);
                                }
                                if (RiskCategoryManagement.InternalAuditTxnExistsAuditManager_ObaservationList(transaction))
                                {
                                    transaction.StatusId = 3;
                                    transaction.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                    transaction.UpdatedOn = DateTime.Now;
                                    Success1 = RiskCategoryManagement.updateInternalAuditTxnObjCatergeriAM(transaction);
                                }
                                using (AuditControlEntities entities = new AuditControlEntities())
                                {
                                    var RecordtoUpdate = (from row in entities.InternalControlAuditResults
                                                          where row.ProcessId == MstRiskResult.ProcessId
                                                          && row.FinancialYear == MstRiskResult.FinancialYear
                                                          && row.ForPerid == MstRiskResult.ForPerid
                                                          && row.CustomerBranchId == MstRiskResult.CustomerBranchId
                                                          && row.UserID == MstRiskResult.UserID
                                                          && row.RoleID == MstRiskResult.RoleID && row.ATBDId == MstRiskResult.ATBDId
                                                          && row.VerticalID == MstRiskResult.VerticalID
                                                          && row.AuditID == MstRiskResult.AuditID
                                                          select row.ID).OrderByDescending(x => x).FirstOrDefault();
                                    if (RecordtoUpdate != null)
                                    {
                                        AuditClosureResult.ResultID = RecordtoUpdate;
                                        if (RiskCategoryManagement.AuditClosureRecordExists(AuditClosureResult))
                                        {
                                            AuditClosureResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            AuditClosureResult.UpdatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.UpdateCategeriAuditClosure_ObservationList(AuditClosureResult);
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(objHistory.Observation))
                                {
                                    if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                    {
                                        objHistory.CreatedBy = AuthenticationHelper.UserID;
                                        objHistory.CreatedOn = DateTime.Now;
                                        RiskCategoryManagement.AddObservationHistory(objHistory);
                                    }
                                }

                                string Testremark = "";
                                if (string.IsNullOrEmpty(Convert.ToString(json.txtRemark)))
                                {
                                    Testremark = "NA";
                                }
                                else
                                {
                                    Testremark = Convert.ToString(json.txtRemark).Trim();
                                }
                                #endregion

                                if (!string.IsNullOrEmpty(objHistory.Observation))
                                {
                                    if (!RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                    {
                                        RiskCategoryManagement.AddObservationHistory(objHistory);
                                    }
                                }

                                #region
                                InternalReviewHistory RH = new InternalReviewHistory()
                                {
                                    ProcessId = Convert.ToInt32(getScheduleonDetails.ProcessId),
                                    InternalAuditInstance = getScheduleonDetails.InternalAuditInstance,
                                    CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                    CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                    Dated = DateTime.Now,
                                    Remarks = Testremark,
                                    AuditScheduleOnID = getScheduleonDetails.ID,
                                    FinancialYear = Convert.ToString(json.FinYear),
                                    CustomerBranchId = Convert.ToInt32(json.BranchID),
                                    ATBDId = Convert.ToInt32(json.ATBDID),
                                    FixRemark = "NA",
                                    VerticalID = Convert.ToInt32(json.VerticalID),
                                    AuditID = Convert.ToInt32(json.AuditID),
                                    //  UniqueFlag= UniqueFlag,
                                };
                                if (RiskCategoryManagement.CheckRecordExist(RH))
                                {
                                    Flag = RiskCategoryManagement.CreateInternalReviewRemark(RH);
                                }

                                #endregion

                                if (Success1 == true)
                                {

                                }
                                else
                                {
                                    ErrorMessage = "Something went wrong.";
                                    if (!string.IsNullOrEmpty(ErrorMessage))
                                    {
                                        string jsonstringError = JsonConvert.SerializeObject(ErrorMessage);
                                        return new HttpResponseMessage()
                                        {
                                            Content = new StringContent(jsonstringError, Encoding.UTF8, "application/json")
                                        };
                                    }
                                }

                                #region Audio Vedeo save
                                if (Success1)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(json.AudioVideoLink)))
                                    {
                                        List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                                        if (observationAudioVideoList.Count > 0)
                                        {
                                            string CheckCommaValOne = Convert.ToString(json.AudioVideoLink);
                                            string[] audioVideoLinks = CheckCommaValOne.Split(',');
                                            var filtered = observationAudioVideoList.Where(i => !audioVideoLinks.Contains(i.AudioVideoLink)).ToList();
                                            if (filtered.Count > 0)
                                            {
                                                foreach (var item in filtered)
                                                {
                                                    RiskCategoryManagement.DeleteObservationAudioVideo(item.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                                }
                                            }
                                        }

                                        ObservationAudioVideo objObservationAudioVideo = new ObservationAudioVideo()
                                        {
                                            AuditId = Convert.ToInt32(json.AuditID),
                                            ATBTID = Convert.ToInt32(json.ATBDID),
                                            IsActive = true,
                                        };

                                        string CheckCommaVal = Convert.ToString(json.AudioVideoLink);
                                        if (CheckCommaVal.Contains(',')) //checking entered single value or multiple values
                                        {
                                            string[] audioVideoLinks = (Convert.ToString(json.AudioVideoLink)).Split(',');
                                            int lenght = audioVideoLinks.Length;
                                            string link = string.Empty;
                                            for (int i = 0; i < audioVideoLinks.Length; i++)
                                            {
                                                link = audioVideoLinks[i].ToString();
                                                objObservationAudioVideo.AudioVideoLink = link.Trim();
                                                if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                                {
                                                    objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                                    objObservationAudioVideo.CreatedOn = DateTime.Now;
                                                    RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                                }
                                                else
                                                {
                                                    objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                                    objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                                    RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                            if (!RiskCategoryManagement.CheckObservationAudioVideoExist(objObservationAudioVideo))
                                            {
                                                objObservationAudioVideo.CreatedBy = AuthenticationHelper.UserID;
                                                objObservationAudioVideo.CreatedOn = DateTime.Now;
                                                RiskCategoryManagement.SaveObservationAudioVideo(objObservationAudioVideo);
                                            }
                                            else
                                            {
                                                objObservationAudioVideo.AudioVideoLink = (Convert.ToString(json.AudioVideoLink)).Trim();
                                                objObservationAudioVideo.UpdatedBy = AuthenticationHelper.UserID;
                                                objObservationAudioVideo.UpdatedOn = DateTime.Now;
                                                RiskCategoryManagement.UpdateObservationAudioVideo(objObservationAudioVideo);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // when user want delete all existing link then we will get null from txtMultilineVideolink
                                        List<ObservationAudioVideo> observationAudioVideoList = RiskCategoryManagement.GetAllObservationAudioVideo(Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID));
                                        if (observationAudioVideoList.Count > 0)
                                        {
                                            foreach (ObservationAudioVideo ObjObservationAudioVideo in observationAudioVideoList)
                                            {
                                                RiskCategoryManagement.DeleteObservationAudioVideo(ObjObservationAudioVideo.AudioVideoLink, Convert.ToInt32(json.AuditID), Convert.ToInt32(json.ATBDID), AuthenticationHelper.UserID);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessage = "Server Error Occured. Please try again.";
                                }
                                #endregion
                            }
                        }
                    }//getScheduleonDetails
                }//ddlFilterStatus  Null
                #endregion
                string result = string.Empty;
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    result = ErrorMessage;
                }
                else
                {
                    result = "success";
                }
                string jsonstring = JsonConvert.SerializeObject(result);
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                string jsonstring = JsonConvert.SerializeObject("Something went wrong");
                return new HttpResponseMessage()
                {
                    Content = new StringContent(jsonstring, Encoding.UTF8, "application/json")
                };
            }
        }
        #endregion

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        [Route("AuditStatus/BindAnnexureFileGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindAnnexureFileGrid(int AuditID, int ATBDID)
        {
            try
            {
                List<Tran_AnnxetureUpload> AnnxetureDocument = new List<Tran_AnnxetureUpload>();
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    AnnxetureDocument = (from row in entities.Tran_AnnxetureUpload
                                         where row.ATBDId == ATBDID && row.IsDeleted == false
                                         && row.AuditID == AuditID
                                         select row).ToList();

                    if (AnnxetureDocument.Count > 0)
                    {
                        AnnxetureDocument = AnnxetureDocument.Where(entry => entry.Version == "1.0").ToList();
                    }

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(AnnxetureDocument).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/DeleteAnnexureFile")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DeleteAnnexureFile(int FileID)
        {
            try
            {
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                bool result = DashboardManagementRisk.DeleteFileAnnxeture(null, FileID);
                if (result)
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                }
                else
                {
                    UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("AuditStatus/BindWorkingFileTwoFilegrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindWorkingFileTwoFilegrid(int AuditID, int ATBDID)
        {
            try
            {
                List<GetInternalAuditDocumentsView> fileData = new List<GetInternalAuditDocumentsView>();
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    fileData = (from row in entities.GetInternalAuditDocumentsViews
                                where row.ATBDId == ATBDID && row.IsDeleted == false
                                && row.AuditID == AuditID && row.TypeOfFile == "OB"
                                select row).ToList();


                    if (fileData.Count > 0)
                    {
                        fileData = fileData.Where(entry => entry.Version == "1.0").ToList();
                    }

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(fileData).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/BindAuditLogGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindAuditLogGrid(int AuditID, int ATBDID, int ScheduleOnID)
        {
            try
            {
                var fileData = DashboardManagementRisk.GetAllInternalAuditTransactions(ScheduleOnID, ATBDID, AuditID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(fileData).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/BindReviewHistoryGrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindReviewHistoryGrid(int AuditID, int ATBDID, int ScheduleOnID, int ProcessID)
        {
            try
            {
                var fileData = Business.DashboardManagementRisk.GetInternalAllRemarks(ProcessID, ScheduleOnID, ATBDID, AuditID);

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(fileData).ToString(), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/BindUploadImagegrid")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindUploadImagegrid(int AuditID, int ATBDID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ImageListdata = (from row in entities.ObservationImages
                                         where row.ATBTID == ATBDID &&
                                         row.AuditID == AuditID && row.IsActive == true
                                         select row).ToList();

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(ImageListdata).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/DeleteUploadedImage")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DeleteUploadedImage(int FileID)
        {
            try
            {
                bool result = false;
                List<UpdateNotification> UpdateNotificationObj = new List<UpdateNotification>();
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var Imagelist = (from row in entities.ObservationImages
                                     where row.ID == FileID
                                     select row).FirstOrDefault();
                    if (Imagelist != null)
                    {
                        Imagelist.IsActive = false;
                        entities.SaveChanges();
                        result = true;
                    }
                    if (result)
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "True" });
                    }
                    else
                    {
                        UpdateNotificationObj.Add(new UpdateNotification { Message = "False" });
                    }
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JArray.FromObject(UpdateNotificationObj).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/BindAuditDetialReviewPeriod")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindAuditDetialReviewPeriod(int AuditID, int ATBDID, int ProcessID)
        {
            try
            {
                object content = null;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var List = (from row in entities.SP_GetAuditDetailsReviewPeriod(AuditID, ATBDID, ProcessID)
                                select row).FirstOrDefault();

                    if (List != null)
                    {
                        content = new
                        {
                            AuditName = List.AuditName
                            // ReviewPeriod = List.ReviewPeriod,
                        };
                    }

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(content).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }


        [Route("AuditStatus/AuditClosureDetailsforHistoricalObj")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage AuditClosureDetailsforHistoricalObj(int AuditID, int ATBDID)
        {
            try
            {
                object content = null;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var List = (from row in entities.AuditClosures
                                where row.AuditID == AuditID
                                && row.ATBDId == ATBDID
                                select row).FirstOrDefault();

                    content = new
                    {
                        ObservationTitle = List.ObservationTitle,
                        Observation = List.Observation,
                        BriefObservation = List.BriefObservation,
                        ObjBackground = List.ObjBackground,
                        Risk = List.Risk,
                        RootCost = List.RootCost,
                        FinancialImpact = List.FinancialImpact,
                        Recomendation = List.Recomendation,
                        ManagementResponse = List.ManagementResponse,
                        PersonResponsible = Convert.ToString(List.PersonResponsible),
                        ObservationRating = Convert.ToString(List.ObservationRating),
                        ObservationCategory = Convert.ToString(List.ObservationCategory),
                    };

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(content).ToString(), Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

        [Route("AuditStatus/BindStatusList")]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage BindStatusList(int CustomerId, int AuditID, int ATBDID, int ProcessID, int BranchID, int StatusID)
        {
            try
            {
                List<StatusListDetails> objStatusList = new List<StatusListDetails>();
                bool PersonApplicable = CustomerBranchManagement.CheckPersonResponsibleFlowApplicable(CustomerId, BranchID);
                var rolelist = CustomerManagementRisk.GetAssignedRolesARS(AuthenticationHelper.UserID);
                var isAuditmanager = CustomerManagementRisk.GetAuditHeadOrManagerid(AuthenticationHelper.UserID);
                List<long> ProcessList = new List<long>();
                ProcessList.Add(ProcessID);
                var statusList = AuditStatusManagement.GetInternalStatusList();
                List<InternalAuditStatu> allowedStatusList = null;
                List<InternalAuditStatusTransition> ComplianceStatusTransitionList = AuditStatusManagement.GetInternalAuditStatusTransitionListByInitialId(StatusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();
                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.ID).ToList();
                var RoleListInAudit = CustomerManagementRisk.GetAuditListRole(AuditID, ProcessList);

                if (PersonApplicable)
                {
                    if (StatusID == 6)
                    {
                        #region                        
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (RoleListInAudit.Contains(5))
                            {
                                if ((st.ID == 3) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    // rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                            else
                            {
                                if ((st.ID == 5) || (st.ID == 2)) //|| (st.ID == 6)
                                {
                                }
                                else
                                {
                                    //rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                        }
                        #endregion
                    }
                    else if (StatusID == 5)
                    {
                        #region
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            var RoleListInAudit1 = CustomerManagementRisk.GetListOfRoleInAudit(AuditID, AuthenticationHelper.UserID, ProcessList);
                            if (RoleListInAudit1.Contains(5))
                            {
                                if ((st.ID == 4) || (st.ID == 6) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    //rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                            else if (RoleListInAudit1.Contains(4))
                            {
                                if ((st.ID == 6) || (st.ID == 3) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                            else
                            {
                                if ((st.ID == 4) || (st.ID == 6) || (st.ID == 3) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                        }
                        #endregion
                    }
                    else if (StatusID == 4)
                    {
                        #region

                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (RoleListInAudit.Contains(5))
                            {
                                if (st.ID == 3 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    //rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                            else
                            {
                                if (st.ID == 5 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    //rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                        }
                        #endregion
                    }
                    else if (StatusID == 2)
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (RoleListInAudit.Contains(5))
                            {
                                if (st.ID == 3 || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    //rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                            else
                            {
                                var RoleListInAudit2 = CustomerManagementRisk.GetListOfRoleInAudit(AuditID, AuthenticationHelper.UserID, ProcessList);
                                if (RoleListInAudit2.Contains(5))
                                {
                                    if (st.ID == 3 || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        //rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        StatusListDetails Objstatus = new StatusListDetails();
                                        Objstatus.ID = Convert.ToString(st.ID);
                                        Objstatus.Name = st.Name;
                                        objStatusList.Add(Objstatus);
                                    }
                                }
                                else
                                {
                                    if (st.ID == 5 || (st.ID == 2))
                                    {
                                    }
                                    else
                                    {
                                        //rdbtnStatus.Items.Insert(0, new ListItem(st.Name, Convert.ToString(st.ID)));
                                        StatusListDetails Objstatus = new StatusListDetails();
                                        Objstatus.ID = Convert.ToString(st.ID);
                                        Objstatus.Name = st.Name;
                                        objStatusList.Add(Objstatus);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        #region
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {
                                //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                StatusListDetails Objstatus = new StatusListDetails();
                                Objstatus.ID = Convert.ToString(st.ID);
                                Objstatus.Name = st.Name;
                                objStatusList.Add(Objstatus);
                            }
                            else
                            {
                                //if (string.IsNullOrEmpty(txtObservation.Text))
                                //{
                                if (!(st.ID == 6))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                        StatusListDetails Objstatus = new StatusListDetails();
                                        Objstatus.ID = Convert.ToString(st.ID);
                                        Objstatus.Name = st.Name;
                                        objStatusList.Add(Objstatus);
                                    }
                                }
                                //}
                                //else
                                //{
                                else if (!(st.ID == 5))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                        StatusListDetails Objstatus = new StatusListDetails();
                                        Objstatus.ID = Convert.ToString(st.ID);
                                        Objstatus.Name = st.Name;
                                        objStatusList.Add(Objstatus);
                                    }
                                }
                                //}
                            }

                        }
                        #endregion
                    }
                }//Personresponsible End
                else
                {
                    if (StatusID == 5)
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if (!(st.ID == 4))
                            {
                                if (!(st.ID == 2))
                                {
                                    //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (InternalAuditStatu st in allowedStatusList)
                        {
                            if ((isAuditmanager == "AM" || isAuditmanager == "AH") && rolelist.Contains(4))
                            {
                                if ((st.ID == 4) || (st.ID == 7) || (st.ID == 2))
                                {
                                }
                                else
                                {
                                    //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                    StatusListDetails Objstatus = new StatusListDetails();
                                    Objstatus.ID = Convert.ToString(st.ID);
                                    Objstatus.Name = st.Name;
                                    objStatusList.Add(Objstatus);
                                }
                            }
                            else
                            {
                                if (!(st.ID == 7))
                                {
                                    if (!(st.ID == 2))
                                    {
                                        //rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                                        StatusListDetails Objstatus = new StatusListDetails();
                                        Objstatus.ID = Convert.ToString(st.ID);
                                        Objstatus.Name = st.Name;
                                        objStatusList.Add(Objstatus);
                                    }
                                }
                            }
                        }
                    }
                }

                var users = (from row in objStatusList
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(users).ToString(), Encoding.UTF8, "application/json")
                };

                //lblStatus.Visible = allowedStatusList.Count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<ErrorDetails> errordetail = new List<ErrorDetails>();
                errordetail.Add(new ErrorDetails { ErrorMessage = ex.Message, ErrorDetailMessage = ex.InnerException.ToString() });
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JArray.FromObject(errordetail).ToString(), Encoding.UTF8, "application/json")
                };
            }
        }

    }
}
