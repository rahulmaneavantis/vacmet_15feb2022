function exportReport(e)
{
    debugger;
    if (document.getElementById('IsLabel').value == 1) {
        var ReportName = "Report of Compliances";
        var customerName = document.getElementById('CustName').value;
        // var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
        var todayDate = moment().format('DD-MMM-YYYY');
        var grid = $("#grid").getKendoGrid();

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportName }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {

                //ExcelData = view.ToTable("Selected", false, "Branch", "ActName", "ShortDescription", "User", "ScheduledOn", "ForMonth", "Status", "RiskCategory", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID", "OverdueBy");

                cells: [
                     { value: "Compliance ID", bold: true },
                    { value: "Location", bold: true },
                    { value: "Act Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Short Form", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "For Month", bold: true },
                    { value: "OverdueBy(Days)", bold: true },
                    { value: "Label", bold: true },
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [
                      { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.ActName },
                    { value: dataItem.ShortDescription },
                    { value: "Short Form", bold: true },
                    { value: dataItem.Performer },
                    { value: dataItem.Reviewer },
                    { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                    { value: dataItem.ForMonth },
                    { value: dataItem.OverdueBy },
                    { value: dataItem.SequenceID }
                ]
            });
        }
        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 10; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;
            }
        }
        excelExport(rows, ReportName);
        e.preventDefault();
    }
    else {
        var ReportName = "Report of Compliances";
        var customerName = document.getElementById('CustName').value;
        var todayDate = moment().format('DD-MM-YYYY');
       // var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
        var grid = $("#grid").getKendoGrid();

        var rows = [
            {
                cells: [
                    { value: "Entity/ Location:", bold: true },
                    { value: customerName }
                ]
            },
            {
                cells: [
                    { value: "Report Name:", bold: true },
                    { value: ReportName }
                ]
            },
            {
                cells: [
                    { value: "Report Generated On:", bold: true },
                    { value: todayDate }
                ]
            },
            {
                cells: [
                    { value: "" }
                ]
            },
            {

                //ExcelData = view.ToTable("Selected", false, "Branch", "ActName", "ShortDescription", "User", "ScheduledOn", "ForMonth", "Status", "RiskCategory", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID", "OverdueBy");

                cells: [
                      { value: "Compliance ID", bold: true },
                    { value: "Location", bold: true },
                    { value: "Act Name", bold: true },
                    { value: "Short Description", bold: true },
                    { value: "Short Form", bold: true },
                    { value: "Performer", bold: true },
                    { value: "Reviewer", bold: true },
                    { value: "Due Date", bold: true },
                    { value: "For Month", bold: true },
                    { value: "OverdueBy(Days)", bold: true }
                ]
            }
        ];

        var trs = grid.dataSource;
        var filteredDataSource = new kendo.data.DataSource({
            data: trs.data(),
            filter: trs.filter()
        });

        filteredDataSource.read();
        var data = filteredDataSource.view();
        for (var i = 0; i < data.length; i++) {
            var dataItem = data[i];
            rows.push({
                cells: [
                    { value: dataItem.ComplianceID },
                    { value: dataItem.Branch },
                    { value: dataItem.ActName },
                    { value: dataItem.ShortDescription },
                    { value: dataItem.ShortForm },
                    { value: dataItem.Performer },
                    { value: dataItem.Reviewer },
                    { value: dataItem.ScheduledOn , format:"dd-MMM-yyyy"},
                    { value: dataItem.ForMonth },
                    { value: dataItem.OverdueBy }
                ]
            });
        }
        for (var i = 4; i < rows.length; i++) {
            for (var j = 0; j < 9; j++) {
                rows[i].cells[j].borderBottom = "#000000";
                rows[i].cells[j].borderLeft = "#000000";
                rows[i].cells[j].borderRight = "#000000";
                rows[i].cells[j].borderTop = "#000000";
                rows[i].cells[j].hAlign = "left";
                rows[i].cells[j].vAlign = "top";
                rows[i].cells[j].wrap = true;
            }
        }
        excelExport(rows, ReportName);
        e.preventDefault();
    }
}

function excelExport(rows, ReportName) {

    if (document.getElementById('IsLabel').value == 1)
    {

        var workbook = new kendo.ooxml.Workbook({
            sheets: [
                {
                    columns: [
                          { width: 180 },
                        { width: 180 },
                        { width: 300 },
                        { width: 300 },
                        { width: 180 },
                        { width: 180 },
                        { width: 180 },
                        { width: 100 },
                        { width: 100 },
                        { width: 150 },
                        { width: 100 },
                        { width: 100 }
                    ],
                    title: "Report",
                    rows: rows
                },
            ]
        });

        var nameOfPage = "StatutoryOverdueReport";
        kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
    }
    else {

        var workbook = new kendo.ooxml.Workbook({
            sheets: [
                {
                    columns: [
                          { width: 180 },
                        { width: 180 },
                        { width: 300 },
                        { width: 300 },
                        { width: 180 },
                        { width: 180 },
                        { width: 180 },
                        { width: 100 },
                        { width: 100 },
                        { width: 150 },
                        { width: 100 }
                    ],
                    title: "Report",
                    rows: rows
                },
            ]
        });

        var nameOfPage = "StatutoryOverdueReport";
        kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
    }


}

function FilterGridStatutory() {
    
    var locationsdetails = [];
    //location details
    if ($("#dropdowntree").data("kendoDropDownTree") != undefined)
    {
        locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
    }

    //risk Details
    var Riskdetails = [];
    if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined)
    {
        Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
    }

    //user Details
    var userdetails = [];
    if ($("#dropdownUser").data("kendoDropDownTree") != undefined)
    {        
        userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
    }
    //datefilter
    var datedetails = [];
    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
        });
    }
    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
        datedetails.push({
            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
        });
    }

    var finalSelectedfilter = { logic: "and", filters: [] };

    if (locationsdetails.length > 0
       || Riskdetails.length > 0
        || $("#txtSearchComplianceID").val() != ""  || $("#txtSearchComplianceID").val() != undefined
       || ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined)
       || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
          || ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "")
       || datedetails.length > 0
        || userdetails.length > 0)
    {

        if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
         
            var SequenceFilter = { logic: "or", filters: [] };
            SequenceFilter.filters.push({
                field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
            });
            finalSelectedfilter.filters.push(SequenceFilter);
        }

        if (locationsdetails.length > 0) {
            var LocationFilter = { logic: "or", filters: [] };

            $.each(locationsdetails, function (i, v) {
                LocationFilter.filters.push({
                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(LocationFilter);
        }

        if (Riskdetails.length > 0) {
            var RiskFilter = { logic: "or", filters: [] };

            $.each(Riskdetails, function (i, v) {

                RiskFilter.filters.push({
                    field: "Risk", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(RiskFilter);
        }

        if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
            if ($("#dropdownfunction").val() != 0) {

                var FunctionFilter = { logic: "or", filters: [] };

                FunctionFilter.filters.push({
                    field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
                });

                finalSelectedfilter.filters.push(FunctionFilter);
            }
        }

        if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
        {
            var ActFilter = { logic: "or", filters: [] };
            ActFilter.filters.push({
                field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
            });
            finalSelectedfilter.filters.push(ActFilter);
        }

        if (datedetails.length > 0) {
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                var DateFilter = { logic: "or", filters: [] };
                DateFilter.filters.push({
                    field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                });

                finalSelectedfilter.filters.push(DateFilter);
            }

            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                var DateFilter = { logic: "or", filters: [] };
                DateFilter.filters.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                });

                finalSelectedfilter.filters.push(DateFilter);
            }
        }

        if (userdetails.length > 0) {
            var UserFilter = { logic: "or", filters: [] };

            $.each(userdetails, function (i, v) {
                UserFilter.filters.push({
                    field: "UserID", operator: "eq", value: parseInt(v)
                });
            });

            finalSelectedfilter.filters.push(UserFilter);
        }
        if ($("#txtSearchComplianceID").val() != "") {
            var RiskFilter = { logic: "or", filters: [] };
            RiskFilter.filters.push({
                field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
            });
            finalSelectedfilter.filters.push(RiskFilter);
        }

        if (finalSelectedfilter.filters.length > 0)
        {
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.filter(finalSelectedfilter);
        }
        else {
            $("#grid").data("kendoGrid").dataSource.filter({});
        }
    }
    else
    {
        $("#grid").data("kendoGrid").dataSource.filter({});
    }
    var dataSource = $("#grid").data("kendoGrid").dataSource;
    if (dataSource._total > 20 && dataSource.pageSize == undefined) {
        dataSource.pageSize(total);
    }
}

function ClearAllFilterMain(e) {
    $("#dropdowntree").data("kendoDropDownTree").value([]);
    $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
    $("#dropdownUser").data("kendoDropDownTree").value([]);
    $("#dropdownlistStatus").data("kendoDropDownList").value([]);
    $("#dropdownfunction").data("kendoDropDownList").value([]);
    $("#dropdownACT").data("kendoDropDownList").value([]);
    $("#dropdownSequence").data("kendoDropDownList").value([]);
    $("#dropdownPastData").data("kendoDropDownList").value([]);
    $("#Startdatepicker").val('');
    $("#Lastdatepicker").val('');
    $("#grid").data("kendoGrid").dataSource.filter({});
    var dataSource = $("#grid").data("kendoGrid").dataSource;
    dataSource.pageSize(10);
 //   e.preventDefault();
}

function fcloseStory(obj) {

    var DataId = $(obj).attr('data-Id');
    var dataKId = $(obj).attr('data-K-Id');
    var seq = $(obj).attr('data-seq');
    var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
    $(deepspan).trigger('click');
    var upperli = $('#' + dataKId);
    $(upperli).remove();

    //for rebind if any pending filter is present (Main Grid)
    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
    fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
    fCreateStoryBoard('dropdownSequence', 'filterCompSubType', 'sequence');
    fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');

    CheckFilterClearorNotMain();
};

function CheckFilterClearorNotMain() {
    if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownlistRisk').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
        ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
    }
}

function fCreateStoryBoard(Id, div, filtername) {

    var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
    $('#' + div).html('');
    $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
    $('#' + div).css('display', 'block');

    //if (div == 'filtersstoryboard') {
    //    $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
    //    $('#ClearfilterMain').css('display', 'block');
    //}
    //else if (div == 'filtertype') {
    //    $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
    //}
    //else if (div == 'filterrisk') {
    //    $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
    //    $('#ClearfilterMain').css('display', 'block');
    //}


    if (div == 'filtersstoryboard') {
        $('#' + div).append('Location&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterrisk') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterUser') {
        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterstatus') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
        $('#ClearfilterMain').css('display', 'block');
    }
    else if (div == 'filterpstData1') {
        $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCategory') {
        $('#' + div).append('Category&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterAct') {
        $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompSubType') {
        $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterCompType') {
        $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtersstoryboard1') {
        $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filtertype1') {
        $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterrisk1') {
        $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterFY') {
        $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterUser') {
        $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }
    else if (div == 'filterstatus1') {
        $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
        $('#Clearfilter').css('display', 'block');
    }

    for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
        var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
        $(button).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
        var buttontest = $($(button).find('span')[0]).text();
        if (buttontest.length > 10) {
            buttontest = buttontest.substring(0, 10).concat("...");
        }
        $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:1px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
        //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');

    }

    if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
        $('#' + div).css('display', 'none');
        $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

    }
    CheckFilterClearorNotMain();
}
