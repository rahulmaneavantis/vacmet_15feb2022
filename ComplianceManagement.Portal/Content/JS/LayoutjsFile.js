﻿
kendo.data.DataSource.prototype.dataFiltered = function () {
    
    // Gets the filter from the dataSource
    var filters = this.filter();

    // Gets the full set of data from the data source
    var allData = this.data();

    // Applies the filter to the data
    var query = new kendo.data.Query(allData);

    // Returns the filtered data
    return query.filter(filters).data;
}

function fRedirect(obj) {   
    document.location.href = $(obj).attr('data-ref');
}

function fmenustyle() {
    
}

function setBreadCrumb(object) {
          
    $.each(object, function (key, eachobject) {
        $('.breadcrumbs').append("<li><a href='" + eachobject.navigateUrl + "'>" + eachobject.displayText + "</a></li>");
    });

    //if (val.length > 0) {
    //    var breadcrumbItems = val.split(",");
    //    if (breadcrumbItems.length > 0) {
    //        $.each(breadcrumbItems, function (index, value) {
    //            $('.breadcrumbs').append("<li><a href='#'>" + value + "</a></li>");
    //        });
    //    }
    //}      
}

function ToggleHideSideMenu() {
    
    if ($('#sidebar').is(":visible") === true) {
        $('#main-content').css({
            'margin-left': '5%'
        });
        $('#sidebar').css({
            'margin-left': '-15%'
        });
        $('#sidebar').hide();
        $('#sidebar-small').show();
        $('#sidebar-small').css({
            'display': 'block'
        });
        $("#container").addClass("sidebar-closed");
    } else {
        $('#main-content').css({
            'margin-left': '15%'
        });
        $('#sidebar').show();
        $('#sidebar-small').hide();
        $('#sidebar-small').css({
            'display': 'none'
        });
        $('#sidebar').css({
            'margin-left': '0%'
        });
        $("#container").removeClass("sidebar-closed");
    }
};

$('.btn-minimize').click(function () {
    var s1 = $(this).find('i');
    if ($(this).hasClass('collapsed')) {
        $(s1).removeClass('fa-chevron-down');
        $(s1).addClass('fa-chevron-up');
    } else {
        $(s1).removeClass('fa-chevron-up');
        $(s1).addClass('fa-chevron-down');
    }
});
//grid border for all page
function getGridborder(grid)
{
    
 
    var data = grid.dataSource.data();
    $.each(data, function (i, row) {
        $('tr[data-uid="' + row.uid + '"] ').removeClass("k-alt");

        $('tr[data-uid="' + row.uid + '"] ').css("box-sizing", "border-box");
        $('tr[data-uid="' + row.uid + '"] ').css("box-shadow", "#DDDDDD 0px 1px 0px 0px");
    });
}

function RemoveAlternateRowColor(gridName) {
    $("#" + gridName + " tr.k-alt").removeClass("k-alt");
}

//icons js for all page
function GetICons() {
    //$(".k-grid-edit").html("<span class=''></span>").css({ "" });
    $(".k-grid-edit").removeClass("k-button k-button-icontext");
    $(".k-grid-edit").html("<span class=''></span>").addClass("k-grid-button");
    $(".k-grid-edit").find("span").append("<img src='/Areas/BM_Management/img/Edit.png'/>");    

    $(".k-grid-delete").html("<span class=''></span>").addClass("k-grid-button");
    $(".k-grid-delete").removeClass("k-button k-button-icontext");
    ///$(".k-grid-delete").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "10px", "margin-right": "10px" });
    $(".k-grid-delete").find("span").append("<img src='/Areas/BM_Management/img/Delete.png'/>");

    $(".k-grid-download").html("<span class=''></span>").addClass("k-grid-button hoverCircle");
    $(".k-grid-download").removeClass("k-button k-button-icontext");    
    $(".k-grid-download").find("span").append("<img src='/Areas/BM_Management/img/Download.png'/>");

    $(".k-grid-view").html("<span class=''></span>").addClass("k-grid-button hoverCircle");
    $(".k-grid-view").removeClass("k-button k-button-icontext");
    $(".k-grid-view").find("span").append("<img src='/Areas/BM_Management/img/View.png'/>");

    $(".k-grid-overview").html("<span class=''></span>").addClass("k-grid-button hoverCircle");
    $(".k-grid-overview").removeClass("k-button k-button-icontext");
    $(".k-grid-overview").find("span").append("<img src='/Areas/BM_Management/img/View.png'/>");   
    
    $(".k-grid-CS").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-CS").find("span").append("<img src='/Areas/BM_Management/img/AssociateCompany.png' style='height:50%'/>");
    $(".k-grid-CS").removeClass("k-button k-button-icontext");

    $(".k-grid-C").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-C").find("span").append("<img src='/Areas/BM_Management/img/CapitalMaster.png' style='height:24px'/>");
    $(".k-grid-C").removeClass("k-button k-button-icontext");

    $(".k-grid-A").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-A").find("span").append("<img src='/Areas/BM_Management/img/Applicabiityicon.png' style='height:50%'/>");
    $(".k-grid-A").removeClass("k-button k-button-icontext");

    $(".k-grid-S").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-S").find("span").append("<img src='/Areas/BM_Management/img/ShareholdingMaster.png' style='height:50%'/>");
    $(".k-grid-S").removeClass("k-button k-button-icontext");

    $(".k-grid-D").html("<span class=''></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-D").find("span").append("<img src='/Areas/BM_Management/img/BoardofDirectors.png' style='height:50%'/>");
    $(".k-grid-D").removeClass("k-button k-button-icontext");

    $(".k-grid-CM").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-CM").find("span").append("<img src='/Areas/BM_Management/img/Committee.png' style='height:50%'/>");
    $(".k-grid-CM").removeClass("k-button k-button-icontext");

    $(".k-grid-AU").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-AU").find("span").append("<img src='/Areas/BM_Management/img/AuditorMaster.png' style='height:24px'/>");
    $(".k-grid-AU").removeClass("k-button k-button-icontext");
    //
    $(".k-grid-R").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-R").find("span").append("<img src='/Areas/BM_Management/img/Rule.png' style='height:24px'/>");
    $(".k-grid-R").removeClass("k-button k-button-icontext");

    $(".k-grid-info").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    //$(".k-grid-info").find("span").append("<img src='/Areas/BM_Management/img/Info.png' style='height:50%'/>");
    $(".k-grid-info").find("span").append("<img src='/Areas/BM_Management/img/ViewIcon_Grey.png' style='height:90%'/>");
    $(".k-grid-info").removeClass("k-button k-button-icontext");

    $(".k-grid-SM").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-SM").find("span").append("<img src='/Areas/BM_Management/img/Start Meeting_01.png' style='height:90%'/>");
    $(".k-grid-SM").removeClass("k-button k-button-icontext");

    $(".k-grid-documents").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-documents").find("span").append("<img src='/Areas/BM_Management/img/Document.png' style='height:90%'/>");
    $(".k-grid-documents").removeClass("k-button k-button-icontext");

	$(".k-grid-t").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-t").find("span").append("<img src='../../Areas/BM_Management/img/Compliances.png' style='height:24px'/>");
    $(".k-grid-t").removeClass("k-button k-button-icontext");

    $(".k-grid-ainfo").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "33%" });
    $(".k-grid-ainfo").find("span").append("<img src='../../Areas/BM_Management/img/Info.png' style='height:90%'/>");
    $(".k-grid-ainfo").removeClass("k-button k-button-icontext");

    $(".k-grid-compliance").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-compliance").find("span").append("<img src='/Areas/BM_Management/img/Compliances.png' style='height:24px'/>");
    $(".k-grid-compliance").removeClass("k-button k-button-icontext");

    $(".k-grid-Agenda").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-Agenda").find("span").append("<img src='/Areas/BM_Management/img/Document.png' style='height:90%'/>");
    $(".k-grid-Agenda").removeClass("k-button k-button-icontext");

    $(".k-grid-Minutes").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-Minutes").find("span").append("<img src='/Areas/BM_Management/img/Documents.png' style='height:65%'/>");
    $(".k-grid-Minutes").removeClass("k-button k-button-icontext");

    $(".k-grid-setting").html("<span></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px", "margin-left": "6%" });
    $(".k-grid-setting").find("span").append("<img src='/Areas/BM_Management/img/settings.svg' />");
    $(".k-grid-setting").removeClass("k-button k-button-icontext");

    $(".k-grid-document").html("<span class=''></span>").addClass("k-grid-button");
    $(".k-grid-document").removeClass("k-button k-button-icontext");
    $(".k-grid-document").find("span").append("<img style='width:22px;' src='../../Areas/BM_Management/img/Document.png'/>");

    
}


function btnminimize(obj) {
    var s1 = $(obj).find('i');
    if ($(obj).hasClass('collapsed')) {

        $(s1).removeClass('fa-chevron-up');
        $(s1).addClass('fa-chevron-down');
    } else {
        $(s1).removeClass('fa-chevron-down');
        $(s1).addClass('fa-chevron-up');
    }
}

function PositionTooltip(e) {    
    var position = e.sender.options.position;
    if (position == "bottom") {
        e.sender.popup.element.css("margin-top", "10px");
    } else if (position == "top") {
        e.sender.popup.element.css("margin-bottom", "10px");
    }
}

function GetGridColumnValues(gridName, columnName) {

    
    const map = new Map();

    var columnDataVector = [];
    var grid = $('#' + gridName);
    if ($('#' + gridName) != null && $('#' + gridName) != undefined) {
        //var gridDataArray = grid.data('kendoGrid')._data;
        var gridDataArray = grid.data('kendoGrid').dataSource.dataFiltered();

        if (gridDataArray != null && gridDataArray != undefined) {
            var columnName = columnName;
            for (var index = 0; index < gridDataArray.length; index++) {
                if (!map.has(gridDataArray[index][columnName]) && gridDataArray[index][columnName] != null) {
                    map.set(gridDataArray[index][columnName], true);
                    columnDataVector[index] = gridDataArray[index][columnName];
                }
            };
        }
    }

    return columnDataVector;
}

function GetListViewColumnValues(gridName, columnName) {
    debugger;
    const map = new Map();

    var columnDataVector = [];
    var grid = $('#' + gridName);
    if ($('#' + gridName) != null && $('#' + gridName) != undefined) {
        //var gridDataArray = grid.data('kendoGrid')._data;
        var gridDataArray = grid.data('kendoListView').dataSource.dataFiltered();

        if (gridDataArray != null && gridDataArray != undefined) {
            var columnName = columnName;
            for (var index = 0; index < gridDataArray.length; index++) {
                if (!map.has(gridDataArray[index][columnName]) && gridDataArray[index][columnName] != null) {
                    map.set(gridDataArray[index][columnName], true);
                    columnDataVector[index] = gridDataArray[index][columnName];
                }
            };
        }
    }

    return columnDataVector;
}

var canExpandCollapse = true;
function cancelExpandCollapse(e) {
    if (canExpandCollapse) {
        e.preventDefault();
        canExpandCollapse = true;
    }
}

function KendoGrid_GetToolTipContent(e) {
    return e.target.context.innerText;
}

function DisplayNoResultsFound(grid, msg) {
    

    // Get the number of Columns in the grid
    var dataSource = grid.data("kendoGrid").dataSource;
    var colCount = grid.find('.k-grid-header colgroup > col').length;

    // If there are no results place an indicator row
    if (dataSource._view.length == 0) {
        grid.find('.k-grid-norecords').html("<div class='col-md-12 colpadding0 norecords'>" +
        "<div><img src='/Areas/BM_Management/img/nodata.svg' /></div>" +
        "<div class='clearfix'></div>" +
        "<div><label class='box-header'>" + msg + "</label></div>" +
        "</div>");
    }
}