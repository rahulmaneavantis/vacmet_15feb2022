﻿
$(document).ready(function () {
    fmenustyle();
});

$('.btn-minimize').click(function () {
    var s1 = $(this).find('i');
    if ($(this).hasClass('collapsed')) {
        $(s1).removeClass('fa-chevron-down');
        $(s1).addClass('fa-chevron-up');
    } else {
        $(s1).removeClass('fa-chevron-up');
        $(s1).addClass('fa-chevron-down');
    }
});
//icons js for all page
function GetICons() {
    $(".k-grid-edit").html("<span class='fa fa-pencil fa-fw'></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px" });
    $(".k-grid-delete").html("<span class='fa fa-times fa-fw'></span>").css({ "min-width": "37px", "min-height": "36px", "font-size": "13px", "padding": "0px 0px 0px 0px" });
}

function btnminimize(obj) {
    var s1 = $(obj).find('i');
    if ($(obj).hasClass('collapsed')) {

        $(s1).removeClass('fa-chevron-up');
        $(s1).addClass('fa-chevron-down');
    } else {
        $(s1).removeClass('fa-chevron-down');
        $(s1).addClass('fa-chevron-up');
    }
}

