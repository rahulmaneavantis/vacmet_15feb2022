﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailUnsubscribe.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.MailUnsubscribe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />
    
    <title>Unsubscribe  :: AVANTIS - Products that simplify</title>  
    <!-- Bootstrap CSS -->
     <link href="NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="NewCSS/bootstrap-theme.css" rel="stylesheet" />             
    <!--external css-->
    <!-- font icon -->
    <link href="NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="NewCSS/style.css" rel="stylesheet" />
    <link href="NewCSS/style-responsive.css" rel="stylesheet" />

    
    <style type="text/css">
        .otpdiv > span > label {
            border: 0px;
            color: black;
            /* margin-bottom: 5px; */
            margin-left: -17px;
            margin-top: 2px;
            position: absolute;
            width: 111px;

        }
      .otpdiv > span  {
            width: 233px;
            height: 26px;
            margin: auto;
            position: relative;
            /* background: #fff; */
            /* border: 1px solid #2e2e2e; */
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
        }
        .otpdiv > span > input {
            width: 80px;
            height: 26px;
            margin: auto;
            position: relative;           
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            margin-left: -9px;
        }
    </style>

    
    <style>
        /* Three image containers (use 25% for four, and 50% for two, etc) */

        /* Clear floats after image containers */
        .row::after {
            content: "";
            clear: both;
            display: table;
        }



        .spanDescription {
            text-align: center;
            font-weight: 700;
            font-size: 16px;
        }


        .col-md-4.img-set {
            border-right: 1px solid #aaa;
            padding: 10px;
            text-align: center;
            /*line-height: 100px;*/
                min-height: 159px; /*190*/
        }

        .keychallenge {
            border: 1px solid silver;
            /*border-radius: 10px;
            padding: 10px;*/
            margin-bottom: 24px;
        }

            .keychallenge .title {
                background-color: #1fd9e1;
                text-align: left;
                line-height: 25px;
                padding: 5px;
                border-bottom: 1px solid #aaa;
                margin: 0;
            }

        .col-md-8.span-desc {
            padding: 10px;
        }

        .k-button {
            float: right;
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .headerdiv {
            background-color: #1fd9e1;
            height: 63px;
            padding-top: 9px;
            font-weight: 600;
            font-size: 16px;
            color: white
        }

        .welcomeheaderdiv {
            float: right;
            padding-top: 9px;
            background-color: #1fd9e1;
            color: white;
            font-size: 14px;
        }
        .profile-ava img {
    border-radius: 50%;
    -webkit-border-radius: 50%;
    display: inline-block;
}
        .rowheight {
                /*max-height: 144px;*/
        }

        .login-form h1{
            text-align:center;
        }
    </style>
    <script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92029518-1', 'auto');
  ga('send', 'pageview');

  function settracknew(e, t, n, r) {
      try {
          ga('send', 'event', e, t, n + "#" + r)
      } catch (t) { } return !0
  }

  function settracknewnonInteraction(e, t, n, r) {
      ga('send', 'event', e, t, n, 0, { 'nonInteraction': 1 })
  }

  settracknewnonInteraction("Login", 'Login', "Page", "0");
  if (window.parent.document.getElementById('Ishome')!=null) {
      window.parent.location.href = '/login.aspx';
  }
</script>
</head>
<body>
 
    <form runat="server"  name="Report">

        <div class="col-md-12">
            <div class="col-md-1 col-sm-1 col-xs-1" style="height: 63px;">
                <img src="../Images/avantil-logo.png" style="height: 60px;">
            </div>

            <div class="col-md-11 col-sm-11 col-xs-11 headerdiv">
                <div style="float: right">
                    <span class="profile-ava">
                        <img src="../img/profile_pic.png" id="ProfilePic" runat="server" style="height: 50px; width: 50px;"/>
                    </span>
                    <span class="username">
                        <asp:Label ID="LabelUserName" runat="server" Style="color: white"></asp:Label></span>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <asp:ScriptManager ID="ScriptManager2" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="login-form">
                            <div class="col-md-12 login-form-head">
                                <p class="login-img">
                                    <a href="https://teamleaseregtech.com">
                                        <img src="Images/TeamLease1.png" /></a>
                                </p>
                            </div>

                            <div class="login-wrap">
                                <h1>Unsubscribe Compliance Updates</h1>
                                <div class="col-md-12" style="margin-top: 15px; text-align:justify;">
                                    <span style="font-family: 'Roboto',san-serif; color: #555555; font-size: 13.5px;">We are sorry to see you unsubscribe to our Compliance Updates. </span>
                                    <div class="clearfix"></div>
                                     <span style="font-family: 'Roboto',san-serif; color: #555555; font-size: 13.5px;">Are you sure, you want to continue to unsubscribe?</span>
                                </div>
                                     <div class="clearfix"></div>

                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 15px;">
                                        <div class="col-md-6">
                                            <asp:Button ID="btnConfirm" CssClass="btn btn-primary btn-lg btn-block" Text="Confirm " runat="server" OnClick="btnConfirm_Click"></asp:Button>
                                        </div>
                                        <%--<div class="col-md-6">
                                            <asp:Button ID="btnCancel" CssClass="btn btn-primary btn-lg btn-block" Text="Cancel" runat="server" OnClick="btnCancel_Click"></asp:Button>
                                        </div>--%>
                                    </div>
                                </div>

                                <div class="clearfix" style="height: 10px"></div>

                                <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-success fade in" EnableClientScript="False" runat="server" Display="None" />
                                <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-success fade in" runat="server" />
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnConfirm" />
                    </Triggers>
                </asp:UpdatePanel>
      
        
    </form>
</body>
</html>
