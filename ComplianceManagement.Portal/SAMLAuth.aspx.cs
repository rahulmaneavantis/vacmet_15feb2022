﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using AjaxControlToolkit;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Data.Services.Client;
using System.Threading.Tasks;
using System.Security.Claims;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class SAMLAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["ID"] != null)
                    {
                        var custRecord = SAMLManagement.CheckCustomerExists(Convert.ToInt32(Request.QueryString["ID"]));
                        if (custRecord != null)
                        {
                            //AppID - 4406f761-8c47-474e-8645-662714fa10bd
                            //tenantId=c0408565-7970-454d-be62-a8fcd708eb27
                            Response.Redirect(@"https://myapps.microsoft.com/signin/" + custRecord.IdentifierEntityID_idpid + "/" + custRecord.AppID + "?tenantId=" + custRecord.TenantID, false);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}