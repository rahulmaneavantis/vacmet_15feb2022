﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SSOLogin.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.SSOLogin" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />

    <title>SSO Login :: AVANTIS - Products that simplify</title>
    <!-- Bootstrap CSS -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="https://avacdn.azureedge.net/newcss/elegant-icons-style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="https://avacdn.azureedge.net/newcss/style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/style-responsive.css" rel="stylesheet" />

    <style type="text/css">
        .otpdiv > span > label {
            border: 0px;
            color: black;
            /* margin-bottom: 5px; */
            margin-left: -17px;
            margin-top: 2px;
            position: absolute;
            width: 111px;
        }

        .otpdiv > span {
            width: 233px;
            height: 26px;
            margin: auto;
            position: relative;
            /* background: #fff; */
            /* border: 1px solid #2e2e2e; */
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
        }

        .otpdiv > span > input {
            width: 80px;
            height: 26px;
            margin: auto;
            position: relative;
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            margin-left: -9px;
        }
    </style>
    <script type="text/javascript">

        var _paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function () {
            var u = "//analytics.avantis.co.in/";
            _paq.push(['setTrackerUrl', u + 'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
        })();

        function settracknew(e, t, n, r) {
            debugger;
            try {
                _paq.push(['trackEvent', e, t, n])
            } catch (t) { } return !0
        }

        function settracknewForSuccess() {
            settracknew('SSOLogin', 'SSOLogin', 'Success', '');
        }

        function settracknewForFailed() {
            settracknew('SSOLogin', 'SSOLogin', 'Failed', '');
        }

        function fclearcookie() {
            window.location.href = "/logout.aspx"
            return false;
        }
       
    </script>
</head>
<body>
    <div class="container">
        <%--<form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">--%>
        <form runat="server" class="login-form" name="login">
            <asp:Panel ID="Panel2" runat="server" DefaultButton="Submit">

                <asp:ScriptManager ID="ScriptManager2" runat="server" />
                <cc2:NoBot ID="PageNoBot" runat="server" Enabled="true" ResponseMinimumDelaySeconds="0" />
                <%--CutoffMaximumInstances="1" CutoffWindowSeconds="5"--%>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">
                                <a href="https://teamleaseregtech.com">
                                    <img src="Images/TeamLease1.png" /></a>
                            </p>
                        </div>

                        <div class="login-wrap">
                            <div id="divLogin" class="row" runat="server">
                                <h1>Login with SSO</h1>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                                    <asp:TextBox ID="txtemail" CssClass="form-control" runat="server" placeholder="Username/Email" data-toggle="tooltip" MaxLength="100" data-placement="right" ToolTip="Please enter Username/Email" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ErrorMessage="Username/Email cannot be empty"
                                        ControlToValidate="txtemail" runat="server" />
                                </div>

                                <div class="clearfix"></div>

                                <div>
                                    <asp:Button ID="Submit" CssClass="btn btn-primary btn-lg btn-block" Text="Proceed" runat="server" OnClick="Submit_Click" ></asp:Button>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div style="margin-top: 3%;">
                                <asp:LinkButton ID="lnkLogOut" OnClientClick="javascript:return fclearcookie();" runat="server">Sign in as a different user</asp:LinkButton>
                            </div>

                            <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" />
                            <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" />
                        </div>

                        <div style="clear: both; height: 10px; background: #eeeeee;"></div>
                        
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="Submit" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <!--js-->
    
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.js"></script>
    <script type='text/javascript' src="https://avacdn.azureedge.net/newjs/bootstrap.min.js"></script>
    
    <script type="text/javascript">
        
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
    </script>

</body>
</html>

