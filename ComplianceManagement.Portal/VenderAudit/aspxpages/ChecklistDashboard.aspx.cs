﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Business.VenderAudits;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.aspxpages
{
    public partial class CheckListDashboard : System.Web.UI.Page
    {
        protected int VendorIDBCK;
        protected int AuditIDBCK;
        protected int ddlAuditTypeBCK;
        protected int ddlPageSizeBCK;
        protected string location;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLocationFilter();
                ddlAuditType.SelectedValue = "-1";

                ViewState["AuditID"] = Request.QueryString["AuditID"];

                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["AuditID"])))               
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ddlVendorDRP"])))
                    {
                        VendorIDBCK = Convert.ToInt32(Request.QueryString["ddlVendorDRP"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ddlAuditDRP"])))
                    {
                        AuditIDBCK = Convert.ToInt32(Request.QueryString["ddlAuditDRP"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ddlAuditTypeDRP"])))
                    {
                        ddlAuditTypeBCK = Convert.ToInt32(Request.QueryString["ddlAuditTypeDRP"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["ddlPageSizeDRP"])))
                    {
                        ddlPageSizeBCK = Convert.ToInt32(Request.QueryString["ddlPageSizeDRP"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Location"])))
                    {
                        location = Convert.ToString(Request.QueryString["Location"]);
                    }

                    var data = VenderAuditManagment.GetAuditDetailByID(Convert.ToInt64(ViewState["AuditID"]));
                    AuditName.Text = data.AuditName;
                    if (data.AuditCloseFlag != true)
                    {
                        ViewState["AuditFlag"] = "False";
                    }
                    else
                    {
                        ViewState["AuditFlag"] = "True";
                    }
                }
               
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ID";
                long AuditorID = AuthenticationHelper.UserID;
                BindAuditCheckList(Convert.ToInt32(ViewState["AuditID"]));

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = "S";
               
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindAuditCheckList(long AuditDetailID)
        {
            try
            {
                grdChecklist.DataSource = null;
                grdChecklist.DataBind();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CustomerBranchID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var auditDetail = (from row in entities.AuditCheckListMappings
                                       join row1 in entities.AuditDetails on row.AuditDetailID equals row1.ID
                                       join row2 in entities.AuditChecklists on row.ChecklistID equals row2.ID
                                       join row3 in entities.AuditCategories on row2.CategoryID equals row3.ID
                                       where row.Isactive == false && row1.CustomerID == CustomerBranchID
                                       && row1.ID == AuditDetailID
                                       select new
                                       {
                                           CheckListMappingID = row.ID
                                                    ,
                                           AuditDetailID = row1.ID
                                                    ,
                                           CheckListID = row2.ID
                                                    ,
                                           CheckListName = row2.Name
                                                    ,
                                           CategoryName = row3.Name
                                                    ,
                                           SequenceID = row2.SequanceID
                                                    ,
                                           SrNo = row2.SrNo
                                                    ,
                                           //SrNo1 = row2.SrNo != null ? Convert.ToDecimal(row2.SrNo) : 0
                                           //          ,
                                           Observation = row.Observation
                                                    ,
                                           AuditStartDate = row1.AuditStartDate
                                               ,
                                           AuditEndDate = row1.AuditEndDate
                                                 
                                      });

                    var AuditDetail = auditDetail.OrderBy(x => x.SequenceID).ToList();

                    //var AuditDetail = auditDetail.OrderBy(x => decimal.Parse(x.SrNo)).ToList();

                    //var AuditDetail = auditDetail.OrderBy(o => o.SrNo.Length).ThenBy(o => o.SrNo).ToList();


                    grdChecklist.DataSource = AuditDetail;
                    grdChecklist.DataBind();

                    Session["TotalRows"] = AuditDetail.Count;
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindAuditComplianceDetails(long AuditDetailID,long ChecklistID,DateTime fromdate,DateTime Todate,string Type)
        {
            try
            {
                grdDetails.DataSource = null;
                grdDetails.DataBind();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                      var AuditDetail = (from row in entities.SP_GetAuditChecklistLicenceDetail(AuditDetailID, ChecklistID,fromdate,Todate,Type)
                                  select row).ToList();

                    grdDetails.DataSource = AuditDetail;
                    grdDetails.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            long AuditorID = AuthenticationHelper.UserID;
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        protected void grdChecklist_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                string observation = Request[((TextBox)grdChecklist.Rows[e.RowIndex].FindControl("txtObservation")).UniqueID].ToString();
                int AuditDetailID = Convert.ToInt32((grdChecklist.Rows[e.RowIndex].FindControl("lblAuditDetailID") as Label).Text);
                int ChecklistMappingID = Convert.ToInt32((grdChecklist.Rows[e.RowIndex].FindControl("lblChecklistMappingID") as Label).Text);

                if (observation != "")
                {
                    AuditCheckListMapping auditCheckListMapping = new AuditCheckListMapping()
                    {
                        AuditDetailID = AuditDetailID,
                        ID = ChecklistMappingID,
                        Observation = observation,
                        UpdatedBy =AuthenticationHelper.UserID,
                        UpdatedOn =DateTime.Now
                    };
                    Business.VenderAudits.VenderAuditManagment.UpdateObservation(auditCheckListMapping);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Observation saved successfully')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void btnSaveDraft_Click(object sender, EventArgs e)
        {
            try
            {
                int flag = 0;
                foreach (GridViewRow item in grdChecklist.Rows)
                {
                    CheckBox chkAssign = (CheckBox)item.FindControl("CheckBox1");
                    if (chkAssign.Checked)
                    {
                        flag = 1;
                        Label lblAuditDetailId = (Label)item.FindControl("lblAuditDetailID");
                        int AuditId = Convert.ToInt32(lblAuditDetailId.Text);

                        Label lblChecklistMappingID = (Label)item.FindControl("lblChecklistMappingID");
                        int ChecklistMappingID = Convert.ToInt32(lblChecklistMappingID.Text);

                        TextBox txtObservation = (TextBox)item.FindControl("txtObservation");
                        string Observation = txtObservation.Text;

                        if (!string.IsNullOrEmpty(Observation))
                        {
                            AuditCheckListMapping auditCheckListMapping = new AuditCheckListMapping()
                            {
                                AuditDetailID = AuditId,
                                ID = ChecklistMappingID,
                                Observation = Observation,
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now
                            };
                            Business.VenderAudits.VenderAuditManagment.UpdateObservation(auditCheckListMapping);
                        }
                    }
                }
                if (flag > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Observation saved successfully')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Please select at least one Audit Checklist')", true);
                }
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Observation saved successfully')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            try
            {
                int AuditId = -1;
                foreach (GridViewRow item in grdChecklist.Rows)
                {
                    Label lblAuditDetailId = (Label)item.FindControl("lblAuditDetailID");
                    AuditId = Convert.ToInt32(lblAuditDetailId.Text);

                    CheckBox chkAssign = (CheckBox)item.FindControl("CheckBox1");
                    if (chkAssign.Checked)
                    {
                        Label lblChecklistMappingID = (Label)item.FindControl("lblChecklistMappingID");
                        int ChecklistMappingID = Convert.ToInt32(lblChecklistMappingID.Text);

                        TextBox txtObservation = (TextBox)item.FindControl("txtObservation");
                        string Observation = txtObservation.Text;

                        if (!string.IsNullOrEmpty(Observation))
                        {
                            AuditCheckListMapping auditCheckListMapping = new AuditCheckListMapping()
                            {
                                AuditDetailID = AuditId,
                                ID = ChecklistMappingID,
                                Observation = Observation,
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now
                            };
                            Business.VenderAudits.VenderAuditManagment.UpdateObservation(auditCheckListMapping);
                        }
                    }
                }
                if (AuditId != -1)
                {
                    Business.VenderAudits.VenderAuditManagment.UpdateAuditDetail(Convert.ToInt64(AuditId));
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Observation Save and Closed successfully')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdChecklist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("Download"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long CheckListID = Convert.ToInt64(commandArgs[0]);
                    long CheckListMappingID = Convert.ToInt64(commandArgs[1]);

                    var AuditSampleData = Business.VenderAudits.VenderAuditManagment.GetAuditSampleDocumentData(CheckListID);
                    if (AuditSampleData.Count > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            int i = 0;
                            foreach (var file in AuditSampleData)
                            {
                                if (file.FilePath != null && File.Exists(Server.MapPath(file.FilePath)))
                                {
                                    string ext = Path.GetExtension(file.Name);
                                    string[] filename = file.Name.Split('.');
                                    //string str = filename[0] + i + "." + filename[1];
                                    string str = filename[0] + i + "." + ext;
                                    ComplianceZip.AddEntry(str, Business.DocumentManagement.ReadDocFiles(Server.MapPath(file.FilePath)));
                                    i++;
                                }
                            }
                            string fileName = "CheckListID_" + CheckListID + "_SampleForms.zip";
                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                    }
                }
                else if (e.CommandName.Equals("View"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long CheckListID = Convert.ToInt64(commandArgs[0]);
                    long CheckListMappingID = Convert.ToInt64(commandArgs[1]);

                    // var AuditSampleData = Business.VenderAudits.VenderAuditManagment.GetAuditSampleDocumentData(CheckListID);

                    // var AuditDetails = VenderAuditManagment.GetAuditData(CheckListID);
                    var AuditDetails = Business.VenderAudits.VenderAuditManagment.AuditDocument(CheckListID);
                    rptComplianceSampleView.DataSource = AuditDetails;
                    rptComplianceSampleView.DataBind();

                    AuditSampleDocumentData CMPDocuments = Business.VenderAudits.VenderAuditManagment.GetLatestAuditSampleDocumentData(CheckListID);

                    if (CMPDocuments != null)
                    {
                        string fullfilePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath));
                        string filePath = CMPDocuments.FilePath;
                        string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                        if (CMPDocuments.FilePath != null && File.Exists(fullfilePath))
                        {
                            string extension = System.IO.Path.GetExtension(CompDocPath);

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocPath + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                    }

                }
                else if (e.CommandName.Equals("ViewRecord"))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int flag = 0;
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        long AuditDetailID = Convert.ToInt64(commandArgs[0]);
                        long CheckListMappingID = Convert.ToInt64(commandArgs[1]);
                        DateTime AuditStartDate = Convert.ToDateTime(commandArgs[2]);
                        DateTime AuditEndDate = Convert.ToDateTime(commandArgs[3]);
                        string CheckListName = Convert.ToString(commandArgs[4]);

                        ViewState["AuditStartDate"] = AuditStartDate;
                        ViewState["AuditEndDate"] = AuditEndDate;

                        var AuditDetail = AuditName;
                        var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType(AuditDetailID, CheckListMappingID);
                        
                        if (ComplianceType == "S")
                        {
                            #region Statutory
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditDetailID, CheckListMappingID, AuditStartDate, AuditEndDate, "S", AuthenticationHelper.CustomerID,"").ToList();
                                var DistinctComplianceInstanceIDList = MasterComplianceDocument.Select(o => o.ComplianceInstanceID).Distinct().ToList();
                                rptComplianceSampleViewRecord.DataSource = MasterComplianceDocument;
                                rptComplianceSampleViewRecord.DataBind();

                                if (DistinctComplianceInstanceIDList.Count > 0)
                                {
                                    var CMPDocuments = MasterComplianceDocument.ToList().FirstOrDefault();
                                    if (CMPDocuments != null)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath), CMPDocuments.FileKey + Path.GetExtension(CMPDocuments.FileName));
                                        string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                                        if (CMPDocuments.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                            string DateFolder = Folder + "/" + File;
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileRecord();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }
                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                                string FileName = DateFolder + "/" + User + "" + extension;
                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (CMPDocuments.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                CompDocPath = FileName;
                                                CompDocPath = CompDocPath.Substring(2, CompDocPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFileRecord('" + CompDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileRecord();", true);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                                }
                            }
                            #endregion
                        }
                        else  //Internal
                        {
                            #region Internal
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditDetailID, CheckListMappingID, AuditStartDate, AuditEndDate, "I", AuthenticationHelper.CustomerID, "").ToList();

                                var DistinctComplianceInstanceIDList = MasterComplianceDocument.Select(o => o.ComplianceInstanceID).Distinct().ToList();

                                rptComplianceSampleViewRecord.DataSource = MasterComplianceDocument;
                                rptComplianceSampleViewRecord.DataBind();

                                if (DistinctComplianceInstanceIDList.Count > 0)
                                {
                                    var CMPDocuments = MasterComplianceDocument.ToList().FirstOrDefault();

                                    if (CMPDocuments != null)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath), CMPDocuments.FileKey + Path.GetExtension(CMPDocuments.FileName));
                                        string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                                        if (CMPDocuments.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileRecord();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (CMPDocuments.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                CompDocPath = FileName;
                                                CompDocPath = CompDocPath.Substring(2, CompDocPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFileRecord('" + CompDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileRecord();", true);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                                    }
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                                }
                            }
                            #endregion
                        }
                    }

                }
                if (e.CommandName.Equals("DownloadRecord"))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int flag = 0;
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        long AuditDetailID = Convert.ToInt64(commandArgs[0]);
                        long CheckListMappingID = Convert.ToInt64(commandArgs[1]);
                        DateTime AuditStartDate = Convert.ToDateTime(commandArgs[2]);
                        DateTime AuditEndDate = Convert.ToDateTime(commandArgs[3]);
                        string CheckListName = Convert.ToString(commandArgs[4]);
                        var AuditDetail = AuditName;
                        var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType(AuditDetailID, CheckListMappingID);
                        //ComplianceType = "S";


                        string checkListNamestring = "";
                        var checkList_Name = "";
                        if (CheckListName.Length > 100)
                        {
                            checkList_Name = Regex.Replace(CheckListName, "[^a-zA-Z0-9.]+", "_", RegexOptions.Compiled);
                            checkListNamestring = checkList_Name.Substring(0, 100);
                            checkListNamestring = checkListNamestring.Trim('_');
                        }
                        else
                        {
                            checkList_Name = Regex.Replace(CheckListName, "[^a-zA-Z0-9.]+", "_", RegexOptions.Compiled);
                            checkListNamestring = checkList_Name;
                            checkListNamestring = checkListNamestring.Trim('_');
                        }

                        if (ComplianceType == "S")
                        {
                            #region Statutory
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditDetailID, CheckListMappingID, AuditStartDate, AuditEndDate, "S",AuthenticationHelper.CustomerID, "").ToList();

                                var DistinctComplianceInstanceIDList = MasterComplianceDocument.Select(o => o.ComplianceInstanceID).Distinct().ToList();
                                if (DistinctComplianceInstanceIDList.Count > 0)
                                {
                                    int j = 1;
                                    foreach (var item in DistinctComplianceInstanceIDList)
                                    {
                                        var ComplianceDocument = MasterComplianceDocument.Where(t => t.ComplianceInstanceID == item).ToList();

                                        var Compliance = Business.VenderAudits.VenderAuditManagment.GetComplianceFromInstance(Convert.ToInt64(item));
                                        var ShortDescription = "";
                                        string shortstring = "";
                                        if (Compliance != null)
                                        {
                                            ShortDescription = Regex.Replace(Compliance.ShortDescription, "[^a-zA-Z0-9.]+", "_", RegexOptions.Compiled);
                                            if (ShortDescription.Length > 100)
                                            {
                                                shortstring = ShortDescription.Substring(0, 100);
                                                shortstring = shortstring.Trim('_');
                                                shortstring = shortstring + "...";
                                            }
                                            else
                                            {
                                                shortstring = ShortDescription.Trim('_');
                                            }
                                            shortstring = shortstring + " " + j;
                                        }
                                        if (ComplianceDocument.Count > 0)
                                        {
                                            int i = 0;
                                            foreach (var file in ComplianceDocument)
                                            {
                                                flag = 1;
                                                try
                                                {
                                                    if(file.ForMonth.Length > 0)
                                                    {
                                                        ComplianceZip.AddDirectoryByName(file.Branch + "/" + shortstring + "/" + file.ForMonth);
                                                    }
                                                    else
                                                    {
                                                        ComplianceZip.AddDirectoryByName(file.Branch + "/" + shortstring);
                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                }
                                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    string ext = Path.GetExtension(file.FileName);
                                                    string[] filename = file.FileName.Split('.');                                                    
                                                    string str = filename[0] + i + "." + ext;

                                                    if (file.ForMonth.Length > 0)
                                                    {
                                                        if (file.EnType == "M")
                                                        {
                                                            ComplianceZip.AddEntry(file.Branch + "/" + shortstring + "/" + file.ForMonth + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                        else
                                                        {
                                                            ComplianceZip.AddEntry(file.Branch + "/" + shortstring + "/" + file.ForMonth + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (file.EnType == "M")
                                                        {
                                                            ComplianceZip.AddEntry(file.Branch + "/" + shortstring + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                        else
                                                        {
                                                            ComplianceZip.AddEntry(file.Branch + "/" + shortstring + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                    }                                                    
                                                    i++;
                                                }
                                            }
                                        }
                                        j++;
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();

                                    Response.Buffer = true;

                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    
                                    Response.AddHeader("content-disposition", "attachment; filename="+ checkListNamestring + ".zip");
                                    //Response.AddHeader("content-disposition", "attachment; filename=ChecklistComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                                }
                            }
                            #endregion
                        }
                        else  //Internal
                        {
                            #region Internal
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditDetailID, CheckListMappingID, AuditStartDate, AuditEndDate, "I",AuthenticationHelper.CustomerID, "").ToList();

                                var DistinctComplianceInstanceIDList = MasterComplianceDocument.Select(o => o.ComplianceInstanceID).Distinct().ToList();
                                int j = 1;

                                if (DistinctComplianceInstanceIDList.Count > 0)
                                {
                                    foreach (var item in DistinctComplianceInstanceIDList)
                                    {
                                        var ComplianceDocument = MasterComplianceDocument.Where(t => t.ComplianceInstanceID == item).ToList();

                                        var Compliance = Business.VenderAudits.VenderAuditManagment.GetInternalComplianceFromInstance(Convert.ToInt64(item));

                                        var ShortDescription = "";
                                        string shortstring = "";
                                        if (Compliance != null)
                                        {
                                            ShortDescription = Regex.Replace(Compliance.IShortDescription, "[^a-zA-Z0-9.]+", "_", RegexOptions.Compiled);
                                            if (ShortDescription.Length > 100)
                                            {
                                                shortstring = ShortDescription.Substring(0, 100);
                                                shortstring = shortstring.Trim('_');
                                                shortstring = shortstring + "...";
                                            }
                                            else
                                            {
                                                shortstring = ShortDescription.Trim('_');
                                            }

                                            shortstring = shortstring + " " + j;
                                        }
                                        
                                        if (ComplianceDocument.Count > 0)
                                        {
                                            int i = 0;
                                            foreach (var file in ComplianceDocument)
                                            {
                                                flag = 1;
                                                try
                                                {
                                                    if (file.ForMonth.Length > 0)
                                                    {
                                                        ComplianceZip.AddDirectoryByName(file.Branch + "/" + shortstring + "/" + file.ForMonth);
                                                    }
                                                    else
                                                    {
                                                        ComplianceZip.AddDirectoryByName(file.Branch + "/" + shortstring);
                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    string ext = Path.GetExtension(file.FileName);
                                                    string[] filename = file.FileName.Split('.');
                                                    //string str = filename[0] + i + "." + filename[1];
                                                    string str = filename[0] + i + "." + ext;
                                                    if (file.ForMonth.Length > 0)
                                                    {
                                                        if (file.EnType == "M")
                                                        {
                                                            ComplianceZip.AddEntry(file.Branch + "/" + shortstring + "/" + file.ForMonth + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                        else
                                                        {
                                                            ComplianceZip.AddEntry(file.Branch + "/" + shortstring + "/" + file.ForMonth + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (file.EnType == "M")
                                                        {
                                                            ComplianceZip.AddEntry(file.Branch + "/" + shortstring + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                        else
                                                        {
                                                            ComplianceZip.AddEntry(file.Branch + "/" + shortstring + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                        }
                                                    }
                                                    
                                                    i++;
                                                }
                                            }
                                        }
                                        j++;
                                    }
                                    var zipMs = new MemoryStream();
                                    ComplianceZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] data = zipMs.ToArray();

                                    Response.Buffer = true;

                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=" + checkListNamestring + ".zip");
                                    //Response.AddHeader("content-disposition", "attachment; filename=ChecklistComplianceDocuments.zip");
                                    Response.BinaryWrite(data);
                                    Response.Flush();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                                }
                            }
                            #endregion
                        }
                        if (flag == 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                        }
                    }
                }
                if (e.CommandName.Equals("ViewDetails"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long AuditDetailID = Convert.ToInt64(commandArgs[0]);
                    long CheckListMappingID = Convert.ToInt64(commandArgs[1]);
                    DateTime AuditStartDate = Convert.ToDateTime(commandArgs[2]);
                    DateTime AuditEndDate = Convert.ToDateTime(commandArgs[3]);
                    string CheckListName = Convert.ToString(commandArgs[4]);
                    var AuditDetail = AuditName;
                    var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType1(AuditDetailID, CheckListMappingID);

                    BindAuditComplianceDetails(AuditDetailID, CheckListMappingID, AuditStartDate, AuditEndDate, ComplianceType);
                    upDetails.Update();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDetails();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    AuditSampleDocumentData CMPDocuments = Business.VenderAudits.VenderAuditManagment.GetSelectedAuditSampleDocumentData(Convert.ToInt64(commandArgs[0]), Convert.ToInt64(commandArgs[1]));

                    if (CMPDocuments != null)
                    {
                        string fullfilePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath));
                        string filePath = CMPDocuments.FilePath;
                        string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                        if (CMPDocuments.FilePath != null && File.Exists(fullfilePath))
                        {
                            string extension = System.IO.Path.GetExtension(CompDocPath);

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocPath + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                        }
                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView);
            }
        }

        protected void lblSampleView_Click(object sender,EventArgs e)
        {
            // LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
            //string labelvalue= lblSampleView


            //var AuditDetails = VenderAuditManagment.GetAuditData(CheckListID);
            //rptComplianceSampleView.DataSource = AuditDetails;
            //rptComplianceSampleView.DataBind();
            
        }



        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
                grdChecklist.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdChecklist.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindAuditCheckList(Convert.ToInt32(ViewState["AuditID"]));
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdChecklist.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdChecklist.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindAuditCheckList(Convert.ToInt32(ViewState["AuditID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdChecklist.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdChecklist.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindAuditCheckList(Convert.ToInt32(ViewState["AuditID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                FileUpload file = (FileUpload)gvr.FindControl("docFileUpload");

                Label CheKlistID = (Label)gvr.FindControl("lblChecklistID");

                if (file != null)
                {
                    if (file.HasFile)
                    {
                        AuditSampleDocumentData Obj1 = new AuditSampleDocumentData()
                        {
                            ChecklistID = Convert.ToInt32(CheKlistID.Text),
                            Name = file.FileName,
                            FileData = file.FileBytes,
                            FilePath = "~/VenderAudit/SampleDocument/" + file.FileName,
                            CreatedBy = Portal.Common.AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            IsDeleted = false
                        };
                        CreateSampleFile(Obj1);
                        file.SaveAs(Server.MapPath("~/VenderAudit/SampleDocument/" + file.FileName));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void CreateSampleFile(AuditSampleDocumentData form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                form.CreatedOn = DateTime.Now;
                entities.AuditSampleDocumentDatas.Add(form);
                entities.SaveChanges();
            }

        }

        protected void checkAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdChecklist.HeaderRow.FindControl("checkAll");
            foreach (GridViewRow row in grdChecklist.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("CheckBox1");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }
        }

        protected void grdChecklist_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox checkAll = (CheckBox)grdChecklist.HeaderRow.FindControl("checkAll");                  
                    CheckBox CheckBox1 = (CheckBox)e.Row.FindControl("CheckBox1");
                    TextBox txtObservation = (TextBox)e.Row.FindControl("txtObservation");
                    Label lblChecklistMappingID = (Label)e.Row.FindControl("lblChecklistMappingID");
                    Label lblAuditDetailID = (Label)e.Row.FindControl("lblAuditDetailID");
                    Label lblChecklistID = (Label)e.Row.FindControl("lblChecklistID");
                    Button btn_Update = (Button)e.Row.FindControl("btn_Update");
                    Button UploadDocument = (Button)e.Row.FindControl("UploadDocument");
                    Label lblAuditStartDate = (Label)e.Row.FindControl("lblAuditStartDate");
                    Label lblAuditEndDate = (Label)e.Row.FindControl("lblAuditEndDate");

                    DateTime AuditStartDate = Convert.ToDateTime(lblAuditStartDate.Text);
                    DateTime AuditEndDate = Convert.ToDateTime(lblAuditEndDate.Text);
                    LinkButton lbkDownload = (LinkButton)e.Row.FindControl("lbkDownload");
                    LinkButton lbkView = (LinkButton)e.Row.FindControl("lbkView");
                    LinkButton lbkRecordView = (LinkButton)e.Row.FindControl("lbkRecordView");
                    LinkButton lbkDownloadRecord = (LinkButton)e.Row.FindControl("lbkDownloadRecord");
                    LinkButton lbkViewDetails = (LinkButton)e.Row.FindControl("lbkViewDetails");

                    long AuditDetailID = Convert.ToInt64(lblAuditDetailID.Text);
                    long ChecklistID = Convert.ToInt64(lblChecklistID.Text);
                    long ChecklistMappingID = Convert.ToInt64(lblChecklistMappingID.Text);

                    var AuditSampleData = Business.VenderAudits.VenderAuditManagment.GetAuditSampleDocumentData(ChecklistID);
                    if (AuditSampleData.Count <= 0)
                    {
                        lbkDownload.Visible = false;
                        lbkView.Visible = false;
                    }
                    else
                    {
                        lbkDownload.Visible = true;
                        lbkView.Visible = true;
                    }

                    var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType(AuditDetailID, ChecklistMappingID);
                    if (ComplianceType == "S")
                    {
                        var IsLicenceDetail = Business.VenderAudits.VenderAuditManagment.AuditChecklistLicence(AuditDetailID, ChecklistID, AuditStartDate, AuditEndDate, "S").ToList();
                        if (IsLicenceDetail.Count <= 0)
                        {
                            lbkViewDetails.Visible = false;
                        }
                        else
                        {
                            lbkViewDetails.Visible = true;
                        }

                        var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditDetailID, ChecklistMappingID, AuditStartDate, AuditEndDate, "S", AuthenticationHelper.CustomerID, "").ToList();
                        var DistinctComplianceInstanceIDList = MasterComplianceDocument.Select(o => o.ComplianceInstanceID).Distinct().ToList();
                        if (DistinctComplianceInstanceIDList.Count <= 0)
                        {
                            lbkDownloadRecord.Visible = false;
                            lbkRecordView.Visible = false;
                        }
                        else
                        {
                            lbkRecordView.Visible = true;
                            lbkDownloadRecord.Visible = true;
                        }
                    }
                    else
                    {
                        var IsLicenceDetail = Business.VenderAudits.VenderAuditManagment.AuditChecklistLicence(AuditDetailID, ChecklistID, AuditStartDate, AuditEndDate, "I").ToList();
                        if (IsLicenceDetail.Count <= 0)
                        {
                            lbkViewDetails.Visible = false;
                        }
                        else
                        {
                            lbkViewDetails.Visible = true;
                        }

                        var MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocument(AuditDetailID, ChecklistMappingID, AuditStartDate, AuditEndDate, "I", AuthenticationHelper.CustomerID, "").ToList();
                        var DistinctComplianceInstanceIDList = MasterComplianceDocument.Select(o => o.ComplianceInstanceID).Distinct().ToList();
                        if (DistinctComplianceInstanceIDList.Count <= 0)
                        {
                            lbkRecordView.Visible = false;
                            lbkDownloadRecord.Visible = false;
                        }
                        else
                        {
                            lbkRecordView.Visible = true;
                            lbkDownloadRecord.Visible = true;
                        }
                    }
                    var observation = Business.VenderAudits.VenderAuditManagment.GetAuditCheckListObservation(AuditDetailID, ChecklistID, ChecklistMappingID);

                    if (!string.IsNullOrEmpty(observation))
                    {
                        txtObservation.Text = observation;
                        //btn_Update.Visible = false;
                        //CheckBox1.Enabled = false;
                    }
                    else
                    {
                        //CheckBox1.Enabled = true;
                        //btn_Update.Visible = true;
                    }

                    if (Convert.ToString(ViewState["AuditFlag"]).Equals("True"))
                    {
                        CheckBox1.Enabled = false;
                        btn_Update.Visible = false;
                        UploadDocument.Enabled = false;
                        checkAll.Enabled = false;
                    }
                    else
                    {
                        checkAll.Enabled = true;
                        CheckBox1.Enabled = true;
                        UploadDocument.Enabled = true;
                        btn_Update.Visible = true;
                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void rptComplianceSampleViewRecord_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleRecordView = (LinkButton)e.Item.FindControl("lblSampleRecordView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleRecordView);
            }
        }

        protected void rptComplianceSampleViewRecord_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("View"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    long FileID = Convert.ToInt64(commandArgs[0]);
                    long AuditDetailID = Convert.ToInt64(commandArgs[1]);
                    long ScheduledOnID = Convert.ToInt64(commandArgs[2]);
                    long ChecklistID = Convert.ToInt64(commandArgs[3]);
                    long ChecklistMappingID = Convert.ToInt64(commandArgs[4]);

                    DateTime AuditStartDate =Convert.ToDateTime(ViewState["AuditStartDate"]);
                    DateTime AuditEndDate = Convert.ToDateTime(ViewState["AuditEndDate"]);
                   List<SP_GetAuditChecklistComplianceDocument_Result> MasterComplianceDocument = new List<SP_GetAuditChecklistComplianceDocument_Result>();
                    var ComplianceType = Business.VenderAudits.VenderAuditManagment.GetComplianceType(AuditDetailID, ChecklistMappingID);

                    if (ComplianceType == "S")
                    {
                         MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocumentSelected(FileID, ScheduledOnID, AuditDetailID, ChecklistMappingID, AuditStartDate, AuditEndDate, "S", AuthenticationHelper.CustomerID).ToList();
                    }
                    else
                    {
                         MasterComplianceDocument = Business.VenderAudits.VenderAuditManagment.AuditChecklistComplianceDocumentSelected(FileID, ScheduledOnID, AuditDetailID, ChecklistMappingID, AuditStartDate, AuditEndDate, "I", AuthenticationHelper.CustomerID).ToList();
                    }
                    var DistinctComplianceInstanceIDList = MasterComplianceDocument.Select(o => o.ComplianceInstanceID).Distinct().ToList();

                    if (DistinctComplianceInstanceIDList.Count > 0)
                    {
                        var CMPDocuments = MasterComplianceDocument.ToList().FirstOrDefault();

                        if (CMPDocuments != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath), CMPDocuments.FileKey + Path.GetExtension(CMPDocuments.FileName));
                            string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                            if (CMPDocuments.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileRecord();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }
                                    string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (CMPDocuments.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();

                                    CompDocPath = FileName;
                                    CompDocPath = CompDocPath.Substring(2, CompDocPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFileRecord('" + CompDocPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFileRecord();", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document not uploaded.')", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}