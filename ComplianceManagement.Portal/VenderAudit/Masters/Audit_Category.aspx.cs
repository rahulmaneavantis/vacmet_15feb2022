﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.VenderAudits;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.VenderAudit.Masters
{
    public partial class Audit_Category : System.Web.UI.Page
    {
        public bool MGM_KEy;
        protected static int customerid;
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            bool ISCADMN = false;
            if (AuthenticationHelper.Role == "CADMN")
            {
                ISCADMN = true;
            }
            bool ISIMPL = false;
            if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "RREV")
            {
                ISIMPL = true;
            }
            if (AuthenticationHelper.Role == "MGMT")
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            MGM_KEy = CaseManagement.CheckForClient(customerid, "MGMT_CompanyAdmin");
            if (!IsPostBack)
            {
                bool ISMGMT = false;
                if (MGM_KEy && AuthenticationHelper.Role != "EXCT")
                {
                    ISMGMT = true;
                }
                else
                {
                    ISMGMT = false;
                }

                if (HttpContext.Current.Request.IsAuthenticated && (ISMGMT || ISCADMN || ISIMPL))
                {
                        BindVendor();
                        BindAuditCategory();
                        if ((AuthenticationHelper.Role.Equals("CADMN")))
                        {
                            btnAddCategory.Visible = true;
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
               
            }
        }
        protected void btnAddCategory_Click(object sender, EventArgs e)
        {
            saveopo.Value = "false";
            ViewState["Mode"] = 0;
            ddlVendor.SelectedValue = "-1";
            tbxAudit_Category.Text = string.Empty;
            upCategory.Update();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAudit_CategoryDialog\").dialog('open')", true);
        }
        protected void upCategory_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateAuditCategory.IsValid = false;
                cvDuplicateAuditCategory.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplienceAudit.PageIndex = 0;
                BindAuditCategory();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateAuditCategory.IsValid = false;
                cvDuplicateAuditCategory.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindAuditCategory()
        {
            try
            {
                var AuditList = VenderAuditManagment.GetAuditCategory(CustomerID);

                if (AuditList.Count > 0)
                    AuditList = AuditList.OrderBy(entry => entry.AuditCategoryName).ToList();

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    AuditList = AuditList.Where(entry => entry.AuditCategoryName.ToUpper().Trim().Contains(tbxFilter.Text.ToUpper().Trim())).ToList();
                }
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);

                    }
                }
                Session["TotalRows"] = null;

                if (AuditList.Count > 0)
                {
                    grdComplienceAudit.DataSource = AuditList;
                    Session["TotalRows"] = AuditList.Count;
                    grdComplienceAudit.DataBind();
                }
                else
                {
                    grdComplienceAudit.DataSource = AuditList;
                    grdComplienceAudit.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindVendor()
        {
            try
            {
                ddlVendor.DataTextField = "Name";
                ddlVendor.DataValueField = "ID";
                var vendorDtls = VenderAuditManagment.FillVendorDropdown(Convert.ToInt32(CustomerID));
                if (vendorDtls.Count > 0)
                {
                    ddlVendor.DataSource = vendorDtls.OrderBy(entry => entry.Name);
                    ddlVendor.DataBind();
                }
                ddlVendor.Items.Insert(0, new ListItem("< Select Vendor >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplienceAudit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }
        protected void grdComplienceAudit_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplienceAudit_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_Audit_Category"))
                {                   
                    int CategoryID = Convert.ToInt32(e.CommandArgument);
                    var getAudiCategorybyId = VenderAuditManagment.getdataformACategory(CategoryID, Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (getAudiCategorybyId != null)
                    {
                        tbxAudit_Category.Text = getAudiCategorybyId.Name;
                        ddlVendor.SelectedValue = getAudiCategorybyId.VendorID.ToString();
                        ViewState["Mode"] = 1;
                        ViewState["AuditCategoryID"] = CategoryID;
                    }
                    upCategory.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAudit_CategoryDialog\").dialog('open')", true);                    
                }
                else if (e.CommandName.Equals("DELETE_Audit_Category"))
                {
                    int CategoryID = Convert.ToInt32(e.CommandArgument);
                    if (VenderAuditManagment.AuditCategoryExists(CategoryID))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Category already use in system you can not delete.')", true);
                    }
                    else
                    {
                        VenderAuditManagment.AuditCategoryDelete(CategoryID, AuthenticationHelper.UserID);
                        upCategory.Update();
                        BindVendor();
                        BindAuditCategory();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplienceAudit_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                saveopo.Value = "true";
                grdComplienceAudit.PageIndex = e.NewPageIndex;
                BindAuditCategory();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateAuditCategory.IsValid = false;
                cvDuplicateAuditCategory.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                AuditCategory newAuditCategory = new AuditCategory()
                {
                    Name = tbxAudit_Category.Text.Trim(),
                    Isactive =false,
                    VendorID = Convert.ToInt32(ddlVendor.SelectedValue),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,                
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    newAuditCategory.ID = Convert.ToInt32(ViewState["AuditCategoryID"]);
                }
                if (VenderAuditManagment.AuditCategoryExists(newAuditCategory))
                {
                    saveopo.Value = "true";
                    cvDuplicateAuditCategory.ErrorMessage = "Audit category already exists.";
                    cvDuplicateAuditCategory.IsValid = false;
                    return;
                }
                if ((int)ViewState["Mode"] == 0)
                {
                    VenderAuditManagment.CreateAuditCategory(newAuditCategory);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    newAuditCategory.UpdatedBy = AuthenticationHelper.UserID;
                    newAuditCategory.UpdatedOn = DateTime.Now;
                    VenderAuditManagment.UpdateAuditCategory(newAuditCategory);
                }
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divAudit_CategoryDialog\").dialog('close')", true);
               
                BindAuditCategory();
                upCategory.Update();
                saveopo.Value = "true";
                cvDuplicateAuditCategory.ErrorMessage = "Audit category saved sucessfully.";
                cvDuplicateAuditCategory.IsValid = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateAuditCategory.IsValid = false;
                cvDuplicateAuditCategory.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;            
                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../../Images/down_arrow1.png";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../../Images/up_arrow1.png";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }

        }
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
    }
}