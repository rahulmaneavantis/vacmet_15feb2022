﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Linq;
using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Web.UI;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using Ionic.Zip;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class ReportFile : System.Web.UI.Page
    {
        protected static int UID;
        protected static string Date;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["UserId"]) && !string.IsNullOrEmpty(Request.QueryString["CreateDate"]))
                {                  
                    int UId = Convert.ToInt32(Request.QueryString["UserId"].ToString());
                    UserId.Text = Request.QueryString["UserId"].ToString();
                    DateReportDate.Text = Request.QueryString["CreateDate"].ToString();

                    User user = GetDetailUser(UId);
                    if (user != null)
                    {
                        LabelUserName.Text = user.FirstName + ' ' + user.LastName;
                       
                    }
                }
              
            }
        }

        public static bool DownloadFileRahul(string urlAddress, string fileSavePath,long userID)
        {

            try
            {
                string azure_ContainerName = "avareport/ReportFile/" + userID;
                string filetoDownload = Path.GetFileName(urlAddress);
                string storageAccount_connectionString = ConfigurationManager.AppSettings["StandardReportstorageconnectionstring"];

                CloudStorageAccount mycloudStorageAccount = CloudStorageAccount.Parse(storageAccount_connectionString);
                CloudBlobClient blobClient = mycloudStorageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference(azure_ContainerName);
                CloudBlockBlob cloudBlockBlob = container.GetBlockBlobReference(filetoDownload);

                cloudBlockBlob.DownloadToFile(fileSavePath, FileMode.OpenOrCreate);

                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

            //try
            //{
            //    string azure_ContainerName = "avareport/ReportFile/" + userID;
            //    string filetoDownload = Path.GetFileName(urlAddress);
            //    string storageAccount_connectionString = ConfigurationManager.AppSettings["storageconnectionstring"];

            //    CloudStorageAccount mycloudStorageAccount = CloudStorageAccount.Parse(storageAccount_connectionString);
            //    CloudBlobClient blobClient = mycloudStorageAccount.CreateCloudBlobClient();

            //    CloudBlobContainer container = blobClient.GetContainerReference(azure_ContainerName);
            //    CloudBlockBlob cloudBlockBlob = container.GetBlockBlobReference(filetoDownload);

            //    cloudBlockBlob.DownloadToFile(fileSavePath, FileMode.OpenOrCreate);

            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    return false;
            //}

            //try
            //{
            //    using (WebClient client = new WebClient())
            //    {
            //        ServicePointManager.Expect100Continue = true;
            //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //        //DownlaodFile method directely downlaod file on you given specific path ,Here i've saved in E: Drive
            //        client.DownloadFile(urlAddress, fileSavePath);
            //        client.Dispose();
            //        return true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    return false;
            //}
        }

        public static bool DownloadFileAndSaveDocuments(string downloadFilePath, string fileName, long userid, out string downloadedFilePath)
        {
            bool downloadSuccess = false;
            downloadedFilePath = string.Empty;

            try
            {
                string directoryPath = string.Empty;
                string DocAPIURlwithPATH = string.Empty;
                directoryPath = ConfigurationManager.AppSettings["standardReportURL"].ToString() + "\\" + userid + "\\" + Convert.ToString(DateTime.Now.ToString("ddMMyy")) + "\\" + Convert.ToString(DateTime.Now.ToString("HHmmss")) + DateTime.Now.ToString("ffff");
                
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                directoryPath = directoryPath + @"\" + fileName;

                DocAPIURlwithPATH = downloadFilePath;
                if (!string.IsNullOrEmpty(directoryPath))
                {
                    downloadSuccess = DownloadFileRahul(DocAPIURlwithPATH, directoryPath, userid);
                    if (downloadSuccess)
                        downloadedFilePath = directoryPath;
                }
                return downloadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return downloadSuccess;
            }
        }

        public static bool DownloadFile(string url, string fileName)
        {
            //Create a stream for the file
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);

                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
            return false;
        }

        public static User GetDetailUser(int UId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Users
                             where (row.ID == UId)
                               && row.IsActive == true
                               && row.IsDeleted == false
                             select row).FirstOrDefault();

                return data;
            }
        }

        public static List<sp_Get_Reportgeneratelog_Result> GetDetailReports(int UID,DateTime checkDate,string PDFName,string Flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string getDate = checkDate.ToString("yyyy-MM-dd");
                
                var data = entities.sp_Get_Reportgeneratelog(UID, PDFName, getDate, Flag).ToList();
                
                //var data = (from row in entities.report_generate_log
                //            where row.user_id == UID
                //            && row.created_at.Value.Date.ToString("yyyy-dd-yy") == checkDate.ToString("yyyy-dd-yy")
                //            && row.pdf_name.Equals(PDFName)
                //            && row.IsSend == true
                //            select row).FirstOrDefault();
                return data;
            }
        }
        
        protected void lbgotoReport_Click(object sender, EventArgs e)
        {
            UID = Convert.ToInt32(UserId.Text);

            User user = GetDetailUser(UID);

            if (user != null)
                {
                    string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string role = RoleManagement.GetByID(user.RoleID).Code;
                    int checkInternalapplicable = 0;
                    int checkTaskapplicable = 0;
                    int checkVerticalapplicable = 2;
                    int checkLabelApplicable = 2;
                    bool IsPaymentCustomer = false;
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    //Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Report/KendoReportNew.aspx", false);
                }
            
        }

        protected void ComprehensiveReport_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;// "2018-11-21";
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "ComprehensiveReport","S");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
                //GetResult = DownloadFile(url, fileName);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void RiskSummaryReport_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;// "2018-11-21";
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "RiskReport", "S");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
                //GetResult = DownloadFile(url, fileName);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void UserReport_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;// "2018-11-21";
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "UserReport", "S");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
                //GetResult = DownloadFile(url, fileName);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void LocationReport_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;// "2018-11-21";
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "LocationReport", "S");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
                //GetResult = DownloadFile(url, fileName);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void CategoryReport_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;// "2018-11-21";
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "CategoryReport", "S");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
                //GetResult = DownloadFile(url, fileName);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void DetailedReport_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "DetailReport", "S");

            if (Reports.Count > 0)
            {
                if (Reports.Count > 1)
                {
                    string userpath = null;
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        foreach (var item in Reports)
                        {
                            url = item.location.ToString();
                            fileName = item.report_name;

                            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
                            {
                                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                                if (GetResult)
                                    ComplianceZip.AddEntry(fileName, DocumentManagement.ReadDocFiles(userpath));
                            }
                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
                else
                {
                    if (Reports.Count > 0)
                    {
                        url = Reports[0].location.ToString();
                        fileName = Reports[0].pdf_name + ".xlsx";
                    }
                    if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
                    {
                        #region updated change 
                        string userpath = null;
                        GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                        if (GetResult)
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                            Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                            Response.Flush(); // send it to the client to download
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        #endregion
                        //GetResult = DownloadFile(url, fileName);
                    }
                }
            }
            else
            {               
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }

        }

        protected void InternalOverall_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;// "2018-11-21";
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "ComprehensiveReport", "I");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void InternalLocation_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "LocationReport", "I");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void InternalRisk_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "RiskReport", "I");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion           
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void InternalUser_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "UserReport", "I");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void InternalCategory_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "CategoryReport", "I");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void InternalDetail_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "DetailReport", "I");

            if (Reports.Count > 0)
            {
                if (Reports.Count > 1)
                {
                    string userpath = null;
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        foreach (var item in Reports)
                        {
                            url = item.location.ToString();
                            fileName = item.report_name;

                            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
                            {
                                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                                if (GetResult)
                                    ComplianceZip.AddEntry(fileName, DocumentManagement.ReadDocFiles(userpath));
                            }
                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
                else
                {
                    if (Reports.Count > 0)
                    {
                        url = Reports[0].location.ToString();
                        fileName = Reports[0].pdf_name + ".xlsx";
                    }
                    if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
                    {
                        #region updated change 
                        string userpath = null;
                        GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                        if (GetResult)
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                            Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                            Response.Flush(); // send it to the client to download
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                        #endregion
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void CriticalReport_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;// "2018-11-21";
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "CriticalRiskReport", "S");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
                //GetResult = DownloadFile(url, fileName);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }

        protected void InternalCriticalRiskReport_ServerClick(object sender, EventArgs e)
        {
            List<sp_Get_Reportgeneratelog_Result> Reports = new List<sp_Get_Reportgeneratelog_Result>();

            bool GetResult = false;
            string url = string.Empty;
            string fileName = string.Empty;

            UID = Convert.ToInt32(UserId.Text);

            string Createdate = DateReportDate.Text;// "2018-11-21";
            DateTime checkDate = Convert.ToDateTime(Createdate);

            Reports = GetDetailReports(UID, checkDate, "CriticalRiskReport", "I");

            if (Reports.Count > 0)
            {
                url = Reports[0].location.ToString();
                fileName = Reports[0].pdf_name + ".pdf";
            }
            if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(fileName))
            {
                #region updated change 
                string userpath = null;
                GetResult = DownloadFileAndSaveDocuments(url, fileName, UID, out userpath);
                if (GetResult)
                {
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(DocumentManagement.ReadDocFiles(userpath)); // create the file
                    Response.Flush(); // send it to the client to download
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                #endregion
                //GetResult = DownloadFile(url, fileName);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
        }
    }
}