﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class QuestionModal
    {
        public int ID { get; set; }
        public string QuestionName { get; set; }
        public int ControlID { get; set; }
        public int MasterID { get; set; }
        public Nullable<int> txtvalue { get; set; }
        public Nullable<int> minnumber { get; set; }
        public Nullable<int> maxnumber { get; set; }
        public Nullable<int> mastertypeID { get; set; }
    }
    public class PossibleAnswers
    {        
        public string name { get; set; }
    }
   
    public class QuestionMasterModal
    {
        public int ID { get; set; }
        public string QuestionName { get; set; }
        public int ControlID { get; set; }
        public int TabID { get; set; }
        public Nullable<int> IsQuestionType { get; set; }
        //public string PossibleAnswers { get; set; }        
        public List<PossibleAnswers> PossibleAnswer { get; set; }       
    }
    public class LinkedToALicenseType
    {
        public long LID { get; set; }
        public string LName { get; set; }
        public int LinkedID { get; set; }
    }
    public class QAC_Mapping
    {
        public long Id { get; set; }
        public long ActID { get; set; }
        public long ComplianceID { get; set; }
        public long QuestionID { get; set; }
        public long ControlId { get; set; }
        public long ServiceID { get; set; }
        public Nullable<long> AnswerValue { get; set; }
        public string AnswerSingle { get; set; }
        public string AnswerMultiple { get; set; }
        public Nullable<long> min { get; set; }
        public Nullable<long> max { get; set; }        
        public Nullable<long> IslicenseID { get; set; }
        public Nullable<long> LicenseTypeID { get; set; }
        public Nullable<long> CategoryID { get; set; }
        public Nullable<long> SubCategoryID { get; set; }
    }

    public class QA_Mapping
    {
        public long Id { get; set; }
        public long ActID { get; set; }     
        public long QuestionID { get; set; }
        public long ControlId { get; set; }
        public long ServiceID { get; set; }
        public Nullable<long> AnswerValue { get; set; }
        public string AnswerSingle { get; set; }
        public string AnswerMultiple { get; set; }
        public Nullable<long> min { get; set; }
        public Nullable<long> max { get; set; }
        public Nullable<long> CompanyTypeID { get; set; }
        public Nullable<long> BusinessActivityID { get; set; }
        public Nullable<long> LocationTypeID { get; set; }
        public Nullable<long> ActApplicabilityID { get; set; }
    }


    public class QAC_MappingUpdate
    {

        public long TaggedQACMMappingID { get; set; }
        public long TaggedActID { get; set; }
        public long TaggedComplianceID { get; set; }
        public long TaggedQuestionID { get; set; }
        public long TaggedControllID { get; set; }
        public long TaggedServiceID { get; set; }
        public Nullable<long> TaggedAnswer { get; set; }
        public string TaggedAnswerSingle { get; set; }
        public string TaggedAnswerMultiple { get; set; }
        public Nullable<long> TaggedAnsweRangeMin { get; set; }
        public Nullable<long> TaggedAnsweRangeMax { get; set; }
        public Nullable<long> TaggedIslicenseID { get; set; }
        public Nullable<long> TaggedLicenseTypeID { get; set; }
        public Nullable<long> TaggedCategoryID { get; set; }
        public Nullable<long> TaggedSubCategoryID { get; set; }
    }
    public class QACR_MappingUpdate
    {

        public long ApprovedQACMMappingID { get; set; }
        public long ApprovedActID { get; set; }
        public long ApprovedComplianceID { get; set; }
        public long ApprovedQuestionID { get; set; }
        public long ApprovedControllID { get; set; }
        public long ApprovedServiceID { get; set; }
        public Nullable<long> ApprovedAnswer { get; set; }
        public string ApprovedAnswerSingle { get; set; }
        public string ApprovedAnswerMultiple { get; set; }
        public Nullable<long> ApprovedAnsweRangeMin { get; set; }
        public Nullable<long> ApprovedAnsweRangeMax { get; set; }
        public Nullable<long> ApprovedIslicenseID { get; set; }
        public Nullable<long> ApprovedLicenseTypeID { get; set; }
        public Nullable<long> ApprovedCategoryID { get; set; }
        public Nullable<long> ApprovedSubCategoryID { get; set; }
    }
    public class QA_MappingUpdate
    {
        public long TaggedQACMMappingID { get; set; }
        public long TaggedActID { get; set; }      
        public long TaggedQuestionID { get; set; }
        public long TaggedControllID { get; set; }
        public long TaggedServiceID { get; set; }
        public Nullable<long> TaggedAnswer { get; set; }
        public string TaggedAnswerSingle { get; set; }
        public string TaggedAnswerMultiple { get; set; }        
        public Nullable<long> TaggedAnsweRangeMin { get; set; }
        public Nullable<long> TaggedAnsweRangeMax { get; set; }
        public Nullable<long> TaggedCompanyTypeID { get; set; }
        public Nullable<long> TaggedBusinessActivityID { get; set; }
        public Nullable<long> TaggedLocationTypeID { get; set; }
        public Nullable<long> TaggedActApplicabilityID { get; set; }
    }
    public class ControllFormQuestionModal
    {
        public long ControlId { get; set; }
        public string QuestionName { get; set; }
        public List<PossibleAnswers> PossibleAnswer { get; set; }
    }
}