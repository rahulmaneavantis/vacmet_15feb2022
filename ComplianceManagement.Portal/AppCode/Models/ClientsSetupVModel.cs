﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    public class ClientsSetupVModel
    {

    }

    public class BasicSetupModel
    {
        public BasicSetupModel()
        {

            States = new List<StateVModel>();
            StateModel = new StateVModel();
            ClientDetailsVModel = new ClientDetailsVModel();
            Locations = new List<LocationVModel>();
            LocationVModel = new LocationVModel();
        }
        public bool Exception { get; set; }
        public ClientDetailsVModel ClientDetailsVModel { get; set; }
        public bool SheetNameError { get; set; }
        public StateVModel StateModel { get; set; }
        public List<StateVModel> States { get; set; }
        public List<LocationVModel> Locations { get; set; }
        public LocationVModel LocationVModel { get; set; }
        public string hdnClientID { get; set; }
        [Required(ErrorMessage = "Act Applicablity is required")]
        [Display(Name = "Act Applicablity")]
        public string ActApplicablity { get; set; }
        [Display(Name = "Excemption")]
        public bool CM_Excemption { get; set; }
        public string ContractType { get; set; }
        public int AVACOM_BranchID { get; set; }
        public string AVACOM_BranchName { get; set; }
        public string BranchType { get; set; }
        [Required(ErrorMessage = "CorporateID is required")]
        [Display(Name = "CorporateID")]
        public string CO_CorporateID { get; set; }
        public int CL_ID { get; set; }
        public bool Message { get; set; }
        public string city { get; set; }
        public string CM_ClientID { get; set; }
        [Required(ErrorMessage = "Client Name is required")]
        [Display(Name = "Client Name")]
        public string CM_ClientName { get; set; }
        [Required(ErrorMessage = "Establishment Type is required")]
        [Display(Name = "Establishment Type")]
        public string CM_EstablishmentType { get; set; }
        public string CL_OfficeType { get; set; }

        [Required(ErrorMessage = "Service Start Date is required")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Service Start Date")]
        public string CM_ServiceStartDate { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [Display(Name = "Address")]
        public string CM_Address { get; set; }
        [Display(Name = "Bonus Percentage")]
        [Required(ErrorMessage = "Bonus Percentage is required")]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Only Numbers are allowed")]
        //[RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed.")]
        public decimal CM_BonusPercentage { get; set; }
        public string CM_Country { get; set; }
        public string CM_State { get; set; }
        [Required(ErrorMessage = "City is required")]
        [Display(Name = "City")]
        public string CM_City { get; set; }
        public string CM_Pincode { get; set; }
        [Required(ErrorMessage ="Status is required")]
        [Display(Name = "Status")]
        public string CM_Status { get; set; }

        public string FileName { get; set; }
        public bool ExcelErrors { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNumber { get; set; }
        public string EmailID { get; set; }

        //public Nullable<bool> CM_IsBonusExcempted { get; set; }
        //public Nullable<bool> CM_IsPOApplicable { get; set; }
        public bool CM_IsPOApplicable { get; set; }
        public int Type { get; set; }
        public string AgreementID { get; set; }
        public string BDAnchor { get; set; }
        public string RAMAnchor { get; set; }
        public string ProcessAnchor { get; set; }
        public string ChallanAnchor { get; set; }
        public string ReturnAnchor { get; set; }
        public string RLCSAnchor { get; set; }
        public string AuditAnchor { get; set; }
        public string LocationAnchor { get; set; }

        public string SPOCSANITATION { get; set; }
        public string SPOC_FirstName { get; set; }
        public string SPOC_LastName { get; set; }
        public string SPOC_ContactNumber { get; set; }
        public string SPOC_Email { get; set; }
        public string SPOC_DESIGNATION { get; set; }

        public string EP1SANITATION { get; set; }
        public string EP1_FirstName { get; set; }
        public string EP1_LastName { get; set; }
        public string EP1_ContactNumber { get; set; }
        public string EP1_Email { get; set; }
        public string EP1_DESIGNATION { get; set; }

        public string EP2SANITATION { get; set; }
        public string EP2_FirstName { get; set; }
        public string EP2_LastName { get; set; }
        public string EP2_ContactNumber { get; set; }
        public string EP2_Email { get; set; }
        public string EP2_DESIGNATION { get; set; }

        public string PFType { get; set; }
        public string EDLIExcemptionType { get; set; }
    }


    public class jsonClientDetails
    {
        public string ClientId { get; set; }
        public string CorporateId { get; set; }
        public string ClientName { get; set; }
        public string Mandate { get; set; }
        public string AgreementID { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }
        public string ActApplicabilty { get; set; }
        public string EstablishmentType { get; set; }
        public DateTime? ServiceStartDate { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string CM_Country { get; set; }
        public string Status { get; set; }
        public decimal BonusPercentage { get; set; }
        //public bool Excemption { get; set; }
        public string WagePeriodFrom { get; set; }

        public string WagePeriodTo { get; set; }

        public string PaymentDate { get; set; }

        public string ServiceTaxExcempted { get; set; }
        // public DateTime? ServiceStartDate { get; set; }
        public DateTime? DateOfCommencement { get; set; }
        public string EDLIExcemption { get; set; }

        public string PFCode { get; set; }
        public string CB_PF_CodeType { get; set; }
        public int? ClientFlag { get; set; }

        public bool? POApplicabilty { get; set; }
        public bool? excemption { get; set; }
        
        public string BDAnchor { get; set; }
        public string RAMAnchor { get; set; }
        public string RegisterAnchor { get; set; }
        public string ChallanAnchor { get; set; }
        public string ReturnAnchor { get; set; }
        public string LocationAnchor { get; set; }
        public string CRAnchor { get; set; }
        public string AuditAnchor { get; set; }

        public string SpocSalutation { get; set; }
        public string SpocFirstName { get; set; }
        public string SpocLastName { get; set; }
        public string SpocContactNo { get; set; }
        public string SpocEmail { get; set; }
        public string SpocDesignation { get; set; }

        public string Ep1Salutation { get; set; }
        public string Ep1FirstName { get; set; }
        public string Ep1LastName { get; set; }
        public string Ep1ContactNo { get; set; }
        public string Ep1Email { get; set; }
        public string Ep1Designation { get; set; }

        public string Ep2Salutation { get; set; }
        public string Ep2FirstName { get; set; }
        public string Ep2LastName { get; set; }
        public string Ep2ContactNo { get; set; }
        public string Ep2Email { get; set; }
        public string Ep2Designation { get; set; }
        public string CB_EDLI_ExcemptionType { get; set; }
        public string Industry_Type { get; set; }
    }
    public class ClientLocationVModel
    {
        public ClientLocationVModel()
        {
            CityList = new List<CityVModel>();
            States = new List<StateVModel>();
            PTStates = new List<StateVModel>();
            StateModel = new StateVModel();
            CityVModel = new CityVModel();
            Locations = new List<LocationVModel>();
            LocationsAnchor = new List<LocationAnchorVModel>();
           
        }
        public bool SheetNameError { get; set; }
        public bool Exception { get; set; }
        public StateVModel StateModel { get; set; }
        public List<StateVModel> PTStates { get; set; }
        public List<StateVModel> States { get; set; }
        public CityVModel CityVModel { get; set; }
        public List<CityVModel> CityList { get; set; }
        public List<LocationVModel> Locations { get; set; }
        public List<LocationAnchorVModel> LocationsAnchor { get; set; }
       
        public string FileName { get; set; }
        public string CorporateID { get; set; }
        public bool ExcelErrors { get; set; }
        public long ID { get; set; }
        [Required(ErrorMessage = "State is required")]
        [Display(Name = "State")]
        public string CM_State { get; set; }
        [Required(ErrorMessage = "City is required")]
        [Display(Name = "City")]
        public string CM_City { get; set; }
        public bool Message { get; set; }
        public string CM_ClientID { get; set; }
        public bool BranchExist { get; set; }
        public string SaveMode { get; set; }

        [Required(ErrorMessage = "Client Name is required")]
        [Display(Name = "Client Name")]
        public string CM_ClientName { get; set; }
        [Required(ErrorMessage = "Branch is required")]
        [Display(Name = "Branch")]
        public int AVACOM_BranchID { get; set; }
        public string CL_PT_State { get; set; }
        [Display(Name = "PF Code")]
        //[RegularExpression("[a-zA-Z0-9-\\\\;:\"\\/,\\[\\]]*$", ErrorMessage = "Only alphanumeric characters, slash and hyphens are allowed")]

        [RegularExpression("[a-zA-Z0-9-\\/\\\\]*$", ErrorMessage = "Only alphanumeric characters, slash and hyphens are allowed")]
        //[StringLength(22, MinimumLength = 12, ErrorMessage = "PF NO maximum 22 and minimum 12 digit allowed")]
        [StringLength(12, ErrorMessage = "PF NO maximum 12 digit allowed")]
        public string CL_PF_Code { get; set; }
        [Display(Name = "ESIC Code")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed")]
        public string CL_ESIC_Code { get; set; }
        public string CL_LWF_State { get; set; }
        [Required(ErrorMessage = "Establishment Type is required")]
        [Display(Name = "Establishment Type")]
        public string CM_EstablishmentType { get; set; }
        [Required(ErrorMessage = "Office Type is required")]
        [Display(Name = "Office Type")]
        public string CL_OfficeType { get; set; }
        public string AVACOM_BranchName { get; set; }
        [Required(ErrorMessage = "Employer Name is required")]
        [Display(Name = "Employer Name")]
        [RegularExpression("^[a-zA-Z. ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string CL_EmployerName { get; set; }
        [Required(ErrorMessage = "Employer Address is required")]
        [Display(Name = "Employer Address")]
        public string CL_EmployerAddress { get; set; }
        [Required(ErrorMessage = "Manager Name is required")]
        [Display(Name = "Manager Name")]
        [RegularExpression("^[a-zA-Z. ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string CL_ManagerName { get; set; }
        [Required(ErrorMessage = "Manager Address is required")]
        [Display(Name = "Manager Address")]
        public string CL_ManagerAddress { get; set; }
        [Required(ErrorMessage = "Branch Address is required")]
        [Display(Name = "Address")]
        public string CM_Address { get; set; }

        [Required(ErrorMessage = "Company Phone No is required")]
        [Display(Name = "Company Phone No")]
        //[MaxLength(12, ErrorMessage = "Company phone no should have max 12 digit")]
        //[MinLength(10, ErrorMessage = "Company phone no should have atleast 10 digit")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed")]
        public string CL_CompPhoneNo { get; set; }
        [Display(Name = "HR Contact Person")]
        [RegularExpression("^[a-zA-Z. ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string CL_HRContactPerson { get; set; }

        [Required(ErrorMessage = "HR Phone No is required")]
        //[MaxLength(12, ErrorMessage = "HR phone no should have max 12 digit")]
        //[MinLength(10, ErrorMessage = "HR phone no should have atleast 10 digit")]
        [Display(Name = "HR Phone No")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed")]
        public string CL_HRPhNo { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "HR Mail ID")]
        public string CL_HRMailID { get; set; }
        [Display(Name = "HR 1st Level Phone No")]
        //[MaxLength(10, ErrorMessage = "HR 1st level phone no should have max 10 digit")]
        //[MinLength(5, ErrorMessage = "HR 1st level phone no should have atleast 5 digit")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed")]
       
        public string CL_HR1stLevelPhNo { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "HR 1st Level Mail")]
        public string CL_HR1stLevelMail { get; set; }
        [Display(Name = "RC No")]
        // [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Characters and Numbers are alloewd")] //char,num and spl chr to be allowed as req by cpo team 29Apr2020
        [RegularExpression("[A-Za-z0-9 @!@#$%&*(){|}_=+\\\\;:\"\\/?>.<,\\[\\]-]*$", ErrorMessage = "Not allowed ~`'")]
        public string CL_RCNo { get; set; }
        [Display(Name = "RC Valid From")]
        [RCValidAttribute("CL_RCValidTo", ErrorMessage = "RC From Should be less than RC Valid To")]
        public string CL_RCValidFrom { get; set; }
        public string CL_RCValidTo { get; set; }
        [Required(ErrorMessage = "Nature of Business is required")]
        [Display(Name = "Nature Of Business")]
        //[RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed")]
        
        [RegularExpression("[A-Za-z @!@#$%&*(){|}_=+\\\\;:\"\\/?>.<,\\[\\]-]*$", ErrorMessage = "Only Alphabets and Special Characters are allowed")]
        public string CL_NatureOfBusiness { get; set; }
        [Required(ErrorMessage = "Work Hours (From) is required")]
       [WorkHourLessThanAttribute("CL_WorkHoursTo", ErrorMessage = "Work Hours From Should be less than Work Hours To")]
        [Display(Name = "Work Hours From")]
        public string CL_WorkHoursFrom { get; set; }
        [Required(ErrorMessage = "Work Hours (To) is required")]
        [Display(Name = "Work Hours To")]
        public string CL_WorkHoursTo { get; set; }
        [Required(ErrorMessage = "Work off Day is required")]
        [Display(Name = "Week off Day")]
        public string CL_WeekoffDay { get; set; }
        [Required(ErrorMessage = "Intervals (From) is required")]
        [Display(Name = "Intervals From")]
        [IntervalHourMoreThanAttribute("CL_WorkHoursFrom", ErrorMessage = "Intervals From Should be More than Work Hours From")]
        [WorkHourLessThanAttribute("CL_WorkHoursTo", ErrorMessage = "Intervals From Should be less than Work Hours To")]
        [IntervalLessThanAttribute("CL_IntervalsTo", ErrorMessage = "Intervals From Should be less than Intervals To")]
        public string CL_IntervalsFrom { get; set; }
        [Required(ErrorMessage = "Intervals (To) is required")]
        [Display(Name = "Intervals To")]
        [WorkHourLessThanAttribute("CL_WorkHoursTo", ErrorMessage = "Intervals To Should be less than Work Hours To")]
        public string CL_IntervalsTo { get; set; }
        [Required(ErrorMessage = "Work Timings is required")]
        [Display(Name = "Work Timings")]
        public string CL_WorkTimings { get; set; }
        public string CL_LocationAnchor { get; set; }
        [Required(ErrorMessage = "Status is required")]
        [Display(Name = "Status")]
        public string CL_Status { get; set; }
        public string CL_LIN { get; set; }

        [Display(Name = "Municipality")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed")]
        [RequiredOnEstablishmentTypeAttribute("CM_EstablishmentType", ErrorMessage = "Municipality is required")]
        public string CL_Municipality { get; set; }

        [Display(Name = "Maintain Forms")]
        [RequiredOnEstablishmentTypeAttribute("CM_EstablishmentType", ErrorMessage = "Maintain Forms is required")]
        public string CL_Permission_MaintaingForms { get; set; }

        [Display(Name = "Power for Fines")]
        [RequiredOnEstablishmentTypeAttribute("CM_EstablishmentType", ErrorMessage = "Power for Fines is required")]
        public string CL_RequirePowerforFines { get; set; }

        [Required(ErrorMessage = "Business Type is required")]
        [Display(Name = "Business Type")]
        public string CL_BusinessType { get; set; }

        public string CL_CommencementDate { get; set; }
        [Display(Name = "Classsification of Estabilishment")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string CL_ClasssificationofEstabilishment { get; set; }
        public string CL_EmployerDesignation { get; set; }
       
        public string CL_EDLI_Excemption { get; set; }

        [Display(Name = "Licence No")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only numbers are allowed")]
        public string CL_LicenceNo { get; set; }
        [Display(Name = "NIC Code")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and numbers are allowed")]
        public string CL_NICCode { get; set; }
        public string CL_SectionofAct { get; set; }
        [Display(Name = "District")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string CL_District { get; set; }
        [Display(Name ="Pincode")]
        [DataType(DataType.PostalCode)]
        //[MaxLength(6,ErrorMessage ="Pincode must be of 6 digit"), MinLength(6)]
        public string CL_Pincode { get; set; }
        [Display(Name = "Jurisdiction")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Alphabets are allowed")]
        public string CL_Jurisdiction { get; set; }
        
        public int NoofEmployees { get; set; }
        public string CL_BranchCode { get; set; }
        public int? ServiceProviderID { get; set; }
        
        public string CL_TradeLicenceApplicability { get; set; }
        public string CL_BranchEndDate { get; set; }
        public string PFType { get; set; }
        public string EDLIType { get; set; }
        public string CL_BranchStartDate { get; set; }
    }

    public class CustomerBranchVModel
    {
        public int SubEntityID { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Address { get; set; }
        public int State { get; set; }
        public int City { get; set; }
        public string PinCode { get; set; }
        public string ContactPerson { get; set; }
        public string Email { get; set; }
    }

    public class ClientDetailsVModel
    {
        public int CB_ID { get; set; }
        public string CB_ClientID { get; set; }
        [Required(ErrorMessage = "Payment Date is required")]
        public string CB_PaymentDate { get; set; }
        [Required(ErrorMessage = "Wage Period From is required")]
        [Display(Name = "Wage Period From")]
        public string CB_WagePeriodFrom { get; set; }
        [Required(ErrorMessage = "Wage Period To is required")]
        [Display(Name = "Wage Period To")]
        public string CB_WagePeriodTo { get; set; }
        public string CB_LeaveCreditApplicablity { get; set; }

        //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Commencement Date is required")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Commencement Date")]
        public string CB_DateOfCommencement { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Service Tax Exmpted is required")]
        [Display(Name = "Service Tax Exmpted")]
        public string CB_ServiceTaxExmpted { get; set; }
        //[Required]
        //[Display(Name = "EDLI Excemption")]
        public string CB_EDLIExcemption { get; set; }
        public string CB_ActType { get; set; }
        [Required(ErrorMessage ="PF Code is required")]
         [Display(Name = "PF Code")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers are allowed")]
        [StringLength(22, MinimumLength = 12, ErrorMessage = "PF Code maximum 22 and minimum 12 digit allowed")]
        public string PFCode { get; set; }
    }

    public class UploadDocumentVModel
    {
        public UploadDocumentVModel()
        {
            Setup = new BasicSetupModel();
            ClientLocationVModel = new ClientLocationVModel();
        }
        public bool ClientCheck { get; set; }
        public ClientLocationVModel ClientLocationVModel { get; set; }
        public bool LocationCheck { get; set; }
        public bool Message { get; set; } = false;
        public bool ServerError { get; set; }
        public bool ExtensionError { get; set; }
        public bool Error { get; set; }
        public BasicSetupModel Setup { get; set; }

    }

    public class EmployeeMasterVModel
    {
        public EmployeeMasterVModel()
        {
            StateModel = new StateVModel();
            States = new List<StateVModel>();
            LocationVModel = new LocationVModel();
            Locations = new List<LocationVModel>();
            CountryVModel = new CountryVModel();
            Countries = new List<CountryVModel>();
            PTStates = new List<StateVModel>();
            UploadEmployee = new UploadEmployeeDocumentVModel();
        }

        public UploadEmployeeDocumentVModel UploadEmployee { get; set; }

        [Display(Name = "Branch")]
        [Required(ErrorMessage = "Branch is required")]
       // [Remote("CheckBranch", "Setup", AdditionalFields = "AVACOM_BranchID", HttpMethod = "POST", ErrorMessage = "Please select branch!")]
        public int AVACOM_BranchID { get; set; }
        public int AVACOMCustomerID { get; set; }
        public bool Uploaded { get; set; }
        public bool update { get; set; }
        public int UserId { get; set; }
        public string ProfileID { get; set; }
        public string Path1 { get; set; }
        public int CustId { get; set; }
        public bool Exception { get; set; }
        public CountryVModel CountryVModel { get; set; }
        public List<CountryVModel> Countries { get; set; }
        public List<StateVModel> PTStates { get; set; }
        public LocationVModel LocationVModel { get; set; }
        public StateVModel StateModel { get; set; }
        public bool Message { get; set; }
        public List<LocationVModel> Locations { get; set; }
        public List<StateVModel> States { get; set; }
        public int EM_ID { get; set; }
        public List<SectorVModel> VMSector { get; set; }
        public List<JobCategoryVModel> VMJobCategory { get; set; }
        public List<IndustryTypeVModel> VMIndustryType { get; set; }

        [Display(Name = "Employee ID")]
        [RegularExpression("^[a-zA-Z0-9 -]*$", ErrorMessage = "Only Characters and Numbers are allowed")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "EmployeeID is required")]
      //  [Remote("CheckExistingEmployee", "Setup", HttpMethod = "POST", ErrorMessage = "EmployeeID already exists!")]
        public string EM_EmpID { get; set; }
        [Display(Name = "Client")]

        public string EM_ClientID { get; set; }

        public string EM_ClientDateOfCommencement { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Employee Name is required")]
        [Display(Name = "Employee Name")]
        public string EM_EmpName { get; set; }
        [Display(Name = "State")]
        // [Required(ErrorMessage = "State is required")]
        public string EM_State { get; set; }
        public string EM_Location { get; set; }
        public string EM_Branch { get; set; }
        //[Display(Name = "PT State")]
        //[Required(ErrorMessage = "PT State is required")]
        public string EM_PT_State { get; set; }
        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Gender is required")]
        public string EM_Gender { get; set; }
        [Display(Name = "Father Name")]
        //[Required(ErrorMessage = "Father Name is required")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Characters are allowed")]
        public string EM_FatherName { get; set; }
        [Display(Name = "Relationship")]
        //[Required(ErrorMessage = "Relationship is required")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Characters are allowed")]
        public string EM_Relationship { get; set; }
        [Display(Name = "Date of Birth")]
        [Required(ErrorMessage = "Date of Birth is required")]
        public string EM_DOB { get; set; }
        [Display(Name = "Marital Status")]
        //[Required(ErrorMessage = "Marital Status is required")]
        public string EM_MaritalStatus { get; set; }
        [Display(Name = "Date of Joining")]
        [Required(ErrorMessage = "Date of Joining is required")]
        public string EM_DOJ { get; set; }
        [Display(Name = "Date of Leaving")]

        public string EM_DOL { get; set; }
        [Display(Name = "PF NO")]
        //[RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Characters and Numbers are allowed")]
        [RegularExpression("[a-zA-Z0-9-\\/\\\\]*$", ErrorMessage = "Only alphanumeric characters, slash and hyphens are allowed")]
        [StringLength(22, MinimumLength =12, ErrorMessage = "PF NO maximum 22 and minimum 12 digit allowed")]
        public string EM_PFNO { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed")]
        public string EM_ESICNO { get; set; }
       

        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EM_Emailid { get; set; }
        [Display(Name = "Mobile No")]
        [StringLength(10, ErrorMessage = "Enter valid Mobile No", MinimumLength = 10)]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("([0-9]+)", ErrorMessage = "Only Numbers are allowed")]
        public string EM_MobileNo { get; set; }
        //[MaxLength(10, ErrorMessage = "PAN NO must be of 10 digit")]
        //[MinLength(10, ErrorMessage = "PAN NO must be of 10 digit")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Characters and Numbers are allowed")]
        public string EM_PAN { get; set; }
        //[MaxLength(12, ErrorMessage = "UAN NO must be of 12 digit")]
        //[MinLength(12, ErrorMessage = "UAN NO must be of 12 digit")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed")]
        public string EM_UAN { get; set; }
       
        //[MaxLength(12, ErrorMessage = "Aadhar must be of 12 digit")]
        //[MinLength(12, ErrorMessage = "Aadhar must be of 12 digit")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed")]
        public string EM_Aadhar { get; set; }
        [Display(Name = "Bank Name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only Characters are allowed")]
        public string EM_BankName { get; set; }
        [RegularExpression("^[a-zA-Z .-]*$", ErrorMessage = "Only Characters are allowed")]
        public string EM_Department { get; set; }
        public string EM_Designation { get; set; }
        [Display(Name = "Skill Category")]
        [Required(ErrorMessage = "Skill Category is required")]
        public string EM_SkillCategory { get; set; }
        [Display(Name = "Bank Account Number")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Characters and Numbers are allowed")]
        public string EM_Bankaccountnumber { get; set; }
        [Display(Name = "IFSC")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Characters and Numbers are allowed")]
        public string EM_IFSC { get; set; }
        public string EM_Address { get; set; }
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Characters and Numbers are allowed")]
        public string EM_PassportNo { get; set; }
        [Display(Name = "Physically Challenged")]
        [Required(ErrorMessage = "Physically Challenged is required")]
        public string EM_PhysicallyChallenged { get; set; }
        [Display(Name = "Employee First time or second time")]
        [Required(ErrorMessage = "Employee first time or second time is required")]
        public string EM_Firsttime_secondtime { get; set; }
        [Required(ErrorMessage = "International Workers is required")]
        [Display(Name = "International Workers")]
        public string EM_International_workers { get; set; }
        [Display(Name = "PF Capping Applicability")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "PF Capping Applicability is required")]
        public string EM_PF_Capping_Applicability { get; set; }
        [Display(Name = "Employment Type")]
        [Required(ErrorMessage = "Employment Type is required")]
        public string EM_Employmenttype { get; set; }

        public string EM_ChangeEffective_from { get; set; }
        [Display(Name = "Status")]
        [Required(ErrorMessage = "Status is required")]
        public string EM_Status { get; set; }

        public string EM_EPS_Applicabilty { get; set; }
        [Display(Name = "Client ESI Number")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only Numbers are allowed")]
        public string EM_Client_ESI_Number { get; set; }
        public string EM_Client_PT_State { get; set; }
        public string EM_modifiedBy { get; set; }
        public string EM_CreateDate { get; set; }

        public string EM_ModifiedDate { get; set; }
        public int EM_Version { get; set; }
        public string EM_Nationality { get; set; }

        [Display(Name = "Passport Valid From")]
        public string EM_Passport_Valid_From { get; set; }

        [Display(Name = "Passport Valid To")]
        public string EM_Passport_Valid_Upto { get; set; }

        [Required(ErrorMessage = "Payroll Month is required")]
        [Display(Name = "Payroll Month")]
        public int? EM_PayrollMonth { get; set; }

        [Required(ErrorMessage = "Payroll Year is required")]
        [Display(Name = "Payroll Year")]
        public int EM_PayrollYear { get; set; }
        public string EM_PF_Applicability { get; set; }
        public string EM_ESI_Applicability { get; set; }
        [Required(ErrorMessage = "Courage Month is required")]
        [Display(Name = "Courage Month")]
        public int? EM_ESI_Out_of_Courage_Month { get; set; }
        [Required(ErrorMessage = "Courage Year is required")]
        [Display(Name = "Courage Year")]
        public int EM_ESI_Out_of_Courage_Year { get; set; }
        public string EM_PassportIssued_Country { get; set; }
        public string EM_EPFO_Aadhar_Upload { get; set; }
        public string EM_EPFO_Bank_Ac_Upload { get; set; }
        public string EM_EPFO_PAN_Upload { get; set; }
        public string EM_PMRPY { get; set; }
        public string EM_PT_Applicability { get; set; }
        [Display(Name = "No of Certificate")]
        [RegularExpression("([0-9 ]+)", ErrorMessage = "Only Numbers are allowed")]
        public string EM_NoOf_Certificate { get; set; }
        public string EM_NoOf_Certificate_Date { get; set; }
        [Display(Name = "Token Number")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Only Numbers are allowed")]
        public string EM_TokenNo { get; set; }
        [Display(Name = "Relay Assigned")]
        [RegularExpression("([a-zA-Z ]+)", ErrorMessage = "Only Characters are allowed")]
        public string EM_Relay_Assigned { get; set; }
        [Display(Name = "Letter of Group")]
        //[Required(ErrorMessage ="PF No is required")]
        [RegularExpression("^[a-zA-Z0-9 ]*$", ErrorMessage = "Only Characters and Numbers are allowed")]
        public string EM_Letter_Of_Group { get; set; }
        public string EM_WomenWorkingNightshift { get; set; }
        public string EM_ModeofTransport { get; set; }
        [Display(Name = "Security Provided")]
        [Required(ErrorMessage = "Security is required")]
        public string EM_SecurityProvided { get; set; }
        public string EM_ExistReasonCode { get; set; }
        [Display(Name = "Years of Experience")]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Only Decimal Numbers are allowed")]
        public string EM_YearsOfExperience { get; set; }
        public string EM_DateWhenClothesGiven { get; set; }
        public string EM_NumberandDateOfExemptingOrder { get; set; }
        [Display(Name = "Particulars of Transfer")]
        [RegularExpression("([a-zA-Z ,]+)", ErrorMessage = "Only Characters are allowed")]
        public string EM_ParticularsOfTransferFromOneGroupToAnother { get; set; }
        [Display(Name = "Sales Promotion")]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Only Characters are allowed")]
        public string EM_SalesPromotion { get; set; }
        [Display(Name = "Payment Mode")]
        [RegularExpression("([a-zA-Z ,]+)", ErrorMessage = "Only Characters are allowed")]
        public string EM_PaymentMode { get; set; }
        public string EM_PermanentAddress { get; set; }
        [Display(Name = "Mark of Identification")]
        [RegularExpression("([a-zA-Z ]+)", ErrorMessage = "Only Characters are allowed")]
        public string EM_Markof_Identification { get; set; }
        public string EM_Placeof_work { get; set; }
        [Display(Name = "Remarks")]
        [RegularExpression("([a-zA-Z0-9 ]+)", ErrorMessage = "Only Characters and Numbers are allowed")]
        public string EM_Remarks { get; set; }
        [Display(Name = "Eduction Level")]
        [RegularExpression("([a-zA-Z0-9 ]+)", ErrorMessage = "Only Characters and Numbers are allowed")]
        public string EM_EductionLevel { get; set; }
        [Display(Name = "Place of Employment")]
        [RegularExpression("([a-zA-Z ]+)", ErrorMessage = "Only Characters are allowed")]
        public string EM_Place_of_Employment { get; set; }
        [Display(Name = "Training Number")]
        [RegularExpression("([a-zA-Z0-9 ]+)", ErrorMessage = "Only Characters and Numbers are allowed")]
        public string EM_Training_Number { get; set; }
        public string EM_Training_Date { get; set; }
        public string EM_Emergency_contact_Address { get; set; }
        [Display(Name = "Mobile No")]
        [StringLength(10, ErrorMessage = "Contact No must be of 10 digits", MinimumLength = 10)]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("([0-9]+)", ErrorMessage = "Only Numbers are allowed")]
        public string EM_Emergency_contact_mobile_no { get; set; }
		public Nullable<bool> EM_IsLwf_Exempted { get; set; }

        public string EM_ExemptedSEA_Act { get; set; }
        public string EM_VPF_Applicability { get; set; }
        public string EM_VPF_Type { get; set; }
        [RegularExpression("([0-9]+)", ErrorMessage = "VPF Value should be numeric")]
        public int? EM_VPF_Value { get; set; }
        public int? EM_Sectorid { get; set; }
        public int? EM_JobCategory { get; set; }
        public int? EM_IndustryType { get; set; }

        public int? ServiceProviderID { get; set; }
        public string EM_LWD { get; set; }
    }

    public class TableColumnMappingVModel
    {
        public string ColumnName { get; set; }
        public string ColumnDescription { get; set; }
    }

    public class ClientHeaderMappingVModel
    {
        public long ID { get; set; }
        public int CustomerID { get; set; }
        public string TableName { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Dropdown value")]
        [Display(Name = "Column Name")]
        public string ColName { get; set; }
        public string ClientHeaderName { get; set; }   
        public bool Check { get; set; }
    }

    public class UploadEmployeeDocumentVModel
    {
        public bool ISUpdateCTS { get; set; }
        public bool ISUpdate { get; set; }
        public bool ISAdd { get; set; }
        
        public List<string> ClientHeaderName { get; set; }
        public List<string> ClientExcelHeaders { get; set; }
        public Dictionary<string, bool> ClientHeaders { get; set; }
        public TableColumnMappingVModel TableColumnMappingVModel { get; set; }
        public List<TableColumnMappingVModel> TableColumnMappingList { get; set; }
        public List<ClientHeaderMappingVModel> ClientHeaderMappingVModelList { get; set; }
        public ClientHeaderMappingVModel ClientHeaderMappingVModel { get; set; }
        public string File { get; set; }
        public bool EmployeeCheck { get; set; }
        public bool Message { get; set; }
        public bool ServerError { get; set; }
        public bool MappingMessage { get; set; }
        public bool Error { get; set; }
        public string FileName { get; set; }
        public bool ExcelErrors { get; set; }
        public int CustomerID { get; set; }

        public int UserID { get; set; }
        public bool RowCount { get; set; }
        public bool ColumnCount { get; set; }

        public UploadEmployeeDocumentVModel()
        {
            //   EmployeeMasterVModel = new EmployeeMasterVModel();
            TableColumnMappingVModel = new TableColumnMappingVModel();
            TableColumnMappingList = new List<TableColumnMappingVModel>();
            ClientExcelHeaders = new List<string>();
            ClientHeaders = new Dictionary<string, bool>();
            ClientHeaderMappingVModelList = new List<ClientHeaderMappingVModel>();
            ClientHeaderMappingVModel = new ClientHeaderMappingVModel();
        }
    }

    public class StateVModel
    {
        [Required]
        [Display(Name = "StateID")]
        public int AVACOM_StateID { get; set; }
        public string SM_Name { get; set; }
        public string SM_Code { get; set; }
    }
    public class SectorVModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
    public class JobCategoryVModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
    public class IndustryTypeVModel
    {
        public int Value { get; set; }
        public string Text { get; set; }

    }
    public class CityVModel
    {
        [Required]
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class CountryVModel
    {
        public string CNT_ID { get; set; }
        public string CNT_Name { get; set; }
    }
    public class LocationVModel
    {
        public string LM_Code { get; set; }
        public string LM_Name { get; set; }
        public string SM_Code { get; set; }
    }

    public class LocationAnchorVModel
    {
        public string ID { get; set; }
        public string UserName { get; set; }

    }
    public class BranchVModel
    {
        public string AVACOM_BranchID { get; set; }
        public string AVACOM_BranchName { get; set; }

    }

    public class EmployeeSaveorUpdateBulkUploadModel
    {
        public string EmployeeID { get; set; }
        public string ClientID { get; set; }
        public string EmployeeName { get; set; }
        public string StateCode { get; set; }
        public string LocationCode { get; set; }
        public string Branch { get; set; }
        public string PTState { get; set; }
        public string Gender { get; set; }
        public string FatherName { get; set; }
        public string Relationship { get; set; }
        public DateTime? DOB { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime? DOJ { get; set; }
        public DateTime? EM_DOL { get; set; }
        public string PFNO { get; set; }
        public string ESICNO { get; set; }
        public string UAN { get; set; }
        public string Emailid { get; set; }
        public string MobileNo { get; set; }
        public string PAN { get; set; }
        public string Aadhar { get; set; }
        public string BankName { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string SkillCategory { get; set; }
        public string Bankaccountnumber { get; set; }
        public string IFSC { get; set; }
        public string International_workers { get; set; }
        public string PF_Capping_Applicability { get; set; }

        // Not Mentioned in TL Api Paramters
        public string Address { get; set; }
        public string EM_PassportNo { get; set; }
        public string PhysicallyChallenged { get; set; }
        public string Firsttime_secondtime { get; set; }
        public string EM_Employmenttype { get; set; }
        public DateTime? EM_ChangeEffective_from { get; set; }
        public string EM_Status { get; set; }
        public DateTime? EM_CreateDate { get; set; }
        public string EM_ModifiedDate { get; set; }
        public int EM_Version { get; set; }
        public string EM_Nationality { get; set; }
        public DateTime? EM_Passport_Valid_From { get; set; }
        public DateTime? EM_Passport_Valid_Upto { get; set; }
        public int EM_PayrollMonth { get; set; }
        public int EM_PayrollYear { get; set; }
        public string EM_ESI_Applicability { get; set; }
        public int EM_ESI_Out_of_Courage_Month { get; set; }
        public int EM_ESI_Out_of_Courage_Year { get; set; }
        public string EM_PassportIssued_Country { get; set; }
        public string EM_EPFO_Aadhar_Upload { get; set; }
        public string EM_EPFO_Bank_Ac_Upload { get; set; }
        public string EM_EPFO_PAN_Upload { get; set; }
        public string EM_PMRPY { get; set; }
        public string EM_PT_Applicability { get; set; }
        public string EM_NoOf_Certificate { get; set; }
        public DateTime? EM_NoOf_Certificate_Date { get; set; }
        public string EM_TokenNo { get; set; }
        public string EM_Relay_Assigned { get; set; }
        public string EM_Letter_Of_Group { get; set; }
        public string EM_WomenWorkingNightshift { get; set; }
        public string EM_ModeofTransport { get; set; }
        public string EM_SecurityProvided { get; set; }
        public string EM_ExistReasonCode { get; set; }
        public string EM_YearsOfExperience { get; set; }
        public string DateWhenClothesGiven { get; set; }
        public DateTime? EM_NumberandDateOfExemptingOrder { get; set; }
        public string EM_ParticularsOfTransferFromOneGroupToAnother { get; set; }
        public string EM_SalesPromotion { get; set; }
        public string EM_PaymentMode { get; set; }
        public string EM_PermanentAddress { get; set; }
        public string EM_Markof_Identification { get; set; }
        public string EM_Placeof_work { get; set; }
        public string EM_Remarks { get; set; }
        public string EM_EductionLevel { get; set; }
        public string EM_Place_of_Employment { get; set; }
        public string EM_Training_Number { get; set; }
        public DateTime? EM_Training_Date { get; set; }
        public string EM_Emergency_contact_Address { get; set; }
        public string EM_Emergency_contact_mobile_no { get; set; }

    }

    public class EmployeeUpdateSingleUploadModel
    {
        public string EmpID { get; set; }
        public string ClientID { get; set; }
        public string EmpName { get; set; }
        public string State_Code { get; set; }
        public string Location_Code { get; set; }
        public string Branch_Code { get; set; }
        public string PT_State { get; set; }
        public string Gender { get; set; }
        public string FatherName { get; set; }
        public string Relationship { get; set; }
        public DateTime? DOB { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime? DOJ { get; set; }
        public DateTime? EM_DOL { get; set; }
        public string PFNo { get; set; }
        public string ESIC { get; set; }
        public string UAN { get; set; }
        public string Emailid { get; set; }
        public string MobileNo { get; set; }
        public string PAN { get; set; }
        public string Aadhar { get; set; }
        public string BankName { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string SkillCategory { get; set; }
        public string Emp_Status { get; set; }
        public string Bankacnumber { get; set; }
        public string Address { get; set; }
        public string PassportNo { get; set; }
        public string IFSC { get; set; }
        public string Phychlgd { get; set; }
        public string FrstSecndEmp { get; set; }
        public string int_workers { get; set; }
        public string PF_Capping_Applicability { get; set; }

        // Not Mentioned in TL Api Paramters


        public string EmpType { get; set; }
        public DateTime? ChangedFrom { get; set; }

        public DateTime? EM_CreateDate { get; set; }
        public string EM_ModifiedDate { get; set; }
        public int EM_Version { get; set; }
        public string Nationality { get; set; }
        public string PF_Applicability { get; set; }
        public DateTime? PsptVlidFrom { get; set; }
        public DateTime? PsptValidTo { get; set; }
        public int Monthid { get; set; }
        public int Yearid { get; set; }
        public string ESI_Applicability { get; set; }
        public int ESI_Out_of_Courage_Month { get; set; }
        public int ESI_Out_of_Courage_Year { get; set; }
        public string PassportIssued_Country { get; set; }
        public string EPFO_Aadhar_Upload { get; set; }
        public string EPFO_Bank_Ac_Upload { get; set; }
        public string EM_EPFO_PAN_Upload { get; set; }
        public string PMRPY { get; set; }
        public string PT_Applicability { get; set; }
        public string NoOf_Certificate { get; set; }
        public DateTime? NoOf_Certificate_Date { get; set; }
        public string TokenNo { get; set; }
        public string Relay_Assigned { get; set; }
        public string Letter_Of_Group { get; set; }
        public string WomenWorkingNightshift { get; set; }
        public string ModeofTransport { get; set; }
        public string SecurityProvided { get; set; }
        public string EM_ExistReasonCode { get; set; }

        public string YearsOfExperience { get; set; }
        public string DateWhenClothesGiven { get; set; }
        public DateTime? NumberandDateOfExemptingOrder { get; set; }
        public string ParticularsOfTransferFromOneGroupToAnother { get; set; }

        public string SalesPromotion { get; set; }
        public string PaymentMode { get; set; }
        public string PermanentAddress { get; set; }
        public string Markof_Identification { get; set; }
        public string Placeof_work { get; set; }
        public string Remarks { get; set; }
        public string EductionLevel { get; set; }
        public string Place_of_Employment { get; set; }
        public string Training_Number { get; set; }
        public DateTime? Training_Date { get; set; }
        public string Emergency_contact_Address { get; set; }
        public string Emergency_contact_mobile_no { get; set; }
        public int Logged_userid { get; set; }

    }

    public class EmployeeUploadMasterVModel
    {
        public EmployeeUploadMasterVModel()
        {
            list = new List<EmployeeMasterVModel>();
        }
        public List<EmployeeMasterVModel> list { get; set; }
        public string CreatedBy { get; set; }
    }

    public class Model_ClientLocation
    {

        public Nullable<int> AVACOM_BranchID { get; set; }
        public string Branch { get; set; }
        public string BranchType { get; set; }
        public string ClientId { get; set; }
        public string EstablishmentType { get; set; }
        public string OfficeType { get; set; }
        public string ModifiedBy { get; set; } = "Avantis";
        public string EmployerDesignation { get; set; }
        public string CreatedBy { get; set; } = "Avantis";
        public string StateId { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public string PTState { get; set; }
        public string PFCode { get; set; }
        public string ESICCode { get; set; }
        public string LocationAnchor { get; set; }
        public string BranchAddress { get; set; }
        public string EmployerName { get; set; }
        public string ManagerName { get; set; }
        public string EmployerAddress { get; set; }
        public string ManagerAddress { get; set; }
        public Nullable<long> CompanyPhoneNumber { get; set; }
        public string HRContactPerson { get; set; }
        public Nullable<long> HRPhoneNumber { get; set; }
        public string HREmail { get; set; }
        public string HRFirstLevelEmail { get; set; }
        public string HRFirstLevelPhoneNumber { get; set; }
        public string RCNumber { get; set; }
        public string NatureOfBusiness { get; set; }
        public string WorkHoursFrom { get; set; }
        public string WorkHoursTo { get; set; }
        public string IntervalFrom { get; set; }
        public string IntervalTo { get; set; }
        public string WeekOffDays { get; set; }
        public string WorkTimings { get; set; }
        public string LWFState { get; set; }
        public string Municipality { get; set; }
        public string LIN { get; set; }
        public string PermissonMaintainingForms { get; set; }
        public string RequirePowerforFines { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public string ClassificationofEstablishment { get; set; }
        public string LicenceNumber { get; set; }
        public string NICNumber { get; set; }
        public string SectionOfAct { get; set; }
        public string District { get; set; }
        public string Pincode { get; set; }
        public string Jurisdiction { get; set; }
        public Nullable<System.DateTime> RCValidFrom { get; set; }
        public Nullable<System.DateTime> RCValidTo { get; set; }
        public string ActApplicablity { get; set; }
        public string Businesstype { get; set; }

        public int CL_NoOfEmployees { get; set; }
        public Nullable<System.DateTime> CL_CommencementDate { get; set; }
        public Nullable<System.DateTime> CL_MODIFIEDDate { get; set; }
        public string CL_Pincode { get; set; }
        public string CL_District { get; set; }
        public int? CL_IsAventisBranch { get; set; }
        public string CL_BranchCode { get; set; }
        public string CL_BranchEndDate { get; set; }
        public string CL_TradeLicenceApplicability { get; set; }
        public string CL_EmployerDesignation { get; set; }
        public string CL_LocationAnchor { get; set; }
        public string CL_EDLI_Excemption { get; set; }

    }

    public class EmployeeNewUpload
    {
        public string EM_EmpID { get; set; }
        public string EM_ClientID { get; set; }
        public string EM_EmpName { get; set; }
        public string EM_State { get; set; }
        public int EM_Location { get; set; }
        public string EM_Branch { get; set; }
        public string EM_PT_State { get; set; }
        public string EM_Gender { get; set; }
        public string EM_FatherName { get; set; }
        public string EM_Relationship { get; set; }
        public string EM_DOB { get; set; }
        public string EM_MaritalStatus { get; set; }
        public string EM_DOJ { get; set; }
        public string EM_DOL { get; set; }
        public string EM_PFNO { get; set; }
        public string EM_ESICNO { get; set; }
        public string EM_UAN { get; set; }
        public string EM_Emailid { get; set; }
        public string EM_MobileNo { get; set; }
        public string EM_PAN { get; set; }
        public string EM_Aadhar { get; set; }
        public string EM_BankName { get; set; }
        public string EM_Department { get; set; }
        public string EM_Designation { get; set; }
        public string EM_SkillCategory { get; set; }
        public string EM_Bankaccountnumber { get; set; }
        public string EM_IFSC { get; set; }
        public string EM_Address { get; set; }
        public string EM_PassportNo { get; set; }
        public string EM_PhysicallyChallenged { get; set; }
        public string EM_Firsttime_secondtime { get; set; }
        public string EM_International_workers { get; set; }
        public string EM_PF_Capping_Applicability { get; set; }
        public string EM_Employmenttype { get; set; }
        public string EM_ChangeEffective_from { get; set; }
        public string EM_Status { get; set; }
        public string EM_modifiedBy { get; set; }
        public string EM_Nationality { get; set; }
        public string EM_Passport_Valid_From { get; set; }
        public string EM_Passport_Valid_Upto { get; set; }
        public string EM_PayrollMonth { get; set; }
        public string EM_PayrollYear { get; set; }
        public string EM_PF_Applicability { get; set; }
        public object EM_ESI_Applicability { get; set; }
        public string EM_ESI_Out_of_Courage_Month { get; set; }
        public string EM_ESI_Out_of_Courage_Year { get; set; }
        public string EM_PassportIssued_Country { get; set; }
        public string EM_EPFO_Aadhar_Upload { get; set; }
        public string EM_EPFO_Bank_Ac_Upload { get; set; }
        public string EM_EPFO_PAN_Upload { get; set; }
        public string EM_PMRPY { get; set; }
        public string EM_PT_Applicability { get; set; }
        public string EM_NoOf_Certificate { get; set; }
        public string EM_NoOf_Certificate_Date { get; set; }
        public string EM_TokenNo { get; set; }
        public string EM_Relay_Assigned { get; set; }
        public string EM_Letter_Of_Group { get; set; }
        public string EM_WomenWorkingNightshift { get; set; }
        public string EM_ModeofTransport { get; set; }
        public string EM_SecurityProvided { get; set; }
        public string EM_ExistReasonCode { get; set; }
        public string EM_YearsOfExperience { get; set; }
        public string EM_DateWhenClothesGiven { get; set; }
        public string EM_NumberandDateOfExemptingOrder { get; set; }
        public string EM_ParticularsOfTransferFromOneGroupToAnother { get; set; }
        public string EM_SalesPromotion { get; set; }
        public string EM_PaymentMode { get; set; }
        public string EM_PermanentAddress { get; set; }
        public string EM_Markof_Identification { get; set; }
        public string EM_Placeof_work { get; set; }
        public string EM_Remarks { get; set; }
        public string EM_EductionLevel { get; set; }
        public string EM_Place_of_Employment { get; set; }
        public string EM_Training_Number { get; set; }
        public string EM_Training_Date { get; set; }
        public string EM_Emergency_contact_Address { get; set; }
        public string EM_Emergency_contact_mobile_no { get; set; }
        public string EM_Client_ESI_Number { get; set; }
        public string EM_EPS_Applicabilty { get; set; }
        public string EM_Client_PT_State { get; set; }

    }

    public class EmployeeNewUploadModel
    {
        public string EM_EmpID { get; set; }
        public string EM_ClientID { get; set; }
        public string EM_EmpName { get; set; }
        public string EM_State { get; set; }
        public string EM_Location { get; set; }
        public string EM_Branch { get; set; }
        public string EM_PT_State { get; set; }
        public string EM_Gender { get; set; }
        public string EM_FatherName { get; set; }
        public string EM_Relationship { get; set; }
        public Nullable<System.DateTime> EM_DOB { get; set; }
        public string EM_MaritalStatus { get; set; }
        public Nullable<System.DateTime> EM_DOJ { get; set; }
        public Nullable<System.DateTime> EM_DOL { get; set; }
        public string EM_PFNO { get; set; }
        public string EM_ESICNO { get; set; }
        public string EM_UAN { get; set; }
        public string EM_Emailid { get; set; }
        public string EM_MobileNo { get; set; }
        public string EM_PAN { get; set; }
        public string EM_Aadhar { get; set; }
        public string EM_BankName { get; set; }
        public string EM_Department { get; set; }
        public string EM_Designation { get; set; }
        public string EM_SkillCategory { get; set; }
        public string EM_Bankaccountnumber { get; set; }
        public string EM_IFSC { get; set; }
        public string EM_Address { get; set; }
        public string EM_PassportNo { get; set; }
        public string EM_PhysicallyChallenged { get; set; }
        public string EM_Firsttime_secondtime { get; set; }
        public string EM_International_workers { get; set; }
        public string EM_PF_Capping_Applicability { get; set; }
        public string EM_EmploymentType { get; set; }
        public Nullable<System.DateTime> EM_ChangeEffective_From { get; set; }
        public string EM_Status { get; set; }
        public string EM_modifiedBy { get; set; }
        public string EM_Nationality { get; set; }
        public Nullable<System.DateTime> EM_Passport_Valid_From { get; set; }
        public Nullable<System.DateTime> EM_Passport_Valid_Upto { get; set; }
        public Nullable<int> EM_PayrollMonth { get; set; }
        public Nullable<int> EM_PayrollYear { get; set; }
        public string EM_PF_Applicability { get; set; }
        public string EM_ESI_Applicability { get; set; }
        public Nullable<int> EM_ESI_Out_of_Courage_Month { get; set; }
        public Nullable<int> EM_ESI_Out_of_Courage_Year { get; set; }
        public string EM_PassportIssued_Country { get; set; }
        public string EM_EPFO_Aadhar_Upload { get; set; }
        public string EM_EPFO_Bank_Ac_Upload { get; set; }
        public string EM_EPFO_PAN_Upload { get; set; }
        public string EM_PMRPY { get; set; }
        public string EM_PT_Applicability { get; set; }
        public string EM_NoOf_Certificate { get; set; }
        public Nullable<System.DateTime> EM_NoOf_Certificate_Date { get; set; }
        public string EM_TokenNo { get; set; }
        public string EM_Relay_Assigned { get; set; }
        public string EM_Letter_Of_Group { get; set; }
        public string EM_WomenWorkingNightshift { get; set; }
        public string EM_ModeofTransport { get; set; }
        public string EM_SecurityProvided { get; set; }
        public string EM_ExistReasonCode { get; set; }
        public string EM_YearsOfExperience { get; set; }
        public string EM_DateWhenClothesGiven { get; set; }
        public string EM_NumberandDateOfExemptingOrder { get; set; }
        public string EM_ParticularsOfTransferFromOneGroupToAnother { get; set; }
        public string EM_SalesPromotion { get; set; }
        public string EM_PaymentMode { get; set; }
        public string EM_PermanentAddress { get; set; }
        public string EM_Markof_Identification { get; set; }
        public string EM_Placeof_work { get; set; }
        public string EM_Remarks { get; set; }
        public string EM_EductionLevel { get; set; }
        public string EM_Place_of_Employment { get; set; }
        public string EM_Training_Number { get; set; }
        public Nullable<System.DateTime> EM_Training_Date { get; set; }
        public string EM_Emergency_contact_Address { get; set; }
        public string EM_Emergency_contact_mobile_no { get; set; }
        public string EM_EPS_Applicabilty { get; set; }
        public string EM_Client_ESI_Number { get; set; }
        public string EM_Client_PT_State { get; set; }
		
		public Nullable<bool> EM_IsLwf_Exempted { get; set; }
        public string EM_ExemptedSEA_Act { get; set; }
        public string EM_VPF_Applicability { get; set; }
        public string EM_VPF_Type { get; set; }
        
        public int? EM_VPF_Value { get; set; }
        public int? EM_Sectorid { get; set; }
        public int? EM_JobCategory { get; set; }
        public int? EM_IndustryType { get; set; }
    }


    public class WorkHourLessThanAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public WorkHourLessThanAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var currentValue = DateTime.Parse(Convert.ToString(value));

            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);

            if (property == null)
                throw new ArgumentException("Property with this name not found");

            var comparisonValue = DateTime.Parse(Convert.ToString(property.GetValue(validationContext.ObjectInstance)));

            if (currentValue >= comparisonValue)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }


    public class IntervalHourMoreThanAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;
        public IntervalHourMoreThanAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var currentValue = DateTime.Parse(Convert.ToString(value));
            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);
            if (property == null)
                throw new ArgumentException("Property with this name not found");
            var comparisonValue = DateTime.Parse(Convert.ToString(property.GetValue(validationContext.ObjectInstance)));
            if (currentValue < comparisonValue)
                return new ValidationResult(ErrorMessage);
            return ValidationResult.Success;
        }
    }

    public class IntervalLessThanAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public IntervalLessThanAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var currentValue = DateTime.Parse(Convert.ToString(value));

            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);

            if (property == null)
                throw new ArgumentException("Property with this name not found");

            var comparisonValue = DateTime.Parse(Convert.ToString(property.GetValue(validationContext.ObjectInstance)));

            if (currentValue >= comparisonValue)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }

    public class RequiredOnEstablishmentTypeAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public RequiredOnEstablishmentTypeAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var currentValue = Convert.ToString(value);

            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);

            if (property == null)
                throw new ArgumentException("Property with this name not found");

            var comparisonValue = Convert.ToString(property.GetValue(validationContext.ObjectInstance));

            if ((comparisonValue == "FACT" || comparisonValue == "SF") && currentValue == "")
                return new ValidationResult(ErrorMessage);


            return ValidationResult.Success;
        }
    }

    public class RCValidAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public RCValidAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            if (!string.IsNullOrEmpty(Convert.ToString(value)))
            {
                // var currentValue = DateTime.Parse(Convert.ToString(value));
                DateTime currentValue = DateTime.ParseExact(Convert.ToString(value), "dd/MM/yyyy", null);
                var property = validationContext.ObjectType.GetProperty(_comparisonProperty);

                if (property == null)
                    throw new ArgumentException("Property with this name not found");
                try
                {
                    //  var comparisonValue = DateTime.Parse(Convert.ToString(property.GetValue(validationContext.ObjectInstance)));
                    var comparisonValue = (Convert.ToString(property.GetValue(validationContext.ObjectInstance)));
                    DateTime dtcomparisonValue = DateTime.ParseExact(comparisonValue, "dd/MM/yyyy", null);
                    if (currentValue > dtcomparisonValue)
                        return new ValidationResult(ErrorMessage);
                }
                catch (Exception ex)
                {
                }
            }
            return ValidationResult.Success;
        }
    }



    public class ClientModel
    {
        //[Required]
        [Display(Name = "Client ID")]
        public string CM_ClientID { get; set; }
        public string CM_ClientName { get; set; }
    }

    public class StandardColumnModel
    {
        //[Required]
        [Display(Name = "Standard Column")]
        public string CPMD_Standard_Column { get; set; }
        public string Standard_Column_Name { get; set; }
    }
    public class PaycodeMasterModel
    {
        //[Required]
        [Display(Name = "Pay Code")]
        public string PEM_Pay_Code { get; set; }
        [Display(Name = "Pay Code Description")]
        public string PEM_Pay_Code_Description { get; set; }
    }


    public class PaycodeMappingModel
    {
        public PaycodeMappingModel()
        {
            ClientModel = new ClientModel();
            Clients = new List<ClientModel>();
            StandardColumns = new List<StandardColumnModel>();
            StandardColumnModel = new StandardColumnModel();
            //PaycodeMasterModel = new PaycodeMasterModel();
            PaycodeMasterModellist = new List<PaycodeMasterModel>();
            FileName = FileName;
            ExcelErrors = ExcelErrors;

        }

        public bool SheetNameError { get; set; }
        
        public bool Exception { get; set; }
        public bool Message { get; set; }
        public ClientModel ClientModel { get; set; }
        public List<ClientModel> Clients { get; set; }
        public List<StandardColumnModel> StandardColumns { get; set; }
        public StandardColumnModel StandardColumnModel { get; set; }
        public PaycodeMasterModel PaycodeMasterModel { get; set; }
        public List<PaycodeMasterModel> PaycodeMasterModellist { get; set; }
        [Display(Name = "Customer ID")]
        public int AVACOM_CustomerID { get; set; }
        [Required(ErrorMessage = "Please select Client")]
        [Display(Name = "Client ID")]
        public string CPMD_ClientID { get; set; }

        [Display(Name = "Deduction Type")]
        public string Deduction_Type { get; set; }
        [Display(Name = "Pay Code")]
        public string CPMD_PayCode { get; set; }
        [Required(ErrorMessage = "Please select Pay Group")]
        [Display(Name = "Pay Group")]
        public string CPMD_PayGroup { get; set; }
        
        [Display(Name = "Standard Column")]
        public string CPMD_Standard_Column { get; set; }
        [Required(ErrorMessage = "Please enter Header Name")]
        [Display(Name = "Header")]
        public string CPMD_Header { get; set; }
        [Display(Name = "Data Type")]
        public string Column_DataType { get; set; }
        [Required(ErrorMessage = "Please enter Sequence Order")]
        [Display(Name = "Sequence Order")]
        public int CPMD_Sequence_Order { get; set; }
        [Display(Name = "CustomerID")]
        public string CPMD_Deduction_Type { get; set; }
        [Display(Name = "Status")]
        public string CPMD_Status { get; set; }
        [Display(Name = "Applicable For ESI")]
        public string CPMD_appl_ESI { get; set; }
        [Display(Name = "Applicable For PT")]
        public string CPMD_Appl_PT { get; set; }
        [Display(Name = "Applicable For LWF")]
        public string CPMD_Appl_LWF { get; set; }
        [Display(Name = "Applicable For PF")]
        public string CPMD_appl_PF { get; set; }

        public string FileName { get; set; }
        public bool ExcelErrors { get; set; }
    }


    public class UploadPaycodeVModel
    {
        public UploadPaycodeVModel()
        {
            PaycodeMappingModel = new PaycodeMappingModel();
            //ClientLocationVModel = new ClientLocationVModel();
        }
        public bool ClientCheck { get; set; }
        public ClientLocationVModel ClientLocationVModel { get; set; }
        public bool LocationCheck { get; set; }
        public bool Message { get; set; } = false;
        public bool ServerError { get; set; }
        public bool ExtensionError { get; set; }
        public bool Error { get; set; }
        public PaycodeMappingModel PaycodeMappingModel { get; set; }
        public string ClientID { get; set; }
    }


    public class EmployeeCtcVModel
    {
        public EmployeeCtcVModel()
        {
            CustomerModel = new CustomerVModel();
            Customers = new List<CustomerVModel>();

            ClientModel = new ClientVModel();
            Clients = new List<ClientVModel>();
        }

        public CustomerVModel CustomerModel { get; set; }
        public List<CustomerVModel> Customers { get; set; }

        public ClientVModel ClientModel { get; set; }
        public List<ClientVModel> Clients { get; set; }

        public bool Error { get; set; }
        public string ErrorMsg { get; set; }
        public bool Success { get; set; }
    }
    public class CustomerVModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class ClientVModel
    {
        public string CM_ClientID { get; set; }
        public string CM_ClientName { get; set; }
    }

    public class OutputResult
    {
        public string colname { get; set; }
        public string ErrorMessage { get; set; }
        public int rownum { get; set; }
    }

    public class unprocessedList
    {
        public long ECS_Id { get; set; }
        public string ECS_Client_Id { get; set; }
        public string ECS_Emp_Id { get; set; }
        public double ECS_GrossPay { get; set; }
        public DateTime ECS_EffectiveDate { get; set; }
        public string ECS_CreatedBy { get; set; }
        public long ECSD_Id { get; set; }
        public string ECSD_ECS_Id { get; set; }
        public string ECSD_PayCode { get; set; }
        public double ECSD_Amount { get; set; }
        public string ECSD_Remarks { get; set; }
        public int fileid { get; set; }
    }

    public class Model_Ctc_POST
    {
        public Model_Ctc_POST()
        {
            CtcDetails = new List<Model_Ctc_Details_POST>();
        }
        public long ECS_Id { get; set; }
        public string ECS_Emp_Id { get; set; }
        public string ECS_Client_Id { get; set; }
        public Nullable<double> ECS_GrossPay { get; set; }
        public Nullable<System.DateTime> ECS_EffectiveDate { get; set; }
        public string ECS_CreatedBy { get; set; }
        public List<Model_Ctc_Details_POST> CtcDetails { get; set; }
    }

    public class Model_Ctc_Details_POST
    {
        public long ECSD_Id { get; set; }
        public string ECSD_ECS_Id { get; set; }
        public string ECSD_PayCode { get; set; }
        public string ECSD_Remarks { get; set; }
        public Nullable<double> ECSD_Amount { get; set; }
    }
    public class RLCS_POSTAPI_Response
    {
        public string Status { get; set; }
        public int StatusCode { get; set; }
        public int EmpMaster_insertstatus { get; set; }
    }
    #region ADD ENTITY_LOCATION
    public class UploadEntityLocationDocumentVModel
    {
        public int ServiceProviderID { get; set; }

        public bool ISEntity { get; set; }
        public bool ISLocation { get; set; }
        public List<ClientHeaderMappingVModel> ClientHeaderMappingVModelList { get; set; }
        public ClientHeaderMappingVModel ClientHeaderMappingVModel { get; set; }
        public string File { get; set; }
        public bool EmployeeCheck { get; set; }
        public bool Message { get; set; }
        public bool ServerError { get; set; }
        public bool MappingMessage { get; set; }
        public bool Error { get; set; }
        public string FileName { get; set; }
        public bool ExcelErrors { get; set; }
        public int CustomerID { get; set; }

        public int UserID { get; set; }
        public bool RowCount { get; set; }
        public bool ColumnCount { get; set; }

        public UploadEntityLocationDocumentVModel()
        {
            ClientHeaderMappingVModelList = new List<ClientHeaderMappingVModel>();
            ClientHeaderMappingVModel = new ClientHeaderMappingVModel();
        }
    }
    #endregion
    #region Employee Branch Transfer
    public class CreateEmployeeBranchTransferVModel
    {
        public CreateEmployeeBranchTransferVModel()
        {
            
            VMStatesTB = new List<Models.StateVModel>();
            
            VMLocationTB = new List<Models.LocationVModel>();
            
            VMBranchTB = new List<Models.BranchVModel>();
        }
        public int EM_ID { get; set; }
        public int CustomerID { get; set; }
        public bool Message { get; set; }
        public bool ServerError { get; set; }
        public string EMPID { get; set; }
        public string ClientID { get; set; }
        public int AVACOM_BranchID { get; set; }
        public string ExcludeStateCode { get; set; }
        public string ExcludeLocationCode { get; set; }
        public List<BranchVModel> VMBranchTB { get; set; }
        public List<StateVModel> VMStatesTB { get; set; }
        public List<LocationVModel> VMLocationTB { get; set; }

        [Display(Name = "Last Working Date")]
        [Required(ErrorMessage = "Last Working Date is required")]
        public string EMT_LWD { get; set; }
        public string hidBranchName { get; set; }

        [Display(Name = "Date of Joining")]
        [Required(ErrorMessage = "Date of Joining is required")]
        public string EMT_DOJ { get; set; }
         public string EM_DOJ { get; set; }
        public string StateNameCB { get; set; }
        [Required(ErrorMessage = "Transfer State is required")]
        public string EMT_SM_Code { get; set; }

        public string LMNameCB { get; set; }
        [Required(ErrorMessage = "Transfer Location is required")]
        public string EMT_LM_Code { get; set; }

        public string BranchNameCB { get; set; }
        [Required(ErrorMessage = "Transfer Branch is required")]
        public string EMT_Branch { get; set; }

    }
   #endregion
    }