﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
    
    public class QuestionModalEntry
    {
        public List<QuestionModal> customerassignments { get; set; }
    }

    public class QuestionEntry
    {
        public List<QuestionMasterModal> questions { get; set; }
    }
    public class ActAssignmentEntry
    {
        public List<ImplementationActAssignment> actassignments { get; set; }
    }
    public class QACMappingEntry
    {
        public List<QAC_Mapping> Approvals { get; set; }
    }
    public class QAMappingEntry
    {
        public List<QA_Mapping> Approvals { get; set; }
    }
    public class QACMappingEntryUpdate
    {
        public List<QAC_MappingUpdate> BulkUpdates { get; set; }
    }
    public class QACRMappingEntryUpdate
    {
        public List<QACR_MappingUpdate> BulkUpdates { get; set; }
    }
    public class QAMappingEntryUpdate
    {
        public List<QA_MappingUpdate> BulkUpdates { get; set; }
    }

}