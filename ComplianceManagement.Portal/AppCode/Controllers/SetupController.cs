﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nelibur.ObjectMapper;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.IO;
using System.Net;
using OfficeOpenXml;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Globalization;
using System.Net.Http.Headers;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Data;
using OfficeOpenXml.Style;
//using Spire.Pdf.General.Render.ColorSpace;
using System.Drawing;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using static com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models.ClientDetailsVModel;
using System.Xml;
using System.Web.Services;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class SetupController : Controller
    {
        // GET: Setup

        #region Client Basic Details
        [HttpGet]
        public ActionResult CreateClientBasicSetup(string ID, string getMaster)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                ViewData["ButtonType"] = "Edit";

                if (!string.IsNullOrEmpty(getMaster))
                    if (getMaster.Equals("Yes"))
                        ViewData["ButtonType"] = "NotEdit";

                BasicSetupModel BasicSetup = new BasicSetupModel();
                StateVModel CityVM = new StateVModel();
                List<RLCS_Location_City_Mapping> LocationList = new List<RLCS_Location_City_Mapping>();
                List<RLCS_State_Mapping> StateList = new List<RLCS_State_Mapping>();

                DateTime dt = new DateTime();

                try
                {
                    RLCS_Client_BasicDetails record_ClientBasicDetails = null;

                    StateList = RLCS_ClientsManagement.GetAllStates();

                    var clientEntityDetails = RLCS_ClientsManagement.GetClientInfoByID(Convert.ToInt32(ID), "E");

                    if (clientEntityDetails != null)
                    {
                        BasicSetup.hdnClientID = clientEntityDetails.CM_ClientID;
                        BasicSetup.CM_ClientID = clientEntityDetails.CM_ClientID;

                        if (clientEntityDetails.AVACOM_BranchID != null)
                        {
                            BasicSetup.AVACOM_BranchID = (int)clientEntityDetails.AVACOM_BranchID;
                        }

                        BasicSetup.CO_CorporateID = clientEntityDetails.CO_CorporateID;
                        BasicSetup.CM_ClientName = clientEntityDetails.CM_ClientName;
                        BasicSetup.CM_EstablishmentType = clientEntityDetails.CM_EstablishmentType;

                        if (clientEntityDetails.CM_ServiceStartDate != null)
                        {
                            dt = Convert.ToDateTime(clientEntityDetails.CM_ServiceStartDate);
                            BasicSetup.CM_ServiceStartDate = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }

                        BasicSetup.StateModel.AVACOM_StateID = RLCS_ClientsManagement.GetStateidBycode(clientEntityDetails.CM_State);
                        if (BasicSetup.StateModel.AVACOM_StateID != 0)
                            LocationList = RLCS_ClientsManagement.GetAllLocations();
                        //  cityList = RLCS_ClientsManagement.GetCities(Convert.ToString(BasicSetup.StateModel.AVACOM_StateID));
                        if (LocationList != null && LocationList.Count > 0)
                        {
                            TinyMapper.Bind<List<RLCS_Location_City_Mapping>, List<LocationVModel>>();
                            BasicSetup.Locations = TinyMapper.Map<List<LocationVModel>>(LocationList);
                            var citycode = clientEntityDetails.CM_City;
                            BasicSetup.LocationVModel.LM_Code = clientEntityDetails.CM_City;
                            BasicSetup.CM_City = RLCS_ClientsManagement.GetCityIDByCode(clientEntityDetails.CM_City);
                            //BasicSetup.CityList.AddRange(cityList);
                        }

                        BasicSetup.CM_Status = clientEntityDetails.CM_Status;
                        BasicSetup.CM_Address = clientEntityDetails.CM_Address;

                        if (clientEntityDetails.CM_BonusPercentage != null)
                            BasicSetup.CM_BonusPercentage = clientEntityDetails.CM_BonusPercentage.HasValue ? clientEntityDetails.CM_BonusPercentage.Value : 0;

                        BasicSetup.CM_Excemption = clientEntityDetails.CM_Excemption.HasValue ? clientEntityDetails.CM_Excemption.Value : false;
                        BasicSetup.ActApplicablity = clientEntityDetails.CM_ActType;

                        if (clientEntityDetails.CM_ClientID != null)
                            record_ClientBasicDetails = RLCS_ClientsManagement.GetClientBasicByID(Convert.ToInt32(ID), clientEntityDetails.CM_ClientID);

                        if (record_ClientBasicDetails != null)
                        {
                            BasicSetup.ClientDetailsVModel.CB_ID = record_ClientBasicDetails.CB_ID;
                            BasicSetup.ClientDetailsVModel.CB_ClientID = record_ClientBasicDetails.CB_ClientID;
                            BasicSetup.ClientDetailsVModel.CB_PaymentDate = record_ClientBasicDetails.CB_PaymentDate;
                            BasicSetup.ClientDetailsVModel.CB_WagePeriodFrom = record_ClientBasicDetails.CB_WagePeriodFrom;
                            BasicSetup.ClientDetailsVModel.CB_WagePeriodTo = record_ClientBasicDetails.CB_WagePeriodTo;

                            if (record_ClientBasicDetails.CB_DateOfCommencement != null)
                            {
                                dt = Convert.ToDateTime(record_ClientBasicDetails.CB_DateOfCommencement);
                                BasicSetup.ClientDetailsVModel.CB_DateOfCommencement = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                            BasicSetup.ClientDetailsVModel.CB_ServiceTaxExmpted = record_ClientBasicDetails.CB_ServiceTaxExmpted;
                            BasicSetup.ClientDetailsVModel.CB_EDLIExcemption = record_ClientBasicDetails.CB_EDLIExemption;
                            BasicSetup.ClientDetailsVModel.CB_ActType = record_ClientBasicDetails.CB_ActType;
                            BasicSetup.ClientDetailsVModel.PFCode = record_ClientBasicDetails.CB_PF_Code;
                        }
                    }

                    if (StateList != null && StateList.Count > 0)
                    {
                        TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                        BasicSetup.States = TinyMapper.Map<List<StateVModel>>(StateList);

                        //BasicSetup.CityList.AddRange(cityList);
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    BasicSetup.Exception = true;
                }

                return View(BasicSetup);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateClientBasicSetup(BasicSetupModel Setup, string Save)
        {
            ViewData["ButtonType"] = "Edit";

            RLCS_CustomerBranch_ClientsLocation_Mapping rlcs_CustomerBranch = new RLCS_CustomerBranch_ClientsLocation_Mapping();

            StateVModel CityVM = new StateVModel();

            int AVACOM_CustBranchID = 0;
            int cityID = 0;

            List<RLCS_State_Mapping> StateList = new List<RLCS_State_Mapping>();
            jsonClientDetails jsonClientDetails = new jsonClientDetails();

            try
            {
                if (ModelState.IsValid)
                {
                    TinyMapper.Bind<BasicSetupModel, RLCS_CustomerBranch_ClientsLocation_Mapping>(config =>
                    {
                        config.Ignore(source => source.CM_ServiceStartDate);
                    });

                    rlcs_CustomerBranch = TinyMapper.Map<RLCS_CustomerBranch_ClientsLocation_Mapping>(Setup);

                    rlcs_CustomerBranch.CO_CorporateID = Setup.CO_CorporateID;
                    rlcs_CustomerBranch.CM_ClientID = Setup.CM_ClientID;
                    rlcs_CustomerBranch.AVACOM_BranchName = Setup.CM_ClientName;

                    if (!String.IsNullOrEmpty(Setup.CM_City))
                        cityID = Convert.ToInt32(Setup.CM_City);

                    Setup.LocationVModel.LM_Code = RLCS_ClientsManagement.GetCityCodeByID(cityID);

                    rlcs_CustomerBranch.CM_City = Setup.LocationVModel.LM_Code;
                    rlcs_CustomerBranch.CM_Status = Setup.CM_Status;
                    rlcs_CustomerBranch.CM_Excemption = Setup.CM_Excemption;

                    //jsonClientDetails.Excemption = Setup.CM_Excemption;
                    jsonClientDetails.excemption = Setup.CM_Excemption;

                    rlcs_CustomerBranch.CM_ActType = Setup.ActApplicablity;

                    if (Setup.CM_ServiceStartDate != null)
                    {
                        rlcs_CustomerBranch.CM_ServiceStartDate = DateTimeExtensions.GetDate(Setup.CM_ServiceStartDate);
                    }

                    if (rlcs_CustomerBranch.AVACOM_BranchID != null)
                        AVACOM_CustBranchID = (int)rlcs_CustomerBranch.AVACOM_BranchID;

                    if (AVACOM_CustBranchID != 0)
                        rlcs_CustomerBranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetCustomerIDByCustomerBranchID(AVACOM_CustBranchID);
                    else
                        rlcs_CustomerBranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetAVACOMCustIDByCorpID(rlcs_CustomerBranch.CO_CorporateID);

                    bool saveSuccess_rlcsCustomerBranch = RLCS_ClientsManagement.UpdateClientInfo(rlcs_CustomerBranch, Setup.StateModel.AVACOM_StateID, Setup.LocationVModel.LM_Code);

                    if (saveSuccess_rlcsCustomerBranch)
                    {
                        RLCS_Client_BasicDetails record_RLCSClientBasicDetails = new RLCS_Client_BasicDetails();

                        if (rlcs_CustomerBranch.AVACOM_BranchID != null)
                            record_RLCSClientBasicDetails.AVACOM_BranchID = AVACOM_CustBranchID;

                        record_RLCSClientBasicDetails.CB_ClientID = Setup.CM_ClientID;
                        record_RLCSClientBasicDetails.CB_PaymentDate = Setup.ClientDetailsVModel.CB_PaymentDate;
                        record_RLCSClientBasicDetails.CB_WagePeriodFrom = Setup.ClientDetailsVModel.CB_WagePeriodFrom;
                        record_RLCSClientBasicDetails.CB_WagePeriodTo = Setup.ClientDetailsVModel.CB_WagePeriodTo;

                        if (Setup.ClientDetailsVModel.CB_DateOfCommencement != null)
                        {
                            record_RLCSClientBasicDetails.CB_DateOfCommencement = DateTimeExtensions.GetDate(Setup.ClientDetailsVModel.CB_DateOfCommencement);
                        }

                        record_RLCSClientBasicDetails.CB_ServiceTaxExmpted = Setup.ClientDetailsVModel.CB_ServiceTaxExmpted;
                        record_RLCSClientBasicDetails.CB_EDLIExemption = Setup.ClientDetailsVModel.CB_EDLIExcemption;

                        record_RLCSClientBasicDetails.CM_PFCode = Setup.ClientDetailsVModel.PFCode;
                        record_RLCSClientBasicDetails.CB_ActType = Setup.ActApplicablity;
                        record_RLCSClientBasicDetails.CB_PF_Code = Setup.ClientDetailsVModel.PFCode;

                        bool saveSuccess = RLCS_ClientsManagement.UpdateClientBasicInfo(record_RLCSClientBasicDetails);

                        if (saveSuccess)
                        {
                            TinyMapper.Bind<ClientDetailsVModel, jsonClientDetails>();

                            jsonClientDetails = TinyMapper.Map<jsonClientDetails>(Setup);

                            jsonClientDetails.ClientId = Setup.CM_ClientID;
                            jsonClientDetails.CorporateId = Setup.CO_CorporateID;
                            jsonClientDetails.ClientName = Setup.CM_ClientName;
                            jsonClientDetails.AgreementID = null;
                            jsonClientDetails.ActApplicabilty = Setup.ActApplicablity;
                            jsonClientDetails.Mandate = "R";
                            jsonClientDetails.EstablishmentType = Setup.CM_EstablishmentType;
                            jsonClientDetails.PFCode = Setup.ClientDetailsVModel.PFCode;

                            if (!String.IsNullOrEmpty(Setup.CM_ServiceStartDate))
                                jsonClientDetails.ServiceStartDate = DateTimeExtensions.GetDate(Setup.CM_ServiceStartDate);

                            if (Setup.StateModel.AVACOM_StateID != 0)
                            {
                                jsonClientDetails.State = RLCS_Master_Management.GetStateCodeByStateID(Setup.StateModel.AVACOM_StateID);
                            }

                            if (Setup.LocationVModel.LM_Code != null)
                            {
                                jsonClientDetails.City = Setup.LocationVModel.LM_Code;
                            }

                            jsonClientDetails.Address = Setup.CM_Address;
                            jsonClientDetails.Status = Setup.CM_Status;
                            jsonClientDetails.BonusPercentage = Setup.CM_BonusPercentage;
                            jsonClientDetails.WagePeriodFrom = Setup.ClientDetailsVModel.CB_WagePeriodFrom;
                            jsonClientDetails.WagePeriodTo = Setup.ClientDetailsVModel.CB_WagePeriodTo;
                            jsonClientDetails.PaymentDate = Setup.ClientDetailsVModel.CB_PaymentDate;
                            jsonClientDetails.ServiceTaxExcempted = Setup.ClientDetailsVModel.CB_ServiceTaxExmpted;

                            if (Setup.ClientDetailsVModel.CB_DateOfCommencement != null)
                                jsonClientDetails.DateOfCommencement = DateTimeExtensions.GetDate(Setup.ClientDetailsVModel.CB_DateOfCommencement);

                            jsonClientDetails.EDLIExcemption = Setup.ClientDetailsVModel.CB_EDLIExcemption;
                            jsonClientDetails.Status = Setup.CM_Status;
                            jsonClientDetails.CreatedBy = "Avantis";
                            jsonClientDetails.ModifiedBy = "Avantis";

                            if (saveSuccess == true && saveSuccess_rlcsCustomerBranch == true)
                            {
                                bool apiSuccess = ClientBasicApiCall(jsonClientDetails);

                                if (apiSuccess)
                                {
                                    Setup.Message = true;
                                    RLCS_ClientsManagement.Update_ProcessedStatus_ClientBasicDetail(Setup.AVACOM_BranchID, Setup.CM_ClientID, apiSuccess);
                                }

                                StateList = RLCS_ClientsManagement.GetAllStates();
                                TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                                Setup.States = TinyMapper.Map<List<StateVModel>>(StateList);
                                return View(Setup);
                            }
                            else
                            {
                                StateList = RLCS_ClientsManagement.GetAllStates();
                                TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                                Setup.States = TinyMapper.Map<List<StateVModel>>(StateList);
                            }
                        }
                    }
                }
                else
                {
                    StateList = RLCS_ClientsManagement.GetAllStates();
                    TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                    Setup.States = TinyMapper.Map<List<StateVModel>>(StateList);
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                Setup.Exception = true;
            }

            return View(Setup);
        }

        public ActionResult CreateClientLocationSetup(string ID, string getMaster, string Edit)
        {
            ClientLocationVModel ClientLocationVModel = new ClientLocationVModel();
            List<RLCS_State_Mapping> StateList = new List<RLCS_State_Mapping>();
            List<RLCS_State_Mapping> PTStateList = new List<RLCS_State_Mapping>();
            List<RLCS_Location_City_Mapping> LocationList = new List<RLCS_Location_City_Mapping>();
            List<RLCS_Designation_Master> desig = new List<RLCS_Designation_Master>();
            List<SP_GetAllAnchor_Result> LocationAnchorList = new List<SP_GetAllAnchor_Result>();
            DateTime dt = new DateTime();

            try
            {
                ViewData["ButtonType"] = "Edit";

                //if (getMaster!="" && getMaster != null)
                //    ViewData["Edit"] = "Yes";
                //else
                //    ViewData["Edit"] = "No";

                if (!string.IsNullOrEmpty(getMaster))
                    if (getMaster.Equals("Yes"))
                        ViewData["ButtonType"] = "NotEdit";

                if (!string.IsNullOrEmpty(Edit))
                    if (Edit.Equals("YES"))
                        ViewData["Edit"] = "Yes";
                    else
                        ViewData["Edit"] = "No";

                if (!string.IsNullOrEmpty(ID))
                {
                    ClientLocationVModel.AVACOM_BranchID = Convert.ToInt32(ID);
                    var Client = RLCS_ClientsManagement.GetClientInfoByID(Convert.ToInt32(ID), "B");
                    if (Edit != null)
                    {
                        if (Edit.Equals("NewBranch"))
                            Client = null;
                    }
                    if (Client != null)
                    {
                        TinyMapper.Bind<RLCS_CustomerBranch_ClientsLocation_Mapping, ClientLocationVModel>();
                        ClientLocationVModel = TinyMapper.Map<ClientLocationVModel>(Client);
                        if (ClientLocationVModel != null)
                        {
                            ClientLocationVModel.CorporateID = Client.CO_CorporateID;
                            ClientLocationVModel.AVACOM_BranchID = (int)Client.AVACOM_BranchID;
                            ClientLocationVModel.CM_ClientID = Client.CM_ClientID;
                            ClientLocationVModel.CL_NatureOfBusiness = Client.CL_NatureofBusiness;
                            ClientLocationVModel.CL_RequirePowerforFines = Client.CL_RequirePowerforFines;
                            ClientLocationVModel.CL_Status = Client.CM_Status;
                            ClientLocationVModel.CL_Permission_MaintaingForms = Client.CL_PermissionMaintainingForms;
                            ClientLocationVModel.CL_BusinessType = Client.CL_BusinessType;
                            ClientLocationVModel.CL_ESIC_Code = Client.CL_ESIC_Code;
                            ClientLocationVModel.CL_EmployerName = Client.CL_EmployerName;
                            ClientLocationVModel.CL_EmployerAddress = Client.CL_EmployerAddress;
                            ClientLocationVModel.CL_HR1stLevelMail = Client.CL_HR1stLevelMail;
                            ClientLocationVModel.CL_HR1stLevelPhNo = Client.CL_HR1stLevelPhNo;
                            ClientLocationVModel.CL_HRContactPerson = Client.CL_HRContactPerson;
                            ClientLocationVModel.CL_HRMailID = Client.CL_HRMailID;
                            ClientLocationVModel.CL_SectionofAct = Client.CL_SectionofAct;
                            if (Client.CL_HRPhNo.HasValue)
                                ClientLocationVModel.CL_HRPhNo = Convert.ToString(Client.CL_HRPhNo);
                            ClientLocationVModel.CL_District = Client.CL_District;
                            ClientLocationVModel.CL_ESIC_Code = Client.CL_ESIC_Code;
                            if (Client.CL_CommencementDate.HasValue)
                            {
                                dt = Convert.ToDateTime(Client.CL_CommencementDate);
                                ClientLocationVModel.CL_CommencementDate = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (Client.CL_RC_ValidFrom.HasValue)
                            {
                                dt = Convert.ToDateTime(Client.CL_RC_ValidFrom);
                                ClientLocationVModel.CL_RCValidFrom = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                            }
                            if (Client.CL_RC_ValidTo.HasValue)
                            {
                                dt = Convert.ToDateTime(Client.CL_RC_ValidTo);
                                ClientLocationVModel.CL_RCValidTo = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                            ClientLocationVModel.CL_LIN = Client.CL_LIN;
                            ClientLocationVModel.CL_NICCode = Client.CL_NICCode;
                            ClientLocationVModel.CL_PF_Code = Client.CL_PF_Code;
                            ClientLocationVModel.CL_Pincode = Client.CL_Pincode;
                            ClientLocationVModel.CL_RCNo = Client.CL_RCNO;

                            ClientLocationVModel.CL_SectionofAct = Client.CL_SectionofAct;
                            ClientLocationVModel.CL_ClasssificationofEstabilishment = Client.CL_ClassificationofEstablishment;
                            ClientLocationVModel.CL_Pincode = Client.CL_Pincode;
                            ClientLocationVModel.CL_BusinessType = Client.CL_BusinessType;
                            ClientLocationVModel.CL_OfficeType = Client.CL_OfficeType;
                            ClientLocationVModel.CL_Permission_MaintaingForms = Client.CL_PermissionMaintainingForms;
                            ClientLocationVModel.CL_LicenceNo = Client.CL_LicenceNo;
                            ClientLocationVModel.CL_IntervalsFrom = Client.CL_IntervalsFrom;
                            ClientLocationVModel.CL_IntervalsTo = Client.CL_IntervalsTo;
                            ClientLocationVModel.CL_WorkHoursFrom = Client.CL_WorkHoursFrom;
                            ClientLocationVModel.CL_WorkHoursTo = Client.CL_WorkHoursTo;
                            ClientLocationVModel.CL_Jurisdiction = Client.CL_Juridiction;
                            ClientLocationVModel.CL_WorkTimings = Client.CL_WorkTimings;
                            ClientLocationVModel.CL_WeekoffDay = Client.CL_weekoffDay;
                            if (Client.AVACOM_BranchID.HasValue)
                                ClientLocationVModel.AVACOM_BranchID = (int)Client.AVACOM_BranchID;
                            ClientLocationVModel.CM_ClientName = RLCS_ClientsManagement.GetClientID(ClientLocationVModel.CM_ClientID);

                            ClientLocationVModel.CM_City = Client.CM_City;
                            ClientLocationVModel.CL_Status = Client.CM_Status;
                            ClientLocationVModel.NoofEmployees = (Client.NoofEmployees == null ? 0 : (int)Client.NoofEmployees);
                            ClientLocationVModel.CL_BranchCode = Client.CL_BranchCode;
                            ClientLocationVModel.CL_LocationAnchor = Client.CL_LocationAnchor;
                            ClientLocationVModel.CL_EmployerDesignation = Client.CL_EmployerDesignation;
                            ClientLocationVModel.CL_TradeLicenceApplicability = Client.CL_TradeLicenceApplicability;
                            ClientLocationVModel.CL_BranchEndDate = Client.CL_BranchEndDate;
                            ClientLocationVModel.CL_EDLI_Excemption = Client.CL_EDLI_Excemption;
							if (Client.CL_CommencementDate.HasValue)
                            {
                                dt = Convert.ToDateTime(Client.CL_CommencementDate);
                                ClientLocationVModel.CL_BranchStartDate = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                        }
                    }
                }
                if (!string.IsNullOrEmpty(ID))
                {
                    if (ClientLocationVModel.CM_ClientName == "" || ClientLocationVModel.CM_ClientName == null)
                        ClientLocationVModel.CM_ClientName = RLCS_ClientsManagement.GetClientNameByParentID(Convert.ToInt32(ID));
                }
                StateList = RLCS_ClientsManagement.GetAllStates();
                PTStateList = RLCS_ClientsManagement.GetAllPTStates();
                LocationList = RLCS_ClientsManagement.GetAllLocations();
              
                int? CustomerID = RLCS_ClientsManagement.GetCustomerIDByCustomerBranchID(Convert.ToInt32(ID));
                ClientLocationVModel.ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(CustomerID));
                //int BranchID;
                //BranchID = RLCS_ClientsManagement.GetParentBranchID(Convert.ToInt32(ID));
                //if (string.IsNullOrEmpty(Convert.ToString(BranchID))|| BranchID == 0)
                //{
                //    BranchID = Convert.ToInt32(ID);
                //}
                //ClientLocationVModel.PFType = RLCS_ClientsManagement.GetClientPFType(BranchID);
                ClientLocationVModel.PFType = RLCS_ClientsManagement.GetClientPFType(Convert.ToInt32(ID));

                if (StateList != null && StateList.Count > 0)
                {
                    TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                    ClientLocationVModel.States = TinyMapper.Map<List<StateVModel>>(StateList);
                }
                if (PTStateList != null && PTStateList.Count > 0)
                {
                    TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                    ClientLocationVModel.PTStates = TinyMapper.Map<List<StateVModel>>(PTStateList);
                }
                if (LocationList != null && LocationList.Count > 0)
                {
                    if (!String.IsNullOrEmpty(ClientLocationVModel.CM_State))
                    {
                        LocationList = LocationList.Where(c => c.SM_Code.Equals(ClientLocationVModel.CM_State)).ToList();

                    }
                    TinyMapper.Bind<List<RLCS_Location_City_Mapping>, List<LocationVModel>>();
                    ClientLocationVModel.Locations = TinyMapper.Map<List<LocationVModel>>(LocationList);
                }
               int DistributorID = RLCS_Master_Management.Get_DistributorID(Convert.ToInt32(CustomerID));
                LocationAnchorList = GetAllLocationsAnchor(DistributorID, Convert.ToInt32(CustomerID));
                if (LocationAnchorList.Count>0)
                {
                    TinyMapper.Bind<List<SP_GetAllAnchor_Result>, List<LocationAnchorVModel>>();
                    ClientLocationVModel.LocationsAnchor = TinyMapper.Map<List<LocationAnchorVModel>>(LocationAnchorList);
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                ClientLocationVModel.Exception = true;
            }
            return View(ClientLocationVModel);
        }

        [HttpPost]
        public JsonResult CheckDuplicate(string AVACOM_BranchName, string AVACOM_BranchID)
        {
            bool Exist = false;
            int CustomerID = 0;
            int BranchId = Convert.ToInt32(AVACOM_BranchID);
            RLCS_CustomerBranch_ClientsLocation_Mapping Customerbranch = new RLCS_CustomerBranch_ClientsLocation_Mapping();
            if (AVACOM_BranchID != null)
                Customerbranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetCustomerIDByCustomerBranchID((int)BranchId);
            else
                Customerbranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetAVACOMCustIDByCorpID(Customerbranch.CO_CorporateID);

            CustomerID = Convert.ToInt32(Customerbranch.AVACOM_CustomerID);
            if (RLCS_ClientsManagement.CheckDuplicateBranch(AVACOM_BranchName, CustomerID))
            {
                Exist = true;
            }
            return Json(Exist, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateClientLocationSetup(ClientLocationVModel Setup)
        {
            ViewData["ButtonType"] = "Edit";
            RLCS_CustomerBranch_ClientsLocation_Mapping Customerbranch = new RLCS_CustomerBranch_ClientsLocation_Mapping();
            List<RLCS_State_Mapping> StateList = new List<RLCS_State_Mapping>();
            List<RLCS_State_Mapping> PTStateList = new List<RLCS_State_Mapping>();
            List<RLCS_Location_City_Mapping> LocationList = new List<RLCS_Location_City_Mapping>();
            List<SP_GetAllAnchor_Result> LocationAnchorList = new List<SP_GetAllAnchor_Result>();
            

            int AVACOM_CustBranchID;
            int ComplianceProdType;
            if (string.IsNullOrEmpty(Convert.ToString(Setup.AVACOM_BranchID)))
            {
                AVACOM_CustBranchID = 0;
            }

            try
            {
                DateTime dt;
                if (ModelState.IsValid)
                {
                    TinyMapper.Bind<ClientLocationVModel, RLCS_CustomerBranch_ClientsLocation_Mapping>(
                        config =>
                        {
                            config.Ignore(source => source.CL_CommencementDate);
                            config.Ignore(source => source.CL_RCValidFrom);
                            config.Ignore(source => source.CL_RCValidTo);
                        }
                          );
                    Customerbranch = TinyMapper.Map<RLCS_CustomerBranch_ClientsLocation_Mapping>(Setup);
                    if (!string.IsNullOrEmpty(Setup.CM_ClientID))
                    {
                        Setup.CorporateID = RLCS_ClientsManagement.GetCorporateID(Setup.CM_ClientID);
                        Customerbranch.CO_CorporateID = RLCS_ClientsManagement.GetCorporateID(Setup.CM_ClientID);
                        Customerbranch.CM_ClientID = Setup.CM_ClientID;

                    }
                    else
                    {
                        // Customerbranch.CO_CorporateID = RLCS_ClientsManagement.GetCorporateID(Setup.CM_ClientID);
                        Customerbranch.CO_CorporateID = RLCS_ClientsManagement.GetCorporateIDByClientName(Setup.CM_ClientName, Setup.AVACOM_BranchID);
                        Customerbranch.CM_ClientID = RLCS_ClientsManagement.GetClientIDByName_New(Setup.CM_ClientName, Setup.AVACOM_BranchID);
                        Setup.CM_ClientID = Customerbranch.CM_ClientID;
                    }

                    if (Customerbranch.AVACOM_BranchID != null)
                        AVACOM_CustBranchID = (int)Customerbranch.AVACOM_BranchID;

                    if (Setup.CL_RCValidFrom != null)
                    {
                        Customerbranch.CL_RC_ValidFrom = DateTimeExtensions.GetDate(Setup.CL_RCValidFrom);
                    }
                    if (Setup.CL_RCValidTo != null)
                    {
                        Customerbranch.CL_RC_ValidTo = DateTimeExtensions.GetDate(Setup.CL_RCValidTo);
                    }

                    if (!String.IsNullOrEmpty(Setup.CL_WeekoffDay) && Setup.CL_WeekoffDay.EndsWith("|"))
                        Customerbranch.CL_weekoffDay = Setup.CL_WeekoffDay.Remove(Setup.CL_WeekoffDay.Length - 1);
                    else
                        Customerbranch.CL_weekoffDay = Setup.CL_WeekoffDay;

                    if (String.IsNullOrEmpty(Customerbranch.CO_CorporateID) && String.IsNullOrEmpty(Setup.CorporateID) && !String.IsNullOrEmpty(Setup.CM_ClientName))
                    {
                        Customerbranch.CO_CorporateID = RLCS_ClientsManagement.GetCorporateIDByClientName(Setup.CM_ClientName, Setup.AVACOM_BranchID);
                        Customerbranch.CM_ClientID = RLCS_ClientsManagement.GetClientIDByName_New(Setup.CM_ClientName, Setup.AVACOM_BranchID);
                    }
                    if (Customerbranch.AVACOM_BranchID != null)
                        Customerbranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetCustomerIDByCustomerBranchID((int)Customerbranch.AVACOM_BranchID);
                    else
                        Customerbranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetAVACOMCustIDByCorpID(Customerbranch.CO_CorporateID);

                    ////GG ADD ComplianceProductType 13March2020
                    if (!string.IsNullOrEmpty(Convert.ToString(Customerbranch.AVACOM_CustomerID)))
                    {
                        ComplianceProdType = RLCS_Master_Management.GetComplianceProductType(Convert.ToInt32(Customerbranch.AVACOM_CustomerID));
                        if (ComplianceProdType > 0)
                        {
                            Customerbranch.CM_IsAventisClientOrBranch = ComplianceProdType;
                        }
                    }
                    ////END
                    Customerbranch.BranchType = "B";
                    Customerbranch.CL_NatureofBusiness = Setup.CL_NatureOfBusiness;
                    Customerbranch.CL_BusinessType = Setup.CL_BusinessType;
                    Customerbranch.CM_Status = Setup.CL_Status;
                    Customerbranch.CL_HR1stLevelPhNo = Setup.CL_HR1stLevelPhNo;
                    Customerbranch.CL_RCNO = Setup.CL_RCNo;
                    Customerbranch.CL_WorkTimings = Setup.CL_WorkTimings;
                    Customerbranch.CL_Pincode = Setup.CL_Pincode;
                    Customerbranch.CM_Pincode = Setup.CL_Pincode;
                    Customerbranch.CL_HRContactPerson = Setup.CL_HRContactPerson;
                    Customerbranch.CL_HRMailID = Setup.CL_HRMailID;

                    if (!String.IsNullOrEmpty(Setup.CL_CompPhoneNo))
                        Customerbranch.CL_CompPhoneNo = Convert.ToInt64(Setup.CL_CompPhoneNo);
                    if (!String.IsNullOrEmpty(Setup.CL_HRPhNo))
                        Customerbranch.CL_HRPhNo = Convert.ToInt64(Setup.CL_HRPhNo);

                    if (Customerbranch.CM_EstablishmentType == "SEA")
                    {
                        Customerbranch.CL_Municipality = null;
                        Customerbranch.CL_PermissionMaintainingForms = null;
                        Customerbranch.CL_RequirePowerforFines = null;
                        Customerbranch.CL_ClassificationofEstablishment = null;
                        Customerbranch.CL_Juridiction = null;
                        Customerbranch.CL_LIN = null;
                        Customerbranch.CL_CommencementDate = null;
                        Customerbranch.CL_LicenceNo = null;
                        Customerbranch.CL_NICCode = null;
                        Customerbranch.CL_SectionofAct = null;
                        Customerbranch.CL_District = null;
                    }
                    else
                    {
                        Customerbranch.CL_Municipality = Setup.CL_Municipality;
                        Customerbranch.CL_PermissionMaintainingForms = Setup.CL_Permission_MaintaingForms;
                        Customerbranch.CL_RequirePowerforFines = Setup.CL_RequirePowerforFines;
                        Customerbranch.CL_ClassificationofEstablishment = Setup.CL_ClasssificationofEstabilishment;
                        Customerbranch.CL_Juridiction = Setup.CL_Jurisdiction;
                        Customerbranch.CL_LIN = Setup.CL_LIN;
                        if (Setup.CL_CommencementDate != null)
                        {
                            Customerbranch.CL_CommencementDate = DateTimeExtensions.GetDate(Setup.CL_CommencementDate);
                        }
                    }
                    if (Setup.CL_BranchStartDate != null)
                    {
                        Customerbranch.CL_CommencementDate = DateTimeExtensions.GetDate(Setup.CL_BranchStartDate);
                    }
                    bool saveRecord = false;
                    string branchCode = "";
                    string mode = null;

                    if (Setup.SaveMode == "No" || Setup.SaveMode == "" || Setup.SaveMode == null)
                        mode = "add";
                    else
                        mode = "edit";

                    if (mode == "add" && Setup.ServiceProviderID == 94)  ///94
                    {
                        if (Setup.CL_BranchCode == "" || Setup.CL_BranchCode == null)
                            branchCode = GenerateBranchCode(Customerbranch.CM_ClientID, Customerbranch.CM_State, Customerbranch.CM_EstablishmentType);
                        else
                            branchCode = Setup.CL_BranchCode;//if branch code already exists still user clicks on save btn
                        if (!RLCS_ClientsManagement.CheckBranchCodeExist(branchCode)) //must add !
                        {
                            Customerbranch.CL_BranchCode = branchCode;
                            Setup.CL_BranchCode = branchCode;
                            saveRecord = true;
                        }
                        else
                        {
                            Setup.CL_BranchCode = branchCode;
                            ViewData["BranchCodeExists"] = "Yes";
                        }
                        Customerbranch.CL_LocationAnchor = Setup.CL_LocationAnchor;
                        Customerbranch.CL_EDLI_Excemption = Setup.CL_EDLI_Excemption;
                    }
                    else
                        saveRecord = true;

                    if (saveRecord)
                    {
                        bool ClientUp = RLCS_ClientsManagement.CreateUpdate_RLCS_ClientLocationDetails(Customerbranch);
                        Setup.Message = ClientUp;
                        if (ClientUp)
                        {
                            Model_ClientLocation ClientLocation = new Model_ClientLocation();
                            ClientLocation.ClientId = Customerbranch.CM_ClientID;
                            ClientLocation.StateId = Customerbranch.CM_State;
                            ClientLocation.Location = Customerbranch.CM_City;
                            ClientLocation.Branch = Customerbranch.AVACOM_BranchName;
                            ClientLocation.BranchAddress = Customerbranch.CM_Address;
                            ClientLocation.LWFState = Customerbranch.CL_LWF_State;
                            ClientLocation.PTState = Customerbranch.CL_PT_State;
                            ClientLocation.PFCode = Customerbranch.CL_PF_Code;
                            ClientLocation.ESICCode = Customerbranch.CL_ESIC_Code;
                            ClientLocation.OfficeType = Customerbranch.CL_OfficeType;
                            ClientLocation.EstablishmentType = Customerbranch.CM_EstablishmentType;
                            ClientLocation.EmployerName = Customerbranch.CL_EmployerName;
                            ClientLocation.EmployerAddress = Customerbranch.CL_EmployerAddress;
                            ClientLocation.ManagerAddress = Customerbranch.CL_ManagerAddress;
                            ClientLocation.ManagerName = Customerbranch.CL_ManagerName;
                            ClientLocation.CompanyPhoneNumber = Customerbranch.CL_CompPhoneNo.HasValue ? Customerbranch.CL_CompPhoneNo.Value : 0;
                            ClientLocation.HRContactPerson = Customerbranch.CL_HRContactPerson;
                            ClientLocation.HRPhoneNumber = Customerbranch.CL_HRPhNo.HasValue ? Customerbranch.CL_HRPhNo.Value : 0;
                            ClientLocation.HREmail = Customerbranch.CL_HRMailID;
                            ClientLocation.HRFirstLevelEmail = Customerbranch.CL_HR1stLevelMail;
                            ClientLocation.HRFirstLevelPhoneNumber = Customerbranch.CL_HR1stLevelPhNo;
                            ClientLocation.RCNumber = Customerbranch.CL_RCNO;
                            ClientLocation.RCValidFrom = Customerbranch.CL_RC_ValidFrom;
                            ClientLocation.RCValidTo = Customerbranch.CL_RC_ValidTo;
                            ClientLocation.NatureOfBusiness = Customerbranch.CL_NatureofBusiness;
                            ClientLocation.WeekOffDays = Customerbranch.CL_weekoffDay;
                            ClientLocation.WorkHoursFrom = Customerbranch.CL_WorkHoursFrom;
                            ClientLocation.WorkHoursTo = Customerbranch.CL_WorkHoursTo;
                            ClientLocation.WorkTimings = Customerbranch.CL_WorkTimings;
                            ClientLocation.IntervalFrom = Customerbranch.CL_IntervalsFrom;
                            ClientLocation.IntervalTo = Customerbranch.CL_IntervalsTo;
                            ClientLocation.LocationAnchor = Customerbranch.CL_LocationAnchor;
                            ClientLocation.Status = Customerbranch.CM_Status;
                            ClientLocation.LIN = Customerbranch.CL_LIN;
                            ClientLocation.Municipality = Customerbranch.CL_Municipality;
                            ClientLocation.PermissonMaintainingForms = Customerbranch.CL_PermissionMaintainingForms;
                            ClientLocation.RequirePowerforFines = Customerbranch.CL_RequirePowerforFines;
                            ClientLocation.Businesstype = Customerbranch.CL_BusinessType;
                            ClientLocation.EffectiveDate = Customerbranch.CL_CommencementDate;

                            ClientLocation.ClassificationofEstablishment = Customerbranch.CL_ClassificationofEstablishment;
                            ClientLocation.EmployerDesignation = "";
                            ClientLocation.LicenceNumber = Customerbranch.CL_LicenceNo;
                            ClientLocation.NICNumber = Customerbranch.CL_NICCode;
                            ClientLocation.SectionOfAct = Customerbranch.CL_SectionofAct;
                            ClientLocation.District = Customerbranch.CL_District;
                            ClientLocation.Pincode = Customerbranch.CL_Pincode;
                            ClientLocation.Jurisdiction = Customerbranch.CL_Juridiction;
                            ClientLocation.CL_District = Customerbranch.CL_District;
                            ClientLocation.CL_Pincode = Customerbranch.CL_Pincode;
                            ClientLocation.CL_CommencementDate = Customerbranch.CL_CommencementDate;
                            if (string.IsNullOrEmpty(Convert.ToString(Customerbranch.NoofEmployees)))
                            {
                                Customerbranch.NoofEmployees = 0;
                            }
                            ClientLocation.CL_NoOfEmployees = (int)Customerbranch.NoofEmployees;
                            ClientLocation.CL_MODIFIEDDate = DateTime.Now;
                            ClientLocation.CL_IsAventisBranch = Customerbranch.CM_IsAventisClientOrBranch;
                            ClientLocation.CL_BranchCode = Customerbranch.CL_BranchCode;
                            ClientLocation.CL_EDLI_Excemption = Customerbranch.CL_EDLI_Excemption;
                            ClientLocation.CreatedBy = "Avantis";
                            ClientLocation.ModifiedBy = "Avantis";
                            bool cl = ClientLocationApiCall(ClientLocation);
                            if (cl)
                            {
                                Setup.Message = true;
                                RLCS_ClientsManagement.Update_ProcessedStatus_ClientLocationDetail(Setup.AVACOM_BranchID, Setup.CM_ClientID, cl);
                            }
                            BindStates(Setup);
                            int DistributorID = RLCS_Master_Management.Get_DistributorID(Convert.ToInt32(Customerbranch.AVACOM_CustomerID));
                            LocationAnchorList = GetAllLocationsAnchor(DistributorID, Convert.ToInt32(Customerbranch.AVACOM_CustomerID));
                            if (LocationAnchorList.Count > 0)
                            {
                                TinyMapper.Bind<List<SP_GetAllAnchor_Result>, List<LocationAnchorVModel>>();
                                Setup.LocationsAnchor = TinyMapper.Map<List<LocationAnchorVModel>>(LocationAnchorList);
                            }
                            ViewData["Edit"] = "Yes"; //Even after the data is saved, state and est type dropdowns to be readonly
                            return View(Setup);
                        }
                    }
                    else
                    {
                        ViewData["BranchCodeExists"] = "Yes";
                        BindStates(Setup);
                    }
                }
                else
                {
                    BindStates(Setup);
                    if (Setup.SaveMode == "No")
                        ViewData["Edit"] = "No";
                    else
                        ViewData["Edit"] = "Yes";
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                Setup.Exception = true;
            }
            return View(Setup);
        }

        #region BULK UPLOAD UpdateSelectedColumnEntity/Client

        [HttpGet]
        public ActionResult UploadEntityUpdateSelected(string CustID, string UserID,int SPID)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                UploadEntityLocationDocumentVModel BasicDetails = new UploadEntityLocationDocumentVModel();
                List<RLCS_ClientHeader_Mapping> objRLCS_ClientHeader_Mapping = new List<RLCS_ClientHeader_Mapping>();
                if (!String.IsNullOrEmpty(CustID))
                    BasicDetails.CustomerID = Convert.ToInt32(CustID);
                BasicDetails.UserID = Convert.ToInt32(UserID);
                BasicDetails.ServiceProviderID = SPID;
                BasicDetails.ISEntity = true;
                TempData["CustID"] = Convert.ToInt32(CustID);
                TempData["UserID"] = Convert.ToInt32(UserID);

                List<ClientHeaderMappingVModel> compare = new List<ClientHeaderMappingVModel>();

                if (SPID == 94)  //// Parameter  Use As A ServiceProvider
                {
                    objRLCS_ClientHeader_Mapping = RLCS_ClientsManagement.GetMappedClientHeadersColumn("RLCS_Client_BasicDetails", 94);
                }
                else
                {
                    objRLCS_ClientHeader_Mapping = RLCS_ClientsManagement.GetMappedClientHeadersColumn("RLCS_Client_BasicDetails", 0);
                }
                //objRLCS_ClientHeader_Mapping = RLCS_ClientsManagement.GetMappedClientHeadersColumn("RLCS_Client_BasicDetails", 0);

                TinyMapper.Bind<RLCS_ClientHeader_Mapping, ClientHeaderMappingVModel>();
                compare = TinyMapper.Map<List<ClientHeaderMappingVModel>>(objRLCS_ClientHeader_Mapping);
                if (compare.Count > 0)
                {
                    for (int j = 0; j < compare.Count; j++)
                    {
                        BasicDetails.ClientHeaderMappingVModelList.Add(new ClientHeaderMappingVModel { ID = compare[j].ID, CustomerID = Convert.ToInt32(CustID), TableName = "RLCS_Client_BasicDetails", ColName = compare[j].ColName, ClientHeaderName = compare[j].ClientHeaderName });

                    }
                }
                return View(BasicDetails);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        [HttpPost]
        public ActionResult UploadEntityUpdateSelected(FormCollection formCollection, UploadEntityLocationDocumentVModel BasicDetails)
        {
            LogErrorsUpload.Clear();
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                bool update = true;
                string FilePath = "";
                try
                {
                    if (Request != null)
                    {
                        HttpPostedFileBase file = Request.Files["fileUpload"];
                        if (file != null && file.ContentLength > 0)
                        {
                            if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                            {
                                BasicDetails.Error = false;
                                string fileName = file.FileName;
                                string fileContentType = file.ContentType;
                                string Extention = Path.GetExtension(fileName);
                                byte[] fileBytes = new byte[file.ContentLength];
                                string FolderPath = Server.MapPath("~/HRExcelEntity");
                                var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                                if (!Directory.Exists(FolderPath))
                                {
                                    Directory.CreateDirectory(FolderPath);
                                }
                                FilePath = System.Web.HttpContext.Current.Server.MapPath("~") + "/HRExcelEntity/" + fileName;
                                file.SaveAs(FilePath);
                                if (Extention.ToLower() == ".xlsx")
                                {
                                    using (var package = new ExcelPackage(file.InputStream))
                                    {
                                        var currentSheet = package.Workbook.Worksheets;
                                        var workSheet = currentSheet.First();
                                        DataTable EntityDT = new DataTable();
                                        var SheetName = workSheet.Workbook.Worksheets["Entity"];
                                        if (SheetName.ToString() == "Entity")
                                        {
                                            EntityDT = EntitySelectedColumnConvertToDataTable(workSheet.Workbook.Worksheets["Entity"]);
                                            if (EntityDT != null)
                                            {
                                                DataRow dr = null;
                                                string[] arrcolumn = { "Corporate ID", "Client ID", "Act Applicability (State/ Central)", "Estabilishment Type (Shop & Establishment/ Factory/ Both)", "State", "City", "Address", "Bonus Percentage", "Excemption (YES/NO)", "Wage Period From", "Wage Period To", "Payment Day", "ServiceTax Exempted (Yes/NO)", "Company Commencement Date (Date)i.e.(DD/MM/YYYY)", "PF Code", "Status (Active/Inactive)", "Contact Person", "Contact Number", "EmailID", "Type (Public/Private/Listed)","PF Code Type (C/B)", "EDLI Excemption Type (C/B)", "PO Applicable(YES/NO)", "AgreementID", "Mandate(One time/Recurring/Both(OneTime & Recurring)/PayRoll)", "BDAnchor", "RAMAnchor", "RegisterAnchor", "ChallanAnchor", "ReturnAnchor", "LocationAnchor", "CRAnchor", "AuditAnchor", "SPOC SANITATION", "SPOC FIRSTNAME", "SPOC LASTNAME", "SPOC CONTACT", "SPOC EMAIL", "SPOC DESIGNATION", "EP1 SANITATION", "EP1 FIRSTNAME", "EP1 LASTNAME", "EP1 CONTACT", "EP1 EMAIL", "EP1 DESIGNATION", "EP2 SANITATION", "EP2  FIRSTNAME", "EP2 LASTNAME", "EP2 CONTACT", "EP2 EMAIL", "EP2 DESIGNATION" };
                                                DataTable dt = new DataTable("Entity");
                                                if (arrcolumn.Count() == 51)
                                                {
                                                    foreach (var item in arrcolumn)
                                                {
                                                    dt.Columns.Add(Convert.ToString(item));
                                                }
                                                List<string> lstentity = new List<string>();
                                                foreach (DataColumn item in EntityDT.Columns)
                                                {
                                                    lstentity.Add(item.ColumnName);
                                                }
                                                for (int k = 0; k < EntityDT.Rows.Count; k++)
                                                {
                                                    var newrow = dt.NewRow();
                                                    foreach (var emp in lstentity)
                                                    {
                                                        newrow[emp] = EntityDT.Rows[k][emp];
                                                    }
                                                    dt.Rows.Add(newrow);
                                                }
                                                for (int i = 0; i < dt.Rows.Count; i++)
                                                {
                                                    for (int j = 2; j < dt.Columns.Count; j++)
                                                    {
                                                        if (string.IsNullOrEmpty(dt.Rows[i][j].ToString()))
                                                        {
                                                            dt.Rows[i][j] = "N0";
                                                        }
                                                    }
                                                }
                                            }
                                            if (dt.Rows.Count <= 0)
                                            {
                                                BasicDetails.RowCount = true;
                                            }
                                            else
                                            {
                                                if (dt.Columns.Count > 2)
                                                {

                                                    var CustomerID = new SqlParameter("@CustomerID", SqlDbType.Int);
                                                    CustomerID.Value = BasicDetails.CustomerID;
                                                    var UserID = new SqlParameter("@UserID", SqlDbType.Int);
                                                    UserID.Value = BasicDetails.UserID;
                                                    var UpdateFlag = new SqlParameter("@UpdateFlag", SqlDbType.Bit);
                                                    UpdateFlag.Value = update;
                                                    var EntityDT1 = new SqlParameter("@EntityDT", SqlDbType.Structured);
                                                    EntityDT1.Value = dt;
                                                    EntityDT1.TypeName = "dbo.EntityUDT";
                                                    var Result = new SqlParameter("@Result", SqlDbType.Int);
                                                    Result.Direction = ParameterDirection.Output;
                                                    var FileID = new SqlParameter("@FileID", SqlDbType.Int);
                                                    FileID.Direction = ParameterDirection.Output;


                                                    using (ComplianceDBEntities entity = new ComplianceDBEntities())
                                                    {
                                                        entity.Database.ExecuteSqlCommand("Exec dbo.SP_EntityUpdate @EntityDT,@CustomerID,@UserID,@UpdateFlag,@Result OUTPUT,@FileID OUTPUT", EntityDT1, CustomerID, UserID, UpdateFlag, Result, FileID);


                                                        if (Convert.ToInt32(Result.Value) == 200)
                                                        {
                                                            BasicDetails.Message = true;
                                                        }
                                                        else if (Convert.ToInt32(Result.Value) == 400 && Convert.ToInt32(FileID.Value) > 0)
                                                        {
                                                            int fileid = Convert.ToInt32(FileID.Value);
                                                            BasicDetails.Message = false;
                                                            List<string> ErrorList = new List<string>();
                                                            ErrorList = (from emp in entity.Temp_EntityMaster_ErrorList
                                                                         where emp.FileID == (int)FileID.Value && emp.CustomerID == (int)CustomerID.Value
                                                                         select emp.ErrorLog1
                                                                         ).ToList();

                                                            if (ErrorList.Count > 0)
                                                            {
                                                                string path = "";
                                                                BasicDetails.FileName = RLCS_WriteLog.WriteLog(ErrorList, "Upload Entity Master Excel Sheet", out path);
                                                                BasicDetails.ExcelErrors = true;
                                                            }
                                                            else
                                                            {
                                                                ErrorList.Add("Error Occured.....!");
                                                                string path = "";
                                                                BasicDetails.FileName = RLCS_WriteLog.WriteLog(ErrorList, "Upload Entity Master Excel Sheet", out path);
                                                                BasicDetails.ExcelErrors = true;

                                                            }

                                                        }

                                                    }

                                                }
                                                else
                                                {
                                                    BasicDetails.ColumnCount = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string path = "";
                                            BasicDetails.FileName = RLCS_WriteLog.WriteLog(LogErrorsUpload, "Upload Entity Master Excel Sheet", out path);
                                            BasicDetails.ExcelErrors = true;
                                        }
                                        }
                                        else
                                        {
                                            string path = "";
                                            BasicDetails.FileName = RLCS_WriteLog.WriteLog(LogErrorsUpload, "Incorrect Sheet Name......Upload Entity Master Excel Sheet", out path);
                                            BasicDetails.ExcelErrors = true;
                                        }
                                    }
                                }
                                else
                                {
                                    LogErrorsUpload.Add("Please Upload letest Updated Version Excel File..");
                                    LogErrorsUpload.Add("Your File Version " + Extention.ToLower() + " : Required File Version .xlsx");
                                    string path = "";
                                    BasicDetails.FileName = RLCS_WriteLog.WriteLog(LogErrorsUpload, "Upload Entity Master Excel Sheet", out path);
                                    BasicDetails.ExcelErrors = true;
                                }
                            }
                            else
                            {
                                LogErrorsUpload.Add("Please Download Excel Sample Format.");
                                LogErrorsUpload.Add("Your File Version " + Path.GetExtension(file.FileName).ToLower() + " : Required File Version .xlsx");
                                string path = "";
                                BasicDetails.FileName = RLCS_WriteLog.WriteLog(LogErrorsUpload, "Upload Entity Master Excel Sheet", out path);
                                BasicDetails.ExcelErrors = true;
                            }
                        }
                        else
                        {
                            BasicDetails.Error = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                return View(BasicDetails);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }
        private DataTable EntitySelectedColumnConvertToDataTable(ExcelWorksheet oSheet)
        {
            string[] arrcolumn = { "Corporate ID", "Client ID", "Act Applicability (State/ Central)", "Estabilishment Type (Shop & Establishment/ Factory/ Both)", "State", "City", "Address", "Bonus Percentage", "Excemption (YES/NO)", "Wage Period From", "Wage Period To", "Payment Day", "ServiceTax Exempted (Yes/NO)", "Company Commencement Date (Date)i.e.(DD/MM/YYYY)", "PF Code", "Status (Active/Inactive)", "Contact Person", "Contact Number", "EmailID", "Type (Public/Private/Listed)", "PF Code Type (C/B)", "EDLI Excemption Type (C/B)", "PO Applicable(YES/NO)", "AgreementID", "Mandate(One time/Recurring/Both(OneTime & Recurring)/PayRoll)", "BDAnchor", "RAMAnchor", "RegisterAnchor", "ChallanAnchor", "ReturnAnchor", "LocationAnchor", "CRAnchor", "AuditAnchor", "SPOC SANITATION", "SPOC FIRSTNAME", "SPOC LASTNAME", "SPOC CONTACT", "SPOC EMAIL", "SPOC DESIGNATION", "EP1 SANITATION", "EP1 FIRSTNAME", "EP1 LASTNAME", "EP1 CONTACT", "EP1 EMAIL", "EP1 DESIGNATION", "EP2 SANITATION", "EP2  FIRSTNAME", "EP2 LASTNAME", "EP2 CONTACT", "EP2 EMAIL", "EP2 DESIGNATION" };

            int totalRows = GetLastUsedRow(oSheet);
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            List<string> LogErrors = new List<string>();
            for (int i = 1; i <= totalRows; i++)
            {
                if (LogErrors.Count > 0)
                {
                    break;
                }
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        //if (string.IsNullOrEmpty(Convert.ToString(oSheet.Cells[i, j].Value).Trim()))
                        //{
                        //    LogErrors.Add("Column Empty.Please delete columns in Your Excel Sheet..!Coloumn NO: " + j);
                        //    break;
                        //}
                        if (dt.Columns.Contains(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            LogErrors.Add("Column Already Exist.Please check Your Excel Sheet..!" + arrcolumn[j - 1]);
                            break;
                        }
                        if (!arrcolumn.Contains(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            LogErrors.Add("Invalid Column Header.Please check Your Sample Excel Sheet..!" + arrcolumn[j - 1]);
                            break;
                        }
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            dr[j - 1] = " ";
                        }
                        else
                        {
                            dr[j - 1] = Convert.ToString(oSheet.Cells[i, j].Value);
                        }

                    }
                }
            }
            if (LogErrors.Count > 0)
            {
                LogErrorsUpload = LogErrors.ToList();
                dt = null;
            }
            return dt;
        }
        public ActionResult DownloadAttachmentSelectedColumnsEntity(string CustomerID, int ID)
        {
            UploadEntityLocationDocumentVModel EntityUpload = new UploadEntityLocationDocumentVModel();
            try
            {
                if (CustomerID != null)
                {
                    if (ID != null)
                    {
                        using (ComplianceDBEntities entity = new ComplianceDBEntities())
                        {
                            var LstColumn = (from emp in entity.tbl_HeaderColumnJSON
                                             where emp.ID == (int)ID
                                             select emp.ColumnJson
                                             ).ToList();

                            if (LstColumn.Count > 0)
                            {
                                string[] arrList = LstColumn.ToArray();
                                EntityDownloadUpdateSelectedColumns(Convert.ToInt32(CustomerID), arrList[0].ToString());
                            }
                        }
                    }
                }
                else
                {
                    EntityUpload.ServerError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                EntityUpload.ServerError = true;
            }

            return View("UploadEntityUpdateSelected", EntityUpload);
        }
        private void EntityDownloadUpdateSelectedColumns(int CustomerID, string jsonContent)
        {
            UploadEntityLocationDocumentVModel EntityUpload = new UploadEntityLocationDocumentVModel();
            try
            {
                //used NewtonSoft json nuget package
                XmlNode xml = JsonConvert.DeserializeXmlNode("{Tables:{Table:" + jsonContent + "}}");
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xml.InnerXml);
                XmlReader xmlReader = new XmlNodeReader(xml);
                DataSet dataSet = new DataSet();
                dataSet.ReadXml(xmlReader);
                var dataTable = dataSet.Tables[0];
                DataTable dt = new DataTable();
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        dt.Columns.Add(Convert.ToString(row["Table_Text"]));

                    }
                }
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Entity");
                    exWorkSheet.Cells["A1"].LoadFromDataTable(dt, true);
                    exWorkSheet.DefaultColWidth = 15;
                    exWorkSheet.Row(1).Height = 50;
                    exWorkSheet.Cells.Style.Numberformat.Format = "@";
                    if (dt.Columns.Count == 5)
                    {
                        exWorkSheet.Column(5).Width = 25;
                    }
                    if (dt.Columns.Count == 6)
                    {
                        exWorkSheet.Column(6).Width = 25;
                    }
                    if (dt.Columns.Count == 16)
                    {
                        exWorkSheet.Column(16).Width = 25;
                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1, 1 + dt.Columns.Count - 1])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        col.Style.WrapText = true;
                        col.Style.Numberformat.Format = "@";

                    }
                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=EntitySampleUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.Flush(); // Sends all currently buffered output to the client.
                    Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.

                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                EntityUpload.ServerError = true;
            }

        }
        #endregion





        [HttpGet]
        public JsonResult CheckBranchCodeExist(string branchCode)
        {
            string result = string.Empty;
            try
            {
                bool res = RLCS_ClientsManagement.CheckBranchCodeExist(branchCode);
                result = res.ToString();
            }
            catch (Exception ex)
            {
                result = "error";
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ClientLocationVModel BindStates(ClientLocationVModel Setup)
        {
            List<RLCS_State_Mapping> StateList = RLCS_ClientsManagement.GetAllStates();
            List<RLCS_State_Mapping> PTStateList = RLCS_ClientsManagement.GetAllPTStates();
            List<RLCS_Location_City_Mapping> LocationList = RLCS_ClientsManagement.GetAllLocations();

            if (StateList != null && StateList.Count > 0)
            {
                TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                Setup.States = TinyMapper.Map<List<StateVModel>>(StateList);
            }
            if (PTStateList != null && PTStateList.Count > 0)
            {
                TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                Setup.PTStates = TinyMapper.Map<List<StateVModel>>(PTStateList);
            }
            if (LocationList != null && LocationList.Count > 0)
            {
                TinyMapper.Bind<List<RLCS_Location_City_Mapping>, List<LocationVModel>>();
                Setup.Locations = TinyMapper.Map<List<LocationVModel>>(LocationList);
            }
            return Setup;
        }

        public string GenerateBranchCode(string ClientID, string State, string EstablishmentType)
        {
            string branchCode = null;

            try
            {
                string branchNum = null;
                int branchCount = RLCS_ClientsManagement.GetBranchCountByClient(ClientID, EstablishmentType);
                branchCount++;

                if (branchCount >= 0 && branchCount <= 9)
                {
                    branchNum = "000" + branchCount;
                }
                else if (branchCount >= 10 && branchCount <= 99)
                {
                    branchNum = "00" + branchCount;
                }
                else if (branchCount >= 100 && branchCount <= 999)
                {
                    branchNum = "0" + branchCount;
                }
                else
                {
                    branchNum = branchCount.ToString();
                }

                string stCode = State;
                string estTyp = EstablishmentType;


                string clientNm = RLCS_ClientsManagement.GetClientID(ClientID);
                string[] clientArr = clientNm.Split(' ');
                string clientInitials = null;


                foreach (string word in clientArr)
                {
                    if (word != "")
                    {
                        char firstLetter = word[0];
                        clientInitials += firstLetter;
                    }
                }
                branchCode = clientInitials + "-" + estTyp + stCode + "-" + branchNum;

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return branchCode;
        }

        public ActionResult UploadLocationFiles(int SPID)
        {
            UploadDocumentVModel UploadDocumentVModel = new UploadDocumentVModel();
            try
            {
                UploadDocumentVModel.ClientLocationVModel.ServiceProviderID = SPID;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                UploadDocumentVModel.ServerError = true;
            }

            return View(UploadDocumentVModel);
        }

        public ActionResult uploadEmpCTCfiles(string CustID)
        {

            EmployeeCtcVModel EmpCtcVModel = new EmployeeCtcVModel();

            if (!string.IsNullOrEmpty(CustID))
            {
                int customerId = Convert.ToInt32(CustID);
                Customer selectedCustomer = CustomerManagement.GetByID(customerId);   //CustomerList.Where(t => t.ID == customerId).Distinct().ToList();

                List<Customer> lstCust = new List<Customer>();
                lstCust.Add(selectedCustomer);

                TinyMapper.Bind<List<Customer>, List<CustomerVModel>>();
                EmpCtcVModel.Customers = TinyMapper.Map<List<CustomerVModel>>(lstCust);

                var clients = RLCS_ClientsManagement.GetClientsByCustomerId(customerId);

                if (clients != null && clients.Count > 0)
                {
                    TinyMapper.Bind<List<Client>, List<ClientVModel>>();
                    EmpCtcVModel.Clients = TinyMapper.Map<List<ClientVModel>>(clients);
                }
                ViewData["custID"] = CustID;
            }
            else
            {
                var CustomerList = CustomerManagement.GetAll(-1, "");
                if (CustomerList != null && CustomerList.Count > 0)
                {
                    TinyMapper.Bind<List<Customer>, List<CustomerVModel>>();
                    EmpCtcVModel.Customers = TinyMapper.Map<List<CustomerVModel>>(CustomerList);
                }
            }

            return View(EmpCtcVModel);
        }


        public ActionResult getClients(string customerid)
        {
            EmployeeCtcVModel EmpCtcVModel = new EmployeeCtcVModel();
            int customer = Convert.ToInt32(customerid);
            var clients = RLCS_ClientsManagement.GetClientsByCustomerId(customer);

            if (clients != null && clients.Count > 0)
            {
                TinyMapper.Bind<List<Client>, List<ClientVModel>>();
                EmpCtcVModel.Clients = TinyMapper.Map<List<ClientVModel>>(clients);
            }
            return Json(clients, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadCtc()
        {
            List<string> colHeaders = null;
            List<string> unmapped = null;
            string chkBlank = null;
            List<string> repeated = null;
            string success = "1"; //no error
            try
            {
                
                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName).ToLower() == ".csv")
                    {

                        int usrid = Convert.ToInt32(AuthenticationHelper.UserID);

                        int customerID = Convert.ToInt32(Request.Form["customerid"]);
                        string clientid = Request.Form["clientid"].ToString();

                        string myfilepath = Path.GetFileNameWithoutExtension(file.FileName) + DateTime.Now.ToString("ddMMMyyyyhhmmss") + Path.GetExtension(file.FileName);

                        if (!Directory.Exists(Server.MapPath("~/TempCTC/")))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/TempCTC/"));
                        }

                        string FilePath = Server.MapPath("~/TempCTC/") + myfilepath;

                        file.SaveAs(FilePath);

                        DataTable csvDt = new DataTable();
                        csvDt = ConvertCSVtoDataTable(FilePath, out colHeaders, out unmapped, out chkBlank, out repeated, customerID, clientid);

                        if (unmapped.Count == 0 && repeated.Count == 0 && chkBlank == "N")
                        {
                            DataTable UDT_EmployeeCTC_dt = new DataTable();
                            UDT_EmployeeCTC_dt.Columns.Add("Year");
                            UDT_EmployeeCTC_dt.Columns.Add("Month");
                            UDT_EmployeeCTC_dt.Columns.Add("PayGroup");
                            UDT_EmployeeCTC_dt.Columns.Add("PayCode");
                            UDT_EmployeeCTC_dt.Columns.Add("Value");
                            UDT_EmployeeCTC_dt.Columns.Add("excel_row_no");
                            UDT_EmployeeCTC_dt.Columns.Add("user_id");
                            UDT_EmployeeCTC_dt.Columns.Add("fileid");
                            UDT_EmployeeCTC_dt.Columns.Add("AVACOM_CustomerID");
                            UDT_EmployeeCTC_dt.Columns.Add("AVACOM_BranchID");
                            UDT_EmployeeCTC_dt.Columns.Add("ISProcessed");
                            UDT_EmployeeCTC_dt.Columns.Add("ISActive");
                            UDT_EmployeeCTC_dt.Columns.Add("ISValidated");
                            UDT_EmployeeCTC_dt.Columns.Add("created_at");
                            UDT_EmployeeCTC_dt.Columns.Add("updated_at");
                            UDT_EmployeeCTC_dt.Columns.Add("deleted_at");
                            UDT_EmployeeCTC_dt.Columns.Add("PayDeductionType");

                            List<SP_Get_ClientPaycodeMappingDetails_Result> objPaycode = new List<SP_Get_ClientPaycodeMappingDetails_Result>();
                            objPaycode = RLCS_ClientsManagement.GetAllPaycodeDetailsList(clientid);

                            foreach (DataRow row in csvDt.Rows)
                            {
                                for (int i = 0; i < colHeaders.Count; i++)
                                {
                                    DataRow dr = UDT_EmployeeCTC_dt.NewRow();
                                    dr[0] = null;
                                    dr[1] = null;

                                    string CPMD_Standard_Column = colHeaders[i].ToLower();
                                    string deductionType = null;

                                    // RLCS_Client_Paycode_Mapping_Details standardHeaders = RLCS_ClientsManagement.GetStandardHeaderData(customerID, clientid, CPMD_Standard_Column, "S", "");
                                    SP_Get_ClientPaycodeMappingDetails_Result standardHeaders = objPaycode.Where(t => t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid && t.CPMD_PayGroup.ToLower() == "standard" && t.CPMD_Standard_Column.ToLower() == CPMD_Standard_Column.ToLower()).FirstOrDefault();
                                    if (standardHeaders != null)
                                    {
                                        dr[2] = "standard";
                                        deductionType = standardHeaders.CPMD_Deduction_Type;
                                    }
                                    else
                                    {
                                        //RLCS_Client_Paycode_Mapping_Details paycodeHeaders = RLCS_ClientsManagement.GetStandardHeaderData(customerID, clientid, CPMD_Standard_Column, "P", "");
                                        SP_Get_ClientPaycodeMappingDetails_Result paycodeHeaders = objPaycode.Where(t => t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid && t.CPMD_PayGroup.ToLower() == "paycodes" && t.CPMD_PayCode.ToLower() == CPMD_Standard_Column.ToLower()).FirstOrDefault();
                                        if (paycodeHeaders != null)
                                        {
                                            dr[2] = "paycodes";
                                            deductionType = paycodeHeaders.CPMD_Deduction_Type;
                                        }
                                    }

                                    dr[3] = colHeaders[i].ToString();
                                    dr[4] = row[colHeaders[i].ToString()].ToString();
                                    dr[5] = row["excel_row_no"].ToString();
                                    dr[6] = usrid;
                                    dr[7] = "0";
                                    dr[8] = customerID;
                                    dr[9] = null;
                                    dr[10] = "0";
                                    dr[11] = "1";
                                    dr[12] = "0";
                                    dr[13] = null;
                                    dr[14] = null;
                                    dr[15] = null;
                                    dr[16] = deductionType;
                                    UDT_EmployeeCTC_dt.Rows.Add(dr);
                                }
                            }

                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var EmpCtcDT = new SqlParameter("@UDT_EmployeeCTC", SqlDbType.Structured);
                                EmpCtcDT.Value = UDT_EmployeeCTC_dt;
                                EmpCtcDT.TypeName = "dbo.UDT_EmployeeCTC";

                                var CustomerID = new SqlParameter("@customerid", SqlDbType.Int);                            
                                CustomerID.Value = customerID;

                                var ClientID = new SqlParameter("@clientid", SqlDbType.VarChar);
                                ClientID.Value = clientid;

                                var UserID = new SqlParameter("@userid", SqlDbType.Int);
                                UserID.Value = usrid;

                                entities.Database.CommandTimeout = 360;//timeout resolved
                                var resOut = entities.Database.SqlQuery<OutputResult>("Exec dbo.SP_Upload_Employee_CTC_New @UDT_EmployeeCTC, @customerid, @clientid, @userid", EmpCtcDT, CustomerID, ClientID, UserID).ToList<OutputResult>();

                                if (resOut.Count > 0)
                                {
                                    DataTable ErrorDt = new DataTable();
                                    
                                    ErrorDt.Columns.Add("Column Name"); //column name should always be first column in stored procedure bcoz distinct is applied
                                    ErrorDt.Columns.Add("Error Message");
                                    ErrorDt.Columns.Add("RowNo");

                                    foreach (var x in resOut)
                                    {                                      
                                        ErrorDt.Rows.Add(x.colname, x.ErrorMessage, x.rownum);
                                    }

                                    Session["errorFile"] = ErrorDt;
                                    success = "3";//validation errors
                                }
                                else
                                {
                                    bool status = processFile();
                                }
                            }


                        }
                    }
                    else
                        success = "2"; // Please choose [.csv] file.
                }
                else
                    success = "2"; // Please choose [.csv] file.

            }
            catch (Exception ex)
            {               
                success = ex.Message;
            }


            var data = new { repeats = repeated.ToList(), unmapps = unmapped.ToList(), blankss = chkBlank, statuss = success };
                           
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public class OutputResult
        {        
            public string colname { get; set; }
            public string ErrorMessage { get; set; }
            public int rownum { get; set; }
        }

        public ActionResult showSuccess(string error, string CustID)
        {
            EmployeeCtcVModel vmodel = new EmployeeCtcVModel();
            if (error == "0")
            {
                vmodel.Success = true;
            }
            else
            {
                vmodel.Error = true;
                vmodel.ErrorMsg = "Please upload CSV file only";
            }


            var CustomerList = CustomerManagement.GetAll(-1, "");
            if (CustomerList != null && CustomerList.Count > 0)
            {
                TinyMapper.Bind<List<Customer>, List<CustomerVModel>>();
                vmodel.Customers = TinyMapper.Map<List<CustomerVModel>>(CustomerList);

                if (!string.IsNullOrEmpty(CustID))
                {
                    int customerId = Convert.ToInt32(CustID);
                    var selectedCustomer = CustomerList.Where(t => t.ID == customerId).Distinct().ToList();
                    TinyMapper.Bind<List<Customer>, List<CustomerVModel>>();
                    vmodel.Customers = TinyMapper.Map<List<CustomerVModel>>(selectedCustomer);

                    var clients = RLCS_ClientsManagement.GetClientsByCustomerId(customerId);

                    if (clients != null && clients.Count > 0)
                    {
                        TinyMapper.Bind<List<Client>, List<ClientVModel>>();
                        vmodel.Clients = TinyMapper.Map<List<ClientVModel>>(clients);
                    }
                    ViewData["custID"] = CustID;
                }
            }

            return View("uploadEmpCTCfiles", vmodel);
        }

        private bool processFile()
        {
            try
            {                
                bool saveSuccess = false;
                    
                string requestUrl = ConfigurationManager.AppSettings["RLCSAPIURL"];
                requestUrl += "AventisIntegration/InsertUpdateEmployeeCTCMasterDetails";
    
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstUnProcessedRecords = entities.SP_GetUnprocessedMonthlyCtcDetailsRecords().ToList();

                    if (lstUnProcessedRecords != null)
                    {
                        if (lstUnProcessedRecords.Count > 0)
                        {
                            var distinctclients = lstUnProcessedRecords.Select(x => x.ECS_Client_Id).Distinct();
                            foreach (var distinctclient in distinctclients)
                            {
                                try
                                {
                                    List<long> master_id = new List<long>();//master
                                    List<string> det_id = new List<string>();//details

                                    var results = from p in lstUnProcessedRecords
                                                  where p.ECS_Client_Id == distinctclient
                                                  group p by new
                                                  {
                                                      p.ECS_Id,
                                                      p.ECS_Client_Id,
                                                      p.ECS_Emp_Id,
                                                      p.ECS_GrossPay,
                                                      p.ECS_EffectiveDate,
                                                      p.ECS_CreatedBy
                                                  } into gcs
                                                  select new
                                                  {
                                                      ECS_Id = gcs.Key.ECS_Id,
                                                      ECS_Client_Id = gcs.Key.ECS_Client_Id,
                                                      ECS_Emp_Id = gcs.Key.ECS_Emp_Id,
                                                      ECS_GrossPay = gcs.Key.ECS_GrossPay,
                                                      ECS_EffectiveDate = gcs.Key.ECS_EffectiveDate,
                                                      ECS_CreatedBy = gcs.Key.ECS_CreatedBy,
                                                      CtcDetails = gcs.ToList()
                                                  };


                                    List<Model_Ctc_POST> SalaryList = new List<Model_Ctc_POST>();
                                    foreach (var salary in results)
                                    {
                                        try
                                        {
                                            Model_Ctc_POST objRecord = new Model_Ctc_POST();
                                            objRecord.ECS_Id = salary.ECS_Id;
                                            objRecord.ECS_Client_Id = salary.ECS_Client_Id;
                                            objRecord.ECS_Emp_Id = salary.ECS_Emp_Id;
                                            objRecord.ECS_GrossPay = salary.ECS_GrossPay;
                                            objRecord.ECS_EffectiveDate = salary.ECS_EffectiveDate;
                                            objRecord.ECS_CreatedBy = "Avantis";

                                            master_id.Add(objRecord.ECS_Id);
                                            det_id.Add(Convert.ToString(objRecord.ECS_Id));
                                            foreach (var salaryDetail in salary.CtcDetails)
                                            {
                                                Model_Ctc_Details_POST SalaryDetails = new Model_Ctc_Details_POST()
                                                {
                                                    ECSD_Id = salaryDetail.ECSD_Id,
                                                    ECSD_ECS_Id = salaryDetail.ECSD_ECS_Id,
                                                    ECSD_Remarks = salaryDetail.ECSD_Remarks,
                                                    ECSD_PayCode = salaryDetail.ECSD_PayCode,
                                                    ECSD_Amount = salaryDetail.ECSD_Amount
                                                };
                                                objRecord.CtcDetails.Add(SalaryDetails);

                                            }
                                            SalaryList.Add(objRecord);

                                        }
                                        catch (Exception ex)
                                        {
                                            ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                    }

                                    string responseData = POSTINPUT_RLCS_Common<List<Model_Ctc_POST>>(requestUrl, SalaryList);

                                    if (!string.IsNullOrEmpty(responseData))
                                    {
                                        var responsePOST = JsonConvert.DeserializeObject<RLCS_POSTAPI_Response>(responseData);

                                        if (responsePOST != null)
                                        {
                                            if (responsePOST.StatusCode != 0)
                                                saveSuccess = Convert.ToBoolean(responsePOST.StatusCode);

                                            if (saveSuccess)
                                            {                                                
                                                RLCS_ClientsManagement.Update_ProcessedStatus_Ctc(master_id, det_id, true);
                                            }

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                            }
                        }
                    }
                }
                    return saveSuccess;
                //}
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static string POSTINPUT_RLCS_Common<TypeOfValue>(string RequestURI, TypeOfValue objRecord)
        {
            try
            {
                string responseContent = string.Empty;
                string _ContentType = "application/json";

                HttpClient client = new HttpClient();
                int _TimeoutSec = 120;
                client.Timeout = new TimeSpan(0, 0, _TimeoutSec);

                client.BaseAddress = new Uri(RequestURI);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
                JavaScriptSerializer js = new JavaScriptSerializer();

                //string jsonData = js.Serialize(objRecord);

                HttpResponseMessage response = client.PostAsJsonAsync(RequestURI, objRecord).Result;

                if (response.IsSuccessStatusCode)
                    // get the rest/content of the response in a synchronous way
                    responseContent = response.Content.ReadAsStringAsync().Result;

                return responseContent;
            }
            catch (Exception ex)
            {              
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }


        public DataTable ConvertCSVtoDataTable(string strFilePath, out List<string> colHeaders, out List<string> unmapped, out string chkBlank, out List<string> repeated, int customerID,string clientid)
        {
            colHeaders = new List<string>();
            unmapped = new List<string>();
            repeated = new List<string>();

            DataTable dt = new DataTable();

            chkBlank = "N";
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                string[] headers = sr.ReadLine().Split(',');

                bool emptyElement = headers.Any(x => x == "");

                if (emptyElement)
                {
                    chkBlank = "Y";
                }
             
                repeated = headers.GroupBy(x => x)
                           .Where(g => g.Count() > 1)
                           .Select(y => y.Key)
                           .ToList();


                if (chkBlank != "Y" && repeated.Count == 0)
                {
                    dt.Columns.Add("excel_row_no");
                    foreach (string header in headers)
                    {                                                                      
                            RLCS_Client_Paycode_Mapping_Details objRecord = RLCS_ClientsManagement.GetStandardHeaderData(customerID, clientid,"", "S", header);
                            if (objRecord != null)
                            {
                                colHeaders.Add(objRecord.CPMD_Standard_Column);
                                dt.Columns.Add(objRecord.CPMD_Standard_Column);
                            }
                            else
                            {                               
                                RLCS_Client_Paycode_Mapping_Details objPay = RLCS_ClientsManagement.GetStandardHeaderData(customerID, clientid, "", "P", header);
                                if (objPay != null)
                                {
                                    colHeaders.Add(objPay.CPMD_PayCode);
                                    dt.Columns.Add(objPay.CPMD_PayCode);
                                }
                                else if(header == "EffectiveDate" || header == "Confirmation")
                                {
                                    colHeaders.Add(header);
                                    dt.Columns.Add(header);
                                }
                                else 
                                {
                                    unmapped.Add(header);
                                }
                            }                     
                    }

                    if (unmapped.Count == 0)
                    {
                        int rownum = 2;
                        while (!sr.EndOfStream)
                        {
                            string[] rows = sr.ReadLine().Split(',');
                            DataRow dr = dt.NewRow();
                            for (int i = 0; i <= headers.Length; i++)
                            {
                                if (i == 0)
                                {
                                    dr[i] = rownum;
                                }
                                else
                                {
                                    dr[i] = rows[i - 1];
                                }

                            }
                            rownum++;
                            dt.Rows.Add(dr);
                        }
                    }
                   
                }
            }

            return dt;
        }

        public DataTable ConvertCSVtoDataTableNew(string strFilePath, out List<string> colHeaders, out List<string> unmapped, out string chkBlank, out List<string> repeated, int customerID, string clientid)
        {           
            colHeaders = new List<string>();
            unmapped = new List<string>();
            repeated = new List<string>();

            DataTable dt = new DataTable();

            chkBlank = "N";
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                string[] headers = sr.ReadLine().Split(',');

                bool emptyElement = headers.Any(x => x == "");

                if (emptyElement)
                {
                    chkBlank = "Y";
                }

                repeated = headers.GroupBy(x => x)
                           .Where(g => g.Count() > 1)
                           .Select(y => y.Key)
                           .ToList();


                if (chkBlank != "Y" && repeated.Count == 0)
                {
                    dt.Columns.Add("excel_row_no");
                    List<SP_Get_ClientPaycodeMappingDetails_Result> objPaycode = new List<SP_Get_ClientPaycodeMappingDetails_Result>();
                    objPaycode = RLCS_ClientsManagement.GetAllPaycodeDetailsList(clientid);

                    foreach (string header in headers)
                    {
                        //RLCS_Client_Paycode_Mapping_Details objRecord = RLCS_ClientsManagement.GetStandardHeaderData(customerID, clientid, "", "S", header);
                        SP_Get_ClientPaycodeMappingDetails_Result objRecord = objPaycode.Where(t => t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid && t.CPMD_PayGroup.ToLower() == "standard" && t.CPMD_Header.ToLower() == header.ToLower()).FirstOrDefault();
                        if (objRecord != null)
                        {
                            colHeaders.Add(objRecord.CPMD_Standard_Column);
                            dt.Columns.Add(objRecord.CPMD_Standard_Column);
                        }
                        else
                        {
                            //RLCS_Client_Paycode_Mapping_Details objPay = RLCS_ClientsManagement.GetStandardHeaderData(customerID, clientid, "", "P", header);
                            SP_Get_ClientPaycodeMappingDetails_Result objPay = objPaycode.Where(t => t.AVACOM_CustomerID == customerID && t.CPMD_ClientID == clientid && t.CPMD_PayGroup.ToLower() == "paycodes" &&  t.CPMD_Header.ToLower() == header.ToLower()).FirstOrDefault();
                            if (objPay != null)
                            {
                                colHeaders.Add(objPay.CPMD_PayCode);
                                dt.Columns.Add(objPay.CPMD_PayCode);
                            }
                            else if (header == "EffectiveDate" || header == "Confirmation")
                            {
                                colHeaders.Add(header);
                                dt.Columns.Add(header);
                            }
                            else
                            {
                                unmapped.Add(header);
                            }
                        }
                    }

                    if (unmapped.Count == 0)
                    {
                        int rownum = 2;
                        while (!sr.EndOfStream)
                        {
                            string[] rows = sr.ReadLine().Split(',');
                            DataRow dr = dt.NewRow();
                            for (int i = 0; i <= headers.Length; i++)
                            {
                                if (i == 0)
                                {
                                    dr[i] = rownum;
                                }
                                else
                                {
                                    dr[i] = rows[i - 1];
                                }

                            }
                            rownum++;
                            dt.Rows.Add(dr);
                        }
                    }

                }
            }

            return dt;
        }

        public ActionResult downloadErrorCSV()
        {
            StringBuilder sb = new StringBuilder();
            if (Session["errorFile"] != null)
            {
                DataTable dtError = (DataTable)Session["errorFile"];
                
                if (dtError.Columns.Count > 0)
                {
                    foreach (DataColumn dc in dtError.Columns)
                    {
                        sb.Append(FormatCSV(dc.ColumnName.ToString()) + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    sb.AppendLine();

                    if (dtError.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtError.Rows)
                        {
                            foreach (DataColumn dc in dtError.Columns)
                            {
                                sb.Append(FormatCSV(dr[dc.ColumnName].ToString()) + ",");
                            }
                            sb.Remove(sb.Length - 1, 1);
                            sb.AppendLine();
                        }
                    }
                }
                
            }
            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "ErrorFile.csv");  
        }


        



        public ActionResult downloadCSV(string id, string clientid)
        {
            EmployeeCtcVModel vmodel = new EmployeeCtcVModel();
            try
            {
                int customerID = Convert.ToInt32(id);
                DataTable dt = new DataTable();
                List<RLCS_Client_Paycode_Mapping_Details> lstMappings = new List<RLCS_Client_Paycode_Mapping_Details>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstCols = RLCS_ClientsManagement.GetMappedPaycodesForClientsAndCustomers(customerID, clientid);
                    var lstRows = RLCS_ClientsManagement.GetEmployeeByCustomerAndClient(customerID, clientid);
                    if (lstCols.Count > 0)
                    {
                        foreach (var x in lstCols)
                        {
                            dt.Columns.Add(x.CPMD_Header);
                        }
                        dt.Columns.Add("EffectiveDate");
                        dt.Columns.Add("Confirmation");
                        if (lstRows.Count > 0)
                        {
                            foreach (var x in lstRows)
                            {
                                dt.Rows.Add(x.EM_EmpID, x.EM_ClientID);
                            }
                        }
                    }
                }
                StringBuilder sb = new StringBuilder();
                if (dt.Columns.Count > 0)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        sb.Append(FormatCSV(dc.ColumnName.ToString()) + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    sb.AppendLine();
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            foreach (DataColumn dc in dt.Columns)
                            {
                                sb.Append(FormatCSV(dr[dc.ColumnName].ToString()) + ",");
                            }
                            sb.Remove(sb.Length - 1, 1);
                            sb.AppendLine();
                        }
                    }
                }
                return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "InputFile.csv");
            }
            catch (Exception ee)
            {               
                vmodel.Error = true;
                vmodel.ErrorMsg = ee.Message.ToString();
                return View("uploadEmpCTCfiles", vmodel);               
            }
        }
        public static string FormatCSV(string input)
        {
            try
            {
                if (input == null)
                    return string.Empty;

                bool containsQuote = false;
                bool containsComma = false;
                int len = input.Length;
                for (int i = 0; i < len && (containsComma == false || containsQuote == false); i++)
                {
                    char ch = input[i];
                    if (ch == '"')
                        containsQuote = true;
                    else if (ch == ',')
                        containsComma = true;
                }

                if (containsQuote && containsComma)
                    input = input.Replace("\"", "\"\"");

                if (containsComma)
                    return "\"" + input + "\"";
                else
                    return input;
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return input;
            }
        }

        [HttpPost]
        public ActionResult UploadLocationFiles(FormCollection formCollection, UploadDocumentVModel UploadDocumentVModel)
        {
            if (Request != null)
            {
                try
                {
                    HttpPostedFileBase file = Request.Files["fileUpload"];

                    if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            UploadDocumentVModel.Error = false;

                            string fileName = file.FileName;
                            string fileWithoutExtn = Path.GetFileNameWithoutExtension(fileName);
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];

                            var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                            using (var package = new ExcelPackage(file.InputStream))
                            {
                                if (UploadDocumentVModel.ClientCheck == true)
                                {
                                    UploadDocumentVModel.Setup = ProcessClientsBasicDetails(package, fileWithoutExtn);
                                }
                                else
                                {
                                    UploadDocumentVModel.ClientLocationVModel = ProcessClientsLocationDetails(package, fileWithoutExtn);
                                }
                            }
                        }
                        else
                        {
                            UploadDocumentVModel.Error = true;
                        }
                    }
                    else
                    {
                        // UploadDocumentVModel.Error = true;
                        UploadDocumentVModel.ExtensionError = true;
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    UploadDocumentVModel.ServerError = true;
                }
            }
            return View(UploadDocumentVModel);
        }

        public ActionResult DownloadAttachment(string Name, string Sample, bool Emp)
        {
            UploadDocumentVModel UploadDocumentVModel = new UploadDocumentVModel();
            UploadEmployeeDocumentVModel EmpUpload = new UploadEmployeeDocumentVModel();

            try
            {
                string filePath = "";
                Sample = Request.QueryString["Sample"];

                if (Sample != null)
                {
                    if (Sample == "DownloadSampleLocation")
                    {
                        filePath = Server.MapPath("~/RLCSDocuments/SampleEntityBranchUpload.xlsx");
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, "SampleEntityBranchUpload.xlsx");
                    }
                    if (Sample == "RLCSDownloadSampleLocation")
                    {
                        filePath = Server.MapPath("~/RLCSDocuments/RLCSSampleEntityBranchUpload.xlsx");
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, "RLCSSampleEntityBranchUpload.xlsx");
                    }
                    if (Sample == "DownloadSampleEmployee")
                    {
                        filePath = Server.MapPath("~/RLCSDocuments/EmployeeSample.xlsx");
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, "EmployeeSample.xlsx");
                    }
                    else
                    {
                        filePath = ConfigurationManager.AppSettings["RLCS_LogFile"] + Name;
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, Name);
                    }
                }
                else
                {
                    UploadDocumentVModel.ServerError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                if (Emp == true)
                    EmpUpload.ServerError = true;
                else
                    UploadDocumentVModel.ServerError = true;
            }

            return View("UploadLocationFiles", UploadDocumentVModel);
        }



        public void GenerateEmpUploadErrorCSV(List<string> detailView, string FileName, string columnName)
        {

            try
            {

                if (!Directory.Exists(Server.MapPath("~/TempEmpError/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/TempEmpError/"));
                }

                string FilePath = Server.MapPath("~/TempEmpError/") + FileName;

                using (StreamWriter swOutputFile = new StreamWriter(new FileStream(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read)))
                {
                    string delimiter = "";
                    StringBuilder sb = new StringBuilder();
                    List<string> CsvRow = new List<string>();

                    if (detailView.Count > 0)
                    {
                        //string content = "Emp Id, Error Description, Row No";
                        string content = columnName + ", Error Description, Row No";
                        CsvRow.Add(content + Environment.NewLine);

                        foreach (var data in detailView)
                        {
                            if (data != null)
                            {
                                content = data.ToString();
                                CsvRow.Add(content + Environment.NewLine);
                            }
                        }
                    }

                    sb.AppendLine(string.Join(delimiter, CsvRow));
                    swOutputFile.WriteLine(sb.ToString());
                }
            }
            catch (Exception e)
            {

            }
        }


        public ActionResult DownloadEmployeeErrorCSV(string file)
        {
            string fullPath = string.Empty;
            try
            {
                fullPath = Path.Combine(Server.MapPath("~/TempEmpError"), file);

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return File(fullPath, "text/csv", file);
        }

        public ActionResult DownloadAttachmentNEW(string Name, string Sample, bool Emp, string CustomerID)
        {
            UploadDocumentVModel UploadDocumentVModel = new UploadDocumentVModel();
            UploadEmployeeDocumentVModel EmpUpload = new UploadEmployeeDocumentVModel();

            try
            {
                string filePath = "";
                Sample = Request.QueryString["Sample"];
                if (Sample != null && CustomerID != null)
                {
                    if (Sample == "DownloadSampleEmployeeUpdate")
                    {
                        DownloadEmployeeExcel(Convert.ToInt32(CustomerID));
                    }
                    else if (Sample=="DownloadSampleEmployeeBranchTransferUpdate")
                    {
                        filePath = Server.MapPath("~/RLCSDocuments/SampleEmployeeBranchTransferUpload.xlsx");
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, "SampleEmployeeBranchTransferUpload.xlsx");
                    }
                    else
                    {
                        filePath = ConfigurationManager.AppSettings["RLCS_LogFile"] + Name;
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, Name);
                    }
                    // byte[] fileBytes = System.IO.File.ReadAllBytes(file.Filepath);
                }
                else
                {
                    UploadDocumentVModel.ServerError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                if (Emp == true)
                    EmpUpload.ServerError = true;
                else
                    UploadDocumentVModel.ServerError = true;
            }

            return View("UploadEmployeeFilesNew", UploadDocumentVModel);
        }
        private void DownloadEmployeeExcel(int CustomerID)
        {

            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Employee_Master");
                    DataTable ExcelData = null;
                    // int CustomerID = Convert.ToInt32(TempData["CustID"]);
                    var detailView = RLCSManagement.GetEmployeeDetails_Clientwise(CustomerID);

                    ExcelData = (detailView).ToDataTable();
                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Row(1).Height = 65;
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "ClientID";
                    exWorkSheet.Column(1).Width = 15;

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Emp ID";
                    exWorkSheet.Column(2).Width = 15;
                    //exWorkSheet.Cells["B1"].AutoFitColumns(10);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Emp Name";
                    exWorkSheet.Column(3).Width = 20;
                    //exWorkSheet.Cells["C1"].AutoFitColumns(30);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Branch";
                    exWorkSheet.Column(4).Width = 15;
                    //  exWorkSheet.Cells["D1"].AutoFitColumns(15);

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Gender(FEMALE/MALE)";
                    exWorkSheet.Column(5).Width = 10;
                    //exWorkSheet.Cells["E1"].AutoFitColumns(10);

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "Father Name/Husband Name";
                    exWorkSheet.Column(6).Width = 15;
                    //exWorkSheet.Cells["F1"].AutoFitColumns(15);
                    var range0 = exWorkSheet.Cells[1, 1, 1, 5];
                    range0.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range0.Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "DOB(DATE)i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(7).Width = 15;
                    // exWorkSheet.Cells["G1"].AutoFitColumns(15);
                    using (ExcelRange col = exWorkSheet.Cells[1, 7, 1 + ExcelData.Rows.Count, 7])
                    {
                        //col.Style.Numberformat.Format = ("dd/MM/yyyy").ToString();
                        col.Style.Numberformat.Format = "@";
                    }

                    var range = exWorkSheet.Cells[1, 1, 1,7];
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Red);


                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].Value = "Relationship";
                    exWorkSheet.Column(8).Width = 15;
                    // exWorkSheet.Cells["H1"].AutoFitColumns(10);
                    var range8 = exWorkSheet.Cells[1, 8];
                    range8.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range8.Style.Fill.BackgroundColor.SetColor(Color.White);

                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].Value = "DOJ(DATE)i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(9).Width = 15;
                    //exWorkSheet.Cells["I1"].AutoFitColumns(15);
                    using (ExcelRange col = exWorkSheet.Cells[1, 9, 1 + ExcelData.Rows.Count, 9])
                    {
                        col.Style.Numberformat.Format = "@";
                        // col.Style.Numberformat.Format = ("dd/MM/yyyy").ToString();
                    }
                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].Value = "Firsttime_secondtime(FIRST TIME/ SECOND TIME)";
                    exWorkSheet.Column(10).Width = 15;
                    // exWorkSheet.Cells["J1"].AutoFitColumns(10);


                    ///10Column END
                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].Value = "International_workers(YES/NO)";
                    exWorkSheet.Column(11).Width = 10;

                    exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L1"].Value = "PF_Capping_Applicability(YES/NO)";
                    exWorkSheet.Column(12).Width = 10;

                    exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M1"].Value = "Status(ACTIVE/INACTIVE)";
                    exWorkSheet.Column(13).Width = 10;
                    var range01 = exWorkSheet.Cells[1, 7, 1, 13];
                    range01.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range01.Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["N1"].Value = "Martial Status(MARRIED/UNMARRIED)";
                    exWorkSheet.Column(14).Width = 10;

                    exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["O1"].Value = "Physically Challenged(YES/NO)";
                    exWorkSheet.Column(15).Width = 10;

                    exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["P1"].Value = "PayrollMonth(MONTH INTEGER)";
                    exWorkSheet.Column(16).Width = 15;

                    exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Q1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Q1"].Value = "PayrollYear(YEAR INTEGER)";
                    exWorkSheet.Column(17).Width = 10;

                    exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["R1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["R1"].Value = "Courage Month(MONTH INTEGER)";
                    exWorkSheet.Column(18).Width = 10;

                    exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["S1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["S1"].Value = "Courage Year(YEAR INTEGER)";
                    exWorkSheet.Column(19).Width = 10;

                    exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["T1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["T1"].Value = "Security Provided(YES/NO)";
                    exWorkSheet.Column(20).Width = 10;
                    ////20END RED COLOUR
                    var range20 = exWorkSheet.Cells[1,9, 1, 20];
                    range20.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range20.Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet.Cells["U1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["U1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["U1"].Value = "Women working Night shift (YES/NO)";
                    exWorkSheet.Column(21).Width = 10;

                    exWorkSheet.Cells["V1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["V1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["V1"].Value = "Department";
                    exWorkSheet.Column(22).Width = 15;

                    exWorkSheet.Cells["W1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["W1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["W1"].Value = "PT State";
                    exWorkSheet.Column(23).Width = 10;

                    exWorkSheet.Cells["X1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["X1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["X1"].Value = "Date of Leaving(DATE)i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(24).Width = 15;
                    using (ExcelRange col = exWorkSheet.Cells[1, 23, 1 + ExcelData.Rows.Count, 23])
                    {
                        col.Style.Numberformat.Format = "@";
                        //col.Style.Numberformat.Format = ("dd/MM/yyyy").ToString();
                    }

                    exWorkSheet.Cells["Y1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Y1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Y1"].Value = "Email ID";
                    exWorkSheet.Column(25).Width = 15;

                    exWorkSheet.Cells["Z1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Z1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Z1"].Value = "Mobile No";
                    exWorkSheet.Column(26).Width = 10;

                    exWorkSheet.Cells["AA1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AA1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AA1"].Value = "Skill Category i.e.(HIGHLY SKILLED/SKILLED/SEMI SKILLED/UNSKILLED)";
                    exWorkSheet.Column(27).Width = 15;

                    var range27 = exWorkSheet.Cells[1, 27];
                    range27.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range27.Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet.Cells["AB1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AB1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AB1"].Value = "Passport No";
                    exWorkSheet.Column(28).Width = 15;

                    exWorkSheet.Cells["AC1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AC1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AC1"].Value = "Effective change date(DATE)i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(29).Width = 15;
                    using (ExcelRange col = exWorkSheet.Cells[1, 29, 1 + ExcelData.Rows.Count, 29])
                    {
                        col.Style.Numberformat.Format = "@";
                        //col.Style.Numberformat.Format = ("dd/MM/yyyy").ToString();
                    }

                    exWorkSheet.Cells["AD1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AD1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AD1"].Value = "Address";
                    exWorkSheet.Column(30).Width = 15;
                    //30END
                    exWorkSheet.Cells["AE1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AE1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AE1"].Value = "Designation";
                    exWorkSheet.Column(31).Width = 35;

                    exWorkSheet.Cells["AF1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AF1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AF1"].Value = "ESICNO";
                    exWorkSheet.Column(32).Width = 15;

                    exWorkSheet.Cells["AG1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AG1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AG1"].Value = "UAN";
                    exWorkSheet.Column(33).Width = 15;

                    exWorkSheet.Cells["AH1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AH1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AH1"].Value = "PAN Number";
                    exWorkSheet.Column(34).Width = 10;

                    exWorkSheet.Cells["AI1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AI1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AI1"].Value = "Adhar Card No.";
                    exWorkSheet.Column(35).Width = 15;

                    exWorkSheet.Cells["AJ1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AJ1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AJ1"].Value = "Bank Name";
                    exWorkSheet.Column(36).Width = 15;

                    exWorkSheet.Cells["AK1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AK1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AK1"].Value = "Bankaccountnumber";
                    exWorkSheet.Column(37).Width = 15;

                    exWorkSheet.Cells["AL1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AL1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AL1"].Value = "Passport Valid From(DATE)i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(38).Width = 15;
                    using (ExcelRange col = exWorkSheet.Cells[1, 38, 1 + ExcelData.Rows.Count, 38])
                    {
                        col.Style.Numberformat.Format = "@";
                        //col.Style.Numberformat.Format = ("dd/MM/yyyy").ToString();
                    }

                    exWorkSheet.Cells["AM1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AM1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AM1"].Value = "Passport Valid To(DATE)i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(39).Width = 15;
                    using (ExcelRange col = exWorkSheet.Cells[1, 39, 1 + ExcelData.Rows.Count, 39])
                    {
                        col.Style.Numberformat.Format = "@";
                        //col.Style.Numberformat.Format = ("dd/MM/yyyy").ToString();
                    }

                    exWorkSheet.Cells["AN1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AN1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AN1"].Value = "EPFO Aadhar(YES/NO)";
                    exWorkSheet.Column(40).Width = 10;
                    //40END
                    exWorkSheet.Cells["AO1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AO1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AO1"].Value = "IFSC";
                    exWorkSheet.Column(41).Width = 10;

                    exWorkSheet.Cells["AP1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AP1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AP1"].Value = "Nationality";
                    exWorkSheet.Column(42).Width = 10;

                    exWorkSheet.Cells["AQ1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AQ1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AQ1"].Value = "PF_Applicability(YES/NO)";
                    exWorkSheet.Column(43).Width = 10;

                    exWorkSheet.Cells["AR1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AR1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AR1"].Value = "ESI_Applicability(YES/NO)";
                    exWorkSheet.Column(44).Width = 10;

                    exWorkSheet.Cells["AS1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AS1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AS1"].Value = "PFNO";
                    exWorkSheet.Column(45).Width = 15;

                    exWorkSheet.Cells["AT1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AT1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AT1"].Value = "EPFO Bank A/c(YES/NO)";
                    exWorkSheet.Column(46).Width = 10;

                    exWorkSheet.Cells["AU1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AU1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AU1"].Value = "EPFO PAN(YES/NO)";
                    exWorkSheet.Column(47).Width = 10;

                    exWorkSheet.Cells["AV1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AV1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AV1"].Value = "Passport Issued Country";
                    exWorkSheet.Column(48).Width = 10;

                    exWorkSheet.Cells["AW1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AW1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AW1"].Value = "PMPRY(YES/NO)";
                    exWorkSheet.Column(49).Width = 10;

                    exWorkSheet.Cells["AX1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AX1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AX1"].Value = "PT ApplicabilityYES/NO)";
                    exWorkSheet.Column(50).Width = 10;
                    //50END
                    exWorkSheet.Cells["AY1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AY1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AY1"].Value = "No Of Certificate";
                    exWorkSheet.Column(51).Width = 15;

                    exWorkSheet.Cells["AZ1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["AZ1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["AZ1"].Value = "No of Certificate Date (Date)i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(52).Width = 15;
                    using (ExcelRange col = exWorkSheet.Cells[1, 52, 1 + ExcelData.Rows.Count, 52])
                    {
                        col.Style.Numberformat.Format = "@";
                        //col.Style.Numberformat.Format = ("dd/MM/yyyy").ToString();
                    }

                    exWorkSheet.Cells["BA1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BA1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BA1"].Value = "Token NO";
                    exWorkSheet.Column(53).Width = 10;

                    exWorkSheet.Cells["BB1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BB1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BB1"].Value = "Relay Assigned";
                    exWorkSheet.Column(54).Width = 10;

                    exWorkSheet.Cells["BC1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BC1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BC1"].Value = "Letter Of Group";
                    exWorkSheet.Column(55).Width = 10;

                    exWorkSheet.Cells["BD1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BD1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BD1"].Value = "Mode of transport(Cab/Transport Facility";
                    exWorkSheet.Column(56).Width = 15;

                    exWorkSheet.Cells["BE1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BE1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BE1"].Value = "years of Experience";
                    exWorkSheet.Column(57).Width = 10;

                    exWorkSheet.Cells["BF1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BF1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BF1"].Value = "Employment Type(i.e.PERMANENT/CONTRACT)";
                    exWorkSheet.Column(58).Width = 15;
                    var range58 = exWorkSheet.Cells[1, 58];
                    range58.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range58.Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet.Cells["BG1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BG1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BG1"].Value = "Client PT State";
                    exWorkSheet.Column(59).Width = 10;

                    exWorkSheet.Cells["BH1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BH1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BH1"].Value = "EPS Applicability(YES/NO)";
                    exWorkSheet.Column(60).Width = 10;

                    exWorkSheet.Cells["BI1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BI1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BI1"].Value = "Client ESI NO";
                    exWorkSheet.Column(61).Width = 10;

                    //NEW ADD 
                    exWorkSheet.Cells["BJ1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BJ1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BJ1"].Value = "Permanent Address";
                    exWorkSheet.Column(62).Width = 15;

                    exWorkSheet.Cells["BK1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BK1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BK1"].Value = "Identityfication Mark";
                    exWorkSheet.Column(63).Width = 15;

                    exWorkSheet.Cells["BL1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BL1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BL1"].Value = "Emergency Contact Address";
                    exWorkSheet.Column(64).Width = 15;

                    exWorkSheet.Cells["BM1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BM1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BM1"].Value = "Emergency Contact No";
                    exWorkSheet.Column(65).Width = 12;

                    exWorkSheet.Cells["BN1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BN1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BN1"].Value = "Training No";
                    exWorkSheet.Column(66).Width = 10;

                    exWorkSheet.Cells["BO1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BO1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BO1"].Value = "Training Date i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(67).Width = 15;
                    using (ExcelRange col = exWorkSheet.Cells[1, 67, 1 + ExcelData.Rows.Count, 67])
                    {
                        col.Style.Numberformat.Format = "@";
                    }

                    exWorkSheet.Cells["BP1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BP1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BP1"].Value = "Place Of Work(Underground/Open Cast/Surface)";
                    exWorkSheet.Column(68).Width = 15;

                    exWorkSheet.Cells["BQ1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BQ1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BQ1"].Value = "Remark";
                    exWorkSheet.Column(69).Width = 10;            

                    exWorkSheet.Cells["BR1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BR1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BR1"].Value = "Educational Level";
                    exWorkSheet.Column(70).Width = 12;

                    exWorkSheet.Cells["BS1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BS1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BS1"].Value = "Place Of Employment";
                    exWorkSheet.Column(71).Width = 12;

                    exWorkSheet.Cells["BT1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BT1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BT1"].Value = "Date Of Cloth Given i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(72).Width = 15;
                    using (ExcelRange col = exWorkSheet.Cells[1, 72, 1 + ExcelData.Rows.Count, 72])
                    {
                        col.Style.Numberformat.Format = "@";
                    }
                    exWorkSheet.Cells["BU1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BU1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BU1"].Value = "Date Of Exempting Order i.e.(DD/MM/YYYY)";
                    exWorkSheet.Column(73).Width = 15;
                    using (ExcelRange col = exWorkSheet.Cells[1, 73, 1 + ExcelData.Rows.Count, 73])
                    {
                        col.Style.Numberformat.Format = "@";
                    }
                    exWorkSheet.Cells["BV1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BV1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BV1"].Value = "Particulars Of Transfer From One Group To Another";
                    exWorkSheet.Column(74).Width = 15;

                    exWorkSheet.Cells["BW1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BW1"].Style.Font.Size = 14;
                    exWorkSheet.Cells["BW1"].Value = "Sales Promotion(YES/NO)";
                    exWorkSheet.Column(75).Width = 11;

                    exWorkSheet.Cells["BX1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BX1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BX1"].Value = "Payment Mode";
                    exWorkSheet.Column(76).Width = 10;

                    exWorkSheet.Cells["BY1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BY1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BY1"].Value = "Is Lwf Exempted";
                    exWorkSheet.Column(77).Width = 10;
                    //Newly Added Fields
                    exWorkSheet.Cells["BZ1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["BZ1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["BZ1"].Value = "Exempted from S&E Act(YES/NO)";
                    exWorkSheet.Column(78).Width = 10;

                    exWorkSheet.Cells["CA1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["CA1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["CA1"].Value = "VPF Applicability(YES/NO)";
                    exWorkSheet.Column(79).Width = 10;

                    exWorkSheet.Cells["CB1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["CB1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["CB1"].Value = "VPF Type(Inputed Percentage/Fixed)";
                    exWorkSheet.Column(80).Width = 12;

                    exWorkSheet.Cells["CC1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["CC1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["CC1"].Value = "VPF Value";
                    exWorkSheet.Column(81).Width = 10;

                    exWorkSheet.Cells["CD1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["CD1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["CD1"].Value = "Sector";
                    exWorkSheet.Column(82).Width = 10;

                    exWorkSheet.Cells["CE1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["CE1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["CE1"].Value = "Job Category";
                    exWorkSheet.Column(83).Width = 15;

                    exWorkSheet.Cells["CF1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["CF1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["CF1"].Value = "Industry Type";
                    exWorkSheet.Column(84).Width = 15;

                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 84])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        col.Style.WrapText = true;
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=EmployeeSampleUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.Flush(); // Sends all currently buffered output to the client.
                    Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                                      // ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                catch (Exception)
                {
                }

            }
        }


        private void DownloadPaycodeExcel()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    string Paycode = "D";
                    string Paycode1 = "E";
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {

                        var lstEntities = (from row in entities.RLCS_Client_Paycode_Mapping_Details
                                           where !string.IsNullOrEmpty(row.CPMD_Standard_Column)
                                           && row.CPMD_Status == "A"
                                           select row).Distinct().ToList();

                        if (lstEntities.Count > 0)
                        {
                            lstEntities = (from g in lstEntities
                                           group g by new
                                           {
                                               g.CPMD_Standard_Column,
                                               g.CPMD_PayGroup
                                           }
                                           into GCS
                                           select new RLCS_Client_Paycode_Mapping_Details()
                                           {
                                               CPMD_Standard_Column = GCS.Key.CPMD_Standard_Column,
                                               CPMD_PayGroup = GCS.Key.CPMD_PayGroup
                                           }).ToList();
                        }

                        var PaycodeMasterModellist = new List<RLCS_PayCode_Master>();
                        var PaycodeMasterModellist1 = new List<RLCS_PayCode_Master>();
                        PaycodeMasterModellist = RLCS_ClientsManagement.GetPaycodeByType(Paycode);
                        PaycodeMasterModellist1 = RLCS_ClientsManagement.GetPaycodeByType(Paycode1);

                        DataTable table = new DataTable();

                        table.Columns.Add("SrNO", typeof(string));
                        table.Columns.Add("PaycodeType", typeof(string));
                        table.Columns.Add("PaycodeName", typeof(string));

                        int P = 1;
                        string PaycodeDType = "Decuction Paycode";
                        string PaycodeEType = "Earning Paycode";
                        foreach (var item in lstEntities)
                        {
                            table.Rows.Add(P, item.CPMD_PayGroup, item.CPMD_Standard_Column);
                            P++;
                        }

                        foreach (var item in PaycodeMasterModellist)
                        {
                            table.Rows.Add(P, PaycodeDType, item.PEM_Pay_Code_Description);
                            P++;
                        }

                        foreach (var item in PaycodeMasterModellist1)
                        {
                            table.Rows.Add(P, PaycodeEType, item.PEM_Pay_Code_Description);
                            P++;
                        }

                        ExcelWorksheet exWorkSheet9 = exportPackge.Workbook.Worksheets.Add("Paycode");
                        DataView view = new System.Data.DataView(table);
                        DataTable ExcelData = null;
                        ExcelData = view.ToTable("Selected", false, "SrNO", "PaycodeType", "PaycodeName");


                        exWorkSheet9.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet9.Cells["A1"].Value = "Sr No.";
                        exWorkSheet9.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet9.Cells["A1"].AutoFitColumns(20);
                        exWorkSheet9.Cells["A1:A2"].Merge = true;
                        exWorkSheet9.Cells["A1:A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet9.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet9.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet9.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet9.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet9.Cells["B1"].Value = "PayCode Type";
                        exWorkSheet9.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet9.Cells["B1"].AutoFitColumns(20);
                        exWorkSheet9.Cells["B1:B2"].Merge = true;
                        exWorkSheet9.Cells["B1:B2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet9.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet9.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet9.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                        exWorkSheet9.Cells["C1"].Value = "PayCode Name";
                        exWorkSheet9.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet9.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet9.Cells["C1"].AutoFitColumns(20);
                        exWorkSheet9.Cells["C1:C2"].Merge = true;
                        exWorkSheet9.Cells["C1:C2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet9.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet9.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet9.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                        if (ExcelData.Rows.Count > 0)
                        {
                            exWorkSheet9.Cells["A3"].LoadFromDataTable(ExcelData, false);
                            exWorkSheet9.DefaultRowHeight = 42;
                        }

                        exWorkSheet9.Column(1).Width = 9;
                        exWorkSheet9.Column(2).Width = 32;
                        exWorkSheet9.Column(3).Width = 46;

                        int count = Convert.ToInt32(ExcelData.Rows.Count) + 2;
                        using (ExcelRange col = exWorkSheet9.Cells[1, 1, count, 3])
                        {
                            col.Style.WrapText = true;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeeSampleUpdate.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        Response.Flush(); // Sends all currently buffered output to the client.
                        Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                                          // ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
                catch (Exception)
                {
                }

            }
        }

        public BasicSetupModel ProcessClientsBasicDetails(ExcelPackage xlWorkbook, string fileName)
        {
            //
            int CustID = 0;
            int ComplianceProdType;
            int count = 0;

            jsonClientDetails jsonClientDetails = new jsonClientDetails();
            BasicSetupModel BasicSetupModel = new BasicSetupModel();
            ClientDetailsVModel ClientDetailsVModel = new ClientDetailsVModel();
            ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Entity"];

            try
            {
                if (xlWorksheet != null)
                {
                    // CustID = (int)AuthenticationHelper.CustomerID;
                    string Sdate = string.Empty;
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = GetLastUsedRow(xlWorksheet);

                    List<String> LogErrors = new List<string>();

                    #region Validation

                    for (int i = 2; i <= noOfRow; i++)
                    {
                        BasicSetupModel = new BasicSetupModel();

                        count = count + 1;

                        string corporateID = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
                        if (string.IsNullOrEmpty(corporateID))
                            LogErrors.Add("Required CorporateID at Row-" + count);
                        else if (!String.IsNullOrEmpty(corporateID) && corporateID.GetType() == typeof(string))
                        {
                            bool Corporate = RLCS_ClientsManagement.CheckExistsCorporate(corporateID);
                            if (Corporate == false)
                                LogErrors.Add("CorporateID at Row-" + count + "does not Exist");
                            else
                                BasicSetupModel.CO_CorporateID = corporateID;
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.Trim()))
                            LogErrors.Add("Required ClientID at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()) && xlWorksheet.Cells[i, 2].Text.Trim().GetType() == typeof(string))
                        {
                            string excelClientID = xlWorksheet.Cells[i, 2].Text.ToString().Trim();

                            if (excelClientID.Length > 10)
                                LogErrors.Add("ClientID can be maximum of 10 characters at Row-" + count);
                            else
                            {
                                bool entityExists = RLCS_Master_Management.Exists_CorporateID(excelClientID);
                                if (entityExists == true)
                                    LogErrors.Add("Entity with ClientID at Row-" + count + " already exists. Please provide unique ClientID");
                                else
                                {
                                    bool Client = RLCS_ClientsManagement.ExistsClientID(excelClientID);
                                    if (Client == true)
                                        LogErrors.Add("ClientID at Row-" + count + " already exists. Please provide unique ClientID");
                                    else
                                    {
                                        //Check for in other Rows Same ClientID
                                        Client = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 2, excelClientID);
                                        if (Client == true)
                                            LogErrors.Add("ClientID at Row-" + count + " already exists in uploaded excel document. Please provide unique ClientID");
                                        else
                                        {
                                            bool clientExists = false;

                                            string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                                            rlcsAPIURL += "AventisIntegration/CheckClientIdExists?ClientId=" + excelClientID;

                                            string responseData = RLCSAPIClasses.Invoke("GET", rlcsAPIURL, "");

                                            if (responseData != null)
                                            {
                                                string data = responseData;
                                                if (!string.IsNullOrWhiteSpace(data))
                                                    clientExists = Convert.ToBoolean(data);

                                                if (clientExists)
                                                    LogErrors.Add("ClientID at Row-" + count + " already exists in uploaded excel document. Please provide unique ClientID");
                                                else
                                                {
                                                    bool PF = Regex.IsMatch(excelClientID, @"^[a-zA-Z0-9]+$");
                                                    //
                                                    if (PF == false)
                                                        LogErrors.Add("Enter valid ClientID " + excelClientID + " at Row -" + count);
                                                    else
                                                    {
                                                        BasicSetupModel.CM_ClientID = xlWorksheet.Cells[i, 2].Text.Trim();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                            LogErrors.Add("Required Entity/Client Name at Row-" + count + 1);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()) && xlWorksheet.Cells[i, 3].Text.ToString().Trim().GetType() == typeof(string))
                        {

                            bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 3].Text.ToString().Trim(), @"^[A-Za-z0-9 -@.#&()_+,//\\]+$");
                            if (matched == false)
                                LogErrors.Add("Enter valid Entity/Client Name at Row- " + count);
                            else
                            {
                                string excelClientName = xlWorksheet.Cells[i, 3].Text.ToString().Trim();

                                bool Client = RLCS_ClientsManagement.GetClientName(excelClientName);
                                if (Client == true)
                                    LogErrors.Add("Entity/Client Name at Row-" + count + " already exists. Please provide unique Entity/Client Name");
                                else
                                {
                                    //Check for in other Rows Same ClientNme
                                    Client = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 3, excelClientName);
                                    if (Client == true)
                                        LogErrors.Add("Entity/Client Name at Row-" + count + " already exists in uploaded excel document. Please provide unique Entity/Client Name");
                                    else
                                        BasicSetupModel.CM_ClientName = excelClientName;
                                }
                            }
                        }

                        // if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.Trim()))
                        //    LogErrors.Add("Required Act Applicability at Row-" + count);
                        // else
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()) && xlWorksheet.Cells[i, 4].Text.Trim().GetType() == typeof(string))
                        {
                            if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper() == "CENTRAL")
                                BasicSetupModel.ActApplicablity = "CEN";
                            else if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper() == "STATE")
                                BasicSetupModel.ActApplicablity = "STATE";
                            else
                            {
                                if (String.IsNullOrEmpty(BasicSetupModel.ActApplicablity))
                                    LogErrors.Add("Enter valid Act Applicability at Row-" + count + ". Enter either Central or State.");
                            }
                        }

                        //industy Type
                        string ExcelIndustryType = xlWorksheet.Cells[i, 5].Text.ToString().Trim();
                        string[] ArrayIndustryTypes = new string[] { "Oil and Natural gas", "Banking Sector", "Telecommunication", "Air Transportation", "Insurance", "Mines" };
                        if (BasicSetupModel.ActApplicablity == "STATE" && !String.IsNullOrEmpty(ExcelIndustryType))
                        {
                            LogErrors.Add("Industry type can only be selected for central acts at Row-" + count);
                        }
                        if (BasicSetupModel.ActApplicablity == "CEN" && !String.IsNullOrEmpty(ExcelIndustryType))
                        {
                            bool isValidIndustryType = false;
                            foreach (var item in ArrayIndustryTypes)
                            {
                                if (ExcelIndustryType == item) isValidIndustryType = true;
                            }
                            if (!isValidIndustryType)
                            {
                                LogErrors.Add("Select valid industry types at Row-" + count);
                            }
                            else
                            {
                                jsonClientDetails.Industry_Type = ExcelIndustryType;
                            }
                        }

                        //Establishment type
                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                            LogErrors.Add("Required Establishment Type at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()) && xlWorksheet.Cells[i, 6].Text.Trim().GetType() == typeof(string))
                        {
                            string estType = xlWorksheet.Cells[i, 6].Text.ToString().Trim().ToUpper();

                            if (estType == "FACTORY" || estType == "FACT")
                                BasicSetupModel.CM_EstablishmentType = "FACT";
                            else if (estType == "SHOP & ESTABLISHMENT" || estType == "SEA" || estType == "SHOP AND ESTABLISHMENT")
                                BasicSetupModel.CM_EstablishmentType = "SEA";
                            else if (estType == "BOTH" || estType == "BOTH")
                                BasicSetupModel.CM_EstablishmentType = "SF";
                            else
                            {
                                if (String.IsNullOrEmpty(BasicSetupModel.CM_EstablishmentType))
                                    LogErrors.Add("Enter valid value for Establishment Type at Row-" + count + ".Enter Factory or Shop & Establishment or Both");
                            }
                        }

                        //if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.Trim()))
                        //    LogErrors.Add("Required Service Start Date at Row-" + count);
                        //else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()) && xlWorksheet.Cells[i, 6].Text.Trim().GetType() == typeof(string))
                        //{
                        //    string excelserDate = xlWorksheet.Cells[i, 6].Text;

                        //    try
                        //    {
                        //        DateTime dt = DateTime.ParseExact(excelserDate, "dd/MM/yyyy", null);
                        //        //DateTime dt = DateTime.Parse(excelserDate);
                        //        BasicSetupModel.CM_ServiceStartDate = dt.ToString();
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        LogErrors.Add("Enter Valid Service Start Date at Row-" + count);
                        //    }
                        //}

                        //state
                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim())) //check state with database or give error
                            LogErrors.Add("Required State at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim().ToUpper()) && xlWorksheet.Cells[i, 7].Text.Trim().GetType() == typeof(string))
                        {
                            var stateRecord = RLCS_ClientsManagement.GetStateRecordbyCode(xlWorksheet.Cells[i, 7].Text.ToString().Trim().ToUpper());

                            if (stateRecord != null)
                            {
                                BasicSetupModel.StateModel.SM_Code = stateRecord.SM_Code;
                                BasicSetupModel.StateModel.AVACOM_StateID = stateRecord.AVACOM_StateID;
                            }
                            else
                            {
                                LogErrors.Add("No State found with " + xlWorksheet.Cells[i, 7].Text.ToString().Trim().ToUpper() + ", check at Row-" + count);
                            }
                        }


                        //city
                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.Trim()))
                            LogErrors.Add("Required City at Row" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()) && xlWorksheet.Cells[i, 8].Text.Trim().GetType() == typeof(string))
                        {
                            bool city = RLCS_ClientsManagement.GetCitybyCode(BasicSetupModel.StateModel.SM_Code, xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                            if (city == false)
                                LogErrors.Add("No match found for City at Row-" + count);
                            else
                                BasicSetupModel.CM_City = xlWorksheet.Cells[i, 8].Text.ToString().Trim();
                        }

                        //address
                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.Trim()))
                            LogErrors.Add("Required Address at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()) && xlWorksheet.Cells[i, 9].Text.Trim().GetType() == typeof(string))
                        {
                            BasicSetupModel.CM_Address = xlWorksheet.Cells[i, 9].Text.Trim();
                        }

                        //bonus percentage
                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.Trim()))
                            LogErrors.Add("Required Bonus Percentage at Row" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                        {
                            try
                            {
                                if (xlWorksheet.Cells[i, 10].Text.ToString().Trim().Length > 5)
                                {
                                    LogErrors.Add("Bonus percentage should not be more than 5 digits. Check at Row -" + count);
                                }
                                else
                                {
                                    if (Convert.ToDecimal(xlWorksheet.Cells[i, 10].Text.Trim()) >= 0)
                                    {
                                        decimal d = Convert.ToDecimal(xlWorksheet.Cells[i, 10].Text.Trim());
                                        BasicSetupModel.CM_BonusPercentage = d;
                                    }
                                    else
                                        LogErrors.Add("please enter positive value for bonus percentage at Row" + count);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Bonus Percentage must be number, check at Row-" + count);
                            }
                        }

                        //Exemption
                        string bonusExemption = xlWorksheet.Cells[i, 11].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(bonusExemption))
                            LogErrors.Add("Required Exemption at Row-" + count);
                        else
                        {
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                            {
                                if (xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 11].Text != null)
                                {
                                    LogErrors.Add("Invalid value for Exemption. Enter Yes or No");
                                }
                                else
                                {
                                    if (xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() == "YES" || xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() == "Y")
                                        BasicSetupModel.CM_Excemption = true;
                                    else
                                        BasicSetupModel.CM_Excemption = false;
                                }
                            }
                        }

                        //wage period from
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                            LogErrors.Add("Required Wage Period(From) To at Row" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()) && xlWorksheet.Cells[i, 12].Text.Trim().GetType() == typeof(string))
                        {
                            try
                            {
                                if (Convert.ToInt32(xlWorksheet.Cells[i, 12].Text.Trim()) > 0)
                                {
                                    int wagefrom = Convert.ToInt32(xlWorksheet.Cells[i, 12].Text.Trim());

                                    if (wagefrom > 31)
                                        LogErrors.Add("Wage Period(From) should be between 1 to 31 at Row-" + count);
                                    else
                                        ClientDetailsVModel.CB_WagePeriodFrom = Convert.ToString(xlWorksheet.Cells[i, 12].Text);
                                }
                                else
                                    LogErrors.Add("Please enter positive value for Wage Period(From) at Row-" + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Wage Period(From) must be number and between 1 to 31, Check at Row-" + count);
                            }
                        }

                        //wage period to
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                            LogErrors.Add("Required Wage Period(To) at Row" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()) && xlWorksheet.Cells[i, 13].Text.Trim().GetType() == typeof(string))
                        {
                            try
                            {
                                if (Convert.ToInt32(xlWorksheet.Cells[i, 13].Text.Trim()) > 0)
                                {
                                    int wagefrom = Convert.ToInt32(xlWorksheet.Cells[i, 13].Text);
                                    if (wagefrom > 31)
                                        LogErrors.Add("Wage Period(To) should be between 1 to 31 at Row-" + count);
                                    else
                                        ClientDetailsVModel.CB_WagePeriodTo = Convert.ToString(xlWorksheet.Cells[i, 13].Text);
                                }
                                else
                                    LogErrors.Add("Please enter positive value for Wage Period(To) at Row-" + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Wage Period(To) must be number and between 1 to 31, Check at Row-" + count);
                            }
                        }

                        /*if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                            LogErrors.Add("Required Wage Payment Day at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()) && xlWorksheet.Cells[i, 13].Text.Trim().GetType() == typeof(string))
                        {
                            try
                            {
                                if (Convert.ToInt32(xlWorksheet.Cells[i, 13].Text.Trim()) > 0)
                                {
                                    int PaymentDate = Convert.ToInt32(xlWorksheet.Cells[i, 13].Text);
                                    if (PaymentDate > 31)
                                        LogErrors.Add("Payment Day should be between 1 to 31 at Row-" + count);
                                    else
                                        ClientDetailsVModel.CB_PaymentDate = xlWorksheet.Cells[i, 13].Text.Trim();
                                }
                                else
                                    LogErrors.Add("Please enter positive value for Payment Day at Row-" + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Payment Day must be number and between 1 to 31, Check at Row-" + count);
                            }
                        }*/

                        //wage payment day
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                            LogErrors.Add("Required Wage Payment Day at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()) && xlWorksheet.Cells[i, 14].Text.Trim().GetType() == typeof(string))
                        {
                            try
                            {
                                if (Convert.ToInt32(xlWorksheet.Cells[i, 14].Text.Trim()) > 0)
                                {
                                    int PaymentDate = Convert.ToInt32(xlWorksheet.Cells[i, 14].Text);
                                    if (PaymentDate >= 1 && PaymentDate <= 28)
                                        ClientDetailsVModel.CB_PaymentDate = xlWorksheet.Cells[i, 14].Text.Trim();
                                    else
                                        LogErrors.Add("Payment Day should be between 1 to 28 or LDM at Row-" + count);
                                }
                                else
                                    LogErrors.Add("Please enter positive value for Payment Day at Row-" + count);
                            }
                            catch (Exception ex)
                            {
                                if (xlWorksheet.Cells[i, 14].Text.Trim().ToUpper() == "LDM")
                                {
                                    ClientDetailsVModel.CB_PaymentDate = "31";
                                }
                                else
                                    LogErrors.Add("Payment Day must be number and between 1 to 28 or LDM, Check at Row-" + count);
                            }
                        }

                        //service tax exempted
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()) && xlWorksheet.Cells[i, 15].Text.Trim().GetType() == typeof(string))
                        {
                            if (xlWorksheet.Cells[i, 15].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 15].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 15].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 15].Text.Trim().ToUpper() != "Y")
                                LogErrors.Add("Invalid value for service tax exempted. Enter Yes or No");
                            else
                                ClientDetailsVModel.CB_ServiceTaxExmpted = Convert.ToString(xlWorksheet.Cells[i, 15].Text.Trim());
                        }

                        //commencement date
                        string Excelcommdate = xlWorksheet.Cells[i, 16].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(Excelcommdate))
                        {
                            try
                            {
                                DateTime dt = DateTime.ParseExact(Excelcommdate, "dd/MM/yyyy", null);
                                // DateTime dt = DateTime.Parse(Excelcommdate);
                                ClientDetailsVModel.CB_DateOfCommencement = dt.ToString();
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid Commencement Date. Required in DD/MM/YYYY format. Check at Row-" + count);
                            }
                        }
                        else
                        {
                            LogErrors.Add("Required Commencement Date at Row-" + count);
                        }

                        //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()) && xlWorksheet.Cells[i, 17].Text.Trim().GetType() == typeof(string))
                        //{
                        //    if (xlWorksheet.Cells[i, 17].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 17].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 17].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 17].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 17].Text != null)
                        //        LogErrors.Add("Invalid value for EDLI Excemption. Enter Yes or No");
                        //    else
                        //        ClientDetailsVModel.CB_EDLIExcemption = xlWorksheet.Cells[i, 17].Text.Trim();
                        //}

                        //pfcodetype
                        string ExcelPFCodeType = xlWorksheet.Cells[i, 23].Text.ToString().Trim();

                        if (String.IsNullOrEmpty(ExcelPFCodeType))
                            LogErrors.Add("Required PF Code Type at Row-" + count);
                        else
                        {
                            if (ExcelPFCodeType.ToUpper() == "C")
                                BasicSetupModel.PFType = "C";
                            else if (ExcelPFCodeType.ToUpper() == "B")
                                BasicSetupModel.PFType = "B";
                            else
                                LogErrors.Add("Enter valid PF Code Type at Row- " + count + ",it should be C OR B");
                        }

                        //pfcode
                        string ExcelPFCode = xlWorksheet.Cells[i, 17].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(ExcelPFCode) && ExcelPFCodeType == "B")
                            LogErrors.Add("Not Required PF Code  at Row-" + count);

                        if (String.IsNullOrEmpty(ExcelPFCode) && ExcelPFCodeType=="C")
                            LogErrors.Add("Required PF Code at Row-" + count);
                        else
                        {
                            if (!String.IsNullOrEmpty(ExcelPFCode))
                            {
                            //bool PF = Regex.IsMatch(ExcelPFCode, @"^[a-zA-Z0-9]+$");
                            bool PF = Regex.IsMatch(ExcelPFCode, @"^[A-Za-z0-9-//\\]+$");
                            if (PF == false)
                                LogErrors.Add("Invalid PF Code " + ExcelPFCode + ". Only alphanumeric characters, slash and hyphens are allowed in PF Code. Check at Row -" + count);
                            else
                            {
                                ClientDetailsVModel.PFCode = ExcelPFCode;
                                /*if (ExcelPFCode.Length >= 12 && ExcelPFCode.Length <= 22)
                                {
                                    ClientDetailsVModel.PFCode = ExcelPFCode;
                                }
                                else
                                {
                                    LogErrors.Add("PF Code-" + ExcelPFCode + " can have a minimum of 12 digits and maximum of 22 digits, Check at Row -" + count);
                                }*/

                                }
                            }
                        }

                        //status
                        string ExcelStatus = xlWorksheet.Cells[i, 18].Text.ToString().Trim();

                        if (String.IsNullOrEmpty(ExcelStatus))
                            LogErrors.Add("Required Status (Active/Inactive) at Row-" + count);
                        else if (!String.IsNullOrEmpty(ExcelStatus.Trim()) && ExcelStatus.Trim().GetType() == typeof(string))
                        {
                            if (ExcelStatus.ToUpper() == "ACTIVE" || ExcelStatus.ToUpper() == "A")
                                BasicSetupModel.CM_Status = "A";
                            else if (ExcelStatus.ToUpper() == "INACTIVE" || ExcelStatus.ToUpper() == "I")
                                BasicSetupModel.CM_Status = "I";
                            else
                                LogErrors.Add("Enter valid status at Row- " + count + ",it should be Active/InActive");
                        }

                        //contact person
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                        {
                            string ContactPerson = xlWorksheet.Cells[i, 19].Text.ToString().Trim();
                            bool valid = Regex.IsMatch(ContactPerson, @"^[a-zA-Z. ]+$");
                            if (valid == false)
                            {
                                LogErrors.Add("Invalid Contact Person, Only characters allowed, Check at Row- " + count);
                            }
                            else
                            {
                                BasicSetupModel.ContactPerson = ContactPerson;
                            }
                        }
                        else
                        {
                            LogErrors.Add("Required Contact Person at Row-" + count);
                        }

                        //contact number
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                        {
                            string ContactNumber = xlWorksheet.Cells[i, 20].Text.ToString().Trim();
                            bool valid = Regex.IsMatch(ContactNumber, @"^[0-9]+$");
                            if (valid == false)
                            {
                                LogErrors.Add("Invalid Contact Number, Only Numbers allowed, Check at Row- " + count);
                            }
                            else
                            {
                                if (ContactNumber.Length != 10)
                                {
                                    LogErrors.Add("Contact Number must be of 10 digit, Check at Row-" + count);
                                }
                                else
                                {
                                    if (Convert.ToInt64(ContactNumber) > 0)
                                    {
                                        BasicSetupModel.ContactNumber = ContactNumber;
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Contact Number-" + ContactNumber + ", it should be positive numbers only, Check at Row-" + count);
                                    }
                                }
                               
                            }
                        }
                        else
                        {
                            LogErrors.Add("Required Contact Number at Row-" + count);
                        }

                        //emailID
                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                        {
                            string Email_ID = xlWorksheet.Cells[i, 21].Text.ToString().Trim();
                            try
                            {
                                Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                                Match valid = regex.Match(Email_ID);
                                if (valid.Success)
                                    BasicSetupModel.EmailID = Email_ID;
                                else
                                    LogErrors.Add("Invalid EmailID, Check at Row-" + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid EmailID-" + Email_ID + ", Check at Row-" + count);
                            }
                        }
                        else
                        {
                            LogErrors.Add("Required Email ID at Row-" + count);
                        }

                        //Type
                        string Type = xlWorksheet.Cells[i, 22].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(Type))
                            LogErrors.Add("Required Type at Row-" + count);
                        else
                        {
                            int CompanyType = RLCS_ClientsManagement.GetValidateCompanyType(Type);

                            if (CompanyType == 0)
                            {
                                LogErrors.Add("Enter valid value for Type at Row-" + count + ".Enter Public/Private/Listed");
                            }
                            else
                            {
                                BasicSetupModel.Type = CompanyType;
                            }
                        }
                        //23 Column ExcelPFCodeType Added Near 17 Column --- (Dependency)
                        //24 for EDLI Type
                        string ExcelEDLIType = xlWorksheet.Cells[i, 24].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(ExcelEDLIType))
                            LogErrors.Add("Required EDLI Excemption Type at Row-" + count);
                        else
                        {
                            if (ExcelEDLIType.ToUpper() == "C")
                                BasicSetupModel.EDLIExcemptionType = "C";
                            else if (ExcelEDLIType.ToUpper() == "B")
                                BasicSetupModel.EDLIExcemptionType = "B";
                            else
                                LogErrors.Add("Enter valid EDLI Excemption Type at Row- " + count + ",it should be C OR B");
                        }

                        //END
                        int customerID = UserManagement.GetCustomerIDRLCS(BasicSetupModel.CO_CorporateID);
                        int? ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(customerID));

                        if (ServiceProviderID == 94)//Check 94    
                        {

                            //poapplicability
                            string PoApplicabilitty = xlWorksheet.Cells[i, 25].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(PoApplicabilitty))
                                LogErrors.Add("Required PO Applicability at Row-" + count);
                            else
                            {
                                if (PoApplicabilitty.ToUpper() != "YES" && PoApplicabilitty.ToUpper() != "NO" && PoApplicabilitty.ToUpper() != "Y" && PoApplicabilitty.ToUpper() != "N" && PoApplicabilitty != null)
                                {
                                    LogErrors.Add("Invalid value for PO Applicability. Enter Yes or No");
                                }
                                else
                                {
                                    if (PoApplicabilitty.ToUpper() == "YES" || PoApplicabilitty.ToUpper() == "Y")
                                        BasicSetupModel.CM_IsPOApplicable = true;
                                    else
                                        BasicSetupModel.CM_IsPOApplicable = false;
                                }
                            }

                            //AgreementID
                            string AgreementID = xlWorksheet.Cells[i, 26].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(AgreementID))
                                LogErrors.Add("Required AgreementID at Row-" + count);
                            else
                            {
                                BasicSetupModel.AgreementID = AgreementID;
                            }

                            //mandate
                            string Mandate = xlWorksheet.Cells[i, 27].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(Mandate))
                                LogErrors.Add("Required Mandate at Row-" + count);
                            else
                            {
                                if (Mandate.ToUpper().Trim() == "ONE TIME" || Mandate.ToUpper() == "O")
                                    BasicSetupModel.ContractType = "O";
                                else if (Mandate.ToUpper().Trim() == "RECURRING" || Mandate.ToUpper() == "R")
                                    BasicSetupModel.ContractType = "R";
                                else if (Mandate.ToUpper().Trim() == "BOTH(ONETIME & RECURRING)" || Mandate.ToUpper() == "B")
                                    BasicSetupModel.ContractType = "B";
                                else if (Mandate.ToUpper().Trim() == "PAYROLL" || Mandate.ToUpper() == "P")
                                    BasicSetupModel.ContractType = "P";
                                else
                                {
                                    LogErrors.Add("Invalid value for Mandate. Enter One time or Recurring or Both(OneTime & Recurring) or PayRoll");
                                }
                            }
                            int distributorID = RLCS_ClientsManagement.GetParentIDforCustomer(customerID);

                            //BDAnnchor
                            string BDAnchor = xlWorksheet.Cells[i, 28].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(BDAnchor))
                                LogErrors.Add("Required BDAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, BDAnchor);
                                if (valid == true)
                                {
                                    BasicSetupModel.BDAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, BDAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for BD Anchor.");
                                }
                            }

                            //RAM anchor
                            string RAMAnchor = xlWorksheet.Cells[i, 29].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(RAMAnchor))
                                LogErrors.Add("Required RAMAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, RAMAnchor);
                                if (valid == true)
                                {
                                    BasicSetupModel.RAMAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, RAMAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for RAM Anchor.");
                                }
                            }

                            //RegisterAnchor
                            string RegisterAnchor = xlWorksheet.Cells[i, 30].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(RegisterAnchor))
                                LogErrors.Add("Required RegisterAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, RegisterAnchor);
                                if (valid == true)
                                {
                                    BasicSetupModel.ProcessAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, RegisterAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for Register Anchor.");
                                }
                            }

                            //challanAnchor
                            string ChallanAnchor = xlWorksheet.Cells[i, 31].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(RegisterAnchor))
                                LogErrors.Add("Required RegisterAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, ChallanAnchor);
                                if (valid == true)
                                {
                                    BasicSetupModel.ChallanAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, ChallanAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for Challan Anchor.");
                                }

                            }

                            //return anchor
                            string ReturnAnchor = xlWorksheet.Cells[i, 32].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(ReturnAnchor))
                                LogErrors.Add("Required ReturnAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, ReturnAnchor);
                                if (valid == true)
                                {
                                    BasicSetupModel.ReturnAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, ReturnAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for Return Anchor.");
                                }
                            }

                            //location anchor
                            string LocationAnchor = xlWorksheet.Cells[i, 33].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(LocationAnchor))
                                LogErrors.Add("Required LocationAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, LocationAnchor);
                                if (valid == true)
                                {
                                    BasicSetupModel.LocationAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, LocationAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for Location Anchor.");
                                }
                            }

                            //CRAnchor
                            string CRAnchor = xlWorksheet.Cells[i, 34].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(CRAnchor))
                                LogErrors.Add("Required CRAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, CRAnchor);
                                if (valid == true)
                                {
                                    BasicSetupModel.RLCSAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, CRAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for CR Anchor.");
                                }
                            }

                            //AuditAnchor
                            string AuditAnchor = xlWorksheet.Cells[i, 35].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(AuditAnchor))
                                LogErrors.Add("Required AuditAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, AuditAnchor);
                                if (valid == true)
                                {
                                    BasicSetupModel.AuditAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, AuditAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for Audit Anchor.");
                                }
                            }
                            //SPOC sanitation
                            string SPOCSANITATION = xlWorksheet.Cells[i, 36].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(SPOCSANITATION))
                                LogErrors.Add("Required Spoc Sanitation at Row-" + count);
                            else
                            {
                                if (SPOCSANITATION.ToUpper() == "MR")
                                    BasicSetupModel.SPOCSANITATION = "Mr";
                                else if (SPOCSANITATION.ToUpper() == "MS")
                                    BasicSetupModel.SPOCSANITATION = "Ms";
                                else
                                {
                                    LogErrors.Add("Invalid value for Spoc Sanitation. Enter Mr or Ms");
                                }
                            }

                            //SPOC first name
                            string SPOC_FirstName = xlWorksheet.Cells[i, 37].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(SPOC_FirstName))
                                LogErrors.Add("Required Spoc FirstName at Row-" + count);
                            else
                            {
                                bool valid = Regex.IsMatch(SPOC_FirstName, @"^[a-zA-Z.]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid Spoc FirstName " + SPOC_FirstName + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.SPOC_FirstName = SPOC_FirstName;
                                }
                            }

                            //SPOC last name
                            string SPOC_LastName = xlWorksheet.Cells[i, 38].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(SPOC_LastName))
                                LogErrors.Add("Required Spoc LastName at Row-" + count);
                            else
                            {
                                bool valid = Regex.IsMatch(SPOC_LastName, @"^[a-zA-Z.]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid Spoc FirstName " + SPOC_LastName + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.SPOC_LastName = SPOC_LastName;
                                }
                            }

                            //SPOC contact number
                            string SPOC_ContactNumber = xlWorksheet.Cells[i, 39].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(SPOC_ContactNumber))
                                LogErrors.Add("Required Spoc Contact Number at Row-" + count);
                            else
                            {
                                bool valid = Regex.IsMatch(SPOC_ContactNumber, @"^[0-9]+$");
                                if (valid == false)
                                {
                                    LogErrors.Add("Invalid Spoc Contact Number, Only Numbers allowed, Check at Row- " + count);
                                }
                                else
                                {
                                    if (SPOC_ContactNumber.Length != 10)
                                    {
                                        LogErrors.Add("Spoc Contact Number must be of 10 digit, Check at Row-" + count);
                                    }
                                    else
                                    {
                                        if (Convert.ToInt64(SPOC_ContactNumber) > 0)
                                        {
                                            BasicSetupModel.SPOC_ContactNumber = SPOC_ContactNumber;
                                        }
                                        else
                                        {
                                            LogErrors.Add("Invalid Spoc Contact Number-" + SPOC_ContactNumber + ", it should be positive numbers only, Check at Row-" + count);
                                        }
                                    }
                                }
                            }

                            //SPOC email
                            string SPOC_Email = xlWorksheet.Cells[i, 40].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(SPOC_Email))
                                LogErrors.Add("Required Spoc Email at Row-" + count);
                            else
                            {
                                try
                                {
                                    Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                                    Match valid = regex.Match(SPOC_Email);
                                    if (valid.Success)
                                        BasicSetupModel.SPOC_Email = SPOC_Email;
                                    else
                                        LogErrors.Add("Invalid Spoc EmailID, Check at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Invalid Spoc EmailID-" + SPOC_Email + ", Check at Row-" + count);
                                }
                            }

                            //SPOC designation
                            string SPOC_DESIGNATION = xlWorksheet.Cells[i, 41].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(SPOC_DESIGNATION))
                                LogErrors.Add("Required Spoc Designation at Row-" + count);
                            else
                            {
                                bool valid = Regex.IsMatch(SPOC_DESIGNATION, @"^[a-zA-Z0-9 .]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid Spoc Designation " + SPOC_DESIGNATION + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.SPOC_DESIGNATION = SPOC_DESIGNATION;
                                }
                            }

                            //EP1 sanitation
                            string EP1SANITATION = xlWorksheet.Cells[i, 42].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(EP1SANITATION))
                                LogErrors.Add("Required EP1 Sanitation at Row-" + count);
                            else
                            {
                                if (EP1SANITATION.ToUpper() == "MR")
                                    BasicSetupModel.EP1SANITATION = "Mr";
                                else if (EP1SANITATION.ToUpper() == "MS")
                                    BasicSetupModel.EP1SANITATION = "Ms";
                                else
                                {
                                    LogErrors.Add("Invalid value for EP1 Sanitation. Enter Mr or Ms");
                                }
                            }

                            //ep1 first name
                            string EP1_FirstName = xlWorksheet.Cells[i, 43].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(EP1_FirstName))
                                LogErrors.Add("Required EP1 FirstName at Row-" + count);
                            else
                            {
                                bool valid = Regex.IsMatch(EP1_FirstName, @"^[a-zA-Z.]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid EP1 FirstName " + EP1_FirstName + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.EP1_FirstName = EP1_FirstName;
                                }
                            }

                            //ep1 last name
                            string EP1_LastName = xlWorksheet.Cells[i, 44].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(EP1_LastName))
                                LogErrors.Add("Required EP1 LastName at Row-" + count);
                            else
                            {
                                bool valid = Regex.IsMatch(EP1_LastName, @"^[a-zA-Z.]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid EP1 FirstName " + EP1_LastName + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.EP1_LastName = EP1_LastName;
                                }
                            }

                            //ep1 contact number
                            string EP1_ContactNumber = xlWorksheet.Cells[i, 45].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(EP1_ContactNumber))
                                LogErrors.Add("Required EP1 Contact Number at Row-" + count);
                            else
                            {
                                bool valid = Regex.IsMatch(EP1_ContactNumber, @"^[0-9]+$");
                                if (valid == false)
                                {
                                    LogErrors.Add("Invalid EP1 Contact Number, Only Numbers allowed, Check at Row- " + count);
                                }
                                else
                                {
                                    if (EP1_ContactNumber.Length != 10)
                                    {
                                        LogErrors.Add("EP1 Contact Number must be of 10 digit, Check at Row-" + count);
                                    }
                                    else
                                    {
                                        if (Convert.ToInt64(EP1_ContactNumber) > 0)
                                        {
                                            BasicSetupModel.EP1_ContactNumber = EP1_ContactNumber;
                                        }
                                        else
                                        {
                                            LogErrors.Add("Invalid EP1 Contact Number-" + EP1_ContactNumber + ", it should be positive numbers only, Check at Row-" + count);
                                        }
                                    }
                                }
                            }

                            //ep1 email
                            string EP1_Email = xlWorksheet.Cells[i, 46].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(EP1_Email))
                                LogErrors.Add("Required EP1 Email at Row-" + count);
                            else
                            {
                                try
                                {
                                    Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                                    Match valid = regex.Match(EP1_Email);
                                    if (valid.Success)
                                        BasicSetupModel.EP1_Email = EP1_Email;
                                    else
                                        LogErrors.Add("Invalid EP1 EmailID, Check at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Invalid EP1 EmailID-" + EP1_Email + ", Check at Row-" + count);
                                }
                            }

                            //ep1 designation
                            string EP1_DESIGNATION = xlWorksheet.Cells[i, 47].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(EP1_DESIGNATION))
                                LogErrors.Add("Required EP1 Designation at Row-" + count);
                            else
                            {
                                bool valid = Regex.IsMatch(EP1_DESIGNATION, @"^[a-zA-Z0-9 .]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid EP1 Designation " + EP1_DESIGNATION + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.EP1_DESIGNATION = EP1_DESIGNATION;
                                }
                            }

                            //EP2 sanitation
                            string EP2SANITATION = xlWorksheet.Cells[i, 48].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(EP2SANITATION))
                            {
                                if (EP2SANITATION.ToUpper() == "MR")
                                    BasicSetupModel.EP2SANITATION = "Mr";
                                else if (EP2SANITATION.ToUpper() == "MS")
                                    BasicSetupModel.EP2SANITATION = "Ms";
                                else
                                {
                                    LogErrors.Add("Invalid value for EP2 Sanitation. Enter Mr or Ms");
                                }
                            }

                            //ep2 first name
                            string EP2_FirstName = xlWorksheet.Cells[i, 49].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(EP2_FirstName))
                            {
                                bool valid = Regex.IsMatch(EP2_FirstName, @"^[a-zA-Z.]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid EP2 FirstName " + EP2_FirstName + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.EP2_FirstName = EP2_FirstName;
                                }
                            }

                            //ep2 last name
                            string EP2_LastName = xlWorksheet.Cells[i, 50].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(EP2_LastName))
                            {
                                bool valid = Regex.IsMatch(EP2_LastName, @"^[a-zA-Z.]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid EP2 FirstName " + EP2_LastName + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.EP2_LastName = EP2_LastName;
                                }
                            }

                            //ep2 contact number
                            string EP2_ContactNumber = xlWorksheet.Cells[i, 51].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(EP2_ContactNumber))
                            {
                                bool valid = Regex.IsMatch(EP2_ContactNumber, @"^[0-9]+$");
                                if (valid == false)
                                {
                                    LogErrors.Add("Invalid EP2 Contact Number, Only Numbers allowed, Check at Row- " + count);
                                }
                                else
                                {
                                    if (EP2_ContactNumber.Length != 10)
                                    {
                                        LogErrors.Add("EP2 Contact Number must be of 10 digit, Check at Row-" + count);
                                    }
                                    else
                                    {
                                        if (Convert.ToInt64(EP2_ContactNumber) > 0)
                                        {
                                            BasicSetupModel.EP2_ContactNumber = EP2_ContactNumber;
                                        }
                                        else
                                        {
                                            LogErrors.Add("Invalid EP2 Contact Number-" + EP2_ContactNumber + ", it should be positive numbers only, Check at Row-" + count);
                                        }
                                    }
                                }
                            }

                            //ep2 email
                            string EP2_Email = xlWorksheet.Cells[i, 52].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(EP2_Email))
                               {
                                try
                                {
                                    Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                                    Match valid = regex.Match(EP2_Email);
                                    if (valid.Success)
                                        BasicSetupModel.EP2_Email = EP2_Email;
                                    else
                                        LogErrors.Add("Invalid EP2 EmailID, Check at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Invalid EP2 EmailID-" + EP2_Email + ", Check at Row-" + count);
                                }
                            }

                            //ep2 designation
                            string EP2_DESIGNATION = xlWorksheet.Cells[i, 53].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(EP2_DESIGNATION))
                            {
                                bool valid = Regex.IsMatch(EP2_DESIGNATION, @"^[a-zA-Z0-9 .]+$");
                                if (valid == false)
                                    LogErrors.Add("Enter valid EP2 Designation " + EP2_DESIGNATION + " at Row -" + count);
                                else
                                {
                                    BasicSetupModel.EP2_DESIGNATION = EP2_DESIGNATION;
                                }
                            }
                            ///END
                        }
                    }
                    #endregion

                    if (LogErrors.Count <= 0)
                    {
                        #region Save 

                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = count + 1;

                            BasicSetupModel = new BasicSetupModel();

                            BasicSetupModel.CO_CorporateID = xlWorksheet.Cells[i, 1].Text.Trim();
                            BasicSetupModel.CM_ClientID = xlWorksheet.Cells[i, 2].Text.Trim();
                            BasicSetupModel.CM_ClientName = xlWorksheet.Cells[i, 3].Text.Trim();

                            //if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.Trim()))
                            //    LogErrors.Add("Required Act Applicability at Row-" + count);
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()) && xlWorksheet.Cells[i, 4].Text.Trim().GetType() == typeof(string))
                            {
                                if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper() == "CENTRAL")
                                    BasicSetupModel.ActApplicablity = "CEN";
                                else if (xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper() == "STATE")
                                    BasicSetupModel.ActApplicablity = "STATE";
                                else
                                {
                                    if (String.IsNullOrEmpty(BasicSetupModel.ActApplicablity))
                                        LogErrors.Add("Enter valid Act Applicability at Row-" + count + ". Enter either Central or State.");
                                }
                            }

                            //industry type
                            string ExcelIndustryType = xlWorksheet.Cells[i, 5].Text.ToString().Trim();
                            string[] ArrayIndustryTypes = new string[] { "Oil and Natural gas", "Banking Sector", "Telecommunication", "Air Transportation", "Insurance", "Mines" };
                            if (BasicSetupModel.ActApplicablity == "STATE" && !String.IsNullOrEmpty(ExcelIndustryType))
                            {
                                LogErrors.Add("Industry type can only be selected for central acts at Row-" + count);
                            }
                            if (BasicSetupModel.ActApplicablity == "CEN" && !String.IsNullOrEmpty(ExcelIndustryType))
                            {
                                bool isValidIndustryType = false;
                                foreach (var item in ArrayIndustryTypes)
                                {
                                    if (ExcelIndustryType == item) isValidIndustryType = true;
                                }
                                if (!isValidIndustryType)
                                {
                                    LogErrors.Add("Select valid industry types at Row-" + count);
                                }
                                else
                                {
                                    jsonClientDetails.Industry_Type = ExcelIndustryType;
                                }
                            }

                            //establishment type
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                                LogErrors.Add("Required Establishment Type at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()) && xlWorksheet.Cells[i, 6].Text.Trim().GetType() == typeof(string))
                            {
                                string estType = xlWorksheet.Cells[i, 6].Text.ToString().Trim().ToUpper();

                                if (estType == "FACTORY" || estType == "FACT")
                                    BasicSetupModel.CM_EstablishmentType = "FACT";
                                else if (estType == "SHOP & ESTABLISHMENT" || estType == "SEA" || estType == "SHOP AND ESTABLISHMENT")
                                    BasicSetupModel.CM_EstablishmentType = "SEA";
                                else if (estType == "BOTH" || estType == "BOTH")
                                    BasicSetupModel.CM_EstablishmentType = "SF";
                                else
                                {
                                    if (String.IsNullOrEmpty(BasicSetupModel.CM_EstablishmentType))
                                        LogErrors.Add("Enter valid value for Establishment Type at Row-" + count + ".Enter Factory or Shop & Establishment or Both");
                                }
                            }

                            //if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text))
                            //    LogErrors.Add("Required Service Start Date at Row-" + count);
                            //else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()) && xlWorksheet.Cells[i, 6].Text.Trim().GetType() == typeof(string))
                            //{
                            //    string excelserDate = xlWorksheet.Cells[i, 6].Text;

                            //    try
                            //    {
                            //        DateTime dt = DateTime.ParseExact(excelserDate, "dd/MM/yyyy", null);
                            //        BasicSetupModel.CM_ServiceStartDate = dt.ToString();
                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        LogErrors.Add("Enter Valid Service Start Date at Row-" + count);
                            //    }
                            //}

                            //state
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim())) //check state with database or give error
                                LogErrors.Add("Required State at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim().ToUpper()) && xlWorksheet.Cells[i, 7].Text.Trim().GetType() == typeof(string))
                            {
                                var stateRecord = RLCS_ClientsManagement.GetStateRecordbyCode(xlWorksheet.Cells[i, 7].Text.ToString().Trim().ToUpper());

                                if (stateRecord != null)
                                {
                                    BasicSetupModel.StateModel.SM_Code = stateRecord.SM_Code;
                                    BasicSetupModel.StateModel.AVACOM_StateID = stateRecord.AVACOM_StateID;
                                }
                                else
                                {
                                    LogErrors.Add("No State found with " + xlWorksheet.Cells[i, 7].Text.ToString().Trim().ToUpper() + ", check at Row-" + count);
                                }
                            }


                            //city
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.Trim()))
                                LogErrors.Add("Required City at Row" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()) && xlWorksheet.Cells[i, 8].Text.Trim().GetType() == typeof(string))
                            {
                                bool city = RLCS_ClientsManagement.GetCitybyCode(BasicSetupModel.StateModel.SM_Code, xlWorksheet.Cells[i, 8].Text.ToString().Trim());
                                if (city == false)
                                    LogErrors.Add("No match found for City at Row-" + count);
                                else
                                    BasicSetupModel.CM_City = xlWorksheet.Cells[i, 8].Text.Trim();
                            }

                            //address
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.Trim()))
                                LogErrors.Add("Required Address at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()) && xlWorksheet.Cells[i, 9].Text.Trim().GetType() == typeof(string))
                            {
                                BasicSetupModel.CM_Address = xlWorksheet.Cells[i, 9].Text.Trim();
                            }

                            //bonus percentage
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.Trim()))
                                LogErrors.Add("Required Bonus Percentage at Row" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                            {
                                try
                                {
                                    Convert.ToDecimal(xlWorksheet.Cells[i, 10].Text.Trim());
                                    BasicSetupModel.CM_BonusPercentage = Convert.ToDecimal(xlWorksheet.Cells[i, 10].Text.Trim());
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Bonus Percentage must be number, check at Row-" + count);
                                }
                            }

                            //bonusExemption
                            string bonusExemption = xlWorksheet.Cells[i, 11].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(bonusExemption))
                                LogErrors.Add("Required Exemption at Row-" + count);
                            else
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                {
                                    if (xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 11].Text != null)
                                    {
                                        LogErrors.Add("Invalid value for Exemption. Enter Yes or No");
                                    }
                                    else
                                    {
                                        if (xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() == "YES" || xlWorksheet.Cells[i, 11].Text.Trim().ToUpper() == "Y")
                                            BasicSetupModel.CM_Excemption = true;
                                        else
                                            BasicSetupModel.CM_Excemption = false;
                                    }
                                }
                            }


                            //wage period 
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                                LogErrors.Add("Required Wage Period(From) To at Row" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()) && xlWorksheet.Cells[i, 12].Text.Trim().GetType() == typeof(string))
                            {
                                try
                                {
                                    int wagefrom = Convert.ToInt32(xlWorksheet.Cells[i, 12].Text.Trim());

                                    if (wagefrom > 31)
                                        LogErrors.Add("Wage Period(From) should be between 1 to 31 at Row-" + count);
                                    else
                                        ClientDetailsVModel.CB_WagePeriodFrom = Convert.ToString(xlWorksheet.Cells[i, 12].Text);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Wage Period(From) must be number and between 1 to 31, Check at Row-" + count);
                                }
                            }

                            //wage period 
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                LogErrors.Add("Required Wage Period(To) at Row" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()) && xlWorksheet.Cells[i, 13].Text.Trim().GetType() == typeof(string))
                            {
                                try
                                {
                                    int wagefrom = Convert.ToInt32(xlWorksheet.Cells[i, 13].Text);
                                    if (wagefrom > 31)
                                        LogErrors.Add("Wage Period(To) should be between 1 to 31 at Row-" + count);
                                    else
                                        ClientDetailsVModel.CB_WagePeriodTo = Convert.ToString(xlWorksheet.Cells[i, 13].Text);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Wage Period(To) must be number and between 1 to 31, Check at Row-" + count);
                                }
                            }

                            /*if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                LogErrors.Add("Required Wage Payment Day at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()) && xlWorksheet.Cells[i, 13].Text.Trim().GetType() == typeof(string))
                            {
                                try
                                {
                                    int PaymentDate = Convert.ToInt32(xlWorksheet.Cells[i, 13].Text);
                                    if (PaymentDate > 31)
                                        LogErrors.Add("Payment Day should be between 1 to 31 at Row-" + count);
                                    else
                                        ClientDetailsVModel.CB_PaymentDate = xlWorksheet.Cells[i, 13].Text;
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Payment Day must be number and between 1 to 31, Check at Row-" + count);
                                }
                            }*/

                            //payment day
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                                LogErrors.Add("Required Wage Payment Day at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()) && xlWorksheet.Cells[i, 14].Text.Trim().GetType() == typeof(string))
                            {
                                try
                                {
                                    if (Convert.ToInt32(xlWorksheet.Cells[i, 14].Text.Trim()) > 0)
                                    {
                                        int PaymentDate = Convert.ToInt32(xlWorksheet.Cells[i, 14].Text);
                                        if (PaymentDate >= 1 && PaymentDate <= 28)
                                            ClientDetailsVModel.CB_PaymentDate = xlWorksheet.Cells[i, 14].Text.Trim();
                                        else
                                            LogErrors.Add("Payment Day should be between 1 to 28 or LDM at Row-" + count);
                                    }
                                    else
                                        LogErrors.Add("Please enter positive value for Payment Day at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    if (xlWorksheet.Cells[i, 14].Text.Trim().ToUpper() == "LDM")
                                    {
                                        ClientDetailsVModel.CB_PaymentDate = "31";
                                    }
                                    else
                                        LogErrors.Add("Payment Day must be number and between 1 to 28 or LDM, Check at Row-" + count);
                                }
                            }

                            //service tax exempted
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()) && xlWorksheet.Cells[i, 15].Text.Trim().GetType() == typeof(string))
                            {
                                if (xlWorksheet.Cells[i, 15].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 15].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 15].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 15].Text.Trim().ToUpper() != "N")
                                    LogErrors.Add("Invalid value for service tax exempted. Enter Yes or No");
                                else
                                    ClientDetailsVModel.CB_ServiceTaxExmpted = Convert.ToString(xlWorksheet.Cells[i, 15].Text.Trim());
                            }

                            //commencement date
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.Trim()))
                                LogErrors.Add("Required Commencement date at Row" + count);
                            string Excelcommdate = xlWorksheet.Cells[i, 16].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(Excelcommdate))
                            {
                                try
                                {
                                    DateTime dt = DateTime.ParseExact(Excelcommdate, "dd/MM/yyyy", null);
                                    ClientDetailsVModel.CB_DateOfCommencement = dt.ToString();
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Invalid Commencement Date. Required in DD/MM/YYYY format. Check at Row-" + count);
                                }
                            }

                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()) && xlWorksheet.Cells[i, 17].Text.Trim().GetType() == typeof(string))
                            //{
                            //    if (xlWorksheet.Cells[i, 17].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 17].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 17].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 17].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 17].Text != null)
                            //        LogErrors.Add("Invalid value for EDLI Excemption. Enter Yes or No");
                            //    else
                            //        ClientDetailsVModel.CB_EDLIExcemption = xlWorksheet.Cells[i, 17].Text.Trim();
                            //}

                            //GG ADD
                            //pfc code
                            string ExcelPFCodeType = xlWorksheet.Cells[i, 23].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(ExcelPFCodeType))
                                LogErrors.Add("Required PF Code Type at Row-" + count);
                            else
                            {
                                if (ExcelPFCodeType.ToUpper() == "C")
                                    BasicSetupModel.PFType = "C";
                                else if (ExcelPFCodeType.ToUpper() == "B")
                                    BasicSetupModel.PFType = "B";
                                else
                                    LogErrors.Add("Enter valid PF Code Type at Row- " + count + ",it should be C OR B");
                            }

                            //pf code
                            string ExcelPFcode = xlWorksheet.Cells[i, 17].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(ExcelPFcode) && ExcelPFCodeType == "B")
                                LogErrors.Add("Not Required PF Code  at Row-" + count);

                            if (String.IsNullOrEmpty(ExcelPFcode) && ExcelPFCodeType == "C")
                                LogErrors.Add("Required PF Code at Row-" + count);
                            //END
                            if (!String.IsNullOrEmpty(ExcelPFcode.Trim()) && ExcelPFcode.Trim().GetType() == typeof(string) && ExcelPFCodeType=="C")
                            {
                                bool PF = Regex.IsMatch(ExcelPFcode, @"^[A-Za-z0-9-//\\]+$");
                                if (PF == false)                        
                                    LogErrors.Add("Invalid PF Code " + ExcelPFcode + ". Only alphanumeric characters, slash and hyphens are allowed in PF Code. Check at Row -" + count);
                                else
                                {
                                    ClientDetailsVModel.PFCode = ExcelPFcode;
                                    /*if (ExcelPFcode.Length >= 12 && ExcelPFcode.Length <= 22)
                                    {
                                        ClientDetailsVModel.PFCode = ExcelPFcode;
                                    }
                                    else
                                    {
                                        LogErrors.Add("PF Code-" + ExcelPFcode + " can have a minimum of 12 digits and maximum of 22 digits, Check at Row -" + count);
                                    }*/

                                }
                            }

                            //status
                            string ExcelStatus = xlWorksheet.Cells[i, 18].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(ExcelStatus.Trim()) && ExcelPFcode.Trim().GetType() == typeof(string))
                            {
                                if (ExcelStatus.ToUpper() == "ACTIVE" || ExcelStatus.ToUpper() == "A")
                                    BasicSetupModel.CM_Status = "A";
                                else if (ExcelStatus.ToUpper() == "INACTIVE" || ExcelStatus.ToUpper() == "I")
                                    BasicSetupModel.CM_Status = "I";
                                else
                                    LogErrors.Add("Enter valid status at Row- " + count);
                            }

                            //contact person
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                            {
                                string ContactPerson = xlWorksheet.Cells[i, 19].Text.ToString().Trim();
                                bool valid = Regex.IsMatch(ContactPerson, @"^[a-zA-Z. ]+$");
                                if (valid == false)
                                {
                                    LogErrors.Add("Invalid Contact Person, Only characters allowed, Check at Row- " + count);
                                }
                                else
                                {
                                    BasicSetupModel.ContactPerson = ContactPerson;
                                }
                            }
                            else
                            {
                                LogErrors.Add("Required Contact Person at Row-" + count);
                            }

                            //contact number
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                            {
                                string ContactNumber = xlWorksheet.Cells[i, 20].Text.ToString().Trim();
                                bool valid = Regex.IsMatch(ContactNumber, @"^[0-9]+$");
                                if (valid == false)
                                {
                                    LogErrors.Add("Invalid Contact Number, Only Numbers allowed, Check at Row- " + count);
                                }
                                else
                                {
                                    if (ContactNumber.Length != 10)
                                    {
                                        LogErrors.Add("Contact Number must be of 10 digit, Check at Row-" + count);
                                    }
                                    else
                                    {
                                        if (Convert.ToInt64(ContactNumber) > 0)
                                        {
                                            BasicSetupModel.ContactNumber = ContactNumber;
                                        }
                                        else
                                        {
                                            LogErrors.Add("Invalid Contact Number-" + ContactNumber + ", it should be positive numbers only, Check at Row-" + count);
                                        }
                                    }

                                }
                            }
                            else
                            {
                                LogErrors.Add("Required Contact Number at Row-" + count);
                            }

                            //emailID
                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                            {
                                string Email_ID = xlWorksheet.Cells[i, 21].Text.ToString().Trim();
                                try
                                {
                                    Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                                    Match valid = regex.Match(Email_ID);
                                    if (valid.Success)
                                        BasicSetupModel.EmailID = Email_ID;
                                    else
                                        LogErrors.Add("Invalid EmailID, Check at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Invalid EmailID-" + Email_ID + ", Check at Row-" + count);
                                }
                            }
                            else
                            {
                                LogErrors.Add("Required Email ID at Row-" + count);
                            }

                            //type
                            string Type = xlWorksheet.Cells[i, 22].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(Type))
                                LogErrors.Add("Required Type at Row-" + count);
                            else
                            {
                                int CompanyType = RLCS_ClientsManagement.GetValidateCompanyType(Type);

                                    if (CompanyType == 0)
                                    {
                                        LogErrors.Add("Enter valid value for Type at Row-" + count + ".Enter Public/Private/Listed");
                                    }
                                    else
                                    {
                                        BasicSetupModel.Type = CompanyType;
                                    }
                            }

                            //EDLType
                            string ExcelEDLIType = xlWorksheet.Cells[i, 24].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(ExcelEDLIType))
                                LogErrors.Add("Required EDLI Excemption Type at Row-" + count);
                            else
                            {
                                if (ExcelEDLIType.ToUpper() == "C")
                                    BasicSetupModel.EDLIExcemptionType = "C";
                                else if (ExcelEDLIType.ToUpper() == "B")
                                    BasicSetupModel.EDLIExcemptionType = "B";
                                else
                                    LogErrors.Add("Enter valid EDLI Excemption Type at Row- " + count + ",it should be C OR B");
                            }
                            int customerID = UserManagement.GetCustomerIDRLCS(BasicSetupModel.CO_CorporateID);
                            int? ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(customerID));
                            if (ServiceProviderID == 94)//Check 94    
                            {
                                //poapplicability
                                string PoApplicabilitty = xlWorksheet.Cells[i, 25].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(PoApplicabilitty))
                                    LogErrors.Add("Required PO Applicability at Row-" + count);
                                else
                                {
                                    if (PoApplicabilitty.ToUpper() != "YES" && PoApplicabilitty.ToUpper() != "NO" && PoApplicabilitty.ToUpper() != "Y" && PoApplicabilitty.ToUpper() != "N" && PoApplicabilitty != null)
                                    {
                                        LogErrors.Add("Invalid value for PO Applicability. Enter Yes or No");
                                    }
                                    else
                                    {
                                        if (PoApplicabilitty.ToUpper() == "YES" || PoApplicabilitty.ToUpper() == "Y")
                                            BasicSetupModel.CM_IsPOApplicable = true;
                                        else
                                            BasicSetupModel.CM_IsPOApplicable = false;
                                    }
                                }

                                //agreementID
                                string AgreementID = xlWorksheet.Cells[i, 26].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(AgreementID))
                                    LogErrors.Add("Required AgreementID at Row-" + count);
                                else
                                {
                                    BasicSetupModel.AgreementID = AgreementID;
                                }

                                //mandate
                                string Mandate = xlWorksheet.Cells[i, 27].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(Mandate))
                                    LogErrors.Add("Required Mandate at Row-" + count);
                                else
                                {
                                    if (Mandate.ToUpper() == "ONE TIME" || Mandate.ToUpper() == "O")
                                        BasicSetupModel.ContractType = "O";
                                    else if (Mandate.ToUpper() == "RECURRING" || Mandate.ToUpper() == "R")
                                        BasicSetupModel.ContractType = "R";
                                    else if (Mandate.ToUpper() == "BOTH(ONETIME & RECURRING)" || Mandate.ToUpper() == "B")
                                        BasicSetupModel.ContractType = "B";
                                    else if (Mandate.ToUpper() == "PAYROLL" || Mandate.ToUpper() == "P")
                                        BasicSetupModel.ContractType = "P";
                                    else
                                    {
                                        LogErrors.Add("Invalid value for Mandate. Enter One time or Recurring or Both(OneTime & Recurring) or PayRoll");
                                    }
                                }
                                int distributorID = RLCS_ClientsManagement.GetParentIDforCustomer(customerID);
                                //bd annchor
                                string BDAnchor = xlWorksheet.Cells[i, 28].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(BDAnchor))
                                    LogErrors.Add("Required BDAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, BDAnchor);
                                    if (valid == true)
                                    {
                                        BasicSetupModel.BDAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, BDAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for BD Anchor.");
                                    }
                                }

                                //ram anchor
                                string RAMAnchor = xlWorksheet.Cells[i, 29].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(RAMAnchor))
                                    LogErrors.Add("Required RAMAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, RAMAnchor);
                                    if (valid == true)
                                    {
                                        BasicSetupModel.RAMAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, RAMAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for RAM Anchor.");
                                    }
                                }

                                //registerAnchor
                                string RegisterAnchor = xlWorksheet.Cells[i, 30].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(RegisterAnchor))
                                    LogErrors.Add("Required RegisterAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, RegisterAnchor);
                                    if (valid == true)
                                    {
                                        BasicSetupModel.ProcessAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, RegisterAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for Register Anchor.");
                                    }
                                }

                                //challanAnchor
                                string ChallanAnchor = xlWorksheet.Cells[i, 31].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(RegisterAnchor))
                                    LogErrors.Add("Required RegisterAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, ChallanAnchor);
                                    if (valid == true)
                                    {
                                        BasicSetupModel.ChallanAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, ChallanAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for Challan Anchor.");
                                    }

                                }

                                //returnAnchor
                                string ReturnAnchor = xlWorksheet.Cells[i, 32].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(ReturnAnchor))
                                    LogErrors.Add("Required ReturnAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, ReturnAnchor);
                                    if (valid == true)
                                    {
                                        BasicSetupModel.ReturnAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, ReturnAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for Return Anchor.");
                                    }
                                }

                                //LocationAnchor
                                string LocationAnchor = xlWorksheet.Cells[i, 33].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(LocationAnchor))
                                    LogErrors.Add("Required LocationAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, LocationAnchor);
                                    if (valid == true)
                                    {
                                        BasicSetupModel.LocationAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, LocationAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for Location Anchor.");
                                    }
                                }

                                //cr anchor
                                string CRAnchor = xlWorksheet.Cells[i, 34].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(CRAnchor))
                                    LogErrors.Add("Required CRAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, CRAnchor);
                                    if (valid == true)
                                    {
                                        BasicSetupModel.RLCSAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, CRAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for CR Anchor.");
                                    }
                                }

                                //AuditAnchor
                                string AuditAnchor = xlWorksheet.Cells[i, 35].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(AuditAnchor))
                                    LogErrors.Add("Required AuditAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, AuditAnchor);
                                    if (valid == true)
                                    {
                                        BasicSetupModel.AuditAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, AuditAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for Audit Anchor.");
                                    }
                                }

                                //SPOC
                                string SPOCSANITATION = xlWorksheet.Cells[i, 36].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(SPOCSANITATION))
                                    LogErrors.Add("Required Spoc Sanitation at Row-" + count);
                                else
                                {
                                    if (SPOCSANITATION.ToUpper() == "MR")
                                        BasicSetupModel.SPOCSANITATION = "Mr";
                                    else if (SPOCSANITATION.ToUpper() == "MS")
                                        BasicSetupModel.SPOCSANITATION = "Ms";
                                    else
                                    {
                                        LogErrors.Add("Invalid value for Spoc Sanitation. Enter Mr or Ms");
                                    }
                                }

                                //SPOC first name
                                string SPOC_FirstName = xlWorksheet.Cells[i, 37].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(SPOC_FirstName))
                                    LogErrors.Add("Required Spoc FirstName at Row-" + count);
                                else
                                {
                                    bool valid = Regex.IsMatch(SPOC_FirstName, @"^[a-zA-Z.]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid Spoc FirstName " + SPOC_FirstName + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.SPOC_FirstName = SPOC_FirstName;
                                    }
                                }

                                //spoc last name
                                string SPOC_LastName = xlWorksheet.Cells[i, 38].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(SPOC_LastName))
                                    LogErrors.Add("Required Spoc LastName at Row-" + count);
                                else
                                {
                                    bool valid = Regex.IsMatch(SPOC_LastName, @"^[a-zA-Z.]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid Spoc FirstName " + SPOC_LastName + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.SPOC_LastName = SPOC_LastName;
                                    }
                                }

                                //spoc  contact number
                                string SPOC_ContactNumber = xlWorksheet.Cells[i, 39].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(SPOC_ContactNumber))
                                    LogErrors.Add("Required Spoc Contact Number at Row-" + count);
                                else
                                {
                                    bool valid = Regex.IsMatch(SPOC_ContactNumber, @"^[0-9]+$");
                                    if (valid == false)
                                    {
                                        LogErrors.Add("Invalid Spoc Contact Number, Only Numbers allowed, Check at Row- " + count);
                                    }
                                    else
                                    {
                                        if (SPOC_ContactNumber.Length != 10)
                                        {
                                            LogErrors.Add("Spoc Contact Number must be of 10 digit, Check at Row-" + count);
                                        }
                                        else
                                        {
                                            if (Convert.ToInt64(SPOC_ContactNumber) > 0)
                                            {
                                                BasicSetupModel.SPOC_ContactNumber = SPOC_ContactNumber;
                                            }
                                            else
                                            {
                                                LogErrors.Add("Invalid Spoc Contact Number-" + SPOC_ContactNumber + ", it should be positive numbers only, Check at Row-" + count);
                                            }
                                        }
                                    }
                                }

                                //spoc email 
                                string SPOC_Email = xlWorksheet.Cells[i, 40].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(SPOC_Email))
                                    LogErrors.Add("Required Spoc Email at Row-" + count);
                                else
                                {
                                    try
                                    {
                                        Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                                        Match valid = regex.Match(SPOC_Email);
                                        if (valid.Success)
                                            BasicSetupModel.SPOC_Email = SPOC_Email;
                                        else
                                            LogErrors.Add("Invalid Spoc EmailID, Check at Row-" + count);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogErrors.Add("Invalid Spoc EmailID-" + SPOC_Email + ", Check at Row-" + count);
                                    }
                                }

                                //spoc designation
                                string SPOC_DESIGNATION = xlWorksheet.Cells[i, 41].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(SPOC_DESIGNATION))
                                    LogErrors.Add("Required Spoc Designation at Row-" + count);
                                else
                                {
                                    bool valid = Regex.IsMatch(SPOC_DESIGNATION, @"^[a-zA-Z0-9 .]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid Spoc Designation " + SPOC_DESIGNATION + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.SPOC_DESIGNATION = SPOC_DESIGNATION;
                                    }
                                }

                                //EP1
                                string EP1SANITATION = xlWorksheet.Cells[i, 42].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(EP1SANITATION))
                                    LogErrors.Add("Required EP1 Sanitation at Row-" + count);
                                else
                                {
                                    if (EP1SANITATION.ToUpper() == "MR")
                                        BasicSetupModel.EP1SANITATION = "Mr";
                                    else if (EP1SANITATION.ToUpper() == "MS")
                                        BasicSetupModel.EP1SANITATION = "Ms";
                                    else
                                    {
                                        LogErrors.Add("Invalid value for EP1 Sanitation. Enter Mr or Ms");
                                    }
                                }

                                //ep1_ first name
                                string EP1_FirstName = xlWorksheet.Cells[i, 43].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(EP1_FirstName))
                                    LogErrors.Add("Required EP1 FirstName at Row-" + count);
                                else
                                {
                                    bool valid = Regex.IsMatch(EP1_FirstName, @"^[a-zA-Z.]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid EP1 FirstName " + EP1_FirstName + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.EP1_FirstName = EP1_FirstName;
                                    }
                                }

                                //ep1 last name
                                string EP1_LastName = xlWorksheet.Cells[i, 44].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(EP1_LastName))
                                    LogErrors.Add("Required EP1 LastName at Row-" + count);
                                else
                                {
                                    bool valid = Regex.IsMatch(EP1_LastName, @"^[a-zA-Z.]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid EP1 FirstName " + EP1_LastName + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.EP1_LastName = EP1_LastName;
                                    }
                                }

                                //ep1 contanct number
                                string EP1_ContactNumber = xlWorksheet.Cells[i, 45].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(EP1_ContactNumber))
                                    LogErrors.Add("Required EP1 Contact Number at Row-" + count);
                                else
                                {
                                    bool valid = Regex.IsMatch(EP1_ContactNumber, @"^[0-9]+$");
                                    if (valid == false)
                                    {
                                        LogErrors.Add("Invalid EP1 Contact Number, Only Numbers allowed, Check at Row- " + count);
                                    }
                                    else
                                    {
                                        if (EP1_ContactNumber.Length != 10)
                                        {
                                            LogErrors.Add("EP1 Contact Number must be of 10 digit, Check at Row-" + count);
                                        }
                                        else
                                        {
                                            if (Convert.ToInt64(EP1_ContactNumber) > 0)
                                            {
                                                BasicSetupModel.EP1_ContactNumber = EP1_ContactNumber;
                                            }
                                            else
                                            {
                                                LogErrors.Add("Invalid EP1 Contact Number-" + EP1_ContactNumber + ", it should be positive numbers only, Check at Row-" + count);
                                            }
                                        }
                                    }
                                }

                                //ep1 email
                                string EP1_Email = xlWorksheet.Cells[i, 46].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(EP1_Email))
                                    LogErrors.Add("Required EP1 Email at Row-" + count);
                                else
                                {
                                    try
                                    {
                                        Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                                        Match valid = regex.Match(EP1_Email);
                                        if (valid.Success)
                                            BasicSetupModel.EP1_Email = EP1_Email;
                                        else
                                            LogErrors.Add("Invalid EP1 EmailID, Check at Row-" + count);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogErrors.Add("Invalid EP1 EmailID-" + EP1_Email + ", Check at Row-" + count);
                                    }
                                }

                                //ep1 designation
                                string EP1_DESIGNATION = xlWorksheet.Cells[i, 47].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(EP1_DESIGNATION))
                                    LogErrors.Add("Required EP1 Designation at Row-" + count);
                                else
                                {
                                    bool valid = Regex.IsMatch(EP1_DESIGNATION, @"^[a-zA-Z0-9 .]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid EP1 Designation " + EP1_DESIGNATION + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.EP1_DESIGNATION = EP1_DESIGNATION;
                                    }
                                }


                                //EP2
                                string EP2SANITATION = xlWorksheet.Cells[i, 48].Text.ToString().Trim();
                                if (!String.IsNullOrEmpty(EP2SANITATION))
                                {
                                    if (EP2SANITATION.ToUpper() == "MR")
                                        BasicSetupModel.EP2SANITATION = "Mr";
                                    else if (EP2SANITATION.ToUpper() == "MS")
                                        BasicSetupModel.EP2SANITATION = "Ms";
                                    else
                                    {
                                        LogErrors.Add("Invalid value for EP2 Sanitation. Enter Mr or Ms");
                                    }
                                }

                                //ep2 first name
                                string EP2_FirstName = xlWorksheet.Cells[i, 49].Text.ToString().Trim();
                                if (!String.IsNullOrEmpty(EP2_FirstName))
                                {
                                    bool valid = Regex.IsMatch(EP2_FirstName, @"^[a-zA-Z.]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid EP2 FirstName " + EP2_FirstName + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.EP2_FirstName = EP2_FirstName;
                                    }
                                }

                                //ep2 last name
                                string EP2_LastName = xlWorksheet.Cells[i, 50].Text.ToString().Trim();
                                if (!String.IsNullOrEmpty(EP2_LastName))
                                {
                                    bool valid = Regex.IsMatch(EP2_LastName, @"^[a-zA-Z.]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid EP2 FirstName " + EP2_LastName + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.EP2_LastName = EP2_LastName;
                                    }
                                }

                                //ep2 contact number
                                string EP2_ContactNumber = xlWorksheet.Cells[i, 51].Text.ToString().Trim();
                                if (!String.IsNullOrEmpty(EP2_ContactNumber))
                                {
                                    bool valid = Regex.IsMatch(EP2_ContactNumber, @"^[0-9]+$");
                                    if (valid == false)
                                    {
                                        LogErrors.Add("Invalid EP2 Contact Number, Only Numbers allowed, Check at Row- " + count);
                                    }
                                    else
                                    {
                                        if (EP2_ContactNumber.Length != 10)
                                        {
                                            LogErrors.Add("EP2 Contact Number must be of 10 digit, Check at Row-" + count);
                                        }
                                        else
                                        {
                                            if (Convert.ToInt64(EP2_ContactNumber) > 0)
                                            {
                                                BasicSetupModel.EP2_ContactNumber = EP2_ContactNumber;
                                            }
                                            else
                                            {
                                                LogErrors.Add("Invalid EP2 Contact Number-" + EP2_ContactNumber + ", it should be positive numbers only, Check at Row-" + count);
                                            }
                                        }
                                    }
                                }

                                //ep2 email
                                string EP2_Email = xlWorksheet.Cells[i, 52].Text.ToString().Trim();
                                if (!String.IsNullOrEmpty(EP2_Email))
                                {
                                    try
                                    {
                                        Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                                        Match valid = regex.Match(EP2_Email);
                                        if (valid.Success)
                                            BasicSetupModel.EP2_Email = EP2_Email;
                                        else
                                            LogErrors.Add("Invalid EP2 EmailID, Check at Row-" + count);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogErrors.Add("Invalid EP2 EmailID-" + EP2_Email + ", Check at Row-" + count);
                                    }
                                }

                                //ep2 designation
                                string EP2_DESIGNATION = xlWorksheet.Cells[i, 53].Text.ToString().Trim();
                                if (!String.IsNullOrEmpty(EP2_DESIGNATION))
                                {
                                    bool valid = Regex.IsMatch(EP2_DESIGNATION, @"^[a-zA-Z0-9 .]+$");
                                    if (valid == false)
                                        LogErrors.Add("Enter valid EP2 Designation " + EP2_DESIGNATION + " at Row -" + count);
                                    else
                                    {
                                        BasicSetupModel.EP2_DESIGNATION = EP2_DESIGNATION;
                                    }
                                }
                                ///END
                            }
                            if (ModelState.IsValid && LogErrors.Count <= 0)
                            {
                                int AVACOM_CustBranchID = 0;
                               
                                RLCS_CustomerBranch_ClientsLocation_Mapping Customerbranch = new RLCS_CustomerBranch_ClientsLocation_Mapping();
                                RLCS_Client_BasicDetails Clients = new RLCS_Client_BasicDetails();

                                TinyMapper.Bind<BasicSetupModel, RLCS_CustomerBranch_ClientsLocation_Mapping>(config =>
                                {
                                    config.Ignore(source => source.CM_ServiceStartDate);
                                });

                                Customerbranch = TinyMapper.Map<RLCS_CustomerBranch_ClientsLocation_Mapping>(BasicSetupModel);
                                BasicSetupModel.LocationVModel.LM_Code = RLCS_ClientsManagement.GetCityCode(BasicSetupModel.CM_City);

                                Customerbranch.BranchType = "E";

                                if (Customerbranch.AVACOM_BranchID != null)
                                    AVACOM_CustBranchID = (int)Customerbranch.AVACOM_BranchID;

                                if (AVACOM_CustBranchID != 0)
                                    Customerbranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetCustomerIDByCustomerBranchID(AVACOM_CustBranchID);
                                else
                                    Customerbranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetAVACOMCustIDByCorpID(Customerbranch.CO_CorporateID);

                                Customerbranch.CM_State = BasicSetupModel.StateModel.SM_Code;
                                Customerbranch.CM_City = BasicSetupModel.LocationVModel.LM_Code;
                                Customerbranch.CM_Address = BasicSetupModel.CM_Address;

                                //if (BasicSetupModel.CM_ServiceStartDate != null)
                                //{
                                //    Customerbranch.CM_ServiceStartDate = DateTime.Parse(BasicSetupModel.CM_ServiceStartDate);
                                //}

                                Customerbranch.CM_BonusPercentage = BasicSetupModel.CM_BonusPercentage;
                                Customerbranch.CM_ActType = BasicSetupModel.ActApplicablity;
                                Customerbranch.CM_Address = BasicSetupModel.CM_Address;
                                Customerbranch.CM_BonusPercentage = BasicSetupModel.CM_BonusPercentage;
                                Customerbranch.CM_Excemption = BasicSetupModel.CM_Excemption;
                                Customerbranch.CM_Status = BasicSetupModel.CM_Status;
                                //Customerbranch.CM_IsBonusExcempted = BasicSetupModel.CM_IsBonusExcempted;
                               
                                Customerbranch.CL_PF_Code = ClientDetailsVModel.PFCode;
                                //NEW ADD BY GG
                                Customerbranch.CL_HRContactPerson = BasicSetupModel.ContactPerson;
                                Customerbranch.CL_HR1stLevelPhNo = BasicSetupModel.ContactNumber;
                                Customerbranch.CL_HRMailID = BasicSetupModel.EmailID;
                                Customerbranch.CL_ID = BasicSetupModel.Type;  //CL_ID Use As ComType Parameter
                                //ADD ComplianceProdType 13MARCH2020
                                if (!string.IsNullOrEmpty(Convert.ToString(Customerbranch.AVACOM_CustomerID)))
                                {
                                    ComplianceProdType = RLCS_Master_Management.GetComplianceProductType(Convert.ToInt32(Customerbranch.AVACOM_CustomerID));
                                    if (ComplianceProdType > 0)
                                    {
                                        Customerbranch.CM_IsAventisClientOrBranch = ComplianceProdType;
                                    }
                                }
                                    ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(Customerbranch.AVACOM_CustomerID));
                                if (ServiceProviderID == 94)
                                {
                                    //Extra Fields
                                    Customerbranch.CM_AgreementID = BasicSetupModel.AgreementID;     ///TBD
                                    Customerbranch.CM_ContractType = BasicSetupModel.ContractType;
                                    //Anchor
                                    Customerbranch.CM_BDAnchor = BasicSetupModel.BDAnchor;
                                    Customerbranch.CM_RAMAnchor = BasicSetupModel.RAMAnchor;
                                    Customerbranch.CM_ProcessAnchor = BasicSetupModel.ProcessAnchor;
                                    Customerbranch.CM_ChallanAnchor = BasicSetupModel.ChallanAnchor;
                                    Customerbranch.CM_ReturnsAnchor = BasicSetupModel.ReturnAnchor;
                                    Customerbranch.CM_LocationAnchor = BasicSetupModel.LocationAnchor;
                                    Customerbranch.CM_RCP_Anchor = BasicSetupModel.LocationAnchor;
                                    Customerbranch.CM_RLCSAnchor = BasicSetupModel.RLCSAnchor;
                                    Customerbranch.CM_AuditAnchor = BasicSetupModel.AuditAnchor;
                                    //Spoc
                                    Customerbranch.CB_SPOCSANITATION = BasicSetupModel.SPOCSANITATION;
                                    Customerbranch.CB_SPOC_NAME = BasicSetupModel.SPOC_FirstName;
                                    Customerbranch.CB_SPOC_LASTNAME = BasicSetupModel.SPOC_LastName;
                                    Customerbranch.CB_SPOC_CONTACT = BasicSetupModel.SPOC_ContactNumber;
                                    Customerbranch.CB_SPOC_EMAIL = BasicSetupModel.SPOC_Email;
                                    Customerbranch.CB_SPOC_DESIGNATION = BasicSetupModel.SPOC_DESIGNATION;
                                    //EP1
                                    Customerbranch.CB_EP1_SANITATION = BasicSetupModel.EP1SANITATION;
                                    Customerbranch.CB_EP1_NAME = BasicSetupModel.EP1_FirstName;
                                    Customerbranch.CB_EP1LASTNAME = BasicSetupModel.EP1_LastName;
                                    Customerbranch.CB_EP1_CONTACT = BasicSetupModel.EP1_ContactNumber;
                                    Customerbranch.CB_EP1_EMAIL = BasicSetupModel.EP1_Email;
                                    Customerbranch.CB_EP1_DESIGNATION = BasicSetupModel.EP1_DESIGNATION;
                                    //EP2
                                    Customerbranch.CB_EP2_SANITATION = BasicSetupModel.EP1SANITATION;
                                    Customerbranch.CB_EP2_NAME = BasicSetupModel.EP1_FirstName;
                                    Customerbranch.CB_EP2LASTNAME = BasicSetupModel.EP1_LastName;
                                    Customerbranch.CB_EP2_CONTACT = BasicSetupModel.EP1_ContactNumber;
                                    Customerbranch.CB_EP2_EMAIL = BasicSetupModel.EP1_Email;
                                    Customerbranch.CB_EP2_DESIGNATION = BasicSetupModel.EP1_DESIGNATION;
                                    //Setting
                                    Customerbranch.CM_IsPOApplicable = BasicSetupModel.CM_IsPOApplicable;
                                }
                                ////END
                                bool ClientUp = RLCS_ClientsManagement.UpdateClientInfo(Customerbranch, BasicSetupModel.StateModel.AVACOM_StateID, BasicSetupModel.LocationVModel.LM_Code);

                                if (ClientUp)
                                {
                                    Clients.CB_ClientID = BasicSetupModel.CM_ClientID;
                                    Clients.CB_WagePeriodFrom = ClientDetailsVModel.CB_WagePeriodFrom;
                                    Clients.CB_WagePeriodTo = ClientDetailsVModel.CB_WagePeriodTo;
                                    Clients.CB_PaymentDate = ClientDetailsVModel.CB_PaymentDate;
                                    Clients.CB_ServiceTaxExmpted = ClientDetailsVModel.CB_ServiceTaxExmpted;
                                    Clients.CB_ActType = BasicSetupModel.ActApplicablity;
                                    //Clients.CB_EDLIExemption = ClientDetailsVModel.CB_EDLIExcemption;
                                    Clients.CM_PFCode = ClientDetailsVModel.PFCode;
                                    Clients.CB_PF_Code = ClientDetailsVModel.PFCode;

                                    if (ClientDetailsVModel.CB_DateOfCommencement != null)
                                    {
                                        Clients.CB_DateOfCommencement = DateTime.Parse(ClientDetailsVModel.CB_DateOfCommencement);
                                    }

                                    if (ClientDetailsVModel.CB_ServiceTaxExmpted != null)
                                    {
                                        if (ClientDetailsVModel.CB_ServiceTaxExmpted.Trim().ToUpper() == "YES" || ClientDetailsVModel.CB_ServiceTaxExmpted.Trim().ToUpper() == "Y")
                                        {
                                            Clients.CB_ServiceTaxExmpted = "Y";
                                        }
                                        else
                                        {
                                            Clients.CB_ServiceTaxExmpted = "N";
                                        }
                                    }
                                    //GG ADD
                                    Clients.CB_PF_CodeType = BasicSetupModel.PFType;
                                    //END
                                    //ADD EDLI
                                    Clients.CB_EDLI_ExcemptionType = BasicSetupModel.EDLIExcemptionType;
                                    Clients.CM_Industry_Type = jsonClientDetails.Industry_Type; //added by Vaibhav 29/06/2021
                                    //END

                                    //if (ClientDetailsVModel.CB_EDLIExcemption != null)
                                    //{
                                    //    if (ClientDetailsVModel.CB_EDLIExcemption.ToUpper() == "YES" || ClientDetailsVModel.CB_EDLIExcemption.ToUpper() == "Y")
                                    //        Clients.CB_EDLIExemption = "Y";
                                    //    else
                                    //        Clients.CB_EDLIExemption = "N";
                                    //}

                                    bool successClientBasicDetails = false;
                                    successClientBasicDetails = RLCS_ClientsManagement.UpdateClientBasicInfo(Clients);

                                    if (successClientBasicDetails)
                                    {
                                        jsonClientDetails.ClientId = BasicSetupModel.CM_ClientID;
                                        jsonClientDetails.CorporateId = BasicSetupModel.CO_CorporateID;
                                        jsonClientDetails.ClientName = BasicSetupModel.CM_ClientName;
                                        jsonClientDetails.AgreementID = Customerbranch.CM_AgreementID; 
                                        jsonClientDetails.ActApplicabilty = BasicSetupModel.ActApplicablity;
                                        jsonClientDetails.Mandate = Customerbranch.CM_ContractType; //TBD     
                                        jsonClientDetails.PFCode = ClientDetailsVModel.PFCode;
                                        jsonClientDetails.CB_PF_CodeType = Clients.CB_PF_CodeType;
                                        jsonClientDetails.EstablishmentType = BasicSetupModel.CM_EstablishmentType;
                                        //jsonClientDetails.ServiceStartDate = Customerbranch.CM_ServiceStartDate;
                                        jsonClientDetails.State = BasicSetupModel.StateModel.SM_Code;
                                        jsonClientDetails.City = BasicSetupModel.LocationVModel.LM_Code;
                                        jsonClientDetails.Address = BasicSetupModel.CM_Address;
                                        jsonClientDetails.Status = BasicSetupModel.CM_Status;
                                        jsonClientDetails.BonusPercentage = BasicSetupModel.CM_BonusPercentage;
                                        jsonClientDetails.WagePeriodFrom = ClientDetailsVModel.CB_WagePeriodFrom;
                                        jsonClientDetails.WagePeriodTo = ClientDetailsVModel.CB_WagePeriodTo;
                                        jsonClientDetails.PaymentDate = ClientDetailsVModel.CB_PaymentDate;
                                        jsonClientDetails.ServiceTaxExcempted = Clients.CB_ServiceTaxExmpted;
                                        jsonClientDetails.DateOfCommencement = Clients.CB_DateOfCommencement;//Customerbranch.CL_CommencementDate;
                                        //jsonClientDetails.EDLIExcemption = Clients.CB_EDLIExemption;
                                        jsonClientDetails.ClientFlag = Customerbranch.CM_IsAventisClientOrBranch;
                                        jsonClientDetails.CM_Country = "001";
                                        jsonClientDetails.ModifiedBy = "Avantis";
                                        jsonClientDetails.POApplicabilty = Customerbranch.CM_IsPOApplicable;
                                        jsonClientDetails.excemption = Customerbranch.CM_Excemption;
                                        //New Added
                                        jsonClientDetails.BDAnchor = Customerbranch.CM_BDAnchor;
                                        jsonClientDetails.RAMAnchor = Customerbranch.CM_RAMAnchor;
                                        jsonClientDetails.RegisterAnchor = Customerbranch.CM_ProcessAnchor;
                                        jsonClientDetails.ChallanAnchor = Customerbranch.CM_ChallanAnchor;
                                        jsonClientDetails.ReturnAnchor = Customerbranch.CM_ReturnsAnchor;
                                        jsonClientDetails.LocationAnchor = Customerbranch.CM_LocationAnchor;
                                        jsonClientDetails.CRAnchor = Customerbranch.CM_RLCSAnchor;
                                        jsonClientDetails.AuditAnchor = Customerbranch.CM_AuditAnchor;
                                        //Spoc
                                        jsonClientDetails.SpocSalutation = Customerbranch.CB_SPOCSANITATION;
                                        jsonClientDetails.SpocFirstName = Customerbranch.CB_SPOC_NAME;
                                        jsonClientDetails.SpocLastName = Customerbranch.CB_SPOC_LASTNAME;
                                        jsonClientDetails.SpocContactNo = Customerbranch.CB_SPOC_CONTACT;
                                        jsonClientDetails.SpocEmail = Customerbranch.CB_SPOC_EMAIL;
                                        jsonClientDetails.SpocDesignation = Customerbranch.CB_SPOC_DESIGNATION;
                                        //EP1
                                        jsonClientDetails.Ep1Salutation = Customerbranch.CB_EP1_SANITATION;
                                        jsonClientDetails.Ep1FirstName = Customerbranch.CB_EP1_NAME;
                                        jsonClientDetails.Ep1FirstName = Customerbranch.CB_EP1LASTNAME;
                                        jsonClientDetails.Ep1ContactNo = Customerbranch.CB_EP1_CONTACT;
                                        jsonClientDetails.Ep1Email = Customerbranch.CB_EP1_EMAIL;
                                        jsonClientDetails.Ep1Designation = Customerbranch.CB_EP1_DESIGNATION;
                                        //EP2
                                        jsonClientDetails.Ep2Salutation = Customerbranch.CB_EP2_SANITATION;
                                        jsonClientDetails.Ep2FirstName = Customerbranch.CB_EP2_NAME;
                                        jsonClientDetails.Ep2LastName = Customerbranch.CB_EP2LASTNAME;
                                        jsonClientDetails.Ep2ContactNo = Customerbranch.CB_EP2_CONTACT;
                                        jsonClientDetails.Ep2Email = Customerbranch.CB_EP2_EMAIL;
                                        jsonClientDetails.Ep2Designation = Customerbranch.CB_EP2_DESIGNATION;
                                        //EDLI
                                        jsonClientDetails.CB_EDLI_ExcemptionType = Clients.CB_EDLI_ExcemptionType;
                                        //industry type
                                        jsonClientDetails.Industry_Type = Clients.CM_Industry_Type;
                                        //END
                                        bool call = ClientBasicApiCall(jsonClientDetails);

                                        if (call)
                                        {
                                            BasicSetupModel.Message = true;
                                            RLCS_ClientsManagement.Update_ProcessedStatus_ClientBasicDetail(Clients.AVACOM_BranchID, BasicSetupModel.CM_ClientID, call);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string path = "";
                                //BasicSetupModel.FileName = RLCS_WriteLog.WriteLog(LogErrors, "Client Master Excel Sheet", out path);
                                BasicSetupModel.FileName = RLCS_WriteLog.WriteLog(LogErrors, fileName, out path);
                                BasicSetupModel.ExcelErrors = true;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        string path = "";
                        BasicSetupModel.FileName = RLCS_WriteLog.WriteLog(LogErrors, fileName, out path);
                        BasicSetupModel.ExcelErrors = true;
                    }
                }
                else
                {
                    BasicSetupModel.SheetNameError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                BasicSetupModel.Exception = true;
            }
            return BasicSetupModel;
        }

        public ClientLocationVModel ProcessClientsLocationDetails(ExcelPackage xlWorkbook, string fileName)
        {
            bool saveSuccess = false;
            ClientLocationVModel ClientLocationVModel = new ClientLocationVModel();
            ClientDetailsVModel ClientDetailsVModel = new ClientDetailsVModel();

            List<ClientLocationVModel> lstClientLocationVModel = new List<ClientLocationVModel>();

            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Branch"];

                if (xlWorksheet != null)
                {
                    int ComplianceProdType;
                    int count = 0;
                    string Sdate = string.Empty;
                    var noOfCol = xlWorksheet.Dimension.End.Column;
                    var noOfRow = xlWorksheet.Dimension.End.Row;
                    List<String> LogErrors = new List<string>();

                    #region Validation
                    for (int i = 2; i <= noOfRow; i++)
                    {
                        ClientLocationVModel = new ClientLocationVModel();
                        count = i;

                        string excelClientID = xlWorksheet.Cells[i, 1].Text.Trim();
                        if (string.IsNullOrEmpty(excelClientID))
                            LogErrors.Add("Required ClientID at Row -" + count);
                        else if (!String.IsNullOrEmpty(excelClientID) && xlWorksheet.Cells[i, 1].Text.Trim().GetType() == typeof(string))
                        {
                            bool ClientID = RLCS_ClientsManagement.GetClientByid(excelClientID);
                            if (ClientID == false)
                                LogErrors.Add("Invalid ClientID, Please make sure that Client should be Active, Check at Row-" + count);
                            else
                                ClientLocationVModel.CM_ClientID = excelClientID;
                        }

                        string excelState = xlWorksheet.Cells[i, 2].Text.Trim();
                        if (string.IsNullOrEmpty(excelState))
                            LogErrors.Add("Required State at Row -" + count);
                        else if (!String.IsNullOrEmpty(excelState) && xlWorksheet.Cells[i, 2].Text.ToString().Trim().GetType() == typeof(string))
                        {
                            var State = RLCS_ClientsManagement.GetStateRecordbyCode(excelState.ToUpper());

                            if (State != null)
                            {
                                ClientLocationVModel.CM_State = State.SM_Code;
                                ClientLocationVModel.StateModel.AVACOM_StateID = State.AVACOM_StateID;
                            }
                            else
                            {
                                LogErrors.Add("No State found-" + excelState.ToUpper() + ", Check at Row-" + count);
                            }
                        }

                        string excelCity = xlWorksheet.Cells[i, 3].Text.Trim();
                        if (string.IsNullOrEmpty(excelCity))
                            LogErrors.Add("Required Location at Row -" + count);
                        else if (!String.IsNullOrEmpty(excelCity) && xlWorksheet.Cells[i, 3].Text.ToString().Trim().GetType() == typeof(string))
                        {
                            if (!String.IsNullOrEmpty(excelCity) && !String.IsNullOrEmpty(ClientLocationVModel.CM_State))
                            {
                                var locationCityRecord = RLCS_ClientsManagement.GetCity(ClientLocationVModel.CM_State, excelCity.ToUpper());

                                if (locationCityRecord == null)
                                    LogErrors.Add("No Location found-" + excelCity + " in State-" + ClientLocationVModel.CM_State.ToUpper() + ", Check at Row-" + count + ", Please Refer Masters->Location-City");
                                else
                                {
                                    ClientLocationVModel.CM_City = locationCityRecord.LM_Code;
                                }
                            }
                            else
                                LogErrors.Add("Invalid Location ,Check at Row -" + count);
                            //bool city = RLCS_ClientsManagement.GetCitybyCode(ClientLocationVModel.CM_State, excelCity.ToUpper());
                            //if (city == false)
                            //    LogErrors.Add("No Location found-" + excelCity + ", Check at Row-" + count);
                            //else
                            //{
                            //    var Location = RLCS_ClientsManagement.GetCity(excelCity.ToUpper());
                            //    if (Location != null)
                            //        ClientLocationVModel.CM_City = Location.LM_Code;
                            //}
                        }

                        string excelBranchName = xlWorksheet.Cells[i, 4].Text.Trim();
                        if (string.IsNullOrEmpty(excelBranchName))
                            LogErrors.Add("Required Branch Name at Row -" + count);
                        else if (!String.IsNullOrEmpty(excelBranchName) && xlWorksheet.Cells[i, 4].Text.ToString().Trim().GetType() == typeof(string))
                        {
                            if (!string.IsNullOrEmpty(excelClientID))
                            {
                                if (!string.IsNullOrEmpty(ClientLocationVModel.CM_ClientID))
                                {
                                    string corpID = string.Empty;
                                    corpID = RLCS_ClientsManagement.GetCorporateID(excelClientID);

                                    if (!string.IsNullOrEmpty(corpID))
                                    {
                                        //GG ADDED
                                        int? CustomerID = RLCS_ClientsManagement.GetAVACOMCustIDByCorpID(corpID);
                                        ClientLocationVModel.ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(CustomerID));
                                        //END
                                        ClientLocationVModel.CorporateID = corpID;

                                        bool ExistsBranch = RLCS_ClientsManagement.CheckBranch(excelClientID, ClientLocationVModel.CorporateID, excelBranchName, "B");
                                        if (ExistsBranch)
                                        {
                                            LogErrors.Add("Branch with same name already exists, Check at Row-" + count + ". Please provide unique Branch Name");
                                        }
                                        else
                                            ClientLocationVModel.AVACOM_BranchName = excelBranchName;
                                    }
                                    else
                                        LogErrors.Add("Invalid ClientID, Check at Row-" + count);
                                }
                            }
                            //else
                            //    LogErrors.Add("Invalid Branch Name,Check at Row -" + count);
                        }


                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                            LogErrors.Add("Required branch address at Row -" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()) && xlWorksheet.Cells[i, 5].Text.ToString().Trim().GetType() == typeof(string))
                        {
                            ClientLocationVModel.CM_Address = xlWorksheet.Cells[i, 5].Text.ToString().Trim();
                        }

                        string excelLWFState = xlWorksheet.Cells[i, 6].Text.ToString();
                        if (!String.IsNullOrEmpty(excelLWFState))
                        {
                            bool matched = Regex.IsMatch(excelLWFState, @"^[a-zA-Z]+$");
                            if (matched == false)
                                LogErrors.Add("Invalid LWF State-" + excelLWFState + ", Only Characters allowed in LWF State, Check at Row-" + count);
                            else
                            {
                                var StateDetails = RLCS_ClientsManagement.GetStateRecordbyCode(excelLWFState.ToUpper());

                                if (StateDetails != null)
                                    ClientLocationVModel.CL_LWF_State = StateDetails.SM_Code;  //excelLWFState;
                                else
                                    LogErrors.Add("No LWF State found with " + xlWorksheet.Cells[i, 6].Text.ToString().Trim() + " at Row -" + count);
                            }
                        }

                        string excelPTState = xlWorksheet.Cells[i, 7].Text.ToString().Trim().ToUpper();
                        if (!String.IsNullOrEmpty(excelPTState) && xlWorksheet.Cells[i, 7].Text.ToString().Trim().GetType() == typeof(string))
                        {
                            var PTstate = RLCS_ClientsManagement.GetPTStateRecordbyCode(excelPTState);

                            if (PTstate == null)
                                LogErrors.Add("Invalid PT State, or PT not applicable to-" + excelPTState + ",Check at Row -" + count);
                            else
                                ClientLocationVModel.CL_PT_State = PTstate.SM_Code;
                        }
                        string PFCodeType=RLCS_ClientsManagement.GetClientPFType(excelClientID);
                        string excelPFcode = xlWorksheet.Cells[i, 8].Text.ToString().Trim();
                        if (PFCodeType == "C" && !String.IsNullOrEmpty(excelPFcode))
                        {
                            LogErrors.Add("Please check the Client Details,PF Code Type is Client level, Check at Row-" + count);
                        }
                        if (PFCodeType == "B")
                        {
                            //string excelPFcode = xlWorksheet.Cells[i, 8].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(excelPFcode))
                            {
                                //bool PF = Regex.IsMatch(excelPFcode, @"^[a-zA-Z0-9]+$");
                                bool PF = Regex.IsMatch(excelPFcode, @"^[A-Za-z0-9-//\\]+$");
                                if (PF == false)
                                    LogErrors.Add("Invalid PF Code-" + excelPFcode + ", Only alphanumeric characters, slash and hyphens are allowed in PF Code, Check at Row-" + count);
                                //LogErrors.Add("Enter valid PF Code " + excelPFcode + " at Row -" + count);
                                else
                                {
                                    if (RLCS_ClientsManagement.CheckPFCode(excelPFcode))
                                    {
                                        LogErrors.Add("PF Code-" + excelPFcode + " already present in database, it should be unique, Check at Row -" + count);
                                    }
                                    else
                                    {
                                        if (excelPFcode.Length <= 12)
                                        {
                                            ClientLocationVModel.CL_PF_Code = excelPFcode;
                                        }
                                        else
                                        {
                                            LogErrors.Add("PF Code-" + excelPFcode + " can have a maximum of 12 digits, Check at Row -" + count);
                                        }

                                    }

                                }
                            }
                            else
                            {
                                LogErrors.Add("Required PF Code at Row - " + count);
                            }
                        }

                        string excelESICNO = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(excelESICNO))
                        {
                            bool matched = Regex.IsMatch(excelESICNO, @"^[0-9]+$");
                            if (matched == false)
                                LogErrors.Add("Invalid ESI Code-" + excelESICNO + ", Only numbers allowed in ESI Code, Check at Row-" + count);
                            //   LogErrors.Add("Enter valid ESIC Code at Row- " + count);
                            else
                            {
                                if (RLCS_ClientsManagement.CheckESICCode(excelESICNO))
                                {
                                    LogErrors.Add("ESIC No-" + excelESICNO + " already present in database, it should be unique, Check at Row -" + count);
                                }
                                else
                                {
                                    if (excelESICNO.Length <= 21)
                                    {
                                        ClientLocationVModel.CL_ESIC_Code = excelESICNO;
                                    }
                                    else
                                    {
                                        LogErrors.Add("ESIC No-" + excelESICNO + "can have a maximum of 21 digits, Check at Row -" + count);
                                    }
                                }
                            }

                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                            LogErrors.Add("Required office type at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()) && xlWorksheet.Cells[i, 10].Text.Trim().GetType() == typeof(string))
                        {
                            string officeType = xlWorksheet.Cells[i, 10].Text.ToString().Trim().ToUpper();

                            if (officeType == "FACTORY")
                                ClientLocationVModel.CL_OfficeType = "Factory";
                            else if (officeType == "HEADOFFICE" || officeType == "HEAD OFFICE")
                                ClientLocationVModel.CL_OfficeType = "HeadOffice";
                            else if (officeType == "BRANCH")
                                ClientLocationVModel.CL_OfficeType = "Branch";
                            else
                            {
                                if (String.IsNullOrEmpty(ClientLocationVModel.CL_OfficeType))
                                    LogErrors.Add("Invalid office type, Possible Values will be Factory or Branch or HeadOffice, Check at Row-" + count);
                            }
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                            LogErrors.Add("Required Establishment Type at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()) && xlWorksheet.Cells[i, 11].Text.Trim().GetType() == typeof(string))
                        {
                            string estType = xlWorksheet.Cells[i, 11].Text.ToString().Trim().ToUpper();

                            if (estType == "FACTORY" || estType == "FACT")
                                ClientLocationVModel.CM_EstablishmentType = "FACT";
                            else if (estType == "SHOP & ESTABLISHMENT" || estType == "SEA" || estType == "SHOP AND ESTABLISHMENT")
                                ClientLocationVModel.CM_EstablishmentType = "SEA";
                            else if (estType == "BOTH" || estType == "BOTH")
                                ClientLocationVModel.CM_EstablishmentType = "SF";
                            else
                            {
                                if (String.IsNullOrEmpty(ClientLocationVModel.CM_EstablishmentType))
                                    LogErrors.Add("Invalid Establishment, Possible Values will be Factory or Shop & Establishment or Both, Check at Row-" + count);
                            }
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                            LogErrors.Add("Required Employer Name at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()) && xlWorksheet.Cells[i, 12].Text.Trim().GetType() == typeof(string))
                        {
                            bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 12].Text.ToString().Trim(), @"^[a-zA-Z. ]+$");
                            if (matched == false)
                                LogErrors.Add("Invalid Employer Name, Only characters allowed, Check at Row- " + count);
                            else
                                ClientLocationVModel.CL_EmployerName = xlWorksheet.Cells[i, 12].Text.ToString().Trim();
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                            LogErrors.Add("Required Employer Address at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()) && xlWorksheet.Cells[i, 13].Text.Trim().GetType() == typeof(string))
                        {
                            ClientLocationVModel.CL_EmployerAddress = xlWorksheet.Cells[i, 13].Text.ToString().Trim();

                            //bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 13].Text.ToString().Trim(), @"^[a-zA-Z0-9 ]+$");
                            //if (matched == false)
                            //    LogErrors.Add("Enter valid Employer Address at Row-" + count);
                            //else
                            //    ClientLocationVModel.CL_EmployerAddress = xlWorksheet.Cells[i, 13].Text.ToString().Trim();
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                            LogErrors.Add("Required Manager Name at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()) && xlWorksheet.Cells[i, 14].Text.Trim().GetType() == typeof(string))
                        {
                            bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 14].Text.ToString().Trim(), @"^[a-zA-Z. ]+$");
                            if (matched == false)
                                LogErrors.Add("Invalid Manager Name, Only characters allowed, Check at Row- " + count);
                            //LogErrors.Add("Enter valid Manager Name at Row- " + count);
                            else
                                ClientLocationVModel.CL_ManagerName = xlWorksheet.Cells[i, 14].Text.ToString().Trim();
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                            LogErrors.Add("Required Manager Address at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()) && xlWorksheet.Cells[i, 15].Text.Trim().GetType() == typeof(string))
                        {
                            ClientLocationVModel.CL_ManagerAddress = xlWorksheet.Cells[i, 15].Text.ToString().Trim();

                            //bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 15].Text.ToString().Trim(), @"^[a-zA-Z0-9 ]+$");
                            //if (matched == false)
                            //    LogErrors.Add("Enter valid Manager Address at Row- " + count);
                            //else
                            //    ClientLocationVModel.CL_ManagerAddress = xlWorksheet.Cells[i, 15].Text.ToString().Trim();
                        }

                        string comphno = xlWorksheet.Cells[i, 16].Text.ToString().Trim();
                        if (string.IsNullOrEmpty(comphno))
                            LogErrors.Add("Required Company phone no at Row-" + count);
                        else if (!String.IsNullOrEmpty(comphno) && comphno.GetType() == typeof(string))
                        {
                            try
                            {
                                if (Convert.ToInt64(comphno) > 0)
                                {
                                    if (comphno.Length < 10 || comphno.Length > 15)
                                        LogErrors.Add("Company phone number can have a minimum of 10 digits and maximum of 15 digits, Check at Row-" + count);
                                    else
                                    {
                                        long compnyphno = Convert.ToInt64(comphno);
                                        ClientLocationVModel.CL_CompPhoneNo = Convert.ToString(compnyphno);
                                    }
                                }
                                else
                                    LogErrors.Add("Invalid Company Phone Number, Only Numbers are allowed, Check at Row-" + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Enter valid Company phone no at Row-" + count);
                            }
                        }


                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()) && xlWorksheet.Cells[i, 17].Text.Trim().GetType() == typeof(string))
                        {
                            bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 17].Text.ToString().Trim(), @"^[a-zA-Z. ]+$");
                            if (matched == false)
                                LogErrors.Add("Enter valid HR contact person at Row- " + count);
                            else
                                ClientLocationVModel.CL_HRContactPerson = xlWorksheet.Cells[i, 17].Text.ToString().Trim();
                        }

                        string HRphno = xlWorksheet.Cells[i, 18].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                            LogErrors.Add("Required Hr phone no at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()) && xlWorksheet.Cells[i, 18].Text.Trim().GetType() == typeof(string))
                        {
                            try
                            {
                                if (Convert.ToInt64(HRphno) > 0)
                                {
                                    if (HRphno.Length < 10 || HRphno.Length > 15)
                                        LogErrors.Add("HR phone number can have a minimum of 10 digits and maximum of 15 digits, Check at Row-" + count);
                                    else
                                    {
                                        long hrphno = Convert.ToInt64(HRphno);
                                        ClientLocationVModel.CL_HRPhNo = Convert.ToString(hrphno);
                                    }
                                }
                                else
                                    LogErrors.Add("Invalid HR Phone Number, Only Numbers are allowed, Check at Row-" + count);
                                //LogErrors.Add("Please enter positive value for HR phone no at Row-" + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid HR phone number, Check at Row-" + count);
                            }
                        }

                        string excelHrmail = xlWorksheet.Cells[i, 19].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(excelHrmail) && xlWorksheet.Cells[i, 19].Text.Trim().GetType() == typeof(string))
                        {
                            try
                            {
                                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                                Match match = regex.Match(excelHrmail);
                                if (match.Success)
                                    ClientLocationVModel.CL_HRMailID = excelHrmail;
                                else
                                    LogErrors.Add("Invalid HR EMailID, Check at Row- " + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid HR EMailID, Check at Row- " + count);
                            }
                        }

                        string excelHr1stmail = xlWorksheet.Cells[i, 20].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(excelHr1stmail) && excelHr1stmail.GetType() == typeof(string))
                        {
                            try
                            {
                                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                                Match match = regex.Match(excelHr1stmail);
                                if (match.Success)
                                    ClientLocationVModel.CL_HR1stLevelMail = excelHr1stmail;
                                else
                                    LogErrors.Add("Invalid HR 1st Level EMail, Check at Row- " + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid HR 1st Level EMail, Check at Row- " + count);
                            }
                        }

                        string HR1stphno = xlWorksheet.Cells[i, 21].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()) && xlWorksheet.Cells[i, 21].Text.Trim().GetType() == typeof(string))
                        {
                            try
                            {
                                if (Convert.ToInt64(HR1stphno) > 0)
                                {
                                    if (HRphno.Length < 10 || HRphno.Length > 15)
                                        LogErrors.Add("HR 1st level phone no can have a minimum of 10 digits and maximum of 15 digits, Check at Row-" + count);
                                    else
                                    {
                                        long hrphno = Convert.ToInt64(HR1stphno);
                                        ClientLocationVModel.CL_HR1stLevelPhNo = HR1stphno;
                                    }
                                }
                                else
                                    LogErrors.Add("Invalid HR 1st level phone no, Check at Row- " + count);
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid HR 1st level phone no, Check at Row- " + count);
                            }
                        }

                        string RCNO = xlWorksheet.Cells[i, 22].Text.ToString().Trim();

                        if (!String.IsNullOrEmpty(RCNO) && xlWorksheet.Cells[i, 22].Text.Trim().GetType() == typeof(string))
                        {
                            /*bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 22].Text.ToString().Trim(), @"^[a-zA-Z0-9]+$");
                            if (matched == false)
                            {
                                LogErrors.Add("Enter valid RC NO at Row- " + count);
                            }*/
                            bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 22].Text.ToString().Trim(), @"^[A-Za-z0-9 -@.!#$%&*(){|}_=+,;//\\]+$");
                            if (matched == false)
                                LogErrors.Add("Enter valid RC NO at Row- " + count + " Not allowed ~`'");
                            else
                            {
                                if (RCNO.Length <= 20)
                                {
                                    ClientLocationVModel.CL_RCNo = xlWorksheet.Cells[i, 22].Text.ToString().Trim();
                                }
                                else
                                {
                                    LogErrors.Add("RC NO can have a maximum of 20 digits at Row-" + count);
                                }
                            }

                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()) && xlWorksheet.Cells[i, 23].Text.Trim().GetType() == typeof(string))
                        {
                            string rcValidFrom = xlWorksheet.Cells[i, 23].Text.ToString().Trim();

                            try
                            {
                                //DateTime abc = DateTime.Parse(rcValidFrom);
                                DateTime dt = DateTime.ParseExact(rcValidFrom, "dd/MM/yyyy", null);
                                ClientLocationVModel.CL_RCValidFrom = dt.ToString();
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Enter valid RC valid (from) date in DD/MM/YYYY format at Row-" + count);
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()) && xlWorksheet.Cells[i, 24].Text.Trim().GetType() == typeof(string))
                        {
                            string rcValidTo = xlWorksheet.Cells[i, 24].Text.ToString().Trim();
                            string rcValidFrom = xlWorksheet.Cells[i, 23].Text.ToString().Trim();
                            try
                            {
                                DateTime dtrcValidTo = DateTime.ParseExact(rcValidTo, "dd/MM/yyyy", null);
                                DateTime dtrcValidFrom = DateTime.ParseExact(rcValidFrom, "dd/MM/yyyy", null);
                                if (dtrcValidFrom < dtrcValidTo)
                                {
                                    // DateTime abc = DateTime.Parse(rcValidTo);
                                    ClientLocationVModel.CL_RCValidTo = dtrcValidTo.ToString();
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Date: RC Valid (from) should be less than RC valid (to), Check at Row-" + count);
                                }

                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Enter valid RC valid (to) date in DD/MM/YYYY format at Row-" + count);
                            }
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                            LogErrors.Add("Required Nature of business at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()) && xlWorksheet.Cells[i, 25].Text.Trim().GetType() == typeof(string))
                        {
                            //bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 25].Text.ToString().Trim(), @"^[a-zA-Z ]+$");
                            bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 25].Text.ToString().Trim(), @"^[A-Za-z -@.!#$%&*(){|}_=+,;//\\]+$");
                            if (matched == false)
                                LogErrors.Add("Enter valid Nature of business at Row- " + count + " Numbers and ~`' are not allowed");
                            else
                                ClientLocationVModel.CL_NatureOfBusiness = xlWorksheet.Cells[i, 25].Text.ToString().Trim();
                        }

                        string excelWeekoff = xlWorksheet.Cells[i, 26].Text.ToString().Trim();
                        if (String.IsNullOrEmpty(excelWeekoff))
                            LogErrors.Add("Required Weekoff day at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()) && xlWorksheet.Cells[i, 26].Text.Trim().GetType() == typeof(string))
                        {
                            string estType1 = xlWorksheet.Cells[i, 26].Text.ToString().Trim().ToUpper();
                            string estType = estType1.Replace(" ", "");
                            string[] weekday = estType.Split('|');
                            foreach (var s in weekday)
                            {
                                if (weekday.Count() > 1)
                                {
                                    if (s == "SUNDAY" || s == "SUN")
                                        ClientLocationVModel.CL_WeekoffDay += "SUN|";
                                    else if (s == "MONDAY" || s == "MON")
                                        ClientLocationVModel.CL_WeekoffDay += "MON|";
                                    else if (s == "TUESDAY" || s == "TUE")
                                        ClientLocationVModel.CL_WeekoffDay += "TUE|";
                                    else if (s == "WEDNESDAY" || s == "WED")
                                        ClientLocationVModel.CL_WeekoffDay += "WED|";
                                    else if (s == "THURSDAY" || s == "THU")
                                        ClientLocationVModel.CL_WeekoffDay += "THU|";
                                    else if (s == "FRIDAY" || s == "FRI")
                                        ClientLocationVModel.CL_WeekoffDay += "FRI|";
                                    else if (s == "SATURDAY" || s == "SAT")
                                        ClientLocationVModel.CL_WeekoffDay += "SAT|";
                                    else
                                    {
                                        if (String.IsNullOrEmpty(ClientLocationVModel.CL_WeekoffDay))
                                            LogErrors.Add("Invalid value for Week off day, Check at Row-" + count);
                                    }
                                }
                                else
                                {
                                    if (estType == "SUNDAY" || estType == "SUN")
                                        ClientLocationVModel.CL_WeekoffDay = "SUN";
                                    else if (estType == "MONDAY" || estType == "MON")
                                        ClientLocationVModel.CL_WeekoffDay = "MON";
                                    else if (estType == "TUESDAY" || estType == "TUE")
                                        ClientLocationVModel.CL_WeekoffDay = "TUE";
                                    else if (estType == "WEDNESDAY" || estType == "WED")
                                        ClientLocationVModel.CL_WeekoffDay = "WED";
                                    else if (estType == "THURSDAY" || estType == "THU")
                                        ClientLocationVModel.CL_WeekoffDay = "THU";
                                    else if (estType == "FRIDAY" || estType == "FRI")
                                        ClientLocationVModel.CL_WeekoffDay = "FRI";
                                    else if (estType == "SATURDAY" || estType == "SAT")
                                        ClientLocationVModel.CL_WeekoffDay = "SAT";
                                    else
                                    {
                                        if (String.IsNullOrEmpty(ClientLocationVModel.CL_WeekoffDay))
                                            LogErrors.Add("Invalid value of Week off day, Check at Row-" + count);
                                    }
                                }
                            }

                            if (!String.IsNullOrEmpty(ClientLocationVModel.CL_WeekoffDay) && ClientLocationVModel.CL_WeekoffDay.EndsWith("|"))
                                ClientLocationVModel.CL_WeekoffDay = ClientLocationVModel.CL_WeekoffDay.Remove(ClientLocationVModel.CL_WeekoffDay.Length - 1);
                        }

                        DateTime fromWorkHrs = new DateTime();
                        string excelwrkhrsfrom = xlWorksheet.Cells[i, 27].Text.ToString().Trim();
                        if (string.IsNullOrEmpty(excelwrkhrsfrom))
                            LogErrors.Add("Required Work hours (from) in HH:MM format, Check at Row-" + count);
                        else if (!String.IsNullOrEmpty(excelwrkhrsfrom) && excelwrkhrsfrom.GetType() == typeof(string))
                        {
                            try
                            {
                                bool matcht = Regex.IsMatch(excelwrkhrsfrom, @"(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)");
                                //
                                if (matcht == false)
                                    LogErrors.Add("Invalid time format for Work Hours(From), It should be in HH:MM (i.e. 09:30), Check at Row-" + count);
                                else
                                {
                                    DateTime time = DateTime.Parse(excelwrkhrsfrom);
                                    fromWorkHrs = time;
                                    string timevalue = time.ToString("hh:mm tt");
                                    bool val = SetupController.CheckTimings(timevalue);
                                    if (val)
                                    {

                                        ClientLocationVModel.CL_WorkHoursFrom = timevalue;
                                    }
                                    else
                                        LogErrors.Add("Invalid time format for Work Hours(From), It should be in HH:MM (i.e. 09:15 AM or 09:30 AM), Check at Row-" + count);

                                }
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid time format for Work Hours(From), It should be in HH:MM (i.e. 09:30), Check at Row-" + count);
                            }
                        }

                        DateTime ToWorkHrs = new DateTime();
                        string excelwrkhrsto = xlWorksheet.Cells[i, 28].Text.ToString().Trim();
                        if (string.IsNullOrEmpty(excelwrkhrsto))
                            LogErrors.Add("Required Work hours (to) in HH:MM format, Check at Row-" + count);
                        else if (!String.IsNullOrEmpty(excelwrkhrsto) && excelwrkhrsto.GetType() == typeof(string))
                        {
                            try
                            {
                                bool matcht = Regex.IsMatch(excelwrkhrsto, @"(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)");
                                //
                                if (matcht == false)
                                    LogErrors.Add("Invalid time format for Work Hours(To), It should be in HH:MM (i.e. 09:30), Check at Row-" + count);
                                else
                                {
                                    DateTime time = DateTime.Parse(excelwrkhrsto);
                                    ToWorkHrs = time;
                                    if (fromWorkHrs < time)
                                    {
                                        string timevalue = time.ToString("hh:mm tt");
                                        bool val = SetupController.CheckTimings(timevalue);
                                        if (val)
                                        {
                                            ClientLocationVModel.CL_WorkHoursTo = timevalue;
                                        }
                                        else
                                            LogErrors.Add("Invalid time format for Work Hours(To), It should be in HH:MM (i.e. 09:15 AM or 09:30 AM), Check at Row-" + count);
                                    }
                                    else
                                    {
                                        LogErrors.Add("Work Hours From should be less than Work Hours To, Check at Row-" + count);
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid time format for Work Hours(To), It should be in HH:MM (i.e. 09:30), Check at Row-" + count);
                                //LogErrors.Add("enter valid time format for Work Hours From at Row- " + count);
                            }
                        }

                        DateTime IntervalFromHrs = new DateTime();
                        string excelintervalfrm = xlWorksheet.Cells[i, 29].Text.ToString().Trim();
                        if (string.IsNullOrEmpty(excelintervalfrm))
                            LogErrors.Add("Required Intervals (from) in HH:MM format, Check at Row-" + count);
                        else if (!String.IsNullOrEmpty(excelintervalfrm))
                        {
                            try
                            {
                                bool matcht = Regex.IsMatch(excelintervalfrm, @"(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)");
                                //
                                if (matcht == false)
                                    LogErrors.Add("Invalid time format for Intervals(From), It should be in HH:MM (i.e. 09:30), Check at Row-" + count);
                                else
                                {
                                    DateTime time = DateTime.Parse(excelintervalfrm);
                                    IntervalFromHrs = time;

                                    if (IntervalFromHrs >= fromWorkHrs && IntervalFromHrs < ToWorkHrs)
                                    {

                                        string timevalue = time.ToString("hh:mm tt");
                                        bool val = SetupController.CheckTimings(timevalue);
                                        if (val)
                                        {
                                            ClientLocationVModel.CL_IntervalsFrom = timevalue;
                                        }
                                        else
                                            LogErrors.Add("Invalid time format for Intervals(From), It should be in HH:MM (i.e. 09:15 AM or 09:30 AM), Check at Row-" + count);
                                    }
                                    else
                                    {
                                        LogErrors.Add("Interval(From) should be between Work Hours(From) and Work Hours(To), Check at Row-" + count);
                                    }


                                }
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid time format for Intervals(From), It should be in HH:MM (i.e. 09:30), Check at Row-" + count);
                                //LogErrors.Add("enter valid time format for Intervals From at Row- " + count);
                            }
                        }

                        DateTime IntervalToHrs = new DateTime();
                        string excelintervalto = xlWorksheet.Cells[i, 30].Text.ToString().Trim();
                        if (string.IsNullOrEmpty(excelintervalto))
                            LogErrors.Add("Required Intervals (to) in HH:MM format, Check at Row-" + count);
                        else if (!String.IsNullOrEmpty(excelintervalto))
                        {
                            try
                            {
                                bool matcht = Regex.IsMatch(excelintervalto, @"(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)");
                                //bool matcht = Regex.IsMatch(excelintervalto, @"^(?:[01][0-9]|2[0-3]):[0-5][0-9]$");

                                if (matcht == false)
                                    LogErrors.Add("Invalid time format for Intervals(To), It should be in HH:MM (i.e. 09:30), Check at Row-" + count);
                                else
                                {
                                    DateTime time1 = DateTime.Parse(excelintervalto);
                                    IntervalToHrs = time1;

                                    if (IntervalToHrs > fromWorkHrs && IntervalToHrs <= ToWorkHrs)
                                    {
                                        if (IntervalFromHrs < time1)
                                        {
                                            string timevalue = time1.ToString("hh:mm tt");
                                            bool val = SetupController.CheckTimings(timevalue);
                                            if (val)
                                            {
                                                ClientLocationVModel.CL_IntervalsTo = timevalue;
                                            }
                                            else
                                                LogErrors.Add("Invalid time format for Intervals(To), It should be in HH:MM (i.e. 09:15 AM or 09:30 AM), Check at Row-" + count);
                                        }
                                        else
                                            LogErrors.Add("Interval(From) should be less than Interval(To), Check at Row-" + count);

                                    }
                                    else
                                        LogErrors.Add("Interval(To) should be between Work Hours(From) and Work Hours(To), Check at Row-" + count);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Invalid time format for Intervals(To), It should be in HH:MM (i.e. 09:30), Check at Row-" + count);
                                //LogErrors.Add("enter valid time format for Intervals To at Row- " + count);
                            }
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                            LogErrors.Add("Required Work timings, Check at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()) && xlWorksheet.Cells[i, 31].Text.Trim().GetType() == typeof(string))
                        {
                            string wrktimings = xlWorksheet.Cells[i, 31].Text.ToString().Trim();

                            if (wrktimings.ToUpper() == "SHIFT")
                                ClientLocationVModel.CL_WorkTimings = "Shift";
                            else if (wrktimings.ToUpper() == "STANDARD")
                                ClientLocationVModel.CL_WorkTimings = "Standard";
                            else
                                LogErrors.Add("Invalid Work timings, Check at Row- " + count);
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                            LogErrors.Add("Required status, Check at Row-" + count);
                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()) && xlWorksheet.Cells[i, 32].Text.Trim().GetType() == typeof(string))
                        {
                            string status = xlWorksheet.Cells[i, 32].Text.ToString().Trim();

                            if (status.ToUpper() == "ACTIVE" || status.ToUpper() == "A")
                                ClientLocationVModel.CL_Status = "A";
                            else if (status.ToUpper() == "INACTIVE" || status.ToUpper() == "I")
                                ClientLocationVModel.CL_Status = "I";
                            else
                                LogErrors.Add("Invalid Status, it should be Active or Inactive, Check at Row-" + count);
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()) && xlWorksheet.Cells[i, 33].Text.Trim().GetType() == typeof(string))
                        {
                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_LIN = null;
                            }
                            else
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 33].Text.ToString().Trim(), @"^[a-zA-Z0-9]+$");
                                if (matched == false)
                                    LogErrors.Add("Invalid Lin, it should contains only characters and numbers, Check at Row-" + count);
                                else
                                    ClientLocationVModel.CL_LIN = xlWorksheet.Cells[i, 33].Text.ToString().Trim();
                            }
                        }

                        if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                        {
                            ClientLocationVModel.CL_Municipality = null;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()))
                                LogErrors.Add("Required Muncipality at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()) && xlWorksheet.Cells[i, 34].Text.Trim().GetType() == typeof(string))
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 34].Text.ToString().Trim(), @"^[a-zA-Z ]+$");
                                if (matched == false)
                                    LogErrors.Add("Invalid Muncipality, only characters allowed, Check at Row-" + count);
                                else
                                    ClientLocationVModel.CL_Municipality = xlWorksheet.Cells[i, 34].Text.ToString().Trim();
                            }
                        }

                        if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                        {
                            ClientLocationVModel.CL_Permission_MaintaingForms = null;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text.ToString().Trim()))
                                LogErrors.Add("Required Permission for maintaining forms at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text.ToString().Trim()) && xlWorksheet.Cells[i, 35].Text.Trim().GetType() == typeof(string))
                            {
                                string mainfrms = xlWorksheet.Cells[i, 35].Text.ToString().Trim();
                                if (mainfrms.ToUpper() == "YES" || mainfrms.ToUpper() == "Y")
                                    ClientLocationVModel.CL_Permission_MaintaingForms = "Y";
                                else if (mainfrms.ToUpper() == "NO" || mainfrms.ToUpper() == "N")
                                    ClientLocationVModel.CL_Permission_MaintaingForms = "N";
                                else
                                    LogErrors.Add("Enter either Yes or No for permission to maintain forms, Check at Row-" + count);
                            }
                        }

                        if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                        {
                            ClientLocationVModel.CL_RequirePowerforFines = null;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 36].Text.ToString().Trim()))
                                LogErrors.Add("Required Require power to impose fines at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 36].Text.ToString().Trim()) && xlWorksheet.Cells[i, 36].Text.Trim().GetType() == typeof(string))
                            {
                                string powerfrms = xlWorksheet.Cells[i, 36].Text.ToString().Trim();
                                if (powerfrms.ToUpper() == "YES" || powerfrms.ToUpper() == "Y")
                                    ClientLocationVModel.CL_RequirePowerforFines = "Y";
                                else if (powerfrms.ToUpper() == "NO" || powerfrms.ToUpper() == "N")
                                    ClientLocationVModel.CL_RequirePowerforFines = "N";
                                else
                                    LogErrors.Add("Enter either Yes or No for Require power to impose fines at Row- " + count);
                            }
                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text.ToString().Trim()))
                            LogErrors.Add("Required Business type forms at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text.ToString().Trim()) && xlWorksheet.Cells[i, 37].Text.Trim().GetType() == typeof(string))
                        {
                            string businesstype = xlWorksheet.Cells[i, 37].Text.ToString().Trim();
                            if (businesstype.Trim().ToUpper() == "IT")
                                ClientLocationVModel.CL_BusinessType = "IT";
                            else if (businesstype.ToUpper() == "ITES")
                                ClientLocationVModel.CL_BusinessType = "ITES";
                            else if (businesstype.ToUpper() == "NON-IT")
                                ClientLocationVModel.CL_BusinessType = "NON-IT";
                            else
                                LogErrors.Add("Invalid Business type, Possible values will be IT or ITES or NON-IT at Row- " + count);
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 38].Text.ToString().Trim()) && xlWorksheet.Cells[i, 38].Text.Trim().GetType() == typeof(string))
                        {
                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_CommencementDate = null;
                            }
                            else
                            {
                                string effectdate = xlWorksheet.Cells[i, 38].Text.ToString().Trim();

                                try
                                {
                                    DateTime dtrcValidTo = DateTime.ParseExact(effectdate, "dd/MM/yyyy", null);
                                    ClientLocationVModel.CL_CommencementDate = dtrcValidTo.ToString();
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Enter valid Effective date in DD/MM/YYYY format at Row-" + count);
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 39].Text.ToString().Trim()) && xlWorksheet.Cells[i, 39].Text.Trim().GetType() == typeof(string))
                        {
                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_ClasssificationofEstabilishment = null;
                            }
                            else
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 39].Text.ToString().Trim(), @"^[a-zA-Z ]+$");
                                if (matched == false)
                                    LogErrors.Add("Invalid Classsification of estabilishment it should be Characters only, Check at Row- " + count);
                                else
                                    ClientLocationVModel.CL_ClasssificationofEstabilishment = xlWorksheet.Cells[i, 39].Text.ToString().Trim();
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 40].Text.ToString().Trim()) && xlWorksheet.Cells[i, 41].Text.Trim().GetType() == typeof(string))
                        {
                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_LicenceNo = null;
                            }
                            else
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 40].Text.ToString().Trim(), @"^[a-zA-Z0-9]+$");
                                if (matched == false)
                                    LogErrors.Add("Invalid Licence No it should be Number only, Check at Row- " + count);
                                else
                                {
                                    ClientLocationVModel.CL_LicenceNo = xlWorksheet.Cells[i, 40].Text.ToString().Trim();
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 41].Text.ToString().Trim()) && xlWorksheet.Cells[i, 42].Text.Trim().GetType() == typeof(string))
                        {
                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_NICCode = null;
                            }
                            else
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 41].Text.ToString().Trim(), @"^[a-zA-z0-9]+$");
                                if (matched == false)
                                    LogErrors.Add("Invalid valid NIC Code it should be Characters and Number only,Check  at Row- " + count);
                                else
                                    ClientLocationVModel.CL_NICCode = xlWorksheet.Cells[i, 41].Text.ToString().Trim();
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 42].Text.ToString().Trim()) && xlWorksheet.Cells[i, 42].Text.Trim().GetType() == typeof(string))
                        {
                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_SectionofAct = null;
                            }
                            else
                            {
                                string section = xlWorksheet.Cells[i, 42].Text.ToString().Trim();
                                if (xlWorksheet.Cells[i, 42].Text.ToString().Trim().ToUpper() == "2M(I)")
                                    ClientLocationVModel.CL_SectionofAct = "2m(i)";
                                else if (xlWorksheet.Cells[i, 42].Text.ToString().Trim().ToUpper() == "2M(II)")
                                    ClientLocationVModel.CL_SectionofAct = "2m(ii)";
                                else if (xlWorksheet.Cells[i, 42].Text.ToString().Trim().ToUpper() == "SECTION-85")
                                    ClientLocationVModel.CL_SectionofAct = "Section-85";
                                else
                                    LogErrors.Add("Invalid value for Section of act it should be 2m(i) or 2m(ii) or section-85,Check at Row-" + count);
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 43].Text.ToString().Trim()) && xlWorksheet.Cells[i, 43].Text.Trim().GetType() == typeof(string))
                        {
                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_District = null;
                            }
                            else
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 43].Text.ToString().Trim(), @"^[a-zA-Z]+$");
                                if (matched == false)
                                    LogErrors.Add("Enter valid District. It should be Characters only, Check at Row- " + count);
                                else
                                    ClientLocationVModel.CL_District = xlWorksheet.Cells[i, 43].Text.ToString().Trim();
                            }
                        }

                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 44].Text.ToString().Trim()) && xlWorksheet.Cells[i, 44].Text.Trim().GetType() == typeof(string))
                        {
                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_Jurisdiction = null;
                            }
                            else
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 44].Text.ToString().Trim(), @"^[a-zA-z ]+$");
                                if (matched == false)
                                    LogErrors.Add("Invalid Jurisdiction it should be Characters only,Check at Row- " + count);
                                else
                                    ClientLocationVModel.CL_Jurisdiction = xlWorksheet.Cells[i, 44].Text.ToString().Trim();
                            }
                        }
                        //ADD NEW FIELDS GAP
                        string BranchEndDate = xlWorksheet.Cells[i, 45].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(BranchEndDate))                           
                        {
                            try
                            {
                                DateTime dtBranchEndDate = DateTime.ParseExact(BranchEndDate, "dd/MM/yyyy", null);
                                ClientLocationVModel.CL_BranchEndDate = dtBranchEndDate.ToString();
                            }
                            catch (Exception ex)
                            {
                                LogErrors.Add("Enter valid RC valid (from) date in DD/MM/YYYY format at Row-" + count);
                            }
                        }
                        string TradeLicence = xlWorksheet.Cells[i, 46].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(TradeLicence))
                        {
                                if (TradeLicence.ToUpper() == "YES" || TradeLicence.ToUpper() == "Y")
                                    ClientLocationVModel.CL_TradeLicenceApplicability = "Y";
                                else if (TradeLicence.ToUpper() == "NO" || TradeLicence.ToUpper() == "N")
                                    ClientLocationVModel.CL_TradeLicenceApplicability = "N";
                                else
                                    LogErrors.Add("Enter either Yes or No for Require Trade Licence at Row- " + count);
                        }
                        string EmployerDesignation = xlWorksheet.Cells[i, 47].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(EmployerDesignation))
                        {
                            bool Designation = Regex.IsMatch(EmployerDesignation, @"^[a-zA-Z ]+$");
                            if (Designation)
                                ClientLocationVModel.CL_EmployerDesignation = EmployerDesignation;
                            else
                                LogErrors.Add("Invalid Designation-" + EmployerDesignation + ", it should be Characters only, Check at Row-" + count);
                        }
                        string EDLIExcemption = xlWorksheet.Cells[i, 48].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(EDLIExcemption))
                        {
                            if (EDLIExcemption.ToUpper() == "YES" || EDLIExcemption.ToUpper() == "Y")
                                ClientLocationVModel.CL_EDLI_Excemption = "Y";
                            else if (EDLIExcemption.ToUpper() == "NO" || EDLIExcemption.ToUpper() == "N")
                                ClientLocationVModel.CL_EDLI_Excemption = "N";
                            else
                                LogErrors.Add("Enter either Yes or No for Require EDLI Excemption at Row- " + count);
                        }
                        int customerID = RLCS_ClientsManagement.GetCustomerIDByClientID(ClientLocationVModel.CM_ClientID);
                        int? ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(customerID));
                        if (ServiceProviderID == 94)
                        {
                            int distributorID = RLCS_ClientsManagement.GetParentIDforCustomer(customerID);
                            string LocationAnchor = xlWorksheet.Cells[i, 49].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(LocationAnchor))
                                LogErrors.Add("Required LocationAnchor at Row-" + count);
                            else
                            {
                                bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, LocationAnchor);
                                if (valid == true)
                                {
                                    ClientLocationVModel.CL_LocationAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, LocationAnchor));
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Name for Location Anchor.");
                                }
                            }
                        }
                        if (LogErrors.Count == 0)
                            lstClientLocationVModel.Add(ClientLocationVModel);

                    }

                    if (LogErrors.Count == 0)//to check if branch code already exists
                    {
                        if (lstClientLocationVModel.Select(k => k.ServiceProviderID).Distinct().FirstOrDefault() == 94)
                        {
                            var establishmentTypes = lstClientLocationVModel.Select(t => t.CM_EstablishmentType).Distinct().ToList();
                            foreach (var objEstType in establishmentTypes)
                            {
                                string estbType = objEstType;

                                var lstRecords = lstClientLocationVModel.Where(t => t.CM_EstablishmentType == estbType).ToList();
                                if (lstRecords != null)
                                {
                                    int branchCount = 1;
                                    foreach (var obj in lstRecords)
                                    {
                                        string branchCode = GenerateBranchCode(obj.CM_ClientID, obj.CM_State, obj.CM_EstablishmentType);

                                        string[] genBranchCode = branchCode.Split('-');
                                        string initials = genBranchCode[0];
                                        string estbtypeLocation = genBranchCode[1];
                                        string branchNum = null;

                                        if (branchCount >= 0 && branchCount <= 9)
                                            branchNum = "000" + branchCount;
                                        else if (branchCount >= 10 && branchCount <= 99)
                                            branchNum = "00" + branchCount;
                                        else if (branchCount >= 100 && branchCount <= 999)
                                            branchNum = "0" + branchCount;
                                        else
                                            branchNum = branchCount.ToString();

                                        branchCode = initials + "-" + estbtypeLocation + "-" + branchNum;

                                        if (RLCS_ClientsManagement.CheckBranchCodeExist(branchCode))
                                        {
                                            LogErrors.Add("Branch code " + branchCode + " already exists.");
                                        }
                                        branchCount++;
                                    }
                                }

                            }
                        }
                        //End
                    }




                    #endregion

                    if (LogErrors.Count <= 0)
                    {
                        #region Save 

                        for (int i = 2; i <= noOfRow; i++)
                        {
                            count = i;
                            ClientLocationVModel = new ClientLocationVModel();

                            ClientLocationVModel.CM_ClientID = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
                            ClientLocationVModel.CorporateID = RLCS_ClientsManagement.GetCorporateID(ClientLocationVModel.CM_ClientID);
                            //GG ADDED
                            int? CustomerID = RLCS_ClientsManagement.GetAVACOMCustIDByCorpID(ClientLocationVModel.CorporateID);
                            ClientLocationVModel.ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(CustomerID));
                            //END

                            string excelState = xlWorksheet.Cells[i, 2].Text.Trim();
                            if (string.IsNullOrEmpty(excelState))
                                LogErrors.Add("Required State at Row -" + count);
                            else if (!String.IsNullOrEmpty(excelState) && xlWorksheet.Cells[i, 2].Text.ToString().Trim().GetType() == typeof(string))
                            {
                                var State = RLCS_ClientsManagement.GetStateRecordbyCode(xlWorksheet.Cells[i, 2].Text.ToString().Trim().ToUpper());

                                if (State != null)
                                {
                                    ClientLocationVModel.CM_State = State.SM_Code;
                                    ClientLocationVModel.StateModel.AVACOM_StateID = State.AVACOM_StateID;
                                }
                                else
                                {
                                    LogErrors.Add("No State found with " + xlWorksheet.Cells[i, 2].Text.ToString().Trim().ToUpper() + ", check at Row-" + count);
                                }
                            }

                            string excelCity = xlWorksheet.Cells[i, 3].Text.Trim();
                            if (string.IsNullOrEmpty(excelCity))
                                LogErrors.Add("Required City at Row -" + count);
                            else if (!String.IsNullOrEmpty(excelCity) && xlWorksheet.Cells[i, 3].Text.ToString().Trim().GetType() == typeof(string))
                            {
                                var locationCityRecord = RLCS_ClientsManagement.GetCity(ClientLocationVModel.CM_State, excelCity.ToUpper());

                                if (locationCityRecord == null)
                                    LogErrors.Add("No Location found-" + excelCity + " in State-" + ClientLocationVModel.CM_State.ToUpper() + ", Check at Row-" + count + ", Please Refer Masters->Location-City");
                                else
                                {
                                    ClientLocationVModel.CM_City = locationCityRecord.LM_Code;
                                }

                                //bool city = RLCS_ClientsManagement.GetCitybyCode(ClientLocationVModel.CM_State, excelCity);
                                //if (city == false)
                                //    LogErrors.Add("No match found for City at Row-" + count);

                                //else
                                //{
                                //    var Location = RLCS_ClientsManagement.GetCity(excelCity);
                                //    if (Location != null)
                                //        ClientLocationVModel.CM_City = Location.LM_Code;
                                //}
                            }

                            string excelBranchName = xlWorksheet.Cells[i, 4].Text.Trim();
                            if (string.IsNullOrEmpty(excelBranchName))
                                LogErrors.Add("Required Branch Name at Row -" + count);
                            else if (!String.IsNullOrEmpty(excelBranchName) && xlWorksheet.Cells[i, 4].Text.ToString().Trim().GetType() == typeof(string))
                            {
                                if (!string.IsNullOrEmpty(ClientLocationVModel.CM_ClientID))
                                {
                                    bool ExistsBranch = RLCS_ClientsManagement.CheckBranch(ClientLocationVModel.CM_ClientID, ClientLocationVModel.CorporateID, excelBranchName, "B");
                                    if (ExistsBranch)
                                    {
                                        LogErrors.Add("Branch at Row-" + count + " already exists . Please provide unique Branch Name");
                                    }
                                    else
                                        ClientLocationVModel.AVACOM_BranchName = excelBranchName;
                                }
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                                LogErrors.Add("Required branch address at Row -" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()) && xlWorksheet.Cells[i, 5].Text.ToString().Trim().GetType() == typeof(string))
                            {
                                ClientLocationVModel.CM_Address = xlWorksheet.Cells[i, 5].Text.ToString().Trim();
                            }

                            string excelLWFState = xlWorksheet.Cells[i, 6].Text.ToString();
                            if (!String.IsNullOrEmpty(excelLWFState))
                            {
                                bool matched = Regex.IsMatch(excelLWFState, @"^[a-zA-Z]+$");
                                if (matched == false)
                                    LogErrors.Add("Invalid LWF State-" + excelLWFState + ", Only Characters allowed in LWF State, Check at Row-" + count);
                                else
                                {
                                    var StateDetails = RLCS_ClientsManagement.GetStateRecordbyCode(excelLWFState.ToUpper());

                                    if (StateDetails != null)
                                        ClientLocationVModel.CL_LWF_State = StateDetails.SM_Code;  //excelLWFState;
                                    else
                                        LogErrors.Add("No LWF State found with " + xlWorksheet.Cells[i, 6].Text.ToString().Trim() + " at Row -" + count);
                                }
                            }

                            string excelPTState = xlWorksheet.Cells[i, 7].Text.ToString().Trim().ToUpper();
                            if (!String.IsNullOrEmpty(excelPTState) && xlWorksheet.Cells[i, 7].Text.ToString().Trim().GetType() == typeof(string))
                            {
                                var PTstate = RLCS_ClientsManagement.GetPTStateRecordbyCode(excelPTState);

                                if (PTstate == null)
                                    LogErrors.Add("Invalid PT State, or PT not applicable to-" + excelPTState + ",Check at Row -" + count);
                                else
                                    ClientLocationVModel.CL_PT_State = PTstate.SM_Code;
                            }

                            string excelPFcode = xlWorksheet.Cells[i, 8].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(excelPFcode))
                            {
                                //bool PF = Regex.IsMatch(excelPFcode, @"^[a-zA-Z0-9]+$");
                                bool PF = Regex.IsMatch(excelPFcode, @"^[A-Za-z0-9-//\\]+$");

                                if (PF == false)
                                    LogErrors.Add("Invalid PF Code-" + excelPFcode + ", Only alphanumeric characters, slash and hyphens are allowed in PF Code, Check at Row-" + count);              
                                else
                                {
                                    if (excelPFcode.Length <= 12)
                                    {
                                        ClientLocationVModel.CL_PF_Code = excelPFcode;
                                    }
                                    else
                                    {
                                        LogErrors.Add("PF Code-" + excelPFcode + " can have a maximum of 12 digits, Check at Row -" + count);
                                    }

                                }
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()) && xlWorksheet.Cells[i, 9].Text.ToString().Trim().GetType() == typeof(string))
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 9].Text.ToString().Trim(), @"^[0-9]+$");
                                if (matched == false)
                                    LogErrors.Add("Enter valid ESIC Code at Row- " + count);
                                else
                                    ClientLocationVModel.CL_ESIC_Code = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                                LogErrors.Add("Required office type at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()) && xlWorksheet.Cells[i, 10].Text.Trim().GetType() == typeof(string))
                            {
                                string officeType = xlWorksheet.Cells[i, 10].Text.ToString().Trim().ToUpper();

                                if (officeType == "FACTORY")
                                    ClientLocationVModel.CL_OfficeType = "Factory";
                                else if (officeType == "HEADOFFICE" || officeType == "HEAD OFFICE")
                                    ClientLocationVModel.CL_OfficeType = "HeadOffice";
                                else if (officeType == "BRANCH")
                                    ClientLocationVModel.CL_OfficeType = "Branch";
                                {
                                    if (String.IsNullOrEmpty(ClientLocationVModel.CL_OfficeType))
                                        LogErrors.Add("Enter valid value for office type at Row-" + count + ".Enter Factory or Branch or HeadOffice");
                                }
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                                LogErrors.Add("Required Establishment Type at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()) && xlWorksheet.Cells[i, 11].Text.Trim().GetType() == typeof(string))
                            {
                                string estType = xlWorksheet.Cells[i, 11].Text.ToString().Trim().ToUpper();

                                if (estType == "FACTORY" || estType == "FACT")
                                    ClientLocationVModel.CM_EstablishmentType = "FACT";
                                else if (estType == "SHOP & ESTABLISHMENT" || estType == "SEA" || estType == "SHOP AND ESTABLISHMENT")
                                    ClientLocationVModel.CM_EstablishmentType = "SEA";
                                else if (estType == "BOTH" || estType == "BOTH")
                                    ClientLocationVModel.CM_EstablishmentType = "SF";
                                else
                                {
                                    if (String.IsNullOrEmpty(ClientLocationVModel.CM_EstablishmentType))
                                        LogErrors.Add("Enter valid value for Establishment Type at Row-" + count + ".Enter Factory or Shop & Establishment or Both");
                                }
                            }

                            string EmpName = xlWorksheet.Cells[i, 12].Text.ToString().Trim();
                            if (string.IsNullOrEmpty(EmpName))
                                LogErrors.Add("Required Employer Name at Row-" + count);
                            else if (!String.IsNullOrEmpty(EmpName) && EmpName.GetType() == typeof(string))
                            {
                                bool matched = Regex.IsMatch(EmpName, @"^[a-zA-Z. ]+$");
                                if (matched == false)
                                    LogErrors.Add("Enter valid Employer Name at Row- " + count);
                                else
                                    ClientLocationVModel.CL_EmployerName = EmpName;
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                                LogErrors.Add("Required Employer Address at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()) && xlWorksheet.Cells[i, 13].Text.Trim().GetType() == typeof(string))
                            {
                                ClientLocationVModel.CL_EmployerAddress = xlWorksheet.Cells[i, 13].Text.ToString().Trim();
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                                LogErrors.Add("Required Manager Name at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()) && xlWorksheet.Cells[i, 14].Text.Trim().GetType() == typeof(string))
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 14].Text.ToString().Trim(), @"^[a-zA-Z. ]+$");
                                if (matched == false)
                                    LogErrors.Add("Enter valid Manager Name at Row- " + count);
                                else
                                    ClientLocationVModel.CL_ManagerName = xlWorksheet.Cells[i, 14].Text.ToString().Trim();
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                                LogErrors.Add("Required Manager Address at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()) && xlWorksheet.Cells[i, 15].Text.Trim().GetType() == typeof(string))
                            {
                                ClientLocationVModel.CL_ManagerAddress = xlWorksheet.Cells[i, 15].Text.ToString().Trim();
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                                LogErrors.Add("Required Company phone no at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()) && xlWorksheet.Cells[i, 16].Text.Trim().GetType() == typeof(string))
                            {
                                string comphno = xlWorksheet.Cells[i, 16].Text.ToString().Trim();
                                try
                                {
                                    if (Convert.ToInt64(comphno) > 0)
                                    {
                                        if (comphno.Length < 10 || comphno.Length > 15)
                                            LogErrors.Add("Company phone number can have a minimum of 10 digits and maximum of 15 digits, Check at Row-" + count);
                                        else
                                        {
                                            long compnyphno = Convert.ToInt64(comphno);
                                            ClientLocationVModel.CL_CompPhoneNo = Convert.ToString(compnyphno);
                                        }
                                    }
                                    else
                                        LogErrors.Add("Invalid Company Phone Number, Only Numbers are allowed, Check at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Enter valid Company phone no at Row-" + count);
                                }
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()) && xlWorksheet.Cells[i, 17].Text.Trim().GetType() == typeof(string))
                            {
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 17].Text.ToString().Trim(), @"^[a-zA-Z. ]+$");
                                if (matched == false)
                                    LogErrors.Add("Enter valid HR contact person at Row- " + count);
                                else
                                    ClientLocationVModel.CL_HRContactPerson = xlWorksheet.Cells[i, 17].Text.ToString().Trim();
                            }
                            if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                                LogErrors.Add("Required Hr phone no at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()) && xlWorksheet.Cells[i, 18].Text.Trim().GetType() == typeof(string))
                            {
                                string HRphno = xlWorksheet.Cells[i, 18].Text.ToString().Trim();

                                try
                                {
                                    if (Convert.ToInt64(HRphno) > 0)
                                    {
                                        if (HRphno.Length < 10 || HRphno.Length > 15)
                                            LogErrors.Add("HR phone number can have a minimum of 10 digits and maximum of 15 digits, Check at Row-" + count);
                                        else
                                        {
                                            long hrphno = Convert.ToInt64(HRphno);
                                            ClientLocationVModel.CL_HRPhNo = Convert.ToString(hrphno);
                                        }
                                    }
                                    else
                                        LogErrors.Add("Invalid HR Phone Number, Only Numbers are allowed, Check at Row-" + count);
                                    //LogErrors.Add("Please enter positive value for HR phone no at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Invalid HR phone number, Check at Row-" + count);
                                }

                            }

                            string excelHrmail = xlWorksheet.Cells[i, 19].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(excelHrmail) && xlWorksheet.Cells[i, 19].Text.Trim().GetType() == typeof(string))
                            {
                                try
                                {
                                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                                    Match match = regex.Match(excelHrmail);
                                    if (match.Success)
                                        ClientLocationVModel.CL_HRMailID = excelHrmail;
                                    else
                                        LogErrors.Add("Enter valid HR Mail ID at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Enter valid HR Mail ID at Row-" + count);
                                }
                            }

                            string excelHr1stmail = xlWorksheet.Cells[i, 20].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(excelHr1stmail) && excelHr1stmail.GetType() == typeof(string))
                            {
                                try
                                {
                                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                                    Match match = regex.Match(excelHr1stmail);
                                    if (match.Success)
                                        ClientLocationVModel.CL_HR1stLevelMail = excelHr1stmail;
                                    else
                                        LogErrors.Add("Enter valid 1st HR Mail ID at Row-" + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Enter valid HR 1st Mail ID at Row-" + count);
                                }
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()) && xlWorksheet.Cells[i, 21].Text.Trim().GetType() == typeof(string))
                            {
                                string HR1stphno = xlWorksheet.Cells[i, 21].Text.ToString().Trim();

                                try
                                {
                                    if (Convert.ToInt64(HR1stphno) > 0)
                                    {
                                        if (HR1stphno.Length < 10 || HR1stphno.Length > 15)
                                            LogErrors.Add("HR 1st level phone no can have a minimum of 10 digits and maximum of 15 digits, Check at Row-" + count);
                                        else
                                        {
                                            long hrphno = Convert.ToInt64(HR1stphno);
                                            ClientLocationVModel.CL_HR1stLevelPhNo = HR1stphno;
                                        }
                                    }
                                    else
                                        LogErrors.Add("Invalid HR 1st level phone no it should be numbers only, Check at Row- " + count);
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Invalid HR 1st level phone no, Check at Row- " + count);
                                }
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()) && xlWorksheet.Cells[i, 22].Text.Trim().GetType() == typeof(string))
                            {
                                /*bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 22].Text.ToString().Trim(), @"^[a-zA-Z0-9]+$");
                                if (matched == false)
                                    LogErrors.Add("Enter valid RC NO at Row- " + count);
                                else
                                     if (Convert.ToInt32(xlWorksheet.Cells[i, 22].Text.ToString().Trim()) < 0)
                                     {
                                        LogErrors.Add("RC NO should be positive at Row-" + count);
                                     }
                                     else
                                        ClientLocationVModel.CL_RCNo = xlWorksheet.Cells[i, 22].Text.ToString().Trim();*/
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 22].Text.ToString().Trim(), @"^[A-Za-z0-9 -@.!#$%&*(){|}_=+,;//\\]+$");
                                if (matched == false)
                                    LogErrors.Add("Enter valid RC NO at Row- " + count + " Not allowed ~`'");
                                else
                                    ClientLocationVModel.CL_RCNo = xlWorksheet.Cells[i, 22].Text.ToString().Trim();
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()) && xlWorksheet.Cells[i, 23].Text.Trim().GetType() == typeof(string))
                            {
                                string rcvalidfrom = xlWorksheet.Cells[i, 23].Text.ToString().Trim();

                                try
                                {
                                    DateTime dtrcValidFrom = DateTime.ParseExact(rcvalidfrom, "dd/MM/yyyy", null);
                                    //  DateTime abc = DateTime.Parse(rcvalidfrom);
                                    ClientLocationVModel.CL_RCValidFrom = dtrcValidFrom.ToString();

                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Enter valid RC valid (from) date in DD/MM/YYYY format at Row-" + count);
                                }
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()) && xlWorksheet.Cells[i, 24].Text.Trim().GetType() == typeof(string))
                            {
                                string rcvalidto = xlWorksheet.Cells[i, 24].Text.ToString().Trim();

                                try
                                {
                                    DateTime dtrcValidTo = DateTime.ParseExact(rcvalidto, "dd/MM/yyyy", null);
                                    //DateTime abc = DateTime.Parse(rcvalidto);
                                    ClientLocationVModel.CL_RCValidTo = dtrcValidTo.ToString();
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Enter valid RC valid (to) date in DD/MM/YYYY format at Row-" + count);
                                }
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                                LogErrors.Add("Required Nature of business at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()) && xlWorksheet.Cells[i, 25].Text.Trim().GetType() == typeof(string))
                            {
                                //bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 25].Text.ToString().Trim(), @"^[a-zA-Z ]+$");
                                bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 25].Text.ToString().Trim(), @"^[A-Za-z -@.!#$%&*(){|}_=+,;//\\]+$");
                                if (matched == false)
                                    LogErrors.Add("Enter valid Nature of business at Row- " + count + " Numbers and ~`' are not allowed");
                                else
                                    ClientLocationVModel.CL_NatureOfBusiness = xlWorksheet.Cells[i, 25].Text.ToString().Trim();
                            }

                            string excelWeekoff = xlWorksheet.Cells[i, 26].Text.ToString().Trim();
                            if (String.IsNullOrEmpty(excelWeekoff))
                                LogErrors.Add("Required Weekoff day at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()) && xlWorksheet.Cells[i, 26].Text.Trim().GetType() == typeof(string))
                            {
                                //string estType = xlWorksheet.Cells[i, 26].Text.ToString().Trim().ToUpper();
                                string estType1 = xlWorksheet.Cells[i, 26].Text.ToString().Trim().ToUpper();
                                string estType = estType1.Replace(" ", "");
                                string[] weekday = estType.Split('|');
                                foreach (var s in weekday)
                                {
                                    if (weekday.Count() > 1)
                                    {
                                        if (s == "SUNDAY" || s == "SUN")
                                            ClientLocationVModel.CL_WeekoffDay += "SUN|";
                                        else if (s == "MONDAY" || s == "MON")
                                            ClientLocationVModel.CL_WeekoffDay += "MON|";
                                        else if (s == "TUESDAY" || s == "TUE")
                                            ClientLocationVModel.CL_WeekoffDay += "TUE|";
                                        else if (s == "WEDNESDAY" || s == "WED")
                                            ClientLocationVModel.CL_WeekoffDay += "WED|";
                                        else if (s == "THURSDAY" || s == "THU")
                                            ClientLocationVModel.CL_WeekoffDay += "THU|";
                                        else if (s == "FRIDAY" || s == "FRI")
                                            ClientLocationVModel.CL_WeekoffDay += "FRI|";
                                        else if (s == "SATURDAY" || s == "SAT")
                                            ClientLocationVModel.CL_WeekoffDay += "SAT|";
                                        else
                                        {
                                            if (String.IsNullOrEmpty(ClientLocationVModel.CL_WeekoffDay))
                                                LogErrors.Add("Enter valid value for Weekday off at Row-" + count + ".Enter Factory or Shop & Establishment or Both");
                                        }
                                    }
                                    else
                                    {
                                        if (estType == "SUNDAY" || estType == "SUN")
                                            ClientLocationVModel.CL_WeekoffDay = "SUN";
                                        else if (estType == "MONDAY" || estType == "MON")
                                            ClientLocationVModel.CL_WeekoffDay = "MON";
                                        else if (estType == "TUESDAY" || estType == "TUE")
                                            ClientLocationVModel.CL_WeekoffDay = "TUE";
                                        else if (estType == "WEDNESDAY" || estType == "WED")
                                            ClientLocationVModel.CL_WeekoffDay = "WED";
                                        else if (estType == "THURSDAY" || estType == "THU")
                                            ClientLocationVModel.CL_WeekoffDay = "THU";
                                        else if (estType == "FRIDAY" || estType == "FRI")
                                            ClientLocationVModel.CL_WeekoffDay = "FRI";
                                        else if (estType == "SATURDAY" || estType == "SAT")
                                            ClientLocationVModel.CL_WeekoffDay = "SAT";
                                        else
                                        {
                                            if (String.IsNullOrEmpty(ClientLocationVModel.CL_WeekoffDay))
                                                LogErrors.Add("Enter valid value for Weekday off at Row-" + count + ".Enter Factory or Shop & Establishment or Both");
                                        }
                                    }
                                }

                                if (!String.IsNullOrEmpty(ClientLocationVModel.CL_WeekoffDay) && ClientLocationVModel.CL_WeekoffDay.EndsWith("|"))
                                    ClientLocationVModel.CL_WeekoffDay = ClientLocationVModel.CL_WeekoffDay.Remove(ClientLocationVModel.CL_WeekoffDay.Length - 1);
                            }

                            string excelwrkhrsfrom = xlWorksheet.Cells[i, 27].Text.ToString().Trim();
                            if (string.IsNullOrEmpty(excelwrkhrsfrom))
                                LogErrors.Add("Required Work hours from(in HH:MM format) at Row-" + count);
                            else if (!String.IsNullOrEmpty(excelwrkhrsfrom) && excelwrkhrsfrom.GetType() == typeof(string))
                            {
                                try
                                {
                                    bool matcht = Regex.IsMatch(excelwrkhrsfrom, @"(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)");
                                    //
                                    if (matcht == false)
                                        LogErrors.Add("enter valid time format for Work Hours To at Row- " + count);
                                    else
                                    {
                                        DateTime time = DateTime.Parse(excelwrkhrsfrom);
                                        ClientLocationVModel.CL_WorkHoursFrom = time.ToString("hh:mm tt");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("enter valid time format for Work Hours From at Row- " + count);
                                }
                            }

                            string excelwrkhrsto = xlWorksheet.Cells[i, 28].Text.ToString().Trim();
                            if (string.IsNullOrEmpty(excelwrkhrsto))
                                LogErrors.Add("Required Work hours to(in HH:MM format) at Row-" + count);
                            else if (!String.IsNullOrEmpty(excelwrkhrsto) && excelwrkhrsto.GetType() == typeof(string))
                            {
                                try
                                {
                                    bool matcht = Regex.IsMatch(excelwrkhrsto, @"(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)");
                                    //
                                    if (matcht == false)
                                        LogErrors.Add("enter valid time format for Work Hours To at Row- " + count);
                                    else
                                    {
                                        DateTime time = DateTime.Parse(excelwrkhrsto);
                                        ClientLocationVModel.CL_WorkHoursTo = time.ToString("hh:mm tt");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("enter valid time format for Work Hours From at Row- " + count);
                                }
                            }

                            string excelintervalfrm = xlWorksheet.Cells[i, 29].Text.ToString().Trim();
                            if (string.IsNullOrEmpty(excelintervalfrm))
                                LogErrors.Add("Required Intervals from(in HH:MM format) at Row-" + count);
                            else if (!String.IsNullOrEmpty(excelintervalfrm))
                            {
                                try
                                {
                                    bool matcht = Regex.IsMatch(excelintervalfrm, @"(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)");
                                    //
                                    if (matcht == false)
                                        LogErrors.Add("enter valid time format for Intervals From at Row- " + count);
                                    else
                                    {
                                        DateTime time = DateTime.Parse(excelintervalfrm);
                                        ClientLocationVModel.CL_IntervalsFrom = time.ToString("hh:mm tt");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("enter valid time format for Intervals From at Row- " + count);
                                }
                            }

                            string excelintervalto = xlWorksheet.Cells[i, 30].Text.ToString().Trim();
                            if (string.IsNullOrEmpty(excelintervalto))
                                LogErrors.Add("Required Intervals to(in HH:MM format) at Row-" + count);
                            else if (!String.IsNullOrEmpty(excelintervalto))
                            {
                                try
                                {
                                    bool matcht = Regex.IsMatch(excelintervalto, @"(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)");
                                    //bool matcht = Regex.IsMatch(excelintervalto, @"^(?:[01][0-9]|2[0-3]):[0-5][0-9]$");

                                    if (matcht == false)
                                        LogErrors.Add("enter valid time format for Intervals To at Row- " + count);
                                    else
                                    {
                                        DateTime time = DateTime.Parse(excelintervalto);
                                        ClientLocationVModel.CL_IntervalsTo = time.ToString("hh:mm tt");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("enter valid time format for Intervals To at Row- " + count);
                                }
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                                LogErrors.Add("Required Work timings at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()) && xlWorksheet.Cells[i, 31].Text.Trim().GetType() == typeof(string))
                            {
                                string wrktimings = xlWorksheet.Cells[i, 31].Text.ToString().Trim();

                                if (wrktimings.ToUpper() == "SHIFT")
                                    ClientLocationVModel.CL_WorkTimings = "Shift";
                                else if (wrktimings.ToUpper() == "STANDARD")
                                    ClientLocationVModel.CL_WorkTimings = "Standard";
                                else
                                    LogErrors.Add("Enter valid work timings at Row- " + count);
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()) && xlWorksheet.Cells[i, 32].Text.Trim().GetType() == typeof(string))
                            {
                                string status = xlWorksheet.Cells[i, 32].Text.ToString().Trim();

                                if (status.ToUpper() == "ACTIVE" || status.ToUpper() == "A")
                                    ClientLocationVModel.CL_Status = "A";
                                else if (status.ToUpper() == "INACTIVE" || status.ToUpper() == "I")
                                    ClientLocationVModel.CL_Status = "I";
                                else
                                    LogErrors.Add("Enter valid status at Row- " + count);
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()) && xlWorksheet.Cells[i, 33].Text.Trim().GetType() == typeof(string))
                            {
                                if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                                {
                                    ClientLocationVModel.CL_LIN = null;
                                }
                                else
                                {
                                    bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 33].Text.ToString().Trim(), @"^[a-zA-Z0-9]+$");
                                    if (matched == false)
                                        LogErrors.Add("Enter valid Lin at Row- " + count);
                                    else
                                        ClientLocationVModel.CL_LIN = xlWorksheet.Cells[i, 33].Text.ToString().Trim();
                                }
                            }

                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_Municipality = null;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()))
                                    LogErrors.Add("Required Muncipality at Row-" + count);
                                else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()) && xlWorksheet.Cells[i, 34].Text.Trim().GetType() == typeof(string))
                                {
                                    bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 34].Text.ToString().Trim(), @"^[a-zA-Z ]+$");
                                    if (matched == false)
                                        LogErrors.Add("Enter valid Muncipality at Row- " + count);
                                    else
                                        ClientLocationVModel.CL_Municipality = xlWorksheet.Cells[i, 34].Text.ToString().Trim();
                                }
                            }




                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_Permission_MaintaingForms = null;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text.ToString().Trim()))
                                    LogErrors.Add("Required Permission for maintaining forms at Row-" + count);
                                else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text.ToString().Trim()) && xlWorksheet.Cells[i, 35].Text.Trim().GetType() == typeof(string))
                                {
                                    string mainfrms = xlWorksheet.Cells[i, 35].Text.ToString().Trim();
                                    if (mainfrms.ToUpper() == "YES" || mainfrms.ToUpper() == "Y")
                                        ClientLocationVModel.CL_Permission_MaintaingForms = "Y";
                                    else if (mainfrms.ToUpper() == "NO" || mainfrms.ToUpper() == "N")
                                        ClientLocationVModel.CL_Permission_MaintaingForms = "N";
                                    else
                                        LogErrors.Add("Enter either yes or no for permission to maintain forms at Row- " + count);
                                }
                            }

                            if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                            {
                                ClientLocationVModel.CL_RequirePowerforFines = null;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 36].Text.ToString().Trim()))
                                    LogErrors.Add("Required Require power to impose fines at Row-" + count);
                                else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 36].Text.ToString().Trim()) && xlWorksheet.Cells[i, 36].Text.Trim().GetType() == typeof(string))
                                {
                                    string powerfrms = xlWorksheet.Cells[i, 36].Text.ToString().Trim();
                                    if (powerfrms.ToUpper() == "YES" || powerfrms.ToUpper() == "Y")
                                        ClientLocationVModel.CL_RequirePowerforFines = "Y";
                                    else if (powerfrms.ToUpper() == "NO" || powerfrms.ToUpper() == "N")
                                        ClientLocationVModel.CL_RequirePowerforFines = "N";
                                    else
                                        LogErrors.Add("Enter either Yes or No for Require power to impose fines at Row- " + count);
                                }
                            }

                            if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text.ToString().Trim()))
                                LogErrors.Add("Required Business type forms at Row-" + count);
                            else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text.ToString().Trim()) && xlWorksheet.Cells[i, 37].Text.Trim().GetType() == typeof(string))
                            {
                                string businesstype = xlWorksheet.Cells[i, 37].Text.ToString().Trim();
                                if (businesstype.ToUpper() == "IT")
                                    ClientLocationVModel.CL_BusinessType = "IT";
                                else if (businesstype.ToUpper() == "ITES")
                                    ClientLocationVModel.CL_BusinessType = "ITES";
                                else if (businesstype.ToUpper() == "NON-IT")
                                    ClientLocationVModel.CL_BusinessType = "NON-IT";
                                else
                                    LogErrors.Add("Enter valid value for Business type at Row- " + count);
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 38].Text.ToString().Trim()) && xlWorksheet.Cells[i, 38].Text.Trim().GetType() == typeof(string))
                            {
                                if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                                {
                                    ClientLocationVModel.CL_CommencementDate = null;
                                }
                                else
                                {
                                    string effectdate = xlWorksheet.Cells[i, 38].Text.ToString().Trim();

                                    try
                                    {
                                        DateTime dteffectdate = DateTime.ParseExact(effectdate, "dd/MM/yyyy", null);
                                        ClientLocationVModel.CL_CommencementDate = dteffectdate.ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        LogErrors.Add("Enter valid Effective date in DD/MM/YYYY format at Row-" + count);
                                    }
                                }
                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 39].Text.ToString().Trim()) && xlWorksheet.Cells[i, 39].Text.Trim().GetType() == typeof(string))
                            {
                                if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                                {
                                    ClientLocationVModel.CL_ClasssificationofEstabilishment = null;
                                }
                                else
                                {
                                    bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 39].Text.ToString().Trim(), @"^[a-zA-Z ]+$");
                                    if (matched == false)
                                        LogErrors.Add("Enter valid Classsification of estabilishment at Row- " + count);
                                    else
                                        ClientLocationVModel.CL_ClasssificationofEstabilishment = xlWorksheet.Cells[i, 39].Text.ToString().Trim();
                                }
                            }

                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 40].Text.ToString().Trim()) && xlWorksheet.Cells[i, 40].Text.Trim().GetType() == typeof(string))
                            //{
                            //    ClientLocationVModel.CL_EmployerDesignation = xlWorksheet.Cells[i, 40].Text.ToString().Trim();
                            //}

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 40].Text.ToString().Trim()) && xlWorksheet.Cells[i, 40].Text.Trim().GetType() == typeof(string))
                            {
                                if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                                {
                                    ClientLocationVModel.CL_LicenceNo = null;
                                }
                                else
                                {
                                    bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 40].Text.ToString().Trim(), @"^[a-zA-Z0-9]+$");
                                    if (matched == false)
                                        LogErrors.Add("Enter valid Licence No at Row- " + count);
                                    else
                                    {
                                        ClientLocationVModel.CL_LicenceNo = xlWorksheet.Cells[i, 40].Text.ToString().Trim();
                                    }
                                }

                            }

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 41].Text.ToString().Trim()) && xlWorksheet.Cells[i, 41].Text.Trim().GetType() == typeof(string))
                            {
                                if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                                {
                                    ClientLocationVModel.CL_NICCode = null;
                                }
                                else
                                {
                                    bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 41].Text.ToString().Trim(), @"^[a-zA-z0-9]+$");
                                    if (matched == false)
                                        LogErrors.Add("Enter valid NIC Code at Row- " + count);
                                    else
                                        ClientLocationVModel.CL_NICCode = xlWorksheet.Cells[i, 41].Text.ToString().Trim();
                                }
                            }



                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 42].Text.ToString().Trim()) && xlWorksheet.Cells[i, 42].Text.Trim().GetType() == typeof(string))
                            {
                                if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                                {
                                    ClientLocationVModel.CL_SectionofAct = null;
                                }
                                else
                                {
                                    string section = xlWorksheet.Cells[i, 42].Text.ToString().Trim();

                                    if (section.ToUpper() == "2M(I)")
                                        ClientLocationVModel.CL_SectionofAct = "2m(i)";
                                    else if (section.ToUpper() == "2M(II)")
                                        ClientLocationVModel.CL_SectionofAct = "2m(ii)";
                                    else if (section.ToUpper() == "SECTION-85")
                                        ClientLocationVModel.CL_SectionofAct = "Section-85";
                                    else
                                        LogErrors.Add("Enter valid Section of Act at Row-" + count);
                                }
                            }
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 43].Text.ToString().Trim()) && xlWorksheet.Cells[i, 43].Text.Trim().GetType() == typeof(string))
                            {
                                if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                                {
                                    ClientLocationVModel.CL_District = null;
                                }
                                else
                                {
                                    bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 43].Text.ToString().Trim(), @"^[a-zA-Z]+$");
                                    if (matched == false)
                                        LogErrors.Add("Enter valid District. It should be Characters only, Check at at Row- " + count);
                                    else
                                        ClientLocationVModel.CL_District = xlWorksheet.Cells[i, 43].Text.ToString().Trim();
                                }
                            }
                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 44].Text.ToString().Trim()) && xlWorksheet.Cells[i, 44].Text.Trim().GetType() == typeof(string))
                            //{
                            //    bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 44].Text.ToString().Trim(), @"^[a-zA-z0-9]+$");
                            //    if (matched == false)
                            //        LogErrors.Add("Enter valid District at Row- " + count);
                            //    else
                            //        ClientLocationVModel.CL_District = xlWorksheet.Cells[i, 44].Text.ToString().Trim();
                            //}

                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 45].Text.ToString().Trim()) && xlWorksheet.Cells[i, 45].Text.Trim().GetType() == typeof(string))
                            //{
                            //    ClientLocationVModel.CL_Pincode = xlWorksheet.Cells[i, 45].Text.ToString().Trim();
                            //    string pincode = xlWorksheet.Cells[i, 45].Text.ToString().Trim();
                            //    if (pincode.Length != 6)
                            //        LogErrors.Add("Enter valid Pincode no at Row-" + count);
                            //    else
                            //    {
                            //        try
                            //        {
                            //            bool valid = pincode.All(char.IsDigit);
                            //            ClientLocationVModel.CL_Pincode = pincode;
                            //        }
                            //        catch (Exception ex)
                            //        {
                            //            LogErrors.Add("Enter valid Pincode no at Row-" + count);
                            //        }
                            //    }
                            //}

                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 44].Text.ToString().Trim()) && xlWorksheet.Cells[i, 44].Text.Trim().GetType() == typeof(string))
                            {
                                if (ClientLocationVModel.CM_EstablishmentType == "SEA")
                                {
                                    ClientLocationVModel.CL_Jurisdiction = null;
                                }
                                else
                                {
                                    bool matched = Regex.IsMatch(xlWorksheet.Cells[i, 44].Text.ToString().Trim(), @"^[a-zA-z ]+$");
                                    if (matched == false)
                                        LogErrors.Add("Enter valid Jurisdiction at Row- " + count);
                                    else
                                        ClientLocationVModel.CL_Jurisdiction = xlWorksheet.Cells[i, 44].Text.ToString().Trim();
                                }
                            }
                            //ADD NEW FIELDS GAP
                            string BranchEndDate = xlWorksheet.Cells[i, 45].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(BranchEndDate))
                            {
                                try
                                {
                                    DateTime dtBranchEndDate = DateTime.ParseExact(BranchEndDate, "dd/MM/yyyy", null);
                                    ClientLocationVModel.CL_BranchEndDate = dtBranchEndDate.ToString();
                                }
                                catch (Exception ex)
                                {
                                    LogErrors.Add("Enter valid RC valid (from) date in DD/MM/YYYY format at Row-" + count);
                                }
                            }
                            string TradeLicence = xlWorksheet.Cells[i, 46].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(TradeLicence))
                            {
                                if (TradeLicence.ToUpper() == "YES" || TradeLicence.ToUpper() == "Y")
                                    ClientLocationVModel.CL_TradeLicenceApplicability = "Y";
                                else if (TradeLicence.ToUpper() == "NO" || TradeLicence.ToUpper() == "N")
                                    ClientLocationVModel.CL_TradeLicenceApplicability = "N";
                                else
                                    LogErrors.Add("Enter either Yes or No for Require Trade Licence at Row- " + count);
                            }
                            string EmployerDesignation = xlWorksheet.Cells[i, 47].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(EmployerDesignation))
                            {
                                bool Designation = Regex.IsMatch(EmployerDesignation, @"^[a-zA-Z ]+$");
                                if (Designation)
                                    ClientLocationVModel.CL_EmployerDesignation = EmployerDesignation;
                                else
                                    LogErrors.Add("Invalid Designation-" + EmployerDesignation + ", it should be Characters only, Check at Row-" + count);
                            }
                            string EDLIExcemption = xlWorksheet.Cells[i, 48].Text.ToString().Trim();
                            if (!String.IsNullOrEmpty(EDLIExcemption))
                            {
                                if (EDLIExcemption.ToUpper() == "YES" || EDLIExcemption.ToUpper() == "Y")
                                    ClientLocationVModel.CL_EDLI_Excemption = "Y";
                                else if (EDLIExcemption.ToUpper() == "NO" || EDLIExcemption.ToUpper() == "N")
                                    ClientLocationVModel.CL_EDLI_Excemption = "N";
                                else
                                    LogErrors.Add("Enter either Yes or No for Require EDLI Excemption at Row- " + count);
                            }
                            int customerID = RLCS_ClientsManagement.GetCustomerIDByClientID(ClientLocationVModel.CM_ClientID);
                            int? ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(customerID));
                            if (ServiceProviderID == 94)
                            {
                                int distributorID = RLCS_ClientsManagement.GetParentIDforCustomer(customerID);
                                string LocationAnchor = xlWorksheet.Cells[i, 49].Text.ToString().Trim();
                                if (String.IsNullOrEmpty(LocationAnchor))
                                    LogErrors.Add("Required LocationAnchor at Row-" + count);
                                else
                                {
                                    bool valid = RLCS_ClientsManagement.ValidateAnchor(distributorID, customerID, LocationAnchor);
                                    if (valid == true)
                                    {
                                        ClientLocationVModel.CL_LocationAnchor = Convert.ToString(RLCS_ClientsManagement.GetAllAnchorID(distributorID, customerID, LocationAnchor));
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Name for Location Anchor.");
                                    }
                                }
                            }
                            if (LogErrors.Count <= 0 && ModelState.IsValid)
                            {
                                RLCS_CustomerBranch_ClientsLocation_Mapping rlcsCustomerBranch = new RLCS_CustomerBranch_ClientsLocation_Mapping();

                                TinyMapper.Bind<ClientLocationVModel, RLCS_CustomerBranch_ClientsLocation_Mapping>(
                                     config =>
                                     {
                                         config.Ignore(source => source.CL_CommencementDate);
                                         config.Ignore(source => source.CL_RCValidFrom);
                                         config.Ignore(source => source.CL_RCValidTo);
                                     });

                                int AVACOM_CustBranchID = 0;
                                rlcsCustomerBranch = TinyMapper.Map<RLCS_CustomerBranch_ClientsLocation_Mapping>(ClientLocationVModel);
                                rlcsCustomerBranch.CO_CorporateID = ClientLocationVModel.CorporateID;

                                if (rlcsCustomerBranch.AVACOM_BranchID != null)
                                    AVACOM_CustBranchID = (int)rlcsCustomerBranch.AVACOM_BranchID;

                                if (AVACOM_CustBranchID != 0)
                                    rlcsCustomerBranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetCustomerIDByCustomerBranchID(AVACOM_CustBranchID);
                                else
                                    rlcsCustomerBranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetAVACOMCustIDByCorpID(rlcsCustomerBranch.CO_CorporateID);

                                //Customerbranch.AVACOM_CustomerID = RLCS_ClientsManagement.GetAVACOMCustIDByCorpID(Customerbranch.CO_CorporateID);
                                ////GG ADD ComplianceProductType 13March2020
                                if (!string.IsNullOrEmpty(Convert.ToString(rlcsCustomerBranch.AVACOM_CustomerID)))
                                {
                                    ComplianceProdType = RLCS_Master_Management.GetComplianceProductType(Convert.ToInt32(rlcsCustomerBranch.AVACOM_CustomerID));
                                    if (ComplianceProdType > 0)
                                    {
                                        rlcsCustomerBranch.CM_IsAventisClientOrBranch = ComplianceProdType;
                                    }
                                }
                                ////END
                                rlcsCustomerBranch.BranchType = "B";
                                rlcsCustomerBranch.CM_ClientName = ClientLocationVModel.AVACOM_BranchName;
                                rlcsCustomerBranch.CM_Status = ClientLocationVModel.CL_Status;
                                rlcsCustomerBranch.CM_Pincode = ClientLocationVModel.CM_City;
                                rlcsCustomerBranch.CL_Pincode = ClientLocationVModel.CM_City;
                                rlcsCustomerBranch.CL_SectionofAct = ClientLocationVModel.CL_SectionofAct;
                                rlcsCustomerBranch.CL_PermissionMaintainingForms = ClientLocationVModel.CL_Permission_MaintaingForms;
                                rlcsCustomerBranch.CL_weekoffDay = ClientLocationVModel.CL_WeekoffDay;
                                rlcsCustomerBranch.CL_NatureofBusiness = ClientLocationVModel.CL_NatureOfBusiness;
                                rlcsCustomerBranch.CL_Juridiction = ClientLocationVModel.CL_Jurisdiction;
                                rlcsCustomerBranch.CL_ClassificationofEstablishment = ClientLocationVModel.CL_ClasssificationofEstabilishment;

                                rlcsCustomerBranch.CL_CompPhoneNo = Convert.ToInt64(ClientLocationVModel.CL_CompPhoneNo);
                                rlcsCustomerBranch.CL_RCNO = ClientLocationVModel.CL_RCNo;
                                rlcsCustomerBranch.CL_SectionofAct = ClientLocationVModel.CL_SectionofAct;
                                if (ClientLocationVModel.CL_RCValidFrom != null)
                                {
                                    rlcsCustomerBranch.CL_RC_ValidFrom = DateTime.Parse(ClientLocationVModel.CL_RCValidFrom);
                                }
                                if (ClientLocationVModel.CL_RCValidTo != null)
                                {
                                    rlcsCustomerBranch.CL_RC_ValidTo = DateTime.Parse(ClientLocationVModel.CL_RCValidTo);
                                }
                                if (ClientLocationVModel.CL_CommencementDate != null)
                                {
                                    rlcsCustomerBranch.CL_CommencementDate = DateTime.Parse(ClientLocationVModel.CL_CommencementDate);
                                }

                                //94
                                if (ClientLocationVModel.ServiceProviderID == 94)
                                {
                                    string branchCode = GenerateBranchCode(ClientLocationVModel.CM_ClientID, ClientLocationVModel.CM_State, ClientLocationVModel.CM_EstablishmentType);
                                    if (branchCode != null)
                                        rlcsCustomerBranch.CL_BranchCode = branchCode;
                                        rlcsCustomerBranch.CL_LocationAnchor = ClientLocationVModel.CL_LocationAnchor;

                                }
                                //End
                                //EDLI
                                rlcsCustomerBranch.CL_EDLI_Excemption = ClientLocationVModel.CL_EDLI_Excemption;
                                //END
                                saveSuccess = RLCS_ClientsManagement.CreateUpdate_RLCS_ClientLocationDetails(rlcsCustomerBranch);

                                if (saveSuccess)
                                {
                                    Model_ClientLocation ClientLocation = new Model_ClientLocation();
                                    ClientLocation.ClientId = rlcsCustomerBranch.CM_ClientID;
                                    ClientLocation.StateId = rlcsCustomerBranch.CM_State;
                                    ClientLocation.Location = rlcsCustomerBranch.CM_City;
                                    ClientLocation.ActApplicablity = rlcsCustomerBranch.CM_ActType;
                                    ClientLocation.Branch = rlcsCustomerBranch.AVACOM_BranchName;
                                    ClientLocation.BranchAddress = rlcsCustomerBranch.CM_Address;
                                    ClientLocation.LWFState = rlcsCustomerBranch.CL_LWF_State;
                                    ClientLocation.PTState = rlcsCustomerBranch.CL_PT_State;
                                    ClientLocation.PFCode = rlcsCustomerBranch.CL_PF_Code;
                                    ClientLocation.ESICCode = rlcsCustomerBranch.CL_ESIC_Code;
                                    ClientLocation.OfficeType = rlcsCustomerBranch.CL_OfficeType;
                                    ClientLocation.EstablishmentType = rlcsCustomerBranch.CM_EstablishmentType;
                                    ClientLocation.EmployerName = rlcsCustomerBranch.CL_EmployerName;
                                    ClientLocation.EmployerAddress = rlcsCustomerBranch.CL_EmployerAddress;
                                    ClientLocation.ManagerAddress = rlcsCustomerBranch.CL_ManagerAddress;
                                    ClientLocation.ManagerName = rlcsCustomerBranch.CL_ManagerName;
                                    ClientLocation.CompanyPhoneNumber = rlcsCustomerBranch.CL_CompPhoneNo.HasValue ? rlcsCustomerBranch.CL_CompPhoneNo.Value : 0;
                                    ClientLocation.HRContactPerson = rlcsCustomerBranch.CL_HRContactPerson;
                                    ClientLocation.HRPhoneNumber = rlcsCustomerBranch.CL_HRPhNo.HasValue ? rlcsCustomerBranch.CL_HRPhNo.Value : 0;
                                    ClientLocation.HREmail = rlcsCustomerBranch.CL_HRMailID;
                                    ClientLocation.HRFirstLevelEmail = rlcsCustomerBranch.CL_HR1stLevelMail;
                                    ClientLocation.HRFirstLevelPhoneNumber = rlcsCustomerBranch.CL_HR1stLevelPhNo;
                                    ClientLocation.RCNumber = rlcsCustomerBranch.CL_RCNO;
                                    ClientLocation.RCValidFrom = rlcsCustomerBranch.CL_RC_ValidFrom;
                                    ClientLocation.RCValidTo = rlcsCustomerBranch.CL_RC_ValidTo;
                                    ClientLocation.NatureOfBusiness = rlcsCustomerBranch.CL_NatureofBusiness;
                                    ClientLocation.WeekOffDays = rlcsCustomerBranch.CL_weekoffDay;
                                    ClientLocation.WorkHoursFrom = rlcsCustomerBranch.CL_WorkHoursFrom;
                                    ClientLocation.WorkHoursTo = rlcsCustomerBranch.CL_WorkHoursTo;
                                    ClientLocation.WorkTimings = rlcsCustomerBranch.CL_WorkTimings;
                                    ClientLocation.IntervalFrom = rlcsCustomerBranch.CL_IntervalsFrom;
                                    ClientLocation.IntervalTo = rlcsCustomerBranch.CL_IntervalsTo;
                                    //ClientLocation.LocationAnchor = "";
                                    ClientLocation.Status = rlcsCustomerBranch.CM_Status;
                                    ClientLocation.Municipality = rlcsCustomerBranch.CL_Municipality;
                                    ClientLocation.PermissonMaintainingForms = rlcsCustomerBranch.CL_PermissionMaintainingForms;
                                    ClientLocation.RequirePowerforFines = rlcsCustomerBranch.CL_RequirePowerforFines;
                                    ClientLocation.Businesstype = rlcsCustomerBranch.CL_BusinessType;
                                    ClientLocation.LIN = rlcsCustomerBranch.CL_LIN;
                                    ClientLocation.EffectiveDate = rlcsCustomerBranch.CL_CommencementDate;
                                    ClientLocation.ClassificationofEstablishment = rlcsCustomerBranch.CL_ClassificationofEstablishment;
                                    ClientLocation.EmployerDesignation = "";
                                    ClientLocation.LicenceNumber = rlcsCustomerBranch.CL_LicenceNo;
                                    ClientLocation.NICNumber = rlcsCustomerBranch.CL_NICCode;
                                    ClientLocation.SectionOfAct = rlcsCustomerBranch.CL_SectionofAct;
                                    ClientLocation.District = rlcsCustomerBranch.CL_District;
                                    ClientLocation.Pincode = rlcsCustomerBranch.CL_Pincode;
                                    ClientLocation.Jurisdiction = rlcsCustomerBranch.CL_Juridiction;
                                    ClientLocation.CL_IsAventisBranch = rlcsCustomerBranch.CM_IsAventisClientOrBranch;
                                    ClientLocation.CreatedBy = "Avantis";
                                    ClientLocation.ModifiedBy = "Avantis";
                                    ClientLocation.CL_BranchCode = rlcsCustomerBranch.CL_BranchCode;
                                    ClientLocation.CL_BranchEndDate = rlcsCustomerBranch.CL_BranchEndDate;
                                    ClientLocation.CL_TradeLicenceApplicability = rlcsCustomerBranch.CL_TradeLicenceApplicability;
                                    ClientLocation.CL_EmployerDesignation = rlcsCustomerBranch.CL_EmployerDesignation;
                                    ClientLocation.CL_LocationAnchor = rlcsCustomerBranch.CL_LocationAnchor;
                                    ClientLocation.CL_EDLI_Excemption = rlcsCustomerBranch.CL_EDLI_Excemption;
                                    bool apiCallSuccess = ClientLocationApiCall(ClientLocation);

                                    if (apiCallSuccess)
                                    {
                                        ClientLocationVModel.Message = true;
                                        RLCS_ClientsManagement.Update_ProcessedStatus_ClientLocationDetail(ClientLocationVModel.AVACOM_BranchID, ClientLocationVModel.CM_ClientID, apiCallSuccess);
                                    }
                                }
                            }
                            else
                            {
                                string path = "";
                                ClientLocationVModel.FileName = RLCS_WriteLog.WriteLog(LogErrors, fileName, out path);
                                ClientLocationVModel.ExcelErrors = true;
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        string path = "";
                        ClientLocationVModel.FileName = RLCS_WriteLog.WriteLog(LogErrors, fileName, out path);
                        ClientLocationVModel.ExcelErrors = true;
                    }
                }
                else
                {
                    ClientLocationVModel.SheetNameError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                ClientLocationVModel.Exception = true;
            }
            return ClientLocationVModel;
        }

        public bool ClientBasicApiCall(jsonClientDetails jsonClientDetails)
        {
            bool call = false;
            try
            {
                HttpClient client = new HttpClient();
                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                client.BaseAddress = new Uri(rlcsAPIURL);
                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                JArray paramList = new JArray();
                jsonClientDetails.ModifiedBy = "Avantis";
                jsonClientDetails.CreatedBy = "Avantis";

                var response = client.PostAsJsonAsync<jsonClientDetails>("AventisIntegration/CreateUpdateClientBasicDetail", jsonClientDetails).Result;

                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool ClientLocationApiCall(Model_ClientLocation ClientLocation)
        {
            try
            {
                HttpClient client = new HttpClient();
                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                client.BaseAddress = new Uri(rlcsAPIURL);
                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                JArray paramList = new JArray();

                paramList.Add(JsonConvert.SerializeObject(ClientLocation));
                var json = new JavaScriptSerializer().Serialize(ClientLocation);
                var response = client.PostAsJsonAsync<Model_ClientLocation>("AventisIntegration/CreateUpdateClientLocationMaster", ClientLocation).Result;

                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region EmployeeMaster
        private UploadEmployeeDocumentVModel ProcessEmployeeMasterDetails(ExcelPackage xlWorkbook, int CustomerID)
        {
            bool emp = false;
            long customer = 0;
            bool update = false;

            EmployeeMasterVModel EmployeeMasterVModel = new EmployeeMasterVModel();
            ClientDetailsVModel ClientDetailsVModel = new ClientDetailsVModel();
            UploadEmployeeDocumentVModel UploadDocumentVModel = new UploadEmployeeDocumentVModel();

            try
            {
                bool Sucess = false;
                if (TempData["Update"] != null)
                {
                    update = (bool)TempData["Update"];
                }

                if (!string.IsNullOrEmpty(Convert.ToString(CustomerID)))
                {
                    customer = Convert.ToInt64(CustomerID);
                    TempData["CustID"] = Convert.ToString(CustomerID);
                }
                else
                {
                    customer = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Employee_Master"];
                TempData["Employee_Master"] = (ExcelWorksheet)xlWorksheet;

                if (update)
                    EmployeeMasterVModel = ValidateEmployeeExcel((ExcelWorksheet)xlWorksheet, customer, true);

                else
                    EmployeeMasterVModel = ValidateEmployeeExcel((ExcelWorksheet)xlWorksheet, customer, false);

                UploadDocumentVModel = EmployeeMasterVModel.UploadEmployee;

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                UploadDocumentVModel.ServerError = true;
            }
            return UploadDocumentVModel;
        }

        [HttpGet]
        public ActionResult CreateEmplodyeeMaster(string EmpID, string CustID, string Client)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                EmployeeMasterVModel Employee = new EmployeeMasterVModel();
                RLCS_Employee_Master RLCS_Employee = new RLCS_Employee_Master();
                List<RLCS_State_Mapping> StateList = new List<RLCS_State_Mapping>();
                List<RLCS_Country_Mapping> countryList = new List<RLCS_Country_Mapping>();
                List<RLCS_State_Mapping> PTStateList = new List<RLCS_State_Mapping>();
                List<Proc_ESIC_SECTOR_MASTERDATA_Result> SectorList = new List<Proc_ESIC_SECTOR_MASTERDATA_Result>();
                List<Proc_ESIC_GROUP_MASTERDATA_Result> JobCategoryList = new List<Proc_ESIC_GROUP_MASTERDATA_Result>();
                List<Proc_ESIC_IndustryTypeDATA_Result> IndustryTypeList = new List<Proc_ESIC_IndustryTypeDATA_Result>();
                DateTime dt = new DateTime();

                try
                {
                    if (!String.IsNullOrEmpty(EmpID) && !String.IsNullOrEmpty(CustID) && !String.IsNullOrEmpty(Client))
                    {
                        RLCS_Employee = RLCS_ClientsManagement.GetEmployeeDetails(EmpID, Convert.ToInt32(CustID), Client);
                        if (RLCS_Employee != null)
                        {
                            TinyMapper.Bind<RLCS_Employee_Master, EmployeeMasterVModel>();
                            Employee = TinyMapper.Map<EmployeeMasterVModel>(RLCS_Employee);
                            Employee.update = true;
                            Employee.EM_State = RLCS_Employee.EM_State;
                            Employee.AVACOM_BranchID = RLCS_Employee.AVACOM_BranchID;
                            Employee.EM_Emailid = RLCS_Employee.EM_Emailid;
                            Employee.EM_Relationship = RLCS_Employee.EM_Relationship;
                            Employee.EM_International_workers = RLCS_Employee.EM_International_workers;
                            Employee.EM_Department = RLCS_Employee.EM_Department;
                            Employee.EM_Designation = RLCS_Employee.EM_Designation;
                            Employee.EM_Nationality = RLCS_Employee.EM_Nationality;   //NEW
                            Employee.EM_MobileNo = RLCS_Employee.EM_MobileNo;
                            Employee.EM_BankName = RLCS_Employee.EM_BankName;
                            Employee.EM_PF_Applicability = RLCS_Employee.EM_PF_Applicability;
                            Employee.EM_PF_Capping_Applicability = RLCS_Employee.EM_PF_Capping_Applicability;
                            Employee.EM_EPFO_Aadhar_Upload = RLCS_Employee.EM_EPFO_Aadhar_Upload;
                            Employee.EM_EPFO_Bank_Ac_Upload = RLCS_Employee.EM_EPFO_Bank_Ac_Upload;
                            Employee.EM_EPFO_PAN_Upload = RLCS_Employee.EM_EPFO_PAN_Upload;
                            Employee.EM_EPFO_Bank_Ac_Upload = RLCS_Employee.EM_EPFO_Bank_Ac_Upload;
                            Employee.EM_ModeofTransport = RLCS_Employee.EM_ModeofTransport;
                            if (RLCS_Employee.EM_PayrollYear.HasValue)
                                Employee.EM_PayrollYear = (int)RLCS_Employee.EM_PayrollYear;
                            if (RLCS_Employee.EM_PayrollMonth.HasValue)
                                Employee.EM_PayrollMonth = (int)RLCS_Employee.EM_PayrollMonth;
                            Employee.EM_ModeofTransport = RLCS_Employee.EM_ModeofTransport;
                            if (RLCS_Employee.EM_ESI_Out_of_Courage_Month.HasValue)
                                Employee.EM_ESI_Out_of_Courage_Month = (int)RLCS_Employee.EM_ESI_Out_of_Courage_Month;
                            if (RLCS_Employee.EM_ESI_Out_of_Courage_Year.HasValue)
                                Employee.EM_ESI_Out_of_Courage_Year = (int)RLCS_Employee.EM_ESI_Out_of_Courage_Year;
                            Employee.EM_MaritalStatus = RLCS_Employee.EM_MaritalStatus;
                            Employee.EM_Markof_Identification = RLCS_Employee.EM_Markof_Identification;
                            Employee.EM_PT_Applicability = RLCS_Employee.EM_PT_Applicability;
                            Employee.EM_PT_State = RLCS_Employee.EM_PT_State;
                            Employee.EM_NoOf_Certificate = RLCS_Employee.EM_NoOf_Certificate;
                            Employee.EM_TokenNo = RLCS_Employee.EM_TokenNo;
                            Employee.EM_PhysicallyChallenged = RLCS_Employee.EM_PhysicallyChallenged;
                            Employee.EM_Letter_Of_Group = RLCS_Employee.EM_Letter_Of_Group;
                            Employee.EM_NumberandDateOfExemptingOrder = RLCS_Employee.EM_NumberandDateOfExemptingOrder;
                            Employee.EM_ParticularsOfTransferFromOneGroupToAnother = RLCS_Employee.EM_ParticularsOfTransferFromOneGroupToAnother;
                            Employee.EM_Markof_Identification = RLCS_Employee.EM_Markof_Identification;
                            Employee.EM_Remarks = RLCS_Employee.EM_Remarks;
                            Employee.EM_Place_of_Employment = RLCS_Employee.EM_Place_of_Employment;
                            Employee.EM_PermanentAddress = RLCS_Employee.EM_PermanentAddress;
                            Employee.EM_Placeof_work = RLCS_Employee.EM_Placeof_work;
                            Employee.EM_NoOf_Certificate = RLCS_Employee.EM_NoOf_Certificate;
                            Employee.EM_ESICNO = RLCS_Employee.EM_ESICNO;
                            Employee.EM_Place_of_Employment = RLCS_Employee.EM_Place_of_Employment;
                            Employee.EM_SecurityProvided = RLCS_Employee.EM_SecurityProvided;
                            Employee.EM_PMRPY = RLCS_Employee.EM_PMRPY;
                            Employee.EM_Status = RLCS_Employee.EM_Status;
                            Employee.EM_Branch = RLCS_Employee.EM_Branch;
                            Employee.EM_PassportIssued_Country = RLCS_Employee.EM_PassportIssued_Country;
                            Employee.EM_EPS_Applicabilty = RLCS_Employee.EM_EPS_Applicabilty;
                            Employee.EM_Client_ESI_Number = RLCS_Employee.EM_Client_ESI_Number;
                            Employee.EM_Client_PT_State = RLCS_Employee.EM_Client_PT_State;

                            Employee.EM_IsLwf_Exempted = RLCS_Employee.EM_IsLwf_Exempted;
                            Employee.EM_ExemptedSEA_Act = RLCS_Employee.EM_ExemptedSEA_Act;
                            Employee.EM_VPF_Applicability = RLCS_Employee.EM_VPF_Applicability;
                            Employee.EM_VPF_Type = RLCS_Employee.EM_VPF_Type;
                            Employee.EM_VPF_Value = RLCS_Employee.EM_VPF_Value;
                            Employee.EM_Sectorid = RLCS_Employee.EM_Sectorid;
                            Employee.EM_JobCategory = RLCS_Employee.EM_JobCategory;
                            Employee.EM_IndustryType = RLCS_Employee.EM_IndustryType;
                            TempData["IsCheckEmp"] = false;
                            TempData["EmpID"] = EmpID;
                            if (RLCS_Employee.EM_DOB != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_DOB);
                                Employee.EM_DOB = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (RLCS_Employee.EM_DOL != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_DOL);
                                Employee.EM_DOL = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                            if (RLCS_Employee.EM_DOJ != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_DOJ);
                                Employee.EM_DOJ = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (RLCS_Employee.EM_DOL != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_DOL);
                                Employee.EM_DOL = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (RLCS_Employee.EM_Passport_Valid_From != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_Passport_Valid_From);
                                Employee.EM_Passport_Valid_From = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (RLCS_Employee.EM_Passport_Valid_Upto != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_Passport_Valid_Upto);
                                Employee.EM_Passport_Valid_Upto = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (RLCS_Employee.EM_ChangeEffective_from != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_ChangeEffective_from);
                                Employee.EM_ChangeEffective_from = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (RLCS_Employee.EM_Training_Date != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_Training_Date);
                                Employee.EM_Training_Date = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (RLCS_Employee.EM_NoOf_Certificate_Date != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_NoOf_Certificate_Date);
                                Employee.EM_NoOf_Certificate_Date = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            if (RLCS_Employee.EM_DateWhenClothesGiven != null)
                            {
                                dt = Convert.ToDateTime(RLCS_Employee.EM_DateWhenClothesGiven);
                                Employee.EM_DateWhenClothesGiven = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                        string CDate = RLCS_ClientsManagement.GetCommencementDate(Client);
                        Employee.EM_ClientDateOfCommencement = CDate;
                    }
                    else
                    {
                        TempData["IsCheckEmp"] = null;
                    }
                    if (!string.IsNullOrEmpty(CustID))
                    {
                        Employee.CustId = Convert.ToInt32(CustID);
                    }
                    else
                    {
                        if (Session["CustID"] != null)
                        {
                            if (Convert.ToInt32(Session["CustID"]) > 0)
                                Employee.CustId = Convert.ToInt32(Session["CustID"]);
                            else
                            {
                                Employee.CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            }
                        }
                        else
                        {
                            Employee.CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                    }

                    Employee.ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
                    //Employee.ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
                    // 
                    Employee.UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                    Employee.Path1 = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                    StateList = RLCS_ClientsManagement.GetAllStates();
                    PTStateList = RLCS_ClientsManagement.GetAllPTStates();
                    countryList = RLCS_ClientsManagement.GetAllCountry();
                    if (StateList != null && StateList.Count > 0)
                    {
                        TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                        Employee.States = TinyMapper.Map<List<StateVModel>>(StateList);
                    }
                    if (PTStateList != null && PTStateList.Count > 0)
                    {
                        TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                        Employee.PTStates = TinyMapper.Map<List<StateVModel>>(PTStateList);
                    }
                    if (countryList != null && countryList.Count > 0)
                    {
                        TinyMapper.Bind<List<Country>, List<CountryVModel>>();
                        Employee.Countries = TinyMapper.Map<List<CountryVModel>>(countryList);
                    }
                    Employee.ServiceProviderID = CustomerManagement.GetServiceProviderID(Convert.ToInt32(CustID));
                    if (Employee.ServiceProviderID== 94)
                    {
                        SectorList = RLCS_ClientsManagement.GetSector(Employee.EM_State);
                        JobCategoryList = RLCS_ClientsManagement.GetJobCategory(Employee.EM_State, Employee.EM_Sectorid);
                        IndustryTypeList = RLCS_ClientsManagement.GetIndustryType(Employee.EM_State, Employee.EM_Sectorid, Employee.EM_JobCategory);
                        //VMSector
                        if (SectorList != null && SectorList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_SECTOR_MASTERDATA_Result>, List<SectorVModel>>();
                            Employee.VMSector = TinyMapper.Map<List<SectorVModel>>(SectorList);
                        }
                        //VMJobCategory
                        if (JobCategoryList != null && JobCategoryList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_GROUP_MASTERDATA_Result>, List<JobCategoryVModel>>();
                            Employee.VMJobCategory = TinyMapper.Map<List<JobCategoryVModel>>(JobCategoryList);
                        }
                        //VMIndustryType
                        if (IndustryTypeList != null && IndustryTypeList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_IndustryTypeDATA_Result>, List<IndustryTypeVModel>>();
                            Employee.VMIndustryType = TinyMapper.Map<List<IndustryTypeVModel>>(IndustryTypeList);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    //UploadDocumentVModel.ServerError = true;
                }
                return View(Employee);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }

        }

        [HttpPost]
        public ActionResult CreateEmplodyeeMaster(EmployeeMasterVModel Employee)
        {
            RLCS_Employee_Master employeeMaster = new RLCS_Employee_Master();
            RLCS_Employee_Master_Transfer employeeTransfer = new RLCS_Employee_Master_Transfer();

            EmployeeUploadMasterVModel EmployeeUploadMasterVModel = new EmployeeUploadMasterVModel();
            long customer = 0;
            EmployeeSaveorUpdateBulkUploadModel bulkUpload = new EmployeeSaveorUpdateBulkUploadModel();
            RLCS_CustomerBranch_ClientsLocation_Mapping Branch = null;
            EmployeeUpdateSingleUploadModel EmployeeSaveorUpdateSingleUploadModel = new EmployeeUpdateSingleUploadModel();
            List<RLCS_State_Mapping> StateList = new List<RLCS_State_Mapping>();
            List<RLCS_Country_Mapping> countryList = new List<RLCS_Country_Mapping>();
            List<RLCS_State_Mapping> PTStateList = new List<RLCS_State_Mapping>();
            List<Proc_ESIC_SECTOR_MASTERDATA_Result> SectorList = new List<Proc_ESIC_SECTOR_MASTERDATA_Result>();
            List<Proc_ESIC_GROUP_MASTERDATA_Result> JobCategoryList = new List<Proc_ESIC_GROUP_MASTERDATA_Result>();
            List<Proc_ESIC_IndustryTypeDATA_Result> IndustryTypeList = new List<Proc_ESIC_IndustryTypeDATA_Result>();
            try
            {
                if (ModelState.IsValid)
                {
                    if (Session["CustID"] != null)
                    {
                        if (Convert.ToInt32(Session["CustID"]) > 0)
                            Employee.CustId = Convert.ToInt32(Session["CustID"]);
                        else
                        {
                            Employee.CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                    }
                    else
                    {
                        customer = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }

                    Branch = RLCS_ClientsManagement.GetStateLocationByBranch(Employee.AVACOM_BranchID);

                    if (Branch != null)
                    {
                        Employee.EM_State = Branch.CM_State;
                        Employee.EM_Location = Branch.CM_City;
                        Employee.EM_ClientID = Branch.CM_ClientID;
                    }

                    TinyMapper.Bind<EmployeeMasterVModel, RLCS_Employee_Master>(config =>
                    {
                        config.Ignore(source => source.EM_DOB);
                        config.Ignore(source => source.EM_DOJ);
                        config.Ignore(source => source.EM_DOL);
                        config.Ignore(source => source.EM_NoOf_Certificate_Date);
                        config.Ignore(source => source.EM_Training_Date);
                        config.Ignore(source => source.EM_Passport_Valid_From);
                        config.Ignore(source => source.EM_Passport_Valid_Upto);
                        config.Ignore(source => source.EM_NumberandDateOfExemptingOrder);
                        config.Ignore(source => source.EM_ChangeEffective_from);
                        config.Ignore(source => source.EM_DateWhenClothesGiven);
                    });

                    employeeMaster = TinyMapper.Map<RLCS_Employee_Master>(Employee);

                    employeeMaster.AVACOM_CustomerID = Convert.ToInt32(Employee.CustId);
                    employeeMaster.EM_Location = Employee.EM_Location;
                    employeeMaster.EM_PF_Applicability = Employee.EM_PF_Applicability;
                    employeeMaster.EM_PF_Capping_Applicability = Employee.EM_PF_Capping_Applicability;
                    employeeMaster.EM_SecurityProvided = Employee.EM_SecurityProvided;
                    employeeMaster.EM_MaritalStatus = Employee.EM_MaritalStatus;
                    employeeMaster.EM_Nationality = Employee.EM_Nationality;
                    employeeMaster.EM_MobileNo = Employee.EM_MobileNo;
                    employeeMaster.EM_Designation = Employee.EM_Designation;
                    employeeMaster.EM_Location = Employee.EM_Location;
                    employeeMaster.EM_PhysicallyChallenged = Employee.EM_PhysicallyChallenged;
                    employeeMaster.EM_PT_State = Employee.EM_PT_State;
                    employeeMaster.EM_PT_Applicability = Employee.EM_PT_Applicability;
                    employeeMaster.EM_PermanentAddress = Employee.EM_PermanentAddress;
                    employeeMaster.EM_NoOf_Certificate = Employee.EM_NoOf_Certificate;
                    employeeMaster.EM_Status = Employee.EM_Status;
                    employeeMaster.EM_EPS_Applicabilty = Employee.EM_EPS_Applicabilty;
                    employeeMaster.EM_Client_ESI_Number = Employee.EM_Client_ESI_Number;
                    if (string.IsNullOrEmpty(Employee.EM_Client_PT_State))
                    {
                        employeeMaster.EM_Client_PT_State = Employee.EM_PT_State;
                    }
                    else
                    {
                        employeeMaster.EM_Client_PT_State = Employee.EM_Client_PT_State;
                    }

                    employeeMaster.EM_Relay_Assigned = Employee.EM_Relay_Assigned;
                    employeeMaster.EM_ExistReasonCode = Employee.EM_ExistReasonCode;
                    employeeMaster.EM_Department = Employee.EM_Department;
                    employeeMaster.EM_Employmenttype = Employee.EM_Employmenttype;
                    employeeMaster.EM_PMRPY = Employee.EM_PMRPY;

                    if (!String.IsNullOrEmpty(Employee.EM_DOB))
                        employeeMaster.EM_DOB = DateTimeExtensions.GetDate(Employee.EM_DOB);
                    if (!String.IsNullOrEmpty(Employee.EM_DOL))
                        employeeMaster.EM_DOL = DateTimeExtensions.GetDate(Employee.EM_DOL);
                    if (!String.IsNullOrEmpty(Employee.EM_DOJ))
                        employeeMaster.EM_DOJ = DateTimeExtensions.GetDate(Employee.EM_DOJ);
                    if (!String.IsNullOrEmpty(Employee.EM_Passport_Valid_From))
                        employeeMaster.EM_Passport_Valid_From = DateTimeExtensions.GetDate(Employee.EM_Passport_Valid_From);
                    if (!String.IsNullOrEmpty(Employee.EM_Passport_Valid_Upto))
                        employeeMaster.EM_Passport_Valid_Upto = DateTimeExtensions.GetDate(Employee.EM_Passport_Valid_Upto);
                    if (!String.IsNullOrEmpty(Employee.EM_Training_Date))
                        employeeMaster.EM_Training_Date = DateTimeExtensions.GetDate(Employee.EM_Training_Date);
                    if (!String.IsNullOrEmpty(Employee.EM_NoOf_Certificate_Date))
                        employeeMaster.EM_NoOf_Certificate_Date = DateTimeExtensions.GetDate(Employee.EM_NoOf_Certificate_Date);
                    if (!String.IsNullOrEmpty(Employee.EM_NumberandDateOfExemptingOrder))
                        employeeMaster.EM_NumberandDateOfExemptingOrder = Employee.EM_NumberandDateOfExemptingOrder;
                    if (!String.IsNullOrEmpty(Employee.EM_DateWhenClothesGiven))
                        employeeMaster.EM_DateWhenClothesGiven = DateTimeExtensions.GetDate(Employee.EM_DateWhenClothesGiven);
                    if (!String.IsNullOrEmpty(Employee.EM_ChangeEffective_from))
                        employeeMaster.EM_ChangeEffective_from = DateTimeExtensions.GetDate(Employee.EM_ChangeEffective_from);
                    //ADD New Fields
                    employeeMaster.EM_ExemptedSEA_Act = Employee.EM_ExemptedSEA_Act;
                    employeeMaster.EM_VPF_Applicability = Employee.EM_VPF_Applicability;
                    employeeMaster.EM_VPF_Type = Employee.EM_VPF_Type;
                    employeeMaster.EM_VPF_Value = Employee.EM_VPF_Value;
                    employeeMaster.EM_Sectorid = Employee.EM_Sectorid;
                    employeeMaster.EM_JobCategory = Employee.EM_JobCategory;
                    employeeMaster.EM_IndustryType = Employee.EM_IndustryType;
                    //END
                    bool EmployeeUp = RLCS_ClientsManagement.CreateUpdateEmployeeMaster(employeeMaster);
                    if (EmployeeUp)
                    {
                        employeeTransfer.AVACOM_CustomerID = employeeMaster.AVACOM_CustomerID;
                        employeeTransfer.AVACOM_BranchID = Employee.AVACOM_BranchID;
                        employeeTransfer.EMT_ClientId = Employee.EM_ClientID;
                        employeeTransfer.EMT_EmpID = Employee.EM_EmpID;
                        employeeTransfer.EMT_State = Employee.EM_State;
                        employeeTransfer.EMT_Location = Employee.EM_Location;
                        employeeTransfer.EMT_Branch = Employee.EM_Branch;
                        employeeTransfer.EMT_FromDate = DateTimeExtensions.GetDate(Employee.EM_DOJ);
                        employeeTransfer.EMT_Status = "A";
                        employeeTransfer.EMT_CreatedDate = DateTime.Now;
                        employeeTransfer.ISProcessed = false;
                        EmployeeUp = RLCS_ClientsManagement.InsertUpdateEmployeeTransfer(employeeTransfer);
                    }
                    if (EmployeeUp)
                    {
                        Employee.Message = EmployeeUp;
                    }
                    if (EmployeeUp)
                    {
                        EmployeeNewUploadModel Emp = new EmployeeNewUploadModel();
                        TinyMapper.Bind<EmployeeMasterVModel, EmployeeNewUploadModel>(config =>
                        {
                            config.Ignore(source => source.EM_DOB);
                            config.Ignore(source => source.EM_DOJ);
                            config.Ignore(source => source.EM_DOL);
                            config.Ignore(source => source.EM_NoOf_Certificate_Date);
                            config.Ignore(source => source.EM_Training_Date);
                            config.Ignore(source => source.EM_Passport_Valid_From);
                            config.Ignore(source => source.EM_Passport_Valid_Upto);
                            config.Ignore(source => source.EM_NumberandDateOfExemptingOrder);
                            config.Ignore(source => source.EM_ChangeEffective_from);
                        });
                        Emp = TinyMapper.Map<EmployeeNewUploadModel>(Employee);
                        Emp.EM_modifiedBy = "Avantis";
                        if (!String.IsNullOrEmpty(Employee.EM_DOB))
                            Emp.EM_DOB = DateTimeExtensions.GetDate(Employee.EM_DOB);
                        if (!String.IsNullOrEmpty(Employee.EM_DOL))
                            Emp.EM_DOL = DateTimeExtensions.GetDate(Employee.EM_DOL);
                        if (!String.IsNullOrEmpty(Employee.EM_DOJ))
                            Emp.EM_DOJ = DateTimeExtensions.GetDate(Employee.EM_DOJ);
                        if (!String.IsNullOrEmpty(Employee.EM_Passport_Valid_From))
                            Emp.EM_Passport_Valid_From = DateTimeExtensions.GetDate(Employee.EM_Passport_Valid_From);
                        if (!String.IsNullOrEmpty(Employee.EM_Passport_Valid_Upto))
                            Emp.EM_Passport_Valid_Upto = DateTimeExtensions.GetDate(Employee.EM_Passport_Valid_Upto);
                        if (!String.IsNullOrEmpty(Employee.EM_Training_Date))
                            Emp.EM_Training_Date = DateTimeExtensions.GetDate(Employee.EM_Training_Date);
                        if (!String.IsNullOrEmpty(Employee.EM_NoOf_Certificate_Date))
                            Emp.EM_NoOf_Certificate_Date = DateTimeExtensions.GetDate(Employee.EM_NoOf_Certificate_Date);
                        if (!String.IsNullOrEmpty(Employee.EM_NumberandDateOfExemptingOrder))
                            Emp.EM_NumberandDateOfExemptingOrder = Employee.EM_NumberandDateOfExemptingOrder;
                        if (!String.IsNullOrEmpty(Employee.EM_DateWhenClothesGiven))
                            //Emp.EM_DateWhenClothesGiven = DateTimeExtensions.GetDate(Employee.EM_DateWhenClothesGiven);
                            Emp.EM_DateWhenClothesGiven = Employee.EM_DateWhenClothesGiven;
                        if (!String.IsNullOrEmpty(Employee.EM_ChangeEffective_from))
                            Emp.EM_ChangeEffective_From = DateTimeExtensions.GetDate(Employee.EM_ChangeEffective_from);
                        Emp.EM_EmploymentType = Employee.EM_Employmenttype;
                        Emp.EM_Client_ESI_Number = Employee.EM_Client_ESI_Number;
                        if (string.IsNullOrEmpty(Employee.EM_Client_PT_State))
                        {
                            Emp.EM_Client_PT_State = Employee.EM_PT_State;
                        }
                        else
                        {
                            Emp.EM_Client_PT_State = Employee.EM_Client_PT_State;
                        }

                        Emp.EM_EPS_Applicabilty = Employee.EM_EPS_Applicabilty;
                        string Url = "EmployeeMaster/Save_EmployeeMasterBulk";
                        EmployeeUploadMasterVModel.CreatedBy = "Avantis";
                        bool Apisuccess = EmployeeApiCall(Emp, Url);
                        if (Apisuccess)
                        {
                            Employee.Message = EmployeeUp;
                            RLCS_ClientsManagement.Update_ProcessedStatus_EmployeeMaster(Employee.EM_EmpID, Employee.EM_ClientID, Apisuccess);
                        }

                        StateList = RLCS_ClientsManagement.GetAllStates();
                        PTStateList = RLCS_ClientsManagement.GetAllPTStates();
                        countryList = RLCS_ClientsManagement.GetAllCountry();
                        Employee.ProfileID = Convert.ToString(AuthenticationHelper.UserID);
                        //Employee.ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
                        // Employee.CustId = Convert.ToInt32(customer);
                        Employee.UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                        Employee.Path1 = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                        if (StateList != null && StateList.Count > 0)
                        {
                            TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                            Employee.States = TinyMapper.Map<List<StateVModel>>(StateList);
                        }
                        if (PTStateList != null && PTStateList.Count > 0)
                        {
                            TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                            Employee.PTStates = TinyMapper.Map<List<StateVModel>>(PTStateList);
                        }
                        if (countryList != null && countryList.Count > 0)
                        {
                            TinyMapper.Bind<List<Country>, List<CountryVModel>>();
                            Employee.Countries = TinyMapper.Map<List<CountryVModel>>(countryList);
                        }
                        if (Employee.ServiceProviderID==94)
                        {
                        SectorList = RLCS_ClientsManagement.GetSector(Employee.EM_State);
                        JobCategoryList = RLCS_ClientsManagement.GetJobCategory(Employee.EM_State, Employee.EM_Sectorid);
                        IndustryTypeList = RLCS_ClientsManagement.GetIndustryType(Employee.EM_State, Employee.EM_Sectorid, Employee.EM_JobCategory);
                        //VMSector
                        if (SectorList != null && SectorList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_SECTOR_MASTERDATA_Result>, List<SectorVModel>>();
                            Employee.VMSector = TinyMapper.Map<List<SectorVModel>>(SectorList);
                        }
                        //VMJobCategory
                        if (JobCategoryList != null && JobCategoryList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_GROUP_MASTERDATA_Result>, List<JobCategoryVModel>>();
                            Employee.VMJobCategory = TinyMapper.Map<List<JobCategoryVModel>>(JobCategoryList);
                        }
                        //VMIndustryType
                        if (IndustryTypeList != null && IndustryTypeList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_IndustryTypeDATA_Result>, List<IndustryTypeVModel>>();
                            Employee.VMIndustryType = TinyMapper.Map<List<IndustryTypeVModel>>(IndustryTypeList);
                        }

                        }
                    }
                }
                else
                {
                    Employee.ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
                    if (Session["CustID"] != null)
                    {
                        if (Convert.ToInt32(Session["CustID"]) > 0)
                            Employee.CustId = Convert.ToInt32(Session["CustID"]);
                        else
                        {
                            Employee.CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                    }
                    else
                    {
                        customer = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }

                    //Employee.CustId = Convert.ToInt32(customer);
                    Employee.UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                    Employee.Path1 = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

                    StateList = RLCS_ClientsManagement.GetAllStates();
                    PTStateList = RLCS_ClientsManagement.GetAllPTStates();
                    countryList = RLCS_ClientsManagement.GetAllCountry();
                    if (StateList != null && StateList.Count > 0)
                    {
                        TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                        Employee.States = TinyMapper.Map<List<StateVModel>>(StateList);
                    }
                    if (PTStateList != null && PTStateList.Count > 0)
                    {
                        TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                        Employee.PTStates = TinyMapper.Map<List<StateVModel>>(PTStateList);
                    }
                    if (countryList != null && countryList.Count > 0)
                    {
                        TinyMapper.Bind<List<Country>, List<CountryVModel>>();
                        Employee.Countries = TinyMapper.Map<List<CountryVModel>>(countryList);
                    }
                    if (Employee.ServiceProviderID == 94)
                    {
                        SectorList = RLCS_ClientsManagement.GetSector(Employee.EM_State);
                        JobCategoryList = RLCS_ClientsManagement.GetJobCategory(Employee.EM_State, Employee.EM_Sectorid);
                        IndustryTypeList = RLCS_ClientsManagement.GetIndustryType(Employee.EM_State, Employee.EM_Sectorid, Employee.EM_JobCategory);
                        //VMSector
                        if (SectorList != null && SectorList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_SECTOR_MASTERDATA_Result>, List<SectorVModel>>();
                            Employee.VMSector = TinyMapper.Map<List<SectorVModel>>(SectorList);
                        }
                        //VMJobCategory
                        if (JobCategoryList != null && JobCategoryList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_GROUP_MASTERDATA_Result>, List<JobCategoryVModel>>();
                            Employee.VMJobCategory = TinyMapper.Map<List<JobCategoryVModel>>(JobCategoryList);
                        }
                        //VMIndustryType
                        if (IndustryTypeList != null && IndustryTypeList.Count > 0)
                        {
                            TinyMapper.Bind<List<Proc_ESIC_IndustryTypeDATA_Result>, List<IndustryTypeVModel>>();
                            Employee.VMIndustryType = TinyMapper.Map<List<IndustryTypeVModel>>(IndustryTypeList);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // UploadDocumentVModel.ServerError = true;
            }
            return View(Employee);
        }

        [HttpPost]
        public JsonResult CheckExistingEmployee(string EM_EmpID)
        {

            bool ifEmployeeExist = false;
            int? CustID = 0;
            try
            {
                if (Session["CustID"] != null)
                {
                    if (Convert.ToInt32(Session["CustID"]) > 0)
                        CustID = Convert.ToInt32(Session["CustID"]);
                    else
                    {
                        CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                }
                else
                {
                    CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (TempData["IsCheckEmp"] != null)
                {
                    if (!String.IsNullOrEmpty((TempData["IsCheckEmp"]).ToString()))
                    {
                        bool IsCheckEmp = Convert.ToBoolean(TempData["IsCheckEmp"]);
                        ifEmployeeExist = false;
                        TempData["IsCheckEmp"] = null;
                    }
                    if (TempData["EmpID"] != null)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(TempData["EmpID"])))
                        {
                            String EmployeeID = Convert.ToString(TempData["EmpID"]);
                            if (EmployeeID == EM_EmpID)
                            {
                                ifEmployeeExist = false;
                                TempData["EmpID"] = null;
                            }

                            else
                            {
                                // CustID = Convert.ToInt32(AuthenticationHelper.CustomerID) != 0 ? Convert.ToInt32(AuthenticationHelper.CustomerID) : 0;
                                ifEmployeeExist = RLCS_ClientsManagement.CheckEmployeeID(EM_EmpID, CustID);
                                TempData["EmpID"] = null;
                            }
                        }
                    }
                }
                else
                {
                    // CustID = Convert.ToInt32(AuthenticationHelper.CustomerID) != 0 ? Convert.ToInt32(AuthenticationHelper.CustomerID) : 0;
                    ifEmployeeExist = RLCS_ClientsManagement.CheckEmployeeID(EM_EmpID, CustID);
                }

                return Json(!ifEmployeeExist, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult UploadEmployeeFiles(string CustID)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {

                UploadEmployeeDocumentVModel UploadDocumentVModel = new UploadEmployeeDocumentVModel();
                if (!String.IsNullOrEmpty(CustID))
                    UploadDocumentVModel.CustomerID = Convert.ToInt32(CustID);
                UploadDocumentVModel.ISAdd = true;
                TempData["CustID"] = Convert.ToInt32(CustID);
                return View(UploadDocumentVModel);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }
        [HttpGet]
        public ActionResult UploadEmployeeFilesNew(string CustID, string UserID)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {

                UploadEmployeeDocumentVModel UploadDocumentVModel = new UploadEmployeeDocumentVModel();
                if (!String.IsNullOrEmpty(CustID))
                    UploadDocumentVModel.CustomerID = Convert.ToInt32(CustID);
                UploadDocumentVModel.UserID = Convert.ToInt32(UserID);

                UploadDocumentVModel.ISAdd = true;
                TempData["CustID"] = Convert.ToInt32(CustID);
                TempData["UserID"] = Convert.ToInt32(UserID);
                return View(UploadDocumentVModel);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }
        #region BULK UPLOAD UpdateSelectedColumnEmployee
        [HttpGet]
        public ActionResult UploadEmployeeUpdateSelected(string CustID,string UserID)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {

                UploadEmployeeDocumentVModel UploadDocumentVModel = new UploadEmployeeDocumentVModel();
                List<RLCS_ClientHeader_Mapping> objRLCS_ClientHeader_Mapping = new List<RLCS_ClientHeader_Mapping>();
                if (!String.IsNullOrEmpty(CustID))
                    UploadDocumentVModel.CustomerID = Convert.ToInt32(CustID);
                UploadDocumentVModel.UserID = Convert.ToInt32(UserID);
                UploadDocumentVModel.ISAdd = true;
                TempData["CustID"] = Convert.ToInt32(CustID);
                TempData["UserID"] = Convert.ToInt32(UserID);

                List<ClientHeaderMappingVModel> compare = new List<ClientHeaderMappingVModel>();

                objRLCS_ClientHeader_Mapping = RLCS_ClientsManagement.GetMappedClientHeadersColumn("RLCS_Employee_Master", 0);
                TinyMapper.Bind<RLCS_ClientHeader_Mapping, ClientHeaderMappingVModel>();
                compare = TinyMapper.Map<List<ClientHeaderMappingVModel>>(objRLCS_ClientHeader_Mapping);
                if (compare.Count > 0)
                {
                    for (int j = 0; j < compare.Count; j++)
                    {
                        UploadDocumentVModel.ClientHeaderMappingVModelList.Add(new ClientHeaderMappingVModel { ID = compare[j].ID, CustomerID = Convert.ToInt32(CustID), TableName = "RLCS_Employee_Master", ColName = compare[j].ColName, ClientHeaderName = compare[j].ClientHeaderName });

                    }
                }
                return View(UploadDocumentVModel);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        [HttpPost]
        public ActionResult UploadEmployeeUpdateSelected(FormCollection formCollection, UploadEmployeeDocumentVModel UploadDocumentVModel)
        {
            LogErrorsUpload.Clear();
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                bool update = true;
                string FilePath = "";
                EmployeeMasterVModel EmployeeMasterVModel = new EmployeeMasterVModel();
                try
                {
                    if (Request != null)
                    {
                        HttpPostedFileBase file = Request.Files["fileUpload"];
                        if (file != null && file.ContentLength > 0)
                        {
                            string ErrorFileName = "ErrorFile_UploadEmployeeMasterExcelSheet_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                            List<string> ErrorList = new List<string>();
                            if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                            {
                                UploadDocumentVModel.Error = false;
                                string fileName = file.FileName;
                                string fileContentType = file.ContentType;
                                string Extention = Path.GetExtension(fileName);
                                byte[] fileBytes = new byte[file.ContentLength];
                                string FolderPath = Server.MapPath("~/HRExcelEmployee");
                                var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                                if (!Directory.Exists(FolderPath))
                                {
                                    Directory.CreateDirectory(FolderPath);
                                }
                                FilePath = System.Web.HttpContext.Current.Server.MapPath("~") + "/HRExcelEmployee/" + fileName;
                                file.SaveAs(FilePath);
                                if (Extention.ToLower() == ".xlsx")
                                {
                                    using (var package = new ExcelPackage(file.InputStream))
                                    {
                                        var currentSheet = package.Workbook.Worksheets;
                                        var workSheet = currentSheet.First();

                                        if (workSheet.Name=="Employee_Master")
                                        {
                                        DataTable EmpDT = new DataTable();

                                        EmpDT = SelectedColumnConvertToDataTable(workSheet);
                                        if (EmpDT != null)
                                        {
                                            DataRow dr = null;
                                            string[] arrcolumn = { "ClientID", "Emp ID", "Emp Name", "Branch", "Gender(FEMALE/MALE)", "Father Name/Husband Name", "DOB(DATE)i.e.(DD/MM/YYYY)", "Relationship", "DOJ(DATE)i.e.(DD/MM/YYYY)", "Firsttime_secondtime(FIRST TIME/ SECOND TIME)", "International_workers(YES/NO)", "PF_Capping_Applicability(YES/NO)", "Status(ACTIVE/INACTIVE)", "Martial Status(MARRIED/UNMARRIED)", "Physically Challenged(YES/NO)", "PayrollMonth(MONTH INTEGER)", "PayrollYear(YEAR INTEGER)", "Courage Month(MONTH INTEGER)", "Courage Year(YEAR INTEGER)", "Security Provided(YES/NO)", "Women working Night shift (YES/NO)", "Department", "PT State", "Date of Leaving(DATE)i.e.(DD/MM/YYYY)", "Email ID", "Mobile No", "Skill Category i.e.(HIGHLY SKILLED/SKILLED/SEMI SKILLED/UNSKILLED)", "Passport No", "Effective change date(DATE)i.e.(DD/MM/YYYY)", "Address", "Designation", "ESICNO", "UAN", "PAN Number", "Adhar Card No.", "Bank Name", "Bankaccountnumber", "Passport Valid From(DATE)i.e.(DD/MM/YYYY)", "Passport Valid To(DATE)i.e.(DD/MM/YYYY)", "EPFO Aadhar(YES/NO)", "IFSC", "Nationality", "PF_Applicability(YES/NO)", "ESI_Applicability(YES/NO)", "PFNO", "EPFO Bank A/c(YES/NO)", "EPFO PAN(YES/NO)", "Passport Issued Country", "PMPRY(YES/NO)", "PT ApplicabilityYES/NO)", "No Of Certificate", "No of Certificate Date (Date)i.e.(DD/MM/YYYY)", "Token NO", "Relay Assigned", "Letter Of Group", "Mode of transport(Cab/Transport Facility", "years of Experience", "Employment Type(i.e.PERMANENT/CONTRACT)", "Client PT State", "EPS Applicability(YES/NO)", "Client ESI NO", "Permanent Address", "Identityfication Mark", "Emergency Contact Address", "Emergency Contact No", "Training No", "Training Date i.e.(DD/MM/YYYY)", "Place Of Work(Underground/Open Cast/Surface)", "Remark", "Educational Level", "Place Of Employment", "Date Of Cloth Given i.e.(DD/MM/YYYY)", "Date Of Exempting Order i.e.(DD/MM/YYYY)", "Particulars Of Transfer From One Group To Another", "Sales Promotion(YES/NO)", "Payment Mode", "Is Lwf Exempted", "Exempted from S&E Act(YES/NO)", "VPF Applicability(YES/NO)", "VPF Type(Inputed Percentage/Fixed)", "VPF Value", "Sector", "Job Category", "Industry Type" };
                                            DataTable dt = new DataTable("Employee_Master");
                                            if (arrcolumn.Count() == 84)
                                            {
                                                foreach (var item in arrcolumn)
                                                {
                                                    dt.Columns.Add(Convert.ToString(item));

                                                }

                                                List<string> lstemp = new List<string>();
                                                foreach (DataColumn item in EmpDT.Columns)
                                                {
                                                    lstemp.Add(item.ColumnName);
                                                }
                                                for (int k = 0; k < EmpDT.Rows.Count; k++)
                                                {
                                                    var newrow = dt.NewRow();
                                                    foreach (var emp in lstemp)
                                                    {
                                                        newrow[emp] = EmpDT.Rows[k][emp];
                                                    }
                                                    dt.Rows.Add(newrow);
                                                }

                                                for (int i = 0; i < dt.Rows.Count; i++)
                                                {
                                                    for (int j = 2; j < dt.Columns.Count; j++)
                                                    {
                                                        if (string.IsNullOrEmpty(dt.Rows[i][j].ToString()))
                                                        {
                                                            // Write your Custom Code
                                                            dt.Rows[i][j] = "N0";
                                                        }
                                                    }
                                                }
                                            }
                                            if (dt.Rows.Count <= 0)
                                            {
                                                UploadDocumentVModel.RowCount = true;
                                            }
                                            else
                                            {
                                                if (dt.Columns.Count > 2)
                                                {

                                                    var CustomerID = new SqlParameter("@CustomerID", SqlDbType.Int);
                                                    CustomerID.Value = UploadDocumentVModel.CustomerID;
                                                    var UserID = new SqlParameter("@UserID", SqlDbType.Int);
                                                    UserID.Value = UploadDocumentVModel.UserID;
                                                    var UpdateFlag = new SqlParameter("@UpdateFlag", SqlDbType.Bit);
                                                    UpdateFlag.Value = update;
                                                    var EmployeeDT = new SqlParameter("@EmployeeDT", SqlDbType.Structured);
                                                    EmployeeDT.Value = dt;
                                                    EmployeeDT.TypeName = "dbo.EmplyeeUDT";
                                                    var Result = new SqlParameter("@Result", SqlDbType.Int);
                                                    Result.Direction = ParameterDirection.Output;
                                                    var FileID = new SqlParameter("@FileID", SqlDbType.Int);
                                                    FileID.Direction = ParameterDirection.Output;


                                                    using (ComplianceDBEntities entity = new ComplianceDBEntities())
                                                    {
                                                        entity.Database.CommandTimeout = 360;//timeout resolved
                                                        entity.Database.ExecuteSqlCommand("Exec dbo.SP_EmployeeInsert @EmployeeDT,@CustomerID,@UserID,@UpdateFlag,@Result OUTPUT,@FileID OUTPUT", EmployeeDT, CustomerID, UserID, UpdateFlag, Result, FileID);

                                                        if (Convert.ToInt32(Result.Value) == 200)
                                                        {
                                                            UploadDocumentVModel.Message = true;
                                                        }
                                                        else if (Convert.ToInt32(Result.Value) == 400 && Convert.ToInt32(FileID.Value) > 0)
                                                        {
                                                            int fileid = Convert.ToInt32(FileID.Value);
                                                            UploadDocumentVModel.Message = false;
                                                            //List<string> ErrorList = new List<string>();
                                                            ErrorList = (from emp in entity.Temp_EmployeeMaster_ErrorList
                                                                         where emp.FileID == (int)FileID.Value && emp.CustomerID == (int)CustomerID.Value
                                                                         select emp.ErrorLog1
                                                                         ).ToList();

                                                            if (ErrorList.Count > 0)
                                                            {
                                                                //string path = "";
                                                                //UploadDocumentVModel.FileName = RLCS_WriteLog.WriteLog(ErrorList, "Upload Employee Master Excel Sheet", out path);
                                                                UploadDocumentVModel.ExcelErrors = true;
                                                                ViewData["ErrorFileName"] = ErrorFileName;
                                                                GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                                                            }
                                                            else
                                                            {
                                                                ErrorList.Add("Error Occured.....!");
                                                                //string path = "";
                                                                //UploadDocumentVModel.FileName = RLCS_WriteLog.WriteLog(ErrorList, "Upload Employee Master Excel Sheet", out path);
                                                                UploadDocumentVModel.ExcelErrors = true;
                                                                ViewData["ErrorFileName"] = ErrorFileName;
                                                                GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");

                                                            }

                                                        }

                                                    }

                                                }
                                                else
                                                {
                                                    UploadDocumentVModel.ColumnCount = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ErrorList.Add(" , Uploaded file is invalid. Please Download Excel Sample Format. , ");
                                            UploadDocumentVModel.ExcelErrors = true;
                                            ViewData["ErrorFileName"] = ErrorFileName;
                                            GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                                        }
                                    }
                                    else if(workSheet.Name == "Employee_Transfer")
	                                {
                                            bool Valid = ValidateInsertEmployeeTransfer(workSheet, UploadDocumentVModel.CustomerID);
                                            if (Valid==false)
                                            {
                                                UploadDocumentVModel.ExcelErrors = true;
                                            }
                                            UploadDocumentVModel.Message = Valid;
                                    }
                                     else
                                        {
                                            ErrorList.Add(" , Uploaded file is invalid. Please Download Excel Sample Format. , ");
                                            UploadDocumentVModel.ExcelErrors = true;
                                            ViewData["ErrorFileName"] = ErrorFileName;
                                            GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                                        }

                                    }
                                }
                                else
                                {                                    
                                    UploadDocumentVModel.ExcelErrors = true;
                                    ErrorList.Add(" , Please Upload latest Updated Version Excel File. Your File Version is " + Extention.ToLower() + " : Required File Version .xlsx , ");
                                    ViewData["ErrorFileName"] = ErrorFileName;
                                    GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                                }
                            }
                            else
                            {
                                UploadDocumentVModel.ExcelErrors = true;
                                ErrorList.Add(" , Please Download Excel Sample Format. Your File Version is " + Path.GetExtension(file.FileName).ToLower() + " : Required File Version .xlsx , ");
                                ViewData["ErrorFileName"] = ErrorFileName;
                                GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                            }
                        }
                        else
                        {
                            UploadDocumentVModel.Error = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                return View(UploadDocumentVModel);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        [HttpPost]
        public JsonResult JSONInsert(string ColumnJSON)
        {
            //XmlDocument doc = JsonConvert.DeserializeXmlNode(IOJson);
            string ID = "0";
            try
            {
                var IOJsonNew = ColumnJSON;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    tbl_HeaderColumnJSON Record1 = new tbl_HeaderColumnJSON()
                    {
                        ColumnJson = Convert.ToString(IOJsonNew)
                    };

                    entities.tbl_HeaderColumnJSON.Add(Record1);
                    entities.SaveChanges();
                    ID = Convert.ToString(Record1.ID);
                }

                return Json(ID, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(ID, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult DownloadAttachmentSelectedColumns(string CustomerID, int ID)
        {
            UploadDocumentVModel UploadDocumentVModel = new UploadDocumentVModel();
            UploadEmployeeDocumentVModel EmpUpload = new UploadEmployeeDocumentVModel();

            try
            {
                string filePath = "";
                if (CustomerID != null)
                {
                    if (ID != null)
                    {
                        using (ComplianceDBEntities entity = new ComplianceDBEntities())
                        {
                            var LstColumn = (from emp in entity.tbl_HeaderColumnJSON
                                             where emp.ID == (int)ID
                                             select emp.ColumnJson
                                             ).ToList();

                            if (LstColumn.Count > 0)
                            {
                                string[] arrList = LstColumn.ToArray();
                                DownloadUpdateSelectedColumns(Convert.ToInt32(CustomerID), arrList[0].ToString());
                            }
                        }
                    }
                }
                else
                {
                    UploadDocumentVModel.ServerError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                UploadDocumentVModel.ServerError = true;
            }

            return View("UploadEmployeeUpdateSelected", UploadDocumentVModel);
        }
        private void DownloadUpdateSelectedColumns(int CustomerID, string jsonContent)
        {
            try
            {
                //used NewtonSoft json nuget package
                XmlNode xml = JsonConvert.DeserializeXmlNode("{Tables:{Table:" + jsonContent + "}}");
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xml.InnerXml);
                XmlReader xmlReader = new XmlNodeReader(xml);
                DataSet dataSet = new DataSet();
                dataSet.ReadXml(xmlReader);
                var dataTable = dataSet.Tables[0];
                DataTable dt = new DataTable();
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        dt.Columns.Add(Convert.ToString(row["Table_Text"]));

                    }
                }


                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Employee_Master");

                    exWorkSheet.Cells["A1"].LoadFromDataTable(dt, true);
                    exWorkSheet.DefaultColWidth = 15;
                    exWorkSheet.Row(1).Height = 45;
                    exWorkSheet.Cells.Style.Numberformat.Format = "@";
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1, 1 + dt.Columns.Count - 1])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        col.Style.WrapText = true;
                        col.Style.Numberformat.Format = "@";

                    }
                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=EmployeeSampleUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.Flush(); // Sends all currently buffered output to the client.
                    Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.

                }
            }
            catch (Exception ex)
            {
            }

        }

        #endregion

        [HttpPost]
        public ActionResult UploadEmployeeFiles(FormCollection formCollection, UploadEmployeeDocumentVModel UploadDocumentVModel)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                bool update = false;
                EmployeeMasterVModel EmployeeMasterVModel = new EmployeeMasterVModel();
                try
                {
                    if (Request != null)
                    {
                        HttpPostedFileBase file = Request.Files["fileUpload"];
                        if (file != null && file.ContentLength > 0)
                        {

                            if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                            {
                                UploadDocumentVModel.Error = false;
                                string fileName = file.FileName;
                                string fileContentType = file.ContentType;
                                byte[] fileBytes = new byte[file.ContentLength];
                                var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                                using (var package = new ExcelPackage(file.InputStream))
                                {
                                    if (UploadDocumentVModel.ISAdd == true)
                                    {
                                        update = false;
                                        TempData["Update"] = update;
                                    }
                                    else
                                    {
                                        update = true;
                                        TempData["Update"] = update;
                                    }
                                    int CustomerID = UploadDocumentVModel.CustomerID;
                                    UploadDocumentVModel = ProcessEmployeeMasterDetails(package, CustomerID);
                                }
                            }
                        }
                        else
                        {
                            UploadDocumentVModel.Error = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                return View(UploadDocumentVModel);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        //ADD NEW CODE POST DT TO SQL
        [HttpPost]
        public ActionResult UploadEmployeeFilesNew(FormCollection formCollection, UploadEmployeeDocumentVModel UploadDocumentVModel)
        {
            UploadDocumentVModel.ExcelErrors = false;
            UploadDocumentVModel.ColumnCount = false;
            UploadDocumentVModel.RowCount = false;
            LogErrorsUpload.Clear();
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                bool update = false;
                string FilePath = "";
                EmployeeMasterVModel EmployeeMasterVModel = new EmployeeMasterVModel();
                try
                {
                    if (Request != null)
                    {
                        HttpPostedFileBase file = Request.Files["fileUpload"];
                        if (file != null && file.ContentLength > 0)
                        {

                            string ErrorFileName = "ErrorFile_UploadEmployeeMasterExcelSheet_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                            List<string> ErrorList = new List<string>();

                            if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                            {
                                if (UploadDocumentVModel.ISAdd == true)
                                {
                                    update = false;
                                    TempData["Update"] = update;
                                }
                                else
                                {
                                    update = true;
                                    TempData["Update"] = update;
                                }
                                UploadDocumentVModel.Error = false;
                                string fileName = file.FileName;
                                string fileContentType = file.ContentType;
                                string Extention = Path.GetExtension(fileName);
                                byte[] fileBytes = new byte[file.ContentLength];
                                string FolderPath = Server.MapPath("~/HRExcelEmployee");
                                var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                                if (!Directory.Exists(FolderPath))
                                {
                                    Directory.CreateDirectory(FolderPath);
                                }
                                FilePath = System.Web.HttpContext.Current.Server.MapPath("~") + "/HRExcelEmployee/" + fileName;
                                file.SaveAs(FilePath);

                                //DataTable dt = new DataTable();
                                //String excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                                //OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                                //try
                                //{
                                //    excelConnection.Open();
                                //    OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Employee_Master$]", excelConnection);
                                //    cmd.CommandTimeout = 360;
                                //    OleDbDataAdapter adp = new OleDbDataAdapter(cmd);
                                //    adp.Fill(dt);
                                //    excelConnection.Close();
                                //}
                                //catch (Exception ex)
                                //{
                                //    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                //}
                                if (Extention.ToLower() == ".xlsx")
                                {
                                    using (var package = new ExcelPackage(file.InputStream))
                                    {
                                        var currentSheet = package.Workbook.Worksheets;
                                        var workSheet = currentSheet.First();
                                        if (workSheet.Name== "Employee_Master" && UploadDocumentVModel.ISUpdateCTS == false)
                                        {
                                        DataTable EmpDT = new DataTable();
                                        EmpDT = ConvertToDataTable(workSheet);
                                        //EmpDT = dt;
                                        if (EmpDT != null)
                                        {
                                            if (EmpDT.Rows.Count <= 0)
                                            {
                                                UploadDocumentVModel.RowCount = true;
                                            }
                                            else
                                            {
                                                if (EmpDT.Columns.Count == 84)
                                                {

                                                    var CustomerID = new SqlParameter("@CustomerID", SqlDbType.Int);
                                                    CustomerID.Value = UploadDocumentVModel.CustomerID;
                                                    var UserID = new SqlParameter("@UserID", SqlDbType.Int);
                                                    UserID.Value = UploadDocumentVModel.UserID;
                                                    var UpdateFlag = new SqlParameter("@UpdateFlag", SqlDbType.Bit);
                                                    UpdateFlag.Value = update;
                                                    var EmployeeDT = new SqlParameter("@EmployeeDT", SqlDbType.Structured);
                                                    EmployeeDT.Value = EmpDT;
                                                    EmployeeDT.TypeName = "dbo.EmplyeeUDT";
                                                    var Result = new SqlParameter("@Result", SqlDbType.Int);
                                                    Result.Direction = ParameterDirection.Output;
                                                    var FileID = new SqlParameter("@FileID", SqlDbType.Int);
                                                    FileID.Direction = ParameterDirection.Output;

                                                    using (ComplianceDBEntities entity = new ComplianceDBEntities())
                                                    {
                                                        entity.Database.CommandTimeout = 360;//timeout issue solved
                                                        entity.Database.ExecuteSqlCommand("Exec dbo.SP_EmployeeInsert @EmployeeDT,@CustomerID,@UserID,@UpdateFlag,@Result OUTPUT,@FileID OUTPUT", EmployeeDT, CustomerID, UserID, UpdateFlag, Result, FileID);

                                                        if (Convert.ToInt32(Result.Value) == 200)
                                                        {
                                                            UploadDocumentVModel.Message = true;
                                                        }
                                                        else if (Convert.ToInt32(Result.Value) == 400 && Convert.ToInt32(FileID.Value) > 0)
                                                        {
                                                            int fileid = Convert.ToInt32(FileID.Value);
                                                            UploadDocumentVModel.Message = false;

                                                            ErrorList = (from emp in entity.Temp_EmployeeMaster_ErrorList
                                                                         where emp.FileID == (int)FileID.Value && emp.CustomerID == (int)CustomerID.Value
                                                                         select emp.ErrorLog1
                                                                         ).ToList();

                                                            if (ErrorList.Count > 0)
                                                            {
                                                                UploadDocumentVModel.ExcelErrors = true;
                                                                ViewData["ErrorFileName"] = ErrorFileName;
                                                                GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");

                                                            }
                                                            else
                                                            {
                                                                ErrorList.Add(" ,Error Occured.....!, ");
                                                                UploadDocumentVModel.ExcelErrors = true;
                                                                ViewData["ErrorFileName"] = ErrorFileName;
                                                                GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");

                                                            }

                                                        }

                                                    }

                                                }
                                                else
                                                {
                                                    UploadDocumentVModel.ColumnCount = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ErrorList.Add(" , Uploaded file is invalid. Please Download Excel Sample Format. , ");
                                                
                                                foreach (var item in LogErrorsUpload)
                                                {
                                                    ErrorList.Add(","+item);
                                                }   
                                            UploadDocumentVModel.ExcelErrors = true;
                                            ViewData["ErrorFileName"] = ErrorFileName;
                                            GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                                        }
                                    }
                                    else if(workSheet.Name== "Employee_Transfer" && UploadDocumentVModel.ISUpdateCTS == true)
                                     {
                                            bool valid= ValidateInsertEmployeeTransfer(workSheet, UploadDocumentVModel.CustomerID);
                                            if (valid==false)
                                            {
                                                UploadDocumentVModel.ExcelErrors = true;
                                            }
                                            UploadDocumentVModel.Message = valid;
                                     }
                                        else
                                        {
                                            UploadDocumentVModel.ExcelErrors = true;
                                            ErrorList.Add(" , Please Check Sample Excel File.");
                                            ViewData["ErrorFileName"] = ErrorFileName;
                                            GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                                        }

                                    }
                                }
                                else
                                {
                                    UploadDocumentVModel.ExcelErrors = true;
                                    ErrorList.Add(" , Please Upload latest Updated Version Excel File. Your File Version is " + Extention.ToLower() + " : Required File Version .xlsx , ");
                                    ViewData["ErrorFileName"] = ErrorFileName;
                                    GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                                }
                            }
                            else
                            {
                                UploadDocumentVModel.ExcelErrors = true;
                                ErrorList.Add(" , Please Download Excel Sample Format. Your File Version is " + Path.GetExtension(file.FileName).ToLower() + " : Required File Version .xlsx , ");
                                ViewData["ErrorFileName"] = ErrorFileName;
                                GenerateEmpUploadErrorCSV(ErrorList, ErrorFileName, "Emp Id");
                            }
                        }
                        else
                        {
                            UploadDocumentVModel.Error = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    UploadDocumentVModel.ServerError = true;
                }

                return View(UploadDocumentVModel);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        private bool ValidateInsertEmployeeTransfer(ExcelWorksheet xlWorksheet, long CustomerID)
        {
            bool MSG = false;
            EmployeeMasterVModel EmployeeMasterVModel = new EmployeeMasterVModel();
            List<EmployeeMasterVModel> employeeMasterList = new List<EmployeeMasterVModel>();
            RLCS_CustomerBranch_ClientsLocation_Mapping Branch = new RLCS_CustomerBranch_ClientsLocation_Mapping();
            UploadEmployeeDocumentVModel UploadDocumentVModel = new UploadEmployeeDocumentVModel();
            try
            {
                string ErrorFileName = "ErrorFile_UploadEmployeeTransferExcelSheet_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".csv";
                long customer;
                var noOfCol = xlWorksheet.Dimension.End.Column;
                var noOfRow = xlWorksheet.Dimension.End.Row;
                int count = 1;

                if (Convert.ToString(CustomerID) != null)
                {
                    customer = Convert.ToInt64(CustomerID);
                    TempData["CustID"] = Convert.ToString(customer);
                }
                else
                {
                    customer = Convert.ToInt64(AuthenticationHelper.CustomerID);

                }
                #region Employee Transfer
                List<string> LogErrors = new List<string>();
                for (int i = 2; i <= noOfRow; i++)
                {
                    count = count + 1;
                    EmployeeMasterVModel = new EmployeeMasterVModel();

                    #region Validation

                    if (xlWorksheet.Cells[i, 1].Text.ToString().Trim() != "")
                    {
                        string excelClientID = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
                        bool ClientID = RLCS_ClientsManagement.GetClientByid(excelClientID);
                        if (ClientID == false)
                            LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Invalid ClientID-" + excelClientID + " Please make sure that Client should be Active, Check at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_ClientID = excelClientID;
                            EmployeeMasterVModel.AVACOMCustomerID = (int)customer;
                        }
                    }
                    else if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                    {
                        LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",ClientID can not be blank, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                    {
                        string excelEmpID = xlWorksheet.Cells[i, 2].Text.ToString().Trim();
                       bool ValidEMPID=  RLCS_ClientsManagement.GetEmployeeByid(excelEmpID, (int)customer, EmployeeMasterVModel.EM_ClientID);
                        if (ValidEMPID)
                        {
                            bool emp = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 2, excelEmpID);
                            if (emp == true)
                                LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",EmployeeID already exists in uploaded excel document. Please provide unique EmployeeID, Check at Row-" + count);
                            else
                                EmployeeMasterVModel.EM_EmpID = excelEmpID;
                        }
                        else
                        {
                            LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Invalid EmployeeID, Check at Row-" + count);
                        }
                    }
                    else if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                    {
                        LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",EmployeeID can not be blank, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                    {
                        string excelBranch = xlWorksheet.Cells[i, 3].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(excelBranch))
                        {
                            //Get the Previous Branch Name
                            RLCS_Employee_Master EmpDetails = RLCS_ClientsManagement.GetEmployeeDetails(EmployeeMasterVModel.EM_EmpID, Convert.ToInt32(CustomerID), EmployeeMasterVModel.EM_ClientID);
                            if (EmpDetails != null)
                            {
                                string PreviousBranchName = EmpDetails.EM_Branch;
                                if (PreviousBranchName == excelBranch)
                                {
                                    LogErrors.Add( EmployeeMasterVModel.EM_EmpID + ", Same branch not use to transfer Employee, Check at Row-" + count);
                                }
                                else
                                {
                                    RLCS_CustomerBranch_ClientsLocation_Mapping BranchDetails = RLCS_ClientsManagement.GetStateLocationByBranchName(excelBranch, (int)customer);
                                    if (BranchDetails != null)
                                    {
                                        if (BranchDetails.AVACOM_BranchID.HasValue)
                                            EmployeeMasterVModel.AVACOM_BranchID = BranchDetails.AVACOM_BranchID.Value;
                                        EmployeeMasterVModel.EM_State = BranchDetails.CM_State;
                                        EmployeeMasterVModel.EM_Location = BranchDetails.CM_City;
                                        EmployeeMasterVModel.EM_Branch = excelBranch;
                                    }
                                    if (!string.IsNullOrEmpty(EmployeeMasterVModel.EM_ClientID))
                                    {
                                        bool branchExist = RLCS_ClientsManagement.CheckBranchIDExist(excelBranch, EmployeeMasterVModel.EM_ClientID);
                                        if (branchExist == false)
                                        {
                                            LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Invalid Transfer Branch. Branch-" + excelBranch + " does not exist with ClientID-" + EmployeeMasterVModel.EM_ClientID + ", Check at Row-" + count);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Transfer Branch can not be blank, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                    {
                        try
                        {
                            string LastWorkingDate = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
                            //Get the Previous DOJ
                            RLCS_Employee_Master EmpDetails = RLCS_ClientsManagement.GetEmployeeDetails(EmployeeMasterVModel.EM_EmpID, Convert.ToInt32(CustomerID), EmployeeMasterVModel.EM_ClientID);
                            if (EmpDetails != null)
                            {
                                DateTime? PreviousEmpDOJ = EmpDetails.EM_DOJ;
                                DateTime dt = Convert.ToDateTime(PreviousEmpDOJ);
                                string dtp = dt.ToString("dd/MM/yyyy");
                                //Check Date
                                if (DateTime.ParseExact(LastWorkingDate, "dd/MM/yyyy", null) <= PreviousEmpDOJ)
                                {
                                    LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Last Working Date greater than Previous Joining Date. Please Check Previous DOJ :" + dtp + ",Check at Row-" + count);
                                }
                                DateTime LWD = DateTime.ParseExact(LastWorkingDate, "dd/MM/yyyy", null);
                                EmployeeMasterVModel.EM_LWD = LWD.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Invalid Last Working Date, it should be in DD/MM/YYYY format, Check at Row-" + count);
                        }
                    }
                    else
                    {
                        LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Last Working Date can not be blank, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                    {
                        try
                        {
                            string NEWDateOfJoining = xlWorksheet.Cells[i, 5].Text.ToString().Trim();
                            string LastWorkingDate = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
                            if (DateTime.ParseExact(NEWDateOfJoining, "dd/MM/yyyy", null) < DateTime.ParseExact(LastWorkingDate, "dd/MM/yyyy", null))
                                {
                                    LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Last Working Date greater than New Joining Date. Please Check New DOJ :" + NEWDateOfJoining + "Check at Row-" + count);
                                }
                                DateTime joiningdate = DateTime.ParseExact(NEWDateOfJoining, "dd/MM/yyyy", null);
                                EmployeeMasterVModel.EM_DOJ = joiningdate.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",Invalid New Joining Date, it should be in DD/MM/YYYY format, Check at Row-" + count);
                        }
                    }
                    else
                    {
                        LogErrors.Add(EmployeeMasterVModel.EM_EmpID + ",New Joining Date can not be blank, Check at Row-" + count);
                    }
                   
                    #endregion
                    employeeMasterList.Add(EmployeeMasterVModel);
                }
                if (LogErrors.Count == 0 && employeeMasterList.Count>0)
                {
                    //Save
                   
                    foreach (var item in employeeMasterList)
                    {
                        bool success = false;
                        RLCS_Employee_Master_Transfer empTransfer = new RLCS_Employee_Master_Transfer();
                        empTransfer.EMT_ClientId = item.EM_ClientID;
                        empTransfer.AVACOM_CustomerID = item.AVACOMCustomerID;
                        empTransfer.EMT_EmpID = item.EM_EmpID;
                        empTransfer.AVACOM_BranchID = item.AVACOM_BranchID;
                        empTransfer.EMT_Branch = item.EM_Branch;
                        empTransfer.EMT_State = item.EM_State;
                        empTransfer.EMT_Location = item.EM_Location;
                        empTransfer.EMT_FromDate = Convert.ToDateTime(item.EM_DOJ);
                        empTransfer.EMT_ToDate = Convert.ToDateTime(item.EM_LWD);
                        empTransfer.EMT_Status = "A";
                        empTransfer.EMT_CreatedDate = DateTime.Now;
                        success = RLCS_ClientsManagement.CreateUpdateEmployeeMasterTransfer(empTransfer);
                        if (success)
                        {
                            empTransfer.EMT_ToDate = null;
                            MSG = RLCS_ClientsManagement.InsertUpdateEmployeeTransfer(empTransfer);
                        }
                    }
                }
                else
                {
                    //Error
                    UploadDocumentVModel.ExcelErrors = true;
                    ViewData["ErrorFileName"] = ErrorFileName;
                    GenerateEmpUploadErrorCSV(LogErrors, ErrorFileName, "Emp Id");
                    MSG = false;
                }
                #endregion
                }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                MSG = false;
            }
            return MSG;
        }

        List<string> LogErrorsUpload = new List<string>();
        //private string GetValue(DocumentFormat.OpenXml.Packaging.SpreadsheetDocument doc, DocumentFormat.OpenXml.Spreadsheet.Cell cell)
        //{
        //    string value = cell.CellValue.InnerText;
        //    if (cell.DataType != null && cell.DataType.Value == DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString)
        //    {
        //        return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
        //    }
        //    return value;
        //}
        private DataTable ConvertToDataTable(ExcelWorksheet oSheet)
        {
            string[] arrcolumn = { "ClientID", "Emp ID", "Emp Name", "Branch", "Gender(FEMALE/MALE)", "Father Name/Husband Name", "DOB(DATE)i.e.(DD/MM/YYYY)", "Relationship", "DOJ(DATE)i.e.(DD/MM/YYYY)", "Firsttime_secondtime(FIRST TIME/ SECOND TIME)", "International_workers(YES/NO)", "PF_Capping_Applicability(YES/NO)", "Status(ACTIVE/INACTIVE)", "Martial Status(MARRIED/UNMARRIED)", "Physically Challenged(YES/NO)", "PayrollMonth(MONTH INTEGER)", "PayrollYear(YEAR INTEGER)", "Courage Month(MONTH INTEGER)", "Courage Year(YEAR INTEGER)", "Security Provided(YES/NO)", "Women working Night shift (YES/NO)", "Department", "PT State", "Date of Leaving(DATE)i.e.(DD/MM/YYYY)", "Email ID", "Mobile No", "Skill Category i.e.(HIGHLY SKILLED/SKILLED/SEMI SKILLED/UNSKILLED)", "Passport No", "Effective change date(DATE)i.e.(DD/MM/YYYY)", "Address", "Designation", "ESICNO", "UAN", "PAN Number", "Adhar Card No.", "Bank Name", "Bankaccountnumber", "Passport Valid From(DATE)i.e.(DD/MM/YYYY)", "Passport Valid To(DATE)i.e.(DD/MM/YYYY)", "EPFO Aadhar(YES/NO)", "IFSC", "Nationality", "PF_Applicability(YES/NO)", "ESI_Applicability(YES/NO)", "PFNO", "EPFO Bank A/c(YES/NO)", "EPFO PAN(YES/NO)", "Passport Issued Country", "PMPRY(YES/NO)", "PT ApplicabilityYES/NO)", "No Of Certificate", "No of Certificate Date (Date)i.e.(DD/MM/YYYY)", "Token NO", "Relay Assigned", "Letter Of Group", "Mode of transport(Cab/Transport Facility", "years of Experience", "Employment Type(i.e.PERMANENT/CONTRACT)", "Client PT State", "EPS Applicability(YES/NO)", "Client ESI NO", "Permanent Address", "Identityfication Mark", "Emergency Contact Address", "Emergency Contact No", "Training No", "Training Date i.e.(DD/MM/YYYY)", "Place Of Work(Underground/Open Cast/Surface)", "Remark", "Educational Level", "Place Of Employment", "Date Of Cloth Given i.e.(DD/MM/YYYY)", "Date Of Exempting Order i.e.(DD/MM/YYYY)", "Particulars Of Transfer From One Group To Another", "Sales Promotion(YES/NO)", "Payment Mode", "Is Lwf Exempted", "Exempted from S&E Act(YES/NO)", "VPF Applicability(YES/NO)", "VPF Type(Inputed Percentage/Fixed)", "VPF Value", "Sector", "Job Category", "Industry Type" };
            var totalRows = GetLastUsedRow(oSheet);
            //int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            List<string> LogErrors = new List<string>();
            for (int i = 1; i <= totalRows; i++)
            {
                if (LogErrors.Count > 0)
                {
                    break;
                }
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(oSheet.Cells[i, j].Value).Trim()))
                        {
                            LogErrors.Add("Column Empty.Please delete columns in Your Excel Sheet..!Coloumn NO: " + j);
                            break;
                        }

                        if (dt.Columns.Contains(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            LogErrors.Add("Column Already Exist.Please check Your Excel Sheet..!" + arrcolumn[j - 1]);
                            break;
                        }
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                        if (arrcolumn[j - 1] != Convert.ToString(oSheet.Cells[i, j].Value))
                        {
                            LogErrors.Add("Column Mismatch. Please Check..." + arrcolumn[j - 1] + ":Actual Column No " + j);
                        }
                    }
                    else
                    {
                        dr[j - 1] = Convert.ToString(oSheet.Cells[i, j].Value);
                    }
                }
            }
            if (LogErrors.Count > 0)
            {
                LogErrorsUpload = LogErrors.ToList();
                dt = null;
            }
            return dt;
        }
        private DataTable SelectedColumnConvertToDataTable(ExcelWorksheet oSheet)
        {
            string[] arrcolumn = { "ClientID", "Emp ID", "Emp Name", "Branch", "Gender(FEMALE/MALE)", "Father Name/Husband Name", "DOB(DATE)i.e.(DD/MM/YYYY)", "Relationship", "DOJ(DATE)i.e.(DD/MM/YYYY)", "Firsttime_secondtime(FIRST TIME/ SECOND TIME)", "International_workers(YES/NO)", "PF_Capping_Applicability(YES/NO)", "Status(ACTIVE/INACTIVE)", "Martial Status(MARRIED/UNMARRIED)", "Physically Challenged(YES/NO)", "PayrollMonth(MONTH INTEGER)", "PayrollYear(YEAR INTEGER)", "Courage Month(MONTH INTEGER)", "Courage Year(YEAR INTEGER)", "Security Provided(YES/NO)", "Women working Night shift (YES/NO)", "Department", "PT State", "Date of Leaving(DATE)i.e.(DD/MM/YYYY)", "Email ID", "Mobile No", "Skill Category i.e.(HIGHLY SKILLED/SKILLED/SEMI SKILLED/UNSKILLED)", "Passport No", "Effective change date(DATE)i.e.(DD/MM/YYYY)", "Address", "Designation", "ESICNO", "UAN", "PAN Number", "Adhar Card No.", "Bank Name", "Bankaccountnumber", "Passport Valid From(DATE)i.e.(DD/MM/YYYY)", "Passport Valid To(DATE)i.e.(DD/MM/YYYY)", "EPFO Aadhar(YES/NO)", "IFSC", "Nationality", "PF_Applicability(YES/NO)", "ESI_Applicability(YES/NO)", "PFNO", "EPFO Bank A/c(YES/NO)", "EPFO PAN(YES/NO)", "Passport Issued Country", "PMPRY(YES/NO)", "PT ApplicabilityYES/NO)", "No Of Certificate", "No of Certificate Date (Date)i.e.(DD/MM/YYYY)", "Token NO", "Relay Assigned", "Letter Of Group", "Mode of transport(Cab/Transport Facility", "years of Experience", "Employment Type(i.e.PERMANENT/CONTRACT)", "Client PT State", "EPS Applicability(YES/NO)", "Client ESI NO", "Permanent Address", "Identityfication Mark", "Emergency Contact Address", "Emergency Contact No", "Training No", "Training Date i.e.(DD/MM/YYYY)", "Place Of Work(Underground/Open Cast/Surface)", "Remark", "Educational Level", "Place Of Employment", "Date Of Cloth Given i.e.(DD/MM/YYYY)", "Date Of Exempting Order i.e.(DD/MM/YYYY)", "Particulars Of Transfer From One Group To Another", "Sales Promotion(YES/NO)", "Payment Mode", "Is Lwf Exempted", "Exempted from S&E Act(YES/NO)", "VPF Applicability(YES/NO)", "VPF Type(Inputed Percentage/Fixed)", "VPF Value", "Sector", "Job Category", "Industry Type" };

            int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            List<string> LogErrors = new List<string>();
            for (int i = 1; i <= totalRows; i++)
            {
                if (LogErrors.Count > 0)
                {
                    break;
                }
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(oSheet.Cells[i, j].Value).Trim()))
                        {
                            LogErrors.Add("Column Empty.Please delete columns in Your Excel Sheet..!Coloumn NO: " + j);
                            break;
                        }
                        if (dt.Columns.Contains(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            LogErrors.Add("Column Already Exist.Please check Your Excel Sheet..!" + arrcolumn[j - 1]);
                            break;
                        }
                        if (!arrcolumn.Contains(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            LogErrors.Add("Invalid Column Header.Please check Your Sample Excel Sheet..!" + arrcolumn[j - 1]);
                            break;
                        }
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            dr[j - 1] = " ";
                        }
                        else
                        {
                            dr[j - 1] = Convert.ToString(oSheet.Cells[i, j].Value);
                        }
                        
                    }
                }
            }
            if (LogErrors.Count > 0)
            {
                LogErrorsUpload = LogErrors.ToList();
                dt = null;
            }
            return dt;
        }


        //[HttpPost]
        //public ActionResult CreateUpdateClientMapping(List<ClientHeaderMappingVModel> ClientHeaderMappingVModelList)
        //{
        //    if (System.Web.HttpContext.Current.Request.IsAuthenticated)
        //    {
        //        bool update = false;
        //        long customer = 0;
        //        EmployeeMasterVModel EmployeeMasterVModel = new EmployeeMasterVModel();
        //        UploadEmployeeDocumentVModel EmployeeUploadModel = new UploadEmployeeDocumentVModel();
        //        // List<RLCS_ClientHeader_Mapping> ClientHeaderMapping = new List<RLCS_ClientHeader_Mapping>();

        //        try
        //        {
        //            bool Sucess = false;
        //            if (TempData["Update"] != null)
        //            {
        //                update = (bool)TempData["Update"];
        //            }

        //            if (Convert.ToString(TempData["CustID"]) != null)
        //            {
        //                customer = Convert.ToInt64(TempData["CustID"]);
        //                TempData["CustID"] = Convert.ToString(customer);
        //            }
        //            else
        //            {
        //                customer = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //            }


        //            //TinyMapper.Bind<List<ClientHeaderMappingVModel>, List<RLCS_ClientHeader_Mapping>>();
        //            //ClientHeaderMapping = TinyMapper.Map<List<RLCS_ClientHeader_Mapping>>(ClientHeaderMappingVModelList);
        //            //ClientHeaderMapping.ForEach(m => m.CustomerID = (int)customer);
        //            //ClientHeaderMapping.ForEach(m => m.TableName = "RLCS_Employee_Master");
        //            //Sucess = RLCS_ClientsManagement.SaveclientHeaderMapping(ClientHeaderMapping, "RLCS_Employee_Master", (int)customer);

        //            //if (update)
        //                //EmployeeMasterVModel = ValidateEmployeeExcel(ClientHeaderMappingVModelList, true);
        //           // else
        //                //EmployeeMasterVModel = ValidateEmployeeExcel(ClientHeaderMappingVModelList, false);
        //        }
        //        catch (Exception ex)
        //        {
        //            ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            EmployeeMasterVModel.UploadEmployee.ServerError = true;
        //        }

        //        return View("UploadEmployeeFiles", EmployeeMasterVModel.UploadEmployee);
        //    }
        //    else
        //    {
        //        FormsAuthentication.SignOut();
        //        Session.Abandon();
        //        return RedirectToAction("~/Login.aspx");
        //    }
        //}

        private EmployeeMasterVModel ValidateEmployeeExcel(ExcelWorksheet xlWorksheet, long CustomerID, bool Update)
        {
            EmployeeMasterVModel EmployeeMasterVModel = new EmployeeMasterVModel();
            List<EmployeeMasterVModel> employeeModelList = new List<EmployeeMasterVModel>();
            List<RLCS_Employee_Master> EmployeeDModelList = new List<RLCS_Employee_Master>();
            List<string> XcelHeaders = new List<string>();
            RLCS_CustomerBranch_ClientsLocation_Mapping Branch = new RLCS_CustomerBranch_ClientsLocation_Mapping();
            EmployeeUploadMasterVModel EmployeeUploadMasterVModel = new EmployeeUploadMasterVModel();
            EmployeeNewUploadModel bulkUpload = new EmployeeNewUploadModel();
            List<EmployeeNewUploadModel> bulkUploadALL = new List<EmployeeNewUploadModel>();
            bool Flag = false;
            long customer = 0;
            try
            {
                List<string> LogErrors = new List<string>();
                bool sucess = false;
                var noOfCol = xlWorksheet.Dimension.End.Column;
                var noOfRow = xlWorksheet.Dimension.End.Row;
                int count = 1;

                if (Convert.ToString(CustomerID) != null)
                {
                    customer = Convert.ToInt64(CustomerID);
                    TempData["CustID"] = Convert.ToString(customer);
                }
                else
                {
                    customer = Convert.ToInt64(AuthenticationHelper.CustomerID);

                }

                #region Declare Parameters
                Dictionary<string, bool> CheckErrors = new Dictionary<string, bool>();
                string excelBranch = "";
                string excelClientID = "";
                string Father = "";
                string excelEmpID = "", empname = "";
                string status = "";
                string Married = "";
                string skillC = "";
                string employment = "";
                string Relation = "";
                string womenworkinNight = "";

                string Pmon = "";
                string pyear = "";
                string Cmonth = "";
                string cYear = "";

                int? couragemnth = 0;
                int? payrollmnth = 0;
                int? payrollyear = 0;
                int? courageyear = 0;
                string Phychallenged = "";
                string Security = "";
                string Changeeffectdate = "";
                string DOB = "";
                string DOL = "";
                string DOJ = "";
                string pfcappingApp = "";
                string internation = "";
                string passvalidfrom = "";
                string passvalidto = "";
                string gender = "";
                #endregion

                for (int i = 2; i <= noOfRow; i++)
                {
                    count = count + 1;
                    EmployeeMasterVModel = new EmployeeMasterVModel();

                    #region 

                    if (xlWorksheet.Cells[i, 1].Text.ToString().Trim() != "")
                    {
                        excelClientID = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
                        bool ClientID = RLCS_ClientsManagement.GetClientByid(excelClientID);
                        if (ClientID == false)
                            LogErrors.Add("Invalid ClientID-" + excelClientID + ", Please make sure that Client should be Active, Check at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_ClientID = excelClientID;
                            EmployeeMasterVModel.AVACOMCustomerID = (int)customer;
                            customer = RLCS_ClientsManagement.GetCustomerIDByClientID(excelClientID);
                        }
                    }
                    else if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                    {
                        LogErrors.Add("ClientID can not be blank, Check at Row-" + count);
                    }

                    // LogErrors.Add("Client ID is required at Row" + noOfRow);
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                    {
                        excelEmpID = xlWorksheet.Cells[i, 2].Text.ToString().Trim();
                        if (Update == false)
                        {
                            if (RLCS_ClientsManagement.GetEmployeeByid(excelEmpID, (int)customer, EmployeeMasterVModel.EM_ClientID) == true)
                                LogErrors.Add("EmployeeID-" + excelEmpID + " is already exist, Check at Row-" + noOfRow + ".Please provide unique EmployeeID");
                            else
                            {
                                bool emp = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 2, excelEmpID);

                                if (emp == true)
                                    LogErrors.Add("EmployeeID at Row-" + count + " already exists in uploaded excel document. Please provide unique EmployeeID");
                                else
                                {
                                    bool matched = Regex.IsMatch(excelEmpID, @"^[a-zA-Z0-9 -]+$");
                                    if (matched == false)
                                        LogErrors.Add("Invalid EmployeeID it should be Characters and Numbers only, Check at Row- " + count);
                                    else
                                        EmployeeMasterVModel.EM_EmpID = excelEmpID;
                                }
                            }
                        }
                        else
                        {
                            bool emp = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 2, excelEmpID);

                            if (emp == true)
                                LogErrors.Add("EmployeeID at Row-" + count + " already exists in uploaded excel document. Please provide unique EmployeeID");
                            else
                                EmployeeMasterVModel.EM_EmpID = excelEmpID;
                        }
                    }
                    else if (String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                    {
                        LogErrors.Add("EmployeeID can not be blank, Check at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                    {
                        empname = xlWorksheet.Cells[i, 3].Text.ToString().Trim();
                        bool Emp = Regex.IsMatch(empname, @"^[a-zA-Z0-9 ]+$");
                        if (Emp == false)
                            LogErrors.Add("Invalid Employee Name " + empname + " it should be Characters and Numbers only, Check at Row -" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_EmpName = empname;
                        }
                    }
                    else
                    {
                        LogErrors.Add("Invalid Employee Name " + empname + " Employee Name is Mandatory, Check at Row -" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                    {
                        excelBranch = xlWorksheet.Cells[i, 4].Text.ToString().Trim();
                        if (!String.IsNullOrEmpty(excelBranch))
                        {
                            RLCS_CustomerBranch_ClientsLocation_Mapping BranchDetails = RLCS_ClientsManagement.GetStateLocationByBranchName(excelBranch, (int)customer);
                            if (BranchDetails != null)
                            {
                                if (BranchDetails.AVACOM_BranchID.HasValue)
                                    EmployeeMasterVModel.AVACOM_BranchID = BranchDetails.AVACOM_BranchID.Value;
                                EmployeeMasterVModel.EM_State = BranchDetails.CM_State;
                                EmployeeMasterVModel.EM_Location = BranchDetails.CM_City;
                                EmployeeMasterVModel.EM_Branch = excelBranch;
                            }
                            if (!string.IsNullOrEmpty(EmployeeMasterVModel.EM_ClientID))
                            {
                                bool branchExist = RLCS_ClientsManagement.CheckBranchIDExist(excelBranch, EmployeeMasterVModel.EM_ClientID);
                                if (branchExist == false)
                                {
                                    LogErrors.Add("Invalid Branch. Branch-" + excelBranch + " does not exist with ClientID-" + EmployeeMasterVModel.EM_ClientID + ", Check at Row-" + count);
                                }
                            }
                        }



                    }
                    else
                    {
                        LogErrors.Add("Invalid Branch Name " + xlWorksheet.Cells[i, 4].Text.ToString() + " Branch Name is Mandatory, Check at Row -" + count);
                    }


                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                    {
                        gender = xlWorksheet.Cells[i, 5].Text.ToString().Trim();
                        if (gender.ToUpper() == "FEMALE" || gender.ToUpper() == "F")
                            EmployeeMasterVModel.EM_Gender = "F";
                        else if (gender.ToUpper() == "MALE" || gender.ToUpper() == "M")
                            EmployeeMasterVModel.EM_Gender = "M";
                        else
                            LogErrors.Add("Invalid Gender, It should be Male or Female, Check at Row-" + count);
                    }
                    else
                    {
                        LogErrors.Add("Gender is Mandatory, Check at Row - " + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                    {
                        Father = xlWorksheet.Cells[i, 6].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(Father, @"^[a-zA-Z ]+$");
                        if (matched == false)
                            LogErrors.Add("Only Characters allowed in Father Name, Check at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_FatherName = Father;
                        }
                    }
                    else
                    {
                        LogErrors.Add("Father Name is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                    {
                        try
                        {
                            DOB = xlWorksheet.Cells[i, 7].Text.ToString().Trim();
                            DateTime dbirth = DateTime.ParseExact(DOB, "dd/MM/yyyy", null);
                            if (dbirth >= DateTime.Now)
                            {
                                LogErrors.Add("Invalid Date of birth, it can not be future date Check at Row-" + count);
                            }
                            else
                                EmployeeMasterVModel.EM_DOB = dbirth.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid Date of birth, it should be in DD/MM/YYYY format, Check at Row-" + count);
                        }
                    }
                    else
                    {
                        LogErrors.Add("Invalid Date of birth, Date of birth is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                    {
                        Relation = xlWorksheet.Cells[i, 8].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(Relation, @"^[a-zA-Z]+$");
                        if (matched == false)
                            LogErrors.Add("Invalid Relationship Value-" + Relation + ", it should be Characters only, Check at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_Relationship = Relation;
                        }
                    }
                    else
                    {
                        LogErrors.Add("Relationship is Mandatory, Check at Row-" + count);
                    }
                    //DOJ
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                    {
                        try
                        {
                            if (xlWorksheet.Cells[i, 1].Text.ToString().Trim() != "")
                            {
                                string CommecementDate = RLCS_ClientsManagement.GetCommencementDate(xlWorksheet.Cells[i, 1].Text.ToString().Trim());
                                string DateOfJoining = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                                if (DateTime.ParseExact(DateOfJoining, "dd/MM/yyyy", null) < DateTime.ParseExact(CommecementDate, "dd/MM/yyyy", null))
                                {
                                    LogErrors.Add("Joining Date  greater than Commencement Date. Please Check!,Commencement Date:"+ CommecementDate + "Check at Row-" + count);
                                }
                            }
                            DOJ = xlWorksheet.Cells[i, 9].Text.ToString().Trim();
                            DateTime joiningdate = DateTime.ParseExact(DOJ, "dd/MM/yyyy", null);
                            EmployeeMasterVModel.EM_DOJ = joiningdate.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid Date of joining-" + DOJ + ", it should be in DD/MM/YYYY format, Check at Row-" + count);
                        }
                    }
                    else
                    {
                        LogErrors.Add("Date of joining is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 10].Text.ToString().Trim()))
                    {
                        employment = xlWorksheet.Cells[i, 10].Text.ToString().Trim();
                        if (employment.ToUpper() == "FIRST TIME" || employment.ToUpper() == "F")
                            EmployeeMasterVModel.EM_Firsttime_secondtime = "F";
                        else if (employment.ToUpper() == "SECOND TIME" || employment.ToUpper() == "S")
                            EmployeeMasterVModel.EM_Firsttime_secondtime = "S";

                        else
                            LogErrors.Add("Invalid Employment, Possible Value will be First time or Second time, Check at Row-" + count);
                    }
                    else
                    {
                        LogErrors.Add("Employment Value will be First time or Second time are Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 11].Text.ToString().Trim()))
                    {

                        internation = xlWorksheet.Cells[i, 11].Text.ToString().Trim();
                        if (internation.ToUpper() == "YES" || internation.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_International_workers = "Y";
                        else if (internation.ToUpper() == "NO" || internation.ToUpper() == "N")
                            EmployeeMasterVModel.EM_International_workers = "N";
                        else
                            LogErrors.Add("Invalid International Worker, it should be Yes or No, Check at Row-" + count);
                    }
                    else
                    {
                        LogErrors.Add("International Worker is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 12].Text.ToString().Trim()))
                    {
                        pfcappingApp = xlWorksheet.Cells[i, 12].Text.ToString().Trim();
                        if (pfcappingApp.ToUpper() == "YES" || pfcappingApp.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_PF_Capping_Applicability = "Y";
                        else if (pfcappingApp.ToUpper() == "NO" || pfcappingApp.ToUpper() == "N")
                            EmployeeMasterVModel.EM_PF_Capping_Applicability = "N";
                        else
                            LogErrors.Add("Invalid PF capping applicability,Enter either Yes or No for PF capping applicability, Check at Row-" + count);
                    }
                    else
                    {
                        LogErrors.Add("PF capping applicability is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 13].Text.ToString().Trim()))
                    {
                        status = xlWorksheet.Cells[i, 13].Text.ToString().Trim();
                        if (status.ToUpper() == "ACTIVE" || status.ToUpper() == "A")
                            EmployeeMasterVModel.EM_Status = "A";
                        else if (status.ToUpper() == "INACTIVE" || status.ToUpper() == "I")
                            EmployeeMasterVModel.EM_Status = "I";
                        else
                            LogErrors.Add("Invalid Status, It should be either Active or Inactive, Check at Row- " + count);
                    }
                    else
                    {
                        LogErrors.Add("Status is Mandatory, Check at Row- " + count);
                    }


                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 14].Text.ToString().Trim()))
                    {
                        Married = xlWorksheet.Cells[i, 14].Text.ToString().Trim();
                        if (Married.ToUpper() == "MARRIED" || Married.ToUpper() == "M")
                            EmployeeMasterVModel.EM_MaritalStatus = "M";
                        else if (Married.ToUpper() == "UNMARRIED" || Married.ToUpper() == "U")
                            EmployeeMasterVModel.EM_MaritalStatus = "U";
                        else
                            LogErrors.Add("Invalid Marital Status, it should be Married or Unmarried, Check at Row-" + count);
                    }
                    else
                    {
                        LogErrors.Add("Marital Status is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 15].Text.ToString().Trim()))
                    {
                        Phychallenged = xlWorksheet.Cells[i, 15].Text.ToString().Trim();
                        if (Phychallenged.ToUpper() == "YES" || Phychallenged.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_PhysicallyChallenged = "Y";
                        else if (Phychallenged.ToUpper() == "NO" || Phychallenged.ToUpper() == "N")
                            EmployeeMasterVModel.EM_PhysicallyChallenged = "N";

                        else
                            LogErrors.Add("Invalid Is Physically Challenged Value, it should be Yes or No, Check at Row-" + count);
                    }
                    else
                    {
                        LogErrors.Add("Physically Challenged Value is Mandatory, Check at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 16].Text.ToString().Trim()))
                    {
                        try
                        {
                            Pmon = xlWorksheet.Cells[i, 16].Text.ToString().Trim();
                            payrollmnth = Convert.ToInt32(Pmon);
                            if (payrollmnth > 12)
                                LogErrors.Add("Payroll month should be between 1 to 12 at Row-" + count);
                            else
                            {
                                if (Convert.ToInt32(Pmon) > 0)
                                    EmployeeMasterVModel.EM_PayrollMonth = payrollmnth;
                                else
                                    LogErrors.Add("Invalid Payroll Month " + Pmon + ".Only positive numbers allowed, Check at Row-" + count);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid Payroll month-" + Pmon + ", it should be Numbers only , Check at Row-" + count);
                        }


                    }
                    else
                    {
                        LogErrors.Add("Payroll Month is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 17].Text.ToString().Trim()))
                    {
                        try
                        {
                            pyear = xlWorksheet.Cells[i, 17].Text.ToString().Trim();
                            payrollyear = Convert.ToInt32(pyear);
                            if (payrollyear > DateTime.Now.Year)
                            {
                                LogErrors.Add("Invalid payroll year-" + pyear + ", it should be Numbers only, Check at Row-" + count);
                            }
                            else
                            {
                                if (pyear.Length != 4)
                                {
                                    LogErrors.Add("Invalid payroll year-" + pyear + ", Check at Row-" + count);
                                }
                                else
                                {
                                    if (Convert.ToInt64(pyear) > 0)
                                        if (Convert.ToInt32(pyear) >= 1981)
                                        {
                                            EmployeeMasterVModel.EM_PayrollYear = Convert.ToInt32(pyear);
                                        }
                                        else
                                        {
                                            LogErrors.Add("Invalid Payroll Year-" + pyear + ". Payroll Year Should not be less than 1981, Check at Row-" + count);
                                        }
                                    else
                                        LogErrors.Add("Invalid Payroll Year-" + pyear + ". Only positive numbers allowed, Check at Row-" + count);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid payroll year-" + pyear + ", it should be in Numbers only, Check at Row-" + count);
                        }
                    }
                    else
                    {
                        LogErrors.Add("Payroll year is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 18].Text.ToString().Trim()))
                    {
                        try
                        {
                            Cmonth = xlWorksheet.Cells[i, 18].Text.ToString().Trim();
                            couragemnth = Convert.ToInt32(Cmonth);
                            if (couragemnth > 12)
                                LogErrors.Add("Courage month should be between 1 to 12 at Row-" + count);
                            else
                            {
                                if (Convert.ToInt32(Cmonth) > 0)
                                    EmployeeMasterVModel.EM_ESI_Out_of_Courage_Month = couragemnth;
                                else
                                    LogErrors.Add("Invalid Courage Month-" + Cmonth + ".Only positive numbers allowed, Check at Row-" + count);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid courage month-" + Cmonth + ", it should be in Numbers only, Check at Row-" + count);
                        }
                    }
                    else
                    {
                        LogErrors.Add("Courage Month is Mandatory  Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 19].Text.ToString().Trim()))
                    {
                        try
                        {
                            cYear = xlWorksheet.Cells[i, 19].Text.ToString().Trim();
                            courageyear = Convert.ToInt32(cYear);
                            if (courageyear > DateTime.Now.Year)
                            {
                                LogErrors.Add("Invalid courage Year, it can not be future year, Check at Row-" + count);
                            }
                            else
                            {
                                if (pyear.Length != 4)
                                {
                                    LogErrors.Add("Invalid courage year, Check at Row-" + count);
                                }
                                else
                                {
                                    if (Convert.ToInt32(cYear) >= 2016)
                                    {
                                        if (Convert.ToInt32(cYear) > 0)
                                            EmployeeMasterVModel.EM_ESI_Out_of_Courage_Year = Convert.ToInt32(cYear);
                                        else
                                            LogErrors.Add("Invalid courage year-" + cYear + ", Only Numbers allowed, Check at Row-" + count);
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid courage year-" + cYear + ", courage Year should be greater than '2015', Check at Row-" + count);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid courage year-" + cYear + ", it should be Numbers only, Check at Row-" + count);
                        }
                    }
                    else
                    {
                        LogErrors.Add("Courage year is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 20].Text.ToString().Trim()))
                    {
                        Security = xlWorksheet.Cells[i, 20].Text.ToString().Trim();
                        if (Security.ToUpper() == "YES" || Security.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_SecurityProvided = "Y";
                        else if (Security.ToUpper() == "NO" || Security.ToUpper() == "N")
                            EmployeeMasterVModel.EM_SecurityProvided = "N";
                        else
                        {
                            if (string.IsNullOrEmpty(EmployeeMasterVModel.EM_SecurityProvided))
                                LogErrors.Add("Invalid security provided " + Security + " it should be yes no ,Check at Row-" + count);
                        }
                    }
                    else
                    {
                        LogErrors.Add("Security provided is Mandatory,Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 21].Text.ToString().Trim()))
                    {
                        womenworkinNight = xlWorksheet.Cells[i, 21].Text.ToString().Trim();
                        if (womenworkinNight.ToUpper() == "YES" || womenworkinNight.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_WomenWorkingNightshift = "Y";
                        else if (womenworkinNight.ToUpper() == "NO" || womenworkinNight.ToUpper() == "N")
                            EmployeeMasterVModel.EM_WomenWorkingNightshift = "N";

                        else
                            LogErrors.Add("Invalid either Yes or No for women working night shift " + womenworkinNight + " at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 22].Text.ToString().Trim()))
                    {
                        string Dept = xlWorksheet.Cells[i, 22].Text.ToString().Trim();
                        bool Department = Regex.IsMatch(Dept, @"^[a-zA-Z .-]+$");
                        //
                        if (Department == false)
                            LogErrors.Add("Invalid Department-" + Dept + ", it should be Characters only, Check at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_Department = Dept;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 23].Text.ToString().Trim()))
                    {
                        string state = xlWorksheet.Cells[i, 23].Text.ToString().Trim();
                        var PTstate = RLCS_ClientsManagement.GetPTStateRecordbyCode(state);
                        if (PTstate == null)
                            LogErrors.Add("No PT State found with " + state + ", or PT not applicable to State-" + state + ", Check at Row-" + count);
                        else
                            EmployeeMasterVModel.EM_PT_State = PTstate.SM_Code;


                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 24].Text.ToString().Trim()))
                    {
                        string DTDOL = xlWorksheet.Cells[i, 24].Text.ToString().Trim();
                        try
                        {
                            DateTime LeavingDate = DateTime.ParseExact(DTDOL, "dd/MM/yyyy", null);
                            EmployeeMasterVModel.EM_DOL = LeavingDate.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid Date of Leaving-" + DTDOL + ", it should be in DD/MM/YYYY format, Check at Row-" + count);
                        }
                    }
                    //25
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 25].Text.ToString().Trim()))
                    {
                        string EmailID = xlWorksheet.Cells[i, 25].Text.ToString().Trim();
                        try
                        {
                            Regex regex = new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                            Match match = regex.Match(EmailID);
                            if (match.Success)
                                EmployeeMasterVModel.EM_Emailid = EmailID;
                            else
                                LogErrors.Add("Invalid EmailID, Check at Row-" + count);
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid EmailID-" + EmailID + ", Check at Row-" + count);
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 26].Text.ToString().Trim()))
                    {
                        string MobileNo = xlWorksheet.Cells[i, 26].Text.ToString().Trim();
                        bool valid = MobileNo.All(char.IsDigit);
                        if (valid == false)
                            LogErrors.Add("Invalid Mobile No-" + MobileNo + ", it should be Numbers only, Check at Row-" + count);
                        else
                        {
                            if (MobileNo.Length != 10)
                                LogErrors.Add("Mobile no must be of 10 digit, Check at Row-" + count);
                            else
                            {
                                if (Convert.ToInt64(MobileNo) > 0)
                                    EmployeeMasterVModel.EM_MobileNo = MobileNo;
                                else
                                    LogErrors.Add("Invalid mobile no-" + MobileNo + ", it should be positive numbers only, Check at Row-" + count);
                            }
                        }
                    }
                    //27
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 27].Text.ToString().Trim()))
                    {
                        skillC = xlWorksheet.Cells[i, 27].Text.ToString().Trim();
                        bool Skill = Regex.IsMatch(skillC, @"^[a-zA-Z ]+$");

                        if (Skill == false)
                            LogErrors.Add("Invalid Skill " + skillC + ", it should be Characters only, Check at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_SkillCategory = skillC;
                        }
                    }
                    else
                    {
                        LogErrors.Add("Skill is Mandatory, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 28].Text.ToString().Trim()))
                    {
                        string PassportNo = xlWorksheet.Cells[i, 28].Text.ToString().Trim();
                        bool Passport = Regex.IsMatch(PassportNo, @"^[a-zA-Z0-9]+$");
                        //
                        if (Passport == false)
                            LogErrors.Add("Invalid Passport No-" + PassportNo + ", it should be Characters and Numbers only, Check at Row-" + count);
                        else
                        {
                            bool Exist = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 28, PassportNo);
                            if (Exist)
                                LogErrors.Add("Duplicate Passport No-" + PassportNo + ", it should be unique, Check at Row -" + count);
                            else
                                EmployeeMasterVModel.EM_PassportNo = PassportNo;
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 29].Text.ToString().Trim()))
                    {
                        try
                        {
                            Changeeffectdate = xlWorksheet.Cells[i, 29].Text.ToString().Trim();
                            DateTime effectdate = DateTime.ParseExact(Changeeffectdate, "dd/MM/yyyy", null);
                            EmployeeMasterVModel.EM_ChangeEffective_from = effectdate.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid Change effective From Date, Check at Row-" + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 30].Text.ToString().Trim()))
                    {
                        EmployeeMasterVModel.EM_Address = xlWorksheet.Cells[i, 30].Text.ToString().Trim();
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 31].Text.ToString().Trim()))
                    {
                        RLCS_ClientsManagement map = new RLCS_ClientsManagement();
                        string EXDesignation = xlWorksheet.Cells[i, 31].Text.ToString().Trim();

                        bool Designation = Regex.IsMatch(EXDesignation, @"^[a-zA-Z ]+$");
                        string EMDesignation = RLCS_ClientsManagement.GetAllDesignation(EXDesignation);

                        if (!string.IsNullOrEmpty(EMDesignation))
                        {
                            if (Designation == false)
                            {
                                LogErrors.Add("Invalid Designation-" + EXDesignation + ", it should be Characters only, Check at Row-" + count);
                            }
                            else
                            {
                                EmployeeMasterVModel.EM_Designation = EMDesignation;
                            }
                        }
                        else
                        {
                            LogErrors.Add("Invalid Designation-" + EXDesignation + ", Designation is not present, Check at Row-" + count);
                        }
                    }
                    //ESICNO
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 32].Text.ToString().Trim()))
                    {
                        string ESICNO = xlWorksheet.Cells[i, 32].Text.ToString().Trim();
                        bool ESIC = ESICNO.All(char.IsDigit);

                        if (ESIC == false)
                            LogErrors.Add("Invalid ESIC No-" + ESICNO + ", it should be Numbers only, Check at Row -" + count);
                        else
                        {
                            if (Convert.ToInt64(ESICNO) > 0)
                            {
                                bool Exist = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 32, ESICNO);
                                if (Exist)
                                {
                                    if (Update)
                                    {
                                        EmployeeMasterVModel.EM_ESICNO = ESICNO;
                                    }
                                    else
                                    {
                                        LogErrors.Add("Duplicate ESIC No-" + ESICNO + ", it should be unique, Check at Row -" + count);
                                    }
                                }

                                else
                                {
                                    if (RLCS_ClientsManagement.CheckESICCode(ESICNO))
                                        if (Update)
                                        {
                                            if (ESICNO.Length <= 10)
                                            {
                                                EmployeeMasterVModel.EM_ESICNO = ESICNO;
                                            }
                                            else
                                            {
                                                LogErrors.Add("ESIC No-" + ESICNO + " can have a maximum of 10 digits, Check at Row -" + count);
                                            }
                                        }
                                        else
                                        {
                                            LogErrors.Add("ESIC No-" + ESICNO + " already present in database, it should be unique, Check at Row -" + count);
                                        }

                                    else
                                    {
                                        if (ESICNO.Length<=10)
                                        {
                                            EmployeeMasterVModel.EM_ESICNO = ESICNO;
                                        }
                                        else
                                        {
                                            LogErrors.Add("ESIC No-" + ESICNO + " can have a maximum of 10 digits, Check at Row -" + count);
                                        }
                                       

                                    }

                                }

                            }

                            else
                                LogErrors.Add("Invalid ESIC No-" + ESICNO + ".Only positive numbers allowed, Check at Row-" + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 33].Text.ToString().Trim()))
                    {
                        string UANNo = xlWorksheet.Cells[i, 33].Text.ToString().Trim();
                        bool PAN = Regex.IsMatch(UANNo, @"^[0-9]+$");
                        //
                        if (PAN == false)
                            LogErrors.Add("Invalid UAN No-" + UANNo + ", it should be Numbers only, Check at Row-" + count);
                        else
                        {
                            if (UANNo.Length != 12)
                                LogErrors.Add("UAN No must be of 12 digits at Row-" + count);
                            else
                            {
                                if (Convert.ToInt64(UANNo) > 0)
                                {
                                    bool Exist = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 33, UANNo);
                                    if (Exist)
                                    {
                                        if (Update)
                                        {
                                            EmployeeMasterVModel.EM_UAN = UANNo;
                                        }
                                        else
                                        {
                                            LogErrors.Add("Duplicate UAN No-" + UANNo + ", it should be unique, Check at Row -" + count);
                                        }
                                    }
                                    else
                                    {
                                        if (RLCS_ClientsManagement.CheckUANCode(UANNo))
                                            if (Update)
                                            {
                                                EmployeeMasterVModel.EM_UAN = UANNo;
                                            }
                                            else
                                            {
                                                LogErrors.Add("UAN No-" + UANNo + " already present in database, it should be unique, Check at Row -" + count);
                                            }

                                        else
                                            EmployeeMasterVModel.EM_UAN = UANNo;
                                    }
                                }

                                else
                                    LogErrors.Add("Invalid UAN NO-" + UANNo + ", it should be positive numbers only, Check at Row- " + count);
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 34].Text.ToString().Trim()))
                    {
                        string PanNo = xlWorksheet.Cells[i, 34].Text.ToString().Trim();
                        bool PAN = Regex.IsMatch(PanNo, @"^[a-zA-Z0-9]+$");
                        //
                        if (PAN == false)
                            LogErrors.Add("Invalid PAN No-" + PanNo + ", it should be Characters and Numbers only, Check at Row-" + count);
                        else
                        {
                            if (PanNo.Length != 10)
                                LogErrors.Add("PAN No must be of 10 digit Check at Row-" + count);
                            else
                                EmployeeMasterVModel.EM_PAN = PanNo;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 35].Text.ToString().Trim()))
                    {
                        string AdharNo = xlWorksheet.Cells[i, 35].Text.ToString().Trim();

                        bool valid = AdharNo.All(char.IsDigit);
                        if (valid == false)
                            LogErrors.Add("Invalid Aadhar No-" + AdharNo + ", it should be Numbers only, Check at Row-" + count);
                        else
                        {
                            if (AdharNo.Length != 12)
                                LogErrors.Add("Aadhar no must be 12 digit at Row- " + count);
                            else
                            {
                                if (Convert.ToInt64(AdharNo) > 0)
                                    EmployeeMasterVModel.EM_Aadhar = AdharNo;
                                else
                                    LogErrors.Add("Invalid Aadhar No-" + AdharNo + ", Only positive numbers allowed, Check at Row-" + count);
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 36].Text.ToString().Trim()))
                    {
                        string BankName = xlWorksheet.Cells[i, 36].Text.ToString().Trim();
                        bool bank = Regex.IsMatch(BankName, @"^[a-zA-Z ]+$");
                        //
                        if (bank == false)
                            LogErrors.Add("Invalid Bank Name-" + BankName + ", it should be Characters only, Check at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_BankName = BankName;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 37].Text.ToString().Trim()))
                    {
                        string BankAccountNo = xlWorksheet.Cells[i, 37].Text.ToString().Trim();
                        if (BankAccountNo.Length < 8 || BankAccountNo.Length > 20)
                            LogErrors.Add("Bank A/c No must be between 8 to 20 digits-" + BankAccountNo + ", Check at Row-" + count);
                        else
                            EmployeeMasterVModel.EM_Bankaccountnumber = BankAccountNo;
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 38].Text.ToString().Trim()))
                    {
                        try
                        {
                            passvalidfrom = xlWorksheet.Cells[i, 38].Text.ToString().Trim();
                            DateTime pvalidfrm = DateTime.ParseExact(passvalidfrom, "dd/MM/yyyy", null);
                            EmployeeMasterVModel.EM_Passport_Valid_From = pvalidfrm.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid Passport Valid (From), Date should be in DD/MM/YYYY Format, Check at Row-" + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 39].Text.ToString().Trim()))
                    {
                        try
                        {
                            if (DateTime.ParseExact(xlWorksheet.Cells[i, 38].Text.ToString().Trim(), "dd/MM/yyyy", null) < DateTime.ParseExact(xlWorksheet.Cells[i, 39].Text.ToString().Trim(), "dd/MM/yyyy", null))
                            {
                                passvalidto = xlWorksheet.Cells[i, 39].Text.ToString().Trim();
                                DateTime pvalidto = DateTime.ParseExact(passvalidto, "dd/MM/yyyy", null);
                                EmployeeMasterVModel.EM_Passport_Valid_Upto = pvalidto.ToString();
                            }
                            else
                            {
                                LogErrors.Add("Invalid Passport Valid (To), Date should be greater than Passport Valid (From), Check at Row-" + count);
                            }

                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid Passport Valid (To), Date should be in DD/MM/YYYY Format, Check at Row-" + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 40].Text.ToString().Trim()))
                    {
                        string EPFOAdhar = xlWorksheet.Cells[i, 40].Text.ToString().Trim();
                        if (EPFOAdhar.ToUpper() == "YES" || EPFOAdhar.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_EPFO_Aadhar_Upload = "Y";
                        else if (EPFOAdhar.ToUpper() == "NO" || EPFOAdhar.ToUpper() == "N")
                            EmployeeMasterVModel.EM_EPFO_Aadhar_Upload = "N";

                        else
                            LogErrors.Add("Invalid EPFO adhar upload " + EPFOAdhar + " it should be yes or no for  at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 41].Text.ToString().Trim()))
                    {
                        string IFSCNO = xlWorksheet.Cells[i, 41].Text.ToString().Trim();
                        bool valid = Regex.IsMatch(IFSCNO, @"^[a-zA-Z0-9]+$");
                        if (valid == false)
                            LogErrors.Add("Invalid IFSC No-" + IFSCNO + ", it should be Characters and Numbers only, Check at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_IFSC = IFSCNO;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 42].Text.ToString().Trim()))
                    {
                        string Nationality = xlWorksheet.Cells[i, 42].Text.ToString().Trim();
                        bool valid = Regex.IsMatch(Nationality, @"^[a-zA-Z ]+$");
                        //
                        if (valid == false)
                            LogErrors.Add("Invalid Nationality " + Nationality + ", it should be Characters only, Check at Row-" + count);
                        else
                        {
                            string EmCountry = RLCS_ClientsManagement.GetRLCSCountry(Nationality);
                            if (string.IsNullOrEmpty(EmCountry))
                                LogErrors.Add("Invalid Nationality Check at Row-" + count);
                            else
                                EmployeeMasterVModel.EM_Nationality = EmCountry;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 43].Text.ToString().Trim()))
                    {
                        string PF_Applicability = xlWorksheet.Cells[i, 43].Text.ToString().Trim();
                        if (PF_Applicability.ToUpper() == "YES" || PF_Applicability.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_PF_Applicability = "Y";
                        else if (PF_Applicability.ToUpper() == "NO" || PF_Applicability.ToUpper() == "N")
                            EmployeeMasterVModel.EM_PF_Applicability = "N";
                        else
                            LogErrors.Add("Invalid PF applicability-" + PF_Applicability + ", it should be Yes or No, Check at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 44].Text.ToString().Trim()))
                    {
                        string ESI_Applicability = xlWorksheet.Cells[i, 44].Text.ToString().Trim();
                        if (ESI_Applicability.ToUpper() == "YES" || ESI_Applicability.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_ESI_Applicability = "Y";
                        else if (ESI_Applicability.ToUpper() == "NO" || ESI_Applicability.ToUpper() == "N")
                            EmployeeMasterVModel.EM_ESI_Applicability = "N";
                        else
                            LogErrors.Add("Invalid ESI applicability-" + ESI_Applicability + ", it should be Yes or No, Check at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 45].Text.ToString().Trim()))
                    {
                        string PFNO = xlWorksheet.Cells[i, 45].Text.ToString().Trim();
                        bool PF = Regex.IsMatch(PFNO, @"^[A-Za-z0-9-//\\]+$");
                        //
                        if (PF == false)
                            LogErrors.Add("Invalid PF No-" + PFNO + ", it should be Characters and Numbers only, Check at Row -" + count);

                        else
                        {
                            if (PFNO.Length > 22)
                            {
                                LogErrors.Add(" PF NO-" + PFNO + "can have a maximum of 22 digits, Check at Row -" + count);
                            }
                            else
                            {
                                bool Exist = RLCS_DocumentManagement.checkDuplicateDataExistExcelSheet(xlWorksheet, i, 45, PFNO);
                                if (Exist)
                                {
                                    if (Update)
                                    {
                                        EmployeeMasterVModel.EM_PFNO = PFNO;
                                    }
                                    else
                                    {
                                        LogErrors.Add("Duplicate PF No-" + PFNO + ", it should be unique, Check at Row -" + count);
                                    }
                                }
                                else
                                {
                                    if (RLCS_ClientsManagement.CheckPFCode(PFNO))
                                        if (Update)
                                        {
                                            EmployeeMasterVModel.EM_PFNO = PFNO;
                                        }
                                        else
                                        {
                                            LogErrors.Add("pf No-" + PFNO + " already present in database, it should be unique, Check at Row -" + count);
                                        }

                                    else
                                        EmployeeMasterVModel.EM_PFNO = PFNO;
                                }
                            }

                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 46].Text.ToString().Trim()))
                    {
                        string EPFO_BankAcc = xlWorksheet.Cells[i, 46].Text.ToString().Trim();
                        if (EPFO_BankAcc.ToUpper() == "YES" || EPFO_BankAcc.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_EPFO_Bank_Ac_Upload = "Y";
                        else if (EPFO_BankAcc.ToUpper() == "NO" || EPFO_BankAcc.ToUpper() == "N")
                            EmployeeMasterVModel.EM_EPFO_Bank_Ac_Upload = "N";

                        else
                            LogErrors.Add("Invalid EPFO bank a/c upload " + EPFO_BankAcc + " it should be  yes or no, Check at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 47].Text.ToString().Trim()))
                    {
                        string EPFO_PAN = xlWorksheet.Cells[i, 47].Text.ToString().Trim();
                        if (EPFO_PAN.ToUpper() == "YES" || EPFO_PAN.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_EPFO_PAN_Upload = "Y";
                        else if (EPFO_PAN.ToUpper() == "NO" || EPFO_PAN.ToUpper() == "N")
                            EmployeeMasterVModel.EM_EPFO_PAN_Upload = "N";

                        else
                            LogErrors.Add("Invalid EPFO pan upload " + EPFO_PAN + "it should be yes or no,Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 48].Text.ToString().Trim()))
                    {
                        string PassportCountry = xlWorksheet.Cells[i, 48].Text.ToString().Trim();
                        bool Nationality = Regex.IsMatch(PassportCountry, @"^[a-zA-Z ]+$");
                        //
                        if (Nationality == false)
                            LogErrors.Add("Invalid Passport Issued country " + PassportCountry + ", It should be in Characters Check at Row-" + count);
                        else
                        {
                            string EmCountry = RLCS_ClientsManagement.GetRLCSCountry(PassportCountry);
                            if (string.IsNullOrEmpty(EmCountry))
                                LogErrors.Add("Invalid Passport Issued country-" + PassportCountry + ", it does not exist in our database, Check at Row-" + count);
                            else
                                EmployeeMasterVModel.EM_PassportIssued_Country = EmCountry;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 49].Text.ToString().Trim()))
                    {
                        string PMPRY = xlWorksheet.Cells[i, 49].Text.ToString().Trim();
                        if (PMPRY.ToUpper() == "YES" || PMPRY.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_PMRPY = "Y";
                        else if (PMPRY.ToUpper() == "NO" || PMPRY.ToUpper() == "N")
                            EmployeeMasterVModel.EM_PMRPY = "N";

                        else
                            LogErrors.Add("Invalid PMPRY" + PMPRY + " it should be yes or no ,Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 50].Text.ToString().Trim()))
                    {
                        string PT_Applicability = xlWorksheet.Cells[i, 50].Text.ToString().Trim();
                        if (PT_Applicability.ToUpper() == "YES" || PT_Applicability.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_PT_Applicability = "Y";
                        else if (PT_Applicability.ToUpper() == "NO" || PT_Applicability.ToUpper() == "N")
                            EmployeeMasterVModel.EM_PT_Applicability = "N";

                        else
                            LogErrors.Add("Invalid PT Applicability " + PT_Applicability + " it should be yes or no, Check at Row-" + count);
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 51].Text.ToString().Trim()))
                    {
                        string NoOfCertificate = xlWorksheet.Cells[i, 51].Text.ToString().Trim();
                        bool valid = NoOfCertificate.All(char.IsDigit);
                        if (valid == false)
                            LogErrors.Add("Invalid No Of Certificate-" + NoOfCertificate + ", it should be Numbers only, Check at Row-" + count);
                        else
                        {
                            if (Convert.ToInt64(NoOfCertificate) > 0)
                                EmployeeMasterVModel.EM_NoOf_Certificate = NoOfCertificate;
                            else
                                LogErrors.Add("Invalid No Of Certificate-" + NoOfCertificate + ", Check at Row- " + count);

                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 52].Text.ToString().Trim()))
                    {
                        string NoOfCertificateDate = xlWorksheet.Cells[i, 52].Text.ToString().Trim();
                        try
                        {
                            DateTime certificatedate = DateTime.ParseExact(NoOfCertificateDate, "dd/MM/yyyy", null);
                            EmployeeMasterVModel.EM_NoOf_Certificate_Date = certificatedate.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid Certificate date  " + NoOfCertificateDate + " at Row-" + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 53].Text.ToString().Trim()))
                    {
                        string TokenNo = xlWorksheet.Cells[i, 53].Text.ToString().Trim();
                        bool valid = Regex.IsMatch(TokenNo, @"^[a-zA-Z0-9]+$");
                        //
                        if (valid == false)
                            LogErrors.Add("Invalid Token No " + TokenNo + " it should be Characters and Numbers only,Check at Row -" + count);
                        else
                        {
                            if (Convert.ToInt32(TokenNo) > 0)
                                EmployeeMasterVModel.EM_TokenNo = TokenNo;
                            else
                                LogErrors.Add("Invalid Token No " + TokenNo + ".Enter positive value, Check at Row- " + count);
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 54].Text.ToString().Trim()))
                    {
                        string RelayAssign = xlWorksheet.Cells[i, 54].Text.ToString().Trim();
                        bool token = Regex.IsMatch(RelayAssign, @"^[a-zA-Z ]+$");
                        //
                        if (token == false)
                            LogErrors.Add("Invalid valid Relay assigned " + RelayAssign + " it should be Characters only ,Check at Row -" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_Relay_Assigned = RelayAssign;
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 55].Text.ToString().Trim()))
                    {
                        string LetterGroup = xlWorksheet.Cells[i, 55].Text.ToString().Trim();
                        bool valid = Regex.IsMatch(LetterGroup, @"^[a-zA-Z ]+$");
                        //
                        if (valid == false)
                            LogErrors.Add("Invalid valid Letter of Group " + LetterGroup + " it should be Charaters only,Check at Row -" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_Letter_Of_Group = LetterGroup;
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 56].Text.ToString().Trim()))
                    {
                        string TransportFacility = xlWorksheet.Cells[i, 56].Text.ToString().Trim();
                        if (TransportFacility.ToUpper() == "CAB")
                            EmployeeMasterVModel.EM_ModeofTransport = "Cab";
                        else if (TransportFacility.ToUpper() == "TRANSPORT FACILITY")
                            EmployeeMasterVModel.EM_ModeofTransport = "Transport facility";
                        else
                        {
                            if (String.IsNullOrEmpty(EmployeeMasterVModel.EM_ModeofTransport))
                                LogErrors.Add("Invalid Transport facility " + TransportFacility + " it should be Cab or Transport at Row- " + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 57].Text.ToString().Trim()))
                    {
                        string YearsofExperience = xlWorksheet.Cells[i, 57].Text.ToString().Trim();
                        bool valid = Regex.IsMatch(YearsofExperience, @"[0-9]+(\.[0-9][0-9]?)?");

                        if (valid == false)
                            LogErrors.Add("Invalid valid Years of Experience" + YearsofExperience + " at Row -" + count);
                        else
                        {
                            if (Convert.ToDecimal(YearsofExperience) > 0)
                                EmployeeMasterVModel.EM_YearsOfExperience = YearsofExperience;
                            else
                                LogErrors.Add("Invalid Years of Experience " + YearsofExperience + ".Enter positive value, Check at Row- " + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 58].Text.ToString().Trim()))
                    {
                        employment = xlWorksheet.Cells[i, 58].Text.ToString().Trim();
                        if (employment.ToUpper() == "CONTRACT" || employment.ToUpper() == "C")
                            EmployeeMasterVModel.EM_Employmenttype = "C";
                        else if (employment.ToUpper() == "PERMANENT" || employment.ToUpper() == "P")
                            EmployeeMasterVModel.EM_Employmenttype = "P";
                        else
                            LogErrors.Add("Invalid Employment Type, it should be Contract or Permanent, Check at Row-" + count);
                    }
                    else
                    {
                        LogErrors.Add("Employment Type is Mandatory, at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 59].Text.ToString().Trim()))
                    {
                        string ClientPTState = xlWorksheet.Cells[i, 59].Text.ToString().Trim();
                        var PTstate = RLCS_ClientsManagement.GetPTStateRecordbyCode(ClientPTState);

                        if (PTstate == null)
                            LogErrors.Add("No PT State found with " + ClientPTState + ", or PT not applicable to State-" + ClientPTState + ", Check at Row-" + count);
                        else
                            EmployeeMasterVModel.EM_Client_PT_State = PTstate.SM_Code;
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 60].Text.ToString().Trim()))
                    {
                        string EPSApplicabilty = xlWorksheet.Cells[i, 60].Text.ToString().Trim();
                        if (EPSApplicabilty.ToUpper().Trim().Equals("YES") || EPSApplicabilty.ToUpper().Trim().Equals("Y"))
                            EmployeeMasterVModel.EM_EPS_Applicabilty = "Y";
                        else if (EPSApplicabilty.ToUpper().Trim().Equals("NO") || EPSApplicabilty.ToUpper().Trim().Equals("N"))
                            EmployeeMasterVModel.EM_EPS_Applicabilty = "N";
                        else
                            LogErrors.Add("Invalid either yes or no for EPS Applicabilty " + EPSApplicabilty + "at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 61].Text.ToString().Trim()))
                    {
                        string ClientESINO = xlWorksheet.Cells[i, 61].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(ClientESINO, @"^[0-9]+$");
                        if (matched == false)
                            LogErrors.Add("Client ESI Number " + ClientESINO + " must be number only  at Row- " + count);
                        else
                        {
                            try
                            {
                                if (Convert.ToInt64(ClientESINO) > 0)
                                {
                                    if (ClientESINO.Length <= 17)
                                    {
                                        EmployeeMasterVModel.EM_Client_ESI_Number = ClientESINO;
                                    }
                                    else
                                    {
                                        LogErrors.Add(" Client ESI NO-" + ClientESINO + "can have a maximum of 17 digits, Check at Row -" + count);
                                    }
                                    
                                }
                                else
                                {
                                    LogErrors.Add("Invalid Client ESI NO " + ClientESINO + ".Enter positive value, Check at Row- " + count);
                                }
                            }
                            catch (Exception)
                            {
                                LogErrors.Add("Invalid Client ESI NO " + ClientESINO + ".Number Length Exceed, Check at Row- " + count);
                            }
                        }
                    }
                    ////------------*New Add Employee Master Fields*------------////
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 62].Text.ToString().Trim()))
                    {
                        string PermanentAddress = xlWorksheet.Cells[i, 62].Text.ToString().Trim();
                        EmployeeMasterVModel.EM_PermanentAddress = PermanentAddress;
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 63].Text.ToString().Trim()))
                    {
                        string Mark_Identity = xlWorksheet.Cells[i, 63].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(Mark_Identity, @"^[a-zA-Z ]+$");
                        if (matched == false)
                            LogErrors.Add("Invalid Valid Identification mark" + Mark_Identity + " at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_Markof_Identification = Mark_Identity;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 64].Text.ToString().Trim()))
                    {
                            string Em_ContactAddress = xlWorksheet.Cells[i, 64].Text.ToString().Trim();
                            EmployeeMasterVModel.EM_Emergency_contact_Address = Em_ContactAddress;
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 65].Text.ToString().Trim()))
                    {
                        string Em_ContactNo = xlWorksheet.Cells[i, 65].Text.ToString().Trim();
                        bool valid = Em_ContactNo.All(char.IsDigit);
                        if (valid == false)
                            LogErrors.Add("Enter valid Emergency mobile no" + Em_ContactNo + " at Row- " + count);
                        else
                        {
                            if (Em_ContactNo.Length != 10)
                                LogErrors.Add("Emergency mobile no must be 10 digit " + Em_ContactNo + " at Row- " + count);
                            else
                                EmployeeMasterVModel.EM_Emergency_contact_mobile_no = Em_ContactNo;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 66].Text.ToString().Trim()))
                    {
                        string TrainingNo = xlWorksheet.Cells[i, 66].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(TrainingNo, @"^[a-zA-Z0-9 ]+$");
                        if (matched == false)
                            LogErrors.Add("Training Number must be number only at Row- " + count);
                        else
                            EmployeeMasterVModel.EM_Training_Number = TrainingNo;
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 67].Text.ToString().Trim()))
                    {
                        string TrainingDate = xlWorksheet.Cells[i, 67].Text.ToString().Trim();
                        try
                        {
                            DateTime DtTrainingDate = DateTime.ParseExact(TrainingDate, "dd/MM/yyyy", null);
                            EmployeeMasterVModel.EM_Training_Date = DtTrainingDate.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Enter valid date for training date given at Row-" + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 68].Text.ToString().Trim()))
                    {
                        string PlaceOfWork = xlWorksheet.Cells[i, 68].Text.ToString().Trim();

                        if (PlaceOfWork.ToUpper() == "UNDERGROUND")
                            EmployeeMasterVModel.EM_Placeof_work = PlaceOfWork;
                        else if (PlaceOfWork.ToUpper() == "OPEN CAST")
                            EmployeeMasterVModel.EM_Placeof_work = PlaceOfWork;
                        else if (PlaceOfWork.ToUpper() == "SURFACE")
                            EmployeeMasterVModel.EM_Placeof_work = PlaceOfWork;
                        else
                            LogErrors.Add("Invalid Place Of Work- Underground or Open cast or surface for place of work at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 69].Text.ToString().Trim()))
                    {
                        string Remark = xlWorksheet.Cells[i, 69].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(Remark, @"^[a-zA-Z ]+$");
                        if (matched == false)
                            LogErrors.Add("Invalid Valid Remark" + Remark + " at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_Remarks = Remark;
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 70].Text.ToString().Trim()))
                    {
                        string EducationalLevel = xlWorksheet.Cells[i, 70].Text.ToString().Trim();
                        EmployeeMasterVModel.EM_EductionLevel = EducationalLevel;
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 71].Text.ToString().Trim()))
                    {
                        string PlaceOfEmployment = xlWorksheet.Cells[i, 71].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(PlaceOfEmployment, @"^[a-zA-Z ]+$");
                        if (matched == false)
                            LogErrors.Add("Enter Valid place of employment" + PlaceOfEmployment + " at Row-" + count);
                        else
                        {
                            EmployeeMasterVModel.EM_Place_of_Employment = PlaceOfEmployment;
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 72].Text.ToString().Trim()))
                    {
                        string ClothDate = xlWorksheet.Cells[i, 72].Text.ToString().Trim();
                        try
                        {
                            DateTime DtClothDate = DateTime.ParseExact(ClothDate, "dd/MM/yyyy", null);
                            EmployeeMasterVModel.EM_DateWhenClothesGiven = DtClothDate.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid valid date for date when cloths given " + ClothDate + " at Row-" + count);
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 73].Text.ToString().Trim()))
                    {
                        string ExemptingOrderDate = xlWorksheet.Cells[i, 73].Text.ToString().Trim();
                        try
                        {
                            DateTime DtExemptingOrderDate = DateTime.ParseExact(ExemptingOrderDate, "dd/MM/yyyy", null);
                            EmployeeMasterVModel.EM_NumberandDateOfExemptingOrder = DtExemptingOrderDate.ToString();
                        }
                        catch (Exception ex)
                        {
                            LogErrors.Add("Invalid valid date for Number and date of exempting order date " + ExemptingOrderDate + " at Row-" + count);
                        }
                    }
                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 74].Text.ToString().Trim()))
                    {
                        //([a-zA-Z ,]+)
                        string TransferOneToAnother = xlWorksheet.Cells[i, 74].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(TransferOneToAnother, @"([a-zA-Z ,]+)");
                        if (matched == false)
                        {
                            LogErrors.Add("Enter Valid Transfer One Group ToAnother" + TransferOneToAnother + " at Row-" + count);
                        }
                        else
                        {
                            EmployeeMasterVModel.EM_ParticularsOfTransferFromOneGroupToAnother = TransferOneToAnother;
                        }
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 75].Text.ToString().Trim()))
                    {
                        string SalePromotion = xlWorksheet.Cells[i, 75].Text.ToString().Trim();
                        if (SalePromotion.ToUpper() == "YES" || SalePromotion.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_SalesPromotion = "Y";
                        else if (SalePromotion.ToUpper() == "NO" || SalePromotion.ToUpper() == "N")
                            EmployeeMasterVModel.EM_SalesPromotion = "N";
                        else
                            LogErrors.Add("Invalid either yes or no for sales promotion " + SalePromotion + "at Row-" + count);
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 76].Text.ToString().Trim()))
                    {
                        string PaymentMode = xlWorksheet.Cells[i, 76].Text.ToString().Trim();
                        bool matched = Regex.IsMatch(PaymentMode, @"([a-zA-Z ,]+)");
                        if (matched == false)
                        {
                            LogErrors.Add("Enter Valid PaymentMode " + PaymentMode + " at Row-" + count);
                        }
                        else
                        {
                            EmployeeMasterVModel.EM_PaymentMode = PaymentMode;
                        }
                        
                    }

                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 77].Text.ToString().Trim()))
                    {
                        string lwfExempted = xlWorksheet.Cells[i, 77].Text.ToString().Trim();
                        if (lwfExempted.ToUpper() == "YES" || lwfExempted.ToUpper() == "Y")
                            EmployeeMasterVModel.EM_IsLwf_Exempted = true;
                        else if (lwfExempted.ToUpper() == "NO" || lwfExempted.ToUpper() == "N")
                            EmployeeMasterVModel.EM_IsLwf_Exempted = false;
                        else
                            LogErrors.Add("Invalid Is Lwf Exempted, Enter either Yes or No for Is Lwf Exempted, Check at Row-" + count);
                    }
                    #endregion

                    employeeModelList.Add(EmployeeMasterVModel);
                }

                if (EmployeeMasterVModel != null && LogErrors.Count <= 0)
                {
                    int c = 1;
                    long EmployeeCount = 0;
                    EmployeeCount = employeeModelList.Count;
                    using (ComplianceDBEntities entity = new ComplianceDBEntities())
                    {

                    }

                    #region 
                    foreach (EmployeeMasterVModel employee in employeeModelList)
                    {
                        if (LogErrors.Count <= 0)
                        {
                            EmployeeMasterVModel.UploadEmployee.ExcelErrors = false;
                            bool EmployeeUp = false;

                            RLCS_Employee_Master RLCSEmployee = new RLCS_Employee_Master();
                            TinyMapper.Bind<EmployeeMasterVModel, RLCS_Employee_Master>(config =>
                            {
                                config.Ignore(source => source.EM_DOB);
                                config.Ignore(source => source.EM_DOJ);
                                config.Ignore(source => source.EM_DOL);
                                config.Ignore(source => source.EM_NoOf_Certificate_Date);
                                config.Ignore(source => source.EM_Training_Date);
                                config.Ignore(source => source.EM_Passport_Valid_From);
                                config.Ignore(source => source.EM_Passport_Valid_Upto);
                                config.Ignore(source => source.EM_NumberandDateOfExemptingOrder);
                                config.Ignore(source => source.EM_ChangeEffective_from);
                                config.Ignore(source => source.EM_DateWhenClothesGiven);

                            });

                            RLCSEmployee = TinyMapper.Map<RLCS_Employee_Master>(employee);

                            RLCSEmployee.AVACOM_CustomerID = employee.AVACOMCustomerID;
                            RLCSEmployee.AVACOM_BranchID = employee.AVACOM_BranchID;
                            RLCSEmployee.EM_Location = employee.EM_Location;
                            RLCSEmployee.EM_State = employee.EM_State;
                            RLCSEmployee.EM_Employmenttype = employee.EM_Employmenttype;
                            RLCSEmployee.EM_PassportNo = employee.EM_PassportNo;
                            RLCSEmployee.EM_NoOf_Certificate = employee.EM_NoOf_Certificate;
                            RLCSEmployee.EM_WomenWorkingNightshift = employee.EM_WomenWorkingNightshift;
                            RLCSEmployee.EM_Relay_Assigned = employee.EM_Relay_Assigned;
                            RLCSEmployee.EM_ParticularsOfTransferFromOneGroupToAnother = employee.EM_ParticularsOfTransferFromOneGroupToAnother;
                            RLCSEmployee.EM_SalesPromotion = employee.EM_SalesPromotion;
                            RLCSEmployee.EM_PaymentMode = employee.EM_PaymentMode;
                            RLCSEmployee.EM_Training_Number = employee.EM_Training_Number;
                            RLCSEmployee.EM_Emergency_contact_Address = employee.EM_Emergency_contact_Address;
                            RLCSEmployee.EM_Emergency_contact_mobile_no = employee.EM_Emergency_contact_mobile_no;
                            RLCSEmployee.EM_Status = employee.EM_Status;
                            RLCSEmployee.EM_SkillCategory = employee.EM_SkillCategory;
                            RLCSEmployee.EM_EPS_Applicabilty = employee.EM_EPS_Applicabilty;
                            RLCSEmployee.EM_Client_ESI_Number = employee.EM_Client_ESI_Number;
                            RLCSEmployee.EM_Nationality = employee.EM_Nationality;
                            RLCSEmployee.EM_Designation = employee.EM_Designation;
                            RLCSEmployee.EM_Department = employee.EM_Department;
                            RLCSEmployee.EM_ExistReasonCode = employee.EM_ExistReasonCode;
                            RLCSEmployee.EM_Client_PT_State = employee.EM_Client_PT_State;
                            RLCSEmployee.EM_ESICNO = employee.EM_ESICNO;
                            RLCSEmployee.EM_NoOf_Certificate = employee.EM_NoOf_Certificate;
                            RLCSEmployee.EM_PF_Capping_Applicability = employee.EM_PF_Capping_Applicability;
                            RLCSEmployee.EM_ParticularsOfTransferFromOneGroupToAnother = employee.EM_ParticularsOfTransferFromOneGroupToAnother;
                            RLCSEmployee.EM_SalesPromotion = employee.EM_SalesPromotion;
                            RLCSEmployee.EM_PaymentMode = employee.EM_PaymentMode;
                            RLCSEmployee.EM_PermanentAddress = employee.EM_PermanentAddress;
                            RLCSEmployee.EM_Markof_Identification = employee.EM_Markof_Identification;
                            RLCSEmployee.EM_Placeof_work = employee.EM_Placeof_work;
                            RLCSEmployee.EM_Remarks = employee.EM_Remarks;
                            RLCSEmployee.EM_EductionLevel = employee.EM_EductionLevel;

                            RLCSEmployee.EM_Place_of_Employment = employee.EM_Place_of_Employment;
                            RLCSEmployee.EM_Training_Number = employee.EM_Training_Number;
                            RLCSEmployee.EM_Emergency_contact_Address = employee.EM_Emergency_contact_Address;
                            RLCSEmployee.EM_Emergency_contact_mobile_no = employee.EM_Emergency_contact_mobile_no;
                            RLCSEmployee.EM_IsLwf_Exempted = employee.EM_IsLwf_Exempted;

                            if (!String.IsNullOrEmpty(employee.EM_DOB))
                                RLCSEmployee.EM_DOB = DateTime.Parse(employee.EM_DOB);
                          
                            if (!String.IsNullOrEmpty(employee.EM_DOJ))
                                RLCSEmployee.EM_DOJ = DateTime.Parse(employee.EM_DOJ);
                          

                            if (!String.IsNullOrEmpty(employee.EM_DOL))
                                RLCSEmployee.EM_DOL = DateTime.Parse(employee.EM_DOL);

                            if (!String.IsNullOrEmpty(employee.EM_NoOf_Certificate_Date))
                                RLCSEmployee.EM_NoOf_Certificate_Date = DateTime.Parse(employee.EM_NoOf_Certificate_Date);

                            if (!String.IsNullOrEmpty(employee.EM_Training_Date))
                                RLCSEmployee.EM_Training_Date = DateTime.Parse(employee.EM_Training_Date);
                            if (!String.IsNullOrEmpty(employee.EM_Passport_Valid_From))
                                RLCSEmployee.EM_Passport_Valid_From = DateTime.Parse(employee.EM_Passport_Valid_From);
                         
                            if (!String.IsNullOrEmpty(employee.EM_Passport_Valid_Upto))
                                RLCSEmployee.EM_Passport_Valid_Upto = DateTime.Parse(employee.EM_Passport_Valid_Upto);
                        
                            if (!String.IsNullOrEmpty(employee.EM_NumberandDateOfExemptingOrder))
                                RLCSEmployee.EM_NumberandDateOfExemptingOrder = DateTime.Parse(employee.EM_NumberandDateOfExemptingOrder).ToString("dd/MM/yyyy");
                            if (!String.IsNullOrEmpty(employee.EM_ChangeEffective_from))
                                RLCSEmployee.EM_ChangeEffective_from = DateTime.Parse(employee.EM_ChangeEffective_from);
                          
                            if (!String.IsNullOrEmpty(employee.EM_DateWhenClothesGiven))
                                RLCSEmployee.EM_DateWhenClothesGiven = DateTime.Parse(employee.EM_DateWhenClothesGiven);

                            if (Update)
                            {
                                EmployeeUp = RLCS_ClientsManagement.CreateUpdateEmployeeMaster(RLCSEmployee);
                            }
                            else
                            {
                                EmployeeUp = RLCS_ClientsManagement.CreateUpdateEmployeeMaster(RLCSEmployee);
                            }

                            if (EmployeeUp && EmployeeCount <= 500)
                            {
                                bulkUpload.EM_ClientID = employee.EM_ClientID;
                                bulkUpload.EM_Bankaccountnumber = employee.EM_Bankaccountnumber;
                                bulkUpload.EM_BankName = employee.EM_BankName;
                                bulkUpload.EM_Aadhar = employee.EM_Aadhar;
                                bulkUpload.EM_Address = employee.EM_Address;
                                bulkUpload.EM_Branch = employee.EM_Branch;
                                bulkUpload.EM_Location = employee.EM_Location;
                                bulkUpload.EM_Emailid = employee.EM_Emailid;
                                bulkUpload.EM_MaritalStatus = employee.EM_MaritalStatus;
                                bulkUpload.EM_Relationship = employee.EM_Relationship;
                                bulkUpload.EM_MobileNo = employee.EM_MobileNo;
                                bulkUpload.EM_State = employee.EM_State;
                                bulkUpload.EM_PT_State = employee.EM_PT_State;
                                bulkUpload.EM_Gender = employee.EM_Gender;
                                bulkUpload.EM_FatherName = employee.EM_FatherName;

                                bulkUpload.EM_EmpID = employee.EM_EmpID;
                                bulkUpload.EM_EmpName = employee.EM_EmpName;

                                if (!String.IsNullOrEmpty(employee.EM_DOB))
                                    bulkUpload.EM_DOB = DateTime.Parse(employee.EM_DOB);
                                if (!String.IsNullOrEmpty(employee.EM_DOJ))
                                    bulkUpload.EM_DOJ = DateTime.Parse(employee.EM_DOJ);
                                if (!String.IsNullOrEmpty(employee.EM_DOL))
                                    bulkUpload.EM_DOL = DateTime.Parse(employee.EM_DOL);
                                if (!String.IsNullOrEmpty(employee.EM_NoOf_Certificate_Date))
                                    bulkUpload.EM_NoOf_Certificate_Date = DateTime.Parse(employee.EM_NoOf_Certificate_Date);
                                if (!String.IsNullOrEmpty(employee.EM_Training_Date))
                                    bulkUpload.EM_Training_Date = DateTime.Parse(employee.EM_Training_Date);
                                if (!String.IsNullOrEmpty(employee.EM_Passport_Valid_From))
                                    bulkUpload.EM_Passport_Valid_From = DateTime.Parse(employee.EM_Passport_Valid_From);
                                if (!String.IsNullOrEmpty(employee.EM_Passport_Valid_Upto))
                                    bulkUpload.EM_Passport_Valid_Upto = DateTime.Parse(employee.EM_Passport_Valid_Upto);
                                if (!String.IsNullOrEmpty(employee.EM_NumberandDateOfExemptingOrder))
                                    bulkUpload.EM_NumberandDateOfExemptingOrder = employee.EM_NumberandDateOfExemptingOrder;

                                bulkUpload.EM_PFNO = employee.EM_PFNO;
                                bulkUpload.EM_ESICNO = employee.EM_ESICNO;
                                bulkUpload.EM_UAN = employee.EM_UAN;
                                bulkUpload.EM_PAN = employee.EM_PAN;
                                bulkUpload.EM_International_workers = employee.EM_International_workers;
                                bulkUpload.EM_IFSC = employee.EM_IFSC;
                                bulkUpload.EM_Department = employee.EM_Department;
                                bulkUpload.EM_Designation = employee.EM_Designation;
                                bulkUpload.EM_SkillCategory = employee.EM_SkillCategory;
                                bulkUpload.EM_PF_Capping_Applicability = employee.EM_PF_Capping_Applicability;
                                bulkUpload.EM_ModeofTransport = employee.EM_ModeofTransport;
                                bulkUpload.EM_PassportNo = employee.EM_PassportNo;
                                bulkUpload.EM_PhysicallyChallenged = employee.EM_PhysicallyChallenged;
                                bulkUpload.EM_Firsttime_secondtime = employee.EM_Firsttime_secondtime;
                                bulkUpload.EM_Status = employee.EM_Status;
                                bulkUpload.EM_EmploymentType = employee.EM_Employmenttype;
                                bulkUpload.EM_Nationality = employee.EM_Nationality;
                                bulkUpload.EM_PayrollMonth = employee.EM_PayrollMonth;
                                bulkUpload.EM_PayrollYear = employee.EM_PayrollYear;
                                bulkUpload.EM_ESI_Out_of_Courage_Month = employee.EM_ESI_Out_of_Courage_Month;
                                bulkUpload.EM_PF_Applicability = employee.EM_PF_Applicability;
                                bulkUpload.EM_ESI_Applicability = employee.EM_ESI_Applicability;
                                bulkUpload.EM_PassportIssued_Country = employee.EM_PassportIssued_Country;
                                bulkUpload.EM_EPFO_Aadhar_Upload = employee.EM_EPFO_Aadhar_Upload;
                                bulkUpload.EM_EPFO_Bank_Ac_Upload = employee.EM_EPFO_Bank_Ac_Upload;
                                bulkUpload.EM_EPFO_PAN_Upload = employee.EM_EPFO_PAN_Upload;
                                bulkUpload.EM_PMRPY = employee.EM_PMRPY;
                                bulkUpload.EM_NoOf_Certificate = employee.EM_NoOf_Certificate;
                                bulkUpload.EM_TokenNo = employee.EM_TokenNo;
                                bulkUpload.EM_Relay_Assigned = employee.EM_Relay_Assigned;
                                bulkUpload.EM_Letter_Of_Group = employee.EM_Letter_Of_Group;
                                bulkUpload.EM_WomenWorkingNightshift = employee.EM_WomenWorkingNightshift;
                                bulkUpload.EM_SecurityProvided = employee.EM_SecurityProvided;
                                bulkUpload.EM_ExistReasonCode = employee.EM_ExistReasonCode;
                                bulkUpload.EM_YearsOfExperience = employee.EM_YearsOfExperience;
                                bulkUpload.EM_ParticularsOfTransferFromOneGroupToAnother = employee.EM_ParticularsOfTransferFromOneGroupToAnother;
                                bulkUpload.EM_SalesPromotion = employee.EM_SalesPromotion;
                                bulkUpload.EM_PaymentMode = employee.EM_PaymentMode;
                                bulkUpload.EM_PermanentAddress = employee.EM_PermanentAddress;
                                bulkUpload.EM_Placeof_work = employee.EM_Placeof_work;
                                bulkUpload.EM_Remarks = employee.EM_Remarks;
                                bulkUpload.EM_EductionLevel = employee.EM_EductionLevel;
                                bulkUpload.EM_Place_of_Employment = employee.EM_Place_of_Employment;
                                bulkUpload.EM_Training_Number = employee.EM_Training_Number;
                                bulkUpload.EM_Emergency_contact_Address = employee.EM_Emergency_contact_Address;
                                bulkUpload.EM_Emergency_contact_mobile_no = employee.EM_Emergency_contact_mobile_no;
                                bulkUpload.EM_EPS_Applicabilty = employee.EM_EPS_Applicabilty;
                                bulkUpload.EM_Client_ESI_Number = employee.EM_Client_ESI_Number;
                                bulkUpload.EM_Client_PT_State = employee.EM_Client_PT_State;
                                bulkUpload.EM_Markof_Identification = employee.EM_Markof_Identification;
                                bulkUpload.EM_PT_Applicability = employee.EM_PT_Applicability;
                                bulkUpload.EM_IsLwf_Exempted = employee.EM_IsLwf_Exempted;
                                //string Url = "EmployeeMaster/Save_EmployeeMasterBulk";
                                //EmployeeUploadMasterVModel.CreatedBy = "Avantis";
                                //bool ApiSuccess = EmployeeApiCall(bulkUpload, Url);
                                //if (ApiSuccess)
                                //{
                                //    EmployeeMasterVModel.UploadEmployee.Message = true;
                                //    if (Update == true)
                                //        RLCS_ClientsManagement.Update_ProcessedStatus_EmployeeMaster(bulkUpload.EM_EmpID, bulkUpload.EM_ClientID, ApiSuccess);
                                //}
                                Flag = true;
                            }
                            bulkUploadALL.Add(bulkUpload);

                        }

                        c++;
                    }
                    #endregion
                    #region MyRegion
                    if (EmployeeCount <= 500 && Flag == true)
                    {
                        string Url = "EmployeeMaster/Save_EmployeeMasterBulk";
                        EmployeeUploadMasterVModel.CreatedBy = "Avantis";
                        bool ApiSuccess = EmployeeApiCall(bulkUpload, Url);
                        if (ApiSuccess)
                        {
                            EmployeeMasterVModel.UploadEmployee.Message = true;
                            if (Update == true)
                                RLCS_ClientsManagement.Update_ProcessedStatus_EmployeeMaster(bulkUpload.EM_EmpID, bulkUpload.EM_ClientID, ApiSuccess);
                        }
                    }
                    else
                    {
                        EmployeeMasterVModel.UploadEmployee.Message = true;
                    }
                    #endregion
                }
                else
                {
                    UploadEmployeeDocumentVModel UploadEmployee = new UploadEmployeeDocumentVModel();
                    string path = "";
                    EmployeeMasterVModel.UploadEmployee.FileName = RLCS_WriteLog.WriteLog(LogErrors, "Employee Master Excel Sheet", out path);
                    EmployeeMasterVModel.UploadEmployee.ExcelErrors = true;

                }

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                EmployeeMasterVModel.Exception = true;
            }

            return EmployeeMasterVModel;
        }

        public bool EmployeeApiCall(EmployeeNewUploadModel Employee, string Url)
        {
            List<EmployeeNewUploadModel> Employeelist = new List<EmployeeNewUploadModel>();

            try
            {
                HttpClient client = new HttpClient();
                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                rlcsAPIURL += Url;
                Employeelist.Add(Employee);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.PostAsJsonAsync<List<EmployeeNewUploadModel>>(rlcsAPIURL, Employeelist).Result;
                if (response.IsSuccessStatusCode)
                {
                    // get the rest/content of the response in a synchronous way
                    var Content = response.Content.ReadAsStringAsync().Result;

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool SingleEmployeeApiUpdate(EmployeeUploadMasterVModel EmployeeUploadMasterVModel, string Url)
        {
            EmployeeUploadMasterVModel EmployeeMaster = new EmployeeUploadMasterVModel();
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://rlcsuat3api.teamlease.com/");
                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.PostAsJsonAsync<EmployeeUploadMasterVModel>(Url, EmployeeUploadMasterVModel).Result;
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpGet]
        public ActionResult EmployeeMasterList()
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                return View();
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        [HttpPost]
        public JsonResult CheckBranch(string AVACOM_BranchID)
        {

            bool ifBranchchExist = false;
            int? CustID = 0;
            try
            {
                if (Session["CustID"] != null)
                {
                    if (Convert.ToInt32(Session["CustID"]) > 0)
                        CustID = Convert.ToInt32(Session["CustID"]);
                    else
                    {
                        CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                }
                else
                {
                    CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (!String.IsNullOrEmpty(AVACOM_BranchID) && CustID > 0)
                {
                    ifBranchchExist = RLCS_ClientsManagement.CheckBranchExist(AVACOM_BranchID, CustID);
                }

                return Json(ifBranchchExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }


        [HttpPost]
        public JsonResult ISEmpIDExistCheckExist(string EmpID, string CustID)
        {

            bool ifSequenceExist = false;

            try
            {

                if (!String.IsNullOrEmpty(EmpID))
                {
                    ifSequenceExist = RLCS_ClientsManagement.CheckEmployeeID(Convert.ToString(EmpID), Convert.ToInt32(CustID));
                }

                return Json(ifSequenceExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult CheckPFExist(string EM_PFNO)
        {

            bool ifPFExist = false;
            try
            {

                if (!String.IsNullOrEmpty(EM_PFNO))
                {
                    ifPFExist = RLCS_ClientsManagement.CheckPFCode(EM_PFNO);
                }

                return Json(ifPFExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult CheckESICExist(string ESIC)
        {

            bool ifPFExist = false;
            try
            {
                if (!String.IsNullOrEmpty(ESIC))
                {
                    ifPFExist = RLCS_ClientsManagement.CheckESICCode(ESIC);
                }

                return Json(ifPFExist, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult GetCommDateforNewEmployee(string BranchID)
        {

            string CDate = "";
            try
            {
                if (!String.IsNullOrEmpty(BranchID))
                {
                    CDate = RLCS_ClientsManagement.GetCommDateforNewEmployee(BranchID);
                }

                return Json(CDate, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)

            {

                return Json(CDate, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult CheckUANExist(string UAN)
        {

            bool ifPFExist = false;
            try
            {
                if (!String.IsNullOrEmpty(UAN))
                {
                    ifPFExist = RLCS_ClientsManagement.CheckUANCode(UAN);
                }

                return Json(ifPFExist, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }
        #endregion

        #region Common
        // to get cities using state code
        public ActionResult GetCities(string state)
        {
            List<CityVModel> CityVM = new List<CityVModel>();
            List<City> cityList = new List<City>();
            BasicSetupModel BasicSetup = new BasicSetupModel();
            try
            {

                cityList = RLCS_ClientsManagement.GetCities(state);
                if (cityList != null && cityList.Count > 0)
                {
                    TinyMapper.Bind<List<City>, List<CityVModel>>();
                    CityVM = TinyMapper.Map<List<CityVModel>>(cityList);

                    //BasicSetup.CityList.AddRange(cityList);
                }
            }
            catch (Exception Ex)
            {

            }
            return Json(CityVM, JsonRequestBehavior.AllowGet);
        }

        // To get locations using state code
        public ActionResult GetLocations(string state)
        {
            List<RLCS_Location_City_Mapping> LocationsList = new List<RLCS_Location_City_Mapping>();
            //  List<City> cityList = new List<City>();
            BasicSetupModel BasicSetup = new BasicSetupModel();
            try
            {
                LocationsList = RLCS_ClientsManagement.GetLocationCitiesByStateCode(state);
            }
            catch (Exception Ex)
            {

            }
            return Json(LocationsList, JsonRequestBehavior.AllowGet);
        }
        public static List<SP_GetAllAnchor_Result> GetAllLocationsAnchor(int DistributorID, int CustomerID)
        {
            List<SP_GetAllAnchor_Result> LocationsAnchorList = new List<SP_GetAllAnchor_Result>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    LocationsAnchorList = (from c in entities.SP_GetAllAnchor( DistributorID, CustomerID) select c).Distinct().ToList();
                    return LocationsAnchorList;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #region Employee New Added Common Methods
        //Add New Common Methods
        public ActionResult BindSector(string state)
        {
            List<Proc_ESIC_SECTOR_MASTERDATA_Result> SectorsList = new List<Proc_ESIC_SECTOR_MASTERDATA_Result>();
            BasicSetupModel BasicSetup = new BasicSetupModel();
            try
            {
                SectorsList = RLCS_ClientsManagement.GetSector(state);
            }
            catch (Exception Ex)
            {

            }
            return Json(SectorsList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BindJobCategory(string state, int SectorID)
        {
            List<Proc_ESIC_GROUP_MASTERDATA_Result> JobCategoryList = new List<Proc_ESIC_GROUP_MASTERDATA_Result>();
            BasicSetupModel BasicSetup = new BasicSetupModel();
            try
            {
                JobCategoryList = RLCS_ClientsManagement.GetJobCategory(state, SectorID);
            }
            catch (Exception Ex)
            {

            }
            return Json(JobCategoryList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BindIndustryType(string state, int SectorID, int JobCategoryID)
        {
            List<Proc_ESIC_IndustryTypeDATA_Result> IndustryTypeList = new List<Proc_ESIC_IndustryTypeDATA_Result>();
            BasicSetupModel BasicSetup = new BasicSetupModel();
            try
            {
                IndustryTypeList = RLCS_ClientsManagement.GetIndustryType(state, SectorID, JobCategoryID);
            }
            catch (Exception Ex)
            {

            }
            return Json(IndustryTypeList, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Paycode

        [HttpGet]
        public ActionResult CreateClientPaycodeMappingDetails(string ClientID, string ID, string Edit)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                PaycodeMappingModel PaycodeMappingModel = new PaycodeMappingModel();

                try
                {
                    ViewData["Edit"] = null;
                    ViewData["ID"] = null;

                    if (Edit == "YES")
                    {
                        ViewData["ID"] = ID;
                        ViewData["Edit"] = "YES";
                        int id = Convert.ToInt32(ID);
                        RLCS_Client_Paycode_Mapping_Details paycodeDetails = RLCS_ClientsManagement.GetMappedPaycodeDetails(id);

                        if (paycodeDetails != null)
                        {
                            TinyMapper.Bind<RLCS_Client_Paycode_Mapping_Details, PaycodeMappingModel>();
                            PaycodeMappingModel = TinyMapper.Map<PaycodeMappingModel>(paycodeDetails);

                            if (PaycodeMappingModel.CPMD_PayGroup == "standard")
                            {
                                List<StandardColumnModel> lst = new List<StandardColumnModel>();
                                //string[] arrStandard = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "PTGROSS", "ESIGROSS" };
                                //string[] arrStandard = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "ArrearPFGROSS", "PTGROSS", "ArrearPTGROSS", "ESIGROSS", "ArrearESIGROSS", "ReceiptNo_BankTransactionID", "DateofPayment", "SalaryConfirmation", "ArrESIGross", "ArrPFGross", "ArrPTGROSS", "EffectiveDate", "LOPR", "ARRDAYS" };
                                string[] arrStandard = { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "PTGROSS", "ESIGROSS", "RECEIPTNO_BANKTRANSACTIONID", "DATEOFPAYMENT", "SALARYCONFIRMATION", "ARRESIGROSS", "ARRPFGROSS", "ARRPTGROSS", "EFFECTIVEDATE", "LOPR", "ARRDAYS" };

                                foreach (var item in arrStandard)
                                {
                                    StandardColumnModel objColumn = new StandardColumnModel();
                                    objColumn.CPMD_Standard_Column = item;
                                    lst.Add(objColumn);
                                }

                                PaycodeMappingModel.StandardColumns = TinyMapper.Map<List<StandardColumnModel>>(lst);
                            }
                            else if (PaycodeMappingModel.CPMD_PayGroup == "paycodes")
                            {
                                string paycode = PaycodeMappingModel.CPMD_Deduction_Type;
                                if (paycode != null)
                                {
                                    List<RLCS_PayCode_Master> PaycodeMasterModellist = RLCS_ClientsManagement.GetPaycodeByType(paycode);
                                    PaycodeMappingModel.PaycodeMasterModellist = TinyMapper.Map<List<PaycodeMasterModel>>(PaycodeMasterModellist);
                                }
                            }

                        }
                    }

                    PaycodeMappingModel.CPMD_ClientID = ClientID;
                    var Clients = RLCS_ClientsManagement.GetAll_client();

                    if (Clients != null && Clients.Count > 0)
                    {
                        TinyMapper.Bind<List<RLCS_CustomerBranch_ClientsLocation_Mapping>, List<ClientModel>>();
                        PaycodeMappingModel.Clients = TinyMapper.Map<List<ClientModel>>(Clients);
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    // BasicSetup.Exception = true;
                }

                return View(PaycodeMappingModel);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        [HttpPost]
        public ActionResult CreateClientPaycodeMappingDetails(PaycodeMappingModel PaycodeMappingModel)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                List<RLCS_Client_Paycode_Mapping_Details> LstPaycodedetails = new List<RLCS_Client_Paycode_Mapping_Details>();
                RLCS_Client_Paycode_Mapping_Details paycodedetails = new RLCS_Client_Paycode_Mapping_Details();

                try
                {
                    int CustID = 0;
                    if (ModelState.IsValid)
                    {
                        if (!string.IsNullOrEmpty(PaycodeMappingModel.CPMD_ClientID))
                        {
                            CustID = RLCS_ClientsManagement.GetCustomerIDByClientID(PaycodeMappingModel.CPMD_ClientID);

                        }
                        List<RLCS_Client_Paycode_Mapping> PaycodeList = new List<RLCS_Client_Paycode_Mapping>();
                        RLCS_Client_Paycode_Mapping newRecord = new RLCS_Client_Paycode_Mapping()
                        {
                            AVACOM_CustomerID = CustID,
                            CPM_ClientID = PaycodeMappingModel.CPMD_ClientID,
                            CPM_PayGroup = "Default",
                            CPM_CreatedBy = "Avantis",
                            CPM_Status = "A"
                        };
                        PaycodeList.Add(newRecord);
                        //bool sucess = RLCS_ClientsManagement.CreateUpdate_ClientPaycodeMapping(newRecord); 
                        //if(sucess)
                        //{

                        TinyMapper.Bind<PaycodeMappingModel, RLCS_Client_Paycode_Mapping_Details>();
                        paycodedetails = TinyMapper.Map<RLCS_Client_Paycode_Mapping_Details>(PaycodeMappingModel);
                        paycodedetails.CPMD_Standard_Column = paycodedetails.CPMD_Standard_Column ?? "";

                        if (paycodedetails.CPMD_PayCode == null)
                            paycodedetails.CPMD_PayCode = "";

                        paycodedetails.AVACOM_CustomerID = CustID;
                        LstPaycodedetails.Add(paycodedetails);
                        bool sucess = RLCS_ClientsManagement.CreateUpdate_ClientPaycodeMapping(PaycodeList, LstPaycodedetails);
                        if (sucess)
                        {
                            PaycodeMappingModel.CPMD_PayGroup = "";
                            PaycodeMappingModel.CPMD_Header = "";
                            PaycodeMappingModel.CPMD_Sequence_Order = 0;

                            PaycodeMappingModel.Message = true;
                            ModelState.Clear();
                        }


                        //}
                    }
                    var Clients = RLCS_ClientsManagement.GetAll_client();
                    // var StandardColumnList = RLCS_ClientsManagement.GetAll_StandardColumnsForPaycode();

                    if (Clients != null && Clients.Count > 0)
                    {
                        TinyMapper.Bind<List<RLCS_CustomerBranch_ClientsLocation_Mapping>, List<ClientModel>>();
                        PaycodeMappingModel.Clients = TinyMapper.Map<List<ClientModel>>(Clients);

                    }
                    //if (StandardColumnList != null && StandardColumnList.Count > 0)
                    //{
                    //    TinyMapper.Bind<List<RLCS_Client_Paycode_Mapping_Details>, List<StandardColumnModel>>();
                    //    PaycodeMappingModel.StandardColumns = TinyMapper.Map<List<StandardColumnModel>>(Clients);
                    //}
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    PaycodeMappingModel.Exception = true;
                }

                return View(PaycodeMappingModel);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        public ActionResult UploadPaycodeFile(string Client)
        {
            UploadPaycodeVModel UploadPaycodeVModel = new UploadPaycodeVModel();
            try
            {
                UploadPaycodeVModel.ClientID = Client;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                UploadPaycodeVModel.ServerError = true;
            }

            return View(UploadPaycodeVModel);
        }

        [HttpPost]
        public ActionResult UploadPaycodeFile(FormCollection formCollection, UploadPaycodeVModel UploadPaycodeVModel)
        {
            if (Request != null)
            {
                try
                {
                    string ClientID = UploadPaycodeVModel.ClientID;
                    HttpPostedFileBase file = Request.Files["fileUpload"];

                    if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            UploadPaycodeVModel.Error = false;

                            string fileName = file.FileName;
                            string fileWithoutExtn = Path.GetFileNameWithoutExtension(fileName);
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];
                            bool canread = file.InputStream.CanRead;
                            var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                            using (var package = new ExcelPackage(file.InputStream))
                            {

                                UploadPaycodeVModel.PaycodeMappingModel = ProcessPaycodeDetails(package, fileWithoutExtn, ClientID);

                            }
                        }
                        else
                        {
                            UploadPaycodeVModel.Error = true;
                        }
                    }
                    else
                    {
                        // UploadDocumentVModel.Error = true;
                        UploadPaycodeVModel.ExtensionError = true;
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    UploadPaycodeVModel.ServerError = true;
                }
            }
            return View(UploadPaycodeVModel);
        }

        public ActionResult DownloadPaycodeFiles(string Name, string Sample, bool Emp)
        {
            UploadDocumentVModel UploadDocumentVModel = new UploadDocumentVModel();
            UploadEmployeeDocumentVModel EmpUpload = new UploadEmployeeDocumentVModel();

            try
            {
                string filePath = "";
                Sample = Request.QueryString["Sample"];

                if (Sample != null)
                {
                    if (Sample == "DownloadSampleLocation")
                    {
                        filePath = Server.MapPath("~/RLCSDocuments/SamplePaycodeMappingUpload.xlsx");
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, "SamplePaycodeMappingUpload.xlsx");
                    }
                    else
                    {
                        filePath = ConfigurationManager.AppSettings["RLCS_LogFile"] + Name;
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, Name);
                    }
                    // byte[] fileBytes = System.IO.File.ReadAllBytes(file.Filepath);
                }
                else
                {
                    UploadDocumentVModel.ServerError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                if (Emp == true)
                    EmpUpload.ServerError = true;
                else
                    UploadDocumentVModel.ServerError = true;
            }

            return View("UploadLocationFiles", UploadDocumentVModel);
        }


        public ActionResult DownloadPaycodeFiles1(string Name, string Sample, bool Emp)
        {
            UploadDocumentVModel UploadDocumentVModel = new UploadDocumentVModel();
            UploadEmployeeDocumentVModel EmpUpload = new UploadEmployeeDocumentVModel();

            try
            {
                string filePath = "";
                Sample = Request.QueryString["Sample"];

                if (Sample != null)
                {
                    if (Sample == "DownloadSampleLocation")
                    {
                        DownloadPaycodeExcel();
                    }
                    else
                    {
                        filePath = ConfigurationManager.AppSettings["RLCS_LogFile"] + Name;
                        return File(filePath, System.Net.Mime.MediaTypeNames.Application.Octet, Name);
                    }
                }
                else
                {
                    UploadDocumentVModel.ServerError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                if (Emp == true)
                    EmpUpload.ServerError = true;
                else
                    UploadDocumentVModel.ServerError = true;
            }

            return View("UploadLocationFiles", UploadDocumentVModel);
        }

        int GetLastUsedRow(ExcelWorksheet sheet)
        {
            if (sheet.Dimension == null) { return 0; } // In case of a blank sheet
            var row = sheet.Dimension.End.Row;
            while (row >= 1)
            {
                var range = sheet.Cells[row, 1, row, sheet.Dimension.End.Column];
                if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
                {
                    break;
                }
                row--;
            }
            return row;
        }


        public PaycodeMappingModel ProcessPaycodeDetails(ExcelPackage xlWorkbook, string fileName, string ClientID)
        {
            //
            int CustID = 0;

            int count = 0;

            UploadPaycodeVModel UploadPaycodeVModel = new UploadPaycodeVModel();
            PaycodeMappingModel PaycodeMappingModel = new PaycodeMappingModel();

            ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["PaycodeDetails"];
            List<PaycodeMappingModel> LstPaycodedetails = new List<PaycodeMappingModel>();
            List<RLCS_Client_Paycode_Mapping> PaycodeMappingLst = new List<RLCS_Client_Paycode_Mapping>();
            try
            {
                int[] arrSeqNo = { };
                string[] arrstandardType = { };
                string[] arrstandardConstant = { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS"};
                //string[] arrstandardTypeFixed = { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "ARREARPFGROSS", "PTGROSS", "ARREARPTGROSS", "ESIGROSS", "ARREARESIGROSS", "RECEIPTNO_BANKTRANSACTIONID", "DATEOFPAYMENT", "SALARYCONFIRMATION", "ARRESIGROSS", "ARRPFGROSS", "ARRPTGROSS", "EFFECTIVEDATE", "LOPR", "ARRDAYS" };
                string[] arrstandardTypeFixed = { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "PTGROSS", "ESIGROSS", "RECEIPTNO_BANKTRANSACTIONID", "DATEOFPAYMENT", "SALARYCONFIRMATION", "ARRESIGROSS", "ARRPFGROSS", "ARRPTGROSS", "EFFECTIVEDATE", "LOPR", "ARRDAYS" };
                string[] arrClientID = { };
                if (xlWorksheet != null)
                {
                    // CustID = (int)AuthenticationHelper.CustomerID;
                    string Sdate = string.Empty;
                    var noOfRow = GetLastUsedRow(xlWorksheet);

                    Array.Resize(ref arrClientID, arrClientID.Length + 1);
                    arrClientID[arrClientID.GetUpperBound(0)] = ClientID;
                    List<String> LogErrors = new List<string>();

                    #region Validation

                    for (int i = 2; i <= noOfRow; i++)
                    {
                        PaycodeMappingModel = new PaycodeMappingModel();

                        count = i;

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.Trim()))
                            LogErrors.Add("Required ClientID at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                        {
                            string excelClientID = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
                            if (!arrClientID.Contains(excelClientID))
                            {
                                LogErrors.Add("Invalid ClientID :" + excelClientID + " at Row-" + count);
                            }
                            else
                            {
                                bool Client = RLCS_ClientsManagement.CheckPaycodeClient(excelClientID);
                                if (Client == true)
                                {
                                    PaycodeMappingModel.CPMD_ClientID = excelClientID;
                                    if (!string.IsNullOrEmpty(PaycodeMappingModel.CPMD_ClientID))
                                    {
                                        CustID = RLCS_ClientsManagement.GetCustomerIDByClientID(PaycodeMappingModel.CPMD_ClientID);
                                        PaycodeMappingModel.AVACOM_CustomerID = CustID;
                                    }
                                }

                                else
                                    LogErrors.Add("ClientID at Row-" + count + " does not exists.Check at Row - " + count);
                            }

                        }

                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                            LogErrors.Add("Required Header name at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString().Trim()))
                        {
                            string Header = xlWorksheet.Cells[i, 2].Text.ToString().Trim().ToUpper();
                            if (!string.IsNullOrEmpty(PaycodeMappingModel.CPMD_ClientID))
                            {
                                bool HeaderPresent = RLCS_ClientsManagement.CheckHeader(Header, PaycodeMappingModel.CPMD_ClientID, "0");

                                if (HeaderPresent)
                                    LogErrors.Add("Header name is already exist at Row-" + count + ".");
                                else
                                    PaycodeMappingModel.CPMD_Header = Header;
                            }
                        }


                        if(!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()) && xlWorksheet.Cells[i, 3].Text.ToString().Trim().GetType() == typeof(string))
                        {
                            string paycodeType = xlWorksheet.Cells[i, 3].Text.ToString().Trim().ToUpper();

                            if (paycodeType == "Earning" || paycodeType == "EARNING" || paycodeType == "E")
                            {
                                PaycodeMappingModel.CPMD_Deduction_Type = "E";
                                PaycodeMappingModel.CPMD_PayGroup = "paycodes";
                            }
                            else if (paycodeType == "Deduction" || paycodeType == "DEDUCTION" || paycodeType == "D")
                            {
                                PaycodeMappingModel.CPMD_Deduction_Type = "D";
                                PaycodeMappingModel.CPMD_PayGroup = "paycodes";
                            }
                            else if (paycodeType == "STANDARD" || paycodeType == "Standard" || paycodeType == "S")
                            {
                                PaycodeMappingModel.CPMD_Deduction_Type = null;
                                PaycodeMappingModel.CPMD_PayGroup = "standard";
                            }
                            else
                            {
                                LogErrors.Add("Enter valid value for Paycode Type at Row-" + count + ".Enter Earning or Deduction as E or D ");
                            }
                        }
                        else
                        {
                            LogErrors.Add("Required Paycode Type at Row-" + count);
                        }



                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.Trim()))
                            LogErrors.Add("Required Paycode at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString().Trim()))
                        {
                            string PaycodeStandardType = xlWorksheet.Cells[i, 4].Text.ToString().Trim().ToUpper();
                            string PaycodeType = xlWorksheet.Cells[i, 3].Text.ToString().Trim().ToUpper();

                            if (PaycodeType == "EARNING")
                            {
                                PaycodeType = "E";
                            }
                            else if (PaycodeType == "DEDUCTION")
                            {
                                PaycodeType = "D";
                            }
                            else if (PaycodeType == "STANDARD")
                            {
                                PaycodeType = "S";
                            }

                            if (PaycodeType == "E" || PaycodeType == "D")
                            {
                                bool Paycode = RLCS_ClientsManagement.CheckPaycodes(PaycodeStandardType, PaycodeType);

                                if (Paycode)
                                {
                                    string excelClientID = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
                                    bool PaycodeExist = RLCS_ClientsManagement.CheckPaycodesExist(PaycodeStandardType, excelClientID);
                                    if (PaycodeExist)
                                    {
                                        LogErrors.Add("Paycode is already exist at Row-" + count + ".");
                                    }
                                    else
                                    {
                                        PaycodeMappingModel.CPMD_PayCode = PaycodeStandardType;
                                        PaycodeMappingModel.CPMD_Standard_Column = "";
                                    }
                                }
                                else
                                {
                                    LogErrors.Add("Enter valid Paycode:Paycode/Standard Type-, " + PaycodeStandardType + ":" + PaycodeType + " at Row-" + count + ".");
                                }
                            }
                            
                            if (PaycodeType == "S")
                            {
                                Array.Resize(ref arrstandardType, arrstandardType.Length + 1);
                                if (arrstandardType.Contains(PaycodeStandardType))
                                {
                                    LogErrors.Add("Standard Type already Exist, check at Row - " + count);
                                }
                                arrstandardType[arrstandardType.GetUpperBound(0)] = PaycodeStandardType;
                                string excelClientID = xlWorksheet.Cells[i, 1].Text.ToString().Trim();
                                bool PaycodeExist = RLCS_ClientsManagement.CheckStandardPaycodesExist(PaycodeStandardType, excelClientID);
                                if (PaycodeExist)
                                {
                                    LogErrors.Add("Paycode is already exist at Row-" + count + ".");
                                }
                                else
                                {
                                    if (arrstandardTypeFixed.Contains(PaycodeStandardType))
                                    {
                                        PaycodeMappingModel.CPMD_Standard_Column = PaycodeStandardType.Trim().ToUpper();
                                        PaycodeMappingModel.CPMD_PayCode = "";
                                    }
                                    else
                                    {
                                        LogErrors.Add("Invalid Standard Paycode is:" + PaycodeStandardType + " at Row-" + count + ".");
                                    }
                                }
                            }
                        }


                        if (string.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim())) //check state with database or give error
                            LogErrors.Add("Required Sequence code at Row-" + count);
                        else if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim().ToUpper()))
                        {
                            int Sequence = 0;
                            try
                            {
                                Sequence = Convert.ToInt32(xlWorksheet.Cells[i, 5].Text.ToString().Trim());
                            }
                            catch (Exception)
                            {
                                LogErrors.Add("Enter valid Sequence order , check at Row - " + count);
                            }


                            if (!string.IsNullOrEmpty(PaycodeMappingModel.CPMD_ClientID))
                            {
                                Array.Resize(ref arrSeqNo, arrSeqNo.Length + 1);
                                if (arrSeqNo.Contains(Sequence))
                                {
                                    LogErrors.Add("Sequence order already Exist, check at Row - " + count);
                                }
                                arrSeqNo[arrSeqNo.GetUpperBound(0)] = Sequence;
                                var SequencePresent = RLCS_ClientsManagement.CheckSequence(Sequence, PaycodeMappingModel.CPMD_ClientID, 0);

                                if (SequencePresent)
                                {
                                    LogErrors.Add("Sequence order already Exist, check at Row - " + count);

                                }
                                else
                                {
                                    PaycodeMappingModel.CPMD_Sequence_Order = Convert.ToInt32(Sequence);
                                }
                            }
                        }

                        if(!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                        {
                            if(PaycodeMappingModel.CPMD_Deduction_Type == "E" || PaycodeMappingModel.CPMD_Deduction_Type == "D")
                            {
                                if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.Trim()))
                                {
                                    if (xlWorksheet.Cells[i, 6].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 6].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 6].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 6].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 6].Text != null)

                                    {
                                        LogErrors.Add("Invalid value for Applicable for ESI. Enter Yes or No");
                                    }
                                    else
                                    {
                                        if (xlWorksheet.Cells[i, 6].Text.Trim().ToUpper() == "YES" || xlWorksheet.Cells[i, 6].Text.Trim().ToUpper() == "Y")
                                            PaycodeMappingModel.CPMD_appl_ESI = "Y";
                                        else
                                            PaycodeMappingModel.CPMD_appl_ESI = "N";
                                    }
                                }
                                else
                                {
                                    LogErrors.Add("Required Applicable for ESI at Row - " + count);
                                }
                            }
                        }

                        if(!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                        {
                            if (PaycodeMappingModel.CPMD_Deduction_Type == "E" || PaycodeMappingModel.CPMD_Deduction_Type == "D")
                            {
                                if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.Trim()))
                                {
                                    if (xlWorksheet.Cells[i, 7].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 7].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 7].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 7].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 7].Text != null)

                                    {
                                        LogErrors.Add("Invalid value for Applicable for PT. Enter Yes or No");
                                    }
                                    else
                                    {
                                        if (xlWorksheet.Cells[i, 7].Text.Trim().ToUpper() == "YES" || xlWorksheet.Cells[i, 7].Text.Trim().ToUpper() == "Y")
                                            PaycodeMappingModel.CPMD_Appl_PT = "Y";
                                        else
                                            PaycodeMappingModel.CPMD_Appl_PT = "N";
                                    }
                                }
                                else
                                {
                                    LogErrors.Add("Required Applicable for PT at Row - " + count);
                                }
                            }
                        }


                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                        {
                            if (PaycodeMappingModel.CPMD_Deduction_Type == "E" || PaycodeMappingModel.CPMD_Deduction_Type == "D")
                            {
                                if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.Trim()))
                                {
                                    if (xlWorksheet.Cells[i, 8].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 8].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 8].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 8].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 8].Text != null)

                                    {
                                        LogErrors.Add("Invalid value for Applicable for PF. Enter Yes or No");
                                    }
                                    else
                                    {
                                        if (xlWorksheet.Cells[i, 8].Text.Trim().ToUpper() == "YES" || xlWorksheet.Cells[i, 8].Text.Trim().ToUpper() == "Y")
                                            PaycodeMappingModel.CPMD_appl_PF = "Y";
                                        else
                                            PaycodeMappingModel.CPMD_appl_PF = "N";
                                    }
                                }
                                else
                                {
                                    LogErrors.Add("Required Applicable for PF at Row - " + count);
                                }
                            }
                        }

                        if(!String.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.ToString().Trim()))
                        {
                            if (PaycodeMappingModel.CPMD_Deduction_Type == "E" || PaycodeMappingModel.CPMD_Deduction_Type == "D")
                            {
                                if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 9].Text.Trim()))
                                {
                                    if (xlWorksheet.Cells[i, 9].Text.Trim().ToUpper() != "YES" && xlWorksheet.Cells[i, 9].Text.Trim().ToUpper() != "NO" && xlWorksheet.Cells[i, 9].Text.Trim().ToUpper() != "Y" && xlWorksheet.Cells[i, 9].Text.Trim().ToUpper() != "N" && xlWorksheet.Cells[i, 9].Text != null)

                                    {
                                        LogErrors.Add("Invalid value for Applicable for LWF. Enter Yes or No");
                                    }
                                    else
                                    {
                                        if (xlWorksheet.Cells[i, 9].Text.Trim().ToUpper() == "YES" || xlWorksheet.Cells[i, 9].Text.Trim().ToUpper() == "Y")
                                            PaycodeMappingModel.CPMD_Appl_LWF = "Y";
                                        else
                                            PaycodeMappingModel.CPMD_Appl_LWF = "N";
                                    }
                                }
                                else
                                {
                                    LogErrors.Add("Required Applicable for LWF at Row - " + count);
                                }
                            }
                        }

                        LstPaycodedetails.Add(PaycodeMappingModel);

                    }
                    #endregion
                    string strstandard = "";
                    if (arrstandardType.Length > 0)
                    {

                        foreach (var item in arrstandardConstant)
                        {
                            string ExistItem = "";

                            if (arrstandardType.Contains(item))
                            {
                                ExistItem = "T";
                            }
                            else
                            {
                                ExistItem = "F";
                            }
                            if (ExistItem == "F")
                            {
                                LogErrors.Add("Upload Remaining Standard Paycode is:" + item);
                            }
                        }
                    }
                    else
                    {
                        foreach (var item1 in arrstandardConstant)
                        {
                            strstandard += item1 + ",";
                        }

                        LogErrors.Add("Standard Paycode must be Required:" + strstandard);
                    }

                    if (LogErrors.Count <= 0)
                    {
                        #region Save 

                        if (ModelState.IsValid && LogErrors.Count <= 0)
                        {


                            if (LstPaycodedetails != null)
                            {
                                var Clients = LstPaycodedetails.Select(x => x.CPMD_ClientID).Distinct().ToList();


                                Clients.ForEach(each =>
                                {
                                    if (!string.IsNullOrEmpty(each))
                                        CustID = RLCS_ClientsManagement.GetCustomerIDByClientID(each);
                                    RLCS_Client_Paycode_Mapping newRecord = new RLCS_Client_Paycode_Mapping();
                                    newRecord.AVACOM_CustomerID = CustID;
                                    newRecord.CPM_ClientID = each;
                                    newRecord.CPM_PayGroup = "Default";
                                    newRecord.CPM_CreatedBy = "Avantis";
                                    newRecord.CPM_Status = "A";

                                    PaycodeMappingLst.Add(newRecord);

                                    LstPaycodedetails.ForEach(x =>
                                    {
                                        x.CPMD_Status = "A";
                                    });
                                    //standard remove
                                });


                            }



                            RLCS_CustomerBranch_ClientsLocation_Mapping Customerbranch = new RLCS_CustomerBranch_ClientsLocation_Mapping();
                            List<RLCS_Client_Paycode_Mapping_Details> PaycodeDetails = new List<RLCS_Client_Paycode_Mapping_Details>();

                            TinyMapper.Bind<List<PaycodeMappingModel>, List<RLCS_Client_Paycode_Mapping_Details>>();
                            PaycodeDetails = TinyMapper.Map<List<RLCS_Client_Paycode_Mapping_Details>>(LstPaycodedetails);

                            bool sucess = RLCS_ClientsManagement.CreateUpdate_ClientPaycodeMapping(PaycodeMappingLst, PaycodeDetails);
                            if (sucess)
                            {
                                PaycodeMappingModel.Message = true;
                            }

                        }
                        #endregion
                    }
                    else
                    {
                        string path = "";
                        PaycodeMappingModel.FileName = RLCS_WriteLog.WriteLog(LogErrors, fileName, out path);
                        PaycodeMappingModel.ExcelErrors = true;
                    }
                }
                else
                {
                    UploadPaycodeVModel.ServerError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // BasicSetupModel.Exception = true;
            }
            return PaycodeMappingModel;
        }




        [HttpGet]
        public ActionResult GetPaycodeDetails(string Pay_Code, string Client)
        {
            //var PaycodeModellist = new RLCS_Client_Paycode_Mapping_Details();
            //try
            //{
            //    PaycodeModellist = RLCS_ClientsManagement.GetPaycodeDetails(Pay_Code, Client);

            //}
            //catch (Exception Ex)
            //{

            //}
            //return Json(PaycodeModellist, JsonRequestBehavior.AllowGet);


            string MaxRec = RLCS_ClientsManagement.GetPaycodeDetails(Pay_Code, Client);
            return Json(MaxRec, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetStandardColumnList(string Client)
        {
            var PaycodeModellist = new List<object>();
            try
            {
                //string[] arrStandard = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "PTGROSS", "ESIGROSS" };
                //string[] arrStandard = new[] { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "ArrearPFGROSS", "PTGROSS", "ArrearPTGROSS", "ESIGROSS", "ArrearESIGROSS", "ReceiptNo_BankTransactionID", "DateofPayment", "SalaryConfirmation", "ArrESIGross", "ArrPFGross", "ArrPTGROSS", "EffectiveDate", "LOPR", "ARRDAYS" };
                string[] arrStandard = { "CLIENTID", "EMP_ID", "LOP", "NETPAY", "TOTALDEDUCTION", "TOTALEARNING", "WORKINGDAYS", "PFGROSS", "PTGROSS", "ESIGROSS", "RECEIPTNO_BANKTRANSACTIONID", "DATEOFPAYMENT", "SALARYCONFIRMATION", "ARRESIGROSS", "ARRPFGROSS", "ARRPTGROSS", "EFFECTIVEDATE", "LOPR", "ARRDAYS" };
                foreach (var item in arrStandard)
                {
                    PaycodeModellist.Add(item);
                }
               // PaycodeModellist = RLCS_ClientsManagement.GetAll_StandardColumnsForPaycode(Client);

            }
            catch (Exception Ex)
            {

            }
            return Json(PaycodeModellist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPaycodes(string Pay_Code)
        {
            PaycodeMappingModel PaycodeMappingModel = new PaycodeMappingModel();
            var PaycodeMasterModellist = new List<RLCS_PayCode_Master>();
            try
            {

                PaycodeMasterModellist = RLCS_ClientsManagement.GetPaycodeByType(Pay_Code);
            }
            catch (Exception Ex)
            {

            }

            return Json(PaycodeMasterModellist, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ISSequenceOrderExist(string Sequence, string Client, string Id)
        {

            bool ifSequenceExist = false;
            int? CustID = 0;
            try
            {

                if (!String.IsNullOrEmpty(Sequence))
                {
                    ifSequenceExist = RLCS_ClientsManagement.CheckSequence(Convert.ToInt32(Sequence), Client, Convert.ToInt32(Id));
                }

                return Json(ifSequenceExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }


        [HttpPost]
        public JsonResult ISHeaderExist(string Header, string Client, string Id)
        {

            bool ifHeaderExist = false;
            int? CustID = 0;
            try
            {

                if (!String.IsNullOrEmpty(Header))
                {
                    ifHeaderExist = RLCS_ClientsManagement.CheckHeader(Header, Client, Id);
                }

                return Json(ifHeaderExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult ISStandardTypeExist(string Type, string Client)
        {

            bool ifHeaderExist = false;
            int? CustID = 0;
            try
            {

                if (!String.IsNullOrEmpty(Type))
                {
                    ifHeaderExist = RLCS_ClientsManagement.CheckTypeOFStandardExist(Type, Client);
                }
                return Json(ifHeaderExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)
            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }
        [HttpPost]
        public JsonResult ISPayCodeTypeExist(string Type, string Client)
        {

            bool ifHeaderExist = false;
            int? CustID = 0;
            try
            {

                if (!String.IsNullOrEmpty(Type))
                {
                    ifHeaderExist = RLCS_ClientsManagement.CheckTypeOFPaycodeExist(Type, Client);
                }
                return Json(ifHeaderExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)
            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }
        #endregion



        public static bool CheckTimings(string time)
        {
            bool validate = false;

            if (!string.IsNullOrEmpty(time))
            {
                List<Tuple<string, string>> tupleList = new List<Tuple<string, string>>();

                tupleList.Add(new Tuple<string, string>("12:00 AM", "12:00 AM"));
                tupleList.Add(new Tuple<string, string>("12:15 AM", "12:15 AM"));
                tupleList.Add(new Tuple<string, string>("12:30 AM", "12:30 AM"));
                tupleList.Add(new Tuple<string, string>("12:45 AM", "12:45 AM")); tupleList.Add(new Tuple<string, string>("01:00 AM", "01:00 AM"));
                tupleList.Add(new Tuple<string, string>("01:15 AM", "01:15 AM")); tupleList.Add(new Tuple<string, string>("01:30 AM", "01:30 AM"));
                tupleList.Add(new Tuple<string, string>("01:45 AM", "01:45 AM")); tupleList.Add(new Tuple<string, string>("02:00 AM", "02:00 AM"));
                tupleList.Add(new Tuple<string, string>("02:15 AM", "02:15 AM")); tupleList.Add(new Tuple<string, string>("02:30 AM", "02:30 AM"));
                tupleList.Add(new Tuple<string, string>("02:45 AM", "02:45 AM")); tupleList.Add(new Tuple<string, string>("03:00 AM", "03:00 AM"));
                tupleList.Add(new Tuple<string, string>("03:15 AM", "03:15 AM")); tupleList.Add(new Tuple<string, string>("03:30 AM", "03:30 AM"));
                tupleList.Add(new Tuple<string, string>("03:45 AM", "03:45 AM")); tupleList.Add(new Tuple<string, string>("04:00 AM", "04:00 AM")); tupleList.Add(new Tuple<string, string>("04:15 AM", "04:15 AM"));
                tupleList.Add(new Tuple<string, string>("04:30 AM", "04:30 AM")); tupleList.Add(new Tuple<string, string>("04:45 AM", "04:45 AM")); tupleList.Add(new Tuple<string, string>("05:00 AM", "05:00 AM"));
                tupleList.Add(new Tuple<string, string>("05:15 AM", "05:15 AM")); tupleList.Add(new Tuple<string, string>("05:30 AM", "05:30 AM")); tupleList.Add(new Tuple<string, string>("05:45 AM", "05:45 AM"));
                tupleList.Add(new Tuple<string, string>("06:00 AM", "06:00 AM")); tupleList.Add(new Tuple<string, string>("06:15 AM", "06:15 AM")); tupleList.Add(new Tuple<string, string>("06:30 AM", "06:30 AM")); tupleList.Add(new Tuple<string, string>("06:45 AM", "06:45 AM"));
                tupleList.Add(new Tuple<string, string>("07:00 AM", "07:00 AM")); tupleList.Add(new Tuple<string, string>("07:15 AM", "07:15 AM")); tupleList.Add(new Tuple<string, string>("07:30 AM", "07:30 AM")); tupleList.Add(new Tuple<string, string>("07:45 AM", "07:45 AM"));
                tupleList.Add(new Tuple<string, string>("08:00 AM", "08:00 AM")); tupleList.Add(new Tuple<string, string>("08:15 AM", "08:15 AM")); tupleList.Add(new Tuple<string, string>("08:30 AM", "08:30 AM")); tupleList.Add(new Tuple<string, string>("08:45 AM", "08:45 AM"));
                tupleList.Add(new Tuple<string, string>("09:00 AM", "09:00 AM")); tupleList.Add(new Tuple<string, string>("09:15 AM", "09:15 AM")); tupleList.Add(new Tuple<string, string>("09:30 AM", "09:30 AM")); tupleList.Add(new Tuple<string, string>("09:45 AM", "09:45 AM"));
                tupleList.Add(new Tuple<string, string>("10:00 AM", "10:00 AM")); tupleList.Add(new Tuple<string, string>("10:15 AM", "10:15 AM")); tupleList.Add(new Tuple<string, string>("10:30 AM", "10:30 AM")); tupleList.Add(new Tuple<string, string>("10:45 AM", "10:45 AM"));
                tupleList.Add(new Tuple<string, string>("11:00 AM", "11:00 AM")); tupleList.Add(new Tuple<string, string>("11:15 AM", "11:15 AM")); tupleList.Add(new Tuple<string, string>("11:30 AM", "11:30 AM")); tupleList.Add(new Tuple<string, string>("11:45 AM", "11:45 AM"));
                tupleList.Add(new Tuple<string, string>("12:00 PM", "12:00 PM")); tupleList.Add(new Tuple<string, string>("12:15 PM", "12:15 PM")); tupleList.Add(new Tuple<string, string>("12:30 PM", "12:30 PM")); tupleList.Add(new Tuple<string, string>("12:45 PM", "12:45 PM"));

                tupleList.Add(new Tuple<string, string>("01:00 PM", "01:00 PM")); tupleList.Add(new Tuple<string, string>("01:15 PM", "01:15 PM")); tupleList.Add(new Tuple<string, string>("01:30 PM", "01:30 PM")); tupleList.Add(new Tuple<string, string>("01:45 PM", "01:45 PM"));
                tupleList.Add(new Tuple<string, string>("02:00 PM", "02:00 PM")); tupleList.Add(new Tuple<string, string>("02:15 PM", "02:15 PM")); tupleList.Add(new Tuple<string, string>("02:30 PM", "02:30 PM")); tupleList.Add(new Tuple<string, string>("02:45 PM", "02:45 PM"));
                tupleList.Add(new Tuple<string, string>("03:00 PM", "03:00 PM")); tupleList.Add(new Tuple<string, string>("03:15 PM", "03:15 PM")); tupleList.Add(new Tuple<string, string>("03:30 PM", "03:30 PM")); tupleList.Add(new Tuple<string, string>("03:45 PM", "03:45 PM"));
                tupleList.Add(new Tuple<string, string>("04:00 PM", "04:00 PM")); tupleList.Add(new Tuple<string, string>("04:15 PM", "04:15 PM")); tupleList.Add(new Tuple<string, string>("04:30 PM", "04:30 PM")); tupleList.Add(new Tuple<string, string>("04:45 PM", "04:45 PM"));
                tupleList.Add(new Tuple<string, string>("05:00 PM", "05:00 PM")); tupleList.Add(new Tuple<string, string>("05:15 PM", "05:15 PM")); tupleList.Add(new Tuple<string, string>("05:30 PM", "05:30 PM")); tupleList.Add(new Tuple<string, string>("05:45 PM", "05:45 PM"));
                tupleList.Add(new Tuple<string, string>("06:00 PM", "06:00 PM")); tupleList.Add(new Tuple<string, string>("06:15 PM", "06:15 PM")); tupleList.Add(new Tuple<string, string>("06:30 PM", "06:30 PM")); tupleList.Add(new Tuple<string, string>("06:45 PM", "06:45 PM"));
                tupleList.Add(new Tuple<string, string>("07:00 PM", "07:00 PM")); tupleList.Add(new Tuple<string, string>("07:15 PM", "07:15 PM")); tupleList.Add(new Tuple<string, string>("07:30 PM", "07:30 PM")); tupleList.Add(new Tuple<string, string>("07:45 PM", "07:45 PM"));

                tupleList.Add(new Tuple<string, string>("08:00 PM", "08:00 PM")); tupleList.Add(new Tuple<string, string>("08:15 PM", "08:15 PM")); tupleList.Add(new Tuple<string, string>("08:30 PM", "08:30 PM")); tupleList.Add(new Tuple<string, string>("08:45 PM", "08:45 PM"));
                tupleList.Add(new Tuple<string, string>("09:00 PM", "09:00 PM")); tupleList.Add(new Tuple<string, string>("09:15 PM", "09:15 PM")); tupleList.Add(new Tuple<string, string>("09:30 PM", "09:30 PM")); tupleList.Add(new Tuple<string, string>("09:45 PM", "09:45 PM"));
                tupleList.Add(new Tuple<string, string>("10:00 PM", "10:00 PM")); tupleList.Add(new Tuple<string, string>("10:15 PM", "10:15 PM")); tupleList.Add(new Tuple<string, string>("10:30 PM", "10:30 PM")); tupleList.Add(new Tuple<string, string>("10:45 PM", "10:45 PM"));
                tupleList.Add(new Tuple<string, string>("11:00 PM", "11:00 PM")); tupleList.Add(new Tuple<string, string>("11:15 PM", "11:15 PM")); tupleList.Add(new Tuple<string, string>("11:30 PM", "11:30 PM")); tupleList.Add(new Tuple<string, string>("11:45 PM", "11:45 PM"));

                var lst = tupleList.Find(m => m.Item1 == time);
                if (lst != null)
                    validate = true;
                //{
                //    foreach (var item in lst)
                //    {
                //        ddlPeriod.Items.Add(new ListItem { Text = item.Item2, Value = item.Item2 });
                //    }
                //}


            }
            return validate;
        }
        [HttpGet]
        public ActionResult GetComplianceAssignment(string branchID)
        {
            int cnt = 0;
            try
            {
                 cnt = RLCS_ClientsManagement.GetComplianceAssignment(branchID);
            }
            catch (Exception Ex)
            {

            }
            return Json(cnt, JsonRequestBehavior.AllowGet);
        }
        #region BULK UPLOAD UpdateSelectedColumnLocation/Branch

        [HttpGet]
        public ActionResult UploadLocationUpdateSelected(string CustID, string UserID, int SPID)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                UploadEntityLocationDocumentVModel LocationDetails = new UploadEntityLocationDocumentVModel();
                List<RLCS_ClientHeader_Mapping> objRLCS_ClientHeader_Mapping = new List<RLCS_ClientHeader_Mapping>();
                if (!String.IsNullOrEmpty(CustID))
                    LocationDetails.CustomerID = Convert.ToInt32(CustID);
                LocationDetails.UserID = Convert.ToInt32(UserID);
                LocationDetails.ISEntity = true;
                TempData["CustID"] = Convert.ToInt32(CustID);
                TempData["UserID"] = Convert.ToInt32(UserID);

                List<ClientHeaderMappingVModel> compare = new List<ClientHeaderMappingVModel>();
                if (SPID == 94)  //// Parameter  Use As A ServiceProvider
                {
                    objRLCS_ClientHeader_Mapping = RLCS_ClientsManagement.GetMappedClientHeadersColumn("RLCS_CustomerBranch_ClientsLocation_Mapping", 94);
                }
                else
                {
                    objRLCS_ClientHeader_Mapping = RLCS_ClientsManagement.GetMappedClientHeadersColumn("RLCS_CustomerBranch_ClientsLocation_Mapping", 0);
                }
                //objRLCS_ClientHeader_Mapping = RLCS_ClientsManagement.GetMappedClientHeadersColumn("RLCS_CustomerBranch_ClientsLocation_Mapping", 0);
                TinyMapper.Bind<RLCS_ClientHeader_Mapping, ClientHeaderMappingVModel>();
                compare = TinyMapper.Map<List<ClientHeaderMappingVModel>>(objRLCS_ClientHeader_Mapping);
                if (compare.Count > 0)
                {
                    for (int j = 0; j < compare.Count; j++)
                    {
                        LocationDetails.ClientHeaderMappingVModelList.Add(new ClientHeaderMappingVModel { ID = compare[j].ID, CustomerID = Convert.ToInt32(CustID), TableName = "RLCS_CustomerBranch_ClientsLocation_Mapping", ColName = compare[j].ColName, ClientHeaderName = compare[j].ClientHeaderName });
                    }
                }
                return View(LocationDetails);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }

        [HttpPost]
        public ActionResult UploadLocationUpdateSelected(FormCollection formCollection, UploadEntityLocationDocumentVModel LocationDetails)
        {
            LogErrorsUpload.Clear();
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                bool update = true;
                string FilePath = "";
                try
                {
                    if (Request != null)
                    {
                        HttpPostedFileBase file = Request.Files["fileUpload"];
                        if (file != null && file.ContentLength > 0)
                        {
                            if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".xlsx"))
                            {
                                LocationDetails.Error = false;
                                string fileName = file.FileName;
                                string fileContentType = file.ContentType;
                                string Extention = Path.GetExtension(fileName);
                                byte[] fileBytes = new byte[file.ContentLength];
                                string FolderPath = Server.MapPath("~/HRExcelLocation");
                                var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                                if (!Directory.Exists(FolderPath))
                                {
                                    Directory.CreateDirectory(FolderPath);
                                }
                                FilePath = System.Web.HttpContext.Current.Server.MapPath("~") + "/HRExcelLocation/" + fileName;
                                file.SaveAs(FilePath);
                                if (Extention.ToLower() == ".xlsx")
                                {
                                    using (var package = new ExcelPackage(file.InputStream))
                                    {
                                        var currentSheet = package.Workbook.Worksheets;
                                        var workSheet = currentSheet.First();
                                        DataTable BranchDT = new DataTable();

                                        var SheetName = workSheet.Workbook.Worksheets["Branch"];
                                        if (SheetName.ToString() == "Branch")
                                        {
                                            BranchDT = LocationSelectedColumnConvertToDataTable(workSheet.Workbook.Worksheets["Branch"]);
                                            if (BranchDT != null)
                                            {
                                                DataRow dr = null;
                                                string[] arrcolumn = { "ClientID", "Branch Code", "State", "Location", "Branch Address", "LWF State", "PT State", "PF Code", "ESIC Code", "Office Type(Branch/Factory/Head Office)", "Estabilishment Type (Shop & Establishment/ Factory/ Both)", "Employer Name", "Employer Address", "Manager Name", "Manager Address", "Company Ph. No", "HR Contact Person", "HR Phn No", "HR Mail ID", "HR 1st Level Mail", "HR 1st Level Phn No", "RCNO", "RC Valid From(Date) i.e.(DD/MM/YYYY)", "RC Valid To(Date) i.e.(DD/MM/YYYY)", "Nature of Business", "Week off Days eg.(Saturday|Sunday)", "Work Hours From(eg.09:15AM)", "Work Hours To(eg.07:15AM)", "Intervals From(eg.02:15PM)", "Intervals To(eg.03:15PM)", "Work Timings(SHIFT/STANDARD)", "Status", "LIN", "Municipality", "Permission for maintaining forms(Yes/NO)", "Require power to impose fines(Yes/NO)", "Business Type(IT/ITES/NON-IT)", "Effective Date(Date)i.e.(DD/MM/YYYY)", "Classification of Est", "Licence No", "NIC Code", "Section of Act(2m(i)/2m(ii)/Section-85", "District", "Jurisdiction","Branch End Date(Date)i.e.(DD/MM/YYYY)", "Trade Licence(Yes/No)", "Employer Designation","EDLI Excemption (Yes/NO)", "Location Anchor" };
                                                DataTable dt = new DataTable("Branch");
                                                if (arrcolumn.Count() == 49)
                                                {
                                                    foreach (var item in arrcolumn)
                                                    {
                                                        dt.Columns.Add(Convert.ToString(item));
                                                    }
                                                    List<string> lstentity = new List<string>();
                                                    foreach (DataColumn item in BranchDT.Columns)
                                                    {
                                                        lstentity.Add(item.ColumnName);
                                                    }
                                                    for (int k = 0; k < BranchDT.Rows.Count; k++)
                                                    {
                                                        var newrow = dt.NewRow();
                                                        foreach (var emp in lstentity)
                                                        {
                                                            newrow[emp] = BranchDT.Rows[k][emp];
                                                        }
                                                        dt.Rows.Add(newrow);
                                                    }
                                                    for (int i = 0; i < dt.Rows.Count; i++)
                                                    {
                                                        for (int j = 2; j < dt.Columns.Count; j++)
                                                        {
                                                            if (string.IsNullOrEmpty(dt.Rows[i][j].ToString()))
                                                            {
                                                                dt.Rows[i][j] = "N0";
                                                            }
                                                        }
                                                    }
                                                }
                                                if (dt.Rows.Count <= 0)
                                                {
                                                    LocationDetails.RowCount = true;
                                                }
                                                else
                                                {
                                                    if (dt.Columns.Count > 2)
                                                    {
                                                        var CustomerID = new SqlParameter("@CustomerID", SqlDbType.Int);
                                                        CustomerID.Value = LocationDetails.CustomerID;
                                                        var UserID = new SqlParameter("@UserID", SqlDbType.Int);
                                                        UserID.Value = LocationDetails.UserID;
                                                        var UpdateFlag = new SqlParameter("@UpdateFlag", SqlDbType.Bit);
                                                        UpdateFlag.Value = update;
                                                        var BranchDT1 = new SqlParameter("@LocationDT", SqlDbType.Structured);
                                                        BranchDT1.Value = dt;
                                                        BranchDT1.TypeName = "dbo.LocationUDT";
                                                        var Result = new SqlParameter("@Result", SqlDbType.Int);
                                                        Result.Direction = ParameterDirection.Output;
                                                        var FileID = new SqlParameter("@FileID", SqlDbType.Int);
                                                        FileID.Direction = ParameterDirection.Output;


                                                        using (ComplianceDBEntities entity = new ComplianceDBEntities())
                                                        {
                                                            entity.Database.ExecuteSqlCommand("Exec dbo.[SP_LocationUpdate] @LocationDT,@CustomerID,@UserID,@UpdateFlag,@Result OUTPUT,@FileID OUTPUT", BranchDT1, CustomerID, UserID, UpdateFlag, Result, FileID);


                                                            if (Convert.ToInt32(Result.Value) == 200)
                                                            {
                                                                LocationDetails.Message = true;
                                                            }
                                                            else if (Convert.ToInt32(Result.Value) == 400 && Convert.ToInt32(FileID.Value) > 0)
                                                            {
                                                                int fileid = Convert.ToInt32(FileID.Value);
                                                                LocationDetails.Message = false;
                                                                List<string> ErrorList = new List<string>();
                                                                ErrorList = (from emp in entity.Temp_LocationMaster_ErrorList
                                                                             where emp.FileID == (int)FileID.Value && emp.CustomerID == (int)CustomerID.Value
                                                                             select emp.ErrorLog1
                                                                             ).ToList();

                                                                if (ErrorList.Count > 0)
                                                                {
                                                                    string path = "";
                                                                    LocationDetails.FileName = RLCS_WriteLog.WriteLog(ErrorList, "Upload Location Master Excel Sheet", out path);
                                                                    LocationDetails.ExcelErrors = true;
                                                                }
                                                                else
                                                                {
                                                                    ErrorList.Add("Error Occured.....!");
                                                                    string path = "";
                                                                    LocationDetails.FileName = RLCS_WriteLog.WriteLog(ErrorList, "Upload Location Master Excel Sheet", out path);
                                                                    LocationDetails.ExcelErrors = true;

                                                                }

                                                            }

                                                        }

                                                    }
                                                    else
                                                    {
                                                        LocationDetails.ColumnCount = true;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                string path = "";
                                                LocationDetails.FileName = RLCS_WriteLog.WriteLog(LogErrorsUpload, "Upload Location Master Excel Sheet", out path);
                                                LocationDetails.ExcelErrors = true;
                                            }
                                        }
                                        else
                                        {
                                            string path = "";
                                            LocationDetails.FileName = RLCS_WriteLog.WriteLog(LogErrorsUpload, "Incorrect Sheet Name......Upload Location Master Excel Sheet", out path);
                                            LocationDetails.ExcelErrors = true;
                                        }

                                    }
                                }
                                else
                                {
                                    LogErrorsUpload.Add("Please Upload letest Updated Version Excel File..");
                                    LogErrorsUpload.Add("Your File Version " + Extention.ToLower() + " : Required File Version .xlsx");
                                    string path = "";
                                    LocationDetails.FileName = RLCS_WriteLog.WriteLog(LogErrorsUpload, "Upload Location Master Excel Sheet", out path);
                                    LocationDetails.ExcelErrors = true;
                                }
                            }
                            else
                            {
                                LogErrorsUpload.Add("Please Download Excel Sample Format.");
                                LogErrorsUpload.Add("Your File Version " + Path.GetExtension(file.FileName).ToLower() + " : Required File Version .xlsx");
                                string path = "";
                                LocationDetails.FileName = RLCS_WriteLog.WriteLog(LogErrorsUpload, "Upload Location Master Excel Sheet", out path);
                                LocationDetails.ExcelErrors = true;
                            }
                        }
                        else
                        {
                            LocationDetails.Error = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

                return View(LocationDetails);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }
        private DataTable LocationSelectedColumnConvertToDataTable(ExcelWorksheet oSheet)
        {
            string[] arrcolumn = { "ClientID", "Branch Code", "State", "Location", "Branch Address", "LWF State", "PT State", "PF Code", "ESIC Code", "Office Type(Branch/Factory/Head Office)", "Estabilishment Type (Shop & Establishment/ Factory/ Both)", "Employer Name", "Employer Address", "Manager Name", "Manager Address", "Company Ph. No", "HR Contact Person", "HR Phn No", "HR Mail ID", "HR 1st Level Mail", "HR 1st Level Phn No", "RCNO", "RC Valid From(Date) i.e.(DD/MM/YYYY)", "RC Valid To(Date) i.e.(DD/MM/YYYY)", "Nature of Business", "Week off Days eg.(Saturday|Sunday)", "Work Hours From(eg.09:15AM)", "Work Hours To(eg.07:15AM)", "Intervals From(eg.02:15PM)", "Intervals To(eg.03:15PM)", "Work Timings(SHIFT/STANDARD)", "Status", "LIN", "Municipality", "Permission for maintaining forms(Yes/NO)", "Require power to impose fines(Yes/NO)", "Business Type(IT/ITES/NON-IT)", "Effective Date(Date)i.e.(DD/MM/YYYY)", "Classification of Est", "Licence No", "NIC Code", "Section of Act(2m(i)/2m(ii)/Section-85", "District", "Jurisdiction","Branch End Date(Date)i.e.(DD/MM/YYYY)", "Trade Licence(Yes/No)", "Employer Designation", "EDLI Excemption (Yes/NO)", "Location Anchor" };

            int totalRows = GetLastUsedRow(oSheet);
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            List<string> LogErrors = new List<string>();
            for (int i = 1; i <= totalRows; i++)
            {
                if (LogErrors.Count > 0)
                {
                    break;
                }
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        //if (string.IsNullOrEmpty(Convert.ToString(oSheet.Cells[i, j].Value).Trim()))
                        //{
                        //    LogErrors.Add("Column Empty.Please delete columns in Your Excel Sheet..!Coloumn NO: " + j);
                        //    break;
                        //}
                        if (dt.Columns.Contains(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            LogErrors.Add("Column Already Exist.Please check Your Excel Sheet..!" + arrcolumn[j - 1]);
                            break;
                        }
                        if (!arrcolumn.Contains(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            LogErrors.Add("Invalid Column Header.Please check Your Sample Excel Sheet..!" + arrcolumn[j - 1]);
                            break;
                        }
                        dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(oSheet.Cells[i, j].Value)))
                        {
                            dr[j - 1] = " ";
                        }
                        else
                        {
                            dr[j - 1] = Convert.ToString(oSheet.Cells[i, j].Value);
                        }
                    }
                }
            }
            if (LogErrors.Count > 0)
            {
                LogErrorsUpload = LogErrors.ToList();
                dt = null;
            }
            return dt;
        }
        public ActionResult DownloadAttachmentSelectedColumnsLocation(string CustomerID, int ID)
        {
            UploadEntityLocationDocumentVModel LocationUpload = new UploadEntityLocationDocumentVModel();
            try
            {
                if (CustomerID != null)
                {
                    if (ID != null)
                    {
                        using (ComplianceDBEntities entity = new ComplianceDBEntities())
                        {
                            var LstColumn = (from emp in entity.tbl_HeaderColumnJSON
                                             where emp.ID == (int)ID
                                             select emp.ColumnJson
                                             ).ToList();

                            if (LstColumn.Count > 0)
                            {
                                string[] arrList = LstColumn.ToArray();
                                LocationDownloadUpdateSelectedColumns(Convert.ToInt32(CustomerID), arrList[0].ToString());
                            }
                        }
                    }
                }
                else
                {
                    LocationUpload.ServerError = true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                LocationUpload.ServerError = true;
            }

            return View("UploadLocationUpdateSelected", LocationUpload);
        }
        private void LocationDownloadUpdateSelectedColumns(int CustomerID, string jsonContent)
        {
            UploadEntityLocationDocumentVModel LocationUpload = new UploadEntityLocationDocumentVModel();
            try
            {
                //used NewtonSoft json nuget package
                XmlNode xml = JsonConvert.DeserializeXmlNode("{Tables:{Table:" + jsonContent + "}}");
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xml.InnerXml);
                XmlReader xmlReader = new XmlNodeReader(xml);
                DataSet dataSet = new DataSet();
                dataSet.ReadXml(xmlReader);
                var dataTable = dataSet.Tables[0];
                DataTable dt = new DataTable();
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        dt.Columns.Add(Convert.ToString(row["Table_Text"]));

                    }
                }
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Branch");
                    exWorkSheet.Cells["A1"].LoadFromDataTable(dt, true);
                    exWorkSheet.DefaultColWidth = 15;
                    exWorkSheet.Row(1).Height = 50;
                    exWorkSheet.Cells.Style.Numberformat.Format = "@";
                    if (dt.Columns.Count == 10)
                    {
                        exWorkSheet.Column(10).Width = 25;
                    }
                    if (dt.Columns.Count == 11)
                    {
                        exWorkSheet.Column(11).Width = 25;
                    }
                    if (dt.Columns.Count == 23)
                    {
                        exWorkSheet.Column(23).Width = 25;
                    }
                    if (dt.Columns.Count == 24)
                    {
                        exWorkSheet.Column(24).Width = 25;
                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1, 1 + dt.Columns.Count - 1])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        col.Style.WrapText = true;
                        col.Style.Numberformat.Format = "@";

                    }
                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LocationSampleUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.Flush(); // Sends all currently buffered output to the client.
                    Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.

                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                LocationUpload.ServerError = true;
            }

        }
        #endregion
        #region EmployeeBranchTransfer
        [HttpGet]
        public ActionResult CreateEmployeeBranchTransfer(string EMPID, string ClientID, int CustomerID ,int BranchID,int EM_ID)
        {
            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                CreateEmployeeBranchTransferVModel BranchTransferDetails = new CreateEmployeeBranchTransferVModel();
                SP_GetEmployeeBranchTransferData_Result lstEmpDataTransfer = new SP_GetEmployeeBranchTransferData_Result();
                List<RLCS_State_Mapping> StateList = new List<RLCS_State_Mapping>();
                List<RLCS_Location_City_Mapping> LocationList = new List<RLCS_Location_City_Mapping>();
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> BranchList = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

                if (!String.IsNullOrEmpty(ClientID))
                BranchTransferDetails.CustomerID = Convert.ToInt32(CustomerID);
                BranchTransferDetails.ClientID = Convert.ToString(ClientID);
                BranchTransferDetails.EMPID = Convert.ToString(EMPID);
                BranchTransferDetails.AVACOM_BranchID = Convert.ToInt32(BranchID);
                BranchTransferDetails.EM_ID = Convert.ToInt32(EM_ID);
                TempData["CustID"] = Convert.ToInt32(CustomerID);
                lstEmpDataTransfer = RLCS_ClientsManagement.GetEmployeeTransferData(CustomerID, BranchID, EMPID);

                BranchTransferDetails.StateNameCB = lstEmpDataTransfer.SM_Name;
                BranchTransferDetails.LMNameCB = lstEmpDataTransfer.LM_Name;
                BranchTransferDetails.BranchNameCB = lstEmpDataTransfer.AVACOM_BranchName;
                BranchTransferDetails.ExcludeStateCode = lstEmpDataTransfer.SM_Code;
                BranchTransferDetails.ExcludeLocationCode = lstEmpDataTransfer.LM_Code;
                BranchTransferDetails.EM_DOJ = lstEmpDataTransfer.EM_DOJ;

                StateList = RLCS_ClientsManagement.GetClientwiseStates(ClientID);
                if (StateList != null && StateList.Count > 0)
                {
                    TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                    BranchTransferDetails.VMStatesTB = TinyMapper.Map<List<StateVModel>>(StateList);
                }
                LocationList = RLCS_ClientsManagement.GetClientwiseLocationCitiesByStateCode(ClientID,"");
                if (LocationList != null && LocationList.Count > 0)
                {
                    TinyMapper.Bind<List<RLCS_Location_City_Mapping>, List<LocationVModel>>();
                    BranchTransferDetails.VMLocationTB = TinyMapper.Map<List<LocationVModel>>(LocationList);                   
                }
                BranchList = RLCS_ClientsManagement.GetBranchesClientwise(CustomerID,ClientID,"","",BranchID);
                if (BranchList != null && BranchList.Count > 0)
                {
                    TinyMapper.Bind<List<RLCS_CustomerBranch_ClientsLocation_Mapping>, List<BranchVModel>>();
                    BranchTransferDetails.VMBranchTB = TinyMapper.Map<List<BranchVModel>>(BranchList);
                }
                BranchTransferDetails.EMT_LWD = DateTime.Now.ToString("dd/MM/yyyy");
                BranchTransferDetails.EMT_DOJ = DateTime.Now.ToString("dd/MM/yyyy");
                
                return View(BranchTransferDetails);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
        }
        [HttpPost]
        public ActionResult CreateEmployeeBranchTransfer(CreateEmployeeBranchTransferVModel BranchTransferDetails)
        {
            RLCS_Employee_Master_Transfer employeeTransfer = new RLCS_Employee_Master_Transfer();
            List<RLCS_State_Mapping> StateList = new List<RLCS_State_Mapping>();
            List<RLCS_Location_City_Mapping> LocationList = new List<RLCS_Location_City_Mapping>();
            List<RLCS_CustomerBranch_ClientsLocation_Mapping> BranchList = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

            if (System.Web.HttpContext.Current.Request.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    employeeTransfer.AVACOM_CustomerID = BranchTransferDetails.CustomerID;
                    employeeTransfer.EMT_ClientId = BranchTransferDetails.ClientID;
                    employeeTransfer.EMT_EmpID = BranchTransferDetails.EMPID;
                    employeeTransfer.EMT_State = BranchTransferDetails.EMT_SM_Code;
                    employeeTransfer.EMT_Location = BranchTransferDetails.EMT_LM_Code;
                    employeeTransfer.AVACOM_BranchID = Convert.ToInt32(BranchTransferDetails.EMT_Branch);
                    employeeTransfer.EMT_Branch = BranchTransferDetails.hidBranchName; 
                    employeeTransfer.EMT_FromDate = DateTimeExtensions.GetDate(BranchTransferDetails.EMT_DOJ);
                    employeeTransfer.EMT_ToDate = DateTimeExtensions.GetDate(BranchTransferDetails.EMT_LWD);
                    bool EmployeeTR = RLCS_ClientsManagement.CreateUpdateEmployeeMasterTransfer(employeeTransfer);
                    if (EmployeeTR)
                    {
                        employeeTransfer.EMT_ToDate = null;
                        employeeTransfer.EMT_Status = "A";
                        employeeTransfer.EMT_CreatedDate = DateTime.Now;
                        employeeTransfer.ISProcessed = false;
                        EmployeeTR = RLCS_ClientsManagement.InsertUpdateEmployeeTransfer(employeeTransfer); 
                    }
                    BranchTransferDetails.Message = EmployeeTR;
                }
                StateList = RLCS_ClientsManagement.GetClientwiseStates(BranchTransferDetails.ClientID);
                if (StateList != null && StateList.Count > 0)
                {
                    TinyMapper.Bind<List<RLCS_State_Mapping>, List<StateVModel>>();
                    BranchTransferDetails.VMStatesTB = TinyMapper.Map<List<StateVModel>>(StateList);
                }
                LocationList = RLCS_ClientsManagement.GetClientwiseLocationCitiesByStateCode(BranchTransferDetails.ClientID, "");
                if (LocationList != null && LocationList.Count > 0)
                {
                    TinyMapper.Bind<List<RLCS_Location_City_Mapping>, List<LocationVModel>>();
                    BranchTransferDetails.VMLocationTB = TinyMapper.Map<List<LocationVModel>>(LocationList);
                }
                BranchList = RLCS_ClientsManagement.GetBranchesClientwise(BranchTransferDetails.CustomerID, BranchTransferDetails.ClientID, "", "", BranchTransferDetails.AVACOM_BranchID);
                if (BranchList != null && BranchList.Count > 0)
                {
                    TinyMapper.Bind<List<RLCS_CustomerBranch_ClientsLocation_Mapping>, List<BranchVModel>>();
                    BranchTransferDetails.VMBranchTB = TinyMapper.Map<List<BranchVModel>>(BranchList);
                }
                BranchTransferDetails.EMT_LWD = DateTime.Now.ToString("dd/MM/yyyy");
                BranchTransferDetails.EMT_DOJ = DateTime.Now.ToString("dd/MM/yyyy");
                
                return View(BranchTransferDetails);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                return RedirectToAction("~/Login.aspx");
            }
            
        }
        public ActionResult GetClientwiseLocationCitiesByStateCode(string ClientID, string SM_Code)
        {
            List<RLCS_Location_City_Mapping> LocationList = new List<RLCS_Location_City_Mapping>();
            try
            {
                LocationList = RLCS_ClientsManagement.GetClientwiseLocationCitiesByStateCode(ClientID, SM_Code);
            }
            catch (Exception Ex)
            {

            }
            return Json(LocationList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetBranchesClientwise(int CustomerID,string ClientID, string SM_Code, string LM_Code, int BranchID)
        {
            List<RLCS_CustomerBranch_ClientsLocation_Mapping> BranchList = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
            try
            {
                BranchList= RLCS_ClientsManagement.GetBranchesClientwise(CustomerID, ClientID, SM_Code, LM_Code, BranchID);
            }
            catch (Exception Ex)
            {
            }
            return Json(BranchList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult getAllContractdetails()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var ContractDetails = (from row in entities.Cont_SP_GetAssignedContractsforUpdate(CustomerID)
                                       select row).ToList();
                var JsonResult = Json(ContractDetails, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
    }
}