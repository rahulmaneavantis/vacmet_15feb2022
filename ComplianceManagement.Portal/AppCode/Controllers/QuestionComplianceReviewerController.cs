﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers
{
    public class QuestionComplianceReviewerController : Controller
    {
        // GET: QuestionComplianceReviewer
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ComplianceApplicabilityReviewerQuestions()
        {
            return View();
        }
        #region   Submitted Question  Compliance Mapping
        [HttpGet]
        public ActionResult GetActAgrup()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;

                var role = (from A in entities.IMA_GetAssignedAct(AuthenticationHelper.UserID, "ACTGROUP","REV")
                            select new
                            {
                                ID = A.ID,
                                Name = A.Name,
                                ActGroupID = A.ActGroupID
                            }).ToList();

                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetAct(int groupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;

                var role = (from A in entities.IMA_GetAssignedAct(AuthenticationHelper.UserID, "ACT", "REV")
                            where A.ActGroupID == groupid
                            select new
                            {
                                ID = A.ID,
                                Name = A.Name,
                                ActGroupID = A.ActGroupID
                            }).ToList();


                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getAllCompliances(int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.IMA_GetReviewerSubmittedActCompliancedetails(actid, AuthenticationHelper.UserID)
                                    select usr).ToList();               
                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }
        [HttpGet]
        public ActionResult GetQuestionsDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //IsQuestionType
                List<int> d = new List<int>();
                d.Add(1);
                d.Add(3);
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.QuestionMasters.Where(x => d.Contains((int)x.IsQuestionType) && x.IsActive == false).Select(model => new { model.ID, model.QuestionName }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetQuestionsACTDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> d = new List<int>();
                d.Add(2);
                d.Add(3);
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.QuestionMasters.Where(x => d.Contains((int)x.IsQuestionType) && x.IsActive == false).Select(model => new { model.ID, model.QuestionName }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCategorizationDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.mst_categorization.Where(x => x.IsActive == false).Select(model => new { model.ID, model.Name }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetSubCategorizationDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var role = entities.mst_Subcategorization.Where(x => x.IsActive == false).Select(model => new { model.ID, model.Name, model.categorid }).ToList();
                return Json(role, JsonRequestBehavior.AllowGet);
            }
        }

        public class LinkedToALicenseType
        {
            public long LID { get; set; }
            public string LName { get; set; }
            public int LinkedID { get; set; }
        }

        [HttpGet]
        public ActionResult GetLicenseTypesDropDown()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<LinkedToALicenseType> expansedetails = new List<LinkedToALicenseType>();
                var expansedetail = entities.Lic_tbl_LicenseType_Master.Where(x => x.IsDeleted == false).Select(model => new { model.ID, model.Name }).ToList();
                foreach (var detail in expansedetail)
                {
                    expansedetails.Add(new LinkedToALicenseType
                    {
                        LID = detail.ID,
                        LName = detail.Name,
                        LinkedID = 1,
                    });
                }
                LinkedToALicenseType cc = new LinkedToALicenseType();
                cc.LID = 0;
                cc.LName = "No";
                cc.LinkedID = 2;
                expansedetails.Add(cc);
                return Json(expansedetails, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetControllsfromQuestion(int qID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var master = (from data in entities.QuestionMasters
                              where data.ID == qID
                              select data
                            ).FirstOrDefault();

                return Json(master, JsonRequestBehavior.AllowGet);
            }
        }
        public class QACR_Mapping
        {
            public long SubmittedQACMMappingID { get; set; }
            public long SubmittedActID { get; set; }
            public long SubmittedComplianceID { get; set; }
            public long SubmittedQuestionID { get; set; }
            public long SubmittedControllID { get; set; }
            public long SubmittedServiceID { get; set; }
            public Nullable<long> SubmittedAnswer { get; set; }
            public string SubmittedAnswerSingle { get; set; }
            public string SubmittedAnswerMultiple { get; set; }
            public Nullable<long> SubmittedAnsweRangeMin { get; set; }
            public Nullable<long> SubmittedAnsweRangeMax { get; set; }
            public Nullable<long> SubmittedIslicenseID { get; set; }
            public Nullable<long> SubmittedLicenseTypeID { get; set; }
            public Nullable<long> SubmittedCategoryID { get; set; }
            public Nullable<long> SubmittedSubCategoryID { get; set; }
        }
        public class QACRMappingEntry
        {
            public List<QACR_Mapping> Approvals { get; set; }
        }
        public string AddQuestionActComplianceMapping(QACR_Mapping detail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        Questions_ActComplianceMapping Qmst = new Questions_ActComplianceMapping();
                        Qmst.ActId = detail.SubmittedActID;
                        Qmst.ComplianceId = detail.SubmittedComplianceID;
                        Qmst.QuestionID = detail.SubmittedQuestionID;
                        Qmst.ControllId = detail.SubmittedControllID;
                        Qmst.AnswerTOQ = detail.SubmittedServiceID;
                        Qmst.AnswerValue = detail.SubmittedAnswer;
                        if (detail.SubmittedControllID == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.SubmittedAnswerSingle;
                        }
                        else if (detail.SubmittedControllID == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.SubmittedAnswerMultiple;
                        }

                        Qmst.StatusId = 3;
                        Qmst.AnsweRangeMin = detail.SubmittedAnsweRangeMin;
                        Qmst.AnsweRangeMax = detail.SubmittedAnsweRangeMax;

                        Qmst.IslicenseID = detail.SubmittedIslicenseID;
                        Qmst.LicenseTypeID = detail.SubmittedLicenseTypeID;
                        Qmst.CategoryID = detail.SubmittedCategoryID;
                        Qmst.SubCategoryID = detail.SubmittedSubCategoryID;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        if (QuestionACt_Compliance_MappingExists(Qmst))
                        {
                            result = "Question already approved";
                        }
                        else
                        {
                            entities.Questions_ActComplianceMapping.Add(Qmst);
                            entities.SaveChanges();
                            var QuestionID = Qmst.Id;

                            result = "Details Saved Successfully!";
                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }

        public bool QuestionACt_Compliance_MappingExists(Questions_ActComplianceMapping customer)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Questions_ActComplianceMapping
                             where row.IsActive == false
                                  && row.ComplianceId == customer.ComplianceId
                                  && row.QuestionID == customer.QuestionID
                                  && row.ControllId == customer.ControllId
                                  && row.StatusId==customer.StatusId
                             select row);

                if (customer.Id > 0)
                {
                    query = query.Where(entry => entry.Id != customer.Id);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        [HttpPost]
        public ActionResult SaveApprovals(QACRMappingEntry TimeSheets)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (QACR_Mapping detail in TimeSheets.Approvals)
                    {
                        int empId = Convert.ToInt32(Session["userID"]);
                        Questions_ActComplianceMapping Qmst = new Questions_ActComplianceMapping();
                        Qmst.ActId = detail.SubmittedActID;
                        Qmst.ComplianceId = detail.SubmittedComplianceID;
                        Qmst.QuestionID = detail.SubmittedQuestionID;
                        Qmst.ControllId = detail.SubmittedControllID;
                        Qmst.AnswerTOQ = detail.SubmittedServiceID;
                        Qmst.AnswerValue = detail.SubmittedAnswer;
                        Qmst.StatusId = 3;
                        if (detail.SubmittedControllID == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.SubmittedAnswerSingle;
                        }
                        else if (detail.SubmittedControllID == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.SubmittedAnswerMultiple;
                        }
                        Qmst.AnsweRangeMin = detail.SubmittedAnsweRangeMin;
                        Qmst.AnsweRangeMax = detail.SubmittedAnsweRangeMax;
                        Qmst.IslicenseID = detail.SubmittedIslicenseID;
                        Qmst.LicenseTypeID = detail.SubmittedLicenseTypeID;
                        Qmst.CategoryID = detail.SubmittedCategoryID;
                        Qmst.SubCategoryID = detail.SubmittedSubCategoryID;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        entities.Questions_ActComplianceMapping.Add(Qmst);
                        entities.SaveChanges();
                    }
                    result = "Details Saved Successfully!";
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        #region Approved  Question  Compliance Mapping
        public JsonResult getAllTaggedCompliances(int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Configuration.ProxyCreationEnabled = false;
                var activityList = (from usr in entities.IMA_GetReviewerApprovedActCompliancedetails(actid, AuthenticationHelper.UserID)
                                    select usr).ToList();

                var JsonResult = Json(activityList, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }

        public string UpdateTaggedQuestion(QACR_MappingUpdate detail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Something went wrong please try again";
                using (var dbContextTransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        Questions_ActComplianceMapping Qmst = new Questions_ActComplianceMapping();
                        Qmst.ActId = detail.ApprovedActID;
                        Qmst.ComplianceId = detail.ApprovedComplianceID;
                        Qmst.QuestionID = detail.ApprovedQuestionID;
                        Qmst.ControllId = detail.ApprovedControllID;
                        Qmst.AnswerTOQ = detail.ApprovedServiceID;
                        Qmst.AnswerValue = detail.ApprovedAnswer;
                        if (detail.ApprovedControllID == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.ApprovedAnswerSingle;
                        }
                        else if (detail.ApprovedControllID == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.ApprovedAnswerMultiple;
                        }

                        Qmst.StatusId = 3;
                        Qmst.AnsweRangeMin = detail.ApprovedAnsweRangeMin;
                        Qmst.AnsweRangeMax = detail.ApprovedAnsweRangeMax;

                        Qmst.IslicenseID = detail.ApprovedIslicenseID;
                        Qmst.LicenseTypeID = detail.ApprovedLicenseTypeID;
                        Qmst.CategoryID = detail.ApprovedCategoryID;
                        Qmst.SubCategoryID = detail.ApprovedSubCategoryID;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;

                        entities.Questions_ActComplianceMapping.Add(Qmst);
                        entities.SaveChanges();
                        var QuestionID = Qmst.Id;

                        result = "Details Saved Successfully!";
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        result = "Error! Something went wrong please try again";
                        dbContextTransaction.Rollback();
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
                return result;
            }
        }
        [HttpPost]
        public ActionResult BulkUpdateTaggedQuestion(QACRMappingEntryUpdate bupdate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string result = "Error! Approval Is Not Complete!";
                try
                {
                    foreach (QACR_MappingUpdate detail in bupdate.BulkUpdates)
                    {
                        int empId = Convert.ToInt32(Session["userID"]);
                        Questions_ActComplianceMapping Qmst = new Questions_ActComplianceMapping();
                        Qmst.ActId = detail.ApprovedActID;
                        Qmst.ComplianceId = detail.ApprovedComplianceID;
                        Qmst.QuestionID = detail.ApprovedQuestionID;
                        Qmst.ControllId = detail.ApprovedControllID;
                        Qmst.AnswerTOQ = detail.ApprovedServiceID;
                        Qmst.AnswerValue = detail.ApprovedAnswer;
                        Qmst.StatusId = 3;
                        if (detail.ApprovedControllID == 1)
                        {
                            Qmst.AnsweSingleMulti = detail.ApprovedAnswerSingle;
                        }
                        else if (detail.ApprovedControllID == 2)
                        {
                            Qmst.AnsweSingleMulti = detail.ApprovedAnswerMultiple;
                        }
                        Qmst.AnsweRangeMin = detail.ApprovedAnsweRangeMin;
                        Qmst.AnsweRangeMax = detail.ApprovedAnsweRangeMax;
                        Qmst.IslicenseID = detail.ApprovedIslicenseID;
                        Qmst.LicenseTypeID = detail.ApprovedLicenseTypeID;
                        Qmst.CategoryID = detail.ApprovedCategoryID;
                        Qmst.SubCategoryID = detail.ApprovedSubCategoryID;
                        Qmst.Createdon = DateTime.Now;
                        Qmst.Createdby = AuthenticationHelper.UserID;
                        Qmst.IsActive = false;
                        entities.Questions_ActComplianceMapping.Add(Qmst);
                        entities.SaveChanges();
                    }
                    result = "Details Updated Successfully!";
                }
                catch (Exception ex)
                {
                    return Json(ex.Message, JsonRequestBehavior.AllowGet);
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}