﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class WidgetModalclass
    {
        public class WidgetData
        {
            public int ID { get; set; }
            public string Name { get; set; }
          //  public string Name { get; set; }
            public int WidgetID { get; set; }

            public int ActID { get; set; }

            public int ComplianceID { get; set; }


        }
        public static void CreateOrUpdateWidgetMapping(List<WidgetDetail> mappingSubInustries, int actID, bool updateFlag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (updateFlag)
                {
                    //List<Act_SubIndustryMapping> mappedSubIndustrys = (from row in entities.Act_SubIndustryMapping
                    //                                                   where row.ActID == actID
                    //                                                   select row).ToList();
                    //List<WidgetDetail> mappedSubIndustrys = (from row in entities.WidgetDetails
                    //                    where row.ActID == actID
                    //                    select row).ToList();


                    //return actMappedIDs;
                     List< WidgetDetail > mappedSubIndustrys = (from row in entities.WidgetDetails
                                        where row.ActID== actID
                                         select row).ToList();

                    foreach (WidgetDetail Subindustry in mappedSubIndustrys)
                    {
                        entities.WidgetDetails.Remove(Subindustry);
                        entities.SaveChanges();
                    }

                    int lastID = (from row in entities.WidgetDetails
                                  orderby row.ID descending
                                  select row.ID).FirstOrDefault();
                }

                foreach (WidgetDetail mappingSubIndustry in mappingSubInustries)
                {
                    entities.WidgetDetails.Add(mappingSubIndustry);
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdateWidgetMaster(WidgetMaster obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                WidgetMaster widgetmaster = (from row in entities.WidgetMasters
                                                where row.ID == obj.ID
                                                && row.IsDeleted == false
                                                && row.CustomerID==obj.CustomerID 
                                                select row).FirstOrDefault();
                if (widgetmaster != null)
                {
                    widgetmaster.WidgetName= obj.WidgetName; 
                    entities.SaveChanges();
                }
            }
        }
        public static Mst_Group UserGroupMasterGetByNameAuditUser(string groupName, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MstIP = (from row in entities.Mst_Group
                             where row.GroupName.ToUpper().Trim() == groupName.ToUpper().Trim()
                             && row.CustomerID == customerID
                             && row.IsActive == true
                             select row).FirstOrDefault();
                return MstIP;
            }
        }
        public static WidgetMaster GetByID(int widgetid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.WidgetMasters
                           where row.ID == widgetid && row.IsDeleted == false
                           select row).SingleOrDefault();

                return act;
            }
        }
        public static WidgetMaster WidgetDetailByName(string widgetname, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.WidgetMasters
                             where
                             row.WidgetName.ToUpper().Trim().Equals(widgetname.ToUpper().Trim())
                             && row.IsDeleted == false
                             && row.CustomerID == customerid
                             select row).FirstOrDefault();
                return query;

            }
        }

        public static int GetWidgetDetail(long widgetid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.WidgetDetails
                             where row.WidgetID == widgetid
                             select (int)row.BranchID).FirstOrDefault();
                return query;

            }
        }

        public static List<int> GetWidgetActMappedID(long widgetid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actMappedIDs = (from row in entities.WidgetDetails
                                    where row.WidgetID == widgetid
                                    select (int)row.ActID).ToList();

                return actMappedIDs;
            }
        }
        public static List<int> GetWidgetComplinceMappedID(long widgetid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceMappedIDs = (from row in entities.WidgetDetails
                                           where row.WidgetID == widgetid
                                           select (int)row.ComplianceID).ToList();

                return complianceMappedIDs;
            }

        }

        public static List<SP_WidgetComplianceMaster_Result> GetAllCompliance(long customerid,List<long?> actidlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var clist = (from row in entities.SP_WidgetComplianceMaster(customerid)
                             where actidlist.Contains(row.ActID)
                                     select row).ToList();

                return clist.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static WidgetMaster GroupDetailsByName(string widgetname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.WidgetMasters
                             where
                             row.WidgetName.ToUpper().Trim().Equals(widgetname.ToUpper().Trim())
                             && row.IsDeleted == false 
                             select row).FirstOrDefault();
                return query;

            }
        }
        public static void DeleteWidgetData(int widgetID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserMastertoDelete = (from row in entities.WidgetMasters
                                          where row.ID == widgetID  
                                          select row).FirstOrDefault();
                if (UserMastertoDelete != null)
                {
                    UserMastertoDelete.IsDeleted = true;
                    entities.SaveChanges();
                }
            }
        }
        public static List<SP_WidgetActMaster_Result> GetAllACTs(long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.SP_WidgetActMaster(customerid)                            
                            select row).ToList();
              
                return acts.OrderBy(entry => entry.Name).ToList();
            }
        }
        private static bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static List<ActView> GetAll(int widgetid, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.ActViews
                            select row).ToList();
                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        acts = acts.Where(entry => entry.ID == a).ToList();
                    }
                    else
                    {
                        acts = acts.Where(entry => entry.Name.Contains(filter) || entry.ComplianceCategoryName.Contains(filter) || entry.ComplianceTypeName.Contains(filter) || entry.State.Contains(filter) || entry.City.Contains(filter)).ToList();
                    }
                }
                if (widgetid != -1)
                {
                    acts = acts.Where(entry => entry.ComplianceCategoryId == widgetid).ToList();
                }
                return acts.ToList();
            }
        }

        public static List<WidgetData> GetAllWidgetName()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var result = (from row in entities.WidgetMasters

                              where row.IsDeleted == true

                              //  && row1.IsActive==true
                              select new WidgetData()
                              {
                                  Name = row.WidgetName

                              }).ToList();
                return result.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static object GetData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.WidgetMasters
                            where row.IsDeleted==false
                            select new
                            {
                                row.ID,
                                row.WidgetName
                            }).ToList();
                return data;
            }
        }
        public static List<WidgetMaster> GetAll(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.WidgetMasters
                                           where row.IsDeleted==false
                                           && row.CustomerID == customerID
                                           select row);

                //if (!string.IsNullOrEmpty(filter))
                //{
                //    complianceCategorys = complianceCategorys.Where(entry => entry.WidgetName.Contains(filter));
                //}

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.WidgetName);

                return complianceCategorys.OrderBy(entry => entry.WidgetName).ToList();
            }
        }

        public static List<WidgetMaster> GetNameAll(string widgetname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.WidgetMasters
                                           where row.IsDeleted == false
                                           && row.WidgetName == widgetname
                                           select row);

                //if (!string.IsNullOrEmpty(filter))
                //{
                //    complianceCategorys = complianceCategorys.Where(entry => entry.WidgetName.Contains(filter));
                //}

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.WidgetName);

                return complianceCategorys.OrderBy(entry => entry.WidgetName).ToList();
            }
        }
    }
}
