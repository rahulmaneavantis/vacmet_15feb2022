//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataLitigation
{
    using System;
    using System.Collections.Generic;
    
    public partial class View_LitigationReminderView
    {
        public Nullable<int> CustomerID { get; set; }
        public Nullable<int> CustomerStatus { get; set; }
        public long ReminderID { get; set; }
        public long InstanceID { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string ReminderTitle { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public System.DateTime RemindOn { get; set; }
        public int ReminderStatus { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool UserIsActive { get; set; }
        public bool UserIsDeleted { get; set; }
        public Nullable<bool> IsExternal { get; set; }
    }
}
