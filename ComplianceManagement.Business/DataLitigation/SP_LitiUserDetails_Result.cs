//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataLitigation
{
    using System;
    
    public partial class SP_LitiUserDetails_Result
    {
        public long ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Code { get; set; }
        public string UserType { get; set; }
        public string LitigationRole { get; set; }
        public string MailServicesID { get; set; }
    }
}
