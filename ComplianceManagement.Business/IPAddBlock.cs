﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class IPAddBlock
    {
        public class CustomerData
        {

            public long IPID { get; set; }
            public string IPName { get; set; }
            public string IpAddress { get; set; }
            public int CustomerId { get; set; }
            public string CustomerName { get; set; }
            public string Remark { get; set; }

        }
        public class UserData
        {
            public long GroupID { get; set; }
            public long IPID { get; set; }
            public string Name { get; set; }
            public string IPAddress { get; set; }
            public Nullable<int> RememberMe { get; set; }
            public Nullable <int> PasswordExpiry { get; set; }
            public string IPName { get; set; }
            public string Remark { get; set; }
        }
        public class CustomerDataNew
        {
            public long ID { get; set; }
            public string GroupName { get; set; }
            public Nullable<bool> mobileaccess { get; set; }
            public Nullable<bool> IMEI { get; set; }
            public Nullable<bool> OTP { get; set; }

        }
        public static List<CustomerDataNew> GetAllGroupNameNew(int id)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = (from row in entities.Mst_Group
                              where row.Id == id
                             && row.IsActive == true
                              select new CustomerDataNew()
                              {
                                  ID = row.Id,
                                  GroupName = row.GroupName,
                                  mobileaccess = row.MobileAccess,
                                  IMEI = row.IMEINumber,
                                  OTP = row.OTP
                              }).ToList();
                return result;

            }
        }
        public static long ipIDcheck(Mst_BlockIP deptmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long Addressquery = (from row in entities.Mst_BlockIP
                                     where
                                     row.IPName.ToUpper().Trim() == deptmaster.IPName.ToUpper().Trim()
                                     && row.IsActive == true
                                     && row.CustomerID == deptmaster.CustomerID
                                     select row.Id).FirstOrDefault();
                return Addressquery;
            }

        }
        public static void CreateIPMaster(Mst_BlockIP dept)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Mst_BlockIP.Add(dept);
                entities.SaveChanges();
            }
        }
        //public static void UpdateIPMaster(Mst_BlockIP dept)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        Mst_BlockIP deptMasterToUpdate = (from row in entities.Mst_BlockIP
        //                                          where row.Id == dept.Id
        //                                             && row.IsActive == true
        //                                             && row.CustomerID == dept.CustomerID
        //                                          select row).FirstOrDefault();

        //        deptMasterToUpdate.IPName = dept.IPName;
        //        deptMasterToUpdate.IPAddress = dept.IPAddress;
        //        deptMasterToUpdate.Remark = dept.Remark;
        //        entities.SaveChanges();
        //    }
        //}

        public static void UpdateIPMaster(Mst_BlockIP dept)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Mst_BlockIP deptMasterToUpdate = (from row in entities.Mst_BlockIP
                                                  where row.Id == dept.Id
                                                     && row.IsActive == true
                                                     && row.CustomerID == dept.CustomerID
                                                  select row).FirstOrDefault();

                deptMasterToUpdate.IPName = dept.IPName;
                deptMasterToUpdate.IPAddress = dept.IPAddress;
                deptMasterToUpdate.Remark = dept.Remark;
                deptMasterToUpdate.UpdatedBy = dept.UpdatedBy;
                deptMasterToUpdate.UpdatedOn = dept.UpdatedOn;
                entities.SaveChanges();
            }
        }

        public static bool DeleteIPAddressMasterAudit(int ipID, long CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var check = (from row in entities.mst_User_Group
                                 where row.IPAddressID == ipID
                                 && row.CustomerID == CustomerID
                                 && row.IsActive == true
                                 select row).ToList();

                    if (check.Count == 0)
                    {
                        var DepartmentMastertoDelete = (from row in entities.Mst_BlockIP
                                                        where row.Id == ipID
                                                        && row.CustomerID == CustomerID
                                                        select row).FirstOrDefault();

                        DepartmentMastertoDelete.IsActive = false;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "IPAddBlock";
                msg.FunctionName = "DeleteIPAddressMasterAudit";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);
                return false;
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {
            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();
                });
            }
        }
        public static Mst_BlockIP IPMasterGetByIDAudit(int deptID, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MstIP = (from row in entities.Mst_BlockIP
                             where row.Id == deptID
                            && row.CustomerID == customerID
                            && row.IsActive == true
                             select row).SingleOrDefault();

                return MstIP;
            }
        }
        public static bool ipExistNew(Mst_BlockIP deptmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool isexists1 = false;
                bool isexists2 = false;
                var Addressquery = (from row in entities.Mst_BlockIP
                                    where
                                    row.IPAddress.ToUpper().Trim() == deptmaster.IPAddress.ToUpper().Trim()
                                    && row.IsActive == true
                                    && row.CustomerID == deptmaster.CustomerID
                                    select row).FirstOrDefault();
                if (Addressquery != null)
                {
                    isexists1 = true;
                }
                var namequery = (from row in entities.Mst_BlockIP
                                 where
                                 row.IsActive == true
                                 && row.IPName.Trim() == deptmaster.IPName.Trim()
                                 && row.CustomerID == deptmaster.CustomerID
                                 select row).FirstOrDefault();
                if (namequery != null)
                {
                    isexists2 = true;
                }

                if (isexists1 == true && isexists2 == true)
                {
                    return true;
                }
                else if (isexists1 == true && isexists2 == false)
                {
                    return true;
                }
                else if (isexists1 == false && isexists2 == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool ipExist(Mst_BlockIP deptmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool isexists1 = false;
                bool isexists2 = false;
                var Addressquery = (from row in entities.Mst_BlockIP
                                    where
                                    row.Id == deptmaster.Id
                                    && row.IsActive == true
                                    && row.CustomerID == deptmaster.CustomerID
                                    select row).FirstOrDefault();
                if (Addressquery != null)
                {
                    isexists1 = true;
                }
                var namequery = (from row in entities.Mst_BlockIP
                                 where
                                 row.IsActive == true
                                 && row.IPName.Trim() == deptmaster.IPName.Trim()
                                 && row.CustomerID == deptmaster.CustomerID
                                 select row).FirstOrDefault();
                if (namequery != null)
                {
                    isexists2 = true;
                }

                if (isexists1 ==true && isexists2==true)
                {
                    return true;
                }
                else if (isexists1 == true && isexists2 == false)
                {
                    return true;
                }
                else if (isexists1 == false && isexists2 == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool ipExists(Mst_BlockIP deptmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                                
                var query = (from row in entities.Mst_BlockIP
                             where                             
                             row.IPAddress.ToUpper().Trim() ==deptmaster.IPAddress.ToUpper().Trim()
                             && row.IPName.Trim()==deptmaster.IPName.Trim()
                             && row.IsActive==true
                             && row.CustomerID == deptmaster.CustomerID
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool ipExistNew(Mst_BlockIP deptmaster, string remark)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool isexists1 = false;
                bool isexists2 = false;
                //bool isexists3 = false;
                if (remark == "")
                { remark = null; }
                var Addressquery = (from row in entities.Mst_BlockIP
                                    where
                                    row.IPAddress.ToUpper().Trim() == deptmaster.IPAddress.ToUpper().Trim()
                                    && row.IsActive == true
                                    && row.CustomerID == deptmaster.CustomerID
                                    select row).FirstOrDefault();
                if (Addressquery != null)
                {
                    isexists1 = true;
                }
                var namequery = (from row in entities.Mst_BlockIP
                                 where
                                 row.IsActive == true
                                 && row.IPName.Trim() == deptmaster.IPName.Trim()
                                 && row.CustomerID == deptmaster.CustomerID
                                 select row).FirstOrDefault();
                if (namequery != null)
                {
                    isexists2 = true;
                }
                //var remarkquery = (from row in entities.Mst_BlockIP
                //                   where
                //                   row.Remark == remark
                //                   && row.IsActive == true
                //                   && row.CustomerID == deptmaster.CustomerID
                //                   select row).FirstOrDefault();
                //if (remarkquery != null)
                //{
                //    isexists3 = true;
                //}

                //if (isexists1 == true && isexists2 == true && isexists3 == true)
                //{
                //    return true;
                //}
                //else 
                if (isexists1 == true && isexists2 == false)
                {
                    return true;
                }
                else if (isexists1 == false && isexists2 == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool ipExists(Mst_BlockIP deptmaster, string remark)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool isexists1 = false;
                bool isexists2 = false;
                bool isexists3 = false;
                if (remark == "")
                { remark = null; }
                var Addressquery = (from row in entities.Mst_BlockIP
                                    where
                                    row.IPAddress.ToUpper().Trim() == deptmaster.IPAddress.ToUpper().Trim()
                                    && row.IsActive == true
                                    && row.CustomerID == deptmaster.CustomerID
                                    select row).FirstOrDefault();
                if (Addressquery != null)
                {
                    isexists1 = true;
                }
                var namequery = (from row in entities.Mst_BlockIP
                                 where
                                 row.IsActive == true
                                 && row.IPName.Trim() == deptmaster.IPName.Trim()
                                 && row.CustomerID == deptmaster.CustomerID
                                 select row).FirstOrDefault();
                if (namequery != null)
                {
                    isexists2 = true;
                }
                var remarkquery = (from row in entities.Mst_BlockIP
                                   where
                                   row.Remark == remark
                                   && row.IsActive == true
                                   && row.CustomerID == deptmaster.CustomerID
                                   select row).FirstOrDefault();
                if (remarkquery != null)
                {
                    isexists3 = true;
                }

                if (isexists1 == true && isexists2 == true && isexists3 == true)
                {
                    return true;
                }
                else if (isexists1 == true && isexists2 == false)
                {
                    return false;
                }
                else if (isexists1 == false && isexists2 == true)
                {
                    return false;
                }
                else
                {
                    return false;
                }

            }
        }
        public static List<CustomerData> GetAllMasterList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = (from row in entities.Customers
                              join row1 in entities.Mst_BlockIP
                              on row.ID equals row1.CustomerID
                              where row.IsDeleted == false
                              && row1.IsActive == true
                              && row.ID== customerID
                              select new CustomerData()
                              {
                                  IPID = row1.Id,
                                  IPName = row1.IPName,
                                  IpAddress = row1.IPAddress,
                                  CustomerId = row.ID,
                                  CustomerName = row.Name,
                                  Remark=row1.Remark,
                              }).ToList();
                return result.OrderBy(entry => entry.IPName).ToList();
            }
        }
        public static List<UserData> GetAllUserMasterList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                //var rest = (from ab in entities.ComplianceAssignments
                //            select ab).ToList();

                var result = (from MBP in entities.Mst_BlockIP
                              join MUG in entities.mst_User_Group
                              on MBP.Id equals MUG.IPAddressID
                              join MG in entities.Mst_Group                            
                              on MUG.GroupID equals MG.Id
                              
                              where
                              MUG.CustomerID == customerID
                              && MBP.IsActive == true
                              && MUG.IsActive == true
                              && MG.IsActive == true
                              select new UserData()
                              {
                                  GroupID= MUG.GroupID,
                                  IPID = MUG.Id,
                                  Name = MG.GroupName,
                                  IPName=MBP.IPName,
                                  IPAddress = MBP.IPAddress,
                                  RememberMe = MG.RememberMe,
                                  PasswordExpiry = MG.PasswordExpiry,
                                  Remark=MG.Remark

                              }).ToList();
                return result.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static void DeselectAllUserGroupMapping(long GroupID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var DeptIDs = (from row in entities.mst_User_Group
                               where row.GroupID == GroupID
                               select row).ToList();

                if (DeptIDs.Count > 0)
                {
                    foreach (var item in DeptIDs)
                    {
                        item.IsActive = false;
                        entities.SaveChanges();
                    }
                }
            }
        }
        public static void CreateUserGroupMapping(List<mst_User_Group> groupmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var item in groupmapping)
                {
                    var DeptIDs = (from row in entities.mst_User_Group
                                   where row.GroupID==item.GroupID
                                   && row.IPAddressID == item.IPAddressID
                                   && row.CustomerID == item.CustomerID
                                   select row).FirstOrDefault();

                    if (DeptIDs == null)
                    {
                        entities.mst_User_Group.Add(item);
                        entities.SaveChanges();
                    }
                    else
                    {
                        if (!(bool)DeptIDs.IsActive)
                        {
                            DeptIDs.IsActive = true;
                            DeptIDs.UpdatedBy = item.CreatedBy;
                            DeptIDs.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                }
            }
        }
        public static object GetAll(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = (from row in entities.Mst_BlockIP
                              where row.CustomerID == customerID
                              && row.IsActive == true
                              select new { Id = row.Id, Name = row.IPName }).ToList();

                return result;

            }
        }
    public static object GetAllGroupName(int customerID)
    {
        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        {
                var result = (from row in entities.Mst_Group
                              where row.CustomerID == customerID
                              && row.IsActive==true
                              select new { Id = row.Id, Name = row.GroupName }).ToList();

                return result;
            }
    }
        public static void CreateUserGroupNameMapping(List<mst_UserList_Group> groupmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var item in groupmapping)
                {
                    var UserGroupNameIDs = (from row in entities.mst_UserList_Group
                                   where row.UserID==item.UserID
                                   && row.GroupID == item.GroupID
                                   select row).FirstOrDefault();

                    if (UserGroupNameIDs == null)
                    {
                        entities.mst_UserList_Group.Add(item);
                        entities.SaveChanges();
                    }
                    else
                    {
                        if (!UserGroupNameIDs.IsActive)
                        {
                            UserGroupNameIDs.IsActive = true;
                            UserGroupNameIDs.UpdatedBy = item.UserID;
                            UserGroupNameIDs.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                }
            }
        }
        
        public static void DeselectAllGroupName(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var DeptIDs = (from row in entities.mst_UserList_Group
                               where row.UserID == UserID
                               select row).ToList();

                if (DeptIDs.Count > 0)
                {
                    foreach (var item in DeptIDs)
                    {
                        item.IsActive = false;
                        entities.SaveChanges();
                    }
                }
            }
        }   
    }
}

