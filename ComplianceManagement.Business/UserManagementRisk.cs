﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using EntityFramework.BulkInsert.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class UserManagementRisk
    {
        #region 
        public static void DeleteProcess(long ID, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_ProcessRatingAssessment updatestates = (from row in entities.mst_ProcessRatingAssessment
                                                            where row.ID == ID
                                                            && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                if (updatestates != null)
                {
                    updatestates.UpdatedBy = UserID;
                    updatestates.UpdatedOn = DateTime.Now;
                    updatestates.IsDeleted = true;
                    entities.SaveChanges();
                }

            }
        }
        public static mst_ProcessRatingAssessment ProcessGetID(long iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessMaster = (from row in entities.mst_ProcessRatingAssessment
                                     where row.ID == iD && row.IsDeleted == false
                                     select row).FirstOrDefault();
                return ProcessMaster;
            }
        }
        public static List<mst_ProcessRatingAssessment> GetAllProcess()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.mst_ProcessRatingAssessment
                                   where row.Name != null && row.IsDeleted == false
                                   select row).ToList();
                return ProcessList.ToList();
            }
        }
        public static void UpdateProcess(mst_ProcessRatingAssessment objProcess)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_ProcessRatingAssessment updatestates = (from row in entities.mst_ProcessRatingAssessment
                                                            where row.ID == objProcess.ID
                                                            && row.IsDeleted == false
                                                            select row).FirstOrDefault();

                if (updatestates != null)
                {
                    updatestates.Name = objProcess.Name;
                    updatestates.Description = objProcess.Description;
                    updatestates.UpdatedBy = objProcess.UpdatedBy;
                    updatestates.UpdatedOn = objProcess.UpdatedOn;
                    updatestates.IsDeleted = false;

                    entities.SaveChanges();
                }
            }
        }

        public static void CreateProcess(mst_ProcessRatingAssessment objProcess)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_ProcessRatingAssessment.Add(objProcess);
                entities.SaveChanges();
            }
        }
        public static bool ProcessRatingExist(string name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_ProcessRatingAssessment
                             where row.Name.Equals(name)
                             && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }

        }

        public static object FillAllVerticalForSchedule(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Vertical
                             where row.CustomerID == customerID
                             select row).ToList();
                if (query.Count > 0)
                {
                    var users = (from row in query
                                 select new { VerticalsId = row.ID, VerticalName = row.VerticalName }).OrderBy(entry => entry.VerticalName).ToList<object>();

                    return users;
                }
                else
                {
                    return -1;
                }
            }
        }
        #endregion

        public static List<UserView_Risk> GetAllUser(int customerID, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var users = (from row in entities.UserView_Risk
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }

        public static mst_Vertical getverticaldetails(long customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Vertical
                             where row.VerticalName.ToUpper().Trim() == "NA"
                             && row.CustomerID == customerid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return query;
                }
                else
                {
                    return null;
                }
            }
        }
        public static int VerticalgetBycustomerid(long customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Vertical
                             where row.VerticalName.ToUpper().Trim() == "NA"
                             && row.CustomerID == customerid
                             select row.ID).FirstOrDefault();
                if (query != null)
                {
                    return query;
                }
                else
                {
                    return -1;
                }
            }
        }
        public static string GetFrequencyFlag(Int64 CustomerBranchId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.InternalAuditSchedulings
                                     where row.AuditID == AuditID && row.CustomerBranchId == CustomerBranchId
                                     select row.ISAHQMP).ToList().Distinct().FirstOrDefault();
                if (MstRiskResult != null)
                {
                    return MstRiskResult;
                }
                else
                {
                    return "";
                }
            }
        }
        public static int GetPhaseCountNew(Int64 CustomerBranchId, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.InternalAuditSchedulings
                                     where row.AuditID == AuditID
                                     select row.PhaseCount).ToList().Distinct().FirstOrDefault();
                if (MstRiskResult != null)
                {
                    return (int)MstRiskResult;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static bool DeleteAssignmentByAuditInstance(long instanceID)
        {
            bool result = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var audittransactiondetails = (from row in entities.AuditTransactions
                                               where row.AuditInstanceId == instanceID
                                               select row).FirstOrDefault();

                if (audittransactiondetails != null)
                {
                    entities.AuditTransactions.Remove(audittransactiondetails);
                    entities.SaveChanges();
                    result = true;
                }
                else
                {
                    result = false;
                }

                if (result)
                {

                    var Scheduledetails = (from row in entities.AuditScheduleOns
                                           where row.AuditInstanceId == instanceID
                                           select row).FirstOrDefault();

                    if (Scheduledetails != null)
                    {
                        entities.AuditScheduleOns.Remove(Scheduledetails);
                        entities.SaveChanges();
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }

                }
                if (result)
                {
                    var Instancedetails = (from row in entities.AuditInstances
                                           where row.ID == instanceID
                                           select row).FirstOrDefault();

                    if (Instancedetails != null)
                    {
                        entities.AuditInstances.Remove(Instancedetails);
                        entities.SaveChanges();
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                if (result)
                {

                    var Assignmentdetails = (from row in entities.AuditAssignments
                                             where row.AuditInstanceId == instanceID
                                             select row).ToList();

                    if (Assignmentdetails.Count > 0)
                    {
                        Assignmentdetails.ForEach(item =>
                        {
                            entities.AuditAssignments.Remove(item);
                            entities.SaveChanges();
                            result = true;
                        });
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
        }

        public static bool DeleteAssignmentByAuditInstance(List<long> instanceIDList)
        {
            bool result = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                instanceIDList.ForEach(entry =>
                {
                    var Scheduledetails = (from row in entities.AuditTransactions
                                           where row.AuditInstanceId == entry
                                           select row).FirstOrDefault();

                    if (Scheduledetails != null)
                    {
                        entities.AuditTransactions.Remove(Scheduledetails);
                        entities.SaveChanges();
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                });
                if (result)
                {
                    instanceIDList.ForEach(entry =>
                    {
                        var Scheduledetails = (from row in entities.AuditScheduleOns
                                               where row.AuditInstanceId == entry
                                               select row).FirstOrDefault();

                        if (Scheduledetails != null)
                        {
                            entities.AuditScheduleOns.Remove(Scheduledetails);
                            entities.SaveChanges();
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    });
                }
                if (result)
                {
                    instanceIDList.ForEach(entry =>
                    {
                        var Scheduledetails = (from row in entities.AuditInstances
                                               where row.ID == entry
                                               select row).FirstOrDefault();

                        if (Scheduledetails != null)
                        {
                            entities.AuditInstances.Remove(Scheduledetails);
                            entities.SaveChanges();
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    });
                }
                if (result)
                {
                    instanceIDList.ForEach(entry =>
                    {
                        var Scheduledetails = (from row in entities.AuditAssignments
                                               where row.AuditInstanceId == entry
                                               select row).ToList();


                        if (Scheduledetails.Count > 0)
                        {
                            Scheduledetails.ForEach(item =>
                            {
                                entities.AuditAssignments.Remove(item);
                                entities.SaveChanges();
                                result = true;
                            });
                        }
                        else
                        {
                            result = false;
                        }
                    });
                }
                return result;
            }
        }
        public static bool DeleteAllKickOffData(int AuditID)
        {
            bool result = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //AuditStepMappings
                var AuditSteps = (from row in entities.AuditStepMappings
                                  where row.AuditID == AuditID
                                  select row).ToList();

                if (AuditSteps.Count > 0)
                {
                    AuditSteps.ForEach(item =>
                    {
                        entities.AuditStepMappings.Remove(item);
                        entities.SaveChanges();
                        result = true;
                    });
                }
                else
                {
                    result = false;
                }
                //InternalAuditTransaction
                if (result)
                {
                    var AuditTransaction = (from row in entities.InternalAuditTransactions
                                            where row.AuditID == AuditID
                                            select row).ToList();

                    if (AuditTransaction.Count > 0)
                    {
                        AuditTransaction.ForEach(item =>
                        {
                            entities.InternalAuditTransactions.Remove(item);
                            entities.SaveChanges();
                            result = true;
                        });
                    }
                    //else
                    //{
                    //    result = false;
                    //}
                }
                //InternalControlAuditAssignment
                if (result)
                {
                    var AuditAssigns = (from row in entities.InternalControlAuditAssignments
                                        where row.AuditID == AuditID
                                        select row).ToList();

                    if (AuditAssigns.Count > 0)
                    {
                        AuditAssigns.ForEach(item =>
                        {
                            entities.InternalControlAuditAssignments.Remove(item);
                            entities.SaveChanges();
                            result = true;
                        });
                    }
                    else
                    {
                        result = false;
                    }
                }
                //InternalAuditScheduleOn
                if (result)
                {
                    var AuditSchedules = (from row in entities.InternalAuditScheduleOns
                                          where row.AuditID == AuditID
                                          select row).ToList();

                    if (AuditSchedules.Count > 0)
                    {
                        AuditSchedules.ForEach(item =>
                        {
                            entities.InternalAuditScheduleOns.Remove(item);
                            entities.SaveChanges();
                            result = true;
                        });
                    }
                    else
                    {
                        result = false;
                    }
                }
                //InternalAuditInstance
                if (result)
                {
                    var AuditInstancess = (from row in entities.InternalAuditInstances
                                           where row.AuditID == AuditID
                                           select row).ToList();

                    if (AuditInstancess.Count > 0)
                    {
                        AuditInstancess.ForEach(item =>
                        {
                            entities.InternalAuditInstances.Remove(item);
                            entities.SaveChanges();
                            result = true;
                        });
                    }
                    else
                    {
                        result = false;
                    }
                }
                //AuditClosureDetails
                if (result)
                {
                    var Scheduledetails = (from row in entities.AuditClosureDetails
                                           where row.ID == AuditID
                                           select row).FirstOrDefault();

                    if (Scheduledetails != null)
                    {
                        Scheduledetails.Total = 0;
                        Scheduledetails.IsKickOff = false;
                        Scheduledetails.Closed = 0;
                        entities.SaveChanges();
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
        }
        public static List<Sp_ReassignProcessOwner_Result> GetAllReassignDataProcessOwner(int userID, List<long> blist)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.Sp_ReassignProcessOwner(userID)
                                   select row).Distinct().ToList();

                if (blist.Count > 0)
                    processList = processList.Where(Entry => blist.Contains((int)Entry.BranchID)).ToList();


                if (processList != null)
                {
                    return processList;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<AuditSummaryCountView> GetAllARSData(string buttonType, List<AuditSummaryCountView> auditsummarycountview)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                auditsummarycountview = auditsummarycountview.GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                if (buttonType == "Not Done")
                {
                    auditsummarycountview = auditsummarycountview.Where(entity => entity.AuditStatusID == null).ToList();
                }
                if (buttonType == "Submited Count")
                {
                    auditsummarycountview = auditsummarycountview.Where(entity => entity.AuditStatusID == 2).ToList();
                }
                if (buttonType == "Team Review")
                {
                    auditsummarycountview = auditsummarycountview.Where(entity => entity.AuditStatusID == 4).ToList();
                }
                if (buttonType == "Auditee Review")
                {
                    auditsummarycountview = auditsummarycountview.Where(entity => entity.AuditStatusID == 6).ToList();
                }
                if (buttonType == "Final Review")
                {
                    auditsummarycountview = auditsummarycountview.Where(entity => entity.AuditStatusID == 5).ToList();
                }
                if (buttonType == "Closed")
                {
                    auditsummarycountview = auditsummarycountview.Where(entity => entity.AuditStatusID == 3).ToList();
                }
                return auditsummarycountview.ToList();
            }
        }

        public static void DeleteAllKickOffDataImplementation(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditAssigns = (from row in entities.AuditImplementationAssignments
                                    where row.AuditID == AuditID
                                    select row).ToList();

                if (AuditAssigns.Count > 0)
                {
                    AuditAssigns.ForEach(item =>
                    {
                        entities.AuditImplementationAssignments.Remove(item);
                        entities.SaveChanges();
                    });
                }
            }
        }

        public static AuditClosureDetail GetDataAuditClosureDetails(int AuditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditclosuredetail = (from row in entities.AuditClosureDetails
                                          where row.ID == AuditId
                                          select row).FirstOrDefault();
                if (auditclosuredetail != null)
                {
                    return auditclosuredetail;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<sp_GroupObsrvationCategory_Result> GetObservationCatergory(int customerBranchId, int verticalID, string fnancialYear, string forPeriod, int processID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RatingList = (from row in entities.sp_GroupObsrvationCategory(customerBranchId, processID, forPeriod, fnancialYear, verticalID)
                                  select row).ToList();
                return RatingList;
            }
        }
        public static List<int> SP_GETAssigned_AsPerformerORReviewerProcedure(int userId, string FinalcialYear, string Formonth, int Customerbrnchid, int VerticalID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> roles = new List<int>();
                var rolesfromsp = entities.SP_GETAssigned_AsPerformerORReviewer(userId, FinalcialYear, Formonth, Customerbrnchid, VerticalID, AuditID).ToList();
                roles = rolesfromsp.Where(x => x != null).Cast<int>().ToList();
                return roles;
            }
        }
        public static Sp_CheckStatusIdSubmittedCount_Result Sp_CheckStatusIdSubmittedCountProcedure(int Customerbrnchid, int VerticalID, string FinalcialYear, string Formonth, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.Sp_CheckStatusIdSubmittedCount(Customerbrnchid, VerticalID, FinalcialYear, Formonth, AuditID).FirstOrDefault();
                if (complianceReminders != null)
                {
                    return complianceReminders;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<sp_GroupObsrvation_Result> GetObservation(int customerBranchId, int verticalID, string fnancialYear, string forPeriod, int processID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RatingList = (from row in entities.sp_GroupObsrvation(customerBranchId, processID, forPeriod, fnancialYear, verticalID)
                                  select row).ToList();
                return RatingList;
            }
        }
        public static List<int> GetPerformerReviewerAuditID(long branchID, long VerticalId, string finYear, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var auditDetails = (from ACD in entities.AuditClosureDetails
                                    join ICAA in entities.InternalControlAuditAssignments
                                    on ACD.BranchId equals ICAA.CustomerBranchID
                                    where ICAA.AuditID == ACD.ID
                                    && ACD.BranchId == branchID
                                    && ACD.VerticalId == VerticalId
                                    && ACD.FinancialYear == finYear
                                    && ICAA.UserID == UserID
                                    && ACD.IsDeleted == false
                                    select ACD.ID).Distinct().ToList();

                if (auditDetails != null)
                {
                    if (auditDetails.Count > 0)
                    {
                        return auditDetails.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }
        public static bool SaveAuditStepsDetails(List<int> ActiveAuditStepIDList, long custID, Int64 CustomerBranchid, int verticalID, string finYear, string period, long auditID)
        {
            try
            {
                bool saveSuccess = false;
                long AuditID = 0;

                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    if (ActiveAuditStepIDList.Count > 0)
                    {
                        //Save Total Steps
                        var AuditDetails = (from ACD in entities.AuditClosureDetails
                                            where ACD.BranchId == CustomerBranchid
                                                             && ACD.VerticalId == verticalID
                                                             && ACD.FinancialYear == finYear
                                                             && ACD.ForMonth == period
                                                             && ACD.ID == auditID
                                            select ACD).FirstOrDefault();

                        if (AuditDetails != null)
                        {
                            if (AuditDetails.Total < ActiveAuditStepIDList.Count)
                            {
                                AuditDetails.Total += ActiveAuditStepIDList.Count;
                                AuditDetails.IsKickOff = true;
                                entities.SaveChanges();
                            }

                            AuditID = AuditDetails.ID;
                            saveSuccess = true;
                        }
                        else
                        {
                            AuditClosureDetail newACD = new AuditClosureDetail()
                            {
                                CustomerID = custID,
                                BranchId = CustomerBranchid,
                                VerticalId = verticalID,
                                FinancialYear = finYear,
                                ForMonth = period,
                                Total = ActiveAuditStepIDList.Count,
                                IsKickOff = true,
                                IsDeleted = false,
                            };

                            entities.AuditClosureDetails.Add(newACD);
                            entities.SaveChanges();

                            AuditID = newACD.ID;
                            saveSuccess = true;
                        }

                        //Save Each Step in AuditStepMapping
                        if (ActiveAuditStepIDList.Count > 0)
                        {
                            if (AuditID != 0)
                            {
                                List<AuditStepMapping> AuditStepMappingList = new List<AuditStepMapping>();

                                ActiveAuditStepIDList.ForEach(EachAuditStep =>
                                {
                                    AuditStepMapping newASM = new AuditStepMapping()
                                    {
                                        AuditID = AuditID,
                                        ATBDId = EachAuditStep,
                                        IsActive = true,
                                    };

                                    if (!RiskCategoryManagement.AuditStepMappingExists(newASM))
                                        AuditStepMappingList.Add(newASM);
                                });

                                if (AuditStepMappingList.Count > 0)
                                {
                                    saveSuccess = UserManagementRisk.CreateAuditStepMapping(AuditStepMappingList);
                                }
                            } //AuditID is ZERO
                            else
                                saveSuccess = false;
                        } //No Audit Step
                        else
                            saveSuccess = false;
                    } //No Audit Step
                    else
                        saveSuccess = false;

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }



        public static List<int> GetAuditStepDetailsCount(List<RiskActivityToBeDoneMapping> MasterRiskActivityToBeDoneMappingRecords, Int64 CustomerBranchid, int verticalID, long AuditID, int userid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    var ActiveAuditStepIDList = (from RATBDM in entities.GetRATDMDetailKickoff(userid, CustomerBranchid, verticalID, AuditID)
                                                 select (int)RATBDM).ToList();

                    //Commented by Dhammapal on 09 Dec 2021
                    //replaced with stored procedure taking time to save
                    //var ActiveAuditStepIDList = (from RATBDM in MasterRiskActivityToBeDoneMappingRecords
                    //                             join IAS in entities.InternalAuditSchedulings
                    //                             on RATBDM.CustomerBranchID equals IAS.CustomerBranchId
                    //                             join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                    //                             on IAS.Process equals EAAMR.ProcessId
                    //                             join SP in entities.AuditScheduling_SubProcessMapping
                    //                             on RATBDM.SubProcessId equals SP.SubProcessID
                    //                             where IAS.CustomerBranchId == EAAMR.BranchID
                    //                             && RATBDM.VerticalID == verticalID
                    //                             && EAAMR.UserID == userid
                    //                             && RATBDM.IsActive == true
                    //                             && IAS.IsDeleted == false
                    //                             && IAS.Process == RATBDM.ProcessId
                    //                             && IAS.AuditID == AuditID
                    //                             && EAAMR.ISACTIVE == true
                    //                             && IAS.AuditID == SP.AuditID
                    //                             && SP.IsDeleted == false
                    //                             && IAS.CustomerBranchId == CustomerBranchid
                    //                             && RATBDM.CustomerBranchID == CustomerBranchid
                    //                             select RATBDM.ID).Distinct().ToList();


                    //var ActiveAuditStepIDList = (from RATBDM in MasterRiskActivityToBeDoneMappingRecords
                    //                            join IAS in entities.InternalAuditSchedulings
                    //                            on RATBDM.CustomerBranchID equals IAS.CustomerBranchId                                                
                    //                            where RATBDM.VerticalID == verticalID
                    //                            && RATBDM.IsActive == true
                    //                            && IAS.IsDeleted == false
                    //                            &&  IAS.Process == RATBDM.ProcessId
                    //                            && IAS.AuditID == AuditID                                               
                    //                            select RATBDM.ID).Distinct().ToList();


                    if (ActiveAuditStepIDList.ToList().Count > 0)
                    {
                        return ActiveAuditStepIDList.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static bool ReAssignedProcessOwner(List<Tuple<long, long, long, long>> TList)
        {
            try
            {
                // Tuple < long OldUserID, long NewUserid, long Branchid, long ProcessID>
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    if (TList.Count > 0)
                    {
                        foreach (var item in TList)
                        {
                            //Get Assigned ProcessList
                            var assignedProcessList = (from EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                       where EAAMR.UserID == item.Item1 && EAAMR.BranchID == item.Item3
                                                       && EAAMR.ProcessId == item.Item4 && EAAMR.ISACTIVE == true
                                                       select EAAMR).FirstOrDefault();

                            if (assignedProcessList != null)
                            {
                                //Update Process Details                        
                                assignedProcessList.UserID = item.Item2;
                                entities.SaveChanges();
                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static string GetVerticalName(int customerid, int verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Customer = (from row in entities.mst_Vertical
                                where row.CustomerID == customerid
                                && row.ID == verticalid
                                select row.VerticalName).FirstOrDefault();
                if (Customer != null)
                {
                    return Customer;
                }
                else
                {
                    return null;
                }
            }
        }
        public static string GetCustomerBranchName(int customerid, Int64 customerbranchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Customer = (from row in entities.mst_CustomerBranch
                                where row.CustomerID == customerid
                                && row.ID == customerbranchid && row.IsDeleted == false && row.Status == 1
                                select row.Name).FirstOrDefault();
                if (Customer != null)
                {
                    return Customer;
                }
                else
                {
                    return null;
                }
            }
        }
        public static string GetCustomerName(int customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Customer = (from row in entities.mst_Customer
                                where row.ID == customerid
                                select row.Name).FirstOrDefault();
                if (Customer != null)
                {
                    return Customer;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<int> GetAuditManagerAuditID(long branchID, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditDetails = (from ACD in entities.AuditClosureDetails
                                    join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                    on ACD.BranchId equals EAAR.BranchID
                                    join ICAA in entities.InternalControlAuditAssignments
                                    on EAAR.BranchID equals ICAA.CustomerBranchID
                                    where ICAA.AuditID == ACD.ID
                                    && EAAR.ProcessId == ICAA.ProcessId
                                    && EAAR.UserID == UserID
                                    && ACD.BranchId == branchID
                                    && ACD.IsDeleted == false
                                    && EAAR.ISACTIVE == true
                                    select ACD.ID).Distinct().ToList();

                if (auditDetails != null)
                {
                    if (auditDetails.Count > 0)
                    {
                        return auditDetails.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }
        public static long CreateAuditID(long branchID, long verticalID, string finYear, string period, int customerID)
        {
            long AuditID = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                AuditClosureDetail newACD = new AuditClosureDetail()
                {
                    CustomerID = customerID,
                    BranchId = branchID,
                    VerticalId = verticalID,
                    FinancialYear = finYear,
                    ForMonth = period,
                    Total = 0,
                    Closed = 0,
                    IsDeleted = false,
                };

                entities.AuditClosureDetails.Add(newACD);
                entities.SaveChanges();
                AuditID = newACD.ID;

                return AuditID;
            }
        }
        public static FeedbackFormUpload GetFeedBackFormDate(List<int> auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fDate = (from row in entities.FeedbackFormUploads
                             where auditID.Contains((int)row.AuditId)
                             select row).FirstOrDefault();
                return fDate;
            };
        }
        public static bool IsExistAuditScheduling(Int64 CustomerBranchid, int verticalID, int ProcessID, string finYear, long AuditID, string period, int NoofPhases = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (!period.Contains("Phase"))
                {
                    var ProcessList = (from IAS in entities.InternalAuditSchedulings
                                       where IAS.CustomerBranchId == CustomerBranchid
                                       && IAS.VerticalID == verticalID
                                       && IAS.FinancialYear == finYear
                                       && IAS.TermName == period
                                       && IAS.Process == ProcessID
                                       && IAS.IsDeleted == false
                                       && IAS.AuditID == AuditID
                                       select IAS).ToList();
                    if (ProcessList.Count > 0)
                        return false;
                    else
                        return true;
                }
                else
                {
                    var ProcessList = (from IAS in entities.InternalAuditSchedulings
                                       where IAS.CustomerBranchId == CustomerBranchid
                                       && IAS.VerticalID == verticalID
                                       && IAS.FinancialYear == finYear
                                       && IAS.TermName == period
                                       && IAS.Process == ProcessID
                                       && IAS.PhaseCount == NoofPhases
                                       && IAS.IsDeleted == false
                                       && IAS.AuditID == AuditID
                                       select IAS).ToList();
                    if (ProcessList.Count > 0)
                        return false;
                    else
                        return true;
                }
            }
        }
        public static void UpdateDetailsInternalAuditSchedulingTable(List<InternalAuditScheduling> Tempassignments)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    var Scheduledetails = (from row in entities.InternalAuditSchedulings
                                           where row.FinancialYear == entry.FinancialYear
                                           && row.ISAHQMP == entry.ISAHQMP && row.CustomerBranchId == entry.CustomerBranchId
                                           && row.TermName == entry.TermName && row.VerticalID == entry.VerticalID
                                           && row.Process == entry.Process && row.AuditID == entry.AuditID
                                           select row).FirstOrDefault();

                    if (Scheduledetails == null)
                    {
                        entities.InternalAuditSchedulings.Add(entry);
                        entities.SaveChanges();
                    }
                    else
                    {
                        Scheduledetails.IsDeleted = false;
                        entities.SaveChanges();
                    }

                });
            }
        }
        public static long InternalControlAuditAssignmentsPerformerID(Int64 CustomerBranchid, int Verticalid, string FinancialYear, string ForMonth, int AuditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from ICAA in entities.InternalControlAuditAssignments
                             join IASO in entities.InternalAuditScheduleOns
                             on ICAA.InternalAuditInstance equals IASO.InternalAuditInstance
                             where ICAA.ProcessId == IASO.ProcessId
                             && IASO.FinancialYear == FinancialYear
                             && IASO.ForMonth == ForMonth
                             && ICAA.CustomerBranchID == CustomerBranchid
                             && ICAA.VerticalID == Verticalid
                             && ICAA.RoleID == 3
                             && ICAA.AuditID == AuditId
                             select ICAA.UserID).FirstOrDefault();

                if (query != 0)
                {
                    return query;
                }
                else
                {
                    return -1;
                }
            }
        }
        public static List<AuditMYReoprt> getAuditReports(int customerid, List<int?> BranchList, int VerticalID, string FinancialYear, string Period, int RoleID, int userID, string auditHeadFlag, string departmentheadFlag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditMYReoprt> masterquery = new List<AuditMYReoprt>();
                List<AuditMYReoprt> transactionquery = new List<AuditMYReoprt>();

                masterquery = (from row in entities.AuditMYReoprts
                               where row.CustomerID == customerid
                               select row).ToList();
                if (CustomerManagementRisk.CheckIsManagement(userID) == 8)
                {
                    transactionquery = (from row in masterquery
                                        join ICAA in entities.InternalControlAuditAssignments
                                        on row.AuditID equals ICAA.AuditID
                                        join EAAR in entities.EntitiesAssignmentManagementRisks
                                        on ICAA.ProcessId equals (int)EAAR.ProcessId
                                        where row.HubID == (int)EAAR.BranchID
                                        && EAAR.UserID == userID
                                        && row.RoleID == 4
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();

                    if (transactionquery.Count > 0)
                        transactionquery = transactionquery.GroupBy(entity => entity.AuditID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (auditHeadFlag == "AM" || auditHeadFlag == "AH")
                {
                    entities.Database.CommandTimeout = 300;
                    transactionquery = (from row in masterquery
                                        join ICAA in entities.InternalControlAuditAssignments
                                        on row.AuditID equals ICAA.AuditID
                                        join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                        on ICAA.ProcessId equals (int)EAAR.ProcessId
                                        where row.HubID == ICAA.CustomerBranchID
                                        && EAAR.UserID == userID
                                        && row.RoleID == 4
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();
                    if (transactionquery.Count > 0)
                        transactionquery = transactionquery.GroupBy(entity => entity.AuditID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else if (departmentheadFlag == "DH")
                {


                    transactionquery = (from row in masterquery
                                        join ICAA in entities.InternalControlAuditAssignments
                                        on row.AuditID equals ICAA.AuditID
                                        join EAAR in entities.EntitiesAssignmentDepartmentHeads
                                        on row.HubID equals (int)EAAR.BranchID
                                        join MSP in entities.Mst_Process
                                        on ICAA.ProcessId equals MSP.Id
                                        where EAAR.DepartmentID == MSP.DepartmentID
                                        && EAAR.UserID == userID
                                        && row.RoleID == 4
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();

                    if (transactionquery.Count > 0)
                        transactionquery = transactionquery.GroupBy(entity => entity.AuditID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else
                {
                    transactionquery = (from row in entities.AuditMYReoprts
                                        where row.UserID == userID
                                        select row).ToList();
                }

                if (customerid != -1)
                    transactionquery = transactionquery.Where(Entry => Entry.CustomerID == customerid).ToList();
                if (BranchList.Count > 0)
                    transactionquery = transactionquery.Where(Entry => BranchList.Contains(Entry.HubID)).ToList();      /* HubID refers to BranchID */
                if (VerticalID != -1)
                    transactionquery = transactionquery.Where(Entry => Entry.VerticalID == VerticalID).ToList();
                if (FinancialYear != "")
                    transactionquery = transactionquery.Where(Entry => Entry.FinancialYear == FinancialYear).ToList();
                if (Period != "")
                    transactionquery = transactionquery.Where(Entry => Entry.Period == Period).ToList();
                //if (RoleID != -1)
                //    transactionquery = transactionquery.Where(Entry => Entry.RoleID == RoleID).ToList();

                return transactionquery;
            }
        }
        public static List<Sp_MyDocumentICFRView_Result> GetICFROrignalFilesData(int Customerid, Int64 CustomerBranchId, int UserID, int RoleID, int RiskCreationId, string FinancialYear, string ForMonth, string auditHeadFlag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_MyDocumentICFRView_Result> query = new List<Sp_MyDocumentICFRView_Result>();

                query = (from row in entities.Sp_MyDocumentICFRView("NA", Customerid, UserID)
                         where row.CustomerBranchId == CustomerBranchId && row.RoleID == RoleID
                         && row.RiskCreationId == RiskCreationId
                         && row.FinancialYear == FinancialYear && row.ForMonth == ForMonth
                         select row).ToList();
                query = query.GroupBy(s => new { s.ControlNo, s.Name }).Select(entity => entity.FirstOrDefault()).ToList();

                return query.ToList();
            }
        }
        public static List<Sp_MyDocumentICFRView_Result> GetAllIFCDocuments(List<long> branchlist, string financialYear, string forMonth, int processid, int subProcessID, int userID, int RoleID, string auditHeadFlag, int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_MyDocumentICFRView_Result> query = new List<Sp_MyDocumentICFRView_Result>();

                if (auditHeadFlag == "AM" || auditHeadFlag == "AH") /*if (CheckUserISAuditManager(userID) == "AM")*/
                {
                    List<long> BranchAssigned = CheckAuditManagerLocation(userID);

                    query = (from row in entities.Sp_MyDocumentICFRView("AM", Customerid, userID)
                             where BranchAssigned.Contains(row.CustomerBranchId)
                             select row).ToList();
                }
                else
                {
                    query = (from row in entities.Sp_MyDocumentICFRView("NA", Customerid, userID)
                             where row.RoleID == RoleID
                             select row).ToList();
                }

                if (branchlist.Count > 0)
                    query = query.Where(entry => branchlist.Contains(entry.CustomerBranchId)).ToList();

                if (financialYear != "")
                    query = query.Where(entry => entry.FinancialYear == financialYear).ToList();

                if (forMonth != "Period" && forMonth != "")
                    query = query.Where(entry => entry.ForMonth == forMonth).ToList();

                if (processid != -1)
                    query = query.Where(entry => entry.ProcessId == processid).ToList();

                if (subProcessID != -1)
                    query = query.Where(entry => entry.SubProcessId == subProcessID).ToList();

                if (query.Count > 0)
                    query = query.OrderBy(entry => entry.ControlNo).ToList();

                if (query.Count > 0)
                    query = query.GroupBy(entity => entity.ControlNo).Select(entity => entity.FirstOrDefault()).ToList();
                return query.ToList();
            }
        }

        //public static List<MyDocumentICFRView> GetAllIFCDocuments(List<long> branchlist, string financialYear, string forMonth, int processid, int subProcessID, int userID, int RoleID, string auditHeadFlag)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<MyDocumentICFRView> query = new List<MyDocumentICFRView>();

        //        if (auditHeadFlag == "AM" || auditHeadFlag == "AH") /*if (CheckUserISAuditManager(userID) == "AM")*/
        //        {
        //            List<long> BranchAssigned = CheckAuditManagerLocation(userID);

        //            query = (from row in entities.MyDocumentICFRViews
        //                     where BranchAssigned.Contains(row.CustomerBranchId)
        //                     select row).ToList();
        //        }
        //        else
        //        {
        //            query = (from row in entities.MyDocumentICFRViews
        //                     where row.UserID == userID && row.RoleID == RoleID
        //                     select row).ToList();
        //        }

        //        if (branchlist.Count > 0)
        //            query = query.Where(entry => branchlist.Contains(entry.CustomerBranchId)).ToList();

        //        if (financialYear != "")
        //            query = query.Where(entry => entry.FinancialYear == financialYear).ToList();

        //        if (forMonth != "Period" && forMonth != "")
        //            query = query.Where(entry => entry.ForMonth == forMonth).ToList();

        //        if (processid != -1)
        //            query = query.Where(entry => entry.ProcessId == processid).ToList();

        //        if (subProcessID != -1)
        //            query = query.Where(entry => entry.SubProcessId == subProcessID).ToList();

        //        if (query.Count > 0)
        //            query = query.OrderBy(entry => entry.ControlNo).ToList();

        //        return query.ToList();
        //    }
        //}
        public static void AddObservationDraft(Observation_DraftList objauditDraft)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Observation_DraftList.Add(objauditDraft);
                entities.SaveChanges();
            }
        }
        public static void UpdatePersonResposible(InternalAuditTransaction objPersonResp)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<InternalAuditTransaction> objcust = (from row in entities.InternalAuditTransactions
                                                          where row.ATBDId == objPersonResp.ATBDId && row.FinancialYear == objPersonResp.FinancialYear &&
                                                    row.VerticalID == objPersonResp.VerticalID && row.ForPeriod == objPersonResp.ForPeriod && row.CustomerBranchId == objPersonResp.CustomerBranchId
                                                          select row).ToList();
                objcust.ForEach(entry =>
                {
                    entry.ATBDId = objPersonResp.ATBDId;
                    entry.PersonResponsible = objPersonResp.PersonResponsible;
                });

                entities.SaveChanges();
            }
        }
        public static object GetQuarterList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.Mst_Quarter
                             select row).Distinct();


                var users = (from row in query
                             select new { ID = row.Id, Name = row.QuarterName }).OrderBy(entry => entry.ID).ToList<object>();

                return users;
            }
        }
        public static object GetQuarterList(int UserID, Int64 Customerbranchid, int ProcessId, string FinancialYear, int Atbtid, string flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var getriskCreationId = (
                                   from row in entities.RiskActivityToBeDoneMappings
                                   where row.ID == Atbtid
                                   select row.RiskCategoryCreationId
                            ).FirstOrDefault();
                List<string> ForMonths = new List<string>();
                if (flag == "P")
                {
                    ForMonths = (from ASO in entities.AuditScheduleOns
                                 join AA in entities.AuditAssignments
                                 on ASO.RiskCreationID equals AA.RiskCreationId
                                 join AT in entities.AuditTransactions
                                 on ASO.ID equals AT.AuditScheduleOnID
                                 where ASO.AuditInstanceId == AA.AuditInstanceId
                                 && AA.UserID == UserID
                                 && AA.CustomerBranchID == Customerbranchid
                                 && AA.ProcessId == ProcessId
                                 && AA.RiskCreationId == getriskCreationId
                                 && ASO.FinancialYear == FinancialYear
                                 && AT.StatusId == 2
                                 select ASO.ForMonth).ToList();

                }
                else if (flag == "R")
                {
                    ForMonths = (from ASO in entities.AuditScheduleOns
                                 join AA in entities.AuditAssignments
                                 on ASO.RiskCreationID equals AA.RiskCreationId
                                 join AT in entities.AuditTransactions
                                 on ASO.ID equals AT.AuditScheduleOnID
                                 where ASO.AuditInstanceId == AA.AuditInstanceId
                                 && AA.UserID == UserID
                                 && AA.CustomerBranchID == Customerbranchid
                                 && AA.ProcessId == ProcessId
                                 && AA.RiskCreationId == getriskCreationId
                                 && ASO.FinancialYear == FinancialYear
                                 && AT.StatusId == 3
                                 select ASO.ForMonth).ToList();

                }


                var query = (from ASO in entities.AuditScheduleOns
                             join AA in entities.AuditAssignments
                             on ASO.RiskCreationID equals AA.RiskCreationId
                             join MQ in entities.Mst_Quarter
                             on ASO.ForMonth equals MQ.QuarterName
                             where ASO.AuditInstanceId == AA.AuditInstanceId
                             && AA.UserID == UserID
                             && AA.CustomerBranchID == Customerbranchid
                             && AA.ProcessId == ProcessId
                             && ASO.FinancialYear == FinancialYear
                             select MQ).Distinct();

                if (ForMonths.Count > 0)
                {
                    query = query.Where(entry => !ForMonths.Contains(entry.QuarterName));
                }


                var users = (from row in query
                             select new { ID = row.Id, Name = row.QuarterName }).OrderBy(entry => entry.ID).ToList<object>();

                return users;
            }
        }
        public static bool CreateScheduleOn(string ForPeriod, long AuditInstanceID, long RiskCreationId, long createdByID, string creatdByName, string FinancialYear, long branchid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    DateTime curruntDate = DateTime.Now;
                    List<AuditTransaction> transactionlist = new List<AuditTransaction>();
                    AuditScheduleOn auditScheduleon = new AuditScheduleOn();
                    auditScheduleon.AuditInstanceId = AuditInstanceID;
                    auditScheduleon.RiskCreationID = RiskCreationId;
                    auditScheduleon.ForMonth = ForPeriod;
                    auditScheduleon.FinancialYear = FinancialYear;
                    entities.AuditScheduleOns.Add(auditScheduleon);
                    entities.SaveChanges();

                    AuditTransaction transaction = new AuditTransaction()
                    {
                        AuditInstanceId = AuditInstanceID,
                        RiskCreationID = RiskCreationId,
                        AuditScheduleOnID = auditScheduleon.ID,
                        CreatedBy = createdByID,
                        CreatedByText = creatdByName,
                        StatusId = 1,
                        CustomerBranchId = branchid,
                        Remarks = "New Risk Control Assigned.",
                        FinancialYear = FinancialYear
                    };

                    transactionlist.Add(transaction);
                    CreateTransaction(transactionlist);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool ReAssignControlTestingAssignment(long rccID, int OldUserID, long NewUserID, string newUserName, int roleID, int custBranchID, int processid, int subprocessid, string finYear)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var assignmentData = (from AA in entities.AuditAssignments
                                          join ASO in entities.AuditScheduleOns
                                          on AA.RiskCreationId equals ASO.RiskCreationID
                                          where AA.RiskCreationId == rccID
                                          && AA.UserID == OldUserID
                                          && AA.RoleID == roleID
                                          && AA.CustomerBranchID == custBranchID
                                          && AA.ProcessId == processid
                                          && AA.SubProcessId == subprocessid
                                          && ASO.FinancialYear == finYear
                                          select AA.ID).Distinct().ToList();

                    if (assignmentData.Count > 0)
                    {
                        var AssignmentRecordsToUpdate = (from AA in entities.AuditAssignments
                                                         where assignmentData.Contains(AA.ID)
                                                         select AA).ToList();

                        AssignmentRecordsToUpdate.ForEach(entry => entry.UserID = NewUserID);
                    }

                    //var scheduleOnIDData = (from AA in entities.AuditAssignments
                    //                        join ASO in entities.AuditScheduleOns
                    //                        on AA.RiskCreationId equals ASO.RiskCreationID
                    //                        where AA.RiskCreationId == rccID
                    //                        && AA.UserID == OldUserID
                    //                        && AA.RoleID == roleID
                    //                        && AA.CustomerBranchID == custBranchID
                    //                        && AA.ProcessId == processid
                    //                        && AA.SubProcessId == subprocessid
                    //                        && ASO.FinancialYear == finYear
                    //                        select ASO.ID).ToList();



                    //if (scheduleOnIDData.Count > 0)
                    //{
                    //    var TransactionsToUpdate = (from AT in entities.AuditTransactions
                    //                                where scheduleOnIDData.Contains((long)AT.AuditScheduleOnID)
                    //                                select AT).ToList();

                    //    TransactionsToUpdate.ForEach(entry => entry.CreatedBy = NewUserID);
                    //    TransactionsToUpdate.ForEach(entry => entry.CreatedByText = newUserName);
                    //}

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static AuditClosureDetail GetAuditDatail(long auditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.AuditClosureDetails
                               where row.ID == auditId
                               select row).FirstOrDefault();
                if (objcust != null)
                {
                    return objcust;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<sp_Observation_DraftList_Result> GetObservationDetails(long branchid, string finYear, string period, int verticalid, int processID, int Statusid, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.sp_Observation_DraftList(branchid, finYear, period, verticalid, AuditID)
                               select row).ToList();

                if (processID != -1)
                {
                    objcust = objcust.Where(entry => entry.ProcessId == processID).ToList();
                }
                if (Statusid != -1)
                {
                    objcust = objcust.Where(entry => entry.AuditStatusID == Statusid).ToList(); // changed by sagar more on 16-01-2020
                }

                return objcust.ToList();
            }
        }
        public static List<ControlTestingAssignmentDetails> GetAllReassignDataICFR(int customerID, List<long> branchList, int userID, int processid, int subprocessid, int roleid, string finYear, string ForMonth)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ControlTestingAssignmentRecords = (from AITV in entities.AuditInstanceTransactionViews
                                                       where AITV.UserID == userID
                                                       && AITV.CustomerID == customerID
                                                       && AITV.RoleID == roleid
                                                       select new ControlTestingAssignmentDetails
                                                       {
                                                           RiskCreationId = AITV.RiskCreationId,
                                                           AuditInstanceID = AITV.AuditInstanceID,
                                                           //ScheduledOnID = AITV.ScheduledOnID,
                                                           ControlNo = AITV.ControlNo,
                                                           ActivityDescription = AITV.ActivityDescription,
                                                           ControlObjective = AITV.ControlObjective,
                                                           ControlDescription = AITV.ControlDescription,
                                                           CustomerBranchID = AITV.CustomerBranchID,
                                                           Branch = AITV.Branch,
                                                           FinancialYear = AITV.FinancialYear,
                                                           ForMonth = AITV.ForMonth,
                                                           ProcessId = AITV.ProcessId,
                                                           SubProcessId = (long)AITV.SubProcessId,
                                                       }).Distinct().ToList();

                if (finYear != "")
                    ControlTestingAssignmentRecords = ControlTestingAssignmentRecords.Where(entry => entry.FinancialYear == finYear).ToList();

                if (ForMonth != "")
                    ControlTestingAssignmentRecords = ControlTestingAssignmentRecords.Where(entry => entry.ForMonth == ForMonth).ToList();

                if (branchList.Count > 0)
                    ControlTestingAssignmentRecords = ControlTestingAssignmentRecords.Where(entry => branchList.Contains(entry.CustomerBranchID)).ToList();

                if (processid != -1)
                    ControlTestingAssignmentRecords = ControlTestingAssignmentRecords.Where(entry => entry.ProcessId == processid).ToList();

                if (subprocessid != -1)
                    ControlTestingAssignmentRecords = ControlTestingAssignmentRecords.Where(entry => entry.SubProcessId == subprocessid).ToList();

                if (ControlTestingAssignmentRecords.Count > 0)
                    ControlTestingAssignmentRecords = ControlTestingAssignmentRecords.OrderBy(entry => entry.ControlNo).ToList();

                return ControlTestingAssignmentRecords;
            }
        }
        public static mst_User GetByID(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var user = (from row in entities.mst_User
                            where row.ID == userID && row.IsDeleted == false && row.IsActive == true
                            select row).SingleOrDefault();

                return user;
            }
        }
        public static void AddDetailsInternalAuditScheduleOn(InternalAuditScheduleOn IASO)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.InternalAuditScheduleOns.Add(IASO);
                entities.SaveChanges();
            }
        }
        public static bool CreateAuditStepMapping(List<AuditStepMapping> AuditStepMappingList)
        {
            try
            {
                int saveCount = 0;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //entities.BulkInsert(AuditStepMappingList);

                    AuditStepMappingList.ForEach(entry =>
                    {
                        entities.AuditStepMappings.Add(entry);
                        saveCount++;
                        if (saveCount > 100)
                        {
                            entities.SaveChanges();
                            saveCount = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static List<mst_Vertical> FillVerticalList(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.mst_Vertical
                               join row1 in entities.BranchVerticals
                               on row.CustomerID equals row1.CustomerID
                               where (row1.CustomerID == customerID)
                               && row1.IsActive == true
                               select row).Distinct();
                return objcust.ToList();
            }
        }
        public static bool ReAssignedAudit(int OldUserID, long NewUserID, int roleID, long AuditID, int ProcessID, int SubProcessID, int BranchID, int VerticalID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //Get Assigned Audits --- InstanceIDs
                    var assignedAuditInstanceList = (from ACV in entities.Sp_UserwiseAssignedAudit(OldUserID)
                                                     where ACV.AuditID == AuditID
                                                     && ACV.ProcessId == ProcessID
                                                     && ACV.SubProcessId == SubProcessID
                                                     select ACV.InstanceID).Distinct().ToList();

                    if (assignedAuditInstanceList.Count > 0)
                    {
                        //Update Assignment Details
                        assignedAuditInstanceList.ForEach(EachInstance =>
                        {
                            var assignmentToUpdate = (from ICAA in entities.InternalControlAuditAssignments
                                                      where ICAA.InternalAuditInstance == EachInstance
                                                      && ICAA.AuditID == AuditID
                                                             && ICAA.UserID == OldUserID
                                                             && ICAA.RoleID == roleID
                                                             && ICAA.ProcessId == ProcessID
                                                             && ICAA.SubProcessId == SubProcessID
                                                      select ICAA).FirstOrDefault();
                            if (assignmentToUpdate != null)
                                assignmentToUpdate.UserID = NewUserID;
                        });

                        var ATBDIDList = (from row in entities.RiskActivityToBeDoneMappings
                                          where row.CustomerBranchID == BranchID
                                          && row.VerticalID == VerticalID
                                          && row.ProcessId == ProcessID
                                          && row.SubProcessId == SubProcessID
                                          select row.ID).ToList();
                        //Update Audit Step Result Details
                        assignedAuditInstanceList.ForEach(EachInstance =>
                        {
                            ATBDIDList.ForEach(eachATBDId =>
                                {
                                    var auditResultToUpdate = (from ICAR in entities.InternalControlAuditResults
                                                               where ICAR.InternalAuditInstance == EachInstance
                                                         && ICAR.UserID == OldUserID
                                                         && ICAR.AuditID == AuditID
                                                         && ICAR.RoleID == roleID
                                                         && ICAR.ProcessId == ProcessID
                                                         && ICAR.ATBDId == eachATBDId
                                                               select ICAR).ToList();


                                    auditResultToUpdate.ForEach(EachResultRecord =>
                                {
                                    EachResultRecord.UserID = NewUserID;
                                });
                                });
                        });

                        //Update Audit Transaction Details

                        assignedAuditInstanceList.ForEach(EachInstance =>
                        {
                            ATBDIDList.ForEach(eachATBDId =>
                            {
                                var auditTransactionToUpdate = (from ICAR in entities.InternalAuditTransactions
                                                                where ICAR.InternalAuditInstance == EachInstance
                                                                && ICAR.UserID == OldUserID
                                                                && ICAR.AuditID == AuditID
                                                                && ICAR.RoleID == roleID
                                                                && ICAR.ProcessId == ProcessID
                                                                && ICAR.ATBDId == eachATBDId
                                                                select ICAR).ToList();

                                auditTransactionToUpdate.ForEach(EachResultRecord =>
                                {
                                    EachResultRecord.UserID = NewUserID;
                                });
                            });
                        });

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static long CheckProcessInstanceIDExist(InternalAuditInstance riskinstance)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditInstances
                             where row.AuditID == riskinstance.AuditID
                             && row.ProcessId == riskinstance.ProcessId
                             select row.ID).FirstOrDefault();
                if (query > 0)
                {
                    return query;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static bool ReAssignedIMPAudit(int OldUserID, long NewUserID, int roleID, int custBranchID, int verticalID, string finYear, string period, long AuditId, int CustomerID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //Get Assigned Audits --- InstanceIDs
                    var assignedAuditInstanceList = (from IASCV in entities.SP_ImplementationAuditSummary(CustomerID)
                                                     where IASCV.CustomerBranchID == custBranchID
                                                     && IASCV.VerticalID == verticalID
                                                     && IASCV.FinancialYear == finYear
                                                     && IASCV.ForMonth == period
                                                     && IASCV.UserID == OldUserID
                                                     && IASCV.RoleID == roleID
                                                     && IASCV.AuditID == AuditId
                                                     select IASCV.InstanceID).Distinct().ToList();

                    if (assignedAuditInstanceList.Count > 0)
                    {
                        //Update Assignment Details
                        assignedAuditInstanceList.ForEach(EachInstance =>
                        {
                            var assignmentToUpdate = (from AIA in entities.AuditImplementationAssignments
                                                      where AIA.ImplementationInstance == EachInstance
                                                        && AIA.UserID == OldUserID
                                                        && AIA.RoleID == roleID
                                                      select AIA).FirstOrDefault();
                            if (assignmentToUpdate != null)
                                assignmentToUpdate.UserID = NewUserID;
                        });

                        //Update Audit Step Result Details
                        assignedAuditInstanceList.ForEach(EachInstance =>
                        {
                            var auditResultToUpdate = (from IAR in entities.ImplementationAuditResults
                                                       where IAR.ImplementationInstance == EachInstance
                                                         && IAR.UserID == OldUserID
                                                         && IAR.RoleID == roleID
                                                       select IAR).ToList();

                            auditResultToUpdate.ForEach(EachResultRecord =>
                            {
                                EachResultRecord.UserID = NewUserID;
                            });
                        });

                        //Update Audit Transaction Details
                        assignedAuditInstanceList.ForEach(EachInstance =>
                        {
                            var auditTransactionToUpdate = (from AIT in entities.AuditImplementationTransactions
                                                            where AIT.ImplementationInstance == EachInstance
                                                            && AIT.UserID == OldUserID
                                                            && AIT.RoleID == roleID
                                                            select AIT).ToList();

                            auditTransactionToUpdate.ForEach(EachResultRecord =>
                            {
                                EachResultRecord.UserID = NewUserID;
                            });
                        });

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }



        public static bool CheckRiskData(int processid, int userID, int Atbdid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from RATBDM in entities.RiskActivityToBeDoneMappings
                             join RCC in entities.RiskCategoryCreations
                             on RATBDM.RiskCategoryCreationId equals RCC.Id
                             join AA in entities.AuditAssignments
                             on RATBDM.RiskCategoryCreationId equals AA.RiskCreationId
                             where RATBDM.CustomerBranchID == RCC.CustomerBranchId
                             && RATBDM.ProcessId == AA.ProcessId
                             && RATBDM.ID == Atbdid
                             && RATBDM.ProcessId == processid
                             && AA.UserID == userID
                             && RCC.ControlNo.ToUpper().Trim() != "NA"
                             select RATBDM).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static List<OpenCloseAuditDetailsClass> getAllMyDocumentFile(int customerid, List<int?> BranchList, int VerticalID, string FinancialYear, string Period, int RoleID, int userID, int Statusid, string auditHeadFlag, string departmentheadFlag) /*int CustBranchID,*/
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditCountView> masterquery = new List<AuditCountView>();
                List<OpenCloseAuditDetailsClass> transactionquery = new List<OpenCloseAuditDetailsClass>();
                entities.Database.CommandTimeout = 300;
                masterquery = (from row in entities.AuditCountViews
                               select row).ToList();

                if (CustomerManagementRisk.CheckIsManagement(userID) == 8)
                {
                    #region Management                  
                    if (Statusid == 2)
                    {
                        transactionquery = (from C in masterquery
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAR in entities.EntitiesAssignmentManagementRisks
                                            on C.ProcessId equals (int)EAAR.ProcessId
                                            where EAAR.ProcessId == ICAA.ProcessId
                                            && EAAR.UserID == userID
                                            && C.RoleID == 4
                                            && EAAR.ISACTIVE == true
                                            && C.ACCStatus == null
                                            group C by new
                                            {
                                                C.CustomerID,
                                                C.CustomerBranchID,
                                                C.Branch,
                                                C.ForMonth,
                                                C.RoleID,
                                                C.UserID,
                                                C.Role,
                                                C.User,
                                                C.FinancialYear,
                                                C.ExpectedStartDate,
                                                C.ExpectedEndDate,
                                                C.VerticalsId,
                                                C.VerticalName,
                                                C.AuditID,
                                                //C.CProcessName,
                                                //C.SubProcess
                                            } into GCS
                                            select new OpenCloseAuditDetailsClass()
                                            {
                                                CustomerID = GCS.Key.CustomerID,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                Branch = GCS.Key.Branch,
                                                ForMonth = GCS.Key.ForMonth,
                                                RoleID = GCS.Key.RoleID,
                                                UserID = GCS.Key.UserID,
                                                Role = GCS.Key.Role,
                                                User = GCS.Key.User,
                                                FinancialYear = GCS.Key.FinancialYear,
                                                ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                                ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                                VerticalId = GCS.Key.VerticalsId,
                                                VerticalName = GCS.Key.VerticalName,
                                                AuditID = GCS.Key.AuditID,
                                                //CProcessName = GCS.Key.CProcessName,
                                                //SubProcess = GCS.Key.SubProcess,
                                            }).ToList();


                    }
                    else if (Statusid == 1)
                    {
                        transactionquery = (from C in masterquery
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAR in entities.EntitiesAssignmentManagementRisks
                                            on C.ProcessId equals (int)EAAR.ProcessId
                                            where EAAR.ProcessId == ICAA.ProcessId
                                            && EAAR.UserID == userID
                                            && C.RoleID == 4
                                            && EAAR.ISACTIVE == true
                                            && C.ACCStatus == 1
                                            group C by new
                                            {
                                                C.CustomerID,
                                                C.CustomerBranchID,
                                                C.Branch,
                                                C.ForMonth,
                                                C.RoleID,
                                                C.UserID,
                                                C.Role,
                                                C.User,
                                                C.FinancialYear,
                                                C.ExpectedStartDate,
                                                C.ExpectedEndDate,
                                                C.VerticalsId,
                                                C.VerticalName,
                                                C.AuditID,
                                                //C.CProcessName,
                                                //C.SubProcess,
                                            } into GCS
                                            select new OpenCloseAuditDetailsClass()
                                            {
                                                CustomerID = GCS.Key.CustomerID,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                Branch = GCS.Key.Branch,
                                                ForMonth = GCS.Key.ForMonth,
                                                RoleID = GCS.Key.RoleID,
                                                UserID = GCS.Key.UserID,
                                                Role = GCS.Key.Role,
                                                User = GCS.Key.User,
                                                FinancialYear = GCS.Key.FinancialYear,
                                                ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                                ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                                VerticalId = GCS.Key.VerticalsId,
                                                VerticalName = GCS.Key.VerticalName,
                                                AuditID = GCS.Key.AuditID,
                                                //CProcessName = GCS.Key.CProcessName,
                                                //SubProcess = GCS.Key.SubProcess,
                                            }).ToList();

                    }
                    #endregion
                }
                else if (auditHeadFlag == "AM" || auditHeadFlag == "AH") /*if (CheckUserISAuditManager(userID) == "AM")*/
                {
                    #region Audit Head

                    if (Statusid == 2)
                    {
                        transactionquery = (from C in masterquery
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.ProcessId equals (int)EAAR.ProcessId
                                            where EAAR.ProcessId == ICAA.ProcessId
                                            && EAAR.UserID == userID
                                            && C.RoleID == 4
                                            && EAAR.ISACTIVE == true
                                            && C.ACCStatus == null
                                            group C by new
                                            {
                                                C.CustomerID,
                                                C.CustomerBranchID,
                                                C.Branch,
                                                C.ForMonth,
                                                C.RoleID,
                                                C.UserID,
                                                C.Role,
                                                C.User,
                                                C.FinancialYear,
                                                C.ExpectedStartDate,
                                                C.ExpectedEndDate,
                                                C.VerticalsId,
                                                C.VerticalName,
                                                C.AuditID,
                                                //C.CProcessName,
                                                //C.SubProcess,
                                            } into GCS
                                            select new OpenCloseAuditDetailsClass()
                                            {
                                                CustomerID = GCS.Key.CustomerID,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                Branch = GCS.Key.Branch,
                                                ForMonth = GCS.Key.ForMonth,
                                                RoleID = GCS.Key.RoleID,
                                                UserID = GCS.Key.UserID,
                                                Role = GCS.Key.Role,
                                                User = GCS.Key.User,
                                                FinancialYear = GCS.Key.FinancialYear,
                                                ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                                ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                                VerticalId = GCS.Key.VerticalsId,
                                                VerticalName = GCS.Key.VerticalName,
                                                AuditID = GCS.Key.AuditID,
                                                //CProcessName = GCS.Key.CProcessName,
                                                //SubProcess = GCS.Key.SubProcess,
                                            }).ToList();

                        //transactionquery = (from C in Masterrecord
                        //          where C.CustomerID == customerid
                        //          && BranchAssigned.Contains(C.CustomerBranchID)
                        //          && C.ACCStatus == null
                        //          group C by new
                        //          {
                        //              C.CustomerID,
                        //              C.CustomerBranchID,
                        //              C.Branch,
                        //              C.ForMonth,
                        //              C.RoleID,
                        //              C.UserID,
                        //              C.Role,
                        //              C.User,
                        //              C.FinancialYear,
                        //              C.ExpectedStartDate,
                        //              C.ExpectedEndDate,
                        //              C.VerticalsId,
                        //              C.VerticalName
                        //          } into GCS
                        //          select new OpenCloseAuditDetailsClass()
                        //          {
                        //              CustomerID = GCS.Key.CustomerID,
                        //              CustomerBranchID = GCS.Key.CustomerBranchID,
                        //              Branch = GCS.Key.Branch,
                        //              ForMonth = GCS.Key.ForMonth,
                        //              RoleID = GCS.Key.RoleID,
                        //              UserID = GCS.Key.UserID,
                        //              Role = GCS.Key.Role,
                        //              User = GCS.Key.User,
                        //              FinancialYear = GCS.Key.FinancialYear,
                        //              ExpectedStartDate = GCS.Key.ExpectedStartDate,
                        //              ExpectedEndDate = GCS.Key.ExpectedEndDate,
                        //              VerticalId = GCS.Key.VerticalsId,
                        //              VerticalName = GCS.Key.VerticalName,
                        //          }).ToList();


                    }
                    else if (Statusid == 1)
                    {
                        transactionquery = (from C in masterquery
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.ProcessId equals (int)EAAR.ProcessId
                                            where EAAR.ProcessId == ICAA.ProcessId
                                            && EAAR.UserID == userID
                                            && C.RoleID == 4
                                            && EAAR.ISACTIVE == true
                                            && C.ACCStatus == 1
                                            group C by new
                                            {
                                                C.CustomerID,
                                                C.CustomerBranchID,
                                                C.Branch,
                                                C.ForMonth,
                                                C.RoleID,
                                                C.UserID,
                                                C.Role,
                                                C.User,
                                                C.FinancialYear,
                                                C.ExpectedStartDate,
                                                C.ExpectedEndDate,
                                                C.VerticalsId,
                                                C.VerticalName,
                                                C.AuditID,
                                                //C.CProcessName,
                                                //C.SubProcess,
                                            } into GCS
                                            select new OpenCloseAuditDetailsClass()
                                            {
                                                CustomerID = GCS.Key.CustomerID,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                Branch = GCS.Key.Branch,
                                                ForMonth = GCS.Key.ForMonth,
                                                RoleID = GCS.Key.RoleID,
                                                UserID = GCS.Key.UserID,
                                                Role = GCS.Key.Role,
                                                User = GCS.Key.User,
                                                FinancialYear = GCS.Key.FinancialYear,
                                                ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                                ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                                VerticalId = GCS.Key.VerticalsId,
                                                VerticalName = GCS.Key.VerticalName,
                                                AuditID = GCS.Key.AuditID,
                                                //CProcessName = GCS.Key.CProcessName,
                                                //SubProcess = GCS.Key.SubProcess,
                                            }).ToList();

                        //transactionquery = (from C in Masterrecord
                        //          where C.CustomerID == customerid
                        //          && BranchAssigned.Contains(C.CustomerBranchID)
                        //          && C.ACCStatus == 1
                        //          group C by new
                        //          {
                        //              C.CustomerID,
                        //              C.CustomerBranchID,
                        //              C.Branch,
                        //              C.ForMonth,
                        //              C.RoleID,
                        //              C.UserID,
                        //              C.Role,
                        //              C.User,
                        //              C.FinancialYear,
                        //              C.ExpectedStartDate,
                        //              C.ExpectedEndDate,
                        //              C.VerticalsId,
                        //              C.VerticalName
                        //          } into GCS
                        //          select new OpenCloseAuditDetailsClass()
                        //          {
                        //              CustomerID = GCS.Key.CustomerID,
                        //              CustomerBranchID = GCS.Key.CustomerBranchID,
                        //              Branch = GCS.Key.Branch,
                        //              ForMonth = GCS.Key.ForMonth,
                        //              RoleID = GCS.Key.RoleID,
                        //              UserID = GCS.Key.UserID,
                        //              Role = GCS.Key.Role,
                        //              User = GCS.Key.User,
                        //              FinancialYear = GCS.Key.FinancialYear,
                        //              ExpectedStartDate = GCS.Key.ExpectedStartDate,
                        //              ExpectedEndDate = GCS.Key.ExpectedEndDate,
                        //              VerticalId = GCS.Key.VerticalsId,
                        //              VerticalName = GCS.Key.VerticalName,
                        //          }).ToList();
                    }
                    #endregion
                }
                else if (departmentheadFlag == "DH")
                {
                    #region Department Head

                    //List<long> branchids = AssignEntityManagementRisk.CheckDepartMentHeadLocation(userID);
                    //List<int?> BranchAssigned = branchids.Select(x => (int?) x).ToList();

                    if (Statusid == 2)
                    {

                        transactionquery = (from C in masterquery
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAR in entities.EntitiesAssignmentDepartmentHeads
                                            on C.CustomerBranchID equals (int)EAAR.BranchID
                                            join MSP in entities.Mst_Process
                                            on ICAA.ProcessId equals MSP.Id
                                            where EAAR.DepartmentID == MSP.DepartmentID
                                            && EAAR.UserID == userID
                                            && C.RoleID == 4
                                            && EAAR.ISACTIVE == true
                                            && C.ACCStatus == null
                                            group C by new
                                            {
                                                C.CustomerID,
                                                C.CustomerBranchID,
                                                C.Branch,
                                                C.ForMonth,
                                                C.RoleID,
                                                C.UserID,
                                                C.Role,
                                                C.User,
                                                C.FinancialYear,
                                                C.ExpectedStartDate,
                                                C.ExpectedEndDate,
                                                C.VerticalsId,
                                                C.VerticalName,
                                                C.AuditID,
                                                //C.CProcessName,
                                                //C.SubProcess,
                                            } into GCS
                                            select new OpenCloseAuditDetailsClass()
                                            {
                                                CustomerID = GCS.Key.CustomerID,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                Branch = GCS.Key.Branch,
                                                ForMonth = GCS.Key.ForMonth,
                                                RoleID = GCS.Key.RoleID,
                                                UserID = GCS.Key.UserID,
                                                Role = GCS.Key.Role,
                                                User = GCS.Key.User,
                                                FinancialYear = GCS.Key.FinancialYear,
                                                ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                                ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                                VerticalId = GCS.Key.VerticalsId,
                                                VerticalName = GCS.Key.VerticalName,
                                                AuditID = GCS.Key.AuditID,
                                                //CProcessName = GCS.Key.CProcessName,
                                                //SubProcess = GCS.Key.SubProcess,
                                            }).ToList();


                        //transactionquery = (from C in Masterrecord
                        //          where C.CustomerID == customerid
                        //          && BranchAssigned.Contains(C.CustomerBranchID)
                        //          && C.ACCStatus == null
                        //          group C by new
                        //          {
                        //              C.CustomerID,
                        //              C.CustomerBranchID,
                        //              C.Branch,
                        //              C.ForMonth,
                        //              C.RoleID,
                        //              C.UserID,
                        //              C.Role,
                        //              C.User,
                        //              C.FinancialYear,
                        //              C.ExpectedStartDate,
                        //              C.ExpectedEndDate,
                        //              C.VerticalsId,
                        //              C.VerticalName
                        //          } into GCS
                        //          select new OpenCloseAuditDetailsClass()
                        //          {
                        //              CustomerID = GCS.Key.CustomerID,
                        //              CustomerBranchID = GCS.Key.CustomerBranchID,
                        //              Branch = GCS.Key.Branch,
                        //              ForMonth = GCS.Key.ForMonth,
                        //              RoleID = GCS.Key.RoleID,
                        //              UserID = GCS.Key.UserID,
                        //              Role = GCS.Key.Role,
                        //              User = GCS.Key.User,
                        //              FinancialYear = GCS.Key.FinancialYear,
                        //              ExpectedStartDate = GCS.Key.ExpectedStartDate,
                        //              ExpectedEndDate = GCS.Key.ExpectedEndDate,
                        //              VerticalId = GCS.Key.VerticalsId,
                        //              VerticalName = GCS.Key.VerticalName,
                        //          }).ToList();


                    }
                    else if (Statusid == 1)
                    {
                        transactionquery = (from C in masterquery
                                            join ICAA in entities.InternalControlAuditAssignments
                                            on C.AuditID equals ICAA.AuditID
                                            join EAAR in entities.EntitiesAssignmentDepartmentHeads
                                            on C.CustomerBranchID equals (int)EAAR.BranchID
                                            join MSP in entities.Mst_Process
                                            on ICAA.ProcessId equals MSP.Id
                                            where EAAR.DepartmentID == MSP.DepartmentID
                                            && EAAR.UserID == userID
                                            && C.RoleID == 4
                                            && EAAR.ISACTIVE == true
                                            && C.ACCStatus == 1
                                            group C by new
                                            {

                                                C.CustomerID,
                                                C.CustomerBranchID,
                                                C.Branch,
                                                C.ForMonth,
                                                C.RoleID,
                                                C.UserID,
                                                C.Role,
                                                C.User,
                                                C.FinancialYear,
                                                C.ExpectedStartDate,
                                                C.ExpectedEndDate,
                                                C.VerticalsId,
                                                C.VerticalName,
                                                C.AuditID,
                                                //C.CProcessName,
                                                //C.SubProcess,
                                            } into GCS
                                            select new OpenCloseAuditDetailsClass()
                                            {
                                                CustomerID = GCS.Key.CustomerID,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                Branch = GCS.Key.Branch,
                                                ForMonth = GCS.Key.ForMonth,
                                                RoleID = GCS.Key.RoleID,
                                                UserID = GCS.Key.UserID,
                                                Role = GCS.Key.Role,
                                                User = GCS.Key.User,
                                                FinancialYear = GCS.Key.FinancialYear,
                                                ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                                ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                                VerticalId = GCS.Key.VerticalsId,
                                                VerticalName = GCS.Key.VerticalName,
                                                AuditID = GCS.Key.AuditID,
                                                //CProcessName = GCS.Key.CProcessName,
                                                //SubProcess = GCS.Key.SubProcess,
                                            }).ToList();

                        //transactionquery = (from C in Masterrecord
                        //          where C.CustomerID == customerid
                        //          && BranchAssigned.Contains(C.CustomerBranchID)
                        //          && C.ACCStatus == 1
                        //          group C by new
                        //          {
                        //              C.CustomerID,
                        //              C.CustomerBranchID,
                        //              C.Branch,
                        //              C.ForMonth,
                        //              C.RoleID,
                        //              C.UserID,
                        //              C.Role,
                        //              C.User,
                        //              C.FinancialYear,
                        //              C.ExpectedStartDate,
                        //              C.ExpectedEndDate,
                        //              C.VerticalsId,
                        //              C.VerticalName
                        //          } into GCS
                        //          select new OpenCloseAuditDetailsClass()
                        //          {
                        //              CustomerID = GCS.Key.CustomerID,
                        //              CustomerBranchID = GCS.Key.CustomerBranchID,
                        //              Branch = GCS.Key.Branch,
                        //              ForMonth = GCS.Key.ForMonth,
                        //              RoleID = GCS.Key.RoleID,
                        //              UserID = GCS.Key.UserID,
                        //              Role = GCS.Key.Role,
                        //              User = GCS.Key.User,
                        //              FinancialYear = GCS.Key.FinancialYear,
                        //              ExpectedStartDate = GCS.Key.ExpectedStartDate,
                        //              ExpectedEndDate = GCS.Key.ExpectedEndDate,
                        //              VerticalId = GCS.Key.VerticalsId,
                        //              VerticalName = GCS.Key.VerticalName,
                        //          }).ToList();
                    }
                    #endregion
                }
                else
                {
                    #region Performer/Reviewer


                    if (Statusid == 2)
                    {
                        transactionquery = (from C in masterquery
                                            where C.UserID == userID
                                  && C.ACCStatus == null
                                            group C by new
                                            {
                                                C.CustomerID,
                                                C.CustomerBranchID,
                                                C.Branch,
                                                C.ForMonth,
                                                C.RoleID,
                                                C.UserID,
                                                C.Role,
                                                C.User,
                                                C.FinancialYear,
                                                C.ExpectedStartDate,
                                                C.ExpectedEndDate,
                                                C.VerticalsId,
                                                C.VerticalName,
                                                C.AuditID,
                                                //C.CProcessName,
                                                //C.SubProcess,
                                            } into GCS
                                            select new OpenCloseAuditDetailsClass()
                                            {

                                                CustomerID = GCS.Key.CustomerID,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                Branch = GCS.Key.Branch,
                                                ForMonth = GCS.Key.ForMonth,
                                                RoleID = GCS.Key.RoleID,
                                                UserID = GCS.Key.UserID,
                                                Role = GCS.Key.Role,
                                                User = GCS.Key.User,
                                                FinancialYear = GCS.Key.FinancialYear,
                                                ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                                ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                                VerticalId = GCS.Key.VerticalsId,
                                                VerticalName = GCS.Key.VerticalName,
                                                AuditID = GCS.Key.AuditID,
                                                //CProcessName = GCS.Key.CProcessName,
                                                //SubProcess = GCS.Key.SubProcess,
                                            }).ToList();
                    }
                    else if (Statusid == 1)
                    {
                        transactionquery = (from C in masterquery
                                            where C.UserID == userID
                                   && C.ACCStatus == 1
                                            group C by new
                                            {
                                                C.CustomerID,
                                                C.CustomerBranchID,
                                                C.Branch,
                                                C.ForMonth,
                                                C.RoleID,
                                                C.UserID,
                                                C.Role,
                                                C.User,
                                                C.FinancialYear,
                                                C.ExpectedStartDate,
                                                C.ExpectedEndDate,
                                                C.VerticalsId,
                                                C.VerticalName,
                                                C.AuditID,
                                                //C.CProcessName,
                                                //C.SubProcess,
                                            } into GCS
                                            select new OpenCloseAuditDetailsClass()
                                            {
                                                CustomerID = GCS.Key.CustomerID,
                                                CustomerBranchID = GCS.Key.CustomerBranchID,
                                                Branch = GCS.Key.Branch,
                                                ForMonth = GCS.Key.ForMonth,
                                                RoleID = GCS.Key.RoleID,
                                                UserID = GCS.Key.UserID,
                                                Role = GCS.Key.Role,
                                                User = GCS.Key.User,
                                                FinancialYear = GCS.Key.FinancialYear,
                                                ExpectedStartDate = GCS.Key.ExpectedStartDate,
                                                ExpectedEndDate = GCS.Key.ExpectedEndDate,
                                                VerticalId = GCS.Key.VerticalsId,
                                                VerticalName = GCS.Key.VerticalName,
                                                AuditID = GCS.Key.AuditID,
                                                //CProcessName = GCS.Key.CProcessName,
                                                //SubProcess = GCS.Key.SubProcess,
                                            }).ToList();
                    }
                    #endregion
                }

                if (transactionquery.Count > 0)
                    transactionquery = transactionquery.GroupBy(entity => entity.AuditID).Select(entity => entity.FirstOrDefault()).ToList();

                if (customerid != -1)
                    transactionquery = transactionquery.Where(Entry => Entry.CustomerID == customerid).ToList();
                if (BranchList.Count > 0)
                    transactionquery = transactionquery.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();
                if (VerticalID != -1)
                    transactionquery = transactionquery.Where(Entry => Entry.VerticalId == VerticalID).ToList();
                if (FinancialYear != "")
                    transactionquery = transactionquery.Where(Entry => Entry.FinancialYear == FinancialYear).ToList();
                if (Period != "")
                    transactionquery = transactionquery.Where(Entry => Entry.ForMonth == Period).ToList();
                //if (RoleID != -1)
                //    transactionquery = transactionquery.Where(Entry => Entry.RoleID == RoleID).ToList();

                if (transactionquery.Count > 0)
                    transactionquery = transactionquery.OrderBy(Entry => Entry.Branch).ThenBy(entry => entry.VerticalName).ThenBy(entry => entry.FinancialYear).ToList();

                return transactionquery;
            }
        }

        public static long CheckInstanceIDExist(AuditImplementationInstance riskinstanceIMP)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.AuditImplementationInstances
                                         where row.AuditID == riskinstanceIMP.AuditID
                                         && row.ProcessId == riskinstanceIMP.ProcessId
                                         select row).FirstOrDefault();
                if (transactionsQuery != null)
                {
                    return transactionsQuery.Id;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static List<long> CheckAuditManagerLocation(int userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.EntitiesAssignmentAuditManagerRisks
                                         where row.UserID == userid
                                         select row.BranchID).Distinct().ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<AuditScheduling_SubProcessMapping> GetProcessListAuditScheduled(Int64 CustomerBranchid, int verticalID, string finYear, string period, int auditid, int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from IAS in entities.InternalAuditSchedulings
                                   join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                   on IAS.Process equals EAAMR.ProcessId
                                   join row in entities.AuditScheduling_SubProcessMapping
                                   on IAS.Id equals row.ScheduleOnID
                                   where IAS.CustomerBranchId == EAAMR.BranchID
                                   && EAAMR.UserID == Userid
                                   && IAS.CustomerBranchId == CustomerBranchid
                                   && IAS.VerticalID == verticalID
                                   && IAS.FinancialYear == finYear
                                   && IAS.TermName == period &&
                                   IAS.AuditID == auditid
                                   && EAAMR.ISACTIVE == true
                                   && row.IsDeleted == false
                                   select row).Distinct().ToList();

                // Added by rahul on 7 OCT 2017
                //var ProcessList = (from IAS in entities.Sp_GetScheduledProcessList(CustomerBranchid, verticalID, finYear, period)
                //                   select (long) IAS).Distinct().ToList();


                return ProcessList;
            }
        }
        public static List<long> GetProcessListImplementationAudit(Int64 CustomerBranchid, int verticalID, string finYear, string period)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.AuditKickOffImplementationFillViews
                                   where row.CustomerBranchId == CustomerBranchid
                                   && row.VerticalId == verticalID
                                   && row.FinancialYear == finYear
                                   && row.ForPeriod == period
                                   select row.ProcessId).Distinct().ToList();
                return ProcessList;
            }
        }
        public static void deleteMstUser(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                try
                {
                    var tet = entities.mst_User.Where(x => x.ID == Userid).FirstOrDefault();
                    entities.mst_User.Remove(tet);
                    entities.SaveChanges();
                    Userid--;
                    entities.SP_ResetIDMstUser(Userid);
                }
                catch (Exception ex)
                {
                }
            }
        }
        public static List<ReviewHistory> GetReveiwFilesData(Int64 CustomerBranchId, int UserID, int RiskCreationId, string FinancialYear, string ForMonth, int ScheduledOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.ReviewHistories
                             where row.CustomerBranchId == CustomerBranchId
                             && row.RiskCreationId == RiskCreationId
                             && row.FinancialYear == FinancialYear
                             && row.ForMonth == ForMonth
                             && row.AuditScheduleOnID == ScheduledOnID
                             select row).ToList();

                return query.ToList();
            }
        }
        //public static List<MyDocumentICFRView> GetICFROrignalFilesData(int CustomerBranchId,int UserID ,int RoleID,int RiskCreationId,string FinancialYear ,string ForMonth)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var query = (from row in entities.MyDocumentICFRViews
        //        where row.CustomerBranchId == CustomerBranchId && row.UserID == UserID && row.RoleID == RoleID
        //        && row.RiskCreationId == RiskCreationId
        //        && row.FinancialYear == FinancialYear && row.ForMonth == ForMonth
        //        select row).ToList();

        //        return query.ToList();
        //    }
        //}       
        public static List<RegionView> GetAllRegionData(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.RegionViews
                                         where row.CustomerID == CustomerID
                                         select row).ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<StateView> GetAllStateData(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.StateViews
                                         where row.CustomerID == CustomerID
                                         select row).ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<HubView> GetAllHubData(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.HubViews
                                         where row.CustomerID == CustomerID
                                         select row).ToList();
                return transactionsQuery.ToList();
            }
        }
        public static List<BranchView> GetAllBranchData(long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.BranchViews
                                         where row.CustomerID == CustomerID
                                         select row).ToList();
                return transactionsQuery.ToList();
            }
        }
        public static void updateActionPlanAssignment(ActionPlanAssignment updatePlanAssignment)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                ActionPlanAssignment upData = (from row in entities.ActionPlanAssignments
                                               where row.CustomerBranchId == updatePlanAssignment.CustomerBranchId && row.RiskCreationId == updatePlanAssignment.RiskCreationId &&
                                               row.AuditScheduleOnID == updatePlanAssignment.AuditScheduleOnID
                                               select row).FirstOrDefault();

                upData.FinancialYear = updatePlanAssignment.FinancialYear;
                upData.TOE = updatePlanAssignment.TOE;
                upData.TOD = updatePlanAssignment.TOD;
                upData.PersonResponsible = updatePlanAssignment.PersonResponsible;
                upData.ActionPlan = updatePlanAssignment.ActionPlan;
                upData.TimeLine = updatePlanAssignment.TimeLine;
                entities.SaveChanges();

            }
        }
        public static ActionPlanAssignment Get_PlanAssignment(Int64 CustomerBranchID, int RiskCreationID, int AuditScheduleOnId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fetchData = (from row in entities.ActionPlanAssignments
                                 where row.CustomerBranchId == CustomerBranchID && row.RiskCreationId == RiskCreationID && row.AuditScheduleOnID == AuditScheduleOnId
                                 select row).FirstOrDefault();
                return fetchData;
            }
        }
        public static bool PersonResponsibleExists(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.ActionPlanAssignments
                             where row.PersonResponsible == Userid
                             select row.PersonResponsible).Distinct();
                return query.Select(entry => true).SingleOrDefault();

            }
        }
        public static void UpdateDocumentExamine(RiskActivityTransaction obj)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var list = (from row in entities.RiskActivityTransactions
                            where row.RiskCreationId == obj.RiskCreationId
                            select row).ToList();

                foreach (var item in list)
                {
                    RiskActivityTransaction updObj = (from row in entities.RiskActivityTransactions
                                                      where row.RiskCreationId == obj.RiskCreationId &&
                                                            row.Id == item.Id
                                                      select row).FirstOrDefault();

                    updObj.DocumentsExamined = obj.DocumentsExamined;
                    entities.SaveChanges();
                }
            }
        }
        public static List<long> GetAuditInstanceID(int branchid, string FinancialYear, string ForMonth, int RoleId, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<long> AuditInstanceIDs = new List<long>();

                var Records = (from row in entities.AuditInstanceTransactionViews
                               where row.CustomerBranchID == branchid
                               && row.FinancialYear == FinancialYear
                               && row.ForMonth == ForMonth
                               && row.RoleID == RoleId
                               && row.UserID == UserID
                               select row.AuditInstanceID).Distinct().ToList();

                return AuditInstanceIDs = Records.ToList();
            }
        }
        public static List<mst_Vertical> FillVerticalList(int customerID, int? branchId, int? VerticalID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.mst_Vertical
                               join row1 in entities.BranchVerticals
                               on row.CustomerID equals row1.CustomerID
                               where (row1.CustomerID == customerID) && row1.Branch == branchId && row.ID == VerticalID && row1.IsActive == true
                               select row).Distinct();
                return objcust.ToList();
            }
        }
        public static List<mst_Vertical> FillVerticalList(int customerID, int? branchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.mst_Vertical
                               join row1 in entities.BranchVerticals
                               on row.CustomerID equals row1.CustomerID
                               where (row1.CustomerID == customerID) && row1.Branch == branchId && row1.IsActive == true
                               select row).Distinct();
                return objcust.ToList();
            }
        }
        public static List<RiskActivityTransactionForddl> FillVerticalListFromRiskActTrasa(int? branchId, int? VerticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var objcust = (from row in entities.RiskActivityTransactions
                               join row3 in entities.Internal_AuditAssignment
                               on row.CustomerBranchId equals row3.CustomerBranchid
                               join row2 in entities.mst_Vertical
                               on row.VerticalsId equals row2.ID
                               where row.VerticalsId == row3.VerticalID
                               && row.CustomerBranchId == branchId
                               && row3.VerticalID == VerticalId
                               select new RiskActivityTransactionForddl()
                               {
                                   VerticalsId = row.VerticalsId,
                                   VerticalName = row2.VerticalName,
                               }).Distinct().ToList();

                return objcust;

            }
        }
        public static List<RiskActivityTransactionForddl> FillVerticalListFromRiskActTrasa()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var objcust = (from row in entities.RiskActivityTransactions
                               join row3 in entities.Internal_AuditAssignment
                               on row.CustomerBranchId equals row3.CustomerBranchid
                               join row2 in entities.mst_Vertical
                               on row.VerticalsId equals row2.ID
                               where row.VerticalsId == row3.VerticalID
                              // && row2.CustomerID == CustomerID
                               select new RiskActivityTransactionForddl()
                               {
                                   VerticalsId = row.VerticalsId,
                                   VerticalName = row2.VerticalName,
                               }).Distinct().ToList();

                return objcust;

            }
        }

        public static List<RiskActivityTransactionForddl> FillVerticalListFromRiskActTrasaBYCustomer(int? customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var objcust = (from row in entities.RiskActivityTransactions
                               join row3 in entities.Internal_AuditAssignment
                               on row.CustomerBranchId equals row3.CustomerBranchid
                               join row2 in entities.mst_Vertical
                               on row.VerticalsId equals row2.ID
                               where row.VerticalsId == row3.VerticalID
                                && row2.CustomerID == customerID
                               select new RiskActivityTransactionForddl()
                               {
                                   VerticalsId = row.VerticalsId,
                                   VerticalName = row2.VerticalName,
                               }).Distinct().ToList();

                return objcust;

            }
        }
        public static List<RiskActivityTransactionForddl> FillVerticalListFromRiskActTrasa(int? branchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var objcust = (from row in entities.RiskActivityTransactions
                               join row3 in entities.Internal_AuditAssignment
                               on row.CustomerBranchId equals row3.CustomerBranchid
                               join row2 in entities.mst_Vertical
                               on row.VerticalsId equals row2.ID
                               where row.VerticalsId == row3.VerticalID && row.CustomerBranchId == branchId
                               select new RiskActivityTransactionForddl()
                               {
                                   VerticalsId = row.VerticalsId,
                                   VerticalName = row2.VerticalName,
                               }).Distinct().ToList();

                return objcust;

            }
        }
        public static ICFRAUDITSTEPCLOSE GetScheduledOnID(Int64 CustomerBranchid, string finYear, string period, long riskCID, int userid, int roleid, int Processid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from AA in entities.AuditAssignments
                             join ASO in entities.AuditScheduleOns
                             on AA.RiskCreationId equals ASO.RiskCreationID
                             join AT in entities.AuditTransactions
                             on ASO.ID equals AT.AuditScheduleOnID
                             where AA.CustomerBranchID == CustomerBranchid
                             && ASO.FinancialYear == finYear
                             && AA.UserID == userid
                             && AA.RoleID == roleid
                             && AA.RiskCreationId == riskCID
                             && AA.ProcessId == Processid
                             && ASO.ForMonth == period
                             select new ICFRAUDITSTEPCLOSE()
                             {
                                 ScheduledOnID = ASO.ID,
                                 RiskCreationID = ASO.RiskCreationID,
                                 ForMonth = ASO.ForMonth,
                                 FinancialYear = ASO.FinancialYear,
                                 StatusId = AT.StatusId,
                             }).OrderByDescending(x => x.StatusId).Take(1);
                return query.FirstOrDefault();
            }
        }
        public static List<Internal_AuditAssignment> GetPopUpAudAssignmentData(int branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.Internal_AuditAssignment
                               where row.CustomerBranchid == branchid && row.IsActive == false
                               select row).ToList();
                return objcust.ToList();
            }
        }
        public static void UpdateInternalControlAuditAssignment(DateTime StartDate, DateTime EndDate, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RecordToUpdate = (from row in entities.InternalControlAuditAssignments
                                      where row.AuditID == AuditID
                                      select row).ToList();

                RecordToUpdate.ForEach(entry =>
                {
                    entry.ExpectedStartDate = StartDate;
                    entry.ExpectedEndDate = EndDate;
                });

                entities.SaveChanges();
            }
        }
        public static List<InternalAuditScheduling> GetDataFromPopUpSave(int branchid, int? VerticalId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.InternalAuditSchedulings
                               where row.CustomerBranchId == branchid && row.VerticalID == VerticalId && row.IsDeleted == false
                               select row).ToList();
                return objcust.ToList();
            }
        }
        public static List<mst_Industry> GetAllIndustry()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var IndustryList = (from row in entities.mst_Industry
                                    where row.Name != null
                                    select row);
                return IndustryList.ToList();
            }
        }
        public static bool IndustyExist(string name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Industry
                             where row.Name.Equals(name)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void CreateIndustry(mst_Industry objIndustry)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Industry.Add(objIndustry);
                entities.SaveChanges();
            }
        }
        public static void UpdateIndustry(mst_Industry objIndustry)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Industry updatestates = (from row in entities.mst_Industry
                                             where row.ID == objIndustry.ID
                                             select row).FirstOrDefault();

                updatestates.Name = objIndustry.Name;
                entities.SaveChanges();
            }
        }
        public static mst_Industry IndustryGetID(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var StateMaster = (from row in entities.mst_Industry
                                   where row.ID == iD
                                   select row).SingleOrDefault();
                return StateMaster;
            }
        }
        public static void DeleteIndustry(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Industry StateDalete = (from row in entities.mst_Industry
                                            where row.ID == iD
                                            select row).FirstOrDefault();

                entities.mst_Industry.Remove(StateDalete);
                entities.SaveChanges();
            }
        }
        public static List<mst_LegalEntityType> LegalentityAllData()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var LegalEntityList = (from row in entities.mst_LegalEntityType
                                       where row.EntityTypeName != null
                                       select row);
                return LegalEntityList.ToList();
            }
        }
        public static bool LegalEntityExist(string entityTypeName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_LegalEntityType
                             where row.EntityTypeName.Equals(entityTypeName)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void CreateLegalEntityType(mst_LegalEntityType objEntity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_LegalEntityType.Add(objEntity);
                entities.SaveChanges();
            }
        }
        public static void UpdateLegalEntityType(mst_LegalEntityType objEntity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_LegalEntityType objLegEntity = (from row in entities.mst_LegalEntityType
                                                    where row.ID == objEntity.ID
                                                    select row).FirstOrDefault();

                objLegEntity.EntityTypeName = objEntity.EntityTypeName;
                entities.SaveChanges();
            }
        }
        public static mst_LegalEntityType GetLegalEntitybyID(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ObjEntityType = (from row in entities.mst_LegalEntityType
                                     where row.ID == iD
                                     select row).SingleOrDefault();
                return ObjEntityType;
            }
        }
        public static void DeleteLegalEntity(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_LegalEntityType DeleteEntity = (from row in entities.mst_LegalEntityType
                                                    where row.ID == iD
                                                    select row).FirstOrDefault();

                entities.mst_LegalEntityType.Remove(DeleteEntity);
                entities.SaveChanges();
            }
        }
        public static List<mst_NodeType> GetSubEntityList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var SubEntityList = (from row in entities.mst_NodeType
                                     where row.Name != null && row.isDelete == false
                                     select row);
                return SubEntityList.ToList();
            }
        }
        public static bool SubEntityTypeExist(string subEntityType)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_NodeType
                             where row.Name.Equals(subEntityType) && row.isDelete == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void CreateSubEntity(mst_NodeType objSubEntity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_NodeType.Add(objSubEntity);
                entities.SaveChanges();
            }
        }
        public static void UpdateSubEntity(mst_NodeType objSubEntity)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_NodeType objLegEntity = (from row in entities.mst_NodeType
                                             where row.ID == objSubEntity.ID
                                             select row).FirstOrDefault();

                objLegEntity.Name = objSubEntity.Name;
                entities.SaveChanges();
            }
        }
        public static mst_NodeType GetSubEntityTypeByID(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ObjEntityType = (from row in entities.mst_NodeType
                                     where row.ID == iD
                                     select row).SingleOrDefault();
                return ObjEntityType;
            }
        }
        public static void DeleteSubEntity(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_NodeType userToDelete = (from row in entities.mst_NodeType
                                             where row.ID == iD
                                             select row).FirstOrDefault();
                userToDelete.isDelete = true;
                entities.SaveChanges();
            }
        }
        public static List<mst_Vertical> GetAllVertical(long Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<mst_Vertical> ObjVerlist = new List<mst_Vertical>();
                if (Customerid != -1)
                {
                    ObjVerlist = (from row in entities.mst_Vertical
                                  where row.VerticalName != null && row.CustomerID == Customerid
                                  select row).ToList();
                }
                else
                {
                    ObjVerlist = (from row in entities.mst_Vertical
                                  where row.VerticalName != null
                                  select row).ToList();
                }

                return ObjVerlist.ToList();
            }
        }
        public static bool VerticalNameExist(mst_Vertical objVerti)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Vertical
                             where row.VerticalName.ToLower() == objVerti.VerticalName.ToLower() && row.CustomerID == objVerti.CustomerID
                             select objVerti.CustomerID);

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void CreateVerticalName(mst_Vertical objVerti)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Vertical.Add(objVerti);
                entities.SaveChanges();
            }
        }
        public static void UpdateVerticalName(mst_Vertical objVerti)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Vertical objQuery = (from row in entities.mst_Vertical
                                         where row.ID == objVerti.ID
                                         select row).FirstOrDefault();

                objQuery.VerticalName = objVerti.VerticalName;
                objQuery.CustomerID = objVerti.CustomerID;
                entities.SaveChanges();
            }
        }
        public static mst_Vertical GetVerticalListByID(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objTypes = (from row in entities.mst_Vertical
                                where row.ID == iD
                                select row).SingleOrDefault();
                return objTypes;
            }
        }
        public static void DeleteVertical(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Vertical objTypes = (from row in entities.mst_Vertical
                                         where row.ID == iD
                                         select row).FirstOrDefault();

                entities.mst_Vertical.Remove(objTypes);
                entities.SaveChanges();
            }
        }
        public static void UpdateUserPhoto(int ID, String FilePath, String FileName)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_User userToUpdate = (from Row in entities.mst_User
                                         where Row.ID == ID
                                         select Row).FirstOrDefault();

                userToUpdate.ImagePath = FilePath;
                userToUpdate.ImageName = FileName;
                entities.SaveChanges();
            }
        }
        public static bool Delete(long userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<SecurityQuestionAnswarRisk> securityQuestionAns = (from row in entities.SecurityQuestionAnswarRisks
                                                                        where row.UserID == userID
                                                                        select row).ToList();

                securityQuestionAns.ForEach(entry =>
                    entities.SecurityQuestionAnswarRisks.Remove(entry)
                );
                entities.SaveChanges();

                return true;
            }
        }
        public static bool HasAuditsAssigned(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var assignedCount = (from row in entities.AuditAssignments
                                     where row.UserID == userID
                                     select 1).Count();

                return assignedCount > 0;
            }
        }
        public static bool HasInternalControlAuditsAssigned(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var assignedCount = (from row in entities.InternalControlAuditAssignments
                                     where row.UserID == userID
                                     select 1).Count();

                return assignedCount > 0;
            }
        }
        public static mst_Customer GetCustomer(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Customer = (from row in entities.mst_User
                                join row1 in entities.mst_Customer
                                on row.CustomerID equals row1.ID
                                where row.ID == userID
                                select row1).SingleOrDefault();

                return Customer;
            }
        }
        public static bool Update(mst_User user, List<UserParameterValue_Risk> parameters)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User userToUpdate = (from row in entities.mst_User
                                         where row.ID == user.ID
                                         select row).FirstOrDefault();

                userToUpdate.IsHead = user.IsHead;
                userToUpdate.FirstName = user.FirstName;
                userToUpdate.LastName = user.LastName;
                userToUpdate.Designation = user.Designation;
                userToUpdate.Email = user.Email;
                userToUpdate.ContactNumber = user.ContactNumber;
                userToUpdate.Address = user.Address;
                //userToUpdate.CustomerID = user.CustomerID;
                userToUpdate.CustomerBranchID = user.CustomerBranchID;
                userToUpdate.RoleID = user.RoleID;
                userToUpdate.DepartmentID = user.DepartmentID;
                userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;
                userToUpdate.AuditorID = user.AuditorID;
                userToUpdate.AuditEndPeriod = user.AuditEndPeriod;
                userToUpdate.AuditStartPeriod = user.AuditStartPeriod;
                userToUpdate.Startdate = user.Startdate;
                userToUpdate.Enddate = user.Enddate;
                userToUpdate.VendorRoleID = user.VendorRoleID;
                userToUpdate.IMEIChecked = user.IMEIChecked;
                userToUpdate.IMEINumber = user.IMEINumber;
                userToUpdate.DesktopRestrict = user.DesktopRestrict;
                userToUpdate.MobileAccess = user.MobileAccess;
                userToUpdate.HRRoleID = user.HRRoleID;
                userToUpdate.SecretarialRoleID = user.SecretarialRoleID;

                userToUpdate.SSOAccess = user.SSOAccess;
                userToUpdate.VendorAuditRoleID = user.VendorAuditRoleID;
                List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                var existingParameters = (from parameterRow in entities.UserParameterValue_Risk
                                          where parameterRow.UserId == user.ID
                                          select parameterRow).ToList();

                existingParameters.ForEach(entry =>
                {
                    if (parameterIDs.Contains(entry.ID))
                    {
                        UserParameterValue_Risk parameter = parameters.Find(param1 => param1.ID == entry.ID);
                        entry.Value = parameter.Value;
                    }
                    else
                    {
                        entities.UserParameterValue_Risk.Remove(entry);
                    }
                });

                //parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValue_Risk.Add(entry));

                entities.SaveChanges();
                return true;
            }
        }
        public static bool GetPreviousAuditScheduling(Int64 CustomerBranchid, int verticalID, string finYear, string period, int NoofPhases = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (!period.Contains("Phase"))
                {
                    var ProcessList = (from IAS in entities.InternalAuditSchedulings
                                       where IAS.CustomerBranchId == CustomerBranchid
                                       && IAS.VerticalID == verticalID
                                       && IAS.FinancialYear == finYear
                                       && IAS.TermName == period
                                       select IAS).ToList();
                    if (ProcessList.Count > 0)
                        return false;
                    else
                        return true;
                }
                else
                {
                    var ProcessList = (from IAS in entities.InternalAuditSchedulings
                                       where IAS.CustomerBranchId == CustomerBranchid
                                       && IAS.VerticalID == verticalID
                                       && IAS.FinancialYear == finYear
                                       && IAS.TermName == period
                                       && IAS.PhaseCount == NoofPhases
                                       select IAS).ToList();
                    if (ProcessList.Count > 0)
                        return false;
                    else
                        return true;
                }
            }
        }
        public static void AddDetailsInternalAuditSchedulingTableEditSave(List<InternalAuditScheduling> Tempassignments)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Tempassignments.ForEach(entry =>
                {

                    var AuditorMastertoDelete = (from row in entities.InternalAuditSchedulings
                                                 where row.FinancialYear == entry.FinancialYear && row.IsDeleted == true
                                                 && row.ISAHQMP == entry.ISAHQMP && row.CustomerBranchId == entry.CustomerBranchId
                                                 && row.TermName == entry.TermName && row.VerticalID == entry.VerticalID && row.Process == entry.Process
                                                 select row).FirstOrDefault();
                    if (AuditorMastertoDelete != null)
                    {
                        AuditorMastertoDelete.IsDeleted = false;
                        entities.SaveChanges();
                    }
                    else
                    {
                        entities.InternalAuditSchedulings.Add(entry);
                        entities.SaveChanges();

                    }
                });
            }

        }
        public static void AddDetailsInternalAuditSchedulingTable(List<InternalAuditScheduling> Tempassignments)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    entities.InternalAuditSchedulings.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static void AddDetailsTMP_ImplementationAssignment(List<TMP_ImplementationAssignment> tmpimplementationassignment)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                tmpimplementationassignment.ForEach(entry =>
                {
                    entities.TMP_ImplementationAssignment.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static void AddDetailsTMPAssignments(List<TMP_Assignment> TMPAssignments)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                TMPAssignments.ForEach(entry =>
                {
                    entities.TMP_Assignment.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static void AddDetailsAuditImplementationAssignment(List<AuditImplementationAssignment> auditimplementationassignment)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                auditimplementationassignment.ForEach(entry =>
                {
                    entities.AuditImplementationAssignments.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static void AddDetailsInternalControlAuditAssignment(List<InternalControlAuditAssignment> Internalcontrolauditassignment)
        {
            int saveCount = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Internalcontrolauditassignment.ForEach(entry =>
                {
                    entities.InternalControlAuditAssignments.Add(entry);
                    saveCount++;
                    if (saveCount > 100)
                    {
                        entities.SaveChanges();
                        saveCount = 0;
                    }
                });

                entities.SaveChanges();
            }

        }
        public static void AddDetailsTempAssignmentTable(List<AuditAssignment> Tempassignments)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    entities.AuditAssignments.Add(entry);
                    entities.SaveChanges();

                });
            }

        }

        #region  Implementation Transaction
        public static bool CreateAuditImplementationTransaction(AuditImplementationTransaction transaction)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    transaction.Dated = DateTime.Now;
                    entities.AuditImplementationTransactions.Add(transaction);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }

        }
        public static bool CreateAuditImplementationTransaction(AuditImplementationTransaction transaction, List<ImplementationFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.AuditImplementationTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (ImplementationFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.ImplementationFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    ImplementationFileDataMapping_Risk fileMapping = new ImplementationFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ImplementationScheduledOnID = transaction.ImplementationScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.ImplementationFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool UpdateAuditImplementationTransaction(AuditImplementationTransaction transaction, List<ImplementationFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);

                            AuditImplementationTransaction RecordtoUpdate = (from row in entities.AuditImplementationTransactions
                                                                             where row.FinancialYear == transaction.FinancialYear
                                                                                 && row.ForPeriod == transaction.ForPeriod && row.CustomerBranchId == transaction.CustomerBranchId
                                                                                 && row.UserID == transaction.UserID && row.RoleID == transaction.RoleID && row.ResultID == transaction.ResultID
                                                                             select row).FirstOrDefault();

                            RecordtoUpdate.PersonResponsible = transaction.PersonResponsible;
                            RecordtoUpdate.ForPeriod = transaction.ForPeriod;
                            RecordtoUpdate.Dated = DateTime.Now;
                            long transactionId = RecordtoUpdate.ID;
                            entities.SaveChanges();
                            if (files != null)
                            {
                                foreach (ImplementationFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.ImplementationFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    ImplementationFileDataMapping_Risk fileMapping = new ImplementationFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ImplementationScheduledOnID = transaction.ImplementationScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.ImplementationFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        public static bool UpdateTransactionNew(InternalAuditTransaction transaction, List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, long userid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);

                            InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                                       where row.ProcessId == transaction.ProcessId && row.FinancialYear == transaction.FinancialYear
                                                                           && row.ForPeriod == transaction.ForPeriod && row.CustomerBranchId == transaction.CustomerBranchId
                                                                           && row.UserID == transaction.UserID && row.RoleID == transaction.RoleID && row.ATBDId == transaction.ATBDId
                                                                       select row).FirstOrDefault();

                            RecordtoUpdate.PersonResponsible = transaction.PersonResponsible;
                            RecordtoUpdate.ObservatioRating = transaction.ObservatioRating;
                            RecordtoUpdate.ObservationCategory = transaction.ObservationCategory;
                            RecordtoUpdate.ForPeriod = transaction.ForPeriod;
                            RecordtoUpdate.Dated = DateTime.Now;
                            RecordtoUpdate.CreatedBy = userid;
                            RecordtoUpdate.StatusId = transaction.StatusId;
                            RecordtoUpdate.Remarks = transaction.Remarks;
                            long transactionId = RecordtoUpdate.ID;
                            entities.SaveChanges();
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {


                return false;
            }
        }
        public static bool UpdateTransaction(InternalAuditTransaction transaction, List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, long userid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);

                            InternalAuditTransaction RecordtoUpdate = (from row in entities.InternalAuditTransactions
                                                                       where row.ProcessId == transaction.ProcessId && row.FinancialYear == transaction.FinancialYear
                                                                           && row.ForPeriod == transaction.ForPeriod && row.CustomerBranchId == transaction.CustomerBranchId
                                                                           && row.UserID == transaction.UserID && row.RoleID == transaction.RoleID && row.ATBDId == transaction.ATBDId
                                                                       select row).FirstOrDefault();

                            RecordtoUpdate.PersonResponsible = transaction.PersonResponsible;
                            RecordtoUpdate.ObservatioRating = transaction.ObservatioRating;
                            RecordtoUpdate.ObservationCategory = transaction.ObservationCategory;
                            RecordtoUpdate.ForPeriod = transaction.ForPeriod;
                            RecordtoUpdate.Dated = DateTime.Now;
                            RecordtoUpdate.CreatedBy = userid;
                            long transactionId = RecordtoUpdate.ID;
                            entities.SaveChanges();
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {


                return false;
            }
        }
        public static bool CreateTransaction(InternalAuditTransaction transaction, List<InternalFileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Audit_SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.InternalAuditTransactions.Add(transaction);
                            entities.SaveChanges();

                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {


                return false;
            }
        }
        public static bool CreateTransaction(InternalAuditTransaction transaction)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    transaction.Dated = DateTime.Now;
                    entities.InternalAuditTransactions.Add(transaction);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }

        }

        #region IFC
        public static bool CreateTransaction(AuditTransaction transaction)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.AuditTransactions.Add(transaction);
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool CreateTransaction(AuditTransaction transaction, List<FileData_Risk> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.IFC_SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.AuditTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (FileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping_Risk fileMapping = new FileDataMapping_Risk();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMapping_Risk.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                //List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                //LogMessage msg = new LogMessage();
                //msg.ClassName = "ComplianceManagement";
                //msg.FunctionName = "CreateTransaction";
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                //ReminderUserList.Add(msg);
                //InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("rahulmane3192@gmail.com");
                //TO.Add("manishajalnekar07@gmail.com");

                ////List<string> CC = new List<string>();
                ////CC.Add("sandeep@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");



                return false;
            }
        }
        public static void CreateTransaction(List<AuditTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.AuditTransactions.Add(entry);
                });

                entities.SaveChanges();

            }
        }
        public static void CreateActionPlanAssignment(ActionPlanAssignment actionplanassignment)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.ActionPlanAssignments.Add(actionplanassignment);
                entities.SaveChanges();

            }
        }
        #endregion
        public static void AddDetailsAuditImplementationInstance(AuditImplementationInstance auditimplementationinstance)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.AuditImplementationInstances.Add(auditimplementationinstance);
                entities.SaveChanges();
            }

        }
        public static void AddDetailsInternalAuditInstances(InternalAuditInstance Internalauditinstance)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.InternalAuditInstances.Add(Internalauditinstance);
                entities.SaveChanges();
            }

        }
        public static void AddDetailsRiskInstance(AuditInstance auditinstance)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.AuditInstances.Add(auditinstance);
                entities.SaveChanges();
            }

        }
        public static object FillCustomerNew(long customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == customerid
                             select row).Distinct();
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillSchedulingType()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.Mst_AuditPeriodMaster
                             select row).Distinct();

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static List<GetSchedulingTypeAsAssign_Result> SP_GetSchedulingTypeAsAssignResultProcedure(int branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceReminders = entities.GetSchedulingTypeAsAssign(branchid).ToList();
                return complianceReminders;
            }
        }
        public static object FillSchedulingTypeRahul(int branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<GetSchedulingTypeAsAssign_Result> query = new List<GetSchedulingTypeAsAssign_Result>();

                query = SP_GetSchedulingTypeAsAssignResultProcedure(branchid).ToList();
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillFnancialYear()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.Mst_FinancialYear
                             select row).Distinct();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.FinancialYear }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillCustomerTransactionSubProcess(long Processid, long branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.RiskActivityTransactions
                             join row1 in entities.mst_Subprocess
                             on row.SubProcessId equals row1.Id
                             //join row2 in entities.CustomerBranchMappings
                             //on row.RiskCreationId equals row2.RiskCategoryCreationId
                             where row.CustomerBranchId == branchid && row.SubProcessId != -1 && row.ProcessId == Processid
                             select row1).Distinct();


                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static void InsertLogToDatabase(List<LogMessage_Risk> objEscalation)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessage_Risk.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static bool Create(List<SecurityQuestionAnswarRisk> securityQuestionAns)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                securityQuestionAns.ForEach(entry =>
                    entities.SecurityQuestionAnswarRisks.Add(entry)
                );
                entities.SaveChanges();

                return true;
            }
        }
        public static mst_User GetByID_OnlyEditOption(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var user = (from row in entities.mst_User
                            where row.ID == userID// && row.IsDeleted == false && row.IsActive == true
                            select row).SingleOrDefault();

                return user;
            }
        }
        public static string GetFrequencyFlag(Int64 CustomerBranchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.InternalAuditSchedulings
                                     where row.CustomerBranchId == CustomerBranchId
                                     select row.ISAHQMP).ToList().Distinct().FirstOrDefault();
                if (MstRiskResult != null)
                {
                    return MstRiskResult;
                }
                else
                {
                    return "";
                }
            }
        }
        public static int GetPhaseCount(Int64 CustomerBranchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.InternalAuditSchedulings
                                     where row.CustomerBranchId == CustomerBranchId
                                     select row.PhaseCount).ToList().Distinct().FirstOrDefault();
                if (MstRiskResult != null)
                {
                    return (int)MstRiskResult;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static object FillReportee(int CustomerID, Int64 CustomerBranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditInstanceTransactionViews
                             where row.AuditStatusID != 3 && row.RoleID == 3 && row.CustomerID == CustomerID && row.CustomerBranchID == CustomerBranchID
                             select new FillRepoteeData()
                             {
                                 UserID = row.UserID,
                                 UserName = row.User,
                             }).Distinct();

                var users = (from row in query
                             select new { ID = row.UserID, Name = row.UserName }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static List<object> GetAllNVP(int customerID = -1, List<long> ids = null, bool Flags = false)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.IsDeleted == false && row.IsActive == true
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                if (Flags == true)
                {
                    query = query.Where(entry => entry.RoleID != 8 || entry.RoleID != 12);
                }

                if (ids != null)
                {
                    query = query.Where(entry => ids.Contains(entry.ID));
                }

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                return users.ToList<object>();
            }
        }
        public static List<object> GetAllNonAdminUser(int customerID, int RiskCreationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = entities.GetPersonResponsibleByStoreProcedure(customerID, RiskCreationId).GroupBy(entry => entry.Name).Select(en => en.FirstOrDefault()).ToList();

                //var query = (from row in entities.GetPersonResponsibleByStoreProcedure(customerID, RiskCreationId)
                //             //where row.IsDeleted == false && row.IsActive == true
                //             //&& row.RoleID == 7
                //             select row);

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                return users.ToList<object>();
            }
        }
        public static bool CheckProductmapping(long customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var productid = GetByProductID(Convert.ToInt32(customerID));
                List<long?> parameterIDs = productid.Select(entry => entry.ProductID).ToList();
                if (parameterIDs.Contains(3))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckProductmapping(long customerID, int ProductId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var productid = GetByProductID(Convert.ToInt32(customerID));
                List<long?> parameterIDs = productid.Select(entry => entry.ProductID).ToList();
                if (parameterIDs.Contains(ProductId))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static List<ProductMapping_Risk> GetByProductID(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var productmapping = (from row in entities.ProductMapping_Risk
                                      where row.CustomerID == customerID && row.IsActive == false
                                      //  && row.ProductID == 2// hard code because check risk software applicable or not
                                      select row).ToList();

                return productmapping;
            }
        }
        public static bool Create(mst_User user, List<UserParameterValue_Risk> parameters, string SenderEmailAddress, string message, bool accessToDirector = true, bool isActive = true)
        {
            int Addid = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var customerStatus = CustomerManagementRisk.GetByID(Convert.ToInt32(user.CustomerID)).Status;

                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.UtcNow;
                        if (customerStatus != null && customerStatus == 1)
                        {
                            user.IsActive = true;
                        }
                        else
                        {
                            if (user.RoleID == 2)
                                user.IsActive = true;
                            else
                                user.IsActive = false;
                        }
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.UtcNow;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        //parameters.ForEach(entry => user.UserParameterValue_Risk.Add(entry));
                        entities.mst_User.Add(user);
                        entities.SaveChanges();
                        Addid = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();

                        if (accessToDirector)
                        {
                            //string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                            EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "AVACOM Account Created.", message);
                        }

                        return true;

                    }
                    catch (Exception ex)
                    {
                        dbtransaction.Rollback();
                        Addid--;
                        if (Addid > 0)
                        {
                            entities.SP_ResetIDMstUser(Addid);
                        }
                        return false;
                    }
                }
            }
        }
        public static void UpdateUserStatus(int CustomerId, bool status)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<mst_User> userToUpdate = (from Row in entities.mst_User
                                               where Row.CustomerID == CustomerId
                                               select Row).ToList();
                userToUpdate.ForEach(entry =>
                {
                    entry.IsActive = status;
                });
                entities.SaveChanges();
            }
        }
        public static void Delete(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_User userToDelete = (from row in entities.mst_User
                                         where row.ID == userID
                                         select row).FirstOrDefault();

                userToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }
        public static void Exists(mst_User user, out bool emailExists)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var emailQuery = (from row in entities.mst_User
                                  where row.IsDeleted == false
                                       && row.Email.Equals(user.Email)
                                  select row);

                if (user.ID > 0)
                {
                    emailQuery = emailQuery.Where(entry => entry.ID != user.ID);
                }

                emailExists = emailQuery.Select(entry => true).FirstOrDefault();

            }
        }

        public static void Update(mst_User user)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User userToUpdate = (from Row in entities.mst_User
                                         where Row.ID == user.ID
                                         select Row).FirstOrDefault();

                userToUpdate.LastLoginTime = user.LastLoginTime;
                userToUpdate.WrongAttempt = 0;
                entities.SaveChanges();
            }
        }

        public static bool ChangePassword(mst_User user)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        mst_User userToUpdate = new mst_User();

                        userToUpdate = (from entry in entities.mst_User
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();


                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = DateTime.UtcNow;
                        userToUpdate.EnType = "A";
                        userToUpdate.ChangePasswordFlag = true;
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }
        public static bool ChangePassword(mst_User user, string SenderEmailAddress, string message)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {

                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        mst_User userToUpdate = new mst_User();
                        userToUpdate = (from entry in entities.mst_User
                                        where entry.ID == user.ID
                                        select entry).FirstOrDefault();

                        userToUpdate.Password = user.Password;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = DateTime.UtcNow;
                        userToUpdate.ChangePasswordFlag = true;
                        userToUpdate.EnType = "A";
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }
        public static void WrongAttemptCountUpdate(long userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User userToUpdate = (from Row in entities.mst_User
                                         where Row.ID == userID && Row.IsDeleted == false && Row.IsActive == true
                                         select Row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.WrongAttempt = 0;

                    entities.SaveChanges();
                }
            }
        }
        public static void ToggleStatus(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                bool isActive = !(from row in entities.mst_User
                                  where row.ID == userID
                                  select row.IsActive).SingleOrDefault();


                mst_User userToUpdate = (from row in entities.mst_User
                                         where row.ID == userID
                                         select row).FirstOrDefault();

                userToUpdate.IsActive = isActive;

                if (isActive)
                {
                    userToUpdate.DeactivatedOn = null;
                }
                else
                {
                    userToUpdate.DeactivatedOn = DateTime.UtcNow;
                }

                entities.SaveChanges();
            }
        }
        public static object GetVerticalBranchList(long customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.mst_Vertical
                             where row.CustomerID == customerID
                             select row).Distinct();


                var users = (from row in query
                             select new { ID = row.ID, Name = row.VerticalName }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static List<FillBranchVertical> GetAllVerticals(long CustomerId, List<int> Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<FillBranchVertical> listverticals = new List<FillBranchVertical>();
                if (Branchid.Count > 0)
                {
                    listverticals = (from row in entities.FillBranchVerticals
                                     where row.CustomerID == CustomerId
                                     && Branchid.Contains(row.Branch)
                                     select row).OrderBy(entry => entry.Branch).ToList();
                }
                else
                {
                    listverticals = (from row in entities.FillBranchVerticals
                                     where row.CustomerID == CustomerId
                                     select row).OrderBy(entry => entry.Branch).ToList();

                }
                return listverticals.ToList();
            }
        }
        public static List<BranchVertical> GetAllBranchVerticalList(long custID, int Branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objcust = (from row in entities.BranchVerticals
                               where (row.CustomerID == custID) && row.Branch == Branchid && row.IsActive == true
                               select row);
                return objcust.ToList();

            }
        }
        public static void DeleteBranchVertical(Int64 customerBranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                BranchVertical objTypes = (from row in entities.BranchVerticals
                                           where row.Branch == customerBranchID
                                           select row).FirstOrDefault();

                entities.BranchVerticals.Remove(objTypes);
                entities.SaveChanges();
            }
        }
        public static bool CreateVerticalBranchlist(BranchVertical branchverticals)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    entities.BranchVerticals.Add(branchverticals);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool CreateVerticalBranchlist(List<BranchVertical> branchverticals)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    branchverticals.ForEach(entry =>
                    {
                        entities.BranchVerticals.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool EditAssignmentVisibleorNot(int CustBranchID, int VerticalID)
        {
            try
            {
                return RiskCategoryManagement.CheckAuditKickedOfforNot(CustBranchID, VerticalID);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool UpdateBranchVerticals(int customerid, List<int> Branchid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ids = (from row in entities.BranchVerticals
                               where row.CustomerID == customerid && Branchid.Contains(row.Branch)
                               select row).ToList();

                    ids.ForEach(entry =>
                    {
                        if (EditAssignmentVisibleorNot(entry.Branch, entry.VerticalID))
                        {
                            BranchVertical BranchVerticals = (from row in entities.BranchVerticals
                                                              where row.ID == entry.ID
                                                              select row).FirstOrDefault();
                            BranchVerticals.IsActive = false;
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool VerticalBrachExist(int customerid, int Branchid, int verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.BranchVerticals
                             where row.CustomerID == customerid && row.Branch == Branchid && row.VerticalID == verticalid
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static void EditVerticalBrachExist(int customerid, int Branchid, int verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var RiskToUpdate = (from row in entities.BranchVerticals
                                    where row.CustomerID == customerid && row.Branch == Branchid && row.VerticalID == verticalid
                                    select row).FirstOrDefault();
                RiskToUpdate.IsActive = true;
                entities.SaveChanges();
            };
        }
        #region Sushant 27 MAR 2017 Reassign Performer Process      
        public static List<ProcessReassing> GetddlProcessDataProc(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.InternalControlAuditAssignments
                                   join row1 in entities.Mst_Process
                                   on row.ProcessId equals row1.Id
                                   where row.UserID == userID
                                   select new ProcessReassing
                                   {
                                       ProcessId = (int)row.ProcessId,
                                       Name = row1.Name
                                   }).Distinct().ToList();
                return processList;
            }
        }
        public static List<ProcessReassing> GetddlProcessDataICFR(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.AuditAssignments
                                   join row1 in entities.Mst_Process
                                   on row.ProcessId equals row1.Id
                                   where row.UserID == userID
                                   select new ProcessReassing
                                   {
                                       ProcessId = (int)row.ProcessId,
                                       Name = row1.Name
                                   }).Distinct().ToList();
                return processList;
            }
        }
        public static List<ProcessReassing> GetddlProcessDataImp(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.AuditImplementationAssignments
                                   join row1 in entities.Mst_Process
                                   on row.ProcessId equals row1.Id
                                   where row.UserID == userID
                                   select new ProcessReassing
                                   {
                                       ProcessId = (int)row.ProcessId,
                                       Name = row1.Name
                                   }).Distinct().ToList();
                return processList;
            }
        }
        public static List<ReAssigningData> GetAllReassignData(int userID, int processid, string FilterValue)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.InternalControlAuditAssignments
                                   join row1 in entities.InternalAuditScheduleOns
                                   on row.InternalAuditInstance equals row1.InternalAuditInstance
                                   join row2 in entities.mst_CustomerBranch
                                   on row.CustomerBranchID equals row2.ID
                                   join row3 in entities.Mst_Process
                                   on row.ProcessId equals row3.Id
                                   where row.UserID == userID && row2.IsDeleted == false
                                   select new ReAssigningData
                                   {
                                       ForMonth = row1.ForMonth,
                                       FinancialYear = row1.FinancialYear,
                                       CustomerBranchID = (int)row.CustomerBranchID,
                                       Branch = row2.Name,
                                       AuditID= (long)row.AuditID
                                   }).Distinct().ToList();

                //if (processid != -1)
                //{
                //    processList = processList.Where(entry => entry.ProcessID == processid).ToList();
                //}
                //if (!string.IsNullOrEmpty(FilterValue))
                //{
                //    processList = processList.Where(entry => entry.ProcessName.ToLower().Trim() == FilterValue.ToLower().Trim() || entry.Branch.ToLower().Trim() == FilterValue.ToLower().Trim() || entry.FinancialYear.ToLower().Trim() == FilterValue.ToLower().Trim()).ToList();
                //}
                return processList;
            }
        }
        public static List<ReAssigningData> GetAllReassignDataICFR(int userID, int processid, string FilterValue)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.AuditAssignments
                                   join row1 in entities.AuditScheduleOns
                                   on row.RiskCreationId equals row1.RiskCreationID
                                   join row2 in entities.mst_CustomerBranch
                                   on row.CustomerBranchID equals row2.ID
                                   join row3 in entities.Mst_Process
                                   on row.ProcessId equals row3.Id
                                   where row.UserID == userID
                                   select new ReAssigningData
                                   {
                                       ForMonth = row1.ForMonth,
                                       FinancialYear = row1.FinancialYear,
                                       CustomerBranchID = (int)row.CustomerBranchID,
                                       Branch = row2.Name,
                                       AuditID = (int)row.AuditInstanceId,
                                   }).Distinct().ToList();

                //if (processid != -1)
                //{
                //    processList = processList.Where(entry => entry.ProcessID == processid).ToList();
                //}
                //if (!string.IsNullOrEmpty(FilterValue))
                //{
                //    processList = processList.Where(entry => entry.ProcessName.ToLower().Trim() == FilterValue.ToLower().Trim() || entry.Branch.ToLower().Trim() == FilterValue.ToLower().Trim() || entry.FinancialYear.ToLower().Trim() == FilterValue.ToLower().Trim()).ToList();
                //}
                return processList;
            }
        }
        public static List<ReAssigningData> GetAllReassignDataImplement(int userID, int processid, string FilterValue)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var processList = (from row in entities.AuditImplementationAssignments
                                   join row1 in entities.AuditImpementationScheduleOns
                                   on row.ImplementationInstance equals row1.ImplementationInstance
                                   join row2 in entities.mst_CustomerBranch
                                   on row.CustomerBranchID equals row2.ID
                                   join row3 in entities.Mst_Process
                                   on row.ProcessId equals row3.Id
                                   where row.UserID == userID && row2.IsDeleted == false
                                   select new ReAssigningData
                                   {
                                       ForMonth = row1.ForMonth,
                                       FinancialYear = row1.FinancialYear,
                                       CustomerBranchID = (int)row.CustomerBranchID,
                                       Branch = row2.Name,
                                       AuditID = (int)row.AuditID,
                                   }).Distinct().ToList();

                //if (processid != -1)
                //{
                //    processList = processList.Where(entry => entry.ProcessID == processid).ToList();
                //}

                //if (!string.IsNullOrEmpty(FilterValue))
                //{
                //    processList = processList.Where(entry => entry.ProcessName.ToLower().Trim() == FilterValue.ToLower().Trim() || entry.Branch.ToLower().Trim() == FilterValue.ToLower().Trim() || entry.FinancialYear.ToLower().Trim() == FilterValue.ToLower().Trim()).ToList();
                //}

                return processList;
            }
        }
        public static void UpdateReAssignedProcess(int OldUserID, int NewUserID, List<Tuple<int, int>> chkList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                foreach (var item in chkList)
                {
                    var assignmentData = (from row in entities.InternalControlAuditAssignments
                                          where row.AuditID == item.Item1 && row.CustomerBranchID == item.Item2 && row.UserID == OldUserID
                                          select row).ToList();

                    assignmentData.ForEach(entry =>
                    {
                        entry.UserID = NewUserID;
                    });
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdateReAssignedImplementation(int OldUserID, int NewUserID, List<Tuple<int, int>> chkList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                foreach (var item in chkList)
                {
                    var assignmentData = (from row in entities.AuditImplementationAssignments
                                          where row.AuditID == item.Item1 && row.CustomerBranchID == item.Item2 && row.UserID == OldUserID
                                          select row).ToList();

                    assignmentData.ForEach(entry =>
                    {
                        entry.UserID = NewUserID;
                    });
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdateReAssignedICFR(int OldUserID, int NewUserID, List<Tuple<int, int>> chkList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                foreach (var item in chkList)
                {
                    var assignmentData = (from row in entities.AuditAssignments
                                          where row.AuditInstanceId == item.Item1 && row.CustomerBranchID == item.Item2 && row.UserID == OldUserID
                                          select row).ToList();

                    assignmentData.ForEach(entry =>
                    {
                        entry.UserID = NewUserID;
                    });
                    entities.SaveChanges();
                }
            }
        }
        public static List<InternalControlAuditAssignment> GetAllProcessID(int id)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditAssignments
                             where row.UserID == id
                             select row);

                return query.ToList();
            }
        }
        public static List<AuditImplementationAssignment> GetAllImplementationID(int id)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditImplementationAssignments
                             where row.UserID == id
                             select row);

                return query.ToList();
            }
        }
        public static List<AuditAssignment> GetAllICFRID(int id)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.AuditAssignments
                             where row.UserID == id
                             select row);

                return query.ToList();
            }
        }
        public static object FillCustomerTransactionProcess(long branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityTransactions
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.CustomerBranchId == branchid
                             select row1).Distinct();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillCustomerCreationProcess(long branchid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskCategoryCreations
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             where row.CustomerId == row1.CustomerID && row.CustomerBranchId == branchid
                             select row1).Distinct();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillInternalAuditSchedulingProcess(long branchid, string FinancialYear, string ForMonth)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditSchedulings
                             join row1 in entities.Mst_Process
                             on row.Process equals row1.Id
                             where row.CustomerBranchId == branchid
                             && row.FinancialYear == FinancialYear && row.TermName == ForMonth
                             && row.IsDeleted == false
                             select row1).Distinct();

                var users = (from row in query
                             select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        #endregion

        public static bool CreateNewAuditUser(mst_User mstUser)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_User.Add(mstUser);
                entities.SaveChanges();
                return true;
            }
        }

        public static object FillProcessList(long customerID, int BrachID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             join row1 in entities.EntitiesAssignmentAuditManagerRisks
                             on row.Id equals row1.ProcessId
                             where row.IsDeleted == false
                             && row1.ISACTIVE == true
                             && row.CustomerID == customerID
                             && row1.BranchID == BrachID
                             select row).Distinct().ToList();

                var ProcessList = (from row in query
                                   select new { ID = row.Id, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return ProcessList;
            }
        }

        public static int AddDetailsInternalAuditSchedulingTable_ScheduleFirst(InternalAuditScheduling Tempassignments)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.InternalAuditSchedulings.Add(Tempassignments);
                entities.SaveChanges();
                return Tempassignments.Id;
            }
        }

        public static void AddSchedulingSubProcess_ScheduleFirst(AuditScheduling_SubProcessMapping Tempassignments)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.AuditScheduling_SubProcessMapping.Add(Tempassignments);
                entities.SaveChanges();
            }
        }

        public static bool IsExistAuditScheduling_ScheduleFirst(Int64 CustomerBranchid, Int64 verticalID, Int64 ProcessID, string finYear, long AuditID, int subprocessid, string period, int NoofPhases = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (!period.Contains("Phase"))
                {
                    var ProcessList = (from IAS in entities.InternalAuditSchedulings
                                       where IAS.CustomerBranchId == CustomerBranchid
                                       && IAS.VerticalID == verticalID
                                       && IAS.FinancialYear == finYear
                                       && IAS.TermName == period
                                       && IAS.Process == ProcessID
                                       && IAS.IsDeleted == false
                                       && IAS.AuditID == AuditID
                                       select IAS.Id).ToList();
                    if (ProcessList.Count > 0)
                    {
                        var SubProcessList = (from Row in entities.AuditScheduling_SubProcessMapping
                                              where Row.SubProcessID == subprocessid
                                              select Row).ToList();

                        SubProcessList = SubProcessList.Where(entry => ProcessList.Contains((int)entry.SubProcessID)).ToList();
                        if (SubProcessList.Count > 0)
                        {
                            return false;
                        }
                        else
                            return true;
                    }
                    else
                        return true;
                }
                else
                {
                    var ProcessList = (from IAS in entities.InternalAuditSchedulings
                                       where IAS.CustomerBranchId == CustomerBranchid
                                       && IAS.VerticalID == verticalID
                                       && IAS.FinancialYear == finYear
                                       && IAS.TermName == period
                                       && IAS.Process == ProcessID
                                       && IAS.PhaseCount == NoofPhases
                                       && IAS.IsDeleted == false
                                       && IAS.AuditID == AuditID
                                       select IAS.Id).ToList();
                    if (ProcessList.Count > 0)
                    {
                        var SubProcessList = (from Row in entities.AuditScheduling_SubProcessMapping
                                              where Row.SubProcessID == subprocessid
                                              select Row).ToList();

                        SubProcessList = SubProcessList.Where(entry => ProcessList.Contains((int)entry.SubProcessID)).ToList();
                        if (SubProcessList.Count > 0)
                        {
                            return false;
                        }
                        else
                            return true;
                    }
                    else
                        return true;
                }
            }
        }

        public static void UpdateDetailsInternalAuditSchedulingTable_ScheduleFirst(InternalAuditScheduling Tempassignments, AuditScheduling_SubProcessMapping SubProcessMapping)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Scheduledetails = (from row in entities.InternalAuditSchedulings
                                       where row.FinancialYear == Tempassignments.FinancialYear
                                       && row.ISAHQMP == Tempassignments.ISAHQMP && row.CustomerBranchId == Tempassignments.CustomerBranchId
                                       && row.TermName == Tempassignments.TermName && row.VerticalID == Tempassignments.VerticalID
                                       && row.Process == Tempassignments.Process && row.AuditID == Tempassignments.AuditID
                                       select row).FirstOrDefault();

                if (Scheduledetails == null)
                {
                    entities.InternalAuditSchedulings.Add(Tempassignments);
                    entities.SaveChanges();
                    SubProcessMapping.ScheduleOnID = Tempassignments.Id;
                    entities.AuditScheduling_SubProcessMapping.Add(SubProcessMapping);
                    entities.SaveChanges();
                }
                else
                {
                    Scheduledetails.IsDeleted = false;
                    entities.SaveChanges();

                    var SubPMapping = (from row in entities.AuditScheduling_SubProcessMapping
                                       where row.ScheduleOnID == Scheduledetails.Id
                                       && row.SubProcessID == SubProcessMapping.SubProcessID
                                       select row).FirstOrDefault();

                    SubPMapping.UpdaetdOn = DateTime.Now;
                    SubPMapping.UpdatedBy = Tempassignments.Createdby;
                    SubPMapping.IsDeleted = false;
                    entities.SaveChanges();
                }
            }
        }

        public static bool CheckAssignedUserCount(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var result = (from row in entities.SP_GetAssignedUserList(userID)
                              select row).ToList();

                if (result.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static DateTime? UpdateDraftReportDate(long CustomerID, long BranchId, long VerticalId, string ForMonth, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var auditDetails = (from row in entities.AuditClosureDetails
                                    where row.CustomerID == CustomerID && row.BranchId == BranchId &&
                                    row.VerticalId == VerticalId && row.ForMonth == ForMonth && row.FinancialYear == FinancialYear
                                    select row).FirstOrDefault();
                if (auditDetails != null)
                {
                    auditDetails.DraftReportDate = DateTime.Now;
                    entities.SaveChanges();
                    return auditDetails.DraftReportDate;
                }
                else
                {
                    return null;
                }
            }
        }

        public static DateTime? GetDraftReportDate(long CustomerID, long BranchId, long VerticalId, string ForMonth, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var draftDate = (from cs in entities.AuditClosureDetails
                                 where cs.CustomerID == CustomerID && cs.BranchId == BranchId &&
                                 cs.VerticalId == VerticalId && cs.ForMonth == ForMonth &&
                                 cs.FinancialYear == FinancialYear
                                 select cs).FirstOrDefault();
                if (draftDate != null)
                {
                    if (draftDate.DraftReportDate != null)
                    {
                        return draftDate.DraftReportDate;
                    }
                    else
                    {
                        return DateTime.Now;
                    }
                }
                else
                    return DateTime.Now;
            }
        }

        public static DateTime? GetCreatedDateForGenerateReport(long CustomerID, long BranchId, string Period, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var createdDate = (from cs in entities.Tran_FinalDeliverableUpload
                                   where cs.CustomerId == CustomerID && cs.CustomerBranchId == BranchId
                                   && cs.Period == Period && cs.FinancialYear == FinancialYear
                                   select cs).FirstOrDefault();
                if (createdDate != null)
                    return createdDate.CreatedDate;
                else
                    return DateTime.Now;
            }
        }
 
        public static bool IsExistAuditClosureDetails(long CustomerID, long BranchId, long VerticalId, string Period, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.AuditClosureDetails
                             where cs.BranchId == BranchId && cs.CustomerID == CustomerID && cs.VerticalId == VerticalId
                             && cs.ForMonth == Period && cs.FinancialYear == FinancialYear
                             select cs).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<RiskActivityToBeDoneMapping> GetListOfActiveAuditStep(string ActivityToBeDone, long processId, long subProcessId, long customerBranchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<RiskActivityToBeDoneMapping> activeAuditStepIDList = (from RATBDM in entities.RiskActivityToBeDoneMappings
                                                                           where RATBDM.IsActive == true
                                                                           && RATBDM.ActivityTobeDone == ActivityToBeDone
                                                                           && RATBDM.ProcessId == processId
                                                                           && RATBDM.SubProcessId == subProcessId
                                                                           && RATBDM.CustomerBranchID == customerBranchId
                                                                           select RATBDM).Distinct().ToList();
                return activeAuditStepIDList;
            }
        }

        public static void CreateTransactionForHistroricalObservation(InternalAuditTransaction transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.Dated = DateTime.Now;
                entities.InternalAuditTransactions.Add(transaction);
                entities.SaveChanges();
            }
        }

        public static void CreateScheduleOnImplementation(long ImplementationInstance, long createdByID, string creatdByName, string FinancialYear, long branchid, string Period, int VerticalId, int ProcessId, long AuditID, int SubProcessID, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<AuditImplementationTransaction> transactionlist = new List<AuditImplementationTransaction>();

                AuditImpementationScheduleOn auditScheduleon = new AuditImpementationScheduleOn();
                auditScheduleon.ImplementationInstance = ImplementationInstance;
                auditScheduleon.ForMonth = Period;
                auditScheduleon.FinancialYear = FinancialYear;
                auditScheduleon.ProcessId = ProcessId;
                auditScheduleon.AuditID = AuditID;
                entities.AuditImpementationScheduleOns.Add(auditScheduleon);
                entities.SaveChanges();

                AuditImplementationTransaction transaction = new AuditImplementationTransaction()
                {
                    ImplementationInstance = ImplementationInstance,
                    ImplementationScheduleOnID = auditScheduleon.Id,
                    CreatedBy = createdByID,
                    CreatedByText = creatdByName,
                    ForPeriod = Period,
                    FinancialYear = FinancialYear,
                    StatusId = 1,
                    CustomerBranchId = branchid,
                    VerticalID = VerticalId,
                    ProcessId = ProcessId,
                    SubProcessId = SubProcessID,
                    AuditID = AuditID,
                    ImplementationStatusId = 1,
                    UserID = UserID,
                    RoleID = 3,
                    Remarks = "New Implementation Steps Assigned."
                };

                transactionlist.Add(transaction);

                CreateTransactionImplementation(transactionlist);
            }
        }

        public static void CreateTransactionImplementation(List<AuditImplementationTransaction> transaction)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    entities.AuditImplementationTransactions.Add(entry);
                });
                entities.SaveChanges();
            }
        }

        public static long GetAuditIdFromAuditClosureDetails(long CustomerID, long BranchId, long VerticalId, string Period, string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long auditId = 0;
                var query = (from cs in entities.AuditClosureDetails
                             where cs.BranchId == BranchId && cs.CustomerID == CustomerID && cs.VerticalId == VerticalId
                             && cs.ForMonth == Period && cs.FinancialYear == FinancialYear
                             select cs).FirstOrDefault();

                if (query != null)
                {
                    auditId = Convert.ToInt32(query.ID);
                    return auditId;
                }
                else
                {
                    return auditId;
                }
            }
        }

        public static int GetVerticalIDByName(string VerticalName, int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var objTypes = (from row in entities.mst_Vertical
                                where row.VerticalName == VerticalName && row.CustomerID == CustomerID
                                select row.ID).FirstOrDefault();
                return objTypes;
            }
        }

        public static List<int> GetAuditStepDetailsCountForHistoricalObservation(List<RiskActivityToBeDoneMapping> MasterRiskActivityToBeDoneMappingRecords, Int64 CustomerBranchid, int verticalID, long AuditID, int userid, long ProcessId, long subProcessId)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    var ActiveAuditStepIDList = (from RATBDM in MasterRiskActivityToBeDoneMappingRecords
                                                 join IAS in entities.InternalAuditSchedulings
                                                 on RATBDM.CustomerBranchID equals IAS.CustomerBranchId
                                                 join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                 on IAS.Process equals EAAMR.ProcessId
                                                 join SP in entities.AuditScheduling_SubProcessMapping
                                                 on RATBDM.SubProcessId equals SP.SubProcessID
                                                 where IAS.CustomerBranchId == EAAMR.BranchID
                                                 && RATBDM.VerticalID == verticalID
                                                 && EAAMR.UserID == userid
                                                 && RATBDM.IsActive == true
                                                 && IAS.IsDeleted == false
                                                 && IAS.Process == RATBDM.ProcessId
                                                 && IAS.AuditID == AuditID
                                                 && EAAMR.ISACTIVE == true
                                                 && IAS.AuditID == SP.AuditID
                                                 && SP.IsDeleted == false
                                                 && RATBDM.ProcessId == ProcessId
                                                 && RATBDM.SubProcessId == subProcessId
                                                 select RATBDM.ID).Distinct().ToList();





                    if (ActiveAuditStepIDList.ToList().Count > 0)
                    {
                        return ActiveAuditStepIDList.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool SaveAuditStepsDetailsForHistoricalObservation(List<int> ActiveAuditStepIDList, long custID, int CustomerBranchid, int verticalID, string finYear, string period, long auditID)
        {
            try
            {
                bool saveSuccess = false;
                long AuditID = 0;

                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    if (ActiveAuditStepIDList.Count > 0)
                    {
                        //Save Total Steps
                        var AuditDetails = (from ACD in entities.AuditClosureDetails
                                            where ACD.BranchId == CustomerBranchid
                                                             && ACD.VerticalId == verticalID
                                                             && ACD.FinancialYear == finYear
                                                             && ACD.ForMonth == period
                                                             && ACD.ID == auditID
                                            select ACD).FirstOrDefault();

                        if (AuditDetails != null)
                        {
                            //if (AuditDetails.Total < ActiveAuditStepIDList.Count)
                            //{
                            AuditDetails.Total += ActiveAuditStepIDList.Count;
                            AuditDetails.IsKickOff = true;
                            entities.SaveChanges();
                            //}

                            AuditID = AuditDetails.ID;
                            saveSuccess = true;
                        }
                        else
                        {
                            AuditClosureDetail newACD = new AuditClosureDetail()
                            {
                                CustomerID = custID,
                                BranchId = CustomerBranchid,
                                VerticalId = verticalID,
                                FinancialYear = finYear,
                                ForMonth = period,
                                Total = ActiveAuditStepIDList.Count,
                                IsKickOff = true,
                                IsDeleted = false,
                            };

                            entities.AuditClosureDetails.Add(newACD);
                            entities.SaveChanges();

                            AuditID = newACD.ID;
                            saveSuccess = true;
                        }

                        if (ActiveAuditStepIDList.Count > 0)
                        {
                            if (AuditID != 0)
                            {
                                List<AuditStepMapping> AuditStepMappingList = new List<AuditStepMapping>();
                                foreach (int ATBDID in ActiveAuditStepIDList)
                                {
                                    AuditStepMapping newASM = new AuditStepMapping()
                                    {
                                        AuditID = AuditID,
                                        ATBDId = ATBDID,
                                        IsActive = true,
                                    };
                                    if (!RiskCategoryManagement.AuditStepMappingExists(newASM))
                                    {
                                        AuditStepMappingList.Add(newASM);
                                    }
                                    if (AuditStepMappingList.Count > 0)
                                    {
                                        saveSuccess = UserManagementRisk.CreateAuditStepMapping(AuditStepMappingList);
                                    }
                                }
                            }
                            else
                                saveSuccess = false;
                        }
                        //else
                        //    saveSuccess = false;
                        //Save Each Step in AuditStepMapping
                        //if (ActiveAuditStepIDList.Count > 0)
                        //{
                        //    if (AuditID != 0)
                        //    {
                        //        List<AuditStepMapping> AuditStepMappingList = new List<AuditStepMapping>();

                        //        ActiveAuditStepIDList.ForEach(EachAuditStep =>
                        //        {
                        //            AuditStepMapping newASM = new AuditStepMapping()
                        //            {
                        //                AuditID = AuditID,
                        //                ATBDId = EachAuditStep,
                        //                IsActive = true,
                        //            };

                        //            if (!RiskCategoryManagement.AuditStepMappingExists(newASM))
                        //                AuditStepMappingList.Add(newASM);
                        //        });

                        //        if (AuditStepMappingList.Count > 0)
                        //        {
                        //            saveSuccess = UserManagementRisk.CreateAuditStepMapping(AuditStepMappingList);
                        //        }
                        //    } //AuditID is ZERO
                        //    else
                        //        saveSuccess = false;
                        //} //No Audit Step
                        //else
                        //    saveSuccess = false;
                    } //No Audit Step
                    else
                        saveSuccess = false;

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public static bool SaveAuditStepsDetailsForHistoricalObservation(List<int> ActiveAuditStepIDList, long custID, int CustomerBranchid, int verticalID, string finYear, string period, long auditID, int ProcessID, int SubProcessID)
        //{
        //    try
        //    {
        //        bool saveSuccess = false;
        //        long AuditID = 0;

        //        using (AuditControlEntities entities = new AuditControlEntities())
        //        {
        //            if (ActiveAuditStepIDList.Count > 0)
        //            {
        //                //Save Total Steps
        //                var GetCount = (from row in entities.CheckAuditProcessSubProcesses
        //                                where row.AuditID == auditID
        //                                && row.ProcessID == ProcessID
        //                                && row.SubProcessID == SubProcessID
        //                                && ActiveAuditStepIDList.Contains((int)row.ATBDID)
        //                                select row).ToList();
        //                if (GetCount.Count == 0)
        //                {
        //                    var AuditDetails = (from ACD in entities.AuditClosureDetails
        //                                        where ACD.BranchId == CustomerBranchid
        //                                                         && ACD.VerticalId == verticalID
        //                                                         && ACD.FinancialYear == finYear
        //                                                         && ACD.ForMonth == period
        //                                                         && ACD.ID == auditID
        //                                        select ACD).FirstOrDefault();

        //                    if (AuditDetails != null)
        //                    {
        //                        AuditDetails.Total += ActiveAuditStepIDList.Count;
        //                        AuditDetails.IsKickOff = true;
        //                        entities.SaveChanges();
        //                        AuditID = AuditDetails.ID;
        //                        saveSuccess = true;
        //                    }
        //                    else
        //                    {
        //                        AuditClosureDetail newACD = new AuditClosureDetail()
        //                        {
        //                            CustomerID = custID,
        //                            BranchId = CustomerBranchid,
        //                            VerticalId = verticalID,
        //                            FinancialYear = finYear,
        //                            ForMonth = period,
        //                            Total = ActiveAuditStepIDList.Count,
        //                            IsKickOff = true,
        //                            IsDeleted = false,
        //                        };

        //                        entities.AuditClosureDetails.Add(newACD);
        //                        entities.SaveChanges();

        //                        AuditID = newACD.ID;
        //                        saveSuccess = true;
        //                    }
        //                    CheckAuditProcessSubProcess obj = new CheckAuditProcessSubProcess();
        //                    obj.AuditID = (int)auditID;
        //                    obj.ProcessID = ProcessID;
        //                    obj.SubProcessID = SubProcessID;
        //                    entities.CheckAuditProcessSubProcesses.Add(obj);
        //                    entities.SaveChanges();
        //                }

        //                if (ActiveAuditStepIDList.Count > 0)
        //                {
        //                    if (AuditID != 0)
        //                    {
        //                        List<AuditStepMapping> AuditStepMappingList = new List<AuditStepMapping>();
        //                        foreach (int ATBDID in ActiveAuditStepIDList)
        //                        {
        //                            AuditStepMapping newASM = new AuditStepMapping()
        //                            {
        //                                AuditID = AuditID,
        //                                ATBDId = ATBDID,
        //                                IsActive = true,
        //                            };
        //                            if (!RiskCategoryManagement.AuditStepMappingExists(newASM))
        //                            {
        //                                AuditStepMappingList.Add(newASM);
        //                            }
        //                            if (AuditStepMappingList.Count > 0)
        //                            {
        //                                saveSuccess = UserManagementRisk.CreateAuditStepMapping(AuditStepMappingList);
        //                            }
        //                        }
        //                    }
        //                    else
        //                        saveSuccess = false;
        //                }
        //                //else
        //                //    saveSuccess = false;
        //                //Save Each Step in AuditStepMapping
        //                //if (ActiveAuditStepIDList.Count > 0)
        //                //{
        //                //    if (AuditID != 0)
        //                //    {
        //                //        List<AuditStepMapping> AuditStepMappingList = new List<AuditStepMapping>();

        //                //        ActiveAuditStepIDList.ForEach(EachAuditStep =>
        //                //        {
        //                //            AuditStepMapping newASM = new AuditStepMapping()
        //                //            {
        //                //                AuditID = AuditID,
        //                //                ATBDId = EachAuditStep,
        //                //                IsActive = true,
        //                //            };

        //                //            if (!RiskCategoryManagement.AuditStepMappingExists(newASM))
        //                //                AuditStepMappingList.Add(newASM);
        //                //        });

        //                //        if (AuditStepMappingList.Count > 0)
        //                //        {
        //                //            saveSuccess = UserManagementRisk.CreateAuditStepMapping(AuditStepMappingList);
        //                //        }
        //                //    } //AuditID is ZERO
        //                //    else
        //                //        saveSuccess = false;
        //                //} //No Audit Step
        //                //else
        //                //    saveSuccess = false;
        //            } //No Audit Step
        //            else
        //                saveSuccess = false;

        //            return saveSuccess;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public static bool IsExistsInInternalAuditInstance(Int64 CustomerBranchID, long ProcessId, long subProcessId, int verticalId, long auditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditInstances
                             where row.AuditID == auditId
                             && row.ProcessId == ProcessId
                             && row.SubProcessId == -1
                             && row.CustomerBranchID == CustomerBranchID
                             && row.VerticalID == verticalId
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool IsExistsInInternalAuditScheduleOns(long ProcessId, string ForMonth, string FinancialYear, long auditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditScheduleOns
                             where row.AuditID == auditId
                             && row.ProcessId == ProcessId
                             && row.ForMonth == ForMonth
                             && row.FinancialYear == FinancialYear
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        
        public static bool IsExistsInInternalAuditSchedulings(long ProcessId, Int64 CustomerBranchId, string FinancialYear, string TermName, int VerticalID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditSchedulings
                             where row.CustomerBranchId == CustomerBranchId
                             && row.Process == ProcessId
                             && row.TermName == TermName
                             && row.FinancialYear == FinancialYear
                             && row.AuditID == AuditID
                             && row.VerticalID == VerticalID
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        
        public static List<int> GetAuditStepDetailsCountForHistoricalObservation(List<RiskActivityToBeDoneMapping> MasterRiskActivityToBeDoneMappingRecords, Int64 CustomerBranchid, int verticalID, long AuditID, int userid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {

                    var ActiveAuditStepIDList = (from RATBDM in MasterRiskActivityToBeDoneMappingRecords
                                                 join IAS in entities.InternalAuditSchedulings
                                                 on RATBDM.CustomerBranchID equals IAS.CustomerBranchId
                                                 join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                 on IAS.Process equals EAAMR.ProcessId
                                                 join SP in entities.AuditScheduling_SubProcessMapping
                                                 on RATBDM.SubProcessId equals SP.SubProcessID
                                                 where IAS.CustomerBranchId == EAAMR.BranchID
                                                 && RATBDM.VerticalID == verticalID
                                                 && EAAMR.UserID == userid
                                                 && RATBDM.IsActive == true
                                                 && IAS.IsDeleted == false
                                                 && IAS.Process == RATBDM.ProcessId
                                                 && IAS.AuditID == AuditID
                                                 && EAAMR.ISACTIVE == true
                                                 && IAS.AuditID == SP.AuditID
                                                 && SP.IsDeleted == false
                                                 select RATBDM.ID).Distinct().ToList();

                    var ATBDIDListCheck = (from row in entities.InternalAuditTransactions
                                           where row.AuditID == AuditID && ActiveAuditStepIDList.Contains((int)row.ProcessId)
                                           select row.ATBDId).Distinct().ToList();

                    ActiveAuditStepIDList = ActiveAuditStepIDList.Where(e => !ATBDIDListCheck.Contains(e)).ToList();


                    //var ActiveAuditStepIDList = (from RATBDM in MasterRiskActivityToBeDoneMappingRecords
                    //                            join IAS in entities.InternalAuditSchedulings
                    //                            on RATBDM.CustomerBranchID equals IAS.CustomerBranchId                                               
                    //                            where RATBDM.VerticalID == verticalID
                    //                            && RATBDM.IsActive == true
                    //                            && IAS.IsDeleted == false
                    //                            &&  IAS.Process == RATBDM.ProcessId
                    //                            && IAS.AuditID == AuditID                                              
                    //                            select RATBDM.ID).Distinct().ToList();


                    if (ActiveAuditStepIDList.ToList().Count > 0)
                    {
                        return ActiveAuditStepIDList.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        public static InternalAuditScheduling GetDetailsInternalAuditSchedulingsId(long ProcessId, Int64 CustomerBranchId, string FinancialYear, string TermName, int VerticalID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditSchedulings
                             where row.CustomerBranchId == CustomerBranchId
                             && row.Process == ProcessId
                             && row.TermName == TermName
                             && row.FinancialYear == FinancialYear
                             && row.AuditID == AuditID
                             && row.VerticalID == VerticalID
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return query;
                }
                else
                {
                    return null;
                }
            }
        }

        public static InternalAuditInstance GetDetailsInternalAuditInstanceId(Int64 CustomerBranchID, long ProcessId, long subProcessId, int verticalId, long auditId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalAuditInstances
                             where row.AuditID == auditId
                             && row.ProcessId == ProcessId
                             && row.SubProcessId == -1
                             && row.CustomerBranchID == CustomerBranchID
                             && row.VerticalID == verticalId
                             select row).FirstOrDefault();
                if (query != null)
                {
                    return query;
                }
                else
                {
                    return null;
                }
            }
        }
        
        public static bool IsExistsInternalControlAuditAssignment(InternalControlAuditAssignment ICAA)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.InternalControlAuditAssignments
                             where cs.RoleID == ICAA.RoleID
                             && cs.UserID == ICAA.UserID
                             && cs.ProcessId == ICAA.ProcessId
                             && cs.SubProcessId == ICAA.SubProcessId
                             && cs.CustomerBranchID == ICAA.CustomerBranchID
                             && cs.VerticalID == ICAA.VerticalID
                             && cs.AuditID == ICAA.AuditID
                             select cs).FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        public static bool IsExistsAuditImplementationInstance(AuditImplementationInstance AII)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.AuditImplementationInstances
                             where cs.CustomerBranchID == AII.CustomerBranchID
                             && cs.ProcessId == AII.ProcessId
                             && cs.ForPeriod == AII.ForPeriod
                             && cs.VerticalID == AII.VerticalID
                             && cs.AuditID == AII.AuditID
                             select cs).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static AuditImplementationInstance GetIdFromAuditImplementationInstance(AuditImplementationInstance AII)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.AuditImplementationInstances
                             where cs.CustomerBranchID == AII.CustomerBranchID
                             && cs.ProcessId == AII.ProcessId
                             && cs.ForPeriod == AII.ForPeriod
                             && cs.VerticalID == AII.VerticalID
                             && cs.AuditID == AII.AuditID
                             select cs).FirstOrDefault();

                if (query != null)
                {
                    return query;
                }
                else
                {
                    return null;
                }
            }
        }

        public static bool IsExistAuditImplementationAssignment(AuditImplementationAssignment auditImplementationAssignment)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.AuditImplementationAssignments
                             where cs.RoleID == auditImplementationAssignment.RoleID && cs.ForPeriod == auditImplementationAssignment.ForPeriod
                             && cs.CustomerBranchID == auditImplementationAssignment.CustomerBranchID && cs.ImplementationInstance == auditImplementationAssignment.ImplementationInstance
                             && cs.FinancialYear == auditImplementationAssignment.FinancialYear && cs.VerticalID == auditImplementationAssignment.VerticalID
                             && cs.ProcessId == auditImplementationAssignment.ProcessId && cs.SubProcessId == auditImplementationAssignment.SubProcessId
                             && cs.AuditID == auditImplementationAssignment.AuditID
                             select cs).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static InternalControlAuditAssignment GetAuditStartDateEndDateFromInternalControlAuditAssignment(int RoleID, long UserID, long ProcessId, long SubProcessId, long CustomerBranchID, int VerticalID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.InternalControlAuditAssignments
                             where
                             cs.CustomerBranchID == CustomerBranchID
                             && cs.VerticalID == VerticalID
                             && cs.AuditID == AuditID
                             //&& cs.UserID == UserID
                             //&& cs.RoleID == RoleID 
                             //&& cs.ProcessId == ProcessId
                             //&& cs.SubProcessId == SubProcessId

                             select cs).FirstOrDefault();
                if (query != null)
                {
                    return query;
                }
                else
                {
                    return null;
                }
            }
        }
        
        public static InternalAuditScheduling GeDatestAuditScheduling_ScheduleFirst(Int64 CustomerBranchid, Int64 verticalID, Int64 ProcessID, string finYear, long AuditID, int subprocessid, string period, int NoofPhases = 0)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.InternalAuditSchedulings
                             where cs.CustomerBranchId == CustomerBranchid
                             && cs.VerticalID == verticalID
                             //&& cs.Process == ProcessID
                             && cs.FinancialYear == finYear
                             && cs.TermName == period
                             && cs.AuditID == AuditID
                             select cs).FirstOrDefault();
                if (query != null)
                {
                    return query;
                }
                else
                {
                    return null;
                }
            }
        }
        
        public static List<int?> GetPhaseCountForAuditHeadDashboard(List<Int64> CustomerBranchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var MstRiskResult = (from row in entities.InternalAuditSchedulings
                                     where CustomerBranchId.Contains(row.CustomerBranchId)
                                     select row.PhaseCount).ToList().Distinct().ToList();
                if (MstRiskResult != null)
                {
                    return MstRiskResult;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<RiskActivityTransactionForddl> FillVerticalListFromMultiselect(List<int> branchId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var objcust = (from row in entities.RiskActivityTransactions
                               join row3 in entities.Internal_AuditAssignment
                               on row.CustomerBranchId equals row3.CustomerBranchid
                               join row2 in entities.mst_Vertical
                               on row.VerticalsId equals row2.ID
                               where row.VerticalsId == row3.VerticalID
                               && branchId.Contains((int)row.CustomerBranchId)
                               //&& row3.VerticalID == VerticalId
                               select new RiskActivityTransactionForddl()
                               {
                                   VerticalsId = row.VerticalsId,
                                   VerticalName = row2.VerticalName,
                               }).Distinct().ToList();

                return objcust;

            }
        }

        public static object FillSchedulingTypeMultiple(List<int> branchIdList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<GetSchedulingTypeAsAssignForAuditHeadDashboard_Result> query = new List<GetSchedulingTypeAsAssignForAuditHeadDashboard_Result>();

                var query1 = (from row in entities.GetSchedulingTypeAsAssignForAuditHeadDashboard()
                              select row);

                query = query1.Where(entry => branchIdList.Contains(entry.CustomerBranchId)).ToList();

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>().Distinct();

                return users;
            }
        }
        
        public static List<int> GetAuditStepDetailsCountForHistoricalObservation(Int64 CustomerBranchid, int verticalID, long AuditID, int userid, long ProcessId, long subProcessId, long RiskCategoryCreationID, int RiskActivityTransaction)//, string AuditstepName, string AuditObjective
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ActiveAuditStepIDList = (from RATBDM in entities.RiskActivityToBeDoneMappings
                                                 join IAS in entities.InternalAuditSchedulings
                                                 on RATBDM.CustomerBranchID equals IAS.CustomerBranchId
                                                 join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                                 on IAS.Process equals EAAMR.ProcessId
                                                 join SP in entities.AuditScheduling_SubProcessMapping
                                                 on RATBDM.SubProcessId equals SP.SubProcessID
                                                 //join step in entities.AuditStepMasters
                                                 //on RATBDM.AuditStepMasterID equals step.ID
                                                 where IAS.CustomerBranchId == EAAMR.BranchID
                                                 && RATBDM.VerticalID == verticalID
                                                 && EAAMR.UserID == userid
                                                 && RATBDM.IsActive == true
                                                 && IAS.IsDeleted == false
                                                 && IAS.Process == RATBDM.ProcessId
                                                 && IAS.AuditID == AuditID
                                                 && EAAMR.ISACTIVE == true
                                                 && IAS.AuditID == SP.AuditID
                                                 && SP.IsDeleted == false
                                                 && RATBDM.ProcessId == ProcessId
                                                 && RATBDM.SubProcessId == subProcessId
                                                 && RATBDM.RiskCategoryCreationId == RiskCategoryCreationID
                                                 && RATBDM.RiskActivityId == RiskActivityTransaction
                                                 //&& RATBDM.AuditObjective == AuditObjective
                                                 //&& step.AuditStep == AuditstepName
                                                 select RATBDM.ID).Distinct().ToList();

                    if (ActiveAuditStepIDList.ToList().Count > 0)
                    {
                        return ActiveAuditStepIDList.ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool CheckAssignedProcess(int UserID, int processID, int auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var result = (from row in entities.InternalControlAuditAssignments
                              where row.AuditID == auditID && row.ProcessId == processID
                              && row.UserID == UserID && row.RoleID == 4
                              select row).ToList();

                if (result.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int CheckAssignedRoleOfUser(int AuditID, int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Checkexist = (from row in entities.InternalControlAuditAssignments
                                  where row.AuditID == AuditID
                                  && row.UserID == userID
                                  && row.IsActive == true
                                  select row.RoleID).FirstOrDefault();

                return Checkexist;
            }
        }

        public static List<SP_UserListReport_Result> GetUserReportList(int customerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.SP_UserListReport(customerId)
                             select row).ToList();

                return query;
            }
        }

        public static List<SP_CustomerBranchReport_Result> GetCustomerBranchReportList(int customerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.SP_CustomerBranchReport(customerId)
                             select row).ToList();

                return query;
            }
        }

        public static InternalAuditTransaction GetTrasactionID(int AuditID, int ATBDID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Checkexist = (from row in entities.InternalAuditTransactions
                                  where row.AuditID == AuditID
                                  && row.ATBDId == ATBDID
                                  select row).FirstOrDefault();

                return Checkexist;
            }
        }

        public static bool CheckWorkingFileExist(InternalFileData_Risk file)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Checkexist = (from row in entities.InternalFileData_Risk
                                  where row.Name == file.Name
                                  && row.ATBDId == file.ATBDId
                                  && row.AuditID == file.AuditID
                                  && row.IsDeleted == false
                                  && row.TypeOfFile != "OB"
                                  && row.IsDeleted == false
                                  select row).FirstOrDefault();

                if (Checkexist == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckWorkingFileTwoExist(InternalFileData_Risk file)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Checkexist = (from row in entities.InternalFileData_Risk
                                  where row.Name == file.Name
                                  && row.ATBDId == file.ATBDId
                                  && row.AuditID == file.AuditID
                                  && row.IsDeleted == false
                                  && row.TypeOfFile == "OB"
                                  select row).FirstOrDefault();

                if (Checkexist == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static string GetUserName(int userID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Checkexist = (from row in entities.mst_User
                                  where row.ID == userID
                                  select row.FirstName + " " + row.LastName).FirstOrDefault();

                return Checkexist;
            }
        }

        public static bool UpdateTransactionNew(InternalAuditTransaction transaction, List<InternalFileData_Risk> files, long userid)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            if (files.Count > 0)
                            {
                                foreach (InternalFileData_Risk fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileData_Risk.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping_Risk fileMapping = new InternalFileDataMapping_Risk();
                                    fileMapping.TransactionID = transaction.ID;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.AuditScheduleOnID;
                                    fileMapping.FileType = 1;
                                    entities.InternalFileDataMapping_Risk.Add(fileMapping);
                                }
                                entities.SaveChanges();
                                dbtransaction.Commit();
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            //if (filesList != null)
                            //{
                            //    foreach (var dfile in filesList)
                            //    {
                            //        if (System.IO.File.Exists(dfile.Key))
                            //        {
                            //            System.IO.File.Delete(dfile.Key);
                            //        }
                            //    }
                            //}
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string GetstatusbyID(int StatusID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var fetchData = (from row in entities.InternalAuditStatus
                                 where row.ID == StatusID
                                 select row.Name).FirstOrDefault();
                return fetchData;
            }
        }
        
    }
}
