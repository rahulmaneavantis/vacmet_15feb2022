﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class NoticeManagement
    {
        #region Notice
        public static long CreateNotice(tbl_LegalNoticeInstance newNotice)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    newNotice.CreatedOn = DateTime.Now;
                    newNotice.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalNoticeInstance.Add(newNotice);
                    entities.SaveChanges();

                    return newNotice.ID;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }

        public static bool ExistsNotice(string TitleNotice, long NoticeInstanceID,long CustomerId)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalNoticeInstance
                             where row.IsDeleted == false && row.CustomerID ==CustomerId
                             && row.NoticeTitle.Equals(TitleNotice)
                             select row);

                if (NoticeInstanceID != 0)
                {
                    query = query.Where(entry => entry.ID != NoticeInstanceID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool ExistsNotice(string TitleNotice, long NoticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalNoticeInstance
                             where row.IsDeleted == false
                             && row.NoticeTitle.Equals(TitleNotice)
                             select row);

                if (NoticeInstanceID != 0)
                {
                    query = query.Where(entry => entry.ID != NoticeInstanceID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool ExistsNoticeRefNo(string noticeRefNo, long noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalNoticeInstance
                             where row.RefNo.ToUpper().Trim().Equals(noticeRefNo.ToUpper().Trim())
                             && row.IsDeleted == false
                             select row);

                if (noticeInstanceID != 0)
                {
                    query = query.Where(entry => entry.ID != noticeInstanceID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool UpdateNotice(tbl_LegalNoticeInstance newNotice)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var noticeRecordToUpdate = (from row in entities.tbl_LegalNoticeInstance
                                                where row.ID == newNotice.ID
                                                select row).FirstOrDefault();

                    if (noticeRecordToUpdate != null)
                    {
                        noticeRecordToUpdate.NoticeType = newNotice.NoticeType;
                        noticeRecordToUpdate.RefNo = newNotice.RefNo;
                        noticeRecordToUpdate.NoticeDate = newNotice.NoticeDate;
                        noticeRecordToUpdate.Section = newNotice.Section;
                        noticeRecordToUpdate.NoticeCategoryID = newNotice.NoticeCategoryID;
                        noticeRecordToUpdate.NoticeTitle = newNotice.NoticeTitle;
                        noticeRecordToUpdate.NoticeTerm = newNotice.NoticeTerm;
                        noticeRecordToUpdate.NoticeDetailDesc = newNotice.NoticeDetailDesc;
                        noticeRecordToUpdate.CustomerBranchID = newNotice.CustomerBranchID;
                        noticeRecordToUpdate.Jurisdiction = newNotice.Jurisdiction;
                        noticeRecordToUpdate.DepartmentID = newNotice.DepartmentID;
                        noticeRecordToUpdate.ContactPersonOfDepartment = newNotice.ContactPersonOfDepartment;
                        noticeRecordToUpdate.OwnerID = newNotice.OwnerID;
                        noticeRecordToUpdate.NoticeRiskID = newNotice.NoticeRiskID;
                        noticeRecordToUpdate.ClaimAmt = newNotice.ClaimAmt;
                        noticeRecordToUpdate.ProbableAmt = newNotice.ProbableAmt;
                        noticeRecordToUpdate.ImpactType = newNotice.ImpactType;
                        noticeRecordToUpdate.Monetory = newNotice.Monetory;
                        noticeRecordToUpdate.NonMonetory = newNotice.NonMonetory;
                        noticeRecordToUpdate.Years = newNotice.Years;
                        noticeRecordToUpdate.UpdatedBy = newNotice.UpdatedBy;
                        noticeRecordToUpdate.UpdatedOn = DateTime.Now;
                        noticeRecordToUpdate.DateOfReciept = newNotice.DateOfReciept;
                        noticeRecordToUpdate.CustomerID = newNotice.CustomerID;
                        noticeRecordToUpdate.NoticeBudget = newNotice.NoticeBudget;
                        noticeRecordToUpdate.Penalty = newNotice.Penalty;
                        noticeRecordToUpdate.Intrest = newNotice.Intrest;
                        noticeRecordToUpdate.Taxdemand = newNotice.Taxdemand;
                        noticeRecordToUpdate.ProvisioninBook = newNotice.ProvisioninBook;
                        noticeRecordToUpdate.state = newNotice.state;
                        noticeRecordToUpdate.Period = newNotice.Period;
                        noticeRecordToUpdate.ProtestMoney = newNotice.ProtestMoney;
                        noticeRecordToUpdate.BankGurantee = newNotice.BankGurantee;
                        noticeRecordToUpdate.Provisionalamt = newNotice.Provisionalamt;
                        noticeRecordToUpdate.RecoveryAmount = newNotice.RecoveryAmount;
                        noticeRecordToUpdate.Remark = newNotice.Remark;
                        noticeRecordToUpdate.AuditScrutiny = newNotice.AuditScrutiny;
                        //  noticeRecordToUpdate.FinancialYear = newNotice.FinancialYear;
                        noticeRecordToUpdate.RiskTypeID = newNotice.RiskTypeID;

                        if (newNotice.NoticeDocDate!=null)
                        {
                            noticeRecordToUpdate.NoticeDocDate = Convert.ToDateTime(newNotice.NoticeDocDate);
                        }
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }




        public static bool UpdateNoticeType(long noticeInstanceID, int noticeTypeID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var recordToUpdate = (from row in entities.tbl_LegalNoticeInstance
                                          where row.ID == noticeInstanceID
                                          select row).FirstOrDefault();

                    if (recordToUpdate != null)
                    {
                        recordToUpdate.NoticeCategoryID = noticeTypeID;
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DeleteNoticeByID(int noticeInstanceID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var queryResult = (from row in entities.tbl_LegalNoticeInstance
                                       where row.ID == noticeInstanceID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteNoticeByID(int noticeInstanceID, int deletedByUserID)
        {
            try
            {
                bool deleteSuccess = false;
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    List<tbl_LitigationFileData> noticeDocToDelete = entities.tbl_LitigationFileData
                                                                .Where(x => x.NoticeCaseInstanceID == noticeInstanceID
                                                                && (x.DocType.Trim() == "N" || x.DocType.Trim() == "NT" || x.DocType.Trim() == "NR")).ToList();

                    if (noticeDocToDelete.Count > 0)
                    {
                        noticeDocToDelete.ForEach(entry => entry.IsDeleted = true);
                        noticeDocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        noticeDocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<tbl_LegalNoticeResponse> ResponsesToDelete = entities.tbl_LegalNoticeResponse.Where(x => x.NoticeInstanceID == noticeInstanceID).ToList();

                    if (ResponsesToDelete.Count > 0)
                    {
                        ResponsesToDelete.ForEach(entry => entry.IsActive = false);
                        ResponsesToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        ResponsesToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<tbl_TaskScheduleOn> tasksToDelete = entities.tbl_TaskScheduleOn
                                                            .Where(x => x.NoticeCaseInstanceID == noticeInstanceID
                                                            && x.TaskType == "N").ToList();

                    if (tasksToDelete.Count > 0)
                    {
                        tasksToDelete.ForEach(entry => entry.IsActive = false);
                        tasksToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        tasksToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    var queryRating = (from row in entities.tbl_LawyerListRating
                                       where row.CaseNoticeID == noticeInstanceID
                                       && row.Type == 2
                                       select row).ToList();

                    if (queryRating.Count > 0)
                    {
                        queryRating.ForEach(entry => entry.IsActive = false);
                        queryRating.ForEach(entry => entry.UpdatedBy = deletedByUserID);
                        queryRating.ForEach(entry => entry.UpdatedOn = DateTime.Now);
                        entities.SaveChanges();

                        queryRating.ForEach(eachRatingRecord =>
                        {
                            var LawyerRating = (from row in entities.sp_LiGetLawyerRatingAVG(Convert.ToInt32(eachRatingRecord.LawyerID))
                                                select row).FirstOrDefault();

                            if (!string.IsNullOrEmpty(Convert.ToString(LawyerRating)))
                            {
                                tbl_LawyerFinalRating objNewRate = new tbl_LawyerFinalRating()
                                {
                                    LawyerID = Convert.ToInt32(eachRatingRecord.LawyerID),
                                    Rating = LawyerRating,
                                    IsActive = true
                                };

                                var checkUser = (from row in entities.tbl_LawyerFinalRating
                                                 where row.LawyerID == objNewRate.LawyerID
                                                 && row.IsActive == true
                                                 select row).FirstOrDefault();
                                if (checkUser == null)
                                {
                                    objNewRate.CreatedBy = deletedByUserID;
                                    objNewRate.CreatedOn = DateTime.Now;
                                    entities.tbl_LawyerFinalRating.Add(objNewRate);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkUser.Rating = objNewRate.Rating;
                                    checkUser.UpdatedBy = objNewRate.UpdatedBy;
                                    checkUser.UpdatedOn = objNewRate.UpdatedOn;
                                    entities.SaveChanges();
                                }
                            }
                        });
                    }

                    var queryResult = (from row in entities.tbl_LegalNoticeInstance
                                       where row.ID == noticeInstanceID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                    else
                        deleteSuccess = false;

                    return deleteSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        
        public static int GetAssignedNoticeCount(List<View_NoticeAssignedInstance> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID, int noticeStatus,int custID)
        {
            //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in masterRecords
                             select row).ToList();

                var assignentity = (from row in entities.LitigationEntitiesAssignments
                                    where row.UserID== loggedInUserID
                                    select row.BranchID).ToList();

                if (noticeStatus == 3) //3--Closed otherwise Open
                    query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                else
                    query = query.Where(entry => entry.TxnStatusID < 3).ToList();

                com.VirtuosoITech.ComplianceManagement.Business.Data.ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(custID));
                if (isexist != null)
                {
                	 query = query.Where(entry => entry.RoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {                       
                        query = query.Where(entry => (entry.UserID == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID) || assignentity.Contains(entry.CustomerBranchID)).ToList();
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                    else
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList(); // In case of MGMT or CADMN
                    }
                }
                //ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(custID));
                //if (isexist != null)
                //{
                //    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID) || assignentity.Contains(entry.CustomerBranchID)).ToList();

                //}

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeInstanceID,
                                 g.RefNo,
                                 g.NoticeTitle,
                                 g.NoticeTerm,
                                 g.NoticeDetailDesc,
                                 g.NoticeType,
                                 g.NoticeTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName
                             } into GCS
                             select new View_NoticeAssignedInstance()
                             {
                                 NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                 NoticeTitle = GCS.Key.NoticeTitle,
                                 NoticeTerm = GCS.Key.NoticeTerm,
                                 NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                 NoticeTypeName = GCS.Key.NoticeTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                             }).ToList();
                }

                return query.Count();
            }
        }
        public static List<View_NoticeAssignedInstance> GetAssignedNoticeList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int partyID, int deptID, int noticeStatus, string noticeType) /*int branchID*/
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.View_NoticeAssignedInstance
                             where row.CustomerID == customerID
                             select row).ToList();

                if (noticeStatus != -1)
                {
                    //if (noticeStatus == 3) //3--Closed otherwise Open
                    //    query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                    //else
                    //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                    query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                }


                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (noticeType != "" && noticeType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.NoticeType == noticeType).ToList();

                com.VirtuosoITech.ComplianceManagement.Business.Data.ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 1);
                        if (caseList.Count > 0)
                        {
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeInstanceID))).ToList();
                        }
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                    }
                }


                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeInstanceID,
                                 g.RefNo,
                                 g.NoticeTitle,
                                 g.NoticeDetailDesc,
                                 g.NoticeCategoryID,
                                 g.NoticeType,
                                 g.NoticeTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 g.FYName

                             } into GCS
                             select new View_NoticeAssignedInstance()
                             {
                                 NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 NoticeTitle = GCS.Key.NoticeTitle,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeTypeName = GCS.Key.NoticeTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 FYName = GCS.Key.FYName
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.NoticeType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }

        //public static List<View_NoticeAssignedInstance> GetAssignedNoticeList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int partyID, int deptID, int noticeStatus, string noticeType) /*int branchID*/
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var query = (from row in entities.View_NoticeAssignedInstance
        //                     where row.CustomerID == customerID
        //                     select row).ToList();

        //        if (noticeStatus != -1)
        //        {
        //            //if (noticeStatus == 3) //3--Closed otherwise Open
        //            //    query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
        //            //else
        //            //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();
        //            query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
        //        }


        //        if (branchList.Count > 0)
        //            query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //        if (partyID != -1)
        //            query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

        //        if (deptID != -1)
        //            query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //        if (noticeType != "" && noticeType != "B") //B--Both --Inward(I) and Outward(O)
        //            query = query.Where(entry => entry.NoticeType == noticeType).ToList();

        //        if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //            query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
        //        else // In case of MGMT or CADMN 
        //        {
        //            query = query.Where(entry => entry.RoleID == 3).ToList();
        //        }

        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.NoticeInstanceID,
        //                         g.RefNo,
        //                         g.NoticeTitle,
        //                         g.NoticeDetailDesc,
        //                         g.NoticeCategoryID,
        //                         g.NoticeType,
        //                         g.NoticeTypeName,
        //                         g.CustomerID,
        //                         g.CustomerBranchID,
        //                         g.BranchName,
        //                         g.DepartmentID,
        //                         g.DeptName,
        //                         g.PartyID,
        //                         g.PartyName,
        //                         g.CreatedOn,
        //                         g.OwnerID,
        //                         g.UpdatedOn,
        //                         g.FYName

        //                     } into GCS
        //                     select new View_NoticeAssignedInstance()
        //                     {
        //                         NoticeInstanceID = GCS.Key.NoticeInstanceID,
        //                         RefNo = GCS.Key.RefNo,
        //                         NoticeTitle = GCS.Key.NoticeTitle,
        //                         NoticeCategoryID = GCS.Key.NoticeCategoryID,
        //                         NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
        //                         NoticeType = GCS.Key.NoticeType,
        //                         NoticeTypeName = GCS.Key.NoticeTypeName,
        //                         CustomerID = GCS.Key.CustomerID,
        //                         CustomerBranchID = GCS.Key.CustomerBranchID,
        //                         BranchName = GCS.Key.BranchName,
        //                         DepartmentID = GCS.Key.DepartmentID,
        //                         DeptName = GCS.Key.DeptName,
        //                         PartyID = GCS.Key.PartyID,
        //                         PartyName = GCS.Key.PartyName,
        //                         CreatedOn = GCS.Key.CreatedOn,
        //                         OwnerID = GCS.Key.OwnerID,
        //                         UpdatedOn = GCS.Key.UpdatedOn,
        //                         FYName = GCS.Key.FYName
        //                     }).ToList();
        //        }

        //        if (query.Count > 0)
        //        {
        //            query = query.OrderByDescending(entry => entry.UpdatedOn)
        //                .ThenByDescending(entry => entry.CreatedOn).ToList();
        //            //query = query.OrderBy(entry => entry.NoticeType)
        //            //    .ThenBy(entry => entry.CustomerBranchID).ToList();
        //        }

        //        return query.ToList();
        //    }
        //}

        public static List<View_NoticeAssignedInstance> GetNoticeDetailList()
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.View_NoticeAssignedInstance
                             select row).ToList();

                return query.ToList();
            }
        }

        public static tbl_LegalNoticeInstance GetNoticeByID(int noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.tbl_LegalNoticeInstance
                             where row.ID == noticeInstanceID
                             select row).FirstOrDefault();
                return Query;
            }
        }

        public static List<int> GetNoticeFileByID(int noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                List<int> Query = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == noticeInstanceID
                                   && row.IsDeleted == false
                                   select row.ID).ToList();
                return Query;
            }
        }

        #endregion

        #region Notice-Document
        public static bool CreateNoticeDocumentMapping(tbl_LitigationFileData newNoticeDoc)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    newNoticeDoc.EnType = "A";
                    entities.tbl_LitigationFileData.Add(newNoticeDoc);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistsNoticeDocumentMapping(tbl_LitigationFileData ICR)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LitigationFileData
                             where row.FileName == ICR.FileName
                             && row.IsDeleted == false
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        public static int ExistsNoticeDocumentReturnVersion(tbl_LitigationFileData ICR)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LitigationFileData
                             where row.FileName == ICR.FileName
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }
        public static List<Sp_Litigation_CaseDocument_Result> GetNoticeDocumentMapping(int noticeInstanceID, string docType, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.Sp_Litigation_CaseDocument(noticeInstanceID,CustomerID)
                                   select row).ToList();

                if (queryResult.Count > 0)
                {
                    if (docType != "")
                        queryResult = queryResult.Where(row => row.DocType.Trim() == docType).ToList();
                    else
                        queryResult = queryResult.Where(row => (row.DocType.Trim() == "N") || (row.DocType.Trim() == "NR") || (row.DocType.Trim() == "NT") || (row.DocType.Trim() == "CD")).ToList();
                }

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderBy(entry => entry.FileName).ThenByDescending(entry => entry.CreatedOn).ToList();

                return queryResult;
            }
        }


        public static tbl_LitigationFileData GetNoticeDocumentByID(int noticeFileID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_LitigationFileData file = entities.tbl_LitigationFileData.Where(x => x.ID == noticeFileID).FirstOrDefault();
                return file;
            }
        }

        public static bool DeleteNoticeDocument(int noticeFileID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    tbl_LitigationFileData fileToDelete = entities.tbl_LitigationFileData.Where(x => x.ID == noticeFileID).FirstOrDefault();

                    if (fileToDelete != null)
                    {
                        fileToDelete.IsDeleted = true;
                        fileToDelete.DeletedBy = deletedByUserID;
                        fileToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Notice-Lawyer
        public static bool CreateNoticeLawyerMapping(tbl_LegalNoticeLawyerMapping lstNoticeLawyer)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    lstNoticeLawyer.CreatedOn = DateTime.Now;
                    lstNoticeLawyer.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalNoticeLawyerMapping.Add(lstNoticeLawyer);

                    entities.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool UpdateNoticeLawyerMapping(tbl_LegalNoticeLawyerMapping lstNoticeLawyer)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    lstNoticeLawyer.CreatedOn = DateTime.Now;
                    lstNoticeLawyer.UpdatedOn = DateTime.Now;

                    var noticeLawyerMappingRecord = (from row in entities.tbl_LegalNoticeLawyerMapping
                                                     where row.NoticeInstanceID == lstNoticeLawyer.NoticeInstanceID
                                                     && row.LawyerID == lstNoticeLawyer.LawyerID
                                                     select row).FirstOrDefault();

                    if (noticeLawyerMappingRecord != null)
                    {
                        noticeLawyerMappingRecord.IsActive = true;
                        noticeLawyerMappingRecord.UpdatedOn = lstNoticeLawyer.UpdatedOn;
                        noticeLawyerMappingRecord.UpdatedBy = lstNoticeLawyer.UpdatedBy;
                    }
                    else
                        entities.tbl_LegalNoticeLawyerMapping.Add(lstNoticeLawyer);


                    entities.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DeActiveExistingNoticeLawyerMapping(long noticeInstanceID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var queryResult = (from row in entities.tbl_LegalNoticeLawyerMapping
                                       where row.NoticeInstanceID == noticeInstanceID
                                       && row.IsActive == true
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static tbl_LegalNoticeLawyerMapping GetNoticeLawyerMapping(int noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalNoticeLawyerMapping
                                   where row.NoticeInstanceID == noticeInstanceID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();
                return queryResult;
            }
        }

        #endregion

        #region Notice-UserAssignement

        public static List<int> GetLitigationAssignedRoles(int Userid)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstAssignedRoles = (from row in entities.tbl_LegalNoticeAssignment
                                        where row.UserID == Userid
                                        && row.IsActive == true
                                        select row.RoleID).Distinct().ToList();

                var lstCaseAssignedRoles = (from row in entities.tbl_LegalCaseAssignment
                                            where row.UserID == Userid
                                            && row.IsActive == true
                                            select row.RoleID).Distinct().ToList();

                return lstAssignedRoles.Union(lstCaseAssignedRoles).Distinct().ToList();
            }
        }

        public static List<SP_Litigation_NoticeCaseUserAssigned_Result> GetNoticeUserAssignments(int customerID, long noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.SP_Litigation_NoticeCaseUserAssigned(customerID, noticeInstanceID, "N")
                                   select row).ToList();

                return queryResult;
            }
        }

        public static bool CreateNoticeAssignment(tbl_LegalNoticeAssignment objNewAssignmentRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNewAssignmentRecord.CreatedOn = DateTime.Now;
                    objNewAssignmentRecord.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalNoticeAssignment.Add(objNewAssignmentRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistNoticeAssignment(tbl_LegalNoticeAssignment objNewAssignmentRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeAssignment
                                  where row.NoticeInstanceID == objNewAssignmentRecord.NoticeInstanceID
                                  && row.UserID == objNewAssignmentRecord.UserID
                                  && row.RoleID == objNewAssignmentRecord.RoleID
                                  //&& row.IsActive == true
                                  select row).FirstOrDefault();
                    if (record != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_LegalNoticeAssignment> GetNoticeAssignment(int noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalNoticeAssignment
                                   where row.NoticeInstanceID == noticeInstanceID
                                   && row.IsActive == true
                                   select row).ToList();
                return queryResult;
            }
        }

        public static bool DeActivenoticeAssignments(long caseInstanceID, int loggedInUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeAssignment
                                  where row.NoticeInstanceID == caseInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = loggedInUserID);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateNoticeAssignments(tbl_LegalNoticeAssignment objNewAssignmentRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeAssignment
                                  where row.NoticeInstanceID == objNewAssignmentRecord.NoticeInstanceID
                                  && row.UserID == objNewAssignmentRecord.UserID
                                  && row.RoleID == objNewAssignmentRecord.RoleID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = true);
                        record.ForEach(entry => entry.UpdatedBy = objNewAssignmentRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdateNoticeAssignment(tbl_LegalNoticeAssignment objNewAssignmentRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var prevAssignmentRecord = (from row in entities.tbl_LegalNoticeAssignment
                                                where row.NoticeInstanceID == objNewAssignmentRecord.NoticeInstanceID
                                                && row.UserID == objNewAssignmentRecord.UserID
                                                && row.RoleID == objNewAssignmentRecord.RoleID
                                                select row).FirstOrDefault();

                    if (prevAssignmentRecord != null)
                    {
                        prevAssignmentRecord.IsActive = true;
                        prevAssignmentRecord.UpdatedBy = objNewAssignmentRecord.CreatedBy;
                        prevAssignmentRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        objNewAssignmentRecord.CreatedOn = DateTime.Now;
                        objNewAssignmentRecord.UpdatedOn = DateTime.Now;

                        entities.tbl_LegalNoticeAssignment.Add(objNewAssignmentRecord);

                        saveSuccess = true;
                    }
                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteNoticeUserAssignment(int assignmentRecordID, long noticeInstanceID)
        {
            bool saveSuccess = false;
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var queryResult = (from row in entities.tbl_LegalNoticeAssignment
                                       where row.NoticeInstanceID == noticeInstanceID
                                       && row.ID == assignmentRecordID
                                       select row).FirstOrDefault();
                    if (queryResult != null)
                    {
                        queryResult.IsActive = false;
                        entities.SaveChanges();
                        saveSuccess = true;
                    }
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        #endregion

        #region Notice-Response
        public static List<tbl_LegalNoticeResponse> GetNoticeResponseDetails(int noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalNoticeResponse
                                   where row.NoticeInstanceID == noticeInstanceID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }

        public static long CreateNoticeResponseLog(tbl_LegalNoticeResponse objNextActionRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNextActionRecord.CreatedOn = DateTime.Now;

                    entities.tbl_LegalNoticeResponse.Add(objNextActionRecord);
                    entities.SaveChanges();

                    return objNextActionRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool DeleteNoticeResponseLog(int noticeResponseID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    List<tbl_LitigationFileData> ResponseRelatedDocToDelete = entities.tbl_LitigationFileData.Where(x => x.DocTypeInstanceID == noticeResponseID).ToList();

                    if (ResponseRelatedDocToDelete.Count > 0)
                    {
                        ResponseRelatedDocToDelete.ForEach(entry => entry.IsDeleted = true);
                        ResponseRelatedDocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        ResponseRelatedDocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    tbl_LegalNoticeResponse ResponseLogToDelete = entities.tbl_LegalNoticeResponse.Where(x => x.ID == noticeResponseID).FirstOrDefault();

                    if (ResponseLogToDelete != null)
                    {
                        ResponseLogToDelete.IsActive = false;
                        ResponseLogToDelete.DeletedBy = deletedByUserID;
                        ResponseLogToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_LitigationFileData> GetNoticeResponseDocuments(long noticeInstanceID, long responseID, string docType)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == noticeInstanceID
                                   && row.DocTypeInstanceID == responseID
                                   && row.DocType.Trim() == docType
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        #endregion

        #region Notice Status-Transaction

        public static bool CreateNoticeStatusTransaction(tbl_LegalNoticeStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    objNewStatusRecord.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalNoticeStatusTransaction.Add(objNewStatusRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistNoticeStatusTransaction(tbl_LegalNoticeStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeStatusTransaction
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.NoticeInstanceID == objNewStatusRecord.NoticeInstanceID
                                  && row.StatusID == objNewStatusRecord.StatusID
                                  //&& row.UserID == objNewStatusRecord.UserID
                                  //&& row.RoleID == objNewStatusRecord.RoleID
                                  select row).FirstOrDefault();
                    if (record != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateNoticeStatusTransaction(tbl_LegalNoticeStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeStatusTransaction
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.NoticeInstanceID == objNewStatusRecord.NoticeInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();

                        var recordToUpdate = record.Where(entry => entry.StatusID == objNewStatusRecord.StatusID).FirstOrDefault();

                        if (recordToUpdate != null)
                        {
                            recordToUpdate.IsActive = true;
                            entities.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActiveNoticeStatusTransaction(tbl_LegalNoticeStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeStatusTransaction
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.NoticeInstanceID == objNewStatusRecord.NoticeInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Notice Status

        public static bool CreateNoticeStatus(tbl_LegalNoticeStatus objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    objNewStatusRecord.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalNoticeStatus.Add(objNewStatusRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistNoticeStatus(tbl_LegalNoticeStatus objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeStatus
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.NoticeInstanceID == objNewStatusRecord.NoticeInstanceID
                                  //&& row.StatusID == objNewStatusRecord.StatusID
                                  select row).FirstOrDefault();
                    if (record != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateNoticeStatus(tbl_LegalNoticeStatus objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeStatus
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.NoticeInstanceID == objNewStatusRecord.NoticeInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();

                        var recordToUpdate = record.Where(entry => entry.StatusID == objNewStatusRecord.StatusID).FirstOrDefault();

                        if (recordToUpdate != null)
                        {
                            recordToUpdate.IsActive = true;
                            recordToUpdate.CloseDate = objNewStatusRecord.CloseDate;
                            recordToUpdate.ClosureRemark = objNewStatusRecord.ClosureRemark;

                            entities.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActiveNoticeStatus(tbl_LegalNoticeStatus objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeStatus
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.NoticeInstanceID == objNewStatusRecord.NoticeInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static View_NoticeStatusCloseRemark GetNoticeCurrentStatusDetails(int noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.View_NoticeStatusCloseRemark
                                   where row.NoticeInstanceID == noticeInstanceID && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        public static View_NoticeAssignedInstance GetNoticeStatusDetail(int noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.View_NoticeAssignedInstance
                                   where row.NoticeInstanceID == noticeInstanceID
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        #endregion

        public static tbl_NoticeCasePayment GetPaymentDetailsByID(int caseInstanceID, int PaymentID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_NoticeCasePayment
                                   where row.NoticeOrCaseInstanceID == caseInstanceID
                                   && row.ID == PaymentID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        #region Notice-Payment
        public static List<tbl_NoticeCasePayment> GetNoticeCasePaymentDetails(int noticeInstanceID, string noticeOrCase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_NoticeCasePayment
                                   where row.NoticeOrCaseInstanceID == noticeInstanceID
                                   && row.IsActive == true
                                   && row.NoticeOrCase == noticeOrCase
                                   select row).ToList();

                return queryResult;
            }
        }

        public static long UpdatePayment(tbl_NoticeCasePayment newRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {

                    var query = (from row in entities.tbl_NoticeCasePayment
                                 where row.ID == newRecord.ID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        query.IsActive = newRecord.IsActive;
                        query.PaymentDate = newRecord.PaymentDate;
                        query.PaymentID = newRecord.PaymentID;
                        query.Amount = newRecord.Amount;
                        query.AmountPaid = newRecord.AmountPaid;
                        query.AmountTax = newRecord.AmountTax;
                        query.Lawyer = newRecord.Lawyer;
                        query.UpdatedBy = newRecord.CreatedBy;
                        query.Remark = newRecord.Remark;
                        query.CreatedByText = newRecord.CreatedByText;
                        query.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        return query.ID;
                    }
                    else
                        return 0;

                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        //public static long UpdatePayment(tbl_NoticeCasePayment newRecord)
        //{
        //    try
        //    {
        //        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //        {

        //            var query = (from row in entities.tbl_NoticeCasePayment
        //                         where row.ID == newRecord.ID
        //                         select row).FirstOrDefault();
        //            if (query != null)
        //            {
        //                query.IsActive = newRecord.IsActive;
        //                query.PaymentDate = newRecord.PaymentDate;
        //                query.PaymentID = newRecord.PaymentID;
        //                query.Amount = newRecord.Amount;
        //                query.AmountPaid = newRecord.AmountPaid;
        //                query.Lawyer = newRecord.Lawyer;
        //                query.UpdatedBy = newRecord.CreatedBy;
        //                query.Remark = newRecord.Remark;
        //                query.CreatedByText = newRecord.CreatedByText;
        //                query.UpdatedOn = DateTime.Now;
        //                entities.SaveChanges();
        //                return query.ID;
        //            }
        //            else
        //                return 0;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return 0;
        //    }
        //}
        public static bool CreateNoticePaymentLog(tbl_NoticeCasePayment objPaymentRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objPaymentRecord.CreatedOn = DateTime.Now;

                    entities.tbl_NoticeCasePayment.Add(objPaymentRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteNoticePaymentLog(int noticePaymentID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    tbl_NoticeCasePayment PaymentLogToDelete = entities.tbl_NoticeCasePayment.Where(x => x.ID == noticePaymentID).FirstOrDefault();

                    if (PaymentLogToDelete != null)
                    {
                        PaymentLogToDelete.IsActive = false;
                        PaymentLogToDelete.UpdatedBy = deletedByUserID;
                        PaymentLogToDelete.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Notice-Task
        //public static List<View_NoticeCaseTaskDetail> GetNoticeTaskDetails(int noticeInstanceID)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var queryResult = (from row in entities.View_NoticeCaseTaskDetail
        //                           where row.NoticeCaseInstanceID == noticeInstanceID
        //                           && row.IsActive == true
        //                           select row).ToList();

        //        return queryResult;
        //    }
        //}

        //public static tbl_TaskScheduleOn GetNoticeTaskDetailByTaskID(int noticeInstanceID, int taskID, string taskType)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var queryResult = (from row in entities.tbl_TaskScheduleOn
        //                           where row.NoticeCaseInstanceID == noticeInstanceID
        //                           && row.ID == taskID
        //                           && row.TaskType== taskType
        //                           && row.IsActive == true
        //                           select row).FirstOrDefault();
        //        return queryResult;
        //    }
        //}

        //public static View_NoticeCaseTaskDetail GetNoticeTaskDetails(int noticeInstanceID, int taskID)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var queryResult = (from row in entities.View_NoticeCaseTaskDetail
        //                           where row.NoticeCaseInstanceID == noticeInstanceID
        //                           && row.TaskID==taskID
        //                           && row.IsActive == true
        //                           select row).FirstOrDefault();

        //        return queryResult;
        //    }
        //}

        //public static bool CreateNoticeTask(tbl_TaskScheduleOn objNextActionRecord)
        //{
        //    try
        //    {
        //        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //        {
        //            objNextActionRecord.CreatedOn = DateTime.Now;

        //            entities.tbl_TaskScheduleOn.Add(objNextActionRecord);
        //            entities.SaveChanges();

        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static bool ExistNoticeTaskTitle(string taskTitle)
        //{
        //    try
        //    {
        //        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //        {
        //            tbl_TaskScheduleOn taskDetail = entities.tbl_TaskScheduleOn
        //                .Where(x => x.TaskTitle.Trim().ToUpper() == taskTitle.Trim().ToUpper()).FirstOrDefault();

        //            if (taskDetail != null)
        //                return true;
        //            else
        //                return false;                    
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static bool UpdateTaskAccessURL(long taskID, tbl_TaskScheduleOn taskRecord)
        //{
        //    try
        //    {
        //        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //        {
        //            tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID).FirstOrDefault();

        //            if (taskToUpdate != null)
        //            {
        //                taskToUpdate.AccessURL = taskRecord.AccessURL;
        //                taskToUpdate.UpdatedBy = taskRecord.UpdatedBy;
        //                taskToUpdate.UpdatedOn = DateTime.Now;

        //                entities.SaveChanges();

        //                return true;
        //            }
        //            else
        //                return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static bool UpdateTaskStatus(long taskID, int statusID, int loggedInUserID)
        //{
        //    try
        //    {
        //        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //        {
        //            tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID).FirstOrDefault();

        //            if (taskToUpdate != null)
        //            {
        //                taskToUpdate.StatusID = statusID;
        //                taskToUpdate.UpdatedBy = loggedInUserID;
        //                taskToUpdate.UpdatedOn = DateTime.Now;

        //                entities.SaveChanges();

        //                return true;
        //            }
        //            else
        //                return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static bool ExpiredTaskURL(long taskID, int loggedInUserID)
        //{
        //    try
        //    {
        //        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //        {
        //            tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID).FirstOrDefault();

        //            if (taskToUpdate != null)
        //            {
        //                taskToUpdate.URLExpired = true;
        //                taskToUpdate.UpdatedBy = loggedInUserID;
        //                taskToUpdate.UpdatedOn = DateTime.Now;

        //                entities.SaveChanges();

        //                return true;
        //            }
        //            else
        //                return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public static bool DeleteNoticeTask(int taskID, int deletedByUserID)
        //{
        //    try
        //    {
        //        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //        {
        //            tbl_TaskScheduleOn ActionLogToDelete = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID).FirstOrDefault();

        //            if (ActionLogToDelete != null)
        //            {
        //                ActionLogToDelete.IsActive = false;
        //                ActionLogToDelete.DeletedBy = deletedByUserID;
        //                ActionLogToDelete.DeletedOn = DateTime.Now;

        //                entities.SaveChanges();

        //                return true;
        //            }
        //            else
        //                return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        #endregion

        #region Notice-Report
        //public static List<View_LitigationNoticeReport> GetGridPopupNoticeData(int NoticeCaseInstanceID)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var AllDocument = (from row in entities.View_LitigationNoticeReport
        //                           where row.NoticeCaseInstanceID == NoticeCaseInstanceID
        //                           select row).ToList();
        //        return AllDocument;
        //    }
        //}
        #endregion


        //public static long LegalNoticeInstanceID(string RefNo)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var query = (from row in entities.tbl_LegalNoticeInstance
        //                     where row.IsDeleted == false
        //                     && row.RefNo.ToUpper().Trim().Equals(RefNo.ToUpper().Trim())
        //                     select row.ID).FirstOrDefault();

        //        return query;
        //    }
        //}

        public static long GetNoticeInstanceIDByRefNo(string RefNo)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalNoticeInstance
                             where row.IsDeleted == false
                             && row.RefNo.ToUpper().Trim().Equals(RefNo.ToUpper().Trim())
                             select row.ID).FirstOrDefault();

                return query;
            }
        }
        public static bool ExistsRefNo(string RefNo)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalNoticeInstance
                             where row.RefNo.ToUpper().Trim().Equals(RefNo.ToUpper().Trim())
                             && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static bool ClosedDateCheck(string RefNo,string ResponseDate)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalNoticeInstance
                             where row.RefNo.ToUpper().Trim().Equals(RefNo.ToUpper().Trim())
                             && row.IsDeleted == false
                             select row.ID).FirstOrDefault();

                var query2= (from row in entities.tbl_LegalNoticeStatus
                             where row.NoticeInstanceID== query
                             && row.IsDeleted == false
                             select row.CloseDate).FirstOrDefault();
                var Response_Date = DateTime.Parse(ResponseDate).Date;

                if (query2.HasValue)
                {
                    var NoticeCloseDate = query2.Value.Date;


                    if ( Response_Date <= NoticeCloseDate)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public static tbl_LegalNoticeResponse GetNoticeResponseDetailsByID(int responseID, int noticeInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalNoticeResponse
                                   where row.NoticeInstanceID == noticeInstanceID
                                   && row.ID == responseID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        public static bool DeleteResponseEditDocument(int ID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LitigationFileData
                                   where row.ID == ID
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    QueryResult.IsDeleted = true;
                    Entities.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static tbl_LitigationFileData ShowandDownloadFileById(int ID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LitigationFileData
                                   where row.ID == ID
                                   select row).FirstOrDefault();

                return QueryResult;
            }
        }

        public static long UpdateNoticeResponse(tbl_LegalNoticeResponse newRecord)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LegalNoticeResponse
                                   where row.ID == newRecord.ID
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    QueryResult.IsActive = newRecord.IsActive;
                    QueryResult.NoticeInstanceID = newRecord.NoticeInstanceID;
                    QueryResult.Description = newRecord.Description;
                    QueryResult.ResponseDate = newRecord.ResponseDate;
                    QueryResult.RespondedBy = newRecord.RespondedBy;
                    QueryResult.ResponseThrough = newRecord.ResponseThrough;
                    QueryResult.ResponseRefNo = newRecord.ResponseRefNo;
                    QueryResult.ResponseType = newRecord.ResponseType;
                    QueryResult.UpdatedBy = newRecord.CreatedBy;
                    QueryResult.CreatedByText = newRecord.CreatedByText;
                    QueryResult.UpdatedOn = DateTime.Now;
                    QueryResult.UserID = newRecord.UserID;
                    QueryResult.Remark = newRecord.Remark;
                    Entities.SaveChanges();
                }
                return QueryResult.ID;

            }
        }

        public static tbl_TaskScheduleOn GetNoticeTaskDetailByTaskID(int noticeInstanceID, int taskID, string taskType, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_TaskScheduleOn
                                   where row.NoticeCaseInstanceID == noticeInstanceID
                                   && row.CustomerID == CustomerID
                                   && row.ID == taskID
                                   && row.TaskType == taskType
                                   && row.IsActive == true
                                   select row).FirstOrDefault();
                return queryResult;
            }
        }

        // After 31 May
        public static List<string> getNoticeOwnerAndInternalUser(int NoticeInstanceID, long customerID)
        {

            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var user = (from row in entities.SP_GetNoticeOwnerAndInternalUserMail(Convert.ToInt32(customerID), NoticeInstanceID)
                            select row).ToList();

                return user;
            }
        }

        public static List<int> GetAllAssingedUserList(long newNoticeID, int userID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeAssignment
                                  where row.NoticeInstanceID == newNoticeID &&
                                  row.IsActive == true
                                  select row.UserID).ToList();

                    return record;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string GetTaskType(int taskID ,long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_TaskScheduleOn
                                   where row.CustomerID == CustomerID
                                   && row.ID == taskID
                                   && row.IsActive == true
                                   select row.TaskType).FirstOrDefault();
                return queryResult;
            }
        }
    }
}
