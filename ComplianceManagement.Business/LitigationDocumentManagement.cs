﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class LitigationDocumentManagement
    {
        public static List<sp_LitigationNoticeReportWithCustomParameter_Result> GetNoticeReportData(long customerID, int loggedInUserID, string loggedInUserRole, int RoleID, List<int> branchList, int partyID, int deptID, int caseStatus, string caseType,string Financialyear)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(customerID))
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.Title,
                                 g.OpenDate,
                                 g.CloseDate,
                                 g.Status,
                                 g.CustomerID,
                                 g.BranchName,
                                 g.TypeCaseNotice,
                                 g.NoticeCategoryID,
                                 g.Category,
                                 g.Department,
                                 g.StatusID,
                                 g.CustBrachID,
                                 g.DepartmentID,
                                 g.CustomerBranchID,
                                 g.NoticeType,
                                 g.NoticeCaseDesc,
                                 g.RefNo,
                                 g.TypeName,
                                 g.Act,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedBy,
                                 g.FYName,
                                 
                             } into GCS
                             select new sp_LitigationNoticeReportWithCustomParameter_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 Title = GCS.Key.Title,
                                 OpenDate = GCS.Key.OpenDate,
                                 CloseDate = GCS.Key.CloseDate,
                                 Status = GCS.Key.Status,
                                 CustomerID = GCS.Key.CustomerID,
                                 BranchName = GCS.Key.BranchName,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 Category = GCS.Key.Category,
                                 Department = GCS.Key.Department,
                                 StatusID = GCS.Key.StatusID,
                                 CustBrachID = GCS.Key.CustBrachID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
                                 RefNo = GCS.Key.RefNo,
                                 TypeName = GCS.Key.TypeName,
                                 Act = GCS.Key.Act,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedBy = GCS.Key.CreatedBy,
                                 FYName = GCS.Key.FYName,
                             }).ToList();
                }

                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();
                if (caseStatus != -1)
                {
                    query = query.Where(entry => entry.StatusID == caseStatus).ToList();
                }
                //if (caseStatus == 3) //3--Closed otherwise Open
                //    query = query.Where(entry => entry.StatusID == caseStatus).ToList();
                //else
                //    query = query.Where(entry => entry.StatusID < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();

                if (caseType != "" && caseType != "B")//B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();
                if (Financialyear != "" && Financialyear != "-1")
                {
                    query = query.Where(entry => entry.FYName != "" && entry.FYName != null).ToList();
                    string a = Financialyear + "" + ",";
                    query = query.Where(aa => aa.FYName.Contains(a)).ToList();
                    //query = query.Where(entry => entry.FYName==Financialyear).ToList();

                    //query = query.Where(entry => Financialyear.Contains(entry.FYName)).ToList();
                }
                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query;
            }
        }

        public static List<SP_LitigationCaseReportWithCustomParameter_Result> GetAllCaseReportData(long customerID, int loggedInUserID, string loggedInUserRole, int RoleID, List<int> branchList, int partyID, int deptID, int caseStatus, string caseType,string FYName)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.SP_LitigationCaseReportWithCustomParameter(Convert.ToInt32(customerID))
                             select row).ToList();
              //  string dlFY = string.Empty;
                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssingedUserID == loggedInUserID && entry.RoleID == 3) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.Title,
                                 g.OpenDate,
                                 g.CloseDate,
                                 g.Status,
                                 g.CustomerID,
                                 g.BranchName,
                                 g.TypeCaseNotice,
                                 g.CaseCategoryID,
                                 g.Category,
                                 g.Department,
                                 g.TxnStatusID,
                                 g.CustBrachID,
                                 g.DepartmentID,
                                 g.CustomerBranchID,
                                 g.CaseType,
                                 g.NoticeCaseDesc,
                                 g.CaseRefNo,
                                 g.TypeName,
                                 g.Act,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedBy,
                                 g.FYName,
                                 
                             } into GCS
                             select new SP_LitigationCaseReportWithCustomParameter_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 Title = GCS.Key.Title,
                                 OpenDate = GCS.Key.OpenDate,
                                 CloseDate = GCS.Key.CloseDate,
                                 Status = GCS.Key.Status,
                                 CustomerID = GCS.Key.CustomerID,
                                 BranchName = GCS.Key.BranchName,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 CaseCategoryID = GCS.Key.CaseCategoryID,
                                 Category = GCS.Key.Category,
                                 Department = GCS.Key.Department,
                                 TxnStatusID = GCS.Key.TxnStatusID,
                                 CustBrachID = GCS.Key.CustBrachID,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 CaseType = GCS.Key.CaseType,
                                 NoticeCaseDesc = GCS.Key.NoticeCaseDesc,
                                 CaseRefNo = GCS.Key.CaseRefNo,
                                 TypeName = GCS.Key.TypeName,
                                 Act = GCS.Key.Act,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedBy = GCS.Key.CreatedBy,
                                 FYName = GCS.Key.FYName,
                             }).ToList();
                }

                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                //if (caseStatus == 3) //3--Closed otherwise Open
                //    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                //else
                //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();

                if (caseType != "" && caseType != "B") //B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (FYName != "" && FYName != "-1")
                {
                    query = query.Where(entry => entry.FYName != "" && entry.FYName != null).ToList();
                    string a = FYName + "" + ",";
                    query = query.Where(aa => aa.FYName.Contains(a)).ToList();
                    //query = query.Where(entry => entry.FYName == FYName).ToList();
                }
                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query;
            }
        }
        public static List<SP_LitigationNoticeDocumentfor_Tag_Result> GetAllDocumentListofNotice(int customerID, int loggedInUserID, string loggedInUserRole, int RoleID, List<int> branchList, int partyID, int deptID, int caseStatus, string caseType, List<string> lstSelectedFileTags,string FileName,string dlFY)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {

                var query = (from row in entities.SP_LitigationNoticeDocumentfor_Tag(customerID)
                             select row).ToList();
                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN
                {
                    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                }
                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (caseStatus == 3) //3--Closed otherwise Open
                    query = query.Where(entry => entry.Status == caseStatus).ToList();
                else
                    query = query.Where(entry => entry.Status < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();

                if (caseType != "" && caseType != "All")//B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }
                if (lstSelectedFileTags.Count > 0)
                {
                    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                }
                if (FileName!=null)
                {
                    query = (from row in query where row.DocumentName != null select row).ToList();
                    query = query.Where(Entry => Entry.DocumentName.ToLower().Contains(FileName.ToLower())).ToList();
                }
                if (dlFY != "" && dlFY != "-1")
                {
                    query = query.Where(entry => entry.FYName.Contains(dlFY)).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefNo,
                                 g.TypeCaseNotice,
                                 g.TypeName,
                                 g.Title,
                                 g.Description,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.Status,
                                 g.DocumentCount,
                                 g.FYName,
                                 //g.Filetag
                             } into GCS
                             select new SP_LitigationNoticeDocumentfor_Tag_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 TypeName = GCS.Key.TypeName,
                                 Title = GCS.Key.Title,
                                 Description = GCS.Key.Description,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 Status = GCS.Key.Status,
                                 DocumentCount = GCS.Key.DocumentCount,
                                 FYName = GCS.Key.FYName,
                                 //Filetag = GCS.Key.Filetag
                             }).ToList();
                }
                return query;
            }
        }

        public static List<SP_LitigationCaseDocumentfor_tag_Result> GetAllDocumentListofCase(long customerID, int loggedInUserID, string loggedInUserRole, int RoleID, string caseType, /*List<string> lstSelectedFileTags,*/ string FileName, int status, string TagFilter)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.SP_LitigationCaseDocumentfor_tag(customerID)
                             select row).ToList();
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 2);
                        if (caseList.Count > 0)
                        {
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        }
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN
                    {
                        query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                    }
                }

                //if (branchList.Count > 0)
                //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();
                if (status != -1)
                {
                    if (status == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.Status == status).ToList();
                    else
                        query = query.Where(entry => entry.Status < 3).ToList();
                }
                if (caseType != "" && caseType != "All") //B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }
                if (TagFilter != null && TagFilter != "")
                {
                    System.Collections.Generic.IList<string> lstSelectedFileTags = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(TagFilter);
                    if (lstSelectedFileTags.Count != 0)
                    {
                        query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                        //objTaskCompDocument = objTaskCompDocument.Where(x => Ids.Contains(x.CustomerBranchID)).ToList();
                    }
                }
                //if (lstSelectedFileTags.Count > 0)
                //{
                //    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                //}
                //if (!(string.IsNullOrEmpty(FileName)))
                {
                    query = query.Where(Entry => Entry.FileName.ToLower().Contains(FileName.ToLower())).ToList();

                }
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefNo,
                                 //g.FileName,
                                 g.TypeCaseNotice,
                                 g.TypeName,
                                 g.Title,
                                 g.Description,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.Status,
                                 g.DocumentCount,
                                 g.FYName
                             } into GCS
                             select new SP_LitigationCaseDocumentfor_tag_Result()
                             {
                                 //FileName=GCS.Key.FileName,
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 TypeName = GCS.Key.TypeName,
                                 Title = GCS.Key.Title,
                                 Description = GCS.Key.Description,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 Status = GCS.Key.Status,
                                 DocumentCount = GCS.Key.DocumentCount,
                                 FYName = GCS.Key.FYName
                             }).ToList();
                }
                return query;
            }

        }
 public static List<SP_LitigationNoticeDocumentfor_Tag_Result> GetAllDocumentListofNotice(int customerID, int loggedInUserID, string loggedInUserRole, int RoleID, string caseType, string FileName, int status, string TagFilter)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {

                var query = (from row in entities.SP_LitigationNoticeDocumentfor_Tag(customerID)
                             select row).ToList();
                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 1);
                        if (caseList.Count > 0)
                        {
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.NoticeCaseInstanceID))).ToList();
                        }
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN
                    {
                        query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                    }
                }


                //if (branchList.Count > 0)
                //    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                //if (partyID != -1)
                //    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                //if (deptID != -1)
                //    query = query.Where(entry => entry.DepartmentID == deptID).ToList();
                if (status != -1)
                {
                    if (status == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.Status == status).ToList();
                    else
                        query = query.Where(entry => entry.Status < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();
                }
                if (caseType != "" && caseType != "All")//B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }


                if (TagFilter != null && TagFilter != "")
                {
                    System.Collections.Generic.IList<string> lstSelectedFileTags = new JavaScriptSerializer().Deserialize<System.Collections.Generic.IList<string>>(TagFilter);
                    if (lstSelectedFileTags.Count != 0)
                    {
                        query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                    }
                }
                //if (lstSelectedFileTags.Count > 0)
                //{
                //    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                //}
                if (FileName != null)
                {
                    query = (from row in query where row.DocumentName != null select row).ToList();
                    query = query.Where(Entry => Entry.DocumentName.ToLower().Contains(FileName.ToLower())).ToList();
                }
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefNo,
                                 g.TypeCaseNotice,
                                 g.TypeName,
                                 g.Title,
                                 g.Description,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.Status,
                                 g.DocumentCount,
                                 g.FYName
                                 //g.Filetag
                             } into GCS
                             select new SP_LitigationNoticeDocumentfor_Tag_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 TypeName = GCS.Key.TypeName,
                                 Title = GCS.Key.Title,
                                 Description = GCS.Key.Description,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 Status = GCS.Key.Status,
                                 DocumentCount = GCS.Key.DocumentCount,
                                 FYName = GCS.Key.FYName
                             }).ToList();
                }
                return query;
            }
        }



        public static List<SP_LitigationCaseDocumentfor_tag_Result> GetAllDocumentListofCase(long customerID, int loggedInUserID, string loggedInUserRole, int RoleID, List<int> branchList, int partyID, int deptID, int caseStatus, string caseType, List<string> lstSelectedFileTags, string FileName, string dlFY)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.Database.CommandTimeout = 180;
                var query = (from row in entities.SP_LitigationCaseDocumentfor_tag(customerID)
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN
                {
                    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                }
                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (caseStatus == 3) //3--Closed otherwise Open
                    query = query.Where(entry => entry.Status == caseStatus).ToList();
                else
                    query = query.Where(entry => entry.Status < 3).ToList();

                if (caseType != "" && caseType != "All") //B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }
                if (lstSelectedFileTags.Count > 0)
                {
                    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                }
                if (!(string.IsNullOrEmpty(FileName)))
                {
                    query = query.Where(Entry => Entry.FileName.ToLower().Contains(FileName.ToLower())).ToList();

                }
                //if (dlFY != "" && dlFY != "-1")
                //{
                //    query = query.Where(entry => entry.FYName == dlFY).ToList();
                //}
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefNo,
                                 //g.FileName,
                                 g.TypeCaseNotice,
                                 g.TypeName,
                                 g.Title,
                                 g.Description,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.Status,
                                 g.DocumentCount,
                                 g.FYName,
                                 //g.Filetag
                             } into GCS
                             select new SP_LitigationCaseDocumentfor_tag_Result()
                             {
                                 //FileName=GCS.Key.FileName,
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 TypeName = GCS.Key.TypeName,
                                 Title = GCS.Key.Title,
                                 Description = GCS.Key.Description,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 Status = GCS.Key.Status,
                                 DocumentCount = GCS.Key.DocumentCount,
                                 FYName = GCS.Key.FYName,
                                 //Filetag = GCS.Key.Filetag
                             }).ToList();
                }
                return query;
            }
        }

        public static List<View_LitigationCaseDocument> GetAllDocumentListofNoticeAndCase()
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var AllDocument = (from row in entities.View_LitigationCaseDocument
                                   select row)
                                   .ToList();
                return AllDocument;
            }
        }
        public static List<tbl_LitigationFileData> GetDocuments(long v, List<string> doctypes)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var AllDocument = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == v && doctypes.Contains(row.DocType.Trim())
                                   && row.IsDeleted == false
                                   select row).ToList();

                return AllDocument;
            }
        }
        public static List<tbl_LitigationFileData> GetTaskRelatedDoc(int TaskID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var AllDocument = (from row in entities.tbl_LitigationFileData
                                   where row.DocTypeInstanceID == TaskID && row.DocType == "T"
                                   select row).ToList();

                return AllDocument;
            }
        }
        //Added By Ruchi 
        public static List<string> GetDistinctFileTags(int customerID, long litigationInstanceID)
        {
            try
            {
                List<string> lstFileTags = new List<string>();
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    if (litigationInstanceID != 0)
                    {
                        lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                       join row1 in entities.tbl_LitigationFileData
                                       on row.FileID equals row1.ID
                                       where row1.NoticeCaseInstanceID == litigationInstanceID
                                       && row.CustomerID == customerID
                                        && row1.IsDeleted == false
                                       && row.IsActive == true
                                       select row.FileTag.Trim()).Distinct().ToList();
                    }
                    else
                    {
                        lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                       join row1 in entities.tbl_LitigationFileData
                                       on row.FileID equals row1.ID
                                       where row1.IsDeleted == false
                                       && row.CustomerID == customerID
                                       && row.IsActive == true
                                       select row.FileTag.Trim()).Distinct().ToList();
                    }

                    return lstFileTags;
                }
            }
            catch (Exception ex)
            {
                /*LogMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);*/
                return new List<string>();
            }

        }
        public static List<string> GetDistinctFileTagsNotice(int customerID, long litigationInstanceID)
        {
            try
            {
                List<string> lstFileTags = new List<string>();
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    if (litigationInstanceID != 0)
                    {
                        lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                       join row1 in entities.tbl_LitigationFileData
                                       on row.FileID equals row1.ID
                                       where row1.NoticeCaseInstanceID == litigationInstanceID
                                       && (row1.DocType.Contains("N") || row1.DocType.Contains("NT") || row1.DocType.Contains("NR"))
                                       && row1.IsDeleted == false
                                       && row.CustomerID == customerID
                                       && row.IsActive == true
                                       select row.FileTag.Trim()).Distinct().ToList();
                    }
                    else
                    {
                        lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                       join row1 in entities.tbl_LitigationFileData
                                       on row.FileID equals row1.ID
                                       where
                                       (row1.DocType.Contains("N") || row1.DocType.Contains("NT") || row1.DocType.Contains("NR"))
                                       && row1.IsDeleted == false
                                       && row.CustomerID == customerID
                                       && row.IsActive == true
                                       select row.FileTag.Trim()).Distinct().ToList();
                    }
                    return lstFileTags;
                }
            }
            catch (Exception ex)
            {
                /*LogMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);*/
                return new List<string>();
            }

        }

        public static List<sp_LitigationNoticeFileTag_Result> GetDistinctFileTagsNoticeMyDocument(int customerID, long litigationInstanceID, long UserID, string Role)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.sp_LitigationNoticeFileTag(customerID)
                             select row).Distinct().ToList();

                if (Role != "MGMT" && Role != "CADMN")
                    Query = Query.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID) || (entry.CreatedBy == UserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    Query = Query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (Query.Count > 0)
                {
                    Query = (from g in Query
                             group g by new
                             {
                                 g.FileTag
                             } into GCS
                             select new sp_LitigationNoticeFileTag_Result()
                             {
                                 FileTag = GCS.Key.FileTag
                             }).ToList();
                }
                return Query;
            }
        }

        public static List<sp_LitigationCaseFileTag_Result> GetDistinctFileTagsCaseMyDocument(int customerID, long litigationInstanceID, long UserID, string Role)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.sp_LitigationCaseFileTag(customerID)
                             select row).Distinct().ToList();

                if (Role != "MGMT" && Role != "CADMN")
                    Query = Query.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID) || (entry.CreatedBy == UserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    Query = Query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (Query.Count > 0)
                {
                    Query = (from g in Query
                             group g by new
                             {
                                 g.FileTag
                             } into GCS
                             select new sp_LitigationCaseFileTag_Result()
                             {
                                 FileTag = GCS.Key.FileTag
                             }).ToList();
                }
                return Query;
            }
        }

        public static List<string> GetDistinctFileTagsCase(int customerID, long litigationInstanceID)
        {
            try
            {
                List<string> lstFileTags = new List<string>();
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    if (litigationInstanceID != 0)
                    {
                        lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                       join row1 in entities.tbl_LitigationFileData
                                       on row.FileID equals row1.ID
                                       where row1.NoticeCaseInstanceID == litigationInstanceID
                                      && (row1.DocType.Contains("C") || row1.DocType.Contains("CT") || row1.DocType.Contains("CH") || row1.DocType.Contains("CO"))
                                       && row1.IsDeleted == false
                                       && row.CustomerID == customerID
                                       && row.IsActive == true
                                       select row.FileTag.Trim()).Distinct().ToList();
                    }
                    else
                    {
                        lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                       join row1 in entities.tbl_LitigationFileData
                                       on row.FileID equals row1.ID
                                       where row1.IsDeleted == false
                                       && row.CustomerID == customerID
                                       && (row1.DocType.Contains("C") || row1.DocType.Contains("CT") || row1.DocType.Contains("CH") || row1.DocType.Contains("CO"))
                                       && row.IsActive == true
                                       select row.FileTag.Trim()).Distinct().ToList();
                    }

                    return lstFileTags;
                }
            }
            catch (Exception ex)
            {
                /*LogMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);*/
                return new List<string>();
            }

        }
        //Added By Ruchi 
        public static List<Sp_Litigation_CaseDocument_Result> Sp_Litigation_CaseDocument_Result(int caseInstanceID, List<string> lstSelectedFileTags, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = entities.Sp_Litigation_CaseDocument(caseInstanceID, CustomerID).ToList();

                if (lstSelectedFileTags.Count > 0)
                    queryResult = queryResult.Where(Entry => lstSelectedFileTags.Contains(Entry.FileTag)).ToList();

                if (queryResult.Count > 0)
                {
                    queryResult = (from g in queryResult
                                   group g by new
                                   {
                                       g.ID,
                                       g.DocType,
                                       g.FileName,
                                       g.Version,
                                       g.CreatedByText,
                                       g.CreatedOn,
                                       g.TypeName,
                                       g.FinancialYear
                                      
                                   } into GCS
                                   select new Sp_Litigation_CaseDocument_Result()
                                   {
                                       ID = GCS.Key.ID,
                                       DocType = GCS.Key.DocType,
                                       FileName = GCS.Key.FileName,
                                       Version = GCS.Key.Version,
                                       CreatedByText = GCS.Key.CreatedByText,
                                       CreatedOn = GCS.Key.CreatedOn,
                                       TypeName= GCS.Key.TypeName,
                                       FinancialYear = GCS.Key.FinancialYear
                                   }).ToList();
                }

                return queryResult;
            }
        }

        public static List<string> GetDistinctFileTagsTasks(int customerID, int litigationInstanceID)
        {

            try
            {
                List<string> lstFileTags = new List<string>();
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    if (litigationInstanceID != 0)
                    {
                        lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                       join row1 in entities.tbl_LitigationFileData
                                       on row.FileID equals row1.ID
                                       where row1.NoticeCaseInstanceID == litigationInstanceID
                                      && (row1.DocType.Contains("T"))
                                       && row1.IsDeleted == false
                                       && row.CustomerID == customerID
                                       && row.IsActive == true
                                       select row.FileTag.Trim()).Distinct().ToList();
                    }
                    else
                    {
                        lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                       join row1 in entities.tbl_LitigationFileData
                                       on row.FileID equals row1.ID
                                       where row1.IsDeleted == false
                                       && (row1.DocType.Contains("T"))
                                       && row.CustomerID == customerID
                                       && row.IsActive == true
                                       select row.FileTag.Trim()).Distinct().ToList();
                    }

                    return lstFileTags;
                }
            }
            catch (Exception ex)
            {
                /*LogMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);*/
                return new List<string>();
            }

        }

        //public static object GetDocumentList(int customerID, int userID, string role, string docsearchvalue)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}


        //Added By Ruchi 
    }
}
