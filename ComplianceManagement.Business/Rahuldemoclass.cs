﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class Rahuldemoclass
    {
       
        public ComplianceDashboardSummaryView UserID { get; set; }
        public User FirstName { get; set; }
        public User LastName { get; set; }       
        public ComplianceDashboardSummaryView ShortDescription { get; set; }
        public ComplianceDashboardSummaryView ScheduledOn { get; set; }
        public ComplianceDashboardSummaryView ComplianceInstanceID { get; set; }
        public ComplianceDashboardSummaryView CustomerBranchID { get; set; }
        public ComplianceDashboardSummaryView Branch { get; set; }
        public ComplianceDashboardSummaryView ForMonth { get; set; }
        public ComplianceDashboardSummaryView Status { get; set; }
        public ComplianceDashboardSummaryView RiskCategory { get; set; }
        public ComplianceDashboardSummaryView Risk { get; set; }
        public ComplianceDashboardSummaryView RoleID { get; set; }
        public ComplianceDashboardSummaryView ScheduledOnID { get; set; }
        public ComplianceDashboardSummaryView ComplianceStatusID { get; set; }

    }
}
