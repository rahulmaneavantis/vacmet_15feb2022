﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class MileStonedetail
    {
        public long ID { get; set; }
        public long ContractTemplateID { get; set; }
        public long CustomerID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long DeptID { get; set; }
        public long UserID { get; set; }
        public System.DateTime DueDate { get; set; }
        public long StatusID { get; set; }
        public string Remark { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<long> UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public long MilestoneMasterID { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public string ForPeriod { get; set; }
    }
    public class ContractTaskManagement
    {
        public static List<SP_GetContract_StatusMaster_Result> GetStatusListAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.SP_GetContract_StatusMaster("3")
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<Cont_SP_GetApprovedTemplates_Result> GetApprovedTemplateList(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.Cont_SP_GetApprovedTemplates(CustomerID)
                                  select row).ToList();

                return statusList;
            }
        }
        public static List<Cont_SP_AssignMilestoneDetail_Result> GetContractMileStone(int customerID, long contractID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = entities.Cont_SP_AssignMilestoneDetail(contractID, customerID, UserID).ToList();
                return queryResult;
            }
        }

        #region Contract Status
        public static string ShowTaskType(int taskType)
        {
            try
            {
                if (taskType != 0)
                {
                    if (taskType == 4)
                        return "Review";
                    else if (taskType == 6)
                        return "Approval";
                    else if (taskType == 1)
                        return "Other";
                    else
                        return string.Empty;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public static List<Cont_tbl_StatusMaster> GetStatusList_All()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.Cont_tbl_StatusMaster
                                  where row.IsDeleted == false
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<Cont_tbl_StatusMaster> GetStatusList_All(bool isForTask, bool isVisibleToUser)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.Cont_tbl_StatusMaster
                                  where row.IsDeleted == false
                                  && row.IsForTask== isForTask                                  
                                  select row).ToList();

                if (!isVisibleToUser)
                    statusList = statusList.Where(row => row.IsVisibleToUser != isVisibleToUser).ToList();

                return statusList;
            }
        }

        public static List<Cont_tbl_StatusTransition> GetListStatusTransition()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transitionList = (from row in entities.Cont_tbl_StatusTransition
                                      select row).ToList();

                return transitionList;
            }
        }

        public static List<Cont_tbl_StatusTransition> GetStatusTransitionListByInitialID(int initialID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transitionList = (from row in entities.Cont_tbl_StatusTransition
                                      select row).ToList();

                if (initialID != 0)
                {
                    transitionList = transitionList.Where(row => row.InitialStateID == initialID).ToList();
                }

                return transitionList.ToList();
            }
        }

        public static List<int> GetFinalStatusByInitialID(int initialID, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transitionList = (from row in entities.Cont_tbl_StatusTransition
                                      select row).ToList();

                if (initialID != 0)
                {
                    transitionList = transitionList.Where(row => row.InitialStateID == initialID).ToList();
                }

                if (roleID != 0)
                {
                    transitionList = transitionList.Where(row => row.RoleID == roleID).ToList();
                }

                return transitionList.Select(entry => entry.FinalStateID).ToList();
            }
        }
        
        #endregion


        #region Contract-Task

        public static bool CreateTaskDocumentMapping(Cont_tbl_TaskDocumentMapping newDoc)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_TaskDocumentMapping.Add(newDoc);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_TaskDocumentMapping(List<Cont_tbl_TaskDocumentMapping> lstTaskDocs)
        {
            bool saveSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstTaskDocs.ForEach(eachTaskDoc =>
                    {
                        var prevAssignmentRecord = (from row in entities.Cont_tbl_TaskDocumentMapping
                                                    where row.ContractID == eachTaskDoc.ContractID
                                                    && row.TaskID == eachTaskDoc.TaskID
                                                    && row.FileID == eachTaskDoc.FileID
                                                    select row).FirstOrDefault();

                        if (prevAssignmentRecord != null)
                        {
                            prevAssignmentRecord.IsActive = true;

                            prevAssignmentRecord.UpdatedBy = eachTaskDoc.UpdatedBy;
                            prevAssignmentRecord.UpdatedOn = DateTime.Now;

                            saveSuccess = true;
                        }
                        else
                        {
                            eachTaskDoc.CreatedOn = DateTime.Now;
                            eachTaskDoc.UpdatedOn = DateTime.Now;

                            entities.Cont_tbl_TaskDocumentMapping.Add(eachTaskDoc);

                            saveSuccess = true;
                        }
                    });
                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        public static List<long> GetTaskDocumentMapping(long contractID, long taskID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Cont_tbl_TaskDocumentMapping
                                 where row.ContractID == contractID
                                 && row.TaskID == taskID
                                 && row.IsActive == true
                                 select row.FileID).ToList();

                    return query;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<Cont_SP_GetTaskDocuments_All_Result> GetTaskDocuments_All(int customerID, long contractID, long taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = entities.Cont_SP_GetTaskDocuments_All(contractID, taskID,customerID).ToList();

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderBy(row => row.FileName).ToList();

                return queryResult;
            }
        }

        public static bool DeActive_TaskDocumentMapping(long contractID, long taskID,  int loggedInUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.Cont_tbl_TaskDocumentMapping
                                  where row.TaskID == taskID
                                  && row.ContractID == contractID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = loggedInUserID);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<Cont_SP_GetAssignedTasks_All_Result> GetAssignedTaskList( int customerID, int loggedInUserID, string loggedInUserRole, int roleID, int priorityID, string taskStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_GetAssignedTasks_All(customerID)
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => entry.AssignToUserID == loggedInUserID).ToList(); //|| entry.OwnerID == loggedInUserID

                if (priorityID != 0)
                    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                if (!string.IsNullOrEmpty(taskStatus))
                {
                    if (!taskStatus.ToUpper().Equals("ALL"))
                    {
                        query = ContractTaskManagement.GetTasksStatusWise(query, taskStatus);
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ContractID,
                                 g.ContractType,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.TaskID,
                                 g.TaskType,
                                 g.TaskTitle,
                                 g.TaskDesc,
                                 g.AssignOn,
                                 g.DueDate,
                                 g.PriorityID,
                                 g.Priority,
                                 g.CreatedOn,
                                 g.UpdatedOn,
                                 g.RoleID,
                                 g.StatusName,
                                 g.AssignedTaskUser,
                                 g.CreatedBy
                             } into GCS
                             select new Cont_SP_GetAssignedTasks_All_Result()
                             {
                                 ContractID = GCS.Key.ContractID,
                                 ContractType = GCS.Key.ContractType,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 TaskID = GCS.Key.TaskID,
                                 TaskType = GCS.Key.TaskType,
                                 TaskTitle = GCS.Key.TaskTitle,
                                 TaskDesc = GCS.Key.TaskDesc,
                                 AssignOn = GCS.Key.AssignOn,                                
                                 DueDate = GCS.Key.DueDate,
                                 PriorityID = GCS.Key.PriorityID,
                                 Priority = GCS.Key.Priority,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 RoleID = GCS.Key.RoleID,
                                 StatusName = GCS.Key.StatusName,
                                 AssignedTaskUser=GCS.Key.AssignedTaskUser,
                                 CreatedBy=GCS.Key.CreatedBy
                             }).ToList();
                }

                

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.DueDate)
                        .ThenBy(entry => entry.PriorityID)
                        .ThenBy(entry => entry.TaskType).ToList();
                }

                return query.ToList();
            }
        }

        public static List<Cont_SP_GetAssignedTasks_All_Result> GeTaskList_Dashboard(int customerID, int loggedInUserID, string loggedInUserRole, int roleID, int priorityID, string taskStatus)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cont_SP_GetAssignedTasks_All(customerID)
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                    query = query.Where(entry => entry.AssignToUserID == loggedInUserID).ToList(); //|| entry.OwnerID == loggedInUserID

                if (priorityID != 0)
                    query = query.Where(entry => entry.PriorityID == priorityID).ToList();

                if (!string.IsNullOrEmpty(taskStatus))
                {
                    if (!taskStatus.ToUpper().Equals("ALL"))
                    {
                        query = ContractTaskManagement.GetTasksStatusWise(query, taskStatus);
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.ContractID,
                                 g.ContractType,
                                 g.ContractNo,
                                 g.ContractTitle,
                                 g.TaskID,
                                 g.TaskType,
                                 g.TaskTitle,
                                 g.TaskDesc,
                                 g.AssignOn,
                                 g.DueDate,
                                 g.PriorityID,
                                 g.Priority,
                                 g.CreatedOn,
                                 g.UpdatedOn,
                                 g.RoleID,
                                 g.StatusName
                             } into GCS
                             select new Cont_SP_GetAssignedTasks_All_Result()
                             {
                                 ContractID = GCS.Key.ContractID,
                                 ContractType = GCS.Key.ContractType,
                                 ContractNo = GCS.Key.ContractNo,
                                 ContractTitle = GCS.Key.ContractTitle,
                                 TaskID = GCS.Key.TaskID,
                                 TaskType = GCS.Key.TaskType,
                                 TaskTitle = GCS.Key.TaskTitle,
                                 TaskDesc = GCS.Key.TaskDesc,
                                 AssignOn = GCS.Key.AssignOn,
                                 DueDate = GCS.Key.DueDate,
                                 PriorityID = GCS.Key.PriorityID,
                                 Priority = GCS.Key.Priority,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 RoleID = GCS.Key.RoleID,
                                 StatusName = GCS.Key.StatusName,
                             }).ToList();
                }



                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.DueDate)
                        .ThenBy(entry => entry.PriorityID)
                        .ThenBy(entry => entry.TaskType).ToList();
                }

                return query.ToList();
            }
        }

        public static List<Cont_SP_GetAssignedTasks_All_Result> GetTasksStatusWise(List<Cont_SP_GetAssignedTasks_All_Result> lstTaskRecords, string statusName) 
        {
            try
            {
                if (statusName.Equals("Upcoming"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                                                && (row.AssignOn > DateTime.Now && row.AssignOn <= DateTime.Now.AddDays(30))).ToList();
                    //&& (row.DueDate > DateTime.Now)

                }
                else if (statusName.Equals("Submitted for Review"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                                        && row.AssignOn <= DateTime.Now
                                        && row.TaskType == 4
                                        && (row.DueDate >= DateTime.Now)).ToList();
                }
                else if (statusName.Equals("Submitted for Approval"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress")
                                        && row.AssignOn <= DateTime.Now
                                        && row.TaskType == 6
                                        && row.DueDate >= DateTime.Now).ToList();
                }
                else if (statusName.Equals("Reviewed/Approved"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => row.StatusName.Contains("Submitted")).ToList();
                    //&& row.AssignOn >= DateTime.Now                                                                                        
                    //&& !(row.DueDate > DateTime.Now)
                }
                else if (statusName.Equals("Overdue"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => (row.StatusName == "Open" || row.StatusName == "In-Progress") && row.DueDate < DateTime.Now).ToList();
                }
                else if (statusName.Equals("Closed"))
                {
                    lstTaskRecords = lstTaskRecords.Where(row => row.StatusName == "Closed").ToList();
                }

                return lstTaskRecords;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return new List<Cont_SP_GetAssignedTasks_All_Result>();
            }
        }

        public static List<Cont_SP_GetContractTasks_Paging_Result> GetContractTasks_Paging(int customerID, long contractID, int pageSize, int pageNumber)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = entities.Cont_SP_GetContractTasks_Paging(pageSize, pageNumber, contractID, customerID).ToList();
                return queryResult;
            }
        }

        public static Cont_tbl_TaskInstance GetTaskDetailsByTaskID(long contractID, long taskID) //, int customerID
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_TaskInstance
                                   where row.ContractID == contractID
                                   && row.ID == taskID
                                   //&& row.CustomerID == customerID
                                   select row).FirstOrDefault();
                return queryResult;
            }
        }

        public static Cont_SP_GetTaskDetails_UserRoleWise_Result GetTaskDetailsByUserRoleWise(long contractID, long taskID, int userID, int roleID) //, int customerID
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_SP_GetTaskDetails_UserRoleWise(contractID, taskID, userID, roleID)                                  
                                   select row).FirstOrDefault();
                return queryResult;
            }
        }

        public static bool ExistTaskTitle(string taskTitle, long contractID, long taskID, int customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var taskDetail = (from row in entities.Cont_tbl_TaskInstance
                                      where row.TaskTitle.Trim().ToUpper().Trim().Equals(taskTitle.Trim().ToUpper().Trim())
                                      && row.ContractID == contractID
                                      && row.CustomerID == customerID
                                      && row.IsActive == true 
                                      select row);

                    if (taskID != 0)
                    {
                        taskDetail = taskDetail.Where(entry => entry.ID != taskID);
                    }
                    return taskDetail.Select(entry => true).SingleOrDefault();
                    //if (taskDetail != null)
                    //    return true;
                    //else
                    //    return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static long CreateTask(Cont_tbl_TaskInstance objTaskRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objTaskRecord.CreatedOn = DateTime.Now;

                    entities.Cont_tbl_TaskInstance.Add(objTaskRecord);
                    entities.SaveChanges();

                    return objTaskRecord.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool UpdateTask(Cont_tbl_TaskInstance objTaskRecord)
        {
            try
            {
                using (ComplianceDBEntities Entities = new ComplianceDBEntities())
                {
                    var QueryResult = (from row in Entities.Cont_tbl_TaskInstance
                                       where row.ID == objTaskRecord.ID
                                       && row.ContractID == objTaskRecord.ContractID
                                       select row).FirstOrDefault();

                    if (QueryResult != null)
                    {
                        QueryResult.IsActive = objTaskRecord.IsActive;
                        QueryResult.TaskType = objTaskRecord.TaskType;
                        QueryResult.CustomerID = objTaskRecord.CustomerID;
                        QueryResult.ContractID = objTaskRecord.ContractID;
                        QueryResult.TaskTitle = objTaskRecord.TaskTitle;
                        QueryResult.TaskDesc = objTaskRecord.TaskDesc;
                        QueryResult.AssignOn = objTaskRecord.AssignOn;
                        QueryResult.DueDate = objTaskRecord.DueDate;
                        QueryResult.PriorityID = objTaskRecord.PriorityID;
                        QueryResult.ExpectedOutcome = objTaskRecord.ExpectedOutcome;
                        QueryResult.Remark = objTaskRecord.Remark;

                        QueryResult.UpdatedBy = objTaskRecord.UpdatedBy;
                        QueryResult.UpdatedOn = DateTime.Now;
                        QueryResult.Permission = objTaskRecord.Permission;
                        Entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteTask(long contractID, long taskID, int deletedByUserID, int customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_TaskInstance taskToDelete = entities.Cont_tbl_TaskInstance
                        .Where(x => x.ID == taskID && x.ContractID == contractID && x.CustomerID == customerID).FirstOrDefault();

                    if (taskToDelete != null)
                    {
                        taskToDelete.IsActive = false;
                        taskToDelete.DeletedBy = deletedByUserID;
                        taskToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActive_TaskUserAssignments(long taskID, long contractID, int loggedInUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var record = (from row in entities.Cont_tbl_TaskUserAssignment
                                  where row.TaskID == taskID
                                  && row.ContractID == contractID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = loggedInUserID);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_TaskUserAssignments(List<Cont_tbl_TaskUserAssignment> lstAssignmentRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstAssignmentRecord.ForEach(_objAssignmentRecord =>
                    {
                        var prevAssignmentRecord = (from row in entities.Cont_tbl_TaskUserAssignment
                                                    where row.UserID == _objAssignmentRecord.UserID
                                                    && row.RoleID == _objAssignmentRecord.RoleID
                                                    && row.ContractID == _objAssignmentRecord.ContractID
                                                    && row.TaskID == _objAssignmentRecord.TaskID
                                                    select row).FirstOrDefault();

                        if (prevAssignmentRecord != null)
                        {
                            prevAssignmentRecord.IsActive = true;

                            prevAssignmentRecord.AccessURL = _objAssignmentRecord.AccessURL;
                            prevAssignmentRecord.LinkCreatedOn = _objAssignmentRecord.LinkCreatedOn;
                            prevAssignmentRecord.URLExpired = _objAssignmentRecord.URLExpired;

                            prevAssignmentRecord.UpdatedBy = _objAssignmentRecord.UpdatedBy;
                            prevAssignmentRecord.UpdatedOn = DateTime.Now;

                            saveSuccess = true;
                        }
                        else
                        {
                            _objAssignmentRecord.CreatedOn = DateTime.Now;
                            _objAssignmentRecord.UpdatedOn = DateTime.Now;

                            entities.Cont_tbl_TaskUserAssignment.Add(_objAssignmentRecord);

                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_TaskTrasactions(List<Cont_tbl_TaskUserAssignment> lstAssignmentRecord, int loggedInUserID, int statusID, string comments)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstAssignmentRecord.ForEach(_objAssignmentRecord =>
                    {
                        Cont_tbl_TaskTransaction taskTxn = new Cont_tbl_TaskTransaction()
                        {
                            ContractID = _objAssignmentRecord.ContractID,
                            TaskID = _objAssignmentRecord.TaskID,
                            StatusID = statusID,
                            Comment = comments,
                            UserID = _objAssignmentRecord.UserID,
                            RoleID = _objAssignmentRecord.RoleID,
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = loggedInUserID,
                            CreatedOn = DateTime.Now,
                        };

                        var prevTxnRecord = (from row in entities.Cont_tbl_TaskTransaction
                                             where row.ContractID == taskTxn.ContractID
                                             && row.TaskID == taskTxn.TaskID
                                             && row.UserID == taskTxn.UserID
                                             && row.RoleID == taskTxn.RoleID
                                             && row.StatusID == taskTxn.StatusID
                                             && row.IsActive == true
                                             select row).FirstOrDefault();

                        if (prevTxnRecord == null)
                        {
                            entities.Cont_tbl_TaskTransaction.Add(taskTxn);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateContractStatus(int customerID,  long contractID, long taskID, string statusName, int newStatusID)
        {
            bool saveSuccess = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstTaskRecentTxns = entities.Cont_View_RecentTaskStatusTransaction.Where(x => x.ContractID == contractID && x.TaskID == taskID).ToList();

                    if (lstTaskRecentTxns.Count > 0)
                    {
                        int otherTxnStatusCount = lstTaskRecentTxns.Where(row => row.StatusName.Trim().ToUpper() != statusName.Trim().ToUpper()).ToList().Count;

                        if (otherTxnStatusCount == 0)
                        {
                            Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                            {
                                CustomerID = customerID,
                                ContractID = contractID,
                                
                                StatusID = newStatusID,                               
                                StatusChangeOn = DateTime.Now,

                                IsActive = true,
                                CreatedBy = 0,
                                UpdatedBy = 0,                                
                            };

                            saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);
                        }
                    }

                    return saveSuccess;               
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateTaskStatus(int customerID, int loggedInUserID, long contractID, long taskID, string newStatusName)
        {
            bool saveSuccess = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var newStatusID = entities.Cont_tbl_StatusMaster.Where(x => x.StatusName.Trim().ToUpper().Equals(newStatusName.Trim().ToUpper())).Select(row => row.ID).FirstOrDefault();

                    if (newStatusID != 0)
                    {
                        var lstTaskAssignedUsers = ContractTaskManagement.GetContractTaskUserAssignment(customerID, contractID, taskID);

                        if (lstTaskAssignedUsers.Count > 0)
                        {
                            List<Cont_tbl_TaskTransaction> lstTaskTxns = new List<Cont_tbl_TaskTransaction>();

                            lstTaskAssignedUsers.ForEach(eachUserAssignment =>
                            {
                                Cont_tbl_TaskTransaction newTaskTxnRecord = new Cont_tbl_TaskTransaction()
                                {
                                    ContractID = contractID,
                                    TaskID = taskID,
                                    StatusID = newStatusID,
                                    Comment = "Task Closed",
                                    UserID = eachUserAssignment.UserID,
                                    RoleID = eachUserAssignment.RoleID,
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    CreatedBy = loggedInUserID,
                                    CreatedOn = DateTime.Now,
                                };

                                lstTaskTxns.Add(newTaskTxnRecord);
                            });

                             saveSuccess = ContractTaskManagement.CreateTaskTransactionLogs(lstTaskTxns);                            
                        }                        
                    }

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        
        public static bool UpdateURLExpired(int customerID, long contractID, long taskID, bool flag)
        {
            bool saveSuccess = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstTaskAssignedUsers = (from row in entities.Cont_tbl_TaskUserAssignment
                                       where row.ContractID == contractID
                                       && row.TaskID == taskID
                                       && row.IsActive == true
                                       select row).ToList();

                    if (lstTaskAssignedUsers.Count > 0)
                    {
                        lstTaskAssignedUsers.ForEach(row => row.URLExpired = true);
                        entities.SaveChanges();
                    }                    
                }

                return saveSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<Cont_tbl_TaskUserAssignment> GetContractTaskUserAssignment(int customerID, long contractID, long taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_TaskUserAssignment
                                   where row.ContractID == contractID
                                   && row.TaskID == taskID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }

        public static int GetRandomAssignedUserofTask(int customerID, long contractID, long taskID, int loggedInUserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_TaskUserAssignment
                                   where row.ContractID == contractID
                                   && row.TaskID == taskID
                                   && row.IsActive == true
                                   select row.UserID).ToList();

                if (queryResult.Count > 0)
                    queryResult = queryResult.Where(row => row == loggedInUserID).ToList();

                return queryResult.FirstOrDefault();
            }
        }

        public static List<Cont_tbl_TaskUserAssignment> GetContractTaskUserAssignment_RoleWise(int customerID, long contractID, long taskID, long roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_TaskUserAssignment
                                   where row.ContractID == contractID
                                   && row.TaskID == taskID
                                   && row.RoleID== roleID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }

        public static bool UpdateTaskAccessURL_Contract(long contractID, long taskID, int loggedInUserID, int AssignedUserID,int roleID, string accessURL)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_TaskUserAssignment taskToUpdate = entities.Cont_tbl_TaskUserAssignment
                                                                .Where(x => x.ContractID == contractID
                                                                && x.TaskID == taskID
                                                                && x.UserID == AssignedUserID
                                                                && x.RoleID == roleID
                                                                && x.IsActive == true).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.AccessURL = accessURL.ToString();
                        taskToUpdate.LinkCreatedOn = DateTime.Now;
                        taskToUpdate.URLExpired = false;

                        taskToUpdate.UpdatedBy = loggedInUserID;
                        taskToUpdate.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Task-Documents
        public static List<Cont_tbl_FileData> GetTaskDocuments(int customerID, long contractID, long docTypeID, long? taskID, long? taskResponseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_FileData
                                   where row.ContractID == contractID
                                   && row.DocTypeID == docTypeID
                                   && row.TaskID == taskID
                                   && row.TaskResponseID == taskResponseID
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        #endregion

        #region Task-Responses
        public static List<Cont_SP_GetTaskResponses_All_Result> GetTaskResponses(long contractID, long taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_SP_GetTaskResponses_All(contractID, taskID)                                  
                                   select row).ToList();

                return queryResult;
            }
        }

        public static List<Cont_tbl_TaskTransaction> GetTaskResponseDetails(long contractID, long taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_TaskTransaction
                                   where row.ContractID == contractID
                                   && row.TaskID == taskID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }

        public static long CreateTaskTransactionLog(Cont_tbl_TaskTransaction objRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objRecord.CreatedOn = DateTime.Now;

                    entities.Cont_tbl_TaskTransaction.Add(objRecord);
                    entities.SaveChanges();

                    return objRecord.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool CreateTaskTransactionLogs(List<Cont_tbl_TaskTransaction> lstObjRecords)
        {
            bool saveSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstObjRecords.ForEach(_objTxnRecord =>
                    {
                        _objTxnRecord.CreatedOn = DateTime.Now;
                        entities.Cont_tbl_TaskTransaction.Add(_objTxnRecord);
                    });

                    entities.SaveChanges();
                   return saveSuccess = true;
                }
                return saveSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        public static bool CreateTaskTransactionLog(long contractID, long taskID,long taskTxnID, int deletedByUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<Cont_tbl_FileData> DocsToDelete = entities.Cont_tbl_FileData
                        .Where(x => x.ContractID == contractID && x.TaskID== taskID && x.TaskResponseID== taskTxnID).ToList();

                    if (DocsToDelete.Count > 0)
                    {
                        DocsToDelete.ForEach(entry => entry.IsDeleted = true);
                        DocsToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        DocsToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    Cont_tbl_TaskTransaction taskTxnLogToDelete = entities.Cont_tbl_TaskTransaction.Where(x => x.ID == taskTxnID).FirstOrDefault();

                    if (taskTxnLogToDelete != null)
                    {
                        taskTxnLogToDelete.IsActive = false;
                        taskTxnLogToDelete.UpdatedBy = deletedByUserID;
                        taskTxnLogToDelete.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteTaskTransactionLog(long contractID, long taskID, long taskResponseID, int deletedByUserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<Cont_tbl_FileData> ResponseRelatedDocToDelete = entities.Cont_tbl_FileData
                        .Where(x => x.ContractID == contractID && x.TaskID == taskID && x.TaskResponseID == taskResponseID).ToList();

                    if (ResponseRelatedDocToDelete.Count > 0)
                    {
                        ResponseRelatedDocToDelete.ForEach(entry => entry.IsDeleted = true);
                        ResponseRelatedDocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        ResponseRelatedDocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    Cont_tbl_TaskTransaction taskResponseLogToDelete = entities.Cont_tbl_TaskTransaction.Where(x => x.ID == taskResponseID).FirstOrDefault();

                    if (taskResponseLogToDelete != null)
                    {
                        taskResponseLogToDelete.IsActive = false;
                        taskResponseLogToDelete.UpdatedBy = deletedByUserID;
                        taskResponseLogToDelete.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<Cont_tbl_FileData> GetTaskResponseDocuments(long contractID, long taskID, long taskResponseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_FileData
                                   where row.ContractID == contractID
                                   && row.TaskID == taskID
                                   && row.TaskResponseID == taskResponseID
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        #endregion

        public static List<Cont_tbl_FileData> GetMilestoneResponseDocuments(long contractID, long MilestoneID, long MilestoneResponceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_tbl_FileData
                                   where row.ContractID == contractID
                                   && row.MilestoneID == MilestoneID
                                   && row.MilestoneResponceID == MilestoneResponceID
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        public static List<Cont_SP_MilestoneDetail_Result> GetContractMileStone_Paging(int customerID, long contractID, int pageSize, int pageNumber, int MilestoneID, int MilestoneStatusID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = entities.Cont_SP_MilestoneDetail(pageSize, pageNumber, contractID, customerID, MilestoneID, MilestoneStatusID).ToList();
                return queryResult;
            }
        }
        public static List<MileStonedetail> GetMilestoneDetailsByID(long contractID, long MilestoneID, int customerID) //, 
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<MileStonedetail> queryResult = (from row in entities.Cont_tbl_MilestoneInstance
                                                     join row1 in entities.Cont_tbl_MilestoneDetail
                                                     on row.MilestoneMasterID equals row1.ID
                                                     where row.ContractTemplateID == contractID
                                                     && row.ID == MilestoneID
                                                     && row.CustomerID == customerID
                                                     select new MileStonedetail
                                                     {
                                                         ID = row.ID,
                                                         ContractTemplateID = row.ContractTemplateID,
                                                         CustomerID = row.CustomerID,
                                                         Title = row1.Title,
                                                         Description = row1.Description,
                                                         DeptID = row1.DeptID,
                                                         UserID = row1.UserID,
                                                         DueDate = row.DueDate,
                                                         StatusID = row.StatusID,
                                                         Remark = row.Remark,
                                                         CreatedBy = row.CreatedBy,
                                                         CreatedOn = row.CreatedOn,
                                                         UpdateBy = row.UpdateBy,
                                                         UpdatedOn = row.UpdatedOn,
                                                         IsActive = row.IsActive,
                                                         MilestoneMasterID = row.MilestoneMasterID,
                                                         StartDate = row.StartDate,
                                                         EndDate = row.EndDate,
                                                         ForPeriod = row.ForPeriod
                                                     }).ToList();
                return queryResult;
            }
        }
        public static bool DeleteMilestone(long contractID, long MilestoneID, int deletedByUserID, int customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_MilestoneInstance milestoneToDelete = entities.Cont_tbl_MilestoneInstance
                        .Where(x => x.ID == MilestoneID && x.ContractTemplateID == contractID && x.CustomerID == customerID).FirstOrDefault();

                    if (milestoneToDelete != null)
                    {
                        milestoneToDelete.IsActive = false;
                        milestoneToDelete.UpdateBy = deletedByUserID;
                        milestoneToDelete.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<Cont_SP_GetMilestoneResponses_All_Result> GetMilestoneResponses(long contractID, long MilestoneID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Cont_SP_GetMilestoneResponses_All(contractID, MilestoneID)
                                   select row).ToList();

                return queryResult;
            }
        }
        public static bool DeleteCustomschedule(long ID, long UserID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempMileStoneSchedule milestoneToDelete = entities.TempMileStoneSchedules
                        .Where(x => x.ID == ID).FirstOrDefault();

                    if (milestoneToDelete != null)
                    {
                        milestoneToDelete.IsDelete = true;
                        milestoneToDelete.UpdatedBy = UserID;
                        milestoneToDelete.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
      
    }
}
