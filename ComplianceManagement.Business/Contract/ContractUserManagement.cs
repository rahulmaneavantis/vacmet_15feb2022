﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class ContractUserManagement
    {
        public static object GetAllMGMTContract(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Users
                             where row.IsDeleted == false
                             && (row.ContractRoleID == 2 || row.ContractRoleID == 8)
                             select row).ToList();
               
                users = users.Where(entry => entry.CustomerID == customerID && entry.IsActive == true).ToList();
                var lstUsers = (from row in users
                               select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();


                return lstUsers.ToList();
             }
        }
        public static object GetAllUsersContract(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             select new
                             {
                                 row.ID,
                                 Name = row.FirstName + " " + row.LastName
                             }).ToList();

                return users.ToList();


            }
        }
        public static string GetUserNameByUserID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserDetails = (from row in entities.Users
                                   where row.ID == userID
                                   select row.FirstName + " " + row.LastName).FirstOrDefault();

                return UserDetails;
            }
        }
      
        public static int GetContractOwnerUserIDByEmail(List<User> lstUsers, int customerID, string email)
        {
            var UserID = (from row in lstUsers
                          where row.Email.Trim().ToUpper().Equals(email.Trim().ToUpper())
                          && row.CustomerID == customerID
                          && row.IsDeleted == false
                          && row.IsActive == true
                          && row.ContractRoleID != null
                          select row.ID).FirstOrDefault();

            return Convert.ToInt32(UserID);
        }
        public static int GetPaymentTermMapping(string PaymentTerm)
        {
            // lstMappingIDs = new List<long>();
           
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                int lstMappingIDs = (from row in entities.Cont_tbl_PaymentTerm
                                     where row.PaymentTermName == PaymentTerm
                                     && row.IsDeleted == false
                                     select (int)row.PaymentTermID).FirstOrDefault();
                    return lstMappingIDs;
                }
           
        }

        public static List<User> GetAllUsers_Contract(int customerID) 
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.IsActive == true
                             && row.CustomerID == customerID
                             && row.ContractRoleID != null
                             select row).ToList();

                return query.ToList();
            }
        }

        public static List<User> GetUsers_Contract(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             //&& row.IsActive == true
                             && row.CustomerID == customerID
                             && row.ContractRoleID != 8
                             && row.ContractRoleID != 2
                             select row).ToList();  
                return query.ToList();
            }
        }

        public static List<User> GetAllUser(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Users
                             where row.IsDeleted == false
                             select row);
              
                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }

        public static List<object> GetRequiredUsers(List<User> userList, int userType) //UserType 1=Internal, 2-Lawyer, 3-External,4-ALL
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in userList
                             where row.IsDeleted == false  
                             //&& row.RoleID!=8                           
                             select row).ToList();

                if (userType != 0)
                {
                    if (userType == 1)
                        query = query.Where(entry => entry.IsExternal == false && entry.IsActive == true).ToList();
                    else if (userType == 2)
                        query = query.Where(entry => entry.IsExternal == false && entry.IsActive == true).ToList();
                    else if (userType == 3)
                        query = query.Where(entry => entry.IsExternal == true && entry.IsActive == false).ToList();
                }

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return lstUsers.ToList();
            }
        }

        public static int CreateNewUserCompliance(User user)
        {
            int newUserID = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //entities.Connection.Open();
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        entities.Users.Add(user);
                        entities.SaveChanges();

                        newUserID = Convert.ToInt32(user.ID);

                        dbtransaction.Commit();

                        return newUserID;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        newUserID--;
                        entities.SP_ResetIDUser(newUserID);

                        return newUserID = 0;
                    }
                }
            }
        }

        public static bool CreateNewUserAudit(mst_User user)
        {
            int newUserID = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        entities.mst_User.Add(user);

                        entities.SaveChanges();

                        newUserID = Convert.ToInt32(user.ID);

                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        newUserID--;
                        entities.SP_ResetIDMstUser(newUserID);
                        return false;
                    }
                }
            }
        }

        public static bool UpdateUserContract_Compliance(User user, List<UserParameterValue> parameters)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User userToUpdate = (from row in entities.Users
                                     where row.ID == user.ID
                                     select row).FirstOrDefault();
                if (userToUpdate != null)
                {
                    userToUpdate.IsHead = user.IsHead;
                    userToUpdate.FirstName = user.FirstName;
                    userToUpdate.LastName = user.LastName;
                    userToUpdate.Designation = user.Designation;
                    userToUpdate.Email = user.Email;
                    userToUpdate.ContactNumber = user.ContactNumber;
                    userToUpdate.Address = user.Address;
                    userToUpdate.CustomerID = user.CustomerID;
                    userToUpdate.CustomerBranchID = user.CustomerBranchID;
                    userToUpdate.ReportingToID = user.ReportingToID;
                    userToUpdate.DepartmentID = user.DepartmentID;
                    userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;
                    userToUpdate.ContractRoleID = user.ContractRoleID;                    
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.AuditorID = user.AuditorID;
                    userToUpdate.IsExternal = user.IsExternal;

                    List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                    var existingParameters = (from parameterRow in entities.UserParameterValues
                                              where parameterRow.UserId == user.ID
                                              select parameterRow).ToList();

                    existingParameters.ForEach(entry =>
                    {
                        if (parameterIDs.Contains(entry.ID))
                        {
                            UserParameterValue parameter = parameters.Find(param1 => param1.ID == entry.ID);
                            entry.Value = parameter.Value;
                        }
                        else
                        {
                            entities.UserParameterValues.Remove(entry);
                        }
                    });

                    parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValues.Add(entry));

                    entities.SaveChanges();
                }

                return true;
            }
        }

        public static bool UpdateUserContract_Audit(mst_User user, List<UserParameterValue_Risk> parameters)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User userToUpdate = (from row in entities.mst_User
                                         where row.ID == user.ID
                                         select row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.IsHead = user.IsHead;
                    userToUpdate.FirstName = user.FirstName;
                    userToUpdate.LastName = user.LastName;
                    userToUpdate.Designation = user.Designation;
                    userToUpdate.Email = user.Email;
                    userToUpdate.ContactNumber = user.ContactNumber;
                    userToUpdate.Address = user.Address;
                    userToUpdate.CustomerID = user.CustomerID;
                    userToUpdate.CustomerBranchID = user.CustomerBranchID;
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.DepartmentID = user.DepartmentID;
                    userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;

                    userToUpdate.ContractRoleID = user.ContractRoleID;                    
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.AuditorID = user.AuditorID;
                    userToUpdate.IsExternal = user.IsExternal;

                    List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                    var existingParameters = (from parameterRow in entities.UserParameterValue_Risk
                                              where parameterRow.UserId == user.ID
                                              select parameterRow).ToList();

                    existingParameters.ForEach(entry =>
                    {
                        if (parameterIDs.Contains(entry.ID))
                        {
                            UserParameterValue_Risk parameter = parameters.Find(param1 => param1.ID == entry.ID);
                            entry.Value = parameter.Value;
                        }
                        else
                        {
                            entities.UserParameterValue_Risk.Remove(entry);
                        }
                    });

                    //parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValue_Risk.Add(entry));

                    entities.SaveChanges();
                }
                return true;
            }
        }
       
    }
}
