﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Business.Contract
{
    public class ContractMastersManagement
    {
        public static List<Cont_SP_GetCommentedContracts_All_Result> GetContractComments(long cutomerID, long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractcomments = entities.Cont_SP_GetCommentedContracts_All(Convert.ToInt32(cutomerID), Convert.ToInt32(userID), -1).ToList();

                if (lstContractcomments.Count > 0)
                {
                    lstContractcomments = (from g in lstContractcomments
                                           group g by new
                                           {
                                               g.ID, //ContractID
                                               g.CustomerID,
                                               g.ContractNo,
                                               g.ContractTitle,
                                               g.ContractDetailDesc,
                                               g.VendorIDs,
                                               g.VendorNames,
                                               g.CustomerBranchID,
                                               g.BranchName,
                                               g.DepartmentID,
                                               g.DeptName,
                                               g.ContractTypeID,
                                               g.TypeName,
                                               g.ContractSubTypeID,
                                               g.SubTypeName,
                                               g.ProposalDate,
                                               g.AgreementDate,
                                               g.EffectiveDate,
                                               g.ReviewDate,
                                               g.ExpirationDate,
                                               g.CreatedOn,
                                               g.UpdatedOn,
                                               g.StatusName,
                                               g.ContractAmt,
                                               g.ContactPersonOfDepartment,
                                               g.PaymentType,
                                               g.AddNewClause,
                                               g.Owner
                                           } into GCS
                                           select new Cont_SP_GetCommentedContracts_All_Result()
                                           {
                                               ID = GCS.Key.ID, //ContractID
                                               CustomerID = GCS.Key.CustomerID,
                                               ContractNo = GCS.Key.ContractNo,
                                               ContractTitle = GCS.Key.ContractTitle,
                                               ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                               VendorIDs = GCS.Key.VendorIDs,
                                               VendorNames = GCS.Key.VendorNames,
                                               CustomerBranchID = GCS.Key.CustomerBranchID,
                                               BranchName = GCS.Key.BranchName,
                                               DepartmentID = GCS.Key.DepartmentID,
                                               DeptName = GCS.Key.DeptName,
                                               ContractTypeID = GCS.Key.ContractTypeID,
                                               TypeName = GCS.Key.TypeName,
                                               ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                               SubTypeName = GCS.Key.SubTypeName,
                                               ProposalDate = GCS.Key.ProposalDate,
                                               AgreementDate = GCS.Key.AgreementDate,
                                               EffectiveDate = GCS.Key.EffectiveDate,
                                               ReviewDate = GCS.Key.ReviewDate,
                                               ExpirationDate = GCS.Key.ExpirationDate,
                                               CreatedOn = GCS.Key.CreatedOn,
                                               UpdatedOn = GCS.Key.UpdatedOn,
                                               StatusName = GCS.Key.StatusName,
                                               ContractAmt = GCS.Key.ContractAmt,
                                               ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                               PaymentType = GCS.Key.PaymentType,
                                               AddNewClause = GCS.Key.AddNewClause,
                                               Owner = GCS.Key.Owner
                                           }).ToList();
                }

                if (lstContractcomments.Count > 0)
                {
                    lstContractcomments = lstContractcomments.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                }

                return lstContractcomments.ToList();
            }
        }
        #region dept
        public static List<Department> getDepartmentdetails(long cutomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var QueryResult = (from row in entities.Departments
                                   where row.CustomerID == cutomerID
                                   select row).ToList();

                return QueryResult;
            }
        }
        public static bool DeleteDepartmentType(long ID, long customerID)
        {
            bool deleteSuccess = false;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Department objCase = (from row in entities.Departments
                                          where row.ID == ID
                                                           && row.CustomerID == customerID
                                          select row).FirstOrDefault();
                    if (objCase != null)
                    {
                        objCase.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }
        #endregion
        public static bool DeleteContractUserDepatMapping(long Id, long customerID)
        {
            bool deleteSuccess = false;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_UserDeptMapping objCase = (from row in entities.Cont_tbl_UserDeptMapping
                                                        where row.ID == Id
                                                        && row.CustomerID == customerID
                                                        select row).FirstOrDefault();
                    if (objCase != null)
                    {
                        objCase.IsActive = false;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        public static List<Cont_SP_GetVendorDetail_Result> GetVendors_All(int CustomerID, int ContractID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = entities.Cont_SP_GetVendorDetail(CustomerID, ContractID).ToList();
                return Query.ToList();
            }
        }

        public static List<Cont_SP_AssignMilestoneDetailNew_Result> GetContractMilestoneDetail(long cutomerID, long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractMilestoneType = entities.Cont_SP_AssignMilestoneDetailNew(Convert.ToInt32(cutomerID), Convert.ToInt32(userID)).ToList();
                return lstContractMilestoneType;
            }
        }
        public static List<Cont_SP_GetDepartmentUserMapping_Result> GetContractUserDepartmentMapping_Paging(long cutomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractDocType = entities.Cont_SP_GetDepartmentUserMapping(Convert.ToInt32(cutomerID)).ToList();
                return lstContractDocType;
            }
        }
        public static int GetStateID(String StateName, int ContID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int objStateID = (from row in entities.States
                                  where row.Name.Equals(StateName) && row.CountryID == ContID
                                  && row.IsDeleted == false
                                  select row.ID).FirstOrDefault();


                return objStateID;
            }
        }

        public static int GetCityID(string CityName, int StateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int objCityID = (from row in entities.Cities
                                 where row.Name.Equals(CityName) && row.StateId == StateID
                                 && row.IsDeleted == false
                                 select row.ID).FirstOrDefault();

                return objCityID;
            }
        }

        public static int GetBranchIDByName(int customerID, string branchName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EntityLocationBranchID = (from row in entities.CustomerBranches
                                              where row.Name.Trim().ToUpper().Equals(branchName.Trim().ToUpper())
                                              && row.IsDeleted == false 
                                              && row.Status == 1
                                              && row.CustomerID == customerID
                                              select row.ID).FirstOrDefault();

                return Convert.ToInt32(EntityLocationBranchID);
            }
        }

        public static int GetBranchIDByName(List<CustomerBranch> lstCustomerBranches, int customerID, string branchName)
        {
            var EntityLocationBranchID = (from row in lstCustomerBranches
                                          where row.Name.Trim().ToUpper().Equals(branchName.Trim().ToUpper())
                                          && row.IsDeleted == false
                                          && row.Status == 1
                                          && row.CustomerID == customerID
                                          select row.ID).FirstOrDefault();

            return Convert.ToInt32(EntityLocationBranchID);
        }

        //public static long GetVendorIDByName(string vendorName, int customerID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var objVendorID = (from row in entities.Cont_tbl_VendorMaster
        //                            where row.VendorName.Trim().ToUpper().Equals(vendorName.Trim().ToUpper())
        //                            && row.CustomerID == customerID
        //                            && row.IsDeleted == false
        //                            select row.ID).FirstOrDefault();

        //        return Convert.ToInt64(objVendorID);
        //    }
        //}

        //public static long GetVendorIDByName(List<Cont_tbl_VendorMaster> lstVendors, string vendorName, int customerID)
        //{
        //    var objVendorID = (from row in lstVendors
        //                       where row.VendorName.Trim().ToUpper().Equals(vendorName.Trim().ToUpper())
        //                       && row.CustomerID == customerID
        //                       && row.IsDeleted == false
        //                       select row.ID).FirstOrDefault();

        //    return Convert.ToInt64(objVendorID);
        //}           
       


        //#region Vendor Master
        //public static List<Cont_tbl_VendorMaster> GetVendors_All(long CustomerID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var Query = (from row in entities.Cont_tbl_VendorMaster
        //                     where row.CustomerID == CustomerID
        //                     && row.IsDeleted == false
        //                     select row).OrderBy(entry => entry.VendorName);
        //        return Query.ToList();
        //    }
        //}

        //public static List<Cont_SP_GetVendors_Paging_Result> GetVendorDetails_Paging( int CustomerID, string filter)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var Query = entities.Cont_SP_GetVendors_Paging(CustomerID, filter).ToList();
        //        return Query.ToList();
        //    }
        //}

        //public static bool ExistsVendorData(String VendorName, int CustomerID, int ContractVendorID, int VendorTypeID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var objVendor = (from row in entities.Cont_tbl_VendorMaster
        //                         where row.VendorName.Equals(VendorName)
        //                         && row.Type == VendorTypeID
        //                         && row.CustomerID == CustomerID 
        //                         && row.IsDeleted == false
        //                         select row);

        //        if (ContractVendorID != 0)
        //        {
        //            objVendor = objVendor.Where(entry => entry.ID != ContractVendorID);
        //        }

        //        return objVendor.Select(entry => true).SingleOrDefault();
        //    }
        //}

        //public static bool ExistsVendorName(Cont_tbl_VendorMaster objVendor, long VendorID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var query = (from row in entities.Cont_tbl_VendorMaster
        //                     where row.VendorName.Equals(objVendor.VendorName)
        //                     && row.CustomerID == objVendor.CustomerID
        //                     && row.IsDeleted == false
        //                     select row);

        //        if (query != null)
        //        {
        //            if (VendorID > 0)
        //            {
        //                query = query.Where(entry => entry.ID != VendorID);
        //            }
        //        }

        //        return query.Select(entry => true).FirstOrDefault();
        //    }
        //}

        //public static bool CreateVendorData(Cont_tbl_VendorMaster objVendor)
        //{
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            entities.Cont_tbl_VendorMaster.Add(objVendor);
        //            entities.SaveChanges();
        //        }

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return false;
        //    }
        //}

        //public static Cont_tbl_VendorMaster GetVendorDetailsByID(long ID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var objVendoretails = (from row in entities.Cont_tbl_VendorMaster
        //                               where row.ID == ID
        //                               select row).FirstOrDefault();
        //        return objVendoretails;
        //    }
        //}

        //public static bool UpdateVendorData(Cont_tbl_VendorMaster objVendor)
        //{
        //    bool updateSuccess = false;
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            Cont_tbl_VendorMaster ObjQuery = (from row in entities.Cont_tbl_VendorMaster
        //                                              where row.ID == objVendor.ID
        //                                              && row.CustomerID == objVendor.CustomerID
        //                                              select row).FirstOrDefault();
        //            if (ObjQuery != null)
        //            {
        //                ObjQuery.Type = objVendor.Type;
        //                ObjQuery.VendorName = objVendor.VendorName;
        //                ObjQuery.ContactPerson = objVendor.ContactPerson;
        //                ObjQuery.Email = objVendor.Email;
        //                ObjQuery.ContactNumber = objVendor.ContactNumber;
        //                ObjQuery.Address = objVendor.Address;
        //                ObjQuery.CountryID = objVendor.CountryID;
        //                ObjQuery.StateID = objVendor.StateID;
        //                ObjQuery.CityID = objVendor.CityID;
        //                ObjQuery.VAT = objVendor.VAT;
        //                ObjQuery.TIN = objVendor.TIN;
        //                ObjQuery.GSTIN = objVendor.GSTIN;

        //                ObjQuery.UpdatedBy = objVendor.UpdatedBy;
        //                ObjQuery.UpdatedOn = DateTime.Now;

        //                entities.SaveChanges();

        //                updateSuccess = true;
        //            }
        //        }

        //        return updateSuccess;
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

        //        return false;
        //    }
        //}
        //public static bool CheckVendorMapping(long vendorID)
        //{
        //    bool CheckSuccess = true;
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var objRecord = (from row in entities.Cont_tbl_VendorMapping
        //                         where row.VendorID == vendorID
        //                         && row.IsActive == true
        //                         select row).FirstOrDefault();
        //        if(objRecord!=null)
        //        {
        //            CheckSuccess = false;
        //        }
        //    }
        //    return CheckSuccess;
        //}
        //public static bool DeleteVendor(long vendorID, int customerID)
        //{
        //    bool deleteSuccess = false;
        //    try
        //    {

        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            var objRecord = (from row in entities.Cont_tbl_VendorMaster
        //                                                   where row.ID == vendorID
        //                                                   && row.CustomerID == customerID
        //                                                   select row).FirstOrDefault();
        //            if (objRecord != null)
        //            {
        //                objRecord.IsDeleted = true;
        //                entities.SaveChanges();

        //                deleteSuccess = true;
        //            }
        //        }

        //        return deleteSuccess;
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return deleteSuccess;
        //    }
        //}

        //#endregion


        #region Contract-Document Type Master

        public static List<Cont_tbl_DocumentTypeMaster> GetContractDocTypes_All(long CutomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractType = (from row in entities.Cont_tbl_DocumentTypeMaster
                                       where row.TypeName != null
                                       && row.IsDeleted == false
                                       && row.CustomerID == CutomerID
                                       select row).ToList();
                return lstContractType;
            }
        }

        public static Cont_tbl_DocumentTypeMaster GetDocTypeDetailsByID(long docTypeID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Objcase = (from row in entities.Cont_tbl_DocumentTypeMaster
                               where row.ID == docTypeID
                               && row.CustomerID == customerID
                               select row).FirstOrDefault();
                return Objcase;
            }
        }

        public static bool ExistsContDocumentType(Cont_tbl_DocumentTypeMaster _objDoctype)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objCase = (from row in entities.Cont_tbl_DocumentTypeMaster
                               where row.TypeName.Equals(_objDoctype.TypeName)
                               && row.CustomerID == _objDoctype.CustomerID
                               select row);

                return objCase.Select(entry => true).SingleOrDefault();
            }
        }

        public static long CreateDocumentType(Cont_tbl_DocumentTypeMaster _objDoctype)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_DocumentTypeMaster.Add(_objDoctype);
                    entities.SaveChanges();

                    return _objDoctype.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool UpdateDocTypeDetails(Cont_tbl_DocumentTypeMaster _objDoctype)
        {
            bool updateSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_DocumentTypeMaster objRecord = (from row in entities.Cont_tbl_DocumentTypeMaster
                                                             where row.ID == _objDoctype.ID
                                                             && row.CustomerID == _objDoctype.CustomerID
                                                             select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.TypeName = _objDoctype.TypeName;
                        objRecord.UpdatedBy = _objDoctype.UpdatedBy;
                        objRecord.UpdatedOn = _objDoctype.UpdatedOn;

                        entities.SaveChanges();

                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteDocType(long docTypeID, long customerID)
        {
            bool deleteSuccess = false;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Cont_tbl_DocumentTypeMaster objCase = (from row in entities.Cont_tbl_DocumentTypeMaster
                                                           where row.ID == docTypeID
                                                           && row.CustomerID == customerID
                                                           select row).FirstOrDefault();
                    if (objCase != null)
                    {
                        objCase.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        public static List<Cont_SP_GetDocTypes_Paging_Result> GetContractDocumentTypes_Paging(long cutomerID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractDocType = entities.Cont_SP_GetDocTypes_Paging(Convert.ToInt32(cutomerID), filter).ToList();
                return lstContractDocType;
            }
        }

        #endregion


        #region Contract-Custom Field

        public static string GetCustomFieldID()
        {
            string custFildId = string.Empty;
            HttpContext context = HttpContext.Current;
            if (context.Session["custFieldID"] != null)
            {
                custFildId = context.Session["custFieldID"].ToString();
            }
            return custFildId;
        }

        public static string GetDepartmenID()
        {
            string depaId = string.Empty;
            HttpContext context = HttpContext.Current;
            if (context.Session["DID"].ToString() != null)
            {
                depaId = context.Session["DID"].ToString();
            }
            return depaId;
        }

        public static List<Cont_tbl_CustomField> GetCustomFields_All(int customerID, long typeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objCustField = (from row in entities.Cont_tbl_CustomField
                                    where row.TypeID == typeID
                                     && row.CustomerID == customerID
                                     && row.IsActive == true
                                    select row).ToList();
                return objCustField;
            }
        }

        public static Cont_tbl_CustomField GetContCustomFieldByID(int iD, long customerID)
        {
            throw new NotImplementedException();
        }

        public static List<Cont_SP_GetCustomField_Paging_Result> GetCustomFields_Paging(long cutomerID, string filter, int typeid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstContractDocType = entities.Cont_SP_GetCustomField_Paging(Convert.ToInt32(cutomerID), filter, typeid).ToList();
                return lstContractDocType;
            }
        }

        public static bool ExistsCustomField(int customerID, long contractTypeID, string labelName, long fieldID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (ComplianceDBEntities Entities = new ComplianceDBEntities())
                {
                    var recordCustomField = (from row in Entities.Cont_tbl_CustomField
                                        where row.TypeID == contractTypeID
                                        && row.Label.Trim().ToUpper() == labelName.Trim().ToUpper()
                                        && row.CustomerID == customerID
                                        && row.IsActive == true
                                        select row);

                    if (recordCustomField != null)
                    {
                        if (fieldID != 0)
                        {
                            recordCustomField = recordCustomField.Where(entry => entry.ID != fieldID);
                        }                        
                    }

                    return recordCustomField.Select(entry => true).SingleOrDefault();
                }
            }
        }

        public static long CreateCustomFieldDetails(Cont_tbl_CustomField objCase)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_CustomField.Add(objCase);
                    entities.SaveChanges();

                    return objCase.ID;
                }
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool UpdateCustomFieldDetails(Cont_tbl_CustomField objCase)
        {
            bool saveSuccess = false;
            try
            {
                using (ComplianceDBEntities Entities = new ComplianceDBEntities())
                {
                    var ListcustFild = (from row in Entities.Cont_tbl_CustomField
                                        where row.ID == objCase.ID
                                        && row.IsActive == true
                                        select row).FirstOrDefault();

                    if (ListcustFild != null)
                    {
                        ListcustFild.TypeID = objCase.TypeID;
                        ListcustFild.Label = objCase.Label;

                        ListcustFild.UpdatedBy = objCase.UpdatedBy;
                        ListcustFild.UpdatedOn = DateTime.Now;

                        Entities.SaveChanges();

                        saveSuccess = true;
                    }
                }

                return saveSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        public static Cont_tbl_CustomField GetContCustomFieldDetailByID(long ID, long typeID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objSubType = (from row in entities.Cont_tbl_CustomField
                                  where row.ID == ID
                                  && row.TypeID == typeID
                                  && row.CustomerID == customerID
                                  select row).FirstOrDefault();
                return objSubType;
            }
        }

        public static bool DeleteCustomField(long ID, int customerID)
        {
            bool deleteSuccess = false;
            try
            {
                using (ComplianceDBEntities Entities = new ComplianceDBEntities())
                {
                    var QueryResult = (from row in Entities.Cont_tbl_CustomField
                                       where row.ID == ID && row.CustomerID == customerID 
                                       select row).FirstOrDefault();

                    if (QueryResult != null)
                    {
                        QueryResult.IsActive = true;
                        Entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        //public static List<FilterCustemFielData> GetFilterCustomField(int ddlselectedvalueindex, long cutomerID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var filterdata = (from row in entities.Cont_tbl_CustomField
        //                          join rows in entities.Cont_tbl_TypeMaster
        //                          on row.TypeID equals rows.ID
        //                          where row.TypeID == ddlselectedvalueindex && row.CustomerID == cutomerID
        //                         && row.IsActive == false
        //                          select new FilterCustemFielData
        //                          {
        //                              ID = row.ID,
        //                              ContractTypeID = row.TypeID,
        //                              TypeName = rows.TypeName,
        //                              Label = row.Label

        //                          }).ToList();

        //        return filterdata;
        //    }

        //}

        #endregion
    }
}
