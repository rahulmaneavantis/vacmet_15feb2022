﻿
using EntityFramework.BulkInsert.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class LitigationLaw
    {
        #region Code changes by Sushant Jully 2017 

        public static bool Exists(Act_Litigation act)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_Litigation
                             where row.IsDeleted == false && row.Name.Equals(act.Name)
                             select row);

                if (act.ID > 0)
                {
                    query = query.Where(entry => entry.ID != act.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static List<LawyerCourtMapping> GetLawyerCourtMappingData(int ID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objAssig = (from row in entities.LawyerCourtMappings
                                where row.LawyerID == ID && row.IsActive == true
                                select row);
                return objAssig.ToList();
            }
        }

        public static bool CheckLawyercourtMappingExist(LawyerCourtMapping objCourt)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objAssig = (from row in entities.LawyerCourtMappings
                                where row.CourtID == objCourt.CourtID && row.LawyerID == objCourt.LawyerID
                                select row).FirstOrDefault();
                if (objAssig != null)
                {
                    return true;
                }
                else
                { return false; }
            }
        }

        public static List<tbl_PartyDetail> GetLCPartyDetails(long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.tbl_PartyDetail
                             where row.CustomerID== CustomerID
                             select row).OrderBy(entry => entry.Name);
                return Query.ToList();
            }
        }
        
        public static tbl_PartyDetail GetLCPartyDetailsByID(int ID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objLCDetails = (from row in entities.tbl_PartyDetail
                                    where row.ID == ID
                                          select row).FirstOrDefault();
                return objLCDetails;
            }
        }

        public static void DeleteLCDetailData(int ID,long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_PartyDetail Query = (from row in entities.tbl_PartyDetail
                                         where row.ID == ID && row.CustomerID== CustomerID
                                         select row).FirstOrDefault();
                if (Query != null)
                {
                    entities.tbl_PartyDetail.Remove(Query);
                    entities.SaveChanges();
                }
            }
        }

        public static bool CheckLCDataExist(tbl_PartyDetail objlawyer)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_PartyDetail
                             where row.Name.Equals(objlawyer.Name) 
                             && row.Email.Equals(objlawyer.Email)
                             && row.CustomerID==objlawyer.CustomerID
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool ExistsPartyName(tbl_PartyDetail objlawyer, int partyID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_PartyDetail
                             where row.Name.Equals(objlawyer.Name)                            
                             && row.CustomerID == objlawyer.CustomerID                             
                             select row);

                if (query != null)
                {
                    if (partyID > 0)
                    {
                        query = query.Where(entry => entry.ID != partyID);
                    }
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static void CreateLegalCasePartyData(tbl_PartyDetail objlawyer)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_PartyDetail.Add(objlawyer);
                entities.SaveChanges();
            }
        }

        public static void UpdateLegalCasePartyData(tbl_PartyDetail objlawyer)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_PartyDetail ObjQuery = (from row in entities.tbl_PartyDetail
                                            where row.ID == objlawyer.ID 
                                            && row.CustomerID==objlawyer.CustomerID
                                            select row).FirstOrDefault();
                if (ObjQuery != null)
                {
                    ObjQuery.Name = objlawyer.Name;
                    ObjQuery.StateID = objlawyer.StateID;
                    ObjQuery.CityID = objlawyer.CityID;
                    ObjQuery.Email = objlawyer.Email;
                    ObjQuery.ContactNumber = objlawyer.ContactNumber;
                    ObjQuery.Address = objlawyer.Address;
                    ObjQuery.Country = objlawyer.Country;
                    ObjQuery.PartyType = objlawyer.PartyType;
                    entities.SaveChanges();
                }
            }
        }

        public static object FillLegalEntityData(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.CustomerID == customerID &&
                             row.IsDeleted == false && row.ParentID == null
                             select row);
                var Frequency = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return Frequency;
            }
        }                             
        public static object GetAllAct()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_Litigation
                             select row).ToList();

                return query.OrderBy(entry => entry.Name).ToList();
            }
        }


        //public static List<mst_Country> GetAllContryData()
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var ObjCountry = (from row in entities.mst_Country
        //                          where row.Name != null
        //                          select row);
        //        return ObjCountry.ToList();
        //    }
        //}


        //public static mst_Country GetCountryDetailByID(int ID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var ObjCountry = (from row in entities.mst_Country
        //                          where row.ID == ID
        //                          select row).SingleOrDefault();
        //        return ObjCountry;
        //    }
        //}

        //public static void DeleteCountryDetail(int ID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        mst_Country ObjCountry = (from row in entities.mst_Country
        //                                  where row.ID == ID
        //                                  select row).FirstOrDefault();

        //        entities.mst_Country.Remove(ObjCountry);
        //        entities.SaveChanges();
        //    }
        //}

        //public static bool CheckCountryExist(string name)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var ObjCountry = (from row in entities.mst_Country
        //                          where row.Name.Equals(name)
        //                          select row);
        //        return ObjCountry.Select(entry => true).SingleOrDefault();
        //    }
        //}

        //public static void CreateContryDetails(mst_Country objCountry)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        entities.mst_Country.Add(objCountry);
        //        entities.SaveChanges();
        //    }
        //}

        //public static void UpdateCountryDetails(mst_Country objCountry)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        mst_Country updatestates = (from row in entities.mst_Country
        //                                    where row.ID == objCountry.ID
        //                                    select row).FirstOrDefault();

        //        updatestates.Name = objCountry.Name;
        //        entities.SaveChanges();
        //    }
        //}
        #endregion
    }
}
