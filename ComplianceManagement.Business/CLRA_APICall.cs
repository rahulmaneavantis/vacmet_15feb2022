﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class CLRA_APICall
    {
        #region API CALL
        public bool PrincipleEmployerMasterApiCall(PrincipleEmployerMaster PEMAPI)
        {
            try
            {
                HttpClient client = new HttpClient();
                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCS_API_URL"];
                client.BaseAddress = new Uri(rlcsAPIURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var json = new JavaScriptSerializer().Serialize(PEMAPI);
                List<PrincipleEmployerMaster> lstPrincipleEmployerMaster = new List<PrincipleEmployerMaster>();
                lstPrincipleEmployerMaster.Add(PEMAPI);
                JArray paramList = new JArray();
                var response = client.PostAsJsonAsync<List<PrincipleEmployerMaster>>("api/AventisIntegration/Insert_PrincipleEmployerMaster", lstPrincipleEmployerMaster).Result;

                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public bool PrincipleEmployerLocationMasterApiCall(PrincipleEmployerLocationMaster PELMAPI)
        {
            try
            {
                HttpClient client = new HttpClient();
                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCS_API_URL"];
                client.BaseAddress = new Uri(rlcsAPIURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var json = new JavaScriptSerializer().Serialize(PELMAPI);
                List<PrincipleEmployerLocationMaster> lstPrincipleEmployerLocationMaster = new List<PrincipleEmployerLocationMaster>();
                lstPrincipleEmployerLocationMaster.Add(PELMAPI);
                JArray paramList = new JArray();
                var response = client.PostAsJsonAsync<List<PrincipleEmployerLocationMaster>>("api/AventisIntegration/Insert_PrincipleEmployerLocationMaster", lstPrincipleEmployerLocationMaster).Result;
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public bool PrincipleEmployerContractorApiCall(PrincipleEmployerContractor PECAPI)
        {
            try
            {
                HttpClient client = new HttpClient();
                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCS_API_URL"];
                client.BaseAddress = new Uri(rlcsAPIURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var json = new JavaScriptSerializer().Serialize(PECAPI);
                List<PrincipleEmployerContractor> lstPrincipleEmployerContractor = new List<PrincipleEmployerContractor>();
                lstPrincipleEmployerContractor.Add(PECAPI);
                JArray paramList = new JArray();
                var response = client.PostAsJsonAsync<List<PrincipleEmployerContractor>>("api/AventisIntegration/InsertPrincipleEmployerContractor", lstPrincipleEmployerContractor).Result;
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public bool PrincipleEmployeeMasterApiCall(PrincipleEmployeeMaster PEMAPI)
        {
            try
            {
                HttpClient client = new HttpClient();
                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCS_API_URL"];
                client.BaseAddress = new Uri(rlcsAPIURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var json = new JavaScriptSerializer().Serialize(PEMAPI);
                List<PrincipleEmployeeMaster> lstPrincipleEmployeeMaster = new List<PrincipleEmployeeMaster>();
                lstPrincipleEmployeeMaster.Add(PEMAPI);
                JArray paramList = new JArray();
                var response = client.PostAsJsonAsync<List<PrincipleEmployeeMaster>>("api/AventisIntegration/InsertPrincipleEmployeeMaster", lstPrincipleEmployeeMaster).Result;
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        #endregion
        #region CLRA_JSON Parameters
        public class PrincipleEmployerMaster
        {
            public string PEID { get; set; }
            public string PEName { get; set; }
            public string ClientID { get; set; }
            public string NatureOfBusiness { get; set; }
            public DateTime ContractFrom { get; set; }
            public DateTime ContractTo { get; set; }
            public int NoOfEmployees { get; set; }
            public string Address { get; set; }
            public string CreatedBy { get; set; }
            public double ContractValue { get; set; }
            public double SecurityDeposit { get; set; }
            public string IsCentralAct { get; set; }
            public string PE_PAN { get; set; }
        }
        public class PrincipleEmployerLocationMaster
        {
            public int? PEID { get; set; }
            public int? PELID { get; set; }
            public string State { get; set; }
            public string Location { get; set; }
            public string Branch { get; set; }         
            public string NatureOfBusiness { get; set; }
            public string Mines { get; set; }
            public string WeekOff { get; set; }
            public string PE_LIN { get; set; }
            public string PE_AuthorisedPerson_EmailID { get; set; }
            public string PE_Company_PhoneNo { get; set; }
            public string Client_LINNo { get; set; }
            public string Client_CompanyEmailID { get; set; }
            public string Client_Company_Phone_No { get; set; }
            public string Contract_Licence_No { get; set; }
            public string Licence_Valid_From_date { get; set; }
            public string Licence_Valid_To_date { get; set; }
            public string Contractor_Person_Incharge_Name { get; set; }
            public string Contractor_Person_Incharge_LIN { get; set; }
            public string Contractor_Person_Incharge_PAN { get; set; }
            public string Contractor_Person_Incharge_EmailID { get; set; }
            public string CreatedBy { get; set; }
            public string Contractor_Person_Incharge_MobileNo { get; set; }
            public string Client_Nature_of_business { get; set; }
            public string PE_Address { get; set; }
            public string Contractor_Licensing_Officer_Designation { get; set; }
            public string Licencing_officer_Head_Quarter { get; set; }
            public string Nature_ofwelfare_amenities_provided { get; set; }
            public string Statutory_statute { get; set; }

            public string Address { get; set; }
            public Nullable<int> NumberOfEmp { get; set; }
            public Nullable<System.DateTime> ContractFrom { get; set; }
            public Nullable<System.DateTime> ContractTo { get; set; }
        }
        public class PrincipleEmployerContractor
        {
            public int? PECID { get; set; }
            public int? PEC_PEID { get; set; }
            public int? PEC_PELID { get; set; }
            public string PEC_ContractorName { get; set; }
            public string PEC_Address { get; set; }
            public string PEC_NatureOfWork { get; set; }
            public DateTime? PEC_ContractFrom { get; set; }
            public DateTime? PEC_ContractTo { get; set; }
            public int PEC_NoOfEmployees { get; set; }
            public string PEC_CanteenProvided { get; set; }
            public string PEC_RestroomProvided { get; set; }
            public string PEC_Creches { get; set; }
            public string PEC_DrinkingWater { get; set; }
            public string PEC_FirstAid { get; set; }
            public string PEC_CreatedBy { get; set; }
        }
        public class PrincipleEmployeeMaster
        {
            public string EmpID { get; set; }
            public string PELID { get; set; }
            public Nullable<DateTime> ContractFrom { get; set; }
            public Nullable<DateTime> ContractTo { get; set; }
            public Nullable<DateTime> ContractEndDate { get; set; }
            public string ReasonForContractEnd { get; set; }
            public string status { get; set; }
            public string CreatedBy { get; set; }

        }
        #endregion
        #region UpdateIsprocessStatus
        public static void Update_ProcessedStatus_PrincipleEmployerMaster(int PEID,bool status)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var PEMRecord = (from row in entities.RLCS_PrincipleEmployerMaster
                                             where row.PEID == PEID
                                             select row).FirstOrDefault();
                    if (PEMRecord != null)
                    {
                        PEMRecord.IsProcessed = status;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void Update_ProcessedStatus_PrincipleEmployerLocationMaster(int PELID, bool status)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var PEMRecord = (from row in entities.RLCS_PrincipleEmployerLocationMaster
                                     where row.PELID == PELID
                                     select row).FirstOrDefault();
                    if (PEMRecord != null)
                    {
                        PEMRecord.IsProcessed = status;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void Update_ProcessedStatus_PrincipleEmployeeMaster(int PELID,string EMPID, bool status)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var PELMRecord = (from row in entities.RLCS_PrincipleEmployeeMaster
                                     where row.PELID == PELID && row.EmpID== EMPID
                                     select row).FirstOrDefault();
                    if (PELMRecord != null)
                    {
                        PELMRecord.IsProcessed = status;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void Update_ProcessedStatus_PrincipleEmployerContractor(int PECID, bool status)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var PECMRecord = (from row in entities.RLCS_PrincipleEmployerContractor
                                     where row.PECID == PECID
                                     select row).FirstOrDefault();
                    if (PECMRecord != null)
                    {
                        PECMRecord.IsProcessed = status;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
    }
}
