﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class NewScheduleInbetweenLogicInternal
    {
        public static long GetScheduledOnPresentOrNot(long InternalComplianceInstanceID, DateTime ScheduledOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
                                     where row.InternalComplianceInstanceID == InternalComplianceInstanceID && row.ScheduledOn == ScheduledOn
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row.ID).FirstOrDefault();
                return ScheduleOnID;
            }
        }
        public static long GetNextMonthFromFrequency(long LastforMonth, byte? Frequency, int nForMonth, InternalComplianceScheduledOn Icsdata)
        {
            long NextPeriod;
            switch (Frequency)
            {
                case 0:
                    if (Icsdata == null)
                    {
                        NextPeriod = LastforMonth;
                    }
                    else
                    {
                        if (LastforMonth == 12)
                            NextPeriod = 1;
                        else
                            NextPeriod = LastforMonth + 1;
                    }
                    break;

                case 1:
                    if (Icsdata == null)
                    {
                        NextPeriod = LastforMonth;
                    }
                    else
                    {
                        if (LastforMonth == 10)
                            NextPeriod = 1;                       
                        else if (LastforMonth == 0)
                            if (nForMonth == 4)
                                NextPeriod = 4;
                            else
                                NextPeriod = 1;                      
                        else
                            NextPeriod = LastforMonth + 3;
                    }
                    break;

                case 2:
                    if (Icsdata == null)
                    {
                        NextPeriod = LastforMonth;
                    }
                    else
                    {
                        if (LastforMonth == 7)
                            NextPeriod = 1;
                        else if (LastforMonth == 10)
                            NextPeriod = 4;                       
                        else if (LastforMonth == 0)
                            if (nForMonth == 4)
                                NextPeriod = 4;
                            else
                                NextPeriod = 1;                     
                        else
                            NextPeriod = LastforMonth + 6;
                    }
                    break;


                case 3:
                    if (Icsdata == null)
                    {
                        NextPeriod = LastforMonth;
                    }
                    else
                    {
                        if (LastforMonth == 4)
                        {
                            NextPeriod = 4;
                        }                      
                        else if (LastforMonth == 0)
                            if (nForMonth == 4)
                                NextPeriod = 4;
                            else
                                NextPeriod = 1;                    
                        else
                        {
                            NextPeriod = 1;
                        }
                    }
                    break;

                case 4:
                    if (Icsdata == null)
                    {
                        NextPeriod = LastforMonth;
                    }
                    else
                    {
                        if (LastforMonth == 9)
                            NextPeriod = 1;
                        else if (LastforMonth == 12)
                            NextPeriod = 4;                      
                        else if (LastforMonth == 0)
                            if (nForMonth == 4)
                                NextPeriod = 4;
                            else
                                NextPeriod = 1;                       
                        else
                            NextPeriod = LastforMonth + 4;
                    }
                    break;
                case 5:
                    if (Icsdata == null)
                    {
                        NextPeriod = LastforMonth;
                    }
                    else
                    {
                        if (LastforMonth == 4)
                        {
                            NextPeriod = 4;
                        }
                        else
                        {
                            NextPeriod = 1;
                        }
                    }
                    break;

                case 6:
                    if (Icsdata == null)
                    {
                        NextPeriod = LastforMonth;
                    }
                    else
                    {
                        if (LastforMonth == 4)
                        {
                            NextPeriod = 4;
                        }
                        else
                        {
                            NextPeriod = 1;
                        }
                    }
                    break;

                default:
                    NextPeriod = 0;
                    break;
            }

            return NextPeriod;
        }
        public static long GetLastPeriodFromDate(int forMonth, byte? Frequency)
        {
            long forMonthName = 0;
            switch (Frequency)
            {
                case 0:
                    if (forMonth == 1)
                    {
                        forMonthName = 12;
                    }
                    else
                    {
                        forMonthName = forMonth - 1;
                    }
                    break;
                case 1:
                    if (forMonth == 1)
                    {
                        forMonthName = 10;
                    }
                    else
                    {
                        forMonthName = forMonth - 3;
                    }
                    break;
                case 2:
                    if (forMonth == 1)
                    {
                        forMonthName = 7;
                    }
                    else
                    {
                        forMonthName = forMonth - 6;
                    }
                    break;

                case 3:
                    forMonthName = 1;
                    break;

                case 4:

                    if (forMonth == 1)
                    {
                        forMonthName = 9;
                    }
                    else
                    {
                        forMonthName = forMonthName - 4;
                    }

                    break;
                case 5:
                    forMonthName = 1;
                    break;
                case 6:

                    forMonthName = 1;
                    break;

                default:
                    forMonthName = 0;
                    break;
            }

            return forMonthName;
        }
        public static Tuple<DateTime, string, long> GetNextDate(DateTime scheduledOn, long InternalComplianceID, long InternalComplianceInstanceID,List<InternalComplianceSchedule> ICList,int frequency, InternalComplianceScheduledOn Icsdata,DateTime getinstancescheduleondate)
        {            
            var complianceSchedule = ICList;          
            var lastSchedule = Icsdata;
           
            long lastPeriod = 0;

            if (lastSchedule==null)
            {
                #region Monthly
                if (frequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region Quarterly
                else if (frequency == 1)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {                    
                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                        {
                            lastPeriod = 1;
                        }
                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 6)
                        {
                            lastPeriod = 4;
                        }
                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                        {
                            lastPeriod = 7;
                        }
                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                        {
                            lastPeriod = 10;
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
                #endregion
                #region   Half Yearly
                else if (frequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {                        
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                            {
                                lastPeriod = 4;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                            {
                                lastPeriod = 10;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (frequency == 3)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            lastPeriod = 1;
                        }
                        else
                        {
                            lastPeriod = 4;
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (frequency == 4)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 5 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 9 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                            {
                                lastPeriod = 4;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 11)
                            {
                                lastPeriod = 8;
                            }
                            else
                            {
                                lastPeriod = 12;
                            }

                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (frequency == 5)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            lastPeriod = 1;
                        }
                        else
                        {
                            lastPeriod = 4;
                        }
                    }
                }
                #endregion
                #region Seven Yearly
                else if (frequency == 6)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            lastPeriod = 1;
                        }
                        else
                        {
                            lastPeriod = 4;
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }
            else
            {
                lastPeriod =(long) Icsdata.ForPeriod;
            }                      
            List<Tuple<DateTime, long>> complianceScheduleDates;
            int ForMonth = 0; 


            if (frequency != 0 || frequency != 1)
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();                                     
                    else if (frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year , Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    ForMonth = 1;//for Calender year 
                }
                else
                {
                    if (frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();                    
                    ForMonth = 4;// for Financial year
                }
                #endregion


                #region  Second Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (frequency == 0)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 1)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 2)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 3)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 4)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 5)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                        //complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    }
                    else if (frequency == 6)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                        //complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    }
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    
                }
                else
                {
                    if (frequency == 0)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 1)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 2)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 3)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 4)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                    }
                    else if (frequency == 5)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                        //complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    }
                    else if (frequency == 6)
                    {
                        var newlistnextyear = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates.AddRange(newlistnextyear);
                        //complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    }
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    
                }
                #endregion                
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }            
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (frequency == 0)
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency, ForMonth, Icsdata);
            }
            else if (frequency == 1)
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency, ForMonth, Icsdata);
            }
            else if (frequency == 2)
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency, ForMonth, Icsdata);
            }
            else if (frequency == 3)
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency, ForMonth, Icsdata);
            }
            else if (frequency == 4)
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency, ForMonth, Icsdata);
            }
            else if (frequency == 5)
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency, ForMonth, Icsdata);
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency, ForMonth, Icsdata);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                if (frequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(x => x.Item1).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            //date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            //if (complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1.Year == getinstancescheduleondate.Year)
                            //{
                            //    date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            //}
                            //else
                            //{
                            //    ActualForMonth += 1;
                            //    date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            //}
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        //complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(x => x.Item1).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            //if (complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1.Year == getinstancescheduleondate.Year)
                            //{
                            //    date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            //}
                            //else
                            //{
                            //    ActualForMonth += 3;
                            //    date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            //}
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }

                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(scheduledOn)).ToList();                    
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(scheduledOn)).ToList();                    
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(x => x.Item1).ToList(); 
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();                      
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(x => x.Item1).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(x => x.Item1).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        //complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(InternalComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduledOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), (byte)frequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }
        public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    #region 0
                    string year = date.ToString("yy");                   
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    #endregion                    
                    break;
                case 1:
                    #region 1
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;                    
                    #endregion
                    break;
                case 2:
                    #region 2
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {                      
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                               " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    if (forMonth == 10)
                    {                       
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        if (year1== date.ToString("yy"))
                        {
                            forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                     " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + (date.AddYears(1)).ToString("yy");

                        }
                        else
                        {
                            forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                        }
                       
                    }
                    if (forMonth == 4)
                    {
                        year1 = (date).ToString("yy");                      
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(9).Substring(0, 3) + " " + year1;
                    }
                    if (forMonth == 1)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    #endregion
                    break;
                case 3:
                    #region 3 
                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        endFinancial1Year = (date.AddYears(1)).ToString("yy");
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        if (date.Month == 3)
                        {
                            startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                            endFinancial1Year = date.ToString("yy");
                        }
                        else
                        {
                            startFinancial1Year = (date).ToString("yy");
                            endFinancial1Year = (date.AddYears(1)).ToString("yy");
                        }                        
                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }                   
                    #endregion
                    break;
                case 4:
                    #region 4
                    string year2 = date.ToString("yy");
                    //if (forMonth == 1)
                    //{
                    //    year2 = (date.AddYears(1)).ToString("yy");
                    //}
                    if (forMonth == 12)
                    {
                        if (year2 == DateTime.Now.ToString("yy"))
                        {
                            forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;

                        }
                        else
                        {
                            year2 = (date.AddYears(1)).ToString("yy");
                            forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date).ToString("yy") +
                                 " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                          
                        }
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                                       " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    }
                    //if (forMonth == 9)
                    //{
                    //    if (Convert.ToInt32(year2) >= Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                    //    {
                    //        year2 = (date.AddYears(-1)).ToString("yy");
                    //    }
                    //    else
                    //    {
                    //        year2 = (date).ToString("yy");
                    //    }
                    //}
                    //if (forMonth == 12)
                    //{
                    //    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                    //              " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                    //}
                    //else
                    //{
                    //    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                    //                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    //}
                    #endregion
                    break;
                case 5:
                    #region 5
                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    else if (forMonth == 4)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                        forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    #endregion
                    break;
                case 6:
                    #region 6
                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    #endregion                    
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }
        public static bool CreateTransaction(InternalComplianceTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            try
                            {
                                transaction.Dated = DateTime.Now;
                                entities.InternalComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                                dbtransaction.Commit();
                            }
                            catch (Exception ex)
                            {

                                var aa = ex.Message.ToString();
                            }
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<LogMessage> ReminderUserList = new List<LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)0;
                ReminderUserList.Add(msg);
                //Commanclass.InsertLogToDatabase(ReminderUserList);
                //List<string> TO = new List<string>();
                //TO.Add("ajit@avantis.co.in");
                //TO.Add("rahul@avantis.co.in");
                //TO.Add("sachin@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as Internal CreateTransaction Function", "Internal CreateTransaction");

                ComplianceManagement.SendgridSenEmail("Error Occured as Internal CreateTransaction Function", "Internal CreateTransaction");
                return false;
            }
        }
        public static List<long> GetUpcomingReminderDaysDetail(long ComplianceAssignmentId, int FreqID, string Type)
        {
            List<long> output = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User User = (from row in entities.Users
                             join row1 in entities.InternalComplianceAssignments
                             on row.ID equals row1.UserID
                             where row1.ID == ComplianceAssignmentId
                             && row.IsActive == true
                             && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (User != null)
                {
                    long customerID = Convert.ToInt32(User.CustomerID);
                    long UserID = Convert.ToInt32(User.ID);

                    var UserObj = (from row in entities.ReminderTemplate_UserMapping
                                   where row.CustomerID == customerID
                                   && row.UserID == UserID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   && row.Frequency == FreqID
                                   && row.Flag == "UN"
                                   select row).ToList();

                    if (UserObj.Count > 0)
                    {
                        output.Clear();
                        output = UserObj.Select(x => x.TimeInDays).ToList();
                    }
                    else
                    {
                        var CustObj = (from row in entities.ReminderTemplate_CustomerMapping
                                       where row.CustomerID == customerID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       && row.Frequency == FreqID
                                       && row.Flag == "UN"
                                       select row).ToList();

                        if (CustObj.Count > 0)
                        {
                            output.Clear();
                            output = CustObj.Select(x => x.TimeInDays).ToList();
                        }
                        else
                        {
                            output.Clear();
                            //var StdObj = (from row in entities.ReminderTemplates
                            //              where row.Frequency == FreqID
                            //              && row.IsSubscribed == true
                            //              select row).ToList();

                            //if (StdObj.Count > 0)
                            //{
                            //    output.Clear();
                            //    output = StdObj.Select(x => Convert.ToInt64(x.TimeInDays)).ToList();
                            //}
                        }
                    }
                }
                return output;
            }
        }
        public static void CreateReminders(long InternalComplianceID, long InternalComplianceInstanceId, int InternalComplianceAssignmentId, DateTime scheduledOn)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    InternalCompliance compliance = (from row in entities.InternalCompliances
                                                     where row.ID == InternalComplianceID
                                                     select row).FirstOrDefault();
                    if (compliance.IFrequency == 7)
                    {
                        #region Frequency Daily
                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date,
                        };
                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder);
                        #endregion
                    }
                    else if (compliance.IFrequency == 8)
                    {
                        #region Frequency Weekly
                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date,
                        };
                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder);
                        InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                        {
                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                        };
                        reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.InternalComplianceReminders.Add(reminder1);
                        #endregion
                    }
                    else
                    {
                        List<long> getdays = GetUpcomingReminderDaysDetail(InternalComplianceAssignmentId, Convert.ToInt32(compliance.IFrequency), "I");
                        if (getdays.Count > 0)
                        {
                            List<InternalComplianceScheduledOn> ScheduledOnList = (from row in entities.InternalComplianceScheduledOns
                                                                                   where row.InternalComplianceInstanceID == InternalComplianceInstanceId
                                                                                   && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                                                                   select row).ToList();
                            foreach (var item1 in ScheduledOnList)
                            {
                                foreach (var item in getdays)
                                {
                                    InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                    {
                                        ComplianceAssignmentID = InternalComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = item1.ScheduledOn,
                                        RemindOn = item1.ScheduledOn.Date.AddDays(-item),
                                    };
                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.InternalComplianceReminders.Add(reminder1);
                                }
                            }
                        }
                        else
                        {
                            #region Normal Frequency
                            if (compliance.ISubComplianceType == 0)
                            {

                                if (compliance.IReminderType == 0)
                                {
                                    InternalComplianceReminder reminder = new InternalComplianceReminder()
                                    {
                                        ComplianceAssignmentID = InternalComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.InternalComplianceReminders.Add(reminder);


                                    InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                                    {
                                        ComplianceAssignmentID = InternalComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = scheduledOn,
                                        RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                                    };
                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.InternalComplianceReminders.Add(reminder1);
                                }
                                else
                                {
                                    DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));
                                    while (TempScheduled.Date < scheduledOn)
                                    {
                                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                                        {
                                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = TempScheduled.Date,
                                        };

                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.InternalComplianceReminders.Add(reminder);
                                        TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                                    }
                                }
                            }//Function Based
                            else
                            {

                                if (compliance.IReminderType == 0)
                                {
                                    if (compliance.IFrequency.HasValue)
                                    {
                                        var reminders = (from row in entities.ReminderTemplates
                                                         where row.Frequency == compliance.IFrequency.Value && row.IsSubscribed == true
                                                         select row).ToList();

                                        reminders.ForEach(day =>
                                        {
                                            InternalComplianceReminder reminder = new InternalComplianceReminder()
                                            {
                                                ComplianceAssignmentID = InternalComplianceAssignmentId,
                                                ReminderTemplateID = day.ID,
                                                ComplianceDueDate = scheduledOn,
                                                RemindOn = scheduledOn.Date.AddDays(-1 * day.TimeInDays),
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.InternalComplianceReminders.Add(reminder);
                                        });
                                    }
                                }
                                else
                                {
                                    DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.IReminderBefore)));
                                    while (TempScheduled.Date < scheduledOn)
                                    {
                                        InternalComplianceReminder reminder = new InternalComplianceReminder()
                                        {
                                            ComplianceAssignmentID = InternalComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = TempScheduled.Date,
                                        };

                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.InternalComplianceReminders.Add(reminder);
                                        TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.IReminderGap));
                                    }
                                }
                            }//function Based End
                            #endregion
                        }
                    }
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                List<LogMessage> ReminderUserList = new List<LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "NewScheduleClassInternal";
                msg.FunctionName = "CreateReminders" + "InternalComplianceAssignmentId" + InternalComplianceAssignmentId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)0;
                ReminderUserList.Add(msg);
                //Commanclass.InsertLogToDatabase(ReminderUserList);
                //List<string> TO = new List<string>();
                //TO.Add("ajit@avantis.co.in");
                //TO.Add("rahul@avantis.co.in");
                //TO.Add("sachin@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as Internal CreateReminders Function", "Internal CreateReminders" + "InternalComplianceAssignmentId" + InternalComplianceAssignmentId);

                ComplianceManagement.SendgridSenEmail("Error Occured as Internal CreateReminders Function", "Internal CreateReminders" + "InternalComplianceAssignmentId" + InternalComplianceAssignmentId);
            }
        }
        public static List<InternalComplianceSchedule> GetScheduleByComplianceID(long internalcomplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.InternalComplianceSchedules
                                    where row.IComplianceID == internalcomplianceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }      
        public static Tuple<DateTime, string, long> GetNextDatePeriodically(DateTime InternalscheduledOn, long InternalComplianceID,int Frequency,List<InternalComplianceSchedule> Icslist)
        {
            int formonthscheduleR = -1;
            //var complianceSchedule = GetScheduleByComplianceID(InternalComplianceID).OrderBy(entry => entry.ForMonth).ToList();
            var complianceSchedule = Icslist;
            //var objCompliance = GetByID(InternalComplianceID);

            DateTime date = complianceSchedule.Select(row => new DateTime(InternalscheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > InternalscheduledOn).FirstOrDefault();

            if (Frequency == 3 && date != DateTime.MinValue)
            {
                date = new DateTime(InternalscheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (Frequency == 5 && date != DateTime.MinValue)
            {
                date = new DateTime(InternalscheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (Frequency == 6 && date != DateTime.MinValue)
            {
                date = new DateTime(InternalscheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(InternalscheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (Frequency == 5)
                    {
                        date = new DateTime(InternalscheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (Frequency == 6)
                    {
                        date = new DateTime(InternalscheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }
            string forMonth = string.Empty;
            if (complianceSchedule.Count > 0)
            {
                string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");
                var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth,(byte)Frequency);
                formonthscheduleR = ForMonthSchedule.ForMonth;
            }
            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);
        }
        public static string GetForMonthPeriodically(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 12;
                        year = (date.AddYears(-1)).ToString("yy");
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 10;
                        yearq = (date.AddYears(-1)).ToString("yy");
                    }


                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 2)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 7;
                        year1 = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 5)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1;
                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = date.ToString("yy");
                        endFinancial1Year = (date.AddYears(1)).ToString("yy"); date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    break;

                case 4:
                    string year2 = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 9;
                        year2 = (date.AddYears(-1)).ToString("yy");
                    }


                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 3)).Substring(0, 3) + " " + year2 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2;
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial2Year = (date.AddYears(1)).ToString("yy");
                    }
                    else
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;

                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-6)).ToString("yy");
                        endFinancial7Year = (date.AddYears(1)).ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }      
        public static InternalComplianceScheduledOn GetLastScheduleOnByInstanceInternal(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.InternalComplianceScheduledOns
                                   where row.InternalComplianceInstanceID == complianceInstanceID
                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                   orderby row.ScheduledOn descending, row.ID descending
                                   select row).FirstOrDefault();
               
                return scheduleObj;
            }
        }
        public static Tuple<DateTime, string, long> GetNextDate1(DateTime scheduledOn, long complianceID, long ComplianceInstanceID)
        {
            return new Tuple<DateTime, string, long>(scheduledOn, Convert.ToString(complianceID), ComplianceInstanceID);
        }
        public static List<InternalComplianceAssignment> GetAssignedUsers(int InternalComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.InternalComplianceAssignments
                                     where row.InternalComplianceInstanceID == InternalComplianceInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }
        //public static void GenerateScheduleOnForUpcomingInternal()
        //{
        //    long testid = 0;
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            DateTime endDate = DateTime.Now;
        //            DateTime curruntDate = DateTime.Now;
        //            List<SP_GetAllInstanceOfCurrentDateInternal_Result> masterResult = new List<SP_GetAllInstanceOfCurrentDateInternal_Result>();
        //            List<SP_GetAllInstanceOfCurrentDateInternal_Result> transactionresult = new List<SP_GetAllInstanceOfCurrentDateInternal_Result>();
        //            entities.Database.CommandTimeout = 180;
        //            masterResult = (from CI in entities.SP_GetAllInstanceOfCurrentDateInternal((int)DateTime.Today.Day, "INB")
        //                            select CI).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

        //            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\InternalComplianceInstanceIDs.txt";
        //            if (System.IO.File.Exists(fileName))
        //            {
        //                List<long> listofIDs = new List<long>();
        //                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
        //                {
        //                    string checktextfiledata = sr.ReadLine();
        //                    if (checktextfiledata != null)
        //                    {
        //                        listofIDs = checktextfiledata.Split(',').Select(t => long.Parse(t)).ToList();
        //                        transactionresult = masterResult.Where(entry => listofIDs.Contains(entry.ID)).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
        //                    }
        //                    else
        //                    {
        //                        transactionresult = masterResult.GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                transactionresult = masterResult.GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
        //            }
        //            var lic_complianceInstanceIDlist = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
        //                                                select row.ComplianceInstanceID).ToList();

        //            if (lic_complianceInstanceIDlist.Count > 0)
        //            {
        //                transactionresult = transactionresult.Where(entry => !lic_complianceInstanceIDlist.Contains(entry.ID)).ToList();
        //            }
        //            int cnt = Convert.ToInt32(ConfigurationManager.AppSettings["countofloop"]);
        //            transactionresult.ForEach(entry =>
        //            {
        //                try
        //                {
        //                    endDate = (DateTime)entry.EndDate;
        //                    for (int i = 1; i <= cnt; i++)
        //                    {
        //                        var lastcomplianceScheduleon = GetLastScheduleOnByInstanceInternal(entry.ID);
        //                        var internalcomplianceSchedule = GetScheduleByComplianceID(entry.InternalComplianceID);
        //                        if (entry.IFrequency == 7)
        //                        {
        //                            #region Schedule Daily Frequency 
        //                            testid = entry.ID;
        //                            string ic = "1/1/0001 12:00:00 AM";
        //                            Tuple<DateTime, string, long> nextDateMonth;
        //                            if (lastcomplianceScheduleon != null)
        //                            {
        //                                nextDateMonth = new Tuple<DateTime, string, long>(lastcomplianceScheduleon.ScheduledOn.AddDays(1).Date, "", 0);
        //                                if (nextDateMonth.Item1 != Convert.ToDateTime(ic))
        //                                {
        //                                    DateTime ToDate = DateTime.UtcNow.AddDays(3);
        //                                    while (nextDateMonth.Item1 < ToDate)
        //                                    {
        //                                        long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
        //                                                             where row.InternalComplianceInstanceID == entry.ID
        //                                                            && row.ScheduledOn == nextDateMonth.Item1
        //                                                            && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                                             select row.ID).FirstOrDefault();

        //                                        if (ScheduleOnID <= 0)
        //                                        {
        //                                            if (nextDateMonth.Item1.Date.DayOfWeek != DayOfWeek.Sunday)
        //                                            {
        //                                                InternalComplianceScheduledOn internalcomplianceScheduleon = new InternalComplianceScheduledOn();
        //                                                internalcomplianceScheduleon.InternalComplianceInstanceID = entry.ID;
        //                                                internalcomplianceScheduleon.ScheduledOn = nextDateMonth.Item1.Date;
        //                                                internalcomplianceScheduleon.ForMonth = nextDateMonth.Item2;
        //                                                internalcomplianceScheduleon.ForPeriod = nextDateMonth.Item3;
        //                                                internalcomplianceScheduleon.IsActive = true;
        //                                                internalcomplianceScheduleon.IsUpcomingNotDeleted = true;
        //                                                entities.InternalComplianceScheduledOns.Add(internalcomplianceScheduleon);
        //                                                entities.SaveChanges();

        //                                                var AssignedRole = GetAssignedUsers((int)entry.ID);
        //                                                var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
        //                                                if (performerRole != null)
        //                                                {
        //                                                    var user = UserManagement.GetByID((int)performerRole.UserID);
        //                                                    InternalComplianceTransaction transaction = new InternalComplianceTransaction()
        //                                                    {
        //                                                        InternalComplianceInstanceID = entry.ID,
        //                                                        InternalComplianceScheduledOnID = internalcomplianceScheduleon.ID,
        //                                                        CreatedBy = performerRole.UserID,
        //                                                        CreatedByText = user.FirstName + " " + user.LastName,
        //                                                        StatusId = 1,
        //                                                        Remarks = "New Internal compliance assigned."
        //                                                    };

        //                                                    CreateTransaction(transaction);
        //                                                    foreach (var roles in AssignedRole)
        //                                                    {
        //                                                        if (roles.RoleID != 6)
        //                                                        {
        //                                                            CreateReminders(entry.InternalComplianceID, entry.ID, roles.ID, nextDateMonth.Item1);
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                        nextDateMonth = new Tuple<DateTime, string, long>(nextDateMonth.Item1.AddDays(1).Date, "", 0);
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                nextDateMonth = new Tuple<DateTime, string, long>(entry.ScheduledOn.Date, "", 0);
        //                                if (nextDateMonth.Item1 != Convert.ToDateTime(ic))
        //                                {
        //                                    DateTime ToDate = DateTime.UtcNow.AddDays(3);
        //                                    while (nextDateMonth.Item1 < ToDate)
        //                                    {
        //                                        long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
        //                                                             where row.InternalComplianceInstanceID == entry.ID
        //                                                            && row.ScheduledOn == nextDateMonth.Item1
        //                                                            && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                                             select row.ID).FirstOrDefault();

        //                                        if (ScheduleOnID <= 0)
        //                                        {
        //                                            if (nextDateMonth.Item1.Date.DayOfWeek != DayOfWeek.Sunday)
        //                                            {
        //                                                InternalComplianceScheduledOn internalcomplianceScheduleon = new InternalComplianceScheduledOn();
        //                                                internalcomplianceScheduleon.InternalComplianceInstanceID = entry.ID;
        //                                                internalcomplianceScheduleon.ScheduledOn = nextDateMonth.Item1.Date;
        //                                                internalcomplianceScheduleon.ForMonth = nextDateMonth.Item2;
        //                                                internalcomplianceScheduleon.ForPeriod = nextDateMonth.Item3;
        //                                                internalcomplianceScheduleon.IsActive = true;
        //                                                internalcomplianceScheduleon.IsUpcomingNotDeleted = true;
        //                                                entities.InternalComplianceScheduledOns.Add(internalcomplianceScheduleon);
        //                                                entities.SaveChanges();

        //                                                var AssignedRole = GetAssignedUsers((int)entry.ID);
        //                                                var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
        //                                                if (performerRole != null)
        //                                                {
        //                                                    var user = UserManagement.GetByID((int)performerRole.UserID);
        //                                                    InternalComplianceTransaction transaction = new InternalComplianceTransaction()
        //                                                    {
        //                                                        InternalComplianceInstanceID = entry.ID,
        //                                                        InternalComplianceScheduledOnID = internalcomplianceScheduleon.ID,
        //                                                        CreatedBy = performerRole.UserID,
        //                                                        CreatedByText = user.FirstName + " " + user.LastName,
        //                                                        StatusId = 1,
        //                                                        Remarks = "New Internal compliance assigned."
        //                                                    };

        //                                                    CreateTransaction(transaction);
        //                                                    foreach (var roles in AssignedRole)
        //                                                    {
        //                                                        if (roles.RoleID != 6)
        //                                                        {
        //                                                            CreateReminders(entry.InternalComplianceID, entry.ID, roles.ID, nextDateMonth.Item1);
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                        nextDateMonth = new Tuple<DateTime, string, long>(nextDateMonth.Item1.AddDays(1).Date, "", 0);
        //                                    }
        //                                }
        //                            }
        //                            #endregion
        //                        }
        //                        else if (entry.IFrequency == 8)
        //                        {
        //                            #region Schedule Weekly Frequency                            
        //                            testid = entry.ID;
        //                            string ic = "1/1/0001 12:00:00 AM";
        //                            Tuple<DateTime, string, long> nextDateMonth;
        //                            if (lastcomplianceScheduleon != null)
        //                            {
        //                                nextDateMonth = new Tuple<DateTime, string, long>(lastcomplianceScheduleon.ScheduledOn.AddDays(7).Date, "", 0);
        //                                if (nextDateMonth.Item1 != Convert.ToDateTime(ic))
        //                                {
        //                                    DateTime ToDate = DateTime.UtcNow.AddDays(30);
        //                                    while (nextDateMonth.Item1 < ToDate)
        //                                    {
        //                                        long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
        //                                                             where row.InternalComplianceInstanceID == entry.ID
        //                                                            && row.ScheduledOn == nextDateMonth.Item1
        //                                                            && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                                             select row.ID).FirstOrDefault();

        //                                        if (ScheduleOnID <= 0)
        //                                        {
        //                                            if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
        //                                            {
        //                                                nextDateMonth = new Tuple<DateTime, string, long>(nextDateMonth.Item1.AddDays(1).Date, "", 0);
        //                                            }

        //                                            InternalComplianceScheduledOn internalcomplianceScheduleon = new InternalComplianceScheduledOn();
        //                                            internalcomplianceScheduleon.InternalComplianceInstanceID = entry.ID;
        //                                            internalcomplianceScheduleon.ScheduledOn = nextDateMonth.Item1.Date;
        //                                            internalcomplianceScheduleon.ForMonth = nextDateMonth.Item2;
        //                                            internalcomplianceScheduleon.ForPeriod = nextDateMonth.Item3;
        //                                            internalcomplianceScheduleon.IsActive = true;
        //                                            internalcomplianceScheduleon.IsUpcomingNotDeleted = true;
        //                                            entities.InternalComplianceScheduledOns.Add(internalcomplianceScheduleon);
        //                                            entities.SaveChanges();

        //                                            var AssignedRole = GetAssignedUsers((int)entry.ID);
        //                                            var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
        //                                            if (performerRole != null)
        //                                            {
        //                                                var user = UserManagement.GetByID((int)performerRole.UserID);
        //                                                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
        //                                                {
        //                                                    InternalComplianceInstanceID = entry.ID,
        //                                                    InternalComplianceScheduledOnID = internalcomplianceScheduleon.ID,
        //                                                    CreatedBy = performerRole.UserID,
        //                                                    CreatedByText = user.FirstName + " " + user.LastName,
        //                                                    StatusId = 1,
        //                                                    Remarks = "New Internal compliance assigned."
        //                                                };

        //                                                CreateTransaction(transaction);
        //                                                foreach (var roles in AssignedRole)
        //                                                {
        //                                                    if (roles.RoleID != 6)
        //                                                    {
        //                                                        CreateReminders(entry.InternalComplianceID, entry.ID, roles.ID, nextDateMonth.Item1.Date);
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                        nextDateMonth = new Tuple<DateTime, string, long>(nextDateMonth.Item1.AddDays(7).Date, "", 0);
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                nextDateMonth = new Tuple<DateTime, string, long>(entry.ScheduledOn.Date, "", 0);
        //                                if (nextDateMonth.Item1 != Convert.ToDateTime(ic))
        //                                {
        //                                    DateTime ToDate = DateTime.UtcNow.AddDays(30);
        //                                    while (nextDateMonth.Item1 < ToDate)
        //                                    {
        //                                        long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
        //                                                             where row.InternalComplianceInstanceID == entry.ID
        //                                                            && row.ScheduledOn == nextDateMonth.Item1
        //                                                            && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                                             select row.ID).FirstOrDefault();

        //                                        if (ScheduleOnID <= 0)
        //                                        {
        //                                            if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
        //                                            {
        //                                                nextDateMonth = new Tuple<DateTime, string, long>(nextDateMonth.Item1.AddDays(1).Date, "", 0);
        //                                            }

        //                                            InternalComplianceScheduledOn internalcomplianceScheduleon = new InternalComplianceScheduledOn();
        //                                            internalcomplianceScheduleon.InternalComplianceInstanceID = entry.ID;
        //                                            internalcomplianceScheduleon.ScheduledOn = nextDateMonth.Item1.Date;
        //                                            internalcomplianceScheduleon.ForMonth = nextDateMonth.Item2;
        //                                            internalcomplianceScheduleon.ForPeriod = nextDateMonth.Item3;
        //                                            internalcomplianceScheduleon.IsActive = true;
        //                                            internalcomplianceScheduleon.IsUpcomingNotDeleted = true;
        //                                            entities.InternalComplianceScheduledOns.Add(internalcomplianceScheduleon);
        //                                            entities.SaveChanges();

        //                                            var AssignedRole = GetAssignedUsers((int)entry.ID);
        //                                            var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
        //                                            if (performerRole != null)
        //                                            {
        //                                                var user = UserManagement.GetByID((int)performerRole.UserID);
        //                                                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
        //                                                {
        //                                                    InternalComplianceInstanceID = entry.ID,
        //                                                    InternalComplianceScheduledOnID = internalcomplianceScheduleon.ID,
        //                                                    CreatedBy = performerRole.UserID,
        //                                                    CreatedByText = user.FirstName + " " + user.LastName,
        //                                                    StatusId = 1,
        //                                                    Remarks = "New Internal compliance assigned."
        //                                                };

        //                                                CreateTransaction(transaction);
        //                                                foreach (var roles in AssignedRole)
        //                                                {
        //                                                    if (roles.RoleID != 6)
        //                                                    {
        //                                                        CreateReminders(entry.InternalComplianceID, entry.ID, roles.ID, nextDateMonth.Item1.Date);
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                        nextDateMonth = new Tuple<DateTime, string, long>(nextDateMonth.Item1.AddDays(7).Date, "", 0);
        //                                    }
        //                                }
        //                            }
        //                            #endregion
        //                        }
        //                        else
        //                        {
        //                            #region Schedule Normal Frequency
        //                            testid = entry.ID;
        //                            string ic = "1/1/0001 12:00:00 AM";
        //                            Tuple<DateTime, string, long> nextDateMonth;
        //                            if (entry.ISubComplianceType == 0)
        //                            {
        //                                if (lastcomplianceScheduleon != null)
        //                                {
        //                                    if (lastcomplianceScheduleon.ScheduledOn.AddDays(Convert.ToDouble(entry.IDueDate)).Date <= curruntDate.AddDays(30).Date)
        //                                    {
        //                                        nextDateMonth = new Tuple<DateTime, string, long>(lastcomplianceScheduleon.ScheduledOn.AddDays(Convert.ToDouble(entry.IDueDate)).Date, "", 0);
        //                                    }
        //                                    else
        //                                    {
        //                                        nextDateMonth = new Tuple<DateTime, string, long>(lastcomplianceScheduleon.ScheduledOn.Date, "", 0);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    if (entry.EffectiveDate != null)
        //                                    {
        //                                        DateTime rahuldt = Convert.ToDateTime(entry.EffectiveDate);
        //                                        nextDateMonth = new Tuple<DateTime, string, long>(rahuldt.AddDays(Convert.ToDouble(entry.IDueDate)), "", 0);
        //                                    }
        //                                    else
        //                                    {
        //                                        DateTime rahuldt = Convert.ToDateTime(entry.ScheduledOn);
        //                                        nextDateMonth = new Tuple<DateTime, string, long>(rahuldt.AddDays(Convert.ToDouble(entry.IDueDate)), "", 0);
        //                                    }
        //                                }
        //                            }
        //                            else if (entry.ISubComplianceType == 1 || entry.ISubComplianceType == 2)
        //                            {
        //                                //var parameterDate = DateTime.ParseExact(curruntDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture);                                
        //                                var parameterDate = DateTime.ParseExact(endDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture);
        //                                if (lastcomplianceScheduleon != null)
        //                                {
        //                                    if (lastcomplianceScheduleon.ScheduledOn.Date > parameterDate.Date)
        //                                    {
        //                                        nextDateMonth = GetNextDate1(Convert.ToDateTime(ic), entry.InternalComplianceID, entry.ID);
        //                                    }
        //                                    else
        //                                    {
        //                                        nextDateMonth = GetNextDatePeriodically(lastcomplianceScheduleon.ScheduledOn, entry.InternalComplianceID, (int)entry.IFrequency, internalcomplianceSchedule);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    DateTime? getinstancescheduleondate = entry.ScheduledOn;
        //                                    if (getinstancescheduleondate != null)
        //                                    {
        //                                        DateTime rahuldt = new DateTime();
        //                                        rahuldt = Convert.ToDateTime(getinstancescheduleondate);
        //                                        nextDateMonth = GetNextDatePeriodically(rahuldt, entry.InternalComplianceID, (int)entry.IFrequency, internalcomplianceSchedule);
        //                                    }
        //                                    else
        //                                    {
        //                                        nextDateMonth = GetNextDatePeriodically(curruntDate, entry.InternalComplianceID, (int)entry.IFrequency, internalcomplianceSchedule);
        //                                    }
        //                                }
        //                            }
        //                            else if (entry.ISubComplianceType == null && entry.IFrequency == 5)
        //                            {
        //                                var parameterDate = DateTime.ParseExact(endDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture);
        //                                // var parameterDate = DateTime.ParseExact(curruntDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture);                                
        //                                if (lastcomplianceScheduleon != null)
        //                                {
        //                                    if (lastcomplianceScheduleon.ScheduledOn.Date > parameterDate.Date)
        //                                    {
        //                                        nextDateMonth = GetNextDate1(Convert.ToDateTime(ic), entry.InternalComplianceID, entry.ID);
        //                                    }
        //                                    else
        //                                    {
        //                                        nextDateMonth = GetNextDate(lastcomplianceScheduleon.ScheduledOn, entry.InternalComplianceID, entry.ID, internalcomplianceSchedule, (int)entry.IFrequency, lastcomplianceScheduleon, entry.ScheduledOn);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    DateTime? getinstancescheduleondate = entry.ScheduledOn;
        //                                    if (getinstancescheduleondate != null)
        //                                    {
        //                                        DateTime rahuldt = new DateTime();
        //                                        rahuldt = Convert.ToDateTime(getinstancescheduleondate);
        //                                        nextDateMonth = GetNextDate(Convert.ToDateTime(rahuldt), entry.InternalComplianceID, entry.ID, internalcomplianceSchedule, (int)entry.IFrequency, lastcomplianceScheduleon, entry.ScheduledOn);
        //                                    }
        //                                    else
        //                                    {
        //                                        nextDateMonth = GetNextDate(curruntDate, entry.InternalComplianceID, entry.ID, internalcomplianceSchedule, (int)entry.IFrequency, lastcomplianceScheduleon, entry.ScheduledOn);
        //                                    }
        //                                }
        //                            }
        //                            else if (entry.IFrequency == 0 || entry.IFrequency == 1 || entry.IFrequency == 2 || entry.IFrequency == 3 || entry.IFrequency == 4)
        //                            {                                        
        //                                var parameterDate = DateTime.ParseExact(endDate.ToString("MM/dd/yyyy"), "MM/dd/yyyy", CultureInfo.InvariantCulture);
        //                                if (lastcomplianceScheduleon != null)
        //                                {
        //                                    if (lastcomplianceScheduleon.ScheduledOn.Date > parameterDate.Date)
        //                                    {
        //                                        nextDateMonth = GetNextDate1(Convert.ToDateTime(ic), entry.InternalComplianceID, entry.ID);
        //                                    }                                           
        //                                    else
        //                                    {
        //                                        nextDateMonth = GetNextDate(lastcomplianceScheduleon.ScheduledOn, entry.InternalComplianceID, entry.ID, internalcomplianceSchedule, (int)entry.IFrequency, lastcomplianceScheduleon, entry.ScheduledOn);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    DateTime? getinstancescheduleondate = entry.ScheduledOn;
        //                                    if (getinstancescheduleondate != null)
        //                                    {
        //                                        nextDateMonth = GetNextDate(Convert.ToDateTime(getinstancescheduleondate), entry.InternalComplianceID, entry.ID, internalcomplianceSchedule, (int)entry.IFrequency, lastcomplianceScheduleon, entry.ScheduledOn);
        //                                    }
        //                                    else
        //                                    {
        //                                        nextDateMonth = GetNextDate(curruntDate, entry.InternalComplianceID, entry.ID, internalcomplianceSchedule, (int)entry.IFrequency, lastcomplianceScheduleon, entry.ScheduledOn);
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                nextDateMonth = GetNextDate(curruntDate, entry.InternalComplianceID, entry.ID, internalcomplianceSchedule, (int)entry.IFrequency, lastcomplianceScheduleon, entry.ScheduledOn);
        //                            }

        //                            if (nextDateMonth.Item1 != Convert.ToDateTime(ic))
        //                            {
        //                                long ScheduleOnID = (from row in entities.InternalComplianceScheduledOns
        //                                                     where row.InternalComplianceInstanceID == entry.ID
        //                                                    && row.ForMonth == nextDateMonth.Item2
        //                                                    && row.IsActive == true && row.IsUpcomingNotDeleted == true
        //                                                     select row.ID).FirstOrDefault();

        //                                if (ScheduleOnID <= 0)
        //                                {
        //                                    InternalComplianceScheduledOn internalcomplianceScheduleon = new InternalComplianceScheduledOn();
        //                                    internalcomplianceScheduleon.InternalComplianceInstanceID = entry.ID;
        //                                    internalcomplianceScheduleon.ScheduledOn = nextDateMonth.Item1.Date;
        //                                    internalcomplianceScheduleon.ForMonth = nextDateMonth.Item2;
        //                                    internalcomplianceScheduleon.ForPeriod = nextDateMonth.Item3;
        //                                    internalcomplianceScheduleon.IsActive = true;
        //                                    internalcomplianceScheduleon.IsUpcomingNotDeleted = true;
        //                                    entities.InternalComplianceScheduledOns.Add(internalcomplianceScheduleon);
        //                                    entities.SaveChanges();

        //                                    var AssignedRole = GetAssignedUsers((int)entry.ID);
        //                                    var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
        //                                    if (performerRole != null)
        //                                    {
        //                                        var user = UserManagement.GetByID((int)performerRole.UserID);
        //                                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
        //                                        {
        //                                            InternalComplianceInstanceID = entry.ID,
        //                                            InternalComplianceScheduledOnID = internalcomplianceScheduleon.ID,
        //                                            CreatedBy = performerRole.UserID,
        //                                            CreatedByText = user.FirstName + " " + user.LastName,
        //                                            StatusId = 1,
        //                                            Remarks = "New Internal compliance assigned."
        //                                        };

        //                                        CreateTransaction(transaction);

        //                                        foreach (var roles in AssignedRole)
        //                                        {
        //                                            if (roles.RoleID != 6)
        //                                            {
        //                                                CreateReminders(entry.InternalComplianceID, entry.ID, roles.ID, nextDateMonth.Item1);
        //                                            }
        //                                        }
        //                                    }

        //                                    #region Task Scheduele                                       
        //                                    var Taskapplicable = TaskManagment.TaskApplicable(entry.CustomerBranchID);
        //                                    if (Taskapplicable != null)
        //                                    {

        //                                        if (Taskapplicable == 1)
        //                                        {
        //                                            var TaskIDList = (from row in entities.Tasks
        //                                                              join row1 in entities.TaskComplianceMappings
        //                                                              on row.ID equals row1.TaskID
        //                                                              where row1.ComplianceID == entry.InternalComplianceID
        //                                                              && row.TaskType == 2 && row.ParentID == null
        //                                                              && row.Isdeleted == false && row.IsActive == true
        //                                                              select row.ID).ToList();

        //                                            if (TaskIDList.Count > 0)
        //                                            {
        //                                                TaskIDList.ForEach(entrytask =>
        //                                                {
        //                                                    bool? IsAfter = TaskManagment.GetTaskBeforeAfter(entrytask);

        //                                                    if (IsAfter != null && IsAfter != true)
        //                                                    {
        //                                                        IsAfter = false;
        //                                                    }

        //                                                    List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

        //                                                    var taskSubTaskList = (from row in entities.Tasks
        //                                                                           join row1 in entities.TaskInstances
        //                                                                           on row.ID equals row1.TaskId
        //                                                                           where row.MainTaskID == entrytask && row1.CustomerBranchID == entry.CustomerBranchID
        //                                                                           select new { row.ID, row.ParentID, row.ActualDueDays }).ToList();

        //                                                    taskSubTaskList.ForEach(entrysubtask =>
        //                                                    {
        //                                                        TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long)entrysubtask.ID, ParentID = (int?)entrysubtask.ParentID, ActualDueDays = (int)entrysubtask.ActualDueDays });

        //                                                    });

        //                                                    TaskSubTaskList.ForEach(entryTaskSubTaskList =>
        //                                                    {
        //                                                        var taskInstances = (from row in entities.TaskInstances
        //                                                                             where row.IsDeleted == false
        //                                                                             && row.TaskId == entryTaskSubTaskList.TaskID
        //                                                                             && row.CustomerBranchID == entry.CustomerBranchID
        //                                                                             && row.ScheduledOn <= nextDateMonth.Item1.Date
        //                                                                             select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

        //                                                        taskInstances.ForEach(entrytaskinstance =>
        //                                                        {
        //                                                            DateTime ScheduleOn;
        //                                                            if (IsAfter == true)
        //                                                            {
        //                                                                ScheduleOn = nextDateMonth.Item1.AddDays(Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                ScheduleOn = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
        //                                                            }

        //                                                            if (TaskManagment.IsTaskAssignmentAvailable(entrytaskinstance.ID))
        //                                                            {
        //                                                                TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
        //                                                                taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
        //                                                                taskScheduleOn.ComplianceScheduleOnID = internalcomplianceScheduleon.ID;
        //                                                                taskScheduleOn.ForMonth = nextDateMonth.Item2;
        //                                                                taskScheduleOn.ForPeriod = nextDateMonth.Item3;
        //                                                                taskScheduleOn.ScheduleOn = ScheduleOn; // nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
        //                                                            taskScheduleOn.IsActive = true;
        //                                                                taskScheduleOn.IsUpcomingNotDeleted = true;
        //                                                                entities.TaskScheduleOns.Add(taskScheduleOn);
        //                                                                entities.SaveChanges();

        //                                                                var AssignedTaskRole = TaskManagment.GetAssignedTaskUsers((int)entrytaskinstance.ID);
        //                                                                var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
        //                                                                if (performerTaskRole != null)
        //                                                                {
        //                                                                    var user = UserManagement.GetByID((int)performerTaskRole.UserID);
        //                                                                    TaskTransaction tasktransaction = new TaskTransaction()
        //                                                                    {
        //                                                                        TaskInstanceId = entrytaskinstance.ID,
        //                                                                        TaskScheduleOnID = taskScheduleOn.ID,
        //                                                                        ComplianceScheduleOnID = internalcomplianceScheduleon.ID,
        //                                                                        CreatedBy = performerTaskRole.UserID,
        //                                                                        CreatedByText = user.FirstName + " " + user.LastName,
        //                                                                        StatusId = 1,
        //                                                                        Remarks = "New task assigned."
        //                                                                    };

        //                                                                    TaskManagment.CreateTaskTransaction(tasktransaction);

        //                                                                    foreach (var roles in AssignedTaskRole)
        //                                                                    {
        //                                                                        if (roles.RoleID != 6)
        //                                                                        {
        //                                                                            TaskManagment.InternalCreateTaskReminders(entrytaskinstance.TaskId, entry.InternalComplianceID, roles.ID, ScheduleOn, internalcomplianceScheduleon.ID, IsAfter);
        //                                                                        }
        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                        });
        //                                                    });
        //                                                });
        //                                            }
        //                                        }
        //                                    }
        //                                    #endregion
        //                                }
        //                            }//added by rahul
        //                            #endregion
        //                        }
        //                    }// inner for loop end count
        //                }
        //                catch (Exception ex)
        //                {
        //                    long newtestid = testid;
        //                    List<LogMessage> ReminderUserList = new List<LogMessage>();
        //                    LogMessage msg = new LogMessage();
        //                    msg.ClassName = "NewScheduleClassInternal";
        //                    msg.FunctionName = "GenerateScheduleOnForUpcomingInternal" + "InternalComplianceInstanceID Error" + testid;
        //                    msg.CreatedOn = DateTime.Now;
        //                    msg.Message = ex.Message + "----\r\n" + ex.InnerException;
        //                    msg.StackTrace = ex.StackTrace;
        //                    msg.LogLevel = (byte)0;
        //                    ReminderUserList.Add(msg);
        //                    //Commanclass.InsertLogToDatabase(ReminderUserList);
        //                    List<string> TO = new List<string>();
        //                    TO.Add("ajit@avantis.co.in");
        //                    TO.Add("rahul@avantis.co.in");
        //                    TO.Add("sachin@avantis.info");
        //                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
        //                        null, "Error Occured as Internal CreateReminders Function", " Internal GenerateScheduleOnForUpcomingInternal" + "InternalComplianceInstanceID Error" + testid);
        //                }
        //            });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        long newtestid = testid;
        //        List<LogMessage> ReminderUserList = new List<LogMessage>();
        //        LogMessage msg = new LogMessage();
        //        msg.ClassName = "NewScheduleClassInternal";
        //        msg.FunctionName = "GenerateScheduleOnForUpcomingInternal" + "InternalComplianceInstanceID Error" + testid;
        //        msg.CreatedOn = DateTime.Now;
        //        msg.Message = ex.Message + "----\r\n" + ex.InnerException;
        //        msg.StackTrace = ex.StackTrace;
        //        msg.LogLevel = (byte)0;
        //        ReminderUserList.Add(msg);
        //        //Commanclass.InsertLogToDatabase(ReminderUserList);
        //        List<string> TO = new List<string>();
        //        TO.Add("ajit@avantis.co.in");
        //        TO.Add("rahul@avantis.co.in");
        //        TO.Add("sachin@avantis.info");
        //        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
        //            null, "Error Occured as Internal CreateReminders Function", " Internal GenerateScheduleOnForUpcomingInternal" + "InternalComplianceInstanceID Error" + testid);
        //    }
        //}
    }
}
