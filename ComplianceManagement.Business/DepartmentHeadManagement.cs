﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using System.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class DepartmentHeadManagement
    {
        
        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> GetCannedReportDataForPerformer_DeptHead(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int ActID, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> transactionsQuery = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();

                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userID, Customerid, "DEPT")).ToList();
                    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
                    }
                }

                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                }
                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.DepartmentID == Convert.ToByte(category))).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(ActID))).ToList();
                }

                //Datr Filter
                if (Convert.ToDateTime(FromDate.ToString()).Date.Year != 1900 && Convert.ToDateTime(ToDate.ToString()).Date.Year != 1900)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }
                if (RoleID == 8)
                {
                    if (Convert.ToDateTime(ToDate.ToString()).Date.Year != 1900)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ScheduledOn <= ToDate).ToList();
                    }
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();

                }
                //Status
                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> GetCannedReportDataForReviewer_DeptHead(int Customerid, int userID, int risk, CannedReportFilterNewStatus status, int location, int type, int category, int actid, DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> transactionsQuery = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                var GetApprover = (entities.Sp_GetApproverUsers(userID)).ToList();
                if (GetApprover.Count > 0)
                {
                    RoleID = 6;
                }
                if (RoleID == 8 || RoleID == 9)
                {
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userID, Customerid, "DEPT")).ToList();
                    transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();                   
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true && entry.ComplinceVisible == true)).ToList();
                    }
                }
                else if (RoleID == 6)
                {                   
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userID, Customerid, "APPR")).ToList();
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 6).ToList();
                }
                else
                {                    
                    transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userID, Customerid, "RVW")).ToList();
                    if (ComplianceType == 1)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => (entry.EventFlag == true)).ToList();
                    }
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID && entry.RoleID == 4).ToList();
                }
                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == Convert.ToByte(type))).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.DepartmentID == Convert.ToByte(category))).ToList();
                }

                //actid Filter
                if (actid != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == Convert.ToByte(actid))).ToList();
                }

                //Datr Filter
                if (FromDate.ToString() != "1/1/1900 12:00:00 AM" && ToDate.ToString() != "1/1/1900 12:00:00 AM")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                switch (status)
                {
                    case CannedReportFilterNewStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case CannedReportFilterNewStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case CannedReportFilterNewStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case CannedReportFilterNewStatus.Rejected:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }              
        public static List<SP_GetOverDueHighCompliance_DeptHead_Result> GetComplianceDashboardOverdueListNew(int Customerid, int Userid = -1, bool ISApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var transactionsQuery = (from row in entities.SP_GetOverDueHighCompliance_DeptHead(Userid, Customerid,0)
                                         where row.ScheduledOn <DateTime.Now
                                         select row).ToList();

                return transactionsQuery;
            }
        }        
      
        #region Internal        
        public static List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> FunctionBARInternalGetManagementDetailView_DeptHeadNew(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername,string Roles, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else if (Roles == "MGMT")
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "MGMT")).ToList();
                }
                else 
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.DepartmentID == CategoryID && entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }        
        public static List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> FunctionBARInternalGetManagementDetailView_DeptHeadNew(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, string Roles, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else if (Roles == "MGMT")
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "MGMT")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.DepartmentID == CategoryID && entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }                        
        public static List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> FunctionPIEInternalGetManagementDetailView_DeptHeadNEW(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.DepartmentID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }        
        public static List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> FunctionPIEInternalGetManagementDetailView_DeptHeadNew(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportInternalCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportInternalCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.DepartmentID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        #endregion

        #region Statutory
        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> FunctionBARGetManagementDetailView_DeptHead(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, string Roles, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else if (Roles == "MGMT")
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "MGMT")).ToList();
                }
                else
                {                    
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.DepartmentID == CategoryID && entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> FunctionBARGetManagementDetailView_DeptHead(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername,string Roles, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else if (Roles =="MGMT")
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "MGMT")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.DepartmentID == CategoryID && entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> FunctionPIEGetManagementDetailView_DeptHead(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.DepartmentID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> FunctionPIEGetManagementDetailView_DeptHead(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_GetCannedReportCompliancesSummary_DeptHead_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_DeptHead_Result>();
                DateTime EndDate = DateTime.Today.Date;
                if (approver == true)
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "APPR")).ToList();
                }
                else
                {
                    detailView = (entities.SP_GetCannedReportCompliancesSummary_DeptHead(userId, customerid, "DEPT")).ToList();
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.DepartmentID)).ToList();
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }
                return detailView;
            }
        }
        #endregion

        public static List<sp_ComplianceAssignedDepartment_Result> GetFunctionDetailsStatutory_DeptHead(int UserID, int CustomerBranchId, List<long> Branchlist, string isstatutoryinternal, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedDepartment_Result> complianceCategorys = new List<sp_ComplianceAssignedDepartment_Result>();
                if (approver == true)
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "DEPT", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "DEPT", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
     
        public static List<InternalCompliancesCategory> GetFunctionDetailsInternal(int Userid, List<long> branchlist, int CustomerBranchId, string IsDeptHead, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalCompliancesCategory> complianceCategorys = new List<InternalCompliancesCategory>();
                if (approver == true)
                {
                    if (branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& row1.CustomerBranchID == CustomerBranchId
                                               && branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.InternalCompliancesCategories
                                               join row1 in entities.InternalComplianceAssignedInstancesViews
                                               on row.ID equals row1.InternalComplianceCategoryID
                                               where row1.UserID == Userid
                                               //&& branchlist.Contains((long)row1.CustomerBranchID)
                                               && row1.RoleID == 6
                                               select row).GroupBy(g => g.ID).Select(a => a.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    List<InternalComplianceAssignedInstancesView> InternalComplianceCount = new List<InternalComplianceAssignedInstancesView>();

                    if (branchlist.Count > 0)
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where branchlist.Contains((long)row.CustomerBranchID) //&& row.CustomerBranchID == row1.BranchID
                                                && row1.UserID == Userid && row1.BranchID == CustomerBranchId
                                                   select row).Distinct().ToList();
                    }
                    else
                    {
                        InternalComplianceCount = (from row in entities.InternalComplianceAssignedInstancesViews
                                                   join row1 in entities.EntitiesAssignmentInternals
                                                   on (long)row.InternalComplianceCategoryID equals row1.ComplianceCatagoryID
                                                   where row.CustomerBranchID == row1.BranchID
                                                   // && branchlist.Contains((long)row.CustomerBranchID)
                                                   && row1.UserID == Userid
                                                   select row).Distinct().ToList();
                    }
                    InternalComplianceCount = InternalComplianceCount.GroupBy(a => (int?)a.InternalComplianceCategoryID).Select(a => a.FirstOrDefault()).ToList();
                    complianceCategorys = (from row in InternalComplianceCount
                                           join row1 in entities.InternalCompliancesCategories
                                           on row.InternalComplianceCategoryID equals row1.ID
                                           select row1).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }


        public static List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result> GetInternalComplianceDetailsDashboard_DeptHead(int customerid, List<long> Branchlist, int RoleID, int CustomerBranchID,  int CategoryID, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result> ComplianceDetails = new List<sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead_Result>();

                if (IsApprover)
                {
                    ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead(Userid, "APPR")
                                         select row).ToList();
                }
                else
                {
                    ComplianceDetails = (from row in entities.sp_InternalComplianceInstanceAssignmentViewDetails_DeptHead(Userid, "DEPT")
                                         select row).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }
                if (RoleID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.RoleID == RoleID).Distinct().ToList();
                }

                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.DepartmentID == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.InternalComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }

        

        public static List<SP_GetOverDueHighInternalCompliance_DeptHead_Result> GetComplianceDashboardOverdueInternalNew(int Customerid, int Userid = -1, bool ISApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var transactionsQuery = (from row in entities.SP_GetOverDueHighInternalCompliance_DeptHead(Userid, Customerid,0)
                                         where row.InternalScheduledOn < DateTime.Now
                                         select row).ToList();

                return transactionsQuery;
            }
        }

        public static List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result> GetComplianceDetailsDashboard_DeptHead(int customerid, List<long> Branchlist, int RoleID, int CustomerBranchID, int ActId, int CategoryID, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result> ComplianceDetails = new List<sp_ComplianceInstanceAssignmentViewDetails_DeptHead_Result>();

                if (IsApprover)
                {
                    ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails_DeptHead(Userid, "APPR")
                                         select row).ToList();
                }
                else
                {
                    ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails_DeptHead(Userid, "DEPT")
                                         select row).ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }
                if (RoleID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.RoleID == RoleID).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.DepartmentID == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }

        public static List<SP_GetPenaltyQuarterWise_DeptHead_Result> GetPenaltyComplianceDetailsDashboard_DeptHead(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, DateTime startdate, DateTime enddate, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise_DeptHead(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "S" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && row.IsPenaltySave == false && row.RoleID == 4
                                         select row).ToList();

                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.DepartmentID == CategoryID).Distinct().ToList();
                }

                if (startdate.ToString() != "" && enddate.ToString() != "" && startdate.ToString() != "1/1/0001 12:00:00 AM" && enddate.ToString() != "1/1/0001 12:00:00 AM")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ScheduledOn >= startdate && row.ScheduledOn <= enddate).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }

        public static List<SP_GetPenaltyQuarterWise_DeptHead_Result> GetPenaltyComplianceDetailsDashboardPending_DeptHead(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise_DeptHead(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "P" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && row.IsPenaltySave == true && (row.RoleID == 3 || row.RoleID == 4)
                                         select row).ToList();

                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.DepartmentID == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }


        public static List<int> GetDepartmentData(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DeptIDs = (from row in entities.DepartmentMappings
                               where row.UserID == userID && row.IsActive == true
                               select row.DepartmentID).ToList();

                return DeptIDs;
            }
        }

        public static void CreateDepartmentMapping(List<DepartmentMapping> deptMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var item in deptMapping)
                {
                    var DeptIDs = (from row in entities.DepartmentMappings
                                   where row.UserID == item.UserID
                                   && row.DepartmentID == item.DepartmentID
                                   select row).FirstOrDefault();

                    if (DeptIDs == null)
                    {
                        entities.DepartmentMappings.Add(item);
                        entities.SaveChanges();
                    }
                    else
                    {
                        if (!DeptIDs.IsActive)
                        {
                            DeptIDs.IsActive = true;
                            DeptIDs.UpdatedBy = item.CreatedBy;
                            DeptIDs.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                }
            }
        }

        public static void DeselectAllDepartment(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var DeptIDs = (from row in entities.DepartmentMappings
                               where row.UserID == UserID
                               select row).ToList();

                if (DeptIDs.Count > 0)
                {
                    foreach (var item in DeptIDs)
                    {
                        item.IsActive = false;
                        entities.SaveChanges();
                    }
                }
            }
        }

    }
}
