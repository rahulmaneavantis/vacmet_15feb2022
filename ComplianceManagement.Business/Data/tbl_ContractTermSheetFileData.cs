//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_ContractTermSheetFileData
    {
        public int ID { get; set; }
        public string ContractNo { get; set; }
        public Nullable<long> TermSheetNo { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileKey { get; set; }
        public string Version { get; set; }
        public string VersionDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string CreatedByText { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public string DeletedOn { get; set; }
        public string EnType { get; set; }
        public Nullable<int> FileSize { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}
