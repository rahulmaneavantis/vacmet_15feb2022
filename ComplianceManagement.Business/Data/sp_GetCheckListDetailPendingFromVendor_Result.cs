//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class sp_GetCheckListDetailPendingFromVendor_Result
    {
        public string AM_ActName { get; set; }
        public string NatureOfCompliance { get; set; }
        public string TypeOfCompliance { get; set; }
        public string StateID { get; set; }
    }
}
