﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    [Serializable]
    public class DataItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Count { get; set; }
    }
}