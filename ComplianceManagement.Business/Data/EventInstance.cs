//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventInstance
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EventInstance()
        {
            this.EventAssignments = new HashSet<EventAssignment>();
            this.EventEscalationNotifications = new HashSet<EventEscalationNotification>();
            this.EventScheduleOns = new HashSet<EventScheduleOn>();
        }
    
        public long ID { get; set; }
        public Nullable<long> EventID { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<int> CustomerBranchID { get; set; }
        public Nullable<long> UserID { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventAssignment> EventAssignments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventEscalationNotification> EventEscalationNotifications { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventScheduleOn> EventScheduleOns { get; set; }
        public virtual CustomerBranch CustomerBranch { get; set; }
        public virtual User User { get; set; }
        public virtual Event Event { get; set; }
    }
}
