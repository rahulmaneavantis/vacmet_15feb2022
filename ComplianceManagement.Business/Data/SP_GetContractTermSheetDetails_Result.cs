//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_GetContractTermSheetDetails_Result
    {
        public int ID { get; set; }
        public string Contract_no { get; set; }
        public string IsContract_Summary { get; set; }
        public string VendorName { get; set; }
        public string Vendor_Address { get; set; }
        public string Scope_of_service { get; set; }
        public Nullable<System.DateTime> Startdate { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public string Term { get; set; }
        public Nullable<long> ServiceFees { get; set; }
        public string PaymentTerm { get; set; }
        public string TaxationInstruction { get; set; }
        public string Taxation_addDetails { get; set; }
        public string IsSharedData { get; set; }
        public string SharedData_adddetails { get; set; }
        public string IsConfidentialdata1 { get; set; }
        public string Confidetialdata1_details { get; set; }
        public string IsConfidentialdata2 { get; set; }
        public string IsRelatedParty { get; set; }
        public string IsOutsourcing_contract { get; set; }
        public string CustomerBranch { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<int> SPOC_Nameid { get; set; }
        public string SPOC_Name { get; set; }
        public string SPOC_Designation { get; set; }
        public string EntityType { get; set; }
        public string AttentionReq { get; set; }
        public string Contract_objective { get; set; }
        public string IsRisk_assessment { get; set; }
        public Nullable<System.DateTime> todate { get; set; }
        public string Define_Risk { get; set; }
        public string Consequence_defineRisk { get; set; }
        public string Risk_MitigationAction { get; set; }
        public string Contract_Clauses { get; set; }
        public string Escalation_process { get; set; }
        public string Exit_Termination_plan { get; set; }
        public string TerminitationNoticeperiod { get; set; }
        public string Termination_plan { get; set; }
        public string Audit_approval { get; set; }
        public string IsEGIC { get; set; }
        public string IsEGIC_Additionaldetails { get; set; }
        public string IPR_Rights { get; set; }
        public string IsIntellectualshared { get; set; }
        public string IsIntellectualshared_details { get; set; }
        public Nullable<long> RequestTo { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string Creator { get; set; }
        public Nullable<bool> Isdeleted { get; set; }
        public string Requestor { get; set; }
        public string Approver { get; set; }
    }
}
