//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_TempAssignmentInternal_Result
    {
        public int ID { get; set; }
        public long ComplianceID { get; set; }
        public string ShortDescription { get; set; }
        public string Location { get; set; }
        public int CustomerBranchID { get; set; }
        public long UserID { get; set; }
        public string Users { get; set; }
        public int RoleID { get; set; }
        public string Role { get; set; }
        public Nullable<long> DepartmentID { get; set; }
        public string SequenceID { get; set; }
        public string Department { get; set; }
    }
}
