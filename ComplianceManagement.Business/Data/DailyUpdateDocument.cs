//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DailyUpdateDocument
    {
        public int ID { get; set; }
        public int DailyUpdateID { get; set; }
        public string Name { get; set; }
        public byte[] FileData { get; set; }
        public string FilePath { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
