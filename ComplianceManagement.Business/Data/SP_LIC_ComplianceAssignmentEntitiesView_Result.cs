//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_LIC_ComplianceAssignmentEntitiesView_Result
    {
        public int ID { get; set; }
        public long userID { get; set; }
        public string UserName { get; set; }
        public int BranchID { get; set; }
        public string Branch { get; set; }
        public long LicenseTypeID { get; set; }
        public string LicenseType { get; set; }
    }
}
