//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class RLCS_Vendor_RecentCheckListStatus_Result
    {
        public long AuditID { get; set; }
        public Nullable<long> VendorAuditScheduleOnID { get; set; }
        public int StatusID { get; set; }
        public System.DateTime Dated { get; set; }
        public Nullable<long> CustomerBranchID { get; set; }
        public Nullable<long> ResultID { get; set; }
        public Nullable<long> CheckListID { get; set; }
        public string Recommendation { get; set; }
        public Nullable<System.DateTime> TimeLine { get; set; }
        public string AuditCheckListStatus { get; set; }
        public bool IsDocument { get; set; }
    }
}
