//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_Litigation_PageAuthorization_Result
    {
        public bool ADDval { get; set; }
        public bool Deleteval { get; set; }
        public bool UpdateVal { get; set; }
        public bool ViewVal { get; set; }
        public string Name { get; set; }
        public long ID { get; set; }
    }
}
