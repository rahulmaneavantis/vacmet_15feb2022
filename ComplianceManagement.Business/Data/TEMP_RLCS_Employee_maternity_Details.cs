//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TEMP_RLCS_Employee_maternity_Details
    {
        public int EM_ID { get; set; }
        public string EM_EmpID { get; set; }
        public string EM_ClientID { get; set; }
        public Nullable<int> EM_Month { get; set; }
        public Nullable<int> EM_Year { get; set; }
        public Nullable<System.DateTime> EM_Maternity_From { get; set; }
        public Nullable<System.DateTime> EM_Maternity_To { get; set; }
        public Nullable<double> EM_Amountofbonus_paidunderSection8 { get; set; }
        public Nullable<System.DateTime> EM_Dateofbonus_paidunderSection8 { get; set; }
        public Nullable<System.DateTime> EM_Section6NoticeDate { get; set; }
        public Nullable<System.DateTime> EM_DischargeDate { get; set; }
        public Nullable<System.DateTime> EM_DateProofofPregnancyunderSection6 { get; set; }
        public Nullable<System.DateTime> EM_ChildDOB { get; set; }
        public Nullable<System.DateTime> EM_DateproofOfDelivery_miscarriage_death { get; set; }
        public Nullable<System.DateTime> EM_DateofProofIllness { get; set; }
        public Nullable<System.DateTime> EM_Dateofmaternitybenefitpaidinadvanceofexpecteddelivery { get; set; }
        public Nullable<double> EM_Amountofmaternitybenefitpaidinadvanceofexpecteddelivery { get; set; }
        public Nullable<System.DateTime> EM_DateofSubsequentBenefitpaid { get; set; }
        public Nullable<double> EM_Amountofsubsequentpaymentofmaternitybenefit { get; set; }
        public Nullable<System.DateTime> EM_Date_WagesPaid_leave_under_Section9 { get; set; }
        public Nullable<double> EM_Amount_of_wages_paid_on_leave_under_Section9 { get; set; }
        public Nullable<System.DateTime> EM_Date_LeaveWagesPaid { get; set; }
        public Nullable<System.DateTime> EM_Date_of_wages_paid_on_account_of_leave_under_Section10_and_period_of_leavegranted { get; set; }
        public Nullable<double> EM_Amount_of_wages_paid_on_account_of_leave_under_Section10_and_period_of_leavegranted { get; set; }
        public Nullable<System.DateTime> EM_Ifthewomandies_thedateofherdeath { get; set; }
        public string EM_Ifthewomandies_thenameoftheperson_towhom_maternitybenefit_paid { get; set; }
        public Nullable<double> EM_Ifthewomandies_amountwaspaid_to_the_person { get; set; }
        public Nullable<System.DateTime> EM_Ifthewomandies_dateofpayment { get; set; }
        public string EM_Ifthewomandies_andthechildsurvives_thenameoftheperson_towhom_theamountofmaternitybenefit_waspaid_onbehalf_of_thechild { get; set; }
        public Nullable<System.DateTime> EM_Ifthewomandies_andthechildsurvives_and_the_period_for_which_it_was_paidtothepersonFromDate { get; set; }
        public Nullable<System.DateTime> EM_Ifthewomandies_andthechildsurvives_and_the_period_for_which_it_was_paid_tothepersonToDate { get; set; }
        public string EM_DeathDate_towhom_Amount { get; set; }
        public string EM_TypeOfLeave { get; set; }
        public string EM_CreatedBy { get; set; }
        public Nullable<System.DateTime> EM_CreatedDate { get; set; }
        public string EM_ModifiedBy { get; set; }
        public Nullable<System.DateTime> EM_modifieddate { get; set; }
        public Nullable<int> EM_Version { get; set; }
        public Nullable<System.DateTime> EM_Dateofproductionofproofofillness_referredtoinSection10 { get; set; }
        public Nullable<double> EM_Maternity_benefit_under_section7 { get; set; }
        public string EM_Maternity_benefit_rejected { get; set; }
        public string EM_Medical_bonus_rejected { get; set; }
        public string EM_Leave_for_miscarriage_was_granted { get; set; }
        public string EM_Leave_for_miscarriage_was_applied_for_but_was_rejected { get; set; }
        public string EM_Additional_leave_for_illness_under_Section10_was_granted { get; set; }
        public Nullable<System.DateTime> EM_Fromdate_of_additional_leave { get; set; }
        public Nullable<System.DateTime> EM_To_date_of_additional_leave { get; set; }
        public string EM_Leave_for_illness_under_section_10_was_applied_for_but_rejected { get; set; }
        public string EM_Nameoftheperson_nominated_by_the_woman_under_Section6 { get; set; }
        public string EM_Payment_was_made_to_persons_other_than_the_woman_concerned { get; set; }
        public string EM_Women_deprived_of_maternity_benefit_medical_bonus_under_proviso_to_sub_section2_of_section12 { get; set; }
        public string EM_Payment_was_made_on_the_order_of_the_Appellate_Authority_or_Inspector { get; set; }
        public string EM_Insurance_provided_by_the_Management { get; set; }
        public string EM_Women_died_before_delivery { get; set; }
        public string EM_Women_died_after_delivery { get; set; }
        public string EM_Women_discharged { get; set; }
        public string EM_Legal_Representative_of_the_women { get; set; }
        public Nullable<double> EM_Amount_paid_for_miscarriage { get; set; }
        public string EM_Number_of_the_women_workers_who_absconded_after_receiving_the_first_installment_of_maternity_benefit { get; set; }
        public string EM_Number_of_cases_where_prenatal_confinement_and_postnatal_care_was_provided_by_the_management_free_of_charge_section8 { get; set; }
        public Nullable<System.DateTime> EM_Dateson_which_she_is_laid_off_and_not_employed { get; set; }
        public Nullable<System.DateTime> EM_Leavegranted_under_section10_from_date { get; set; }
        public Nullable<System.DateTime> EM_Leavegranted_under_section10_to_date { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> fileid { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> deleted_at { get; set; }
        public Nullable<double> Total_amount_paid_forMaternity_Illness { get; set; }
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public Nullable<bool> ISProcessed { get; set; }
        public Nullable<bool> ISActive { get; set; }
        public Nullable<bool> ISValidated { get; set; }
        public Nullable<int> excel_row_no { get; set; }
    }
}
