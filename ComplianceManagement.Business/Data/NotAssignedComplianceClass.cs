﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    public class NotAssignedComplianceClass
    {
        public long ID { get; set; }
        public long EventID { get; set; }
        public long CustomerBranchID { get; set; }
        public string EvnetName { get; set; }
        public string ComplianceName { get; set; }
        public string CustomerBranchName { get; set; }
    }
    
}
