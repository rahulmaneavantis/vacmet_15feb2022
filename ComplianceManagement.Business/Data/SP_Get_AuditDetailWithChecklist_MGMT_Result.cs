//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_Get_AuditDetailWithChecklist_MGMT_Result
    {
        public long ChecklistID { get; set; }
        public string VendorName { get; set; }
        public string AuditName { get; set; }
        public string CategoryName { get; set; }
        public string ChecklistName { get; set; }
        public string LocationName { get; set; }
        public long AuditID { get; set; }
        public int CustomerId { get; set; }
        public long vendorId { get; set; }
        public long CategoryID { get; set; }
        public int AuditorID { get; set; }
        public System.DateTime AuditStartDate { get; set; }
        public System.DateTime AuditEndDate { get; set; }
        public bool isactive { get; set; }
        public Nullable<bool> AuditCloseFlag { get; set; }
        public string Observation { get; set; }
    }
}
