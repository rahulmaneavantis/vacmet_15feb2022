//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblContractEditApproval
    {
        public int Id { get; set; }
        public int ContractInstanceID { get; set; }
        public string RequestStatus { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<int> RequestedTo { get; set; }
        public Nullable<System.DateTime> Validtill { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}
