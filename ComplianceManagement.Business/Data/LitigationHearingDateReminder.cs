//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class LitigationHearingDateReminder
    {
        public int ID { get; set; }
        public string Reminder { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> days { get; set; }
        public Nullable<bool> Isactive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
    }
}
