//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TEMP_RLCS_WorkmenCompensation_Details
    {
        public int WCD_ID { get; set; }
        public string WCD_ClientID { get; set; }
        public string WCD_StateID { get; set; }
        public string WCD_LocationID { get; set; }
        public string WCD_BranchName { get; set; }
        public Nullable<int> WCD_Year { get; set; }
        public Nullable<int> WCD_Death_No_Accidents { get; set; }
        public Nullable<int> WCD_Perm_Disablement_No_accidents { get; set; }
        public Nullable<int> WCD_Temp_Disablement_No_accidents { get; set; }
        public Nullable<double> WCD_Death_Amount_Paid_Accidents { get; set; }
        public Nullable<double> WCD_Permanent_Disablement_Amount_paid_accidents { get; set; }
        public Nullable<double> WCD_Temporary_Disablement_Amount_paid_accidents { get; set; }
        public Nullable<int> WCD_Death_No_OD { get; set; }
        public Nullable<int> WCD_Perm_Disablement_No_OD { get; set; }
        public Nullable<int> WCD_Temp_Disablement_No_OD { get; set; }
        public Nullable<double> WCD_Death_Amount_Paid_OD { get; set; }
        public Nullable<double> WCD_Permanent_Disablement_Amount_paid_OD { get; set; }
        public Nullable<double> WCD_Temporary_Disablement_Amount_paid_OD { get; set; }
        public string WCD_CreatedBy { get; set; }
        public Nullable<System.DateTime> WCD_CreatedDate { get; set; }
        public string WCD_ModifiedBy { get; set; }
        public Nullable<System.DateTime> WCD_ModifiedDate { get; set; }
        public Nullable<int> WCD_Version { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> fileid { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> deleted_at { get; set; }
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public Nullable<bool> ISProcessed { get; set; }
        public Nullable<bool> ISActive { get; set; }
        public Nullable<bool> ISValidated { get; set; }
        public Nullable<int> excel_row_no { get; set; }
    }
}
