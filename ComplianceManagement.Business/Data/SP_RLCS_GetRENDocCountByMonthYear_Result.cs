//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_RLCS_GetRENDocCountByMonthYear_Result
    {
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public Nullable<int> AVACOM_UserID { get; set; }
        public int ID { get; set; }
        public string OA_ID { get; set; }
        public string OA_ClientID { get; set; }
        public int OA_CrtMonth { get; set; }
        public int OA_CrtYear { get; set; }
        public string OA_ActivityID { get; set; }
        public string OA_State { get; set; }
        public string OA_Location { get; set; }
        public string OA_Branch { get; set; }
        public string OA_AssignedTo { get; set; }
        public Nullable<System.DateTime> OA_ActivityDate { get; set; }
        public Nullable<System.DateTime> OA_StartDate { get; set; }
        public Nullable<System.DateTime> OA_EndDate { get; set; }
        public string OA_Status { get; set; }
        public string OA_CloseMonth { get; set; }
        public string OA_CloseYear { get; set; }
        public Nullable<decimal> OA_ServiceCharge { get; set; }
        public Nullable<decimal> OA_SetupPrice { get; set; }
        public Nullable<decimal> OA_DeclarePrice { get; set; }
        public Nullable<decimal> OA_GovtFee { get; set; }
        public Nullable<decimal> OA_AscFee { get; set; }
        public string OA_Remarks { get; set; }
        public string OA_DocPath { get; set; }
        public string OA_CreatedBy { get; set; }
        public Nullable<System.DateTime> OA_CreatedDate { get; set; }
        public string OA_ModifiedBy { get; set; }
        public Nullable<System.DateTime> OA_ModifiedDate { get; set; }
        public string OA_ClosedBy { get; set; }
        public Nullable<System.DateTime> OA_ClosedDate { get; set; }
        public Nullable<int> OA_Version { get; set; }
        public string OA_Type { get; set; }
        public Nullable<System.DateTime> OA_ExpiryDate { get; set; }
        public string OA_RC_Number { get; set; }
        public string OA_Level { get; set; }
        public string OA_YEAR { get; set; }
        public string OA_Govt_DocPath { get; set; }
        public Nullable<bool> OA_AscAppl { get; set; }
        public Nullable<bool> OA_AscITAppl { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string DocType { get; set; }
        public string FileName { get; set; }
        public System.DateTime RHFDCreatedOn { get; set; }
    }
}
