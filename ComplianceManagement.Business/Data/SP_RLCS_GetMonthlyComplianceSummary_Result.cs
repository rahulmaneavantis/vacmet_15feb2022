//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class SP_RLCS_GetMonthlyComplianceSummary_Result
    {
        public Nullable<int> AVACOM_CustomerID { get; set; }
        public Nullable<int> AVACOM_BranchID { get; set; }
        public string ClientID { get; set; }
        public string Entity { get; set; }
        public string Month { get; set; }
        public string MonthName { get; set; }
        public string Year { get; set; }
        public string State { get; set; }
        public string SM_Name { get; set; }
        public string Branch { get; set; }
        public string LM_Name { get; set; }
        public Nullable<int> CompliedNo { get; set; }
        public Nullable<int> NotCompliedNo { get; set; }
        public string Location { get; set; }
    }
}
