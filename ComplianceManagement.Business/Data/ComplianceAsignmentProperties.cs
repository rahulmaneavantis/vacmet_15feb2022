﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    [Serializable()]
    public class ComplianceAsignmentProperties
    {
        private int complianceId;
        private string startDate;
        private bool performer;
        private bool reviewer1;
        private bool reviewer2;
        private int complianceType;
        private string stateCode;
        
        public int ComplianceId
        {
            get { return complianceId; }
            set { complianceId = value; }
        }

        public string StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public bool Performer
        {
            get { return performer; }
            set { performer = value; }
        }

        public bool Reviewer1
        {
            get { return reviewer1; }
            set { reviewer1 = value; }
        }

        public bool Reviewer2
        {
            get { return reviewer2; }
            set { reviewer2 = value; }
        }

        public int ComplianceType
        {
            get { return complianceType; }
            set { complianceType = value; }
        }

        public string StateCode
        {
            get { return stateCode; }
            set { stateCode = value; }
        }

    }
}
