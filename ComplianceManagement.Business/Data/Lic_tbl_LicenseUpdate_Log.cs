//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Lic_tbl_LicenseUpdate_Log
    {
        public long ID { get; set; }
        public Nullable<long> LicenseID { get; set; }
        public Nullable<long> ScheduleOnID { get; set; }
        public Nullable<System.DateTime> OldStartdate { get; set; }
        public Nullable<System.DateTime> OldEndDate { get; set; }
        public Nullable<int> OldLicenseStatusID { get; set; }
        public Nullable<System.DateTime> OldScheduleOnDate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Createdon { get; set; }
    }
}
