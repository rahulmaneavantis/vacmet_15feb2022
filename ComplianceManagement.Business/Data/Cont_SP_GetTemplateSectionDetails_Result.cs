//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.Data
{
    using System;
    
    public partial class Cont_SP_GetTemplateSectionDetails_Result
    {
        public long TemplateID { get; set; }
        public string TemplateName { get; set; }
        public string TemplateVersion { get; set; }
        public Nullable<int> SectionOrder { get; set; }
        public long SectionID { get; set; }
        public string Header { get; set; }
        public string Version { get; set; }
        public string BodyContent { get; set; }
    }
}
