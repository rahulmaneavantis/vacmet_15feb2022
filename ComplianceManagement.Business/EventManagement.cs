﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class EventManagement
    {
        public static bool EventAssignmentExists(int EventID, int Customerbranchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.EventInstances
                             where row.IsDeleted == false
                                  && row.EventID == EventID
                                  && row.CustomerBranchID == Customerbranchid
                             select row).ToList();
                if (query != null)
                {
                    if (query.Count == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public static List<Event> BindNonSecretrialEventData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objstate = (from row in entities.Events
                                where row.EventClassificationID == 2
                                && row.Visible == "Y"
                                select row).Distinct().OrderBy(entry => entry.Name);
                return objstate.ToList();
            }
        }

       
        public static List<SP_GetEventAct_Result> BindActData(int EventID,int ActID,int StateId,string Flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetEventAct(EventID, ActID,StateId, Flag)
                            select row).Distinct().OrderBy(entry => entry.ActName).ToList();
                return data;

            }
        }

        public static List<SP_GetEventState_Result> BindStateData(int EventID, int ActID, int StateId, string Flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetEventState(EventID, ActID, StateId, Flag)
                            select row).Distinct().OrderBy(entry => entry.State).ToList();
                return data;

            }
        }
        public static List<int> GetEventTypeMappedID(long EventId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EventTypeMappedIDs = (from row in entities.Events
                                          where row.ID == EventId
                                          select row.EventTypeID).ToList();

                return EventTypeMappedIDs;
            }

        }
        public static bool CheckComplianceTrasactionClosure(long ComplianceID, long EventScheduleOnID, DateTime dtClosureDate, long ParentEventID, long IntermediateEventID, long SubEventID)
        {
            bool isTransactionDone = true;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceScheduleOns
                                    join row1 in entities.ComplianceInstances
                                    on row.ComplianceInstanceID equals row1.ID
                                    where row1.ComplianceId == ComplianceID && row.EventScheduledOnID == EventScheduleOnID
                                    && row.ScheduleOn < dtClosureDate
                                    && row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                    && row.SubEventID == SubEventID
                                    && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                    select row).ToList();

                scheduleList.ForEach(complianceScheduleOnssentry =>
                {
                    var complianceTransactions = (from row in entities.ComplianceTransactions
                                                  where row.ComplianceInstanceId == complianceScheduleOnssentry.ComplianceInstanceID
                                                          && row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID
                                                  select row).ToList();
                    if (complianceTransactions.Count == 1)
                    {
                        isTransactionDone = false;
                    }
                });
            }

            return isTransactionDone;
        }

        public static void RemoveComplianceScheduleEventDateUpdation(long ComplianceID, long ParentEventID, long IntermediateEventID, long SubEventID, long ComplianceInstanceID, long EventScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceScheduleOns
                                    join row1 in entities.ComplianceInstances
                                    on row.ComplianceInstanceID equals row1.ID
                                    where row1.ComplianceId == ComplianceID && row.EventScheduledOnID == EventScheduleOnID
                                    && row.ParentEventD == ParentEventID
                                    && row.IntermediateEventID == IntermediateEventID && row.SubEventID == SubEventID
                                    && row1.ID == ComplianceInstanceID
                                    select row).ToList();

                scheduleList.ForEach(entry =>
                {
                    entry.IsActive = false;
                    entry.IsUpcomingNotDeleted = false;
                    entities.SaveChanges();
                });
            }
        }
        public static void RemoveComplianceScheduleFromClosure(long ComplianceID, long EventScheduleOnID, DateTime dtScheduleDate, long ParentEventD, long IntermediateEventID, long SubEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceScheduleOns
                                    join row1 in entities.ComplianceInstances
                                    on row.ComplianceInstanceID equals row1.ID
                                    where row1.ComplianceId == ComplianceID && row.EventScheduledOnID == EventScheduleOnID
                                    && row.ScheduleOn >= dtScheduleDate
                                    && row.ParentEventD == ParentEventD
                                    && row.IntermediateEventID == IntermediateEventID
                                    && row.SubEventID == SubEventID
                                    && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                    select row).ToList();

                scheduleList.ForEach(complianceScheduleOnssentry =>
                {
                    var complianceTransactions = (from row in entities.ComplianceTransactions
                                                  where row.ComplianceInstanceId == complianceScheduleOnssentry.ComplianceInstanceID
                                                          && row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID
                                                  select row).ToList();
                    if (complianceTransactions.Count == 1)
                    {
                        UpdateComplianceScheduleOn(complianceScheduleOnssentry.ID);
                    }
                });
            }
        }


        public static List<object> GetCustomer()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.EventInstances
                            join row1 in entities.EventScheduleOns
                            on row.ID equals row1.EventInstanceID
                            join row2 in entities.CustomerBranches
                            on row.CustomerBranchID equals row2.ID
                            join row3 in entities.Customers
                            on row2.CustomerID equals row3.ID
                            orderby row3.Name ascending
                            where row.IsDeleted == false &&
                            row1.IsDeleted == false && row2.IsDeleted == false
                            select new
                            {
                                row3.ID,
                                row3.Name
                            }).Distinct().OrderBy(entry => entry.Name).ToList<object>();
                return data;

            }
        }

        public static object GetEventNameBy(int branchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventlist = (from row in entities.Events
                                 join row1 in entities.EventInstances
                                 on row.ID equals row1.EventID
                                 join row2 in entities.EventScheduleOns
                                 on row1.ID equals row2.EventInstanceID
                                 where row.IsDeleted == false && row1.IsDeleted==false
                                 && row2.IsDeleted == false
                                 && row1.CustomerBranchID == branchid 
                                 select row).Distinct().ToList();
                return eventlist;
            }
        }

        public static object GetEventNature(long eventid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventlist = (from row1 in entities.EventInstances
                                 join row2 in entities.EventScheduleOns
                                  on row1.ID equals row2.EventInstanceID
                                 where row1.IsDeleted == false
                                 && row1.EventID == eventid
                                 select new
                                 {
                                     row2.ID,
                                     Name = row2.Description
                                 }
                                ).ToList();
                return eventlist;
            }
        }
        public static void UpdateComplianceScheduleOn(long complianceScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn complianceScheduleOn = (from row in entities.ComplianceScheduleOns
                                                             where row.ID == complianceScheduleOnID
                                                             select row).FirstOrDefault();
                complianceScheduleOn.IsActive = false;
                complianceScheduleOn.IsUpcomingNotDeleted = false;
                entities.SaveChanges();
            }
        }


        public static List<DateTime> GetComplianceSchdeuleDates(long ComplianceInstanceID, long EventScheduleOnID, long ParentEventD, long IntermediateEventID, long SubEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceScheduleOns
                            where row.ComplianceInstanceID == ComplianceInstanceID
                            && row.EventScheduledOnID == EventScheduleOnID
                            && row.IsActive == true && row.IsUpcomingNotDeleted == true
                            && row.ParentEventD == ParentEventD && row.IntermediateEventID == IntermediateEventID
                            && row.SubEventID == SubEventID
                            select row.ScheduleOn).ToList();




                return data;
            }
        }
        public static void UpdateEventComplianceClosure(EventComplianceClosure eventComplianceClosure)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                EventComplianceClosure eventComplianceMapping = (from row in entities.EventComplianceClosures
                                                                 where row.ComplianceID == eventComplianceClosure.ComplianceID
                                                                 && row.ComplianceInstanceID == eventComplianceClosure.ComplianceInstanceID
                                                                 && row.EventScheduleOnID == eventComplianceClosure.EventScheduleOnID
                                                                 && row.ParentEventD == eventComplianceClosure.ParentEventD
                                                                 && row.IntermediateEventID == eventComplianceClosure.IntermediateEventID
                                                                 && row.SubEventID == eventComplianceClosure.SubEventID
                                                                 select row).FirstOrDefault();

                eventComplianceMapping.FromDate = eventComplianceClosure.FromDate;
                eventComplianceMapping.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static void CreateEventComplianceClosure(EventComplianceClosure Mapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventComplianceClosures.Add(Mapping);
                entities.SaveChanges();
            }
        }
        public static bool CheckEventComplianceClosureExist(long complianceID, long ClosureComplianceInstanceID, long EventScheduledOnID, long ParentEventD, long IntermediateEventID, long SubEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.EventComplianceClosures
                             where row.ComplianceID == complianceID
                             && row.ComplianceInstanceID == ClosureComplianceInstanceID
                             && row.EventScheduleOnID == EventScheduledOnID
                             && row.ParentEventD == ParentEventD
                             && row.IntermediateEventID == IntermediateEventID
                             && row.SubEventID == SubEventID
                             select row).FirstOrDefault();
                if (query == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static EventAutoTrigger GetAutoTrigger(int ParentEventID, int IntermediateEventID, int SubEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.EventAutoTriggers
                             where row.ParentEventID == ParentEventID
                             && row.IntermediateEventID == IntermediateEventID
                             && row.SubEventID == SubEventID
                             select row).FirstOrDefault();

                return query;
            }
        }
        public static void CreateEventAutoTrigger(EventAutoTrigger Mapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventAutoTriggers.Add(Mapping);
                entities.SaveChanges();
            }
        }

        public static void UpdateEventAutoTrigger(EventAutoTrigger eventAutoTrigger)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                EventAutoTrigger eeventAutoTrigger = (from row in entities.EventAutoTriggers
                                                      where row.ParentEventID == eventAutoTrigger.ParentEventID && row.IntermediateEventID == eventAutoTrigger.IntermediateEventID
                                                      && row.SubEventID == eventAutoTrigger.SubEventID
                                                      && row.ID == eventAutoTrigger.ID
                                                      select row).FirstOrDefault();
                eeventAutoTrigger.UpdatedOn = DateTime.Now;
                eeventAutoTrigger.AutoTriggerDays = eventAutoTrigger.AutoTriggerDays;
                eeventAutoTrigger.IsAutoTrigger = eventAutoTrigger.IsAutoTrigger;
                entities.SaveChanges();
            }
        }
        public static EventAutoTrigger GetEventAutoTrigger(int ParentEventID, int IntermediateEventID, int SubEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.EventAutoTriggers
                             where row.ParentEventID == ParentEventID
                             && row.IntermediateEventID == IntermediateEventID
                             && row.SubEventID == SubEventID
                             select row).FirstOrDefault();
                return query;
                //return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static void UpdateEventSubEventMappedID(long EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.EventSubEventMappings
                           where row.ParentEventID == EventID && row.ComplianceID == 0
                           //&& row.IntermediateFlag == InterFlag
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    EventSubEventMapping prevmappedids = (from row in entities.EventSubEventMappings
                                                          where row.ID == entry
                                                          select row).FirstOrDefault();
                    entities.EventSubEventMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }
        public static List<Event> GetSubEventRelationshipMappedID(long eventId, int CompanyType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.Events
                                      join row1 in entities.EventSubEventMappings
                                      on row.ID equals row1.SubEventID
                                      where row1.ParentEventID == eventId && row1.IsActive == true && row.Type == CompanyType && row1.ComplianceID == 0
                                      select row).ToList();
                return eventMappedIDs;
            }

        }
        public static List<Event> GetIntermediateEventRelationshipMappedID(long eventId, int CompanyType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.Events
                                      join row1 in entities.EventSubEventMappings
                                      on row.ID equals row1.IntermediateEventID
                                      where row1.ParentEventID == eventId && row1.IsActive == true && row.Type == CompanyType && row1.ComplianceID == 0
                                      select row).ToList();
                return eventMappedIDs;
            }

        }
        public static SP_GetActivatedEventData_Result GetActivatedEventData(int ParentEventID, int eventScheduleOnId, int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetActivatedEventData(ParentEventID, eventScheduleOnId, Type)
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static bool ExistsEventMappingForEvent(int EventID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean flagSubeventmaaping = false;
                Boolean flagParenteventmaaping = false;
                Boolean flagassignment = false;

                //Subevent check
                var query = (from row in entities.EventMappings
                             where row.EventID == EventID
                             select row.ID).Distinct().ToList();

                if (query != null && query.Count() > 0)
                    flagSubeventmaaping = true;
                else
                    flagSubeventmaaping = false;

                //event check
                var query1 = (from row in entities.EventSubEventMappings
                              where row.SubEventID == EventID || row.ParentEventID == EventID
                              select row.ID).Distinct().ToList();

                if (query1 != null && query1.Count() > 0)
                    flagParenteventmaaping = true;
                else
                    flagParenteventmaaping = false;

                //Assignment check
                var query2 = (from row in entities.EventInstances
                              join row1 in entities.EventAssignments
                              on row.ID equals row1.EventInstanceID
                              where row.EventID == EventID
                              select row.ID).Distinct().ToList();

                if (query2 != null && query2.Count() > 0)
                    flagassignment = true;
                else
                    flagassignment = false;

                if (flagSubeventmaaping == true || flagParenteventmaaping == true || flagassignment == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        
        public static bool ExistsInternalComplianceInEvent(long ParentEventID, long IntermediateEventID, long SubEventID, long InternalComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.EventInternalComplianceMappings
                             where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                             && row.SubEventID == SubEventID && row.InternalComplianceID == InternalComplianceID
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static List<InternalCompliance> GetInternalEventCompliance(int CustomerID, int CustomerbranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.InternalCompliances
                                  join row1 in entities.InternalComplianceInstances
                                  on row.ID equals row1.InternalComplianceID
                                  join row2 in entities.InternalComplianceAssignments
                                  on row1.ID equals row2.InternalComplianceInstanceID
                                  where row.CustomerID == CustomerID
                                  && row1.CustomerBranchID == CustomerbranchID
                                   && row.IsDeleted == false
                                   && row.EventFlag == true
                                  select row).Distinct().ToList();

                return compliance;
            }
        }
        public static void UpdateInternalComplianceSheduleOnDates(int ParentEventID, int IntermediateEventID, int subEventID, int EventSheduleOnID, int ComplinaceID, int ComplianceInstanceID, DateTime UpdatedDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceScheduledOn ScheduleOns = (from row in entities.InternalComplianceScheduledOns
                                                             join row1 in entities.InternalComplianceInstances
                                                             on row.InternalComplianceInstanceID equals row1.ID
                                                             where row1.InternalComplianceID == ComplinaceID
                                                             && row.EventScheduledOnID == EventSheduleOnID
                                                             && row.ParentEventD == ParentEventID
                                                             && row.IntermediateEventID == IntermediateEventID
                                                             && row.SubEventID == subEventID
                                                             && row1.ID == ComplianceInstanceID
                                                             select row).FirstOrDefault();
                ScheduleOns.ScheduledOn = UpdatedDate;
                entities.SaveChanges();
            }
        }

        public static void UpdateInternalReminderSheduleOnDates(int EventSheduleOnID, int ComplinaceID, DateTime UpdatedDate, int ComplianceInstanceID, DateTime Date)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<InternalComplianceReminder> InternalcomplianceReminder = (from row in entities.InternalComplianceInstances
                                                                               join row1 in entities.InternalComplianceAssignments
                                                                               on row.ID equals row1.InternalComplianceInstanceID
                                                                               join row2 in entities.InternalComplianceScheduledOns on
                                                                               row1.InternalComplianceInstanceID equals row2.InternalComplianceInstanceID
                                                                               join row3 in entities.InternalComplianceReminders on
                                                                               row1.ID equals row3.ComplianceAssignmentID
                                                                               where row.InternalComplianceID == ComplinaceID
                                                                               && row2.EventScheduledOnID == EventSheduleOnID
                                                                               && row.ID == ComplianceInstanceID
                                                                               && row2.ScheduledOn == row3.ComplianceDueDate
                                                                               && row3.EventScheduledOnID == EventSheduleOnID
                                                                               select row3).ToList();

                List<int> ComplianceAssignmentList = new List<int>();
                foreach (var cil in InternalcomplianceReminder)
                {
                    ComplianceAssignmentList.Add(cil.ComplianceAssignmentID);
                }
                foreach (var cil in InternalcomplianceReminder)
                {
                    entities.InternalComplianceReminders.Remove(cil);
                    entities.SaveChanges();
                }

                foreach (var AssignID in ComplianceAssignmentList.Distinct())
                {
                    InternalComplianceReminder reminder = new InternalComplianceReminder()
                    {
                        ComplianceAssignmentID = AssignID,
                        ReminderTemplateID = 1,
                        ComplianceDueDate = UpdatedDate,
                        EventScheduledOnID = EventSheduleOnID,
                        RemindOn = UpdatedDate.AddDays(-1 * 2),
                    };
                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                    entities.InternalComplianceReminders.Add(reminder);


                    InternalComplianceReminder reminder1 = new InternalComplianceReminder()
                    {
                        ComplianceAssignmentID = AssignID,
                        ReminderTemplateID = 1,
                        ComplianceDueDate = UpdatedDate,
                        EventScheduledOnID = EventSheduleOnID,
                        RemindOn = UpdatedDate.AddDays(-1 * 7),
                    };
                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                    entities.InternalComplianceReminders.Add(reminder1);


                    InternalComplianceReminder reminder2 = new InternalComplianceReminder()
                    {
                        ComplianceAssignmentID = AssignID,
                        ReminderTemplateID = 1,
                        ComplianceDueDate = UpdatedDate,
                        EventScheduledOnID = EventSheduleOnID,
                        RemindOn = UpdatedDate.AddDays(-1 * 15),
                    };
                    reminder2.Status = (byte)(reminder2.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                    entities.InternalComplianceReminders.Add(reminder2);

                    entities.SaveChanges();
                }
            }
        }

        public static void GenerateEventInternalComplianceScheduele(int ParentEventID, int IntermediateEventID, int subEventID, long EventScheduledOnID, DateTime eventHeldOnDate, long ComplianceID, int ComplianceInstanceID, int days)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int IncludeDays = 0;
                if (days == 0)
                {
                    IncludeDays = 0;
                }
                else
                {
                    if (days > 0)
                    {
                        IncludeDays = days - 1;
                    }
                    else
                    {
                        IncludeDays = days + 1;
                    }
                }
                DateTime scheduledOnDate = eventHeldOnDate.AddDays(Convert.ToInt32(IncludeDays));

                InternalComplianceScheduledOn complianceScheduleon = new InternalComplianceScheduledOn();
                complianceScheduleon.InternalComplianceInstanceID = ComplianceInstanceID;
                complianceScheduleon.ParentEventD = ParentEventID;
                complianceScheduleon.IntermediateEventID = IntermediateEventID;
                complianceScheduleon.SubEventID = subEventID;
                complianceScheduleon.ScheduledOn = scheduledOnDate;
                complianceScheduleon.IsActive = true;
                complianceScheduleon.IsUpcomingNotDeleted = true;
                complianceScheduleon.EventScheduledOnID = EventScheduledOnID;
                entities.InternalComplianceScheduledOns.Add(complianceScheduleon);
                entities.SaveChanges();

                var AssignedRole = InternalComplianceManagement.GetAssignedUsers(ComplianceInstanceID);
                var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                if (performerRole != null)
                {
                    var user = UserManagement.GetByID((int)performerRole.UserID);
                    InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                    {
                        InternalComplianceInstanceID = ComplianceInstanceID,
                        InternalComplianceScheduledOnID = complianceScheduleon.ID,
                        CreatedBy = performerRole.UserID,
                        CreatedByText = user.FirstName + " " + user.LastName,
                        StatusId = 1,
                        Remarks = "New compliance assigned."
                    };

                    InternalComplianceManagement.CreateTransaction(transaction);

                    foreach (var roles in AssignedRole)
                    {
                        if (roles.RoleID != 6)
                        {
                            InternalComplianceManagement.CreateEventBasedInternalComplinceReminders(ComplianceID, ComplianceInstanceID, roles.ID, eventHeldOnDate, scheduledOnDate, EventScheduledOnID);
                        }
                    }
                }
            }
        }

        public static bool CheckEventInternalComplianceAssigned(int ParentEventID, int IntermediateEventID, int SubEventID, int complianceID, int EventsheduleOnID, int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_CheckInternalComplianceSchedulePresent(ParentEventID, IntermediateEventID, SubEventID, EventsheduleOnID, ComplianceInstanceID, complianceID)
                            select row).FirstOrDefault();

                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static InternalComplianceInstance GetInternalComplianceInstance(long ComplianceID, int BranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.InternalComplianceInstances
                            where row.InternalComplianceID == ComplianceID
                            && row.CustomerBranchID == BranchID
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static bool CheckEventInternalComplianceMappingExist(int complianceID, int ParentEventID, int IntermediateEventID, int SubEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.EventInternalComplianceMappings
                             where row.InternalComplianceID == complianceID
                             && row.ParentEventID == ParentEventID
                             && row.IntermediateEventID == IntermediateEventID
                             && row.SubEventID == SubEventID
                             select row);

                return query.Select(entry => true).FirstOrDefault();

            }
        }
        public static void CreateEventInternalComplianceMapping(EventInternalComplianceMapping Mapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventInternalComplianceMappings.Add(Mapping);
                entities.SaveChanges();
            }
        }
        public static void ChengeFlagGenerateSchedule(List<long> complianclist, int CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var row in entities.ComplianceInstances.Where(x => complianclist.Contains(x.ComplianceId) && x.CustomerBranchID == CustomerBranchID).ToList())
                {
                    row.GenerateSchedule = false;
                }
                entities.SaveChanges();
            }
        }
        public static List<long> GetStatutoryChecklistCompliance(int eventType, List<long> Eventlist, int baranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (eventType == 1)  //Secretrial
                {
                    List<long> AssignDaysCompliance = (from row in entities.EventCompAssignDays
                                                       join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                       join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                       join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                       join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                       where Eventlist.Contains(row.ParentEventID) && row5.ID == baranchID
                                                       && row1.EventFlag == null && row1.ComplianceType == 1
                                                       select (long)row.ComplianceID).Distinct().ToList();
                    return AssignDaysCompliance;

                }
                else if (eventType == 2) //NonSecretrial
                {
                    List<long> AssignDaysCompliance = (from row in entities.EventCompAssignDays
                                                       join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                       join row6 in entities.Acts on row1.ActID equals row6.ID
                                                       join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                       join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                       join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                       where Eventlist.Contains(row.ParentEventID) && row5.ID == baranchID
                                                       && (row6.StateID == row5.StateID || row6.ComplianceTypeId == 2)
                                                       & row1.EventFlag == null && row1.ComplianceType == 1
                                                       select (long)row.ComplianceID).Distinct().ToList();

                    return AssignDaysCompliance;
                }
                return null;
            }
        }

        public static List<Sp_GetStatutoryInternalAssignment_Result> GetComplianceAssignedNew(int userId, int CustomerID, int branch, string type, string userType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserDetails = UserManagement.GetByID(userId);
                if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
                {
                    if ((bool)UserDetails.IsHead)
                    {
                        userType = "DEPT";
                    }
                }

                if (userType.Equals("MGMT"))
                {
                    var result = entities.Sp_GetStatutoryInternalAssignment(CustomerID, type, "MGMT", userId).ToList();
                    if (branch != -1)
                    {
                        result = result.Where(entry => entry.BranchID == branch).ToList();
                    }
                    return result;
                }
                else if (userType.Equals("DEPT"))
                {
                    var result = entities.Sp_GetStatutoryInternalAssignment(CustomerID, type, "DEPT", userId).ToList();
                    if (branch != -1)
                    {
                        result = result.Where(entry => entry.BranchID == branch).ToList();
                    }
                    return result;
                }
                else
                {
                    var result = entities.Sp_GetStatutoryInternalAssignment(CustomerID, type, "EXCT", userId).ToList();

                    if (branch != -1)
                    {
                        result = result.Where(entry => entry.BranchID == branch).ToList();
                    }
                    return result;
                }
            }
        }
        public static void UpdateComplianceInstanceScheduleFlag(long ComplinaceID, long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceInstance complianceInstance = (from row in entities.ComplianceInstances
                                                         where row.ComplianceId == ComplinaceID
                                                         && row.ID == ComplianceInstanceID
                                                         select row).FirstOrDefault();
                complianceInstance.GenerateSchedule = false;
                complianceInstance.IsAvantis = true;
                entities.SaveChanges();
            }
        }
        public static bool CheckIndustryMapping(int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.CustomerBranchIndustryMappings
                            where row.CustomerBranchID == customerBranchID
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool CheckComplianceStatutoryChecklist(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            && row.ComplianceType == 1 && row.EventFlag == null
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static long GenerateEventComplianceSchedueleEntryNotApplicable(int ParentEventID, int IntermediateEventID, int subEventID, long EventScheduledOnID, long ComplianceID, int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                complianceScheduleon.ComplianceInstanceID = ComplianceInstanceID;
                complianceScheduleon.ParentEventD = ParentEventID;
                complianceScheduleon.IntermediateEventID = IntermediateEventID;
                complianceScheduleon.SubEventID = subEventID;
                complianceScheduleon.ScheduleOn = DateTime.Now.Date;
                complianceScheduleon.IsActive = true;
                complianceScheduleon.IsUpcomingNotDeleted = true;
                complianceScheduleon.EventScheduledOnID = EventScheduledOnID;
                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                entities.SaveChanges();

                var AssignedRole = ComplianceManagement.GetAssignedUsers(ComplianceInstanceID);
                var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                if (performerRole != null)
                {
                    var user = UserManagement.GetByID((int)performerRole.UserID);
                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceInstanceId = ComplianceInstanceID,
                        ComplianceScheduleOnID = complianceScheduleon.ID,
                        CreatedBy = performerRole.UserID,
                        CreatedByText = user.FirstName + " " + user.LastName,
                        StatusId = 1,
                        Remarks = "New compliance assigned."
                    };

                    ComplianceManagement.CreateTransaction(transaction);

                }
                return complianceScheduleon.ID;
            }
        }

        public static void CreateEventComplianceTransactionNotApplicable(long ComplianceSchedueleID, int ParentEventID, int IntermediateEventID, int subEventID, long EventScheduledOnID, long ComplianceID, int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedRole = ComplianceManagement.GetAssignedUsers(ComplianceInstanceID);
                var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                if (performerRole != null)
                {
                    var user = UserManagement.GetByID((int)performerRole.UserID);
                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceInstanceId = ComplianceInstanceID,
                        ComplianceScheduleOnID = ComplianceSchedueleID,
                        CreatedBy = performerRole.UserID,
                        CreatedByText = user.FirstName + " " + user.LastName,
                        StatusId = 4,
                        Remarks = "Compliance not applicable."
                    };

                    ComplianceManagement.CreateTransaction(transaction);
                }
            }
        }

        public static long GetComplianceSheduleOnID(int ParentEventID, int IntermediateEventID, int subEventID, int EventSheduleOnID, int ComplinaceID, int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                    join row1 in entities.ComplianceInstances
                                    on row.ComplianceInstanceID equals row1.ID
                                    where row1.ComplianceId == ComplinaceID
                                    && row.EventScheduledOnID == EventSheduleOnID
                                    && row.ParentEventD == ParentEventID
                                    && row.IntermediateEventID == IntermediateEventID
                                    && row.SubEventID == subEventID
                                    && row1.ID == ComplianceInstanceID
                                    select row.ID).FirstOrDefault();
                return ScheduleOnID;
            }
        }

        public static Boolean CheckClosedTransaction(long ComplianceSchedueleID, int ParentEventID, int IntermediateEventID, int subEventID, int EventSheduleOnID, int ComplinaceID, int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean transaction;
                var Transaction = (from row in entities.ComplianceScheduleOns
                                   join row1 in entities.ComplianceInstances
                                   on row.ComplianceInstanceID equals row1.ID
                                   join row2 in entities.ComplianceTransactions
                                   on row.ID equals row2.ComplianceScheduleOnID
                                   where row1.ComplianceId == ComplinaceID
                                   && row.EventScheduledOnID == EventSheduleOnID
                                   && row.ParentEventD == ParentEventID
                                   && row.IntermediateEventID == IntermediateEventID
                                   && row.SubEventID == subEventID
                                   && row1.ID == ComplianceInstanceID
                                   && (row2.StatusId == 4 || row2.StatusId == 5)
                                   && row.ID == ComplianceSchedueleID
                                   select row2).FirstOrDefault();

                if (Transaction == null)
                {
                    transaction = false;
                }
                else
                {
                    transaction = true;
                }
                return transaction;
            }
        }
        public static long? GetEventClassification(long eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long? EventClassificationID = (from row in entities.Events
                                               where row.ID == eventID
                                               select row.EventClassificationID).FirstOrDefault();


                return EventClassificationID;
            }
        }

        public static long? GetEventLocation(long EventScheduledOnId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long? CustomerBranchID = (from row in entities.EventScheduleOns
                                          join row1 in entities.EventInstances on
                                          row.EventInstanceID equals row1.ID
                                          where row.ID == EventScheduledOnId
                                          select row1.CustomerBranchID).FirstOrDefault();
                return CustomerBranchID;
            }
        }
        public static List<SP_GetNonSecretrialIntermediateComplianceAssignWithShortNoticeDays_Result> GetNonSecretrialIntermediateSubEventToCompliance(int ParentEventID, int IntermediateEventID, int SubEventid, int eventScheduleOnId, int Type, int CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetNonSecretrialIntermediateComplianceAssignWithShortNoticeDays(ParentEventID, IntermediateEventID, SubEventid, eventScheduleOnId, Type, CustomerBranchID)
                            select row).ToList();
                return data;
            }
        }
        public static List<SP_GetNonSecretrialComplianceWithShortNoticeDays_Result> GetNonSecretrialSubEventToCompliance(int ParentEventID, int SubEventid, int eventScheduleOnId, int Type, int CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetNonSecretrialComplianceWithShortNoticeDays(ParentEventID, SubEventid, eventScheduleOnId, Type, CustomerBranchID)
                            select row).ToList();
                return data;
            }
        }

        public static List<SP_GetIntermediateComplianceAssignWithShortNoticeDays_Result> GetIntermediateSubEventToCompliance(int ParentEventID, int IntermediateEventID, int SubEventid, int eventScheduleOnId, int Type, int CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetIntermediateComplianceAssignWithShortNoticeDays(ParentEventID, IntermediateEventID, SubEventid, eventScheduleOnId, Type, CustomerBranchID)
                            select row).ToList();
                return data;
            }
        }
        public static List<SP_GetComplianceWithShortNoticeDays_Result> GetSubEventToCompliance(int ParentEventID, int SubEventid, int eventScheduleOnId, int Type, int CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetComplianceWithShortNoticeDays(ParentEventID, SubEventid, eventScheduleOnId, Type, CustomerBranchID)
                            select row).ToList();
                return data;
            }
        }
        public static SP_GetActivatedSubEvent_Result GetEventSchedueleData(int ParentEventID, int eventScheduleOnId, int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetActivatedSubEvent(ParentEventID, eventScheduleOnId, Type)
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static SP_GetActivatedIntermediateEvent_Result GetIntermediateEventSchedueleData(int ParentEventID, int eventScheduleOnId, int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetActivatedIntermediateEvent(ParentEventID, eventScheduleOnId, Type)
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static SP_GetActivatedIntermediateSubEvent_Result GetActivatedIntermediateSubEventSchedueleData(int ParentEventID, int IntermediateEventID, int eventScheduleOnId, int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_GetActivatedIntermediateSubEvent(IntermediateEventID, eventScheduleOnId, ParentEventID, Type)
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static bool CheckEventStructure(long eventScheduleOnId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.EventAssignDates
                            where row.EventScheduleOnID == eventScheduleOnId && row.IntermediateEventID != 0
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static List<EventSubClassification> GetEventSubClassification(long EventClassificationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventSubClassification = (from row in entities.EventSubClassifications
                                              where row.EventClassificationID == EventClassificationID
                                              select row).ToList();

                return eventSubClassification.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<EventClassification> GetEventClassification()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventClassification = entities.EventClassifications;
                return eventClassification.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<EventTypeData> GetLocationClassification()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventTypeData = entities.EventTypeDatas;
                return eventTypeData.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Event> GetCheckdEvent(List<long> EventList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<Event> eventList = new List<Event>();
                eventList = (from row in entities.Events
                             where EventList.Contains(row.ID) && row.IsDeleted == false
                             select row).ToList();

                return eventList;
            }
        }
        public static string CheckEventComplianceType(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Compliances
                            where row.ID == complianceID
                            select row).FirstOrDefault();
                //if (data.ComplinceVisible == true)
                //{
                //    return "Actionable";
                //}
                //else if (data.ComplinceVisible == false)
                //{
                //    return "Informational";
                //}

                if (data.ComplianceType == 0 || data.ComplianceType == 2)
                {
                    return "Statutory";
                }
                else

                if (data.ComplinceVisible == true && data.ComplianceType == 1)
                {
                    return "Actionable";
                }
                else if (data.ComplinceVisible == false && data.ComplianceType == 1)
                {
                    return "Informational";
                }
                return "";
            }
        }
        public static List<SP_GetSubEventName_Result> GetSubEvent(int ParentEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetSubEventName_Result> eventList = new List<SP_GetSubEventName_Result>();

                var data = (from row in entities.SP_GetSubEventName(ParentEventID)
                            select row).ToList();

                return data;
            }
        }
        public static List<Event> GetParentEvent(List<Event> eventList, int CompanyType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Event> eventlst = new List<Event>();
                eventlst = eventList.Where(entry => entry.Type == CompanyType).OrderBy(entry => entry.Name).ToList();
                return eventlst;
            }
        }
        public static List<Event> GetParentEvent(int CompanyType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Event> eventList = new List<Event>();

                eventList = (from row in entities.Events
                             where row.Visible == "Y"
                             && row.Type == CompanyType
                              && row.IsDeleted == false
                             select row).OrderBy(entry => entry.Name).ToList();
                return eventList;
            }
        }
        public static Event GetEventType(int EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Events
                            where row.ID == EventID
                            select row).FirstOrDefault();
                return data;
            }
        }
        public static List<SP_Get_MappedEventComplianceMappingList_Result> GetAllMappedCompliances(int EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MappedLst = (from row in entities.SP_Get_MappedEventComplianceMappingList(EventID)
                                 select row).ToList();

                //var MappedLst = (from row in entities.Compliances
                //                 join row1 in entities.EventMappings
                //                 on row.ID equals row1.ComplianceID
                //                 where row1.EventID == EventID && row.IsDeleted == false
                //                 select row).ToList();

                return MappedLst;
            }
        }
        public static List<Event_Assigned_View> GetAllAssignedInstancesCount(int EventClassificationID, int eventRoleID, string StringType, int branchID = -1, int userID = -1, int customerId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.Event_Assigned_View
                                                where row.RoleID == eventRoleID
                                                select row);
                if (EventClassificationID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.EventClassificationID == EventClassificationID);
                }
                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                if (StringType != "")
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.Name.Contains(StringType));
                    //complianceInstancesQuery = complianceInstancesQuery.Where(entry => (entry.Name.Contains(StringType))).ToList();
                }

                return complianceInstancesQuery.ToList(); //complianceInstancesQuery.OrderByDescending(entry => entry.EventAssignmentID).ToList();
            }
        }

        //public static List<ActiveEventDetailView> GetClosedEvents(long Userid)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var data = (from row in entities.ActiveEventDetailViews
        //                    join row1 in entities.ClosedEventDetails
        //                    on row.EventScheduledOnId equals row1.EventSchuduleID
        //                    where row.UserID == Userid
        //                    select row).ToList();
        //        return data;
        //    }
        //}

        public static List<SP_ActiveEventDetail_Result> GetActivatedEvnts()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_ActiveEventDetail()
                            select row).ToList();
                return data;
            }
        }


        public static List<SP_ClosedEvent_Result> GetClosedEvents(long Userid, int EventClassificationID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_ClosedEvent()
                            join row1 in entities.ClosedEventDetails
                            on row.EventScheduledOnId equals row1.EventSchuduleID
                            where row.UserID == Userid
                            select row).ToList();
                if (EventClassificationID != -1)
                {
                    data = data.Where(entry => entry.EventClassificationID == EventClassificationID).ToList();
                }
                return data;
            }
        }

        public static bool CheckEventClosed(long EventScheduledOnId, long EventID)
        {
            long? EventClassificationID = GetEventClassification(EventID);
            long? CustomerBranchID = GetEventLocation(EventScheduledOnId);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (EventClassificationID == 1)
                {
                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == EventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();

                    var EventAssignedComplianceList = (from row in entities.EventCompAssignDays
                                                       join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                       join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                       join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                       join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                       where row.ParentEventID == EventID && row1.ComplinceVisible == true
                                                       && row5.ID == CustomerBranchID
                                                       select row).ToList();

                    if (ComplainceTransCompleteList.Count >= EventAssignedComplianceList.Count)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (EventClassificationID == 2)
                {

                    var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                       join row1 in entities.ComplianceTransactions
                                                       on row.ID equals row1.ComplianceScheduleOnID
                                                       where row.ParentEventD == EventID && row.EventScheduledOnID == EventScheduledOnId
                                                       && (row1.StatusId == 4 || row1.StatusId == 5)
                                                       select row).ToList();


                    List<long> EventAssignedComplianceList = (from row in entities.EventCompAssignDays
                                                              join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                              join row6 in entities.Acts on row1.ActID equals row6.ID
                                                              join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                              join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                              join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                              where row.ParentEventID == EventID && row5.ID == CustomerBranchID && row1.ComplinceVisible == true
                                                              && (row6.StateID == row5.StateID || row6.ComplianceTypeId == 2)
                                                              select (long)row.ComplianceID).Distinct().ToList();

                    if (ComplainceTransCompleteList.Count >= EventAssignedComplianceList.Count)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return false;
            }

        }

        public static void CreateClosedEvent(ClosedEventDetail eventData)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ClosedEventDetails.Add(eventData);
                entities.SaveChanges();
            }
        }
        public static bool CheckClosedEventEntryPresent(long UserID, int CustomerID, int CustomerBranchID, long EventID, long EventSchuduleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClosedEventDetails
                            where row.UserID == UserID && row.CustomerID == CustomerID &&
                            row.CustomerBranchID == CustomerBranchID && row.EventID == EventID && row.EventSchuduleID == EventSchuduleID
                            select row).FirstOrDefault();

                if (data != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static void UpdateClosedData(EventScheduleOn eventData)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                EventScheduleOn eventToUpdate = (from row in entities.EventScheduleOns
                                                 where row.ID == eventData.ID
                                                 select row).FirstOrDefault();

                eventToUpdate.ClosedDate = eventData.ClosedDate;
                eventToUpdate.ClosedEventFlag = eventData.ClosedEventFlag;
                entities.SaveChanges();
            }
        }
        public static int count = 0;

        #region Event Category

        public static List<EventCategory> GetAll()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventCategories = (from row in entities.EventCategories
                                       select row);

                return eventCategories.OrderBy(entry => entry.Name).ToList();
            }
        }

        #endregion

        #region Event Management 

        public static void Delete(int eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Event eventToDelete = (from row in entities.Events
                                       where row.ID == eventID
                                       select row).FirstOrDefault();

                eventToDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static bool ExistsDailyUpdate(string Title)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.DailyUpdates
                             where row.Title.Equals(Title) && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool ExistsNewsLetter(string Title)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.NewsLetters
                             where row.Title.Equals(Title) && row.IsDeleted == false
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool Exists(string eventName, int? Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Events
                             where row.Name.Equals(eventName) && row.IsDeleted == false
                             && row.Type == Type
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool ExistsID(long eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Events
                             where row.ID == eventID && row.IsDeleted == false
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static int GETEVENTID(string eventName)
        {
            using (var context = new ComplianceDBEntities())
            {
                var query = (from E in context.Events
                             where E.Name == eventName
                             select E);

                var eventid = query.FirstOrDefault<Event>();
                return Convert.ToInt32(eventid.ID);
            }
        }

        public static int GETEVENTandSUBEVENTid(string eventName, string subeventName)
        {
            using (var context = new ComplianceDBEntities())
            {

                var query = (from E in context.Events
                             join SE in context.SubEvents
                             on E.ID equals SE.EventID
                             where E.Name == eventName && SE.Name == subeventName
                             select SE);
                var eventid = query.FirstOrDefault<SubEvent>();
                return Convert.ToInt32(eventid.ID);

            }
        }
        public static void Update(Event eventData)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                Event eventToUpdate = (from row in entities.Events
                                       where row.ID == eventData.ID
                                       select row).FirstOrDefault();

                eventToUpdate.Name = eventData.Name;
                eventToUpdate.Description = eventData.Description;
                eventToUpdate.EventCategoryId = eventData.EventCategoryId;
                eventToUpdate.YearType = eventData.YearType;
                eventToUpdate.Gap = eventData.Gap;
                eventToUpdate.Frequency = eventData.Frequency;
                eventToUpdate.UpdatedBy = eventData.UpdatedBy;
                eventToUpdate.UpdatedOn = eventData.UpdatedOn;
                eventToUpdate.IsDeleted = false;
                eventToUpdate.Visible = eventData.Visible;
                eventToUpdate.Type = eventData.Type;
                eventToUpdate.EventClassificationID = eventData.EventClassificationID;
                eventToUpdate.EventSubClassificationID = eventData.EventSubClassificationID;
                eventToUpdate.EType = eventData.EType;
                //  eventToUpdate.EventTypeID = eventData.EventTypeID;
                entities.SaveChanges();

                if (eventData.Frequency.HasValue)
                {
                    GenerateDefaultScheduleForEvent(eventData.ID, eventData.YearType, true);
                }

            }
        }


        public static void Create(Event eventData)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Events.Add(eventData);
                entities.SaveChanges();
                if (eventData.Frequency.HasValue)
                {
                    GenerateDefaultScheduleForEvent(eventData.ID, eventData.YearType);
                }
            }
        }

        public static Event GetByID(long eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Events
                            where row.ID == eventID
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static ComplianceInstance GetComplianceInstance(long ComplianceID, int BranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceInstances
                            where row.ComplianceId == ComplianceID
                            && row.CustomerBranchID == BranchID
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static List<Event> EventsGetBycategoryId(int CategoryId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventList = (from row in entities.Events
                                 where row.EventCategoryId == CategoryId
                                 select row).ToList();

                return eventList;
            }
        }

        public static List<EventView> GetAllEvents(int catagoryid, string filter, bool all = true)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                List<long> eventIDs = (from row in entities.Compliances
                                       where row.EventID != null
                                       select (long)row.EventID).ToList();

                List<EventView> eventList = new List<EventView>();

                if (all)
                {
                    eventList = (from row in entities.EventViews
                                 select row).ToList();
                }
                else
                {
                    eventList = (from row in entities.EventViews
                                 where eventIDs.Contains(row.ID)
                                 select row).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    eventList = eventList.Where(entry => entry.Name.Contains(filter)).ToList();
                }

                if (catagoryid != -1)
                {
                    eventList = eventList.Where(entry => entry.EventCategoryId == catagoryid).ToList();
                }

                return eventList;
            }
        }

        public static void GenerateDefaultScheduleForEvent(long EventID, string year, bool deleteOldData = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (deleteOldData)
                {
                    var ids = (from row in entities.EventSchedules
                               where row.EventID == EventID
                               select row.ID).ToList();

                    ids.ForEach(entry =>
                    {
                        //EventSchedule schedule = new EventSchedule() { ID = entry };
                        //entities.EventSchedules.Attach(schedule);
                        EventSchedule schedule = (from row in entities.EventSchedules
                                                  where row.ID == entry
                                                  select row).FirstOrDefault();

                        entities.EventSchedules.Remove(schedule);
                    });

                    entities.SaveChanges();
                }
                var compliance = (from row in entities.Events
                                  where row.ID == EventID
                                  select row).First();

                int step = 1;

                switch ((Frequency)compliance.Frequency.Value)
                {
                    case Frequency.Quarterly:
                        step = 3;
                        break;
                    case Frequency.HalfYearly:
                        step = 6;
                        break;
                    case Frequency.Annual:
                        step = 12;
                        break;
                }

                int i = 1;
                if (year.Equals("FY") && ((Frequency)compliance.Frequency.Value == Frequency.HalfYearly || (Frequency)compliance.Frequency.Value == Frequency.Annual))
                {
                    i = 4;
                }

                for (int month = i; month <= 12; month += step)
                {
                    EventSchedule eventSchedule = new EventSchedule();
                    eventSchedule.EventID = EventID;
                    eventSchedule.ForMonth = month;
                    int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                    eventSchedule.SpecialDate = "01" + month.ToString("D2");
                    entities.EventSchedules.Add(eventSchedule);
                }
                entities.SaveChanges();
            }
        }

        public static void EventInstances(List<Tuple<EventInstance, EventAssignment>> assignments, long createdByID)
        {
            if (assignments.Count > 0)
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    assignments.ForEach(entry =>
                    {
                        long eventInstanceID = (from row in entities.EventInstances
                                                where row.EventID == entry.Item1.EventID && row.CustomerBranchID == entry.Item1.CustomerBranchID
                                                select row.ID).SingleOrDefault();

                        string yType = (from row in entities.Events
                                        where row.ID == entry.Item1.EventID && row.IsDeleted == false
                                        select row.YearType).SingleOrDefault();

                        //if (eventInstanceID <= 0)
                        //{
                        //    entry.Item1.CreatedBy = Convert.ToInt32(createdByID);
                        //    entry.Item1.CreatedOn = DateTime.UtcNow;
                        //    entry.Item1.IsDeleted = false;
                        //    entities.EventInstances.Add(entry.Item1);
                        //    entities.SaveChanges();

                        //    eventInstanceID = entry.Item1.ID;
                        //    CreateScheduleOn(entry.Item1.StartDate, eventInstanceID, Convert.ToInt64(entry.Item1.EventID), createdByID, yType);

                        //}
                        //else
                        //{
                        //    entry.Item1.ID = eventInstanceID;
                        //}

                        long eventAssignment = (from row in entities.EventAssignments
                                                where row.EventInstanceID == entry.Item1.ID && row.Role == entry.Item2.Role
                                                select row.UserID).FirstOrDefault();

                        if (eventAssignment <= 0)
                        {
                            entry.Item2.EventInstanceID = eventInstanceID;
                            entry.Item2.CreatedOn = DateTime.UtcNow;
                            entry.Item2.UpdatedOn = DateTime.UtcNow;
                            entry.Item2.IsDeleted = false;
                            entities.EventAssignments.Add(entry.Item2);
                            entities.SaveChanges();

                            //if (entry.Item2.Role != 11)
                            //{
                            //    CreateReminders(Convert.ToInt64(entry.Item1.EventID), eventInstanceID, entry.Item2.ID, createdByID);
                            //}
                        }

                    });
                }

            }
        }

        public static void CreateScheduleOn(DateTime StartDate, long eventInstanceId, long EventID, long createdByID, string YearType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime nextDate = StartDate;
                DateTime curruntDate = DateTime.UtcNow;


                Tuple<DateTime, DateTime, string> nextDateMonth = GetNextDate(nextDate, EventID, 0, YearType);

                long ScheduleOnID = (from row in entities.EventScheduleOns
                                     where row.EventInstanceID == eventInstanceId && row.StartDate == nextDateMonth.Item1
                                     && row.EndDate == nextDateMonth.Item2
                                     select row.ID).SingleOrDefault();

                if (ScheduleOnID <= 0)
                {
                    EventScheduleOn eventScheduleon = new EventScheduleOn();
                    eventScheduleon.EventInstanceID = eventInstanceId;
                    eventScheduleon.StartDate = nextDateMonth.Item1;
                    eventScheduleon.EndDate = nextDateMonth.Item2;
                    eventScheduleon.ClosedDate = nextDateMonth.Item2;
                    eventScheduleon.Period = nextDateMonth.Item3;
                    eventScheduleon.CreatedBy = createdByID;
                    eventScheduleon.CreatedOn = DateTime.UtcNow;
                    eventScheduleon.IsDeleted = false;

                    entities.EventScheduleOns.Add(eventScheduleon);
                    entities.SaveChanges();
                    nextDate = nextDateMonth.Item1;
                }
            }
        }

        public static void CreateScheduleOn(DateTime StartDate, long eventInstanceId, long EventID, long createdByID, DateTime HeldOn, string YearType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime closedDate = DateTime.UtcNow;


                Tuple<DateTime, DateTime, string> nextDateMonth = GetNextDate(StartDate, EventID, 1, YearType);

                Event eventObj = EventManagement.GetByID(EventID);

                if (eventObj.Gap != null)
                {
                    HeldOn = HeldOn.AddDays(Convert.ToInt32(eventObj.Gap));
                    if (HeldOn.Date < nextDateMonth.Item2.Date)
                    {
                        closedDate = HeldOn;
                    }
                    else
                    {
                        closedDate = nextDateMonth.Item2;
                    }
                }
                else
                {
                    closedDate = nextDateMonth.Item2;
                }

                long ScheduleOnID = (from row in entities.EventScheduleOns
                                     where row.EventInstanceID == eventInstanceId && row.StartDate == nextDateMonth.Item1
                                     && row.EndDate == nextDateMonth.Item2
                                     select row.ID).SingleOrDefault();

                if (ScheduleOnID <= 0)
                {
                    EventScheduleOn eventScheduleon = new EventScheduleOn();
                    eventScheduleon.EventInstanceID = eventInstanceId;
                    eventScheduleon.StartDate = nextDateMonth.Item1;
                    eventScheduleon.EndDate = nextDateMonth.Item2;
                    eventScheduleon.ClosedDate = closedDate;
                    eventScheduleon.Period = nextDateMonth.Item3;
                    eventScheduleon.CreatedBy = createdByID;
                    eventScheduleon.CreatedOn = DateTime.UtcNow;
                    eventScheduleon.IsDeleted = false;

                    entities.EventScheduleOns.Add(eventScheduleon);
                    entities.SaveChanges();
                }
            }
        }

        private static void CreateReminders(long eventID, long EventInstanceID, long eventAssignmentID, long createdByID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                byte? frequency = (from row in entities.Events
                                   where row.ID == eventID
                                   select row.Frequency).SingleOrDefault();

                if (frequency.HasValue)
                {
                    int day = -1;
                    if (frequency == 0)
                        day = 7;
                    else
                        day = 15;


                    EventScheduleOn ScheduledOnList = (from row in entities.EventScheduleOns
                                                       where row.EventInstanceID == EventInstanceID
                                                       select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                    DateTime temp = Convert.ToDateTime(ScheduledOnList.ClosedDate);
                    for (int i = 0; i < 4; i++)
                    {
                        temp = temp.Date.AddDays(-day);
                        EventReminder reminder = new EventReminder()
                        {
                            RemindOn = temp,
                            EventAssignmentID = eventAssignmentID,
                            CreatedBy = createdByID,
                            CreatedOn = DateTime.UtcNow,
                        };

                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                        entities.EventReminders.Add(reminder);

                    }

                }

                entities.SaveChanges();
            }
        }

        public static Tuple<DateTime, DateTime, string> GetNextDate(DateTime StartDate, long eventID, int condition, string YearType)
        {
            var eventSchedule = GetScheduleByEventID(eventID);

            var objCompliance = GetByID(eventID);

            DateTime date = eventSchedule.Select(row => new DateTime(StartDate.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row <= StartDate).OrderByDescending(entry => entry).FirstOrDefault();

            if (date == DateTime.MinValue)
            {
                if (eventSchedule.Count > 0)
                {
                    if (condition == 0)
                        date = new DateTime(StartDate.Year - 1, Convert.ToInt32(eventSchedule[eventSchedule.Count - 1].SpecialDate.Substring(2, 2)), Convert.ToInt32(eventSchedule[eventSchedule.Count - 1].SpecialDate.Substring(0, 2)));
                    else
                        date = new DateTime(StartDate.Year + 1, Convert.ToInt32(eventSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(eventSchedule[0].SpecialDate.Substring(0, 2)));
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");
            var ForMonthSchedule = eventSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

            Tuple<string, DateTime> forMonth;
            forMonth = GetForMonth(date, Convert.ToInt32(ForMonthSchedule.ForMonth), objCompliance.Frequency, YearType);

            return new Tuple<DateTime, DateTime, string>(date, forMonth.Item2, forMonth.Item1);
        }

        public static List<EventSchedule> GetScheduleByEventID(long eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.EventSchedules
                                    where row.EventID == eventID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }

        public static Tuple<string, DateTime> GetForMonth(DateTime date, int forMonth, byte? Frequency, string YearType)
        {
            Tuple<string, DateTime> forMonthName;
            DateTime endDate = DateTime.Now;
            switch (Frequency)
            {
                case 0:
                    //string year = date.ToString("yy");
                    //if (forMonth == 1)
                    //{
                    //    forMonth = 12;
                    //    year = (date.AddYears(-1)).ToString("yy");
                    //}
                    //else
                    //{
                    //    forMonth = forMonth - 1;
                    //}

                    forMonthName = new Tuple<string, DateTime>(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month).Substring(0, 3) + " " + date.ToString("yy"), date.AddMonths(1).AddDays(-1));
                    break;
                case 1:
                    //string yearq = date.ToString("yy");
                    //if (forMonth == 1)
                    //{
                    //    forMonth = 10;
                    //    yearq = (date.AddYears(-1)).ToString("yy");
                    //}
                    //else
                    //{
                    //    forMonth = forMonth - 3;
                    //}
                    endDate = date.AddMonths(3).AddDays(-1);
                    forMonthName = new Tuple<string, DateTime>(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month).Substring(0, 3) + " " + date.ToString("yy") +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(endDate.Month).Substring(0, 3) + " " + endDate.ToString("yy"), endDate);
                    break;
                case 2:
                    //string year1 = date.ToString("yy");
                    //if (forMonth == 1)
                    //{
                    //    forMonth = 7;
                    //    year1 = (date.AddYears(-1)).ToString("yy");
                    //}
                    //else
                    //{
                    //    forMonth = forMonth - 6;
                    //}
                    endDate = date.AddMonths(6).AddDays(-1);
                    forMonthName = new Tuple<string, DateTime>(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month).Substring(0, 3) + " " + date.ToString("yy") +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(endDate.Month).Substring(0, 3) + " " + endDate.ToString("yy"), endDate);
                    break;

                case 3:

                    //string startFinancial1Year = date.ToString("yy");
                    //string endFinancial1Year = date.ToString("yy");
                    //if (date.Month >= 4)
                    //{
                    //    startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                    //    endFinancial1Year = date.ToString("yy");
                    //}
                    //else
                    //{
                    //    startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                    //    endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                    //}
                    endDate = date.AddMonths(12).AddDays(-1);
                    if (YearType == "CY")
                    {
                        forMonthName = new Tuple<string, DateTime>("CY - " + endDate.ToString("yy"), endDate);  //"CY - " + endDate.ToString("yy");
                    }
                    else
                    {
                        forMonthName = new Tuple<string, DateTime>("FY " + date.ToString("yy") + " - " + endDate.ToString("yy"), endDate);
                    }
                    //forMonthName = new Tuple<string, DateTime>("FY " + date.ToString("yy") + " - " + endDate.ToString("yy"), endDate);
                    break;

                case 4:
                    //string year2 = date.ToString("yy");
                    //if (forMonth == 1)
                    //{
                    //    forMonth = 9;
                    //    year2 = (date.AddYears(-1)).ToString("yy");
                    //}
                    //else
                    //{
                    //    forMonth = forMonth - 4;
                    //}

                    endDate = date.AddMonths(4).AddDays(-1);
                    forMonthName = new Tuple<string, DateTime>(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date.Month).Substring(0, 3) + " " + date.ToString("yy") +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(endDate.Month).Substring(0, 3) + " " + endDate.ToString("yy"), endDate);
                    break;
                case 5:

                    //string startFinancial2Year = date.ToString("yy");
                    //string endFinancial2Year = date.ToString("yy");
                    //if (date.Month >= 4)
                    //{
                    //    startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                    //    endFinancial2Year = date.ToString("yy");
                    //}
                    //else
                    //{
                    //    startFinancial2Year = (date.AddYears(-3)).ToString("yy");
                    //    endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                    //}

                    endDate = date.AddMonths(24).AddDays(-1);
                    forMonthName = new Tuple<string, DateTime>("FY " + date.ToString("yy") + " - " + endDate.ToString("yy"), endDate);

                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    endDate = date.AddMonths(84).AddDays(-1);
                    forMonthName = new Tuple<string, DateTime>("FY " + date.ToString("yy") + " - " + endDate.ToString("yy"), endDate);
                    break;

                default:
                    forMonthName = new Tuple<string, DateTime>("", endDate);
                    break;
            }

            return forMonthName;
        }

        public static List<EventInstanceTransactionView> GetAllAssignedInstances(int branchID = -1, int userID = -1, int customerId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.EventInstanceTransactionViews
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                return complianceInstancesQuery.OrderByDescending(entry => entry.EventInstanceID).ToList();

            }
        }

        public static List<EventInstanceTransactionView> GetAllAssignedInstancesByUser(long userID = -1, long customerId = -1, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.EventInstanceTransactionViews
                                                select row);
                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.Name.Contains(filter) || entry.CustomerBranchName.Contains(filter) || entry.Role.Contains(filter));
                }

                return complianceInstancesQuery.OrderByDescending(entry => entry.EventInstanceID).ToList();

            }
        }

        public static List<EventAssignment> GetAssignedEventsByEvent(long eventID, int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long instanceid = (from row in entities.EventInstances
                                   where row.EventID == eventID && row.CustomerBranchID == customerBranchID
                                   select row.ID).FirstOrDefault();

                List<EventAssignment> eventAssignment = null;
                if (instanceid > 0)
                {
                    eventAssignment = (from row in entities.EventAssignments
                                       where row.EventInstanceID == instanceid
                                       && (row.Role == 10 || row.Role == 11)
                                       select row).ToList();
                }

                return eventAssignment;
            }
        }

        public static List<EventAssignment> GetAssignedEvents(long eventInstanceID, int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<EventAssignment> eventAssignment = null;
                if (eventInstanceID > 0)
                {
                    eventAssignment = (from row in entities.EventAssignments
                                       where row.EventInstanceID == eventInstanceID
                                       && (row.Role == 10 || row.Role == 11)
                                       select row).ToList();
                }

                return eventAssignment;
            }
        }

        //public static List<long> CheckAllEventCompliance(long eventID,long baranchID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<long> eventInstanceID = (from row in entities.Compliances
        //                                      where row.EventID == eventID 
        //                                      select row.ID).ToList();

        //        List<long> complianceInstances = (from row in entities.ComplianceInstances
        //                                          where row.CustomerBranchID == baranchID && row.IsDeleted==false
        //                                          select row.ComplianceId).ToList();

        //        return eventInstanceID.Except(complianceInstances).ToList();
        //    }

        //}

        public static List<long> CheckAllEventComplianceNotAssigned(int eventType, List<long> Eventlist, int baranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (eventType == 1)  //Secretrial
                {
                    List<long> AssignDaysCompliance = (from row in entities.EventCompAssignDays
                                                       join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                       join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                       join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                       join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                       where Eventlist.Contains(row.ParentEventID) && row5.ID == baranchID
                                                       && row1.ComplinceVisible == true
                                                       && row1.Status == null
                                                       select (long)row.ComplianceID).Distinct().ToList();

                    List<long> complianceInstances = (from row in entities.ComplianceInstances
                                                      join row1 in entities.Compliances on row.ComplianceId equals row1.ID
                                                      join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                      join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                      join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                      where row.CustomerBranchID == baranchID && row.IsDeleted == false
                                                      //&& row1.EventFlag == true
                                                      && row1.ComplinceVisible == true
                                                      && row5.ID == baranchID
                                                      && row1.Status == null
                                                      select row.ComplianceId).Distinct().ToList();
                    return AssignDaysCompliance.Except(complianceInstances).ToList();

                }
                else if (eventType == 2) //NonSecretrial
                {
                    List<long> AssignDaysCompliance = (from row in entities.EventCompAssignDays
                                                       join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                       join row6 in entities.Acts on row1.ActID equals row6.ID
                                                       join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                       join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                       join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                       where Eventlist.Contains(row.ParentEventID) && row5.ID == baranchID
                                                       && row1.ComplinceVisible == true
                                                       && row1.Status == null
                                                       && (row6.StateID == row5.StateID || row6.ComplianceTypeId == 2)
                                                       select (long)row.ComplianceID).Distinct().ToList();

                    List<long> complianceInstances = (from row in entities.ComplianceInstances
                                                      join row1 in entities.Compliances on row.ComplianceId equals row1.ID
                                                      join row6 in entities.Acts on row1.ActID equals row6.ID
                                                      join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                      join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                      join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                      where row.CustomerBranchID == baranchID && row.IsDeleted == false
                                                      //&& row1.EventFlag == true 
                                                      && (row6.StateID == row5.StateID || row6.ComplianceTypeId == 2)
                                                      && row5.ID == baranchID
                                                      && row1.ComplinceVisible == true
                                                       && row1.Status == null
                                                      && row5.ID == baranchID
                                                      select row.ComplianceId).Distinct().ToList();
                    return AssignDaysCompliance.Except(complianceInstances).ToList();
                }
                return null;
            }
        }

        public static List<long> CheckAllEventCompliance(int eventID, int baranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> AssignDaysCompliance = (from row in entities.EventCompAssignDays
                                                   where row.ParentEventID == eventID
                                                   join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                   join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                   join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                   join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                   where row5.ID == baranchID
                                                   && row1.ComplinceVisible == true
                                                   select (long)row.ComplianceID).Distinct().ToList();

                List<long> complianceInstances = (from row in entities.ComplianceInstances
                                                  join row1 in entities.Compliances on row.ComplianceId equals row1.ID
                                                  join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                  join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                  join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                  where row.CustomerBranchID == baranchID && row.IsDeleted == false
                                                  //&& row1.EventFlag == true
                                                  && row5.ID == baranchID
                                                  && row1.ComplinceVisible == true
                                                  select row.ComplianceId).Distinct().ToList();

                return AssignDaysCompliance.Except(complianceInstances).ToList();
            }

        }

        public static List<long> CheckAllEventComplianceNonSecretrial(int eventID, int baranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> AssignDaysCompliance = (from row in entities.EventCompAssignDays
                                                   where row.ParentEventID == eventID
                                                   join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                                   join row6 in entities.Acts on row1.ActID equals row6.ID
                                                   join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                   join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                   join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                   where row5.ID == baranchID
                                                   && row1.ComplinceVisible == true
                                                   && (row6.StateID == row5.StateID || row6.ComplianceTypeId == 2)
                                                   select (long)row.ComplianceID).Distinct().ToList();

                List<long> complianceInstances = (from row in entities.ComplianceInstances
                                                  join row1 in entities.Compliances on row.ComplianceId equals row1.ID
                                                  join row6 in entities.Acts on row1.ActID equals row6.ID
                                                  join row3 in entities.IndustryMappings on row1.ID equals row3.ComplianceId
                                                  join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                                  join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                                  where row.CustomerBranchID == baranchID && row.IsDeleted == false
                                                  //&& row1.EventFlag == true
                                                  && row5.ID == baranchID
                                                  && row1.ComplinceVisible == true
                                                  && (row6.StateID == row5.StateID || row6.ComplianceTypeId == 2)
                                                  select row.ComplianceId).Distinct().ToList();

                return AssignDaysCompliance.Except(complianceInstances).ToList();
            }

        }

        public static void EventScheduledOnUpdate(EventScheduleOn eventData, long eventAssignmentID, long eventID, long createdBy, List<long> OptionalCompliancesList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                EventScheduleOn eventToUpdate = (from row in entities.EventScheduleOns
                                                 where row.ID == eventData.ID
                                                 select row).FirstOrDefault();


                eventToUpdate.HeldOn = eventData.HeldOn;
                entities.SaveChanges();

                string yType = (from row in entities.Events
                                where row.ID == eventID && row.IsDeleted == false
                                select row.YearType).SingleOrDefault();

                CreateScheduleOn(eventToUpdate.EndDate.Value.AddDays(2), Convert.ToInt64(eventToUpdate.EventInstanceID), eventID, createdBy, Convert.ToDateTime(eventToUpdate.HeldOn), yType);
                List<EventReminder> existingReminder = GetExisingEventreminder(eventAssignmentID);
                UpdateStatusOfExisingEvent(existingReminder);
                CreateReminders(eventID, Convert.ToInt64(eventToUpdate.EventInstanceID), eventAssignmentID, createdBy);
                GenerateEventBasedComplianceScheduele(eventID, Convert.ToDateTime(eventData.HeldOn), eventToUpdate.Period, OptionalCompliancesList);
            }
        }

        public static void GenerateEventBasedComplianceScheduele(long eventID, DateTime eventHeldOnDate, string forMonth, List<long> OptionalCompliancesList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstances = ComplianceManagement.GetEventBasedCompliancesInstances(eventID);
                #region Comment By Rahul on 11 FEB 2016 For Testing
                //List<ComplianceInstance> OptionalComplianceInstances = new List<ComplianceInstance>();
                //if (OptionalCompliancesList != null)
                //{
                //    OptionalComplianceInstances = ComplianceManagement.GetEventBasedCompliancesInstances(OptionalCompliancesList);
                //}
                //var finalComplianceList = complianceInstances.Concat(OptionalComplianceInstances);
                #endregion

                List<ComplianceInstance> OptionalComplianceInstances = new List<ComplianceInstance>();
                if (OptionalCompliancesList != null)
                {
                    OptionalComplianceInstances = ComplianceManagement.GetEventBasedCompliancesInstances(OptionalCompliancesList);
                }

                var finalComplianceList = complianceInstances.Concat(OptionalComplianceInstances).ToList();
                var DistinctItems = finalComplianceList.GroupBy(x => x.ComplianceId).Select(y => y.First());

                //var finalComplianceList = complianceInstances;                
                finalComplianceList.ToList().ForEach(entry =>
                {

                    var compliance = ComplianceManagement.GetByID(entry.ComplianceId);
                    DateTime scheduledOnDate = eventHeldOnDate.AddDays(Convert.ToInt32(compliance.DueDate));

                    ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                    complianceScheduleon.ComplianceInstanceID = entry.ID;
                    complianceScheduleon.ScheduleOn = scheduledOnDate;
                    complianceScheduleon.ForMonth = forMonth;
                    complianceScheduleon.IsActive = true;
                    complianceScheduleon.IsUpcomingNotDeleted = true;
                    entities.ComplianceScheduleOns.Add(complianceScheduleon);
                    entities.SaveChanges();

                    var AssignedRole = ComplianceManagement.GetAssignedUsers((int)entry.ID);
                    var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                    if (performerRole != null)
                    {
                        var user = UserManagement.GetByID((int)performerRole.UserID);
                        ComplianceTransaction transaction = new ComplianceTransaction()
                        {
                            ComplianceInstanceId = entry.ID,
                            ComplianceScheduleOnID = complianceScheduleon.ID,
                            CreatedBy = performerRole.UserID,
                            CreatedByText = user.FirstName + " " + user.LastName,
                            StatusId = 1,
                            Remarks = "New compliance assigned."
                        };

                        ComplianceManagement.CreateTransaction(transaction);

                        foreach (var roles in AssignedRole)
                        {
                            if (roles.RoleID != 6)
                            {
                                ComplianceManagementComplianceScheduleon.CreateReminders(entry.ComplianceId, entry.ID, roles.ID, scheduledOnDate);
                            }
                        }
                    }

                });
            }
        }

        public static List<EventReminder> GetExisingEventreminder(long eventAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<EventReminder> eventExistingReminder = (from row in entities.EventReminders
                                                             where row.EventAssignmentID == eventAssignmentID && row.Status == 0
                                                             select row).ToList();
                eventExistingReminder.ForEach(entry =>
                {
                    entry.Status = 2;
                });
                return eventExistingReminder;
            }

        }

        public static void UpdateStatusOfExisingEvent(List<EventReminder> reminders)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                reminders.ForEach(entry =>
                {
                    //EventReminder reminder = new EventReminder()
                    //{
                    //    ID = entry.ID
                    //};

                    //entities.EventReminders.Attach(reminder);

                    EventReminder reminder = (from row in entities.EventReminders
                                              where row.ID == entry.ID
                                              select row).FirstOrDefault();

                    reminder.Status = entry.Status;

                });

                entities.SaveChanges();
            }
        }

        public static List<EventReminderView> GetEventReminderNotificationsForDay(DateTime date)
        {
            date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var eventReminders = (from row in entities.EventReminderViews
                                      where row.EventReminderStatus == 0 && row.RemindOn == date && row.RoleID == 10 && row.HeldOn == null && row.CustomerStatus == 1
                                      select row).OrderBy(entry => entry.RemindOn).ToList();
                return eventReminders;
            }

        }

        //added by sudarshan for escalation notification
        public static List<EventReminderView> GetEscalationNotificationsForDay(DateTime date)
        {
            date = date.Date;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var eventReminders = (from row in entities.EventReminderViews
                                      where row.ScheduledClosedDate <= date && row.RoleID == 10 && row.HeldOn == null && row.CustomerStatus == 1
                                      select row).GroupBy(entry => entry.EventScheduledOnId).Select(entry => entry.FirstOrDefault()).ToList();

                return eventReminders;
            }

        }

        public static bool CheckEscalationNotification(long EventInstanceID, long prentId, DateTime enddate, DateTime currentDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.EventEscalationNotifications
                            where row.EventInstanceID == EventInstanceID && row.UserID == prentId
                            && row.ClosedOn == enddate && row.SentOn == currentDate
                            select row).FirstOrDefault();

                if (data == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void AddEscalationDetail(List<EventEscalationNotification> objEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.EventEscalationNotifications.Add(entry);
                    entities.SaveChanges();

                });
            }

        }

        public static void ReassignEvents(int oldUserID, int newUserID, List<Tuple<int, int>> AssignedEventList, long updatedby)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                foreach (var cil in AssignedEventList)
                {
                    var assignmentData = (from row in entities.EventAssignments
                                          where row.ID == cil.Item1 && row.Role == cil.Item2 && row.UserID == oldUserID
                                          select row).FirstOrDefault();

                    if (assignmentData != null)
                    {
                        assignmentData.UserID = newUserID;
                        assignmentData.UpdatedBy = updatedby;
                        assignmentData.UpdatedOn = DateTime.UtcNow;
                    }
                }

                entities.SaveChanges();
            }
        }

        public static void DeleteEventAssignment(int UserID, List<Tuple<int, int>> AssignedEventList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var cil in AssignedEventList)
                {

                    var assignmentData = (from row in entities.EventAssignments
                                          where row.ID == cil.Item1 && row.Role == cil.Item2 && row.UserID == UserID
                                          select row).FirstOrDefault();

                    List<EventReminder> reminderData = (from row in entities.EventReminders
                                                        where row.EventAssignmentID == assignmentData.ID
                                                        select row).ToList();

                    reminderData.ForEach(entry =>
                           entities.EventReminders.Remove(entry)
                        );

                    entities.EventAssignments.Remove(assignmentData);
                    entities.SaveChanges();

                    var EventScheduleonList = (from row in entities.EventScheduleOns
                                               where row.IsDeleted == false && row.EventInstanceID == assignmentData.EventInstanceID
                                               select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                    if (EventScheduleonList.Count > 0)
                    {
                        EventScheduleonList.ForEach(EventScheduleOnssentry =>
                        {
                            // CreateLogForScheduleOn(Convert.ToInt32(EventScheduleOnssentry.ID),UserID);
                            DeleteEventScheduleOn(EventScheduleOnssentry.ID);


                        });
                    }
                    else
                    {

                    }

                    EventScheduleonList.ForEach(compliancescheduleentry =>
                    {
                        var complianceScheduleOns = (from row in entities.ComplianceScheduleOns
                                                     where row.EventScheduledOnID == compliancescheduleentry.ID &&
                                                     row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                     select row).ToList();


                        if (complianceScheduleOns.Count > 0)
                        {
                            complianceScheduleOns.ForEach(ComplianceScheduleOnssentry =>
                            {
                                CreateLogForScheduleOn(Convert.ToInt32(compliancescheduleentry.ID), UserID);
                                DeleteComplianceScheduleOn(ComplianceScheduleOnssentry.ID);
                            });

                        }

                    });

                }

            }
        }

        public static void CreateLogForScheduleOn(int eventsceduleid, int userid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    LogEventExcludetable logdata = new LogEventExcludetable()
                    {
                        EventscheduleonId = eventsceduleid,
                        CreatedBy = userid,
                        CreatedOn = DateTime.Now,
                    };

                    entities.LogEventExcludetables.Add(logdata);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static void DeleteComplianceScheduleOn(long scheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn complianceScheduleOn = (from row in entities.ComplianceScheduleOns
                                                             where row.ID == scheduleOnID// && row.IsActive==true &&
                                                             //row.IsUpcomingNotDeleted==true
                                                             select row).FirstOrDefault();

                complianceScheduleOn.IsActive = false;
                complianceScheduleOn.IsUpcomingNotDeleted = false;
                entities.SaveChanges();

            }

        }
        public static void DeleteEventScheduleOn(long EventScheduleOnsID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                EventScheduleOn eventScheduleOn = (from row in entities.EventScheduleOns
                                                   where row.ID == EventScheduleOnsID
                                                   select row).FirstOrDefault();
                eventScheduleOn.IsDeleted = true;
                entities.SaveChanges();

            }
        }
        //public static void DeleteEventAssignment(int UserID, List<Tuple<int, int>> AssignedEventList)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        foreach (var cil in AssignedEventList)
        //        {

        //            var assignmentData = (from row in entities.EventAssignments
        //                                  where row.ID == cil.Item1 && row.Role == cil.Item2 && row.UserID == UserID
        //                                  select row).FirstOrDefault();

        //            List<EventReminder> reminderData = (from row in entities.EventReminders
        //                                                where row.EventAssignmentID == assignmentData.ID
        //                                                select row).ToList();

        //            reminderData.ForEach(entry =>
        //                   entities.EventReminders.Remove(entry)
        //                );

        //            entities.EventAssignments.Remove(assignmentData);
        //        }
        //        entities.SaveChanges();
        //    }
        //}


        public static void DeleteEventAssignment(int eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedEventList = (from row in entities.EventAssignments
                                         join row1 in entities.EventInstances
                                         on row.EventInstanceID equals row1.ID
                                         where row1.ID == eventID
                                         select row).ToList();

                foreach (var cil in AssignedEventList)
                {
                    var assignmentData = (from row in entities.EventAssignments
                                          where row.ID == cil.ID
                                          select row).FirstOrDefault();

                    List<EventReminder> reminderData = (from row in entities.EventReminders
                                                        where row.EventAssignmentID == cil.ID
                                                        select row).ToList();

                    reminderData.ForEach(entry =>
                           entities.EventReminders.Remove(entry)
                        );

                    entities.EventAssignments.Remove(assignmentData);
                }

                entities.SaveChanges();
            }
        }

        #endregion
        public static bool CheckEvent(int eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Events
                            where row.ID == eventID
                            select row).FirstOrDefault();
                if (data.Visible == "Y")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public static bool CheckComplianceVisible(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Compliances
                            where row.ID == complianceID
                            select row).FirstOrDefault();
                if (data.ComplinceVisible == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool CheckShortnoticeCompliance(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ShorterNotices
                            where row.ComplianceID == complianceID
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool CheckShortnoticePresent(int ParentEventID, int IntermediateEventID, int SubEventID, int complianceID, int EventScheduledOnID, int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ShortNoticeDays
                            where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID && row.SubEventID == SubEventID
                            && row.ComplianceID == complianceID && row.EventScheduledOnID == EventScheduledOnID && row.ComplianceInstanceID == ComplianceInstanceID
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static void SaveShortNoticeDays(int ParentEventID, int IntermediateEventID, int subEventID, int ComplianceID, int EventScheduledOnID, int ComplianceInstanceID, int days)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ShortNoticeDay shortNoticeDay = new ShortNoticeDay();
                shortNoticeDay.ParentEventID = ParentEventID;
                shortNoticeDay.IntermediateEventID = IntermediateEventID;
                shortNoticeDay.SubEventID = subEventID;
                shortNoticeDay.ComplianceID = ComplianceID;
                shortNoticeDay.EventScheduledOnID = EventScheduledOnID;
                shortNoticeDay.ComplianceInstanceID = ComplianceInstanceID;
                shortNoticeDay.ShortDays = days;
                entities.ShortNoticeDays.Add(shortNoticeDay);
                entities.SaveChanges();
            }
        }

        public static void UpdateShortNotice(int ParentEventID, int IntermediateEventID, int subEventID, int ComplinaceID, int EventSheduleOnID, int ComplianceInstanceID, int days)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ShortNoticeDay shortNoticeDay = (from row in entities.ShortNoticeDays
                                                 where row.ParentEventID == ParentEventID
                                                    && row.IntermediateEventID == IntermediateEventID
                                                    && row.SubEventID == subEventID
                                                    && row.ComplianceID == ComplinaceID
                                                    && row.EventScheduledOnID == EventSheduleOnID
                                                    && row.ComplianceInstanceID == ComplianceInstanceID
                                                 select row).FirstOrDefault();
                shortNoticeDay.ShortDays = days;
                entities.SaveChanges();
            }
        }

        public static void GenerateEventComplianceSchedueleForShortNotice(int ParentEventID, int IntermediateEventID, int subEventID, long EventScheduledOnID, DateTime eventHeldOnDate, long ComplianceID, int ComplianceInstanceID, int days)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int IncludeDays = 0;
                if (days == 0 || days == -0)
                {
                    IncludeDays = 0;
                }
                else
                {
                    if (days > 0)
                    {
                        IncludeDays = days - 1;
                    }
                    else
                    {
                        IncludeDays = days + 1;
                    }
                }
                DateTime scheduledOnDate = eventHeldOnDate.AddDays(Convert.ToInt32(IncludeDays));

                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                complianceScheduleon.ComplianceInstanceID = ComplianceInstanceID;
                complianceScheduleon.ParentEventD = ParentEventID;
                complianceScheduleon.IntermediateEventID = IntermediateEventID;
                complianceScheduleon.SubEventID = subEventID;
                complianceScheduleon.ScheduleOn = scheduledOnDate;
                complianceScheduleon.IsActive = true;
                complianceScheduleon.IsUpcomingNotDeleted = true;
                complianceScheduleon.EventScheduledOnID = EventScheduledOnID;
                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                entities.SaveChanges();

                var AssignedRole = ComplianceManagement.GetAssignedUsers(ComplianceInstanceID);
                var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                if (performerRole != null)
                {
                    var user = UserManagement.GetByID((int)performerRole.UserID);
                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceInstanceId = ComplianceInstanceID,
                        ComplianceScheduleOnID = complianceScheduleon.ID,
                        CreatedBy = performerRole.UserID,
                        CreatedByText = user.FirstName + " " + user.LastName,
                        StatusId = 1,
                        Remarks = "New compliance assigned."
                    };

                    ComplianceManagement.CreateTransaction(transaction);

                    foreach (var roles in AssignedRole)
                    {
                        if (roles.RoleID != 6)
                        {
                            ComplianceManagement.CreateEventBasedComplinceReminders(ComplianceID, ComplianceInstanceID, roles.ID, eventHeldOnDate, scheduledOnDate, EventScheduledOnID);
                        }
                    }
                }
            }
        }


        public static void UpdateComplianceSheduleOnDates(int ParentEventID, int IntermediateEventID, int subEventID, int EventSheduleOnID, int ComplinaceID, int ComplianceInstanceID, DateTime UpdatedDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn ScheduleOns = (from row in entities.ComplianceScheduleOns
                                                    join row1 in entities.ComplianceInstances
                                                    on row.ComplianceInstanceID equals row1.ID
                                                    where row1.ComplianceId == ComplinaceID
                                                    && row.EventScheduledOnID == EventSheduleOnID
                                                    && row.ParentEventD == ParentEventID
                                                    && row.IntermediateEventID == IntermediateEventID
                                                    && row.SubEventID == subEventID
                                                    && row1.ID == ComplianceInstanceID
                                                    select row).FirstOrDefault();
                ScheduleOns.ScheduleOn = UpdatedDate;
                entities.SaveChanges();
            }
        }

        //public static void UpdateReminderSheduleOnDates(int EventSheduleOnID, int ComplinaceID, DateTime UpdatedDate, int ComplianceInstanceID, DateTime Date)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        List<ComplianceReminder> complianceReminder = (from row in entities.ComplianceReminders
        //                                                       join row1 in entities.ComplianceAssignments
        //                                                       on row.ComplianceAssignmentID equals row1.ID
        //                                                       join row2 in entities.ComplianceScheduleOns
        //                                                       on row1.ComplianceInstanceID equals row2.ComplianceInstanceID
        //                                                       join row3 in entities.ComplianceInstances
        //                                                       on row2.ComplianceInstanceID equals row3.ID
        //                                                       where row3.ComplianceId == ComplinaceID
        //                                                       && row2.EventScheduledOnID == EventSheduleOnID
        //                                                       && row3.ID == ComplianceInstanceID
        //                                                       select row).ToList();

        //        foreach (var cil in complianceReminder)
        //        {
        //            var compReminder = (from row in entities.ComplianceReminders
        //                                where row.ID == cil.ID && row.ComplianceAssignmentID == cil.ComplianceAssignmentID
        //                                select row).FirstOrDefault();

        //            if (compReminder != null)
        //            {
        //                compReminder.ComplianceDueDate = UpdatedDate;
        //                compReminder.RemindOn = Date;
        //            }
        //        }

        //        entities.SaveChanges();


        //        //complianceReminder.ForEach(x=> x.e);

        //        //complianceReminder.ComplianceDueDate = UpdatedDate;
        //        //complianceReminder.RemindOn =Date ;
        //        //entities.SaveChanges();
        //    }
        //}

        public static void UpdateReminderSheduleOnDates(int EventSheduleOnID, int ComplinaceID, DateTime UpdatedDate, int ComplianceInstanceID, DateTime Date)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                #region complianceReminder
                //List<ComplianceReminder> complianceReminder = (from row in entities.ComplianceReminders
                //                                               join row1 in entities.ComplianceAssignments
                //                                               on row.ComplianceAssignmentID equals row1.ID
                //                                               join row2 in entities.ComplianceScheduleOns
                //                                               on row1.ComplianceInstanceID equals row2.ComplianceInstanceID 
                //                                               join row3 in entities.ComplianceInstances
                //                                               on row2.ComplianceInstanceID equals row3.ID
                //                                               where row3.ComplianceId == ComplinaceID
                //                                               && row2.EventScheduledOnID == EventSheduleOnID
                //                                               && row3.ID == ComplianceInstanceID
                //                                               select row).ToList();

                //List<ComplianceReminder> complianceReminder = (from row in entities.ComplianceReminders
                //                                             join row1 in entities.ComplianceAssignments
                //                                             on row.ComplianceAssignmentID equals row1.ID
                //                                             join row2 in entities.ComplianceScheduleOns on
                //                                             new { InstanceID = row1.ComplianceInstanceID, scheduleOn = row1.ComplianceDueDate } equals
                //                                             new { InstanceID = row2.ComplianceInstanceID, scheduleOn = row2.ScheduleOn }
                //                                             //on row1.ComplianceInstanceID equals row2.ComplianceInstanceID 
                //                                             join row3 in entities.ComplianceInstances
                //                                             on row2.ComplianceInstanceID equals row3.ID
                //                                             where row3.ComplianceId == ComplinaceID
                //                                             && row2.EventScheduledOnID == EventSheduleOnID
                //                                             && row3.ID == ComplianceInstanceID
                //                                             select row).ToList();


                //new { InstanceID = row.ID, scheduleOn = row3.ComplianceDueDate } equals
                //                                              new { InstanceID = row1.ComplianceInstanceID, scheduleOn = row2.ScheduleOn }


                //foreach (var cil in complianceReminder)
                //{
                //    var compReminder = (from row in entities.ComplianceReminders
                //                        where row.ID == cil.ID && row.ComplianceAssignmentID == cil.ComplianceAssignmentID
                //                        select row).FirstOrDefault();
                //    if (compReminder != null)
                //    {
                //        compReminder.ComplianceDueDate = UpdatedDate;
                //        compReminder.RemindOn = Date;
                //    }
                //}
                //entities.SaveChanges();


                #endregion

                List<ComplianceReminder> complianceReminder = (from row in entities.ComplianceInstances
                                                               join row1 in entities.ComplianceAssignments
                                                               on row.ID equals row1.ComplianceInstanceID
                                                               join row2 in entities.ComplianceScheduleOns on
                                                               row1.ComplianceInstanceID equals row2.ComplianceInstanceID
                                                               join row3 in entities.ComplianceReminders on
                                                               row1.ID equals row3.ComplianceAssignmentID
                                                               where row.ComplianceId == ComplinaceID
                                                               && row2.EventScheduledOnID == EventSheduleOnID
                                                               && row.ID == ComplianceInstanceID
                                                               && row2.ScheduleOn == row3.ComplianceDueDate
                                                               && row3.EventScheduledOnID == EventSheduleOnID
                                                               select row3).ToList();

                List<int> ComplianceAssignmentList = new List<int>();
                foreach (var cil in complianceReminder)
                {
                    ComplianceAssignmentList.Add(cil.ComplianceAssignmentID);
                }
                foreach (var cil in complianceReminder)
                {
                    entities.ComplianceReminders.Remove(cil);
                    entities.SaveChanges();
                }

                foreach (var AssignID in ComplianceAssignmentList.Distinct())
                {
                    ComplianceReminder reminder = new ComplianceReminder()
                    {
                        ComplianceAssignmentID = AssignID,
                        ReminderTemplateID = 1,
                        ComplianceDueDate = UpdatedDate,
                        EventScheduledOnID = EventSheduleOnID,
                        RemindOn = UpdatedDate.AddDays(-1 * 2),
                    };
                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                    entities.ComplianceReminders.Add(reminder);


                    ComplianceReminder reminder1 = new ComplianceReminder()
                    {
                        ComplianceAssignmentID = AssignID,
                        ReminderTemplateID = 1,
                        ComplianceDueDate = UpdatedDate,
                        EventScheduledOnID = EventSheduleOnID,
                        RemindOn = UpdatedDate.AddDays(-1 * 7),
                    };
                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                    entities.ComplianceReminders.Add(reminder1);


                    ComplianceReminder reminder2 = new ComplianceReminder()
                    {
                        ComplianceAssignmentID = AssignID,
                        ReminderTemplateID = 1,
                        ComplianceDueDate = UpdatedDate,
                        EventScheduledOnID = EventSheduleOnID,
                        RemindOn = UpdatedDate.AddDays(-1 * 15),
                    };
                    reminder2.Status = (byte)(reminder2.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                    entities.ComplianceReminders.Add(reminder2);

                    entities.SaveChanges();
                }
            }
        }

        public static bool CheckEventComplianceAssigned(int ParentEventID, int IntermediateEventID, int SubEventID, int Type, int complianceID, int EventsheduleOnID, int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SP_CheckComplianceSchedulePresent(ParentEventID, IntermediateEventID, SubEventID, EventsheduleOnID, ComplianceInstanceID, complianceID, Type)
                            select row).FirstOrDefault();

                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static List<Event_Assigned_View> GetAllAssignedEventsForCompanyAdmin(int branchID = -1, int userID = -1, int customerId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Event_Assigned_View> complianceInstancesQuery = new List<Event_Assigned_View>();

                complianceInstancesQuery = (from row in entities.Event_Assigned_View
                                                //where row.RoleID == eventRoleID
                                            select row).ToList();
                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId).ToList();
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID).ToList();
                }

                return complianceInstancesQuery.ToList(); //complianceInstancesQuery.OrderByDescending(entry => entry.EventAssignmentID).ToList();
            }
        }
        #region Event Canned report

        public static List<EventReportClass> GetEventComplianceNotAssigned(int CustomerID, int branch)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var eventCompliances = (from row in entities.EventComplianceNotAssignedViewNews
                                        where row.CustomerID == CustomerID
                                        select new EventReportClass()
                                        {
                                            ID = row.ID,
                                            Name = row.Name,
                                            Description = row.Name,
                                            CustomerBranchID = row.CustomerBranchID,
                                            CustomerBranchName = row.CustomerBranchName,
                                            CustomerID = row.CustomerID,
                                            ComplianceShortDescription = row.ComplianceShortDescription,
                                            ComplianceID = row.ComplianceID
                                        }).ToList();
                if (branch != -1)
                {
                    eventCompliances = eventCompliances.Where(entry => entry.CustomerBranchID == branch).ToList();
                }


                return eventCompliances;
            }

        }

        public static List<EventReportClass> GetEventNotAssigned(int CustomerID, int branch)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var eventList = (from row in entities.EventNotAssignedViews
                                 where row.CustomerID == CustomerID
                                 select new EventReportClass()
                                 {
                                     ID = row.ID,
                                     Name = row.Name,
                                     Description = row.Name,
                                     CustomerBranchID = row.CustomerBranchID,
                                     CustomerBranchName = row.CustomerBranchName,
                                     CustomerID = row.CustomerID,
                                 }).ToList();

                if (branch != -1)
                {
                    eventList = eventList.Where(entry => entry.CustomerBranchID == branch).ToList();
                }

                return eventList;
            }

        }

        public static List<EventReportClass> GetEventComplianceNotAssignedAllRowsView(int CustomerID, int branch)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventList = (from row in entities.EventComplianceNotAssignedViewNews
                                 where row.CustomerID == CustomerID
                                 select new EventReportClass()
                                 {
                                     ID = row.ID,
                                     Name = row.Name,
                                     Description = row.Name,
                                     CustomerBranchID = row.CustomerBranchID,
                                     CustomerBranchName = row.CustomerBranchName,
                                     CustomerID = row.CustomerID,
                                     ComplianceShortDescription = row.ComplianceShortDescription,
                                     ComplianceID = row.ComplianceID
                                 }).ToList();

                if (branch != -1)
                {
                    eventList = eventList.Where(entry => entry.CustomerBranchID == branch).ToList();
                }

                return eventList;
            }
        }

        public static List<Sp_GetStatutoryAssignment_Result> GetEventComplianceAssignedView(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var result = entities.Sp_GetStatutoryAssignment(CustomerID, "E").ToList();
                if (branch != -1)
                {
                    result = result.Where(entry => entry.BranchID == branch).ToList();
                }
                return result;
            }
        }

        public static List<ComplianceCannedReport> GetComplianceNotAssigned(int CustomerID, int branch)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceList = (from row in entities.ComplianceNotAssignedViews
                                      where row.CustomerID == CustomerID && row.ComplianceType != 1
                                      select new ComplianceCannedReport()
                                      {
                                          ComplianceInstanceID = row.ID,
                                          CustomerBranchID = row.CustomerBranchID,
                                          ComplianceTypeName = row.ComplianceTypeName,
                                          State = row.State,
                                          CustomerID = row.CustomerID,
                                          ActName = row.ActName,
                                          ShortDescription = row.ShortDescription,
                                          CustomerBranchName = row.CustomerBranchName,

                                      }).ToList();

                if (branch != -1)
                {
                    ComplianceList = ComplianceList.Where(entry => entry.CustomerBranchID == branch).ToList();
                }

                return ComplianceList;
            }

        }
        //Added by rahul on 8 FEB 2016 For Check List

        public static List<ComplianceCannedReport> GetCheckListNotAssigned(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceList = (from row in entities.ComplianceNotAssignedViews
                                      where row.CustomerID == CustomerID && row.ComplianceType == 1
                                      select new ComplianceCannedReport()
                                      {

                                          ComplianceInstanceID = row.ID,
                                          CustomerBranchID = row.CustomerBranchID,
                                          ComplianceTypeName = row.ComplianceTypeName,
                                          State = row.State,
                                          CustomerID = row.CustomerID,
                                          ActName = row.ActName,
                                          ShortDescription = row.ShortDescription,
                                          CustomerBranchName = row.CustomerBranchName,

                                      }).ToList();

                if (branch != -1)
                {
                    ComplianceList = ComplianceList.Where(entry => entry.CustomerBranchID == branch).ToList();
                }

                return ComplianceList;
            }

        }
        //added By Manisha
        public static List<InternalComplianceCannedReport> GetInternalComplianceNotAssigned(int CustomerID, int branch)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var IComplianceList = (from row in entities.InternalComplianceNotAssignedViews
                                       where row.CustomerID == CustomerID
                                       select new InternalComplianceCannedReport()
                                       {
                                           InternalComplianceInstanceID = row.ID,
                                           CustomerBranchID = row.CustomerBranchID,
                                           //ComplianceTypeName = row.ComplianceTypeName, Comment by Rahul 0n 28 DEC 2015
                                           InternalComplianceTypeName = row.InternalComplianceTypeName,  //uncomment on 12-Apr-2016       
                                           CustomerID = row.CustomerID,

                                           ShortDescription = row.IShortDescription,
                                           CustomerBranchName = row.CustomerBranchName,

                                       }).ToList();

                if (branch != -1)
                {
                    IComplianceList = IComplianceList.Where(entry => entry.CustomerBranchID == branch).ToList();
                }

                return IComplianceList;
            }

        }

        public static List<Sp_GetStatutoryAssignment_Result> GetComplianceAssigned(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var result = entities.Sp_GetStatutoryAssignment(CustomerID, "S").ToList();

                if (branch != -1)
                {
                    result = result.Where(entry => entry.BranchID == branch).ToList();
                }
                return result;
            }
        }

        public static List<Sp_GetStatutoryAssignment_Result> GetChecklistAssigned(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var result = entities.Sp_GetStatutoryAssignment(CustomerID, "C").ToList();

                if (branch != -1)
                {
                    result = result.Where(entry => entry.BranchID == branch).ToList();
                }
                return result;
            }
        }

        public static long GetbyID_USERID(long ICInstanceID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long getuserid = 0;
                if (flag == "P")
                {

                    getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                 where row.InternalComplianceInstanceID == ICInstanceID && row.RoleID == 3
                                 select row.UserID).FirstOrDefault();

                    // getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 3).Select(x => x.UserID);
                }
                else if (flag == "R")
                {
                    getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                 where row.InternalComplianceInstanceID == ICInstanceID && row.RoleID == 4
                                 select row.UserID).FirstOrDefault();
                    //getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 4).Select(x => x.UserID);
                }
                else if (flag == "A")
                {
                    getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                 where row.InternalComplianceInstanceID == ICInstanceID && row.RoleID == 6
                                 select row.UserID).FirstOrDefault();
                    //getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 6).Select(x => x.UserID);
                }
                return getuserid;
            }
        }
        public static string GetbyName_USERNAME(long ICInstanceID, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string getuserid = "";
                if (flag == "P")
                {

                    getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                 where row.InternalComplianceInstanceID == ICInstanceID && row.RoleID == 3
                                 select row.User).FirstOrDefault();

                    // getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 3).Select(x => x.UserID);
                }
                else if (flag == "R")
                {
                    getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                 where row.InternalComplianceInstanceID == ICInstanceID && row.RoleID == 4
                                 select row.User).FirstOrDefault();
                    //getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 4).Select(x => x.UserID);
                }
                else if (flag == "A")
                {
                    getuserid = (from row in entities.InternalComplianceEscalationNotificationViews
                                 where row.InternalComplianceInstanceID == ICInstanceID && row.RoleID == 6
                                 select row.User).FirstOrDefault();
                    //getuserid = entities.InternalComplianceEscalationNotificationViews.Where(p => p.InternalScheduledOnID == IScheduledOnID && p.RoleID == 6).Select(x => x.UserID);
                }
                return getuserid;
            }
        }
        public static List<Sp_GetInternalAssignment_Result> GetInternalComplianceAssigned(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var result = entities.Sp_GetInternalAssignment(CustomerID, "S").ToList();

                if (branch != -1)
                {
                    result = result.Where(entry => entry.BranchID == branch).ToList();
                }
                return result;
            }

        }

        public static List<Sp_GetInternalAssignment_Result> GetInternalChecklistComplianceAssigned(int CustomerID, int branch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;

                var result = entities.Sp_GetInternalAssignment(CustomerID, "C").ToList();

                if (branch != -1)
                {
                    result = result.Where(entry => entry.BranchID == branch).ToList();
                }
                return result;
            }

        }

        #endregion

        #region Log Messages

        public static List<LogMessage> GetLogMessages(DateTime startDate, DateTime endDate)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var logMessagesList = (from row in entities.LogMessages
                                       where row.CreatedOn >= startDate && row.CreatedOn <= endDate
                                       select row).ToList();

                return logMessagesList.OrderByDescending(entry => entry.ID).ToList();
            }

        }

        #endregion

        public static List<SP_GetEventList_Result> GetCompanyTypeWiseEvents(string filter, List<int> ComType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetEventList_Result> eventList = new List<SP_GetEventList_Result>();

                eventList = (from row in entities.SP_GetEventList()
                             where ComType.Contains((int)row.Type)
                             select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter);
                        eventList = eventList.Where(entry => entry.ID == a).ToList();
                    }
                    else
                    {
                        eventList = eventList.Where(entry => entry.Name.ToUpper().Contains(filter.ToUpper())).ToList();
                    }

                }

                return eventList;
            }
        }


        public static List<SubEventView> GetAll(int eventID, long parentID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventCatagory = (from row in entities.SubEventViews
                                     where row.EventID == eventID
                                     select row);

                if (parentID != -1)
                {
                    eventCatagory = eventCatagory.Where(entry => entry.ParentID == parentID);
                }
                else
                {
                    eventCatagory = eventCatagory.Where(entry => entry.ParentID == null);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    eventCatagory = eventCatagory.Where(entry => entry.Name.Contains(filter));
                }

                return eventCatagory.ToList();
            }
        }

        public static object GetHierarchy(int eventID, long parentCustomerBranchID)
        {
            List<NameValue> hierarchy = new List<NameValue>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (parentCustomerBranchID != -1)
                {
                    long? parentID = parentCustomerBranchID;
                    while (parentID.HasValue)
                    {
                        var eventcatagory = (from row in entities.SubEvents
                                             where row.ID == parentID.Value
                                             select new { row.ID, row.Name, row.ParentID }).SingleOrDefault();

                        hierarchy.Add(new NameValue() { ID = (int)eventcatagory.ID, Name = eventcatagory.Name });
                        parentID = eventcatagory.ParentID;
                    }
                }

                var EventName = (from row in entities.Events
                                 where row.ID == eventID
                                 select row.Name).SingleOrDefault();

                hierarchy.Add(new NameValue() { ID = eventID, Name = EventName });
            }

            hierarchy.Reverse();

            return hierarchy;
        }

        public static void Update(SubEvent subEvent)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //SubEvent subEventToUpdate = new SubEvent() { ID = subEvent.ID };

                SubEvent subEventToUpdate = (from row in entities.SubEvents
                                             where row.ID == subEvent.ID
                                             select row).FirstOrDefault();

                entities.SubEvents.Attach(subEventToUpdate);

                subEventToUpdate.Name = subEvent.Name;
                subEventToUpdate.EventID = subEvent.EventID;
                subEventToUpdate.ParentID = subEvent.ParentID;
                subEventToUpdate.IsDeleted = subEventToUpdate.IsDeleted;
                subEventToUpdate.CreatedBy = subEvent.CreatedBy;
                subEventToUpdate.CreatedOn = subEventToUpdate.CreatedOn;
                subEventToUpdate.UpdatedBy = subEvent.UpdatedBy;
                subEventToUpdate.UpdatedOn = DateTime.UtcNow;
                entities.SaveChanges();
            }
        }

        public static void Create(SubEvent subEvent)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                subEvent.IsDeleted = false;
                subEvent.CreatedOn = DateTime.UtcNow;
                subEvent.UpdatedOn = DateTime.UtcNow;
                entities.SubEvents.Add(subEvent);
                entities.SaveChanges();
            }
        }

        public static bool Exists(SubEvent subEvent, int eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SubEvents
                             where row.IsDeleted == false
                             && row.Name.Equals(subEvent.Name) && row.EventID == eventID
                             select row);

                if (subEvent.ID > 0)
                {
                    query = query.Where(entry => entry.ID != subEvent.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static SubEvent GetSubEventByID(long subeventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.SubEvents
                            where row.ID == subeventID
                            select row).FirstOrDefault();
                return data;
            }
        }

        public static void SubEventDelete(int eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SubEvent> subEventList = new List<SubEvent>();
                SubEvent eventSubObject = new SubEvent() { ID = eventID };
                entities.SubEvents.Attach(eventSubObject);
                eventSubObject.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        #region Compliance Functions
        public static List<NameValueHierarchy> GetAllHierarchy(int eventID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Events
                             where row.IsDeleted == false
                             select row);
                if (eventID != -1)
                {
                    query = query.Where(entry => entry.ID == eventID);
                }
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<SubEvent> query = (from row in entities.SubEvents
                                          where row.IsDeleted == false
                                          select row);
            if (isClient)
            {
                query = query.Where(entry => entry.EventID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }
            var subEntities2 = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
            foreach (var item2 in subEntities2)
            {
                nvp.Children.Add(item2);
                LoadSubEntities(item2, false, entities);
            }
        }

        #endregion

        public static List<NameValueHierarchy> GetAllHierarchyRahul(int eventID = -1)
        {

            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Events
                             where row.IsDeleted == false
                             select row);
                if (eventID != -1)
                {
                    query = query.Where(entry => entry.ID == eventID);
                }
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    LoadSubEntitiesComplianceWithEVENTRahul(item, false, entities);
                    LoadSubEntitiesRahul(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntitiesComplianceWithEVENTRahul(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            //IQueryable<Compliance> query = (from row in entities.ComplianceInstances
            //                                join row1 in entities.Compliances
            //                                on row.ComplianceId equals row1.ID
            //                                where row.IsDeleted == false
            //                                select row1); //Comment by Rahul on 16 FEB 2016

            IQueryable<Compliance> query = (from row in entities.Compliances
                                            where row.IsDeleted == false
                                            select row);


            query = query.Where(entry => entry.EventID == nvp.ID && entry.SubEventID == null);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.ShortDescription }).OrderBy(entry => entry.Name).ToList();
            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
            }
        }

        public static void LoadSubEntitiesComplianceWithSubeventRahul(NameValueHierarchy nvp, bool isClient, long EventID, ComplianceDBEntities entities)
        {
            //IQueryable<Compliance> query = (from row in entities.ComplianceInstances
            //                                join row1 in entities.Compliances
            //                                on row.ComplianceId equals row1.ID
            //                                where row.IsDeleted == false
            //                                select row1);  //Comment by Rahul on 16 FEB 2016

            IQueryable<Compliance> query = (from row in entities.Compliances
                                            where row.IsDeleted == false
                                            select row);

            query = query.Where(entry => entry.EventID == EventID && entry.SubEventID == nvp.ID);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.ShortDescription }).OrderBy(entry => entry.Name).ToList();
            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntitiesComplianceWithSubeventRahul(item, false, nvp.ID, entities);
            }
        }
        public static void LoadSubEntitiesRahul(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<SubEvent> query = (from row in entities.SubEvents
                                          where row.IsDeleted == false
                                          select row);
            if (isClient)
            {
                query = query.Where(entry => entry.EventID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities2 = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
            foreach (var item2 in subEntities2)
            {
                nvp.Children.Add(item2);
                LoadSubEntitiesRahul(item2, false, entities);
                LoadSubEntitiesComplianceWithSubeventRahul(item2, false, nvp.ID, entities);
            }
        }

        public static NameValue GetTopParent(long subEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                SubEvent query = new SubEvent();
                query = (from row in entities.SubEvents
                         where row.IsDeleted == false && row.ID == subEventID
                         select row).FirstOrDefault();

                if (query.ParentID != null)
                {
                    subEventID = Convert.ToInt64(query.ParentID);
                }

                while (query.ParentID != null)
                {

                    query = (from row in entities.SubEvents
                             where row.IsDeleted == false && row.ID == subEventID
                             select row).FirstOrDefault();

                    if (query.ParentID != null)
                    {
                        subEventID = Convert.ToInt64(query.ParentID);
                    }

                }

                return new NameValue() { ID = (int)query.ID, Name = query.Name };
            }
        }

        public static List<NameValueHierarchy> GetSubEventAllHierarchy(int eventID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SubEvents
                             where row.IsDeleted == false
                             select row);

                if (eventID != -1)
                {
                    query = query.Where(entry => entry.ID == eventID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }


        public static List<long> GetCompliaceID(long subEventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<EventChieldParentView> query = (from row in entities.EventChieldParentViews
                                                     where row.ParentID == subEventID
                                                     select row).ToList();

                List<long?> allParent = (from row in entities.SubEvents
                                         where row.ParentID != null
                                         select row.ParentID).ToList();

                List<long> leafNode = query.Where(entry => !allParent.Contains(entry.ID)).Select(entry => entry.ID).ToList();

                List<Compliance> allCompliances = (from row in entities.Compliances
                                                   where leafNode.Contains((long)row.SubEventID)
                                                   select row).ToList();

                return allCompliances.Select(entry => entry.ID).ToList();
            }
        }


        public static int GETEVENTCOMPLIANCEID(long ComplianceID, string ComplianceName)
        {
            try
            {
                using (var context = new ComplianceDBEntities())
                {
                    var actID = (from row in context.Compliances
                                 where row.ID == ComplianceID && row.ShortDescription.ToUpper().Trim().Equals(ComplianceName.ToUpper().Trim())
                                 select row.EventComplianceType).FirstOrDefault();

                    if (actID != null)
                    {
                        return (Int32)actID;
                    }
                    else
                    {
                        return 2;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static List<EventView> GetEvent(int CompanyType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> eventIDs = (from row in entities.Events
                                       where row.Type == CompanyType
                                       select (long)row.ID).ToList();

                List<EventView> eventList = new List<EventView>();

                eventList = (from row in entities.EventViews
                             where eventIDs.Contains(row.ID)
                             select row).OrderBy(entry => entry.Name).ToList();
                return eventList;
            }
        }

        public static List<SP_GetEventBasedCompliance_Result> GetAllCompliances(List<SP_GetEventBasedCompliance_Result> compliance, string filter, List<long> actIdList = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> actIDs = new List<long>();
                actIDs = actIdList;
                if (actIDs.Count > 0)
                {
                    compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {

                    compliance = compliance.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())
                    || entry.FileTag.ToUpper().Contains(filter.ToUpper())
                    ).OrderBy(entry => entry.ShortDescription).Distinct().ToList();

                    compliance = (from obj in compliance
                                  select obj).GroupBy(n => new { n.ID, n.ShortDescription })
                                               .Select(g => g.FirstOrDefault())
                                               .ToList();

                }
                return compliance;
            }
        }
        public static List<SP_GetEventBasedCompliance_Result> GetAllCompliances(string filter, List<long> actIdList = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> actIDs = new List<long>();
                List<SP_GetEventBasedCompliance_Result> compliance = new List<SP_GetEventBasedCompliance_Result>();

                compliance = (from row in entities.SP_GetEventBasedCompliance()
                              select row).ToList();

                actIDs = actIdList;
                if (actIDs.Count > 0)
                {
                    //compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription.Distinct()).Distinct().ToList();
                    compliance = compliance.Where(entry => actIDs.Contains(entry.ActID)).OrderBy(entry => entry.ShortDescription).Distinct().ToList();
                }


                if (!string.IsNullOrEmpty(filter))
                {

                    compliance = compliance.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.FileTag.ToUpper().Contains(filter.ToUpper())).OrderBy(entry => entry.ShortDescription).Distinct().ToList();

                    compliance = (from obj in compliance
                                  select obj).GroupBy(n => new { n.ID, n.ShortDescription })
                                               .Select(g => g.FirstOrDefault())
                                               .ToList();

                }
                return compliance;
            }
        }



        public static List<SP_GetEventList_Result> GetAllEventsNew(string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetEventList_Result> eventList = new List<SP_GetEventList_Result>();

                eventList = (from row in entities.SP_GetEventList()
                             select row).ToList();

                //eventList = (from row in entities.Events
                //             where row.IsDeleted == false
                //             select row).ToList();



                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter);
                        eventList = eventList.Where(entry => entry.ID == a).ToList();
                    }
                    else
                    {
                        eventList = eventList.Where(entry => entry.Name.ToUpper().Contains(filter.ToUpper())).ToList();
                    }

                }
                return eventList;
            }
        }
        public static bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool ExistsAssignment(long EventInstanceID, long UserID, long roleID, int branchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.EventAssignments
                             join row1 in entities.EventInstances
                             on row.EventInstanceID equals row1.ID
                             where row.EventInstanceID == EventInstanceID &&
                              row.UserID == UserID && row.Role == roleID && row1.CustomerBranchID == branchID
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void AddEventAssignment(List<EventAssignment> Tempassignments, int branchID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    if (!(ExistsAssignment(entry.EventInstanceID, entry.UserID, entry.Role, branchID)))
                    {
                        entities.EventAssignments.Add(entry);
                        entities.SaveChanges();
                    }
                });
            }
        }

        public static bool CheckIntermediateSubEventDateAssigned(int parentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceTransactions
                            on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                            where row.ParentEventD == parentEventID && row.IntermediateEventID == IntermediateEventID
                            && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                            && row1.StatusId != 1
                            select row).FirstOrDefault();

                if (data == null)
                {
                    var date = (from row in entities.EventAssignDates
                                where row.ParentEventID == parentEventID && row.IntermediateEventID == IntermediateEventID
                                && row.SubEventID == SubEventID && row.EventScheduleOnID == EventScheduledOnId
                                select row.Date).FirstOrDefault();

                    if (Convert.ToDateTime(date) == Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckParentEventDateAssigned(int parentEventID, int EventScheduledOnId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.EventAssignDates
                            where row.ParentEventID == parentEventID && row.EventScheduleOnID == EventScheduledOnId
                              && row.IntermediateEventID == 0 && row.SubEventID == 0
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void CreateEventAssignDate(EventAssignDate eventAssignDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventAssignDates.Add(eventAssignDate);
                entities.SaveChanges();
            }
        }

        
        public static bool CheckEventAssigned(long eventId, int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.EventInstances
                            where row.EventID == eventId && row.CustomerBranchID == customerBranchID
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static void CreateEventReminders(long eventID, long EventInstanceID, long eventAssignmentID, long createdByID, DateTime remindOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                //byte? frequency = (from row in entities.Events
                //                   where row.ID == eventID
                //                   select row.Frequency).SingleOrDefault();

                //if (frequency.HasValue)
                //{
                //    int day = -1;
                //    if (frequency == 0)
                //        day = 7;
                //    else
                //        day = 15;


                //EventScheduleOn ScheduledOnList = (from row in entities.EventScheduleOns
                //                                   where row.EventInstanceID == EventInstanceID
                //                                   select row).OrderByDescending(entry => entry.ID).FirstOrDefault();

                //DateTime temp = Convert.ToDateTime(ScheduledOnList.ClosedDate);
                //for (int i = 0; i < 4; i++)
                //{
                //    temp = temp.Date.AddDays(-day);
                EventReminder reminder = new EventReminder()
                {
                    RemindOn = remindOn,
                    EventAssignmentID = eventAssignmentID,
                    CreatedBy = createdByID,
                    CreatedOn = DateTime.UtcNow,
                };

                reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                entities.EventReminders.Add(reminder);

                // }

                // }

                entities.SaveChanges();
            }
        }

        public static List<Event_Assigned_View> GetAllAssignedInstancesNew(int eventRoleID, string Role, int branchID = -1, int userID = -1, int customerId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Event_Assigned_View> complianceInstancesQuery = new List<Event_Assigned_View>();

                complianceInstancesQuery = (from row in entities.Event_Assigned_View
                                            where row.RoleID == eventRoleID
                                            select row).ToList();

                //if (Role == "CADMN")
                //{
                //    complianceInstancesQuery = (from row in entities.Event_Assigned_View
                //                                    select row).ToList();
                //}
                //else
                //{
                //    complianceInstancesQuery = (from row in entities.Event_Assigned_View
                //                                    where row.RoleID == eventRoleID
                //                                    select row).ToList();
                //}

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId).ToList();
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID).ToList();
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID).ToList();
                }

                return complianceInstancesQuery.ToList(); //complianceInstancesQuery.OrderByDescending(entry => entry.EventAssignmentID).ToList();
            }
        }

        public static List<Event_Assigned_View> GetAllAssignedInstancesCount(int eventRoleID, int branchID = -1, int userID = -1, int customerId = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var complianceInstancesQuery = (from row in entities.Event_Assigned_View
                                                where row.RoleID == eventRoleID
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                return complianceInstancesQuery.ToList(); //complianceInstancesQuery.OrderByDescending(entry => entry.EventAssignmentID).ToList();
            }
        }

        public static void AddEventInstance(List<EventInstance> Tempassignments)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    entities.EventInstances.Add(entry);
                    entities.SaveChanges();

                });
            }
        }

        public static List<Compliance> GetAllNoAssignedComplinceList(List<long> ComplinceIDList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Compliance> complianceList = new List<Compliance>();
                complianceList = (from row in entities.Compliances
                                  where ComplinceIDList.Contains(row.ID)
                                  select row).ToList();

                return complianceList;
            }
        }

        public static List<NotAssignedComplianceClass> GetAllNotAssignedComplinceListForEvent(int eventType, List<long> ComplinceIDList, List<long> EventIDList, int CustomerBranchID, int EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (eventType == 1)  //Secretrial
                {
                    List<NotAssignedComplianceClass> complianceList = new List<NotAssignedComplianceClass>();
                    complianceList = (from row in entities.Compliances
                                      join row1 in entities.EventCompAssignDays on row.ID equals row1.ComplianceID
                                      join row2 in entities.Events on row1.ParentEventID equals row2.ID
                                      join row3 in entities.IndustryMappings on row.ID equals row3.ComplianceId
                                      join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                      join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                      where ComplinceIDList.Contains(row.ID)
                                      && EventIDList.Contains(row1.ParentEventID) && row5.ID == CustomerBranchID
                                      && row.ComplinceVisible == true //Added 20 FEB 2020
                                      && row.Status == null
                                      select new NotAssignedComplianceClass()
                                      {
                                          ID = row.ID,
                                          EventID = row2.ID,
                                          EvnetName = row2.Name,
                                          ComplianceName = row.ShortDescription,
                                          CustomerBranchName = row5.Name,
                                          CustomerBranchID = row5.ID,
                                      }).Distinct().ToList();

                    if (EventID != -1)
                    {
                        complianceList = complianceList.Where(entry => entry.EventID == EventID).ToList();
                    }

                    return complianceList;
                }
                else if (eventType == 2) //NonSecretrial
                {
                    List<NotAssignedComplianceClass> complianceList = new List<NotAssignedComplianceClass>();
                    complianceList = (from row in entities.Compliances
                                      join row6 in entities.Acts on row.ActID equals row6.ID
                                      join row1 in entities.EventCompAssignDays on row.ID equals row1.ComplianceID
                                      join row2 in entities.Events on row1.ParentEventID equals row2.ID
                                      join row3 in entities.IndustryMappings on row.ID equals row3.ComplianceId
                                      join row4 in entities.CustomerBranchIndustryMappings on row3.IndustryID equals row4.IndustryID
                                      join row5 in entities.CustomerBranches on row4.CustomerBranchID equals row5.ID
                                      where ComplinceIDList.Contains(row.ID)
                                      && EventIDList.Contains(row1.ParentEventID) && row5.ID == CustomerBranchID
                                      && (row6.StateID == row5.StateID || row6.ComplianceTypeId == 2)
                                      && row.ComplinceVisible == true  //Added 20 FEB 2020
                                      && row.Status == null
                                      select new NotAssignedComplianceClass()
                                      {
                                          ID = row.ID,
                                          EventID = row2.ID,
                                          EvnetName = row2.Name,
                                          ComplianceName = row.ShortDescription,
                                          CustomerBranchName = row5.Name,
                                          CustomerBranchID = row5.ID,
                                      }).Distinct().ToList();

                    if (EventID != -1)
                    {
                        complianceList = complianceList.Where(entry => entry.EventID == EventID).ToList();
                    }

                    return complianceList;
                }
                return null;
            }
        }

        public static List<Event> GetAllEventsWithAssignedDays(int Type, int CustomerBranchID, long EventClassificationID, long LocationClassifictionID, string Filter, List<long> actIdList = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> eventIDs = new List<long>();
                List<long> actIDs = new List<long>();

                if (EventClassificationID == 1) //Secretrial
                {
                    if (actIdList.Count != 0)
                    {
                        if (Type != 0)
                        {
                            actIDs = actIdList;
                            eventIDs = (from row in entities.EventCompAssignDays
                                        join row1 in entities.Compliances
                                        on row.ComplianceID equals row1.ID
                                        where actIDs.Contains(row1.ActID) && row.Type == Type
                                        select (long)row.ParentEventID).Distinct().ToList();
                        }
                        else
                        {
                            actIDs = actIdList;
                            eventIDs = (from row in entities.EventCompAssignDays
                                        join row1 in entities.Compliances
                                        on row.ComplianceID equals row1.ID
                                        where actIDs.Contains(row1.ActID)
                                        select (long)row.ParentEventID).Distinct().ToList();
                        }
                    }
                    else
                    {
                        eventIDs = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances
                                    on row.ComplianceID equals row1.ID
                                    select (long)row.ParentEventID).Distinct().ToList();
                    }
                }
                else //Non Secretrial
                {
                    if (actIdList.Count != 0)
                    {
                        actIDs = actIdList;
                        eventIDs = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances
                                    on row.ComplianceID equals row1.ID
                                    where actIDs.Contains(row1.ActID) && row.Type == 4
                                    select (long)row.ParentEventID).Distinct().ToList();
                    }
                    else
                    {
                        eventIDs = (from row in entities.EventCompAssignDays
                                    join row1 in entities.Compliances
                                    on row.ComplianceID equals row1.ID
                                    where row.Type == 4
                                    select (long)row.ParentEventID).Distinct().ToList();
                    }
                }
                List<Event> eventList = new List<Event>();
                eventList = (from row in entities.Events
                             where eventIDs.Contains(row.ID) && row.IsDeleted == false
                             && row.Visible == "Y"
                             select row).ToList();

                if (EventClassificationID != -1)
                {
                    eventList = eventList.Where(entry => entry.EventClassificationID == EventClassificationID).ToList();
                }

                if (LocationClassifictionID != -1)
                {
                    var LocationWiseEventList = (from row1 in entities.EventTypeDataMappings
                                                 join row2 in entities.EventTypeDatas
                                                 on row1.EventTypeID equals row2.ID
                                                 where row1.EventTypeID == LocationClassifictionID && row1.IsActive == true
                                                 && row2.IsActive == true
                                                 select row1.EventId).ToList();

                    eventList = eventList.Where(entry => LocationWiseEventList.Contains(entry.ID)).ToList();
                }

                if (!string.IsNullOrEmpty(Filter))
                {
                    eventList = eventList.Where(entry => entry.Name.ToUpper().Contains(Filter.ToUpper())).ToList();
                }
                return eventList;
            }
        }


        public static int GetCompanyType(int Customer_BranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int type = 0;
                type = (from row in entities.CustomerBranches
                        where row.ID == Customer_BranchID
                        select (int)row.ComType).FirstOrDefault();
                return type;
            }
        }

        public static string GetEventName(int eventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string EventName = "";
                EventName = (from row in entities.Events
                             where row.ID == eventID
                             select row.Name).FirstOrDefault();
                return EventName;
            }
        }

        public static long GetEventAssignmentID(long EventInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long resultset = (from x in entities.EventAssignments
                                  where x.EventInstanceID == EventInstanceID && x.Role == 10
                                  select x.ID).SingleOrDefault();

                return resultset;
            }

        }

        public static void UpdateEventAssignDates(EventAssignDate eventAssignDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                EventAssignDate subEventToUpdate = (from row in entities.EventAssignDates
                                                    where row.ParentEventID == eventAssignDate.ParentEventID
                                                    && row.EventScheduleOnID == eventAssignDate.EventScheduleOnID
                                                    && row.IntermediateEventID == eventAssignDate.IntermediateEventID && row.SubEventID == eventAssignDate.SubEventID
                                                    select row).FirstOrDefault();

                subEventToUpdate.Date = eventAssignDate.Date;
                entities.SaveChanges();
            }
        }

        public static bool CheckEventComplianceAssigned(int complianceID, int EventsheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceInstances
                            on row.ComplianceInstanceID equals row1.ID
                            where row1.ComplianceId == complianceID && row.EventScheduledOnID == EventsheduleOnID
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool CheckSubEventDateAssigned(int parentEventID, int ChildEventID, int EventScheduledOnId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var date = (from row in entities.EventAssignDates
                            where row.ParentEventID == parentEventID && row.SubEventID == ChildEventID && row.EventScheduleOnID == EventScheduledOnId
                              && row.IntermediateEventID == 0
                            select row.Date).FirstOrDefault();

                if (Convert.ToDateTime(date) == Convert.ToDateTime("1/1/1900 12:00:00 AM"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static Compliance GetCompliance(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.Compliances
                            where row.ID == complianceID
                            select row).FirstOrDefault();

                return form;
            }
        }

        public static void GenerateEventComplianceScheduele(int ParentEventID, int IntermediateEventID, int subEventID,
           long EventScheduledOnID, DateTime eventHeldOnDate, long ComplianceID, int ComplianceInstanceID, string days)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int IncludeDays = 0;
                var compdetails = GetCompliance(ComplianceID);
                DateTime scheduledOnDate = DateTime.Now;
                DateTime dtschedule = DateTime.Now;
                if (compdetails.ComplianceType == 0 && (compdetails.IsFrequencyBased == false || compdetails.IsFrequencyBased == null))
                {
                    //Normal Event based
                    int Days = Convert.ToInt32(days);
                    if (Days == 0)
                    {
                        IncludeDays = 0;
                    }
                    else
                    {
                        if (Days > 0)
                        {
                            IncludeDays = Days - 1;
                        }
                        else
                        {
                            IncludeDays = Days + 1;
                        }
                    }
                    scheduledOnDate = eventHeldOnDate.AddDays(Convert.ToInt32(IncludeDays));
                }
                else if (compdetails.ComplianceType == 1)
                {
                    //Checklist 
                    int Days = Convert.ToInt32(days);
                    if (Days == 0)
                    {
                        IncludeDays = 0;
                    }
                    else
                    {
                        if (Days > 0)
                        {
                            IncludeDays = Days - 1;
                        }
                        else
                        {
                            IncludeDays = Days + 1;
                        }
                    }
                    scheduledOnDate = eventHeldOnDate.AddDays(Convert.ToInt32(IncludeDays));
                }
                else if (compdetails.ComplianceType == 0 && compdetails.IsFrequencyBased == true)
                {
                    //Frequency Based 
                    if (compdetails.Frequency == 0) //Monthly
                    {
                        int DueDate = Convert.ToInt32(compdetails.DueDate);
                        var lastDayOfMonth = DateTime.DaysInMonth(eventHeldOnDate.Year, eventHeldOnDate.Month);
                        if (DueDate > lastDayOfMonth)
                        {
                            DueDate = lastDayOfMonth;
                        }
                        dtschedule = new DateTime(eventHeldOnDate.Year, eventHeldOnDate.Month, DueDate);
                        if (dtschedule < eventHeldOnDate)
                        {
                            dtschedule = dtschedule.AddMonths(1);
                            var lastDayOfMonth1 = DateTime.DaysInMonth(dtschedule.Year, dtschedule.Month);
                            if (DueDate > lastDayOfMonth1)
                            {
                                DueDate = lastDayOfMonth1;
                            }
                            dtschedule = new DateTime(dtschedule.Year, dtschedule.Month, DueDate);
                        }
                        scheduledOnDate = dtschedule;
                    }
                    else if (compdetails.Frequency == 1) //Quarterly
                    {
                        int DueDate = Convert.ToInt32(compdetails.DueDate);

                        DateTime dtQuarterly = eventHeldOnDate.AddDays(90);
                        if (dtQuarterly.Year >= eventHeldOnDate.Year)
                        {
                            var lastDayOfMonth = DateTime.DaysInMonth(dtQuarterly.Year, dtQuarterly.Month);
                            if (DueDate > lastDayOfMonth)
                            {
                                DueDate = lastDayOfMonth;
                            }
                            dtschedule = new DateTime(dtQuarterly.Year, dtQuarterly.Month, DueDate);
                        }
                        else
                        {
                            var lastDayOfMonth = DateTime.DaysInMonth(eventHeldOnDate.Year, dtQuarterly.Month);
                            if (DueDate > lastDayOfMonth)
                            {
                                DueDate = lastDayOfMonth;
                            }
                            dtschedule = new DateTime(eventHeldOnDate.Year, dtQuarterly.Month, DueDate);
                        }
                        if (dtschedule < eventHeldOnDate)
                        {
                            dtschedule = dtschedule.AddDays(90);
                            var lastDayOfMonth = DateTime.DaysInMonth(dtschedule.Year, dtschedule.Month);
                            if (DueDate > lastDayOfMonth)
                            {
                                DueDate = lastDayOfMonth;
                            }
                            dtschedule = new DateTime(dtschedule.Year, dtschedule.Month, DueDate);
                        }
                        scheduledOnDate = dtschedule;
                    }
                    else if (compdetails.Frequency == 2) //HalfYearly
                    {
                        int DueDate = Convert.ToInt32(compdetails.DueDate);
                        dtschedule = new DateTime(eventHeldOnDate.Year, 1, DueDate);
                        if (dtschedule < eventHeldOnDate)
                        {
                            var lastDayOfMonth = DateTime.DaysInMonth(eventHeldOnDate.Year, 6);
                            if (DueDate > lastDayOfMonth)
                            {
                                DueDate = lastDayOfMonth;
                            }
                            dtschedule = new DateTime(eventHeldOnDate.Year, 6, DueDate);
                            if (dtschedule < eventHeldOnDate)
                            {
                                DateTime dtNextYear = eventHeldOnDate.AddYears(1);
                                var lastDayOfMonth1 = DateTime.DaysInMonth(dtNextYear.Year, 1);
                                if (DueDate > lastDayOfMonth1)
                                {
                                    DueDate = lastDayOfMonth1;
                                }
                                dtschedule = new DateTime(dtNextYear.Year, 1, DueDate);
                            }
                        }
                        scheduledOnDate = dtschedule;
                    }
                    else if (compdetails.Frequency == 3) //Annual
                    {
                        int DueDate = Convert.ToInt32(compdetails.DueDate);
                        dtschedule = new DateTime(eventHeldOnDate.Year, 1, DueDate);
                        if (dtschedule < eventHeldOnDate)
                        {
                            dtschedule = dtschedule.AddYears(1);
                            dtschedule = new DateTime(dtschedule.Year, 1, DueDate);
                        }
                        scheduledOnDate = dtschedule;
                    }
                    else if (compdetails.Frequency == 4) //FourMonthly
                    {
                        int DueDate = Convert.ToInt32(compdetails.DueDate);
                        DateTime dtFourMonthly = eventHeldOnDate.AddDays(120);

                        if (dtFourMonthly.Year >= eventHeldOnDate.Year)
                        {
                            var lastDayOfMonth = DateTime.DaysInMonth(dtFourMonthly.Year, dtFourMonthly.Month);
                            if (DueDate > lastDayOfMonth)
                            {
                                DueDate = lastDayOfMonth;
                            }
                            dtschedule = new DateTime(dtFourMonthly.Year, dtFourMonthly.Month, DueDate);
                        }
                        else
                        {
                            var lastDayOfMonth = DateTime.DaysInMonth(eventHeldOnDate.Year, dtFourMonthly.Month);
                            if (DueDate > lastDayOfMonth)
                            {
                                DueDate = lastDayOfMonth;
                            }
                            dtschedule = new DateTime(eventHeldOnDate.Year, dtFourMonthly.Month, DueDate);
                        }

                        if (dtschedule < eventHeldOnDate)
                        {
                            dtschedule = dtschedule.AddDays(120);
                            var lastDayOfMonth = DateTime.DaysInMonth(dtschedule.Year, dtschedule.Month);
                            if (DueDate > lastDayOfMonth)
                            {
                                DueDate = lastDayOfMonth;
                            }
                            dtschedule = new DateTime(dtschedule.Year, dtschedule.Month, DueDate);
                        }
                        scheduledOnDate = dtschedule;
                    }
                    else if (compdetails.Frequency == 5) //TwoYearly
                    {
                        int DueDate = Convert.ToInt32(compdetails.DueDate);
                        dtschedule = new DateTime(eventHeldOnDate.Year, 1, DueDate);
                        if (dtschedule < eventHeldOnDate)
                        {
                            dtschedule = dtschedule.AddYears(2);
                            dtschedule = new DateTime(dtschedule.Year, 1, DueDate);
                        }
                        scheduledOnDate = dtschedule;
                    }
                    else if (compdetails.Frequency == 6) //SevenYearly
                    {
                        int DueDate = Convert.ToInt32(compdetails.DueDate);
                        dtschedule = new DateTime(eventHeldOnDate.Year, 1, DueDate);
                        //if (dtschedule <= eventHeldOnDate)
                        //{
                        dtschedule = dtschedule.AddYears(7);
                        dtschedule = new DateTime(dtschedule.Year, 1, DueDate);
                        //}
                        scheduledOnDate = dtschedule;
                    }
                    else if (compdetails.Frequency == 8) //Weekly
                    {
                        DateTime ScheduleDate = GetNextDateWeekly(EventScheduledOnID, eventHeldOnDate, ComplianceID, ComplianceInstanceID, Convert.ToInt32(compdetails.DueWeekDay));
                        scheduledOnDate = ScheduleDate;

                        //DateTime date = DateTime.Now;
                        //DateTime curruntDate;
                        //curruntDate = DateTime.Now;
                        //DateTime dteventHeldOnDate = eventHeldOnDate.Date;
                        //if (dteventHeldOnDate < curruntDate)
                        //{
                        //    while (dteventHeldOnDate < curruntDate)
                        //    {
                        //        int weekdayeventHeldOnDate = Convert.ToInt32(dteventHeldOnDate.DayOfWeek);
                        //        if (weekdayeventHeldOnDate == compdetails.DueWeekDay)
                        //        {
                        //            date = new DateTime(dteventHeldOnDate.Year, Convert.ToInt32(dteventHeldOnDate.Month), Convert.ToInt32(dteventHeldOnDate.Day));
                        //            date = dteventHeldOnDate;
                        //            scheduledOnDate = dteventHeldOnDate;
                        //            break;
                        //        }
                        //        else
                        //        {
                        //            dteventHeldOnDate = dteventHeldOnDate.AddDays(1);
                        //        }
                        //    }
                        //}
                        //else if (dteventHeldOnDate > curruntDate)
                        //{
                        //    DateTime tempdteventHeldOnDate = dteventHeldOnDate;
                        //    while (dteventHeldOnDate > curruntDate)
                        //    {
                        //        int weekdayeventHeldOnDate = Convert.ToInt32(curruntDate.DayOfWeek);
                        //        if (weekdayeventHeldOnDate == compdetails.DueWeekDay)
                        //        {
                        //            date = new DateTime(tempdteventHeldOnDate.Year, Convert.ToInt32(tempdteventHeldOnDate.Month), Convert.ToInt32(tempdteventHeldOnDate.Day));
                        //            date = dteventHeldOnDate;
                        //            scheduledOnDate = dteventHeldOnDate;
                        //            break;
                        //        }
                        //        else
                        //        {
                        //            curruntDate = curruntDate.AddDays(1);
                        //            tempdteventHeldOnDate =  tempdteventHeldOnDate.AddDays(1);
                        //        }
                        //    }
                        //}
                    }
                    else if (compdetails.Frequency == 9) //Fortnight
                    {
                        dtschedule = new DateTime(eventHeldOnDate.Year, eventHeldOnDate.Month, 1);
                        if (dtschedule < eventHeldOnDate)
                        {
                            dtschedule = new DateTime(eventHeldOnDate.Year, eventHeldOnDate.Month, 14);
                            if (dtschedule < eventHeldOnDate)
                            {
                                dtschedule = eventHeldOnDate.AddMonths(1);
                                dtschedule = new DateTime(dtschedule.Year, dtschedule.Month, 1);
                            }
                        }
                        scheduledOnDate = dtschedule;
                    }
                }
                else if (compdetails.ComplianceType == 2)
                {
                    //Time Based 
                    int IntervalDays = Convert.ToInt32(compdetails.IntervalDays);
                    if (IntervalDays == 0)
                    {
                        IncludeDays = 0;
                    }
                    else
                    {
                        if (IntervalDays > 0)
                        {
                            IncludeDays = IntervalDays - 1;
                        }
                        else
                        {
                            IncludeDays = IntervalDays + 1;
                        }
                    }
                    dtschedule = eventHeldOnDate.AddDays(Convert.ToInt32(IncludeDays));
                    scheduledOnDate = dtschedule;
                }
                var onloff = 1;
                var beforafter = 0;
                if (compdetails.duedatetype != null)
                {
                    beforafter = (int)compdetails.duedatetype;
                }
                if (compdetails.onlineoffline != null)
                {
                    onloff = Convert.ToByte(compdetails.onlineoffline);
                }

                #region Holiday Logic
                //DateTime ActuScheduleOn = new DateTime();
                DateTime? ActuScheduleOn = null;
                DateTime ScheduleOn1;
                if (onloff == 1) //online
                {
                    ScheduleOn1 = scheduledOnDate;
                }
                else
                {
                    //offline                                    
                    var HolidayCount = ComplianceManagementComplianceScheduleon.CheckHoliday(ComplianceID, ComplianceInstanceID, scheduledOnDate, beforafter, onloff);
                    if (HolidayCount > 0)
                    {
                        if (beforafter == 1)
                        {
                            ScheduleOn1 = scheduledOnDate.AddDays(-HolidayCount);
                            ActuScheduleOn = scheduledOnDate.Date;
                        }
                        else
                        {
                            ScheduleOn1 = scheduledOnDate.AddDays(HolidayCount);
                            ActuScheduleOn = scheduledOnDate.Date;
                        }
                    }
                    else
                    {
                        ScheduleOn1 = scheduledOnDate;
                    }
                }
                #endregion
                DateTime dtStart;
                if (compdetails.StartDate != null)
                {
                    dtStart = Convert.ToDateTime(compdetails.StartDate);
                }
                else
                {
                    dtStart = new DateTime(2018, 1, 1);
                }
                //if (dtStart <= ScheduleOn1)
                //{
                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                complianceScheduleon.ComplianceInstanceID = ComplianceInstanceID;
                complianceScheduleon.ParentEventD = ParentEventID;
                complianceScheduleon.IntermediateEventID = IntermediateEventID;
                complianceScheduleon.SubEventID = subEventID;
                complianceScheduleon.ScheduleOn = ScheduleOn1;// scheduledOnDate;
                complianceScheduleon.IsActive = true;
                complianceScheduleon.IsUpcomingNotDeleted = true;
                complianceScheduleon.EventScheduledOnID = EventScheduledOnID;
                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                entities.SaveChanges();

                var AssignedRole = ComplianceManagement.GetAssignedUsers(ComplianceInstanceID);
                var performerRole = AssignedRole.Where(en => en.RoleID == 3).FirstOrDefault();
                if (performerRole != null)
                {
                    var user = UserManagement.GetByID((int)performerRole.UserID);
                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceInstanceId = ComplianceInstanceID,
                        ComplianceScheduleOnID = complianceScheduleon.ID,
                        CreatedBy = performerRole.UserID,
                        CreatedByText = user.FirstName + " " + user.LastName,
                        StatusId = 1,
                        Remarks = "New compliance assigned."
                    };

                    ComplianceManagement.CreateTransaction(transaction);

                    foreach (var roles in AssignedRole)
                    {
                        if (roles.RoleID != 6)
                        {
                            ComplianceManagement.CreateEventBasedComplinceReminders(ComplianceID, ComplianceInstanceID, roles.ID, eventHeldOnDate, scheduledOnDate, EventScheduledOnID);
                        }
                    }
                    //}
                }
            }
        }
        public static ComplianceInstance GetComplianceInstance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.ComplianceInstances
                                   where row.ID == ComplianceInstanceID
                                   && row.IsDeleted == false
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }

        public static List<ComplianceScheduleOn> GetScheduleOnEventCompliance(long complianceInstanceID, long eventScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.ComplianceScheduleOns
                                   where row.ComplianceInstanceID == complianceInstanceID
                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                   && row.EventScheduledOnID == eventScheduleOnID
                                   orderby row.ScheduleOn descending, row.ID descending
                                   select row).ToList();

                return scheduleObj;
            }
        }
        public static DateTime GetNextDateWeekly(long EventScheduleOnID, DateTime EventHeldDate, long complianceID, long ComplianceInstanceID, int WeeklyDayFromCompliance)
        {
            var lastSchedule = GetScheduleOnEventCompliance(ComplianceInstanceID, EventScheduleOnID);
            DateTime date = DateTime.Now;
            if (lastSchedule.Count() == 0)
            {
                DateTime curruntDate;
                curruntDate = DateTime.UtcNow.AddDays(30);
                while (EventHeldDate < curruntDate)
                {
                    int weekdayFromInstance = Convert.ToInt32(EventHeldDate.DayOfWeek);
                    if (weekdayFromInstance == WeeklyDayFromCompliance)
                    {
                        date = new DateTime(EventHeldDate.Year, Convert.ToInt32(EventHeldDate.Month), Convert.ToInt32(EventHeldDate.Day));
                        date = EventHeldDate;
                        break;
                    }
                    else
                    {
                        EventHeldDate = EventHeldDate.AddDays(1);
                    }
                }
            }
            else
            {
                date = EventHeldDate.AddDays(7);
            }
            return date;
        }

        public static void UpdateComplianceSheduleOnDates(int ParentEventID, int IntermediateEventID, int subEventID, int EventSheduleOnID, int ComplinaceID, DateTime UpdatedDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn ScheduleOns = (from row in entities.ComplianceScheduleOns
                                                    join row1 in entities.ComplianceInstances
                                                    on row.ComplianceInstanceID equals row1.ID
                                                    where row1.ComplianceId == ComplinaceID
                                                    && row.EventScheduledOnID == EventSheduleOnID
                                                    && row.ParentEventD == ParentEventID
                                                    && row.IntermediateEventID == IntermediateEventID
                                                    && row.SubEventID == subEventID
                                                    select row).FirstOrDefault();
                ScheduleOns.ScheduleOn = UpdatedDate;
                entities.SaveChanges();
            }
        }

        public static void UpdateReminderSheduleOnDates(int EventSheduleOnID, int ComplinaceID, DateTime UpdatedDate, DateTime Date)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceReminder> complianceReminder = (from row in entities.ComplianceReminders
                                                               join row1 in entities.ComplianceAssignments
                                                               on row.ComplianceAssignmentID equals row1.ID
                                                               join row2 in entities.ComplianceScheduleOns
                                                               on row1.ComplianceInstanceID equals row2.ComplianceInstanceID
                                                               join row3 in entities.ComplianceInstances
                                                               on row2.ComplianceInstanceID equals row3.ID
                                                               where row3.ComplianceId == ComplinaceID
                                                               && row2.EventScheduledOnID == EventSheduleOnID
                                                               select row).ToList();

                foreach (var cil in complianceReminder)
                {
                    var compReminder = (from row in entities.ComplianceReminders
                                        where row.ID == cil.ID && row.ComplianceAssignmentID == cil.ComplianceAssignmentID
                                        select row).FirstOrDefault();

                    if (compReminder != null)
                    {
                        compReminder.ComplianceDueDate = UpdatedDate;
                        compReminder.RemindOn = UpdatedDate;
                    }
                }

                entities.SaveChanges();


                //complianceReminder.ForEach(x=> x.e);

                //complianceReminder.ComplianceDueDate = UpdatedDate;
                //complianceReminder.RemindOn =Date ;
                //entities.SaveChanges();
            }
        }


    }
}
