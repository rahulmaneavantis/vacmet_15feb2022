﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ComplianceTypeManagement
    {
        public static List<InternalComplianceType> GetAllInternalCompliances(int CustomerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypes = (from row in entities.InternalComplianceTypes
                                       where row.IsDeleted == false
                                           // where row.CustomerID == CustomerID
                                       select row);
                if (CustomerID != -1)
                {
                    complianceTypes = complianceTypes.Where(entry => entry.CustomerID == CustomerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceTypes = complianceTypes.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceTypes = complianceTypes.OrderBy(entry => entry.Name);

                return complianceTypes.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static void UpdateScheme(Scheme SchemeData)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                Scheme SchemeToUpdate = (from row in entities.Schemes
                                         where row.ID == SchemeData.ID
                                         select row).FirstOrDefault();

                SchemeToUpdate.Title = SchemeData.Title;

                entities.SaveChanges();
            }
        }
        public static List<Scheme> ShowAllScheme()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.Schemes
                             where row.IsActive == true
                             select row).ToList();
                return Query;
            }
        }

        public static bool CheckComplianceSchemeExist(Scheme objScheme)
        {
            using (ComplianceDBEntities Entities = new ComplianceDBEntities())
            {
                var CheckVal = (from row in Entities.Schemes
                                where row.Title == objScheme.Title
                                && row.IsActive == objScheme.IsActive
                                select row).FirstOrDefault();

                if (CheckVal != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static long CreateScheme(Scheme objScheme)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Schemes.Add(objScheme);
                entities.SaveChanges();
                return objScheme.ID;
            }
        }

        public static bool CreateSchemeMapping(SchemeMapping objSchemeMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.SchemeMappings.Add(objSchemeMapping);
                entities.SaveChanges();
                return true;
            }
        }

        public static bool UpdateSchemaMapping(SchemeMapping objSchemeMapping)
        {
            using (ComplianceDBEntities Entities = new ComplianceDBEntities())
            {
                var CheckVal = (from row in Entities.SchemeMappings
                                where row.SchemeID == objSchemeMapping.SchemeID
                                && row.ActID == objSchemeMapping.ActID
                                select row).FirstOrDefault();

                if (CheckVal != null)
                {
                    CheckVal.IsActive = true;
                    CheckVal.UpdatedBy = objSchemeMapping.UpdatedBy;
                    CheckVal.UpdatedOn = DateTime.Now;
                    Entities.SaveChanges();
                    return true;
                }
                else
                {
                    Entities.SchemeMappings.Add(objSchemeMapping);
                    Entities.SaveChanges();
                    return true;
                }
            }
        }

        public static void DeActiveExistingSchemeMapping(long schemeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.SchemeMappings
                                   where row.SchemeID == schemeID
                                   && row.IsActive == true
                                   select row).ToList();

                if (queryResult.Count > 0)
                {
                    queryResult.ForEach(entry => entry.IsActive = false);
                    entities.SaveChanges();
                }
            }
        }

        public static List<SP_SchemeGetByID_Result> GetSchemebyID(long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.SP_SchemeGetByID(ID)
                                   select row).ToList();

                return queryResult;
            }
        }

        public static void DeleteSchemeByID(long schemeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Schemes
                                   where row.ID == schemeID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                if (queryResult != null)
                {
                    queryResult.IsActive = false;
                    entities.SaveChanges();
                }
            }
        }
        public static bool ExistsInternalComplianceType(string Name, int? CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalComplianceTypes
                             where row.Name.Equals(Name) && row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                //return query.Select(entry => true).First();
            }
        }
        public static bool InternalLicenseTypeUsed(int typeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var exists = (from row in entities.Lic_tbl_InternalLicenseInstance
                              where row.LicenseTypeID == typeID
                              && row.IsDeleted == false
                              select row).FirstOrDefault();
                if(exists == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }   
            }
        }
        public static bool InternalComplianceTypeUsed(int typeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var exists = (from row in entities.InternalCompliances
                              where row.IComplianceTypeID == typeID
                              && row.IsDeleted == false
                              select row).FirstOrDefault();
                if (exists == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static bool InternalComplianceCategoryUsed(int CategoryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var exists = (from row in entities.InternalCompliances
                              where row.IComplianceCategoryID == CategoryID
                              && row.IsDeleted == false
                              select row).FirstOrDefault();

                if (exists == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public static List<ComplianceType> GetAll(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypes = (from row in entities.ComplianceTypes
                                       //where row.IsDeleted == false
                                       select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceTypes = complianceTypes.Where(entry => entry.Name.Contains(filter)||entry.Description.Contains(filter));
                }

                complianceTypes = complianceTypes.OrderBy(entry => entry.Name);

                return complianceTypes.OrderBy(entry=>entry.Name).ToList();
            }
        }


        public static List<InternalComplianceType> GetAllInternal(int CustomerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypes = (from row in entities.InternalComplianceTypes
                                           where row.CustomerID == CustomerID
                                           && row.IsDeleted == false
                                       select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceTypes = complianceTypes.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceTypes = complianceTypes.OrderBy(entry => entry.Name);

                return complianceTypes.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<Act> GetAllACTS(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Acts = (from row in entities.Acts
                            where row.IsDeleted == false
                            select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    Acts = Acts.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                Acts = Acts.OrderBy(entry => entry.Name);

                return Acts.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Industry> GetAllIndustrys()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Industries = (from row in entities.Industries                           
                            select row);
                Industries = Industries.OrderBy(entry => entry.Name);
                return Industries.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<M_LegalEntityType> GetAllM_LegalEntityTypes()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var LegalEntityTypes = (from row in entities.M_LegalEntityType                           
                            select row);
                LegalEntityTypes = LegalEntityTypes.OrderBy(entry => entry.EntityTypeName);
                return LegalEntityTypes.OrderBy(entry => entry.EntityTypeName).ToList();
            }
        }
        
        public static ComplianceType GetByID(int complianceTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceType = (from row in entities.ComplianceTypes
                                      where row.ID == complianceTypeID
                                      select row).SingleOrDefault();

                return complianceType;
            }
        }

        public static void Delete(int complianceTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //ComplianceType complianceTypeToDelete = new ComplianceType() { ID = complianceTypeID };
                //entities.ComplianceTypes.Attach(complianceTypeToDelete);

                ComplianceType complianceTypeToDelete = (from row in entities.ComplianceTypes
                                                         where row.ID == complianceTypeID
                                                         select row).FirstOrDefault();

                entities.ComplianceTypes.Remove(complianceTypeToDelete);

                entities.SaveChanges();
            }
        }

        public static bool Exists(ComplianceType complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceTypes
                             where row.Name.Equals(complianceType.Name)
                             select row);

                if (complianceType.ID > 0)
                {
                    query = query.Where(entry => entry.ID != complianceType.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void Update(ComplianceType complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //ComplianceType complianceTypeToUpdate = new ComplianceType() { ID = complianceType.ID };
                //entities.ComplianceTypes.Attach(complianceTypeToUpdate);

                ComplianceType complianceTypeToUpdate = (from row in entities.ComplianceTypes
                                                         where row.ID == complianceType.ID 
                                                         select row).FirstOrDefault();

                complianceTypeToUpdate.Name = complianceType.Name;
                complianceTypeToUpdate.Description = complianceType.Description;

                entities.SaveChanges();
            }
        }

        public static void Create(ComplianceType complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ComplianceTypes.Add(complianceType);

                entities.SaveChanges();
            }
        }

        public static bool CanDelete(int typeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var exists = (from row in entities.Acts
                              where row.ComplianceTypeId == typeID
                              select true).FirstOrDefault();

                return !exists;
            }
        }

        //added by sudarshan for Excelutility
        public static int GetIdByName(string complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypeID = (from row in entities.ComplianceTypes
                                        where row.Name.ToUpper().Trim().Equals(complianceType.ToUpper().Trim())
                                        select row.ID).SingleOrDefault();

                return complianceTypeID;
            }
        }

        public static void Create(List<ComplianceType> complianceType)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                complianceType.ForEach(entry =>
                {
                    entities.ComplianceTypes.Add(entry);

                });
                entities.SaveChanges();
            }

        }

        public static bool Exists(string ComplianceTypeName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceTypes
                             where row.Name.Contains(ComplianceTypeName)
                             select row);

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        /* Internal compliance Type functions */

    
        public static Lic_tbl_InternalLicenseType_Master GetByIDInternalComplianceTypeName(string ComplianceTypeName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceType = (from row in entities.Lic_tbl_InternalLicenseType_Master
                                      where
                                       row.Name.ToUpper().Trim().Equals(ComplianceTypeName.ToUpper().Trim())
                                      //row.Name == ComplianceTypeName
                                      && row.IsDeleted == false
                                      select row).FirstOrDefault();

                return complianceType;
            }
        }
        public static InternalComplianceType GetByIDInternalCompliance(int complianceTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceType = (from row in entities.InternalComplianceTypes
                                      where row.ID == complianceTypeID
                                      && row.IsDeleted == false
                                      select row).SingleOrDefault();

                return complianceType;
            }
        }

        public static void DeleteInternalCompliance(int complianceTypeID, int LicenseTypeID, int CustomerID)
        {
            long CustID = Convert.ToInt64(CustomerID);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //InternalComplianceType complianceTypeToDelete = new InternalComplianceType() { ID = complianceTypeID };
                //entities.InternalComplianceTypes.Attach(complianceTypeToDelete);
                InternalComplianceType complianceTypeToDelete = (from row in entities.InternalComplianceTypes
                                                                 where row.ID == complianceTypeID
                                                                 && row.IsDeleted == false
                                                                 select row).FirstOrDefault();
                if (complianceTypeToDelete != null)
                {
                    complianceTypeToDelete.IsDeleted = true;
                    entities.SaveChanges();
                }
                Lic_tbl_InternalLicenseType_Master complianceTypeToDelete1 = (from row in entities.Lic_tbl_InternalLicenseType_Master
                                                                              where row.ID == LicenseTypeID
                                                                              && row.IsDeleted == false
                                                                              && row.CustomerID == CustomerID
                                                                              select row).FirstOrDefault();
                if (complianceTypeToDelete1 != null)
                {
                    complianceTypeToDelete1.IsDeleted = true;
                    entities.SaveChanges();
                }

                // entities.InternalComplianceTypes.Add(complianceTypeToDelete);
                //  entities.InternalComplianceTypes.Remove(complianceTypeToDelete);//added By Manisha
                //entities.SaveChanges();
            }
        }

        public static bool ExistsInternalCompliances(InternalComplianceType complianceType, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalComplianceTypes
                             where row.Name.Equals(complianceType.Name) && row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row);

                if (complianceType.ID > 0)
                {
                    query = query.Where(entry => entry.ID != complianceType.ID);
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public static InternalComplianceType TypeIDByTypeName(string Typename, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalComplianceTypes
                             where
                             row.Name.ToUpper().Trim().Equals(Typename.ToUpper().Trim())
                             && row.IsDeleted == false
                             && row.CustomerID == customerID
                             select row).FirstOrDefault();
                return query;

            }
        }
        public static bool ExistsInternalCompliances(long complianceTypeID, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalComplianceTypes
                             where row.ID== complianceTypeID && row.CustomerID == CustomerID
                              && row.IsDeleted == false
                             select row);                
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void Update(InternalComplianceType complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                InternalComplianceType complianceTypeToUpdate = (from row in entities.InternalComplianceTypes
                                                                 where row.ID == complianceType.ID
                                                                 && row.IsDeleted == false
                                                                 select row).FirstOrDefault();

                complianceTypeToUpdate.Name = complianceType.Name.Trim();
                complianceTypeToUpdate.Description = complianceType.Description;
                complianceTypeToUpdate.CustomerID = complianceType.CustomerID;
                complianceTypeToUpdate.Is_License= complianceType.Is_License;
                entities.SaveChanges();
            }
        }

        public static void Create(InternalComplianceType complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.InternalComplianceTypes.Add(complianceType);

                entities.SaveChanges();
            }
        }

        public static int GetIdByNameInternalCompliance(string complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypeID = (from row in entities.InternalComplianceTypes
                                        where row.Name.ToUpper().Trim().Equals(complianceType.ToUpper().Trim())
                                        && row.IsDeleted == false
                                        select row.ID).SingleOrDefault();

                return complianceTypeID;
            }
        }

        public static void Create(List<InternalComplianceType> complianceType)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                complianceType.ForEach(entry =>
                {
                    entities.InternalComplianceTypes.Add(entry);

                });
                entities.SaveChanges();
            }

        }

        public static List<object> GetAllNVP(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IType = (from row in entities.InternalComplianceTypes
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             orderby row.Name ascending
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return IType;
            }
        }



        //added By Manisha
        public static List<InternalComplianceType> GetAllInternalCompliancesWC(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypes = (from row in entities.InternalComplianceTypes
                                       where row.IsDeleted == false

                                       select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceTypes = complianceTypes.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceTypes = complianceTypes.OrderBy(entry => entry.Name);

                return complianceTypes.OrderBy(entry => entry.Name).ToList();
            }
        }
    }
}
