﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models
{
   public class TabMasterEntry
    {
        public string TabName { get; set; }

        public int ID { get; set; }
       
        public List<TabDetail> locations { get; set; }
    }
}
