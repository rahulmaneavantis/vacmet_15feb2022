﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ResearchActAssignmentClass
    {
        public static Research_ActAssignment GetByActID(int ActId, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Research_ActAssignment
                                  where row.ActId == ActId && row.UserId == userid
                                  select row).FirstOrDefault();

                return compliance;
            }
        }
        public static List<ComplianceType> GetAllComplianceType(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypes = (from row in entities.ComplianceTypes
                                       select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceTypes = complianceTypes.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceTypes = complianceTypes.OrderBy(entry => entry.Name);

                return complianceTypes.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<ComplianceCategory> GetAllComplianceCategory(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceCategorys = (from row in entities.ComplianceCategories
                                           select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
                }

                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Research_GridDisplayView> GetGridDisplay(int CustomerID, int UserId, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var complianceList = (from row in entities.Research_GridDisplayView
                                      where row.CustomerId == CustomerID
                                      select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceList = complianceList.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                if (UserId != -1)
                {
                    complianceList = complianceList.Where(entry => entry.UserId == UserId).ToList();
                }

                return complianceList;
            }
        }
        public static void ReplaceUserForActAssignment(int oldUserID, int newUserID, List<Tuple<int, string>> ActIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                foreach (var cil in ActIdList)
                {
                    var assignmentData = (from row in entities.Research_ActAssignment
                                          where row.ActId == cil.Item1
                                          && row.UserId == oldUserID
                                          select row).FirstOrDefault();

                    if (assignmentData != null)
                    {
                        assignmentData.UserId = newUserID;
                    }
                }
                entities.SaveChanges();
            }
        }
        public static List<Research_GridDisplayView> GetGridDisplay(int CustomerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var complianceList = (from row in entities.Research_GridDisplayView
                                      where row.CustomerId == CustomerID
                                      select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceList = complianceList.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                if (complianceList.Count > 0)
                {
                    complianceList = complianceList.OrderBy(entry => entry.UserName).ToList();
                }
                return complianceList;
            }
        }
        public static object GetTotalAct(long CustomerID, int catagoryid, int typeId, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var allassignedActs = (from row in entities.Research_ActAssignment
                                       where row.IsActive == false && row.CustomerId == CustomerID
                                       select row.ActId).ToList();


                var acts = (from row in entities.ActViews
                            select row).ToList();

                if (allassignedActs.Count > 0)
                {
                    acts = acts.Where(a => !allassignedActs.Contains(a.ID)).ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    acts = acts.Where(entry => entry.Name.Contains(filter) || entry.ComplianceCategoryName.Contains(filter)
                    || entry.ComplianceTypeName.Contains(filter)).ToList();
                }

                if (catagoryid != -1)
                {
                    acts = acts.Where(entry => entry.ComplianceCategoryId == catagoryid).ToList();
                }

                if (typeId != -1)
                {
                    acts = acts.Where(entry => entry.ComplianceTypeId == typeId).ToList();
                }

                var actlist = (from row in acts
                               select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();
                return actlist;
            }
        }
        public static List<object> GetAllAssignedNVP(int customerID, List<long> ids = null, bool Flags = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             join row1 in entities.Research_ActAssignment
                             on row.ID equals row1.UserId
                             where row.CustomerID == customerID
                             && row.IsDeleted == false && row.IsActive == true
                             && row1.IsActive == false
                             select row).Distinct();



                if (Flags == true)
                {
                    query = query.Where(entry => entry.RoleID != 8);
                }

                if (ids != null)
                {
                    query = query.Where(entry => ids.Contains(entry.ID));
                }

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                return users.ToList<object>();
            }
        }
        public static List<object> GetAllNVP(int customerID = -1, List<long> ids = null, bool Flags = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false && row.IsActive == true
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.CustomerID == customerID);
                }

                if (Flags == true)
                {
                    query = query.Where(entry => entry.RoleID != 8);
                }

                if (ids != null)
                {
                    query = query.Where(entry => ids.Contains(entry.ID));
                }

                var users = (from row in query
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList();

                return users.ToList<object>();
            }
        }
        public static object GetCustomer()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Customers
                            where row.IsDeleted == false && row.Status == 1
                            select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).Distinct().ToList<object>();

                return acts;
            }
        }
        public static void AddDetailsTempAssignmentTable(List<Research_ActAssignment> Tempassignments)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    entities.Research_ActAssignment.Add(entry);
                    entities.SaveChanges();
                });
            }
        }
        public static void ActDelete(int Actid, int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ActAssignmentToDelete = (from row in entities.Research_ActAssignment
                                             where row.ActId == Actid && row.CustomerId == Customerid
                                             select row).ToList();

                ActAssignmentToDelete.ForEach(entry =>
                {
                    entry.IsActive = true;
                    entities.SaveChanges();
                });
            }
        }
        public static bool CheckActAssigned(int ActID, int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Research_ActAssignment
                             where row.ActId == ActID && row.CustomerId == CustomerId && row.IsActive == false
                             select row);
                return query.Select(entry => true).FirstOrDefault();

            }
        }
    }
}
