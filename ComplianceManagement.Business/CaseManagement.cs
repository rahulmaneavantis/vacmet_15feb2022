﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CaseManagement
    {
     
        public static int GetPendingHearing(long CustomerID, int loggedInUserID, string loggedInUserRole, int roleID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {

                var query = CaseManagement.GetPendingHearingList(CustomerID, loggedInUserID, loggedInUserRole, roleID);               
                if (query.Count > 0)
                {

                    return query.Count();
                }
                else
                {
                    return 0;
                }
            }
        }
        public static List<USP_GetPendingHearingList_Result> GetPendingHearingList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var PendingHearingList = (from row in entities.USP_GetPendingHearingList(Convert.ToInt32(customerID), loggedInUserID, loggedInUserRole)
                                              select row).ToList();
                    return PendingHearingList;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static int CreateAdvocateBillCaseHearing(List<tbl_CaseAdvocateBill> lstObjMapping, int advbillid)
        {
            int BillId = 0;

            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    BillId = 0;
                    lstObjMapping.ForEach(eachRecord =>
                    {
                        var prevMappingRecord = (from row in entities.tbl_CaseAdvocateBill
                                                 where row.NoticeCaseInstanceID == eachRecord.NoticeCaseInstanceID
                                                 && row.InvoiceNo.ToLower().Trim() == eachRecord.InvoiceNo.ToLower().Trim()
                                                 && row.IsActive == true
                                                 select row).FirstOrDefault();

                        if (prevMappingRecord != null)
                        {
                            prevMappingRecord.HearingRef = eachRecord.HearingRef;
                            prevMappingRecord.InvoiceNo = eachRecord.InvoiceNo;
                            prevMappingRecord.InvoiceAmount = eachRecord.InvoiceAmount;
                            prevMappingRecord.NoticeCaseInstanceID = eachRecord.NoticeCaseInstanceID;
                            prevMappingRecord.Lawyer = eachRecord.Lawyer;
                            prevMappingRecord.Remark = eachRecord.Remark;
                            prevMappingRecord.IsActive = true;
                            prevMappingRecord.UpdatedBy = eachRecord.CreatedBy;
                            prevMappingRecord.UpdatedOn = DateTime.Now;
                            prevMappingRecord.Currency = eachRecord.Currency;
                            BillId = prevMappingRecord.ID;
                            entities.SaveChanges();
                        }
                        else
                        {
                            entities.tbl_CaseAdvocateBill.Add(eachRecord);
                            entities.SaveChanges();
                            BillId = eachRecord.ID;
                        }
                    });

                    return BillId;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return BillId;
                }
            }
        }

        public static string IFUserEnable(int UserID, int ContractInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.tblContractEditApprovals
                             where row.CreatedBy == UserID
                             && row.IsDeleted == false
                             && row.ContractInstanceID == ContractInstanceID
                             select row).FirstOrDefault();

                if (query != null)
                {
                    if (query.RequestStatus == "Approved")
                    {
                        if (query.Validtill >= DateTime.Now.Date)
                        {
                            return "Approved";
                        }
                        else
                        {
                            return "Validity Expired";
                        }
                    }
                    else if (query.RequestStatus == "Rejected")
                    {
                        return "Rejected";
                    }
                    else
                    {
                        return "Already Requested";
                    }
                }
                else
                {
                    return "Not Requested";
                }
            }
        }
        //public static string IFUserEnable(int UserID, int ContractInstanceID)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var query = (from row in entities.tblContractEditApprovals
        //                     where row.CreatedBy == UserID
        //                     && row.IsDeleted == false
        //                     && row.ContractInstanceID == ContractInstanceID
        //                     select row).FirstOrDefault();

        //        if (query != null && query.Validtill != null)
        //        {
        //            if (query.RequestStatus == "Approved")
        //            {
        //                if (query.Validtill >= DateTime.Now.Date)
        //                {
        //                    return "Approved";
        //                }
        //                else
        //                {
        //                    return "Validity Expired";
        //                }
        //            }
        //            else
        //            {
        //                return "Rejected";
        //            }
        //        }
        //        else
        //        {
        //            return "Request";
        //        }
        //    }
        //}
        public static bool ExistsCourtCaseNoForRPA(string caseRefNo, string CaseYear)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalCaseInstance
                             where row.CaseRefNo.ToUpper().Trim().Equals(caseRefNo.ToUpper().Trim())
                             && row.IsDeleted == false
                             select row);

                if (!string.IsNullOrEmpty(CaseYear))
                {
                    query = query.Where(entry => entry.CaseYear == CaseYear);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static bool CheckForClient(int CustomerID, string Param)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName.ToLower() == Param.ToLower()
                            && row.ClientID == CustomerID
                            select row).Count();

                if (data != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }
        public static bool CheckForClientNew(int CustomerID, string Param)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName.ToLower() == Param.ToLower()
                            && row.ClientID == CustomerID
                            select row).Count();

                if (data != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        public static string GetLawFirmNameByID(int LawFirmIDs)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var LawFirmID = (from row in entities.tbl_Lawyer
                                 where row.ID == LawFirmIDs
                                 && row.IsActive == true
                                 select row.FirstName + " " + row.LastName).FirstOrDefault();

                return LawFirmID;
            }
        }

        public static string GetLawyerUserNameByID(int lawFirmID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserID = (from row in entities.Users
                              where row.ID == lawFirmID
                              select row.FirstName + " " + row.LastName).FirstOrDefault();

                if (UserID != null)
                {
                    return UserID;
                }
                else
                    return "0";
            }
        }

        
        public static List<string> GetAssignedUserAndOwnerMailexceptLawyer(List<long> updatedAssingedUser)
        {
            using (ComplianceDBEntities Entities = new ComplianceDBEntities())
            {
                var QueryResult = (from row in Entities.Users
                                   where row.IsActive == true
                                   && row.IsDeleted == false
                                   && row.LawyerFirmID != null
                                   && updatedAssingedUser.Contains(row.ID)
                                   select row.Email).ToList();

                return QueryResult;
            }
        }
        public static List<FileData> GetAllComplianceDocuments(List<int> scheduledOnIDList)
        {
            using (ComplianceDBEntities Entities = new ComplianceDBEntities())
            {
                var QueryResult = (from row in Entities.FileDataMappings
                                   join row1 in Entities.FileDatas
                                   on row.FileID equals row1.ID
                                   where scheduledOnIDList.Contains((int)(row.ScheduledOnID))
                                   select row1).ToList();

                return QueryResult;
            }
        }
        public static int ValidataFY(string FY)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.FinancialYearDetails
                            where row.FinancialYear == FY
                            select row.Id).FirstOrDefault();

                return user;
            }
        }
        public static List<string> getCaseOwnerAndInternalUser(int CaseInstanceID, long customerID)
        {

            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var user = (from row in entities.SP_GetOwnerAndInternalUserMail(Convert.ToInt32(customerID), CaseInstanceID)
                            select row).ToList();

                return user;
            }
        }

        public static List<string> Mailallowedornotcheck(string ServiceType, long customerid)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                int custid = Convert.ToInt32(customerid);

                var user = (from row in entities.SP_LitiUserDetails(custid)
                            where !row.MailServicesID.ToLower().Contains(ServiceType.ToLower())
                            select row.Email).ToList();

                return user;
            }
        }

        public static List<string> GetmanagementUser(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool CompanyAdminEnable = CustomerManagement.CheckForClient(Convert.ToInt32(customerID), "SendMailToCADMN");

                if (CompanyAdminEnable)
                {
                    List<long> Ids = new List<long>();
                    Ids = (from row in entities.UserMailExclusions where row.CustomerId == customerID select (long)row.userId).ToList();
                    var user = (from row in entities.Users
                                where row.CustomerID == customerID
                                && (row.LitigationRoleID == 8 || row.LitigationRoleID == 2) && !Ids.Contains(row.ID)
                                && row.IsDeleted == false && row.IsActive == true
                                select row.Email).ToList();

                    return user;
                }
                else
                {
                    List<long> Ids = new List<long>();
                    Ids = (from row in entities.UserMailExclusions where row.CustomerId == customerID select (long)row.userId).ToList();
                    var user = (from row in entities.Users
                                where row.CustomerID == customerID
                                && row.LitigationRoleID == 8 && !Ids.Contains(row.ID)
                                && row.IsDeleted == false && row.IsActive == true
                                select row.Email).ToList();

                    return user;
                }
            }
        }
 

        public static void SaveLitigationReminderMail(tbl_LitigationReminderLog objRemind)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_LitigationReminderLog.Add(objRemind);
                entities.SaveChanges();
            }
        }

        public static object GetUserRole(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.Users
                                   where row.ID == ID
                                   select row.RoleID).FirstOrDefault();

                return queryResult;
            }
        }

        #region Case
        public static long CreateCase(tbl_LegalCaseInstance newNotice)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    newNotice.CreatedOn = DateTime.Now;
                    newNotice.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalCaseInstance.Add(newNotice);
                    entities.SaveChanges();

                    return newNotice.ID;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }

        public static List<CustFieldAndType> BindCustomeFieldData(long CustomerID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var ListofField = (from row in Entities.tbl_CustomField
                                   join row1 in Entities.tbl_CaseType
                                   on row.CaseNoticeCategory equals row1.ID
                                   where row.IsActive == true && row1.IsDeleted == false
                                   && row1.CustomerID == CustomerID
                                   select new CustFieldAndType()
                                   {
                                       ID = row.ID,
                                       Lable = row.Label,
                                       TypeID = (int)row.CaseNoticeCategory,
                                       CaseType = row1.CaseType
                                   }).ToList();

                return ListofField;
            }
        }

        public static bool ExistsCase(string TitleNotice, long CaseInstanceID, long customerId)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                if (CaseInstanceID != 0)
                {
                    var query = (from row in entities.tbl_LegalCaseInstance
                                 where row.IsDeleted == false && row.CustomerID == customerId
                                 && row.CaseTitle.Equals(TitleNotice)
                                 select row);

                    query = query.Where(entry => entry.ID != CaseInstanceID);


                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    var query = (from row in entities.tbl_LegalCaseInstance
                                 where row.IsDeleted == false && row.CustomerID == customerId
                                 && row.CaseTitle.Equals(TitleNotice)
                                 select row).FirstOrDefault();

                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                //var query = (from row in entities.tbl_LegalCaseInstance
                //             where row.IsDeleted == false
                //             && row.CaseTitle.Equals(TitleNotice)
                //             select row);

                //if (CaseInstanceID != 0)
                //{
                //    query = query.Where(entry => entry.ID != CaseInstanceID);
                //}              
                //return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static bool ExistsCase(string TitleNotice, long CaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                if (CaseInstanceID != 0)
                {
                    var query = (from row in entities.tbl_LegalCaseInstance
                                 where row.IsDeleted == false
                                 && row.CaseTitle.Equals(TitleNotice)
                                 select row);

                    query = query.Where(entry => entry.ID != CaseInstanceID);


                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    var query = (from row in entities.tbl_LegalCaseInstance
                                 where row.IsDeleted == false
                                 && row.CaseTitle.Equals(TitleNotice)
                                 select row).FirstOrDefault();

                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                //var query = (from row in entities.tbl_LegalCaseInstance
                //             where row.IsDeleted == false
                //             && row.CaseTitle.Equals(TitleNotice)
                //             select row);

                //if (CaseInstanceID != 0)
                //{
                //    query = query.Where(entry => entry.ID != CaseInstanceID);
                //}              
                //return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool UpdateCase(tbl_LegalCaseInstance newNotice, string CustomerID, string IsNoticeDate)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var noticeRecordToUpdate = (from row in entities.tbl_LegalCaseInstance
                                                where row.ID == newNotice.ID
                                                select row).FirstOrDefault();

                    if (noticeRecordToUpdate != null)
                    {
                        noticeRecordToUpdate.CaseType = newNotice.CaseType;
                        noticeRecordToUpdate.CaseRefNo = newNotice.CaseRefNo;
                        noticeRecordToUpdate.OpenDate = newNotice.OpenDate;
                        noticeRecordToUpdate.Section = newNotice.Section;
                        noticeRecordToUpdate.CaseCategoryID = newNotice.CaseCategoryID;
                        noticeRecordToUpdate.CaseTitle = newNotice.CaseTitle;
                        noticeRecordToUpdate.CaseDetailDesc = newNotice.CaseDetailDesc;
                        noticeRecordToUpdate.CustomerBranchID = newNotice.CustomerBranchID;
                        noticeRecordToUpdate.DepartmentID = newNotice.DepartmentID;
                        noticeRecordToUpdate.ContactPersonOfDepartment = newNotice.ContactPersonOfDepartment;
                        noticeRecordToUpdate.CourtID = newNotice.CourtID;
                        noticeRecordToUpdate.Judge = newNotice.Judge;
                        noticeRecordToUpdate.OwnerID = newNotice.OwnerID;
                        noticeRecordToUpdate.CaseRiskID = newNotice.CaseRiskID;
                        noticeRecordToUpdate.ClaimAmt = newNotice.ClaimAmt;
                        noticeRecordToUpdate.ProbableAmt = newNotice.ProbableAmt;
                        noticeRecordToUpdate.ImpactType = newNotice.ImpactType;
                        noticeRecordToUpdate.Monetory = newNotice.Monetory;
                        noticeRecordToUpdate.NonMonetory = newNotice.NonMonetory;
                        noticeRecordToUpdate.Years = newNotice.Years;
                        noticeRecordToUpdate.UpdatedBy = newNotice.UpdatedBy;
                        noticeRecordToUpdate.UpdatedOn = DateTime.Now;
                        noticeRecordToUpdate.InternalCaseNo = newNotice.InternalCaseNo;
                        noticeRecordToUpdate.CustomerID = newNotice.CustomerID;
                        noticeRecordToUpdate.CaseResult = newNotice.CaseResult;
                        noticeRecordToUpdate.PreDeposit = newNotice.PreDeposit;
                        noticeRecordToUpdate.PostDeposit = newNotice.PostDeposit;
                        noticeRecordToUpdate.CaseBudget = newNotice.CaseBudget;
                        noticeRecordToUpdate.Penalty = newNotice.Penalty;
                        noticeRecordToUpdate.ProvisioninBook = newNotice.ProvisioninBook;
                        noticeRecordToUpdate.Taxdemand = newNotice.Taxdemand;
                        noticeRecordToUpdate.Intrest = newNotice.Intrest;
                        noticeRecordToUpdate.Period = newNotice.Period;
                        noticeRecordToUpdate.state = newNotice.state;
                        noticeRecordToUpdate.Jurisdiction = newNotice.Jurisdiction;
                        noticeRecordToUpdate.amountpaid = newNotice.amountpaid;
                        noticeRecordToUpdate.favourable = newNotice.favourable;
                        noticeRecordToUpdate.unfavourable = newNotice.unfavourable;
                        noticeRecordToUpdate.ProtestMoney = newNotice.ProtestMoney;
                        noticeRecordToUpdate.BankGurantee = newNotice.BankGurantee;
                        noticeRecordToUpdate.Provisionalamt = newNotice.Provisionalamt;
                        noticeRecordToUpdate.RecoveryAmount = newNotice.RecoveryAmount;
                        noticeRecordToUpdate.Remark = newNotice.Remark;
                        noticeRecordToUpdate.RiskTypeID = newNotice.RiskTypeID;
                        //  noticeRecordToUpdate.FinancialYear = newNotice.FinancialYear;

                        if (IsNoticeDate == CustomerID)
                        {
                            noticeRecordToUpdate.NoticeDate = newNotice.NoticeDate;
                        }
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }


        public static bool UpdateCaseType(long caseInstanceID, int caseTypeID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var caseRecordToUpdate = (from row in entities.tbl_LegalCaseInstance
                                              where row.ID == caseInstanceID
                                              select row).FirstOrDefault();

                    if (caseRecordToUpdate != null)
                    {
                        caseRecordToUpdate.CaseCategoryID = caseTypeID;
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }



        public static void SaveCustomeFieldData(tbl_CustomField newFieldAdd)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_CustomField.Add(newFieldAdd);
                entities.SaveChanges();
            }
        }



        public static bool IsExistCustomeField(tbl_CustomField newFieldAdd)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var ListcustFild = (from row in Entities.tbl_CustomField
                                    where row.CaseNoticeCategory == newFieldAdd.CaseNoticeCategory
                                    && row.Label == newFieldAdd.Label
                                    && row.IsActive == true
                                    select row).FirstOrDefault();

                if (ListcustFild != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void UpdateCustomeFieldData(tbl_CustomField newFieldAdd)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var ListcustFild = (from row in Entities.tbl_CustomField
                                    where row.ID == newFieldAdd.ID
                                    && row.IsActive == true
                                    select row).FirstOrDefault();

                if (ListcustFild != null)
                {
                    ListcustFild.CaseNoticeCategory = newFieldAdd.CaseNoticeCategory;
                    ListcustFild.Label = newFieldAdd.Label;
                    ListcustFild.UpdatedBy = newFieldAdd.UpdatedBy;
                    ListcustFild.UpdatedOn = DateTime.Now;

                    Entities.SaveChanges();
                }
            }
        }

        public static bool DeleteCaseByID(int caseInstanceID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var queryResult = (from row in entities.tbl_LegalCaseInstance
                                       where row.ID == caseInstanceID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteCaseByID(int caseInstanceID, int deletedByUserID)
        {
            try
            {
                bool deleteSuccess = false;
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    List<tbl_LitigationFileData> CaseDocToDelete = entities.tbl_LitigationFileData
                                                                .Where(x => x.NoticeCaseInstanceID == caseInstanceID
                                                                && (x.DocType.Trim() == "C" || x.DocType.Trim() == "CT" || x.DocType.Trim() == "CH")).ToList();

                    if (CaseDocToDelete.Count > 0)
                    {
                        CaseDocToDelete.ForEach(entry => entry.IsDeleted = true);
                        CaseDocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        CaseDocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<tbl_LegalCaseResponse> ResponsesToDelete = entities.tbl_LegalCaseResponse.Where(x => x.CaseInstanceID == caseInstanceID).ToList();

                    if (ResponsesToDelete.Count > 0)
                    {
                        ResponsesToDelete.ForEach(entry => entry.IsActive = false);
                        ResponsesToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        ResponsesToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    List<tbl_TaskScheduleOn> tasksToDelete = entities.tbl_TaskScheduleOn
                                                            .Where(x => x.NoticeCaseInstanceID == caseInstanceID
                                                            && x.TaskType == "C").ToList();

                    if (tasksToDelete.Count > 0)
                    {
                        tasksToDelete.ForEach(entry => entry.IsActive = false);
                        tasksToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        tasksToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    var queryRating = (from row in entities.tbl_LawyerListRating
                                       where row.CaseNoticeID == caseInstanceID
                                       && row.Type == 1
                                       select row).ToList();

                    if (queryRating.Count > 0)
                    {
                        queryRating.ForEach(entry => entry.IsActive = false);
                        queryRating.ForEach(entry => entry.UpdatedBy = deletedByUserID);
                        queryRating.ForEach(entry => entry.UpdatedOn = DateTime.Now);
                        entities.SaveChanges();

                        queryRating.ForEach(eachRatingRecord =>
                        {
                            var LawyerRating = (from row in entities.sp_LiGetLawyerRatingAVG(Convert.ToInt32(eachRatingRecord.LawyerID))
                                                select row).FirstOrDefault();

                            if (!string.IsNullOrEmpty(Convert.ToString(LawyerRating)))
                            {
                                tbl_LawyerFinalRating objNewRate = new tbl_LawyerFinalRating()
                                {
                                    LawyerID = Convert.ToInt32(eachRatingRecord.LawyerID),
                                    Rating = LawyerRating,
                                    IsActive = true
                                };

                                var checkUser = (from row in entities.tbl_LawyerFinalRating
                                                 where row.LawyerID == objNewRate.LawyerID
                                                 && row.IsActive == true
                                                 select row).FirstOrDefault();
                                if (checkUser == null)
                                {
                                    objNewRate.CreatedBy = deletedByUserID;
                                    objNewRate.CreatedOn = DateTime.Now;
                                    entities.tbl_LawyerFinalRating.Add(objNewRate);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkUser.Rating = objNewRate.Rating;
                                    checkUser.UpdatedBy = objNewRate.UpdatedBy;
                                    checkUser.UpdatedOn = objNewRate.UpdatedOn;
                                    entities.SaveChanges();
                                }
                            }
                        });
                    }



                    var queryResult = (from row in entities.tbl_LegalCaseInstance
                                       where row.ID == caseInstanceID
                                       select row).FirstOrDefault();

                    if (queryResult != null)
                    {
                        queryResult.IsDeleted = true;
                        entities.SaveChanges();
                        deleteSuccess = true;
                    }
                    else
                        deleteSuccess = false;

                    return deleteSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_CustomField> GetCustomsFieldsByCaseType(int customerID, int catType)
        {
            List<tbl_CustomField> customParametersTypeWise = new List<tbl_CustomField>();
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    customParametersTypeWise = (from row in entities.tbl_CustomField
                                                where row.CaseNoticeCategory == catType
                                                && row.CustomerID == customerID
                                                && row.IsActive == true
                                                select row).ToList();
                    return customParametersTypeWise;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return customParametersTypeWise;
            }
        }

        public static List<SP_Litigation_GetCustomParameters_Result> GetCustomsFields(int customerID, long noticeOrCaseInstanceID, int caseOrNotice, int catType)
        {
            List<SP_Litigation_GetCustomParameters_Result> customParametersCaseNoticeWise = new List<SP_Litigation_GetCustomParameters_Result>();
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    customParametersCaseNoticeWise = (from row in entities.SP_Litigation_GetCustomParameters((int)catType, (int)Convert.ToInt32(noticeOrCaseInstanceID), (int)caseOrNotice, customerID)
                                                      select row).ToList();

                    return customParametersCaseNoticeWise;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return customParametersCaseNoticeWise;
            }
        }

        public static bool DeleteCustomsFieldByCaseID(int noticeOrCase, long caseNoticeInstanceID, int labelID)
        {
            try
            {
                using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
                {
                    var QueryResult = (from row in Entities.tbl_NoticeCaseCustomParameter
                                       where row.LabelID == labelID
                                       && row.NoticeCaseInstanceID == caseNoticeInstanceID
                                       && row.NoticeCaseType == noticeOrCase
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                       select row).FirstOrDefault();

                    if (QueryResult != null)
                    {
                        QueryResult.IsActive = false;
                        QueryResult.IsDeleted = true;
                        Entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActiveExistingeCustomsFieldByCaseID(int noticeOrCase, long caseNoticeInstanceID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var QueryResult = (from row in entities.tbl_NoticeCaseCustomParameter
                                       where row.NoticeCaseInstanceID == caseNoticeInstanceID
                                       && row.NoticeCaseType == noticeOrCase
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                       select row).ToList();

                    if (QueryResult.Count > 0)
                    {
                        QueryResult.ForEach(entry => entry.IsActive = false);
                        QueryResult.ForEach(entry => entry.IsDeleted = true);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<View_CaseAssignedInstance> GetAssignedCaseList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int partyID, int deptID, int caseStatus, string caseType) /*int branchID,*/
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.View_CaseAssignedInstance
                             where row.CustomerID == customerID
                             select row).ToList();

                //if (branchID != -1)
                //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (caseStatus != -1)
                {
                    //if (caseStatus == 3) //3--Closed otherwise Open
                    //    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                    //else
                    //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                }

                if (caseType != "" && caseType != "B")//--Both(B) --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.CaseType == caseType).ToList();

                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
                if (isexist != null)
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                    if (loggedInUserRole != "CADMN")
                    {
                        var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 2);
                        if (caseList.Count > 0)
                        {
                            query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.CaseInstanceID))).ToList();
                        }
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                    }
                }



                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.CaseInstanceID,
                                 g.CaseRefNo,
                                 g.CaseTitle,
                                 g.CaseCategoryID,
                                 g.CaseDetailDesc,
                                 g.CaseType,
                                 g.CaseTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CaseStageID,
                                 g.CaseStage,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn,
                                 g.FYName,
                                 g.ClaimAmt,
                                 g.RecoveryAmount

                             } into GCS
                             select new View_CaseAssignedInstance()
                             {
                                 CaseInstanceID = GCS.Key.CaseInstanceID,
                                 CaseRefNo = GCS.Key.CaseRefNo,
                                 CaseTitle = GCS.Key.CaseTitle,
                                 CaseCategoryID = GCS.Key.CaseCategoryID,
                                 CaseDetailDesc = GCS.Key.CaseDetailDesc,
                                 CaseType = GCS.Key.CaseType,
                                 CaseTypeName = GCS.Key.CaseTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CaseStageID = GCS.Key.CaseStageID,
                                 CaseStage = GCS.Key.CaseStage,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn,
                                 FYName = GCS.Key.FYName,
                                 ClaimAmt = GCS.Key.ClaimAmt,
                                 RecoveryAmount = GCS.Key.RecoveryAmount,
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.CaseType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }
        //public static List<View_CaseAssignedInstance> GetAssignedCaseList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int partyID, int deptID, int caseStatus, string caseType) /*int branchID,*/
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var query = (from row in entities.View_CaseAssignedInstance
        //                     where row.CustomerID == customerID
        //                     select row).ToList();

        //        //if (branchID != -1)
        //        //    query = query.Where(entry => entry.CustomerBranchID == branchID).ToList();

        //        if (branchList.Count > 0)
        //            query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

        //        if (partyID != -1)
        //            query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

        //        if (deptID != -1)
        //            query = query.Where(entry => entry.DepartmentID == deptID).ToList();

        //        if (caseStatus != -1)
        //        {
        //            //if (caseStatus == 3) //3--Closed otherwise Open
        //            //    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
        //            //else
        //            //    query = query.Where(entry => entry.TxnStatusID < 3).ToList();
        //            query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
        //        }

        //        if (caseType != "" && caseType != "B")//--Both(B) --Inward(I) and Outward(O)
        //            query = query.Where(entry => entry.CaseType == caseType).ToList();

        //        ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(customerID));
        //        if (isexist != null)
        //        {
        //            query = query.Where(entry => entry.RoleID == 3).ToList();
        //            if (loggedInUserRole != "CADMN")
        //            {
        //                var caseList = CustomerBranchManagement.GetAssignedLocationListLitigationRahulbyuser(Convert.ToInt32(customerID), loggedInUserID, "", 2);
        //                if (caseList.Count > 0)
        //                {
        //                    query = query.Where(entry => caseList.Contains(Convert.ToInt32(entry.CaseInstanceID))).ToList();
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
        //                query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
        //            else // In case of MGMT or CADMN 
        //            {
        //                query = query.Where(entry => entry.RoleID == 3).ToList();
        //            }
        //        }



        //        if (query.Count > 0)
        //        {
        //            query = (from g in query
        //                     group g by new
        //                     {
        //                         g.CaseInstanceID,
        //                         g.CaseRefNo,
        //                         g.CaseTitle,
        //                         g.CaseCategoryID,
        //                         g.CaseDetailDesc,
        //                         g.CaseType,
        //                         g.CaseTypeName,
        //                         g.CustomerID,
        //                         g.CustomerBranchID,
        //                         g.BranchName,
        //                         g.DepartmentID,
        //                         g.DeptName,
        //                         g.PartyID,
        //                         g.PartyName,
        //                         g.CaseStageID,
        //                         g.CaseStage,
        //                         g.CreatedOn,
        //                         g.OwnerID,
        //                         g.UpdatedOn,
        //                         g.FYName

        //                     } into GCS
        //                     select new View_CaseAssignedInstance()
        //                     {
        //                         CaseInstanceID = GCS.Key.CaseInstanceID,
        //                         CaseRefNo = GCS.Key.CaseRefNo,
        //                         CaseTitle = GCS.Key.CaseTitle,
        //                         CaseCategoryID = GCS.Key.CaseCategoryID,
        //                         CaseDetailDesc = GCS.Key.CaseDetailDesc,
        //                         CaseType = GCS.Key.CaseType,
        //                         CaseTypeName = GCS.Key.CaseTypeName,
        //                         CustomerID = GCS.Key.CustomerID,
        //                         CustomerBranchID = GCS.Key.CustomerBranchID,
        //                         BranchName = GCS.Key.BranchName,
        //                         DepartmentID = GCS.Key.DepartmentID,
        //                         DeptName = GCS.Key.DeptName,
        //                         PartyID = GCS.Key.PartyID,
        //                         PartyName = GCS.Key.PartyName,
        //                         CaseStageID = GCS.Key.CaseStageID,
        //                         CaseStage = GCS.Key.CaseStage,
        //                         CreatedOn = GCS.Key.CreatedOn,
        //                         OwnerID = GCS.Key.OwnerID,
        //                         UpdatedOn = GCS.Key.UpdatedOn,
        //                         FYName = GCS.Key.FYName
        //                     }).ToList();
        //        }

        //        if (query.Count > 0)
        //        {
        //            query = query.OrderByDescending(entry => entry.UpdatedOn)
        //                .ThenByDescending(entry => entry.CreatedOn).ToList();
        //            //query = query.OrderBy(entry => entry.CaseType)
        //            //    .ThenBy(entry => entry.CustomerBranchID).ToList();
        //        }

        //        return query.ToList();
        //    }
        //}




        public static List<long> GetLinkedCaseNoticeIDs(int customerID, int caseOrNoticeType, long noticeOrCaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_NoticeCaseLinking
                             where row.Type == caseOrNoticeType
                             && row.NoticeOrCaseInstanceID == noticeOrCaseInstanceID
                             && row.IsActive == true
                             select row.LinkedNoticeOrCaseInstanceID).ToList();

                return query.ToList();
            }
        }

        public static int GetAssignedCaseCount(List<View_CaseAssignedInstance> masterRecords, int loggedInUserID, string loggedInUserRole, int roleID, int caseStatus, int custID)
        {
            //string IsEntityassignmentCustomer = ConfigurationManager.AppSettings["IsEntityLocationAssignment"].ToString();
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in masterRecords
                             select row).ToList();

                var assignentity = (from row in entities.LitigationEntitiesAssignments
                                    where row.UserID == loggedInUserID
                                    select row.BranchID).ToList();

                if (caseStatus == 3) //3--Closed otherwise Open
                    query = query.Where(entry => entry.TxnStatusID == caseStatus).ToList();
                else
                    query = query.Where(entry => entry.TxnStatusID < 3).ToList();

                ClientCustomization isexist = UserManagement.GetAssignedEntity("AssignedEntity", Convert.ToInt32(custID));
                if (isexist != null)
                {
                    if (loggedInUserRole != "CADMN")
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                        query = query.Where(entry => (entry.UserID == loggedInUserID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID) || assignentity.Contains(entry.CustomerBranchID)).ToList();
                    }
                }
                else
                {
                    if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN")
                        query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.CaseCreatedBy == loggedInUserID)).ToList();
                    else // In case of MGMT or CADMN 
                    {
                        query = query.Where(entry => entry.RoleID == 3).ToList();
                    }
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.CaseInstanceID,
                                 g.CaseRefNo,
                                 g.CaseTitle,
                                 g.CaseDetailDesc,
                                 g.CaseType,
                                 g.CaseTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName
                             } into GCS
                             select new View_CaseAssignedInstance()
                             {
                                 CaseInstanceID = GCS.Key.CaseInstanceID,
                                 CaseTitle = GCS.Key.CaseTitle,
                                 CaseDetailDesc = GCS.Key.CaseDetailDesc,
                                 CaseTypeName = GCS.Key.CaseTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                             }).ToList();
                }

                return query.Count();
            }
        }

        public static List<View_CaseAssignedInstance> GetCaseDetailList()
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.View_CaseAssignedInstance
                             select row).ToList();

                return query.ToList();
            }
        }

        public static tbl_LegalCaseInstance GetCaseByID(int CaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.tbl_LegalCaseInstance
                             where row.ID == CaseInstanceID
                             select row).FirstOrDefault();
                return Query;
            }
        }

        public static bool UpdateCaseStage(long caseInstanceID, int caseStageID, int loggedInUserID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var noticeRecordToUpdate = (from row in entities.tbl_LegalCaseInstance
                                                where row.ID == caseInstanceID
                                                select row).FirstOrDefault();

                    if (noticeRecordToUpdate != null)
                    {
                        noticeRecordToUpdate.CaseStageID = caseStageID;
                        noticeRecordToUpdate.UpdatedBy = loggedInUserID;
                        noticeRecordToUpdate.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        #endregion

        #region Case-Document

        public static int ExistsCaseDocumentReturnVersion(tbl_LitigationFileData newDocumentRecord)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LitigationFileData
                             where row.FileName == newDocumentRecord.FileName
                             && row.NoticeCaseInstanceID == newDocumentRecord.NoticeCaseInstanceID
                             && row.DocType.Trim() == newDocumentRecord.DocType
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }

        public static int ExistsCaseAdvocateBillDocumentReturnVersion(tbl_LitigationFileData newDocumentRecord)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LitigationFileData
                             where row.FileName == newDocumentRecord.FileName
                             && row.NoticeCaseInstanceID == newDocumentRecord.NoticeCaseInstanceID
                             && row.DocTypeInstanceID==newDocumentRecord.DocTypeInstanceID
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }

        public static int ExistsCasePaymentDocumentReturnVersion(tbl_LitigationPaymentFileData newDocumentRecord)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LitigationPaymentFileData
                             where row.FileName == newDocumentRecord.FileName
                             && row.NoticeCaseInstanceID == newDocumentRecord.NoticeCaseInstanceID
                             && row.DocType.Trim() == newDocumentRecord.DocType
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }
        public static bool CreateCaseAdvocateBillDocumentMapping(tbl_LitigationFileData newNoticeDoc)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    newNoticeDoc.EnType = "A";
                    entities.tbl_LitigationFileData.Add(newNoticeDoc);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool CreateCaseDocumentMapping(tbl_LitigationFileData newNoticeDoc)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    newNoticeDoc.EnType = "A";
                    entities.tbl_LitigationFileData.Add(newNoticeDoc);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistsCaseDocumentMapping(tbl_LitigationFileData ICR)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LitigationFileData
                             where row.FileName == ICR.FileName
                             && row.IsDeleted == false
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        public static List<Sp_Litigation_CaseDocument_Result> GetCaseDocumentMapping(int caseInstanceID, string docType, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.Sp_Litigation_CaseDocument(caseInstanceID, CustomerID)
                                   select row).ToList();

                if (queryResult.Count > 0)
                {
                    if (docType != "")
                        queryResult = queryResult.Where(row => row.DocType.Trim() == docType).ToList();
                    else
                        queryResult = queryResult.Where(row => (row.DocType.Trim() == "C") || (row.DocType.Trim() == "CH") || (row.DocType.Trim() == "CT") || (row.DocType.Trim() == "CO") || (row.DocType.Trim() == "CD")).ToList();

                    //C-Case, CH-Case Hearing, CO-CaseOrder, CD-Compliance Document
                }

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderBy(entry => entry.FileName).ThenByDescending(entry => entry.CreatedOn).ToList();

                return queryResult;
            }
        }
        public static tbl_LitigationPaymentFileData GetCasePaymentDocumentByID(int PaymentFileID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_LitigationPaymentFileData file = entities.tbl_LitigationPaymentFileData.Where(x => x.NoticeCasePaymentId == PaymentFileID).FirstOrDefault();
                return file;
            }
        }
        public static tbl_LitigationFileData GetCaseDocumentByID(int noticeFileID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_LitigationFileData file = entities.tbl_LitigationFileData.Where(x => x.ID == noticeFileID).FirstOrDefault();
                return file;
            }
        }
        public static tbl_LitigationFileData GetCaseAdvBillDocumentByID(int AdvbillFileID,int casedocumentid)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_LitigationFileData file = entities.tbl_LitigationFileData.Where(x => x.DocTypeInstanceID == AdvbillFileID && x.DocType=="CA" && x.ID==casedocumentid).FirstOrDefault();
                return file;
            }
        }
        public static bool DeleteCaseDocument(int noticeFileID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    tbl_LitigationFileData fileToDelete = entities.tbl_LitigationFileData.Where(x => x.ID == noticeFileID).FirstOrDefault();

                    if (fileToDelete != null)
                    {
                        fileToDelete.IsDeleted = true;
                        fileToDelete.DeletedBy = deletedByUserID;
                        fileToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Case-Lawyer
        public static bool CreateCaseLawyerMapping(tbl_LegalCaseLawyerMapping lstNoticeLawyer)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    lstNoticeLawyer.CreatedOn = DateTime.Now;
                    lstNoticeLawyer.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalCaseLawyerMapping.Add(lstNoticeLawyer);
                    entities.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool UpdateCaseLawyerMapping(tbl_LegalCaseLawyerMapping lstNoticeLawyer)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    lstNoticeLawyer.CreatedOn = DateTime.Now;
                    lstNoticeLawyer.UpdatedOn = DateTime.Now;

                    //Check Notice-Lawyer Mapping Exist Previously or not
                    var noticeLawyerMappingRecord = (from row in entities.tbl_LegalCaseLawyerMapping
                                                     where row.CaseInstanceID == lstNoticeLawyer.CaseInstanceID
                                                     && row.LawyerID == lstNoticeLawyer.LawyerID
                                                     select row).FirstOrDefault();

                    if (noticeLawyerMappingRecord != null)
                    {
                        noticeLawyerMappingRecord.IsActive = true;
                        noticeLawyerMappingRecord.UpdatedOn = lstNoticeLawyer.UpdatedOn;
                        noticeLawyerMappingRecord.UpdatedBy = lstNoticeLawyer.UpdatedBy;
                    }
                    else
                        entities.tbl_LegalCaseLawyerMapping.Add(lstNoticeLawyer);

                    entities.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DeActiveExistingCaseLawyerMapping(long CaseInstanceID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var queryResult = (from row in entities.tbl_LegalCaseLawyerMapping
                                       where row.CaseInstanceID == CaseInstanceID
                                       && row.IsActive == true
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static tbl_LegalCaseLawyerMapping GetCaseLawyerMapping(int CaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalCaseLawyerMapping
                                   where row.CaseInstanceID == CaseInstanceID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();
                return queryResult;
            }
        }

        #endregion

        #region Case-UserAssignement

        public static List<SP_Litigation_NoticeCaseUserAssigned_Result> GetCaseUserAssignments(int customerID, long caseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.SP_Litigation_NoticeCaseUserAssigned(customerID, caseInstanceID, "C")
                                   select row).ToList();

                return queryResult;
            }
        }

        public static List<int> GetLitigationAssignedRoles(int Userid)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstAssignedRoles = (from row in entities.tbl_LegalNoticeAssignment
                                        where row.UserID == Userid
                                        && row.IsActive == true
                                        select row.RoleID).Distinct().ToList();

                return lstAssignedRoles;
            }
        }

        public static bool CreateCaseAssignment(tbl_LegalCaseAssignment objNewAssignmentRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNewAssignmentRecord.CreatedOn = DateTime.Now;
                    objNewAssignmentRecord.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalCaseAssignment.Add(objNewAssignmentRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistCaseAssignment(tbl_LegalCaseAssignment objNewAssignmentRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseAssignment
                                  where row.CaseInstanceID == objNewAssignmentRecord.CaseInstanceID
                                  && row.UserID == objNewAssignmentRecord.UserID
                                  && row.RoleID == objNewAssignmentRecord.RoleID
                                  //&& row.IsActive == true
                                  select row).FirstOrDefault();
                    if (record != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_LegalCaseAssignment> GetCaseAssignment(int CaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalCaseAssignment
                                   where row.CaseInstanceID == CaseInstanceID
                                   && row.IsActive == true
                                   select row).ToList();
                return queryResult;
            }
        }

        public static bool DeleteCaseUserAssignment(int assignmentRecordID, long caseInstanceID)
        {
            bool saveSuccess = false;
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var queryResult = (from row in entities.tbl_LegalCaseAssignment
                                       where row.CaseInstanceID == caseInstanceID
                                       && row.ID == assignmentRecordID
                                       select row).FirstOrDefault();
                    if (queryResult != null)
                    {
                        queryResult.IsActive = false;
                        entities.SaveChanges();
                        saveSuccess = true;
                    }
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        public static bool DeActiveCaseAssignments(long caseInstanceID, int loggedInUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseAssignment
                                  where row.CaseInstanceID == caseInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = loggedInUserID);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateCaseAssignments(tbl_LegalCaseAssignment objNewAssignmentRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseAssignment
                                  where row.CaseInstanceID == objNewAssignmentRecord.CaseInstanceID
                                  && row.UserID == objNewAssignmentRecord.UserID
                                  && row.RoleID == objNewAssignmentRecord.RoleID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = true);
                        record.ForEach(entry => entry.UpdatedBy = objNewAssignmentRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdateCaseAssignment(tbl_LegalCaseAssignment objNewAssignmentRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var prevAssignmentRecord = (from row in entities.tbl_LegalCaseAssignment
                                                where row.CaseInstanceID == objNewAssignmentRecord.CaseInstanceID
                                                && row.UserID == objNewAssignmentRecord.UserID
                                                && row.RoleID == objNewAssignmentRecord.RoleID
                                                select row).FirstOrDefault();

                    if (prevAssignmentRecord != null)
                    {
                        prevAssignmentRecord.IsActive = true;
                        prevAssignmentRecord.UpdatedBy = objNewAssignmentRecord.CreatedBy;
                        prevAssignmentRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        objNewAssignmentRecord.CreatedOn = DateTime.Now;
                        objNewAssignmentRecord.UpdatedOn = DateTime.Now;
                        entities.tbl_LegalCaseAssignment.Add(objNewAssignmentRecord);
                        saveSuccess = true;
                    }
                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        #endregion

        #region Case-Action-Schedule
        public static List<tbl_LegalCaseScheduleOn> GetCaseActionDetails(int CaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalCaseScheduleOn
                                   where row.CaseInstanceID == CaseInstanceID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }

        public static bool CreateCaseNextActionLog(tbl_LegalCaseScheduleOn objNextActionRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNextActionRecord.CreatedOn = DateTime.Now;

                    entities.tbl_LegalCaseScheduleOn.Add(objNextActionRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteCaseNextActionLog(int CaseScheduleOnID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    tbl_LegalCaseScheduleOn ActionLogToDelete = entities.tbl_LegalCaseScheduleOn.Where(x => x.ID == CaseScheduleOnID).FirstOrDefault();

                    if (ActionLogToDelete != null)
                    {
                        ActionLogToDelete.IsActive = false;
                        ActionLogToDelete.DeletedBy = deletedByUserID;
                        ActionLogToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Case-Response

        public static bool ExistsCaseHearingDetails(long caseInstanceID, long hearingRefID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_CaseHearingRef
                                   where row.CaseNoticeInstanceID == caseInstanceID
                                   && row.ID == hearingRefID
                                   && row.IsDeleted == false
                                   select row).FirstOrDefault();

                if (queryResult != null)
                    return true;
                else
                    return false;
            }
        }


        public static List<View_LegalCaseResponse> GetCaseResponseDetails(int CaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.View_LegalCaseResponse
                                   where row.CaseInstanceID == CaseInstanceID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;

                //var queryResult = (from row in entities.tbl_LegalCaseResponse
                //                   where row.CaseInstanceID == CaseInstanceID
                //                   && row.IsActive == true
                //                   select row).ToList();

                //return queryResult;
            }
        }

        public static long CreateCaseResponseLog(tbl_LegalCaseResponse objNextActionRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNextActionRecord.CreatedOn = DateTime.Now;

                    entities.tbl_LegalCaseResponse.Add(objNextActionRecord);
                    entities.SaveChanges();

                    return objNextActionRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool DeleteCaseResponseLog(long caseInstanceID, int caseResponseID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    //Delete Case Hearing Related Document
                    List<tbl_LitigationFileData> ResponseRelatedDocToDelete = entities.tbl_LitigationFileData
                                                                              .Where(x => x.DocTypeInstanceID == caseResponseID
                                                                              && x.NoticeCaseInstanceID == caseInstanceID
                                                                              && x.DocType.Trim() == "CH").ToList();

                    if (ResponseRelatedDocToDelete.Count > 0)
                    {
                        ResponseRelatedDocToDelete.ForEach(entry => entry.IsDeleted = true);
                        ResponseRelatedDocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        ResponseRelatedDocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    tbl_LegalCaseResponse ResponseLogToDelete = entities.tbl_LegalCaseResponse.Where(x => x.ID == caseResponseID).FirstOrDefault();

                    if (ResponseLogToDelete != null)
                    {
                        ResponseLogToDelete.IsActive = false;
                        ResponseLogToDelete.DeletedBy = deletedByUserID;
                        ResponseLogToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_LitigationFileData> GetCaseAdvBillDocuments(long advbillID, long caseInstanceID, string docType)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == caseInstanceID
                                   && row.DocTypeInstanceID==advbillID
                                   && row.DocType.Trim() == docType
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }
        public static List<tbl_LitigationFileData> GetCaseResponseDocuments(long caseInstanceID, long responseID, string docType)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == caseInstanceID
                                   && row.DocTypeInstanceID == responseID
                                   && row.DocType.Trim() == docType
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        #endregion

        #region Case Status-Transaction

        public static bool CreateCaseStatusTransaction(tbl_LegalCaseStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    objNewStatusRecord.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalCaseStatusTransaction.Add(objNewStatusRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistCaseStatusTransaction(tbl_LegalCaseStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseStatusTransaction
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.CaseInstanceID == objNewStatusRecord.CaseInstanceID
                                  && row.StatusID == objNewStatusRecord.StatusID
                                  //&& row.UserID == objNewStatusRecord.UserID
                                  //&& row.RoleID == objNewStatusRecord.RoleID
                                  select row).FirstOrDefault();
                    if (record != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateCaseStatusTransaction(tbl_LegalCaseStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseStatusTransaction
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.CaseInstanceID == objNewStatusRecord.CaseInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();

                        var recordToUpdate = record.Where(entry => entry.StatusID == objNewStatusRecord.StatusID).FirstOrDefault();

                        if (recordToUpdate != null)
                        {
                            recordToUpdate.IsActive = true;
                            recordToUpdate.StatusRemark = objNewStatusRecord.StatusRemark;
                            entities.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActiveCaseStatusTransaction(tbl_LegalCaseStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseStatusTransaction
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.CaseInstanceID == objNewStatusRecord.CaseInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Case Status

        public static bool CreateCaseStatus(tbl_LegalCaseStatus objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    objNewStatusRecord.UpdatedOn = DateTime.Now;

                    entities.tbl_LegalCaseStatus.Add(objNewStatusRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool ExistCaseStatus(tbl_LegalCaseStatus objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseStatus
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.CaseInstanceID == objNewStatusRecord.CaseInstanceID
                                  //&& row.StatusID == objNewStatusRecord.StatusID
                                  select row).FirstOrDefault();
                    if (record != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool UpdateCaseStatus(tbl_LegalCaseStatus objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseStatus
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.CaseInstanceID == objNewStatusRecord.CaseInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();

                        var recordToUpdate = record.Where(entry => entry.StatusID == objNewStatusRecord.StatusID).FirstOrDefault();

                        if (recordToUpdate != null)
                        {
                            recordToUpdate.IsActive = true;
                            recordToUpdate.CloseDate = objNewStatusRecord.CloseDate;
                            recordToUpdate.ClosureRemark = objNewStatusRecord.ClosureRemark;

                            entities.SaveChanges();
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeActiveNoticeStatus(tbl_LegalNoticeStatus objNewStatusRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalNoticeStatus
                                  where row.IsActive == true
                                  && row.IsDeleted == false
                                  && row.NoticeInstanceID == objNewStatusRecord.NoticeInstanceID
                                  select row).ToList();

                    if (record.Count > 0)
                    {
                        record.ForEach(entry => entry.IsActive = false);
                        record.ForEach(entry => entry.UpdatedBy = objNewStatusRecord.UpdatedBy);
                        record.ForEach(entry => entry.UpdatedOn = DateTime.Now);

                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static View_CaseStatusCloseRemark GetCaseDetails(int caseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.View_CaseStatusCloseRemark
                                   where row.CaseInstanceID == caseInstanceID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        public static View_CaseAssignedInstance GetCaseStatusDetail(int caseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.View_CaseAssignedInstance
                                   where row.CaseInstanceID == caseInstanceID
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        #endregion

        #region Case-Payment
        public static List<tbl_NoticeCasePayment> GetNoticeCasePaymentDetails(int noticeInstanceID, string noticeOrCase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_NoticeCasePayment
                                   where row.NoticeOrCaseInstanceID == noticeInstanceID
                                   && row.NoticeOrCase == noticeOrCase
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }

        public static bool CreateCasePaymentLog(tbl_NoticeCasePayment objPaymentRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objPaymentRecord.CreatedOn = DateTime.Now;

                    entities.tbl_NoticeCasePayment.Add(objPaymentRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteCasePaymentLog(int noticePaymentID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    tbl_NoticeCasePayment PaymentLogToDelete = entities.tbl_NoticeCasePayment.Where(x => x.ID == noticePaymentID).FirstOrDefault();

                    if (PaymentLogToDelete != null)
                    {
                        PaymentLogToDelete.IsActive = false;
                        PaymentLogToDelete.UpdatedBy = deletedByUserID;
                        PaymentLogToDelete.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        #endregion

        #region Case-Order

        public static List<tbl_LegalCaseOrder> GetCaseOrderDetails(int CaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalCaseOrder
                                   where row.CaseInstanceID == CaseInstanceID
                                   && row.IsActive == true
                                   select row).ToList();

                return queryResult;
            }
        }

        public static long CreateCaseOrderLog(tbl_LegalCaseOrder objRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objRecord.CreatedOn = DateTime.Now;

                    entities.tbl_LegalCaseOrder.Add(objRecord);
                    entities.SaveChanges();

                    return objRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static bool DeleteCaseOrderLog(long caseInstanceID, int CaseOrderID, int deletedByUserID, string docType)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    List<tbl_LitigationFileData> DocToDelete = entities.tbl_LitigationFileData
                                                                              .Where(x => x.DocTypeInstanceID == CaseOrderID
                                                                              && x.NoticeCaseInstanceID == caseInstanceID
                                                                              && x.DocType.Trim() == docType).ToList();

                    if (DocToDelete.Count > 0)
                    {
                        DocToDelete.ForEach(entry => entry.IsDeleted = true);
                        DocToDelete.ForEach(entry => entry.DeletedBy = deletedByUserID);
                        DocToDelete.ForEach(entry => entry.DeletedOn = DateTime.Now);

                        entities.SaveChanges();
                    }

                    tbl_LegalCaseOrder OrderLogToDelete = entities.tbl_LegalCaseOrder.Where(x => x.ID == CaseOrderID).FirstOrDefault();

                    if (OrderLogToDelete != null)
                    {
                        OrderLogToDelete.IsActive = false;
                        OrderLogToDelete.DeletedBy = deletedByUserID;
                        OrderLogToDelete.DeletedOn = DateTime.Now;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static List<LitigationPaymentForm> GetCasePaymentDocuments(long caseInstanceID, long invoiceno)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.LitigationPaymentForms
                                   where row.NoticeCaseInstanceID == caseInstanceID
                                   && row.InvoiceNo == invoiceno
                                   //&& row.DocType.Trim() == docType
                                   //&& row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }
        public static List<tbl_LitigationFileData> GetCaseadvbillDocuments(long caseInstanceID, long advbillID, string docType)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == caseInstanceID
                                   && row.DocTypeInstanceID == advbillID
                                   && row.DocType.Trim() == docType
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }
        public static List<tbl_LitigationFileData> GetCaseOrderDocuments(long caseInstanceID, long orderID, string docType)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LitigationFileData
                                   where row.NoticeCaseInstanceID == caseInstanceID
                                   && row.DocTypeInstanceID == orderID
                                   && row.DocType.Trim() == docType
                                   && row.IsDeleted == false
                                   select row).ToList();

                return queryResult;
            }
        }

        public static string GetOrderTypeByID(int orderTypeID, string typeCode)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_TypeMaster
                                   where row.TypeCode == typeCode
                                   && row.ID == orderTypeID
                                   select row).FirstOrDefault();

                if (queryResult != null)
                    return queryResult.TypeName;
                else
                    return "";
            }
        }

        public static bool ExistsCaseOrder(string orderTitle, long caseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalCaseOrder
                             where row.IsActive == true
                             && row.OrderTitle.ToUpper().Trim().Equals(orderTitle.ToUpper().Trim())
                             select row);

                if (caseInstanceID != 0)
                {
                    query = query.Where(entry => entry.CaseInstanceID != caseInstanceID);
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        #endregion
        //public static bool ExistsAdvocateBill(long CaseInstanceID, string hearingref)
        //{
        //    bool savesuccess = false;
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        string[] values = hearingref.Split(',');
        //        foreach(var item in values)
        //        {
        //            var query = (from row in entities.tbl_CaseAdvocateBill
        //                         where row.IsActive == true && row.NoticeCaseInstanceID == CaseInstanceID
        //                         && row.HearingRef.ToUpper().Trim().Contains(item.ToUpper().Trim())
        //                         select row).ToList();


        //            if (query.Count > 0)
        //            {
        //                savesuccess = true;
        //            }
        //        }

        //        return savesuccess;
        //    }
        //}

        public static bool ExistsAdvocateBill(long CaseInstanceID, string InvoiceNo)
        {
            bool savesuccess = false;
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_CaseAdvocateBill
                             where row.IsActive == true && row.NoticeCaseInstanceID == CaseInstanceID
                             && row.InvoiceNo.ToUpper().Trim() == InvoiceNo.ToUpper().Trim()
                             select row).ToList();


                if (query.Count > 0)
                {
                    savesuccess = true;
                }

                return savesuccess;
            }
        }
        #region Case-Report

        //public static List<View_LitigationCaseReport> GetGridPopupCaseData(int NoticeCaseInstanceID)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        var AllDocument = (from row in entities.View_LitigationCaseReport
        //                           where row.NoticeCaseInstanceID == NoticeCaseInstanceID
        //                           select row).ToList();
        //        return AllDocument;
        //    }
        //}

        #endregion

        #region

        public static List<tbl_CaseHearingRef> GetAllRefNo(long customerID, long caseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstRefNo = (from row in entities.tbl_CaseHearingRef
                                where row.HearingRefNo != null
                                && row.IsDeleted == false
                                && row.CustomerID == customerID
                                && row.CaseNoticeInstanceID == caseInstanceID
                                select row);

                return lstRefNo.ToList();
            }
        }
        public static List<tbl_CaseHearingRef> GetAllAdvocateRefNo(long customerID, long caseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstRefNo = (from row in entities.tbl_CaseHearingRef
                                join row1 in entities.tbl_LegalCaseResponse
                                on row.ID equals row1.RefID
                                where row.HearingRefNo != null
                                && row1.IsActive == true
                                && row.CustomerID == customerID
                                && row.CaseNoticeInstanceID == caseInstanceID
                               
                                select row);
               

                return lstRefNo.Distinct().ToList();
            }
        }
        public static tbl_CaseHearingRef GetRefNoDetail(long refID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objRefDetail = (from row in entities.tbl_CaseHearingRef
                                    where row.ID == refID
                                    select row).FirstOrDefault();

                return objRefDetail;
            }
        }

        public static bool ExistsRefNo(tbl_CaseHearingRef obj)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objCourt = (from row in entities.tbl_CaseHearingRef
                                where row.CaseNoticeInstanceID == obj.CaseNoticeInstanceID
                                && row.CustomerID == obj.CustomerID
                                && row.HearingDate == obj.HearingDate
                                && row.IsDeleted == false
                                select row);

                if (objCourt != null)
                    return true;
                else
                    return false;
            }
        }

        public static void AddFinalratingSpecilasation(tbl_LawyerFinalRating objrating)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objFinalrating = (from row in entities.tbl_LawyerFinalRating
                                      where row.LawyerID == objrating.LawyerID
                                      && row.IsActive == true
                                      select row).FirstOrDefault();
                if (objFinalrating == null)
                {
                    entities.tbl_LawyerFinalRating.Add(objrating);
                    entities.SaveChanges();
                }
                else
                {
                    objFinalrating.Specilisation = objrating.Specilisation;
                    objFinalrating.UpdatedBy = objrating.CreatedBy;
                    objFinalrating.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }

            }
        }

        public static int GetExistsRefNo(tbl_CaseHearingRef obj)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objCourt = (from row in entities.tbl_CaseHearingRef
                                where row.CaseNoticeInstanceID == obj.CaseNoticeInstanceID
                                && row.CustomerID == obj.CustomerID
                                && row.IsDeleted == false
                                select row);

                if (objCourt != null)
                {
                    if (objCourt.Where(entry => entry.HearingDate == obj.HearingDate).Count() > 0)
                        return -1;
                    else
                        return objCourt.Count();
                }
                else
                    return 0;
            }
        }

        public static long CreateNewRefNo(tbl_CaseHearingRef newObj)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    newObj.CreatedOn = DateTime.Now;

                    entities.tbl_CaseHearingRef.Add(newObj);
                    entities.SaveChanges();

                    return newObj.ID;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }

        //public static bool CreatePartyMapping(List<tbl_PartyMapping> lstObjPartyMapping)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        try
        //        {
        //            lstObjPartyMapping.ForEach(eachPartyRecord =>
        //            {
        //                entities.tbl_PartyMapping.Add(eachPartyRecord);
        //            });

        //            entities.SaveChanges();

        //            return true;
        //        }
        //        catch (Exception ex)
        //        {
        //            return false;
        //        }
        //    }
        //}

        public static bool CreateUpdatePartyMapping(List<tbl_PartyMapping> lstObjPartyMapping) //Action-1-Create 2-Update
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                bool saveSuccess = false;
                try
                {
                    lstObjPartyMapping.ForEach(eachPartyRecord =>
                    {
                        var partyMappingExists = (from row in entities.tbl_PartyMapping
                                                  where row.CaseNoticeInstanceID == eachPartyRecord.CaseNoticeInstanceID
                                                  && row.PartyID == eachPartyRecord.PartyID
                                                  //&& row.IsActive == false
                                                  && row.Type == eachPartyRecord.Type
                                                  select row).FirstOrDefault();

                        if (partyMappingExists != null)
                        {
                            partyMappingExists.IsActive = true;
                            partyMappingExists.UpdatedBy = eachPartyRecord.CreatedBy;
                            partyMappingExists.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.tbl_PartyMapping.Add(eachPartyRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DeActiveExistingPartyMapping(long newCaseID, int Type)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var queryResult = (from row in entities.tbl_PartyMapping
                                       where row.CaseNoticeInstanceID == newCaseID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        //public static bool CreateActMapping(List<tbl_ActMapping> lstObjActMapping)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        try
        //        {
        //            lstObjActMapping.ForEach(eachActRecord =>
        //            {
        //                entities.tbl_ActMapping.Add(eachActRecord);
        //            });

        //            entities.SaveChanges();

        //            return true;
        //        }
        //        catch (Exception ex)
        //        {
        //            return false;
        //        }
        //    }
        //}

        public static bool CreateUpdateActMapping(List<tbl_ActMapping> lstObjActMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    bool saveSuccess = false;

                    lstObjActMapping.ForEach(eachActRecord =>
                    {
                        var prevActMappingRecord = (from row in entities.tbl_ActMapping
                                                    where row.CaseNoticeInstanceID == eachActRecord.CaseNoticeInstanceID
                                                    && row.ActID == eachActRecord.ActID
                                                    //&& row.IsActive == false
                                                    && row.Type == eachActRecord.Type
                                                    select row).FirstOrDefault();

                        if (prevActMappingRecord != null)
                        {
                            prevActMappingRecord.IsActive = true;
                            prevActMappingRecord.UpdatedBy = eachActRecord.CreatedBy;
                            prevActMappingRecord.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.tbl_ActMapping.Add(eachActRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        public static bool CreateUpdateFYMapping(List<FinancialYearMapping> lstObjFYMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    bool saveSuccess = false;

                    lstObjFYMapping.ForEach(eachFYRecord =>
                    {
                        var prevFYMappingRecord = (from row in entities.FinancialYearMappings
                                                    where row.CaseNoticeInstanceID == eachFYRecord.CaseNoticeInstanceID
                                                    && row.FYID == eachFYRecord.FYID
                                                    //&& row.IsActive == false
                                                    && row.Type == eachFYRecord.Type
                                                    select row).FirstOrDefault();

                        if (prevFYMappingRecord != null)
                        {
                            prevFYMappingRecord.IsActive = true;
                            prevFYMappingRecord.UpdatedBy = eachFYRecord.CreatedBy;
                            prevFYMappingRecord.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.FinancialYearMappings.Add(eachFYRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }



        public static bool DeActiveExistingFYMapping(long newCaseID, int Type)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.FinancialYearMappings
                                       where row.CaseNoticeInstanceID == newCaseID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool DeActiveExistingActMapping(long newCaseID, int Type)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var queryResult = (from row in entities.tbl_ActMapping
                                       where row.CaseNoticeInstanceID == newCaseID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_ActMapping> GetListOfAct(long noticeOrCaseInstanceID, int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.tbl_ActMapping
                                   where row.CaseNoticeInstanceID == noticeOrCaseInstanceID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   select row);

                return queryResult.ToList();
            }
        }
        public static List<tbl_CaseAdvocateBill> GetListOfHearing(long? noticeOrCaseInstanceID, int advocatebillid)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_CaseAdvocateBill
                                   where row.NoticeCaseInstanceID == noticeOrCaseInstanceID
                                   && row.IsActive == true
                                   && row.ID == advocatebillid
                                   select row);

                return queryResult.ToList();
            }
        }
        public static List<FinancialYearMapping> GetListOfFY(long noticeOrCaseInstanceID, int Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.FinancialYearMappings
                                   where row.CaseNoticeInstanceID == noticeOrCaseInstanceID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   select row);

                return queryResult.ToList();
            }
        }

        public static List<tbl_PartyMapping> GetListOfParty(long noticeOrCaseInstanceID, int Type)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_PartyMapping
                                   where row.CaseNoticeInstanceID == noticeOrCaseInstanceID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   select row);

                return queryResult.ToList();
            }
        }

        public static bool CreateUpdateOppositionLawyerMapping(List<tbl_OppositionLawyerList> lstObjLawyerMapping)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    bool saveSuccess = false;
                    lstObjLawyerMapping.ForEach(eachOppoLaywerRecord =>
                    {
                        var prevRecOppoLawyerMapping = (from row in entities.tbl_OppositionLawyerList
                                                        where row.CaseNoticeInstanceID == eachOppoLaywerRecord.CaseNoticeInstanceID
                                                        && row.LawyerID == eachOppoLaywerRecord.LawyerID
                                                        && row.Type == eachOppoLaywerRecord.Type
                                                        //&& row.IsActive == false                                                        
                                                        select row).FirstOrDefault();

                        if (prevRecOppoLawyerMapping != null)
                        {
                            prevRecOppoLawyerMapping.IsActive = true;
                            prevRecOppoLawyerMapping.UpdatedBy = eachOppoLaywerRecord.CreatedBy;
                            prevRecOppoLawyerMapping.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.tbl_OppositionLawyerList.Add(eachOppoLaywerRecord);
                            saveSuccess = true;
                        }
                    });

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DeActiveExistingOppoLawyerMapping(long newCaseID, int Type)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var queryResult = (from row in entities.tbl_OppositionLawyerList
                                       where row.CaseNoticeInstanceID == newCaseID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       select row).ToList();

                    if (queryResult.Count > 0)
                    {
                        queryResult.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<tbl_OppositionLawyerList> GetListOfOppoLaywer(long noticeOrCaseInstanceID, int Type)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_OppositionLawyerList
                                   where row.CaseNoticeInstanceID == noticeOrCaseInstanceID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   select row);

                return queryResult.ToList();
            }
        }

        public static bool SaveLaywerRatingListData(List<tbl_LawyerListRating> objMainList)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    objMainList.ForEach(eachRatingRecord =>
                    {
                        var checkUser = (from row in entities.tbl_LawyerListRating
                                         where row.LawyerID == eachRatingRecord.LawyerID
                                         && row.UserID == eachRatingRecord.UserID
                                         && row.CaseNoticeID == eachRatingRecord.CaseNoticeID
                                         && row.Type == eachRatingRecord.Type
                                         && row.CriteriaRatingID == eachRatingRecord.CriteriaRatingID
                                         && row.IsActive == true
                                         select row).FirstOrDefault();
                        if (checkUser != null)
                        {
                            checkUser.Rating = eachRatingRecord.Rating;
                            checkUser.UpdatedBy = eachRatingRecord.UserID;
                            checkUser.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                        else
                        {
                            entities.tbl_LawyerListRating.Add(eachRatingRecord);
                        }
                    });

                    entities.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool CheckUserIsInternal(int userID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var checkUser = (from row in entities.tbl_LegalCaseAssignment
                                     where row.UserID == userID
                                     && row.IsLawyer == false
                                     select row);
                    if (checkUser != null)
                    {
                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool CheckUserIsInternalForNotice(int userID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var checkUser = (from row in entities.tbl_LegalNoticeAssignment
                                     where row.UserID == userID
                                     && row.IsLawyer == false
                                     select row);
                    if (checkUser != null)
                    {
                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static decimal GetLawyerRating(int LawyerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.sp_LiGetLawyerRatingAVG(LawyerID)
                             select row).FirstOrDefault();

                return (decimal)Query;
            }
        }

        public static bool CheckIsExistLawyerRating(tbl_LawyerFinalRating objNewRate)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var checkUser = (from row in entities.tbl_LawyerFinalRating
                                     where row.LawyerID == objNewRate.LawyerID
                                     && row.IsActive == true
                                     select row).FirstOrDefault();
                    if (checkUser != null)
                    {
                        return false;
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static void CreateLawyerFinalRating(tbl_LawyerFinalRating objNewRate)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_LawyerFinalRating.Add(objNewRate);
                entities.SaveChanges();
            }
        }

        public static void UpdateLawyerFinalRating(tbl_LawyerFinalRating objNewRate)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LawyerFinalRating
                                   where row.LawyerID == objNewRate.LawyerID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                if (queryResult != null)
                {
                    queryResult.Rating = objNewRate.Rating;
                    queryResult.UpdatedBy = objNewRate.UpdatedBy;
                    queryResult.UpdatedOn = objNewRate.UpdatedOn;
                    entities.SaveChanges();
                }
            }
        }

        public static object GettDocumentList(int caseNoticeInstanceID, string Doctype)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var ListOfDoc = (from row in entities.tbl_LitigationFileData
                                 where row.NoticeCaseInstanceID == caseNoticeInstanceID
                                 && row.DocType.Trim() == Doctype
                                 && row.IsDeleted == false
                                 select row).ToList();

                return ListOfDoc;
            }
        }

        public static void CreateCustomeFieldParameterValue(tbl_NoticeCaseCustomParameter objParameter)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                Entities.tbl_NoticeCaseCustomParameter.Add(objParameter);
                Entities.SaveChanges();
            }
        }

        public static bool IsExistCustomeFieldParameterValue(tbl_NoticeCaseCustomParameter objParameter)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var checkquery = (from row in entities.tbl_NoticeCaseCustomParameter
                                      where row.LabelID == objParameter.LabelID
                                      && row.LabelValue == objParameter.LabelValue
                                        && row.NoticeCaseInstanceID == objParameter.NoticeCaseInstanceID
                                       && row.NoticeCaseType == objParameter.NoticeCaseType
                                      && row.IsActive == true
                                      select row).FirstOrDefault();
                    if (checkquery != null)
                    {
                        return false;
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool IsExistCustomeFieldByID(tbl_NoticeCaseCustomParameter objParameter)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var checkquery = (from row in entities.tbl_NoticeCaseCustomParameter
                                      where row.LabelID == objParameter.LabelID
                                      && row.NoticeCaseInstanceID == objParameter.NoticeCaseInstanceID
                                      && row.NoticeCaseType == objParameter.NoticeCaseType
                                      && row.IsActive == true
                                      select row).FirstOrDefault();
                    if (checkquery != null)
                    {
                        return false;
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static void UpdateCustomeFieldParameterValue(tbl_NoticeCaseCustomParameter objParameter)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_NoticeCaseCustomParameter
                                   where row.LabelID == objParameter.LabelID
                                   && row.NoticeCaseInstanceID == objParameter.NoticeCaseInstanceID
                                   && row.NoticeCaseType == objParameter.NoticeCaseType
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    QueryResult.LabelValue = objParameter.LabelValue;
                    QueryResult.NoticeCaseType = objParameter.NoticeCaseType;
                    QueryResult.UpdatedBy = objParameter.UpdatedBy;
                    QueryResult.UpdatedOn = objParameter.UpdatedOn;
                    Entities.SaveChanges();
                }
            }
        }

        public static bool CreateUpdateCustomsFieldNoticeOrCaseWise(tbl_NoticeCaseCustomParameter objParameter)
        {
            try
            {
                bool saveSuccess = false;
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    if (objParameter.LabelID != 0)
                    {
                        var prevRecord = (from row in entities.tbl_NoticeCaseCustomParameter
                                          where row.LabelID == objParameter.LabelID
                                          && row.NoticeCaseInstanceID == objParameter.NoticeCaseInstanceID
                                          && row.NoticeCaseType == objParameter.NoticeCaseType
                                          select row).FirstOrDefault();

                        if (prevRecord != null)
                        {
                            prevRecord.LabelValue = objParameter.LabelValue;
                            prevRecord.FYear = objParameter.FYear;
                            prevRecord.Penalty = objParameter.Penalty;
                            prevRecord.Interest = objParameter.Interest;
                            prevRecord.Total = objParameter.Total;
                            prevRecord.SettlementValue = objParameter.SettlementValue;
                            prevRecord.ProvisionInBook = objParameter.ProvisionInBook;

                            prevRecord.IsAllowed = objParameter.IsAllowed;

                            prevRecord.IsActive = true;
                            prevRecord.IsDeleted = false;
                            prevRecord.UpdatedBy = objParameter.UpdatedBy;
                            prevRecord.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.tbl_NoticeCaseCustomParameter.Add(objParameter);
                            saveSuccess = true;
                        }
                    }
                    else
                    {
                        var prevRecord = (from row in entities.tbl_NoticeCaseCustomParameter
                                          where
                                           row.NoticeCaseInstanceID == objParameter.NoticeCaseInstanceID
                                          && row.NoticeCaseType == objParameter.NoticeCaseType
                                          select row).FirstOrDefault();

                        if (prevRecord != null)
                        {
                            prevRecord.LabelValue = objParameter.LabelValue;
                            prevRecord.FYear = objParameter.FYear;
                            prevRecord.Penalty = objParameter.Penalty;
                            prevRecord.Interest = objParameter.Interest;
                            prevRecord.Total = objParameter.Total;
                            prevRecord.SettlementValue = objParameter.SettlementValue;
                            prevRecord.ProvisionInBook = objParameter.ProvisionInBook;

                            prevRecord.IsAllowed = objParameter.IsAllowed;

                            prevRecord.IsActive = true;
                            prevRecord.IsDeleted = false;
                            prevRecord.UpdatedBy = objParameter.UpdatedBy;
                            prevRecord.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            entities.tbl_NoticeCaseCustomParameter.Add(objParameter);
                            saveSuccess = true;
                        }
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateNoticeToCaseMapping(tbl_NoticeCaseMapping objNoticeCaseMapping)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                Entities.tbl_NoticeCaseMapping.Add(objNoticeCaseMapping);
                Entities.SaveChanges();
                return true;
            }
        }

        public static bool IsExistCaseToCaseTrasfer(int caseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var checkquery = (from row in entities.tbl_NoticeCaseMapping
                                      where row.NoticeCaseInstanceID == caseInstanceID
                                      && row.IsActive == true
                                      select row).ToList();
                    if (checkquery.Count > 0)
                    {
                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool UpdateCaseResult(tbl_LegalCaseInstance objcaseInstance)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var noticeRecordToUpdate = (from row in entities.tbl_LegalCaseInstance
                                                where row.ID == objcaseInstance.ID
                                                select row).FirstOrDefault();

                    if (noticeRecordToUpdate != null)
                    {
                        noticeRecordToUpdate.CaseResult = objcaseInstance.CaseResult;
                       // noticeRecordToUpdate.FinancialYear = objcaseInstance.FinancialYear;
                        entities.SaveChanges();
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool UpdateNoticeResult(tbl_LegalNoticeInstance objNoticeInstance)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var noticeRecordToUpdate = (from row in entities.tbl_LegalNoticeInstance
                                                where row.ID == objNoticeInstance.ID
                                                select row).FirstOrDefault();

                    if (noticeRecordToUpdate != null)
                    {
                        noticeRecordToUpdate.NoticeResult = objNoticeInstance.NoticeResult;
                        noticeRecordToUpdate.NoticeStage = objNoticeInstance.NoticeStage;
                        entities.SaveChanges();
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        #endregion

        public static bool ExistsCourtCaseNo(string caseRefNo, long CaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalCaseInstance
                             where row.CaseRefNo.ToUpper().Trim().Equals(caseRefNo.ToUpper().Trim())
                             && row.IsDeleted == false
                             select row);

                if (CaseInstanceID != 0)
                {
                    query = query.Where(entry => entry.ID != CaseInstanceID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static long GetCaseInstanceIDByCourtCaseNo(string caseRefNo)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalCaseInstance
                             where row.CaseRefNo.ToUpper().Trim().Equals(caseRefNo.ToUpper().Trim())
                             && row.IsDeleted == false
                             select row.ID).FirstOrDefault();

                return query;
            }
        }

        public static bool ExistsCaseRefNo(string CaseRefNo, int caseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalCaseInstance
                             where row.IsDeleted == false
                             && row.CaseRefNo.Equals(CaseRefNo)
                             select row);

                if (caseInstanceID != 0)
                {
                    query = query.Where(entry => entry.ID != caseInstanceID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static bool ClosedDateCheck(string CaseRefNo, string CaseDate)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LegalCaseInstance
                             where row.CaseRefNo.Equals(CaseRefNo)
                             && row.IsDeleted == false
                             select row.ID).FirstOrDefault();

                var query2 = (from row in entities.tbl_LegalCaseStatus
                              where row.CaseInstanceID == query
                              && row.IsDeleted == false
                              select row.CloseDate).FirstOrDefault();

                var Response_Date = DateTime.Parse(CaseDate).Date;

                if (query2.HasValue)
                {
                    var CaseCloseDate = query2.Value.Date;


                    if (Response_Date <= CaseCloseDate)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public static tbl_NoticeCasePayment GetPaymentDetailsByID(int caseInstanceID, int PaymentID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_NoticeCasePayment
                                   where row.NoticeOrCaseInstanceID == caseInstanceID
                                   && row.ID == PaymentID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }
        public static tbl_LegalCaseOrder GetCaseOrderDetailsByID(int caseInstanceID, int orderID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalCaseOrder
                                   where row.CaseInstanceID == caseInstanceID
                                   && row.ID == orderID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        public static tbl_LegalCaseResponse GetCaseResponseDetailsByID(int caseInstanceID, int responseID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var queryResult = (from row in entities.tbl_LegalCaseResponse
                                   where row.CaseInstanceID == caseInstanceID
                                   && row.ID == responseID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                return queryResult;
            }
        }

        public static bool DeleteHearingEditDocument(int ID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LitigationFileData
                                   where row.ID == ID
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    QueryResult.IsDeleted = true;
                    Entities.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public static tbl_LitigationFileData ShowandDownloadFileById(int ID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LitigationFileData
                                   where row.ID == ID
                                   select row).FirstOrDefault();


                return QueryResult;
            }
        }
        public static tbl_LitigationFileData ShowandDownloadAdvbillFileById(int ID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LitigationFileData
                                   where row.ID == ID && row.DocType=="CA"
                                   select row).FirstOrDefault();


                return QueryResult;
            }
        }
        public static bool DeleteAdvBillEditDocument(int ID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LitigationFileData
                                   where row.ID == ID && row.DocType=="CA"
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    QueryResult.IsDeleted = true;
                    Entities.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public static bool DeleteOrderEditDocument(int ID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LitigationFileData
                                   where row.ID == ID
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    QueryResult.IsDeleted = true;
                    Entities.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public static long UpdatePayment(tbl_NoticeCasePayment newRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {

                    var query = (from row in entities.tbl_NoticeCasePayment
                                 where row.ID == newRecord.ID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        query.IsActive = newRecord.IsActive;
                        query.PaymentDate = newRecord.PaymentDate;
                        query.PaymentID = newRecord.PaymentID;
                        query.Amount = newRecord.Amount;
                        query.AmountPaid = newRecord.AmountPaid;
                        query.AmountTax = newRecord.AmountTax;
                        query.Lawyer = newRecord.Lawyer;
                        query.Remark = newRecord.Remark;
                        query.HearingID = newRecord.HearingID;
                        query.UpdatedBy = newRecord.CreatedBy;
                        query.CreatedByText = newRecord.CreatedByText;
                        query.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        return query.ID;
                    }
                    else
                        return 0;

                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        //public static long UpdatePayment(tbl_NoticeCasePayment newRecord)
        //{
        //    try
        //    {
        //        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //        {

        //            var query = (from row in entities.tbl_NoticeCasePayment
        //                         where row.ID == newRecord.ID
        //                         select row).FirstOrDefault();
        //            if (query != null)
        //            {
        //                query.IsActive = newRecord.IsActive;
        //                query.PaymentDate = newRecord.PaymentDate;
        //                query.PaymentID = newRecord.PaymentID;
        //                query.Amount = newRecord.Amount;
        //                query.AmountPaid = newRecord.AmountPaid;
        //                query.Lawyer = newRecord.Lawyer;
        //                query.Remark = newRecord.Remark;
        //                query.HearingID = newRecord.HearingID;
        //                query.UpdatedBy = newRecord.CreatedBy;
        //                query.CreatedByText = newRecord.CreatedByText;
        //                query.UpdatedOn = DateTime.Now;
        //                entities.SaveChanges();
        //                return query.ID;
        //            }
        //            else
        //                return 0;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return 0;
        //    }
        //}

        public static long UpdateCaseOrderLog(tbl_LegalCaseOrder newRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {

                    var query = (from row in entities.tbl_LegalCaseOrder
                                 where row.ID == newRecord.ID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        query.IsActive = newRecord.IsActive;
                        query.OrderTypeID = newRecord.OrderTypeID;
                        query.OrderTitle = newRecord.OrderTitle;
                        query.OrderDesc = newRecord.OrderDesc;
                        query.CaseInstanceID = newRecord.CaseInstanceID;
                        query.OrderDate = newRecord.OrderDate;
                        query.UpdatedBy = newRecord.CreatedBy;
                        query.CreatedByText = newRecord.CreatedByText;
                        query.UpdatedOn = DateTime.Now;
                        query.UserID = newRecord.UserID;
                        entities.SaveChanges();
                        return query.ID;
                    }
                    else
                        return 0;

                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static long UpdateCaseResponseLog(tbl_LegalCaseResponse newRecord)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {

                    var query = (from row in entities.tbl_LegalCaseResponse
                                 where row.ID == newRecord.ID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        query.IsActive = newRecord.IsActive;
                        query.RefID = newRecord.RefID;
                        query.CaseInstanceID = newRecord.CaseInstanceID;
                        query.ResponseDate = newRecord.ResponseDate;
                        query.Description = newRecord.Description;
                        query.CreatedByText = newRecord.CreatedByText;

                        query.HearingBudget = newRecord.HearingBudget;
                        query.HearingLawFirm = newRecord.HearingLawFirm;
                        query.HearingLawyer = newRecord.HearingLawyer;

                        query.UserID = newRecord.UserID;
                        query.RoleID = newRecord.RoleID;
                        query.Remark = newRecord.Remark;
                        query.ReminderDate = newRecord.ReminderDate;
                        query.ReminderMeONDate = newRecord.ReminderMeONDate;
                        query.UpdatedBy = newRecord.CreatedBy;
                        query.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        return query.ID;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static List<int> UserCanCheckRating(int caseInstanceID, int Type)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.Sp_GetLawyerListForRating(caseInstanceID, Type)
                             select (int)row).ToList();
                return Query;
            }
        }

        public static Sp_DisplayUserSpecilizationForEdit_Result GetUserWithSpecilization(int userID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.Sp_DisplayUserSpecilizationForEdit(userID)
                             select row).FirstOrDefault();
                return Query;
            }
        }

        public static bool CheckUserSpecilazationExist(int userID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LawyerFinalRating
                                   where row.LawyerID == userID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void UpdateFinalratingSpecilasation(tbl_LawyerFinalRating objrating)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var QueryResult = (from row in Entities.tbl_LawyerFinalRating
                                   where row.LawyerID == objrating.LawyerID
                                   && row.IsActive == true
                                   select row).FirstOrDefault();

                if (QueryResult != null)
                {
                    QueryResult.Specilisation = objrating.Specilisation;
                    Entities.SaveChanges();
                }
            }
        }



        public static List<ShowReportCustomField> GetCustomParameterForReport(long noticeCaseInstanceID, long customerID, int Type)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var user = (from row in entities.tbl_NoticeCaseCustomParameter
                            join row1 in entities.tbl_CustomField
                            on row.LabelID equals row1.ID
                            where row1.CustomerID == customerID
                            && row.NoticeCaseType == Type
                            && row.NoticeCaseInstanceID == noticeCaseInstanceID
                            && row.IsDeleted == false && row.IsActive == true
                            select new ShowReportCustomField()
                            {
                                LabelName = row1.Label,
                                LabelValue = row.LabelValue,
                                SettlementValue = row.SettlementValue,
                                Interest = row.Interest,
                                Penalty = row.Penalty,
                                Total = row.Total,
                                ProvisionInBook = row.ProvisionInBook,
                                IsAllowed = row.IsAllowed,
                            }).ToList();

                return user;
            }
        }


        public static List<HearingDetailReport> GetHearingDetailCaseWise(long noticeCaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.tbl_LegalCaseResponse
                             where row.CaseInstanceID == noticeCaseInstanceID
                             && row.IsActive == true
                             select new HearingDetailReport()
                             {
                                 CaseInstanceID = row.CaseInstanceID,
                                 ResponseDate = row.ResponseDate,
                                 Description = row.Description,
                                 Remark = row.Remark,
                                 NextHearingDate = row.ReminderDate,
                                 CreatedByText = row.CreatedByText
                             }).ToList();

                return Query;
            }
        }
        public static List<ResponseDetailReport> GetResponseDetailNoticeWise(long noticeCaseInstanceID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.tbl_LegalNoticeResponse
                             where row.NoticeInstanceID == noticeCaseInstanceID
                             && row.IsActive == true
                             select new ResponseDetailReport()
                             {
                                 CaseInstanceID = row.NoticeInstanceID,
                                 ResponseDate = row.ResponseDate,
                                 Description = row.Description,
                                 Remark = row.Remark,
                                 NextHearingDate = row.ResponseDate,
                                 CreatedByText = row.CreatedByText
                             }).ToList();

                return Query;
            }
        }

        public static string GetLocationByCaseInstanceID(int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.CustomerBranches
                             where row.ID == customerBranchID
                             select row.Name).FirstOrDefault();

                return Query;
            }
        }

        // After 31 code
        public static List<int> GetAllAssingedUserList(long caseInstanceID, int userID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    var record = (from row in entities.tbl_LegalCaseAssignment
                                  where row.CaseInstanceID == caseInstanceID &&
                                  row.IsActive == true
                                  select row.UserID).ToList();

                    return record;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<string> GetAssignedUserAndOwnerMail(List<long> updatedAssingedUser)
        {
            using (ComplianceDBEntities Entities = new ComplianceDBEntities())
            {
                var QueryResult = (from row in Entities.Users
                                   where row.IsActive == true
                                   && row.IsDeleted == false
                                   && updatedAssingedUser.Contains(row.ID)
                                   select row.Email).ToList();

                return QueryResult;
            }
        }


        //Chages of Ruchi
        public static List<tbl_TypeMaster> GetCaseStageType(int CutomerID, string Filter)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstContractType = (from row in entities.tbl_TypeMaster
                                       where row.customerID == CutomerID
                                       && row.IsActive == true
                                       && row.TypeCode == "CS"
                                       select row).ToList();

                return lstContractType;
            }
        }

        public static tbl_TypeMaster GetCaseStageDetailByID(int iD, long customerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var _objcasestage = (from row in entities.tbl_TypeMaster
                                     where row.ID == iD
                                     && row.customerID == customerID
                                     select row).SingleOrDefault();
                return _objcasestage;
            }
        }

        public static bool ExistsCaseStageType(tbl_TypeMaster _objCaseStage)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var _objcasestagetype = (from row in entities.tbl_TypeMaster
                                         where row.TypeName.Equals(_objCaseStage.TypeName)
                                         && row.customerID == _objCaseStage.customerID
                                         select row);

                return _objcasestagetype.Select(entry => true).SingleOrDefault();
            }
        }

        public static long CreateCaseStageDetails(tbl_TypeMaster _objCaseStage)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_TypeMaster.Add(_objCaseStage);
                entities.SaveChanges();
                long casestageID = _objCaseStage.ID;
                return casestageID;
            }

        }
        public static void UpdateCaseStageDetails(tbl_TypeMaster _objCaseStage)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_TypeMaster objRecord = (from row in entities.tbl_TypeMaster
                                            where row.ID == _objCaseStage.ID
                                            && row.customerID == _objCaseStage.customerID
                                            select row).FirstOrDefault();
                if (objRecord != null)
                {
                    objRecord.TypeName = _objCaseStage.TypeName;
                    objRecord.TypeCode = _objCaseStage.TypeCode;
                    objRecord.UpdatedBy = _objCaseStage.UpdatedBy;
                    objRecord.UpdatedOn = _objCaseStage.UpdatedOn;
                    entities.SaveChanges();
                }
            }
        }

        public static tbl_NoticeCasePayment GetcasedetailofPayment(int noticePaymentID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var noticecaseTypeList = (from row in entities.tbl_NoticeCasePayment
                                          where row.ID == noticePaymentID &&
                                          row.IsActive == true
                                          select row).FirstOrDefault();
                return noticecaseTypeList;
            }
        }

        public static List<SP_tbl_NoticeCasePayment_Result> GetcaseHearingDetails(int? hearingID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstContractType = entities.SP_tbl_NoticeCasePayment(hearingID).ToList();
                return lstContractType;
            }
        }
        //End 

        #region Document Type Tag
        public static tbl_Litigation_DocumentTypeMaster GetDocTypeDetailsByID(long docTypeID, int customerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var noticecaseTypeList = (from row in entities.tbl_Litigation_DocumentTypeMaster
                                          where row.CustomerID == customerID &&
                                          row.ID == docTypeID &&
                                          row.IsDeleted == false
                                          select row).FirstOrDefault();

                return noticecaseTypeList;

            }
        }

        public static bool ExistsContDocumentType(tbl_Litigation_DocumentTypeMaster _objContDoc)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var noticecaseTypeList = (from row in entities.tbl_Litigation_DocumentTypeMaster
                                          where row.CustomerID == _objContDoc.CustomerID &&
                                          row.TypeName == _objContDoc.TypeName &&
                                          row.IsDeleted == false
                                          select row).FirstOrDefault();

                if (noticecaseTypeList != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<object> GetLitigationDocTypes_All(int customerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var noticecaseTypeList = (from row in entities.tbl_Litigation_DocumentTypeMaster
                                          where row.CustomerID == customerID &&
                                          row.IsDeleted == false
                                          select row).ToList();

                var result = (from row in noticecaseTypeList
                              select new { ID = row.ID, Name = row.TypeName }).OrderBy(entry => entry.Name).ToList<object>();

                return result;
            }
        }
        public static long CreateDocumentType(tbl_Litigation_DocumentTypeMaster _objContDoc)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_Litigation_DocumentTypeMaster.Add(_objContDoc);
                entities.SaveChanges();
                return _objContDoc.ID;
            }
        }
        public static bool UpdateDocTypeDetails(tbl_Litigation_DocumentTypeMaster _objContDoc)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var noticecaseTypeList = (from row in entities.tbl_Litigation_DocumentTypeMaster
                                          where row.CustomerID == _objContDoc.CustomerID &&
                                          row.ID == _objContDoc.ID &&
                                          row.IsDeleted == false
                                          select row).FirstOrDefault();
                if (noticecaseTypeList != null)
                {
                    noticecaseTypeList.TypeName = _objContDoc.TypeName;
                    noticecaseTypeList.UpdatedBy = _objContDoc.UpdatedBy;
                    noticecaseTypeList.UpdatedOn = _objContDoc.UpdatedOn;
                    noticecaseTypeList.IsDeleted = false;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static int CreateCaseDocumentMappingGetID(tbl_LitigationFileData objNoticeCaseDoc)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNoticeCaseDoc.EnType = "A";
                    entities.tbl_LitigationFileData.Add(objNoticeCaseDoc);
                    entities.SaveChanges();
                    return objNoticeCaseDoc.ID;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static int CreateCaseDocumentPaymentMappingGetID(tbl_LitigationPaymentFileData objNoticeCaseDoc)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    objNoticeCaseDoc.EnType = "A";
                    entities.tbl_LitigationPaymentFileData.Add(objNoticeCaseDoc);
                    entities.SaveChanges();
                    return objNoticeCaseDoc.ID;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static bool CreateUpdate_FileTagsMapping(List<tbl_Litigation_FileDataTagsMapping> lstFileTagMapping)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                lstFileTagMapping.ForEach(eachFileData =>
                {
                    entities.tbl_Litigation_FileDataTagsMapping.Add(eachFileData);
                    entities.SaveChanges();
                });
                return true;
            }
        }

        public static List<tbl_Litigation_DocumentTypeMaster> GetLitigationDocumentTypes(long cutomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var noticecaseTypeList = (from row in entities.tbl_Litigation_DocumentTypeMaster
                                          where row.CustomerID == cutomerID &&
                                          row.IsDeleted == false
                                          select row).ToList();
                return noticecaseTypeList;
            }
        }
        public static bool DeleteDocType(long docTypeID, long customerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var noticecaseTypeList = (from row in entities.tbl_Litigation_DocumentTypeMaster
                                          where row.CustomerID == customerID
                                          && row.ID == docTypeID
                                          && row.IsDeleted == false
                                          select row).FirstOrDefault();
                if (noticecaseTypeList != null)
                {
                    noticecaseTypeList.IsDeleted = true;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static object GetContractDocTypes_All(int customerID)
        {
            throw new NotImplementedException();
        }
        public static object ExistsContractDocumentReturnVersion(tbl_LitigationFileData objContDoc)
        {
            throw new NotImplementedException();
        }

        public static bool CreateContractDocumentMapping(tbl_LitigationFileData objContDoc)
        {
            throw new NotImplementedException();
        }

        public static void CreateLitigationFileData(string v1, long noticeCaseInstanceID, string v2, string v3, int v4, int userID, string v5, bool v6)
        {
            throw new NotImplementedException();
        }
        #endregion

        public static bool DeleteCaseStageDetailByID(int iD, long customerID)
        {
            using (LitigationDataModelContainer Entities = new LitigationDataModelContainer())
            {
                var dtlcasestagebyID = (from row in Entities.tbl_TypeMaster
                                        where row.ID == iD && row.IsActive == true
                                        && row.customerID == customerID
                                        select row).FirstOrDefault();

                if (dtlcasestagebyID != null)
                {
                    dtlcasestagebyID.DeletedOn = DateTime.Now;
                    dtlcasestagebyID.DeletedBy = Convert.ToInt16(customerID);
                    dtlcasestagebyID.IsActive = false;
                    Entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool DeleteCustomFieldDyID(int iD, int typeID, int CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    var _objdelByID = (from row in entities.tbl_CustomField
                                       where row.IsActive == true
                                       && row.CaseNoticeCategory == typeID
                                       && row.ID == iD
                                       && row.CustomerID == CustomerID
                                       select row).FirstOrDefault();
                    if (_objdelByID != null)
                    {
                        _objdelByID.IsActive = false;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static List<Int32> GetMasterPageDtls(int userid)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstpage_dtls = (from row in entities.tbl_PageAuthorizationMaster
                                    where row.UserID == userid &&
                                      row.isActive == true
                                      && row.Viewval == true
                                    select (int)row.PageID).Distinct().ToList();

                return lstpage_dtls;
            }
        }

        public static bool CheckUserExistInLitigation(int userID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var CheckUserCount = (from row in entities.tbl_LegalNoticeInstance
                                      where row.OwnerID == userID &&
                                      row.IsDeleted == false
                                      select row).ToList();
                if (CheckUserCount.Count > 0)
                {
                    return true;
                }
                var CheckUserCount1 = (from row in entities.tbl_LegalCaseInstance
                                       where row.OwnerID == userID &&
                                       row.IsDeleted == false
                                       select row).ToList();
                if (CheckUserCount1.Count > 0)
                {
                    return true;
                }
                var CheckUserCount2 = (from row in entities.tbl_OppositionLawyerList
                                       where row.LawyerID == userID &&
                                       row.IsActive == true
                                       select row).ToList();
                if (CheckUserCount2.Count > 0)
                {
                    return true;
                }
                var CheckUserCount3 = (from row in entities.tbl_TaskScheduleOn
                                       where row.AssignTo == userID &&
                                       row.IsActive == true
                                       select row).ToList();
                if (CheckUserCount3.Count > 0)
                {
                    return true;
                }
                var CheckUserCount4 = (from row in entities.tbl_LegalCaseAssignment
                                       where row.UserID == userID &&
                                       row.IsActive == true
                                       select row).ToList();
                if (CheckUserCount3.Count > 0)
                {
                    return true;
                }
                var CheckUserCount5 = (from row in entities.tbl_LegalNoticeAssignment
                                       where row.UserID == userID &&
                                       row.IsActive == true
                                       select row).ToList();
                if (CheckUserCount3.Count > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public static List<string> GetDistinctFileTagsCase(int customerID, long NoticeCaseInstanceID)
        {
            try
            {
                List<string> lstFileTags = new List<string>();
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    lstFileTags = (from row in entities.tbl_Litigation_FileDataTagsMapping
                                   join row1 in entities.tbl_LitigationFileData
                                   on row.FileID equals row1.ID
                                   join row2 in entities.tbl_LegalCaseInstance
                                   on row1.NoticeCaseInstanceID equals row2.ID
                                   where row1.NoticeCaseInstanceID == NoticeCaseInstanceID
                                   && row2.CustomerID == customerID
                                   && row1.IsDeleted == false
                                   && row.IsActive == true
                                   && row2.IsDeleted == false
                                   select row.FileTag.Trim()).Distinct().ToList();

                    return lstFileTags;
                }
            }
            catch (Exception ex)
            {
                return new List<string>();
            }
        }

        public static List<ListItem> ReArrange_FileTagsCase(CheckBoxList lstBoxFileTags)
        {
            List<ListItem> lstSelectedTags = new List<ListItem>();
            List<ListItem> lstNonSelectedTags = new List<ListItem>();

            foreach (ListItem eachListItem in lstBoxFileTags.Items)
            {
                if (eachListItem.Selected)
                    lstSelectedTags.Add(eachListItem);
                else
                    lstNonSelectedTags.Add(eachListItem);
            }

            return lstSelectedTags.Union(lstNonSelectedTags).ToList();
        }

        public static List<ListItem> GetSelectedItems(ListControl lst)
        {
            return lst.Items.OfType<ListItem>().Where(i => i.Selected).ToList();
        }


        public static bool CheckRefcasestageexist(int iD, long customerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstpage_dtls = (from row in entities.tbl_LegalCaseInstance
                                    where row.CaseStageID == iD &&
                                    row.CustomerID == customerID &&
                                      row.IsDeleted == false
                                    select row).FirstOrDefault();

                if (lstpage_dtls != null)
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckPartyMappingExist(int partyiD)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstPartyMapping = (from row in entities.tbl_PartyMapping
                                       where row.PartyID == partyiD
                                       && row.IsActive == true
                                       select row).ToList();

                if (lstPartyMapping.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckLawFirmMappingExist(int lawFirmID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                using (ComplianceDBEntities entitiess = new ComplianceDBEntities())
                {

                    var lstCaseLawFirmMapping = (from row in entities.tbl_LegalCaseLawyerMapping
                                                 where row.LawyerID == lawFirmID
                                                 && row.IsActive == true
                                                 select row).ToList();

                    var lstNoticeLawFirmMapping = (from row in entities.tbl_LegalNoticeLawyerMapping
                                                   where row.LawyerID == lawFirmID
                                                   && row.IsActive == true
                                                   select row).ToList();

                    var lstLawFirmUsers = (from row in entitiess.Users
                                           where row.LawyerFirmID == lawFirmID
                                           && row.IsActive == true
                                           select row).ToList();

                    if (lstCaseLawFirmMapping.Count > 0 || lstNoticeLawFirmMapping.Count > 0 || lstLawFirmUsers.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public static bool CheckCourtAssignmentExist(int courtID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstCaseCourtMapping = (from row in entities.tbl_LegalCaseInstance
                                           where row.CourtID == courtID
                                           && row.IsDeleted == false
                                           select row).ToList();

                if (lstCaseCourtMapping.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckCaseNoticeTypeAssignmentExist(int typeID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstCaseTypeMapping = (from row in entities.tbl_LegalCaseInstance
                                          where row.CaseCategoryID == typeID
                                          && row.IsDeleted == false
                                          select row).ToList();

                var lstNoticeTypeMapping = (from row in entities.tbl_LegalNoticeInstance
                                            where row.NoticeCategoryID == typeID
                                            && row.IsDeleted == false
                                            select row).ToList();

                if (lstCaseTypeMapping.Count > 0 || lstNoticeTypeMapping.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckPaymentTypeMappingExist(int typeID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstPaymentTypeMapping = (from row in entities.tbl_NoticeCasePayment
                                             where row.PaymentID == typeID
                                             && row.IsActive == true
                                             select row).ToList();

                if (lstPaymentTypeMapping.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckCustomParameterAssignmentExist(int LabelOrParameterID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstCustomParameterAssignments = (from row in entities.tbl_NoticeCaseCustomParameter
                                                     where row.LabelID == LabelOrParameterID
                                                     && row.IsActive == true
                                                     && row.IsDeleted == false
                                                     select row).ToList();

                if (lstCustomParameterAssignments.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckDocTypeAssignmentExist(long docTypeID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstDocTypeAssignments = (from row in entities.tbl_LitigationFileData
                                             where row.DocTypeID == docTypeID
                                             && row.IsDeleted == false
                                             select row).ToList();

                if (lstDocTypeAssignments.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckRatingCriteriaAssignmentExist(long criteriaID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var lstRatingCriteriaAssignments = (from row in entities.tbl_LawyerListRating
                                                    where row.CriteriaRatingID == criteriaID
                                                    && row.IsActive == true
                                                    select row).ToList();

                if (lstRatingCriteriaAssignments.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static void RemoveExistingHearingRef(string hearing,long? caseinstanceid)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var data = (from row in entities.tbl_CaseAdvocateBill
                            where row.HearingRef == hearing && row.NoticeCaseInstanceID == caseinstanceid
                            select row).FirstOrDefault();

                entities.tbl_CaseAdvocateBill.Remove(data);
                entities.SaveChanges();
            }
        }
        public static bool CheckCaseLawyerHearingExist(string hearing)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var CaseLawyerHearing = (from row in entities.tbl_CaseAdvocateBill
                                                    where row.HearingRef == hearing
                                                    && row.IsActive == true
                                                    select row).ToList();

                if (CaseLawyerHearing.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool UpdateAdvocateBillCaseHearing(List<tbl_CaseAdvocateBill> lstObjMapping, int advbillid)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                try
                {
                    bool saveSuccess = false;

                    lstObjMapping.ForEach(eachRecord =>
                    {
                        var prevMappingRecord = (from row in entities.tbl_CaseAdvocateBill
                                                 where row.ID == advbillid
                                                 select row).FirstOrDefault();

                        if (prevMappingRecord != null)
                        {

                            prevMappingRecord.HearingRef = eachRecord.HearingRef;
                            prevMappingRecord.InvoiceNo = eachRecord.InvoiceNo;
                            prevMappingRecord.InvoiceAmount = eachRecord.InvoiceAmount;
                            prevMappingRecord.NoticeCaseInstanceID = eachRecord.NoticeCaseInstanceID;
                            //prevMappingRecord.AssignTo = eachRecord.AssignTo;
                            prevMappingRecord.Lawyer = eachRecord.Lawyer;
                            prevMappingRecord.Remark = eachRecord.Remark;
                            prevMappingRecord.IsActive = true;
                            prevMappingRecord.UpdatedBy = eachRecord.CreatedBy;
                            prevMappingRecord.UpdatedOn = DateTime.Now;
                            prevMappingRecord.ApproverID1 = Convert.ToInt32(eachRecord.ApproverID1);
                            prevMappingRecord.ApproverID2 = Convert.ToInt32(eachRecord.ApproverID2);
                            if (eachRecord.ApproverID3 != 0)
                            {
                                prevMappingRecord.ApproverID3 = Convert.ToInt32(eachRecord.ApproverID3);
                            }
                            if (prevMappingRecord.status == "Rejected")
                            {
                                prevMappingRecord.status = "Reassigned";

                                var UserDetails = UserManagement.GetUserName(Convert.ToInt32(eachRecord.CreatedBy));
                                if (UserDetails != null)
                                {

                                    try
                                    {
                                        tbl_CaseAdvocateBillAuditLog objBill = new tbl_CaseAdvocateBillAuditLog()
                                        {
                                            NoticeCaseInstanceID = eachRecord.NoticeCaseInstanceID,
                                            IsDeleted = false,
                                            InvoiceNo = eachRecord.InvoiceNo,
                                            Remark = eachRecord.Remark,
                                            Status = "Reassigned",
                                            ApprovedOrRejectedBy = UserDetails,
                                        };

                                        objBill.CreatedOn = DateTime.Now;

                                        entities.tbl_CaseAdvocateBillAuditLog.Add(objBill);
                                        entities.SaveChanges();

                                    }
                                    catch (Exception ex)
                                    {
                                        LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }

                            }
                            prevMappingRecord.InvoiceDate = eachRecord.InvoiceDate;
                            prevMappingRecord.Currency = eachRecord.Currency;
                            saveSuccess = true;
                        }
                        //else
                        //{
                        //    entities.tbl_CaseAdvocateBill.Add(prevMappingRecord);
                        //    saveSuccess = true;
                        //}
                    });

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        //public static bool UpdateAdvocateBillCaseHearing(List<tbl_CaseAdvocateBill> lstObjMapping, int advbillid)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        try
        //        {
        //            bool saveSuccess = false;

        //            lstObjMapping.ForEach(eachRecord =>
        //            {
        //                var prevMappingRecord = (from row in entities.tbl_CaseAdvocateBill
        //                                         where row.ID == advbillid
        //                                         select row).FirstOrDefault();

        //                if (prevMappingRecord != null)
        //                {

        //                    prevMappingRecord.HearingRef = eachRecord.HearingRef;
        //                    prevMappingRecord.InvoiceNo = eachRecord.InvoiceNo;
        //                    prevMappingRecord.InvoiceAmount = eachRecord.InvoiceAmount;
        //                    prevMappingRecord.NoticeCaseInstanceID = eachRecord.NoticeCaseInstanceID;
        //                    //prevMappingRecord.AssignTo = eachRecord.AssignTo;
        //                    prevMappingRecord.Lawyer = eachRecord.Lawyer;
        //                    prevMappingRecord.Remark = eachRecord.Remark;
        //                    prevMappingRecord.IsActive = true;
        //                    prevMappingRecord.UpdatedBy = eachRecord.CreatedBy;
        //                    prevMappingRecord.UpdatedOn = DateTime.Now;
        //                    prevMappingRecord.ApproverID1 = Convert.ToInt32(eachRecord.ApproverID1);
        //                    prevMappingRecord.ApproverID2 = Convert.ToInt32(eachRecord.ApproverID2);
        //                    if (eachRecord.ApproverID3 != 0)
        //                    {
        //                        prevMappingRecord.ApproverID3 = Convert.ToInt32(eachRecord.ApproverID3);
        //                    }
        //                    if (prevMappingRecord.status == "Rejected")
        //                    {
        //                        prevMappingRecord.status = "Reassigned";

        //                        try
        //                        {
        //                            tbl_CaseAdvocateBillAuditLog objBill = new tbl_CaseAdvocateBillAuditLog()
        //                            {
        //                                NoticeCaseInstanceID = eachRecord.NoticeCaseInstanceID,
        //                                IsDeleted = false,
        //                                InvoiceNo = eachRecord.InvoiceNo,
        //                                Remark = eachRecord.Remark,
        //                                Status = "Reassigned",
        //                                ApprovedOrRejectedBy = "Creator",
        //                            };

        //                            objBill.CreatedOn = DateTime.Now;

        //                            entities.tbl_CaseAdvocateBillAuditLog.Add(objBill);
        //                            entities.SaveChanges();

        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //                        }

        //                    }
        //                    prevMappingRecord.InvoiceDate = eachRecord.InvoiceDate;
        //                    prevMappingRecord.Currency = eachRecord.Currency;
        //                    saveSuccess = true;
        //                }
        //                //else
        //                //{
        //                //    entities.tbl_CaseAdvocateBill.Add(prevMappingRecord);
        //                //    saveSuccess = true;
        //                //}
        //            });

        //            entities.SaveChanges();

        //            return saveSuccess;
        //        }
        //        catch (Exception ex)
        //        {
        //            LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            return false;
        //        }
        //    }
        //}


        //public static bool CreateAdvocateBillCaseHearing(List<tbl_CaseAdvocateBill> lstObjMapping, int advbillid)
        //{
        //    using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
        //    {
        //        try
        //        {
        //            bool saveSuccess = false;

        //            lstObjMapping.ForEach(eachRecord =>
        //            {
        //                var prevMappingRecord = (from row in entities.tbl_CaseAdvocateBill
        //                                         where row.NoticeCaseInstanceID == eachRecord.NoticeCaseInstanceID
        //                                         && row.HearingRef == eachRecord.HearingRef
        //                                         select row).FirstOrDefault();

        //                if (prevMappingRecord != null)
        //                {
        //                    prevMappingRecord.HearingRef = eachRecord.HearingRef;
        //                    prevMappingRecord.InvoiceNo = eachRecord.InvoiceNo;
        //                    prevMappingRecord.InvoiceAmount = eachRecord.InvoiceAmount;
        //                    prevMappingRecord.NoticeCaseInstanceID = eachRecord.NoticeCaseInstanceID;
        //                    prevMappingRecord.Lawyer = eachRecord.Lawyer;
        //                    prevMappingRecord.Remark = eachRecord.Remark;
        //                    prevMappingRecord.IsActive = true;
        //                    prevMappingRecord.UpdatedBy = eachRecord.CreatedBy;
        //                    prevMappingRecord.UpdatedOn = DateTime.Now;
        //                    prevMappingRecord.Currency = eachRecord.Currency;
        //                    saveSuccess = true;

        //                }
        //                else
        //                {
        //                    entities.tbl_CaseAdvocateBill.Add(eachRecord);
        //                    saveSuccess = true;
        //                }
        //            });

        //            entities.SaveChanges();

        //            return saveSuccess;
        //        }
        //        catch (Exception ex)
        //        {
        //            LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //            return false;
        //        }
        //    }
        //}
        public static bool DeleteCaseAdvocateBill(long caseInstanceID, int CaseAdvocateBillID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    
                    tbl_CaseAdvocateBill AdvbillLogToDelete = entities.tbl_CaseAdvocateBill.Where(x => x.ID == CaseAdvocateBillID).FirstOrDefault();

                    if (AdvbillLogToDelete != null)
                    {
                        AdvbillLogToDelete.IsActive = false;
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool DeleteCaseAdvocateBillDocument(long caseInstanceID, int CaseAdvocateBillID, int deletedByUserID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {

                    tbl_LitigationFileData AdvbillLogToDelete = entities.tbl_LitigationFileData.Where(x => x.NoticeCaseInstanceID == caseInstanceID && x.DocTypeInstanceID == CaseAdvocateBillID).FirstOrDefault();

                    if (AdvbillLogToDelete != null)
                    {
                        AdvbillLogToDelete.IsDeleted = true;
                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

    }
}
