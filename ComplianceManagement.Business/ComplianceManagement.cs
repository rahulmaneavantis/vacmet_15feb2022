﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using Newtonsoft.Json;
using System.Net;
using System.Reflection;
namespace com.VirtuosoITech.ComplianceManagement.Business
{
   
    public class ComplianceManagement
    {
        public static List<string> GetErrorMailUsers()
        {
            List<string> MailToUserList = new List<string>();
            var ToMailUsers = ConfigurationManager.AppSettings["ErrorMailUsers"].ToString();
            if (!string.IsNullOrEmpty(ToMailUsers))
            {
                MailToUserList = ToMailUsers.Split(',').ToList();
            }
            return MailToUserList;
        }
      
        public static void SendgridSenEmail(string subject, string meassage)
        {

            try
            {
                var TO = GetErrorMailUsers();
                //SendGridEmailManager.SendGridMail(ConfigurationManager.AppSettings["SenderEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"],
                //    TO.ToList(), null, null, subject, meassage);

                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"],TO.ToList(), null, null, subject, meassage);
            }
           
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "SendgridSenEmail";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);  
            }
        }

        public static void UpdateAssignedUser(int TempAssignmentID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTable TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTables
                                                                  where row.ID == TempAssignmentID
                                                                  select row).FirstOrDefault();
                TempAssignmentIDToDeactive.UserID = UserID;
                entities.SaveChanges();
            }
        }
        public static void UpdateAssignedInternalUser(int TempAssignmentID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTableInternal TempAssignment = (from row in entities.TempAssignmentTableInternals
                                                              where row.ID == TempAssignmentID && row.ComplianceFlag == "S"
                                                              select row).FirstOrDefault();
                TempAssignment.UserID = UserID;
                entities.SaveChanges();
            }
        }

        public static void UpdateAssignedInternalCheckListUser(int TempAssignmentID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTableInternal TempAssignment = (from row in entities.TempAssignmentTableInternals
                                                              where row.ID == TempAssignmentID && row.ComplianceFlag == "C"
                                                              select row).FirstOrDefault();
                TempAssignment.UserID = UserID;
                entities.SaveChanges();
            }
        }
        public static List<TempAssignmentInternalView> GetTempAssignedInternalDetails(int lcCustomerBranch = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                List<TempAssignmentInternalView> TempAssignedDetails = (from row in entities.TempAssignmentInternalViews
                                                                        where row.CustomerBranchID == lcCustomerBranch
                                                                        && row.ComplianceFlag == "S"
                                                                        select row).ToList();

                return TempAssignedDetails;

            }
        }

        public static List<SP_TempAssignmentInternal_Result> GetTempAssignedInternalDetails(long CustomerID, long CustomerBranchID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_TempAssignmentInternal_Result> TempAssignedDetails = (from row in entities.SP_TempAssignmentInternal(CustomerID)
                                                                              where row.CustomerBranchID == CustomerBranchID
                                                                              orderby row.ID
                                                                              select row).ToList();
                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return TempAssignedDetails;

            }
        }
        public static List<SP_TempAssignmentInternal_Result> GetTempAssignedInternalDetails(long CustomerID, List<long> CustomerBranchlist, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<SP_TempAssignmentInternal_Result> TempAssignedDetails = (from row in entities.SP_TempAssignmentInternal(CustomerID)
                                                                              orderby row.ID
                                                                              select row).ToList();
                if (CustomerBranchlist.Count > 0)
                {
                    TempAssignedDetails = TempAssignedDetails.Where(entry => CustomerBranchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return TempAssignedDetails;

            }
        }
        public static List<SP_TempAssignmentInternalCheckList_Result> GetTempAssignedInternalCheckListDetails(long CustomerID, long CustomerBranchID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_TempAssignmentInternalCheckList_Result> TempAssignedDetails = (from row in entities.SP_TempAssignmentInternalCheckList(CustomerID)
                                                                                       where row.CustomerBranchID == CustomerBranchID
                                                                                       orderby row.ID
                                                                                       select row).ToList();
                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return TempAssignedDetails;

            }
        }
        public static List<SP_TempAssignmentInternalCheckList_Result> GetTempAssignedInternalCheckListDetails(long CustomerID, List<long> CustomerBranchlist, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                List<SP_TempAssignmentInternalCheckList_Result> TempAssignedDetails = (from row in entities.SP_TempAssignmentInternalCheckList(CustomerID)
                                                                                       orderby row.ID
                                                                                       select row).ToList();
                if (CustomerBranchlist.Count > 0)
                {
                    TempAssignedDetails = TempAssignedDetails.Where(entry => CustomerBranchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return TempAssignedDetails;



            }
        }

        public static List<SP_TempAssignment_Result> GetTempAssignedDetails(long CustomerID, long CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                List<SP_TempAssignment_Result> TempAssignedDetails = (from row in entities.SP_TempAssignment(CustomerID)
                                                                      where row.CustomerBranchID == CustomerBranchID
                                                                      orderby row.ComplianceID descending, row.RoleID ascending
                                                                      select row).ToList();

                return TempAssignedDetails;

            }
        }
        public static List<TempAssignmentView> GetTempAssignedDetails(int lcCustomerBranch = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                List<TempAssignmentView> TempAssignedDetails = (from row in entities.TempAssignmentViews
                                                                where row.CustomerBranchID == lcCustomerBranch
                                                                select row).ToList();

                return TempAssignedDetails;

            }
        }

        public static List<TempAssignmentView> GetTempAssignedDetails(List<int> lstCustBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TempAssignmentView> TempAssignedDetails = (from row in entities.TempAssignmentViews
                                                                where lstCustBranch.Contains(row.CustomerBranchID)
                                                                select row).ToList();

                return TempAssignedDetails;
            }
        }


        public static List<TempDeactivateInternalView> GetDetailsToInternalDeactive(int lcCustomerBranch = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TempDeactivateInternalView> TempGetDetailsToDeactivate = (from row in entities.TempDeactivateInternalViews
                                                                               where row.CustomerBranchID == lcCustomerBranch && row.ComplianceFlag == "S"
                                                                               select row).ToList();

                var TempTransactionDtls = (from row in entities.TempDeactivateInternalViews
                                           join ci in entities.InternalComplianceTransactions
                                           on row.InternalComplianceInstanceID equals ci.InternalComplianceInstanceID
                                           where ci.StatusId != 1
                                           select row.InternalComplianceInstanceID).ToList();

                TempGetDetailsToDeactivate.RemoveAll(a => TempTransactionDtls.Contains(a.InternalComplianceInstanceID));
                return TempGetDetailsToDeactivate;

            }
        }
        public static List<TempDeactivateView> GetDetailsToDeactive(int lcCustomerBranch = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TempDeactivateView> TempGetDetailsToDeactivate = (from row in entities.TempDeactivateViews
                                                                       where row.CustomerBranchID == lcCustomerBranch
                                                                       select row).ToList();

                var TempTransactionDtls = (from row in entities.TempDeactivateViews
                                           join ci in entities.ComplianceTransactions
                                           on row.ComplianceInstanceID equals ci.ComplianceInstanceId
                                           where ci.StatusId != 1
                                           select row.ComplianceInstanceID).ToList();

                TempGetDetailsToDeactivate.RemoveAll(a => TempTransactionDtls.Contains(a.ComplianceInstanceID));
                return TempGetDetailsToDeactivate;

            }
        }
        public static List<TempDeactivateView> GetDetailsToDeactive(List<long> lcCustomerBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TempDeactivateView> TempGetDetailsToDeactivate = (from row in entities.TempDeactivateViews
                                                                       where lcCustomerBranch.Contains(row.CustomerBranchID)
                                                                       select row).ToList();

                var TempTransactionDtls = (from row in entities.TempDeactivateViews
                                           join ci in entities.ComplianceTransactions
                                           on row.ComplianceInstanceID equals ci.ComplianceInstanceId
                                           where ci.StatusId != 1
                                           select row.ComplianceInstanceID).ToList();

                TempGetDetailsToDeactivate.RemoveAll(a => TempTransactionDtls.Contains(a.ComplianceInstanceID));
                return TempGetDetailsToDeactivate;

            }
        }
        public static void UpdateComplianceScheduleOn(long ComplianceID, long ComplianceInstanceID, long complianceScheduleOnID, DateTime Oldscheduleon, DateTime Newscheduleon, int forperiod)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn complianceScheduleOn = (from row in entities.ComplianceScheduleOns
                                                             where row.ID == complianceScheduleOnID
                                                             select row).FirstOrDefault();

                complianceScheduleOn.ScheduleOn = Newscheduleon;
                entities.SaveChanges();

                #region Update Task Schedule
                var taskScheduleIDList = (from row in entities.TaskScheduleOns
                                          where row.ComplianceScheduleOnID == complianceScheduleOnID
                                          && row.ForMonth == complianceScheduleOn.ForMonth
                                          && row.IsUpcomingNotDeleted == true && row.IsActive == true
                                          select row.ID).ToList();
                bool taskTransactionFlag = false;

                if (taskScheduleIDList.Count > 0)
                {
                    taskScheduleIDList.ForEach(entrytaskScheduleID =>
                    {
                        var complianceTransactions = (from row in entities.TaskTransactions
                                                      where row.TaskScheduleOnID == entrytaskScheduleID
                                                      && row.ComplianceScheduleOnID == complianceScheduleOnID
                                                      select row).ToList();

                        if (complianceTransactions.Count >= 2)
                        {
                            taskTransactionFlag = true;
                        }
                    });

                    if (taskTransactionFlag == false)
                    {
                        taskScheduleIDList.ForEach(entrytaskScheduleID =>
                        {

                            TaskScheduleOn taskScheduleOn = (from row in entities.TaskScheduleOns
                                                             where row.ID == entrytaskScheduleID
                                                               && row.ForMonth == complianceScheduleOn.ForMonth
                                                             select row).FirstOrDefault();

                            TaskInstance taskInstance = (from row in entities.TaskInstances
                                                         where row.ID == taskScheduleOn.TaskInstanceID
                                                         select row).FirstOrDefault();

                            Task task = (from row in entities.Tasks
                                         where row.ID == taskInstance.TaskId
                                         select row).FirstOrDefault();

                            DateTime taskOldScheduleOn;
                            if (task.IsAfter == true)
                            {
                                taskScheduleOn.ScheduleOn = Newscheduleon.AddDays(Convert.ToInt64(task.DueDays));
                                taskOldScheduleOn = Oldscheduleon.AddDays(Convert.ToInt64(task.DueDays));
                            }
                            else
                            {
                                taskScheduleOn.ScheduleOn = Newscheduleon.AddDays(-Convert.ToInt64(task.DueDays));
                                taskOldScheduleOn = Oldscheduleon.AddDays(-Convert.ToInt64(task.DueDays));
                            }
                            entities.SaveChanges();

                            var taskAssignments = (from row in entities.TaskAssignments
                                                   where row.TaskInstanceID == taskInstance.ID
                                                   select row).ToList();

                            taskAssignments.ForEach(taskAssignmentsentry =>
                            {
                                var taskReminders = (from row in entities.TaskReminders
                                                     where row.TaskAssignmentID == taskAssignmentsentry.ID
                                                     && row.TaskDueDate == taskOldScheduleOn
                                                     select row).ToList();
                                //Remove taskReminders
                                taskReminders.ForEach(entry =>
                                entities.TaskReminders.Remove(entry));
                                entities.SaveChanges();
                            });

                            var AssignedTaskRole = TaskManagment.GetAssignedTaskUsers((int)taskInstance.ID);

                            foreach (var roles in AssignedTaskRole)
                            {
                                if (roles.RoleID != 6)
                                {
                                    TaskManagment.CreateTaskReminders(taskInstance.ID, ComplianceID, roles.ID, Newscheduleon.AddDays(-Convert.ToInt64(task.DueDays)), complianceScheduleOnID, task.IsAfter);
                                }
                            }

                        });
                    }
                }

                #endregion

                var complianceAssignments = (from row in entities.ComplianceAssignments
                                             where row.ComplianceInstanceID == ComplianceInstanceID
                                             select row).ToList();

                complianceAssignments.ForEach(complianceAssignmentsentry =>
                {
                    var complianceReminders = (from row in entities.ComplianceReminders
                                               where row.ComplianceAssignmentID == complianceAssignmentsentry.ID
                                               && row.ComplianceDueDate == Oldscheduleon
                                               select row).ToList();
                    //Remove ComplianceReminders
                    complianceReminders.ForEach(entry =>
                    entities.ComplianceReminders.Remove(entry));
                    entities.SaveChanges();
                });

                if (ComplianceManagementComplianceScheduleon.CheckGenerateSchedule(ComplianceInstanceID))
                {
                    var AssignedRole = GetAssignedUsers(Convert.ToInt32(ComplianceInstanceID));

                    foreach (var roles in AssignedRole)
                    {
                        if (roles.RoleID != 6)
                        {
                            ComplianceManagementComplianceScheduleon.CreateReminders(ComplianceID, ComplianceInstanceID, roles.ID, Newscheduleon);
                        }
                    }
                }
            }
        }
        public static void ActivateTempAssignmentDetails(int TempAssignmentID, long ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTable TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTables
                                                                  where row.ID == TempAssignmentID
                                                                  select row).FirstOrDefault();
                TempAssignmentIDToDeactive.IsActive = true;
                entities.SaveChanges();

                ComplianceInstance ComplianceInstance = (from row in entities.ComplianceInstances
                                                         where row.ID == ComplianceInstanceId
                                                         select row).FirstOrDefault();
                ComplianceInstance.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static void DeactiveTempAssignmentDetails(int TempAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTable TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTables
                                                                  where row.ID == TempAssignmentID
                                                                  select row).FirstOrDefault();
                TempAssignmentIDToDeactive.IsActive = false;
                entities.SaveChanges();
            }
        }
        public static void ActivateInternalTempAssignmentDetails(int TempAssignmentID, long ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTableInternal TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTableInternals
                                                                          where row.ID == TempAssignmentID && row.ComplianceFlag == "S"
                                                                          select row).FirstOrDefault();
                TempAssignmentIDToDeactive.IsActive = true;
                entities.SaveChanges();

                InternalComplianceInstance ComplianceInstance = (from row in entities.InternalComplianceInstances
                                                                 where row.ID == ComplianceInstanceId
                                                                 select row).FirstOrDefault();
                ComplianceInstance.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static void DeactiveInternalTempAssignmentDetails(int TempAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTableInternal TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTableInternals
                                                                          where row.ID == TempAssignmentID && row.ComplianceFlag == "S"
                                                                          select row).FirstOrDefault();
                TempAssignmentIDToDeactive.IsActive = false;
                entities.SaveChanges();
            }
        }

        public static void DeactiveInternalCheckListTempAssignmentDetails(int TempAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTableInternal TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTableInternals
                                                                          where row.ID == TempAssignmentID && row.ComplianceFlag == "C"
                                                                          select row).FirstOrDefault();
                TempAssignmentIDToDeactive.IsActive = false;
                entities.SaveChanges();
            }
        }
        public static ComplianceReminderView GetComplianceReminderNotificationsByID(int reminderID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceReminder = (from row in entities.ComplianceReminderViews
                                          where row.ComplianceReminderID == reminderID
                                          select row).FirstOrDefault();

                return complianceReminder;
            }
        }


        public static List<ComplianceAssignedInstancesView> GetAllAssignedInstancesCheckList(int userID, int CustomerID, List<long> CustomerBranchlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.ComplianceType == 1
                                                select row).ToList();

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID).ToList();
                }

                if (CustomerBranchlist.Count > 0)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => CustomerBranchlist.Contains((int)entry.CustomerBranchID)).ToList();
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.ComplianceID).ToList(); ;
            }
        }

        public static List<ComplianceAssignedInstancesView> GetAllAssignedInstancesCheckList(int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.ComplianceType == 1
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Role.Contains(filter));
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.ComplianceInstanceID).ToList();

            }
        }
        public static List<ComplianceAssignedInstancesView> GetAllAssignedInstancesCheckListNew(int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.ComplianceType == 1
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }
                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.RoleID == 3);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Branch.Contains(filter) || entry.Role.Contains(filter) || entry.User.Contains(filter));
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.ComplianceInstanceID).ToList();

            }
        }
        public static List<ComplianceAssignedInstancesView> GetAllAssignedInstancesReassginExcludeNew(string EventFlag, int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
        {


            IQueryable<ComplianceAssignedInstancesView> complianceInstancesQuery = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var complianceinstanceidrevirwer = (from row in entities.ComplianceAssignments
                                                    where row.RoleID == 4 && row.UserID == userID
                                                    select row.ComplianceInstanceID).ToList();

                var userid = (from row in entities.ComplianceAssignments
                              where row.RoleID == 3 && complianceinstanceidrevirwer.Contains(row.ComplianceInstanceID)
                              select row.UserID
                                ).Distinct().ToList();



                if (EventFlag == "Y")
                {
                    complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.ComplianceType != 1 && row.EventFlag == true
                                                && userid.Contains((long)row.UserID)
                                                select row);
                }
                else
                {
                    complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.ComplianceType != 1 && row.EventFlag == null
                                                && userid.Contains((long)row.UserID)
                                                select row);
                }

                if (complianceinstanceidrevirwer.Count > 0)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => complianceinstanceidrevirwer.Contains(entry.ComplianceInstanceID));
                }

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }
                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.RoleID == 3);
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Branch.Contains(filter) || entry.Role.Contains(filter) || entry.User.Contains(filter));
                }

                return complianceInstancesQuery.OrderByDescending(entry => entry.ComplianceInstanceID).ToList();

            }
        }
        public static List<ComplianceReminderListView> GetComplianceReminderNotificationsByUserID(int userID, string filter = null)
        {
            DateTime date = DateTime.Now.Date;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                List<ComplianceReminderListView> complianceReminders = (from row in entities.ComplianceReminderListViews
                                                                        where row.UserID == userID && row.RemindOn >= date && row.Status == 0
                                                                        select row).OrderBy(entry => entry.RemindOn).ToList();


                if (!string.IsNullOrEmpty(filter))
                {
                    complianceReminders = complianceReminders.Where(entry => entry.CustomerBranchName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Description.ToUpper().Contains(filter.ToUpper()) || entry.Role.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return complianceReminders;
            }

        }

        public static List<ComplianceAssignedInstancesView> GetAllAssignedInstancesReassginExclude(string EventFlag, int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
        {

            IQueryable<ComplianceAssignedInstancesView> complianceInstancesQuery = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (EventFlag == "Y")
                {
                    complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.ComplianceType != 1 && row.EventFlag == true
                                                select row);
                }
                else
                {
                    complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.ComplianceType != 1 && row.EventFlag == null
                                                select row);
                }

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Role.Contains(filter));
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.ComplianceInstanceID).ToList();

            }
        }
        public static List<ComplianceAssignedInstancesView> GetAllAssignedInstances(int branchID = -1, int userID = -1, int customerId = -1, string filter = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.ComplianceType != 1
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                return complianceInstancesQuery.OrderByDescending(entry => entry.ComplianceInstanceID).ToList();

            }
        }

        public static List<ComplianceInstanceAssignmentView> GetAllInstances(int branchID = -1, int userID = -1, int customerId = -1, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.ComplianceInstanceAssignmentViews
                                                select row);

                if (customerId != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == customerId);
                }

                if (branchID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerBranchID == branchID);
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.ShortDescription.Contains(filter) || entry.Branch.Contains(filter) || entry.Role.Contains(filter));
                }

                return complianceInstancesQuery.OrderByDescending(entry => entry.ScheduledOn).ToList();

            }
        }
        public static void ReplaceUserForComplianceAssignmentnew(int oldUserID, int newUserID, List<Tuple<int, string, int, string>> ComplianceInstanceIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                foreach (var cil in ComplianceInstanceIdList)
                {
                    int role = -1;
                    if (cil.Item2.Equals("Performer"))
                        role = 3;
                    if (cil.Item2.Equals("Reviewer1"))
                        role = 4;
                    if (cil.Item2.Equals("Reviewer2"))
                        role = 5;
                    if (cil.Item2.Equals("Approver"))
                        role = 6;

                    var assignmentData = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == cil.Item1 && row.RoleID == role && row.UserID == cil.Item3
                                          select row).FirstOrDefault();

                    if (assignmentData != null)
                    {
                        assignmentData.UserID = newUserID;
                    }
                }
                entities.SaveChanges();




            }
        }
        public static void ReplaceUserForComplianceAssignment(int oldUserID, int newUserID, List<Tuple<int, string>> ComplianceInstanceIdList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                foreach (var cil in ComplianceInstanceIdList)
                {
                    int role = -1;
                    if (cil.Item2.Equals("Performer"))
                        role = 3;
                    if (cil.Item2.Equals("Reviewer1"))
                        role = 4;
                    if (cil.Item2.Equals("Reviewer2"))
                        role = 5;
                    if (cil.Item2.Equals("Approver"))
                        role = 6;

                    var assignmentData = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == cil.Item1 && row.RoleID == role && row.UserID == oldUserID
                                          select row).FirstOrDefault();

                    if (assignmentData != null)
                    {
                        assignmentData.UserID = newUserID;
                    }
                }
                entities.SaveChanges();
            }
        }

        public static void DeleteComplianceAssignment(int UserID, List<Tuple<int, string>> ComplianceInstanceIdList, int Customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var cil in ComplianceInstanceIdList)
                {
                    int role = -1;
                    if (cil.Item2.Equals("Performer"))
                        role = 3;
                    if (cil.Item2.Equals("Reviewer1"))
                        role = 4;
                    if (cil.Item2.Equals("Reviewer2"))
                        role = 5;
                    if (cil.Item2.Equals("Approver"))
                        role = 6;


                    #region Task Assignmnet Statutory Exclude

                    var compliance = TaskManagment.GetComplianceIDFromInstance(Convert.ToInt32(cil.Item1));

                    TaskManagment.DeleteTaskAssignmentFromCompliance(compliance.ComplianceId, compliance.CustomerBranchID, role, Customerid);

                    #endregion


                    var assignmentData = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == cil.Item1 && row.RoleID == role && row.UserID == UserID
                                          select row).FirstOrDefault();

                    List<ComplianceReminder> reminderData = (from row in entities.ComplianceReminders
                                                             where row.ComplianceAssignmentID == assignmentData.ID
                                                             select row).ToList();

                    reminderData.ForEach(entry =>
                           entities.ComplianceReminders.Remove(entry)
                        );

                    entities.ComplianceAssignments.Remove(assignmentData);
                }
                entities.SaveChanges();
            }
        }

        public static List<ComplianceAssignment> GetAssignedCompliances(long complianceID, int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.ComplianceInstances
                                             where row.ComplianceId == complianceID && row.CustomerBranchID == customerBranchID
                                             && row.IsDeleted == false
                                             select row.ID).SingleOrDefault();

                List<ComplianceAssignment> complianceAssignment = null;
                if (complianceInstanceID > 0)
                {
                    complianceAssignment = (from row in entities.ComplianceAssignments
                                            where row.ComplianceInstanceID == complianceInstanceID
                                            && (row.RoleID == 3 || row.RoleID == 4 || row.RoleID == 5)
                                            select row).ToList();
                }

                return complianceAssignment;
            }
        }
        public static void CreateReminder(ComplianceReminder reminder)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                reminder.ReminderTemplateID = null;
                reminder.Status = (byte)ReminderStatus.Pending;

                entities.ComplianceReminders.Add(reminder);
                entities.SaveChanges();
            }
        }

        public static void UpdateReminder(ComplianceReminder reminder)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                ComplianceReminder reminderToUpdate = (from row in entities.ComplianceReminders
                                                       where row.ID == reminder.ID
                                                       select row).FirstOrDefault();

                reminderToUpdate.ComplianceAssignmentID = reminder.ComplianceAssignmentID;
                reminderToUpdate.RemindOn = reminder.RemindOn;

                entities.SaveChanges();
            }
        }

        public static void UpdateReminders(List<int> reminders)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                reminders.ForEach(entry =>
                {
                    ComplianceReminder reminder = (from row in entities.ComplianceReminders
                                                   where row.ID == entry
                                                   select row).FirstOrDefault();
                    reminder.Status = 1;
                });

                entities.SaveChanges();
            }
        }
        public static List<SP_GetComplianceFormDetails_Result> GetComplianceFormDetails(int customerid)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetComplianceFormDetails(customerid)
                                        select row).ToList();

                return compliancesQuery;

            }
        }
        public static bool DeleteContractDocumentFile(long ID)
        {
            bool deleteSuccess = false;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objRecord = (from row in entities.ContractDocuments
                                 where row.ID == ID
                                 select row).ToList();

                if (objRecord != null)
                {
                    objRecord.ForEach(entry =>
                            entities.ContractDocuments.Remove(entry));
                    entities.SaveChanges();
                    deleteSuccess = true;
                }

                return deleteSuccess;
            }
        }

        public static bool DeleteTempContractDocumentFile(long ID)
        {
            bool deleteSuccess = false;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objRecord = (from row in entities.TempContractDocuments
                                 where row.ID == ID
                                 select row).ToList();

                if (objRecord != null)
                {
                    objRecord.ForEach(entry =>
                            entities.TempContractDocuments.Remove(entry));
                    entities.SaveChanges();
                    deleteSuccess = true;
                }

                return deleteSuccess;
            }
        }
        public static TempContractDocument GetTempContractDocument(long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var temp = (from row in entities.TempContractDocuments
                            where row.ID == ID
                            select row).FirstOrDefault();

                return temp;
            }
        }
        public static ContractDocument GetContractDocument(long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var temp = (from row in entities.ContractDocuments
                            where row.ID == ID
                            select row).FirstOrDefault();

                return temp;
            }
        }
        public static long GetComplianceID(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long branchid = (from row in entities.ComplianceInstances
                                 where row.ID == complianceInstanceID
                                 select row.ComplianceId).FirstOrDefault();

                return branchid;
            }
        }
        public static List<SP_GetComplianceManagementMaster_Result> GetComplianceManagementMaster(int CustId)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetComplianceManagementMaster(CustId)
                                        select row).ToList();

                return compliancesQuery;

            }
        }

        public static ComplianceInfoByCustomer GetDetailsCustomerID(int CustID, int CompID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var details = (from row in entities.ComplianceInfoByCustomers
                               where row.CustomerID == CustID
                               && row.ComplianceID == CompID
                               && row.IsActive == true
                               select row).FirstOrDefault();

                return details;
            }
        }
        public static ComplianceUpdatedCustomer GetCustomerID(int CustID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusID = (from row in entities.ComplianceUpdatedCustomers
                                       where row.CustID == CustID
                                       select row).FirstOrDefault();

                return currentStatusID;
            }
        }
        public static void BindStatutoryCheckListDatainRedis(long ComplianceInstanceID, int Customerid)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int StatusFlag = -1;
                    var getComplianceStatus = (from row in entities.ComplianceInstances
                                               join row1 in entities.Compliances
                                               on row.ComplianceId equals row1.ID
                                               where row.ID == ComplianceInstanceID
                                               select row1).FirstOrDefault();

                    if (getComplianceStatus != null)
                    {
                        if (getComplianceStatus.EventFlag != null)
                            StatusFlag = 4;
                        else
                            StatusFlag = 2;
                    }
                    if (StatusFlag != -1)
                    {
                        var statusList = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == ComplianceInstanceID
                                          select row).ToList();
                        if (statusList.Count > 0)
                        {
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetChecklistEventData_" + userID + "_" + Customerid + "_PRA_" + StatusFlag;
                                }
                                else
                                {
                                    CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_PRA_" + StatusFlag;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetChecklistEventData_" + userID + "_" + Customerid + "_REV_" + StatusFlag;
                                }
                                else
                                {
                                    CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_REV_" + StatusFlag;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Select(y => y.UserID).FirstOrDefault());
                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetChecklistEventData_" + userID + "_" + Customerid + "_APPR_" + StatusFlag;
                                }
                                else
                                {
                                    CacheName = "CacheGetChecklistEventData_" + userID + "_" + Customerid + "_APPR_" + StatusFlag;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                        }

                        var UserList = (from row in entities.ComplianceInstances
                                        join row1 in entities.Compliances
                                        on row.ComplianceId equals row1.ID
                                        join row2 in entities.Acts
                                        on row1.ActID equals row2.ID
                                        join row3 in entities.EntitiesAssignments
                                        on (long)row2.ComplianceCategoryId equals row3.ComplianceCatagoryID
                                        join row4 in entities.Users
                                        on row3.UserID equals row4.ID
                                        where row.ID == ComplianceInstanceID && row1.IsDeleted == false
                                        && row2.IsDeleted == false && row4.IsDeleted == false
                                         && row4.IsActive == true && row4.RoleID == 8
                                        select row3.UserID).ToList();

                        UserList = UserList.Distinct().ToList();

                        foreach (var item in UserList)
                        {
                            string CacheName = string.Empty;

                            if (objlocal == "Local")
                            {
                                CacheName = "Cache_GetChecklistEventData_" + item + "_" + Customerid + "_MGMT_" + StatusFlag;
                            }
                            else
                            {
                                CacheName = "CacheGetChecklistEventData_" + item + "_" + Customerid + "_MGMT_" + StatusFlag;
                            }

                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                {
                                    StackExchangeRedisExtensions.Remove(CacheName);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static void BindDatainRedis(long ComplianceInstanceID, int Customerid)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string type = string.Empty;

                    var getComplianceStatus = (from row in entities.ComplianceInstances
                                               join row1 in entities.Compliances
                                               on row.ComplianceId equals row1.ID
                                               where row.ID == ComplianceInstanceID
                                               select row1).FirstOrDefault();

                    if (getComplianceStatus != null)
                    {
                        if (getComplianceStatus.EventFlag != null)
                            type = "Event Based";
                        else
                            type = "Statutory";
                    }

                    if (!string.IsNullOrEmpty(type))
                    {
                        var statusList = (from row in entities.ComplianceAssignments
                                          where row.ComplianceInstanceID == ComplianceInstanceID
                                          select row).ToList();
                        if (statusList.Count > 0)
                        {
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 3).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetStatutoryEventData_" + userID + "_" + Customerid + "_PRA_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetStatutoryEventData_" + userID + "_" + Customerid + "_PRA_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 4).Select(y => y.UserID).FirstOrDefault());

                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetStatutoryEventData_" + userID + "_" + Customerid + "_REV_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetStatutoryEventData_" + userID + "_" + Customerid + "_REV_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                            if (Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Count()) >= 1)
                            {
                                string CacheName = string.Empty;
                                int userID = Convert.ToInt32(statusList.Where(x => x.RoleID == 6).Select(y => y.UserID).FirstOrDefault());
                                if (objlocal == "Local")
                                {
                                    CacheName = "Cache_GetStatutoryEventData_" + userID + "_" + Customerid + "_APPR_" + type;
                                }
                                else
                                {
                                    CacheName = "CacheGetStatutoryEventData_" + userID + "_" + Customerid + "_APPR_" + type;
                                }

                                try
                                {
                                    if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                    {
                                        StackExchangeRedisExtensions.Remove(CacheName);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                                }
                            }
                        }

                        var UserList = (from row in entities.ComplianceInstances
                                        join row1 in entities.Compliances
                                        on row.ComplianceId equals row1.ID
                                        join row2 in entities.Acts
                                        on row1.ActID equals row2.ID
                                        join row3 in entities.EntitiesAssignments
                                        on (long)row2.ComplianceCategoryId equals row3.ComplianceCatagoryID
                                        join row4 in entities.Users
                                        on row3.UserID equals row4.ID
                                        where row.ID == ComplianceInstanceID && row1.IsDeleted == false
                                        && row2.IsDeleted == false && row4.IsDeleted == false
                                         && row4.IsActive == true && row4.RoleID == 8
                                        select row3.UserID).ToList();

                        UserList = UserList.Distinct().ToList();

                        foreach (var item in UserList)
                        {
                            string CacheName = string.Empty;

                            if (objlocal == "Local")
                            {
                                CacheName = "Cache_GetStatutoryEventData_" + item + "_" + Customerid + "_MGMT_" + type;
                            }
                            else
                            {
                                CacheName = "CacheGetStatutoryEventData_" + item + "_" + Customerid + "_MGMT_" + type;
                            }

                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(CacheName))
                                {
                                    StackExchangeRedisExtensions.Remove(CacheName);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogLibrary.WriteErrorLog("SET all detail report record Error exception :" + CacheName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static List<TempComplianceDocument> GetTempTaskDocumentDataAWS(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.TempComplianceDocuments
                                    where row.ScheduleOnID == ScheduleOnID
                                    && (row.DocType == "T")
                                    select row).ToList();

                return instanceData;
            }
        }

        public static List<TempComplianceDocument> GetTempInternalComplianceDocumentDataAWS(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.TempComplianceDocuments
                                    where row.ScheduleOnID == ScheduleOnID
                                    && (row.DocType == "IS" || row.DocType == "IW" || row.DocType == "IC")
                                    select row).ToList();

                return instanceData;
            }
        }

        public static List<TempComplianceDocument> GetTempComplianceDocumentDataAWS(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.TempComplianceDocuments
                                    where row.ScheduleOnID == ScheduleOnID
                                    && (row.DocType == "S" || row.DocType == "W" || row.DocType == "C")
                                    select row).ToList();

                return instanceData;
            }
        }

        public static Compliance GetComplianceByID(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Compliances
                                  where row.ID == complianceID
                                  select row).FirstOrDefault();
                return compliance;
            }
        }
        public static void CreateEventTypeMapping(EventTypeDataMapping eventmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventTypeDataMappings.Add(eventmapping);
                entities.SaveChanges();
            }
        }
        public static void UpdateEventTypeMappedID(long eventId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var ids = (from row in entities.EventTypeDataMappings
                           where row.EventId == eventId
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    EventTypeDataMapping prevmappedids = (from row in entities.EventTypeDataMappings
                                                          where row.ID == entry
                                                          select row).FirstOrDefault();
                    entities.EventTypeDataMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }

        }

        public static List<int> GetEventTypeMappedID(long EventId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EventTypeMappedIDs = (from row in entities.EventTypeDataMappings
                                          where row.EventId == EventId
                                          select row.EventTypeID).ToList();

                return EventTypeMappedIDs;
            }

        }
        public static string GetEmail(long UserID)
        {
            string result = string.Empty;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from row in entities.Users
                              where row.IsActive == true
                              && row.IsDeleted == false
                              && row.ID == UserID
                              select row.Email).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                return "";
            }
        }
        public static string getToken(string UID)
        {
            string UserEmail = GetEmail(Convert.ToInt64(UID));
            string outputToken = "";
            try
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    string baseAddress = ConfigurationManager.AppSettings["KendoPathApp"].ToString();
                    string requestUrl = baseAddress + "Data/GetTokenLoginUser?Email=" + UserEmail;
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    string responseData = WebAPIUtility.Invoke("GET", requestUrl, "");
                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var lst = JsonConvert.DeserializeObject<List<AvacomTokenDetails>>(responseData);
                        if (lst.Count > 0)
                        {
                            outputToken = lst[0].tokendetail.ToString();
                        }
                    }
                }
                return outputToken;
            }
            catch (Exception ex)
            {
                return outputToken;
            }

            #region another option code
            //string outputToken = "";
            //try
            //{
            //    string UserEmail = "cfo@bitaconsulting.co.in";

            //    string getLinkPath = ConfigurationManager.AppSettings["KendoPathApp"] + "/Data/GetTokenLoginUser?Email=" + UserEmail;

            //    object input = new
            //    {
            //        Name = UserEmail,
            //    };
            //    string inputJson = (new JavaScriptSerializer()).Serialize(input);
            //    WebClient client = new WebClient();
            //    client.Headers["Content-type"] = "application/json";
            //    client.Encoding = Encoding.UTF8;
            //    string json = client.UploadString(getLinkPath, inputJson);

            //    List<AvacomTokenDetails> objTest = new List<AvacomTokenDetails>();
            //    objTest = (new JavaScriptSerializer()).Deserialize<List<AvacomTokenDetails>>(json);
            //    if (objTest.Count > 0)
            //    {
            //        outputToken = objTest[0].tokendetail.ToString();
            //    }
            //    return outputToken;
            //}
            //catch (Exception ex)
            //{
            //    return outputToken;
            //}
            #endregion
        }

        public static log_ChangeScheduleOn GetOriginalDueDate(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.log_ChangeScheduleOn
                             where row.ScheduleOnID == scheduledOnID
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).FirstOrDefault();

                return query;
            }
        }
        public static bool GetCurrentStatusByScheduleONID(int ScheduledOnID, string ComplianceType, int CustID)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    result = (from row in entities.SP_DetailLockingCompliance(ComplianceType, CustID)
                              where row.ScheduleOnID == ScheduledOnID
                              select row.IsLock).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "GetCurrentStatusByScheduleONID";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                return false;
            }
        }
        public static List<ComplianceInstanceTransactionView> NewGetPeriodBranchLocation(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceInstanceTransactionViews
                             where row.ScheduledOnID == scheduledOnID
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).ToList();

                return query;
            }
        }

        public static List<SP_GetComplianceMaster_Result> GetComplianceMaster()
        {
          
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetComplianceMaster()
                                        select row).ToList();

                return compliancesQuery;

            }
        }

        public static List<SP_GetNotMappedComplianceMaster_Result> GetNotMappedComplianceMaster()
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetNotMappedComplianceMaster()
                                        select row).ToList();

                return compliancesQuery;

            }
        }
        public static bool CreateTransaction(ComplianceTransaction transaction, List<FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Statutory_SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.ComplianceScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();

                            try
                            {
                                CreateTransaction_Secretarial(Convert.ToInt64(transaction.ComplianceScheduleOnID), transaction.StatusId, Convert.ToDateTime(transaction.StatusChangedOn), Convert.ToInt32(transaction.CreatedBy));
                            }
                            catch (Exception ex)
                            {
                                ContractManagement.InsertErrorMsg_DBLog("CreateTransaction_Secretarial", "CreateTransaction_Secretarial", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");


                return false;
            }
        }
        public static bool CreateTransactionRevise(ComplianceTransaction transaction, List<FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);
                            if (AWSData == null)
                            {
                                DocumentManagement.Statutory_SaveDocFiles(filesList);
                            }

                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.ComplianceScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();

                            try
                            {
                                CreateTransaction_Secretarial(Convert.ToInt64(transaction.ComplianceScheduleOnID), transaction.StatusId, Convert.ToDateTime(transaction.StatusChangedOn), Convert.ToInt32(transaction.CreatedBy));
                            }
                            catch (Exception ex)
                            {
                                ContractManagement.InsertErrorMsg_DBLog("CreateTransaction_Secretarial", "CreateTransaction_Secretarial", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");
                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }

        public static bool CreateTransaction(ComplianceTransaction transaction, List<FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList, string DirectoryPath, long ScheduleOnID, int CustId)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                AmazonS3.SaveDocFilesAWSStorage(filesList, DirectoryPath, ScheduleOnID, AWSData.BucketName, AWSData.AccessKeyID, AWSData.SecretKeyID);
                            }
                            else
                            {
                                DocumentManagement.Statutory_SaveDocFiles(filesList);
                            }
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.ComplianceScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();

                            try
                            {
                                CreateTransaction_Secretarial(Convert.ToInt64(transaction.ComplianceScheduleOnID), transaction.StatusId, Convert.ToDateTime(transaction.StatusChangedOn), Convert.ToInt32(transaction.CreatedBy));
                            }
                            catch (Exception ex)
                            {
                                ContractManagement.InsertErrorMsg_DBLog("CreateTransaction_Secretarial", "CreateTransaction_Secretarial", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }

        public static bool CreateTransactionBulkGST(ComplianceTransaction transaction, List<FileData> files, List<KeyValuePair<string, int>> list)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            //DocumentManagement.SaveDocFiles(filesList);
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.ComplianceScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);

                                    //GST Bulk Compliance performm log   
                                    GSTComplianceFileLog filedatalog = new GSTComplianceFileLog();
                                    filedatalog.FileID = fl.ID;
                                    filedatalog.ScheduleOnID = transaction.ComplianceScheduleOnID;
                                    filedatalog.PerformReviewFlag = "Perform";
                                    filedatalog.CreatedOn = DateTime.Now;
                                    entities.GSTComplianceFileLogs.Add(filedatalog);
                                    entities.SaveChanges();
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransactionBulkGST";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransactionBulkGST Function", "CreateTransactionBulkGST");

                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }
        public static bool CreateTransaction_Secretarial(long scheduleOnID, int statusID, DateTime dated, int userID)
        {
            bool saveSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.BM_ComplianceScheduleOnNew
                                      where row.ScheduleOnID == scheduleOnID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        long? tempMeetingId = null;
                        DateTime closedDate = dated;
                        if (!DateTime.TryParse(dated.ToString("dd/MMM/yyyy"), out closedDate))
                        {
                            closedDate = dated;
                        }

                        if (prevRecord.StatusId != statusID)
                        {
                            prevRecord.StatusId = statusID;
                            prevRecord.UpdatedOn = DateTime.Now;
                        }

                        if (statusID == 4 || statusID == 5)
                        {
                            #region Set Open Agenda On Compliance Completion

                            var OnComplianceCompletion = (from row in entities.ComplianceScheduleOns
                                                          join instance in entities.ComplianceInstances on row.ComplianceInstanceID equals instance.ID
                                                          join mapping in entities.BM_ComplianceMapping on instance.ComplianceId equals mapping.ComplianceID
                                                          join agenda in entities.BM_AgendaMaster on mapping.AgendaMasterID equals agenda.BM_AgendaMasterId
                                                          where row.ID == scheduleOnID
                                                          && agenda.FrequencyId == 15
                                                          select new
                                                          {
                                                              //row.EntityId,
                                                              agenda.BM_AgendaMasterId,
                                                              agenda.EntityType,
                                                              agenda.MeetingTypeId
                                                          }).FirstOrDefault();

                            if (OnComplianceCompletion != null)
                            {
                                var openAgenda = new BM_MeetingOpenAgendaOnCompliance()
                                {
                                    Id = 0,
                                    ScheduleOnID = scheduleOnID,
                                    EntityId = prevRecord.EntityId != null ? (int)prevRecord.EntityId : 0,
                                    AgendaId = OnComplianceCompletion.BM_AgendaMasterId,
                                    MeetingTypeId = (int)OnComplianceCompletion.MeetingTypeId,
                                    IsNoted = false,
                                    IsDeleted = false,
                                    CreatedBy = userID,
                                    CreatedOn = DateTime.Now
                                };

                                entities.BM_MeetingOpenAgendaOnCompliance.Add(openAgenda);
                                entities.SaveChanges();
                            }
                            #endregion

                            prevRecord.ClosedDate = closedDate;

                            tempMeetingId = prevRecord.MeetingID;
                        }
                        entities.SaveChanges();

                        if (tempMeetingId > 0)
                        {
                            entities.BM_SP_ComplianceTransactionOnClosed(tempMeetingId, userID);
                            entities.SaveChanges();
                        }

                        saveSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                saveSuccess = false;
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return saveSuccess;
        }
        public static bool CreateTransaction(ComplianceTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            dbtransaction.Commit();

                            try
                            {
                                CreateTransaction_Secretarial(Convert.ToInt64(transaction.ComplianceScheduleOnID), transaction.StatusId, Convert.ToDateTime(transaction.StatusChangedOn), Convert.ToInt32(transaction.CreatedBy));
                            }
                            catch (Exception ex)
                            {
                                ContractManagement.InsertErrorMsg_DBLog("CreateTransaction_Secretarial", "CreateTransaction_Secretarial", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }
        public static void CreateTransaction(List<ComplianceTransaction> transaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                transaction.ForEach(entry =>
                {
                    entry.Dated = DateTime.Now;
                    //entry.Dated = DateTime.UtcNow; Comment on 19 JAN 2017 Sachin Nikam
                    entities.ComplianceTransactions.Add(entry);
                });

                entities.SaveChanges();

                transaction.ForEach(eachTransaction =>
                {
                    try
                    {
                        CreateTransaction_Secretarial(Convert.ToInt64(eachTransaction.ComplianceScheduleOnID), eachTransaction.StatusId, Convert.ToDateTime(eachTransaction.StatusChangedOn), Convert.ToInt32(eachTransaction.CreatedBy));
                    }
                    catch (Exception ex)
                    {
                        ContractManagement.InsertErrorMsg_DBLog("CreateTransaction_Secretarial", "CreateTransaction_Secretarial", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                });

            }
        }
        public static void CreateCompliance_Log(Compliance_Log complog)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Compliance_Log.Add(complog);
                entities.SaveChanges();
            }
        }
        public static bool CreateTransactionBulkReviseGST(ComplianceTransaction transaction, List<FileData> files, List<KeyValuePair<string, int>> list)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            if (files != null)
                            {
                                foreach (FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.FileDatas.Add(fl);
                                    entities.SaveChanges();

                                    FileDataMapping fileMapping = new FileDataMapping();
                                    fileMapping.TransactionID = transactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = transaction.ComplianceScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.FileDataMappings.Add(fileMapping);

                                    //GST Bulk Compliance performm log   
                                    GSTComplianceFileLog filedatalog = new GSTComplianceFileLog();
                                    filedatalog.FileID = fl.ID;
                                    filedatalog.ScheduleOnID = transaction.ComplianceScheduleOnID;
                                    filedatalog.PerformReviewFlag = "Revise";
                                    filedatalog.CreatedOn = DateTime.Now;
                                    entities.GSTComplianceFileLogs.Add(filedatalog);
                                    entities.SaveChanges();
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }
        public static ComplianceInstanceTransactionView GetPeriodBranchLocationRevise(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceInstanceTransactionViews
                             where row.ScheduledOnID == scheduledOnID
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).FirstOrDefault();

                return query;
            }
        }
        public static bool CreateTransactionBulkReviewRejectGST(ComplianceTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            long transactionId = transaction.ID;
                            dbtransaction.Commit();

                            //GST Bulk Compliance performm log   
                            GSTComplianceFileLog filedatalog = new GSTComplianceFileLog();
                            //filedatalog.FileID = fl.ID;
                            filedatalog.ScheduleOnID = transaction.ComplianceScheduleOnID;
                            filedatalog.CreatedOn = DateTime.Now;
                            filedatalog.PerformReviewFlag = "Reject";
                            entities.GSTComplianceFileLogs.Add(filedatalog);
                            entities.SaveChanges();
                            return true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }
        public static bool CreateTransactionBulkReviewGST(ComplianceTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();

                            long transactionId = transaction.ID;
                            dbtransaction.Commit();

                            //GST Bulk Compliance performm log   
                            GSTComplianceFileLog filedatalog = new GSTComplianceFileLog();
                            //filedatalog.FileID = fl.ID;
                            filedatalog.ScheduleOnID = transaction.ComplianceScheduleOnID;
                            filedatalog.PerformReviewFlag = "Review";
                            filedatalog.CreatedOn = DateTime.Now;
                            entities.GSTComplianceFileLogs.Add(filedatalog);
                            entities.SaveChanges();
                            return true;
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }

        public static GSTComplianceMapping GetGSTApplicableCustomer(int CustomerID, long complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceList = (from row in entities.GSTComplianceMappings
                                      where row.CustomerID == CustomerID
                                      && row.ComplianceID == complianceid
                                      select row).FirstOrDefault();

                return complianceList;
            }
        }
      
        public static List<long> GetGSTCOmplianceInstanceList(int CustomerID, long ComplianceID, long ComplianceInstanceID, string GstGroupName)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceList = (from row in entities.ComplianceInstances
                                      join row1 in entities.GSTComplianceMappings
                                      on row.ComplianceId equals row1.ComplianceID
                                      where row1.GSTGroup == GstGroupName
                                      && row.IsDeleted == false && row1.CustomerID == CustomerID
                                      && row.ID != ComplianceInstanceID
                                      select row.ID).ToList();

                return complianceList;
            }
        }

        public static string GetGSTCOmplianceGroupName(int CustomerID, long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var GSTGroup = (from row in entities.GSTComplianceMappings
                                where row.ComplianceID == ComplianceID
                                && row.CustomerID == CustomerID
                                select row.GSTGroup).FirstOrDefault();

                return GSTGroup;
            }
        }
        public static List<GetComplianceDocumentsView> GetDocumnetsList(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DocumentList = (from row in entities.GetComplianceDocumentsViews
                                    where row.ScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return DocumentList;
            }
        }
        public static List<GetInternalComplianceDocumentsView> GetInternalComplianceDocumentDetails(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DocumentList = (from row in entities.GetInternalComplianceDocumentsViews
                                    where row.InternalComplianceScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return DocumentList;
            }
        }
       
    
        public static long? GetComplianceInstanceID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceInstanceID = (from row in entities.ComplianceScheduleOns
                                            where row.ID == ScheduleOnID
                                            select row.ComplianceInstanceID).FirstOrDefault();
                return ComplianceInstanceID;
            }
        }
        public static ComplianceTransaction GetPenaltyTransactionReviewer(long ComplianceScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction Transaction = (from row in entities.ComplianceTransactions
                                                     where row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                     && (row.StatusId == 5)
                                                     select row).OrderByDescending(m => m.ID).FirstOrDefault();

                return Transaction;
            }
        }
        public static SP_GetDeactivateAct_Result GetAct(long ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliancesQuery = (from row in entities.SP_GetDeactivateAct()
                                        where row.Old_Act_ID == ActID
                                        select row).FirstOrDefault();

                return compliancesQuery;

            }
        }

        public static ComplianceDeactive GetActDeactivate(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.ComplianceDeactives
                            where row.ComplianceID == complianceID
                            select row).SingleOrDefault();

                return form;
            }
        }
        public static SP_Check_UserLeavePeriodExists_Result GetUserLeavePeriodExists(long userid, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var taskReminders = (from row in entities.SP_Check_UserLeavePeriodExists(userid, flag)
                                     select row).FirstOrDefault();
                return taskReminders;
            }
        }
        public static ClientBasedCheckListFrequency GetClientBasedCheckListFrequency(long customerid,long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.ClientBasedCheckListFrequencies
                            where row.ClientID == customerid
                            && row.ComplianceID== ComplianceID
                            select row).FirstOrDefault();

                return form;
            }
        }
        public static bool GetStatutorycomplianceByType(long ComplianceID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean flag = false;
                var form = (from row in entities.Compliances
                            join row1 in entities.AuditChecklistComplianceMappings
                            on row.ID equals row1.ComplianceID
                            where row1.ComplianceType == "S"
                            && row1.Isactive == false && CustomerID == row1.CustomerID
                            && row1.ComplianceID == ComplianceID
                            select row1).FirstOrDefault();

                if (form == null)
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                }

                return flag;
            }
        }
        public static List<SP_GetAssignedActsToUserNew_Result> GetAssignedActDetils(int userId, int RoleId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var result = entities.SP_GetAssignedActsToUserNew(userId, RoleId).ToList();
                return result;
            }
        }
        public static void DeleteTempAssignmentInternalTable(int TempAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TempAssignmentTableInternals
                             where row.ID == TempAssignmentID
                             select row).FirstOrDefault();

                entities.TempAssignmentTableInternals.Remove(query);
                entities.SaveChanges();
            }
        }
       
        public static List<ComplianceAssignedInstancesView> GetAllAssignedInstanceslist(int userID, int CustomerID, List<long> CustomerBranchlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceInstancesQuery = (from row in entities.ComplianceAssignedInstancesViews
                                                where row.CustomerID== CustomerID
                                                select row).ToList();

                if (CustomerID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.CustomerID == CustomerID).ToList();
                }

                if (CustomerBranchlist.Count > 0)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => CustomerBranchlist.Contains((int)entry.CustomerBranchID)).ToList();
                }

                if (userID != -1)
                {
                    complianceInstancesQuery = complianceInstancesQuery.Where(entry => entry.UserID == userID).ToList(); ;
                }
                return complianceInstancesQuery.OrderByDescending(entry => entry.ComplianceInstanceID).ToList();

            }
        }
        public static List<SP_TempAssignmentCheckList_Result> GetTempAssignedCheckListDetailslist(long CustomerID, List<long> CustomerBranchlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var TempAssignedDetails = (from row in entities.SP_TempAssignmentCheckList(CustomerID)
                                           orderby row.ComplianceID descending, row.RoleID ascending
                                                                         select row).ToList();

                if (CustomerBranchlist.Count > 0)
                {
                    TempAssignedDetails = TempAssignedDetails.Where(entry => CustomerBranchlist.Contains((int)entry.CustomerBranchID)).ToList();
                }
                return TempAssignedDetails;

            }
        }
        public static List<SP_TempAssignment_Result> GetTempAssignedDetails(long CustomerID, List<long> CustomerBranchlist, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_TempAssignment_Result> TempAssignedDetails = (from row in entities.SP_TempAssignment(CustomerID)
                                                                      orderby row.ID
                                                                      //orderby row.ComplianceID descending, row.RoleID ascending
                                                                      select row).ToList();
                if (CustomerBranchlist.Count>0)
                {
                    TempAssignedDetails = TempAssignedDetails.Where(entry => CustomerBranchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Users.ToUpper().Contains(filter.ToUpper()) || entry.Role.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return TempAssignedDetails;

            }
        }
        public static List<SP_TempAssignment_Result> GetTempAssignedDetails(long CustomerID, long CustomerBranchID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_TempAssignment_Result> TempAssignedDetails = (from row in entities.SP_TempAssignment(CustomerID)
                                                                      where row.CustomerBranchID == CustomerBranchID
                                                                      orderby row.ID
                                                                      select row).ToList();
                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ComplianceID == a).ToList();
                    }
                    else
                    {
                        TempAssignedDetails = TempAssignedDetails.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Users.ToUpper().Contains(filter.ToUpper()) || entry.Role.ToUpper().Contains(filter.ToUpper())).ToList();

                    }
                }

                return TempAssignedDetails;

            }
        }
        public static void DeleteTempAssignmentChecklistTable(int TempAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TempAssignmentTableCheckLists
                             where row.ID == TempAssignmentID
                             select row).FirstOrDefault();

                entities.TempAssignmentTableCheckLists.Remove(query);
                entities.SaveChanges();
            }
        }
        public static void DeleteTempAssignmentTable(int TempAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TempAssignmentTables
                             where row.ID == TempAssignmentID
                             select row).FirstOrDefault();

                entities.TempAssignmentTables.Remove(query);
                entities.SaveChanges();
            }
        }
        public static bool DeleteTempTaskDocumentFileFromScheduleOnID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TempComplianceDocuments.RemoveRange(entities.TempComplianceDocuments.Where(x => x.ScheduleOnID == ScheduleOnID
                && x.DocType == "T"));

                entities.SaveChanges();
                return true;
            }
        }

        public static bool DeleteTempInternalDocumentFileFromScheduleOnID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TempComplianceDocuments.RemoveRange(entities.TempComplianceDocuments.Where(x => x.ScheduleOnID == ScheduleOnID
                   && (x.DocType == "IS" || x.DocType == "IW")));

                entities.SaveChanges();
                return true;
            }
        }

        public static bool DeleteTempInternalChecklistDocumentFileFromScheduleOnID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TempComplianceDocuments.RemoveRange(entities.TempComplianceDocuments.Where(x => x.ScheduleOnID == ScheduleOnID
                   && x.DocType == "IC"));

                entities.SaveChanges();
                return true;
            }
        }
        public static bool DeleteTempDocumentFileFromScheduleOnID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.TempComplianceDocuments.RemoveRange(entities.TempComplianceDocuments.Where(x => x.ScheduleOnID == ScheduleOnID
                   && (x.DocType == "S" || x.DocType == "W")));

                entities.SaveChanges();

                return true;
            }
        }
        public static bool DeleteTempDocumentChecklistFileFromScheduleOnID(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.TempComplianceDocuments.RemoveRange(entities.TempComplianceDocuments.Where(x => x.ScheduleOnID == ScheduleOnID
                   && x.DocType == "C"));

                entities.SaveChanges();

                return true;
            }
        }
        public static List<TempComplianceDocument> GetTempComplianceDocumentData(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.TempComplianceDocuments
                                    where row.ScheduleOnID == ScheduleOnID
                                    && (row.DocType =="S" || row.DocType == "W")
                                    select row).ToList();

                return instanceData;
            }
        }

        public static List<TempComplianceDocument> GetTempInternalChecklistDocumentData(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.TempComplianceDocuments
                                    where row.ScheduleOnID == ScheduleOnID
                                    && (row.DocType == "IC")
                                    select row).ToList();

                return instanceData;
            }
        }
        public static List<TempComplianceDocument> GetTempTaskDocumentData(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.TempComplianceDocuments
                                    where row.ScheduleOnID == ScheduleOnID
                                    && row.DocType == "T"
                                    select row).ToList();

                return instanceData;
            }
        }

        public static List<TempComplianceDocument> GetTempInternalComplianceDocumentData(long ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.TempComplianceDocuments
                                    where row.ScheduleOnID == ScheduleOnID
                                    && (row.DocType == "IS" || row.DocType == "IW")
                                    select row).ToList();

                return instanceData;
            }
        }
        public static TempComplianceDocument GetTempComplianceDocument(long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var temp = (from row in entities.TempComplianceDocuments
                            where row.ID == ID
                            select row).FirstOrDefault();

                return temp;
            }

        }
        public static bool DeleteTempDocumentFile(long ID)
        {
            bool deleteSuccess = false;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objRecord = (from row in entities.TempComplianceDocuments
                                 where row.ID == ID
                                 select row).ToList();

                if (objRecord != null)
                {
                    objRecord.ForEach(entry =>
                            entities.TempComplianceDocuments.Remove(entry));
                    entities.SaveChanges();
                    deleteSuccess = true;
                }

                return deleteSuccess;
            }
        }
        public static string GetAuditChecklistName(long complianceID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.AuditChecklists
                            join row1 in entities.AuditChecklistComplianceMappings
                            on row.ID equals row1.ChecklistID
                            where row1.ComplianceID == complianceID
                            && row1.ComplianceType == "S"
                            && row.CustomerID == CustomerID
                            select row.SrNo + " " + row.Name).FirstOrDefault();

                return form;
            }
        }
     

        public static string GetAuditChecklistNameInternal(long complianceID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.AuditChecklists
                            join row1 in entities.AuditChecklistComplianceMappings
                            on row.ID equals row1.ChecklistID
                            where row1.ComplianceID == complianceID
                            && row1.ComplianceType == "I"
                             && row.CustomerID == CustomerID
                            select row.SrNo + " " + row.Name).FirstOrDefault();

                return form;
            }
        }
        public static string GetNatureOfComplianceFromID(int NatureOfComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.NatureOfCompliances
                                  where row.ID == NatureOfComplianceID
                                  select row.Name).FirstOrDefault();

                return compliance;
            }
        }
        public static List<NatureOfCompliance> GetAllComplianceNature(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (filter == null)
                {
                    return entities.NatureOfCompliances.Where(k => k.IsDeleted == false).OrderBy(k => k.Name).ToList();
                }
                else
                {
                    return entities.NatureOfCompliances.Where(k => k.IsDeleted == false && k.Name.Contains(filter)).OrderBy(k => k.Name).ToList();
                }

            }
        }

        public static List<ComplianceForm> GetComplianceFileNameMultiple(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceForms
                             where row.ComplianceID == complianceID
                             select row).ToList();
                return query;
            }
        }
        public static void CreateMultiple(Compliance compliance, List<ComplianceForm> form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                compliance.CreatedOn = DateTime.Now;
                compliance.IsDeleted = false;
                entities.Compliances.Add(compliance);
                entities.SaveChanges();
                if (form.Count > 0)
                {
                    for (int i = 0; i < form.Count; i++)
                    {
                        form[i].ComplianceID = compliance.ID;
                        CreateSampleFile(form[i]);
                    }
                }

                if (compliance.Frequency != 7)
                {
                    if (compliance.Frequency != 8)
                    {
                        if (compliance.Frequency.HasValue)
                        {
                            GenerateDefaultScheduleForComplianceID(compliance.ID, compliance.SubComplianceType);
                        }
                    }
                }
                if (compliance.ID != 0)
                {

                }
            }
        }

        public static List<ComplianceForm> GetMultipleComplianceFormByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.ComplianceForms
                            where row.ComplianceID == complianceID
                            select row).ToList();

                return form;
            }
        }
        public static void DeleteComplianceForm(int ID, int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceForms
                             where row.ComplianceID == complianceID && row.ID == ID
                             select row).FirstOrDefault();

                entities.ComplianceForms.Remove(query);
                entities.SaveChanges();
            }
        }

        public static ComplianceForm GetSelectedComplianceFileName(int ID, int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceForms
                             where row.ComplianceID == complianceID && row.ID == ID
                             select row).FirstOrDefault();
                return query;
            }
        }
        public static List<SP_GetInformativeCompliance_Result> GetInformativeComplianceDetailsByText(String TextToSearch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceDetails = (from row in entities.SP_GetInformativeCompliance()
                                         where (row.ShortDescription.ToLower().Contains(TextToSearch) || row.Description.ToLower().Contains(TextToSearch))
                                         select row).GroupBy(a => a.ComplianceID).Select(a => a.FirstOrDefault()).ToList();

                return ComplianceDetails;
            }
        }

        public static List<SP_GetInformativeCompliance_Result> GetAllInformativeComplianceDetails()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceDetails = (from row in entities.SP_GetInformativeCompliance()
                                         //where (row.ShortDescription.ToLower().Contains(TextToSearch) || row.Description.ToLower().Contains(TextToSearch))
                                         select row).GroupBy(a => a.ComplianceID).Select(a => a.FirstOrDefault()).ToList();

                return ComplianceDetails;
            }
        }
        public static Compliance GetComplianceFromInstance(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.Compliances
                            join row1 in entities.ComplianceInstances
                            on row.ID equals row1.ComplianceId
                            where row1.ID == complianceInstanceID
                            select row).FirstOrDefault();

                return form;
            }
        }
        public static string GetBranchLocationName(int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceInstances
                             join row1 in entities.CustomerBranches
                             on row.CustomerBranchID equals row1.ID
                             where row.ID == complianceInstanceID
                             select row1.Name).FirstOrDefault();

                return query;
            }
        }
        
        public static List<ComplianceView> GetAllNew(int ComplianceTypeID, int ComplianceCatagoryId, int frq, int CmType, int actid, string filter = null)
        {
            List<ComplianceView> complianceInformation = new List<ComplianceView>();
            // return compliances;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliancesQuery = (from row in entities.ComplianceViews
                                        where row.EventFlag == null
                                        select row);

                int risk = -1;


                if (!string.IsNullOrEmpty(filter))
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper()) || entry.Frequency == frq || entry.RiskType == risk);
                }

                if (ComplianceTypeID != -1 && ComplianceTypeID != 0)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == ComplianceTypeID);
                }
                if (ComplianceCatagoryId != -1 && ComplianceCatagoryId != 0)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == ComplianceCatagoryId);
                }
                if (CmType != -1 && CmType != 0)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType);
                }
                if (frq != -1 && frq != 0)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == frq);
                }
                if (actid != -1 && frq != 0)
                {

                    compliancesQuery = compliancesQuery.Where(entry => entry.ActID == actid);
                }

                var compliances = compliancesQuery.ToList();


                compliances.ForEach(entry =>
                {
                    complianceInformation.Add(entry);
                });


                return complianceInformation;
            }


        }
        public static void CreateCustomerBranchIndustryMapping(CustomerBranchIndustryMapping IndustryMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.CustomerBranchIndustryMappings.Add(IndustryMapping);
                entities.SaveChanges();
            }
        }

        public static void UpdateComplianceTagMappedID(long ComplianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.ComplianceDatatagMappings
                           where row.ComplianceID == ComplianceId
                           select row.ID).ToList();
                ids.ForEach(entry =>
                {
                    ComplianceDatatagMapping prevmappedids = (from row in entities.ComplianceDatatagMappings
                                                              where row.ID == entry
                                                              select row).FirstOrDefault();

                    entities.ComplianceDatatagMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }
        public static void CreateUpdate_ComplianceFileTagsMappingData(List<ComplianceDatatagMapping> lstFiletagdataMapping)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstFiletagdataMapping.ForEach(_objRecord =>
                    {
                        var prevRecord = (from row in entities.ComplianceDatatagMappings
                                          where row.ComplianceID == _objRecord.ComplianceID
                                          && row.FileTag.Trim().ToUpper().Equals(_objRecord.FileTag.Trim().ToUpper())
                                          select row).FirstOrDefault();
                        if (prevRecord != null)
                        {
                            prevRecord.IsActive = true;
                            prevRecord.Updatedby = _objRecord.Updatedby;
                            prevRecord.UpdatedOn = DateTime.Now;
                            saveSuccess = true;
                        }
                        else
                        {
                            _objRecord.CreatedOn = DateTime.Now;
                            _objRecord.UpdatedOn = DateTime.Now;
                            entities.ComplianceDatatagMappings.Add(_objRecord);
                            saveSuccess = true;
                        }
                    });
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                //LogMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //return false;
            }
        }

        public static List<string> GetDistinctComplianceTaglist()
        {
            try
            {
                List<string> lstFileTags = new List<string>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var Query = (from row in entities.ComplianceDatatagMappings
                                 join row1 in entities.Compliances
                                 on row.ComplianceID equals row1.ID
                                 where row1.IsDeleted == false && row1.EventFlag == true
                                 select row.FileTag).ToList();
                    
                    return Query;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<ListItem> ReArrange_FileTags(CheckBoxList lstBoxFileTags)
        {
            List<ListItem> lstSelectedTags = new List<ListItem>();
            List<ListItem> lstNonSelectedTags = new List<ListItem>();

            foreach (ListItem eachListItem in lstBoxFileTags.Items)
            {
                if (eachListItem.Selected)
                    lstSelectedTags.Add(eachListItem);
                else
                    lstNonSelectedTags.Add(eachListItem);
            }

            return lstSelectedTags.Union(lstNonSelectedTags).ToList();
        }
        public static List<ComplianceDatatagMapping> GetComplianceDatatagMapping(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.ComplianceDatatagMappings
                                  where row.ComplianceID == complianceID
                                  select row).ToList();

                return compliance;
            }
        }
           public static void UpdateCustomerBranchIndustryMappingMappedID(long CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.CustomerBranchIndustryMappings
                           where row.CustomerBranchID == CustomerBranchID
                           select row.ID).ToList();
                ids.ForEach(entry =>
                {
                    CustomerBranchIndustryMapping prevmappedids = (from row in entities.CustomerBranchIndustryMappings
                                                                   where row.ID == entry
                                                     select row).FirstOrDefault();
                    entities.CustomerBranchIndustryMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }
        
        public static List<int> GetCustomerBranchIndustryMappedID(long CustomerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.CustomerBranchIndustryMappings
                                         where row.CustomerBranchID == CustomerBranchID && row.IsActive == true
                                         select row.IndustryID).ToList();

                return IndustryMappedIDs;
            }

        }
        public static bool CreateUpdate_FileTagsMapping(List<FileDataTagsMapping> lstRecords, long FileID)
        {

            bool saveSuccess = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var objtag = (from row in entities.FileDataTagsMappings
                              where row.FileID == FileID && row.IsActive == true
                              select row).ToList();
                foreach (var item in objtag)
                {
                    FileDataTagsMapping prevRecord = (from row in entities.FileDataTagsMappings
                                                      where row.FileID == FileID && row.IsActive == true
                                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.IsActive = false;
                        entities.SaveChanges();
                    }
                }

                lstRecords.ForEach(_objRecord =>
                {
                    var prevRecord = (from row in entities.FileDataTagsMappings
                                      where row.FileID == _objRecord.FileID
                                      && row.FileTag.Trim().ToUpper().Equals(_objRecord.FileTag.Trim().ToUpper())
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.IsActive = true;

                        prevRecord.UpdatedBy = _objRecord.UpdatedBy;
                        prevRecord.UpdatedOn = DateTime.Now;

                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;
                        _objRecord.UpdatedOn = DateTime.Now;

                        entities.FileDataTagsMappings.Add(_objRecord);
                        saveSuccess = true;
                    }
                });

                entities.SaveChanges();
                return saveSuccess;
            }
        }

        public static FolderFileData GetDocumentByID(long docFileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                FolderFileData file = null;
                UserFolderPermission filepermision = (from row in entities.UserFolderPermissions
                                                      where row.ID == docFileID && row.IsDeleted == false
                                                      select row).FirstOrDefault();
                if (filepermision != null)
                {
                    file = entities.FolderFileDatas.Where(x => x.ID == filepermision.FileID).FirstOrDefault();

                }
                return file;
            }
        }
        public static string PeriodReplace(string formonth)
        {
            var month="";

            if(formonth.Contains("14"))
            {
                month = formonth.Replace("14", "2014");
            }
            if (formonth.Contains("15"))
            {
                month = formonth.Replace("15", "2015");
            }
            if (formonth.Contains("16"))
            {
                month = formonth.Replace("16", "2016");
            }
            if (formonth.Contains("17"))
            {
                month = formonth.Replace("17", "2017");
            }
            if (formonth.Contains("18"))
            {
                month = formonth.Replace("18", "2018");
            }
            if (formonth.Contains("19"))
            {
                month = formonth.Replace("19", "2019");
            }
            if (formonth.Contains("20"))
            {
                month = formonth.Replace("20", "2020");
            }
            if (formonth.Contains("21"))
            {
                month = formonth.Replace("21", "2021");
            }
            if (formonth.Contains("22"))
            {
                month = formonth.Replace("22", "2022");
            }
            if (formonth.Contains("23"))
            {
                month = formonth.Replace("23", "2023");
            }
            if (formonth.Contains("24"))
            {
                month = formonth.Replace("24", "2024");
            }
           
            return month;
        }
        public static long GetComplianceInstanceIDFromScheduleOn(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.ComplianceTransactions
                                             where row.ComplianceScheduleOnID == ScheduledOnID
                                             select row.ComplianceInstanceId).FirstOrDefault();

                 return complianceInstanceID;
            }
        }

        public static bool ComplianceIsStatutory(long ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.Compliances
                                      join row1 in entities.ComplianceInstances 
                                      on row.ID equals row1.ComplianceId 
                                      where row1.ID == ComplianceInstanceId
                                      select row).FirstOrDefault();

                if (ComplianceData.ComplianceType == 1)
                    return false;
                else
                    return true;
            }
        }
        public static List<long> GetAssignedEvent(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EventList = (from row in entities.Events
                                 join row1 in entities.EventInstances
                                 on row.ID equals row1.EventID
                                 join row2 in entities.CustomerBranches
                                 on row1.CustomerBranchID equals row2.ID
                                 join row3 in entities.Customers
                                 on row2.CustomerID equals row3.ID
                                 where row3.ID == CustomerID
                                 select row.ID).ToList();
                return EventList;
            }
        }
       
        public static List<Compliance> GetFilterEventCompliance(int CustomerID, int complianceTypeID, int complianceCatagoryId, string EventChecked, bool setApprover = false, int lcCustomerBranch = -1, List<int> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<Act> actList = new List<Act>();

                actList = (from row in entities.Acts
                           where row.IsDeleted == false
                           select row).ToList();

                if (complianceCatagoryId != -1)
                {
                    actIDs = (from row in actList
                              where row.ComplianceCategoryId == complianceCatagoryId
                              select row.ID).ToList();
                }
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }
                if (complianceCatagoryId != -1 && complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId && row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (actIdList != null)
                {
                    actIDs = actIdList;
                }

                var EventList = Business.ComplianceManagement.GetAssignedEvent(CustomerID);

                var List = Business.ComplianceManagement.GetEventBasedMappedCompliance(EventList);

                List<long> ComplianceList = List.Distinct().ToList();

                List<Compliance> compliances = new List<Compliance>();

                compliances = (from row in entities.Compliances
                               where actIDs.Contains(row.ActID) && ComplianceList.Contains(row.ID)
                               && row.IsDeleted == false
                               && (row.ComplianceType == 0 || row.ComplianceType == 1)
                               && row.EventFlag == true && row.Status == null
                               select row).ToList();

                var TempQueryAssignInstance = (from row in entities.ComplianceInstances
                                               join row1 in entities.ComplianceAssignments
                                               on row.ID equals row1.ComplianceInstanceID
                                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                               select row.ComplianceId).ToList();

                compliances.RemoveAll(b => TempQueryAssignInstance.Contains(b.ID));

                if (setApprover != false)
                {
                    List<long> query = (from row in entities.ComplianceInstances
                                        join ca in entities.ComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.ComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                        select row.ComplianceId).ToList();

                    compliances = compliances.Where(entry => query.Contains(entry.ID)).ToList();

                }

                if (!string.IsNullOrEmpty(filter))
                {
                    compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return compliances;
            }
        }

        public static bool CreateClientEventMapping(ClientEventMapping clientEventMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {

                        clientEventMapping.Customerid = clientEventMapping.Customerid;
                        clientEventMapping.KeyId = clientEventMapping.KeyId;
                        clientEventMapping.EventID = clientEventMapping.EventID;
                        entities.ClientEventMappings.Add(clientEventMapping);
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }
        public static bool ExistClientMapplingEvent(long EventID,int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ClientEventMappings
                             where row.EventID == EventID && row.Customerid  == CustomerID
                             select row.EventID).FirstOrDefault();

                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }                
            }
        }

        public static bool BlukClientMapplingEvent(List<ClientEventMapping> CEMlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {            
                try
                {
                    int Count = 0;
                    CEMlist.ForEach(EachCEM =>
                    {
                        ClientEventMapping clienteventmapping = (from row in entities.ClientEventMappings
                                                                 where row.EventID == EachCEM.EventID
                                                                 && row.Customerid == EachCEM.Customerid
                                                                 select row).FirstOrDefault();
                        if (clienteventmapping != null)
                        {
                        }
                        else
                        {
                            ClientEventMapping newclienteventmapping = new ClientEventMapping();
                            newclienteventmapping.Customerid = EachCEM.Customerid;
                            newclienteventmapping.KeyId = EachCEM.KeyId;
                            newclienteventmapping.EventID = EachCEM.EventID;
                            entities.ClientEventMappings.Add(newclienteventmapping);                            
                            Count++;
                        }
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static bool BlukClientMapplingCompliance(List<ClientComplianceMapping> CCMlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    int Count = 0;
                    CCMlist.ForEach(EachCCM =>
                    {
                        ClientComplianceMapping clientComplianceMapping = (from row in entities.ClientComplianceMappings
                                     where row.ComplianceID == EachCCM.ComplianceID && row.Customerid == EachCCM.Customerid
                                     select row).FirstOrDefault();

                        if (clientComplianceMapping != null)
                        {                            
                        }
                        else
                        {
                            ClientComplianceMapping newclientComplianceMapping = new  ClientComplianceMapping();
                            newclientComplianceMapping.Customerid = EachCCM.Customerid;
                            newclientComplianceMapping.KeyId = EachCCM.KeyId;
                            newclientComplianceMapping.ComplianceID = EachCCM.ComplianceID;
                            entities.ClientComplianceMappings.Add(newclientComplianceMapping);
                            UpdateComplianceUpdatedOn(EachCCM.ComplianceID);
                            Count++;
                        }                 
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static bool ExistClientMapplingCompliance(long ComplianceID,int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ClientComplianceMappings
                             where row.ComplianceID == ComplianceID && row.Customerid == CustomerID
                             select row.ComplianceID).FirstOrDefault();

                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static List<ClientEventMap> GetAllCliantMappingEvent(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from C in entities.Events
                                      join CM in entities.ClientEventMappings
                                      on C.ID equals CM.EventID
                                      join Cust in entities.Customers
                                      on CM.Customerid equals Cust.ID
                                      select new ClientEventMap()
                                      {
                                          EventID = C.ID,
                                          CustomerId = Cust.ID,
                                          EventName = C.Name,
                                          CustomerName = Cust.Name
                                      });

                if (customerID != -1)
                {
                    productmapping = productmapping.Where(entry => entry.CustomerId == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    productmapping = productmapping.Where(entry => entry.EventName.Contains(filter) || entry.CustomerName.Contains(filter));
                }
                return productmapping.OrderBy(entry => entry.EventID).ToList();
            }
        }

        public static List<ClientComplianceMap> GetAllCliantMappingCompliance(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var productmapping = (from C in entities.Compliances
                                      join CM in entities.ClientComplianceMappings
                                      on C.ID equals CM.ComplianceID
                                      join Cust in entities.Customers
                                      on CM.Customerid equals Cust.ID  
                                      select new ClientComplianceMap()
                                      {
                                          ComplianceID = C.ID,
                                          CustomerId = Cust.ID,
                                          ShortDescription =C.ShortDescription,
                                          CustomerName = Cust.Name,
                                             Type = C.EventFlag == null ? "Statutory" :
                                                    C.EventFlag == true ? "EventBased" : "",
                                      });

                if (customerID != -1)
                {
                    productmapping = productmapping.Where(entry => entry.CustomerId == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    productmapping = productmapping.Where(entry => entry.ShortDescription.Contains(filter) || entry.CustomerName.Contains(filter));
                }
                return productmapping.OrderBy(entry => entry.ComplianceID).ToList();
            }
        }
        public static bool CreateClientComplianceMapping(ClientComplianceMapping clientComplianceMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        clientComplianceMapping.Customerid = clientComplianceMapping.Customerid;
                        clientComplianceMapping.KeyId = clientComplianceMapping.KeyId;
                        clientComplianceMapping.ComplianceID = clientComplianceMapping.ComplianceID;
                        entities.ClientComplianceMappings.Add(clientComplianceMapping);
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }
        public static KeyDetail GetKeyDetail(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var keyDetail = (from row in entities.KeyDetails
                                         where row.CustomerID == CustomerID 
                                         select row).FirstOrDefault();

                return keyDetail;
            }
        }
        public static List<long> GetEventBasedMappedCompliance(List<long> eventListID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> ComplianceList = new List<long>();
                var eventList = (from row in entities.Events
                                           where row.IsDeleted == false 
                                           && eventListID.Contains(row.ID)    
                                 select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                //Parent Event
                eventList.ForEach(evententry =>
                {
                    var EventComplianceList = entities.SP_GetEventMappedCompliance((int?)evententry.ID).ToList(); 

                    EventComplianceList.ForEach(eventComplianceListentry =>
                    {
                        //Add Parent Event Complaince
                        ComplianceList.Add(eventComplianceListentry.ComplianceID);
                    });

                    //Sub Event
                    var SubEventList = (from row in entities.EventSubEventMappings
                                                 where row.ParentEventID == evententry.ID
                                                 && row.IntermediateEventID ==0
                                                 select row).ToList();
                    SubEventList.ForEach(subEventListentry =>
                    {
                        var EventSubEventComplianceList = entities.SP_GetEventSubEvntMappedCompliance((int?) evententry.ID, (int?) subEventListentry.SubEventID).ToList(); ;

                        //Add Sub Event Complaince
                        EventSubEventComplianceList.ForEach(eventSubEventComplianceListentry =>
                        {
                            //Add Parent Event -> Sub Event -> Complaince
                            ComplianceList.Add(eventSubEventComplianceListentry.ComplianceID);
                        });
                    });

                    //Intermediate Event 
                    var IntermediateEventList = (from row in entities.EventSubEventMappings
                                        where row.ParentEventID == evententry.ID
                                        && row.IntermediateEventID != 0 && row.IntermediateFlag == true
                                        select row).ToList();


                    IntermediateEventList.ForEach(intermediateEventListentry =>
                    {
                        //Sub Event
                        var IntermediateSubEventList = (from row in entities.EventSubEventMappings
                                            where row.ParentEventID == intermediateEventListentry.IntermediateEventID
                                            && row.IntermediateEventID == 0 && row.IntermediateFlag ==false
                                            select row).ToList();
                        IntermediateSubEventList.ForEach(intermediateSubEventListentry =>
                        {
                            var IntermediateSubEventComplianceList = entities.SP_GetEventIntermediteSubEvntMappedCompliance((int?) evententry.ID, (int?) intermediateEventListentry.IntermediateEventID, (int?) intermediateSubEventListentry.SubEventID).ToList(); ;

                            //Add Intermediate Event Complaince
                            IntermediateSubEventComplianceList.ForEach(intermediateSubEventComplianceListentry =>
                            {
                                //Add Parent Event ->Intermediate Event -> Sub Event -> Complaince
                                ComplianceList.Add(intermediateSubEventComplianceListentry.ComplianceID);
                            });
                        });
                    });
                });

                return ComplianceList.ToList();

            }
        }

   
      
        public static ComplianceInstance GetBranchLocation(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.ComplianceInstances
                            where row.ID == complianceInstanceID
                            select row).SingleOrDefault();

                return form;
            }
        }

        public static Compliance GetComplianceType(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.ComplianceInstances
                            join row1 in entities.Compliances
                            on row.ComplianceId equals row1.ID
                            where row.ID == complianceInstanceID
                            select row1).SingleOrDefault();

                return compliance;
            }
        }

        public static InternalComplianceInstance GetBranchLocationInternal(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceInstances
                            where row.ID == complianceInstanceID
                            select row).SingleOrDefault();

                return form;
            }
        }
        public static void UpdateComplianceUpdatedOn(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance compliance = (from row in entities.Compliances
                                         where row.ID == complianceID
                                         select row).FirstOrDefault();

                compliance.UpdatedOn = DateTime.Now;
                
                Act acts = (from row in entities.Acts
                                         where row.ID == compliance.ActID
                            select row).FirstOrDefault();

                acts.UpdatedOn = DateTime.Now;
                entities.SaveChanges();


            }
        }
        public static void UpdateCustomerwiseComplianceUpdatedOn(long complianceID,int custid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                    CustomerSpecificCompliance compliance = (from row in entities.CustomerSpecificCompliances
                                                         where row.ComplianceId == complianceID
                                                        && row.CustomerID==custid
                                                        select row).FirstOrDefault();

                compliance.UpdatedOn = DateTime.Now;

                Act acts = (from row in entities.Acts
                            where row.ID == compliance.ActID
                            select row).FirstOrDefault();

                acts.UpdatedOn = DateTime.Now;
                entities.SaveChanges();

               
            }
               
        }
        public static void CreateComplianceDetail(ComplianceDetail compliancedetail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ComplianceDetails.Add(compliancedetail);
                entities.SaveChanges();
            }
        }

        public static void UpdateComplianceDetail(long ComplianceID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceDetail compliancedetail = (from row in entities.ComplianceDetails
                                                     where row.ComplianceID == ComplianceID
                                                     select row).FirstOrDefault();
                compliancedetail.UpdatedBy = UserID;
                entities.SaveChanges();
            }
        }

        public static bool ExistsComplianceDetail(long Complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceDetails
                             where row.ComplianceID == Complianceid
                             select row.ComplianceID).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static void CreateInternal(List<InternalCompliance> complianceData, int? customerid)
        {
            string shortdescription = "";
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var extraitems = db.table.Where(t => !list.Select(l => l.EntityID).Contains(t.EntityID));
                    complianceData.ForEach(entry =>
                    {
                        bool chkexists = ExistsInternalNew(entry.IShortDescription, customerid);
                        shortdescription = entry.IShortDescription;
                        if (chkexists == false)
                        {
                            entities.InternalCompliances.Add(entry);
                        }
                        //entities.Compliances.Add(entry);
                    });
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "Create shortdescription" + shortdescription;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);
                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");

                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "Create shortdescription" + shortdescription);
                SendgridSenEmail("Error Occured as CreateTransaction Function", "Create shortdescription" + shortdescription);

            }
        }

        public static bool ExistsInternalNew(string ShortDescription, int? CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliances
                             where row.IShortDescription.Equals(ShortDescription) && row.IsDeleted == false
                             && row.CustomerID == CustomerID
                             select row);
                return query.Select(entry => true).FirstOrDefault();

            }
        }
        public static void CreateInternal(InternalCompliance compliance, InternalComplianceForm form, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                compliance.CreatedOn = DateTime.UtcNow;
                compliance.IsDeleted = false;
                compliance.UpdatedOn = DateTime.UtcNow;

                bool chkexists = ExistsInternalNew(compliance.IShortDescription, customerID);
                if (chkexists == false)
                {
                    entities.InternalCompliances.Add(compliance);

                    entities.SaveChanges();

                    if (form != null)
                    {
                        form.IComplianceID = compliance.ID;
                        CreateSampleFile(form);
                    }

                    if (compliance.IFrequency.HasValue)
                    {
                        if (compliance.IFrequency != 7 && compliance.IFrequency != 8)
                        {
                            GenerateScheduleForInternalCompliance(compliance.ID, compliance.ISubComplianceType);
                        }
                    }
                }
            }
        }
        public static void CreateInternalMultiple(InternalCompliance compliance,List<InternalComplianceForm> form, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                compliance.CreatedOn = DateTime.UtcNow;
                compliance.IsDeleted = false;
                compliance.UpdatedOn = DateTime.UtcNow;

                bool chkexists = ExistsInternalNew(compliance.IShortDescription, customerID);
                if (chkexists == false)
                {
                    entities.InternalCompliances.Add(compliance);

                    entities.SaveChanges();

                    //if (form != null)
                    //{
                    //    form.IComplianceID = compliance.ID;
                    //    CreateSampleFile(form);
                    //}

                    if (form.Count > 0)
                    {
                        for (int i = 0; i < form.Count; i++)
                        {
                            form[i].IComplianceID = compliance.ID;
                            CreateSampleFile(form[i]);
                        }
                    }

                    if (compliance.IFrequency.HasValue)
                    {
                        if (compliance.IFrequency != 7 && compliance.IFrequency != 8)
                        {
                            GenerateScheduleForInternalCompliance(compliance.ID, compliance.ISubComplianceType);
                        }
                    }
                }
            }
        }

        public static bool ExistsCheckList(int ComplianceID, int Customerbranchid, string SequenceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TempAssignmentTableCheckLists
                             where row.IsActive == true
                                  && row.ComplianceId == ComplianceID
                                  && row.CustomerBranchID == Customerbranchid
                                   && row.SequenceID == SequenceID
                             select row).ToList();
                if (query != null)
                {
                    if (query.Count == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool Exists(int ComplianceID, int Customerbranchid,string SequenceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.TempAssignmentTables
                             where row.IsActive == true
                                  && row.ComplianceId == ComplianceID
                                  && row.CustomerBranchID == Customerbranchid
                                  && row.SequenceID== SequenceID
                             select row).ToList();
                if (query != null)
                {
                    if (query.Count == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }
        
        public static List<PenaltyDataDisplayClass> BindPenaltyForPerformer(int customerid, int UserID, int Risk, string Status, int location, int type, int category, int ActID, DateTime DateFrom, DateTime DateTo, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceDetails = (from row in entities.ComplianceTransactionViews
                                         join instran in entities.ComplianceAssignedInstancesViews
                                         on row.ComplianceInstanceId equals instran.ComplianceInstanceID
                                         join schedule in entities.ComplianceScheduleOns
                                         on row.ComplianceScheduleOnID equals schedule.ID
                                         where instran.CustomerID == customerid
                                         && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                         && row.IsPenaltySave == true //&& row.PenaltySubmit == "P"
                                         && instran.RoleID == 3 && instran.UserID == UserID
                                         select new PenaltyDataDisplayClass()
                                         {
                                             ComplianceTransactionID = row.ComplianceTransactionID,
                                             ComplianceID = (long)instran.ComplianceID,
                                             RiskType = (int)instran.RiskType,
                                             ComplianceStatusID = (int)row.ComplianceStatusID,
                                             ComplianceInstanceID = row.ComplianceInstanceId,
                                             ComplianceScheduledID = (long)row.ComplianceScheduleOnID,
                                             Branch = instran.Branch,
                                             ComplianceCategoryID = instran.ComplianceCategoryId,
                                             ActID = (long)instran.ActID,
                                             Status = row.Status,
                                             ComplianceTypeID = (int)instran.ComplianceTypeID,
                                             CustomerBranchID = (int)instran.CustomerBranchID,
                                             ActName = instran.Name,
                                             Section = instran.Section,
                                             ShortDescription = instran.ShortDescription,
                                             ScheduledOn = (DateTime)schedule.ScheduleOn,
                                             User = instran.User,
                                             Penalty = (decimal?)row.Penalty,
                                         }).ToList();

                if (location != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == location).Distinct().ToList();
                }

                if (Status == "CompliedButPendingReview")
                {
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.ComplianceStatusID == 2).ToList();
                }

                if (Status == "CompliedDelayedBuPendingReview")
                {
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.ComplianceStatusID == 3).ToList();
                }

                if (ActID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActID).Distinct().ToList();
                }

                if (Risk != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.RiskType == Risk).Distinct().ToList();
                }

                if (category != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryID == category).Distinct().ToList();
                }
                if (type != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceTypeID == type).Distinct().ToList();
                }

                if (DateFrom.ToString() != "1/1/1900 12:00:00 AM" && DateTo.ToString() != "1/1/1900 12:00:00 AM")
                {
                    ComplianceDetails = ComplianceDetails.Where(entry => (entry.ScheduledOn >= DateFrom && entry.ScheduledOn <= DateTo)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    ComplianceDetails = ComplianceDetails.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceScheduledID).Select(a => a.FirstOrDefault()).ToList();                
                return ComplianceDetails;
            }
        }

        public static object GetComSubTypeData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceSubTypes
                             select row).ToList();
                return query;
            }
        }

        public static void CreateComSubTypeList(ComplianceSubType objComSubType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ComplianceSubTypes.Add(objComSubType);
                entities.SaveChanges();
            }
        }

        public static List<ComplianceSubType> GetComSubTypeDataByID(int complianceSubID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceSubTypes
                             where row.ID == complianceSubID
                             select row).ToList();
                return query;
            }
        }

        public static void DeleteComSubTypeDataByID(int complianceSubID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceSubTypes
                             where row.ID == complianceSubID
                             select row).ToList();

                query.ForEach(entry =>
                            entities.ComplianceSubTypes.Remove(entry));
                entities.SaveChanges();
            }
        }

        public static void UpdateComSubTypeList(ComplianceSubType objComSubType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceSubTypes
                             where row.ID == objComSubType.ID
                             select row).ToList();

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        item.ID = objComSubType.ID;
                        item.Name = objComSubType.Name;
                        item.Description = objComSubType.Description;
                    }
                    entities.SaveChanges();
                }
            }
        }
        public static void AddDetailsTempAssignmentTableCheckList(List<TempAssignmentTableCheckList> Tempassignments)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    entities.TempAssignmentTableCheckLists.Add(entry);
                    entities.SaveChanges();

                });
            }

        }

        public static void UpdateAssignedCheckListUser(int TempAssignmentID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTableCheckList TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTableCheckLists
                                                                           where row.ID == TempAssignmentID
                                                                           select row).FirstOrDefault();
                TempAssignmentIDToDeactive.UserID = UserID;
                entities.SaveChanges();
            }
        }

        public static void DeactiveTempAssignmentCheckListDetails(int TempAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTableCheckList TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTableCheckLists
                                                                           where row.ID == TempAssignmentID
                                                                           select row).FirstOrDefault();
                TempAssignmentIDToDeactive.IsActive = false;
                entities.SaveChanges();
            }
        }
        public static List<TempDeactivateCheckListView> GetDetailsToDeactiveCheckList(int lcCustomerBranch = -1)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TempDeactivateCheckListView> TempGetDetailsToDeactivate = (from row in entities.TempDeactivateCheckListViews
                                                                                where row.CustomerBranchID == lcCustomerBranch
                                                                                select row).ToList();

                var TempTransactionDtls = (from row in entities.TempDeactivateCheckListViews
                                           join ci in entities.ComplianceTransactions
                                           on row.ComplianceInstanceID equals ci.ComplianceInstanceId
                                           where ci.StatusId != 1
                                           select row.ComplianceInstanceID).ToList();

                TempGetDetailsToDeactivate.RemoveAll(a => TempTransactionDtls.Contains(a.ComplianceInstanceID));
                return TempGetDetailsToDeactivate;

            }
        }
        public static List<TempDeactivateCheckListView> GetDetailsToDeactiveCheckList(List<long> lcCustomerBranch)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TempDeactivateCheckListView> TempGetDetailsToDeactivate = (from row in entities.TempDeactivateCheckListViews
                                                                                where lcCustomerBranch.Contains(row.CustomerBranchID)
                                                                                select row).ToList();

                var TempTransactionDtls = (from row in entities.TempDeactivateCheckListViews
                                           join ci in entities.ComplianceTransactions
                                           on row.ComplianceInstanceID equals ci.ComplianceInstanceId
                                           where ci.StatusId != 1
                                           select row.ComplianceInstanceID).ToList();

                TempGetDetailsToDeactivate.RemoveAll(a => TempTransactionDtls.Contains(a.ComplianceInstanceID));
                return TempGetDetailsToDeactivate;

            }
        }

        public static List<SP_TempAssignmentCheckList_Result> GetTempAssignedCheckListDetails(long CustomerID,int lcCustomerBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var TempAssignedDetails = (from row in entities.SP_TempAssignmentCheckList(CustomerID)
                                               //where row.CustomerBranchID == lcCustomerBranch
                                               //orderby row.ComplianceID descending,row.RoleID ascending
                                           orderby row.ID
                                           select row).ToList();

                if (lcCustomerBranch !=-1)
                {
                    TempAssignedDetails = TempAssignedDetails.Where(entry => entry.CustomerBranchID == lcCustomerBranch).ToList();
                }
                return TempAssignedDetails;

            }
        }
        public static List<SP_TempAssignmentCheckList_Result> GetTempAssignedCheckListDetails(long CustomerID, List<long> lcCustomerBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var TempAssignedDetails = (from row in entities.SP_TempAssignmentCheckList(CustomerID)
                                               //where lcCustomerBranch.Contains(row.CustomerBranchID)
                                           orderby row.ID
                                           select row).ToList();


                if (lcCustomerBranch.Count>0)
                {
                    TempAssignedDetails = TempAssignedDetails.Where(entry => lcCustomerBranch.Contains(entry.CustomerBranchID)).ToList();
                }
                return TempAssignedDetails;

            }
        }
        public static List<Compliance> TempGetByTypeCheckList(int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, List<int> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<Act> actList = new List<Act>();

                actList = (from row in entities.Acts
                           where row.IsDeleted == false
                           select row).ToList();


                if (complianceCatagoryId != -1)
                {
                    actIDs = (from row in actList
                              where row.ComplianceCategoryId == complianceCatagoryId
                              select row.ID).ToList();
                }
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (complianceCatagoryId != -1 && complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId && row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (actIdList != null)
                {
                    actIDs = actIdList;
                }


                List<Compliance> compliances = (from row in entities.Compliances
                                                where actIDs.Contains(row.ActID) && row.IsDeleted == false
                                                && row.ComplianceType == 1
                                                && row.EventFlag == null && row.Status == null
                                                && row.ComplinceVisible != false
                                                && row.IsMapped != false  //Added condition 19 NOV 2020
                                                select row).ToList();

                var TempQuery = (from row in entities.TempAssignmentTableCheckLists
                                 where row.CustomerBranchID == lcCustomerBranch && row.IsActive == true && row.UserID != -1
                                 select row.ComplianceId).ToList();

                compliances.RemoveAll(a => TempQuery.Contains(a.ID));

                var TempQueryAssignInstance = (from row in entities.ComplianceInstances
                                               join row1 in entities.ComplianceAssignments
                                               on row.ID equals row1.ComplianceInstanceID
                                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                               select row.ComplianceId).ToList();

                //var TempQueryAssignInstance = (from row in entities.ComplianceInstances
                //                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                //                               select row.ComplianceId).ToList();

                compliances.RemoveAll(b => TempQueryAssignInstance.Contains(b.ID));

                if (setApprover != false)
                {
                    List<long> query = (from row in entities.ComplianceInstances
                                        join ca in entities.ComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.ComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                        select row.ComplianceId).ToList();

                    compliances = compliances.Where(entry => query.Contains(entry.ID)).ToList();

                }

                if (!string.IsNullOrEmpty(filter))
                {
                    compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return compliances;
            }
        }
        public static ComplianceInstanceTransactionView GetPeriodBranchLocation(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceInstanceTransactionViews
                             where row.ScheduledOnID == scheduledOnID
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).FirstOrDefault();

                return query;
            }
        }
        public static List<ComplianceInstanceTransactionView> GetPeriodBranchLocationList(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceInstanceTransactionViews
                             where row.ScheduledOnID == scheduledOnID
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).Distinct().ToList();

                return query;
            }
        }
        public static InternalComplianceInstanceTransactionView GetPeriodLocationPerformerInternal(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.InternalComplianceInstanceTransactionViews
                             where row.InternalScheduledOnID == scheduledOnID
                             && row.InternalComplianceInstanceID == complianceInstanceID
                             select row).FirstOrDefault();

                return query;
            }
        }
        public static InternalComplianceInstanceCheckListTransactionView GetPeriodLocationPerformerInternalCheckList(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                             where row.InternalScheduledOnID == scheduledOnID
                             && row.InternalComplianceInstanceID == complianceInstanceID
                             select row).FirstOrDefault();

                return query;
            }
        }        
       
        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> SPGetMappedComplianceCheckListReviewer(long UserID, List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MasterStatutoryReviewerCheckList, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();

                int performerRoleID = (from row in entities.Roles
                                       where row.Code == "RVW1"
                                       select row.ID).Single();


                DateTime now = DateTime.UtcNow.Date;
                transactionsQuery = (from row in MasterStatutoryReviewerCheckList //entities.CheckListInstanceTransactionReviewerViews
                                     where row.UserID == UserID && row.RoleID == performerRoleID
                                     && (row.ScheduledOn == now || row.ScheduledOn <= now)
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     && row.ComplianceStatusID == 1
                                     select row).ToList();                
                return transactionsQuery;
            }
        }
        public static object GetAllComwisePerformerandRev(int CustomerID, string queryStringFlag, int Location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.CompliceWisePerAndRevLists
                             where row.CustomerID == CustomerID
                             select row).Distinct().ToList();

                if (Location != -1)
                {
                    query = query.Where(entry => entry.CustomerBranchID == Location).ToList();

                }
                if (queryStringFlag == "C")
                {
                    query = query.Where(entry => entry.EventFlag == null && entry.ComplianceType != 1).ToList();

                }
                if (queryStringFlag == "E")
                {
                    query = query.Where(entry => entry.EventFlag == true && entry.ComplianceType != 1).ToList();

                }
                if(queryStringFlag == "CH")
                {
                    query = query.Where(entry => entry.EventFlag == null && entry.ComplianceType == 1).ToList();

                }
                return query;
            }
        }


        public static object GetAllComwisePerformerandRevInternal(int CustomerID, int location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliceWisePerAndRevLists
                             where row.CustomerID == CustomerID
                             select row).Distinct().ToList();

                if (location != -1)
                {
                    query = query.Where(entry => entry.CustomerBranchID == location).ToList();
                }

                return query;
            }
        }
        public static List<EscalationClass> GetComplianceEscalation(int UserID, int CustomerID, int Type, int Risk, string location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         join row2 in entities.ComplianceCustomEscalations
                                         on new { a = (int) row.ComplianceID, b = (int) row.CustomerBranchID }
                                         equals new { a = (int) row2.ComplianceId, b = (int) row2.CustomerBranchID }
                                         into t
                                         from rt in t.DefaultIfEmpty()
                                         where row.CustomerID == CustomerID
                                         && row.UserID == UserID && row.RoleID == 4
                                         && row.Frequency != 7 && row.Frequency != 8
                                         select new EscalationClass()
                                         {
                                             complianceID = (int) row.ComplianceID,
                                             ShortDescription = row.ShortDescription,
                                             ActID = (long) row.ActID,
                                             CustomerBranchID = (int) row.CustomerBranchID,
                                             ActName = row.Name,
                                             User = row.User,
                                             IntreimDays = rt.Interimdays,
                                             EscDays =rt.days,
                                             EventFlag = (Boolean) row.EventFlag,
                                             RiskType = (int) row.RiskType,
                                             Branch = row.Branch,
                                             Frequency = row.Frequency == 0 ? "Monthly" :
                                             row.Frequency == 1 ? "Quarterly" :
                                             row.Frequency == 2 ? "HalfYearly" :
                                             row.Frequency == 3 ? "Annual" :
                                             row.Frequency == 4 ? "FourMonthly" :
                                             row.Frequency == 5 ? "TwoYearly" :
                                             row.Frequency == 6 ? "SevenYearly" : "",
                                             DueDate = (int?) row.DueDate,
                                         }).Distinct().ToList();

                if (Type == -1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == null).ToList();

                if (Type == 1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();

                if (Risk != -1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.RiskType == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.Branch == location).ToList();
                return ComplianceDetails;
            }
        }


        public static List<ManagmentRemindClass> GetComplianceReminderApprover(int UserID, int CustomerID, int Type, int Risk, string location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         join row2 in entities.Managment_Reminder
                                         on new { a = (int) row.ComplianceID, b = (int) row.CustomerBranchID }
                                         equals new { a = row2.ComplianceId, b = (int) row2.CustomerBranchID }
                                         into t
                                         from rt in t.DefaultIfEmpty()
                                         where row.CustomerID == CustomerID //&& rt.UserFlag == "A"
                                         && row.UserID == UserID && row.RoleID == 6
                                         select new ManagmentRemindClass()
                                         {
                                             complianceID = (int) row.ComplianceID,
                                             ShortDescription = row.ShortDescription,
                                             ActID = (long) row.ActID,
                                             CustomerBranchID = (int) row.CustomerBranchID,
                                             ActName = row.Name,
                                             RemindFlag = rt.RemindFlag,
                                             UserFlag = rt.UserFlag,
                                             EventFlag = (Boolean) row.EventFlag,
                                             RiskType = (int) row.RiskType,
                                             Branch = row.Branch,
                                             Frequency = row.Frequency == 0 ? "Monthly" :
                                             row.Frequency == 1 ? "Quarterly" :
                                             row.Frequency == 2 ? "HalfYearly" :
                                             row.Frequency == 3 ? "Annual" :
                                             row.Frequency == 4 ? "FourMonthly" :
                                             row.Frequency == 5 ? "TwoYearly" :
                                             row.Frequency == 6 ? "SevenYearly" : "",
                                             DueDate = (int?) row.DueDate,
                                         }).Distinct().ToList();

                if (Type == -1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == null).ToList();

                if (Type == 1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();

                if (Risk != -1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.RiskType == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.Branch == location).ToList();
                return ComplianceDetails;
            }
        }


        public static List<ManagmentRemindClass> GetComplianceReminderManagment(int UserID, int CustomerID, int Type, int Risk, string location)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         join acte in entities.EntitiesAssignments
                                         on (long?) row.CustomerBranchID equals acte.BranchID
                                         join row2 in entities.Managment_Reminder
                                         on new { a = (int) row.ComplianceID, b = (int) row.CustomerBranchID }
                                         equals new { a = (int) row2.ComplianceId, b = (int) row2.CustomerBranchID }
                                         into t
                                         from rt in t.DefaultIfEmpty()
                                         where row.CustomerID == CustomerID //&& rt.UserFlag == "M"
                                         //&& row.UserID == UserID && row.RoleID == 4
                                         select new ManagmentRemindClass()
                                         {
                                             complianceID = (int) row.ComplianceID,
                                             ShortDescription = row.ShortDescription,
                                             ActID = (long) row.ActID,
                                             CustomerBranchID = (int) row.CustomerBranchID,
                                             ActName = row.Name,
                                             RemindFlag = rt.RemindFlag,
                                             UserFlag = rt.UserFlag,
                                             EventFlag = (Boolean) row.EventFlag,
                                             RiskType = (int) row.RiskType,
                                             Branch = row.Branch,
                                             Frequency = row.Frequency == 0 ? "Monthly" :
                                             row.Frequency == 1 ? "Quarterly" :
                                             row.Frequency == 2 ? "HalfYearly" :
                                             row.Frequency == 3 ? "Annual" :
                                             row.Frequency == 4 ? "FourMonthly" :
                                             row.Frequency == 5 ? "TwoYearly" :
                                             row.Frequency == 6 ? "SevenYearly" : "",
                                             DueDate = (int?) row.DueDate,
                                         }).Distinct().ToList();

                if (Type == -1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == null).ToList();

                if (Type == 1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.EventFlag == true).ToList();

                if (Risk != -1)
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.RiskType == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    ComplianceDetails = ComplianceDetails.Where(entry => entry.Branch == location).ToList();
                return ComplianceDetails;
            }
        }

        public static void UpdateEscalation(ComplianceCustomEscalation UpdateEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.ComplianceCustomEscalations
                                                  where row.UserIdReviewer == UpdateEscalation.UserIdReviewer
                                                  && row.ComplianceId == UpdateEscalation.ComplianceId
                                                  && row.CustomerBranchID == UpdateEscalation.CustomerBranchID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.Interimdays = UpdateEscalation.Interimdays;
                    NotificationRecordToUpdate.days = UpdateEscalation.days;
                    entities.SaveChanges();
                }
            }
        }
        public static void CreateEscalationReminders(ComplianceCustomEscalation cmpcustomescalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ComplianceCustomEscalations.Add(cmpcustomescalation);
                entities.SaveChanges();
            }
        }
        public static void CreateManagmentReminders(Managment_Reminder cmpcustomescalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Managment_Reminder.Add(cmpcustomescalation);
                entities.SaveChanges();
            }
        }
        public static void UpdateApproverReminders(Managment_Reminder UpdateEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.Managment_Reminder
                                                  where row.ComplianceId == UpdateEscalation.ComplianceId
                                                  && row.UserId == UpdateEscalation.UserId
                                                  && row.CustomerBranchID == UpdateEscalation.CustomerBranchID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.RemindFlag = UpdateEscalation.RemindFlag;
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdateManagmentReminders(Managment_Reminder UpdateEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.Managment_Reminder
                                                  where row.ComplianceId == UpdateEscalation.ComplianceId
                                                  && row.UserId == UpdateEscalation.UserId
                                                  && row.CustomerBranchID == UpdateEscalation.CustomerBranchID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.RemindFlag = UpdateEscalation.RemindFlag;
                    entities.SaveChanges();
                }
            }
        }                
        public static List<PenaltyDataDisplayClass> BindPenaltyForReviewer(int customerid, int UserID, int Risk, string Status, int location, int type, int category, int ActID, DateTime DateFrom, DateTime DateTo)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceDetails = (from row in entities.ComplianceTransactionViews
                                         join instran in entities.ComplianceAssignedInstancesViews
                                         on row.ComplianceInstanceId equals instran.ComplianceInstanceID
                                         join schedule in entities.ComplianceScheduleOns
                                         on row.ComplianceScheduleOnID equals schedule.ID
                                         where instran.CustomerID == customerid
                                         && (row.ComplianceStatusID == 5)
                                         && row.IsPenaltySave == true // && row.PenaltySubmit == "P"
                                         && instran.RoleID == 4 && instran.UserID == UserID

                                         select new PenaltyDataDisplayClass()
                                         {
                                             ComplianceTransactionID = row.ComplianceTransactionID,
                                             ComplianceID = (long)instran.ComplianceID,
                                             RiskType = (int)instran.RiskType,
                                             ComplianceStatusID = (int)row.ComplianceStatusID,
                                             ComplianceInstanceID = row.ComplianceInstanceId,
                                             ComplianceScheduledID = (long)row.ComplianceScheduleOnID,
                                             Branch = instran.Branch,
                                             ComplianceCategoryID = instran.ComplianceCategoryId,
                                             ActID = (long)instran.ActID,
                                             Status = row.Status,
                                             ComplianceTypeID = (int)instran.ComplianceTypeID,
                                             CustomerBranchID = (int)instran.CustomerBranchID,
                                             ActName = instran.Name,
                                             Section = instran.Section,
                                             ShortDescription = instran.ShortDescription,
                                             ScheduledOn = (DateTime)schedule.ScheduleOn,
                                             User = instran.User,
                                             Penalty = (decimal)row.Penalty,
                                         }).ToList();

                if (location != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == location).Distinct().ToList();
                }                
                if (ActID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActID).Distinct().ToList();
                }
                if (Risk != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.RiskType == Risk).Distinct().ToList();
                }
                if (category != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryID == category).Distinct().ToList();
                }
                if (type != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceTypeID == type).Distinct().ToList();
                }
                if (DateFrom.ToString() != "1/1/1900 12:00:00 AM" && DateTo.ToString() != "1/1/1900 12:00:00 AM")
                {
                    ComplianceDetails = ComplianceDetails.Where(entry => (entry.ScheduledOn >= DateFrom && entry.ScheduledOn <= DateTo)).ToList();
                }                
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceScheduledID).Select(a => a.FirstOrDefault()).ToList();
                return ComplianceDetails;
            }
        }
        public static ComplianceTransaction GetComplianceTransaction(int TransactionID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTransaction = (from row in entities.ComplianceTransactions
                                             where row.ID == TransactionID
                                             select row).SingleOrDefault();

                return complianceTransaction;
            }
        }
        public static ComplianceTransaction GetComplianceTransactionPerformer(int ComplianceScheduleOnID, int ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTransaction = (from row in entities.ComplianceTransactions
                                             where row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                             && row.ComplianceInstanceId == ComplianceInstanceId
                                             && (row.StatusId == 2 || row.StatusId == 3)
                                             select row).OrderByDescending(m => m.ID).FirstOrDefault();

                return complianceTransaction;
            }
        }
        public static List<SP_GetPenaltyQuarterWise_Result> GetPenaltyComplianceDetailsDashboardPending(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "P" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && (row.RoleID == 3)
                                         select row).ToList();
                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (risk == "Critical")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 3).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.FirstOrDefault()).ToList();

                return ComplianceDetails;
            }
        }

        public static List<SP_GetPenaltyQuarterWise_Result> GetPenaltyComplianceDetailsDashboard(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, DateTime startdate, DateTime enddate, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "S" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && row.IsPenaltySave == false && row.RoleID == 4
                                         select row).ToList();

               
                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (risk == "Critical")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 3).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }

               
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                if (startdate.ToString() != "" && enddate.ToString() != "" && startdate.ToString() != "1/1/0001 12:00:00 AM" && enddate.ToString() != "1/1/0001 12:00:00 AM")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ScheduledOn >= startdate && row.ScheduledOn <= enddate).Distinct().ToList();
                }

                
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }

        public static List<SP_GetPenaltyQuarterWiseApprover_Result> GetPenaltyComplianceDetailsDashboardApprover(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, DateTime startdate, DateTime enddate, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWiseApprover(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "S" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && row.IsPenaltySave == false && row.RoleID == 6
                                         select row).ToList();

                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (risk == "Critical")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 3).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }

                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                if (startdate.ToString() != "" && enddate.ToString() != "" && startdate.ToString() != "1/1/0001 12:00:00 AM" && enddate.ToString() != "1/1/0001 12:00:00 AM")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ScheduledOn >= startdate && row.ScheduledOn <= enddate).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }      
        public static List<SP_GetPenaltyQuarterWiseApprover_Result> GetPenaltyComplianceDetailsDashboardPendingApprover(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                entities.Database.CommandTimeout = 180;
                var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWiseApprover(customerid, Userid)).ToList();

                if (Branchlist.Count > 0)
                {
                    MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
                                         where row.PenaltySubmit == "P" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         && (row.RoleID == 3)
                                         select row).ToList();
                if (risk == "High")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
                }
                if (risk == "Medium")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
                }
                if (risk == "Low")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
                }
                if (risk == "Critical")
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 3).Distinct().ToList();
                }
                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.FirstOrDefault()).ToList();

                return ComplianceDetails;
            }
        }


        //public static List<SP_GetPenaltyQuarterWise_Result> GetPenaltyComplianceDetailsDashboardPending(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, int PenaltyStatus, string risk, bool IsApprover = false)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {

        //        entities.Database.CommandTimeout = 180;
        //        var MasterPenaltyQuarterWiseQuery = (entities.SP_GetPenaltyQuarterWise(customerid, Userid)).ToList();

        //        if (Branchlist.Count > 0)
        //        {
        //            MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
        //        }

        //        var ComplianceDetails = (from row in MasterPenaltyQuarterWiseQuery
        //                                 where row.PenaltySubmit == "P" && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
        //                                 && row.IsPenaltySave == true && (row.RoleID == 3 || row.RoleID == 4)
        //                                 select row).ToList();

        //        //var ComplianceDetails = (from row in entities.PenaltyPendingViews
        //        //                         join acte in entities.Acts
        //        //                         on row.ActID equals acte.ID
        //        //                         join row2 in entities.EntitiesAssignments
        //        //                         on row.CustomerBranchID equals row2.BranchID
        //        //                         join user in entities.Users
        //        //                         on row.UserID equals user.ID
        //        //                         where row.CustomerID == customerid
        //        //                         && row.ComplianceCategoryId == row2.ComplianceCatagoryID
        //        //                         //&& row.ScheduledOn >= startDate
        //        //                         //&& row.ScheduledOn <= EndDate 
        //        //                         && row.Penalty != null && row.Interest != null
        //        //                         && row.PenaltySubmit == "P" && row.IsPenaltySave == true
        //        //                         && (row.RoleID == 3 || row.RoleID == 4)
        //        //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
        //        //                         && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
        //        //                         select new DashboardDataDisplayClass()
        //        //                         {
        //        //                             ActID = row.ActID,
        //        //                             ActName = acte.Name,
        //        //                             ShortDescription = row.ShortDescription,
        //        //                             CustomerBranchID = row.CustomerBranchID,
        //        //                             ComplianceInstanceID = row.ComplianceInstanceID,
        //        //                             ComplianceID = row.ComplianceID,
        //        //                             ComplianceCategoryId = acte.ComplianceCategoryId,
        //        //                             ComplianceStatusID = row.ComplianceStatusID,
        //        //                             ScheduledOn = row.ScheduledOn,
        //        //                             Risk = row.Risk,
        //        //                             ScheduledOnID = row.ScheduledOnID,
        //        //                             RoleID = row.RoleID,
        //        //                             User = user.FirstName + " " + user.LastName,
        //        //                             Penalty = row.Penalty,
        //        //                             Interest = row.Interest,
        //        //                             PenaltyStatus = row.PenaltySubmit,
        //        //                         }).ToList();
        //        if (risk == "High")
        //        {
        //            ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 0).Distinct().ToList();
        //        }
        //        if (risk == "Medium")
        //        {
        //            ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 1).Distinct().ToList();
        //        }
        //        if (risk == "Low")
        //        {
        //            ComplianceDetails = ComplianceDetails.Where(row => row.Risk == 2).Distinct().ToList();
        //        }
        //        if (Branchlist.Count > 0)
        //        {
        //            ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains(row.CustomerBranchID)).Distinct().ToList();
        //        }
        //        //if (CustomerBranchID != -1)
        //        //{
        //        //    ComplianceDetails = ComplianceDetails.Where(row => row.CustomerBranchID == CustomerBranchID).Distinct().ToList();
        //        //}
        //        if (ActId != -1)
        //        {
        //            ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
        //        }
        //        if (CategoryID != -1)
        //        {
        //            ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
        //        }
        //        //if (PenaltyStatus == 0)
        //        //{
        //        //    ComplianceDetails = ComplianceDetails.Where(row => row.PenaltyStatus == "S").Distinct().ToList();
        //        //}
        //        //else
        //        //{
        //        //    ComplianceDetails = ComplianceDetails.Where(row => row.PenaltyStatus == "P").Distinct().ToList();
        //        //}
        //        ComplianceDetails = ComplianceDetails.GroupBy(a => a.ScheduledOnID).Select(a => a.First()).ToList();
        //        return ComplianceDetails;
        //    }
        //}


        public static void UpdatePenaltyTransaction(ComplianceTransaction complianceTransaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction Transaction = (from row in entities.ComplianceTransactions
                                                     where row.ID == complianceTransaction.ID
                                                     select row).FirstOrDefault();

                Transaction.Interest = complianceTransaction.Interest;
                Transaction.Penalty = complianceTransaction.Penalty;
                Transaction.IsPenaltySave = false;
                Transaction.PenaltySubmit = complianceTransaction.PenaltySubmit;
                entities.SaveChanges();
            }
        }

        public static void UpdatePenaltyTransactionByReviewer(ComplianceTransaction complianceTransaction)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction Transaction = (from row in entities.ComplianceTransactions
                                                     where row.ComplianceInstanceId == complianceTransaction.ComplianceInstanceId
                                                     && row.ComplianceScheduleOnID == complianceTransaction.ComplianceScheduleOnID
                                                     && (row.StatusId == 2 || row.StatusId == 3)
                                                     select row).OrderByDescending(m => m.ID).FirstOrDefault();

                Transaction.Interest = complianceTransaction.Interest;
                Transaction.Penalty = complianceTransaction.Penalty;
                Transaction.IsPenaltySave = complianceTransaction.IsPenaltySave;
                Transaction.PenaltySubmit = complianceTransaction.PenaltySubmit;
                entities.SaveChanges();
            }
        }
        public static List<PenaltyDataDisplayClass> PenaltyReminderPerformer()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceDetails = (from row in entities.ComplianceTransactionViews
                                         join instran in entities.ComplianceAssignedInstancesViews
                                         on row.ComplianceInstanceId equals instran.ComplianceInstanceID
                                         where //instran.CustomerID == customerid && 
                                         (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                         && row.IsPenaltySave == true //&& row.PenaltySubmit == "P"
                                         && instran.RoleID == 3 //&& instran.UserID == UserID
                                         select new PenaltyDataDisplayClass()
                                         {
                                             UserID = (long) instran.UserID,
                                             ComplianceTransactionID = row.ComplianceTransactionID,
                                             ComplianceID = (long) instran.ComplianceID,
                                             RiskType = (int) instran.RiskType,
                                             ComplianceStatusID = (int) row.ComplianceStatusID,
                                             ComplianceInstanceID = row.ComplianceInstanceId,
                                             ComplianceScheduledID = (long) row.ComplianceScheduleOnID,
                                             Branch = instran.Branch,
                                             ComplianceCategoryID = instran.ComplianceCategoryId,
                                             ActID = (long) instran.ActID,
                                             Status = row.Status,
                                             ComplianceTypeID = (int) instran.ComplianceTypeID,
                                             CustomerBranchID = (int) instran.CustomerBranchID,
                                             ActName = instran.Name,
                                             Section = instran.Section,
                                             ShortDescription = instran.ShortDescription,
                                             ScheduledOn = (DateTime) instran.ScheduledOn,
                                             User = instran.User,
                                             Penalty = (decimal) row.Penalty,
                                         }).ToList();

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }

        public static String GetSearchList(String TextToSearch, int CustID, int UserID, String Role)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                String str = "<ul>";

                List<string> SearchResult = new List<string>();

                var Suggestions = (from row in entities.ComplianceAssignedInstancesViews
                                   where row.CustomerID == CustID
                                   select row).ToList();

                if (Role != "MGMT")
                    Suggestions = Suggestions.Where(a => a.UserID == UserID).ToList();

                var C = Suggestions.Where(i => i.ShortDescription.ToLower().Contains(TextToSearch.ToLower().Trim()) || i.Description.ToLower().Contains(TextToSearch.ToLower().Trim())).ToList();

                if (C.Count > 0)
                {
                    str += "<li class='eborder-top'><a href='../Management/SearchResult.aspx?Type=C&SearchText=" + TextToSearch + "'>" + TextToSearch + " in Compliance" + "</a></li>";
                }

                var A = Suggestions.Where(i => i.Name.ToLower().Contains(TextToSearch.ToLower().Trim())).ToList();

                if (A.Count > 0)
                {
                    str += "<li class='eborder-top' style='margin-left:0px;'><a href='../Management/SearchResult.aspx?Type=A&SearchText=" + TextToSearch + "'>" + TextToSearch + " in Act" + "</a></li>";
                }

                var InformativeSuggestions = (from row in entities.SP_GetInformativeCompliance()
                                   where (row.ShortDescription.ToLower().Contains(TextToSearch) || row.Description.ToLower().Contains(TextToSearch))
                                   select row).ToList();

                if (InformativeSuggestions.Count > 0)
                {
                    str += "<li class='eborder-top' style='margin-left:0px;'><a href='../Management/SearchResult.aspx?Type=I&SearchText=" + TextToSearch + "'>" + TextToSearch + " in Informative Complinace" + "</a></li>";
                }

                String texttosearch = "informative";
                if (texttosearch.Contains(TextToSearch.ToLower()))
                {
                    str += "<li class='eborder-top' style='margin-left:0px;'><a href='../Management/SearchResult.aspx?Type=AI&SearchText=" + TextToSearch + "'> All Informative Complinace" + "</a></li>";
                }

                if (Role == "MGMT")
                {
                    var UserSuggestions = (from row in entities.Users
                                           where row.CustomerID == CustID
                                           && (row.FirstName.ToLower().Contains(TextToSearch) || row.LastName.ToLower().Contains(TextToSearch))
                                           select row).ToList();

                    if (UserSuggestions.Count > 0)
                    {
                        str += "<li class='eborder-top'><a href='../Management/SearchResult.aspx?Type=U&SearchText=" + TextToSearch + "'>" + TextToSearch + " in User" + "</a></li>";
                    }
                }

                if (str == "<ul>")
                    str += "<li class='eborder-top'><a>No Result Found</a></li>";

                str += "</ul>";

                return str;
            }
        }

        public static List<ComplianceAssignedInstancesView> GetComplianceDetailsByText(String TextToSearch, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceDetails = (from row in entities.ComplianceAssignedInstancesViews
                                         where row.CustomerID == CustomerID
                                         && (row.ShortDescription.ToLower().Contains(TextToSearch) || row.Description.ToLower().Contains(TextToSearch))
                                         select row).GroupBy(a => a.ComplianceID).Select(a => a.FirstOrDefault()).ToList();

                return ComplianceDetails;
            }
        }

        public static ManagementColor Getcolor(int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ManagementColor query = (from row in entities.ManagementColors
                                         where row.UserId == userid
                                         select row).FirstOrDefault();

                return query;
            }
        }
        public bool AddupdateColordata(ManagementColor Tempassignments)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ManagementColor eventToUpdate = (from row in entities.ManagementColors
                                                     where row.UserId == Tempassignments.UserId
                                                     select row).FirstOrDefault();

                    if (eventToUpdate == null)
                    {
                        entities.ManagementColors.Add(Tempassignments);
                        entities.SaveChanges();
                    }
                    else
                    {
                        eventToUpdate.High = Tempassignments.High;
                        eventToUpdate.Medium = Tempassignments.Medium;
                        eventToUpdate.Low = Tempassignments.Low;
                        eventToUpdate.Critical = Tempassignments.Critical;
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch { return false; }

        }
        public static List<string> GetMailList(int CustiD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var listofemails = (from row in entities.Customers
                                    join row1 in entities.Users
                                    on row.ID equals row1.CustomerID
                                    where row.ID == CustiD
                                    select row1.Email).ToList();
                return listofemails;
            }
        }
        public static List<GetInternalComplianceDocumentsView> GetInternalComplianceDocuments(long ScheduledOnID, long transactionID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DocumentList = (from row in entities.GetInternalComplianceDocumentsViews
                                    where row.InternalComplianceScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return DocumentList;
            }
        }
        public bool UpdateWidget(int userid, string controlid, bool yes)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    widget eventToUpdate = (from row in entities.widgets
                                            where row.UserId == userid
                                            select row).FirstOrDefault();

                    if (controlid == "compliancesummary") { eventToUpdate.Performer = yes; }
                    if (controlid == "dailyupdates") { eventToUpdate.DailyUpdate = yes; }
                    if (controlid == "functionsummary") { eventToUpdate.PerformerLocation = yes; }
                    if (controlid == "newsletter") { eventToUpdate.NewsLetter = yes; }
                    if (controlid == "performersummary") { eventToUpdate.Performer = yes; }
                    if (controlid == "performerlocation") { eventToUpdate.PerformerLocation = yes; }
                    if (controlid == "reviewersummary") { eventToUpdate.Reviewer = yes; }
                    if (controlid == "reviewerlocation") { eventToUpdate.ReviewerLocation = yes; }
                    if (controlid == "riskcriteria") { eventToUpdate.RiskCriteria = yes; }
                    if (controlid == "eventownersummary") { eventToUpdate.EventOwner = yes; }
                    if (controlid == "performersummarytask") { eventToUpdate.TaskSummary = yes; }
                    if (controlid == "reviewersummarytask") { eventToUpdate.ReviewerTaskSummary = yes; }
                    if (controlid == "ComplianceCalender") { eventToUpdate.compliancecalender = yes; }
                    if (controlid == "CustomWidgetCriteria") { eventToUpdate.CustomWidget = yes; }
                    if (controlid == "DepartmentCriteria") { eventToUpdate.Department = yes; }
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static void AddDetailsTempAssignmentTable(List<TempAssignmentTable> Tempassignments)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    entities.TempAssignmentTables.Add(entry);
                    entities.SaveChanges();

                });
            }

        }

        public static bool AddDetailsTempAssignmentTableLicense(List<TempAssignmentTable> Tempassignments)
        {
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Tempassignments.ForEach(entry =>
                    {
                        entities.TempAssignmentTables.Add(entry);
                        entities.SaveChanges();
                       
                    });
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool AddDetailsTempAssignmentInternalTableLicense(List<TempAssignmentTableInternal> Tempassignments)
        {
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Tempassignments.ForEach(entry =>
                    {
                        entities.TempAssignmentTableInternals.Add(entry);
                        entities.SaveChanges();

                    });
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static void AddDetailsTempAssignmentInternalTable(List<TempAssignmentTableInternal> Tempassignments)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Tempassignments.ForEach(entry =>
                {
                    entities.TempAssignmentTableInternals.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static bool Addemaildata(Email Tempassignments)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Emails.Add(Tempassignments);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch { return false; }

        }

        public static List<long> GetShorterNotice()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> ShorterNoticeList = (from row in entities.ShorterNotices
                                                join row1 in entities.Compliances
                                                on row.ComplianceID equals row1.ID
                                                where row1.IsDeleted == false
                                                select row.ComplianceID).ToList();

                return ShorterNoticeList;
            }
        }
        public static void RemoveUpcomingSchudule(int complianceID, DateTime deactiveDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance compliance = (from row in entities.Compliances
                                         where row.ID == complianceID
                                         select row).FirstOrDefault();

                //if (ComplianceIsEventBased(complianceID))
                //{
                var complianceInstances = (from row in entities.ComplianceInstances
                                           where row.IsDeleted == false && row.ComplianceId == complianceID                                         
                                           select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                //ComplianceInstances
                complianceInstances.ForEach(complianceInstancesentry =>
                {
                    var complianceScheduleOns = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstancesentry.ID && row.ScheduleOn >= deactiveDate
                                                 select row).ToList();
                    //ComplianceScheduleOns
                    complianceScheduleOns.ForEach(complianceScheduleOnssentry =>
                    {
                        var complianceAssignments = (from row in entities.ComplianceAssignments
                                                     where row.ComplianceInstanceID == complianceScheduleOnssentry.ComplianceInstanceID
                                                     select row).ToList();

                        var complianceTransactions = (from row in entities.ComplianceTransactions
                                                      where row.ComplianceInstanceId == complianceScheduleOnssentry.ComplianceInstanceID
                                                      && row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID //&& row.StatusId == 1
                                                      select row).ToList();

                        if (complianceTransactions.Count < 2)
                        {

                            //Remove ComplianceTransactions
                            complianceTransactions.ForEach(entry =>
                            entities.ComplianceTransactions.Remove(entry));
                            entities.SaveChanges();

                            var taskTransactions = (from row in entities.TaskTransactions
                                                          where row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID 
                                                          select row).ToList();

                            var taskReminders = (from row in entities.TaskReminders
                                                       where row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID
                                                       && row.TaskDueDate >= deactiveDate
                                                       select row).ToList();

                            if (taskTransactions.Count < 2)
                            {
                                //Remove TaskTransactions
                                taskTransactions.ForEach(taskentry =>
                                entities.TaskTransactions.Remove(taskentry));
                                entities.SaveChanges();

                                //Remove TaskReminders
                                taskReminders.ForEach(taskReminderentry =>
                                entities.TaskReminders.Remove(taskReminderentry));
                                entities.SaveChanges();
                            }

                            complianceAssignments.ForEach(complianceAssignmentsentry =>
                            {
                                var complianceReminders = (from row in entities.ComplianceReminders
                                                           where row.ComplianceAssignmentID == complianceAssignmentsentry.ID
                                                           && row.ComplianceDueDate >= deactiveDate
                                                           select row).ToList();
                            //Remove ComplianceReminders
                            complianceReminders.ForEach(entry =>
                                entities.ComplianceReminders.Remove(entry));
                                entities.SaveChanges();
                            });
                            //Remove ComplianceScheduleOns
                            entities.ComplianceScheduleOns.Remove(complianceScheduleOnssentry);
                            entities.SaveChanges();
                        }
                    });
                });
                //}

            }
        }




        public static List<ComplianceView> GetDeactivateCompliance(int ComplianceTypeID, int ComplianceCatagoryId, int frq, int CmType, string filter = null)
        {
            List<ComplianceView> complianceInformation = new List<ComplianceView>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliancesQuery = (from row in entities.ComplianceViews
                                        join row1 in entities.Compliances
                                        on row.ID equals row1.ID
                                        where row1.Status == "D"
                                        select row);

                int risk = -1;
                if (filter.ToUpper().Equals("HIGH"))
                    risk = 0;
                else if (filter.ToUpper().Equals("MEDIUM"))
                    risk = 1;
                else if (filter.ToUpper().Equals("LOW"))
                    risk = 2;
                else if (filter.ToUpper().Equals("CRITICAL"))
                    risk = 3;

                string frequency = filter.ToUpper();
                if (frequency.Equals("MONTHLY"))
                    frequency = "Monthly";
                if (frequency.Equals("QUARTERLY"))
                    frequency = "Quarterly";
                if (frequency.Equals("HALFYEARLY"))
                    frequency = "HalfYearly";
                if (frequency.Equals("ANNUAL"))
                    frequency = "Annual";
                if (frequency.Equals("FOURMONTHALY"))
                    frequency = "FourMonthly";
                if (frequency.Equals("TWOYEARLY"))
                    frequency = "TwoYearly";
                if (frequency.Equals("SEVENYEARLY"))
                    frequency = "SevenYearly";

                int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                if (frequencyId == 0 && frequency != "Monthly")
                {
                    frequencyId = -1;
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper()) || entry.Frequency == frequencyId || entry.RiskType == risk);
                }

                if (ComplianceTypeID != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == ComplianceTypeID);
                }
                if (ComplianceCatagoryId != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == ComplianceCatagoryId);
                }
                if (CmType != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType);
                }
                if (frq != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == frq);
                }

                var compliances = compliancesQuery.ToList();


                compliances.ForEach(entry =>
                {
                    complianceInformation.Add(entry);
                });
                return complianceInformation;
            }


        }


        public static ComplianceDeactive GetComplianceDeactivate(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.ComplianceDeactives
                            where row.ComplianceID == complianceID
                            select row).SingleOrDefault();

                return form;
            }
        }
        public static List<ComplianceDeactive> GetComplianceDeactivateList(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.ComplianceDeactives
                            where row.ComplianceID == complianceID
                            select row);

                return form.ToList();
            }
        }
        public static Compliance GetCompliance(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.Compliances
                            where row.ID == complianceID
                            select row).SingleOrDefault();

                return form;
            }
        }
        public static void CreateDeactivateFile(ComplianceDeactive form, bool deleteOldFiles = true)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var formIDsToDelete = (from row in entities.ComplianceDeactives
                                       where row.ComplianceID == form.ComplianceID
                                       select row).SingleOrDefault();

                entities.ComplianceDeactives.Add(form);
                entities.SaveChanges();
            }

        }

        public static void DeleteOldDeactiveDoc(long CompliancID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.ComplianceDeactives
                           where row.ComplianceID == CompliancID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    ComplianceDeactive prevmappedids = (from row in entities.ComplianceDeactives
                                                        where row.ID == entry
                                                        select row).FirstOrDefault();
                    entities.ComplianceDeactives.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }
        public static void ChangeStatus(int complianceID, DateTime? DeactiveDate, string Status, string Desc)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance complianceStatus = (from row in entities.Compliances
                                               where row.ID == complianceID
                                               select row).FirstOrDefault();

                complianceStatus.Status = Status;
                complianceStatus.DeactivateOn = DeactiveDate;
                complianceStatus.DeactivateDesc = Desc;
                entities.SaveChanges();
            }
        }

        public static void UpdateComplianceDate(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance complianceStatus = (from row in entities.Compliances
                                               where row.ID == complianceID
                                               select row).FirstOrDefault();

                complianceStatus.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static List<Compliance> TempGetByType(int complianceProductType, int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, List<int> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<Act> actList = new List<Act>();

                actList = (from row in entities.Acts
                           where row.IsDeleted == false
                           select row).ToList();


                if (complianceCatagoryId != -1)
                {
                    actIDs = (from row in actList
                              where row.ComplianceCategoryId == complianceCatagoryId
                              select row.ID).ToList();
                }
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (complianceCatagoryId != -1 && complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId && row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (actIdList != null)
                {
                    actIDs = actIdList;
                }
                
                List<Compliance> compliances = (from row in entities.Compliances
                                                where actIDs.Contains(row.ActID) && row.IsDeleted == false
                                                && (row.ComplianceType == 0 || row.ComplianceType == 2 || row.ComplianceType == 3)
                                                && row.EventFlag == null && row.Status == null
                                                && row.ComplinceVisible != false //Add condition 18 Feb 2019
                                                && row.IsMapped != false  //Added condition 19 NOV 2020
                                                select row).ToList();

                var TempQuery = (from row in entities.TempAssignmentTables
                                 where row.CustomerBranchID == lcCustomerBranch && row.IsActive == true && row.UserID !=-1
                                 select row.ComplianceId).ToList();

                compliances.RemoveAll(a => TempQuery.Contains(a.ID));

                var TempQueryAssignInstance = (from row in entities.ComplianceInstances
                                               join row1 in entities.ComplianceAssignments
                                               on row.ID equals row1.ComplianceInstanceID
                                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                               select row.ComplianceId).ToList();

                //var TempQueryAssignInstance = (from row in entities.ComplianceInstances
                //                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                //                               select row.ComplianceId).ToList();

                compliances.RemoveAll(b => TempQueryAssignInstance.Contains(b.ID));

                if (setApprover != false)
                {
                    List<long> query = (from row in entities.ComplianceInstances
                                        join ca in entities.ComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.ComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                        select row.ComplianceId).ToList();

                    compliances = compliances.Where(entry => query.Contains(entry.ID)).ToList();

                }
                var licensecompiancedetails = (from row in entities.Lic_tbl_LicType_Compliance_Mapping
                                               where row.IsDeleted                                               
                                               select row.ComplianceID).ToList();

                if (compliances.Count > 0)
                {
                    compliances = compliances.Where(entry => !licensecompiancedetails.Contains(entry.ID)).ToList();
                }

                //HR Compliance Check
                if (complianceProductType >= 2)
                {
                    var hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                                         where row.IsDeleted == false
                                         && row.EventFlag == null && row.Status == null
                                         select row.ID).ToList();

                    if (hrCompliances.Count > 0)
                    {
                        compliances = compliances.Where(entry => !hrCompliances.Contains(entry.ID)).ToList();
                    }
                }
              

                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        compliances = compliances.Where(entry => entry.ID == a).ToList();
                    }
                    else
                    {
                        compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                    }

                    }

                return compliances;
            }
        }

        public static List<Compliance> TempGetByTypeFilterEvent(int complianceTypeID, int complianceCatagoryId, string EventChecked, bool setApprover = false, int lcCustomerBranch = -1, List<int> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<Act> actList = new List<Act>();

                actList = (from row in entities.Acts
                           where row.IsDeleted == false
                           select row).ToList();


                if (complianceCatagoryId != -1)
                {
                    actIDs = (from row in actList
                              where row.ComplianceCategoryId == complianceCatagoryId
                              select row.ID).ToList();
                }
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }
                if (complianceCatagoryId != -1 && complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId && row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (actIdList != null)
                {
                    actIDs = actIdList;
                }

                List<Compliance> compliances = new List<Compliance>();
                if (EventChecked == "Y")
                {
                    compliances = (from row in entities.Compliances
                                   where actIDs.Contains(row.ActID) && row.IsDeleted == false
                                   && (row.ComplianceType == 0 || row.ComplianceType == 2 || row.ComplianceType == 3)
                                   && row.EventFlag == true && row.Status == null
                                   && row.IsMapped != false  //Added condition 19 NOV 2020
                                   select row).ToList();
                }
                else
                {
                    compliances = (from row in entities.Compliances
                                   where actIDs.Contains(row.ActID) && row.IsDeleted == false
                                   && (row.ComplianceType == 0 || row.ComplianceType == 2 || row.ComplianceType == 3)
                                   && row.Status == null
                                   && row.IsMapped != false  //Added condition 19 NOV 2020
                                   select row).ToList();
                }
                var TempQuery = (from row in entities.TempAssignmentTables
                                 where row.CustomerBranchID == lcCustomerBranch && row.IsActive == true && row.UserID != -1
                                 select row.ComplianceId).ToList();

                compliances.RemoveAll(a => TempQuery.Contains(a.ID));
                //var TempQueryAssignInstance = (from row in entities.ComplianceInstances
                //                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                //                               select row.ComplianceId).ToList();

                //added by sachin on 10 OCT 2017 if compliance is exclude
                var TempQueryAssignInstance = (from row in entities.ComplianceInstances
                                               join row1 in entities.ComplianceAssignments
                                               on row.ID equals row1.ComplianceInstanceID
                                               where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                               select row.ComplianceId).ToList();

                compliances.RemoveAll(b => TempQueryAssignInstance.Contains(b.ID));
                if (setApprover != false)
                {
                    List<long> query = (from row in entities.ComplianceInstances
                                        join ca in entities.ComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.ComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                        select row.ComplianceId).ToList();

                    compliances = compliances.Where(entry => query.Contains(entry.ID)).ToList();

                }

              
                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter.ToUpper());
                        compliances = compliances.Where(entry => entry.ID == a).ToList();
                    }
                    else
                    {
                        compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();

                    }
                }

                return compliances;
            }
        }


        public static List<Compliance> GetComplianceForRevise(int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, List<int> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<Act> actList = new List<Act>();

                actList = (from row in entities.Acts
                           where row.IsDeleted == false
                           select row).ToList();


                if (complianceCatagoryId != -1)
                {
                    actIDs = (from row in actList
                              where row.ComplianceCategoryId == complianceCatagoryId
                              select row.ID).ToList();
                }
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (complianceCatagoryId != -1 && complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId && row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (actIdList != null)
                {
                    actIDs = actIdList;
                }

                List<Compliance> compliances = (from row in entities.Compliances
                                                where actIDs.Contains(row.ActID) && row.IsDeleted == false && (row.ComplianceType == 0 || row.ComplianceType == 2)
                                                // && row.EventFlag == null
                                                select row).ToList();

                if (setApprover != false)
                {
                    List<long> query = (from row in entities.ComplianceInstances
                                        join ca in entities.ComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.ComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                        select row.ComplianceId).ToList();

                    compliances = compliances.Where(entry => query.Contains(entry.ID)).ToList();

                }

                if (!string.IsNullOrEmpty(filter))
                {
                    compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return compliances;
            }
        }


        public static List<Compliance> GetByType(int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, List<int> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<Act> actList = new List<Act>();

                actList = (from row in entities.Acts
                           where row.IsDeleted == false
                           select row).ToList();


                if (complianceCatagoryId != -1)
                {
                    actIDs = (from row in actList
                              where row.ComplianceCategoryId == complianceCatagoryId
                              select row.ID).ToList();
                }
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (complianceCatagoryId != -1 && complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId && row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (actIdList != null)
                {
                    actIDs = actIdList;
                }

                List<Compliance> compliances = (from row in entities.Compliances
                                                where actIDs.Contains(row.ActID) && row.IsDeleted == false && (row.ComplianceType == 0 || row.ComplianceType == 2)
                                                && row.EventFlag == null
                                                select row).ToList();

                if (setApprover != false)
                {
                    List<long> query = (from row in entities.ComplianceInstances
                                        join ca in entities.ComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.ComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                        select row.ComplianceId).ToList();

                    compliances = compliances.Where(entry => query.Contains(entry.ID)).ToList();

                }

                if (!string.IsNullOrEmpty(filter))
                {
                    compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return compliances;
            }
        }

        public static List<Compliance> GetByTypeFilterEvent(int complianceTypeID, int complianceCatagoryId, string EventChecked, bool setApprover = false, int lcCustomerBranch = -1, List<int> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<Act> actList = new List<Act>();

                actList = (from row in entities.Acts
                           where row.IsDeleted == false
                           select row).ToList();


                if (complianceCatagoryId != -1)
                {
                    actIDs = (from row in actList
                              where row.ComplianceCategoryId == complianceCatagoryId
                              select row.ID).ToList();
                }
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (complianceCatagoryId != -1 && complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId && row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (actIdList != null)
                {
                    actIDs = actIdList;
                }

                List<Compliance> compliances = new List<Compliance>();
                if (EventChecked == "Y")
                {
                    compliances = (from row in entities.Compliances
                                   where actIDs.Contains(row.ActID) && row.IsDeleted == false && (row.ComplianceType == 0 || row.ComplianceType == 1)
                                  && row.EventFlag == true
                                   select row).ToList();
                }
                else
                {

                    compliances = (from row in entities.Compliances
                                   where actIDs.Contains(row.ActID) && row.IsDeleted == false && (row.ComplianceType == 0 || row.ComplianceType == 2)
                                   select row).ToList();
                }
                if (setApprover != false)
                {
                    List<long> query = (from row in entities.ComplianceInstances
                                        join ca in entities.ComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.ComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                        select row.ComplianceId).ToList();

                    compliances = compliances.Where(entry => query.Contains(entry.ID)).ToList();

                }

                if (!string.IsNullOrEmpty(filter))
                {
                    compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return compliances;
            }
        }


        


        public static List<ComplianceView> GetAll1(int ComplianceTypeID, int ComplianceCatagoryId, int frq, int CmType, string filter = null)
        {
            List<ComplianceView> complianceInformation = new List<ComplianceView>();
            // return compliances;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliancesQuery = (from row in entities.ComplianceViews
                                        where row.EventFlag == null
                                        select row);

                int risk = -1;
                if (filter.ToUpper().Equals("HIGH"))
                    risk = 0;
                else if (filter.ToUpper().Equals("MEDIUM"))
                    risk = 1;
                else if (filter.ToUpper().Equals("LOW"))
                    risk = 2;
                else if (filter.ToUpper().Equals("CRITICAL"))
                    risk = 3;

                string frequency = filter.ToUpper();
                if (frequency.Equals("MONTHLY"))
                    frequency = "Monthly";
                if (frequency.Equals("QUARTERLY"))
                    frequency = "Quarterly";
                if (frequency.Equals("HALFYEARLY"))
                    frequency = "HalfYearly";
                if (frequency.Equals("ANNUAL"))
                    frequency = "Annual";
                if (frequency.Equals("FOURMONTHALY"))
                    frequency = "FourMonthly";
                if (frequency.Equals("TWOYEARLY"))
                    frequency = "TwoYearly";
                if (frequency.Equals("SEVENYEARLY"))
                    frequency = "SevenYearly";

                int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                if (frequencyId == 0 && frequency != "Monthly")
                {
                    frequencyId = -1;
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter);
                        compliancesQuery = compliancesQuery.Where(entry => entry.ID == a);
                    }
                    else
                    {
                        compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper()) || entry.Frequency == frequencyId || entry.RiskType == risk);
                    }
                }

                if (ComplianceTypeID != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == ComplianceTypeID);
                }
                if (ComplianceCatagoryId != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == ComplianceCatagoryId);
                }
                if (CmType != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType);
                }
                if (frq != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == frq);
                }

                var compliances = compliancesQuery.ToList();

                //if (withParameters)
                //{
                //    var complianceIDs = compliances.Select(entry => entry.ID).ToList();

                //    var complianceParameters = (from row in entities.ComplianceParameters
                //                                where row.IsDeleted == false && complianceIDs.Contains(row.ComplianceId)
                //                                select row).ToList();

                //    compliances.ForEach(entry =>
                //    {
                //        complianceInformation.Add(entry, complianceParameters.Where(param => param.ComplianceId == entry.ID).ToList());
                //    });
                //}
                //else
                //{
                compliances.ForEach(entry =>
                {
                    complianceInformation.Add(entry);
                });
                //}

                return complianceInformation;
            }


        }

        public static List<ComplianceView> GetAllEditCompliance1(int ComplianceTypeID, int ComplianceCatagoryId, int frq, int CmType, int actId, string filter = null)
        {
            List<ComplianceView> complianceInformation = new List<ComplianceView>();
            // return compliances;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliancesQuery = (from row in entities.ComplianceViews
                                        where row.EventFlag == null
                                        select row);

                int risk = -1;
                if (filter.ToUpper().Equals("HIGH"))
                    risk = 0;
                else if (filter.ToUpper().Equals("MEDIUM"))
                    risk = 1;
                else if (filter.ToUpper().Equals("LOW"))
                    risk = 2;
                else if (filter.ToUpper().Equals("CRITICAL"))
                    risk = 3;

                string frequency = filter.ToUpper();
                if (frequency.Equals("MONTHLY"))
                    frequency = "Monthly";
                if (frequency.Equals("QUARTERLY"))
                    frequency = "Quarterly";
                if (frequency.Equals("HALFYEARLY"))
                    frequency = "HalfYearly";
                if (frequency.Equals("ANNUAL"))
                    frequency = "Annual";
                if (frequency.Equals("FOURMONTHALY"))
                    frequency = "FourMonthly";
                if (frequency.Equals("TWOYEARLY"))
                    frequency = "TwoYearly";
                if (frequency.Equals("SEVENYEARLY"))
                    frequency = "SevenYearly";

                int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                if (frequencyId == 0 && frequency != "Monthly")
                {
                    frequencyId = -1;
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter);
                        compliancesQuery = compliancesQuery.Where(entry => entry.ID == a);
                    }
                    else
                    {
                        compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper()) || entry.Frequency == frequencyId || entry.RiskType == risk);
                    }
                }
                if (actId != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ActID == actId);
                }
                if (ComplianceTypeID != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == ComplianceTypeID);
                }
                if (ComplianceCatagoryId != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == ComplianceCatagoryId);
                }
                if (CmType != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType);
                }
                if (frq != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == frq);
                }

                var compliances = compliancesQuery.ToList();

                //if (withParameters)
                //{
                //    var complianceIDs = compliances.Select(entry => entry.ID).ToList();

                //    var complianceParameters = (from row in entities.ComplianceParameters
                //                                where row.IsDeleted == false && complianceIDs.Contains(row.ComplianceId)
                //                                select row).ToList();

                //    compliances.ForEach(entry =>
                //    {
                //        complianceInformation.Add(entry, complianceParameters.Where(param => param.ComplianceId == entry.ID).ToList());
                //    });
                //}
                //else
                //{
                compliances.ForEach(entry =>
                {
                    complianceInformation.Add(entry);
                });
                //}

                return complianceInformation;
            }


        }

        public static List<ComplianceView> GetAllEventCompliances(int ComplianceTypeID, int ComplianceCatagoryId, int frq, int CmType, string filter = null)
        {
            List<ComplianceView> complianceInformation = new List<ComplianceView>();
            // return compliances;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliancesQuery = (from row in entities.ComplianceViews
                                        where row.EventFlag == true
                                        select row);

                int risk = -1;
                if (filter.ToUpper().Equals("HIGH"))
                    risk = 0;
                else if (filter.ToUpper().Equals("MEDIUM"))
                    risk = 1;
                else if (filter.ToUpper().Equals("LOW"))
                    risk = 2;
                else if (filter.ToUpper().Equals("CRITICAL"))
                    risk = 3;

                string frequency = filter.ToUpper();
                if (frequency.Equals("MONTHLY"))
                    frequency = "Monthly";
                if (frequency.Equals("QUARTERLY"))
                    frequency = "Quarterly";
                if (frequency.Equals("HALFYEARLY"))
                    frequency = "HalfYearly";
                if (frequency.Equals("ANNUAL"))
                    frequency = "Annual";
                if (frequency.Equals("FOURMONTHALY"))
                    frequency = "FourMonthly";
                if (frequency.Equals("TWOYEARLY"))
                    frequency = "TwoYearly";
                if (frequency.Equals("SEVENYEARLY"))
                    frequency = "SevenYearly";

                int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                if (frequencyId == 0 && frequency != "Monthly")
                {
                    frequencyId = -1;
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper()) || entry.Frequency == frequencyId || entry.RiskType == risk);
                }

                if (ComplianceTypeID != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == ComplianceTypeID);
                }
                if (ComplianceCatagoryId != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == ComplianceCatagoryId);
                }
                if (CmType != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType);
                }
                if (frq != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == frq);
                }

                var compliances = compliancesQuery.ToList();


                compliances.ForEach(entry =>
                {
                    complianceInformation.Add(entry);
                });

                return complianceInformation;
            }


        }

        public static Compliance GetByCustomerspecificcomplianceID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Compliances
                                  where row.ID == complianceID
                                  select row).FirstOrDefault();

                return compliance;
            }
        }

        public static Customer GetCustomerNameByID(string customername)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Customers
                            where row.Name == customername
                            select row).FirstOrDefault();

                return data;
            }
        }
        public static Compliance GetByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Compliances.Include("ComplianceParameters")
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }

        public static Compliance GetComplianceByInstanceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.ComplianceTransactions
                                             where row.ComplianceScheduleOnID == ScheduledOnID
                                             select row.ComplianceInstanceId).FirstOrDefault();

                long complianceID = (from row in entities.ComplianceInstances
                                     where row.ID == complianceInstanceID && row.IsDeleted == false
                                     select row.ComplianceId).FirstOrDefault();

                var compliance = (from row in entities.Compliances
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }

        public static ComplianceCategory GetComplianceCategoryByInstanceID(int Actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Acts
                                  join row2 in entities.ComplianceCategories
                                  on row.ComplianceCategoryId equals row2.ID
                                  where row.ID == Actid
                                  select row2).FirstOrDefault();

                return compliance;
            }
        }
        public static Compliance CheckMonetary(int complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var file = (from row in entities.Compliances
                            where row.ID == complianceid
                            && row.NonComplianceType  !=null
                            select row).FirstOrDefault();

                return file;
            }
        }
        public static string GetPanalty(Compliance compliance)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string Penalty = "";
                if (compliance.NonComplianceType != null)
                {
                    if (compliance.NonComplianceType == 0)
                    {
                        Penalty = "Penalty of minimum " + compliance.FixedMinimum + " and maximum " + compliance.FixedMaximum + " Rupees is applicable.";
                    }
                    else if (compliance.NonComplianceType == 1)
                    {
                        if (Convert.ToBoolean(compliance.Imprisonment))
                        {
                            Penalty = "Penalty of minimum " + compliance.MinimumYears + " and maximum " + compliance.MaximumYears + " years of Imprisonment for " + compliance.Designation + " of the company.";
                        }
                        else
                        {
                            Penalty = compliance.Others;
                        }

                    }
                    else if (compliance.NonComplianceType == 2)
                    {
                        string Panalty1 = "Penalty of minimum " + compliance.FixedMinimum + " and maximum " + compliance.FixedMaximum + " Rupees is applicable";
                        string Panalty2 = "minimum " + compliance.MinimumYears + " and maximum " + compliance.MaximumYears + " years of Imprisonment for " + compliance.Designation + " of the company.";
                        Penalty = Panalty1 + " and " + Panalty2;
                    }
                }
                else
                {
                    Penalty = "No Penalty Defined.";
                }

                return Penalty;
            }
        }

        public static string GetRisk(Compliance compliance, long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string risk = "";
                if (ComplianceInstanceID != -1)
                {
                    var CInstance = (from row in entities.ComplianceInstances
                                     where row.ID == ComplianceInstanceID
                                     select row.Risk).FirstOrDefault();

                    if (!string.IsNullOrEmpty(Convert.ToString(CInstance)))
                    {
                        if (CInstance == 0)
                            risk = "HIGH";
                        else if (CInstance == 1)
                            risk = "MEDIUM";
                        else if (CInstance == 2)
                            risk = "LOW";
                        else if (CInstance == 3)
                            risk = "CRITICAL";

                    }
                    else
                    {
                        if (compliance.RiskType == 0)
                            risk = "HIGH";
                        else if (compliance.RiskType == 1)
                            risk = "MEDIUM";
                        else if (compliance.RiskType == 2)
                            risk = "LOW";
                        else if (compliance.RiskType == 3)
                            risk = "CRITICAL";

                    }
                }
                else
                {
                    if (compliance.RiskType == 0)
                        risk = "HIGH";
                    else if (compliance.RiskType == 1)
                        risk = "MEDIUM";
                    else if (compliance.RiskType == 2)
                        risk = "LOW";
                    else if (compliance.RiskType == 3)
                        risk = "CRITICAL";

                }
                return risk;
            }
        }

        public static string GetRiskType(Compliance compliance,long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string risk = "";
                if (ComplianceInstanceID !=-1)
                {
                    var CInstance = (from row in entities.ComplianceInstances
                                     where row.ID == ComplianceInstanceID
                                     select row.Risk).FirstOrDefault();
                 
                    if (!string.IsNullOrEmpty(Convert.ToString(CInstance)))
                    {
                        if (CInstance == 0)
                            risk = "HIGH";
                        else if (CInstance == 1)
                            risk = "MEDIUM";
                        else if (CInstance == 2)
                            risk = "LOW";
                        else if (CInstance == 3)
                            risk = "CRITICAL";
                    }
                    else
                    {
                        if (compliance.RiskType == 0)
                            risk = "HIGH";
                        else if (compliance.RiskType == 1)
                            risk = "MEDIUM";
                        else if (compliance.RiskType == 2)
                            risk = "LOW";
                        else if (compliance.RiskType == 3)
                            risk = "CRITICAL";

                    }
                }              
                else
                {
                    if (compliance.RiskType == 0)
                        risk = "HIGH";
                    else if (compliance.RiskType == 1)
                        risk = "MEDIUM";
                    else if (compliance.RiskType == 2)
                        risk = "LOW";
                    else if (compliance.RiskType == 3)
                        risk = "CRITICAL";

                }
                return risk;
            }
        }
        public static string GetRiskType(int complianceMasterRisk, int ClientSpecificComplianceInstanceRisk)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string risk = "";
                if (ClientSpecificComplianceInstanceRisk != -1)
                {
                    if (ClientSpecificComplianceInstanceRisk == 0)
                        risk = "HIGH";
                    else if (ClientSpecificComplianceInstanceRisk == 1)
                        risk = "MEDIUM";
                    else if (ClientSpecificComplianceInstanceRisk == 2)
                        risk = "LOW";
                    else if (ClientSpecificComplianceInstanceRisk == 3)
                        risk = "CRITICAL";
                }
                else
                {
                    if (complianceMasterRisk == 0)
                        risk = "HIGH";
                    else if (complianceMasterRisk == 1)
                        risk = "MEDIUM";
                    else if (complianceMasterRisk == 2)
                        risk = "LOW";
                    else if (complianceMasterRisk == 3)
                        risk = "CRITICAL";

                }
                return risk;
            }
        }

        public static void Delete(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance complianceToDelete = (from row in entities.Compliances
                                                 where row.ID == complianceID
                                                 select row).FirstOrDefault();

                complianceToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }

        //public static void ChangeStatus(int complianceID)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        Compliance complianceStatus = (from row in entities.Compliances
        //                                         where row.ID == complianceID
        //                                         select row).FirstOrDefault();
        //        if (complianceStatus.)

        //        complianceStatus.Status = false;
        //        entities.SaveChanges();
        //    }
        //}

        public static List<ComplianceView> GetByActId(int actId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceList = (from row in entities.ComplianceViews
                                      where row.ActID == actId
                                      select row).ToList();

                return complianceList;
            }
        }

        public static List<ComplianceView> GetAll(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceList = (from row in entities.ComplianceViews
                                      orderby row.ID
                                      select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceList = complianceList.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return complianceList;
            }
        }
        public static List<ComplianceViewExport> GetAllExcelExport(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceList = (from row in entities.ComplianceViewExports
                                      orderby row.ID
                                      select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceList = complianceList.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return complianceList;
            }
        }
        public static List<ComplianceView> GetAllExcel(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceList = (from row in entities.ComplianceViews
                                      orderby row.ID
                                      select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceList = complianceList.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return complianceList;
            }
        }
        public static void UpdateIFInstanceIsCreated(InternalCompliance Icompliance, List<InternalComplianceForm> form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var oldData = (from row in entities.InternalCompliances
                               where row.ID == Icompliance.ID
                               select new { row.IFrequency, row.IDueDate }).SingleOrDefault();



                InternalCompliance complianceToUpdate = (from row in entities.InternalCompliances
                                                         where row.ID == Icompliance.ID
                                                         select row).FirstOrDefault();

                complianceToUpdate.IComplianceTypeID = Icompliance.IComplianceTypeID;

                complianceToUpdate.IComplianceCategoryID = Icompliance.IComplianceCategoryID;
                complianceToUpdate.IShortDescription = Icompliance.IShortDescription;
                complianceToUpdate.IUploadDocument = Icompliance.IUploadDocument;
                //complianceToUpdate.IComplianceType = Icompliance.IComplianceType;
                complianceToUpdate.IRequiredFrom = Icompliance.IRequiredFrom;
                //complianceToUpdate.IFrequency = Icompliance.IFrequency;
                //complianceToUpdate.IDueDate = Icompliance.IDueDate;
                complianceToUpdate.IRiskType = Icompliance.IRiskType;
                //complianceToUpdate.ISubComplianceType = Icompliance.ISubComplianceType;
                //complianceToUpdate.IReminderType = Icompliance.IReminderType;
                //complianceToUpdate.IReminderBefore = Icompliance.IReminderBefore;
                //complianceToUpdate.IReminderGap = Icompliance.IReminderGap;
                complianceToUpdate.UpdatedOn = DateTime.UtcNow;
                //complianceToUpdate.CreatedOn = DateTime.UtcNow;
                complianceToUpdate.UpdatedBy = Icompliance.UpdatedBy;
                //complianceToUpdate.CreatedBy = Icompliance.CreatedBy;
                complianceToUpdate.IsDeleted = false;
                complianceToUpdate.EffectiveDate = Icompliance.EffectiveDate;
                complianceToUpdate.ActID = Icompliance.ActID;
                complianceToUpdate.IsDocumentRequired = Icompliance.IsDocumentRequired;
                complianceToUpdate.ScheduleType = Icompliance.ScheduleType;
                complianceToUpdate.IDetailedDescription = Icompliance.IDetailedDescription;
                complianceToUpdate.IShortForm = Icompliance.IShortForm;
                entities.SaveChanges();

                if (form.Count > 0)
                {
                    for (int i = 0; i < form.Count; i++)
                    {
                        form[i].IComplianceID = Icompliance.ID;
                        CreateSampleFile(form[i]);
                    }
                }

                //if (form != null)
                //{
                //    form.IComplianceID = Icompliance.ID;
                //    CreateSampleFile(form, true);
                //}
                //else
                //{
                //    InternalComplianceForm Complianceform = GetIComplianceFormByID(Icompliance.ID);
                //    if (Complianceform != null)
                //    {
                //        entities.InternalComplianceForms.Attach(Complianceform);
                //        entities.InternalComplianceForms.Remove(Complianceform);
                //        entities.SaveChanges();
                //    }
                //}

                //if (oldData.IFrequency != Icompliance.IFrequency || oldData.IDueDate != Icompliance.IDueDate)
                //{
                //    GenerateScheduleForInternalCompliance(Icompliance.ID, Icompliance.ISubComplianceType, true);
                //}
            }
        }

        public static void UpdateIFInstanceIsCreated(InternalCompliance Icompliance, InternalComplianceForm form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var oldData = (from row in entities.InternalCompliances
                               where row.ID == Icompliance.ID
                               select new { row.IFrequency, row.IDueDate }).SingleOrDefault();



                InternalCompliance complianceToUpdate = (from row in entities.InternalCompliances
                                                         where row.ID == Icompliance.ID
                                                         select row).FirstOrDefault();

                complianceToUpdate.IComplianceTypeID = Icompliance.IComplianceTypeID;

                complianceToUpdate.IComplianceCategoryID = Icompliance.IComplianceCategoryID;
                complianceToUpdate.IShortDescription = Icompliance.IShortDescription;
                complianceToUpdate.IUploadDocument = Icompliance.IUploadDocument;
                //complianceToUpdate.IComplianceType = Icompliance.IComplianceType;
                complianceToUpdate.IRequiredFrom = Icompliance.IRequiredFrom;
                //complianceToUpdate.IFrequency = Icompliance.IFrequency;
                //complianceToUpdate.IDueDate = Icompliance.IDueDate;
                complianceToUpdate.IRiskType = Icompliance.IRiskType;
                //complianceToUpdate.ISubComplianceType = Icompliance.ISubComplianceType;
                //complianceToUpdate.IReminderType = Icompliance.IReminderType;
                //complianceToUpdate.IReminderBefore = Icompliance.IReminderBefore;
                //complianceToUpdate.IReminderGap = Icompliance.IReminderGap;
                complianceToUpdate.UpdatedOn = DateTime.UtcNow;
                complianceToUpdate.CreatedOn = DateTime.UtcNow;
                complianceToUpdate.UpdatedBy = Icompliance.UpdatedBy;
                complianceToUpdate.CreatedBy = Icompliance.CreatedBy;
                complianceToUpdate.IsDeleted = false;
                complianceToUpdate.EffectiveDate = Icompliance.EffectiveDate;
                complianceToUpdate.ActID = Icompliance.ActID;
                complianceToUpdate.IsDocumentRequired = Icompliance.IsDocumentRequired;
                complianceToUpdate.IShortForm = Icompliance.IShortForm;
                entities.SaveChanges();

                if (form != null)
                {
                    form.IComplianceID = Icompliance.ID;
                    CreateSampleFile(form, true);
                }
                else
                {
                    InternalComplianceForm Complianceform = GetIComplianceFormByID(Icompliance.ID);
                    if (Complianceform != null)
                    {
                        entities.InternalComplianceForms.Attach(Complianceform);
                        entities.InternalComplianceForms.Remove(Complianceform);
                        entities.SaveChanges();
                    }
                }

                //if (oldData.IFrequency != Icompliance.IFrequency || oldData.IDueDate != Icompliance.IDueDate)
                //{
                //    GenerateScheduleForInternalCompliance(Icompliance.ID, Icompliance.ISubComplianceType, true);
                //}
            }
        }
        public static void UpdateIFInstanceIsCreatedCompliance(Compliance compliance,List<ComplianceForm> form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var oldData = (from row in entities.Compliances
                               where row.ID == compliance.ID
                               select new { row.Frequency, row.DueDate }).SingleOrDefault();

                Compliance complianceToUpdate = (from row in entities.Compliances
                                                 where row.ID == compliance.ID
                                                 select row).FirstOrDefault();

                complianceToUpdate.ActID = compliance.ActID;
                complianceToUpdate.ShortDescription = compliance.ShortDescription;
                complianceToUpdate.Description = compliance.Description;
                complianceToUpdate.Sections = compliance.Sections;
                complianceToUpdate.UploadDocument = compliance.UploadDocument;
                //complianceToUpdate.ComplianceType = compliance.ComplianceType;
                complianceToUpdate.NatureOfCompliance = compliance.NatureOfCompliance;
                complianceToUpdate.RequiredForms = compliance.RequiredForms;
                //complianceToUpdate.Frequency = compliance.Frequency;
                //complianceToUpdate.DueDate = compliance.DueDate;
                complianceToUpdate.RiskType = compliance.RiskType;
                complianceToUpdate.NonComplianceType = compliance.NonComplianceType;
                complianceToUpdate.NonComplianceEffects = compliance.NonComplianceEffects;
                complianceToUpdate.VariableAmountPerMonth = compliance.VariableAmountPerMonth;

                complianceToUpdate.FixedMinimum = compliance.FixedMinimum;
                complianceToUpdate.FixedMaximum = compliance.FixedMaximum;
                complianceToUpdate.VariableAmountPerDay = compliance.VariableAmountPerDay;
                complianceToUpdate.VariableAmountPerDayMax = compliance.VariableAmountPerDayMax;
                complianceToUpdate.VariableAmountPercent = compliance.VariableAmountPercent;
                complianceToUpdate.VariableAmountPercentMax = compliance.VariableAmountPercentMax;

                complianceToUpdate.Imprisonment = compliance.Imprisonment;
                complianceToUpdate.Designation = compliance.Designation;
                complianceToUpdate.MinimumYears = compliance.MinimumYears;
                complianceToUpdate.MaximumYears = compliance.MaximumYears;
                complianceToUpdate.Others = compliance.Others;
                complianceToUpdate.EventID = compliance.EventID;
                complianceToUpdate.FixedGap = compliance.FixedGap;
                //complianceToUpdate.SubComplianceType = compliance.SubComplianceType;
                complianceToUpdate.EventComplianceType = compliance.EventComplianceType;
                //complianceToUpdate.ReminderType = compliance.ReminderType;
                //complianceToUpdate.ReminderBefore = compliance.ReminderBefore;
                //complianceToUpdate.ReminderGap = compliance.ReminderGap;
                complianceToUpdate.SubEventID = compliance.SubEventID;
                complianceToUpdate.PenaltyDescription = compliance.PenaltyDescription;
                complianceToUpdate.ReferenceMaterialText = compliance.ReferenceMaterialText;
                complianceToUpdate.UpDocs = compliance.UpDocs;
                complianceToUpdate.ComplinceVisible = compliance.ComplinceVisible;
                complianceToUpdate.SampleFormLink = compliance.SampleFormLink;
                complianceToUpdate.ComplianceSubTypeID = compliance.ComplianceSubTypeID;
                complianceToUpdate.UpdatedOn = compliance.UpdatedOn;
                complianceToUpdate.CheckListTypeID = compliance.CheckListTypeID;
                complianceToUpdate.StartDate = compliance.StartDate;
                complianceToUpdate.onlineoffline = compliance.onlineoffline;
                complianceToUpdate.duedatetype = compliance.duedatetype;
                complianceToUpdate.ScheduleType = compliance.ScheduleType;
                complianceToUpdate.IsMapped = compliance.IsMapped;
                complianceToUpdate.ShortForm = compliance.ShortForm;
                entities.SaveChanges();

                if (form.Count > 0)
                {
                    for (int i = 0; i < form.Count; i++)
                    {
                        form[i].ComplianceID = compliance.ID;
                        CreateSampleFile(form[i]);
                    }
                }

                //if (form != null)
                //{
                //    form.ComplianceID = compliance.ID;
                //    CreateSampleFile(form, true);
                //}
                //else
                //{
                //    ComplianceForm Complianceform = GetComplianceFormByID(compliance.ID);
                //    if (Complianceform != null)
                //    {
                //        if (form != null)
                //        {
                //            entities.ComplianceForms.Attach(Complianceform);
                //            entities.ComplianceForms.Remove(Complianceform);
                //            entities.SaveChanges();
                //        }

                //    }
                //}
                if ((oldData.Frequency != compliance.Frequency || oldData.DueDate != compliance.DueDate))
                {
                    GenerateDefaultScheduleForComplianceID(compliance.ID, compliance.SubComplianceType, true);
                }
            }
        }

        public static void UpdateIFInstanceIsCreatedEventCompliance(Compliance compliance, List<ComplianceForm> form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance complianceToUpdate = (from row in entities.Compliances
                                                 where row.ID == compliance.ID
                                                 select row).FirstOrDefault();

                complianceToUpdate.ActID = compliance.ActID;
                complianceToUpdate.ShortDescription = compliance.ShortDescription;
                complianceToUpdate.Description = compliance.Description;
                complianceToUpdate.Sections = compliance.Sections;
                complianceToUpdate.UploadDocument = compliance.UploadDocument;
                complianceToUpdate.NatureOfCompliance = compliance.NatureOfCompliance;
                complianceToUpdate.RequiredForms = compliance.RequiredForms;
                complianceToUpdate.RiskType = compliance.RiskType;
                complianceToUpdate.NonComplianceType = compliance.NonComplianceType;
                complianceToUpdate.NonComplianceEffects = compliance.NonComplianceEffects;
                complianceToUpdate.VariableAmountPerMonth = compliance.VariableAmountPerMonth;
                complianceToUpdate.FixedMinimum = compliance.FixedMinimum;
                complianceToUpdate.FixedMaximum = compliance.FixedMaximum;
                complianceToUpdate.VariableAmountPerDay = compliance.VariableAmountPerDay;
                complianceToUpdate.VariableAmountPerDayMax = compliance.VariableAmountPerDayMax;
                complianceToUpdate.VariableAmountPercent = compliance.VariableAmountPercent;
                complianceToUpdate.VariableAmountPercentMax = compliance.VariableAmountPercentMax;
                complianceToUpdate.Imprisonment = compliance.Imprisonment;
                complianceToUpdate.Designation = compliance.Designation;
                complianceToUpdate.MinimumYears = compliance.MinimumYears;
                complianceToUpdate.MaximumYears = compliance.MaximumYears;
                complianceToUpdate.Others = compliance.Others;
                complianceToUpdate.EventID = compliance.EventID;
                complianceToUpdate.FixedGap = compliance.FixedGap;
                complianceToUpdate.EventComplianceType = compliance.EventComplianceType;
                complianceToUpdate.SubEventID = compliance.SubEventID;
                complianceToUpdate.PenaltyDescription = compliance.PenaltyDescription;
                complianceToUpdate.ReferenceMaterialText = compliance.ReferenceMaterialText;
                complianceToUpdate.UpDocs = compliance.UpDocs;
                complianceToUpdate.ComplinceVisible = compliance.ComplinceVisible;
                complianceToUpdate.SampleFormLink = compliance.SampleFormLink;
                complianceToUpdate.ComplianceSubTypeID = compliance.ComplianceSubTypeID;
                complianceToUpdate.UpdatedOn = compliance.UpdatedOn;
                complianceToUpdate.CheckListTypeID = compliance.CheckListTypeID;
                complianceToUpdate.StartDate = compliance.StartDate;
                complianceToUpdate.onlineoffline = compliance.onlineoffline;
                complianceToUpdate.duedatetype = compliance.duedatetype;

                complianceToUpdate.IsFrequencyBased = compliance.IsFrequencyBased;
                complianceToUpdate.Frequency = compliance.Frequency;
                complianceToUpdate.DueDate = compliance.DueDate;
                complianceToUpdate.DueWeekDay = compliance.DueWeekDay;
                complianceToUpdate.TriggerNo = compliance.TriggerNo;
                complianceToUpdate.triggerNoOfDays = compliance.triggerNoOfDays;
                complianceToUpdate.IntervalDays = compliance.IntervalDays;
                complianceToUpdate.IsForcefulClosure = compliance.IsForcefulClosure;
                complianceToUpdate.IsMapped = compliance.IsMapped;
                entities.SaveChanges();

                if (form.Count > 0)
                {
                    for (int i = 0; i < form.Count; i++)
                    {
                        form[i].ComplianceID = compliance.ID;
                        CreateSampleFile(form[i]);
                    }
                }
            }
        }

        public static void UpdateEventCompliance(Compliance compliance, List<ComplianceForm> form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Compliance complianceToUpdate = (from row in entities.Compliances
                                                 where row.ID == compliance.ID
                                                 select row).FirstOrDefault();

                complianceToUpdate.ActID = compliance.ActID;
                complianceToUpdate.ShortDescription = compliance.ShortDescription;
                complianceToUpdate.Description = compliance.Description;
                complianceToUpdate.Sections = compliance.Sections;
                complianceToUpdate.UploadDocument = compliance.UploadDocument;
                complianceToUpdate.ComplianceType = compliance.ComplianceType;
                complianceToUpdate.NatureOfCompliance = compliance.NatureOfCompliance;
                complianceToUpdate.RequiredForms = compliance.RequiredForms;
                complianceToUpdate.Frequency = compliance.Frequency;
                complianceToUpdate.DueDate = compliance.DueDate;
                complianceToUpdate.RiskType = compliance.RiskType;
                complianceToUpdate.NonComplianceType = compliance.NonComplianceType;
                complianceToUpdate.NonComplianceEffects = compliance.NonComplianceEffects;
                complianceToUpdate.VariableAmountPerMonth = compliance.VariableAmountPerMonth;

                complianceToUpdate.FixedMinimum = compliance.FixedMinimum;
                complianceToUpdate.FixedMaximum = compliance.FixedMaximum;
                complianceToUpdate.VariableAmountPerDay = compliance.VariableAmountPerDay;
                complianceToUpdate.VariableAmountPerDayMax = compliance.VariableAmountPerDayMax;
                complianceToUpdate.VariableAmountPercent = compliance.VariableAmountPercent;
                complianceToUpdate.VariableAmountPercentMax = compliance.VariableAmountPercentMax;

                complianceToUpdate.Imprisonment = compliance.Imprisonment;
                complianceToUpdate.Designation = compliance.Designation;
                complianceToUpdate.MinimumYears = compliance.MinimumYears;
                complianceToUpdate.MaximumYears = compliance.MaximumYears;
                complianceToUpdate.Others = compliance.Others;
                complianceToUpdate.EventID = compliance.EventID;
                complianceToUpdate.FixedGap = compliance.FixedGap;
                complianceToUpdate.SubComplianceType = compliance.SubComplianceType;
                complianceToUpdate.EventComplianceType = compliance.EventComplianceType;
                complianceToUpdate.ReminderType = compliance.ReminderType;
                complianceToUpdate.ReminderBefore = compliance.ReminderBefore;
                complianceToUpdate.ReminderGap = compliance.ReminderGap;
                complianceToUpdate.SubEventID = compliance.SubEventID;
                complianceToUpdate.PenaltyDescription = compliance.PenaltyDescription;
                complianceToUpdate.ReferenceMaterialText = compliance.ReferenceMaterialText;
                complianceToUpdate.UpDocs = compliance.UpDocs;
                complianceToUpdate.ComplinceVisible = compliance.ComplinceVisible;
                complianceToUpdate.SampleFormLink = compliance.SampleFormLink;
                complianceToUpdate.ComplianceSubTypeID = compliance.ComplianceSubTypeID;
                complianceToUpdate.UpdatedOn = compliance.UpdatedOn;
                complianceToUpdate.CheckListTypeID = compliance.CheckListTypeID;
                complianceToUpdate.DueWeekDay = compliance.DueWeekDay;
                complianceToUpdate.OneTimeDate = compliance.OneTimeDate;
                complianceToUpdate.StartDate = compliance.StartDate;
                complianceToUpdate.onlineoffline = compliance.onlineoffline;
                complianceToUpdate.duedatetype = compliance.duedatetype;

                complianceToUpdate.IsFrequencyBased = compliance.IsFrequencyBased;
                complianceToUpdate.TriggerNo = compliance.TriggerNo;
                complianceToUpdate.triggerNoOfDays = compliance.triggerNoOfDays;
                complianceToUpdate.IntervalDays = compliance.IntervalDays;
                complianceToUpdate.IsForcefulClosure = compliance.IsForcefulClosure;
                complianceToUpdate.IsMapped = compliance.IsMapped;

                entities.SaveChanges();

                if (form.Count > 0)
                {
                    for (int i = 0; i < form.Count; i++)
                    {
                        form[i].ComplianceID = compliance.ID;
                        CreateSampleFile(form[i]);
                    }
                }
            }
        }

        public static void Update(Compliance compliance, List<ComplianceForm> form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var oldData = (from row in entities.Compliances
                               where row.ID == compliance.ID
                               select new { row.Frequency, row.DueDate }).SingleOrDefault();

                Compliance complianceToUpdate = (from row in entities.Compliances
                                                 where row.ID == compliance.ID
                                                 select row).FirstOrDefault();

                complianceToUpdate.ActID = compliance.ActID;
                complianceToUpdate.ShortDescription = compliance.ShortDescription;
                complianceToUpdate.Description = compliance.Description;
                complianceToUpdate.Sections = compliance.Sections;
                complianceToUpdate.UploadDocument = compliance.UploadDocument;
                complianceToUpdate.ComplianceType = compliance.ComplianceType;
                complianceToUpdate.NatureOfCompliance = compliance.NatureOfCompliance;
                complianceToUpdate.RequiredForms = compliance.RequiredForms;
                complianceToUpdate.Frequency = compliance.Frequency;
                complianceToUpdate.DueDate = compliance.DueDate;
                complianceToUpdate.RiskType = compliance.RiskType;
                complianceToUpdate.NonComplianceType = compliance.NonComplianceType;
                complianceToUpdate.NonComplianceEffects = compliance.NonComplianceEffects;
                complianceToUpdate.VariableAmountPerMonth = compliance.VariableAmountPerMonth;

                complianceToUpdate.FixedMinimum = compliance.FixedMinimum;
                complianceToUpdate.FixedMaximum = compliance.FixedMaximum;
                complianceToUpdate.VariableAmountPerDay = compliance.VariableAmountPerDay;
                complianceToUpdate.VariableAmountPerDayMax = compliance.VariableAmountPerDayMax;
                complianceToUpdate.VariableAmountPercent = compliance.VariableAmountPercent;
                complianceToUpdate.VariableAmountPercentMax = compliance.VariableAmountPercentMax;

                complianceToUpdate.Imprisonment = compliance.Imprisonment;
                complianceToUpdate.Designation = compliance.Designation;
                complianceToUpdate.MinimumYears = compliance.MinimumYears;
                complianceToUpdate.MaximumYears = compliance.MaximumYears;
                complianceToUpdate.Others = compliance.Others;
                complianceToUpdate.EventID = compliance.EventID;
                complianceToUpdate.FixedGap = compliance.FixedGap;
                complianceToUpdate.SubComplianceType = compliance.SubComplianceType;
                complianceToUpdate.EventComplianceType = compliance.EventComplianceType;
                complianceToUpdate.ReminderType = compliance.ReminderType;
                complianceToUpdate.ReminderBefore = compliance.ReminderBefore;
                complianceToUpdate.ReminderGap = compliance.ReminderGap;
                complianceToUpdate.SubEventID = compliance.SubEventID;
                complianceToUpdate.PenaltyDescription = compliance.PenaltyDescription;
                complianceToUpdate.ReferenceMaterialText = compliance.ReferenceMaterialText;
                complianceToUpdate.UpDocs = compliance.UpDocs;
                complianceToUpdate.ComplinceVisible = compliance.ComplinceVisible;
                complianceToUpdate.SampleFormLink = compliance.SampleFormLink;
                complianceToUpdate.ComplianceSubTypeID = compliance.ComplianceSubTypeID;
                complianceToUpdate.UpdatedOn = compliance.UpdatedOn;
                complianceToUpdate.CheckListTypeID = compliance.CheckListTypeID;
                complianceToUpdate.DueWeekDay = compliance.DueWeekDay;
                complianceToUpdate.OneTimeDate = compliance.OneTimeDate;
                complianceToUpdate.StartDate = compliance.StartDate;
                complianceToUpdate.onlineoffline = compliance.onlineoffline;
                complianceToUpdate.duedatetype = compliance.duedatetype;
                complianceToUpdate.IsMapped = compliance.IsMapped;
                complianceToUpdate.ShortForm = compliance.ShortForm;

                entities.SaveChanges();

                if (form.Count > 0)
                {
                    for (int i = 0; i < form.Count; i++)
                    {
                        form[i].ComplianceID = compliance.ID;
                        CreateSampleFile(form[i]);
                    }
                }

                //if (form != null)
                //{
                //    form.ComplianceID = compliance.ID;
                //    CreateSampleFile(form, true);
                //}
                //else
                //{
                //    ComplianceForm Complianceform = GetComplianceFormByID(compliance.ID);
                //    if (Complianceform != null)
                //    {
                //        if (form != null)
                //        {
                //            entities.ComplianceForms.Attach(Complianceform);
                //            entities.ComplianceForms.Remove(Complianceform);
                //            entities.SaveChanges();
                //        }
                //    }
                //}
                if (oldData.Frequency != compliance.Frequency || oldData.DueDate != compliance.DueDate)
                {
                    GenerateDefaultScheduleForComplianceID(compliance.ID, compliance.SubComplianceType, true);
                }
            }
        }

        //public static void Create(Compliance compliance, List<ComplianceParameter> parameters)
        public static void Create(Compliance compliance, ComplianceForm form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                compliance.CreatedOn = DateTime.Now;
                compliance.UpdatedOn = DateTime.Now;
                compliance.IsDeleted = false;
                bool chkexists = ExistsR(compliance.Description.Trim(), compliance.ShortDescription.Trim(), compliance.ActID);
                if (chkexists == false)
                {
                    entities.Compliances.Add(compliance);
                    entities.SaveChanges();
                    if (form != null)
                    {
                        form.ComplianceID = compliance.ID;
                        CreateSampleFile(form);
                    }

                    if (compliance.Frequency.HasValue)
                    {
                        if (compliance.Frequency != 7 && compliance.Frequency != 8)
                        {
                            GenerateDefaultScheduleForComplianceID(compliance.ID, compliance.SubComplianceType);
                        }                        
                    }
                }

            }
        }


        #region Compliance Parameters
        public static List<ComplianceParameter> GetAllComplianceParameters()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var parameters = (from row in entities.ComplianceParameters
                                  where row.IsDeleted == false
                                  select row);

                return parameters.ToList();
            }
        }
        #endregion

        #region Sample File

        public static void CreateSampleFile(ComplianceForm form, bool deleteOldFiles = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                
                form.CreatedOn = DateTime.Now;
                form.UpdatedOn = DateTime.Now;
                entities.ComplianceForms.Add(form);
                entities.SaveChanges();
            }

        }

      
        public static ComplianceForm GetComplianceFormByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.ComplianceForms
                            where row.ComplianceID == complianceID
                            select row).FirstOrDefault();

                return form;
            }
        }
        #endregion


        public static Event GetEventType(long EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.Events
                            where row.ID == EventID
                            select row).FirstOrDefault();

                return form;
            }
        }

        


        public static List<SP_GetAuditLogTransaction_Result> GetAllTransactionLog(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.SP_GetAuditLogTransaction(ScheduledOnID)
                                  where row.ComplianceScheduleOnID == ScheduledOnID
                                  orderby row.Dated descending
                                  select row).ToList();

                return statusList;
            }
        }

        #region Compliance Transactions

        public static List<ComplianceTransactionView> GetAllTransactions(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceTransactionViews
                                  where row.ComplianceScheduleOnID == ScheduledOnID
                                  orderby row.ComplianceTransactionID descending
                                  select row).ToList();

                return statusList;
            }
        }

        public static ComplianceTransactionView GetClosedTransaction(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceTransactionViews
                                  where row.ComplianceScheduleOnID == ScheduledOnID
                                  && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 || row.ComplianceStatusID == 7 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 9)
                                  orderby row.ComplianceTransactionID descending
                                  select row).ToList();



                return statusList.FirstOrDefault();
            }
        }
      
    

      
        public static List<ComplianceInstanceTransactionView> GetTransactionsByUserID(long userID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row);

                DateTime nextMonthDate = DateTime.UtcNow.AddMonths(1);

                if (userID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.UserID == userID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.Branch.Contains(filter) || entry.Description.Contains(filter) || entry.Status.Contains(filter) || entry.Role.Contains(filter) || entry.User.Contains(filter));
                }

                return transactionsQuery.Where(entry => entry.ScheduledOn <= nextMonthDate).OrderByDescending(entry => entry.ScheduledOn).ToList();
            }
        }

        public static RecentComplianceTransactionView GetCurrentStatusByComplianceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusID = (from row in entities.RecentComplianceTransactionViews
                                       where row.ComplianceScheduleOnID == ScheduledOnID
                                       select row).FirstOrDefault();

                return currentStatusID;
            }
        }
        public static ComplianceTransaction GetCurrentLatestStatusByComplianceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusID = (from row in entities.ComplianceTransactions
                                       where row.ComplianceScheduleOnID == ScheduledOnID
                                       && row.ValuesAsPerReturn != null
                                       orderby row.ID descending
                                       select row).FirstOrDefault();

             

                return currentStatusID;
            }
        }
        public static FileData GetFile(int fileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var file = (from row in entities.FileDatas
                            where row.ID == fileID
                            select row).FirstOrDefault();

                return file;
            }
        }

        #endregion
      
        public static List<ComplianceScheduleClientFrequency> GetScheduleByClientFrequencyComplianceID(long complianceID,long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceScheduleClientFrequencies
                                    where row.ComplianceID == complianceID
                                    && row.CustomerID== CustomerID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
        public static List<ComplianceSchedule> GetScheduleByComplianceID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceSchedules
                                    where row.ComplianceID == complianceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
      
        public static void GenerateDefaultScheduleForComplianceID(long complianceID, byte? subcomplincetype, bool deleteOldData = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!(subcomplincetype == 0))
                {
                    if (deleteOldData)
                    {
                        var ids = (from row in entities.ComplianceSchedules
                                   where row.ComplianceID == complianceID
                                   select row.ID).ToList();
                        ids.ForEach(entry =>
                        {
                            ComplianceSchedule schedule = (from row in entities.ComplianceSchedules
                                                           where row.ID == entry
                                                           select row).FirstOrDefault();

                            entities.ComplianceSchedules.Remove(schedule);
                        });

                        entities.SaveChanges();
                    }
                    var compliance = (from row in entities.Compliances
                                      where row.ID == complianceID
                                      select row).First();

                    int step = 1;

                    switch ((Frequency) compliance.Frequency.Value)
                    {
                        case Frequency.Quarterly:
                            step = 3;
                            break;
                        case Frequency.FourMonthly:
                            step = 4;
                            break;
                        case Frequency.HalfYearly:
                            step = 6;
                            break;
                        case Frequency.Annual:
                            step = 12;
                            break;
                        case Frequency.TwoYearly:
                            step = 12;
                            break;
                        case Frequency.SevenYearly:
                            step = 12;
                            break;
                    }


                    int SpecialMonth;
                    for (int month = 1; month <= 12; month += step)
                    {
                        ComplianceSchedule complianceShedule = new ComplianceSchedule();
                        complianceShedule.ComplianceID = complianceID;
                        complianceShedule.ForMonth = month;

                        if (subcomplincetype == 1 || subcomplincetype == 2)
                        {
                            int monthCopy = month;
                            if (monthCopy == 1)
                            {
                                monthCopy = 12;
                                complianceShedule.ForMonth = monthCopy;
                            }
                            else
                            {
                                monthCopy = month - 1;
                                complianceShedule.ForMonth = monthCopy;
                            }

                            if (compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6)
                            {
                                monthCopy = 3;
                                complianceShedule.ForMonth = monthCopy;
                            }

                            int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                            complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                        }
                        else
                        {
                            SpecialMonth = month;
                            if (compliance.Frequency == 0 && SpecialMonth == 12)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliance.Frequency == 1 && SpecialMonth == 10)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliance.Frequency == 2 && SpecialMonth == 7)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliance.Frequency == 4 && SpecialMonth == 9)
                            {
                                SpecialMonth = 1;
                            }
                            else if ((compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6) && SpecialMonth == 1)
                            {
                                SpecialMonth = 1;
                            }
                            else
                            {
                                SpecialMonth = SpecialMonth + step;
                            }

                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                            if (Convert.ToInt32(compliance.DueDate.Value.ToString("D2")) > lastdate)
                            {
                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                            }
                            else
                            {
                                complianceShedule.SpecialDate = compliance.DueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                            }
                        }
                        entities.ComplianceSchedules.Add(complianceShedule);

                    }

                    entities.SaveChanges();
                }
            }
        }
        public static void GenerateDefaultScheduleForComplianceIDNEW(long complianceID, byte? subcomplincetype, bool deleteOldData = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string calflag = "N";
                int step = 1;
                Compliance compliance = new Compliance();
                if (!(subcomplincetype == 0))
                {
                    if (deleteOldData)
                    {
                        var ids = (from row in entities.ComplianceSchedules
                                   where row.ComplianceID == complianceID
                                   select row.ID).ToList();


                        compliance = (from row in entities.Compliances
                                      where row.ID == complianceID
                                      select row).First();

                        var Formonths = (from row in entities.ComplianceSchedules
                                         where row.ComplianceID == complianceID
                                         select row.ForMonth).ToList();

                        switch ((Frequency) compliance.Frequency.Value)
                        {
                            case Frequency.Quarterly:
                                calflag = "N";
                                step = 3;
                                break;
                            case Frequency.FourMonthly:
                                if (Formonths.Contains(12) || Formonths.Contains(8) || Formonths.Contains(4))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 4;
                                break;
                            case Frequency.HalfYearly:
                                if (Formonths.Contains(4) || Formonths.Contains(10))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 6;
                                break;
                            case Frequency.Annual:
                                if (Formonths.Contains(4))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 12;
                                break;
                            case Frequency.TwoYearly:
                                if (Formonths.Contains(4))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 12;
                                break;
                            case Frequency.SevenYearly:
                                if (Formonths.Contains(4))
                                {
                                    calflag = "Y";
                                }
                                else
                                {
                                    calflag = "N";
                                }
                                step = 12;
                                break;
                        }
                        ids.ForEach(entry =>
                        {
                            ComplianceSchedule schedule = (from row in entities.ComplianceSchedules
                                                           where row.ID == entry
                                                           select row).FirstOrDefault();

                            entities.ComplianceSchedules.Remove(schedule);
                        });

                        entities.SaveChanges();
                    }
                    int SpecialMonth;
                    #region Financial Year And Calender
                    if (calflag == "Y")
                    {
                        for (int month = 4; month <= 12; month += step)
                        {
                            ComplianceSchedule complianceShedule = new ComplianceSchedule();
                            complianceShedule.ComplianceID = complianceID;
                            complianceShedule.ForMonth = month;
                            if (subcomplincetype == 1 || subcomplincetype == 2)
                            {
                                int monthCopy = month;
                                if (monthCopy == 1)
                                {
                                    monthCopy = 12;
                                    complianceShedule.ForMonth = monthCopy;
                                }
                                else
                                {
                                    monthCopy = month - 1;
                                    complianceShedule.ForMonth = monthCopy;
                                }

                                if (compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6)
                                {
                                    monthCopy = 3;
                                    complianceShedule.ForMonth = monthCopy;
                                }

                                int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                            }
                            else
                            {
                                SpecialMonth = month;
                                if (compliance.Frequency == 0 && SpecialMonth == 12)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.Frequency == 1 && SpecialMonth == 10)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.Frequency == 2 && SpecialMonth == 10)
                                {
                                    SpecialMonth = 4;
                                }
                                else if (compliance.Frequency == 2 && SpecialMonth == 4)
                                {
                                    SpecialMonth = 10;
                                }
                                else if (compliance.Frequency == 4 && SpecialMonth == 4)
                                {
                                    SpecialMonth = 8;
                                }
                                else if (compliance.Frequency == 4 && SpecialMonth == 8)
                                {
                                    SpecialMonth = 12;
                                }
                                else if (compliance.Frequency == 4 && SpecialMonth == 12)
                                {
                                    SpecialMonth = 4;
                                }
                                else if (compliance.Frequency == 3 && SpecialMonth == 1)
                                {
                                    SpecialMonth = 4;
                                }
                                else if (compliance.Frequency == 3 && SpecialMonth == 4)
                                {
                                    SpecialMonth = 4;
                                }
                                else if ((compliance.Frequency == 5 || compliance.Frequency == 6) && SpecialMonth == 1)
                                {
                                    SpecialMonth = 1;
                                }
                                else
                                {
                                    SpecialMonth = SpecialMonth + step;
                                }

                                int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, month);
                                if (Convert.ToInt32(compliance.DueDate.Value.ToString("D2")) > lastdate)
                                {
                                    complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                }
                                else
                                {
                                    complianceShedule.SpecialDate = compliance.DueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                                }

                            }
                            entities.ComplianceSchedules.Add(complianceShedule);
                        }//for loop end
                    }//Financial Year close
                    else
                    {
                        for (int month = 1; month <= 12; month += step)
                        {
                            ComplianceSchedule complianceShedule = new ComplianceSchedule();
                            complianceShedule.ComplianceID = complianceID;
                            complianceShedule.ForMonth = month;

                            if (subcomplincetype == 1 || subcomplincetype == 2)
                            {
                                int monthCopy = month;
                                if (monthCopy == 1)
                                {
                                    monthCopy = 12;
                                    complianceShedule.ForMonth = monthCopy;
                                }
                                else
                                {
                                    monthCopy = month - 1;
                                    complianceShedule.ForMonth = monthCopy;
                                }
                                if (compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6)
                                {
                                    monthCopy = 3;
                                    complianceShedule.ForMonth = monthCopy;
                                }
                                int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                                complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");
                            }
                            else
                            {
                                SpecialMonth = month;
                                if (compliance.Frequency == 0 && SpecialMonth == 12)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.Frequency == 1 && SpecialMonth == 10)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.Frequency == 2 && SpecialMonth == 7)
                                {
                                    SpecialMonth = 1;
                                }
                                else if (compliance.Frequency == 4 && SpecialMonth == 9)
                                {
                                    SpecialMonth = 1;
                                }
                                else if ((compliance.Frequency == 3 || compliance.Frequency == 5 || compliance.Frequency == 6) && SpecialMonth == 1)
                                {
                                    SpecialMonth = 1;
                                }
                                else
                                {
                                    SpecialMonth = SpecialMonth + step;
                                }

                                int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                                if (Convert.ToInt32(compliance.DueDate.Value.ToString("D2")) > lastdate)
                                {
                                    complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                                }
                                else
                                {
                                    complianceShedule.SpecialDate = compliance.DueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                                }
                            }
                            entities.ComplianceSchedules.Add(complianceShedule);
                        }
                    }//Calender Year Close
                    #endregion
                    entities.SaveChanges();
                }
            }
        }
        public static void UpdateScheduleInformation(List<ComplianceSchedule> scheduleList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                scheduleList.ForEach(schedule =>
                {
                    //ComplianceSchedule complianceScheduleToUpdate = new ComplianceSchedule() { ID = schedule.ID };
                    //entities.ComplianceSchedules.Attach(complianceScheduleToUpdate);
                    ComplianceSchedule complianceScheduleToUpdate = (from row in entities.ComplianceSchedules
                                                                     where row.ID == schedule.ID
                                                                     select row).FirstOrDefault();

                    complianceScheduleToUpdate.ComplianceID = schedule.ComplianceID;
                    complianceScheduleToUpdate.ForMonth = schedule.ForMonth;
                    complianceScheduleToUpdate.SpecialDate = schedule.SpecialDate;
                });


                entities.SaveChanges();
            }
        }
        public static void UpdateCustomerwiseScheduleInformation(List<CustomerWiseComplianceSchedule> scheduleList,int custid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                scheduleList.ForEach(schedule =>
                {
                    //ComplianceSchedule complianceScheduleToUpdate = new ComplianceSchedule() { ID = schedule.ID };
                    //entities.ComplianceSchedules.Attach(complianceScheduleToUpdate);
                    CustomerWiseComplianceSchedule complianceScheduleToUpdate = (from row in entities.CustomerWiseComplianceSchedules
                                                                     where row.ID == schedule.ID
                                                                     && row.Customerid== custid
                                                                                 select row).FirstOrDefault();

                    complianceScheduleToUpdate.ComplianceID = Convert.ToInt32(schedule.ComplianceID);
                    complianceScheduleToUpdate.ForMonth = schedule.ForMonth;
                    complianceScheduleToUpdate.SpecialDate = schedule.SpecialDate;
                });


                entities.SaveChanges();
            }
        }
        //added by sudarshan for escalation notification
        public static void AddEscalationDetail(EscalationNotification objEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EscalationNotifications.Add(objEscalation);
                entities.SaveChanges();
            }

        }

        //added by sudarshan for sumary email escalation notification
        public static void AddEscalationDetailTable(List<ComplianceEscalationTable> objEscalation)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.ComplianceEscalationTables.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        //added by Rahul for save Error Datain Database
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        //added by sudarshan for sumary email escalation notification
        public static void AddComplianceReminderNotification(List<ComplianceReminderNotification> objEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.ComplianceReminderNotifications.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static void AddCheckListReminderNotification(List<CheckListReminderNotification> objEscalation)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objEscalation.ForEach(entry =>
                    {
                        entities.CheckListReminderNotifications.Add(entry);
                        entities.SaveChanges();

                    });
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        public static void AddEscalationDetail(List<EscalationNotification> objEscalation)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.EscalationNotifications.Add(entry);
                    entities.SaveChanges();

                });
            }

        }

        //added by sudarshan for escalation notification
        public static bool CheckEscalationNotification(long complianceinsID, long prentId, DateTime scheduledOn, DateTime currentDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.EscalationNotifications
                            where row.ComplianceInstanceID == complianceinsID && row.ParentID == prentId
                            && row.ScheduledOn == scheduledOn && row.SentOn == currentDate
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckEscalationNotification1(long userID, DateTime currentDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceEscalationTables
                            where row.UserID == userID && row.SentOn == currentDate
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool CheckComplianceReminderNotification(long userID, DateTime currentDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceReminderNotifications
                            where row.UserID == userID && row.SentOn == currentDate
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool ExistsInternalR(string ShortDescription)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.InternalCompliances
                             where row.IShortDescription.Equals(ShortDescription) && row.IsDeleted == false
                             select row.ID).FirstOrDefault();

                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                //select row);
                //return query.Select(entry => true).SingleOrDefault();

            }
        }
        public static bool ExistsSavedCompliance(string DetailDescription, string ShortDescription, int ActID, int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Compliances
                             where row.Description.Equals(DetailDescription)
                             && row.ShortDescription.Equals(ShortDescription)
                             && row.IsDeleted == false
                             && row.ActID == ActID
                             && row.ID != ComplianceID
                             select row.ID).FirstOrDefault();

                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public static bool ExistsR(string DetailDescription, string ShortDescription, int ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Compliances
                             where row.Description.Equals(DetailDescription)
                             && row.ShortDescription.Equals(ShortDescription)
                             && row.IsDeleted == false
                             && row.ActID == ActID
                             select row.ID).FirstOrDefault();

                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool Exists(string DetailDescription, string ShortDescription, int ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Compliances
                             where row.Description.Equals(DetailDescription)
                             && row.ShortDescription.Equals(ShortDescription)
                             && row.IsDeleted == false
                             && row.ActID == ActID
                             select row.ID).FirstOrDefault();

                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        // added by sudarshan for saving list of Act object
        public static void Create(List<Compliance> complianceData)
        {
            string Delatildescription = "";
            string shortdescription = "";
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var extraitems = db.table.Where(t => !list.Select(l => l.EntityID).Contains(t.EntityID));
                    complianceData.ForEach(entry =>
                    {
                        bool chkexists = ExistsR(entry.Description, entry.ShortDescription,entry.ActID);
                        Delatildescription = entry.Description;
                        shortdescription = entry.ShortDescription;
                        if (chkexists == false)
                        {
                            entities.Compliances.Add(entry);
                        }
                        //entities.Compliances.Add(entry);
                    });
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "Create" + "Delatildescription Error" + Delatildescription + "shortdescription" + shortdescription;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte) com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);
                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "Create" + "Delatildescription Error" + Delatildescription + "shortdescription" + shortdescription);

                SendgridSenEmail("Error Occured as CreateTransaction Function", "Create" + "Delatildescription Error" + Delatildescription + "shortdescription" + shortdescription);
            }
        }

        public static bool Exists(string section)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Compliances
                             where row.Sections.Contains(section)
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        
     
   


        public static string GetMonthIndexFromAbbreviatedMonth(string monthValue)
        {
            string rlcsMonthValue = string.Empty;
            int monthIndex = 0;
            string[] MonthNames = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames;
            monthIndex = Array.IndexOf(MonthNames, monthValue) + 1;

            if (monthIndex != 0)
            {
                if (monthIndex < 10)
                    rlcsMonthValue = "0" + monthIndex;
                else
                    rlcsMonthValue = monthIndex.ToString();
            }

            return rlcsMonthValue;
        }

        public static string GetMonthIndexFromFullMonth(string monthValue)
        {
            string rlcsMonthValue = string.Empty;
            int monthIndex = 0;
            string[] MonthNames = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            monthIndex = Array.IndexOf(MonthNames, monthValue) + 1;

            if (monthIndex != 0)
            {
                if (monthIndex < 10)
                    rlcsMonthValue = "0" + monthIndex;
                else
                    rlcsMonthValue = monthIndex.ToString();
            }

            return rlcsMonthValue;
        }

        public static string GetLast(string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return string.Empty;
            return source.Substring(source.Length - tail_length);
        }        
        
        


        public static long GetInstanceIdOfTransaction(long ScheduleOnId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long complianceInstanceID = (from row in entities.ComplianceTransactions
                                             where row.ComplianceScheduleOnID == ScheduleOnId
                                             select row.ComplianceInstanceId).SingleOrDefault();

                return complianceInstanceID;
            }

        }
        
        public static List<NameValue> GetUserRoles(int userID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<NameValue> AssignedRoles = new List<NameValue>();
                List<NameValue> EventAssignedRoles = new List<NameValue>();
                List<NameValue> roles = new List<NameValue>();
                if (userID != -1)
                {
                    AssignedRoles = (from row in entities.ComplianceAssignments
                                     join ro in entities.Roles
                                     on row.RoleID equals ro.ID
                                     where row.UserID == userID
                                     select new NameValue()
                                     {
                                         ID = row.RoleID,
                                         Name = ro.Name
                                     }).GroupBy(entry => entry.ID).Select(entry => entry.FirstOrDefault()).ToList();

                    EventAssignedRoles = (from row in entities.EventAssignments
                                          join ro in entities.Roles
                                          on row.Role equals ro.ID
                                          where row.UserID == userID
                                          select new NameValue()
                                          {
                                              ID = row.Role,
                                              Name = ro.Name
                                          }).GroupBy(entry => entry.ID).Select(entry => entry.FirstOrDefault()).ToList();
                }
                else
                {
                    AssignedRoles = (from row in entities.ComplianceAssignments
                                     join ro in entities.Roles
                                     on row.RoleID equals ro.ID
                                     select new NameValue()
                                     {
                                         ID = row.RoleID,
                                         Name = ro.Name
                                     }).ToList();

                    EventAssignedRoles = (from row in entities.EventAssignments
                                          join ro in entities.Roles
                                          on row.Role equals ro.ID
                                          select new NameValue()
                                          {
                                              ID = row.Role,
                                              Name = ro.Name
                                          }).ToList();
                }

                roles = AssignedRoles.Union(EventAssignedRoles).ToList();

                return roles;
            }
        }
      
        public static List<ComplianceAssignment> GetAssignedRoles(int userID = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceAssignment> AssignedRoles = new List<ComplianceAssignment>();
                if (userID != -1)
                {
                    AssignedRoles = (from row in entities.ComplianceAssignments
                                     where row.UserID == userID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();
                }
                else
                {
                    AssignedRoles = (from row in entities.ComplianceAssignments
                                     select row).ToList();
                }

                return AssignedRoles;
            }
        }

        public static List<ComplianceAssignment> GetAssignedUsers(int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.ComplianceAssignments
                                     where row.ComplianceInstanceID == ComplianceInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }

        /* for Approve Compliances page*/
        public static List<RecentApproverTransactionView> GetApproverAssignedCompliances(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime curruntDate = DateTime.Now.AddDays(-1);

                var transactionList = (from row in entities.RecentApproverTransactionViews
                                       where row.UserID == userID  //&& row.ScheduledOn <= curruntDate
                                       select row).ToList();

                return transactionList;
            }
        }

        public static List<RecentApproverInternalTransactionView> GetApproverAssignedInternalCompliances(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime curruntDate = DateTime.Now.AddDays(-1);

                var transactionList = (from row in entities.RecentApproverInternalTransactionViews
                                       where row.UserID == userID  //&& row.ScheduledOn <= curruntDate
                                       select row).ToList();

                return transactionList;
            }
        }

        public static void AddApproverNotification(List<ApproverNotification> objApproverNotification)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objApproverNotification.ForEach(entry =>
                {
                    entities.ApproverNotifications.Add(entry);
                    entities.SaveChanges();

                });
            }

        }

        //added by sudarshan for escalation notification
        public static bool CheckApproverNotification(long userId, DateTime sentOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                return (from row in entities.ApproverNotifications
                        where row.UserId == userId && row.SentOn == sentOn
                        select true).FirstOrDefault();
            }
        }

        public static List<GetComplianceDocumentsView> GetDocumnets(long ScheduledOnID, long transactionID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DocumentList = (from row in entities.GetComplianceDocumentsViews
                                    where row.ScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return DocumentList;
            }
        }
    
        /* For Revise Compliance Tab */

        public static List<ComplianceInstanceTransactionView> GetReviseCompliances(int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ReviceCompliances = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.UserID == UserID && row.RoleID == 3 &&
                                         (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5 
                                         || row.ComplianceStatusID==2 || row.ComplianceStatusID == 3)
                                         select row).ToList();

                return ReviceCompliances;
            }
        }

        

       
        public static List<ComplianceInstance> GetEventBasedCompliancesInstances(long ComplianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
              
                List<ComplianceInstance> complianceInstances = (from row in entities.ComplianceInstances
                                                                where row.ComplianceId == ComplianceId
                                                                && row.IsDeleted == false
                                                                select row).ToList();

                return complianceInstances;
            }
        }

        public static List<ComplianceInstance> GetEventBasedCompliancesInstances(List<long> complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<ComplianceInstance> complianceInstances = (from row in entities.ComplianceInstances
                                                                where complianceID.Contains(row.ComplianceId)
                                                                && row.IsDeleted == false
                                                                select row).ToList();


                return complianceInstances;
            }
        }

        public static List<NameValue> GetSubEventBasedCompliances(long eventId, int type = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<NameValue> SubEventList = new List<NameValue>();

                List<Compliance> complianceList1 = (from row in entities.Compliances
                                                    where row.EventID == eventId && row.SubEventID != null
                                                    select row).ToList();

                foreach (var row in complianceList1)
                {
                    NameValue test = EventManagement.GetTopParent(Convert.ToInt64(row.SubEventID));
                    if (SubEventList.Where(entry => entry.ID == test.ID).FirstOrDefault() == null)
                        SubEventList.Add(test);
                }

                return SubEventList;
            }
        }

        public static List<Compliance> GetEventBasedCompliances(long eventId, int type = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Compliance> complianceList = (from row in entities.Compliances
                                                   where row.EventID == eventId
                                                   select row).ToList();

              
                return complianceList;
            }
        }

        public static ComplianceInstanceTransactionView GetInstanceTransactionCompliance(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ReviceCompliances = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.ScheduledOnID == ScheduledOnID && row.RoleID == 3
                                              && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).FirstOrDefault();

                return ReviceCompliances;
            }
        }

        public static List<ComplianceView> GetMappedComplianceCheckList(long UserID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var ActID = (from row in entities.ComplianceAssignedInstancesViews
                             where row.UserID == UserID

                             group row by row.ActID into g
                             select g.Key).ToList();

                var complianceList = (from row in entities.ComplianceViews
                                      where ActID.Contains(row.ActID) && row.ComplianceType == 1
                                      select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                {
                    complianceList = complianceList.Where(entry => entry.ActName.ToUpper().Contains(filter.ToUpper()) || entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper())).ToList();
                }

                return complianceList;
            }
        }
        public static List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> GetMappedComplianceCheckListRahul1(long UserID,
        List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> MaterCheckListTransactionsQuery, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_ComplianceInstanceTransactionCount_Dashboard_Result>();
                if (IsPerformer == true)
                {
                    int performerRoleID = (from row in entities.Roles
                                           where row.Code == "PERF"
                                           select row.ID).Single();

                    DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);
                    //DateTime now = DateTime.UtcNow.Date;
                    transactionsQuery = (from row in MaterCheckListTransactionsQuery //entities.CheckListInstanceTransactionViews
                                         where row.UserID == UserID && row.RoleID == performerRoleID
                                         && (row.ScheduledOn == nextOneMonth || row.ScheduledOn <= nextOneMonth)
                                         && row.ComplianceStatusID == 1                                          
                                         select row).ToList();
                  
                }
                return transactionsQuery;
            }
        }
        public static List<SP_GetCheckListCannedReportCompliancesSummary_Result> GetMappedComplianceCheckListRahul(long UserID,
         List<SP_GetCheckListCannedReportCompliancesSummary_Result> MaterCheckListTransactionsQuery, bool IsPerformer, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCheckListCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCheckListCannedReportCompliancesSummary_Result>();
                if (IsPerformer == true)
                {                    
                    DateTime now = DateTime.UtcNow.Date;
                    transactionsQuery = (from row in MaterCheckListTransactionsQuery 
                                         where (row.ScheduledOn == now || row.ScheduledOn <= now)
                                         && row.ComplianceStatusID == 1
                                          && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList();

                    if (!string.IsNullOrEmpty(filter))
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }
                return transactionsQuery;
            }
        }

        
        public static List<long> GetPerformerListUsingReviewer(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var InstanceIDs = (from row in entities.ComplianceAssignments
                                   where row.UserID == UserID && row.RoleID == 4
                                   select row.ComplianceInstanceID).ToList();

                var performerList = (from row in entities.ComplianceAssignments
                                     where InstanceIDs.Contains(row.ComplianceInstanceID) && row.RoleID == 3
                                     select row.UserID).ToList();

                return performerList;
            }
        }

        public static List<long> GetInstanceIDfromReviewer(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var InstanceIDs = (from row in entities.ComplianceAssignments
                                   where row.UserID == UserID && row.RoleID == 4
                                   select row.ComplianceInstanceID).ToList();

                return InstanceIDs;
            }
        }

        public static ComplianceScheduleOn GetLastScheduleOnByInstance(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.ComplianceScheduleOns
                                   where row.ComplianceInstanceID == complianceInstanceID
                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                   orderby row.ScheduleOn descending, row.ID descending
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }

        
       

        public static void ResetComplianceSchedule(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceSchedule = (from row in entities.ComplianceSchedules
                                          where row.ComplianceID == ComplianceID
                                          select row).ToList();

                complianceSchedule.ForEach(entry =>
                {
                    entities.ComplianceSchedules.Remove(entry);
                    entities.ComplianceSchedules.Attach(entry);                    
                });
                entities.SaveChanges();
            }
        }
        public static void ResetCustomerwiseComplianceSchedule(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceSchedule = (from row in entities.CustomerWiseComplianceSchedules
                                          where row.ComplianceID == ComplianceID
                                          select row).ToList();

                ComplianceSchedule.ForEach(entry =>
                {
                    entities.CustomerWiseComplianceSchedules.Remove(entry);
                    entities.CustomerWiseComplianceSchedules.Attach(entry);
                });
                entities.SaveChanges();
            }
        }
        //added By manisha
        public static void CreateIndustryMapping(IndustryMapping IndustryMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.IndustryMappings.Add(IndustryMapping);
                entities.SaveChanges();
            }
        }
        public static void CreateSecreterialMapping(Compliance_SecretarialTagMapping SecretarialTagMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Compliance_SecretarialTagMapping.Add(SecretarialTagMapping);
                entities.SaveChanges();
            }
        }

        //added By manisha
        public static void CreateLegalEntityMapping(LegalEntityTypeMapping LegalEntityTypeMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.LegalEntityTypeMappings.Add(LegalEntityTypeMapping);
                entities.SaveChanges();
            }
        }



        //added By Manisha
        public static List<int> GetIndustryMappedID(long ComplianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var IndustryMappedIDs = (from row in entities.IndustryMappings
                                         where row.ComplianceId == ComplianceId && row.IsActive == true
                                         select row.IndustryID).ToList();

                return IndustryMappedIDs;
            }

        }

        //added By Manisha
        public static void UpdateIndustryMappedID(long ComplianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var ids = (from row in entities.IndustryMappings
                           where row.ComplianceId == ComplianceId
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    IndustryMapping prevmappedids = (from row in entities.IndustryMappings
                                                     where row.ID == entry
                                                     select row).FirstOrDefault();
                    entities.IndustryMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();

                //entities.IndustryMappings.Add(IndustryMapping);
                //entities.SaveChanges();

            }

        }

        //added By Manisha
        public static List<int> GetLegalEntityTypeMappedID(long ComplianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var LegalEntityTypeMappedIDs = (from row in entities.LegalEntityTypeMappings
                                                where row.ComplianceId == ComplianceId && row.IsActive == true
                                                select row.LegalEntityTypeID).ToList();

                return LegalEntityTypeMappedIDs;
            }

        }

        //added By Manisha on 25-Jan-2016 
        public static List<Compliance> GetByType_Checklist(int complianceTypeID, int complianceCatagoryId, bool setApprover = false, int lcCustomerBranch = -1, List<int> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<Act> actList = new List<Act>();

                actList = (from row in entities.Acts
                           where row.IsDeleted == false
                           select row).ToList();

                if (complianceCatagoryId != -1)
                {
                    actIDs = (from row in actList
                              where row.ComplianceCategoryId == complianceCatagoryId
                              select row.ID).ToList();
                }
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (complianceCatagoryId != -1 && complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        int StateId = -1;
                        if (lcCustomerBranch != -1)
                        {
                            StateId = CustomerBranchManagement.GetByID(lcCustomerBranch).StateID;
                        }
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId
                                  && (row.ComplianceTypeId == complianceTypeID && row.StateID == StateId)
                                  select row.ID).ToList();
                    }
                    else
                    {
                        actIDs = (from row in actList
                                  where row.ComplianceCategoryId == complianceCatagoryId && row.ComplianceTypeId == complianceTypeID
                                  select row.ID).ToList();
                    }
                }

                if (actIdList != null)
                {
                    actIDs = actIdList;
                }

                List<Compliance> compliances = (from row in entities.Compliances
                                                where actIDs.Contains(row.ActID) && row.IsDeleted == false && (row.ComplianceType == 1)
                                                && row.Status == null && row.ComplinceVisible == true
                                                select row).ToList();

                if (setApprover != false)
                {
                    List<long> query = (from row in entities.ComplianceInstances
                                        join ca in entities.ComplianceAssignments.Where(entry => entry.RoleID == 3 || entry.RoleID == 4 || entry.RoleID == 5)
                                        on row.ID equals ca.ComplianceInstanceID
                                        where row.CustomerBranchID == lcCustomerBranch && row.IsDeleted == false
                                        select row.ComplianceId).ToList();

                    compliances = compliances.Where(entry => query.Contains(entry.ID)).ToList();

                }
                if (!string.IsNullOrEmpty(filter))
                {
                    compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                return compliances;
            }
        }

        

        #region Internal compliances

        public static void UpdateMultiple(InternalCompliance Icompliance, List<InternalComplianceForm> form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var oldData = (from row in entities.InternalCompliances
                               where row.ID == Icompliance.ID
                               select new { row.IFrequency, row.IDueDate }).SingleOrDefault();

                //InternalCompliance complianceToUpdate = new InternalCompliance() { ID = Icompliance.ID };
                //entities.InternalCompliances.Attach(complianceToUpdate);

                InternalCompliance complianceToUpdate = (from row in entities.InternalCompliances
                                                         where row.ID == Icompliance.ID
                                                         select row).FirstOrDefault();

                complianceToUpdate.IComplianceTypeID = Icompliance.IComplianceTypeID;
                complianceToUpdate.IComplianceCategoryID = Icompliance.IComplianceCategoryID;
                complianceToUpdate.IShortDescription = Icompliance.IShortDescription;
                complianceToUpdate.IUploadDocument = Icompliance.IUploadDocument;
                complianceToUpdate.IComplianceType = Icompliance.IComplianceType;
                complianceToUpdate.IRequiredFrom = Icompliance.IRequiredFrom;
                complianceToUpdate.IFrequency = Icompliance.IFrequency;
                complianceToUpdate.IDueDate = Icompliance.IDueDate;
                complianceToUpdate.IRiskType = Icompliance.IRiskType;
                complianceToUpdate.ISubComplianceType = Icompliance.ISubComplianceType;
                complianceToUpdate.IReminderType = Icompliance.IReminderType;
                complianceToUpdate.IReminderBefore = Icompliance.IReminderBefore;
                complianceToUpdate.IReminderGap = Icompliance.IReminderGap;
                complianceToUpdate.UpdatedOn = DateTime.UtcNow;
                //complianceToUpdate.CreatedOn = DateTime.UtcNow;
                complianceToUpdate.UpdatedBy = Icompliance.UpdatedBy;
                //complianceToUpdate.CreatedBy = Icompliance.CreatedBy;
                complianceToUpdate.IsDeleted = false;
                complianceToUpdate.EffectiveDate = Icompliance.EffectiveDate;
                complianceToUpdate.DueWeekDay = Icompliance.DueWeekDay;
                complianceToUpdate.ActID = Icompliance.ActID;
                complianceToUpdate.ScheduleType = Icompliance.ScheduleType;
                complianceToUpdate.IDetailedDescription = Icompliance.IDetailedDescription;
                complianceToUpdate.IShortForm = Icompliance.IShortForm;
                entities.SaveChanges();

                //if (form != null)
                //{
                //    form.IComplianceID = Icompliance.ID;
                //    CreateSampleFile(form, true);
                //}
                //else
                //{
                //    InternalComplianceForm Complianceform = GetIComplianceFormByID(Icompliance.ID);
                //    if (Complianceform != null)
                //    {
                //        entities.InternalComplianceForms.Attach(Complianceform);
                //        entities.InternalComplianceForms.Remove(Complianceform);
                //        entities.SaveChanges();
                //    }
                //}

                if (form.Count > 0)
                {
                    for (int i = 0; i < form.Count; i++)
                    {
                        form[i].IComplianceID = Icompliance.ID;
                        CreateSampleFile(form[i]);
                    }
                }
                if (oldData.IFrequency != Icompliance.IFrequency || oldData.IDueDate != Icompliance.IDueDate)
                {
                    GenerateScheduleForInternalCompliance(Icompliance.ID, Icompliance.ISubComplianceType, true);
                }
            }
        }

        public static void Update(InternalCompliance Icompliance, InternalComplianceForm form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var oldData = (from row in entities.InternalCompliances
                               where row.ID == Icompliance.ID
                               select new { row.IFrequency, row.IDueDate }).SingleOrDefault();

                //InternalCompliance complianceToUpdate = new InternalCompliance() { ID = Icompliance.ID };
                //entities.InternalCompliances.Attach(complianceToUpdate);

                InternalCompliance complianceToUpdate = (from row in entities.InternalCompliances
                                                         where row.ID == Icompliance.ID
                                                         select row).FirstOrDefault();

                complianceToUpdate.IComplianceTypeID = Icompliance.IComplianceTypeID;
                complianceToUpdate.IComplianceCategoryID = Icompliance.IComplianceCategoryID;
                complianceToUpdate.IShortDescription = Icompliance.IShortDescription;
                complianceToUpdate.IUploadDocument = Icompliance.IUploadDocument;
                complianceToUpdate.IComplianceType = Icompliance.IComplianceType;
                complianceToUpdate.IRequiredFrom = Icompliance.IRequiredFrom;
                complianceToUpdate.IFrequency = Icompliance.IFrequency;
                complianceToUpdate.IDueDate = Icompliance.IDueDate;
                complianceToUpdate.IRiskType = Icompliance.IRiskType;
                complianceToUpdate.ISubComplianceType = Icompliance.ISubComplianceType;
                complianceToUpdate.IReminderType = Icompliance.IReminderType;
                complianceToUpdate.IReminderBefore = Icompliance.IReminderBefore;
                complianceToUpdate.IReminderGap = Icompliance.IReminderGap;
                complianceToUpdate.UpdatedOn = DateTime.UtcNow;
                complianceToUpdate.CreatedOn = DateTime.UtcNow;
                complianceToUpdate.UpdatedBy = Icompliance.UpdatedBy;
                complianceToUpdate.CreatedBy = Icompliance.CreatedBy;
                complianceToUpdate.IsDeleted = false;
                complianceToUpdate.EffectiveDate = Icompliance.EffectiveDate;
                complianceToUpdate.DueWeekDay = Icompliance.DueWeekDay;
                complianceToUpdate.ActID = Icompliance.ActID;
                complianceToUpdate.IsDocumentRequired = Icompliance.IsDocumentRequired;
                complianceToUpdate.IShortForm = Icompliance.IShortForm;
                entities.SaveChanges();

                if (form != null)
                {
                    form.IComplianceID = Icompliance.ID;
                    CreateSampleFile(form, true);
                }
                //else
                //{
                //    InternalComplianceForm Complianceform = GetIComplianceFormByID(Icompliance.ID);
                //    if (Complianceform != null)
                //    {
                //        entities.InternalComplianceForms.Attach(Complianceform);
                //        entities.InternalComplianceForms.Remove(Complianceform);
                //        entities.SaveChanges();
                //    }
                //}

                if (oldData.IFrequency != Icompliance.IFrequency || oldData.IDueDate != Icompliance.IDueDate)
                {
                    GenerateScheduleForInternalCompliance(Icompliance.ID, Icompliance.ISubComplianceType, true);
                }
            }
        }

        public static void Create(InternalCompliance compliance, InternalComplianceForm form)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                compliance.CreatedOn = DateTime.UtcNow;
                compliance.IsDeleted = false;
                compliance.UpdatedOn = DateTime.UtcNow;

                bool chkexists = ExistsInternalR(compliance.IShortDescription);
                if (chkexists == false)
                {
                    entities.InternalCompliances.Add(compliance);

                    entities.SaveChanges();

                    if (form != null)
                    {
                        form.IComplianceID = compliance.ID;
                        CreateSampleFile(form);
                    }

                    if (compliance.IFrequency.HasValue)
                    {
                        GenerateScheduleForInternalCompliance(compliance.ID, compliance.ISubComplianceType);
                    }
                }
            }
        }

        
        public static void CreateSampleFile(InternalComplianceForm form, bool deleteOldFiles = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //if (deleteOldFiles)
                //{
                //    var formIDsToDelete = (from row in entities.InternalComplianceForms
                //                           where row.IComplianceID == form.IComplianceID
                //                           select row.ID).ToList();

                //    formIDsToDelete.ForEach(entry =>
                //    {
                //        var formToDelete = (from row in entities.InternalComplianceForms
                //                            where row.ID == entry
                //                            select row).FirstOrDefault();

                //        entities.InternalComplianceForms.Attach(formToDelete);
                //        entities.InternalComplianceForms.Remove(formToDelete);
                //    });
                //}

                entities.InternalComplianceForms.Add(form);
                entities.SaveChanges();
            }

        }
        public static InternalComplianceForm GetIComplianceFormByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceForms
                            where row.IComplianceID == complianceID
                            select row).SingleOrDefault();

                return form;
            }
        }

        public static void GenerateScheduleForInternalCompliance(long complianceID, byte? subcomplincetype, bool deleteOldData = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                if (!(subcomplincetype == 0))
                {
                    if (deleteOldData)
                    {
                        var ids = (from row in entities.InternalComplianceSchedules
                                   where row.IComplianceID == complianceID
                                   select row.ID).ToList();



                        ids.ForEach(entry =>
                        {
                            InternalComplianceSchedule schedule = (from row in entities.InternalComplianceSchedules
                                                                   where row.ID == entry
                                                                   select row).FirstOrDefault();

                            entities.InternalComplianceSchedules.Remove(schedule);
                        });

                        entities.SaveChanges();
                    }
                    var compliance = (from row in entities.InternalCompliances
                                      where row.ID == complianceID
                                      select row).First();

                    int step = 1;

                    switch ((Frequency) compliance.IFrequency.Value)
                    {
                        case Frequency.Quarterly:
                            step = 3;
                            break;
                        case Frequency.FourMonthly:
                            step = 4;
                            break;
                        case Frequency.HalfYearly:
                            step = 6;
                            break;
                        case Frequency.Annual:
                            step = 12;
                            break;
                        case Frequency.TwoYearly:
                            step = 12;
                            break;
                        case Frequency.SevenYearly:
                            step = 12;
                            break;
                    }


                    int SpecialMonth;
                    for (int month = 1; month <= 12; month += step)
                    {
                        InternalComplianceSchedule complianceShedule = new InternalComplianceSchedule();
                        complianceShedule.IComplianceID = complianceID;
                        complianceShedule.ForMonth = month;

                        if (subcomplincetype == 1 || subcomplincetype == 2)
                        {
                            int monthCopy = month;
                            if (monthCopy == 1)
                            {
                                monthCopy = 12;
                                complianceShedule.ForMonth = monthCopy;
                            }
                            else
                            {
                                monthCopy = month - 1;
                                complianceShedule.ForMonth = monthCopy;
                            }

                            if (compliance.IFrequency == 3 || compliance.IFrequency == 5 || compliance.IFrequency == 6)
                            {
                                monthCopy = 3;
                                complianceShedule.ForMonth = monthCopy;
                            }

                            int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                            complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");

                        }
                        else
                        {
                            SpecialMonth = month;
                            if (compliance.IFrequency == 0 && SpecialMonth == 12)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliance.IFrequency == 1 && SpecialMonth == 10)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliance.IFrequency == 2 && SpecialMonth == 7)
                            {
                                SpecialMonth = 1;
                            }
                            else if (compliance.IFrequency == 4 && SpecialMonth == 9)
                            {
                                SpecialMonth = 1;
                            }
                            else if ((compliance.IFrequency == 3 || compliance.IFrequency == 5 || compliance.IFrequency == 6) && SpecialMonth == 1)
                            {
                                SpecialMonth = 1;
                            }
                            else
                            {
                                SpecialMonth = SpecialMonth + step;
                            }

                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                            if (Convert.ToInt32(compliance.IDueDate.Value.ToString("D2")) > lastdate)
                            {
                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                            }
                            else
                            {
                                complianceShedule.SpecialDate = compliance.IDueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                            }
                        }
                        entities.InternalComplianceSchedules.Add(complianceShedule);

                    }

                    entities.SaveChanges();
                }
            }
        }
        public static bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static List<InternalComplianceView> GetAllInternalCompliances(int ComplianceTypeID, int complianceCategoryID, int frq, int CmType, int CustomerID, string filter = null)
        {
            List<InternalComplianceView> complianceInformation = new List<InternalComplianceView>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                complianceInformation = (from row in entities.InternalComplianceViews
                                         select row).ToList();

                int risk = -1;
                if (filter.ToUpper().Equals("HIGH"))
                    risk = 0;
                else if (filter.ToUpper().Equals("MEDIUM"))
                    risk = 1;
                else if (filter.ToUpper().Equals("LOW"))
                    risk = 2;
                else if (filter.ToUpper().Equals("CRICITAL"))
                    risk = 3;

                string frequency = filter.ToUpper();
                if (frequency.Equals("MONTHLY"))
                    frequency = "Monthly";
                if (frequency.Equals("QUARTERLY"))
                    frequency = "Quarterly";
                if (frequency.Equals("HALFYEARLY"))
                    frequency = "HalfYearly";
                if (frequency.Equals("ANNUAL"))
                    frequency = "Annual";
                if (frequency.Equals("FOURMONTHALY"))
                    frequency = "FourMonthly";
                if (frequency.Equals("TWOYEARLY"))
                    frequency = "TwoYearly";
                if (frequency.Equals("SEVENYEARLY"))
                    frequency = "SevenYearly";

                int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                if (frequencyId == 0 && frequency != "Monthly")
                {
                    frequencyId = -1;
                }

                if (!string.IsNullOrEmpty(filter))
                {

                    if (CheckInt(filter))
                    {
                        int a = Convert.ToInt32(filter);
                        complianceInformation = complianceInformation.Where(entry => entry.ID == a).ToList();  
                    }
                    else
                    {
                        complianceInformation = complianceInformation.Where(entry => entry.IShortDescription.ToUpper().Contains(filter.ToUpper())
                              || (entry.IComplianceTypeName != null && entry.IComplianceTypeName.ToUpper().Trim().Contains(filter.ToUpper().Trim()))
                              || (entry.IRiskType != null && entry.IRiskType.ToString().Trim().Contains(filter.ToUpper().Trim()))
                              || (entry.IFrequency != null && entry.IRiskType.ToString().Trim().Contains(filter.ToUpper().Trim()))
                              || (entry.IComplianceCategoryName != null && entry.IComplianceCategoryName.ToUpper().Trim().Contains(filter.ToUpper().Trim()))
                             || (entry.IShortForm.ToUpper().Contains(filter.ToUpper().Trim()))).ToList();
                    }
                }

                if (ComplianceTypeID != -1)
                {
                    complianceInformation = complianceInformation.Where(entry => entry.IComplianceTypeID == ComplianceTypeID).ToList();
                }

                if (complianceCategoryID != -1)
                {
                    complianceInformation = complianceInformation.Where(entry => entry.IComplianceCategoryID == complianceCategoryID).ToList();
                }

                if (CmType != -1)
                {
                    complianceInformation = complianceInformation.Where(entry => entry.IComplianceType == CmType).ToList();
                }
                if (frq != -1)
                {
                    complianceInformation = complianceInformation.Where(entry => entry.IFrequency == frq).ToList();
                }
                //added By Manisha
                if (CustomerID != -1)
                {
                    complianceInformation = complianceInformation.Where(entry => entry.CustomerID == CustomerID).ToList();
                }


                return complianceInformation;
            }
        }


        public static InternalCompliance GetInternalComplianceByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.InternalCompliances
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }

        public static InternalComplianceForm GetInternalComplianceFormByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceForms
                            where row.IComplianceID == complianceID
                            select row).FirstOrDefault();

                return form;
            }
        }
        public static List<InternalComplianceForm> GetInternalComplianceFormByIDMultiple(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceForms
                            where row.IComplianceID == complianceID
                            select row).ToList();

                return form;
            }
        }

        public static void DeleteInternalCompliance(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //InternalCompliance complianceToDelete = new InternalCompliance() { ID = complianceID };
                //entities.InternalCompliances.Attach(complianceToDelete);

                InternalCompliance complianceToDelete = (from row in entities.InternalCompliances
                                                         where row.ID == complianceID
                                                         select row).FirstOrDefault();

                complianceToDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static List<InternalComplianceSchedule> GetInternalScheduleByIComplianceID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.InternalComplianceSchedules
                                    where row.IComplianceID == complianceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }    
        public static void ResetInternalComplianceSchedule(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceSchedule = (from row in entities.InternalComplianceSchedules
                                          where row.IComplianceID == ComplianceID
                                          select row).ToList();

                complianceSchedule.ForEach(entry =>
                {
                    entities.InternalComplianceSchedules.Remove(entry);
                    entities.InternalComplianceSchedules.Attach(entry);                    
                });
                entities.SaveChanges();
            }
        }

        public static void UpdateInternalComplianceScheduleInformation(List<InternalComplianceSchedule> scheduleList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                scheduleList.ForEach(schedule =>
                {
                    //InternalComplianceSchedule complianceScheduleToUpdate = new InternalComplianceSchedule() { ID = schedule.ID };
                    //entities.InternalComplianceSchedules.Attach(complianceScheduleToUpdate);

                    InternalComplianceSchedule complianceScheduleToUpdate = (from row in entities.InternalComplianceSchedules
                                                                             where row.ID == schedule.ID
                                                                             select row).FirstOrDefault();

                    complianceScheduleToUpdate.IComplianceID = schedule.IComplianceID;
                    complianceScheduleToUpdate.ForMonth = schedule.ForMonth;
                    complianceScheduleToUpdate.SpecialDate = schedule.SpecialDate;
                });
                entities.SaveChanges();
            }
        }

        public static bool ExistsComplianceScheduleAssignment(long Complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceInstances
                             join row1 in entities.ComplianceAssignments
                             on row.ID equals row1.ComplianceInstanceID
                             where row.ComplianceId.Equals(Complianceid) && row.IsDeleted == false
                             select row1.ID).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public static bool ExistsInternalComplianceScheduleAssignment(long InternalComplianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.InternalComplianceInstances
                             join row1 in entities.InternalComplianceAssignments
                             on row.ID equals row1.InternalComplianceInstanceID
                             where row.InternalComplianceID.Equals(InternalComplianceid) && row.IsDeleted == false
                             select row1.ID).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        public static bool ExistsInternalComplianceAssignment(long InternalComplianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.TempAssignmentTableInternals  
                             where row.ComplianceId.Equals(InternalComplianceid) && row.IsActive == true
                             select row.ID).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public static bool ExistsComplianceAssignment(long Complianceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.TempAssignmentTables
                             where row.ComplianceId.Equals(Complianceid) && row.IsActive == true
                             select row.ID).FirstOrDefault();
                if (query != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion
        //added By manisha on 22-Feb-2016
      
        public static bool TratsactionExists(int complianceInstanceid, int compliancescheduleonid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> statusids = new List<int>();
                statusids.Add(2);
                statusids.Add(3);
                statusids.Add(10);
                statusids.Add(4);
                statusids.Add(5);
                statusids.Add(11);
                var IsUpcomingtoDeleteusingtran = (from row in entities.ComplianceTransactions
                                                   where row.ComplianceInstanceId == complianceInstanceid
                                                   && row.ComplianceScheduleOnID == compliancescheduleonid
                                                   && statusids.Contains(row.StatusId)
                                                   select row.ID).FirstOrDefault();

                if (IsUpcomingtoDeleteusingtran != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
       
        public static void ComplianceScheduleOnUpdateAsNotActive(long ComplianceID, long createdByID, DateTime EffectiveDate)
        {
            try
            {
                bool chktran = true;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var compliances = (from row in entities.Compliances
                                       where row.ID == ComplianceID
                                       select row).FirstOrDefault();

                    if (compliances.Frequency != 7)
                    {
                        var instanceIds = (from row in entities.ComplianceInstances
                                       where row.ComplianceId == ComplianceID                                     
                                       select row.ID).ToList();
                    
                        if (instanceIds.Count > 0)
                        {
                            DateTime startDate = new DateTime(EffectiveDate.Year, EffectiveDate.Month, EffectiveDate.Day);
                            instanceIds.ForEach(entry =>
                            {
                                var ScheduleOnIds = (from row in entities.ComplianceScheduleOns
                                                     where row.ComplianceInstanceID == entry
                                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                     select row.ID).ToList();

                                if (ScheduleOnIds.Count > 0)
                                {

                                    ScheduleOnIds.ForEach(entryScheduleOn =>
                                    {
                                        ComplianceScheduleOn scheduleon = (from row in entities.ComplianceScheduleOns
                                                                           where row.ID == entryScheduleOn
                                                                           select row).FirstOrDefault();

                                        chktran = TratsactionExists(Convert.ToInt32(entry), Convert.ToInt32(entryScheduleOn));
                                        if (chktran == false)
                                        {
                                            scheduleon.IsActive = false;

                                            TaskScheduleOn taskscheduleon = (from row in entities.TaskScheduleOns
                                                                             where row.ComplianceScheduleOnID == entryScheduleOn
                                                                             select row).FirstOrDefault();
                                            if (taskscheduleon != null)
                                            {
                                                taskscheduleon.IsActive = false;
                                            }
                                        }
                                        ComplianceScheduleOn IsUpcomingtoDelete = (from row in entities.ComplianceScheduleOns
                                                                                   where row.ID == entryScheduleOn && row.ScheduleOn >= startDate
                                                                                   select row).FirstOrDefault();
                                        if (IsUpcomingtoDelete != null)
                                        {
                                            IsUpcomingtoDelete.IsUpcomingNotDeleted = false;

                                            TaskScheduleOn taskIsUpcomingtoDelete = (from row in entities.TaskScheduleOns
                                                                                     where row.ComplianceScheduleOnID == entryScheduleOn && row.ScheduleOn >= startDate
                                                                                     select row).FirstOrDefault();
                                            if (taskIsUpcomingtoDelete != null)
                                            {
                                                taskIsUpcomingtoDelete.IsUpcomingNotDeleted = false;
                                            }
                                        }
                                        else
                                        {
                                            chktran = TratsactionExists(Convert.ToInt32(entry), Convert.ToInt32(entryScheduleOn));
                                            if (chktran == false)
                                            {
                                                ComplianceScheduleOn UsingTranIsUpcomingtoDelete = (from row in entities.ComplianceScheduleOns
                                                                                                    where row.ID == entryScheduleOn
                                                                                                    select row).FirstOrDefault();
                                                UsingTranIsUpcomingtoDelete.IsUpcomingNotDeleted = false;

                                                TaskScheduleOn taskUsingTranIsUpcomingtoDelete = (from row in entities.TaskScheduleOns
                                                                                                  where row.ComplianceScheduleOnID == entryScheduleOn
                                                                                                  select row).FirstOrDefault();
                                                if (taskUsingTranIsUpcomingtoDelete != null)
                                                {
                                                    taskUsingTranIsUpcomingtoDelete.IsUpcomingNotDeleted = false;
                                                }
                                            }
                                        }
                                    });
                                    entities.SaveChanges();
                                }
                                //H_Edit_Compliance H_Edit_Compliance = new H_Edit_Compliance()
                                //{
                                //    ComplianceID = ComplianceID,
                                //    PrevComplianceInstanceID = entry,
                                //    EffectiveDate = EffectiveDate,
                                //    Createdby = Convert.ToInt32(createdByID),
                                //    CreatedOn = DateTime.Now,

                                //};
                                //CreateHistoryEditCompliance(H_Edit_Compliance);
                            });
                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        public static void EditCompliance(Compliance compliance, ComplianceForm form, long createdByID, string creatdByName)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var oldData = (from row in entities.Compliances
                                   where row.ID == compliance.ID
                                   select new { row.Frequency, row.DueDate, row.ComplianceType, row.SubComplianceType, row.ReminderType, row.ReminderBefore, row.ReminderGap, row.DueWeekDay }).SingleOrDefault();

                    Compliance complianceToUpdate = (from row in entities.Compliances
                                                     where row.ID == compliance.ID
                                                     select row).FirstOrDefault();

                    complianceToUpdate.ActID = compliance.ActID;
                    complianceToUpdate.ShortDescription = compliance.ShortDescription;
                    complianceToUpdate.Description = compliance.Description;
                    complianceToUpdate.Sections = compliance.Sections;
                    complianceToUpdate.UploadDocument = compliance.UploadDocument;
                    complianceToUpdate.ComplianceType = compliance.ComplianceType;
                    complianceToUpdate.NatureOfCompliance = compliance.NatureOfCompliance;
                    complianceToUpdate.RequiredForms = compliance.RequiredForms;
                    complianceToUpdate.Frequency = compliance.Frequency;
                    complianceToUpdate.DueDate = compliance.DueDate;
                    complianceToUpdate.RiskType = compliance.RiskType;
                    complianceToUpdate.NonComplianceType = compliance.NonComplianceType;
                    complianceToUpdate.NonComplianceEffects = compliance.NonComplianceEffects;
                    complianceToUpdate.VariableAmountPerMonth = compliance.VariableAmountPerMonth;

                    complianceToUpdate.FixedMinimum = compliance.FixedMinimum;
                    complianceToUpdate.FixedMaximum = compliance.FixedMaximum;
                    complianceToUpdate.VariableAmountPerDay = compliance.VariableAmountPerDay;
                    complianceToUpdate.VariableAmountPerDayMax = compliance.VariableAmountPerDayMax;
                    complianceToUpdate.VariableAmountPercent = compliance.VariableAmountPercent;
                    complianceToUpdate.VariableAmountPercentMax = compliance.VariableAmountPercentMax;

                    complianceToUpdate.Imprisonment = compliance.Imprisonment;
                    complianceToUpdate.Designation = compliance.Designation;
                    complianceToUpdate.MinimumYears = compliance.MinimumYears;
                    complianceToUpdate.MaximumYears = compliance.MaximumYears;
                    complianceToUpdate.Others = compliance.Others;
                    complianceToUpdate.EventID = compliance.EventID;
                    complianceToUpdate.FixedGap = compliance.FixedGap;
                    complianceToUpdate.SubComplianceType = compliance.SubComplianceType;
                    complianceToUpdate.EventComplianceType = compliance.EventComplianceType;
                    complianceToUpdate.ReminderType = compliance.ReminderType;
                    complianceToUpdate.ReminderBefore = compliance.ReminderBefore;
                    complianceToUpdate.ReminderGap = compliance.ReminderGap;
                    complianceToUpdate.SubEventID = compliance.SubEventID;
                    complianceToUpdate.PenaltyDescription = compliance.PenaltyDescription;
                    complianceToUpdate.ReferenceMaterialText = compliance.ReferenceMaterialText;
                    complianceToUpdate.UpdatedOn = compliance.UpdatedOn;
                    complianceToUpdate.DueWeekDay = compliance.DueWeekDay;
                    complianceToUpdate.OneTimeDate = compliance.OneTimeDate;
                    entities.SaveChanges();
                    if (form != null)
                    {
                        form.ComplianceID = compliance.ID;
                        CreateSampleFile(form, true);
                    }
                    else
                    {
                        ComplianceForm Complianceform = GetComplianceFormByID(compliance.ID);
                        if (Complianceform != null)
                        {
                            entities.ComplianceForms.Attach(Complianceform);
                            entities.ComplianceForms.Remove(Complianceform);
                            entities.SaveChanges();
                        }
                    }

                    if (compliance.Frequency != 7)
                    {
                        if (compliance.Frequency != 8)
                        {
                            if (oldData.Frequency != compliance.Frequency || oldData.DueDate != compliance.DueDate)
                            {
                                GenerateDefaultScheduleForComplianceIDNEW(compliance.ID, compliance.SubComplianceType, true);
                            }
                        }
                    }

                    if ((oldData.Frequency != compliance.Frequency) || (oldData.DueDate != compliance.DueDate) || (oldData.ComplianceType != compliance.ComplianceType) ||
                        (oldData.SubComplianceType != compliance.SubComplianceType) || (oldData.ReminderType != compliance.ReminderType) ||
                        (oldData.ReminderBefore != compliance.ReminderBefore) || (oldData.ReminderGap != compliance.ReminderGap) || (oldData.DueWeekDay != compliance.DueWeekDay))
                    {
                        DateTime dEffectiveDate = (DateTime)compliance.EffectiveDate;

                        ComplianceScheduleOnUpdateAsNotActive(compliance.ID, createdByID, dEffectiveDate);

                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public static List<int> GetEventSubEventMappedID(long eventId, string eventType, int CompanyType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.EventSubEventMappings
                                      where row.ParentEventID == eventId && row.IsActive == true && row.Type == CompanyType && row.EventType == eventType && row.ComplianceID == 0
                                      select row.SubEventID).ToList();
                return eventMappedIDs;
            }

        }

        public static bool CheckIntermediateEvent(long eventId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.EventSubEventMappings
                            where row.ParentEventID == eventId && row.IsActive == true && row.ComplianceID == 0 && row.IntermediateFlag == true
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static List<int> GetIntermediateEventMappedID(long eventId, string eventType, int CompanyType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.EventSubEventMappings
                                      where row.ParentEventID == eventId && row.IsActive == true && row.Type == CompanyType && row.EventType == eventType && row.ComplianceID == 0
                                      select row.IntermediateEventID).ToList();
                return eventMappedIDs;
            }

        }

        public static void UpdateEventSubEventMappedID(long EventID, string EventType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.EventSubEventMappings
                           where row.ParentEventID == EventID && row.EventType == EventType && row.ComplianceID == 0
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    EventSubEventMapping prevmappedids = (from row in entities.EventSubEventMappings
                                                          where row.ID == entry
                                                          select row).FirstOrDefault();
                    entities.EventSubEventMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }

        public static void CreateEventSubEventMapping(EventSubEventMapping eventSubEventMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventSubEventMappings.Add(eventSubEventMapping);
                entities.SaveChanges();
            }
        }

        public static List<int> GetEventComplianceMappedID(long eventId, string eventType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.EventSubEventMappings
                                      where row.ParentEventID == eventId && row.IsActive == true && row.EventType == eventType && row.SubEventID == 0
                                      select row.ComplianceID).ToList();
                return eventMappedIDs;
            }

        }

        public static void UpdateComplianceMappedID(long EventID, string EventType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.EventSubEventMappings
                           where row.ParentEventID == EventID && row.EventType == EventType && row.SubEventID == null
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    EventSubEventMapping prevmappedids = (from row in entities.EventSubEventMappings
                                                          where row.ID == entry
                                                          select row).FirstOrDefault();
                    entities.EventSubEventMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }

        public static void CreateEventCompAssignDays(EventCompAssignDay eventCompAssignDay)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventCompAssignDays.Add(eventCompAssignDay);
                entities.SaveChanges();
            }
        }

        public static void UpdateComplianceForEventMappedID(long EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.EventMappings
                           where row.EventID == EventID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    EventMapping prevmappedids = (from row in entities.EventMappings
                                                  where row.ID == entry
                                                  select row).FirstOrDefault();
                    entities.EventMappings.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }
        public static void RemoveDailyUpdateActMapping(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.DailyUpdateActMappings
                           where row.DailyUpdateID == DailyUpdateID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    DailyUpdateActMapping prevmappedids = (from row in entities.DailyUpdateActMappings
                                                  where row.ID == entry
                                                  select row).FirstOrDefault();
                    entities.DailyUpdateActMappings.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }

        public static void RemoveActDocument(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.Act_Document
                           where row.DailyUpdateID == DailyUpdateID
                           select row.Id).ToList();

                ids.ForEach(entry =>
                {
                    Act_Document prevmappedids = (from row in entities.Act_Document
                                                           where row.Id == entry
                                                           select row).FirstOrDefault();
                    entities.Act_Document.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }
        public static void UpdateTaskMappedID(long TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.TaskLinkings
                           where row.TaskID == TaskID
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    TaskLinking prevmappedids = (from row in entities.TaskLinkings
                                                  where row.ID == entry
                                                  select row).FirstOrDefault();
                    entities.TaskLinkings.Remove(prevmappedids);
                });
                entities.SaveChanges();
            }
        }

        public static void CreateEventMapping(EventMapping eventMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventMappings.Add(eventMapping);
                entities.SaveChanges();
            }
        }

        public static void CreateDailyUpdateActMapping(DailyUpdateActMapping actMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.DailyUpdateActMappings.Add(actMapping);
                entities.SaveChanges();
            }
        }
        public static void CreateTaskMapping(TaskLinking taskLinking)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TaskLinkings.Add(taskLinking);
                entities.SaveChanges();
            }
        }
        public static void CreateEventSequence(EventSequence eventSequence)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.EventSequences.Add(eventSequence);
                entities.SaveChanges();
            }
        }


        public static List<long> GetTaskLinkingID(long TaskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var TaskLinkingIDs = (from row in entities.Tasks
                                      join row1 in entities.TaskLinkings
                                      on row.ID equals row1.TaskID
                                      where row1.TaskID == TaskID && row.Isdeleted == false 
                                      select row1.TaskLinkID).ToList();

                return TaskLinkingIDs;
            }

        }

        public static List<long> GetCmplianceMappedID(long EventID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.Compliances
                                      join row1 in entities.EventMappings
                                      on row.ID equals row1.ComplianceID
                                      where row1.EventID == EventID && row.IsDeleted == false
                                      select row.ID).ToList();

                return eventMappedIDs;
            }

        }

        public static List<int> GetActMappedID(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var eventMappedIDs = (from row in entities.DailyUpdates
                                      join row1 in entities.DailyUpdateActMappings
                                      on row.ID equals row1.DailyUpdateID
                                      where row.ID == DailyUpdateID && row.IsDeleted == false
                                      select row1.ActID).ToList();

                return eventMappedIDs;
            }

        }

        public static int? CheckSchemeSelected(long DailyUpdateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var SchemeID = (from row in entities.DailyUpdates
                                     where row.ID == DailyUpdateID && row.IsDeleted == false
                                      select (int?)row.SchemeID).FirstOrDefault();

                if (SchemeID != null && SchemeID != 0 && SchemeID != -1)
                    return 0;
                else
                    return SchemeID;
            }
        }


        

        public static void CreateEventBasedComplinceReminders(long complianceID, long ComplianceInstanceId, int ComplianceAssignmentId, DateTime eventDueDate, DateTime scheduledOn, long EventScheduledOnID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<long> getdays = GetUpcomingReminderEventDaysDetail(ComplianceAssignmentId, "SE");
                    if (getdays.Count > 0)
                    {
                        foreach (var item in getdays)
                        {
                            ComplianceReminder reminder = new ComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = scheduledOn,
                                EventScheduledOnID = EventScheduledOnID,
                                RemindOn = scheduledOn.Date.AddDays(-item)
                            };
                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                            entities.ComplianceReminders.Add(reminder);

                            entities.SaveChanges();
                        }
                    }
                    else
                    {
                        ComplianceReminder reminder = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            EventScheduledOnID = EventScheduledOnID,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                        };
                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder);

                        ComplianceReminder reminder1 = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            EventScheduledOnID = EventScheduledOnID,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 7),
                        };
                        reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder1);


                        ComplianceReminder reminder2 = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            EventScheduledOnID = EventScheduledOnID,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                        };
                        reminder2.Status = (byte)(reminder2.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder2);

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();

                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                //msg.FunctionName = "CreateReminders";
                msg.FunctionName = "CreateReminders" + "ComplianceAssignmentId" + ComplianceAssignmentId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateReminders Function", "CreateReminders" + "ComplianceAssignmentId" + ComplianceAssignmentId);

                SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateReminders" + "ComplianceAssignmentId" + ComplianceAssignmentId);

            }
        }

        public static List<long> GetUpcomingReminderEventDaysDetail(long ComplianceAssignmentId, string Type)
        {
            List<long> output = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User User = (from row in entities.Users
                             join row1 in entities.ComplianceAssignments
                             on row.ID equals row1.UserID
                             where row1.ID == ComplianceAssignmentId
                             && row.IsActive == true
                             && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (User != null)
                {
                    long customerID = Convert.ToInt32(User.CustomerID);
                    long UserID = Convert.ToInt32(User.ID);

                    var UserObj = (from row in entities.ReminderTemplate_UserMapping
                                   where row.CustomerID == customerID
                                   && row.UserID == UserID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   && row.Flag == "UN"
                                   select row).ToList();

                    if (UserObj.Count > 0)
                    {
                        output.Clear();
                        output = UserObj.Select(x => x.TimeInDays).ToList();
                    }
                    else
                    {
                        var CustObj = (from row in entities.ReminderTemplate_CustomerMapping
                                       where row.CustomerID == customerID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       && row.Flag == "UN"
                                       select row).ToList();

                        if (CustObj.Count > 0)
                        {
                            output.Clear();
                            output = CustObj.Select(x => x.TimeInDays).ToList();
                        }
                        else
                        {
                            output.Clear();
                        }
                    }
                }
                return output;
            }
        }


        public static List<DailyUpdate> GetDailyUpdates()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DailyUpdateDetail = (from row in entities.DailyUpdates
                                         where row.IsDeleted == false
                                         select row).OrderByDescending(entry => entry.CreatedDate).Take(200).ToList();


                return DailyUpdateDetail;
            }
        }

        public static List<DailyUpdate> GetDailyUpdates_RLCS()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> lstComplianceCategory = new List<int>();
                lstComplianceCategory.Add(2);
                lstComplianceCategory.Add(5);

                var DailyUpdateDetail = (from DU in entities.DailyUpdates
                                         join DUAM in entities.DailyUpdateActMappings
                                         on DU.ID equals DUAM.DailyUpdateID
                                         join rowAct in entities.Acts
                                         on DUAM.ActID equals rowAct.ID
                                         where DU.IsDeleted == false
                                         && rowAct.ComplianceCategoryId != null
                                         && (lstComplianceCategory.Contains((int)rowAct.ComplianceCategoryId))
                                         select DU).OrderByDescending(entry => entry.CreatedDate).Take(200).ToList();

                return DailyUpdateDetail;
            }
        }


        public static widget GetWidgets(int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var WidgetList = (from row in entities.widgets
                                  where row.UserId == userid
                                  select row).FirstOrDefault();


                return WidgetList;
            }
        }
        public static List<emailview> GetEmails(int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var useremail = (from row in entities.Users
                                 where row.ID == userid
                                 select row).FirstOrDefault();

                var WidgetList = (from row in entities.emailviews
                                  where row.Recepient == useremail.Email
                                  select row).OrderBy(entry => entry.IsRead).Take(3).ToList();

                return WidgetList;
            }
        }

        public static emailview GetEmailById(int Id)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var WidgetList = (from row in entities.emailviews
                                  where row.Id == Id
                                  select row).FirstOrDefault();

                // .OrderBy(entry => entry.IsRead).Take(10).ToList();
                Email eventToUpdate = (from row in entities.Emails
                                       where row.Id == Id
                                       select row).FirstOrDefault();

                eventToUpdate.IsRead = true;

                entities.SaveChanges();

                return WidgetList;
            }
        }
        public static int GetEmailscnt(int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var useremail = (from row in entities.Users
                                 where row.ID == userid
                                 select row).FirstOrDefault();

                var WidgetList = (from row in entities.Emails
                                  where row.Recepient == useremail.Email && (row.IsRead == null || row.IsRead == false)
                                  select row).Count();


                return WidgetList;
            }
        }

        public static List<NewsLetter> GetNewsLetter()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NewsLetterDetail = (from row in entities.NewsLetters
                                        where row.IsDeleted == false
                                        select row).OrderByDescending(entry => entry.CreatedDate).Take(100).ToList();
                return NewsLetterDetail;
            }
        }


        public static void CreateNotification(Notification newNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Notifications.Add(newNotification);
                entities.SaveChanges();
            }
        }

        public static void CreateUserNotification(UserNotification newNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.UserNotifications.Add(newNotification);
                entities.SaveChanges();
            }
        }

        public static long UpdateNotification(Notification NewNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.Notifications
                                                  where row.ActID == NewNotification.ActID
                                                  && row.ComplianceID == NewNotification.ComplianceID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.Remark = NewNotification.Remark;
                    NotificationRecordToUpdate.UpdatedBy = NewNotification.UpdatedBy;
                    NotificationRecordToUpdate.UpdatedOn = NewNotification.UpdatedOn;
                    entities.SaveChanges();

                    return NotificationRecordToUpdate.ID;
                }
                else
                    return 0;
            }
        }

        public static void UpdateUserNotification(UserNotification NewNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var NotificationRecordToUpdate = (from row in entities.UserNotifications
                                                  where row.NotificationID == NewNotification.NotificationID
                                                  && row.UserID == NewNotification.UserID
                                                  select row).FirstOrDefault();

                if (NotificationRecordToUpdate != null)
                {
                    NotificationRecordToUpdate.IsRead = NewNotification.IsRead;
                    entities.SaveChanges();
                }
            }
        }

        public static List<long> GetAllUsersAssignedByComplianceID(long CompID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                

                var ListOfUsers = (from row in entities.SP_GetNotificationUsersID(Convert.ToInt32(CompID))                                  
                                   select (long)row).ToList();

                return ListOfUsers;
            }
        }
        public static List<SP_GetNotificationResearchUsersID_Result> GetAllUsersAssignedResearchByComplianceID(long CompID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {               

                var ListOfUsers = (from row in entities.SP_GetNotificationResearchUsersID(Convert.ToInt32(CompID))
                                   select row).ToList();

                return ListOfUsers;
            }
        }

        public static List<long> GetAllUsersAssignedByInternalComplianceID(long ICompID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ListOfUsers = (from row in entities.InternalComplianceInstances
                                   join row1 in entities.InternalComplianceAssignments
                                   on row.ID equals row1.InternalComplianceInstanceID
                                   where row.InternalComplianceID == ICompID
                                   select row1.UserID).ToList();

                return ListOfUsers;
            }
        }

        public static List<SP_GetUserNotificationsByUserID_Result> GetAllUserNotification(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserNotifications = (from row in entities.SP_GetUserNotificationsByUserID(Convert.ToInt32(UserID))
                                         where row.Type != "DailyUpdate"
                                         select row).ToList();
                if (UserNotifications.Count > 0)
                {
                    UserNotifications = UserNotifications.GroupBy(entry => entry.UpdatedOn).Select(entry => entry.FirstOrDefault()).ToList();
                }
                return UserNotifications;
            }
        }

        public static int GetUnreadUserNotification(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserNotifications = (from row in entities.UserNotifications
                                         where row.UserID == UserID
                                         && row.IsRead == false
                                         select row).ToList();

                if (UserNotifications != null)
                    return UserNotifications.Count;
                else
                    return 0;
            }
        }
        public static int GetUnreadNotificationsCount(int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UnreadNotifications = (from UN in entities.UserNotifications
                                           join N in entities.Notifications
                                           on UN.NotificationID equals N.ID
                                           where UN.UserID == userid
                                           && UN.IsRead == false
                                           && N.Type != "DailyUpdate"
                                           select UN).Count();

                return UnreadNotifications;
            }
        }
        

        public static List<String> GetNewUserNotification(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<String> returnNotificationList = new List<string>();

                var UserNotifications = (from row in entities.Notifications
                                         join row1 in entities.UserNotifications
                                         on row.ID equals row1.NotificationID
                                         where row1.UserID == UserID
                                         && row1.IsRead == false
                                         && row.Type != "DailyUpdate"
                                         select row).ToList();

                if (UserNotifications.Count > 0)
                {
                    var TypeList = UserNotifications.GroupBy(p => p.Type).Select(g => g.First()).Select(h => h.Type).ToList();

                    TypeList.ForEach(EachType =>
                    {
                        if (EachType != "Custom")
                        {
                            int Count = UserNotifications.Where(Entry => Entry.Type == EachType).Count();

                            if (Count > 0)
                            {
                                String MsgToAdd = String.Empty;

                                MsgToAdd = Count + " " + EachType + " " + "Details Updated";

                                returnNotificationList.Add(MsgToAdd);
                            }
                        }
                        else
                        {
                            String MsgToAdd = String.Empty;

                            MsgToAdd = "Welcome to a completely redesigned version of our Product. If you wish to learn more, please visit our Help Centre for detailed help and videos.";

                            returnNotificationList.Add(MsgToAdd);

                            List<long> NotificationsDetails = new List<long>();
                            NotificationsDetails.Add(1);

                            Business.ComplianceManagement.SetReadToNotification(NotificationsDetails, Convert.ToInt32(UserID));
                        }
                    });

                }

                return returnNotificationList;
            }
        }

        public static bool ExistsNotification(Notification newNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Notifications
                             where row.ActID == newNotification.ActID
                             && row.ComplianceID == newNotification.ComplianceID
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool ExistsUserNotification(UserNotification newNotification)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.UserNotifications
                             where row.NotificationID == newNotification.NotificationID
                             && row.UserID == newNotification.UserID
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static void SetReadToNotification(List<long> Notifications, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                foreach (var eachNotification in Notifications)
                {
                    var NotificationDetails = (from row in entities.UserNotifications
                                               where row.NotificationID == eachNotification
                                               && row.UserID == userid
                                               select row).FirstOrDefault();

                    if (NotificationDetails != null)
                    {
                        NotificationDetails.IsRead = true;
                    }
                }

                entities.SaveChanges();
            }
        }

        public static void CreateChangeLogs(List<ChangeLog> ChangeLogList)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ChangeLogList.ForEach(entry =>
                {
                    entities.ChangeLogs.Add(entry);
                    entities.SaveChanges();
                });
            }
        }


        public static bool ExistsComplianceReminder(ComplianceReminder CR)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.ComplianceReminders
                             where row.ComplianceAssignmentID == CR.ComplianceAssignmentID
                             && row.ComplianceDueDate == CR.ComplianceDueDate
                             && row.RemindOn == CR.RemindOn
                             select row).FirstOrDefault();
                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        public static bool ExistsInternalComplianceReminder(InternalComplianceReminder CR)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.InternalComplianceReminders
                             where row.ComplianceAssignmentID == CR.ComplianceAssignmentID
                             && row.ComplianceDueDate == CR.ComplianceDueDate
                             && row.RemindOn == CR.RemindOn
                             select row).FirstOrDefault();
                if (query != null)
                    return true;
                else
                    return false;
            }
        }
        public static List<sp_ComplianceInstanceAssignmentViewDetails_Result> GetComplianceDetailsDashboard(int customerid, List<long> Branchlist, int RoleID, int CustomerBranchID, int ActId, int CategoryID, int Userid, bool IsApprover = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<sp_ComplianceInstanceAssignmentViewDetails_Result>();

               

                if (IsApprover == true)
                {
                    ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails(Userid, "APPR")                                          
                                           select row).ToList();

                }
                else
                {
                    ComplianceDetails = (from row in entities.sp_ComplianceInstanceAssignmentViewDetails(Userid, "MGMT")
                                         select row).ToList();

                   
                }

               

                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }
                if (RoleID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.RoleID == RoleID).Distinct().ToList();
                }
                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }
                
                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }

        //Added By Amita as on 4 Jan 2019
        public static List<sp_CompliancesforMappedLocation_Result> TempGetByMappedLocation(int lcMappedCustomerBranch = -1, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {              
                List<sp_CompliancesforMappedLocation_Result> compliances = (
                    from row in entities.sp_CompliancesforMappedLocation(lcMappedCustomerBranch)
                                   select row).ToList();

                if (!string.IsNullOrEmpty(filter))
                    compliances = compliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();

                compliances.OrderBy(entry => entry.ID);

                return compliances;
            }
        }

        //Added By Amita as on 7 Jan 2019
        public static List<ComplianceAsignmentProperties> RemoveMappedCompliances(List<ComplianceAsignmentProperties> complianceList, int lcCustomerMappingBranch = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (lcCustomerMappingBranch != -1)
                {
                    #region start - for comptype=0 or 2 statutory
                    var TempStatQuery = (from row in entities.TempAssignmentTables                                        
                                         where row.CustomerBranchID == lcCustomerMappingBranch
                                         select row.ComplianceId).Distinct().ToList();

                    if(TempStatQuery != null)
                        complianceList.RemoveAll(a => TempStatQuery.Contains(a.ComplianceId));
                    #endregion

                    #region start - for comptype=1 checklist               
                    var TempChkLstQuery = (from row in entities.TempAssignmentTableCheckLists                                           
                                           where row.CustomerBranchID == lcCustomerMappingBranch
                                           select row.ComplianceId).Distinct().ToList();

                    if(TempChkLstQuery != null)
                        complianceList.RemoveAll(a => TempChkLstQuery.Contains(a.ComplianceId));
                    #endregion
                }

                return complianceList;
            }
        }

        public static Act GetStartDateFrAct(int actID)
        {           
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Act act = (from row in entities.Acts
                                       where row.ID == actID
                                       select row).FirstOrDefault();

                return act;
            }            
        }

        public static object ComplianceOldData(string frequency, string duedate, string compliancetype)
        {

            string data = "Frequency=" + frequency + ",duedate=" + duedate + ",compliancetype=" + compliancetype;

            return data;
        }
        public static object ComplianceNewData(string frequency, string duedate, string compliancetype)
        {

            string data = "Frequency=" + frequency + ",duedate=" + duedate + ",compliancetype=" + compliancetype;

            return data;
        }
        //Create Product Mapping 
        public static void CreateProductMapping(ProductMapping IndustryMapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ProductMappings.Add(IndustryMapping);
                entities.SaveChanges();
            }
        }
        //Remove Product Mapping
        public static void UpdateProductMapping(long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ids = (from row in entities.ProductMappings
                           where row.CustomerID == customerid
                           select row.Id).ToList();
                ids.ForEach(entry =>
                {
                    var prevmappedids = (from row in entities.ProductMappings
                                         where row.Id == entry
                                         select row).FirstOrDefault();
                    entities.ProductMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }
        }

       

        public static bool ChangeScheduleOn_Secretarial(long scheduleOnID, DateTime NewscheduleOnDate)
        {
            bool saveSuccess = false;

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.BM_ComplianceScheduleOnNew
                                      where row.ScheduleOnID == scheduleOnID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.ScheduleOn = NewscheduleOnDate;
                        entities.SaveChanges();
                        
                        saveSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                saveSuccess = false;
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return saveSuccess;
        }

        public static bool GetLogDetails(int ComplianceID, int ComplianceInstanceID, int SID, int CustID)
        {
            bool ans = true;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn obj1 = (from row in entities.ComplianceScheduleOns
                                             where row.ID == SID
                                                      && row.IsActive == true
                                                      && row.ComplianceInstanceID == ComplianceInstanceID
                                             select row).FirstOrDefault();
                if (obj1 != null)
                {
                    DateTime ScheduledOndt = Convert.ToDateTime(obj1.ScheduleOn);

                    var details = (from row in entities.CancelledStatutoryLogDetails
                                   where row.ComplianceInstanceID == ComplianceInstanceID
                                   && row.IsForDocument == true
                                   && row.IsActive == true
                                   select row).ToList();

                    if (details.Count > 0)
                    {
                        foreach (var item in details)
                        {
                            if (ScheduledOndt >= item.Statuschanged)
                            {
                                if (item.IsPerformer)
                                    ans = true;
                                else
                                    ans = false;
                            }
                        }
                    }
                    else
                    {
                        ComplianceInfoByCustomer details1 = (from row in entities.ComplianceInfoByCustomers
                                                             where row.CustomerID == CustID
                                                             && row.ComplianceID == ComplianceID
                                                             && row.IsActive == true
                                                             select row).FirstOrDefault();

                        if (details1 != null)
                        {
                            if (details1.IsDocumentCompulsory != null && details1.IsDocumentCompulsory == true)
                            {
                                ans = true;
                            }
                        }
                    }
                }
            }
            return ans;
        }
        public static CancelledStatutoryComplianceAssignment GetDetailsComplianceScheduled(int CompInstanceID, int ScheduleonID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var details = (from row in entities.CancelledStatutoryComplianceAssignments
                               where row.ComplianceInstanceID == CompInstanceID
                               && row.ScheduleonID == ScheduleonID
                               select row).FirstOrDefault();

                return details;
            }
        }

        public static List<SP_GetAllStatutoryInstanceLogDetail_Result> GetComplianceLogDetails(List<long> InstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetAllStatutoryInstanceLogDetail(0)
                                        select row).ToList();

                compliancesQuery = compliancesQuery.Where(x => InstanceId.Contains(x.ComplianceInstanceID)).ToList();

                return compliancesQuery;
            }
        }

        public static List<SP_GetAllStatutoryInstanceDetail_Result> GetComplianceScheduledOnDetails(int InstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                var compliancesQuery = (from row in entities.SP_GetAllStatutoryInstanceDetail(InstanceId)
                                        select row).ToList();

                return compliancesQuery;

            }
        }


        public static List<SP_GetUserNotificationsByUserID_New_Result> GetAllUserNotificationNew(long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var UserNotifications = (from row in entities.SP_GetUserNotificationsByUserID_New(Convert.ToInt32(UserID))
                                         select row).ToList();
                //if (UserNotifications.Count > 0)
                //{
                //    UserNotifications = UserNotifications.GroupBy(entry => entry.UpdatedOn).Select(entry => entry.FirstOrDefault()).ToList();
                //}
                return UserNotifications;
            }
        }

        public static RecentLicenseTransactionView GetCurrentStatusOfLicenseByScheduleOnID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusID = (from row in entities.RecentLicenseTransactionViews
                                       where row.ComplianceScheduleOnID == ScheduledOnID
                                       select row).FirstOrDefault();

                return currentStatusID;
            }
        }
    }
}
