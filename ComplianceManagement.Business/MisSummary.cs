﻿using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class MisSummary
    {
        public static void AddUpdateMisSummary(tbl_LegalCaseInstance obj)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(obj.ProvisioninBook)))
                    {
                        entities.sp_UpdateMISSummaryAndParticulars((int)obj.CustomerID, Convert.ToString(obj.CaseCategoryID), obj.Taxdemand, obj.Intrest, obj.Penalty, Convert.ToDecimal(obj.ProvisioninBook), obj.amountpaid, obj.favourable, obj.unfavourable, obj.state, obj.CreatedBy, (int)obj.ID);
                    }
                    else
                    {
                        entities.sp_UpdateMISSummaryAndParticulars((int)obj.CustomerID, Convert.ToString(obj.CaseCategoryID), obj.Taxdemand, obj.Intrest, obj.Penalty, 0, obj.amountpaid, obj.favourable, obj.unfavourable, obj.state, obj.CreatedBy, (int)obj.ID);
                    }
                }
            }
            catch
            {

            }
        }

    }
}
