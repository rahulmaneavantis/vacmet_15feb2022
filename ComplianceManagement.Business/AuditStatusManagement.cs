﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class AuditStatusManagement
    {
        public static List<AuditStatu> GetStatusList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.AuditStatus
                                  select row).ToList();

                return statusList;
            }
        }
        public static List<InternalAuditStatu> GetInternalStatusList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.InternalAuditStatus
                                  select row).ToList();

                return statusList;
            }
        }
        
        public static List<AuditStatusTransition> GetStatusTransitionList()
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transitionList = (from row in entities.AuditStatusTransitions
                                      select row).ToList();

                return transitionList;
            }
        }



        public static List<InternalAuditStatusTransition> GetInternalAuditStatusTransitionListByInitialId(int initialId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transitionList = (from row in entities.InternalAuditStatusTransitions
                                      select row);

                if (initialId != 0)
                {
                    transitionList = transitionList.Where(row => row.InitialStateID == initialId);
                }

                return transitionList.ToList();
            }
        }
        public static List<AuditStatusTransition> GetStatusTransitionListByInitialId(int initialId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transitionList = (from row in entities.AuditStatusTransitions
                                      select row);

                if (initialId != 0)
                {
                    transitionList = transitionList.Where(row => row.InitialStateID == initialId);
                }

                return transitionList.ToList();
            }
        }
    }
}
