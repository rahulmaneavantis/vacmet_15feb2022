﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using EntityFramework.BulkInsert.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CityStateCountryManagement
    {
        public static bool CityExist(string name)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Cities
                             where row.Name.Equals(name)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateCity(City objCity)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Cities.Add(objCity);
                entities.SaveChanges();
            }
        }

        public static void UpdateCity(City objCity)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                City mstdeptMasterToUpdate = (from row in entities.Cities
                                                  where row.ID == objCity.ID
                                                  select row).FirstOrDefault();

                mstdeptMasterToUpdate.Name = objCity.Name;
                mstdeptMasterToUpdate.StateId = objCity.StateId;
                entities.SaveChanges();
            }
        }

        public static List<View_CountryStateCity> GetAllcity()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var CityList = (from row in entities.View_CountryStateCity
                                where row.CityName != null
                                && row.CityIsDeleted == false
                                select row);
                return CityList.Distinct().ToList();
            }
        }
        
        public static City CityGetById(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Citymaster = (from row in entities.Cities
                                  where row.ID == iD
                                  select row).SingleOrDefault();

                return Citymaster;
            }
        }

        public static void DeleteCity(int cityID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                City CityDelete = (from row in entities.Cities
                                       where row.ID == cityID
                                       select row).FirstOrDefault();

                entities.Cities.Remove(CityDelete);
                entities.SaveChanges();
            }
        }


        //State
        //public static List<View_CountryStateCity> BindAllState()
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var StateList = (from row in entities.View_CountryStateCity
        //                         where row.StateName != null
        //                         && row.StateIsDeleted == false
        //                         select new
        //                         {
        //                             row.StateID,
        //                             row.StateName,
        //                             row.CountryName,
        //                         });
        //        return StateList.ToList();
        //       // return StateList.OrderBy(entry => entry.StateName).ToList();
        //    }
       // }
        public static List<State> GetAllState()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var StateList = (from row in entities.States
                                 where row.Name != null
                                 && row.IsDeleted == false
                                 select row);
                return StateList.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        public static bool StateExist(string name)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.States
                             where row.Name.Equals(name)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateState(State objState)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.States.Add(objState);
                entities.SaveChanges();
            }
        }

        public static void UpdateState(State objState)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                State updatestates = (from row in entities.States
                                          where row.ID == objState.ID
                                          select row).FirstOrDefault();

                updatestates.Name = objState.Name;
                updatestates.CountryID = objState.CountryID;
                entities.SaveChanges();
            }
        }

        public static State StateGetByID(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var StateMaster = (from row in entities.States
                                   where row.ID == iD
                                   select row).SingleOrDefault();

                return StateMaster;
            }
        }

        public static void DeleteState(int iD)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                State StateDalete = (from row in entities.States
                                         where row.ID == iD
                                         select row).FirstOrDefault();
                StateDalete.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        //country

        public static List<Country> GetAllContryData()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.Countries
                                  where row.Name != null && row.IsDeleted == false
                                  select row);
                return ObjCountry.ToList();
            }
        }

        public static Country GetCountryDetailByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.Countries
                                  where row.ID == ID
                                  select row).SingleOrDefault();
                return ObjCountry;
            }
        }

        public static void DeleteCountryDetail(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Country ObjCountry = (from row in entities.Countries
                                      where row.ID == ID
                                      select row).FirstOrDefault();
                
                ObjCountry.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static bool CheckCountryExist(string name)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ObjCountry = (from row in entities.Countries
                                  where row.Name.Equals(name)
                                  select row);
                return ObjCountry.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateContryDetails(Country objCountry)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Countries.Add(objCountry);
                entities.SaveChanges();
            }
        }

        public static void UpdateCountryDetails(Country objCountry)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Country updatestates = (from row in entities.Countries
                                        where row.ID == objCountry.ID
                                        select row).FirstOrDefault();

                updatestates.Name = objCountry.Name;
                entities.SaveChanges();
            }
        }

        public static object GetAllStateCountryWise(int countryID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var StateList = (from row in entities.States
                                 where row.Name != null
                                 && row.IsDeleted == false &&
                                 row.CountryID==countryID
                                 select row);
                return StateList.OrderBy(entry => entry.Name).ToList();
            }
        }
    }
}
