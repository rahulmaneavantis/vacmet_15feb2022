﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Business.License
{
    public class LicenseTypeMasterManagement
    {
        #region Statutory

        public static List<LIC_StatutoryInternalEntitiesAssignment_Result> GetLicenseTypeCustomerwise(int cusid, string flag)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.LIC_StatutoryInternalEntitiesAssignment(cusid, flag)
                             select row);
                return users.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<Sp_BindLicenseTypeInternal_Result> GetInternalLicenseTypeWiseAssinedUser(int userID, string role, string Is_StatutoryInternal,int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                if (customerid != -1)
                {
                    var users = (from row in entities.Sp_BindLicenseTypeInternal(userID, role, Is_StatutoryInternal)
                                 where row.CustomerID == customerid
                                 select row).ToList();
                    return users.OrderBy(entry => entry.Name).ToList();
                    // users = users.Where(entry => entry.CustomerID.Equals(customerid));
                }
                else
                {
                    var users = (from row in entities.Sp_BindLicenseTypeInternal(userID, role, Is_StatutoryInternal)
                                 select row).ToList();
                    return users.OrderBy(entry => entry.Name).ToList();
                }



            }
        }
        public static List<Sp_BindLicenseType_Result> GetLicenseTypeWiseAssinedUser(int userID,string role,string Is_StatutoryInternal)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
               
                var users = (from LTLCIM in entities.Sp_BindLicenseType(userID, role, Is_StatutoryInternal) 
                             select LTLCIM);
                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        public static List<Lic_tbl_LicenseType_Master> GetLicenseType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Lic_tbl_LicenseType_Master
                             where row.IsDeleted == false
                             && row.Is_Statutory_NoStatutory == "S"
                             select row);
                return users.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static bool CheckUserwiseLicenseTypeAssignedorNot(int userID,string isSorI)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var users = (from row in entities.Lic_tbl_UserAndLicenseTypeMapping
                                 where row.IsActive == true
                                 && row.Is_Statutory_NoStatutory == isSorI
                                 && row.UserID == userID
                                 select row).ToList();
                    if (users.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }            
        }

        public static List<Lic_tbl_LicenseType_Master> GetUserWiseLicenseType(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Lic_tbl_LicenseType_Master
                             join row1 in entities.Lic_tbl_UserAndLicenseTypeMapping
                             on row.ID equals row1.LicenseTypeID
                             where row.IsDeleted == false
                             && row1.IsActive==true
                             && row1.Is_Statutory_NoStatutory =="S"
                             && row1.UserID== userID
                             select row);
                return users.OrderBy(entry => entry.Name).ToList();
            }
        }        
        public class LicenseComplianceMap
        {
            public long LicenseTypeId { get; set; }
            public string KeyID { get; set; }
            public long ComplianceID { get; set; }
            public string ShortDescription { get; set; }
            public string LicenseTypeName { get; set; }
            public string Type { get; set; }

        }
        public static void Create(Lic_tbl_LicenseType_Master ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Lic_tbl_LicenseType_Master.Add(ltlm);
                entities.SaveChanges();
            }
        }
        public static bool Exists(Lic_tbl_LicenseType_Master ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseType_Master
                             where row.IsDeleted == false && row.Name.ToUpper().Trim().Equals(ltlm.Name.ToUpper().Trim())
                             select row);

                if (ltlm.ID > 0)
                {
                    query = query.Where(entry => entry.ID != ltlm.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }      
        public static void Update(Lic_tbl_LicenseType_Master ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Lic_tbl_LicenseType_Master ltlmToUpdate = (from row in entities.Lic_tbl_LicenseType_Master
                                                           where row.ID == ltlm.ID
                                                           select row).FirstOrDefault();

                ltlmToUpdate.Name = ltlm.Name.Trim();
                ltlmToUpdate.UpdatedBy = ltlm.UpdatedBy;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static List<Lic_tbl_LicenseType_Master> GetAll(string is_st_nst, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Ltltm = (from row in entities.Lic_tbl_LicenseType_Master
                             where row.IsDeleted == false
                             select row);
                if (!string.IsNullOrEmpty(filter))
                {
                    Ltltm = Ltltm.Where(entry => entry.Name.Contains(filter));
                }
                Ltltm = Ltltm.Where(entry => entry.Is_Statutory_NoStatutory == is_st_nst);
                return Ltltm.ToList();
            }
        }
        public static Lic_tbl_LicenseType_Master GetByID(int licenseId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.Lic_tbl_LicenseType_Master
                           where row.ID == licenseId && row.IsDeleted == false
                           select row).SingleOrDefault();
                return act;
            }
        }
        public static void Delete(int licenseId, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Lic_tbl_LicenseType_Master ltlmToUpdate = (from row in entities.Lic_tbl_LicenseType_Master
                                                           where row.ID == licenseId
                                                           select row).FirstOrDefault();


                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.IsDeleted = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }        
        public static bool BlukLTLCMComplianceMapping(List<Lic_tbl_LicType_Compliance_Mapping> LTLCMlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    int Count = 0;
                    LTLCMlist.ForEach(EachCCM =>
                    {
                        Lic_tbl_LicType_Compliance_Mapping LTLCMComplianceMapping = (from row in entities.Lic_tbl_LicType_Compliance_Mapping
                                                                                     where row.ComplianceID == EachCCM.ComplianceID
                                                                                     && row.LicenseTypeID == EachCCM.LicenseTypeID                                                                                     
                                                                                     select row).FirstOrDefault();

                        if (LTLCMComplianceMapping != null)
                        {
                            LTLCMComplianceMapping.IsDeleted = false;
                            LTLCMComplianceMapping.UpdatedBy = EachCCM.UpdatedBy;
                            entities.SaveChanges();
                        }
                        else
                        {
                            Lic_tbl_LicType_Compliance_Mapping newLTLCMComplianceMapping = new Lic_tbl_LicType_Compliance_Mapping();
                            newLTLCMComplianceMapping.LicenseTypeID = EachCCM.LicenseTypeID;
                            newLTLCMComplianceMapping.ComplianceID = EachCCM.ComplianceID;
                            newLTLCMComplianceMapping.IsDeleted = EachCCM.IsDeleted;
                            newLTLCMComplianceMapping.CreatedBy = EachCCM.CreatedBy;
                            newLTLCMComplianceMapping.CreatedOn = DateTime.Now;                            
                            entities.Lic_tbl_LicType_Compliance_Mapping.Add(newLTLCMComplianceMapping);
                            Count++;
                        }
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static List<LicenseComplianceMap> GetAllLicenseMappingCompliance(int LicenseTypeID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var licensemapping = (from C in entities.Compliances
                                      join ltlcm in entities.Lic_tbl_LicType_Compliance_Mapping
                                      on C.ID equals ltlcm.ComplianceID
                                      join ltlm in entities.Lic_tbl_LicenseType_Master
                                      on ltlcm.LicenseTypeID equals ltlm.ID
                                      where ltlcm.IsDeleted==false && ltlm.IsDeleted == false                                      
                                      select new LicenseComplianceMap()
                                      {
                                          ComplianceID = C.ID,
                                          LicenseTypeId = ltlcm.LicenseTypeID,
                                          ShortDescription = C.ShortDescription,
                                          LicenseTypeName = ltlm.Name,
                                          Type = C.EventFlag == null ? "Statutory" :
                                                    C.EventFlag == true ? "EventBased" : "",
                                      });
                if (LicenseTypeID != -1)
                {
                    licensemapping = licensemapping.Where(entry => entry.LicenseTypeId == LicenseTypeID);
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    licensemapping = licensemapping.Where(entry => entry.ShortDescription.Contains(filter)
                    || entry.LicenseTypeName.Contains(filter));
                }

                return licensemapping.OrderBy(entry => entry.LicenseTypeName).ToList();
            }
        }
        public static List<long> GetLicenseTypeCompliance(int LicenseTypeID,string issi, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var licensecompliancemappinglist = (from ltlcm in entities.Lic_tbl_LicType_Compliance_Mapping
                                                    where ltlcm.LicenseTypeID == LicenseTypeID && ltlcm.IsDeleted==false                                                    
                                                    select ltlcm.ComplianceID).ToList();
               
                return licensecompliancemappinglist.ToList();
            }
        }
        #endregion

        #region Internal
        public static List<InternalComplianceType> GetInternalcompliancetypeAll(int customerid,string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Ltltm = (from row in entities.InternalComplianceTypes                             
                             where row.CustomerID == customerid
                             && row.IsDeleted == false
                             select row);
                if (!string.IsNullOrEmpty(filter))
                {
                    Ltltm = Ltltm.Where(entry => entry.Name.Contains(filter));
                }
                
                return Ltltm.ToList();
            }
        }

        public static List<long> GetInternalcomplianceList(long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Ltltm = (from row in entities.InternalCompliances
                             where row.CustomerID == customerid
                             select row.ID);                
                return Ltltm.ToList();
            }
        }
        public static bool BlukLTLCMInternalComplianceMapping(List<Lic_tbl_LicType_InternalCompliance_Mapping> LTLCMlist)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    int Count = 0;
                    LTLCMlist.ForEach(EachCCM =>
                    {
                        var LTLCMComplianceMapping = (from row in entities.Lic_tbl_LicType_InternalCompliance_Mapping
                        where row.ComplianceID == EachCCM.ComplianceID
                        && row.LicenseTypeID == EachCCM.LicenseTypeID
                        && row.CustomerID==EachCCM.CustomerID
                        select row).FirstOrDefault();

                        if (LTLCMComplianceMapping != null)
                        {
                            LTLCMComplianceMapping.IsDeleted = false;
                            LTLCMComplianceMapping.UpdatedBy = EachCCM.UpdatedBy;
                            entities.SaveChanges();
                        }
                        else
                        {
                            Lic_tbl_LicType_InternalCompliance_Mapping newLTLCMComplianceMapping = new Lic_tbl_LicType_InternalCompliance_Mapping();
                            newLTLCMComplianceMapping.LicenseTypeID = EachCCM.LicenseTypeID;
                            newLTLCMComplianceMapping.ComplianceID = EachCCM.ComplianceID;
                            newLTLCMComplianceMapping.IsDeleted = EachCCM.IsDeleted;
                            newLTLCMComplianceMapping.CreatedBy = EachCCM.CreatedBy;
                            newLTLCMComplianceMapping.CreatedOn = DateTime.Now;
                            newLTLCMComplianceMapping.CustomerID = EachCCM.CustomerID;
                            entities.Lic_tbl_LicType_InternalCompliance_Mapping.Add(newLTLCMComplianceMapping);
                            Count++;
                        }
                        if (Count >= 100)
                        {
                            entities.SaveChanges();
                            Count = 0;
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static List<Lic_tbl_InternalLicenseType_Master> GetAssignedInternalLicenseType(long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Lic_tbl_InternalLicenseType_Master
                             join row1 in entities.InternalComplianceTypes
                             on row.Name equals row1.Name
                             where row.IsDeleted == false    
                             && row.CustomerID== customerid 
                             && row1.Is_License == true
                             && row1.IsDeleted == false
                             select row);              
                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        public static List<InternalComplianceType> GetInternalLicenseType(long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.InternalComplianceTypes
                             where row.IsDeleted == false
                             && row.CustomerID == customerid
                             && row.Is_License == true
                             && row.IsDeleted == false
                             select row);
                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        public static List<Lic_tbl_InternalLicenseType_Master> GetUserWiseInternalLicenseType(int userID,int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Lic_tbl_InternalLicenseType_Master
                             join row1 in entities.Lic_tbl_UserAndLicenseTypeMapping
                             on row.ID equals row1.LicenseTypeID
                             join row2 in entities.InternalComplianceTypes
                             on row.CustomerID equals (long) row2.CustomerID
                             where row.IsDeleted == false
                             && row1.IsActive == true                             
                             && row1.UserID == userID
                             && row1.Is_Statutory_NoStatutory == "I"
                             && row2.Is_License == true
                             && row2.IsDeleted == false
                             select row);
                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }

        public static void CreateInternal(Lic_tbl_InternalLicenseType_Master ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Lic_tbl_InternalLicenseType_Master.Add(ltlm);
                entities.SaveChanges();
            }
        }
        public static bool Exists(Lic_tbl_InternalLicenseType_Master ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_InternalLicenseType_Master
                             where
                             row.ID.Equals(ltlm.ID) &&
                             row.CustomerID == ltlm.CustomerID &&
                             row.IsDeleted == false
                           //  && row.Name.ToUpper().Trim().Equals(ltlm.Name.ToUpper().Trim()
                             select row);

                if (ltlm.ID > 0)
                {
                    query = query.Where(entry => entry.ID != ltlm.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void Update(Lic_tbl_InternalLicenseType_Master ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ltlmToUpdate = (from row in entities.Lic_tbl_InternalLicenseType_Master
                                    where row.CustomerID == ltlm.CustomerID 
                                    && row.IsDeleted == false 
                                    && row.ID == ltlm.ID  
                                    select row).FirstOrDefault();

                ltlmToUpdate.Name = ltlm.Name.Trim();
                ltlmToUpdate.UpdatedBy = ltlm.UpdatedBy;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                ltlmToUpdate.IsDeleted = ltlm.IsDeleted;
                entities.SaveChanges();
            }
        }
        #endregion
    }
}
