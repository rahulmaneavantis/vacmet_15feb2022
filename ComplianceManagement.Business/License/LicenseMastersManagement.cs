﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Business.License
{
    public class LicenseMastersManagement
    {
       
        #region User master
        public static List<Role> GetLicenseRoles(bool? onlyForLitigation = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.IsForLicense == true && row.ID < 14
                             select row);

                if (onlyForLitigation.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForLitigation == onlyForLitigation.Value);
                }
                return roles.ToList();
            }
        }
        public static List<User> GetAllUser(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Users
                             where row.IsDeleted == false && row.CustomerID == customerID
                             //&& row.LicenseRoleID > 0
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }
        public static bool UpdateUserLicense_Compliance(User user, List<UserParameterValue> parameters)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User userToUpdate = (from row in entities.Users
                                     where row.ID == user.ID
                                     select row).FirstOrDefault();
                if (userToUpdate != null)
                {
                    userToUpdate.IsHead = user.IsHead;
                    userToUpdate.FirstName = user.FirstName;
                    userToUpdate.LastName = user.LastName;
                    userToUpdate.Designation = user.Designation;
                    userToUpdate.Email = user.Email;
                    userToUpdate.ContactNumber = user.ContactNumber;
                    userToUpdate.Address = user.Address;
                    userToUpdate.CustomerID = user.CustomerID;
                    userToUpdate.CustomerBranchID = user.CustomerBranchID;
                    userToUpdate.ReportingToID = user.ReportingToID;
                    userToUpdate.DepartmentID = user.DepartmentID;
                    userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;

                    //userToUpdate.LicenseRoleID = user.LicenseRoleID;

                    userToUpdate.LicenseRoleID = user.LicenseRoleID;
                    //userToUpdate.LawyerFirmID = user.LawyerFirmID;
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.AuditorID = user.AuditorID;
                    userToUpdate.IsExternal = user.IsExternal;

                    List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                    var existingParameters = (from parameterRow in entities.UserParameterValues
                                              where parameterRow.UserId == user.ID
                                              select parameterRow).ToList();

                    existingParameters.ForEach(entry =>
                    {
                        if (parameterIDs.Contains(entry.ID))
                        {
                            UserParameterValue parameter = parameters.Find(param1 => param1.ID == entry.ID);
                            entry.Value = parameter.Value;
                        }
                        else
                        {
                            entities.UserParameterValues.Remove(entry);
                        }
                    });

                    parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValues.Add(entry));

                    entities.SaveChanges();
                }

                return true;
            }
        }

        public static bool UpdateUserLicense_Audit(mst_User user, List<UserParameterValue_Risk> parameters)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User userToUpdate = (from row in entities.mst_User
                                         where row.ID == user.ID
                                         select row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.IsHead = user.IsHead;
                    userToUpdate.FirstName = user.FirstName;
                    userToUpdate.LastName = user.LastName;
                    userToUpdate.Designation = user.Designation;
                    userToUpdate.Email = user.Email;
                    userToUpdate.ContactNumber = user.ContactNumber;
                    userToUpdate.Address = user.Address;
                    userToUpdate.CustomerID = user.CustomerID;
                    userToUpdate.CustomerBranchID = user.CustomerBranchID;
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.DepartmentID = user.DepartmentID;
                    userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;

                    userToUpdate.LicenseRoleID = user.LicenseRoleID;

                    //userToUpdate.LitigationRoleID = user.LitigationRoleID;
                    //userToUpdate.LawyerFirmID = user.LawyerFirmID;
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.AuditorID = user.AuditorID;
                    userToUpdate.IsExternal = user.IsExternal;

                    List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                    var existingParameters = (from parameterRow in entities.UserParameterValue_Risk
                                              where parameterRow.UserId == user.ID
                                              select parameterRow).ToList();

                    existingParameters.ForEach(entry =>
                    {
                        if (parameterIDs.Contains(entry.ID))
                        {
                            UserParameterValue_Risk parameter = parameters.Find(param1 => param1.ID == entry.ID);
                            entry.Value = parameter.Value;
                        }
                        else
                        {
                            entities.UserParameterValue_Risk.Remove(entry);
                        }
                    });

                    //parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValue_Risk.Add(entry));

                    entities.SaveChanges();
                }
                return true;
            }
        }
        #endregion        

        #region License Type Master
        public static List<Lic_tbl_LicenseType_Master> GetLicenseTypes_All(long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.Lic_tbl_LicenseType_Master
                             where 
                             //row.CustomerID == CustomerID
                             //&& 
                             row.IsDeleted == false
                             select row).OrderBy(entry => entry.Name);
                return Query.ToList();
            }
        }

        public static List<Lic_tbl_LicenseType_Master> GetLicenseTypeDetails_Paging(int CustomerID, string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Query = (from row in entities.Lic_tbl_LicenseType_Master
                             where 
                            // row.CustomerID == CustomerID 
                            // && 
                             row.IsDeleted == false
                             && row.Name.Contains(filter)
                             select row).ToList<Lic_tbl_LicenseType_Master>();
                return Query;
            }
        }


        public static bool ExistsLicenseTypeName(Lic_tbl_LicenseType_Master objLicenseType, long LicenseTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseType_Master
                             where row.Name.Equals(objLicenseType.Name)
                             //&& row.CustomerID == objLicenseType.CustomerID
                             && row.IsDeleted == false
                             select row);

                if (query != null)
                {
                    if (LicenseTypeID > 0)
                    {
                        query = query.Where(entry => entry.ID != LicenseTypeID);
                    }
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool CreateLicenseTypeData(Lic_tbl_LicenseType_Master objLicenseType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Lic_tbl_LicenseType_Master.Add(objLicenseType);
                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static Lic_tbl_LicenseType_Master GetLicenseTypeDetailsByID(long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objLicenseTypeDetails = (from row in entities.Lic_tbl_LicenseType_Master
                                             where row.ID == ID
                                             select row).FirstOrDefault();
                return objLicenseTypeDetails;
            }
        }

        public static long GetLicenseTypeIDByName(List<Lic_tbl_LicenseType_Master> masterlstExistingLicenseTypes, string LicenseType)
        {
            try
            {
                var LicenseTypeID = (from row in masterlstExistingLicenseTypes
                                     where row.Name.Trim().ToUpper() == LicenseType.Trim().ToUpper()
                                     && row.IsDeleted == false
                                     select row).FirstOrDefault();

                return Convert.ToInt32(LicenseTypeID.ID);
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }


        public static bool UpdateLicenseTypeData(Lic_tbl_LicenseType_Master objLicenseType)
        {
            bool updateSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Lic_tbl_LicenseType_Master ObjQuery = (from row in entities.Lic_tbl_LicenseType_Master
                                                           where row.ID == objLicenseType.ID
                                                // && row.CustomerID == objLicenseType.CustomerID
                                                           select row).FirstOrDefault();
                    if (ObjQuery != null)
                    {
                        ObjQuery.Name = objLicenseType.Name;
                        //ObjQuery.ActID = objLicenseType.ActID;
                        //ObjQuery.ComplianceID = objLicenseType.ComplianceID;
                        //ObjQuery.RegulatorID = objLicenseType.RegulatorID;
                        //ObjQuery.IsSoftwareLicenseChecked = objLicenseType.IsSoftwareLicenseChecked;
                        //ObjQuery.NoOfLicenseProcured = objLicenseType.NoOfLicenseProcured;
                        //ObjQuery.NoOfLicenseUtilized = objLicenseType.NoOfLicenseUtilized;
                        //ObjQuery.RemainingLicenseBalance = objLicenseType.RemainingLicenseBalance;
                        ObjQuery.UpdatedBy = objLicenseType.UpdatedBy;
                        ObjQuery.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return false;
            }
        }

        public static bool DeleteLicenseType(long licensetypeID, int customerID)
        {
            bool deleteSuccess = false;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var objRecord = (from row in entities.Lic_tbl_LicenseType_Master
                                     where 
                                     row.ID == licensetypeID
                                     //&& 
                                     //row.CustomerID == customerID
                                     select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        #endregion

        #region License-Custom Field



        public static string GetDepartmenID()
        {
            string depaId = string.Empty;
            HttpContext context = HttpContext.Current;
            if (context.Session["DID"].ToString() != null)
            {
                depaId = context.Session["DID"].ToString();
            }
            return depaId;
        }


















        #endregion

        #region License Status 
        public static int GetLicenseStatusIDByName(List<Lic_tbl_StatusMaster> masterlstExistingStatus, string licenseStatusName)
        {
            try
            {

                var LicenseStatusID = (from row in masterlstExistingStatus
                                       where row.StatusName.Trim().ToUpper().Equals(licenseStatusName.Trim().ToUpper())
                                              && row.IsDeleted == false
                                       select row.ID).FirstOrDefault();

                return Convert.ToInt32(LicenseStatusID);
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }

        }

        public static bool ExistsLicenseStatus(List<Lic_tbl_LicenseInstance> masterlstExistingLicenses, List<Lic_tbl_LicenseStatusTransaction> masterlstExistingLicenseStatusTransaction, List<Lic_tbl_StatusMaster> masterlstExistingStatus, int customerID, string licenseNo, string licenseStatusName)
        {

            var query = (from row in masterlstExistingLicenses
                         where row.LicenseNo.Trim().ToUpper() == licenseNo.Trim().ToUpper()
                         && row.CustomerID == customerID
                         && row.IsDeleted == false
                         join statusTrans in masterlstExistingLicenseStatusTransaction
                         on row.ID equals statusTrans.LicenseID
                         where statusTrans.IsActive == false && statusTrans.CustomerID == row.CustomerID
                         join statusMst in masterlstExistingStatus
                         on statusTrans.StatusID equals statusMst.ID
                         where statusMst.IsDeleted == false
                         && statusMst.StatusName.Trim().ToUpper() == licenseStatusName.Trim().ToUpper()
                         select row);

            return query.Select(entry => true).SingleOrDefault();

        }
        #endregion

        #region License Instance

        public static List<Lic_tbl_LicenseInstance> GetLicenseDetails_Paging(int CustomerID, string filter)
        {
            List<Lic_tbl_LicenseInstance> Query = new List<Lic_tbl_LicenseInstance>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Query = (from row in entities.Lic_tbl_LicenseInstance
                                 where row.CustomerID == CustomerID && row.IsDeleted == false
                                 && row.LicenseNo.Contains(filter)
                                 select row).ToList<Lic_tbl_LicenseInstance>();
                    return Query;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
                return Query;
            }
        }


        public static bool ExistsLicenseName(Lic_tbl_LicenseType_Master objLicenseType, long LicenseTypeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseType_Master
                             where row.Name.Equals(objLicenseType.Name)
                             //&& row.CustomerID == objLicenseType.CustomerID
                             && row.IsDeleted == false
                             select row);

                if (query != null)
                {
                    if (LicenseTypeID > 0)
                    {
                        query = query.Where(entry => entry.ID != LicenseTypeID);
                    }
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool CreateLicenseData(Lic_tbl_LicenseType_Master objLicenseType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Lic_tbl_LicenseType_Master.Add(objLicenseType);
                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static Lic_tbl_LicenseType_Master GetLicenseDetailsByID(long ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objLicenseTypeDetails = (from row in entities.Lic_tbl_LicenseType_Master
                                             where row.ID == ID
                                             select row).FirstOrDefault();
                return objLicenseTypeDetails;
            }
        }

        public static long GetLicenseIDByName(List<Lic_tbl_LicenseType_Master> masterlstExistingLicenseTypes, string LicenseType)
        {
            try
            {
                var LicenseTypeID = (from row in masterlstExistingLicenseTypes
                                     where row.Name.Trim().ToUpper() == LicenseType.Trim().ToUpper()
                                     && row.IsDeleted == false
                                     select row).FirstOrDefault();

                return Convert.ToInt32(LicenseTypeID.ID);
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }


        public static bool UpdateLicenseData(Lic_tbl_LicenseType_Master objLicenseType)
        {
            bool updateSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Lic_tbl_LicenseType_Master ObjQuery = (from row in entities.Lic_tbl_LicenseType_Master
                                                           where row.ID == objLicenseType.ID
                                                 //&& row.CustomerID == objLicenseType.CustomerID
                                                           select row).FirstOrDefault();
                    if (ObjQuery != null)
                    {
                        ObjQuery.Name = objLicenseType.Name;
                        //ObjQuery.ActID = objLicenseType.ActID;
                        //ObjQuery.ComplianceID = objLicenseType.ComplianceID;
                        //ObjQuery.RegulatorID = objLicenseType.RegulatorID;
                        //ObjQuery.IsSoftwareLicenseChecked = objLicenseType.IsSoftwareLicenseChecked;
                        //ObjQuery.NoOfLicenseProcured = objLicenseType.NoOfLicenseProcured;
                        //ObjQuery.NoOfLicenseUtilized = objLicenseType.NoOfLicenseUtilized;
                        //ObjQuery.RemainingLicenseBalance = objLicenseType.RemainingLicenseBalance;
                        ObjQuery.UpdatedBy = objLicenseType.UpdatedBy;
                        ObjQuery.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return false;
            }
        }

        public static bool DeleteLicense(long licensetypeID, int customerID)
        {
            bool deleteSuccess = false;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var objRecord = (from row in entities.Lic_tbl_LicenseType_Master
                                     where row.ID == licensetypeID
                                    // && row.CustomerID == customerID
                                     select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }
        #endregion
    }
}
