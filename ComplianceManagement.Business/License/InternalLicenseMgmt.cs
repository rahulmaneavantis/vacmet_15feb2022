﻿using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.License
{
    public class InternalLicenseMgmt
    {
        #region Internal
        public static List<long> GetLicenseScheduleonIdList(long customerID, long UserID, long RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping
                             join row1 in entities.Lic_tbl_InternalLicenseInstance
                             on row.LicenseID equals row1.ID
                             join row2 in entities.InternalComplianceAssignments
                           on row.ComplianceInstanceID equals row2.InternalComplianceInstanceID
                             where row1.CustomerID == customerID
                             && row2.UserID == UserID
                             && row2.RoleID == RoleID
                             select row.ComplianceScheduleOnID).ToList();

                return query.ToList();
            }
        }
        public static List<Lic_SP_GetTempActivatedInternalCompliances_All_Result> GetTempActivatedInternalCompliancesList(long customerID, int loggedInUserID, int roleID, List<int> branchList, int deptID, string isStatutory, long licenseTypeID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Lic_SP_GetTempActivatedInternalCompliances_All(customerID)
                                 select row).ToList();

                    if (query.Count > 0)
                    {
                        if (isStatutory != null)
                            query = query.Where(entry => entry.IsStatutory == isStatutory).ToList();

                        if (licenseTypeID != -1 && licenseTypeID != 0)
                            query = query.Where(entry => entry.LicensetypeID == licenseTypeID).ToList();

                        if (branchList.Count > 0)
                            query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                    }
                    return query.ToList();
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<SP_GetAuditLogInternalLicenseTransaction_Result> GetAllTransactionLog(long licenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.SP_GetAuditLogInternalLicenseTransaction(licenseID)
                                  select row).ToList();

                return statusList;
            }
        }
        public static Lic_tbl_InternalLicenseInstance GetInternalLicenseByID(long licenseID)
        {
            Lic_tbl_InternalLicenseInstance record = new Lic_tbl_InternalLicenseInstance();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    record = (from row in entities.Lic_tbl_InternalLicenseInstance
                              where row.ID == licenseID
                              select row).FirstOrDefault();
                    return record;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return record;
            }
        }
        public static List<long> GetLicenseScheduleonIdList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping
                             join row1 in entities.Lic_tbl_InternalLicenseInstance
                             on row.LicenseID equals row1.ID
                             select row.ComplianceScheduleOnID).ToList();

                return query.ToList();
            }
        }
        public static List<InternalComplianceType> GetInternalComplianceType(int CustomerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.InternalComplianceTypes
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter));
                }

                return users.ToList();
            }
        }
        public static List<Lic_SP_GetAssignedInternalCompliances_All_Result> GetAssignedInternalCompliancesList(long customerID, int loggedInUserID, int roleID, List<int> branchList, int deptID, string isStatutory, long licenseTypeID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Lic_SP_GetAssignedInternalCompliances_All(customerID)
                                 select row).ToList();

                    if (query.Count > 0)
                    {
                        if (isStatutory != null)
                            query = query.Where(entry => entry.IsStatutory == isStatutory).ToList();

                        if (licenseTypeID != -1 && licenseTypeID != 0)
                            query = query.Where(entry => entry.LicensetypeID == licenseTypeID).ToList();

                        if (branchList.Count > 0)
                            query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                    }
                    return query.ToList();
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static bool UpdateInternalTemplateAssignment(long complianceId, int customerbranchId)
        {
            bool result = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var recordToUpdate = (from row in entities.TempAssignmentTableInternals
                                      where row.ComplianceId == complianceId
                                      && row.CustomerBranchID == customerbranchId
                                      && row.IsActive == true
                                      && row.ComplianceFlag == "S"
                                      select row).ToList();

                if (recordToUpdate != null && recordToUpdate.Count > 0)
                {
                    if (recordToUpdate.Count == 2 || recordToUpdate.Count == 3)
                    {
                        recordToUpdate.ForEach(entry => entry.IsActive = false);
                        entities.SaveChanges();
                        result = true;

                    }
                    else
                    {
                        List<long> roles = new List<long>();
                        roles.Add(3);
                        roles.Add(4);
                        roles.Add(6);
                        foreach (var item in roles)
                        {
                            var recorddetails = (from row in entities.TempAssignmentTableInternals
                                                 where row.ComplianceId == complianceId
                                                 && row.CustomerBranchID == customerbranchId
                                                 && row.IsActive == true
                                                 && row.RoleID == item
                                                 && row.ComplianceFlag == "S"
                                                 select row).FirstOrDefault();
                            if (recorddetails != null)
                            {
                                recorddetails.IsActive = false;
                                entities.SaveChanges();
                                result = true;
                            }
                        }
                    }
                }
                return result;
            }
        }     
        public static long CreateInternalLicenseLog(Lic_tbl_InternalLicenseInstance_Log licenselogRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    licenselogRecord.CreatedOn = DateTime.Now;
                    entities.Lic_tbl_InternalLicenseInstance_Log.Add(licenselogRecord);
                    entities.SaveChanges();

                    return licenselogRecord.ID;
                }
                catch (Exception ex)
                {
                    LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }
        public static long CreateInternalLicense(Lic_tbl_InternalLicenseInstance licenseRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    licenseRecord.CreatedOn = DateTime.Now;
                    entities.Lic_tbl_InternalLicenseInstance.Add(licenseRecord);
                    entities.SaveChanges();

                    return licenseRecord.ID;
                }
                catch (Exception ex)
                {
                    LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return 0;
                }
            }
        }

        public static bool ExistsInternalLicenseNo(int customerID, string licenseNo)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Lic_tbl_InternalLicenseInstance
                                 where row.LicenseNo.Trim().ToUpper().Equals(licenseNo.Trim().ToUpper())
                                 && row.CustomerID == customerID
                                 && row.IsDeleted == false
                                 select row);
                    result = query.Select(entry => true).SingleOrDefault();

                }
                return result;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool CreateInternalLicenseStatusTransaction(Lic_tbl_InternalLicenseStatusTransaction objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    objNewStatusRecord.UpdatedOn = DateTime.Now;

                    entities.Lic_tbl_InternalLicenseStatusTransaction.Add(objNewStatusRecord);
                    entities.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool CreateInternalLicenseAuditLog(Lic_tbl_InternalLicenseAudit_Log objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    entities.Lic_tbl_InternalLicenseAudit_Log.Add(objNewStatusRecord);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static List<long> GetLicenseToInternalComplianceInstanceMappingList(long licenseId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<long> complianceIdsList = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
                                                where row.LicenseID == licenseId
                                                select row.ComplianceInstanceID).ToList();

                return complianceIdsList;
            }
        }
        public static long CreateInternalComplianceInstance(InternalComplianceInstance objNewComplianceInstanceRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.InternalComplianceInstances.Add(objNewComplianceInstanceRecord);
                    entities.SaveChanges();

                    return objNewComplianceInstanceRecord.ID;


                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static long CreateInternalComplianceAssignment(InternalComplianceAssignment objNewComplianceAssignmentsRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.InternalComplianceAssignments.Add(objNewComplianceAssignmentsRecord);
                    entities.SaveChanges();

                    return objNewComplianceAssignmentsRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static long CreateInternalComplianceScheduleOn(InternalComplianceScheduledOn objNewComplianceScheduleOnRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.InternalComplianceScheduledOns.Add(objNewComplianceScheduleOnRecord);
                    entities.SaveChanges();

                    return objNewComplianceScheduleOnRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static long CreateInternalComplianceTransaction(InternalComplianceTransaction objNewComplianceTransactionRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.InternalComplianceTransactions.Add(objNewComplianceTransactionRecord);
                    entities.SaveChanges();

                    return objNewComplianceTransactionRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static long CreateLicenseLicenseInternalComplianceInstanceScheduleOnMapping(Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping objNewLicComplianceInstMappingRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping.Add(objNewLicComplianceInstMappingRecord);
                    entities.SaveChanges();

                    return objNewLicComplianceInstMappingRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static long CreateLicenseInternalComplianceInstanceMapping(Lic_tbl_LicenseInternalComplianceInstanceMapping objNewLicComplianceInstMappingRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Lic_tbl_LicenseInternalComplianceInstanceMapping.Add(objNewLicComplianceInstMappingRecord);
                    entities.SaveChanges();

                    return objNewLicComplianceInstMappingRecord.ID;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static bool UploadInternalLicenseCreateDocuments(long complianceTransactionId, long complianceScheduleOnId, List<InternalFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, byte[]>> filelist,string DirectoryPath, int CustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustomerID);                            
                            if (AWSData != null)
                            {
                                AmazonS3.SaveDocFilesAWSStorageInternal(filelist, DirectoryPath, complianceScheduleOnId, AWSData.BucketName, AWSData.AccessKeyID, AWSData.SecretKeyID);
                            }
                            else
                            {
                                DocumentManagement.Internal_SaveDocFiles(filelist);
                            }
                            if (files != null)
                            {
                                foreach (InternalFileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.InternalFileDatas.Add(fl);
                                    entities.SaveChanges();

                                    InternalFileDataMapping fileMapping = new InternalFileDataMapping();
                                    fileMapping.TransactionID = complianceTransactionId;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.InternalScheduledOnID = complianceScheduleOnId;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.InternalFileDataMappings.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filelist != null)
                            {
                                foreach (var dfile in filelist)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
       
        
        public static LIC_PreviouslyInternalLicenseDetails_Result GetPreviousInternalLicenseDetail(long licenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var masterquery = (from row in entities.LIC_PreviouslyInternalLicenseDetails(licenseID)
                                   select row).FirstOrDefault();
                return masterquery;
            }
        }
        public static List<SP_GetInternalLicenseDocument_Result> GetInternalFileData(long LicenseID, long fileid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.SP_GetInternalLicenseDocument(LicenseID)
                                select row).ToList();

                if (fileid != -1)
                {
                    fileData = fileData.Where(entry => entry.FileID == fileid).ToList();
                }

                return fileData;
            }
        }
        public static Lic_tbl_InternalLicenseInstance GetLicense(long LicenseID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.Lic_tbl_InternalLicenseInstance
                                      where row.ID == LicenseID
                                      select row).FirstOrDefault();
                return ScheduleOnData;
            }
        }
        public static InternalCompliance GetInternalComplianceByInstanceID(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                long complianceInstanceID = (from row in entities.InternalComplianceTransactions
                                             where row.InternalComplianceScheduledOnID == ScheduledOnID
                                             select row.InternalComplianceInstanceID).FirstOrDefault();

                long complianceID = (from row in entities.InternalComplianceInstances
                                     where row.ID == complianceInstanceID && row.IsDeleted == false
                                     select row.InternalComplianceID).FirstOrDefault();

                var compliance = (from row in entities.InternalCompliances
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }
        public static InternalComplianceForm GetInternalComplianceFormByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.InternalComplianceForms
                            where row.IComplianceID == complianceID
                            select row).FirstOrDefault();

                return form;
            }
        }
        public static string GetRiskType(InternalCompliance compliance)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string risk = "";
                if (compliance.IRiskType == 0)
                    risk = "HIGH";
                else if (compliance.IRiskType == 1)
                    risk = "MEDIUM";
                else if (compliance.IRiskType == 2)
                    risk = "LOW";

                return risk;
            }
        }
        public static bool ExistsActivationDate(long LicenseID, DateTime enddate)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.SP_LIC_InternalCheckDateInBetween(LicenseID, enddate)
                                 select row);

                    result = query.Select(entry => true).SingleOrDefault();

                }
                return result;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool ExistsComplianceScheduleOn(long licenseID, DateTime scheduleon)
        {
            bool result = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping
                                 join row1 in entities.InternalComplianceScheduledOns
                                 on row.ComplianceInstanceID equals row1.InternalComplianceInstanceID
                                 where row.LicenseID == licenseID && row1.ScheduledOn == scheduleon
                                 select row);

                    result = query.Select(entry => true).SingleOrDefault();

                }
                return result;
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static bool UpdateLicenseNew(Lic_tbl_InternalLicenseInstance licenseRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordToUpdate = (from row in entities.Lic_tbl_InternalLicenseInstance
                                          where row.ID == licenseRecord.ID
                                          select row).FirstOrDefault();

                    if (recordToUpdate != null)
                    {
                        recordToUpdate.LicenseNo = licenseRecord.LicenseNo;
                        recordToUpdate.LicenseTitle = licenseRecord.LicenseTitle;
                        recordToUpdate.StartDate = licenseRecord.StartDate;
                        recordToUpdate.EndDate = licenseRecord.EndDate;
                        recordToUpdate.UpdatedBy = licenseRecord.UpdatedBy;
                        recordToUpdate.UpdatedOn = licenseRecord.UpdatedOn;
                        recordToUpdate.Cost = licenseRecord.Cost;
                        recordToUpdate.FileNO = licenseRecord.FileNO;
                        recordToUpdate.PhysicalLocation = licenseRecord.PhysicalLocation;

                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        public static bool UpdateLicense(Lic_tbl_InternalLicenseInstance licenseRecord)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordToUpdate = (from row in entities.Lic_tbl_InternalLicenseInstance
                                          where row.ID == licenseRecord.ID
                                          select row).FirstOrDefault();

                    if (recordToUpdate != null)
                    {
                        recordToUpdate.LicenseNo = licenseRecord.LicenseNo;
                        recordToUpdate.LicenseTitle = licenseRecord.LicenseTitle;
                        recordToUpdate.LicenseDetailDesc = licenseRecord.LicenseDetailDesc;
                        recordToUpdate.StartDate = licenseRecord.StartDate;
                        recordToUpdate.EndDate = licenseRecord.EndDate;
                        recordToUpdate.Cost = licenseRecord.Cost;
                        recordToUpdate.RemindBeforeNoOfDays = licenseRecord.RemindBeforeNoOfDays;
                        recordToUpdate.UpdatedBy = licenseRecord.UpdatedBy;
                        recordToUpdate.UpdatedOn = licenseRecord.UpdatedOn;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }
        public static Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping GetLicenseInternalComplianceInstanceScheduleOnByID(long licenseID)
        {
            Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping record = new Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    record = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceScheduleOnMapping
                              where row.LicenseID == licenseID
                              select row).OrderByDescending(x => x.ID).FirstOrDefault();
                    return record;
                }
            }
            catch (Exception ex)
            {
                LicenseMgmt.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return record;
            }
        }
        public class LicenseInternalComplianceMap
        {
            public long LicenseTypeId { get; set; }
            public string KeyID { get; set; }
            public long ComplianceID { get; set; }
            public string ShortDescription { get; set; }
            public string LicenseTypeName { get; set; }
            public string Type { get; set; }

        }
        public static List<LicenseInternalComplianceMap> GetAllLicenseMappingInternalCompliance(int LicenseTypeID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var licensemapping = (from C in entities.InternalCompliances
                                      join ltlcm in entities.Lic_tbl_LicType_InternalCompliance_Mapping
                                      on C.ID equals ltlcm.ComplianceID
                                      join ltlm in entities.Lic_tbl_InternalLicenseType_Master
                                      on ltlcm.LicenseTypeID equals ltlm.ID
                                      where ltlcm.IsDeleted == false && ltlm.IsDeleted == false
                                      select new LicenseInternalComplianceMap()
                                      {
                                          ComplianceID = C.ID,
                                          LicenseTypeId = ltlcm.LicenseTypeID,
                                          ShortDescription = C.IShortDescription,
                                          LicenseTypeName = ltlm.Name,
                                          Type = "Internal",
                                      });
                if (LicenseTypeID != -1)
                {
                    licensemapping = licensemapping.Where(entry => entry.LicenseTypeId == LicenseTypeID);
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    licensemapping = licensemapping.Where(entry => entry.ShortDescription.Contains(filter)
                    || entry.LicenseTypeName.Contains(filter));
                }

                return licensemapping.OrderBy(entry => entry.LicenseTypeName).ToList();
            }
        }
        #endregion
    }
}
