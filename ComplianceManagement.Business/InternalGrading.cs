﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class InternalGrading
    {
        public static bool FillInternalGrading(int Customerid, int UserID)
        {
            try
            {
                bool sucess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string isstatutoryinternal = "I";
                    var Masteryear = CommanGradingClass.BindYear();
                    var Mastercustomers = CustomerManagement.GetAll();
                    var masterUsers = CommanGradingClass.GetAllUserCustomerID(isstatutoryinternal,"NA");
                    Mastercustomers = Mastercustomers.Where(aa => aa == Customerid).ToList();
                    masterUsers = masterUsers.Where(a => a.CustomerID == Customerid && a.UserID == UserID).ToList();

                    List<SP_InternalGradingReport_Result> MasterTransactionQuery = new List<SP_InternalGradingReport_Result>();
                    List<SP_InternalGradingReport_Result> transactionsQuery = new List<SP_InternalGradingReport_Result>();

                    foreach (var custID in Mastercustomers)
                    {
                        List<GradingPreFillData> masterGradingList = new List<GradingPreFillData>();
                        MasterTransactionQuery = (entities.SP_InternalGradingReport(custID)).ToList();
                        var bracnhes = GetAllHierarchyManagementSatutory(custID);
                        var transaction = masterUsers.Where(entry => entry.CustomerID == custID).ToList();
                        foreach (var Citem in transaction)
                        {
                            var LocationList = CommanGradingClass.GetAssignedLocationList((int)Citem.UserID, custID, Citem.Role, isstatutoryinternal);
                            if (LocationList.Count > 0)
                            {
                                var dt = BindLocationFilterGraddingReport(bracnhes, LocationList, Citem.UserID);
                                List<string> columnList = new List<string>();
                                columnList.Add("C1");
                                columnList.Add("C2");
                                columnList.Add("C3");
                                columnList.Add("C4");
                                columnList.Add("C5");
                                columnList.Add("C6");
                                columnList.Add("C7");
                                columnList.Add("C8");
                                columnList.Add("C9");
                                columnList.Add("C10");
                                columnList.Add("C11");
                                columnList.Add("C12");

                                foreach (var year in Masteryear)
                                {
                                    DateTime startDate = new DateTime(year, 1, 1);
                                    DateTime EndDate = new DateTime(year, 12, 31);
                                    
                                    if (Citem.Role == "APPR")
                                    {
                                        transactionsQuery = (from row in MasterTransactionQuery
                                                             where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                                             && row.RoleID == 6 && row.UserID == Citem.UserID
                                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                                    }
                                    else
                                    {
                                        transactionsQuery = (from row in MasterTransactionQuery
                                                             join row2 in entities.EntitiesAssignmentInternals
                                                             on row.CustomerBranchID equals row2.BranchID
                                                             where row2.UserID == Citem.UserID
                                                             && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                                    }
                                    if (transactionsQuery.Count > 0)
                                    {
                                        foreach (DataRow DR in dt.Rows)
                                        {
                                            GradingPreFillData masterGrading = new GradingPreFillData();
                                            masterGrading.LocationName = Convert.ToString(DR[0]);
                                            masterGrading.Pnode = Convert.ToInt64(DR[2]);
                                            masterGrading.Cnode = Convert.ToInt64(DR[1]);
                                            masterGrading.UserID = Convert.ToInt64(DR[3]);
                                            masterGrading.CustomerID = custID;
                                            masterGrading.LocationID = Convert.ToInt64(DR[4]);
                                            masterGrading.IsStatutory = Convert.ToString(DR[5]);
                                            masterGrading.Year = year;

                                            foreach (var ColName in columnList)
                                            {
                                                #region 
                                                if (ColName == "C1")
                                                {
                                                    DateTime cDate = new DateTime(year, 1, 1);
                                                    masterGrading.C1 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C2")
                                                {
                                                    DateTime cDate = new DateTime(year, 2, 1);
                                                    masterGrading.C2 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C3")
                                                {
                                                    DateTime cDate = new DateTime(year, 3, 1);
                                                    masterGrading.C3 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C4")
                                                {
                                                    DateTime cDate = new DateTime(year, 4, 1);
                                                    masterGrading.C4 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C5")
                                                {
                                                    DateTime cDate = new DateTime(year, 5, 1);
                                                    masterGrading.C5 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C6")
                                                {
                                                    DateTime cDate = new DateTime(year, 6, 1);
                                                    var aa = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                    masterGrading.C6 = aa;
                                                }
                                                else if (ColName == "C7")
                                                {
                                                    DateTime cDate = new DateTime(year, 7, 1);
                                                    masterGrading.C7 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C8")
                                                {
                                                    DateTime cDate = new DateTime(year, 8, 1);
                                                    masterGrading.C8 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C9")
                                                {
                                                    DateTime cDate = new DateTime(year, 9, 1);
                                                    masterGrading.C9 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C10")
                                                {
                                                    DateTime cDate = new DateTime(year, 10, 1);
                                                    masterGrading.C10 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C11")
                                                {
                                                    DateTime cDate = new DateTime(year, 11, 1);
                                                    masterGrading.C11 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                else if (ColName == "C12")
                                                {
                                                    DateTime cDate = new DateTime(year, 12, 1);
                                                    masterGrading.C12 = BindingGradingTableRow(transactionsQuery, cDate, Convert.ToInt64(DR[4]), ColName, (long)Citem.CustomerID);
                                                }
                                                #endregion
                                            }//Coloumn list end
                                            masterGradingList.Add(masterGrading);
                                        }//DataRow End
                                    }//transactionsQuery.Count>0 end                                                                    
                                }// Year End                                
                            }//LocationList.Count > 0 End
                        }// User End

                        if (masterGradingList.Count > 0)
                        {
                            sucess = CommanGradingClass.InsertGrading_PreValue(masterGradingList, custID, "I", UserID);
                        }
                    }//Customer End                                                         
                }
                return sucess;
            }
            catch (Exception ex)
            {
                CommanGradingClass.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        private static int BindingGradingTableRow(List<SP_InternalGradingReport_Result> transactionsQuery, DateTime EndDate, long BranchID, string cname, long cuid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)BranchID, (int)cuid)
                                 select row).ToList();

                int color = 4;
                DateTime date1 = new DateTime();
                DateTime date2 = new DateTime();
                if (cname.ToUpper().Trim() == "C1".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(30);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C2".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(27);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C3".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(30);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C4".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(29);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C5".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(30);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C6".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(29);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C7".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(30);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C8".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(30);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C9".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(29);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C10".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(31);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C11".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(29);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                else if (cname.ToUpper().Trim() == "C12".ToUpper().Trim())
                {
                    DateTime previousDate = EndDate.AddDays(30);
                    date1 = new DateTime(EndDate.Year, EndDate.Month, 1);
                    date2 = new DateTime(previousDate.Year, previousDate.Month, previousDate.Day);
                }
                long highRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                long MediumRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                long LowRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                double seventyfivePercentLow = (0.75 * LowRisktotalCount);
                double seventyfivePercentHigh = (0.75 * highRisktotalCount);
                float dividedbyLow = ((float)LowRisktotalCount / 2);
                float dividedbyMedium = ((float)MediumRisktotalCount / 2);
                if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRisktotalCount != 0)
                {
                    if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                    {
                        color = 1;
                    }
                    //else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                    else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= dividedbyMedium && LowRiskCompletedCount >= dividedbyLow)
                    {
                        color = 2;
                    }
                    else
                    {
                        color = 3;
                    }
                }
                else
                {
                    color = 4;
                }
                return color;
            }
        }


        public static DataTable BindLocationFilterGraddingReport(List<NameValueHierarchy> bracnhes, List<int> LocationList, long UID)
        {
            try
            {
                DataTable table = new DataTable();
                table.Columns.Add("LocationName", typeof(string));
                table.Columns.Add("Node", typeof(long));
                table.Columns.Add("PNode", typeof(long));
                table.Columns.Add("UserID", typeof(long));
                table.Columns.Add("LocationID", typeof(long));
                table.Columns.Add("IsStatutory", typeof(string));
                int Lcnt = 1;
                int Ccnt = 1;
                foreach (var parentitem in bracnhes)
                {
                    DataRow parenttableRow = table.NewRow();
                    Ccnt = (Lcnt * 100 + 1);
                    foreach (var item in parentitem.Children)
                    {
                        if (FindNodeExists(item, LocationList))
                        {
                            DataRow tableRow = table.NewRow();
                            tableRow["LocationName"] = item.Name;
                            tableRow["Node"] = Lcnt;
                            tableRow["PNode"] = 0;
                            tableRow["UserID"] = UID;
                            tableRow["LocationID"] = item.ID;
                            tableRow["IsStatutory"] = "I";


                            table.Rows.Add(tableRow);
                            BindBranchesHierarchy(item, LocationList, table, Ccnt, Lcnt, UID);
                            if (item.Level == 1)
                            {
                                Lcnt++;
                            }
                            Ccnt = (Lcnt * 100 + 1);
                        }
                    }
                }
                return table;
            }
            catch (Exception ex)
            {
                CommanGradingClass.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static void BindBranchesHierarchy(NameValueHierarchy nvp, List<int> LocationList, DataTable table, long cnode, long parentnode, long UID)
        {
            foreach (var item in nvp.Children)
            {
                long CCcnt = 1;
                CCcnt = (cnode * 1000 + 1);
                if (FindNodeExists(item, LocationList))
                {
                    DataRow tableRow = table.NewRow();
                    tableRow["LocationName"] = item.Name;
                    tableRow["Node"] = cnode;
                    tableRow["PNode"] = parentnode;
                    tableRow["UserID"] = UID;
                    tableRow["LocationID"] = item.ID;
                    tableRow["IsStatutory"] = "I";
                    table.Rows.Add(tableRow);
                    BindBranchesHierarchy(item, LocationList, table, CCcnt, cnode, UID);
                    cnode++;
                    if (item.Children.Count > 1)
                    {
                        CCcnt++;
                    }
                    else
                    {
                        CCcnt = (cnode * 1000 + 1);
                    }
                }
            }
        }
        public static bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public static List<NameValueHierarchy> GetAllHierarchyManagementSatutory(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from Cust in entities.Customers
                             where Cust.ID == customerID
                             select Cust);

                //hierarchy = (from row in query
                //             select new NameValueHierarchy() { ID = row.ID, Name = row.Name, Level = 0 }).OrderBy(entry => entry.Name).ToList();

                hierarchy = (from row in query
                             select new NameValueHierarchy() { ID = row.ID, Name = row.Name, Level = 0 }).OrderBy(entry => entry.ID).ToList();

                IQueryable<CustomerBranch> query1 = (from row in entities.CustomerBranches
                                                     where row.IsDeleted == false
                                                     && row.CustomerID == customerID
                                                     select row);
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, query1);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, IQueryable<CustomerBranch> query1)
        {
            var query = query1;
            if (isClient)
            {
                query = query1.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query1.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name, Level = entry.Type }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntities(item, false, entities, query1);
            }
        }
    }
}
