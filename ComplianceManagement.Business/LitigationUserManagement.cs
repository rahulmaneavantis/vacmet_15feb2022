﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class LitigationUserManagement
    {
        public static List<View_DisplayUserWithRating> GetAllUser(int customerID, string filter = null)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var users = (from row in entities.View_DisplayUserWithRating
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.ToLower().Contains(((filter).ToLower()).Trim()) || entry.LastName.ToLower().Contains(((filter).ToLower()).Trim()) || entry.Email.ToLower().Contains(((filter).ToLower()).Trim()) || entry.Address.ToLower().Contains(((filter).ToLower()).Trim()) || entry.ContactNumber.ToLower().Contains(((filter).ToLower()).Trim()) || entry.LitigationRoleID.ToString().Contains(((filter).ToLower()).Trim()));
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }

        
        public static List<object> GetLitigationUsers(int customerID, int userType) //UserType 1=Internal,2-External,3-Both
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.IsActive == true
                             && row.CustomerID == customerID
                             select row).ToList();

                if (userType != 0)
                {
                    if (userType == 1)
                        query = query.Where(entry => entry.IsExternal == false).ToList();
                    else if (userType == 2)
                        query = query.Where(entry => entry.IsExternal == true).ToList();
                }

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return lstUsers.ToList();
            }
        }

        public static object GetAllLitigationAssignUser(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             //&& row.IsActive == true
                             && row.CustomerID == customerID
                             && row.LitigationRoleID != null
                             && row.IsExternal==false
                             select new
                             {
                                 row.ID,
                                 Name=row.FirstName + " " + row.LastName,
                             }).ToList();
                return query;

                
            }
        }

        public static List<User> GetLitigationAllUsers(int customerID) //UserType 1=Internal,2-External,3-Both
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             //&& row.IsActive == true
                             && row.CustomerID == customerID
                             && row.LitigationRoleID != null
                             select row).ToList();

                return query.ToList();
            }
        }
        public static List<object> GetRequiredUsersLitigationAssign(List<User> userList, int userType) //UserType 1=Internal,2-Lawyer,3-External&Lawyer,
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in userList
                             where row.IsDeleted == false
                             && row.IsActive == true
                             select row).ToList();

                if (userType != 0)
                {
                    if (userType == 1)
                        query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID == null).ToList();
                }

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return lstUsers.ToList();
            }
        }

        public static List<object> GetRequiredUsers(List<User> userList, int userType) //UserType 1=Internal,2-Lawyer,3-External&Lawyer,
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in userList
                             where row.IsDeleted == false
                             && row.IsActive == true
                             select row).ToList();

                if (userType != 0)
                {
                    if (userType == 1)
                        query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID == null).ToList();
                    else if (userType == 2)
                        query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID != null).ToList();
                    else if (userType == 3)
                        query = query.Where(entry => entry.IsExternal == true || entry.LawyerFirmID != null).ToList();
                }

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return lstUsers.ToList();
            }
        }


        public static bool checkLawyerorNo(int Userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.IsActive == true
                             && row.ID == Userid
                             && row.IsExternal == false
                             && row.LawyerFirmID == null
                             select row.ID).FirstOrDefault();

                if (query != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }


        public static object GetRequiredUsersByLawFirm(List<User> userList, int userType, int LawfirmID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in userList
                             where row.IsDeleted == false
                             && row.IsActive == true
                             select row).ToList();

                if (userType != 0)
                {
                    if (userType == 1)
                        query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID == null).ToList();
                    else if (userType == 2)
                        query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID != null).ToList();
                }

                query = query.Where(entry => entry.IsExternal == false && entry.LawyerFirmID == LawfirmID).ToList();

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return lstUsers.ToList();
            }
        }
        public static List<object> GetLitigationUsersByLawyerID(int customerID, int lawyerFirmID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             //&& row.IsActive == true
                             && row.CustomerID == customerID
                             && row.LawyerFirmID == lawyerFirmID
                             select row).ToList();

                var lstUsers = (from row in query
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();

                return lstUsers.ToList();
            }
        }

        public static int CreateNewUserCompliance(User user)
        {
            int newUserID = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {                       
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        entities.Users.Add(user);
                        entities.SaveChanges();
                        newUserID = Convert.ToInt32(user.ID);
                        dbtransaction.Commit();
                        return newUserID;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        newUserID--;
                        entities.SP_ResetIDUser(newUserID);
                        return newUserID = 0;
                    }
                }
            }
        }

        public static bool CreateNewUserAudit(mst_User user)
        {
            int newUserID = 0;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {                        
                        user.IsDeleted = false;
                        user.CreatedOn = DateTime.Now;
                        user.Password = user.Password;
                        user.ChangPasswordDate = DateTime.Now;
                        user.ChangePasswordFlag = true;
                        user.EnType = "A";
                        entities.mst_User.Add(user);
                        entities.SaveChanges();

                        newUserID = Convert.ToInt32(user.ID);

                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        newUserID--;
                        entities.SP_ResetIDMstUser(newUserID);
                        return false;
                    }
                }
            }
        }

        public static bool UpdateUserLitigation(User user, List<UserParameterValue> parameters)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User userToUpdate = (from row in entities.Users
                                     where row.ID == user.ID
                                     select row).FirstOrDefault();
                if (userToUpdate != null)
                {
                    userToUpdate.IsHead = user.IsHead;
                    userToUpdate.FirstName = user.FirstName;
                    userToUpdate.LastName = user.LastName;
                    userToUpdate.Designation = user.Designation;
                    userToUpdate.Email = user.Email;
                    userToUpdate.ContactNumber = user.ContactNumber;
                    userToUpdate.Address = user.Address;
                    userToUpdate.CustomerID = user.CustomerID;
                    userToUpdate.CustomerBranchID = user.CustomerBranchID;
                    userToUpdate.ReportingToID = user.ReportingToID;
                    userToUpdate.DepartmentID = user.DepartmentID;
                    userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;

                    userToUpdate.LitigationRoleID = user.LitigationRoleID;
                    userToUpdate.LawyerFirmID = user.LawyerFirmID;
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.AuditorID = user.AuditorID;
                    userToUpdate.IsExternal = user.IsExternal;

                    List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                    var existingParameters = (from parameterRow in entities.UserParameterValues
                                              where parameterRow.UserId == user.ID
                                              select parameterRow).ToList();

                    existingParameters.ForEach(entry =>
                    {
                        if (parameterIDs.Contains(entry.ID))
                        {
                            UserParameterValue parameter = parameters.Find(param1 => param1.ID == entry.ID);
                            entry.Value = parameter.Value;
                        }
                        else
                        {
                            entities.UserParameterValues.Remove(entry);
                        }
                    });

                    parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValues.Add(entry));

                    entities.SaveChanges();
                }

                return true;
            }
        }

        public static bool UpdateAuditDBUserLitigation(mst_User user, List<UserParameterValue_Risk> parameters)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_User userToUpdate = (from row in entities.mst_User
                                         where row.ID == user.ID
                                         select row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.IsHead = user.IsHead;
                    userToUpdate.FirstName = user.FirstName;
                    userToUpdate.LastName = user.LastName;
                    userToUpdate.Designation = user.Designation;
                    userToUpdate.Email = user.Email;
                    userToUpdate.ContactNumber = user.ContactNumber;
                    userToUpdate.Address = user.Address;
                    userToUpdate.CustomerID = user.CustomerID;
                    userToUpdate.CustomerBranchID = user.CustomerBranchID;
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.DepartmentID = user.DepartmentID;
                    userToUpdate.IsAuditHeadOrMgr = user.IsAuditHeadOrMgr;

                    userToUpdate.LitigationRoleID = user.LitigationRoleID;
                    userToUpdate.LawyerFirmID = user.LawyerFirmID;
                    userToUpdate.RoleID = user.RoleID;
                    userToUpdate.AuditorID = user.AuditorID;
                    userToUpdate.IsExternal = user.IsExternal;

                    List<int> parameterIDs = parameters.Select(entry => entry.ID).ToList();

                    var existingParameters = (from parameterRow in entities.UserParameterValue_Risk
                                              where parameterRow.UserId == user.ID
                                              select parameterRow).ToList();

                    existingParameters.ForEach(entry =>
                    {
                        if (parameterIDs.Contains(entry.ID))
                        {
                            UserParameterValue_Risk parameter = parameters.Find(param1 => param1.ID == entry.ID);
                            entry.Value = parameter.Value;
                        }
                        else
                        {
                            entities.UserParameterValue_Risk.Remove(entry);
                        }
                    });

                    //parameters.Where(entry => entry.ID == -1).ToList().ForEach(entry => userToUpdate.UserParameterValue_Risk.Add(entry));

                    entities.SaveChanges();
                }
                return true;
            }
        }

        //13 June later
        public static object GetOwnerList(long customerID,int CaseStatus)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.Sp_LitigationGetOwnerOfCase((int)Convert.ToInt32(customerID),(int) CaseStatus)
                             select row).ToList();
                
                return query.ToList();
            }
        }

        public static object GetOwnerListforNotice(long customerID,int NoticeStatus)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.Sp_LitigationGetOwnerOfNotice((int)Convert.ToInt32(customerID),(int)NoticeStatus)                             
                             select row).ToList();

                return query;
            }
        }

        public static bool IsExternalLawyer(int custID, int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int? query = (from row in entities.Users
                             where row.IsDeleted == false
                             //&& row.IsActive == true
                             && row.CustomerID == custID
                             && row.ID == userID
                             select row.LawyerFirmID).FirstOrDefault();

                if (query > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

                
            }
        }
    }
}
