﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class Subcategorization
    {
        public static object GetAll(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.mst_Subcategorization
                             where row.IsActive == false
                             select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter));
                }

                return users.ToList();
            }
        }
        public static bool categoryExists(mst_Subcategorization cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.mst_Subcategorization
                             where row.Name.ToUpper().Equals(cat.Name.ToUpper()) && row.IsActive == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static void CreatecategorizationMaster(mst_Subcategorization cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.mst_Subcategorization.Add(cat);
                entities.SaveChanges();
            }
        }
        public static void UpdateDepartmentMaster(mst_Subcategorization cat)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                mst_Subcategorization deptMasterToUpdate = (from row in entities.mst_Subcategorization
                                                            where row.ID == cat.ID
                                                    && row.IsActive == false

                                                         select row).FirstOrDefault();

                deptMasterToUpdate.Name = cat.Name;
                entities.SaveChanges();
            }
        }
        public static mst_Subcategorization categorizationMasterGetByIDAudit(int subcategoryid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MsttMaster = (from row in entities.mst_Subcategorization
                                  where row.ID == subcategoryid && row.IsActive == false

                                  select row).SingleOrDefault();

                return MsttMaster;
            }
        }
        public static void DeletecategorizationMaster(int subcategoryid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var categorizationMastertoDelete = (from row in entities.mst_Subcategorization
                                                    where row.ID == subcategoryid
                                                    select row).FirstOrDefault();

                categorizationMastertoDelete.IsActive = true;
                entities.SaveChanges();
            }
        }
        public  class SubcategorizationList
        {
            public int SubCategoryID { get; set; }
            public int CategoryID { get; set; }
            public string CategoryName { get; set; }
            public string SubCategoryName { get; set; }           
        }
        
        public static List<SubcategorizationList> GetAllcategorizationList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DepartmentMasterList = (from row in entities.mst_categorization
                                            join row1 in entities.mst_Subcategorization
                                            on  row.ID equals row1.categorid
                                            where row.IsActive == false
                                            && row1.IsActive == false
                                            select new SubcategorizationList {
                                                SubCategoryID= row1.ID,
                                                CategoryID =row.ID,
                                                CategoryName =row.Name,
                                                SubCategoryName =row1.Name
                                              }).ToList();
                 

                return DepartmentMasterList.OrderBy(entry => entry.CategoryName).ToList();
            }
        }
    }
}
