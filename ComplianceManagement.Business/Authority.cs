﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class Authority
    {
        public static void CreateAuthority(Authorityofthestate objAuth)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.Authorityofthestates.Add(objAuth);
                entities.SaveChanges();
            }
        }
        public static void UpdateAuthoritys(Authorityofthestate objAuth)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                Authorityofthestate objAuths = (from row in entities.Authorityofthestates
                                         where row.Id == objAuth.Id
                                          
                                         select row).FirstOrDefault();

                objAuths.Name = objAuth.Name;
                objAuths.Designation = objAuth.Designation;
      
                entities.SaveChanges();
            }
        }
        public static Authorityofthestate GetAuthority(int ID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Objcase = (from row in entities.Authorityofthestates
                               where row.Id == ID
                             
                               select row).SingleOrDefault();
                return Objcase;
            }
        }

        public static List<Authorityofthestate> GetAllAuthority(int CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Objcase = (from row in entities.Authorityofthestates
                               where row.CustomerId == CustomerID
                               select row).ToList();
                return Objcase;
            }
        }
        public static void DeleteAuthority(int ID, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                Authorityofthestate objCase = (from row in entities.Authorityofthestates
                                           where row.Id == ID
                                           && row.CustomerId == CustomerID
                                         
                                           select row).FirstOrDefault();


                entities.Authorityofthestates.Remove(objCase);
                entities.SaveChanges();
            }
        }
    }
}
