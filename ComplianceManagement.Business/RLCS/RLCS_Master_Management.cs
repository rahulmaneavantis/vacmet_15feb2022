﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCS_Master_Management
    {
        public static string GetBranchType(int custBranchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var branchType = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    where row.AVACOM_BranchID == custBranchID
                                    select row.BranchType).FirstOrDefault();

                    return branchType;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public static List<ListItem> GetYearstoBind(int numberofPrevYears)
        {
            List<ListItem> YearstoBind = new List<ListItem>();

            int currentYear = DateTime.Now.Year;
            for (int i = 1; i < numberofPrevYears; ++i)
            {
                ListItem tmp = new ListItem();
                tmp.Value = currentYear.ToString();
                tmp.Text = currentYear.ToString();
                YearstoBind.Add(tmp);
                currentYear = DateTime.Now.AddYears(-i).Year;
            }

            return YearstoBind;
        }

        public static int CheckDistributorsCustomerCount(int distID)
        {            
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var customerCount = (from row in entities.Customers
                                     where row.ParentID == distID
                                     && row.IsDeleted == false
                                     select row).Count();

                return customerCount;
            }
        }

        public static int Get_DistributorID(int customerID)
        {
            int distID = 0;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var custRecord = (from row in entities.Customers
                                  where row.ID == customerID
                                  select row).FirstOrDefault();

                if (custRecord != null)
                    if (custRecord.ParentID != null)
                        distID = Convert.ToInt32(custRecord.ParentID);
                if (distID == 0)
                {
                    distID = Convert.ToInt32(custRecord.ServiceProviderID);
                }

                return distID;
            }
        }

        public static void ToggleStatus_ComplianceDB(int userID, bool status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User userToUpdate = (from row in entities.Users
                                     where row.ID == userID
                                     select row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.IsActive = status;

                    if (status)
                        userToUpdate.DeactivatedOn = null;
                    else
                        userToUpdate.DeactivatedOn = DateTime.Now;

                    entities.SaveChanges();
                }
            }
        }

        public static void ToggleStatus_AuditDB(int userID, bool status)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_User userToUpdate = (from row in entities.mst_User
                                         where row.ID == userID
                                         select row).FirstOrDefault();

                if (userToUpdate != null)
                {
                    userToUpdate.IsActive = status;

                    if (status)
                    {
                        userToUpdate.DeactivatedOn = null;
                    }
                    else
                    {
                        userToUpdate.DeactivatedOn = DateTime.Now;
                    }

                    entities.SaveChanges();
                }
            }
        }

        public static List<RLCS_CustomerAssessmentDetails> Get_CustomerAssessmentDetails(int customerID)
        {
            List<RLCS_CustomerAssessmentDetails> lstRecords = new List<RLCS_CustomerAssessmentDetails>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                lstRecords = (from row in entities.RLCS_CustomerAssessmentDetails
                              where row.CustomerID == customerID
                              select row).ToList();

                return lstRecords.OrderBy(entry => entry.ParentBranchID).ToList();
            }
        }

        public static void Update_CustomerAssessmentDetails(int custID, int recordID, int avacomBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var prevRecord = (from row in entities.RLCS_CustomerAssessmentDetails
                                  where row.BranchID == recordID
                                  && row.CustomerID == custID
                                  select row).FirstOrDefault();

                if (prevRecord != null)
                {
                    prevRecord.AVACOM_BranchID = avacomBranchID;
                    entities.SaveChanges();
                }
            }
        }

        public static int Create_CustomerBranch_Compliance(CustomerBranch customerBranch)
        {
            int newCustomerBranchID = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.CustomerBranches
                                      where row.Name == customerBranch.Name
                                      //&& row.StateID == customerBranch.StateID
                                      //&& row.CityID == customerBranch.CityID
                                      && row.CustomerID == customerBranch.CustomerID
                                      && row.ParentID == customerBranch.ParentID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        //prevRecord.Name = customerBranch.Name;
                        //prevRecord.Type = customerBranch.Type;
                        //prevRecord.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                        //prevRecord.AddressLine1 = customerBranch.AddressLine1;
                        //prevRecord.AddressLine2 = customerBranch.AddressLine2;
                        //prevRecord.StateID = customerBranch.StateID;
                        //prevRecord.CityID = customerBranch.CityID;
                        //prevRecord.Others = customerBranch.Others;
                        //prevRecord.Industry = customerBranch.Industry;
                        //prevRecord.ContactPerson = customerBranch.ContactPerson;
                        //prevRecord.Landline = customerBranch.Landline;
                        //prevRecord.Mobile = customerBranch.Mobile;
                        //prevRecord.EmailID = customerBranch.EmailID;
                        //prevRecord.PinCode = customerBranch.PinCode;
                        //prevRecord.Status = customerBranch.Status;
                        //prevRecord.LegalEntityTypeID = customerBranch.LegalEntityTypeID;
                        //prevRecord.ComType = customerBranch.ComType;
                        //prevRecord.AuditPR = customerBranch.AuditPR;

                        //entities.SaveChanges();

                        newCustomerBranchID = prevRecord.ID;
                        return newCustomerBranchID;
                    }
                    else
                    {
                        customerBranch.IsDeleted = false;
                        customerBranch.CreatedOn = DateTime.UtcNow;
                        entities.CustomerBranches.Add(customerBranch);
                        entities.SaveChanges();

                        newCustomerBranchID = customerBranch.ID;

                        return newCustomerBranchID;
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return newCustomerBranchID = 0;
            }
        }

        public static int Create_CustomerBranch_Audit(mst_CustomerBranch customerBranch)
        {
            int newCustomerBranchID = 0;
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var prevRecord = (from row in entities.mst_CustomerBranch
                                      where row.Name == customerBranch.Name
                                      //&& row.StateID == customerBranch.StateID
                                      //&& row.CityID == customerBranch.CityID
                                      && row.CustomerID == customerBranch.CustomerID
                                      && row.ParentID == customerBranch.ParentID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        //prevRecord.Name = customerBranch.Name;
                        //prevRecord.Type = customerBranch.Type;
                        //prevRecord.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                        //prevRecord.AddressLine1 = customerBranch.AddressLine1;
                        //prevRecord.AddressLine2 = customerBranch.AddressLine2;
                        //prevRecord.StateID = customerBranch.StateID;
                        //prevRecord.CityID = customerBranch.CityID;
                        //prevRecord.Others = customerBranch.Others;
                        //prevRecord.Industry = customerBranch.Industry;
                        //prevRecord.ContactPerson = customerBranch.ContactPerson;
                        //prevRecord.Landline = customerBranch.Landline;
                        //prevRecord.Mobile = customerBranch.Mobile;
                        //prevRecord.EmailID = customerBranch.EmailID;
                        //prevRecord.PinCode = customerBranch.PinCode;
                        //prevRecord.Status = customerBranch.Status;
                        //prevRecord.LegalEntityTypeID = customerBranch.LegalEntityTypeID;
                        //prevRecord.ComType = customerBranch.ComType;
                        //prevRecord.AuditPR = customerBranch.AuditPR;

                        newCustomerBranchID = prevRecord.ID;
                        return newCustomerBranchID;
                    }
                    else
                    {
                        customerBranch.IsDeleted = false;
                        customerBranch.CreatedOn = DateTime.UtcNow;

                        entities.mst_CustomerBranch.Add(customerBranch);
                        entities.SaveChanges();

                        newCustomerBranchID = customerBranch.ID;

                        return newCustomerBranchID;
                    }
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return newCustomerBranchID = 0;
            }
        }

        public static void DeleteCustomerBranch_Compliance(int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    var recordCustBranch = entities.CustomerBranches.Where(x => x.ID == customerBranchID).FirstOrDefault();
                    if (recordCustBranch != null)
                    {
                        entities.CustomerBranches.Remove(recordCustBranch);
                        entities.SaveChanges();
                    }

                    if (customerBranchID > 0)
                    {
                        customerBranchID--;
                        entities.SP_ResetIDCustomerBranch(customerBranchID);
                    }
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public static bool CreateUpdate_ProductMapping_AuditDB(ProductMapping_Risk prodcutmappingrisk)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var existsProductMapping = (from row in entities.ProductMapping_Risk
                                                where row.CustomerID == prodcutmappingrisk.CustomerID
                                                && row.ProductID == prodcutmappingrisk.ProductID
                                                select row).FirstOrDefault();

                    if (existsProductMapping != null)
                    {
                        existsProductMapping.CustomerID = prodcutmappingrisk.CustomerID;
                        existsProductMapping.ProductID = prodcutmappingrisk.ProductID;
                        existsProductMapping.IsActive = false;

                        entities.SaveChanges();
                    }
                    else
                    {
                        prodcutmappingrisk.CreatedOn = DateTime.Now;
                        prodcutmappingrisk.CreatedBy = prodcutmappingrisk.CreatedBy;

                        entities.ProductMapping_Risk.Add(prodcutmappingrisk);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_ProductMapping_ComplianceDB(ProductMapping prodcutmapping)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var existsProductMapping = (from row in entities.ProductMappings
                                                where row.CustomerID == prodcutmapping.CustomerID
                                                && row.ProductID == prodcutmapping.ProductID
                                                select row).FirstOrDefault();

                    if (existsProductMapping != null)
                    {
                        existsProductMapping.CustomerID = prodcutmapping.CustomerID;
                        existsProductMapping.ProductID = prodcutmapping.ProductID;
                        existsProductMapping.IsActive = false;

                        entities.SaveChanges();
                    }
                    else
                    {
                        prodcutmapping.CreatedOn = DateTime.Now;
                        prodcutmapping.CreatedBy = prodcutmapping.CreatedBy;

                        entities.ProductMappings.Add(prodcutmapping);
                        entities.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<UserView> GetAllUser(int customerID, int serviceProviderID, int distributorID, int prodType, string filter = null)
        {
            List<UserView> users = new List<UserView>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (serviceProviderID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ServiceProviderID == serviceProviderID || cust.ID == serviceProviderID)
                             && cust.ComplianceProductType == prodType
                             select row).ToList();
                }
                else if (distributorID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ParentID == distributorID || cust.ID == distributorID)
                             && cust.ComplianceProductType == prodType
                             select row).ToList();
                }
                else
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where cust.ComplianceProductType == prodType
                             select row).ToList();
                }

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter)).ToList();
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }

        public static List<object> GetAll_RLCSUsers_IncludingServiceProvider(int customerID, int serviceProviderID, int prodType, string filter = null)
        {
            List<UserView> users = new List<UserView>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (serviceProviderID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ServiceProviderID == serviceProviderID || cust.ID == serviceProviderID)
                             && cust.ComplianceProductType == prodType
                             select row).ToList();
                }
                else
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where cust.ComplianceProductType == prodType
                             select row).ToList();
                }

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID || entry.CustomerID == serviceProviderID).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter)).ToList();
                }

                var userList = (from row in users
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).Distinct().ToList<object>();

                return userList;
            }
        }
        public static List<UserView> GetAllUser_IncludingServiceProvider(int customerID, int serviceProviderID, int prodType, string filter = null)
        {
            List<UserView> users = new List<UserView>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (serviceProviderID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ServiceProviderID == serviceProviderID || cust.ID == serviceProviderID)
                             && cust.ComplianceProductType == prodType
                             select row).ToList();
                }
                else
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where cust.ComplianceProductType == prodType
                             select row).ToList();
                }

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID || entry.CustomerID == serviceProviderID).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter)).ToList();
                }

                return users.OrderBy(entry => entry.FirstName).ToList();
            }
        }

        public static List<object> GetAll_RLCSUsers_IncludingServiceProviderOrDistributor(int customerID, int serviceProviderID, int distributorID, int prodType, string filter = null)
        {
            List<UserView> users = new List<UserView>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (serviceProviderID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ID == serviceProviderID || cust.ID == customerID)
                             && cust.ComplianceProductType == prodType
                             select row).ToList();
                }
                else if (distributorID != -1)
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where (cust.ID == distributorID || cust.ID == customerID)
                             && cust.ComplianceProductType == prodType
                             select row).ToList();
                }
                else
                {
                    users = (from row in entities.UserViews
                             join cust in entities.Customers
                             on row.CustomerID equals cust.ID
                             where cust.ComplianceProductType == prodType
                             select row).ToList();
                }

                if (customerID != -1)
                {
                    //users = users.Where(entry => entry.CustomerID == customerID || entry.CustomerID == serviceProviderID).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter)).ToList();
                }

                var userList = (from row in users
                                select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).Distinct().ToList<object>();

                return userList;
            }
        }
        public static bool DeleteEmployee(int recordID, string status)
        {
            bool deleteSuccess = false;

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var objRecord = (from row in entities.RLCS_Employee_Master
                                     where row.EM_ID == recordID                                 
                                     select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.EM_Status = status;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }

        public static string GetClientIDByBranchID(int custBranchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var clientID = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    where row.AVACOM_BranchID == custBranchID
                                    select row.CM_ClientID).FirstOrDefault();

                    return clientID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public static string GetCorporateIDByCustID(int custID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var CorpID = (from row in entities.RLCS_Customer_Corporate_Mapping
                                    where row.AVACOM_CustomerID == custID
                                    select row.CO_CorporateID).FirstOrDefault();

                    return CorpID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public static bool CanCreateSubDistributors(int custID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var CanCreateSubDist = (from row in entities.Customers
                                  where row.ID == custID
                                  select row.CanCreateSubDist).FirstOrDefault();

                    if (CanCreateSubDist != null)
                        return Convert.ToBoolean(CanCreateSubDist);
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CanCreateSubDistributors(Customer custRecord)
        {
            try
            {
                if (custRecord.CanCreateSubDist != null)
                    return Convert.ToBoolean(custRecord.CanCreateSubDist);
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool IsPaymentModeCustomer(Customer custRecord)
        {
            try
            {
                if (custRecord.IsPayment != null)
                    return Convert.ToBoolean(custRecord.IsPayment);
                else
                    return false;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static List<RLCS_Employee_Master> GetAll_EmployeeMaster(int customerID,string filter=null)
        {            
            List<RLCS_Employee_Master> lstEmployees = new List<RLCS_Employee_Master>();

            try
            {
                using (ComplianceDBEntities dbcontext = new ComplianceDBEntities())
                {
                    var employees = dbcontext.RLCS_Employee_Master.Where(row => row.AVACOM_CustomerID == customerID);

                    if (employees != null && !string.IsNullOrEmpty(filter))
                        employees =  employees.Where(x => x.EM_EmpName == filter || x.EM_EmpID == filter || x.EM_Branch == filter);

                        return employees.ToList();
                }


               
            }
            catch(Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstEmployees;
            }            
        }

        #region To Find Entities and Sub-Entities not Branches
        public static List<NameValueHierarchy> GetAll_Entities(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadChildSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadChildSubEntities(int customerID, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.Type == 1
                                                && row.ParentID == null
                                                && row.CustomerID == customerID
                                                select row);
            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadChildSubEntities(customerID, item, true, entities);
            }
        }

        #endregion

        public static object FillCountry()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.RLCS_Country_Mapping
                             where row.IsDeleted == false
                             && row.CNT_Status == "A"
                             select row);

                var countries = (from row in query
                                 select new { ID = row.CNT_ID, Name = row.CNT_Name }).OrderBy(entry => entry.Name).ToList<object>();

                return countries;
            }
        }

        public static object FillStates()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.RLCS_State_Mapping
                             where row.SM_Status == "A"
                             select row);

                var countries = (from row in query
                                 select new { ID = row.AVACOM_StateID, Name = row.SM_Name }).OrderBy(entry => entry.Name).ToList<object>();

                return countries;
            }
        }

        public static object FillLocationCityByStateID(int stateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from RLCM in entities.RLCS_Location_City_Mapping
                             join CM in entities.Cities
                             on RLCM.AVACOM_CityID equals CM.ID
                             where RLCM.LM_Status == "A"
                             && CM.StateId == stateID
                             select RLCM);

                var countries = (from row in query
                                 select new { ID = row.AVACOM_CityID, Name = row.LM_Name + "-" + row.LM_Code }).OrderBy(entry => entry.Name).ToList<object>();

                return countries;
            }
        }

        public static object FillLocationCity(string stateCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.RLCS_Location_City_Mapping
                             where row.LM_Status == "A"
                             && row.SM_Code.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())
                             select row);

                var countries = (from row in query
                                 select new { ID = row.AVACOM_CityID, Name = row.LM_Name }).OrderBy(entry => entry.Name).ToList<object>();

                return countries;
            }
        }

        public static RLCS_Customer_Corporate_Mapping Get_Customer_CorporateClient_Mapping(int avacomCustomerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_Customer_Corporate_Mapping
                                      where row.AVACOM_CustomerID == avacomCustomerID
                                      select row).FirstOrDefault();
                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool CreateUpdate_Customer_CorporateClient_Mapping(RLCS_Customer_Corporate_Mapping _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_Customer_Corporate_Mapping
                                      where row.AVACOM_CustomerID==_objRecord.AVACOM_CustomerID                                      
                                      //where row.CO_CorporateID == _objRecord.CO_CorporateID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.AVACOM_CustomerID = _objRecord.AVACOM_CustomerID;
                        prevRecord.CO_Name = _objRecord.CO_Name;
                        prevRecord.CO_City = _objRecord.CO_City;
                        prevRecord.CO_State = _objRecord.CO_State;
                        prevRecord.CO_Country = _objRecord.CO_Country;
                        prevRecord.CO_BDLocation = _objRecord.CO_BDLocation;
                        prevRecord.CO_BDAnchor = _objRecord.CO_BDAnchor;
                        prevRecord.CO_Pincode = _objRecord.CO_Pincode;
                        prevRecord.CO_PAN = _objRecord.CO_PAN;
                        prevRecord.CO_TAN = _objRecord.CO_TAN;
                        prevRecord.CO_URL = _objRecord.CO_URL;
                        prevRecord.CO_ContactDesignation = _objRecord.CO_ContactDesignation;
                        prevRecord.CO_FAXNo = _objRecord.CO_FAXNo;
                        prevRecord.CO_Status = _objRecord.CO_Status;
                        prevRecord.CO_Version = _objRecord.CO_Version;
                        prevRecord.CO_IsAventisCorporate = _objRecord.CO_IsAventisCorporate;
                        prevRecord.UpdatedOn = DateTime.Now;

                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;

                        entities.RLCS_Customer_Corporate_Mapping.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Exists_CorporateID(string corpID)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_Customer_Corporate_Mapping
                                      where row.CO_CorporateID == corpID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        saveSuccess = true;
                    }
                    else
                    {
                        saveSuccess = false;
                    }

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Exists_ClientID(string clientID)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.CM_ClientID == clientID
                                      && row.BranchType == "E"
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        saveSuccess = true;
                    }
                    else
                    {
                        saveSuccess = false;
                    }

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetClientLocationDetails(int custBranchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var branchtype = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    where row.AVACOM_BranchID == custBranchID
                                    && row.AVACOM_BranchID != null
                                    select row.BranchType).FirstOrDefault();

                    if (branchtype != null)
                    {

                        var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                          where row.AVACOM_BranchID == custBranchID
                                          && row.AVACOM_BranchID != null
                                          && row.BranchType.Trim().ToUpper().Equals(branchtype)
                                          select row).FirstOrDefault();
                        return prevRecord;
                    }
                
                    else
                    {
                        var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                          where row.AVACOM_BranchID == custBranchID
                                          && row.AVACOM_BranchID != null
                                          && row.BranchType.Trim().ToUpper().Equals("B")
                                          select row).FirstOrDefault();
                        return prevRecord;
                    }
                }
                 
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static RLCS_Client_BasicDetails GetClientBasicDetails(string clientid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var clientdetails = (from row in entities.RLCS_Client_BasicDetails
                                      where row.CB_ClientID == clientid
                                      select row).FirstOrDefault();

                    return clientdetails;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetClientLocationDetails(List<int> custBranchIDs)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where custBranchIDs.Contains((int)row.AVACOM_BranchID)
                                      && row.AVACOM_BranchID != null
                                      && row.BranchType.Trim().ToUpper().Equals("B")
                                      select row).ToList();

                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetEntityOrBranchDetails(int custBranchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.AVACOM_BranchID == custBranchID
                                      && row.AVACOM_BranchID != null
                                      //&& row.BranchType.Trim().ToUpper().Equals("B")
                                      select row).FirstOrDefault();

                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetEntityOrBranchDetails(List<int> custBranchIDs)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where custBranchIDs.Contains((int)row.AVACOM_BranchID)
                                      && row.AVACOM_BranchID != null
                                      //&& row.BranchType.Trim().ToUpper().Equals("B")
                                      select row).ToList();

                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<string> GetPForESIorPT_Codes(string clientID, string challanType)
        {
            List<string> lstCodes = new List<string>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (challanType.Trim().ToUpper().Equals("EPF"))
                    {
                        lstCodes = (from RCB in entities.RLCS_Client_BasicDetails
                                    join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    on RCB.CB_ClientID equals RCCM.CM_ClientID
                                    where RCCM.AVACOM_BranchID != null
                                    && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                    && RCB.CB_PF_Code != null
                                    && RCCM.CM_Status == "A"
                                    && RCCM.BranchType == "B"
                                    //&& RCB.IsProcessed == true
                                    select RCB.CB_PF_Code).Distinct().ToList();
                    }
                    else if (challanType.Trim().ToUpper().Equals("ESI"))
                    {
                        lstCodes = (from REM in entities.RLCS_Employee_Master
                                    join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    on REM.AVACOM_BranchID equals RCCM.AVACOM_BranchID
                                    where RCCM.AVACOM_BranchID != null
                                    && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                    && REM.EM_Client_ESI_Number != null
                                    && REM.EM_Status == "A"
                                    && RCCM.CM_Status == "A"
                                    && RCCM.BranchType == "B"
                                    //&& REM.IsProcessed == true
                                    select REM.EM_Client_ESI_Number).Distinct().ToList();
                    }
                    else if (challanType.Trim().ToUpper().Equals("PT"))
                    {
                        lstCodes = (from REM in entities.RLCS_Employee_Master
                                    join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    on REM.AVACOM_BranchID equals RCCM.AVACOM_BranchID
                                    where RCCM.AVACOM_BranchID != null
                                    && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                    && REM.EM_Client_PT_State != null
                                    && REM.EM_Status == "A"
                                    && RCCM.CM_Status == "A"
                                    && RCCM.BranchType == "B"
                                   //&& REM.IsProcessed == true
                                    select REM.EM_Client_PT_State).Distinct().ToList();
                    }

                    return lstCodes;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstCodes;
            }
        }
        //GG 27Aug2021
        public static List<string> GetPFCodeNew(string clientID, string challanType, string PfType, int branchID)
        {
            List<string> lstCodes = new List<string>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (challanType.Trim().ToUpper().Equals("EPF"))
                    {
                        if (PfType == "C")
                        {
                            lstCodes = (from RCB in entities.RLCS_Client_BasicDetails
                                        join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                        on RCB.CB_ClientID equals RCCM.CM_ClientID
                                        where RCCM.AVACOM_BranchID != null
                                        && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                        && RCB.CB_PF_Code != null
                                        && RCCM.CM_Status == "A"
                                        && RCCM.BranchType == "B"
                                        select RCB.CB_PF_Code).Distinct().ToList();
                        }
                        else
                        {
                            lstCodes = (from RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                        where RCCM.AVACOM_BranchID == branchID
                                        && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                        && RCCM.CM_Status == "A"
                                        && RCCM.BranchType == "B"
                                        && RCCM.CL_PF_Code!=null
                                        select RCCM.CL_PF_Code).Distinct().ToList();
                        }

                    }
                    return lstCodes;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstCodes;
            }
        }        
        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetEntityDetailsNew(int custBranchID, string PfType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    string BranchType = "E";
                    if (PfType == "B")
                    {
                        BranchType = "B";
                    }
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.AVACOM_BranchID == custBranchID
                                      && row.AVACOM_BranchID != null
                                      && row.BranchType.Trim().ToUpper().Equals(BranchType)
                                      select row).FirstOrDefault();

                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        //END
        public static List<string> GetPTStates(int branchID, string clientID, string challanType)
        {
            List<string> lstCodes = new List<string>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstCodes = (from REM in entities.RLCS_Employee_Master
                                join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                on REM.AVACOM_BranchID equals RCCM.AVACOM_BranchID
                                where RCCM.AVACOM_BranchID != null
                                && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                && REM.EM_Client_PT_State != null
                                && REM.EM_Status == "A"
                                && RCCM.CM_Status == "A"
                                && RCCM.BranchType == "B"
                                && REM.AVACOM_BranchID == branchID
                                //&& REM.IsProcessed == true
                                select REM.EM_Client_PT_State).Distinct().ToList();

                    return lstCodes;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstCodes;
            }
        }
        
        public static List<string> GetBranchStateDetails(List<int> lstCustBranchIDs)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstBranchStates = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                           where lstCustBranchIDs.Contains((int)row.AVACOM_BranchID)
                                           && row.AVACOM_BranchID != null
                                           && row.CM_State != null && row.CM_State != ""
                                           select row.CM_State).ToList();

                    return lstBranchStates.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static int GetComplianceProductType(int customerID)
        {
            int complianceProductType = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.Customers
                                      where row.ID == customerID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        if (prevRecord.ComplianceProductType != null)
                            complianceProductType = Convert.ToInt32(prevRecord.ComplianceProductType);
                    }

                    return complianceProductType;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return complianceProductType;
            }
        }

        public static bool CheckShowHideInputs(int customerID, int loggedInUserID, int serviceProviderID)
        {
            bool showInputs = false;
            int complianceProductType = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.Customers
                                      where row.ID == customerID
                                      //&& row.ServiceProviderID == serviceProviderID
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        if (prevRecord.ComplianceProductType != null)
                        {
                            complianceProductType = Convert.ToInt32(prevRecord.ComplianceProductType);

                            if (complianceProductType == 2)
                            {
                                showInputs = true;
                                //var assignedRoles = CustomerBranchManagement.GetAssignedroleid(loggedInUserID);
                                //if (assignedRoles.Count > 0)
                                //    showInputs = true;
                            }                            
                        }
                    }

                    return showInputs;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return showInputs;
            }
        }

        public static string GetStateCodeByStateID(int stateID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var stateCode = (from row in entities.RLCS_State_Mapping
                                 where row.AVACOM_StateID == stateID
                                 select row.SM_Code).FirstOrDefault();

                return stateCode;
            }
        }

        public static string GetCityCodeByCityID(int cityID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var locationCityCode = (from row in entities.RLCS_Location_City_Mapping
                                        where row.AVACOM_CityID == cityID
                                        select row.LM_Code).FirstOrDefault();

                return locationCityCode;
            }
        }

        public static int GetCityIDbyCityCode(string cityCode)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int cityID = 0;

                    cityID = (from c in entities.RLCS_Location_City_Mapping
                              where c.LM_Code.Equals(cityCode)
                              select (int)c.AVACOM_CityID).FirstOrDefault();

                    return cityID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }

        }

        public static void CreateUpdate_RLCS_EntityAssignment(int userID, List<int> lstBranchIDs, bool recordActive, List<int> lstComplianceCategory)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstBranchIDs.ForEach(eachBranchID =>
                    {
                        lstComplianceCategory.ForEach(eachCategory =>
                        {
                            var prevEntityAssignmentRecord = (from row in entities.RLCS_EntitiesLocationAssignment
                                                              where row.BranchID == eachBranchID
                                                              && row.UserID == userID
                                                              && row.ComplianceCategoryID == eachCategory
                                                              select row).FirstOrDefault();

                            if (prevEntityAssignmentRecord != null)
                            {
                                if (!recordActive) //If Record Already Exist and Now De-Activated then needs to delete record
                                {
                                    entities.RLCS_EntitiesLocationAssignment.Remove(prevEntityAssignmentRecord);
                                    entities.SaveChanges();
                                }
                            }
                            else
                            {
                                if (recordActive) //If Record not Exist and needs to create new record
                                {
                                    RLCS_EntitiesLocationAssignment objEntitiesAssignment = new RLCS_EntitiesLocationAssignment()
                                    {
                                        UserID = userID,
                                        BranchID = eachBranchID,
                                        ComplianceCategoryID = eachCategory,
                                        UserProfileID = userID.ToString(), //TBD
                                        CreatedOn = DateTime.Now,
                                        IsDeleted = false
                                    };

                                    entities.RLCS_EntitiesLocationAssignment.Add(objEntitiesAssignment);
                                    entities.SaveChanges();
                                }
                            }
                        });
                    });
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static bool CreateUpdate_UserMapping(RLCS_User_Mapping _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_User_Mapping
                                      where row.AVACOM_UserID == _objRecord.AVACOM_UserID
                                      && row.CustomerID.Trim().ToUpper().Equals(_objRecord.CustomerID.Trim().ToUpper())
                                      && row.ProfileID.Trim().ToUpper().Equals(_objRecord.ProfileID.Trim().ToUpper())
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        prevRecord.AVACOM_UserID = _objRecord.AVACOM_UserID;
                        prevRecord.AVACOM_UserRole = _objRecord.AVACOM_UserRole;

                        prevRecord.UserID = _objRecord.UserID;
                        prevRecord.Password = _objRecord.Password;

                        prevRecord.FirstName = _objRecord.FirstName;
                        prevRecord.LastName = _objRecord.LastName;
                        prevRecord.Email = _objRecord.Email;
                        prevRecord.Password = _objRecord.Password;
                        prevRecord.ContactNumber = _objRecord.ContactNumber;
                        prevRecord.Address = _objRecord.Address;

                        prevRecord.Designation = _objRecord.Designation;
                        prevRecord.Department = _objRecord.Department;
                        prevRecord.CustomerID = _objRecord.CustomerID;
                        prevRecord.Role = _objRecord.Role;
                        prevRecord.ProfileID = _objRecord.ProfileID;

                        prevRecord.Status = _objRecord.Status;
                        prevRecord.IsActive = _objRecord.IsActive;

                        prevRecord.ModifiedDate = _objRecord.ModifiedDate;

                        prevRecord.UpdatedOn = DateTime.Now;
                        prevRecord.EnType = "A";
                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;

                        entities.RLCS_User_Mapping.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetEntityDetails(int custBranchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.AVACOM_BranchID == custBranchID
                                      && row.AVACOM_BranchID != null
                                      && row.BranchType.Trim().ToUpper().Equals("E")
                                      select row).FirstOrDefault();

                    return prevRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<string> GetScopeofWork(int custID, string clientID = "")
        {
            try
            {
                List<string> lstScope = new List<string>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (!string.IsNullOrEmpty(clientID))
                    {
                        lstScope = (from SOW in entities.RLCS_ScopeOfWork_Master
                                    join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    on SOW.ClientID equals RCCM.CM_ClientID
                                    join CB in entities.CustomerBranches
                                    on RCCM.AVACOM_BranchID equals CB.ID
                                    where CB.CustomerID == custID
                                    && RCCM.BranchType == "E"
                                    && SOW.Status == "A"
                                    && SOW.ClientID == clientID
                                    select SOW.ScopeID).Distinct().ToList();                       
                    }
                    else
                    {
                        lstScope = (from SOW in entities.RLCS_ScopeOfWork_Master
                                    join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    on SOW.ClientID equals RCCM.CM_ClientID
                                    join CB in entities.CustomerBranches
                                    on RCCM.AVACOM_BranchID equals CB.ID
                                    where CB.CustomerID == custID
                                    && RCCM.BranchType == "E" && SOW.Status == "A"
                                    select SOW.ScopeID).Distinct().ToList();
                    }

                    return lstScope;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

    }
}
