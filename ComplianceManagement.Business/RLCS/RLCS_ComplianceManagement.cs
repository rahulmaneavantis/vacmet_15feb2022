﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Reflection;
using static com.VirtuosoITech.ComplianceManagement.Business.RLCS.RLCSAPIClasses;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCS_ComplianceManagement
    {
        public static int GetAssignedComplianceRole(int customerID, int userID, long complianceInstanceID)
        {
            int roleID = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var complianceAssignmentRecord = entities.USP_RLCS_GetUserComplianceAssignment(userID, customerID).Where(row => row.ComplianceInstanceID == complianceInstanceID).OrderByDescending(row => row.RoleID).FirstOrDefault();

                    if (complianceAssignmentRecord != null)
                    {
                        roleID = complianceAssignmentRecord.RoleID;
                    }
                }

                return roleID;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return roleID;
            }
        }
        public static bool CheckComplianceAssignment(int customerID, int userID)
        {
            bool IsComplianceAssigned = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var numberAssignmentRecords = (from row in entities.USP_RLCS_GetUserComplianceAssignment(userID, customerID)
                                                   select row).Count();

                    if (numberAssignmentRecords > 0)
                    {
                        IsComplianceAssigned= true;
                    }
                }

                return IsComplianceAssigned;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return IsComplianceAssigned;
            }
        }

        public static bool UpdateComplianceAssignment(List<Tuple<long, int, int>> lstNewAssignment)
        {
            bool saveSuccess = false;
            try
            {
                if (lstNewAssignment.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        lstNewAssignment.ForEach(eachRecord =>
                        {
                            var prevAssignmentRecord = (from row in entities.ComplianceAssignments
                                                        where row.ComplianceInstanceID == eachRecord.Item1
                                                        && row.RoleID == eachRecord.Item2
                                                        select row).FirstOrDefault();

                            if (prevAssignmentRecord != null)
                            {
                                prevAssignmentRecord.UserID = eachRecord.Item3;
                            }
                        });

                        entities.SaveChanges();
                        saveSuccess = true;
                    }
                }
                return saveSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }
        
        public static List<RLCS_HRApplicableComplianceView> GetApplicableHRCompliancesByCustomerID(int customerID, List<int> lstBranches)
        {
            List<RLCS_HRApplicableComplianceView> lstApplicableCompliances = new List<RLCS_HRApplicableComplianceView>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstApplicableCompliances = (from row in entities.RLCS_HRApplicableComplianceView
                                                where row.CustomerID == customerID
                                                select row).ToList();

                    if (lstApplicableCompliances.Count > 0 && lstBranches.Count > 0)
                        lstApplicableCompliances = lstApplicableCompliances.Where(row => row.AVACOM_BranchID != null && lstBranches.Contains((int)row.AVACOM_BranchID)).ToList();
                }

                return lstApplicableCompliances;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstApplicableCompliances;
            }
        }

        public static User GetUserByID(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var user = (from row in entities.Users
                            where row.ID == userID
                            select row).SingleOrDefault();

                return user;
            }
        }

        public static SP_RLCS_ComplianceInstanceTransactionCount_Result GetPeriodBranchLocation(int customerID, int userID, string profileID, int scheduledOnID, int complianceInstanceID)
        {
            SP_RLCS_ComplianceInstanceTransactionCount_Result result = new SP_RLCS_ComplianceInstanceTransactionCount_Result();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if(scheduledOnID >0 && complianceInstanceID >0)
                {
                    result = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, profileID)
                                 where                                  
                                 row.ComplianceInstanceID == complianceInstanceID
                                 select row).FirstOrDefault();
                    //row.ScheduledOnID == scheduledOnID &&

                }
                else
                {
                    result = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, profileID)
                                 where //row.ScheduledOnID == scheduledOnID
                                  row.ComplianceInstanceID == complianceInstanceID
                                 select row).FirstOrDefault();
                }
                

                return result;
            }
        }

        public static ComplianceInstance GetComplianceInstanceData(int instanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.ComplianceInstances
                                    where row.ID == instanceID && row.IsDeleted == false
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }

        public static List<ComplianceAssignment> GetComplianceAssignment(long complianceID, int customerBranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long complianceInstanceID = (from row in entities.ComplianceInstances
                                             where row.ComplianceId == complianceID
                                             && row.CustomerBranchID == customerBranchID
                                             && row.IsDeleted == false
                                             select row.ID).FirstOrDefault();

                List<ComplianceAssignment> complianceAssignment = null;

                if (complianceInstanceID > 0)
                {
                    complianceAssignment = (from row in entities.ComplianceAssignments
                                            where row.ComplianceInstanceID == complianceInstanceID
                                            select row).ToList();
                }

                return complianceAssignment;
            }
        }

        public static void SaveTempAssignments(List<TempAssignmentTable> lstTempAssignments)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstTempAssignments.ForEach(entry =>
                    {
                        var prevRecord = (from row in entities.TempAssignmentTables
                                          where row.ComplianceId == entry.ComplianceId
                                          && row.CustomerBranchID == entry.CustomerBranchID
                                          && row.RoleID == entry.RoleID
                                          && row.UserID == entry.UserID
                                          select row).FirstOrDefault();

                        if (prevRecord == null)
                        {
                            entities.TempAssignmentTables.Add(entry);
                            entities.SaveChanges();
                        }
                        else
                        {
                            prevRecord.IsActive = entry.IsActive;
                            entities.SaveChanges();
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPFOrESICCodeOrPTState(string clientID, string challanType, string codeValue, string branchType)
        {
            List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (!string.IsNullOrEmpty(challanType))
                    {
                        if (challanType.Trim().ToUpper().Equals("EPF"))
                        {
                            lstCustomerBranches = (from RCB in entities.RLCS_Client_BasicDetails
                                                   join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                   on RCB.CB_ClientID equals RCCM.CM_ClientID
                                                   where RCB.CB_PF_Code != null
                                                   && RCCM.AVACOM_BranchID != null
                                                   && RCB.CB_PF_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                                   && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                   && RCCM.BranchType.Equals(branchType)
                                                   && RCB.IsProcessed == true
                                                   && RCCM.CM_Status == "A"
                                                   select RCCM).Distinct().ToList();
                        }
                        else if (challanType.Trim().ToUpper().Equals("ESI"))
                        {
                            lstCustomerBranches = (from REM in entities.RLCS_Employee_Master
                                                   join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                   on REM.EM_ClientID equals RCCM.CM_ClientID
                                                   where REM.AVACOM_BranchID == RCCM.AVACOM_BranchID
                                                   && RCCM.AVACOM_BranchID != null
                                                   && REM.EM_Client_ESI_Number != null
                                                   && REM.EM_Client_ESI_Number.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                                   && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                   && RCCM.BranchType.Equals(branchType)
                                                   && REM.IsProcessed == true
                                                   && REM.EM_Status == "A"
                                                   && RCCM.CM_Status == "A"
                                                   select RCCM).Distinct().ToList();
                        }
                        else if (challanType.Trim().ToUpper().Equals("PT"))
                        {
                            lstCustomerBranches = (from REM in entities.RLCS_Employee_Master
                                                   join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                   on REM.EM_ClientID equals RCCM.CM_ClientID
                                                   where REM.AVACOM_BranchID == RCCM.AVACOM_BranchID
                                                   && REM.EM_Client_PT_State != null
                                                   && REM.EM_Client_PT_State.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                                   && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                   && RCCM.BranchType.Equals(branchType)
                                                   && REM.IsProcessed == true
                                                   && REM.EM_Status == "A"
                                                   && RCCM.CM_Status == "A"
                                                   select RCCM).Distinct().ToList();
                        }
                    }

                    return lstCustomerBranches;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstCustomerBranches;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPTState(int customerID, string clientID, string ptState, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                //List<CustomerBranch> lstCustomerBranches = new List<CustomerBranch>();

                lstCustomerBranches = (from row in entities.CustomerBranches
                                       join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                       on row.ID equals row1.AVACOM_BranchID
                                       where row1.AVACOM_BranchID != null
                                        && row.CustomerID == customerID
                                       && row1.CL_PT_State.Trim().ToUpper().Equals(ptState.Trim().ToUpper())
                                       && row1.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                       && row1.BranchType.Equals(branchType)
                                       select row1).ToList();
                return lstCustomerBranches;
            }
        }

        public static List<long> GetAll_Registers_Compliance_StateWise(string stateName, string establishmentType, bool? IsCLRAApplicable)
        {
            List<long> lstComplianceList = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRegisters = (from row in entities.RLCS_Register_Compliance_Mapping
                                    where row.AVACOM_ComplianceID != null
                                    && row.RegisterID != null
                                    && row.Register_Status == "A"
                                    select row).ToList();

                if (lstRegisters.Count > 0)
                {
                    if (!string.IsNullOrEmpty(stateName))
                    {
                        lstRegisters = lstRegisters.Where(row => row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper()) || row.StateID.Trim().ToUpper().Equals("CENTR")).ToList();
                    }

                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            else
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            else
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                        }

                        else if (establishmentType.Equals("CLRA"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("CLRA") || row.CLRA_Applicability == "A")
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            //else
                            //{
                            //    lstRegisters = (from row in lstRegisters
                            //                    where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                            //                    && row.Act_Type != null
                            //                    && row.Register_Status == "A"
                            //                    && row.AVACOM_ComplianceID != null
                            //                    select row).ToList();
                            //}
                        }

                        else if (establishmentType.Equals("SF"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                    }

                    lstComplianceList = lstRegisters.Select(row => (long)row.AVACOM_ComplianceID).ToList();
                }

                return lstComplianceList.Distinct().ToList();
            }
        }

        public static RLCS_Register_Compliance_Mapping Get_RegistersComplianceRecordsByRegisterID(int registerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var registerRecord = (from row in entities.RLCS_Register_Compliance_Mapping
                                      where row.RegisterID == registerID
                                      && row.RegisterID != null
                                      && row.AVACOM_ComplianceID != null
                                      && row.Register_Status == "A"
                                      select row).FirstOrDefault();

                return registerRecord;
            }
        }

        public static RLCS_Returns_Compliance_Mapping Get_ReturnsComplianceRecordsByReturnID(int returnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var returnRecord = (from row in entities.RLCS_Returns_Compliance_Mapping
                                    where row.ReturnID == returnID
                                    && row.ReturnID != null
                                    && row.AVACOM_ComplianceID != null
                                    && row.RM_Status == "A"
                                    select row).FirstOrDefault();

                return returnRecord;
            }
        }

        public static List<RLCS_Register_Compliance_Mapping> GetAll_Registers_StateWise(int loggedInUserID, string stateName, string establishmentType, string register_Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRegisters = (from row in entities.RLCS_Register_Compliance_Mapping
                                    where row.AVACOM_ComplianceID != null
                                    && row.RegisterID != null
                                    && row.Register_Status == "A"
                                    select row).ToList();

                if (lstRegisters.Count > 0)
                {
                    if (!string.IsNullOrEmpty(stateName))
                    {
                        lstRegisters = lstRegisters.Where(row => row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper())).ToList();
                    }

                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT"))
                                            && row.Act_Type != null
                                            && row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                                            && row.Act_Type != null
                                            && row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                        else if (establishmentType.Equals("SF"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                    }

                    if (!string.IsNullOrEmpty(register_Type))
                    {
                        lstRegisters = (from row in lstRegisters
                                        where (row.Register_Type.Trim().ToUpper().Equals(register_Type))
                                        && row.Act_Type != null
                                        && row.Register_Status == "A"
                                        && row.AVACOM_ComplianceID != null
                                        select row).ToList();
                    }
                }

                if (lstRegisters.Count > 0 && loggedInUserID != 0)
                {
                    var myAssignedCompliances = GetAll_AssignedCompliances(loggedInUserID);

                    if (myAssignedCompliances != null)
                        if (myAssignedCompliances.Count > 0)
                            lstRegisters = lstRegisters.Where(row => myAssignedCompliances.Contains((long)row.AVACOM_ComplianceID)).ToList();
                }

                return lstRegisters.Distinct().ToList();
            }
        }

        public static List<long> GetAll_AssignedCompliances(int userID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var myAssignedCompliances = (from CI in entities.ComplianceInstances
                                                 join CA in entities.ComplianceAssignments
                                                 on CI.ID equals CA.ComplianceInstanceID
                                                 where CA.UserID == userID
                                                 && CI.IsDeleted == false
                                                 select CI.ComplianceId).Distinct().ToList();

                    return myAssignedCompliances;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        public static List<long> GetAll_ChallanRelatedCompliance(string stateCode, string challanType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstChallanCompliances = (from row in entities.RLCS_Challan_Compliance_Mapping
                                             where row.AVACOM_ComplianceID != null
                                             && row.IsActive == true
                                             && row.AVACOM_ComplianceID != null
                                             select row).ToList();

                if (!string.IsNullOrEmpty(stateCode))
                    lstChallanCompliances = lstChallanCompliances.Where(row => row.State_Code.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())).ToList();

                if (!string.IsNullOrEmpty(challanType))
                    lstChallanCompliances = lstChallanCompliances.Where(row => row.ChallanType.Trim().ToUpper().Equals(challanType.Trim().ToUpper())).ToList();

                return lstChallanCompliances.Select(row => (long)row.AVACOM_ComplianceID).Distinct().ToList();
            }
        }

        public static List<long> GetAll_ReturnsRelatedCompliance_SectionWise(string taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstReturnCompliances = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where
                                            //&&  row.RM_StateID.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())   &&                                 
                                            row.AVACOM_ComplianceID != null
                                            && row.ReturnID != null
                                            && row.RM_Status == "A"
                                            select row).ToList();

                if (!string.IsNullOrEmpty(taskID))
                    lstReturnCompliances = lstReturnCompliances.Where(row => row.RM_SectionID.Trim().ToUpper().Equals(taskID.Trim().ToUpper())).ToList();

                return lstReturnCompliances.Select(row => (long)row.AVACOM_ComplianceID).Distinct().ToList();
            }
        }

        public static List<long> GetAll_ReturnsRelatedCompliance_SectionWise_CLRA(string taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstReturnCompliances = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where
                                            //&&  row.RM_StateID.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())   &&                                 
                                            row.AVACOM_ComplianceID != null
                                            && row.ReturnID != null
                                            && row.RM_Status == "A"
                                            && row.AVACOM_ActGroup == "CLRA"
                                            select row).ToList();

                if (!string.IsNullOrEmpty(taskID))
                    lstReturnCompliances = lstReturnCompliances.Where(row => row.RM_SectionID.Trim().ToUpper().Equals(taskID.Trim().ToUpper())).ToList();

                return lstReturnCompliances.Select(row => (long)row.AVACOM_ComplianceID).Distinct().ToList();
            }
        }

        public static bool Check_Register_Compliance_Map(long complianceID, string stateName, string establishmentType, bool? IsCLRAApplicable = false)
        {
            bool mapCompliance = false;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRegisters = (from row in entities.RLCS_Register_Compliance_Mapping
                                    where row.AVACOM_ComplianceID == complianceID
                                    && row.Register_Status == "A"
                                    select row).ToList();

                if (lstRegisters.Count > 0)
                {
                    if (!string.IsNullOrEmpty(stateName))
                    {
                        lstRegisters = lstRegisters.Where(row => row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper()) || row.StateID.Trim().ToUpper().Equals("CENTR")).ToList();
                    }

                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            else
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            else
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                        }
                        else if (establishmentType.Equals("CLRA"))
                        {
                            if (IsCLRAApplicable == true)
                            {
                                lstRegisters = (from row in lstRegisters
                                                where (row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                && row.Act_Type != null
                                                && row.Register_Status == "A"
                                                && row.AVACOM_ComplianceID != null
                                                select row).ToList();
                            }
                            //else
                            //{
                            //    lstRegisters = (from row in lstRegisters
                            //                    where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                            //                    && row.Act_Type != null
                            //                    && row.Register_Status == "A"
                            //                    && row.AVACOM_ComplianceID != null
                            //                    select row).ToList();
                            //}
                        }

                        else if (establishmentType.Equals("SF"))
                        {
                            lstRegisters = (from row in lstRegisters
                                            where row.Register_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                    }

                    if (lstRegisters.Count > 0)
                        mapCompliance = true;
                }

                return mapCompliance;
            }
        }

        public static List<SP_RLCS_RegisterReturnChallanCompliance_Result> GetHRComplianceList_Assignment(int customerID, string scopeType, List<int> lstBranches, List<string> lstStates, string selectedStateCode, string establishmentType, string branchState, bool setApprover = false, List<int> actIdList = null, string filter = null, bool? IsCLRAApplicable=false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<long> complianceIDs = new List<long>();

                if (!string.IsNullOrEmpty(scopeType))
                {
                    if (scopeType.Trim().ToUpper().Equals("SOW03"))
                    {
                        if (!string.IsNullOrEmpty(establishmentType) && !string.IsNullOrEmpty(branchState))
                        {
                            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType, IsCLRAApplicable);
                        }

                        //if (lstBranches.Count == 1)
                        //{
                        //    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(lstBranches[0]);

                        //    if (custBranchDetails != null)
                        //    {
                        //        if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                        //        {
                        //            establishmentType = custBranchDetails.CM_EstablishmentType;
                        //            branchState = custBranchDetails.CM_State;

                        //            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                        //        }
                        //    }
                        //}

                        //complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                    }
                    else if (scopeType.Trim().ToUpper().Equals("SOW13") || scopeType.Trim().ToUpper().Equals("SOW14")
                        || scopeType.Trim().ToUpper().Equals("SOW15") || scopeType.Trim().ToUpper().Equals("SOW17"))
                    {
                        string challanType = string.Empty;

                        if (scopeType.Trim().ToUpper().Equals("SOW13"))
                            challanType = "EPF";
                        else if (scopeType.Trim().ToUpper().Equals("SOW14"))
                            challanType = "ESI";
                        else if (scopeType.Trim().ToUpper().Equals("SOW15"))
                            challanType = "LWF";
                        else if (scopeType.Trim().ToUpper().Equals("SOW17"))
                            challanType = "PT";

                        complianceIDs = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(string.Empty, challanType);
                    }
                    else if (scopeType.Trim().ToUpper().Equals("SOW05"))
                    {
                        complianceIDs = RLCS_ComplianceManagement.GetAll_ReturnsRelatedCompliance_SectionWise(string.Empty);
                    }
                }




                List<SP_RLCS_RegisterReturnChallanCompliance_Result> hrCompliances = new List<SP_RLCS_RegisterReturnChallanCompliance_Result>();

                if (complianceIDs.Count > 0)
                {
                    hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                                     where row.IsDeleted == false
                                     && row.ScopeID == scopeType                                     
                                     && row.EventFlag == null && row.Status == null
                                     select row).ToList();

                    if (hrCompliances.Count > 0)
                    {
                        var lstApplicableCompliances = RLCS_ComplianceManagement.GetApplicableHRCompliancesByCustomerID(customerID, lstBranches);

                        if (lstApplicableCompliances != null)
                            if (lstApplicableCompliances.Count > 0)
                                hrCompliances.RemoveAll(row => !lstApplicableCompliances.Select(a => a.MasterID).Contains(row.MasterID));
                    }

                    if (actIDs.Count > 0 && hrCompliances.Count > 0)
                    {
                        hrCompliances = (from row in hrCompliances
                                         where actIDs.Contains(row.ActID)
                                         select row).ToList();
                    }

                    if (hrCompliances.Count > 0)
                    {
                        hrCompliances = (from row in hrCompliances
                                         where complianceIDs.Contains(row.ID)
                                         select row).ToList();
                    }

                    if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW03")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where lstStates.Contains(row.StateID)
                                         select row).ToList();
                    }
                    else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW17")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where lstStates.Contains(row.StateID)
                                         select row).ToList();
                    }
                    else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW05")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where (lstStates.Contains(row.StateID) || row.StateID == "Central")
                                         && row.RM_Type == "Return"
                                         select row).ToList();
                    }

                    if (!string.IsNullOrEmpty(selectedStateCode) && selectedStateCode != "-1")
                    {
                        hrCompliances = hrCompliances.Where(row => row.StateID.Equals(selectedStateCode)).ToList();
                    }

                    var TempQuery = (from row in entities.TempAssignmentTables
                                     where lstBranches.Contains(row.CustomerBranchID)
                                     && row.IsActive == true
                                     && row.UserID != -1
                                     select row.ComplianceId).ToList();

                    hrCompliances.RemoveAll(row => TempQuery.Contains(row.ID));

                    var prevMappedCompliances = (from row in entities.ComplianceInstances
                                                 join row1 in entities.ComplianceAssignments
                                                 on row.ID equals row1.ComplianceInstanceID
                                                 where lstBranches.Contains(row.CustomerBranchID)
                                                 && row.IsDeleted == false
                                                 select row.ComplianceId).ToList();

                    hrCompliances.RemoveAll(row => prevMappedCompliances.Contains(row.ID));

                    if (!string.IsNullOrEmpty(filter))
                    {
                        hrCompliances = hrCompliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return hrCompliances.Distinct().OrderBy(row => row.ActID).ToList();
            }
        }


        public static List<SP_RLCS_RegisterReturnChallanCompliance_Result> GetHRComplianceList_Assignment_CLRA(int customerID, string scopeType, List<int> lstBranches, List<string> lstStates, string selectedStateCode, string establishmentType, string branchState, bool setApprover = false, List<int> actIdList = null, string filter = null, bool? IsCLRAApplicable = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<long> complianceIDs = new List<long>();

                if (!string.IsNullOrEmpty(scopeType))
                {
                    if (scopeType.Trim().ToUpper().Equals("SOW03"))
                    {
                        if (!string.IsNullOrEmpty(establishmentType) && !string.IsNullOrEmpty(branchState))
                        {
                            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType, IsCLRAApplicable);
                        }

                        //if (lstBranches.Count == 1)
                        //{
                        //    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(lstBranches[0]);

                        //    if (custBranchDetails != null)
                        //    {
                        //        if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                        //        {
                        //            establishmentType = custBranchDetails.CM_EstablishmentType;
                        //            branchState = custBranchDetails.CM_State;

                        //            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                        //        }
                        //    }
                        //}

                        //complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                    }
                   
                    else if (scopeType.Trim().ToUpper().Equals("SOW05"))
                    {
                        complianceIDs = RLCS_ComplianceManagement.GetAll_ReturnsRelatedCompliance_SectionWise_CLRA(string.Empty);
                    }
                }




                List<SP_RLCS_RegisterReturnChallanCompliance_Result> hrCompliances = new List<SP_RLCS_RegisterReturnChallanCompliance_Result>();

                if (complianceIDs.Count > 0)
                {
                    hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                                     where row.IsDeleted == false
                                     && row.ScopeID == scopeType
                                     && row.EventFlag == null && row.Status == null
                                     select row).ToList();

                    if (hrCompliances.Count > 0)
                    {
                        var lstApplicableCompliances = RLCS_ComplianceManagement.GetApplicableHRCompliancesByCustomerID(customerID, lstBranches);

                        if (lstApplicableCompliances != null)
                            if (lstApplicableCompliances.Count > 0)
                                hrCompliances.RemoveAll(row => !lstApplicableCompliances.Select(a => a.MasterID).Contains(row.MasterID));
                    }

                    if (actIDs.Count > 0 && hrCompliances.Count > 0)
                    {
                        hrCompliances = (from row in hrCompliances
                                         where actIDs.Contains(row.ActID)
                                         select row).ToList();
                    }

                    if (hrCompliances.Count > 0)
                    {
                        hrCompliances = (from row in hrCompliances
                                         where complianceIDs.Contains(row.ID)
                                         select row).ToList();
                    }

                    if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW03")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where lstStates.Contains(row.StateID)
                                         select row).ToList();
                    }
                    else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW17")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where lstStates.Contains(row.StateID)
                                         select row).ToList();
                    }
                    else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW05")))
                    {
                        hrCompliances = (from row in hrCompliances
                                         where (lstStates.Contains(row.StateID) || row.StateID == "Central")
                                         && row.RM_Type == "Return"
                                         select row).ToList();
                    }

                    if (!string.IsNullOrEmpty(selectedStateCode) && selectedStateCode != "-1")
                    {
                        hrCompliances = hrCompliances.Where(row => row.StateID.Equals(selectedStateCode)).ToList();
                    }

                    var TempQuery = (from row in entities.TempAssignmentTables
                                     where lstBranches.Contains(row.CustomerBranchID)
                                     && row.IsActive == true
                                     && row.UserID != -1
                                     select row.ComplianceId).ToList();

                    hrCompliances.RemoveAll(row => TempQuery.Contains(row.ID));

                    var prevMappedCompliances = (from row in entities.ComplianceInstances
                                                 join row1 in entities.ComplianceAssignments
                                                 on row.ID equals row1.ComplianceInstanceID
                                                 where lstBranches.Contains(row.CustomerBranchID)
                                                 && row.IsDeleted == false
                                                 select row.ComplianceId).ToList();

                    hrCompliances.RemoveAll(row => prevMappedCompliances.Contains(row.ID));

                    if (!string.IsNullOrEmpty(filter))
                    {
                        hrCompliances = hrCompliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                    }
                }

                return hrCompliances.Distinct().OrderBy(row => row.ActID).ToList();
            }
        }

        public static List<SP_RLCS_RegisterReturnChallanCompliance_New_Result> GetHRComplianceList_Assignment_New(int customerID, string scopeType, List<int> lstBranches, List<string> lstStates, string selectedStateCode, string establishmentType, string branchState, bool setApprover = false, List<string> actIdList = null, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<int> actIDs = new List<int>();
                List<long> complianceIDs = new List<long>();

                //if (!string.IsNullOrEmpty(scopeType))
                //{
                //    if (scopeType.Trim().ToUpper().Equals("SOW03"))
                //    {
                //        if (!string.IsNullOrEmpty(establishmentType) && !string.IsNullOrEmpty(branchState))
                //        {
                //            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                //        }

                //        //if (lstBranches.Count == 1)
                //        //{
                //        //    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(lstBranches[0]);

                //        //    if (custBranchDetails != null)
                //        //    {
                //        //        if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                //        //        {
                //        //            establishmentType = custBranchDetails.CM_EstablishmentType;
                //        //            branchState = custBranchDetails.CM_State;

                //        //            complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                //        //        }
                //        //    }
                //        //}

                //        //complianceIDs = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(branchState, establishmentType);
                //    }
                //    else if (scopeType.Trim().ToUpper().Equals("SOW13") || scopeType.Trim().ToUpper().Equals("SOW14")
                //        || scopeType.Trim().ToUpper().Equals("SOW15") || scopeType.Trim().ToUpper().Equals("SOW17"))
                //    {
                //        string challanType = string.Empty;

                //        if (scopeType.Trim().ToUpper().Equals("SOW13"))
                //            challanType = "EPF";
                //        else if (scopeType.Trim().ToUpper().Equals("SOW14"))
                //            challanType = "ESI";
                //        else if (scopeType.Trim().ToUpper().Equals("SOW15"))
                //            challanType = "LWF";
                //        else if (scopeType.Trim().ToUpper().Equals("SOW17"))
                //            challanType = "PT";

                //        complianceIDs = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(string.Empty, challanType);
                //    }
                //    else if (scopeType.Trim().ToUpper().Equals("SOW05"))
                //    {
                //        complianceIDs = RLCS_ComplianceManagement.GetAll_ReturnsRelatedCompliance_SectionWise(string.Empty);
                //    }
                //}

                List<SP_RLCS_RegisterReturnChallanCompliance_New_Result> hrCompliances = new List<SP_RLCS_RegisterReturnChallanCompliance_New_Result>();
                //List<SP_RLCS_RegisterReturnChallanCompliance_Result> hrCompliances = new List<SP_RLCS_RegisterReturnChallanCompliance_Result>();

                //if (complianceIDs.Count > 0)
                //{

                hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance_New(customerID)
                                     //hrCompliances = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance() 
                                 where row.IsDeleted == false
                                 && row.ScopeID == scopeType
                                 && row.EventFlag == null && row.Status == null
                                 select row).ToList();

                if (hrCompliances.Count > 0)
                {
                    var lstApplicableCompliances = RLCS_ComplianceManagement.GetApplicableHRCompliancesByCustomerID(customerID, lstBranches);

                    if (lstApplicableCompliances != null)
                        if (lstApplicableCompliances.Count > 0)
                            hrCompliances.RemoveAll(row => !lstApplicableCompliances.Select(a => a.MasterID).Contains(row.MasterID));
                }

                if (actIDs.Count > 0 && hrCompliances.Count > 0)
                {
                    hrCompliances = (from row in hrCompliances
                                     where actIDs.Contains(row.ActID)
                                     select row).ToList();
                }

                //if (hrCompliances.Count > 0)
                //{
                //    hrCompliances = (from row in hrCompliances
                //                     where complianceIDs.Contains(row.ID)
                //                     select row).ToList();
                //}

                //if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW03")))
                //{
                //    hrCompliances = (from row in hrCompliances
                //                     where lstStates.Contains(row.StateID)
                //                     select row).ToList();
                //}
                //else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW17")))
                //{
                //    hrCompliances = (from row in hrCompliances
                //                     where lstStates.Contains(row.StateID)
                //                     select row).ToList();
                //}
                //else if (lstStates.Count > 0 && (scopeType.Trim().ToUpper().Equals("SOW05")))
                //{
                //    hrCompliances = (from row in hrCompliances
                //                     where (lstStates.Contains(row.StateID) || row.StateID == "Central")
                //                     && row.RM_Type == "Return"
                //                     select row).ToList();
                //}

                hrCompliances = (from row in hrCompliances
                                 where scopeType.Contains(row.ScopeID) 
                                 && lstBranches.Contains(Convert.ToInt32(row.AVACOM_BranchID))
                                 && row.RM_Type != "Covering Letter"
                                 select row).ToList();

                if (actIdList.Count > 0)
                {
                    hrCompliances = (from row in hrCompliances
                                     where actIdList.Contains(row.ActGroup) 
                                     select row).ToList();
                }

                if (lstStates.Count > 0)
                {
                    hrCompliances = (from row in hrCompliances
                                     where (lstStates.Contains(row.StateID) || row.StateID == "Central")
                                     select row).ToList();
                }


                if (!string.IsNullOrEmpty(selectedStateCode) && selectedStateCode != "-1")
                {
                    hrCompliances = hrCompliances.Where(row => row.StateID.Equals(selectedStateCode)).ToList();
                }

                var TempQuery = (from row in entities.TempAssignmentTables
                                 where lstBranches.Contains(row.CustomerBranchID)
                                 && row.IsActive == true
                                 && row.UserID != -1
                                 select new { row.ComplianceId, row.CustomerBranchID }).ToList();

                //hrCompliances.RemoveAll(row => TempQuery.Contains(row.ID));
                foreach (var TempQry in TempQuery)
                {
                    hrCompliances.RemoveAll(row => row.ID == TempQry.ComplianceId && row.AVACOM_BranchID == TempQry.CustomerBranchID);
                }

                var prevMappedCompliances = (from row in entities.ComplianceInstances
                                             join row1 in entities.ComplianceAssignments
                                             on row.ID equals row1.ComplianceInstanceID
                                             where lstBranches.Contains(row.CustomerBranchID)
                                             && row.IsDeleted == false
                                             select new { row.ComplianceId, row.CustomerBranchID }).ToList();

                foreach (var prevMappedCompliance in prevMappedCompliances)
                {
                    hrCompliances.RemoveAll(row => row.ID == prevMappedCompliance.ComplianceId && row.AVACOM_BranchID == prevMappedCompliance.CustomerBranchID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    hrCompliances = hrCompliances.Where(entry => entry.ShortDescription.ToUpper().Contains(filter.ToUpper()) || entry.Sections.ToUpper().Contains(filter.ToUpper()) || entry.RequiredForms.ToUpper().Contains(filter.ToUpper())).ToList();
                }
                //}

                return hrCompliances.Distinct().OrderBy(row => row.AVACOM_BranchID).ToList();
            }
        }


        #region RLCS Document Generation

        public static bool Save_GeneratedRegisters(int customerID, string scopeID, string month, string year, string clientID, string stateCode, string city, string branchName, RLCS_Register_Compliance_Mapping lstRegisters, string documentPath)
        {
            try
            {
                bool processSuccess = false;

                RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails = null;

                if (!string.IsNullOrEmpty(clientID))
                {
                    if (customerID != 0)
                    {
                        //Register
                        #region Register

                        if (scopeID.Trim().ToUpper().Equals("SOW03"))
                        {
                            customerBranchDetails = RLCS_ClientsManagement.GetClientLocationDetails(clientID, stateCode, city, branchName, "B");

                            if (customerBranchDetails != null && customerBranchDetails.CM_EstablishmentType != null)
                            {
                                //Get All Compliances To Map  
                                //Commented by shraddha ...
                               // var lstRegisterComplianceRecord = Get_RegistersComplianceRecordsByRegisterID(generatedRegID);

                                //var lstRegisterMappedCompliances = RLCS_ComplianceManagement.GetAll_Registers_Compliance_StateWise(customerBranchDetails.CM_State, customerBranchDetails.CM_EstablishmentType.Trim().ToUpper());

                                if (lstRegisters != null)
                                {
                                    if (lstRegisters.AVACOM_ComplianceID != null)
                                    {
                                        processSuccess = Generate_InstanceAssignmentScheduleTransaction_Register(customerID, month, year, scopeID, customerBranchDetails, lstRegisters, documentPath);
                                        
                                        //Deduction 
                                        if (processSuccess)
                                        {
                                            if (customerBranchDetails.AVACOM_BranchID != null)
                                                PaymentManagement.ProcessPaymentDeduction_New(customerID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID), month, year);
                                            
                                                //PaymentManagement.ProcessPaymentDeduction(customerID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID), month, year);
                                        }
                                    }
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "lstRegisterMappedCompliances=null-EstablishmentType-" + customerBranchDetails.CM_EstablishmentType,
                                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            else
                                ContractManagement.InsertErrorMsg_DBLog("", "customerBranchDetails=null",
                                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        #endregion
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "customerID=0",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "objRecord.LM_ClientID=NULL",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Generate_InstanceAssignmentScheduleTransaction_Register(int customerID, string month, string year, string scopeID,
            RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails, RLCS_Register_Compliance_Mapping objRegDetailRecord, string documentPath)
        {
            bool processSuccess = false;
            try
            {
                if (customerBranchDetails.AVACOM_BranchID != null && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    DateTime dtStartDate = new DateTime();

                    if (customerBranchDetails.CM_ServiceStartDate != null)
                        dtStartDate = Convert.ToDateTime(customerBranchDetails.CM_ServiceStartDate);
                    else
                        dtStartDate = Convert.ToDateTime("2018-01-01");

                    int customerBranchID = Convert.ToInt32(customerBranchDetails.AVACOM_BranchID);

                    List<long> lstPerformers = new List<long>();
                    List<long> lstReviewers = new List<long>();

                    bool saveAsAVACOMDocumentSuccess = false;

                    //Each Compliance Generate ComplianceInstance and Assignment Records 

                    long complianceID = Convert.ToInt64(objRegDetailRecord.AVACOM_ComplianceID);

                    //Get Assignment Compliance Wise
                    var complianceAssignments = GetComplianceAssignment(complianceID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID));

                    if (complianceAssignments != null)
                    {

                        if (complianceAssignments.Count > 0)
                        {
                            lstPerformers = complianceAssignments.Where(row => row.RoleID == 3).Select(row => row.UserID).ToList();
                            lstReviewers = complianceAssignments.Where(row => row.RoleID == 4).Select(row => row.UserID).ToList();

                            if (lstPerformers.Count > 0 || lstReviewers.Count > 0)
                            {
                                long complianceInstanceID = 0;
                                long complianceScheduleOnID = 0;
                                long complianceTxnID = 0;

                                string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");
                                DateTime ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                #region Compliance Instance Assignment

                                //Performer
                                lstPerformers.ForEach(eachPerformer =>
                                {
                                    complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3);

                                    if (complianceInstanceID == 0)
                                        complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3, ScheduledOn);
                                });

                                //Reviewer
                                lstReviewers.ForEach(eachReviewer =>
                                {
                                    complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4);

                                    if (complianceInstanceID == 0)
                                        complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4, ScheduledOn);
                                });

                                #endregion

                                #region Compliance Schedule

                                if (complianceInstanceID != 0)
                                {
                                    complianceScheduleOnID = RLCS_ComplianceManagement.ExistsComplianceSchedule(complianceInstanceID, month, year);

                                    DateTime? rlcsActivityEndDate = DateTime.Now;

                                    //DateTime? rlcsActivityEndDate = null;
                                    //if (objRecord.LM_ActivityTo != null)
                                    //    rlcsActivityEndDate = Convert.ToDateTime(objRecord.LM_ActivityTo);

                                    if (complianceScheduleOnID == 0)
                                        complianceScheduleOnID = RLCS_ComplianceScheduleManagement.CreateScheduleOn(ScheduledOn, complianceInstanceID, complianceID, 1, "Admin User", month, year, rlcsActivityEndDate); //TBD  
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "complianceInstanceID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                #endregion

                                #region Document Download

                                if (complianceScheduleOnID > 0)
                                {
                                    //complianceTxnID = RLCS_ComplianceScheduleManagement.CloseCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, objRecord);
                                    //complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);
                                    complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitOrCloseCompliance(true, complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);

                                    if (complianceTxnID != 0)
                                    {
                                        //Download Document
                                        bool downloadSuccess = false;

                                        string downloadFilePath = string.Empty;
                                        string fileName = string.Empty;

                                        string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"] + "Masters/";

                                        List<Tuple<string, int>> lstPath = new List<Tuple<string, int>>();

                                        if (documentPath != null) //2--Working Documents, 1--Compliance Document
                                        {
                                            if (!string.IsNullOrEmpty(documentPath))
                                                lstPath.Add(new Tuple<string, int>(documentPath.Replace("|", ""), 1));
                                        }

                                        if (scopeID.Trim().ToUpper().Equals("SOW03")) //Register
                                        {
                                            bool ISEmpWiseRegister = false;
                                            if (objRegDetailRecord != null)
                                            {
                                                if (objRegDetailRecord.Register_Type.Trim().Equals("I") || objRegDetailRecord.Register_Type.Trim().Equals("IN"))
                                                {
                                                    ISEmpWiseRegister = true;
                                                    //fileName = objRegDetailRecord.StateID + " " + objRegDetailRecord.Register_Name + ".zip";
                                                    fileName = objRegDetailRecord.Register_Name + ".zip";
                                                }
                                                else
                                                {
                                                    ISEmpWiseRegister = false;
                                                    fileName = objRegDetailRecord.StateID + " " + objRegDetailRecord.Register_Name + ".xls";
                                                }

                                                lstPath.ForEach(eachPath =>
                                                {
                                                    downloadFilePath = string.Empty;

                                                    if (ISEmpWiseRegister)
                                                    {
                                                        downloadFilePath += rlcsAPIURL + "GenerateZipFilesEmployeewise?FilePath=" + eachPath.Item1;
                                                    }
                                                    else
                                                    {
                                                        downloadFilePath += rlcsAPIURL + "Generate?FilePath=" + eachPath.Item1;
                                                    }

                                                    //if (ISEmpWiseRegister)
                                                    //    fileName = fileName + ".zip";

                                                    if (!string.IsNullOrEmpty(downloadFilePath) && !string.IsNullOrEmpty(fileName))
                                                    {
                                                        string downloadedFilePath = string.Empty;
                                                        downloadSuccess = RLCS_DocumentManagement.DownloadFileAndSaveDocuments(downloadFilePath, fileName, out downloadedFilePath);

                                                        if (downloadSuccess && !string.IsNullOrEmpty(downloadedFilePath))
                                                        {
                                                            saveAsAVACOMDocumentSuccess = RLCS_DocumentManagement.SaveAsAVACOMDocument(downloadedFilePath, customerID, customerBranchID,
                                                                complianceID, complianceInstanceID, complianceScheduleOnID, complianceTxnID, eachPath.Item2);

                                                            processSuccess = saveAsAVACOMDocumentSuccess;
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                    else
                                        ContractManagement.InsertErrorMsg_DBLog("", "complianceTxnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "complianceScheduleOnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                #endregion
                            }
                        }
                        else
                            ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                                MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "FirstConditionFalse",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return processSuccess;
            }
        }

        public static bool Generate_Challans(int customerID, string challanType, string month, string year, string clientID, string codeValue, List<string> documentPath,out string messge)
        {
            try
            {
                bool processSuccess = false;
                string msg = "";
                messge = "";
                Tuple<bool, string> tpl = new Tuple<bool, string>(false, "");
                
                if (!string.IsNullOrEmpty(clientID))
                {
                    if (customerID != 0)
                    {
                        //Challan
                        #region Challan

                        if (challanType.Trim().ToUpper().Contains("PF") || challanType.Trim().ToUpper().Equals("ESI") || challanType.Trim().ToUpper().Equals("PT"))
                        {
                            List<long> lstChallan_Compliances = new List<long>();
                            List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

                            if (challanType.Trim().ToUpper().Contains("PF")) //PF
                            {
                                challanType = "EPF";
                                lstChallan_Compliances = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(string.Empty, challanType);
                                lstCustomerBranches = RLCS_ComplianceManagement.GetCustomerBranchesByPFOrESICCodeOrPTState(clientID, challanType, codeValue, "E");
                            }
                            else if (challanType.Trim().ToUpper().Equals("ESI")) //ESI
                            {
                                challanType = "ESI";
                                lstChallan_Compliances = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(string.Empty, challanType);
                                lstCustomerBranches = RLCS_ComplianceManagement.GetCustomerBranchesByPFOrESICCodeOrPTState(clientID, challanType, codeValue, "B");
                            }
                            else if (challanType.Trim().ToUpper().Equals("PT")) //PT
                            {
                                challanType = "PT";
                                lstChallan_Compliances = RLCS_ComplianceManagement.GetAll_ChallanRelatedCompliance(codeValue, challanType);
                                lstCustomerBranches = RLCS_ComplianceManagement.GetCustomerBranchesByPFOrESICCodeOrPTState(clientID, challanType, codeValue, "B");
                            }

                            if (lstChallan_Compliances.Count > 0)
                            {
                                if (lstCustomerBranches != null)
                                {
                                    if (lstCustomerBranches.Count > 0)
                                    {
                                        lstCustomerBranches.ForEach(eachCustomerBranch =>
                                        {
                                           tpl  = Generate_InstanceAssignmentScheduleTransaction_Challan(customerID, month, year, eachCustomerBranch, lstChallan_Compliances, documentPath);
                                            
                                            if(tpl.Item1)
                                            {
                                                if (eachCustomerBranch.AVACOM_BranchID != null)
                                                    PaymentManagement.ProcessPaymentDeduction_New(customerID, Convert.ToInt32(eachCustomerBranch.AVACOM_BranchID), month, year);
                                                //PaymentManagement.ProcessPaymentDeduction(customerID, Convert.ToInt32(eachCustomerBranch.AVACOM_BranchID), month, year);
                                            }
                                        });
                                    }
                                    else
                                        ContractManagement.InsertErrorMsg_DBLog("", "NoBranch(s)Found-ClientID-" + clientID + "-ChallanType-" + challanType +
                                            "-Code-" + codeValue, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "NoBranch(s)Found-ClientID-" + clientID + "-ChallanType-" + challanType +
                                        "-Code-" + codeValue, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                messge = tpl.Item2;
                                if (messge == "")
                                {
                                    processSuccess = true;
                                }
                               
                            }
                            else
                                ContractManagement.InsertErrorMsg_DBLog("", challanType.Trim() + "-lstChallan_Compliances.Count=0",
                                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        #endregion
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "customerID=0",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "objRecord.LM_ClientID=NULL",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                messge = "";
                return false;
            }
        }

        public static Tuple<bool,string> Generate_InstanceAssignmentScheduleTransaction_Challan(int customerID, string month, string year,
           RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails, List<long> lstChallanCompliances, List<string> lstDocumentPath)
        {
            bool processSuccess = false;
            string msg = "";
            try
            {
                if (customerBranchDetails.AVACOM_BranchID != null && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    DateTime dtStartDate = new DateTime();

                    if (customerBranchDetails.CM_ServiceStartDate != null)
                        dtStartDate = Convert.ToDateTime(customerBranchDetails.CM_ServiceStartDate);
                    else
                        dtStartDate = Convert.ToDateTime("2018-01-01");

                    int customerBranchID = Convert.ToInt32(customerBranchDetails.AVACOM_BranchID);

                    List<long> lstPerformers = new List<long>();
                    List<long> lstReviewers = new List<long>();

                    bool saveAsAVACOMDocumentSuccess = false;

                    if (lstChallanCompliances.Count > 0)
                    {
                        lstChallanCompliances.ForEach(eachChallanCompliance =>
                        {
                            //Each Compliance Generate ComplianceInstance and Assignment Records 
                            long complianceID = eachChallanCompliance;

                            //Get Assignment Compliance Wise
                            var complianceAssignments = GetComplianceAssignment(complianceID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID));

                            if (complianceAssignments != null)
                            {

                                if (complianceAssignments.Count > 0)
                                {
                                    lstPerformers = complianceAssignments.Where(row => row.RoleID == 3).Select(row => row.UserID).ToList();
                                    lstReviewers = complianceAssignments.Where(row => row.RoleID == 4).Select(row => row.UserID).ToList();

                                    if (lstPerformers.Count > 0 && lstReviewers.Count > 0)
                                    {
                                        long complianceInstanceID = 0;
                                        long complianceScheduleOnID = 0;
                                        long complianceTxnID = 0;

                                        string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");
                                        DateTime ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                        #region Compliance Instance Assignment

                                        //Performer
                                        lstPerformers = lstPerformers.Take(1).ToList();
                                        lstPerformers.ForEach(eachPerformer =>
                                        {
                                            complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3);

                                            if (complianceInstanceID == 0)
                                                complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3, ScheduledOn);
                                        });

                                        //Reviewer
                                        lstReviewers = lstReviewers.Take(1).ToList();
                                        lstReviewers.ForEach(eachReviewer =>
                                        {
                                            complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4);

                                            if (complianceInstanceID == 0)
                                                complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4, ScheduledOn);
                                        });

                                        #endregion

                                        #region Compliance Schedule

                                        DateTime scheduleOnDate = DateTime.Now;

                                        if (complianceInstanceID != 0)
                                        {
                                            var complianceScheduleOnRecord = RLCS_ComplianceManagement.ExistsComplianceScheduleRecord(complianceInstanceID, month, year);
                                            //complianceScheduleOnID = RLCS_ComplianceManagement.ExistsComplianceSchedule(complianceInstanceID, month, year);

                                            if (complianceScheduleOnRecord != null)
                                            {
                                                complianceScheduleOnID = complianceScheduleOnRecord.ID;
                                                scheduleOnDate = complianceScheduleOnRecord.ScheduleOn;

                                                DateTime? rlcsActivityEndDate = null;

                                                //DateTime? rlcsActivityEndDate = null;
                                                //if (objRecord.LM_ActivityTo != null)
                                                //    rlcsActivityEndDate = Convert.ToDateTime(objRecord.LM_ActivityTo);

                                                if (complianceScheduleOnID == 0)
                                                    complianceScheduleOnID = RLCS_ComplianceScheduleManagement.CreateScheduleOn(ScheduledOn, complianceInstanceID, complianceID, 1, "Admin User", month, year, rlcsActivityEndDate); //TBD  
                                            }
                                        }
                                        else
                                            ContractManagement.InsertErrorMsg_DBLog("", "complianceInstanceID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                        #endregion

                                        #region Document Download

                                        if (complianceScheduleOnID > 0)
                                        {
                                            //complianceTxnID = RLCS_ComplianceScheduleManagement.CloseCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, objRecord);
                                            //complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, scheduleOnDate, complianceAssignments);
                                            complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitOrCloseCompliance(false, complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);

                                            if (complianceTxnID != 0)
                                            {
                                                //Download Document
                                                bool downloadSuccess = false;

                                                string downloadFilePath = string.Empty;
                                                string fileName = string.Empty;

                                                string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"] + "Masters/";

                                                lstDocumentPath.ForEach(eachPath =>
                                                {
                                                    downloadFilePath = string.Empty;

                                                    fileName = new DirectoryInfo(eachPath).Name;
                                                    downloadFilePath += rlcsAPIURL + "Generate?FilePath=" + eachPath;

                                                    //downloadFilePath = downloadFilePath + @"\" + fileName;                                                   

                                                    if (!string.IsNullOrEmpty(downloadFilePath) && !string.IsNullOrEmpty(fileName))
                                                    {
                                                        string downloadedFilePath = string.Empty;
                                                        downloadSuccess = RLCS_DocumentManagement.DownloadFileAndSaveDocuments(downloadFilePath, fileName, out downloadedFilePath);

                                                        if (downloadSuccess && !string.IsNullOrEmpty(downloadedFilePath))
                                                        {
                                                            saveAsAVACOMDocumentSuccess = RLCS_DocumentManagement.SaveAsAVACOMDocument(downloadedFilePath, customerID, customerBranchID,
                                                                complianceID, complianceInstanceID, complianceScheduleOnID, complianceTxnID, 2);
                                                        }

                                                        if (saveAsAVACOMDocumentSuccess)
                                                            processSuccess = saveAsAVACOMDocumentSuccess;
                                                        else
                                                            ContractManagement.InsertErrorMsg_DBLog("", "DownloadError-" + eachPath, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                    }
                                                });
                                            }
                                            else
                                                ContractManagement.InsertErrorMsg_DBLog("", "complianceTxnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                        else
                                        {
                                            ContractManagement.InsertErrorMsg_DBLog("", "complianceScheduleOnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                            msg = "There are no schedules for selected criteria,Please try later";
                                        }

                                        #endregion
                                    }
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            else
                                ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        });
                    }
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "FirstConditionFalse",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return Tuple.Create(processSuccess,msg);
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return Tuple.Create(false,msg);
            }
        }

        public static bool Save_GeneratedReturns(int customerID, int branchID, string month, string year, RLCS_Returns_Compliance_Mapping returnMasterRecord, string documentPath)
        {
            try
            {
                bool processSuccess = false;

                RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails = null;

                if (customerID != 0)
                {
                    customerBranchDetails = RLCS_ClientsManagement.GetClientLocationDetails(branchID, "B");

                    if (customerBranchDetails != null)
                    {
                        if (returnMasterRecord != null)
                        {
                            if (returnMasterRecord.AVACOM_ComplianceID != null)
                            {
                                processSuccess = Generate_InstanceAssignmentScheduleTransaction_Returns(customerID, month, year, customerBranchDetails, returnMasterRecord, documentPath);

                                //Deduction 
                                if (processSuccess)
                                {
                                    PaymentManagement.ProcessPaymentDeduction_New(customerID, branchID, month, year);

                                    //PaymentManagement.ProcessPaymentDeduction(customerID, branchID, month, year);
                                }
                            }
                        }
                        else
                            ContractManagement.InsertErrorMsg_DBLog("", "returnMasterRecord=null",
                                MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "customerBranchDetails=null",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "customerID=0",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool Generate_InstanceAssignmentScheduleTransaction_Returns(int customerID, string month, string year,
            RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails, RLCS_Returns_Compliance_Mapping objRetDetailRecord, string documentPath)
        {
            bool processSuccess = false;
            try
            {
                if (customerBranchDetails.AVACOM_BranchID != null && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    DateTime dtStartDate = new DateTime();

                    if (customerBranchDetails.CM_ServiceStartDate != null)
                        dtStartDate = Convert.ToDateTime(customerBranchDetails.CM_ServiceStartDate);
                    else
                        dtStartDate = Convert.ToDateTime("2018-01-01");

                    int customerBranchID = Convert.ToInt32(customerBranchDetails.AVACOM_BranchID);

                    List<long> lstPerformers = new List<long>();
                    List<long> lstReviewers = new List<long>();

                    bool saveAsAVACOMDocumentSuccess = false;

                    //Each Compliance Generate ComplianceInstance and Assignment Records 

                    long complianceID = Convert.ToInt64(objRetDetailRecord.AVACOM_ComplianceID);

                    //Get Assignment Compliance Wise
                    var complianceAssignments = GetComplianceAssignment(complianceID, Convert.ToInt32(customerBranchDetails.AVACOM_BranchID));

                    if (complianceAssignments != null)
                    {
                        if (complianceAssignments.Count > 0)
                        {
                            lstPerformers = complianceAssignments.Where(row => row.RoleID == 3).Select(row => row.UserID).ToList();
                            lstReviewers = complianceAssignments.Where(row => row.RoleID == 4).Select(row => row.UserID).ToList();

                            if (lstPerformers.Count > 0 || lstReviewers.Count > 0)
                            {
                                long complianceInstanceID = 0;
                                long complianceScheduleOnID = 0;
                                long complianceTxnID = 0;

                                string StartDate = Convert.ToDateTime(dtStartDate).ToString("dd-MM-yyyy");
                                DateTime ScheduledOn = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                                #region Compliance Instance Assignment

                                //Performer
                                lstPerformers.ForEach(eachPerformer =>
                                {
                                    complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3);

                                    if (complianceInstanceID == 0)
                                        complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachPerformer), 3, ScheduledOn);
                                });

                                //Reviewer
                                lstReviewers.ForEach(eachReviewer =>
                                {
                                    complianceInstanceID = RLCS_ComplianceManagement.ExistsComplianceInstanceAssignment(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4);

                                    if (complianceInstanceID == 0)
                                        complianceInstanceID = RLCS_ComplianceScheduleManagement.Generate_InstanceAssignments(complianceID, customerBranchID, Convert.ToInt32(eachReviewer), 4, ScheduledOn);
                                });

                                #endregion

                                #region Compliance Schedule

                                if (complianceInstanceID != 0)
                                {
                                    complianceScheduleOnID = RLCS_ComplianceManagement.ExistsComplianceSchedule(complianceInstanceID, month, year);

                                    DateTime? rlcsActivityEndDate = DateTime.Now;

                                    //DateTime? rlcsActivityEndDate = null;
                                    //if (objRecord.LM_ActivityTo != null)
                                    //    rlcsActivityEndDate = Convert.ToDateTime(objRecord.LM_ActivityTo);

                                    if (complianceScheduleOnID == 0)
                                        complianceScheduleOnID = RLCS_ComplianceScheduleManagement.CreateScheduleOn(ScheduledOn, complianceInstanceID, complianceID, 1, "Admin User", month, year, rlcsActivityEndDate); //TBD  
                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "complianceInstanceID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                #endregion

                                #region Document Download

                                if (complianceScheduleOnID > 0)
                                {
                                    //complianceTxnID = RLCS_ComplianceScheduleManagement.CloseCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, objRecord);
                                    //complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitCompliance(complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);
                                    complianceTxnID = RLCS_ComplianceScheduleManagement.SubmitOrCloseCompliance(false, complianceID, complianceInstanceID, complianceScheduleOnID, DateTime.Now, complianceAssignments);

                                    if (complianceTxnID != 0)
                                    {
                                        //Download Document
                                        bool downloadSuccess = false;

                                        string downloadFilePath = string.Empty;
                                        string fileName = string.Empty;

                                        string rlcsAPIURL = ConfigurationSettings.AppSettings["RLCSAPIURL"] + "Masters/";

                                        List<Tuple<string, int>> lstPath = new List<Tuple<string, int>>();

                                        if (documentPath != null) //2--Working Documents, 1--Compliance Document
                                        {
                                            if (!string.IsNullOrEmpty(documentPath))
                                                lstPath.Add(new Tuple<string, int>(documentPath.Replace("|", ""), 2));
                                        }


                                        bool ISEmpWiseRegister = false;
                                        if (objRetDetailRecord != null)
                                        {
                                            if (objRetDetailRecord.RM_Return_Category.Trim().Equals("I") || objRetDetailRecord.RM_Return_Category.Trim().Equals("IN"))
                                            {
                                                ISEmpWiseRegister = true;
                                                //fileName = objRegDetailRecord.StateID + " " + objRegDetailRecord.Register_Name + ".zip";
                                                fileName = objRetDetailRecord.RM_Name + ".zip";
                                            }
                                            else
                                            {
                                                ISEmpWiseRegister = false;
                                                if (!string.IsNullOrEmpty(objRetDetailRecord.RM_Doc_Format))
                                                {
                                                    if (objRetDetailRecord.RM_Doc_Format.Trim().ToUpper().Equals("DOC"))
                                                        fileName = objRetDetailRecord.RM_Name + ".doc";
                                                    else if (objRetDetailRecord.RM_Doc_Format.Trim().ToUpper().Equals("EXCEL"))
                                                        fileName = objRetDetailRecord.RM_Name + ".xls";
                                                }
                                            }

                                            lstPath.ForEach(eachPath =>
                                            {
                                                downloadFilePath = string.Empty;

                                                if (ISEmpWiseRegister)
                                                {
                                                    downloadFilePath += rlcsAPIURL + "GenerateZipFilesEmployeewise?FilePath=" + eachPath.Item1;
                                                }
                                                else
                                                {
                                                    downloadFilePath += rlcsAPIURL + "Generate?FilePath=" + eachPath.Item1;
                                                }

                                                //if (ISEmpWiseRegister)
                                                //    fileName = fileName + ".zip";

                                                if (!string.IsNullOrEmpty(downloadFilePath) && !string.IsNullOrEmpty(fileName))
                                                {
                                                    string downloadedFilePath = string.Empty;
                                                    downloadSuccess = RLCS_DocumentManagement.DownloadFileAndSaveDocuments(downloadFilePath, fileName, out downloadedFilePath);

                                                    if (downloadSuccess && !string.IsNullOrEmpty(downloadedFilePath))
                                                    {
                                                        saveAsAVACOMDocumentSuccess = RLCS_DocumentManagement.SaveAsAVACOMDocument(downloadedFilePath, customerID, customerBranchID,
                                                            complianceID, complianceInstanceID, complianceScheduleOnID, complianceTxnID, eachPath.Item2);

                                                        processSuccess = saveAsAVACOMDocumentSuccess;
                                                    }
                                                }
                                            });
                                        }

                                    }
                                    else
                                        ContractManagement.InsertErrorMsg_DBLog("", "complianceTxnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                }
                                else
                                    ContractManagement.InsertErrorMsg_DBLog("", "complianceScheduleOnID=0", MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                                #endregion
                            }
                        }
                        else
                            ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                                MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    else
                        ContractManagement.InsertErrorMsg_DBLog("", "lstPerformers.Count=0||lstReviewers.Count=0",
                            MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                else
                    ContractManagement.InsertErrorMsg_DBLog("", "FirstConditionFalse",
                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return processSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return processSuccess;
            }
        }

        public static long ExistsComplianceInstanceAssignment(long complianceID, int customerBranchID, int userID, int roleID)
        {
            long complianceInstanceID = 0;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    complianceInstanceID = (from CI in entities.ComplianceInstances
                                            join CA in entities.ComplianceAssignments
                                            on CI.ID equals CA.ComplianceInstanceID
                                            where CI.ComplianceId == complianceID
                                            && CI.CustomerBranchID == customerBranchID
                                            && CA.UserID == userID
                                            && CA.RoleID == roleID
                                            && CI.IsDeleted == false
                                            select CI.ID).FirstOrDefault();

                    return complianceInstanceID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return complianceInstanceID;
            }
        }

        public static long ExistsComplianceSchedule(long complianceInstanceID, string rlcsPayrollMonth, string rlcsPayrollYear)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var existComplianceSchduleRecord = (from CSO in entities.ComplianceScheduleOns
                                                        where CSO.ComplianceInstanceID == complianceInstanceID
                                                        && CSO.RLCS_PayrollMonth.Trim().ToUpper().Equals(rlcsPayrollMonth.Trim().ToUpper())
                                                        && CSO.RLCS_PayrollYear.Trim().ToUpper().Equals(rlcsPayrollYear.Trim().ToUpper())
                                                        && CSO.IsActive == true && CSO.IsUpcomingNotDeleted == true
                                                        select CSO).FirstOrDefault();

                    if (existComplianceSchduleRecord != null)
                    {
                        return existComplianceSchduleRecord.ID;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static ComplianceScheduleOn ExistsComplianceScheduleRecord(long complianceInstanceID, string rlcsPayrollMonth, string rlcsPayrollYear)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var existComplianceSchduleRecord = (from CSO in entities.ComplianceScheduleOns
                                                        where CSO.ComplianceInstanceID == complianceInstanceID
                                                        && CSO.RLCS_PayrollMonth.Trim().ToUpper().Equals(rlcsPayrollMonth.Trim().ToUpper())
                                                        && CSO.RLCS_PayrollYear.Trim().ToUpper().Equals(rlcsPayrollYear.Trim().ToUpper())
                                                        && CSO.IsActive == true && CSO.IsUpcomingNotDeleted == true
                                                        select CSO).FirstOrDefault();

                    return existComplianceSchduleRecord;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Result> GetRLCSMyWorkspace(int customerID, int userID, string queryStringFlag, int risk,string month,int year, string status, int location, string stringSearchText, string complianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_RLCS_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Result>();

                int reviewerRoleID = 4;

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Upcoming")
                {
                    //ComplianceInstanceTransactionViews
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where 
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted" || status == "Overdue")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "ClosedTimely")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 4)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "ClosedDelayed")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                         //row.UserID == userID && row.RoleID == reviewerRoleID &&
                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status.Trim().ToUpper().Equals("0")) //0-ALL
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where
                                          //row.UserID == userID
                                          row.IsActive == true && row.IsUpcomingNotDeleted == true                                          
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (!string.IsNullOrEmpty(complianceType))
                {
                    List<long> lstComplianceIDs = new List<long>();

                    if (complianceType.Trim().ToUpper().Equals("SOW03"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.RegisterID != null
                                            && row.Register_Status == "A"
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }
                    else if (complianceType.Trim().ToUpper().Equals("EPF") || complianceType.Trim().ToUpper().Equals("ESI") || complianceType.Trim().ToUpper().Equals("PT"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.IsActive == true
                                            && row.ChallanType.Equals(complianceType)
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }
                    else if (complianceType.Trim().ToUpper().Equals("SOW05"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.IsActive == true
                                            && row.RM_Status == "A"
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }

                    if (lstComplianceIDs.Count > 0)
                    {
                        transactionsQuery = transactionsQuery.Where(row => lstComplianceIDs.Contains(row.ComplianceID)).ToList();
                    }
                }

                if (queryStringFlag == "C")
                {
                    //transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }
                //Month
                if (month != "" && month != "-1")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.RLCS_PayrollMonth == month)).ToList();
                }
                //Year
                if (year != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.RLCS_PayrollYear == Convert.ToString(year))).ToList();
                }

                // Find data through String contained in Description
                if (!string.IsNullOrEmpty(stringSearchText.Trim()))
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(stringSearchText))).ToList();
                }

                return transactionsQuery.ToList();
            }
        }

        //returns statewise
        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Result> GetRLCSMyWorkspace(int customerID, int userID, string queryStringFlag, int risk, string status, int location, string stringSearchText, string complianceType, DateTime? fromDate, DateTime? toDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_RLCS_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Result>();

                int reviewerRoleID = 4;

                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                if (status == "Upcoming")
                {
                    //ComplianceInstanceTransactionViews
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         && (row.PerformerScheduledOn >= now && row.PerformerScheduledOn <= nextOneMonth)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "DueButNotSubmitted" || status == "Overdue")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13) && row.ScheduledOn <= now
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "Rejected")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 6 || row.ComplianceStatusID == 14)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "PendingForReview")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "ClosedTimely")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 4)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status == "ClosedDelayed")
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                          && (row.ComplianceStatusID == 5)
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else if (status.Trim().ToUpper().Equals("0")) //0-ALL
                {
                    transactionsQuery = (from row in entities.SP_RLCS_ComplianceInstanceTransactionCount(customerID, userID, userID.ToString())
                                             //entities.ComplianceInstanceTransactionViews
                                         where row.UserID == userID && row.RoleID == reviewerRoleID
                                         && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                         select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                if (!string.IsNullOrEmpty(complianceType))
                {
                    List<long> lstComplianceIDs = new List<long>();

                    if (complianceType.Trim().ToUpper().Equals("SOW03"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.RegisterID != null
                                            && row.Register_Status == "A"
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }
                    else if (complianceType.Trim().ToUpper().Equals("EPF") || complianceType.Trim().ToUpper().Equals("ESI") || complianceType.Trim().ToUpper().Equals("PT"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Challan_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.IsActive == true
                                            && row.ChallanType.Equals(complianceType)
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }
                    else if (complianceType.Trim().ToUpper().Equals("SOW05"))
                    {
                        lstComplianceIDs = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.IsActive == true
                                            && row.RM_Status == "A"
                                            select (long)row.AVACOM_ComplianceID).ToList();
                    }

                    if (lstComplianceIDs.Count > 0)
                    {
                        transactionsQuery = transactionsQuery.Where(row => lstComplianceIDs.Contains(row.ComplianceID)).ToList();
                    }
                }

                if (queryStringFlag == "C")
                {
                    //transactionsQuery = transactionsQuery.Where(entry => entry.EventFlag == null).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                if (location != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                }

                // Find data through String contained in Description
                if (!string.IsNullOrEmpty(stringSearchText.Trim()))
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(stringSearchText))).ToList();
                }

                if (fromDate != null && toDate != null)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn.Date >= fromDate && entry.ScheduledOn <= toDate)).ToList();
                }

                return transactionsQuery.ToList();
            }
        }

        public static List<RLCS_Returns_Compliance_Mapping> GetAll_Returns_StateWise(string stateName, string establishmentType, string register_Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstReturns = (from row in entities.RLCS_Returns_Compliance_Mapping
                                    where row.AVACOM_ComplianceID != null
                                    && row.ReturnID != null
                                    && row.RM_Status == "A"
                                    select row).ToList();

                if (lstReturns.Count > 0)
                {
                    if (!string.IsNullOrEmpty(stateName))
                    {
                        lstReturns = lstReturns.Where(row => row.RM_StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper())).ToList();
                    }

                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            lstReturns = (from row in lstReturns
                                            where (row.RM_Act.Trim().ToUpper().Equals("SF") || row.RM_Act.Trim().ToUpper().Equals("FACT"))
                                            && row.RM_Act != null
                                            && row.RM_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            lstReturns = (from row in lstReturns
                                            where (row.RM_Act.Trim().ToUpper().Equals("SF") || row.RM_Act.Trim().ToUpper().Equals("SEA"))
                                            && row.RM_Act != null
                                            && row.RM_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                        else if (establishmentType.Equals("SF"))
                        {
                            lstReturns = (from row in lstReturns
                                          where row.RM_Status == "A"
                                            && row.AVACOM_ComplianceID != null
                                            select row).ToList();
                        }
                    }

                    //if (!string.IsNullOrEmpty(register_Type))
                    //{
                    //    lstReturns = (from row in lstReturns
                    //                    where (row.RM_Return_Type.Trim().ToUpper().Equals(register_Type))
                    //                    && row.RM_Act != null
                    //                    && row.RM_Status == "A"
                    //                    && row.AVACOM_ComplianceID != null
                    //                    select row).ToList();
                    //}
                }

                if (lstReturns.Count > 0)
                {

                }

                return lstReturns.Distinct().ToList();
            }
        }

        //public static List<object> GetReturn_Act_Frequency(int custBranchID, string bindType, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string returnType, string selectActType, string selectedFreq)
        //{
        //    try
        //    {
        //        List<object> lstItems = new List<object>();

        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            if (!string.IsNullOrEmpty(bindType))
        //            {
        //                if (bindType == "Act")
        //                {
        //                    #region Act

        //                    if (!string.IsNullOrEmpty(returnType))
        //                    {
        //                        if (returnType == "Central")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID == "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.RS_ActGroup,
        //                                            Name = RRCM.RS_ActName,
        //                                        }).Distinct().OrderBy(item => item.Name).ToList<object>();
        //                        }
        //                        else if (returnType == "State")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID != "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.RS_ActGroup,
        //                                            Name = RRCM.RS_ActName,
        //                                        }).Distinct().OrderBy(item => item.Name).ToList<object>();
        //                        }
        //                    }

        //                    #endregion
        //                }
        //                else if (bindType == "Frequency")
        //                {
        //                    #region Frequency
        //                    if (!string.IsNullOrEmpty(selectActType))
        //                    {
        //                        if (returnType == "Central")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID == "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && RRCM.RM_Act == selectActType
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.RM_Frequency,
        //                                            Name = RRCM.RM_Frequency,
        //                                        }).Distinct().ToList<object>();
        //                        }
        //                        else if (returnType == "State")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID != "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && RRCM.RM_Act == selectActType
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.RM_Frequency,
        //                                            Name = RRCM.RM_Frequency,
        //                                        }).Distinct().ToList<object>();
        //                        }
        //                    }

        //                    #endregion
        //                }
        //                else if (bindType == "Return")
        //                {
        //                    #region Return

        //                    if (!string.IsNullOrEmpty(selectedFreq))
        //                    {
        //                        if (returnType == "Central")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID == "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && RRCM.RM_Act == selectActType
        //                                        && RRCM.RM_Frequency == selectedFreq
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.ReturnID,
        //                                            Name = RRCM.RM_Name,
        //                                        }).Distinct().ToList<object>();
        //                        }
        //                        else if (returnType == "State")
        //                        {
        //                            lstItems = (from RRCM in lstReturnMaster
        //                                        join CI in entities.ComplianceInstances
        //                                        on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
        //                                        where RRCM.RM_StateID != "Central"
        //                                        && RRCM.RM_Status == "A"
        //                                        && RRCM.RM_Act == selectActType
        //                                        && RRCM.RM_Frequency == selectedFreq
        //                                        && CI.CustomerBranchID == custBranchID
        //                                        select new
        //                                        {
        //                                            ID = RRCM.ReturnID,
        //                                            Name = RRCM.RM_Name,
        //                                        }).Distinct().ToList<object>();
        //                        }
        //                    }

        //                    #endregion
        //                }
        //            }

        //            return lstItems;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return null;
        //    }
        //}

        public static RLCS_Returns_Compliance_Mapping GetReturnDetails(long returnID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstMasterRecords = (from RM in entities.RLCS_Returns_Compliance_Mapping
                                            where RM.ReturnID == returnID
                                            select RM).FirstOrDefault();

                    return lstMasterRecords;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static List<RLCS_Returns_Compliance_Mapping> GetAllReturnsDetail(long custBranchID, string Type)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstReturnMaster = GetAll_ReturnMaster();

                    var lstItems = (from RRCM in lstReturnMaster
                                    join CI in entities.ComplianceInstances
                                    on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                    where RRCM.RM_StateID == Type
                                    && RRCM.RM_Status == "A"
                                    && CI.CustomerBranchID == custBranchID
                                    select new RLCS_Returns_Compliance_Mapping()
                                    {
                                        ReturnID = RRCM.ReturnID,
                                        RM_Frequency = RRCM.RM_Frequency,
                                        RM_Act = RRCM.RM_Act,
                                        RM_Return_Category = RRCM.RM_Return_Category,
                                        RM_Doc_Format = RRCM.RM_Doc_Format,
                                        RM_Name=RRCM.RM_Name,
                                        RM_Download_Path = RRCM.RM_Download_Path
                                    }).Distinct().ToList();

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_Returns_Compliance_Mapping> GetAllReturnsDetail(long custBranchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstReturnMaster = GetAll_ReturnMaster();

                    var lstItems = (from RRCM in lstReturnMaster
                                    join CI in entities.ComplianceInstances
                                    on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                    where RRCM.RM_Status == "A"
                                    && CI.CustomerBranchID == custBranchID
                                    select new RLCS_Returns_Compliance_Mapping()
                                    {
                                        ReturnID = RRCM.ReturnID,
                                        RM_Frequency = RRCM.RM_Frequency,
                                        RM_Act = RRCM.RM_Act,
                                        RM_Return_Category = RRCM.RM_Return_Category,
                                        RM_Doc_Format = RRCM.RM_Doc_Format,
                                        RM_Name = RRCM.RM_Name,
                                        RM_Download_Path = RRCM.RM_Download_Path
                                    }).Distinct().ToList();

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<RLCS_Returns_Compliance_Mapping> GetAllReturnsDetailForEntity(List<int>Branches)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstReturnMaster = GetAll_ReturnMaster();

                    var lstItems = (from RRCM in lstReturnMaster
                                    join CI in entities.ComplianceInstances
                                    on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                    where RRCM.RM_Status == "A"
                                    && Branches.Contains(CI.CustomerBranchID)
                                    select new RLCS_Returns_Compliance_Mapping()
                                    {
                                        ReturnID = RRCM.ReturnID,
                                        RM_Frequency = RRCM.RM_Frequency,
                                        RM_Act = RRCM.RM_Act,
                                        RM_Return_Category = RRCM.RM_Return_Category,
                                        RM_Doc_Format = RRCM.RM_Doc_Format,
                                        RM_Name = RRCM.RM_Name,
                                        RM_Download_Path = RRCM.RM_Download_Path
                                    }).Distinct().ToList();

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public static List<SP_RLCS_ReturnMaster_Result> GetAll_ReturnMaster()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstMasterRecords = (from RM in entities.SP_RLCS_ReturnMaster()
                                            where RM.AVACOM_ComplianceID != null
                                            select RM).ToList();

                    return lstMasterRecords;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<SP_RLCS_ReturnMaster_Result> GetAll_ReturnMasterByType(string ReturnChallanType)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstMasterRecords = (from RM in entities.SP_RLCS_ReturnMaster()
                                            where RM.AVACOM_ComplianceID != null && RM.RS_ActGroup.Trim().ToUpper() == ReturnChallanType.ToUpper()
                                            select RM).ToList();

                    return lstMasterRecords;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<object> GetReturn_Act_Frequency(int custBranchID, string bindType, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string returnType, string selectActType, string selectedFreq)
        {
            try
            {
                List<object> lstItems = new List<object>();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (!string.IsNullOrEmpty(bindType))
                    {
                        if (bindType == "Act")
                        {
                            #region Act

                            if (!string.IsNullOrEmpty(returnType))
                            {
                                if (returnType == "Central")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID == "Central"
                                                && RRCM.RM_Status == "A"
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.RS_ActGroup,
                                                    Name = RRCM.RS_ActName,
                                                }).Distinct().OrderBy(item => item.Name).ToList<object>();
                                }
                                else if (returnType == "State")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID != "Central"
                                                && RRCM.RM_Status == "A"
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.RS_ActGroup,
                                                    Name = RRCM.RS_ActName,
                                                }).Distinct().OrderBy(item => item.Name).ToList<object>();
                                }
                            }

                            #endregion
                        }
                        else if (bindType == "Frequency")
                        {
                            #region Frequency
                            if (!string.IsNullOrEmpty(selectActType))
                            {
                                if (returnType == "Central")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID == "Central"
                                                && RRCM.RM_Status == "A"
                                                && RRCM.RM_Act == selectActType
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.RM_Frequency,
                                                    Name = RRCM.RM_Frequency,
                                                }).Distinct().ToList<object>();
                                }
                                else if (returnType == "State")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID != "Central"
                                                && RRCM.RM_Status == "A"
                                                && RRCM.RM_Act == selectActType
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.RM_Frequency,
                                                    Name = RRCM.RM_Frequency,
                                                }).Distinct().ToList<object>();
                                }
                            }

                            #endregion
                        }
                        else if (bindType == "Return")
                        {
                            #region Return

                            if (!string.IsNullOrEmpty(selectedFreq))
                            {
                                if (returnType == "Central")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID == "Central"
                                                && RRCM.RM_Status == "A"
                                                && RRCM.RM_Act == selectActType
                                                && RRCM.RM_Frequency == selectedFreq
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.ReturnID,
                                                    Name = RRCM.RM_Name,
                                                }).Distinct().ToList<object>();
                                }
                                else if (returnType == "State")
                                {
                                    lstItems = (from RRCM in lstReturnMaster
                                                join CI in entities.ComplianceInstances
                                                on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                                where RRCM.RM_StateID != "Central"
                                                && RRCM.RM_Status == "A"
                                                && RRCM.RM_Act == selectActType
                                                && RRCM.RM_Frequency == selectedFreq
                                                && CI.CustomerBranchID == custBranchID
                                                select new
                                                {
                                                    ID = RRCM.ReturnID,
                                                    Name = RRCM.RM_Name,
                                                }).Distinct().ToList<object>();
                                }
                            }

                            #endregion
                        }
                    }

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<object> GetReturnList(int custBranchID, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string selectedFreq)
        {
            try
            {
                List<object> lstItems = new List<object>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (custBranchID > 0)
                    {
                        var returns = (from RRCM in lstReturnMaster
                                    join CI in entities.ComplianceInstances
                                    on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                    where RRCM.RM_Status == "A"
                                    && CI.CustomerBranchID == custBranchID
                                    select new
                                    {
                                        ID = RRCM.ReturnID,
                                        Name = RRCM.RM_Name,
                                        State = RRCM.RM_StateID,
                                        Act = RRCM.RM_Act,
                                        Frequency = RRCM.RM_Frequency
                                    }).Distinct().ToList();

                        if (returns != null)
                        {
                            string state = (from CB in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                            where CB.AVACOM_BranchID == custBranchID
                                            select CB.CM_State).FirstOrDefault();

                            string[] arrReturnTypes = new string[] { "Central", state };

                            var statewisereturns = (from lst in returns.Where(x => arrReturnTypes.Contains(x.State))
                                                    select new
                                                    {
                                                        ID = lst.ID,
                                                        Name = lst.Name,
                                                        Act = lst.Act,
                                                        Frequency = lst.Frequency
                                                    }).Distinct().ToList();

                            if (!string.IsNullOrEmpty(selectedFreq))
                            {
                                statewisereturns = (from lst in statewisereturns
                                                    where lst.Frequency == selectedFreq
                                                    select new
                                                    {
                                                        ID = lst.ID,
                                                        Name = lst.Name,
                                                        Act = lst.Act,
                                                        Frequency = lst.Frequency
                                                    }).Distinct().ToList();
                            }

                            foreach (var statewisereturn in statewisereturns)
                            {
                                lstItems.Add(statewisereturn);
                            }
                        }
                    }

                    return lstItems;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<object> GetReturnChallanList(int branch, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string selectedFreq)
        {
            try
            {
                List<object> lstItems = new List<object>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (branch > 0)
                    {
                        var Branches = RLCS_ComplianceManagement.GetBranchesForEntity(branch);
                        if (Branches != null)
                        {
                            //Branches.ForEach(eachRow =>
                            //{
                                var returns = (from RRCM in lstReturnMaster
                                               join CI in entities.ComplianceInstances
                                               on RRCM.AVACOM_ComplianceID equals CI.ComplianceId
                                               where RRCM.RM_Status == "A"
                                               && Branches.Contains(CI.CustomerBranchID) 
                                               select new
                                               {
                                                   ID = RRCM.ReturnID,
                                                   Name = RRCM.RM_Name,
                                                   State = RRCM.RM_StateID,
                                                   Act = RRCM.RM_Act,
                                                   Frequency = RRCM.RM_Frequency
                                               }).Distinct().ToList();

                                if (returns != null)
                                {
                                    //string state = (from CB in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                    //                where Branches.Contains(CB.AVACOM_BranchID.Value)
                                    //                select CB.CM_State).FirstOrDefault();

                                    //string[] arrReturnTypes = new string[] { "Central", state };

                                    //var statewisereturns = (from lst in returns.Where(x => arrReturnTypes.Contains(x.State))
                                    //                        select new
                                    //                        {
                                    //                            ID = lst.ID,
                                    //                            Name = lst.Name,
                                    //                            Act = lst.Act,
                                    //                            Frequency = lst.Frequency
                                    //                        }).Distinct().ToList();

                                    if (!string.IsNullOrEmpty(selectedFreq))
                                    {
                                       var statewisereturns = (from lst in returns
                                                            where lst.Frequency == selectedFreq
                                                            select new
                                                            {
                                                                ID = lst.ID,
                                                                Name = lst.Name,
                                                                Act = lst.Act,
                                                                Frequency = lst.Frequency
                                                            }).Distinct().ToList();
                                    

                                    foreach (var statewisereturn in statewisereturns)
                                    {
                                        lstItems.Add(statewisereturn);
                                    }
                                    
                                }
                                    if(lstItems.Count <=0)
                                {
                                    foreach (var r in returns)
                                    {
                                        lstItems.Add(r);
                                    }
                                }
                            }
                            
                        }
                    }
                }
                return lstItems;

            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<int> GetBranchesForEntity(int entity)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var branches = (from b in entities.CustomerBranches
                                    where b.ParentID == entity
                                    select b.ID).ToList();

                    return branches;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static SP_RLCS_RegisterReturnChallanCompliance_Result GetRegisterReturnIDComType(int customerID, int branchID, int actID, int complianceID)
        {
            SP_RLCS_RegisterReturnChallanCompliance_Result record = new SP_RLCS_RegisterReturnChallanCompliance_Result();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //Get State Code
                    var branchRecord = RLCS_ClientsManagement.GetStateLocationByBranch(branchID);

                    //Get ComType and Register,Return,ChallanID
                    if (branchRecord != null)
                    {
                        string stateCode = branchRecord.CM_State;

                        record = entities.SP_RLCS_RegisterReturnChallanCompliance().Where(row => row.ActID == actID
                                     && row.ID == complianceID
                                     && (row.StateID == stateCode || row.StateID == "Central")).FirstOrDefault();
                        
                    }
                }

                return record;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool CreateInstances(List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments, long createdByID, string creatdByName, long customerid)
        {
            long CreateInstancesErrorcomplianceInstanceID = -1;
            long CreateInstancesErrorComplianceId = -1;
            try
            {
                if (assignments.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        assignments.ForEach(entry =>
                        {
                            try
                            {
                                long complianceInstanceID = (from CI in entities.ComplianceInstances
                                                             where CI.ComplianceId == entry.Item1.ComplianceId
                                                             && CI.CustomerBranchID == entry.Item1.CustomerBranchID
                                                             && CI.IsDeleted == false
                                                             && CI.SequenceID == entry.Item1.SequenceID
                                                             select CI.ID).SingleOrDefault();
                                if (complianceInstanceID <= 0)
                                {
                                    entry.Item1.CreatedOn = DateTime.UtcNow;
                                    entry.Item1.IsDeleted = false;
                                    entry.Item1.GenerateSchedule = true;
                                    entry.Item1.IsAvantis = true;
                                    entities.ComplianceInstances.Add(entry.Item1);
                                    entities.SaveChanges();

                                    complianceInstanceID = entry.Item1.ID;
                                    if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                    {
                                        if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                        {
                                            CreateInstancesErrorcomplianceInstanceID = complianceInstanceID;
                                            CreateInstancesErrorComplianceId = entry.Item1.ComplianceId;

                                            var compliances = (from row in entities.Compliances
                                                               where row.ID == entry.Item1.ComplianceId
                                                               select row).FirstOrDefault();


                                            if (compliances.ComplianceType != 3)
                                            {
                                                if (compliances.CheckListTypeID != 0)
                                                {
                                                    if (compliances.ComplianceType == 1)
                                                    {
                                                        var CBCF = (from row in entities.ClientBasedCheckListFrequencies
                                                                    where row.ClientID == customerid
                                                                    && row.ComplianceID == entry.Item1.ComplianceId
                                                                    select row).FirstOrDefault();
                                                        if (CBCF != null)
                                                        {
                                                            CreateScheduleOnWithClientFrequency(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName, CBCF.CFrequency, customerid);
                                                        }
                                                        else
                                                        {
                                                            CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    entry.Item1.ID = complianceInstanceID;
                                }

                                long complianceAssignment = (from row in entities.ComplianceAssignments
                                                             where row.ComplianceInstanceID == entry.Item1.ID
                                                             && row.RoleID == entry.Item2.RoleID
                                                             select row.UserID).FirstOrDefault();

                                if (complianceAssignment <= 0)
                                {
                                    entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                    entities.ComplianceAssignments.Add(entry.Item2);
                                    entities.SaveChanges();

                                    if (entry.Item2.RoleID != 6)
                                    {
                                        if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                        {
                                            if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                            {
                                                CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        });
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();


                var errorvar = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                var date = DateTime.Now;
                var FunctionName = "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID;

                msg = GetLogDetails("RLCS_ComplianceManagement", FunctionName, date, ex.Message + "----\r\n" + ex.InnerException, ex.StackTrace, errorvar);
                
                //msg.ClassName = "RLCS_ComplianceManagement";
                //msg.FunctionName = "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateInstances Function", "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateInstances Function", "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);
                return false;

            }
        }

        private static LogMessage GetLogDetails(string ClassName, string FunctionName, DateTime Now, string Message, string StackTrace, byte errorvar)
        {
            LogMessage mess = new LogMessage();
            mess.ClassName = ClassName;
            mess.FunctionName = FunctionName;
            mess.CreatedOn = Now;
            mess.Message = Message;
            mess.StackTrace = StackTrace;
            mess.LogLevel = errorvar;
            return mess;

        }

        public static bool ComplianceIsEventBased(long complianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.Compliances
                                      where row.ID == complianceId
                                      select row).FirstOrDefault();

                if (ComplianceData.EventFlag == null)
                    return true;
                else
                    return false;
            }
        }

        public static bool ComplianceIsActionable(long complianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.Compliances
                                      where row.ID == complianceId
                                      select row).FirstOrDefault();

                if (ComplianceData.ComplinceVisible == true)
                    return true;
                else
                    return false;
            }
        }

        private static void CreateReminders(long complianceID, long ComplianceInstanceId, int ComplianceAssignmentId)
        {
            long CreateRemindersErrorcomplianceInstanceID = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var compliance = (from row in entities.Compliances
                                      where row.ID == complianceID
                                      select row).FirstOrDefault();

                    if (compliance.Frequency == 7)
                    {
                        #region Frequency Daily
                        List<ComplianceScheduleOn> ScheduledOnList = (from row in entities.ComplianceScheduleOns
                                                                      where row.ComplianceInstanceID == ComplianceInstanceId && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                      select row).ToList();

                        CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;

                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                        {
                            ComplianceReminder reminder = new ComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = sco.ScheduleOn,
                                RemindOn = sco.ScheduleOn.Date,
                            };
                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                            entities.ComplianceReminders.Add(reminder);
                        }
                        #endregion
                    }
                    else if (compliance.Frequency == 8)
                    {
                        #region Frequency Weekly
                        List<ComplianceScheduleOn> ScheduledOnList = (from row in entities.ComplianceScheduleOns
                                                                      where row.ComplianceInstanceID == ComplianceInstanceId
                                                                      && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                      select row).ToList();

                        CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;
                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                        {
                            ComplianceReminder reminder = new ComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = sco.ScheduleOn,
                                RemindOn = sco.ScheduleOn.Date,
                            };
                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                            entities.ComplianceReminders.Add(reminder);

                            ComplianceReminder reminder1 = new ComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = sco.ScheduleOn,
                                RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 2),
                            };
                            reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled); //updated by Rahul on 26 April 2016
                            entities.ComplianceReminders.Add(reminder1);
                        }
                        #endregion
                    }
                    else
                    {
                        List<long> getdays = GetUpcomingReminderDaysDetail(ComplianceAssignmentId, Convert.ToInt32(compliance.Frequency), "S");

                        List<ComplianceScheduleOn> ScheduledOnList = (from row in entities.ComplianceScheduleOns
                                                                      where row.ComplianceInstanceID == ComplianceInstanceId && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                      select row).ToList();


                        if (getdays.Count > 0)
                        {
                            foreach (ComplianceScheduleOn sco in ScheduledOnList)
                            {
                                foreach (var item in getdays)
                                {
                                    ComplianceReminder reminder1 = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = sco.ScheduleOn,
                                        RemindOn = sco.ScheduleOn.Date.AddDays(-item),
                                    };
                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder1);
                                }
                            }
                        }
                        else
                        {
                            #region Normal Frequency
                            //Time Based
                            if (compliance.SubComplianceType == 0)
                            {
                                //List<ComplianceScheduleOn> ScheduledOnList = (from row in entities.ComplianceScheduleOns
                                //                                              where row.ComplianceInstanceID == ComplianceInstanceId
                                //                                              && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                //                                              select row).ToList();

                                CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;
                                //Standard
                                if (compliance.ReminderType == 0)
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 2),
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);

                                            ComplianceReminder reminder1 = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 15),
                                            };
                                            reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled); //updated by Rahul on 26 April 2016
                                            entities.ComplianceReminders.Add(reminder1);
                                        }
                                    }
                                }
                                else//Custom
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        ScheduledOnList.ForEach(entry =>
                                        {
                                            DateTime TempScheduled = entry.ScheduleOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));

                                            while (TempScheduled.Date < entry.ScheduleOn)
                                            {
                                                ComplianceReminder reminder = new ComplianceReminder()
                                                {
                                                    ComplianceAssignmentID = ComplianceAssignmentId,
                                                    ReminderTemplateID = 1,
                                                    ComplianceDueDate = entry.ScheduleOn,
                                                    RemindOn = TempScheduled.Date,
                                                };

                                                reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                                entities.ComplianceReminders.Add(reminder);

                                                TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                            }
                                        });
                                    }
                                }

                            }//Function Based Start
                            else
                            {
                                CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;
                                if (compliance.ReminderType == 0)
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 0)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 1)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        if (compliance.Frequency.HasValue)
                                        {
                                            var reminders = (from row in entities.ReminderTemplates
                                                             where row.Frequency == compliance.Frequency.Value && row.IsSubscribed == true
                                                             select row).ToList();

                                            foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                            {
                                                reminders.ForEach(day =>
                                                {
                                                    ComplianceReminder reminder = new ComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = day.ID,
                                                        ComplianceDueDate = sco.ScheduleOn,
                                                        RemindOn = sco.ScheduleOn.Date.AddDays(-1 * day.TimeInDays),
                                                    };

                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                                    entities.ComplianceReminders.Add(reminder);

                                                });
                                            }
                                        }
                                        else //added by rahul on 16 FEB 2016 for Event
                                        {
                                            if (compliance.ComplianceType == 0 && compliance.EventID != null)
                                            {
                                                foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                                {
                                                    ComplianceReminder reminder = new ComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = 1,
                                                        ComplianceDueDate = sco.ScheduleOn,
                                                        RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 2),
                                                    };
                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                    entities.ComplianceReminders.Add(reminder);

                                                    ComplianceReminder reminder1 = new ComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = 1,
                                                        ComplianceDueDate = sco.ScheduleOn,
                                                        RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 15),
                                                    };
                                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                    entities.ComplianceReminders.Add(reminder1);
                                                }
                                            }
                                        } //added by rahul on 16 FEB 2016 for Event
                                    }//Added by rahul on 4 FEB 2016
                                }
                                else
                                {

                                    ScheduledOnList.ForEach(entry =>
                                    {
                                        DateTime TempScheduled = entry.ScheduleOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));

                                        while (TempScheduled.Date < entry.ScheduleOn)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = entry.ScheduleOn,
                                                RemindOn = TempScheduled.Date,
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                            entities.ComplianceReminders.Add(reminder);

                                            TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                        }

                                    });
                                }
                            }
                            #endregion
                        }
                    }
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();

                var errorvar = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                var date = DateTime.Now;
                var FunctionName = "CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID;

                msg = GetLogDetails("RLCS_ComplianceManagement", FunctionName, date, ex.Message + "----\r\n" + ex.InnerException, ex.StackTrace, errorvar);

                //msg.ClassName = "RLCS_ComplianceManagement";
                //msg.FunctionName = "CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateReminders Function", "CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateReminders Function", "CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID);

            }
        }

     

        public static void CreateScheduleOnWithClientFrequency(DateTime scheduledOn, long complianceInstanceId, Compliance compliances, long createdByID, string creatdByName, int frequency, long customerid)
        {
            long CreateScheduleOnErroComplianceid = -1;
            long CreateScheduleOnErrocomplianceInstanceId = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var onloff = 1;
                    var beforafter = 0;
                    if (compliances.duedatetype != null)
                    {
                        beforafter = (int)compliances.duedatetype;
                    }
                    if (compliances.onlineoffline != null)
                    {
                        onloff = Convert.ToByte(compliances.onlineoffline);
                    }
                    DateTime nextDate = scheduledOn;
                    DateTime curruntDate;
                    if (frequency == 7)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(1);
                    }
                    else if (frequency == 8)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(30);
                    }
                    else
                    {
                        curruntDate = DateTime.UtcNow;
                    }

                    while (nextDate < curruntDate) //Commented by rahul on 7 MARCH 2016 For Check Scheduled on
                    {
                        Tuple<DateTime, string, long> nextDateMonth;

                        if (frequency == 7)
                        {
                            #region Frequancy Daily                        
                            CreateScheduleOnErroComplianceid = compliances.ID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateDaily(nextDate, compliances.ID, complianceInstanceId, false);

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                if (nextDateMonth.Item1.Date.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                    complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                    complianceScheduleon.ScheduleOn = nextDateMonth.Item1;
                                    complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                    complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                    complianceScheduleon.IsActive = true;
                                    complianceScheduleon.IsUpcomingNotDeleted = true;
                                    entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDateMonth.Item1;

                                    ComplianceTransaction transaction = new ComplianceTransaction()
                                    {
                                        ComplianceInstanceId = complianceInstanceId,
                                        ComplianceScheduleOnID = complianceScheduleon.ID,
                                        CreatedBy = createdByID,
                                        CreatedByText = creatdByName,
                                        StatusId = 1,
                                        Remarks = "New compliance assigned."
                                    };

                                    CreateTransaction(transaction);
                                }
                                else
                                {
                                    if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        nextDate = nextDateMonth.Item1.AddDays(1);
                                    }
                                    else
                                    {
                                        nextDate = nextDateMonth.Item1;
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (frequency == 8)
                        {
                            #region Frequancy Weekly
                            CreateScheduleOnErroComplianceid = compliances.ID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateWeekly(nextDate, compliances.ID, complianceInstanceId, Convert.ToInt32(compliances.DueWeekDay));

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic 
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime dtNewScheduleOn;
                                if (onloff == 1) //online
                                {
                                    dtNewScheduleOn = nextDateMonth.Item1;
                                    if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                    }
                                }
                                else
                                {
                                    //offline
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        dtNewScheduleOn = nextDateMonth.Item1;
                                        if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                        }
                                    }
                                }
                                #endregion
                                //DateTime dtNewScheduleOn;
                                //if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                //{
                                //    dtNewScheduleOn = nextDateMonth.Item1.Date.AddDays(1);
                                //}
                                //else
                                //{
                                //    dtNewScheduleOn = nextDateMonth.Item1.Date;
                                //}
                                DateTime dtStart;
                                if (compliances.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(compliances.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }
                                //if (dtStart <= dtNewScheduleOn)
                                //{
                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = dtNewScheduleOn; // nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    nextDate = dtNewScheduleOn.AddDays(1);
                                }
                                else
                                {
                                    nextDate = nextDateMonth.Item1;
                                }

                                //if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                //{
                                //    nextDate = nextDateMonth.Item1.AddDays(1);
                                //}
                                //else
                                //{
                                //    nextDate = nextDateMonth.Item1;
                                //}
                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                CreateTransaction(transaction);
                                //}
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Frequancy 
                            if (compliances.SubComplianceType == 0)
                            {
                                nextDateMonth = new Tuple<DateTime, string, long>(nextDate.AddDays(Convert.ToDouble(compliances.DueDate)), "", 0);
                            }
                            else if (compliances.SubComplianceType == 1 || compliances.SubComplianceType == 2)
                            {
                                CreateScheduleOnErroComplianceid = compliances.ID;
                                nextDateMonth = GetNextDatePeriodicallyWithClientBased(nextDate, compliances.ID, complianceInstanceId, frequency);
                            }
                            else
                            {
                                CreateScheduleOnErroComplianceid = compliances.ID;
                                CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                                nextDateMonth = GetNextDateWithClientBased(nextDate, compliances.ID, complianceInstanceId, false, frequency, customerid);
                            }

                            string RLCSMonth = string.Empty;
                            string RLCSYear = string.Empty;

                            if (compliances.Frequency != null)
                            {
                                var tupleRLCSMonthYear = Get_RLCSMonthYear(Convert.ToInt32(compliances.Frequency), nextDateMonth.Item2);

                                if (tupleRLCSMonthYear != null)
                                {
                                    RLCSMonth = tupleRLCSMonthYear.Item1;
                                    RLCSYear = tupleRLCSMonthYear.Item2;
                                }
                            }
                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime ScheduleOn1;
                                if (onloff == 1) //online
                                {
                                    ScheduleOn1 = nextDateMonth.Item1;
                                }
                                else
                                {
                                    //offline                                    
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        ScheduleOn1 = nextDateMonth.Item1;
                                    }
                                }
                                #endregion
                                //Complinace Start Date logic
                                DateTime dtStart;
                                if (compliances.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(compliances.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }
                                //if (dtStart <= ScheduleOn1)
                                //{
                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = ScheduleOn1;// nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                if (!string.IsNullOrEmpty(RLCSMonth))
                                    complianceScheduleon.RLCS_PayrollMonth = RLCSMonth;

                                if (!string.IsNullOrEmpty(RLCSYear))
                                    complianceScheduleon.RLCS_PayrollYear = RLCSYear;

                                complianceScheduleon.RLCS_ActivityEndDate = nextDateMonth.Item1;

                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                nextDate = nextDateMonth.Item1;

                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                CreateTransaction(transaction);

                                #region Task Scheduele
                                long CustomerBranchID = GetCustomerBranchID(complianceInstanceId);
                                var Taskapplicable = TaskManagment.TaskApplicable(CustomerBranchID);
                                if (Taskapplicable != null)
                                {

                                    if (Taskapplicable == 1)
                                    {
                                        var TaskIDList = (from row in entities.Tasks
                                                          join row1 in entities.TaskComplianceMappings
                                                          on row.ID equals row1.TaskID
                                                          where row1.ComplianceID == compliances.ID
                                                          && row.TaskType == 1 && row.ParentID == null
                                                          && (((row.Status != "A") || (row.Status == null))
                                                          || ((row.Status == "A") && (row.DeactivateOn > DateTime.Now)))
                                                          && row.Isdeleted == false && row.IsActive == true
                                                          select row.ID).ToList();

                                        if (TaskIDList.Count > 0)
                                        {
                                            TaskIDList.ForEach(entrytask =>
                                            {
                                                List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

                                                bool? IsAfter = TaskManagment.GetTaskBeforeAfter(entrytask);

                                                if (IsAfter == null)
                                                {
                                                    IsAfter = false;
                                                }

                                                var taskSubTaskList = (from row in entities.Tasks
                                                                       join row1 in entities.TaskInstances
                                                                       on row.ID equals row1.TaskId
                                                                       where row.MainTaskID == entrytask && row1.CustomerBranchID == CustomerBranchID
                                                                       select new { row.ID, row.ParentID, row.DueDays }).ToList();

                                                taskSubTaskList.ForEach(entrysubtask =>
                                                {
                                                    TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long)entrysubtask.ID, ParentID = (int?)entrysubtask.ParentID, ActualDueDays = (int)entrysubtask.DueDays });

                                                });

                                                TaskSubTaskList.ForEach(entryTaskSubTaskList =>
                                                {

                                                    var taskInstances = (from row in entities.TaskInstances
                                                                         where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
                                                                          && row.ScheduledOn <= nextDateMonth.Item1
                                                                          && row.CustomerBranchID == CustomerBranchID
                                                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                                                    DateTime schedueon;
                                                    if (IsAfter == null)
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }
                                                    else
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }

                                                    taskInstances.ForEach(entrytaskinstance =>
                                                    {
                                                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                                                        taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
                                                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleon.ID;
                                                        taskScheduleOn.ForMonth = nextDateMonth.Item2;
                                                        taskScheduleOn.ForPeriod = nextDateMonth.Item3;
                                                        taskScheduleOn.ScheduleOn = schedueon; // nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                        taskScheduleOn.IsActive = true;
                                                        taskScheduleOn.IsUpcomingNotDeleted = true;
                                                        entities.TaskScheduleOns.Add(taskScheduleOn);
                                                        entities.SaveChanges();

                                                        var AssignedTaskRole = TaskManagment.GetAssignedTaskUsers((int)entrytaskinstance.ID);
                                                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                        if (performerTaskRole != null)
                                                        {
                                                            var user = UserManagement.GetByID((int)performerTaskRole.UserID);
                                                            TaskTransaction tasktransaction = new TaskTransaction()
                                                            {
                                                                TaskInstanceId = entrytaskinstance.ID,
                                                                TaskScheduleOnID = taskScheduleOn.ID,
                                                                ComplianceScheduleOnID = complianceScheduleon.ID,
                                                                CreatedBy = performerTaskRole.UserID,
                                                                CreatedByText = user.FirstName + " " + user.LastName,
                                                                StatusId = 1,
                                                                Remarks = "New task assigned."
                                                            };

                                                            TaskManagment.CreateTaskTransaction(tasktransaction);

                                                            foreach (var roles in AssignedTaskRole)
                                                            {
                                                                if (roles.RoleID != 6)
                                                                {
                                                                    DateTime schdulon;
                                                                    if (IsAfter == true)
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    else
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    TaskManagment.CreateTaskReminders(entrytaskinstance.TaskId, compliances.ID, roles.ID, schdulon, complianceScheduleon.ID, IsAfter);
                                                                }
                                                            }
                                                        }

                                                    });
                                                });
                                            });
                                        }
                                    }
                                }
                                #endregion
                                //}
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();


                var errorvar = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                var date = DateTime.Now;
                var FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;

                msg = GetLogDetails("RLCS_ComplianceManagement", FunctionName, date, ex.Message + "----\r\n" + ex.InnerException, ex.StackTrace, errorvar);


                //msg.ClassName = "RLCS_ComplianceManagement";
                //msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);
            }
        }

        public static void CreateScheduleOn(DateTime scheduledOn, long complianceInstanceId, Compliance compliances, long createdByID, string creatdByName)
        {
            long CreateScheduleOnErroComplianceid = -1;
            long CreateScheduleOnErrocomplianceInstanceId = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var onloff = 1;
                    var beforafter = 0;
                    if (compliances.duedatetype != null)
                    {
                        beforafter = (int)compliances.duedatetype;
                    }
                    if (compliances.onlineoffline != null)
                    {
                        onloff = Convert.ToByte(compliances.onlineoffline);
                    }

                    DateTime nextDate = scheduledOn;
                    DateTime curruntDate;
                    if (compliances.Frequency == 7)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(1);
                    }
                    else if (compliances.Frequency == 8)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(30);
                    }
                    else
                    {
                        curruntDate = DateTime.UtcNow;
                    }

                    while (nextDate < curruntDate) //Commented by rahul on 7 MARCH 2016 For Check Scheduled on
                    {
                        Tuple<DateTime, string, long> nextDateMonth;

                        if (compliances.Frequency == 7)
                        {
                            #region Frequancy Daily                        
                            CreateScheduleOnErroComplianceid = compliances.ID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateDaily(nextDate, compliances.ID, complianceInstanceId, false);

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                if (nextDateMonth.Item1.Date.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                    complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                    complianceScheduleon.ScheduleOn = nextDateMonth.Item1;
                                    complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                    complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                    complianceScheduleon.IsActive = true;
                                    complianceScheduleon.IsUpcomingNotDeleted = true;
                                    entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDateMonth.Item1;

                                    ComplianceTransaction transaction = new ComplianceTransaction()
                                    {
                                        ComplianceInstanceId = complianceInstanceId,
                                        ComplianceScheduleOnID = complianceScheduleon.ID,
                                        CreatedBy = createdByID,
                                        CreatedByText = creatdByName,
                                        StatusId = 1,
                                        Remarks = "New compliance assigned."
                                    };

                                    CreateTransaction(transaction);
                                }
                                else
                                {
                                    if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        nextDate = nextDateMonth.Item1.AddDays(1);
                                    }
                                    else
                                    {
                                        nextDate = nextDateMonth.Item1;
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (compliances.Frequency == 8)
                        {
                            #region Frequancy Weekly
                            CreateScheduleOnErroComplianceid = compliances.ID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateWeekly(nextDate, compliances.ID, complianceInstanceId, Convert.ToInt32(compliances.DueWeekDay));

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic 
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime dtNewScheduleOn;
                                if (onloff == 1) //online
                                {
                                    dtNewScheduleOn = nextDateMonth.Item1;
                                    if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                    }
                                }
                                else
                                {
                                    //offline
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        dtNewScheduleOn = nextDateMonth.Item1;
                                        if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                        }
                                    }
                                }
                                #endregion
                                //DateTime dtNewScheduleOn;
                                //if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                //{
                                //    dtNewScheduleOn = nextDateMonth.Item1.Date.AddDays(1);
                                //}
                                //else
                                //{
                                //    dtNewScheduleOn = nextDateMonth.Item1.Date;
                                //}

                                //Complinace Start Date logic
                                DateTime dtStart;
                                if (compliances.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(compliances.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }
                                //if (dtStart <= dtNewScheduleOn)
                                //{
                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = dtNewScheduleOn; // nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    nextDate = dtNewScheduleOn.AddDays(1);
                                }
                                else
                                {
                                    nextDate = nextDateMonth.Item1;
                                }
                                //if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                //{
                                //    nextDate = nextDateMonth.Item1.AddDays(1);
                                //}
                                //else
                                //{
                                //    nextDate = nextDateMonth.Item1;
                                //}   


                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                CreateTransaction(transaction);
                                //}
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Frequancy 
                            if (compliances.SubComplianceType == 0)
                            {
                                nextDateMonth = new Tuple<DateTime, string, long>(nextDate.AddDays(Convert.ToDouble(compliances.DueDate)), "", 0);
                            }
                            else if (compliances.SubComplianceType == 1 || compliances.SubComplianceType == 2)
                            {
                                CreateScheduleOnErroComplianceid = compliances.ID;
                                nextDateMonth = GetNextDatePeriodically(nextDate, compliances.ID, complianceInstanceId);
                            }
                            else
                            {
                                CreateScheduleOnErroComplianceid = compliances.ID;
                                CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                                nextDateMonth = GetNextDate(nextDate, compliances.ID, complianceInstanceId, false);
                            }

                            string RLCSMonth = string.Empty;
                            string RLCSYear = string.Empty;

                            if (compliances.Frequency != null)
                            {
                                var tupleRLCSMonthYear = Get_RLCSMonthYear(Convert.ToInt32(compliances.Frequency), nextDateMonth.Item2);

                                if (tupleRLCSMonthYear != null)
                                {
                                    RLCSMonth = tupleRLCSMonthYear.Item1;
                                    RLCSYear = tupleRLCSMonthYear.Item2;
                                }
                            }

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime ScheduleOn1;
                                if (onloff == 1) //online
                                {
                                    ScheduleOn1 = nextDateMonth.Item1;
                                }
                                else
                                {
                                    //offline                                    
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        ScheduleOn1 = nextDateMonth.Item1;
                                    }
                                }
                                #endregion
                                //Complinace Start Date logic
                                DateTime dtStart;
                                if (compliances.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(compliances.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }

                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = ScheduleOn1;// nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                if (!string.IsNullOrEmpty(RLCSMonth))
                                    complianceScheduleon.RLCS_PayrollMonth = RLCSMonth;

                                if (!string.IsNullOrEmpty(RLCSYear))
                                    complianceScheduleon.RLCS_PayrollYear = RLCSYear;

                                complianceScheduleon.RLCS_ActivityEndDate = nextDateMonth.Item1;

                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                nextDate = nextDateMonth.Item1;

                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                CreateTransaction(transaction);

                                #region Task Scheduele
                                long CustomerBranchID = GetCustomerBranchID(complianceInstanceId);
                                var Taskapplicable = TaskManagment.TaskApplicable(CustomerBranchID);
                                if (Taskapplicable != null)
                                {

                                    if (Taskapplicable == 1)
                                    {
                                        var TaskIDList = (from row in entities.Tasks
                                                          join row1 in entities.TaskComplianceMappings
                                                          on row.ID equals row1.TaskID
                                                          where row1.ComplianceID == compliances.ID
                                                          && row.TaskType == 1 && row.ParentID == null
                                                          && (((row.Status != "A") || (row.Status == null))
                                                          || ((row.Status == "A") && (row.DeactivateOn > DateTime.Now)))
                                                          && row.Isdeleted == false && row.IsActive == true
                                                          select row.ID).ToList();

                                        if (TaskIDList.Count > 0)
                                        {
                                            TaskIDList.ForEach(entrytask =>
                                            {
                                                List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

                                                bool? IsAfter = TaskManagment.GetTaskBeforeAfter(entrytask);

                                                if (IsAfter == null)
                                                {
                                                    IsAfter = false;
                                                }

                                                var taskSubTaskList = (from row in entities.Tasks
                                                                       join row1 in entities.TaskInstances
                                                                       on row.ID equals row1.TaskId
                                                                       where row.MainTaskID == entrytask && row1.CustomerBranchID == CustomerBranchID
                                                                       select new { row.ID, row.ParentID, row.DueDays }).ToList();

                                                taskSubTaskList.ForEach(entrysubtask =>
                                                {
                                                    TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long)entrysubtask.ID, ParentID = (int?)entrysubtask.ParentID, ActualDueDays = (int)entrysubtask.DueDays });

                                                });

                                                TaskSubTaskList.ForEach(entryTaskSubTaskList =>
                                                {

                                                    var taskInstances = (from row in entities.TaskInstances
                                                                         where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
                                                                          && row.ScheduledOn <= nextDateMonth.Item1
                                                                          && row.CustomerBranchID == CustomerBranchID
                                                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                                                    DateTime schedueon;
                                                    if (IsAfter == null)
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }
                                                    else
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }

                                                    taskInstances.ForEach(entrytaskinstance =>
                                                    {
                                                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                                                        taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
                                                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleon.ID;
                                                        taskScheduleOn.ForMonth = nextDateMonth.Item2;
                                                        taskScheduleOn.ForPeriod = nextDateMonth.Item3;
                                                        taskScheduleOn.ScheduleOn = schedueon; // nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                        taskScheduleOn.IsActive = true;
                                                        taskScheduleOn.IsUpcomingNotDeleted = true;
                                                        entities.TaskScheduleOns.Add(taskScheduleOn);
                                                        entities.SaveChanges();

                                                        var AssignedTaskRole = TaskManagment.GetAssignedTaskUsers((int)entrytaskinstance.ID);
                                                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                        if (performerTaskRole != null)
                                                        {
                                                            var user = UserManagement.GetByID((int)performerTaskRole.UserID);
                                                            TaskTransaction tasktransaction = new TaskTransaction()
                                                            {
                                                                TaskInstanceId = entrytaskinstance.ID,
                                                                TaskScheduleOnID = taskScheduleOn.ID,
                                                                ComplianceScheduleOnID = complianceScheduleon.ID,
                                                                CreatedBy = performerTaskRole.UserID,
                                                                CreatedByText = user.FirstName + " " + user.LastName,
                                                                StatusId = 1,
                                                                Remarks = "New task assigned."
                                                            };

                                                            TaskManagment.CreateTaskTransaction(tasktransaction);

                                                            foreach (var roles in AssignedTaskRole)
                                                            {
                                                                if (roles.RoleID != 6)
                                                                {
                                                                    DateTime schdulon;
                                                                    if (IsAfter == true)
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    else
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    TaskManagment.CreateTaskReminders(entrytaskinstance.TaskId, compliances.ID, roles.ID, schdulon, complianceScheduleon.ID, IsAfter);
                                                                }
                                                            }
                                                        }

                                                    });
                                                });
                                            });
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                
                var errorvar = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                var date = DateTime.Now;
                var FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;

                msg = GetLogDetails("RLCS_ComplianceManagement", FunctionName, date, ex.Message + "----\r\n" + ex.InnerException, ex.StackTrace, errorvar);


                //msg.ClassName = "RLCS_ComplianceManagement";
                //msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);
            }
        }

        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }

        public static List<long> GetUpcomingReminderDaysDetail(long ComplianceAssignmentId, int FreqID, string Type)
        {
            List<long> output = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User User = (from row in entities.Users
                             join row1 in entities.ComplianceAssignments
                             on row.ID equals row1.UserID
                             where row1.ID == ComplianceAssignmentId
                             && row.IsActive == true
                             && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (User != null)
                {
                    long customerID = Convert.ToInt32(User.CustomerID);
                    long UserID = Convert.ToInt32(User.ID);

                    var UserObj = (from row in entities.ReminderTemplate_UserMapping
                                   where row.CustomerID == customerID
                                   && row.UserID == UserID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   && row.Frequency == FreqID
                                   && row.Flag == "UN"
                                   select row).ToList();

                    if (UserObj.Count > 0)
                    {
                        output.Clear();
                        output = UserObj.Select(x => x.TimeInDays).ToList();
                    }
                    else
                    {
                        var CustObj = (from row in entities.ReminderTemplate_CustomerMapping
                                       where row.CustomerID == customerID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       && row.Frequency == FreqID
                                       && row.Flag == "UN"
                                       select row).ToList();

                        if (CustObj.Count > 0)
                        {
                            output.Clear();
                            output = CustObj.Select(x => x.TimeInDays).ToList();
                        }
                        else
                        {
                            output.Clear();
                            //var StdObj = (from row in entities.ReminderTemplates
                            //              where row.Frequency == FreqID
                            //              && row.IsSubscribed == true
                            //              select row).ToList();

                            //if (StdObj.Count > 0)
                            //{
                            //    output.Clear();
                            //    output = StdObj.Select(x => Convert.ToInt64(x.TimeInDays)).ToList();
                            //}
                        }
                    }
                }
                return output;
            }
        }

        public static bool CreateTransaction(ComplianceTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.ComplianceTransactions.Add(transaction);
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();

                var errorvar = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                var date = DateTime.Now;
                var FunctionName = "CreateTransaction";

                msg = GetLogDetails("RLCS_ComplianceManagement", FunctionName, date, ex.Message + "----\r\n" + ex.InnerException, ex.StackTrace, errorvar);


                //msg.ClassName = "RLCS_ComplianceManagement";
                //msg.FunctionName = "CreateTransaction";
                //msg.CreatedOn = DateTime.Now;
                //msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                //msg.StackTrace = ex.StackTrace;
                //msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");


                ComplianceManagement.SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }

        public static int CheckHoliday(long ComplianceID, long complianceInstanceId, DateTime ScheduleOn, int duebefore, int isonlineoffline)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int cnt = 0;
                Boolean flag = false;

                //before
                if ((isonlineoffline == 0 || isonlineoffline == null) && duebefore == 1)
                {
                    while (flag == false)
                    {
                        var data = (from row in entities.Sp_CheckHoliday(ComplianceID, complianceInstanceId, ScheduleOn.Day, ScheduleOn.Month, ScheduleOn.Year)
                                    select row).ToList();
                        if (data.Count == 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            cnt += 1;
                            flag = false;
                            ScheduleOn = ScheduleOn.AddDays(-1);
                            if (ScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                            {
                                cnt += 1;
                                ScheduleOn = ScheduleOn.AddDays(-1);
                            }
                        }
                    }
                }
                else
                {
                    while (flag == false)
                    {
                        var data = (from row in entities.Sp_CheckHoliday(ComplianceID, complianceInstanceId, ScheduleOn.Day, ScheduleOn.Month, ScheduleOn.Year)
                                    select row).ToList();
                        if (data.Count == 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            cnt += 1;
                            flag = false;
                            ScheduleOn = ScheduleOn.AddDays(1);
                            if (ScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                            {
                                cnt += 1;
                                ScheduleOn = ScheduleOn.AddDays(1);
                            }
                        }
                    }
                }

                return cnt;
            }
        }

        public static Tuple<string, string> Get_RLCSMonthYear(int frequency, string strPeriod)
        {
            Tuple<string, string> tupleRLCSMonthYear = new Tuple<string, string>(string.Empty, string.Empty);

            try
            {
                string rlcsMonthValue = string.Empty;
                string rlcsYearValue = string.Empty;

                if (frequency == 0) //Monthly
                {
                    string monthValue = strPeriod.Substring(0, 3).Trim();
                    rlcsMonthValue = GetMonthIndexFromAbbreviatedMonth(monthValue);

                    string yearValue = strPeriod.Substring(4, 2).Trim();
                    rlcsYearValue = "20" + yearValue;
                }
                else if (frequency == 1 || frequency == 2) //Quarterly  //Half Yearly
                {
                    string lastMonthinQuarter = GetLast(strPeriod, 6);

                    if (!string.IsNullOrEmpty(lastMonthinQuarter))
                    {
                        string monthValue = lastMonthinQuarter.Substring(0, 3).Trim();
                        rlcsMonthValue = GetMonthIndexFromAbbreviatedMonth(monthValue);

                        string yearValue = lastMonthinQuarter.Substring(4, 2).Trim();
                        rlcsYearValue = "20" + yearValue;
                    }
                }
                else if (frequency == 3) //Annual
                {
                    if (!string.IsNullOrEmpty(strPeriod))
                    {
                        if (strPeriod.Trim().ToUpper().Contains("CY"))
                        {
                            rlcsMonthValue = "12";
                            rlcsYearValue = "20" + GetLast(strPeriod, 2);
                        }
                        if (strPeriod.Trim().ToUpper().Contains("FY"))
                        {
                            rlcsMonthValue = "03";
                            rlcsYearValue = "20" + GetLast(strPeriod, 2);
                        }
                    }
                }
                else if (frequency == 5) //BiAnnual
                {
                    if (strPeriod.Contains("FY"))
                    {
                        rlcsMonthValue = "03";
                    }
                    else if (strPeriod.Contains("CY"))
                    {
                        rlcsMonthValue = "12";
                    }

                    rlcsYearValue = "20" + GetLast(strPeriod, 2);
                }

                tupleRLCSMonthYear = Tuple.Create(rlcsMonthValue, rlcsYearValue);
                return tupleRLCSMonthYear;
            }
            catch (Exception ex)
            {
                return tupleRLCSMonthYear;
            }
        }

        public static List<ComplianceSchedule> GetScheduleByComplianceID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceSchedules
                                    where row.ComplianceID == complianceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }

        public static DateTime? GetComplianceInstanceScheduledon(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime ScheduleOnID = (from row in entities.ComplianceInstances
                                         where row.ID == ComplianceInstanceID
                                         select row.ScheduledOn).FirstOrDefault();
                if (ScheduleOnID != null)
                {
                    return ScheduleOnID;
                }
                else
                {
                    return null;
                }

            }
        }

        public static Tuple<DateTime, string, long> GetNextDatePeriodicallyWithClientBased(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, int frequency)
        {
            int formonthscheduleR = -1;
            var complianceSchedule = GetScheduleByComplianceID(complianceID);//.OrderBy(entry => entry.ForMonth).ToList();           
            DateTime date = complianceSchedule.Select(row => new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > scheduledOn).FirstOrDefault();
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            if (frequency == 3 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 5 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 6 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 2)
            {
                List<Tuple<DateTime, long>> complianceScheduleDates;
                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "07").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "07").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" || row.SpecialDate.Substring(2, 2) == "10").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "10").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                long ActualForMonth = 0;
                if (frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(lastSchedule.ScheduleOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
            }
            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    if (frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                    if (frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }
            string forMonth = string.Empty;
            if (complianceSchedule.Count > 0)
            {
                string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");
                var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();
                forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth, (byte)frequency);
                formonthscheduleR = ForMonthSchedule.ForMonth;
            }
            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);
        }

        public static Tuple<DateTime, string, long> GetNextDateWithClientBased(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, bool iseffectivedate, int frequency, long customerid)
        {
            var complianceSchedule = GetScheduleByClientFrequencyComplianceID(complianceID, customerid);
            //var objCompliance = GetByID(complianceID);
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    if (iseffectivedate)
                    {
                        #region Monthly
                        if (frequency == 0)
                        {
                            lastPeriod = scheduledOn.Month - 1;
                        }
                        #endregion
                        #region   Half Yearly
                        else if (frequency == 2)
                        {
                            if (complianceSchedule[0].ForMonth == 1)
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Annaully
                        else if (frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {

                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 2)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 4;
                                    }
                                }

                            }
                        }
                        #endregion
                        #region Quarterly
                        else if (frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region FourMonthly
                        else if (frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Two Yearly
                        else if (frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                }
                            }
                        }
                        #endregion
                        else
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {
                                    lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                    }
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduleOn.Month), (byte)frequency);
                }
            }
            else
            {
                #region Monthly
                if (frequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (frequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            if (frequency != 0 || frequency != 1)
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if (frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                #endregion

                #region Second Part
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (frequency == 1)
                        {

                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 10).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" && entry.ForMonth == 10).Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            if (frequency == 0)
                            {

                                if (lastPeriod == 11)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                if (lastPeriod == 7)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.SpecialDate.Substring(0, 2) == "10" && row.ForMonth == 8).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                            if (frequency == 2)
                            {
                                if (lastPeriod == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else if (lastPeriod != 10)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "07" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        if (scheduledOn.Year <= DateTime.Today.Year - 1)
                                        {
                                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                        }
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {

                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                                    }
                                }
                            }
                            if (frequency == 4)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 9).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                //if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                //{
                                //    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                //}
                                if (lastPeriod == 9)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }

                            }
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02"
                || row.SpecialDate.Substring(2, 2) == "03"
                || row.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "07"
                || row.SpecialDate.Substring(2, 2) == "08"
                || row.SpecialDate.Substring(2, 2) == "09"
                || row.SpecialDate.Substring(2, 2) == "10"
                || row.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (frequency == 3)
                    {
                        complianceScheduleDates.Add(complianceSchedule.Where(entry =>
                        entry.SpecialDate.Substring(2, 2) == "02"
                        || entry.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "07"
                        || entry.SpecialDate.Substring(2, 2) == "08"
                        || entry.SpecialDate.Substring(2, 2) == "09"
                        || entry.SpecialDate.Substring(2, 2) == "10"
                        || entry.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                    if (frequency == 2)
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "08").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "08").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                    else
                    {
                        if (frequency == 0)
                        {
                            if (lastPeriod == 11)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else if (frequency == 1)
                        {
                            if (frequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && frequency == 3).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && frequency == 2).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            //if (frequency != 0 || frequency != 1)
            //{
            //    if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
            //    {
            //        if (frequency == 3)                       
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 5)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 6)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 1)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 0)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 2)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 4)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else
            //            complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //    }
            //    else
            //    {
            //        if (frequency == 3)                       
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 5)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 6)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 1)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 0)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 2)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (frequency == 4)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else
            //            complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //    }
            //    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
            //    {
            //        if (frequency == 3)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (frequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (frequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //        {
            //            if (frequency == 0 || frequency == 1)
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            else
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        }
            //    }
            //    //Added by Rahul on 5 Jan 2015
            //    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
            //    {
            //        if (frequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (frequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //        {
            //            if (frequency == 0 || frequency == 1)
            //            {
            //                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
            //                {
            //                    if (frequency == 1)
            //                    {
            //                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
            //                        {
            //                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                        }
            //                    }
            //                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                }
            //                else
            //                {
            //                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
            //                    {
            //                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                    }
            //                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
            //                    {
            //                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            }
            //        }
            //    }
            //    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
            //    {
            //        if (frequency == 3)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        if (frequency == 2)
            //        {
            //            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
            //            {
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            }
            //        }
            //    }
            //    //Added by Rahul on 15 Nov 2016
            //    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" || row.SpecialDate.Substring(2, 2) == "11").FirstOrDefault() != null)
            //    {
            //        if (frequency == 3)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        if (frequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (frequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //        {
            //            if (frequency == 0 || frequency == 1)
            //            {
            //                if (frequency == 1)
            //                {
            //                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
            //                    {
            //                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                    }
            //                }
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            }
            //            else
            //            {
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

            //    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
            //    {
            //        if (frequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (frequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

            //    }
            //}
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (frequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (frequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), (byte)frequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static long GetCustomerBranchID(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long branchid = (from row in entities.ComplianceInstances
                                 where row.ID == complianceInstanceID
                                 select row.CustomerBranchID).FirstOrDefault();

                return branchid;
            }
        }

        public static long GetScheduledOnPresentOrNot(long ComplianceInstanceID, DateTime ScheduledOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                     where row.ComplianceInstanceID == ComplianceInstanceID && row.ScheduleOn == ScheduledOn
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row.ID).FirstOrDefault();
                return ScheduleOnID;
            }
        }

        public static string GetMonthIndexFromAbbreviatedMonth(string monthValue)
        {
            string rlcsMonthValue = string.Empty;
            int monthIndex = 0;
            string[] MonthNames = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames;
            monthIndex = Array.IndexOf(MonthNames, monthValue) + 1;

            if (monthIndex != 0)
            {
                if (monthIndex < 10)
                    rlcsMonthValue = "0" + monthIndex;
                else
                    rlcsMonthValue = monthIndex.ToString();
            }

            return rlcsMonthValue;
        }

        public static string GetLast(string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return string.Empty;
            return source.Substring(source.Length - tail_length);
        }

        public static string GetForMonthPeriodically(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 12;
                        year = (date.AddYears(-1)).ToString("yy");
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 10;
                        yearq = (date.AddYears(-1)).ToString("yy");
                    }


                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 2)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    //if (forMonth == 1)
                    //{
                    //    forMonth = 7;
                    //    year1 = (date.AddYears(-1)).ToString("yy");
                    //}
                    if (forMonth == 4)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 10)
                    {

                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (Convert.ToInt32(year1) - 1) +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 7)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 1)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 7)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (Convert.ToInt32(year1) - 1) +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = date.ToString("yy");
                        endFinancial1Year = (date.AddYears(1)).ToString("yy"); date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    break;

                case 4:
                    string year2 = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 9;
                        year2 = (date.AddYears(-1)).ToString("yy");
                    }


                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 3)).Substring(0, 3) + " " + year2 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2;
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial2Year = (date.AddYears(1)).ToString("yy");
                    }
                    else
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;

                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-6)).ToString("yy");
                        endFinancial7Year = (date.AddYears(1)).ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }

        public static List<ComplianceScheduleClientFrequency> GetScheduleByClientFrequencyComplianceID(long complianceID, long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceScheduleClientFrequencies
                                    where row.ComplianceID == complianceID
                                    && row.CustomerID == CustomerID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }

        public static long GetLastPeriodFromDate(int forMonth, byte? Frequency)
        {
            long forMonthName = 0;
            switch (Frequency)
            {
                case 0:
                    if (forMonth == 1)
                    {
                        forMonthName = 12;
                    }
                    else
                    {
                        forMonthName = forMonth - 1;
                    }
                    break;
                case 1:
                    if (forMonth == 1)
                    {
                        forMonthName = 10;
                    }
                    else
                    {
                        forMonthName = forMonth - 3;
                    }
                    break;
                case 2:
                    if (forMonth == 1)
                    {
                        forMonthName = 7;
                    }
                    else
                    {
                        forMonthName = forMonth - 6;
                    }
                    break;

                case 3:
                    forMonthName = 1;
                    break;

                case 4:

                    if (forMonth == 1)
                    {
                        forMonthName = 9;
                    }
                    else
                    {
                        forMonthName = forMonthName - 4;
                    }

                    break;
                case 5:
                    forMonthName = 1;
                    break;
                case 6:

                    forMonthName = 1;
                    break;

                default:
                    forMonthName = 0;
                    break;
            }

            return forMonthName;
        }

        public static long GetNextMonthFromFrequency(long LastforMonth, byte? Frequency)
        {
            long NextPeriod;
            switch (Frequency)
            {
                case 0:
                    if (LastforMonth == 12)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 1;
                    break;

                case 1:
                    if (LastforMonth == 10)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 3;
                    break;

                case 2:
                    if (LastforMonth == 7)
                        NextPeriod = 1;
                    else if (LastforMonth == 10)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 6;
                    break;

                case 3:
                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 4:

                    if (LastforMonth == 9)
                        NextPeriod = 1;
                    else if (LastforMonth == 12)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 4;

                    break;
                case 5:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 6:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                default:
                    NextPeriod = 0;
                    break;
            }

            return NextPeriod;
        }

        public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");
                    if (date > DateTime.Today.Date)
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            if (forMonth == 11)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else if (forMonth == 12)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else
                            {
                                year = (date).ToString("yy");
                            }
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    else
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    break;

                case 3:
                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        if (date.Month >= 4)
                        {
                            startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                            endFinancial1Year = date.ToString("yy");
                        }
                        else if (date.Month >= 3)
                        {
                            startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                            endFinancial1Year = date.ToString("yy");
                        }
                        else
                        {
                            startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                            endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        }

                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }
                    break;

                case 4:

                    string year2 = date.ToString("yy");
                    if (forMonth == 9)
                    {
                        if (Convert.ToInt32(year2) >= Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year2 = (date.AddYears(-1)).ToString("yy");
                        }
                        else
                        {
                            year2 = (date).ToString("yy");
                        }

                    }
                    if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                                       " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    }
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    else if (forMonth == 4)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                        forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }

        public static List<TempDeactivateView> GetDetailsToDeactive(int lcCustomerBranch = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TempDeactivateView> TempGetDetailsToDeactivate = (from row in entities.TempDeactivateViews
                                                                       where row.CustomerBranchID == lcCustomerBranch
                                                                       select row).ToList();

                var TempTransactionDtls = (from row in entities.TempDeactivateViews
                                           join ci in entities.ComplianceTransactions
                                           on row.ComplianceInstanceID equals ci.ComplianceInstanceId
                                           where ci.StatusId != 1
                                           select row.ComplianceInstanceID).ToList();

                TempGetDetailsToDeactivate.RemoveAll(a => TempTransactionDtls.Contains(a.ComplianceInstanceID));
                return TempGetDetailsToDeactivate;

            }
        }

        public static List<TempAssignmentView> GetTempAssignedDetails(List<int> lstCustBranch)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<TempAssignmentView> TempAssignedDetails = (from row in entities.TempAssignmentViews
                                                                where lstCustBranch.Contains(row.CustomerBranchID)
                                                                select row).ToList();

                return TempAssignedDetails;
            }
        }

        public static void DeactiveTempAssignmentDetails(int TempAssignmentID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTable TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTables
                                                                  where row.ID == TempAssignmentID
                                                                  select row).FirstOrDefault();
                TempAssignmentIDToDeactive.IsActive = false;
                entities.SaveChanges();
            }
        }

        public static void ActivateTempAssignmentDetails(int TempAssignmentID, long ComplianceInstanceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                TempAssignmentTable TempAssignmentIDToDeactive = (from row in entities.TempAssignmentTables
                                                                  where row.ID == TempAssignmentID
                                                                  select row).FirstOrDefault();
                TempAssignmentIDToDeactive.IsActive = true;
                entities.SaveChanges();

                ComplianceInstance ComplianceInstance = (from row in entities.ComplianceInstances
                                                         where row.ID == ComplianceInstanceId
                                                         select row).FirstOrDefault();
                ComplianceInstance.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static ComplianceInstance GetComplianceInstance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.ComplianceInstances
                                   where row.ID == ComplianceInstanceID
                                   && row.IsDeleted == false
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }

        public static Tuple<DateTime, string, long> GetNextDateDaily(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, bool iseffectivedate)
        {
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            var getinstancescheduleondate = GetComplianceInstance(ComplianceInstanceID);
            DateTime date = DateTime.Now;
            if (lastSchedule == null)
            {
                if (getinstancescheduleondate.ScheduledOn.Date < scheduledOn.Date)
                {
                    date = new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month), Convert.ToInt32(scheduledOn.Day));
                }
                else
                {
                    date = new DateTime(getinstancescheduleondate.ScheduledOn.Year, Convert.ToInt32(getinstancescheduleondate.ScheduledOn.Month), Convert.ToInt32(getinstancescheduleondate.ScheduledOn.Day));
                }
            }
            else
            {
                int days = -1;
                int totaldays = DateTime.DaysInMonth(scheduledOn.Year, scheduledOn.Month);
                days = Convert.ToInt32(scheduledOn.Day + 1);
                if (totaldays >= days)
                {
                    date = new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month), days);
                }
                else
                {
                    date = new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month + 1), 1);
                }
            }
            if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
            }
            string forMonth = string.Empty;
            long ActualForMonth = 0;
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static Tuple<DateTime, string, long> GetNextDateWeekly(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, int WeeklyDayFromCompliance)
        {
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            var getinstancescheduleondate = GetComplianceInstance(ComplianceInstanceID);
            DateTime date = DateTime.Now;
            if (lastSchedule == null)
            {
                DateTime curruntDate;
                curruntDate = DateTime.UtcNow.AddDays(30);
                DateTime dtinstance = getinstancescheduleondate.ScheduledOn.Date;
                while (dtinstance < curruntDate)
                {
                    int weekdayFromInstance = Convert.ToInt32(dtinstance.DayOfWeek);
                    if (weekdayFromInstance == WeeklyDayFromCompliance)
                    {
                        date = new DateTime(dtinstance.Year, Convert.ToInt32(dtinstance.Month), Convert.ToInt32(dtinstance.Day));

                        date = dtinstance; // new DateTime(dtinstance.Year, Convert.ToInt32(dtinstance.Month), Convert.ToInt32(dtinstance.Day));

                        if (date >= getinstancescheduleondate.ScheduledOn)
                        {
                            break;
                        }
                        else
                        {
                            dtinstance = dtinstance.AddDays(1);
                        }
                    }
                    else
                    {
                        dtinstance = dtinstance.AddDays(1);
                    }
                }
            }
            else
            {
                date = scheduledOn.AddDays(7); // new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month), Convert.ToInt32(scheduledOn.Day + 7));
            }

            string forMonth = string.Empty;
            long ActualForMonth = 0;
            //forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.IFrequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static Compliance GetByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Compliances.Include("ComplianceParameters")
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }

        public static Tuple<DateTime, string, long> GetNextDatePeriodically(DateTime scheduledOn, long complianceID, long ComplianceInstanceID)
        {
            int formonthscheduleR = -1;
            var complianceSchedule = GetScheduleByComplianceID(complianceID);//.OrderBy(entry => entry.ForMonth).ToList();
            var objCompliance = GetByID(complianceID);
            DateTime date = complianceSchedule.Select(row => new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > scheduledOn).FirstOrDefault();
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            if (objCompliance.Frequency == 3 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (objCompliance.Frequency == 5 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (objCompliance.Frequency == 6 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (objCompliance.Frequency == 2)
            {
                List<Tuple<DateTime, long>> complianceScheduleDates;
                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "07").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "07").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" || row.SpecialDate.Substring(2, 2) == "10").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "10").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                long ActualForMonth = 0;
                if (objCompliance.Frequency == 2)
                {

                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(lastSchedule.ScheduleOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.Frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.Frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }
            string forMonth = string.Empty;

            if (complianceSchedule.Count > 0)
            {
                string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

                var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                formonthscheduleR = ForMonthSchedule.ForMonth;
            }


            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);


        }


        public static Tuple<DateTime, string, long> GetNextDate(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, bool iseffectivedate)
        {
            var complianceSchedule = GetScheduleByComplianceID(complianceID);
            var objCompliance = GetByID(complianceID);
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    if (iseffectivedate)
                    {
                        #region Monthly
                        if (objCompliance.Frequency == 0)
                        {
                            lastPeriod = scheduledOn.Month - 1;
                        }
                        #endregion
                        #region   Half Yearly
                        else if (objCompliance.Frequency == 2)
                        {
                            if (complianceSchedule[0].ForMonth == 1)
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Annaully
                        else if (objCompliance.Frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {

                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 2)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 4;
                                    }
                                }

                            }
                        }
                        #endregion
                        #region Quarterly
                        else if (objCompliance.Frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region FourMonthly
                        else if (objCompliance.Frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Two Yearly
                        else if (objCompliance.Frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                }
                            }
                        }
                        #endregion
                        else
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {
                                    lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }

                    }
                    else
                    {
                        lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                    }
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduleOn.Month), objCompliance.Frequency);
                }
            }
            else
            {
                #region Monthly
                if (objCompliance.Frequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (objCompliance.Frequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (objCompliance.Frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (objCompliance.Frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (objCompliance.Frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (objCompliance.Frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            //if (objCompliance.Frequency != 0 || objCompliance.Frequency != 1)
            //{
            //    if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
            //    {
            //        if (objCompliance.Frequency == 3)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 5)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 6)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 1)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 0)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 2)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 4)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else
            //            complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //    }
            //    else
            //    {
            //        if (objCompliance.Frequency == 3)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 5)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 6)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 1)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 0)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 2)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else if (objCompliance.Frequency == 4)
            //            complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //        else
            //            complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
            //    }
            //    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
            //    {
            //        if (objCompliance.Frequency == 3)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (objCompliance.Frequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (objCompliance.Frequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //        {
            //            if (objCompliance.Frequency == 0 || objCompliance.Frequency == 1)
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            else
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        }
            //    }
            //    //Added by Rahul on 5 Jan 2015
            //    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
            //    {
            //        if (objCompliance.Frequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (objCompliance.Frequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //        {
            //            if (objCompliance.Frequency == 0 || objCompliance.Frequency == 1)
            //            {
            //                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
            //                {
            //                    if (objCompliance.Frequency == 1)
            //                    {
            //                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
            //                        {
            //                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                        }
            //                    }
            //                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                }
            //                else
            //                {
            //                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
            //                    {
            //                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                    }
            //                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
            //                    {
            //                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            }
            //        }
            //    }
            //    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
            //    {
            //        if (objCompliance.Frequency == 3)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        if (objCompliance.Frequency == 2)
            //        {
            //            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
            //            {
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            }
            //        }
            //    }
            //    //Added by Rahul on 15 Nov 2016
            //    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" || row.SpecialDate.Substring(2, 2) == "11").FirstOrDefault() != null)
            //    {
            //        if (objCompliance.Frequency == 3)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        if (objCompliance.Frequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (objCompliance.Frequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //        {
            //            if (objCompliance.Frequency == 0 || objCompliance.Frequency == 1)
            //            {
            //                if (objCompliance.Frequency == 1)
            //                {
            //                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
            //                    {
            //                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //                    }
            //                }
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            }
            //            else
            //            {
            //                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

            //    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
            //    {
            //        if (objCompliance.Frequency == 5)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else if (objCompliance.Frequency == 6)
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
            //        else
            //            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

            //    }
            //}

            if (objCompliance.Frequency != 0 || objCompliance.Frequency != 1)
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                #endregion

                #region Second Part
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.Frequency == 1)
                        {

                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 10).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" && entry.ForMonth == 10).Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            if (objCompliance.Frequency == 0)
                            {

                                if (lastPeriod == 11)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                if (lastPeriod == 7)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.SpecialDate.Substring(0, 2) == "10" && row.ForMonth == 8).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                            if (objCompliance.Frequency == 2)
                            {
                                if (lastPeriod == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else if (lastPeriod != 10)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "07" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        if (scheduledOn.Year <= DateTime.Today.Year - 1)
                                        {
                                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                        }
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {

                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                                    }
                                }
                            }
                            if (objCompliance.Frequency == 4)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 9).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                //if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                //{
                                //    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                //}
                                if (lastPeriod == 9)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }

                            }
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02"
                || row.SpecialDate.Substring(2, 2) == "03"
                || row.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "07"
                || row.SpecialDate.Substring(2, 2) == "08"
                || row.SpecialDate.Substring(2, 2) == "09"
                || row.SpecialDate.Substring(2, 2) == "10"
                || row.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                    {
                        complianceScheduleDates.Add(complianceSchedule.Where(entry =>
                        entry.SpecialDate.Substring(2, 2) == "02"
                        || entry.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "07"
                        || entry.SpecialDate.Substring(2, 2) == "08"
                        || entry.SpecialDate.Substring(2, 2) == "09"
                        || entry.SpecialDate.Substring(2, 2) == "10"
                        || entry.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                    if (objCompliance.Frequency == 2)
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "08").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "08").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                    else
                    {
                        if (objCompliance.Frequency == 0)
                        {
                            if (lastPeriod == 11)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else if (objCompliance.Frequency == 1)
                        {
                            if (objCompliance.Frequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && objCompliance.Frequency == 3).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && objCompliance.Frequency == 2).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (objCompliance.Frequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (objCompliance.Frequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.Frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.Frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.Frequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }
    }
}
