﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Threading;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class CustomHeaderHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(
        HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            request.Headers.Add("Authorization", "5E8E3D45-172D-4D58-8DF8-EB0B0E");
            request.Headers.Add("X-User-Id-1", "f2UKmAm0KlI98bEYGGxEHQ==");

            return base.SendAsync(request, cancellationToken);
        }
    }
    
}
