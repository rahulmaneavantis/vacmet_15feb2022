﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static com.VirtuosoITech.ComplianceManagement.Business.RLCS.RLCSAPIClasses;
using System.Threading;
using System.Net.Mail;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using Newtonsoft.Json;
using System.Data;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCSManagement
    {
        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPFOrESICOrPTOrLWFCode(string clientID, string challanType, string codeValue, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                //List<CustomerBranch> lstCustomerBranches = new List<CustomerBranch>();

                if (!string.IsNullOrEmpty(challanType))
                {
                    if (challanType.Trim().ToUpper().Equals("EPF"))
                    {
                        //lstCustomerBranches = (from row in entities.CustomerBranches
                        //                       join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                        //                       on row.ID equals row1.AVACOM_BranchID
                        //                       where row1.AVACOM_BranchID != null
                        //                       && row1.CL_PF_Code != null
                        //                       && row1.CL_PF_Code != ""
                        //                       //&& row1.CL_PF_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                        //                       && row1.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                        //                       && row1.BranchType.Equals(branchType)
                        //                       select row1).ToList();

                        lstCustomerBranches = (from RCB in entities.RLCS_Client_BasicDetails
                                               join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               on RCB.CB_ClientID equals RCCM.CM_ClientID
                                               where RCCM.AVACOM_BranchID != null                                               
                                               && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                               && RCB.CB_PF_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && RCB.CB_PF_Code != null
                                               && RCCM.CM_Status == "A"
                                               && RCCM.BranchType == branchType
                                               //&& RCB.IsProcessed == true
                                               select RCCM).ToList();
                    }
                    else if (challanType.Trim().ToUpper().Equals("ESI"))
                    {
                        //lstCustomerBranches = (from row in entities.CustomerBranches
                        //                       join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                        //                       on row.ID equals row1.AVACOM_BranchID
                        //                       where row1.AVACOM_BranchID != null
                        //                       && row1.CL_ESIC_Code != null
                        //                       && row1.CL_ESIC_Code != ""
                        //                       //&& row1.CL_ESIC_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                        //                       && row1.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                        //                       && row1.BranchType.Equals(branchType)
                        //                       select row1).ToList();

                        lstCustomerBranches = (from REM in entities.RLCS_Employee_Master
                                               join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               on REM.AVACOM_BranchID equals RCCM.AVACOM_BranchID
                                               where RCCM.AVACOM_BranchID != null
                                               && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                               && REM.EM_Client_ESI_Number.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && REM.EM_Client_ESI_Number != null                                               
                                               && REM.EM_Status == "A"
                                               && RCCM.CM_Status == "A"
                                               && RCCM.BranchType == "B"
                                               //&& REM.IsProcessed == true
                                               select RCCM).ToList();
                    }
                    else if (challanType.Trim().ToUpper().Equals("PT"))
                    {
                        lstCustomerBranches = (from REM in entities.RLCS_Employee_Master
                                               join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               on REM.AVACOM_BranchID equals RCCM.AVACOM_BranchID
                                               where RCCM.AVACOM_BranchID != null
                                               && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                               && REM.EM_Client_PT_State.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && REM.EM_Client_PT_State != null                                               
                                               && REM.EM_Status == "A"
                                               && RCCM.CM_Status == "A"
                                               && RCCM.BranchType == "B"                                               
                                               //&& REM.IsProcessed == true
                                               select RCCM).ToList();
                    }
                }

                return lstCustomerBranches;
            }
        }
        //27Aug2021GG
        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPFCodeNew(string clientID, string challanType, string codeValue, string branchType, int branchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                //List<CustomerBranch> lstCustomerBranches = new List<CustomerBranch>();

                if (!string.IsNullOrEmpty(challanType))
                {
                    if (challanType.Trim().ToUpper().Equals("EPF") && branchType != "B")
                    {

                        lstCustomerBranches = (from RCB in entities.RLCS_Client_BasicDetails
                                               join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               on RCB.CB_ClientID equals RCCM.CM_ClientID
                                               where RCCM.AVACOM_BranchID != null
                                               && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                               && RCB.CB_PF_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && RCB.CB_PF_Code != null
                                               && RCCM.CM_Status == "A"
                                               && RCCM.BranchType == "E"
                                               select RCCM).Distinct().ToList();
                    }
                    else
                    {
                        lstCustomerBranches = (from RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               where RCCM.AVACOM_BranchID == branchID
                                               && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                               && RCCM.CL_PF_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && RCCM.CL_PF_Code != null
                                               && RCCM.CM_Status == "A"
                                               && RCCM.BranchType == "B"
                                               select RCCM).Distinct().ToList();
                    }
                }

                return lstCustomerBranches;
            }
        }
        //END
        public static bool CheckScopeApplicability(int customerID, string scopeID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var corporateRecord = (from RSWM in entities.RLCS_ScopeOfWork_Master
                                       join RCBCLM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                       on RSWM.ClientID equals RCBCLM.CM_ClientID
                                       join CB in entities.CustomerBranches
                                       on RCBCLM.AVACOM_BranchID equals CB.ID
                                       where RCBCLM.BranchType == "E"
                                       && RSWM.Status == "A"
                                       && RCBCLM.CM_Status == "A"
                                       && CB.CustomerID == customerID
                                       && RSWM.ScopeID == scopeID
                                       select RSWM).FirstOrDefault();

                if (corporateRecord != null)
                    return true;
                else
                    return false;
            }
        }

        public static void Update_ProcessedStatus_Corporate(int customerID, string corporateID, bool status)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var corporateRecord = (from row in entities.RLCS_Customer_Corporate_Mapping
                                       where row.CO_CorporateID == corporateID
                                       && row.AVACOM_CustomerID == customerID
                                       select row).FirstOrDefault();

                if (corporateRecord != null)
                {
                    corporateRecord.IsProcessed = status;
                    entities.SaveChanges();
                }
            }
        }

        public static bool CreateUpdate_Partial_CustomerBranch_ClientsOrLocation_Mapping(RLCS_CustomerBranch_ClientsLocation_Mapping _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.AVACOM_BranchID == _objRecord.AVACOM_BranchID
                                      && row.CM_ClientID.Trim().ToUpper().Equals(_objRecord.CM_ClientID.Trim().ToUpper())
                                      && row.BranchType.Trim().ToUpper().Equals(_objRecord.BranchType.Trim().ToUpper())
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                        //prevRecord.AVACOM_BranchID = _objRecord.AVACOM_BranchID;
                        //prevRecord.BranchType = _objRecord.BranchType;
                        prevRecord.AVACOM_CustomerID = _objRecord.AVACOM_CustomerID;
                        prevRecord.AVACOM_BranchName = _objRecord.AVACOM_BranchName;
                        prevRecord.CO_CorporateID = _objRecord.CO_CorporateID;
                        prevRecord.CL_ID = _objRecord.CL_ID;
                        prevRecord.CM_ClientID = _objRecord.CM_ClientID;
                        prevRecord.CM_ClientName = _objRecord.CM_ClientName;
                        //prevRecord.CM_EstablishmentType = _objRecord.CM_EstablishmentType;
                        //prevRecord.CM_ServiceStartDate = _objRecord.CM_ServiceStartDate;
                        //prevRecord.CM_Country = _objRecord.CM_Country;
                        prevRecord.CM_Address = _objRecord.CM_Address;
                        prevRecord.CM_State = _objRecord.CM_State;
                        prevRecord.CL_PT_State = _objRecord.CL_PT_State;
                        prevRecord.CM_City = _objRecord.CM_City;
                        prevRecord.CM_Pincode = _objRecord.CM_Pincode;
                        prevRecord.CL_Pincode = _objRecord.CL_Pincode;
                        prevRecord.CM_Status = _objRecord.CM_Status;
                        //prevRecord.CM_RLCSAnchor = _objRecord.CM_RLCSAnchor;
                        //prevRecord.CM_BDAnchor = _objRecord.CM_BDAnchor;
                        //prevRecord.CM_ProcessAnchor = _objRecord.CM_ProcessAnchor;
                        //prevRecord.CL_LocationAnchor = _objRecord.CL_LocationAnchor;

                        prevRecord.IsProcessed = false;
                        prevRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;

                        entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static bool CreateUpdate_CustomerBranch_ClientsOrLocation_Mapping(RLCS_CustomerBranch_ClientsLocation_Mapping _objRecord)
        {
            try
            {
                bool saveSuccess = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var prevRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                      where row.AVACOM_BranchID == _objRecord.AVACOM_BranchID
                                      && row.CM_ClientID.Trim().ToUpper().Equals(_objRecord.CM_ClientID.Trim().ToUpper())                                      
                                      && row.BranchType.Trim().ToUpper().Equals(_objRecord.BranchType.Trim().ToUpper())
                                      select row).FirstOrDefault();

                    if (prevRecord != null)
                    {
                       // prevRecord.AVACOM_BranchID = _objRecord.AVACOM_BranchID;
                       // prevRecord.BranchType = _objRecord.BranchType;
                        prevRecord.AVACOM_CustomerID = _objRecord.AVACOM_CustomerID;
                        prevRecord.AVACOM_BranchName = _objRecord.AVACOM_BranchName;
                        prevRecord.CO_CorporateID = _objRecord.CO_CorporateID;
                        prevRecord.CL_ID = _objRecord.CL_ID;
                        prevRecord.CM_ClientID = _objRecord.CM_ClientID;
                        prevRecord.CM_ClientName = _objRecord.CM_ClientName;
                        prevRecord.CM_EstablishmentType = _objRecord.CM_EstablishmentType;
                        // prevRecord.CM_ServiceStartDate = _objRecord.CM_ServiceStartDate;
                        //  prevRecord.CM_Country = _objRecord.CM_Country;
                        prevRecord.CM_BonusPercentage = _objRecord.CM_BonusPercentage;
                        prevRecord.CM_State = _objRecord.CM_State;
                        prevRecord.CM_City = _objRecord.CM_City;
                        prevRecord.CM_Pincode = _objRecord.CM_Pincode;
                        prevRecord.CM_Status = _objRecord.CM_Status;
                        prevRecord.CM_AgreementID = _objRecord.CM_AgreementID;
                        prevRecord.CM_ContractType = _objRecord.CM_ContractType;
                        prevRecord.CM_RLCSAnchor = _objRecord.CM_RLCSAnchor;
                        prevRecord.CM_BDAnchor = _objRecord.CM_BDAnchor;
                        prevRecord.CM_RAMAnchor = _objRecord.CM_RAMAnchor;
                        prevRecord.CM_ProcessAnchor = _objRecord.CM_ProcessAnchor;
                        prevRecord.CM_ChallanAnchor = _objRecord.CM_ChallanAnchor;
                        prevRecord.CM_ReturnsAnchor = _objRecord.CM_ReturnsAnchor;
                        prevRecord.CM_LocationAnchor = _objRecord.CM_LocationAnchor;
                        prevRecord.CM_RCP_Anchor = _objRecord.CM_RCP_Anchor;
                        prevRecord.CM_AuditAnchor = _objRecord.CM_AuditAnchor;
                        prevRecord.CB_SPOCSANITATION = _objRecord.CB_SPOCSANITATION;
                        prevRecord.CB_SPOC_NAME = _objRecord.CB_SPOC_NAME;
                        prevRecord.CB_SPOC_LASTNAME = _objRecord.CB_SPOC_LASTNAME;
                        prevRecord.CB_SPOC_CONTACT = _objRecord.CB_SPOC_CONTACT;
                        prevRecord.CB_SPOC_EMAIL = _objRecord.CB_SPOC_EMAIL;
                        prevRecord.CB_SPOC_DESIGNATION = _objRecord.CB_SPOC_DESIGNATION;
                        prevRecord.CB_EP1_SANITATION = _objRecord.CB_EP1_SANITATION;
                        prevRecord.CB_EP1_NAME = _objRecord.CB_EP1_NAME;
                        prevRecord.CB_EP1LASTNAME = _objRecord.CB_EP1LASTNAME;
                        prevRecord.CB_EP1_CONTACT = _objRecord.CB_EP1_CONTACT;
                        prevRecord.CB_EP1_EMAIL = _objRecord.CB_EP1_EMAIL;
                        prevRecord.CB_EP1_DESIGNATION = _objRecord.CB_EP1_DESIGNATION;
                        prevRecord.CB_EP2_SANITATION = _objRecord.CB_EP2_SANITATION;
                        prevRecord.CB_EP2_NAME = _objRecord.CB_EP2_NAME;
                        prevRecord.CB_EP2LASTNAME = _objRecord.CB_EP2LASTNAME;
                        prevRecord.CB_EP2_CONTACT = _objRecord.CB_EP2_CONTACT;
                        prevRecord.CB_EP2_EMAIL = _objRecord.CB_EP2_EMAIL;
                        prevRecord.CB_EP2_DESIGNATION = _objRecord.CB_EP2_DESIGNATION;
                        prevRecord.CM_IsAventisClientOrBranch = _objRecord.CM_IsAventisClientOrBranch;
                        prevRecord.CM_Excemption = _objRecord.CM_Excemption;
                        prevRecord.CM_IsPOApplicable = _objRecord.CM_IsPOApplicable;
                        //prevRecord.IsCLRAApplicable = _objRecord.IsCLRAApplicable;
                        prevRecord.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        _objRecord.CreatedOn = DateTime.Now;

                        entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(_objRecord);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();
                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static int AddRLCSCustBranch(RLCS_CustomerBranch_ClientsLocation_Mapping RLCScustomerBranch)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int addid = 0;
                    try
                    {
                        RLCScustomerBranch.CreatedOn = DateTime.Now;

                        entities.RLCS_CustomerBranch_ClientsLocation_Mapping.Add(RLCScustomerBranch);
                        entities.SaveChanges();

                        addid = Convert.ToInt32(RLCScustomerBranch.ID);
                        return addid;
                    }
                    catch (Exception ex)
                    {
                        return addid = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static string GetRLCSBranchNameByID(int branchID, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                   where row.AVACOM_BranchID == branchID
                                   && row.BranchType== branchType
                                   select row).FirstOrDefault();

                if (_objRecord != null)
                    return _objRecord.AVACOM_BranchName;
                else
                    return string.Empty;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetRLCSBranchDetailsByID(int branchID, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objRecord = (from row in entities.CustomerBranches
                                  join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                  on row.ID equals row1.AVACOM_BranchID
                                  where row1.AVACOM_BranchID != null
                                  //&& row1.CL_ID != null
                                  && row1.AVACOM_BranchID == branchID
                                  && row1.BranchType.Equals(branchType)
                                  select row1).ToList();

                return _objRecord;
            }
        }

        public static string GetRLCSBranchNameByHistoricalRecordID(long recordID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objRecord = (from row in entities.RLCS_Historic_Data_Mapping
                                  where row.ID == recordID                                 
                                  select row).FirstOrDefault();

                if (_objRecord != null)
                    return _objRecord.LM_Branch;
                else
                    return string.Empty;
            }
        }

        public static string GetRLCSBranchNameByRecordID(long recordID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objRecord = (from row in entities.RLCS_RegulatoryLocationWiseMonthlyTracker_Mapping
                                  where row.ID == recordID
                                  select row).FirstOrDefault();

                if (_objRecord != null)
                    return _objRecord.LM_Branch;
                else
                    return string.Empty;
            }
        }

        public static List<NameValueHierarchy> GetAll_BranchHierarchy(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from Cust in entities.Customers
                             where Cust.ID == customerID
                             select Cust);

                hierarchy = (from row in query
                             select new NameValueHierarchy() { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList();

                IQueryable<CustomerBranch> query1 = (from row in entities.CustomerBranches
                                                     where row.IsDeleted == false
                                                     && row.CustomerID == customerID
                                                     select row);
                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities, query1);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities, IQueryable<CustomerBranch> query1)
        {
            var query = query1;
            if (isClient)
            {
                query = query1.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query1.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntities(item, false, entities, query1);
            }
        }

        public static List<long> BranchlistGrading = new List<long>();
        public static bool ChangePassword(RLCS_User_Mapping user)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                try
                {
                    RLCS_User_Mapping userToUpdate = (from entry in entities.RLCS_User_Mapping
                                                      where entry.AVACOM_UserID == user.AVACOM_UserID
                                                      select entry).FirstOrDefault();

                    if (userToUpdate != null)
                    {
                        userToUpdate.Password = user.Password;
                        userToUpdate.UpdatedOn = DateTime.Now;
                        userToUpdate.EnType = "A";
                        entities.SaveChanges();
                    }
                    return true;

                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                    
                    return false;
                }

            }
        }
        public static bool CheckProfileID_Authentication(string profileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.RLCS_User_Mapping
                            where row.ProfileID == profileID
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool CheckPaybookUserID_Authentication(string profileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.RLCS_User_Mapping
                            where row.Email == profileID
                            select row).FirstOrDefault();
                if (data == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }



        public static List<Customer> Get_RLCSUserCustomerMapping(string userID, string profileID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstAssignedCustomers = (from RUM in entities.RLCS_User_Mapping
                                                join RCCM in entities.RLCS_Customer_Corporate_Mapping
                                                on RUM.CustomerID equals RCCM.CO_CorporateID
                                                join CUST in entities.Customers
                                                on RCCM.AVACOM_CustomerID equals CUST.ID
                                                where RUM.UserID == userID
                                                && RUM.ProfileID == profileID
                                                && RUM.IsActive == true
                                                && CUST.IsDeleted == false
                                                && CUST.Status == 1
                                                && RCCM.CO_Status == "A"
                                                //&& CUST.IsServiceProvider==false
                                                select CUST).ToList();

                    return lstAssignedCustomers.Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static string GetProfileIDByUserID(long userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {   
                var userProfileID = (from row in entities.RLCS_User_Mapping
                                     where row.AVACOM_UserID == userID
                                     select row.ProfileID).FirstOrDefault();

                return userProfileID;
            }
        }

        public static string GetProfileIDByUserID(string userID, string corpID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var userProfileID = (from row in entities.RLCS_User_Mapping
                                     where row.UserID == userID
                                     && row.CustomerID == corpID
                                     select row.ProfileID).FirstOrDefault();

                return userProfileID;
            }
        }

        public static string GetAuthKeyByProfileID(string requestUrl, string ProfileID, string key)
        {
            string authKey = string.Empty;
            string authEncryptedKey = string.Empty;

            requestUrl += "UserMaster/GetAuthKeyBasedOnProfileId?ProfileId=" + ProfileID;

            //string responseData = RLCSAPIClasses.Invoke_Live("GET", requestUrl, "");
            string responseData = RLCSAPIClasses.Invoke("GET", requestUrl, "");

            if (!string.IsNullOrEmpty(responseData))
            {
                var _objAuthKeyResponse = JsonConvert.DeserializeObject<API_AuthKey_Response>(responseData);

                if (_objAuthKeyResponse != null)
                {
                    if (_objAuthKeyResponse.ProfileId != null && _objAuthKeyResponse.AuthKey != null)
                    {
                        if (_objAuthKeyResponse.ProfileId.Trim().ToUpper().Equals(ProfileID))
                        {
                            authEncryptedKey = _objAuthKeyResponse.AuthKey;

                            if (!string.IsNullOrEmpty(authEncryptedKey))
                            {
                                authKey = CryptographyHandler.decrypt(authEncryptedKey, key);
                            }
                        }
                    }
                }
            }

            return authKey;
        }

        public static List<Customer> Get_UserCustomerMapping(int userID)
        {
            try
            {               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstAssignedCustomers = (from UCM in entities.UserCustomerMappings
                                                join CUST in entities.Customers
                                                on UCM.CustomerID equals CUST.ID
                                                where UCM.UserID == userID
                                                && UCM.IsActive == true
                                                && CUST.IsDeleted == false
                                                && CUST.Status == 1
                                                //&& CUST.IsServiceProvider==false
                                                select CUST).ToList();

                    return lstAssignedCustomers;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static List<SP_RLCS_GetOverDueHighCompliance_Result> GetOverdueComplianceListByRisk(int Customerid,string Profileid, int Userid = -1, int RiskId = 0)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                var transactionsQuery = (from row in entities.SP_RLCS_GetOverDueHighCompliance(Userid, Customerid, RiskId, Profileid)
                                         where row.ScheduledOn < DateTime.Now
                                         select row).ToList();

                return transactionsQuery;
            }
        }

        public static List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> GetComplianceDetails(int customerid, List<long> Branchlist, int CustomerBranchID, int ActId, int CategoryID, int Userid, string Profileid, bool IsAvantis)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result>();

                ComplianceDetails = (from row in entities.SP_RLCS_ComplianceInstanceAssignmentViewDetails(Userid, customerid, IsAvantis, Profileid)
                                     select row).ToList();

                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }

                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();

                return ComplianceDetails;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionBARGetManagementDetailView(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            DateTime EndDate = DateTime.Today.Date;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "RLCS")).ToList();
               
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                if (statusIDs.Count != 0 )
                {
                    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                }
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID && entry.Risk == Risk).ToList();
                    }
                }

                return detailView;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionBARGetManagementDetailView(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, int? CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            DateTime EndDate = DateTime.Today.Date;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();                

                detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "RLCS")).ToList();
                
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }

                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (statusIDs.Count != 0 )
                {
                    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID)).ToList();
                }
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (filter.Equals("Function"))
                {
                    if (CategoryID != -1)
                    {
                        detailView = detailView.Where(entry => entry.ComplianceCategoryId == CategoryID && entry.Risk == Risk).ToList();
                    }
                }

                return detailView;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionPIEGetManagementDetailView(int customerid, List<long> Branchlist, int barnchId, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername,string formonth, string foryear, string forrisk, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime EndDate = DateTime.Today.Date;

                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();                

                detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "RLCS")).ToList();
               
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }
                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                if(statusIDs.Count !=0 && CategoryID.Count!=0)
                {
                    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                }
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                if (!String.IsNullOrEmpty(filter))
                {

                    if (filter.Equals("Function"))
                    {
                        if (forrisk == "-1")
                        {
                            if (Risk != -1)
                            {
                                detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                            }
                        }
                        else
                        {
                            detailView = detailView.Where(entry => entry.RiskCategory == forrisk).ToList();
                        } 
                        
                        if (formonth != "-1" && formonth != "")
                        {
                            detailView = detailView.Where(entry => entry.RLCS_PayrollMonth == formonth).ToList();
                        }
                        if (foryear != "-1" && foryear != "")
                        {
                            detailView = detailView.Where(entry => entry.RLCS_PayrollYear == foryear).ToList();
                        }

                    }
                }
                return detailView;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_Result> FunctionPIEGetManagementDetailView(int customerid, List<long> Branchlist, List<int> statusIDs, string filter, List<int?> CategoryID, int Risk, string pointername, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime EndDate = DateTime.Today.Date;

                List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();                

                detailView = (entities.SP_GetCannedReportCompliancesSummary(userId, customerid, "RLCS")).ToList();                

                if (!FromDate.ToString().Contains("1900") && !EndDateF.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDateF).ToList();
                }
                else if (!FromDate.ToString().Contains("1900"))
                {
                    detailView = detailView.Where(entry => entry.ScheduledOn >= FromDate && entry.ScheduledOn <= EndDate).ToList();
                }
                else if (!EndDateF.ToString().Contains("1900"))
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDateF).ToList();
                    }
                }
                else
                {
                    if (pointername == "Not completed")
                    {
                        detailView = detailView.Where(entry => entry.ScheduledOn <= EndDate).ToList();
                    }
                }
                if (statusIDs.Count != 0 && CategoryID.Count != 0)
                {
                    detailView = detailView.Where(entry => statusIDs.Contains(entry.ComplianceStatusID) && CategoryID.Contains(entry.ComplianceCategoryId)).ToList();
                }
                detailView = detailView.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                
                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                if (filter.Equals("Function"))
                {
                    if (Risk != -1)
                    {
                        detailView = detailView.Where(entry => entry.Risk == Risk).ToList();
                    }
                }

                return detailView;
            }
        }

        public static List<long> GetAll_Registers_Compliance_StateWise(string stateName, string establishmentType)
        {
            List<long> lstComplianceList = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstRegisters = (from row in entities.RLCS_Register_Compliance_Mapping
                                    where row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper())
                                    && row.AVACOM_ComplianceID != null
                                    && row.RegisterID != null
                                    && row.Register_Status == "A"
                                    && row.IsActive == true
                                    select row).ToList();

                if (lstRegisters.Count > 0)
                {
                    if (!string.IsNullOrEmpty(establishmentType))
                    {
                        if (establishmentType.Equals("FACT"))
                        {
                            lstComplianceList = (from row in lstRegisters
                                                 where
                                                 (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                 && row.Act_Type != null
                                                 && row.Register_Status == "A"
                                                 && row.IsActive == true
                                                 select (long)row.AVACOM_ComplianceID).ToList();

                        }
                        else if (establishmentType.Equals("SEA"))
                        {
                            lstComplianceList = (from row in lstRegisters
                                                 where
                                                 (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA") || row.Act_Type.Trim().ToUpper().Equals("CLRA"))
                                                 && row.Act_Type != null
                                                 && row.Register_Status == "A"
                                                 && row.IsActive == true
                                                 select (long)row.AVACOM_ComplianceID).ToList();

                        }
                        else if (establishmentType.Equals("SF"))
                        {
                            lstComplianceList = (from row in lstRegisters
                                                 where row.Register_Status == "A"
                                                 && row.IsActive == true
                                                 select (long)row.AVACOM_ComplianceID).ToList();

                        }
                        else if (establishmentType.Equals("CLRA"))
                        {
                            lstComplianceList = (from row in lstRegisters
                                                 where row.Register_Status == "A"
                                                 && row.Act_Type.Trim().ToUpper().Equals("CLRA")
                                                 && row.IsActive == true
                                                 select (long)row.AVACOM_ComplianceID).ToList();

                        }
                    }
                }

                return lstComplianceList;
            }
        }

        public static List<long> GetAll_Registers_ScheduleOnIDs(List<long> lstComplianceIDs, int customerBranchID, string year, string month)
        {
            List<long> lstComplianceList = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstComplianceScheduleOnIDs = (from CSO in entities.ComplianceScheduleOns
                                                  join CI in entities.ComplianceInstances
                                                  on CSO.ComplianceInstanceID equals CI.ID
                                                  where CI.CustomerBranchID == customerBranchID
                                                  && CSO.RLCS_PayrollMonth.Trim().Equals(month.Trim())
                                                  && CSO.RLCS_PayrollYear.Trim().Equals(year.Trim())
                                                  && lstComplianceIDs.Contains(CI.ComplianceId)
                                                  select CSO.ID).ToList();

                return lstComplianceScheduleOnIDs;
            }
        }

        public static List<long> GetScheduleOnIDsByRecordID(long recordID)
        {
            List<long> lstComplianceList = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstComplianceScheduleOnIDs = (from RARSM in entities.RLCS_ActivityRecord_Schedule_Mapping
                                                  where RARSM.RecordID == recordID
                                                  && RARSM.IsActive == true
                                                  select RARSM.AVACOM_ScheduleOnID).ToList();

                return lstComplianceScheduleOnIDs;
            }
        }

        public static List<SP_GetEmployeeDetails_Clientwise_Result> GetEmployeeDetails_Clientwise(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var EmpData = (from row in entities.SP_GetEmployeeDetails_Clientwise(CustomerID)
                                select row).ToList();
                return EmpData;
            }
        }

        public static List<FileData> GetFileData(List<int> objfile)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objfiledat = (from row in entities.FileDatas
                                   where objfile.Contains(row.ID)
                                   && row.IsDeleted == false
                                   select row).ToList();
                return _objfiledat;
            }
        }

        public static List<int> GetFileIDs(long scheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objfiledat = (from row in entities.FileDataMappings
                                   where row.ScheduledOnID == scheduledOnID
                                   select row.FileID).ToList();
                return _objfiledat;
            }
        }

        public static List<int> GetlstFileIDs(List<long> scheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objfiledat = (from row in entities.FileDataMappings
                                   where row.ScheduledOnID != null
                                   && scheduledOnID.Contains((long)row.ScheduledOnID)
                                   select row.FileID).ToList();
                return _objfiledat;
            }
        }

        public static List<int> GetFileIDByFileTypeId(long scheduledOnID, int FileTypeId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objfiledat = (from row in entities.FileDataMappings
                                    where row.ScheduledOnID == scheduledOnID &&
                                    row.FileType == FileTypeId
                                    select row.FileID).ToList();
                return _objfiledat;
            }
        }

        public static List<int> GetFileIDByFileType_StateCode(long scheduledOnID, int FileTypeId, string StateCode)
        {
            List<int> _objfiledat = new List<int>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                _objfiledat = (from row in entities.FileDataMappings
                               join row1 in entities.FileDatas
                               on row.FileID equals row1.ID
                               where row.ScheduledOnID == scheduledOnID &&
                                     row.FileType == FileTypeId &&
                                     row1.Code_Esi_Epf_Lwf_Pt == StateCode
                               select row.FileID).ToList();

                if(_objfiledat.Count == 0)
                {
                    _objfiledat = (from row in entities.FileDataMappings
                                   join row1 in entities.FileDatas
                                   on row.FileID equals row1.ID
                                   where row.ScheduledOnID == scheduledOnID &&
                                   row.FileType == FileTypeId
                                   select row.FileID).ToList();
                }

            }
            return _objfiledat;
        }

        public static List<int> GetFileIDByFileTypeId(long scheduledOnID, int FileTypeId, string EsiPfCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objfiledat = (from row in entities.FileDataMappings
                                   join FD in entities.FileDatas on row.FileID equals FD.ID  
                                   where row.ScheduledOnID == scheduledOnID &&
                                         row.FileType == FileTypeId
                                         && FD.Name.Contains(EsiPfCode)
                                   select row.FileID).ToList();
                return _objfiledat;
            }
        }

        public static List<int> GetFileIDByFileTypeIdAndCode(long scheduledOnID, int FileTypeId, string EsiPfCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var _objfiledat = (from row in entities.FileDataMappings
                                   join FD in entities.FileDatas on row.FileID equals FD.ID
                                   where row.ScheduledOnID == scheduledOnID &&
                                         row.FileType == FileTypeId
                                         && FD.Code_Esi_Epf_Lwf_Pt.Contains(EsiPfCode)
                                   select row.FileID).ToList();
                return _objfiledat;
            }
        }

        public static List<long> GetAll_ChallanRelatedCompliance(string challanType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstChallans = (from row in entities.RLCS_Challan_Compliance_Mapping
                                   where row.ChallanType.Trim().ToUpper().Equals(challanType.Trim().ToUpper())
                                   //&& row.State_Code.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())                                  
                                   && row.AVACOM_ComplianceID != null
                                   && row.IsActive == true
                                   select (long)row.AVACOM_ComplianceID).ToList();

                return lstChallans;
            }
        }

        public static List<long> GetAll_ChallanRelatedCompliance(string stateCode, string challanType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstChallans = (from row in entities.RLCS_Challan_Compliance_Mapping
                                   where row.ChallanType.Trim().ToUpper().Equals(challanType.Trim().ToUpper())
                                   && row.State_Code.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())
                                   && row.AVACOM_ComplianceID != null
                                   && row.IsActive == true
                                   select (long)row.AVACOM_ComplianceID).ToList();

                return lstChallans.Distinct().ToList();
            }
        }

        public static RLCS_CustomerBranch_ClientsLocation_Mapping GetCustomerBranchByStateCode(int customerID, string stateCode, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                RLCS_CustomerBranch_ClientsLocation_Mapping custBranchRecord = new RLCS_CustomerBranch_ClientsLocation_Mapping();

                var stateRecord = entities.RLCS_State_Mapping.Where(row => row.SM_Code.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())).FirstOrDefault();

                if (stateRecord != null)
                {
                    string branchNameWithState = stateRecord.SM_Name.Trim().ToUpper() + "-" + stateCode;

                    custBranchRecord = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                        join row1 in entities.CustomerBranches
                                        on row.AVACOM_BranchID equals row1.ID
                                        //where row.CM_ClientName.Trim().ToUpper().Equals(branchNameWithState)
                                        where row.AVACOM_BranchName.Trim().ToUpper().Equals(branchNameWithState)
                                        && row1.CustomerID == customerID
                                        && row.BranchType.Equals(branchType)
                                        select row).FirstOrDefault();

                }

                return custBranchRecord;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPTState(int customerID, string ptState, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                //List<CustomerBranch> lstCustomerBranches = new List<CustomerBranch>();

                lstCustomerBranches = (from row in entities.CustomerBranches
                                       join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                       on row.ID equals row1.AVACOM_BranchID
                                       where row1.AVACOM_BranchID != null
                                        && row.CustomerID == customerID
                                       && row1.CL_PT_State.Trim().ToUpper().Equals(ptState.Trim().ToUpper())
                                       //&& row1.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                       && row1.BranchType.Equals(branchType)
                                       select row1).ToList();
                return lstCustomerBranches;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPTState(int customerID, string clientID, string ptState, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                //List<CustomerBranch> lstCustomerBranches = new List<CustomerBranch>();

                lstCustomerBranches = (from row in entities.CustomerBranches
                                       join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                       on row.ID equals row1.AVACOM_BranchID
                                       where row1.AVACOM_BranchID != null
                                       && row.CustomerID == customerID
                                       && row1.CL_PT_State.Trim().ToUpper().Equals(ptState.Trim().ToUpper())
                                       && row1.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                       && row1.BranchType.Equals(branchType)
                                       select row1).ToList();
                return lstCustomerBranches;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPFOrESICCode(string challanType, string codeValue, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                //List<CustomerBranch> lstCustomerBranches = new List<CustomerBranch>();

                if (!string.IsNullOrEmpty(challanType))
                {
                    if (challanType.Trim().ToUpper().Equals("EPF"))
                    {
                        lstCustomerBranches = (from row in entities.CustomerBranches
                                               join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               on row.ID equals row1.AVACOM_BranchID
                                               where row1.AVACOM_BranchID != null
                                               && row1.CL_PF_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && row1.BranchType.Equals(branchType)
                                               select row1).ToList();
                    }
                    else if (challanType.Trim().ToUpper().Equals("ESI"))
                    {
                        lstCustomerBranches = (from row in entities.CustomerBranches
                                               join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               on row.ID equals row1.AVACOM_BranchID
                                               where row1.AVACOM_BranchID != null
                                               && row1.CL_ESIC_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && row1.BranchType.Equals(branchType)
                                               select row1).ToList();
                    }
                }

                return lstCustomerBranches;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByPFOrESICCode(string clientID, string challanType, string codeValue, string branchType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();
                //List<CustomerBranch> lstCustomerBranches = new List<CustomerBranch>();

                if (!string.IsNullOrEmpty(challanType))
                {
                    if (challanType.Trim().ToUpper().Equals("EPF"))
                    {
                        lstCustomerBranches = (from row in entities.CustomerBranches
                                               join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               on row.ID equals row1.AVACOM_BranchID
                                               where row1.AVACOM_BranchID != null
                                               && row1.CL_PF_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && row1.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                               && row1.BranchType.Equals(branchType)
                                               select row1).ToList();
                    }
                    else if (challanType.Trim().ToUpper().Equals("ESI"))
                    {
                        lstCustomerBranches = (from row in entities.CustomerBranches
                                               join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                               on row.ID equals row1.AVACOM_BranchID
                                               where row1.AVACOM_BranchID != null
                                               && row1.CL_ESIC_Code.Trim().ToUpper().Equals(codeValue.Trim().ToUpper())
                                               && row1.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                               && row1.BranchType.Equals(branchType)
                                               select row1).ToList();
                    }
                }

                return lstCustomerBranches;
            }
        }

        public static List<RLCS_CustomerBranch_ClientsLocation_Mapping> GetCustomerBranchesByRecordID(long recordID,string branchType)
        {
            List<RLCS_CustomerBranch_ClientsLocation_Mapping> lstCustomerBranches = new List<RLCS_CustomerBranch_ClientsLocation_Mapping>();

            try
            {
                if (recordID != 0)
                {
                    List<int> lstAVACOMBranchIDs = new List<int>();

                    lstAVACOMBranchIDs = Get_RLCS_ActivityRecord_Branch_Mapping(recordID);

                    if (lstAVACOMBranchIDs != null)
                    {
                        if (lstAVACOMBranchIDs.Count > 0)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                lstCustomerBranches = (from row in entities.CustomerBranches
                                                       join row1 in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                       on row.ID equals row1.AVACOM_BranchID
                                                       where row1.AVACOM_BranchID != null
                                                       && row1.CL_ID != null
                                                       && lstAVACOMBranchIDs.Contains((int)row1.AVACOM_BranchID)
                                                       && row1.BranchType.Equals(branchType)
                                                       select row1).ToList();

                            }
                        }
                    }                    
                }

                return lstCustomerBranches;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstCustomerBranches;
            }
        }
        
        public static List<int> Get_RLCS_ActivityRecord_Branch_Mapping(long recordID)
        {
            List<int> lstPrevRecords = new List<int>();

            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstPrevRecords = (from row in entities.RLCS_ActivityRecord_Branch_Mapping
                                      where row.RecordID == recordID                                    
                                      && row.IsActive == true
                                      select (int)row.AVACOM_BranchID).Distinct().ToList();

                    return lstPrevRecords;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstPrevRecords;
            }
        }
        
        public static List<MappedCompliance> GetAll_ReturnsRelatedCompliance_SectionWise(string stateCode, string taskID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var lstReturnCompliances = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where row.RM_SectionID.Trim().ToUpper().Equals(taskID.Trim().ToUpper())
                                            //&&  row.RM_StateID.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())                                   
                                            && row.AVACOM_ComplianceID != null
                                            && row.ReturnID != null
                                            && row.RM_Status == "A"
                                            select new MappedCompliance
                                            {
                                                rowID = (long)row.ReturnID,
                                                AVACOM_ComplianceID = row.AVACOM_ComplianceID,
                                                ActCode = row.RM_Act
                                            }).ToList();

                return lstReturnCompliances;
            }
        }

        public static List<long> GetAll_ReturnsRelatedCompliance_SectionWise(string stateCode)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                string[] stateCodes = new string[] { stateCode, "Central" };

                var lstReturnCompliances = (from row in entities.RLCS_Returns_Compliance_Mapping
                                            where row.AVACOM_ComplianceID != null
                                            && row.ReturnID != null
                                            && row.RM_Status == "A"
                                            && row.IsActive == true
                                            && stateCodes.Contains(row.RM_StateID)
                                            select (long)row.AVACOM_ComplianceID).ToList();

                return lstReturnCompliances;
            }
        }


        public static List<SP_GetCannedReportCompliancesSummary_Result> GetlstComplianceAssignments(int customerID, int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transactionsQuery = (from row in entities.SP_GetCannedReportCompliancesSummary(userID, customerID, "RLCS")
                                         select row).ToList();
                return transactionsQuery;
            }
        }

        public static List<SP_GetCannedReportCompliancesSummary_Result> GetCannedReportData_RLCS(int Customerid, int userID, int risk, RLCS_CannedReportFilterStatus status, int location, int type, int category, int ActID, 
            DateTime FromDate, DateTime ToDate, int ComplianceType, int EventID, int EventSchudeOnID, int SubTypeID, string StringType, List<long> branchList)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                List<SP_GetCannedReportCompliancesSummary_Result> transactionsQuery = new List<SP_GetCannedReportCompliancesSummary_Result>();

                transactionsQuery = (entities.SP_GetCannedReportCompliancesSummary(userID, Customerid, "RLCS")).ToList();

                if (transactionsQuery != null)
                {
                    if (transactionsQuery.Count > 0)
                    {
                        transactionsQuery = transactionsQuery.GroupBy(entry => entry.ScheduledOnID).Select(q => q.FirstOrDefault()).ToList();
                    }
                }

                if (EventID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchudeOnID != -1)
                    transactionsQuery = transactionsQuery.Where(entry => entry.EventScheduleOnID == EventSchudeOnID).ToList();

                //Type Filter
                if (type != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceTypeId == type)).ToList();
                }

                //category Filter
                if (category != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceCategoryId == category)).ToList();
                }

                //category Filter
                if (ActID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ActID == ActID)).ToList();
                }

                //Datr Filter
                if (Convert.ToDateTime(FromDate.ToString()).Date.Year != 1900 && Convert.ToDateTime(ToDate.ToString()).Date.Year != 1900)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= FromDate && entry.ScheduledOn <= ToDate)).ToList();
                }

                //Risk Filter
                if (risk != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                //Location
                //if (location != -1)
                //{
                //    transactionsQuery = transactionsQuery.Where(entry => (entry.CustomerBranchID == location)).ToList();
                //}

                if (branchList.Count > 0)
                {
                    transactionsQuery = transactionsQuery.Where(row => branchList.Contains((long)row.CustomerBranchID)).ToList();
                }

                //Status
                switch (status)
                {
                    case RLCS_CannedReportFilterStatus.Upcoming:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ScheduledOn >= now && entry.ScheduledOn <= nextOneMonth) && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                        break;

                    case RLCS_CannedReportFilterStatus.Overdue:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10) && entry.ScheduledOn < now).ToList();
                        break;

                    case RLCS_CannedReportFilterStatus.ClosedTimely:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case RLCS_CannedReportFilterStatus.ClosedDelayed:
                        transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case RLCS_CannedReportFilterStatus.PendingForReview:
                        transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                        //case CannedReportFilterNewStatus.Rejected:
                        //    break;
                }
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.ComplianceStatusID == 6).ToList();

                //Type SubType
                if (SubTypeID != -1)
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ComplianceSubTypeID == SubTypeID)).ToList();
                }
                // Find data through String contained in Description
                if (StringType != "")
                {
                    transactionsQuery = transactionsQuery.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                return transactionsQuery.OrderBy(entry => entry.ScheduledOn).ToList();
            }
        }
        
        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Result> DashboardData_ComplianceDisplayCount(List<SP_RLCS_ComplianceInstanceTransactionCount_Result> MastertransactionsQuery,
           string filter)
        {
            List<SP_RLCS_ComplianceInstanceTransactionCount_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Result>();

            if (!string.IsNullOrEmpty(filter))
            {
                DateTime now = DateTime.Now.Date;
                DateTime nextOneMonth = DateTime.Now.AddMonths(1);
                DateTime nextTenDays = DateTime.Now.AddDays(10);

                switch (filter)
                {
                    case "Upcoming":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                             && row.RoleID == 3
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "Overdue":
                        //NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                              && row.RoleID == 3
                                              && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();

                        //transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                        //                    where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12)                                             
                        //                     && row.RoleID == 3
                        //                     && row.PerformerScheduledOn < now
                        //                     select row).ToList().GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "DueToday":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.RoleID == 3
                                             && row.PerformerScheduledOn == now
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "HighRisk":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.RoleID == 3
                                             && row.PerformerScheduledOn < nextTenDays
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "Pending":                       
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
            }

            return transactionsQuery.ToList();
        }
        
        public static List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> DashboardData_ComplianceDisplayCount(List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> MastertransactionsQuery, string filter)
        {
            List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result> transactionsQuery = new List<SP_RLCS_ComplianceInstanceTransactionCount_Dashboard_Result>();

            if (!string.IsNullOrEmpty(filter))
            {
                DateTime now = DateTime.Now.Date;
                DateTime nextOneMonth = DateTime.Now.AddMonths(1);
                DateTime nextTenDays = DateTime.Now.AddDays(10);

                switch (filter)
                {
                    case "Upcoming":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.PerformerScheduledOn > now && row.PerformerScheduledOn <= nextOneMonth)
                                             && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13)
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "Overdue":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 8 || row.ComplianceStatusID == 10)
                                              && row.PerformerScheduledOn < now
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                    case "PendingForReview":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 12)
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();

                        break;
                    case "DueToday":
                        transactionsQuery = (from row in MastertransactionsQuery
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.PerformerScheduledOn == now
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;

                    case "HighRisk":
                        transactionsQuery = (from row in MastertransactionsQuery //entities.ComplianceInstanceTransactionViews
                                             where (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 12 || row.ComplianceStatusID == 13 ||
                                             row.ComplianceStatusID == 2 || row.ComplianceStatusID == 3)
                                             && row.PerformerScheduledOn < nextTenDays
                                             select row).ToList().GroupBy(entity => new { entity.ScheduledOnID, entity.ComplianceSchedules }).Select(entity => entity.FirstOrDefault()).ToList();
                        break;
                }
            }

            return transactionsQuery.ToList();
        }
        

        public static List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> GetComplianceDetailsDashboard(int customerid, List<long> Branchlist, int RoleID, int CustomerBranchID, int ActId, int CategoryID, int Userid, string Profileid, bool IsAvantis)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result> ComplianceDetails = new List<SP_RLCS_ComplianceInstanceAssignmentViewDetails_Result>();

                ComplianceDetails = (from row in entities.SP_RLCS_ComplianceInstanceAssignmentViewDetails(Userid, customerid, IsAvantis, Profileid)
                                     select row).ToList();

                if (Branchlist.Count > 0)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }

                if (ActId != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ActID == ActId).Distinct().ToList();
                }
                if (CategoryID != -1)
                {
                    ComplianceDetails = ComplianceDetails.Where(row => row.ComplianceCategoryId == CategoryID).Distinct().ToList();
                }

                ComplianceDetails = ComplianceDetails.GroupBy(a => a.ComplianceID).Select(a => a.First()).ToList();
                return ComplianceDetails;
            }
        }

        public static List<sp_ComplianceAssignedCategory_Result> GetNewAll(int UserID, int CustomerBranchId, List<long> Branchlist, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedCategory_Result> complianceCategorys = new List<sp_ComplianceAssignedCategory_Result>();

                if (Branchlist.Count > 0)
                {
                    complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "RLCS")
                                           where Branchlist.Contains((long)row.CustomerBranchID)
                                           select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                }
                else
                {
                    complianceCategorys = (from row in entities.sp_ComplianceAssignedCategory(UserID, "RLCS")
                                           select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                }


                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static DataTable GetGradingReportOfManagement(int customerid, int userId, int year, int month, int period, int BranchID, List<SP_RLCS_StatutoryGradingReport_Result> MasterTransactionQuery,string Profileid, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime startDate = new DateTime(year, month, 1).AddMonths(-period);
                DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);
                entities.Database.CommandTimeout = 180;
                List<SP_RLCS_StatutoryGradingReport_Result> transactionsQuery = new List<SP_RLCS_StatutoryGradingReport_Result>();
                List<NameValueHierarchy> bracnhes = new List<NameValueHierarchy>();
                BranchlistGrading.Clear();
                if (BranchID != -1)
                {
                   
                    GetAllHierarchyGreding(customerid, BranchID);
                    if (BranchlistGrading.Count > 0)
                    {
                        transactionsQuery = (from row in MasterTransactionQuery                                              
                                             where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                             && BranchlistGrading.Contains(row.CustomerBranchID)
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        bracnhes = RLCSManagement.GetLocationNew(customerid, BranchlistGrading, userId, Profileid);
                    }
                    else
                    {
                        transactionsQuery = (from row in MasterTransactionQuery                                               
                                             where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                             && row.CustomerBranchID == BranchID
                                             select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                        BranchlistGrading.Add(BranchID);
                        bracnhes = RLCSManagement.GetLocationNew(customerid, BranchlistGrading, userId, Profileid);
                    }
                }
                else
                {

                    transactionsQuery = (from row in MasterTransactionQuery                                            
                                         where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                    bracnhes = RLCSManagement.GetLocationNew(customerid, BranchlistGrading, userId, Profileid);

                }
                DataTable table = new DataTable();
                table.Columns.Add("Location", typeof(string));
                table.Columns.Add("LocationID", typeof(string));                
                // for (int i = period; i >= 1; i--)
                for (int i = 1; i <= period; i++)
                {
                    table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                    // table.Columns.Add(EndDate.AddMonths(i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                }

                foreach (var item in bracnhes)
                {
                    item.Level = 0;
                    var aa = transactionsQuery.Where(entry => entry.CustomerBranchID == item.ID).ToList();
                    if (aa.Count>0)
                    {
                        DataRow tableRow = BindingGradingTableRow(transactionsQuery, item, table, period, EndDate, customerid);
                        table.Rows.Add(tableRow);
                    }                    
                }
                return table;
            }
        }

        private static DataRow BindingGradingTableRow(List<SP_RLCS_StatutoryGradingReport_Result> transactionsQuery, NameValueHierarchy item, DataTable table, int period, DateTime EndDate, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                //comment by rahul on 2 MAY 2018
                //var branchIDs = (from row in entities.CustomerBranches
                //                 where row.ParentID == item.ID
                //                 select row.ID).ToList();
                //branchIDs.Add(item.ID);

                var branchIDs = (from row in entities.CustomerBranches
                                 where row.ID == item.ID
                                 select row.ID).ToList();

                int color;
                DataRow tableRow = table.NewRow();

                tableRow["Location"] = item.Name + "#" + item.Level;
                tableRow["LocationID"] = item.ID;
                //tableRow["Approver"] = ""; // GetApproverName(customerid, item.ID) != null ? GetApproverName(customerid,item.ID).User : "";

                // tableRow["Approver"] = GetApproverName(customerid, item.ID) != null ? GetApproverName(customerid,item.ID).User : "";
                for (int i = period; i >= 1; i--)
                {
                    DateTime previousDate = EndDate.AddMonths(-i);
                    DateTime date1 = new DateTime(previousDate.Year, previousDate.Month, 1);
                    DateTime date2 = new DateTime(previousDate.Year, previousDate.Month, DateTime.DaysInMonth(previousDate.Year, previousDate.Month));

                    long highRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                    long MediumRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                    long LowRisktotalCount = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                    long highRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 0 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                    long MediumRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 1 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();
                    long LowRiskCompletedCount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 9) && branchIDs.Contains((int)entry.CustomerBranchID) && entry.Risk == 2 && entry.ScheduledOn >= date1 && entry.ScheduledOn <= date2).Count();

                    double seventyfivePercentLow = (0.75 * LowRisktotalCount);
                    double seventyfivePercentHigh = (0.75 * highRisktotalCount);

                    if (highRisktotalCount != 0 || MediumRisktotalCount != 0 || LowRiskCompletedCount != 0)
                    {
                        if (highRisktotalCount == highRiskCompletedCount && MediumRisktotalCount == MediumRiskCompletedCount && LowRiskCompletedCount >= seventyfivePercentLow)
                        {
                            color = 1;
                        }
                        else if (highRiskCompletedCount >= seventyfivePercentHigh && MediumRiskCompletedCount >= (MediumRisktotalCount / 2) && LowRiskCompletedCount >= (LowRisktotalCount / 2))
                        {
                            color = 2;
                        }
                        else
                        {
                            color = 3;
                        }
                    }
                    else
                    {
                        color = 4;
                    }
                    tableRow[EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")] = color;
                }
                return tableRow;
            }
        }
        
        public static List<NameValueHierarchy> GetLocationNew(int customerId, List<long> Branchlist, int userID, string Profileid)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 180;
                List<SP_RLCS_GetAssignedLocationBranches_Result> ManagerAssignedBranchlist = new List<SP_RLCS_GetAssignedLocationBranches_Result>();

                ManagerAssignedBranchlist = (entities.SP_RLCS_GetAssignedLocationBranches(customerId, Convert.ToInt32(userID), Profileid)).ToList();

                if (Branchlist.Count>0)
                {
                    var query = (from row in ManagerAssignedBranchlist
                                 where Branchlist.Contains(row.ID)
                                 select row).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    //foreach (var item in hierarchy)
                    //{
                    //    LoadSubEntitiesNew(item, false, entities);
                    //}
                }
                else
                {
                    var query = (from row in ManagerAssignedBranchlist
                                 select row).ToList();

                    hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();
                    //foreach (var item in hierarchy)
                    //{
                    //    LoadSubEntitiesNew(item, false, entities);
                    //}
                }
            }

            return hierarchy;

        }
        
        //Added By Sushant

        public static List<ListItem> GetSelectedItems(CheckBoxList lst)
        {
            return lst.Items.OfType<ListItem>().Where(i => i.Selected).ToList();
        }

        public static List<SP_LitigationNoticeDocumentfor_Tag_Result> GetAllDocumentListofNotice(int customerID, int loggedInUserID, string loggedInUserRole, int RoleID, List<int> branchList, int partyID, int deptID, int caseStatus, string caseType, List<string> lstSelectedFileTags)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {

                var query = (from row in entities.SP_LitigationNoticeDocumentfor_Tag(customerID)
                             select row).ToList();

                if (loggedInUserRole != "MGMT" && loggedInUserRole != "CADMN"
                    && loggedInUserRole != "HMGR" && loggedInUserRole != "HMGMT" && loggedInUserRole != "HAPPR" && loggedInUserRole != "LSPOC")
                    query = query.Where(entry => (entry.AssignedUser == loggedInUserID && entry.AssignedUserRoleID == RoleID) || (entry.OwnerID == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.AssignedUserRoleID == 3).ToList();
                }

                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (caseStatus == 3) //3--Closed otherwise Open
                    query = query.Where(entry => entry.Status == caseStatus).ToList();
                else
                    query = query.Where(entry => entry.Status < 3).ToList(); //query = query.Where(entry => entry.Status == 0).ToList();

                if (caseType != "" && caseType != "All")//B--All --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.TypeName == caseType).ToList();

                if (query.Count > 0)
                {
                    query = query.OrderBy(entry => entry.TypeName)
                        .ThenBy(entry => entry.CustomerBranchID).ToList();
                }
                if (lstSelectedFileTags.Count > 0)
                {
                    query = query.Where(Entry => lstSelectedFileTags.Contains(Entry.Filetag)).ToList();
                }
                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeCaseInstanceID,
                                 g.RefNo,
                                 g.TypeCaseNotice,
                                 g.TypeName,
                                 g.Title,
                                 g.Description,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.Status,
                                 g.DocumentCount,
                                 //g.Filetag
                             } into GCS
                             select new SP_LitigationNoticeDocumentfor_Tag_Result()
                             {
                                 NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 TypeCaseNotice = GCS.Key.TypeCaseNotice,
                                 TypeName = GCS.Key.TypeName,
                                 Title = GCS.Key.Title,
                                 Description = GCS.Key.Description,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 Status = GCS.Key.Status,
                                 DocumentCount = GCS.Key.DocumentCount,
                                 //Filetag = GCS.Key.Filetag
                             }).ToList();

                }
                return query;
            }
        }

        public static List<ListItem> ReArrange_FileTags(CheckBoxList lstBoxFileTags)
        {
            List<ListItem> lstSelectedTags = new List<ListItem>();
            List<ListItem> lstNonSelectedTags = new List<ListItem>();
            foreach (ListItem eachListItem in lstBoxFileTags.Items)
            {
                if (eachListItem.Selected)
                    lstSelectedTags.Add(eachListItem);
                else
                    lstNonSelectedTags.Add(eachListItem);
            }

            return lstSelectedTags.Union(lstNonSelectedTags).ToList();
        }

        public static List<sp_LitigationNoticeFileTag_Result> GetDistinctFileTagsNoticeMyDocument(int customerID, long litigationInstanceID, long UserID, string Role)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Query = (from row in entities.sp_LitigationNoticeFileTag(customerID)
                             select row).Distinct().ToList();

                if (Role != "MGMT" && Role != "CADMN" && Role != "HMGR" && Role != "HMGMT" && Role != "HAPPR" && Role != "LSPOC")
                {
                    Query = Query.Where(entry => (entry.UserID == UserID && entry.RoleID == 3) || (entry.OwnerID == UserID) || (entry.CreatedBy == UserID)).ToList();
                }
                else // In case of MGMT or CADMN 
                {
                    Query = Query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (Query.Count > 0)
                {
                    Query = (from g in Query
                             group g by new
                             {
                                 g.FileTag
                             } into GCS
                             select new sp_LitigationNoticeFileTag_Result()
                             {
                                 FileTag = GCS.Key.FileTag
                             }).ToList();
                }
                return Query;
            }
        }

        //25 OCT 2018
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchyGreding(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    BranchlistGrading.Add(item.ID);
                    LoadSubEntitiesGreding(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntitiesGreding(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                BranchlistGrading.Add(item.ID);
                LoadSubEntitiesGreding(customerid, item, false, entities);
            }
        }

        public static List<View_NoticeAssignedInstance> GetAssignedNoticeList(long customerID, int loggedInUserID, string loggedInUserRole, int roleID, List<int> branchList, int partyID, int deptID, int noticeStatus, string noticeType) /*int branchID*/
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.View_NoticeAssignedInstance
                             where row.CustomerID == customerID
                             select row).ToList();

                if (noticeStatus != -1)
                {
                    if (noticeStatus == 3) //3--Closed otherwise Open
                        query = query.Where(entry => entry.TxnStatusID == noticeStatus).ToList();
                    else
                        query = query.Where(entry => entry.TxnStatusID < 3).ToList();
                }


                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    query = query.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    query = query.Where(entry => entry.DepartmentID == deptID).ToList();

                if (noticeType != "" && noticeType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.NoticeType == noticeType).ToList();

                if (loggedInUserRole != "HMGMT" && loggedInUserRole != "LSPOC" && loggedInUserRole != "HMGR" && loggedInUserRole != "HAPPR")
                    query = query.Where(entry => (entry.UserID == loggedInUserID && entry.RoleID == roleID) || (entry.OwnerID == loggedInUserID) || (entry.NoticeCreatedBy == loggedInUserID)).ToList();
                else // In case of MGMT or CADMN 
                {
                    query = query.Where(entry => entry.RoleID == 3).ToList();
                }

                if (query.Count > 0)
                {
                    query = (from g in query
                             group g by new
                             {
                                 g.NoticeInstanceID,
                                 g.RefNo,
                                 g.NoticeTitle,
                                 g.NoticeDetailDesc,
                                 g.NoticeCategoryID,
                                 g.NoticeType,
                                 g.NoticeTypeName,
                                 g.CustomerID,
                                 g.CustomerBranchID,
                                 g.BranchName,
                                 g.DepartmentID,
                                 g.DeptName,
                                 g.PartyID,
                                 g.PartyName,
                                 g.CreatedOn,
                                 g.OwnerID,
                                 g.UpdatedOn
                             } into GCS
                             select new View_NoticeAssignedInstance()
                             {
                                 NoticeInstanceID = GCS.Key.NoticeInstanceID,
                                 RefNo = GCS.Key.RefNo,
                                 NoticeTitle = GCS.Key.NoticeTitle,
                                 NoticeCategoryID = GCS.Key.NoticeCategoryID,
                                 NoticeDetailDesc = GCS.Key.NoticeDetailDesc,
                                 NoticeType = GCS.Key.NoticeType,
                                 NoticeTypeName = GCS.Key.NoticeTypeName,
                                 CustomerID = GCS.Key.CustomerID,
                                 CustomerBranchID = GCS.Key.CustomerBranchID,
                                 BranchName = GCS.Key.BranchName,
                                 DepartmentID = GCS.Key.DepartmentID,
                                 DeptName = GCS.Key.DeptName,
                                 PartyID = GCS.Key.PartyID,
                                 PartyName = GCS.Key.PartyName,
                                 CreatedOn = GCS.Key.CreatedOn,
                                 OwnerID = GCS.Key.OwnerID,
                                 UpdatedOn = GCS.Key.UpdatedOn
                             }).ToList();
                }

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.NoticeType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }
        
        public static List<SP_RLCS_NoticeAssignedInstance_Result> GetInspectionNoticeList(int customerID, int loggedInUserID, string loggedInUserProfileID, List<int> branchList, string noticeStatus, string noticeType) /*int branchID*/
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.SP_RLCS_NoticeAssignedInstance(customerID, loggedInUserID, loggedInUserProfileID)                            
                             select row).ToList();

                if (!string.IsNullOrEmpty(noticeStatus))
                {
                    if (noticeStatus == "P") //3--Closed otherwise Open
                        query = query.Where(entry => entry.IN_Status == noticeStatus).ToList();
                    else if (noticeStatus == "C")
                        query = query.Where(entry => entry.IN_Status == noticeStatus).ToList();
                }

                if (branchList.Count > 0)
                    query = query.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                           
                if (noticeType != "" && noticeType != "B") //B--Both --Inward(I) and Outward(O)
                    query = query.Where(entry => entry.NoticeType == noticeType).ToList(); 

                if (query.Count > 0)
                {
                    query = query.OrderByDescending(entry => entry.UpdatedOn)
                        .ThenByDescending(entry => entry.CreatedOn).ToList();
                    //query = query.OrderBy(entry => entry.NoticeType)
                    //    .ThenBy(entry => entry.CustomerBranchID).ToList();
                }

                return query.ToList();
            }
        }

        public static List<SP_RLCS_NoticeDocuments_Result> GetNoticeDocuments(int noticeInstanceID, int customerID,  string docType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var queryResult = (from row in entities.SP_RLCS_NoticeDocuments(noticeInstanceID, customerID)
                                   select row).ToList();

                if (queryResult.Count > 0)
                {
                    if (docType != "")
                        queryResult = queryResult.Where(row => row.DocType.Trim() == docType).ToList();
                    else
                        queryResult = queryResult.Where(row => (row.DocType.Trim() == "N") || (row.DocType.Trim() == "NR") || (row.DocType.Trim() == "NT") || (row.DocType.Trim() == "CD")).ToList();
                }

                if (queryResult.Count > 0)
                    queryResult = queryResult.OrderBy(entry => entry.FileName).ThenByDescending(entry => entry.CreatedOn).ToList();

                return queryResult;
            }
        }

        public static RLCS_User_Mapping GetRLCSUserRecord(long userID, string profileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var userRecord = (from row in entities.RLCS_User_Mapping
                                  where row.AVACOM_UserID == userID
                                  && row.ProfileID == profileID
                                  select row).FirstOrDefault();

                return userRecord;
            }
        }

        public static List<int> GetAllAssigenedLocationsBasedOnProfileId(string ProfileId)
        {
            List<int> LstBranches = new List<int>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Lst = (from CL in entities.RLCS_ClientLocationUserMapping where CL.UserId == ProfileId && CL.StatusCode == "A" select CL.AVACOM_BranchID).ToList();
                LstBranches = Lst.Where(x => x != null).Select(x => x.Value).ToList();
            }
            return LstBranches;
        }
    }
}
