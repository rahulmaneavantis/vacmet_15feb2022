﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Threading;
using System.Net.Mail;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ObservationSubcategory
    {

        public static List<Mst_ObservationCategory> GetObservationCategoryAll(long Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditCategorys = (from MOC in entities.Mst_ObservationCategory
                                      where MOC.CustomerID == Customerid && MOC.IsActive == false
                                      select MOC).ToList().Distinct();

                AuditCategorys = AuditCategorys.OrderBy(entry => entry.Name);

                return AuditCategorys.ToList();
            }
        }
        public static object FillObservationCategory(int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var query = (from row in entities.Mst_ObservationCategory
                             where row.CustomerID== Customerid && row.IsActive == false
                             select row).Distinct();

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static object FillObservationSubCategory(int ObservationID,int Customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_ObservationSubCategory
                             join row1 in entities.Mst_ObservationCategory
                             on row.ObscatID equals row1.ID
                             where row.IsActive == false
                             && row.ObscatID == ObservationID 
                             && row1.CustomerID == Customerid
                             && row1.IsActive == false
                             select row).Distinct();

                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static bool ObservationSubCategoryExists(int CustID,int ObservationID, string name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_ObservationSubCategory
                             where row.ObscatID== ObservationID && row.Name.ToUpper().Equals(name.ToUpper())
                             && row.ClientID == CustID
                             && row.IsActive == false
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static object fetchObservationcatgory(int ObservationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
               var ObjNameCategory = (from row in entities.Mst_ObservationCategory
                                     where row.ID == ObservationId
                                                            select row).FirstOrDefault();
                string str = ObjNameCategory.Name;

                return str;
                                
            }
        }

        public static List<ObservationsDetails> GetAllSubObservationList(int ObservationId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ObjSubList = (from row in entities.Mst_ObservationSubCategory
                                  join row1 in entities.Mst_ObservationCategory
                                  on row.ObscatID equals row1.ID
                                  into t
                                  from rt in t.DefaultIfEmpty()
                                  where row.IsActive == false && row.ObscatID == ObservationId
                                  select new ObservationsDetails()
                                  {
                                      id  = row.ID,
                                      obsName = row.Name,
                                      ObscatID = rt.ID,
                                      Name = rt.Name
                                  });                               
                return ObjSubList.ToList();
            }        
        }       
        public static bool IsExistObservationListName(string name)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_ObservationSubCategory
                             where row.Name.Equals(name)
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateSubobsList(Mst_ObservationSubCategory objSublist)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_ObservationSubCategory.Add(objSublist);
                entities.SaveChanges();
            }
        }

        public static void UpdateObjSubList(Mst_ObservationSubCategory objSublist)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                Mst_ObservationSubCategory objSublistUpdate = (from row in entities.Mst_ObservationSubCategory
                                                               where row.ID == objSublist.ID
                                                  select row).FirstOrDefault();

                objSublistUpdate.Name = objSublist.Name;
                objSublistUpdate.ObscatID = objSublist.ObscatID;
                entities.SaveChanges();
            }
        }

        public static int GetObservationCategoryByName(string ObservationCategoryName, int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var observationCategoryId = (from row in entities.Mst_ObservationCategory
                                 where row.Name.ToUpper().Trim() == ObservationCategoryName.ToUpper().Trim()
                                  && row.CustomerID == CustomerID
                                  && row.IsActive == false
                                 select row.ID).FirstOrDefault();

                if (observationCategoryId != 0)
                {
                    return Convert.ToInt32(observationCategoryId);
                }
                else
                {
                    return observationCategoryId = -1;
                }
                
            }
        }

        public static int GetObservationSubCategoryByName(string ObservationSubCategoryName,int ObservationCategoyId, int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var observationSubCategoryId = (from row in entities.Mst_ObservationSubCategory
                                             where row.ObscatID== ObservationCategoyId && row.Name.ToUpper().Trim() == ObservationSubCategoryName.ToUpper().Trim()
                                              && row.ClientID == CustomerID
                                              && row.IsActive == false
                                             select row.ID).FirstOrDefault();

                if (observationSubCategoryId != 0)
                {
                    return Convert.ToInt32(observationSubCategoryId);
                }
                else
                {
                    return observationSubCategoryId = -1;
                }

            }
        }
        public static Mst_ObservationSubCategory ObservationListGetById(int iD)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ObjList = (from row in entities.Mst_ObservationSubCategory
                                  where row.ID == iD
                                  select row).FirstOrDefault();

                if (ObjList != null)
                {
                    return ObjList;
                }
                else
                {
                    return null;
                }
            }
        }

        public static void DeleteObservationList(int ObjListId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                Mst_ObservationSubCategory ObjListDelete = (from row in entities.Mst_ObservationSubCategory
                                       where row.ID == ObjListId
                                       select row).FirstOrDefault();

                entities.Mst_ObservationSubCategory.Remove(ObjListDelete);
                entities.SaveChanges();
            }
        }


        #region added by sagar more on 21-05-2020
        public static bool CreateObservationCategory(Mst_ObservationCategory ObservationCategory)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_ObservationCategory.Add(ObservationCategory);
                entities.SaveChanges();
                return true;
            }
        }

        public static bool CreateObservationSubCategory(Mst_ObservationSubCategory ObservationSubCategory)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.Mst_ObservationSubCategory.Add(ObservationSubCategory);
                entities.SaveChanges();
                return true;
            }
        }

        public static bool IsExistObservationCategoryByName(string observationCategory, int customerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.Mst_ObservationCategory
                             where cs.Name == observationCategory && cs.IsActive == false
                             && cs.CustomerID == customerId
                             select cs).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static int GetIdObservationCategoryByName(string observationCategory, int customerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.Mst_ObservationCategory
                             where cs.Name == observationCategory && cs.IsActive == false
                             && cs.CustomerID == customerId
                             select cs).FirstOrDefault();

                if (query != null)
                {
                    return query.ID;
                }
                else
                {
                    return -1;
                }
            }
        }

        public static bool IsExistObservationSubCategoryByName(string ObservationSubCategory, int observationCategoryId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from cs in entities.Mst_ObservationSubCategory
                             where cs.Name == ObservationSubCategory && cs.IsActive == false
                             && cs.ObscatID == observationCategoryId
                             select cs).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

    }
}
