﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class Provisionclass
    {
        public static void CreateProvision(Provision objCase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.Provisions.Add(objCase);
                entities.SaveChanges();
            }
        }
  
        public static void UpdateProvision(Provision objAuth)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                Provision objAuths = (from row in entities.Provisions
                                                where row.Id == objAuth.Id

                                                select row).FirstOrDefault();

                objAuths.Title = objAuth.Title;
                objAuths.Details = objAuth.Details;

                entities.SaveChanges();
            }
        }
        public static Provision GetProvision(int ID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Objcase = (from row in entities.Provisions
                               where row.Id == ID

                               select row).SingleOrDefault();
                return Objcase;
            }
        }

        public static List<Provision> GetAllProvision(int customerId)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Objcase = (from row in entities.Provisions
                               where row.CustomerId == customerId
                               select row).ToList();
                return Objcase;
            }
        }
        public static void DeleteProvision(int ID, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                Provision objCase = (from row in entities.Provisions
                                               where row.Id == ID
                                               && row.CustomerId == CustomerID

                                               select row).FirstOrDefault();


                entities.Provisions.Remove(objCase);
                entities.SaveChanges();
            }
        }
    }
}
