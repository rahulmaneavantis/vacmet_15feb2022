﻿using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using EntityFramework.BulkInsert.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class LitigationCourtAndCaseType
    {
        public static List<tbl_NoticeStage> GetAllNoticeStagesData(long CutomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objcases = (from row in entities.tbl_NoticeStage
                                where row.NoticeStage != null && row.IsDeleted == false
                                && row.CustomerID == CutomerID
                                select row).OrderBy(entry => entry.NoticeStage);
                return objcases.ToList();
            }
        }
        public static List<tbl_CaseType> GetAllLegalCaseTypeDataForRPA(long CutomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objcases = (from row in entities.tbl_CaseType
                                where row.RPACaseTypeID != null && row.IsDeleted == false
                                && row.CustomerID == CutomerID
                                select row).OrderBy(entry => entry.CaseType);
                return objcases.ToList();
            }
        }
        public static tbl_CaseType GetRPALegalCaseTypeDetailByID(int ID, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Objcase = (from row in entities.tbl_CaseType

                               where row.RPACaseTypeID == ID

                               && row.CustomerID == CustomerID

                               select row).SingleOrDefault();

                return Objcase;
            }
        }

        public static List<tbl_CourtType> GetAllCourtTypeData(long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objCourt = (from row in entities.tbl_CourtType
                                where row.CourtType != null 
                                && row.IsDeleted == false 
                                //&& row.CustomerID== CustomerID
                                select row);
                return objCourt.ToList();
            }
        }

        public static bool CheckCourtTypeExist(tbl_CourtType objCourts)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objCourt = (from row in entities.tbl_CourtType
                                where row.CourtType.Equals(objCourts.CourtType)
                                //&& row.CustomerID==objCourts.CustomerID
                                  select row);
                return objCourt.Select(entry => true).SingleOrDefault();
            }
        }       

        public static void CreateCourtTypeDetails(tbl_CourtType objCourt)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_CourtType.Add(objCourt);
                entities.SaveChanges();
            }
        }

        public static void UpdateCourtTypeDetails(tbl_CourtType objCourt)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_CourtType updatestates = (from row in entities.tbl_CourtType
                                          where row.CourtID == objCourt.CourtID
                                          //&& row.CustomerID==objCourt.CustomerID
                                        select row).FirstOrDefault();

                updatestates.CourtType = objCourt.CourtType;
                updatestates.Updatedby = objCourt.Updatedby;
                updatestates.UpdatedOn = objCourt.UpdatedOn;
                entities.SaveChanges();
            }
        }

        public static tbl_CourtType GetCourtTypeDetailByID(int ID,long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var ObjCourt = (from row in entities.tbl_CourtType
                                where row.CourtID == ID 
                                //&& row.CustomerID== CustomerID
                                select row).SingleOrDefault();
                return ObjCourt;
            }
        }

        public static void DeleteCourtTypeDetail(int ID, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_CourtType objCourt = (from row in entities.tbl_CourtType
                                          where row.CourtID == ID
                                          //&& row.CustomerID== CustomerID
                                          select row).FirstOrDefault();

                objCourt.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        public static int GetCaseTypeIDByName(string typeName, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var caseTypeID = (from row in entities.tbl_CaseType
                                  where row.CaseType.Trim().ToUpper() == typeName.Trim().ToUpper()
                                  && row.CustomerID == CustomerID
                                  && row.IsDeleted == false
                                  select row.ID).FirstOrDefault();
                return caseTypeID;
            }
        }

        public static List<tbl_CaseType> GetAllLegalCaseTypeData(long CutomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objcases = (from row in entities.tbl_CaseType
                                where row.CaseType != null && row.IsDeleted == false
                                && row.CustomerID== CutomerID
                                select row).OrderBy(entry => entry.CaseType);
                return objcases.ToList();
            }
        }        

        public static bool CheckLegalCaseTypeExist(tbl_CaseType objCasetype)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objCase = (from row in entities.tbl_CaseType
                               where row.CaseType.Equals(objCasetype.CaseType)
                               && row.CustomerID==objCasetype.CustomerID
                                select row);
                return objCase.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateLegalCaseTypeDetails(tbl_CaseType objCase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_CaseType.Add(objCase);
                entities.SaveChanges();
            }
        }

        public static void UpdateLegalCaseTypeDetails(tbl_CaseType objCase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_CaseType objcases = (from row in entities.tbl_CaseType
                                         where row.ID == objCase.ID 
                                         && row.CustomerID==objCase.CustomerID
                                      select row).FirstOrDefault();

                objcases.CaseType = objCase.CaseType;
                objcases.UpdatedBy = objCase.UpdatedBy;
                objcases.UpdatedOn = objCase.UpdatedOn;
                entities.SaveChanges();
            }
        }

        public static tbl_CaseType GetLegalCaseTypeDetailByID(int ID,long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Objcase = (from row in entities.tbl_CaseType
                               where row.ID == ID 
                               && row.CustomerID==CustomerID
                                select row).SingleOrDefault();
                return Objcase;
            }
        }

        public static void DeleteLegalCaseTypeDetail(int ID, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_CaseType objCase = (from row in entities.tbl_CaseType
                                        where row.ID == ID && row.CustomerID == CustomerID
                                        && row.IsDeleted == false
                                        select row).FirstOrDefault();

                objCase.IsDeleted = true;
                entities.SaveChanges();
            }
        }

        //CourtMaster

        public static List<View_CourtMaster> BindAllCourtMasterData(long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objCourt = (from row in entities.View_CourtMaster
                                where row.CourtName != null
                                && row.CustomerID == CustomerID
                                select row).OrderBy(entry => entry.CourtName);
                return objCourt.ToList();
            }
        }

        public static tbl_CourtMaster GetCourtMasterDatabyID(int ID,long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var Objcase = (from row in entities.tbl_CourtMaster
                               where row.ID == ID && row.CustomerID== CustomerID
                               select row).SingleOrDefault();
                return Objcase;
            }
        }
        
        public static bool CheckCourtExist(tbl_CourtMaster objCase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objCourts = (from row in entities.tbl_CourtMaster
                                 where row.CourtName.Equals(objCase.CourtName)
                               && row.Country==objCase.Country && row.State==objCase.State
                               && row.City==objCase.City && row.CustomerID==objCase.CustomerID
                                 select row);
                return objCourts.Select(entry => true).SingleOrDefault();
            }
        }

        public static void CreateCourtMasterDetails(tbl_CourtMaster objCase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.tbl_CourtMaster.Add(objCase);
                entities.SaveChanges(); 
            }
        }

        public static void UpdateCourtMasterDetials(tbl_CourtMaster objCase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_CourtMaster objCourt = (from row in entities.tbl_CourtMaster
                                            where row.ID == objCase.ID && row.CustomerID==objCase.CustomerID
                                            select row).FirstOrDefault();

                objCourt.CourtName = objCase.CourtName;
                objCourt.CourtType = objCase.CourtType;
                objCourt.Country = objCase.Country;
                objCourt.State = objCase.State;
                objCourt.City = objCase.City;
                objCourt.Address = objCase.Address;
                objCourt.UpdatedBy = objCase.UpdatedBy;
                objCourt.UpdatedOn = objCase.UpdatedOn;
                entities.SaveChanges();
            }
        }

        public static void DeleteCourtMasterData(int ID, long CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                tbl_CourtMaster objCase = (from row in entities.tbl_CourtMaster
                                           where row.ID == ID
                                           && row.CustomerID == CustomerID
                                           && row.IsDeleted == false
                                           select row).FirstOrDefault();

                objCase.IsDeleted = true;
                entities.SaveChanges();
            }
        }
    }
}
