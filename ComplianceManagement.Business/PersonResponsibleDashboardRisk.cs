﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
     public class PersonResponsibleDashboardRisk
    {
      
        # region Test Of Design Wise Customer 
        public static DataTable GetPRStatusTODWise(int customerid, string financialyear, string periodR, int userId, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long Keycount;
                    long NonKeycount;
                    long totalcount;                    
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Keycount + NonKeycount;

                            if (totalcount != 0)
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Keycount, NonKeycount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long Keycount;
                    long NonKeycount;
                    long totalcount;

                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Keycount + NonKeycount;

                            if (totalcount != 0)
                                table.Rows.Add(ss.ProcessId, cc.Name, ss.Id, ss.Name, Keycount, NonKeycount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        public static DataTable GetPRStatusTODKEYWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);

            List<int> TODPassIds = new List<int>();
            TODPassIds.Add(1);
            TODPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));


                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)                                
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long Passcount;
                    long Failcount;
                    long totalcount;
                 
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {                           
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)                                
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        public static DataTable GetPRStatusTODNONKEYWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);

            List<int> TODPassIds = new List<int>();
            TODPassIds.Add(1);
            TODPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                        
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                return table;
            }
        }

        #region Test Of Design Wise Customer Branch
        public static DataTable GetBranchPRStatusTODWise(int customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(2);
            TODIds.Add(3);
            TODIds.Add(4);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                               && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }

                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Keycount + NonKeycount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Keycount, NonKeycount, totalcount);
                                table.Rows.Add(ss.ProcessId, cc.Name, ss.Id, ss.Name, Keycount, NonKeycount, totalcount);
                        }
                    }

                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                               && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }

                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Keycount + NonKeycount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Keycount, NonKeycount, totalcount);
                                table.Rows.Add(ss.ProcessId, cc.Name, ss.Id, ss.Name, Keycount, NonKeycount, totalcount);
                        }
                    }

                }

                return table;
            }
        }
        public static DataTable GetBranchPRStatusTODKEYWise(int customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);

            List<int> TODPassIds = new List<int>();
            TODPassIds.Add(1);
            TODPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        public static DataTable GetBranchPRStatusTODNONKEYWise(int customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TODIds = new List<int>();
            TODIds.Add(-1);
            TODIds.Add(1);
            TODIds.Add(2);
            TODIds.Add(3);

            List<int> TODPassIds = new List<int>();
            TODPassIds.Add(1);
            TODPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TODIds.Contains((int)row.TOD)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TODPassIds.Contains((int)entry.TOD) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.TOD == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        #endregion
        #endregion

        # region Test Of Effectiveness Wise Customer
        public static DataTable GetPRTOEWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Keycount + NonKeycount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Keycount, NonKeycount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Keycount, NonKeycount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Keycount + NonKeycount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Keycount, NonKeycount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Keycount, NonKeycount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        public static DataTable GetPRStatusTOEKEYWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);

            List<int> TOEPassIds = new List<int>();
            TOEPassIds.Add(1);
            TOEPassIds.Add(3);

            List<int> TOEFailIds = new List<int>();
            TOEFailIds.Add(-1);
            TOEFailIds.Add(2);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TOEFailIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TOEFailIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        public static DataTable GetPRStatusTOENONKEYWise(int customerid, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);

            List<int> TOEPassIds = new List<int>();
            TOEPassIds.Add(1);
            TOEPassIds.Add(3);

            List<int> TOEFailIds = new List<int>();
            TOEFailIds.Add(-1);
            TOEFailIds.Add(2);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TOEFailIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {

                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TOEFailIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        #region Test Of Effectiveness Wise CustomerBranch
        public static DataTable GetBranchPRTOEWise(int customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Keycount + NonKeycount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Keycount, NonKeycount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Keycount, NonKeycount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Keycount;
                    long NonKeycount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Keycount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeycount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Keycount + NonKeycount;

                            if (totalcount != 0)
                               // table.Rows.Add(cc.Id, cc.Name, ss.Name, Keycount, NonKeycount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Keycount, NonKeycount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        public static DataTable GetBranchPRStatusTOEKEYWise(int customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);

            List<int> TOEPassIds = new List<int>();
            TOEPassIds.Add(1);
            TOEPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.TOE == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);

                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.TOE == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        public static DataTable GetBranchPRStatusTOENONKEYWise(int customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            List<int> TOEIds = new List<int>();
            TOEIds.Add(-1);
            TOEIds.Add(1);
            TOEIds.Add(2);
            TOEIds.Add(3);

            List<int> TOEPassIds = new List<int>();
            TOEPassIds.Add(1);
            TOEPassIds.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);
                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.TOE == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId && TOEIds.Contains((int)row.TOE)
                                             && row.FinancialYear == financialyear && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select row).ToList();
                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long Passcount;
                    long Failcount;
                    long totalcount;
                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Pass", typeof(long));
                    table.Columns.Add("Fail", typeof(long));
                    table.Columns.Add("Total", typeof(long));

                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            Passcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && TOEPassIds.Contains((int)entry.TOE) && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            Failcount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.TOE == 2 && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = Passcount + Failcount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, Passcount, Failcount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, Passcount, Failcount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        #endregion
        #endregion

        public static List<DeptDashboardSummaryview> GetPRDetailView(int customerid, string financialyear, string periodR, int UserID, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, int subprocessId, string TwelveMonth = "")
        {
            List<DeptDashboardSummaryview> detailView = new List<DeptDashboardSummaryview>();
            if (filter.Equals("Summary"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                              .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOD"))
            {
                List<int?> TODIds = new List<int?>();
                TODIds.Add(-1);
                TODIds.Add(1);
                TODIds.Add(2);
                TODIds.Add(3);
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId && entry.SubProcessId==subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID && TODIds.Contains((int?)entry.TOD)
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId) && TODIds.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.FinancialYear == financialyear && entry.ForMonth == periodR 
                            && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && TODIds.Contains((int?)entry.TOD)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && TODIds.Contains((int?)entry.TOD)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOE"))
            {
                List<int?> TOEIds = new List<int?>();
                TOEIds.Add(-1);
                TOEIds.Add(1);
                TOEIds.Add(2);
                TOEIds.Add(3);
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.PersonResponsible == UserID
                            && entry.FinancialYear == financialyear && TOEIds.Contains((int?)entry.TOE)
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && statusIDs.Contains((int)entry.KeyId) && TOEIds.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && TOEIds.Contains((int?)entry.TOE)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && TOEIds.Contains((int?)entry.TOE)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TODNONKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.KeyId == 2 && entry.PersonResponsible == UserID
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId  && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 2 && entry.PersonResponsible == UserID
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TODKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId  && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }

            }
            else if (filter.Equals("TOENONKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid 
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.KeyId == 2 && entry.PersonResponsible == UserID
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId  && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 2 && entry.PersonResponsible == UserID
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 2 && entry.PersonResponsible == UserID
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOEKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid
                            && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId  && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerID == customerid && entry.KeyId == 1 && entry.PersonResponsible == UserID
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            return detailView;
        }
        public static List<DeptDashboardSummaryview> GetBranchPRDetailView(int barnchId, string financialyear, string periodR, int UserID, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, int subprocessId, string TwelveMonth = "")
        {
            List<DeptDashboardSummaryview> detailView = new List<DeptDashboardSummaryview>();

            if (filter.Equals("Summary"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                                   .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.KeyId)
                                   && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.PersonResponsible == UserID && entry.FinancialYear == financialyear)
                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                                      .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.KeyId)
                                      && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                                      && entry.ForMonth == periodR).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                                  .Where(entry => entry.CustomerBranchID == barnchId && entry.PersonResponsible == UserID 
                                      && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear)
                                  .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                                     .Where(entry => entry.CustomerBranchID == barnchId && statusIDs.Contains((int)entry.KeyId) 
                                         && entry.PersonResponsible == UserID && entry.FinancialYear == financialyear
                                     && entry.ForMonth == periodR).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOD"))
            {
                List<int?> TODIds = new List<int?>();
                TODIds.Add(-1);
                TODIds.Add(1);
                TODIds.Add(2);
                TODIds.Add(3);
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.PersonResponsible == UserID
                            && entry.FinancialYear == financialyear && TODIds.Contains((int?)entry.TOD)
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusIDs.Contains((int)entry.KeyId) && TODIds.Contains((int?)entry.TOD) && entry.PersonResponsible == UserID
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && TODIds.Contains((int?)entry.TOD)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && TODIds.Contains((int?)entry.TOD)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR 
                            && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOE"))
            {
                List<int?> TOEIds = new List<int?>();
                TOEIds.Add(-1);
                TOEIds.Add(1);
                TOEIds.Add(2);
                TOEIds.Add(3);
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusIDs.Contains((int)entry.KeyId)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.PersonResponsible == UserID
                            && entry.FinancialYear == financialyear && TOEIds.Contains((int?)entry.TOE)
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetDeptDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusIDs.Contains((int)entry.KeyId) && TOEIds.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.FinancialYear == financialyear
                            && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && TOEIds.Contains((int?)entry.TOE)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && TOEIds.Contains((int?)entry.TOE)
                            && statusIDs.Contains((int)entry.KeyId) && entry.FinancialYear == financialyear && entry.ForMonth == periodR 
                            && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TODNONKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.KeyId == 2
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId  
                            && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear 
                            && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TODKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId  && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1 && entry.PersonResponsible == UserID
                            && statusNullableIDs.Contains((int?)entry.TOD) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }

            }
            else if (filter.Equals("TOENONKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && entry.KeyId == 2
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.KeyId == 2 && entry.PersonResponsible == UserID
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 2 && entry.PersonResponsible == UserID
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 2 && entry.PersonResponsible == UserID
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.ForMonth == periodR
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("TOEKEY"))
            {
                if (functionId != -1)
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId
                            && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId
                            && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE)
                            && entry.ProcessId == functionId && entry.SubProcessId == subprocessId && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    if (periodR == "All")
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        detailView = DashboardManagementRisk.GetPRDashboardSummary(UserID)
                            .Where(entry => entry.CustomerBranchID == barnchId && entry.KeyId == 1
                            && statusNullableIDs.Contains((int?)entry.TOE) && entry.FinancialYear == financialyear && entry.ForMonth == periodR && entry.PersonResponsible == UserID
                            ).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            return detailView;
        }

        public static DataTable GetPRWiseSummary(int customerid, string financialyear, string periodR, int userId, bool approver = false)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid
                                             && row.FinancialYear == financialyear && row.PersonResponsible==userId
                                             select new
                                             {
                                                 RiskCreationId = row.RiskCreationId,
                                                 row.ProcessId,
                                                 row.SubProcessId,
                                                 row.AuditStatusID,
                                                 row.KeyId,
                                                 row.ScheduledOnID,
                                                 row.RoleID,
                                                 row.UserID               
                                             }).ToList();

                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long KeyCount;
                    long NonKeyCount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            KeyCount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeyCount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = KeyCount + NonKeyCount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, KeyCount, NonKeyCount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, KeyCount, NonKeyCount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerID == customerid
                                              && row.FinancialYear == financialyear
                                             && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select new
                                             {
                                                 RiskCreationId = row.RiskCreationId,
                                                 row.ProcessId,
                                                 row.SubProcessId,
                                                 row.AuditStatusID,
                                                 row.KeyId,
                                                 row.ScheduledOnID,
                                                 row.RoleID,
                                                 row.UserID
                                             }).ToList();

                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long KeyCount;
                    long NonKeyCount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);
                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            KeyCount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeyCount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = KeyCount + NonKeyCount;

                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, KeyCount, NonKeyCount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, KeyCount, NonKeyCount, totalcount);
                        }
                    }
                }
                return table;
            }
        }
        public static DataTable GetBranchDeptWiseSummary(int customerid, int barnchId, string financialyear, string periodR, int userId = -1, bool approver = false)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                DataTable table = new DataTable();
                if (periodR == "All")
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId
                                             && row.FinancialYear == financialyear && row.PersonResponsible == userId
                                             select new
                                             {
                                                 RiskCreationId = row.RiskCreationId,
                                                 row.ProcessId,
                                                 row.SubProcessId,
                                                 row.AuditStatusID,
                                                 row.KeyId,
                                                 row.ScheduledOnID,
                                                 row.RoleID,
                                                 row.UserID   
                                             }).ToList();

                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long KeyCount;
                    long NonKeyCount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            KeyCount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeyCount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = KeyCount + NonKeyCount;
                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, KeyCount, NonKeyCount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, KeyCount, NonKeyCount, totalcount);
                        }
                    }
                }
                else
                {
                    var transactionsQuery = (from row in entities.DeptDashboardViews
                                             where row.CustomerBranchID == barnchId
                                             && row.FinancialYear == financialyear
                                              && row.ForMonth == periodR && row.PersonResponsible == userId
                                             select new
                                             {
                                                 RiskCreationId = row.RiskCreationId,
                                                 row.ProcessId,
                                                 row.SubProcessId,
                                                 row.AuditStatusID,
                                                 row.KeyId,
                                                 row.ScheduledOnID,
                                                 row.RoleID,
                                                 row.UserID   
                                             }).ToList();

                    if (approver == true)
                    {
                        transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == userId).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long KeyCount;
                    long NonKeyCount;
                    long totalcount;

                    table.Columns.Add("ProcessId", typeof(int));
                    table.Columns.Add("Process", typeof(string));
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("SubProcess", typeof(string));
                    table.Columns.Add("Key", typeof(long));
                    table.Columns.Add("NonKey", typeof(long));
                    table.Columns.Add("Total", typeof(long));
                    var processList = ProcessManagement.GetAllProcess(customerid);
                    var subprocessList = ProcessManagement.GetSubProcessAll(customerid);

                    foreach (Mst_Process cc in processList)
                    {
                        foreach (mst_Subprocess ss in subprocessList)
                        {
                            KeyCount = transactionsQuery.Where(entry => entry.KeyId == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            NonKeyCount = transactionsQuery.Where(entry => entry.KeyId == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id && entry.AuditStatusID != 11).Count();
                            totalcount = KeyCount + NonKeyCount;
                            if (totalcount != 0)
                                //table.Rows.Add(cc.Id, cc.Name, ss.Name, KeyCount, NonKeyCount, totalcount);
                                table.Rows.Add(cc.Id, cc.Name, ss.Id, ss.Name, KeyCount, NonKeyCount, totalcount);
                        }
                    }

                }
                return table;
            }
        }

       

        

    }
}
