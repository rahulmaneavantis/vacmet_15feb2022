﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using Newtonsoft.Json;
using System.Net;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ComplianceManagementComplianceScheduleon
    {
        public static long GetCustomerBranchID(long complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long branchid = (from row in entities.ComplianceInstances
                                 where row.ID == complianceInstanceID
                                 select row.CustomerBranchID).FirstOrDefault();

                return branchid;
            }
        }
        public static string GetMonthIndexFromAbbreviatedMonth(string monthValue)
        {
            string rlcsMonthValue = string.Empty;
            int monthIndex = 0;
            string[] MonthNames = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames;
            monthIndex = Array.IndexOf(MonthNames, monthValue) + 1;

            if (monthIndex != 0)
            {
                if (monthIndex < 10)
                    rlcsMonthValue = "0" + monthIndex;
                else
                    rlcsMonthValue = monthIndex.ToString();
            }

            return rlcsMonthValue;
        }

        public static string GetLast(string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return string.Empty;
            return source.Substring(source.Length - tail_length);
        }

        public static Tuple<string, string> Get_RLCSMonthYear(int frequency, string strPeriod)
        {
            Tuple<string, string> tupleRLCSMonthYear = new Tuple<string, string>(string.Empty, string.Empty);

            try
            {
                string rlcsMonthValue = string.Empty;
                string rlcsYearValue = string.Empty;

                if (frequency == 0) //Monthly
                {
                    string monthValue = strPeriod.Substring(0, 3).Trim();
                    rlcsMonthValue = GetMonthIndexFromAbbreviatedMonth(monthValue);

                    string yearValue = strPeriod.Substring(4, 2).Trim();
                    rlcsYearValue = "20" + yearValue;
                }
                else if (frequency == 1 || frequency == 2) //Quarterly  //Half Yearly
                {
                    string lastMonthinQuarter = GetLast(strPeriod, 6);

                    if (!string.IsNullOrEmpty(lastMonthinQuarter))
                    {
                        string monthValue = lastMonthinQuarter.Substring(0, 3).Trim();
                        rlcsMonthValue = GetMonthIndexFromAbbreviatedMonth(monthValue);

                        string yearValue = lastMonthinQuarter.Substring(4, 2).Trim();
                        rlcsYearValue = "20" + yearValue;
                    }
                }
                else if (frequency == 3) //Annual
                {
                    if (!string.IsNullOrEmpty(strPeriod))
                    {
                        if (strPeriod.Trim().ToUpper().Contains("CY"))
                        {
                            rlcsMonthValue = "12";
                            rlcsYearValue = "20" + GetLast(strPeriod, 2);
                        }
                        if (strPeriod.Trim().ToUpper().Contains("FY"))
                        {
                            rlcsMonthValue = "03";
                            rlcsYearValue = "20" + GetLast(strPeriod, 2);
                        }
                    }
                }
                else if (frequency == 5) //BiAnnual
                {
                    if (strPeriod.Contains("FY"))
                    {
                        rlcsMonthValue = "03";
                    }
                    else if (strPeriod.Contains("CY"))
                    {
                        rlcsMonthValue = "12";
                    }

                    rlcsYearValue = "20" + GetLast(strPeriod, 2);
                }

                tupleRLCSMonthYear = Tuple.Create(rlcsMonthValue, rlcsYearValue);
                return tupleRLCSMonthYear;
            }
            catch (Exception ex)
            {
                return tupleRLCSMonthYear;
            }
        }

        public static Tuple<string, string> Get_RLCSMonthYear(string frequencyText, string period, string year)
        {
            Tuple<string, string> tupleRLCSMonthYear = new Tuple<string, string>(string.Empty, string.Empty);

            try
            {
                int monthIndex = 0;
                string rlcsMonthValue = string.Empty;
                string rlcsYearValue = string.Empty;

                if (frequencyText.Trim().ToUpper().Equals("MONTHLY")) //Monthly
                {
                    string monthValue = period.Trim();
                    string yearValue = year.Trim();

                    if (!string.IsNullOrEmpty(monthValue))
                    {
                        if (Convert.ToInt32(monthValue) < 10 && monthValue.Length < 2)
                            rlcsMonthValue = "0" + monthValue;
                        else
                            rlcsMonthValue = monthValue.ToString();
                    }



                    rlcsYearValue = yearValue;
                }
                else if (frequencyText.Trim().ToUpper().Equals("QUARTERLY")) //Quarterly
                {
                    if (period == "Q1")
                        rlcsMonthValue = "03";
                    else if (period == "Q2")
                        rlcsMonthValue = "06";
                    else if (period == "Q3")
                        rlcsMonthValue = "09";
                    else if (period == "Q4")
                        rlcsMonthValue = "12";

                    rlcsYearValue = year;
                }
                else if (frequencyText.Trim().ToUpper().Equals("HALF YEARLY")) //Half Yearly
                {
                    if (period == "HY1")
                        rlcsMonthValue = "06";
                    else if (period == "HY2")
                        rlcsMonthValue = "12";

                    rlcsYearValue = year;
                }
                else if (frequencyText.Trim().ToUpper().Equals("ANNUAL")) //Annual
                {
                    rlcsMonthValue = "12";
                    //rlcsYearValue = (Convert.ToInt32(year) - 1) + "";
                    rlcsYearValue = Convert.ToInt32(year) + "";
                }
                else if (frequencyText.Trim().ToUpper().Equals("BIANNUAL")) //BiAnnual
                {
                    rlcsMonthValue = "12";
                    rlcsYearValue = (Convert.ToInt32(year)) + "";
                }

                tupleRLCSMonthYear = Tuple.Create(rlcsMonthValue, rlcsYearValue);
                return tupleRLCSMonthYear;
            }
            catch (Exception ex)
            {
                return tupleRLCSMonthYear;
            }
        }
    
        public static List<CustomerWiseComplianceSchedule> GetScheduleByCustomerwiseComplianceID(long complianceID, long customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.CustomerWiseComplianceSchedules
                                    where row.ComplianceID == complianceID
                                    && row.Customerid == customerid
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
       
        public static string GetForMonth(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");
                    if (date > DateTime.Today.Date)
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            if (forMonth == 11)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else if (forMonth == 12)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else
                            {
                                year = (date).ToString("yy");
                            }
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    else
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    break;

                case 3:
                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        if (date.Month >= 4)
                        {
                            startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                            endFinancial1Year = date.ToString("yy");
                        }
                        else if (date.Month >= 3)
                        {
                            startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                            endFinancial1Year = date.ToString("yy");
                        }
                        else
                        {
                            startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                            endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        }

                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }
                    break;

                case 4:

                    string year2 = date.ToString("yy");
                    if (forMonth == 9)
                    {
                        if (Convert.ToInt32(year2) >= Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year2 = (date.AddYears(-1)).ToString("yy");
                        }
                        else
                        {
                            year2 = (date).ToString("yy");
                        }

                    }
                    if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (date.AddYears(-1)).ToString("yy") +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + year2;
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2 +
                                       " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 3)).Substring(0, 3) + " " + year2;
                    }
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        forMonthName = "CY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    else if (forMonth == 4)
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                        forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;
                    }
                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-8)).ToString("yy");
                        endFinancial7Year = (date.AddYears(-1)).ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }

        public static string GetForMonthPeriodically(DateTime date, int forMonth, byte? Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 0:
                    string year = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 12;
                        year = (date.AddYears(-1)).ToString("yy");
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 1:
                    string yearq = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 10;
                        yearq = (date.AddYears(-1)).ToString("yy");
                    }


                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 2)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    
                    if (forMonth == 4)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 10)
                    {

                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (Convert.ToInt32(year1) - 1) +
                                  " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 7)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 1)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 7)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + (Convert.ToInt32(year1) - 1) +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 6)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 5)).Substring(0, 3) + " " + year1 +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1;
                    }
                    else if (forMonth == 12)
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 5)).Substring(0, 3) + " " + (Convert.ToInt32(year1)) +
                                " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1;
                    }
                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = date.ToString("yy");
                        endFinancial1Year = (date.AddYears(1)).ToString("yy"); date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    break;

                case 4:
                    string year2 = date.ToString("yy");
                    if (forMonth == 1)
                    {
                        forMonth = 9;
                        year2 = (date.AddYears(-1)).ToString("yy");
                    }


                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth - 3)).Substring(0, 3) + " " + year2 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year2;
                    break;
                case 5:

                    string startFinancial2Year = date.ToString("yy");
                    string endFinancial2Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial2Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial2Year = (date.AddYears(1)).ToString("yy");
                    }
                    else
                    {
                        startFinancial2Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial2Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial2Year + " - " + endFinancial2Year;

                    break;
                case 6:

                    string startFinancial7Year = date.ToString("yy");
                    string endFinancial7Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial7Year = (date.AddYears(-6)).ToString("yy");
                        endFinancial7Year = (date.AddYears(1)).ToString("yy");
                    }
                    else
                    {
                        startFinancial7Year = (date.AddYears(-7)).ToString("yy");
                        endFinancial7Year = date.ToString("yy");
                    }

                    forMonthName = "FY " + startFinancial7Year + " - " + endFinancial7Year;
                    break;

                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }
        public static Tuple<DateTime, string, long> GetNextDatePeriodicallyWithClientBasedDueDateChange(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, int frequency, int? customerid)
        {
            int formonthscheduleR = -1;
          
            var complianceSchedule = GetScheduleByCustomerwiseComplianceID(complianceID, Convert.ToInt32(customerid));
            DateTime date = complianceSchedule.Select(row => new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > scheduledOn).FirstOrDefault();
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            if (frequency == 3 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 5 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 6 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 2)
            {
                List<Tuple<DateTime, long>> complianceScheduleDates;
                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "07").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "07").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" || row.SpecialDate.Substring(2, 2) == "10").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "10").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                long ActualForMonth = 0;
                if (frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(lastSchedule.ScheduleOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
            }
            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    if (frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                    if (frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }
            string forMonth = string.Empty;
            if (complianceSchedule.Count > 0)
            {
                string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");
                var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();
                forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth, (byte)frequency);
                formonthscheduleR = ForMonthSchedule.ForMonth;
            }
            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);
        }
       
       
        public static bool CheckGenerateSchedule(long complianceInstanceID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var form = (from row in entities.ComplianceInstances
                                where row.ID == complianceInstanceID
                                select row).FirstOrDefault();

                    return form.GenerateSchedule;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region Compliance Instances
        public static bool CreateInstances(List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments, long createdByID, string creatdByName, long customerid)
        {
            long CreateInstancesErrorcomplianceInstanceID = -1;
            long CreateInstancesErrorComplianceId = -1;
            try
            {
                if (assignments.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        assignments.ForEach(entry =>
                        {
                            try
                            {

                                var compliances = (from row in entities.Compliances
                                                   where row.ID == entry.Item1.ComplianceId
                                                   select row).FirstOrDefault();

                              
                                long complianceInstanceID = (from CI in entities.ComplianceInstances
                                                             where CI.ComplianceId == entry.Item1.ComplianceId
                                                             && CI.CustomerBranchID == entry.Item1.CustomerBranchID
                                                             && CI.IsDeleted == false
                                                             && CI.DirectorId == entry.Item1.DirectorId
                                                             select CI.ID).SingleOrDefault();
                                if (complianceInstanceID <= 0)
                                {

                                    int docid = 0;
                                    entry.Item1.CreatedOn = DateTime.UtcNow;
                                    entry.Item1.IsDeleted = false;
                                    entry.Item1.GenerateSchedule = true;
                                    entry.Item1.IsAvantis = true;
                                    if (compliances.ComplianceType == 1)
                                    {
                                        entry.Item1.IsDocMan_NonMan = false;
                                        docid = 0;
                                    }
                                    else
                                    {
                                        entry.Item1.IsDocMan_NonMan = true;
                                        docid = 1;
                                    }
                                    entities.ComplianceInstances.Add(entry.Item1);
                                    entities.SaveChanges();

                                    complianceInstanceID = entry.Item1.ID;
                                    if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                    {
                                        if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                        {
                                            CreateInstancesErrorcomplianceInstanceID = complianceInstanceID;
                                            CreateInstancesErrorComplianceId = entry.Item1.ComplianceId;

                                            var customerspecificcompliance = (from row in entities.CustomerSpecificCompliances
                                                                              where row.CustomerID == customerid
                                                                              && row.ComplianceId == entry.Item1.ComplianceId
                                                                              select row).FirstOrDefault();


                                            if (compliances.ComplianceType != 3)
                                            {
                                                if (compliances.CheckListTypeID != 0)
                                                {
                                                    if (compliances.ComplianceType == 1)
                                                    {
                                                        var CBCF = (from row in entities.ClientBasedCheckListFrequencies
                                                                    where row.ClientID == customerid
                                                                    && row.ComplianceID == entry.Item1.ComplianceId
                                                                    select row).FirstOrDefault();
                                                        if (CBCF != null)
                                                        {

                                                            CreateScheduleOnWithClientFrequency(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName, CBCF.CFrequency, customerid, docid);
                                                        }
                                                        else
                                                        {
                                                            if (customerspecificcompliance != null)
                                                            {
                                                                CreateScheduleOnWithClientBasedDueDateFrequency(entry.Item1.ScheduledOn, complianceInstanceID, customerspecificcompliance, createdByID, creatdByName, Convert.ToInt32(customerspecificcompliance.Frequencyid), customerid, docid);
                                                            }
                                                            else
                                                            {
                                                                CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName, docid);

                                                            }
                                                        }
                                                    }
                                                    else
                                                    {

                                                        if (customerspecificcompliance != null)
                                                        {
                                                            CreateScheduleOnWithClientBasedDueDateFrequency(entry.Item1.ScheduledOn, complianceInstanceID, customerspecificcompliance, createdByID, creatdByName, Convert.ToInt32(customerspecificcompliance.Frequencyid), customerid, docid);
                                                        }
                                                        else
                                                        {
                                                            CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName, docid);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    entry.Item1.ID = complianceInstanceID;
                                }

                                long complianceAssignment = (from row in entities.ComplianceAssignments
                                                             where row.ComplianceInstanceID == entry.Item1.ID
                                                             && row.RoleID == entry.Item2.RoleID
                                                             select row.UserID).FirstOrDefault();




                                if (entry.Item2.RoleID != 6) //Performer,reviewer
                                {
                                    if (complianceAssignment <= 0)
                                    {
                                        entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                        entities.ComplianceAssignments.Add(entry.Item2);
                                        entities.SaveChanges();
                                        if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                        {
                                            if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                            {
                                                CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                                            }
                                        }
                                    }
                                }
                                else //Approver
                                {
                                    entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                    entities.ComplianceAssignments.Add(entry.Item2);
                                    entities.SaveChanges();

                                    if (entry.Item2.RoleID != 6)
                                    {
                                        if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                        {
                                            if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                            {
                                                CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                                            }
                                        }
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        });
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateInstances Function", "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateInstances Function", "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);
                return false;

            }
        }
        public static bool CreateInstances_Secretarial(List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments, long createdByID, string creatdByName, long customerid)
        {
            long CreateInstancesErrorcomplianceInstanceID = -1;
            long CreateInstancesErrorComplianceId = -1;
            try
            {
                if (assignments.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        assignments.ForEach(entry =>
                        {
                            try
                            {
                                var compliances = (from row in entities.Compliances
                                                   where row.ID == entry.Item1.ComplianceId
                                                   select row).FirstOrDefault();

                                long complianceInstanceID = (from CI in entities.ComplianceInstances
                                                             where CI.ComplianceId == entry.Item1.ComplianceId
                                                             && CI.CustomerBranchID == entry.Item1.CustomerBranchID
                                                             && CI.IsDeleted == false
                                                             && CI.DirectorId == entry.Item1.DirectorId
                                                             select CI.ID).SingleOrDefault();
                                if (complianceInstanceID <= 0)
                                {
                                    int docid = 0;
                                    entry.Item1.CreatedOn = DateTime.UtcNow;
                                    entry.Item1.IsDeleted = false;
                                    entry.Item1.GenerateSchedule = true;
                                    entry.Item1.IsAvantis = true;
                                    if (compliances.ComplianceType == 1)
                                    {
                                        entry.Item1.IsDocMan_NonMan = false;
                                        docid = 0;
                                    }
                                    else
                                    {
                                        entry.Item1.IsDocMan_NonMan = true;
                                        docid = 1;
                                    }
                                    entities.ComplianceInstances.Add(entry.Item1);
                                    entities.SaveChanges();

                                    complianceInstanceID = entry.Item1.ID;

                                    if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                    {
                                        if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                        {
                                            CreateInstancesErrorcomplianceInstanceID = complianceInstanceID;
                                            CreateInstancesErrorComplianceId = entry.Item1.ComplianceId;



                                            if (compliances.ComplianceType != 3)
                                            {
                                                if (compliances.CheckListTypeID != 0)
                                                {
                                                    if (compliances.ComplianceType == 1)
                                                    {
                                                        var CBCF = (from row in entities.ClientBasedCheckListFrequencies
                                                                    where row.ClientID == customerid
                                                                    && row.ComplianceID == entry.Item1.ComplianceId
                                                                    select row).FirstOrDefault();
                                                        if (CBCF != null)
                                                        {
                                                            CreateScheduleOnWithClientFrequency(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName, CBCF.CFrequency, customerid, docid);
                                                        }
                                                        else
                                                        {
                                                            CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName, docid);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName, docid);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    entry.Item1.ID = complianceInstanceID;
                                }

                                long complianceAssignment = (from row in entities.ComplianceAssignments
                                                             where row.ComplianceInstanceID == entry.Item1.ID
                                                             && row.RoleID == entry.Item2.RoleID
                                                             select row.UserID).FirstOrDefault();

                                if (complianceAssignment <= 0)
                                {
                                    entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                    entities.ComplianceAssignments.Add(entry.Item2);
                                    entities.SaveChanges();

                                    if (entry.Item2.RoleID != 6)
                                    {
                                        if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                        {
                                            if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                            {
                                                CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        });
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateInstances_Secretarial-ComplianceID-" + CreateInstancesErrorComplianceId + "-ComplianceInstanceID-" + CreateInstancesErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //TO.Add("ankit@avantis.info");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured-CreateInstances_Secretarial Function", "ComplianceID-" + CreateInstancesErrorComplianceId + "-ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);


                ComplianceManagement.SendgridSenEmail("Error Occured-CreateInstances_Secretarial Function", "ComplianceID-" + CreateInstancesErrorComplianceId + "-ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);

                return false;
            }
        }
        public static void CreateInstancesEventBased(List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments, long createdByID, string creatdByName)
        {
            long CreateInstancesErrorcomplianceInstanceID = -1;
            long CreateInstancesErrorComplianceId = -1;
            try
            {
                if (assignments.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        assignments.ForEach(entry =>
                        {

                            var compliances = (from row in entities.Compliances
                                               where row.ID == entry.Item1.ComplianceId
                                               select row).FirstOrDefault();

                            long complianceInstanceID = (from row in entities.ComplianceInstances
                                                         where row.ComplianceId == entry.Item1.ComplianceId && row.CustomerBranchID == entry.Item1.CustomerBranchID
                                                         && row.IsDeleted == false
                                                         select row.ID).FirstOrDefault();

                            if (complianceInstanceID <= 0)
                            {
                                int docid = 0;
                                entry.Item1.CreatedOn = DateTime.UtcNow;
                                entry.Item1.IsDeleted = false;
                                entry.Item1.GenerateSchedule = false;
                                entry.Item1.IsAvantis = true;
                                if (compliances.ComplianceType == 1)
                                {
                                    entry.Item1.IsDocMan_NonMan = false;
                                    docid = 0;
                                }
                                else
                                {
                                    entry.Item1.IsDocMan_NonMan = true;
                                    docid = 1;
                                }
                                entities.ComplianceInstances.Add(entry.Item1);
                                entities.SaveChanges();

                                complianceInstanceID = entry.Item1.ID;

                                //Commented by Sachin 08 June 2021
                                //if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                //{
                                //    if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                //    {
                                //        CreateInstancesErrorcomplianceInstanceID = complianceInstanceID;
                                //        CreateInstancesErrorComplianceId = entry.Item1.ComplianceId;
                                //        var compliances = (from row in entities.Compliances
                                //                           where row.ID == entry.Item1.ComplianceId
                                //                           select row).FirstOrDefault();
                                //        if (compliances.ComplianceType != 3)
                                //        {
                                //            CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName);
                                //        }
                                //    }
                                //}
                            }
                            else
                            {
                                entry.Item1.ID = complianceInstanceID;
                            }

                            long complianceAssignment = (from row in entities.ComplianceAssignments
                                                         where row.ComplianceInstanceID == entry.Item1.ID && row.RoleID == entry.Item2.RoleID
                                                         select row.UserID).FirstOrDefault();
                            if (entry.Item2.RoleID != 6)
                            {
                                if (complianceAssignment <= 0)
                                {
                                    entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                    entities.ComplianceAssignments.Add(entry.Item2);
                                    entities.SaveChanges();

                                    //Commented by Sachin 08 June 2021
                                    //if (entry.Item2.RoleID != 6)
                                    //{
                                    //    if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                    //    {
                                    //        if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                    //        {
                                    //            CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                                    //        }
                                    //    }
                                    //}
                                }
                            }
                            else
                            {
                                entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                entities.ComplianceAssignments.Add(entry.Item2);
                                entities.SaveChanges();

                                //Commented by Sachin 08 June 2021
                                //if (entry.Item2.RoleID != 6)
                                //{
                                //    if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                //    {
                                //        if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                //        {
                                //            CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                                //        }
                                //    }
                                //}
                            }

                        });
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateInstances Function", "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateInstances Function", "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);
            }
        }
        public static void CreateInstancesChecklist(List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments, long createdByID, string creatdByName)
        {
            long CreateInstancesErrorcomplianceInstanceID = -1;
            long CreateInstancesErrorComplianceId = -1;
            try
            {
                if (assignments.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        assignments.ForEach(entry =>
                        {

                            var compliances = (from row in entities.Compliances
                                               where row.ID == entry.Item1.ComplianceId
                                               select row).FirstOrDefault();

                            long complianceInstanceID = (from row in entities.ComplianceInstances
                                                         where row.ComplianceId == entry.Item1.ComplianceId && row.CustomerBranchID == entry.Item1.CustomerBranchID
                                                         && row.IsDeleted == false
                                                         select row.ID).SingleOrDefault();

                            if (complianceInstanceID <= 0)
                            {
                                int docid = 0;
                                entry.Item1.CreatedOn = DateTime.UtcNow;
                                entry.Item1.IsDeleted = false;
                                if (compliances.ComplianceType == 1)
                                {
                                    entry.Item1.IsDocMan_NonMan = false;
                                    docid = 0;
                                }
                                else
                                {
                                    entry.Item1.IsDocMan_NonMan = true;
                                    docid = 1;
                                }
                                entities.ComplianceInstances.Add(entry.Item1);
                                entities.SaveChanges();

                                complianceInstanceID = entry.Item1.ID;

                                if ((compliances.ComplianceType == 1) && (compliances.CheckListTypeID == 0))//for one time checklist
                                {

                                    ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                    complianceScheduleon.ComplianceInstanceID = complianceInstanceID;
                                    if (compliances.OneTimeDate.HasValue == true)
                                    {
                                        complianceScheduleon.ScheduleOn = (DateTime)compliances.OneTimeDate;
                                    }

                                    complianceScheduleon.ForMonth = null;
                                    complianceScheduleon.ForPeriod = null;
                                    complianceScheduleon.IsActive = true;
                                    complianceScheduleon.IsUpcomingNotDeleted = true;
                                    if (docid == 1)
                                    {
                                        complianceScheduleon.IsDocMan_NonMan = true;
                                    }
                                    else
                                    {
                                        complianceScheduleon.IsDocMan_NonMan = false;
                                    }

                                    entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                    entities.SaveChanges();



                                    ComplianceTransaction transaction = new ComplianceTransaction()
                                    {
                                        ComplianceInstanceId = complianceInstanceID,
                                        ComplianceScheduleOnID = complianceScheduleon.ID,
                                        CreatedBy = createdByID,
                                        CreatedByText = creatdByName,
                                        StatusId = 1,
                                        Remarks = "New compliance assigned."
                                    };

                                    ComplianceManagement.CreateTransaction(transaction, null, null, null);


                                }
                                else
                                {
                                    CreateInstancesErrorcomplianceInstanceID = complianceInstanceID;
                                    CreateInstancesErrorComplianceId = entry.Item1.ComplianceId;
                                    CreateScheduleOn(entry.Item1.ScheduledOn, complianceInstanceID, compliances, createdByID, creatdByName, docid);
                                }
                            }
                            else
                            {
                                entry.Item1.ID = complianceInstanceID;
                            }

                            long complianceAssignment = (from row in entities.ComplianceAssignments
                                                         where row.ComplianceInstanceID == entry.Item1.ID && row.RoleID == entry.Item2.RoleID
                                                         select row.UserID).FirstOrDefault();
                            if (entry.Item2.RoleID != 6)
                            {
                                if (complianceAssignment <= 0)
                                {
                                    entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                    entities.ComplianceAssignments.Add(entry.Item2);
                                    entities.SaveChanges();

                                    if (entry.Item2.RoleID != 6)
                                    {
                                        if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                        {
                                            if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                            {
                                                CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                entry.Item2.ComplianceInstanceID = complianceInstanceID;
                                entities.ComplianceAssignments.Add(entry.Item2);
                                entities.SaveChanges();

                                if (entry.Item2.RoleID != 6)
                                {
                                    if (ComplianceIsEventBased(entry.Item1.ComplianceId))
                                    {
                                        if (ComplianceIsActionable(entry.Item1.ComplianceId))
                                        {
                                            CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                                        }
                                    }
                                }
                            }

                        });
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateInstances Function", "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateInstances Function", "CreateInstances" + "ComplianceId" + CreateInstancesErrorComplianceId + "ComplianceInstanceID" + CreateInstancesErrorcomplianceInstanceID);
            }
        }
       
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        

      
        public static Tuple<DateTime, string, long> GetNextDateWeekly(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, int WeeklyDayFromCompliance)
        {
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            var getinstancescheduleondate = GetComplianceInstance(ComplianceInstanceID);
            DateTime date = DateTime.Now;
            if (lastSchedule == null)
            {
                DateTime curruntDate;
                curruntDate = DateTime.UtcNow.AddDays(30);
                DateTime dtinstance = getinstancescheduleondate.ScheduledOn.Date;
                while (dtinstance < curruntDate)
                {
                    int weekdayFromInstance = Convert.ToInt32(dtinstance.DayOfWeek);
                    if (weekdayFromInstance == WeeklyDayFromCompliance)
                    {
                        date = new DateTime(dtinstance.Year, Convert.ToInt32(dtinstance.Month), Convert.ToInt32(dtinstance.Day));

                        date = dtinstance; // new DateTime(dtinstance.Year, Convert.ToInt32(dtinstance.Month), Convert.ToInt32(dtinstance.Day));

                        if (date >= getinstancescheduleondate.ScheduledOn)
                        {
                            break;
                        }
                        else
                        {
                            dtinstance = dtinstance.AddDays(1);
                        }
                    }
                    else
                    {
                        dtinstance = dtinstance.AddDays(1);
                    }
                }
            }
            else
            {
                date = scheduledOn.AddDays(7); // new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month), Convert.ToInt32(scheduledOn.Day + 7));
            }

            string forMonth = string.Empty;
            long ActualForMonth = 0;
            //forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.IFrequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }
      

        public static Tuple<DateTime, string, long> GetNextDateDaily(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, bool iseffectivedate)
        {
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            var getinstancescheduleondate = GetComplianceInstance(ComplianceInstanceID);
            DateTime date = DateTime.Now;
            if (lastSchedule == null)
            {
                if (getinstancescheduleondate.ScheduledOn.Date < scheduledOn.Date)
                {
                    date = new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month), Convert.ToInt32(scheduledOn.Day));
                }
                else
                {
                    date = new DateTime(getinstancescheduleondate.ScheduledOn.Year, Convert.ToInt32(getinstancescheduleondate.ScheduledOn.Month), Convert.ToInt32(getinstancescheduleondate.ScheduledOn.Day));
                }
            }
            else
            {
                int days = -1;
                int totaldays = DateTime.DaysInMonth(scheduledOn.Year, scheduledOn.Month);
                days = Convert.ToInt32(scheduledOn.Day + 1);
                if (totaldays >= days)
                {
                    date = new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month), days);
                }
                else
                {
                    date = new DateTime(scheduledOn.Year, Convert.ToInt32(scheduledOn.Month + 1), 1);
                }
            }
            if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
            }
            string forMonth = string.Empty;
            long ActualForMonth = 0;
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }

        public static void CreateScheduleOnWithClientFrequency(DateTime scheduledOn, long complianceInstanceId, Compliance compliances, long createdByID, string creatdByName, int frequency, long customerid, int isdocflag)
        {
            long CreateScheduleOnErroComplianceid = -1;
            long CreateScheduleOnErrocomplianceInstanceId = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var onloff = 1;
                    var beforafter = 0;
                    if (compliances.duedatetype != null)
                    {
                        beforafter = (int)compliances.duedatetype;
                    }
                    if (compliances.onlineoffline != null)
                    {
                        onloff = Convert.ToByte(compliances.onlineoffline);
                    }
                    DateTime nextDate = scheduledOn;
                    DateTime curruntDate;
                    if (frequency == 7)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(1);
                    }
                    else if (frequency == 8)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(30);
                    }
                    else
                    {
                        curruntDate = DateTime.UtcNow;
                    }

                    while (nextDate < curruntDate) //Commented by rahul on 7 MARCH 2016 For Check Scheduled on
                    {
                        Tuple<DateTime, string, long> nextDateMonth;

                        if (frequency == 7)
                        {
                            #region Frequancy Daily                        
                            CreateScheduleOnErroComplianceid = compliances.ID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateDaily(nextDate, compliances.ID, complianceInstanceId, false);

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                if (nextDateMonth.Item1.Date.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                    complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                    complianceScheduleon.ScheduleOn = nextDateMonth.Item1;
                                    complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                    complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                    complianceScheduleon.IsActive = true;
                                    complianceScheduleon.IsUpcomingNotDeleted = true;
                                    if (isdocflag == 1)
                                    {
                                        complianceScheduleon.IsDocMan_NonMan = true;
                                    }
                                    else
                                    {
                                        complianceScheduleon.IsDocMan_NonMan = false;
                                    }
                                    entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDateMonth.Item1;

                                    ComplianceTransaction transaction = new ComplianceTransaction()
                                    {
                                        ComplianceInstanceId = complianceInstanceId,
                                        ComplianceScheduleOnID = complianceScheduleon.ID,
                                        CreatedBy = createdByID,
                                        CreatedByText = creatdByName,
                                        StatusId = 1,
                                        Remarks = "New compliance assigned."
                                    };

                                    ComplianceManagement.CreateTransaction(transaction);
                                }
                                else
                                {
                                    if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        nextDate = nextDateMonth.Item1.AddDays(1);
                                    }
                                    else
                                    {
                                        nextDate = nextDateMonth.Item1;
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (frequency == 8)
                        {
                            #region Frequancy Weekly
                            CreateScheduleOnErroComplianceid = compliances.ID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateWeekly(nextDate, compliances.ID, complianceInstanceId, Convert.ToInt32(compliances.DueWeekDay));

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic 
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime dtNewScheduleOn;
                                if (onloff == 1) //online
                                {
                                    dtNewScheduleOn = nextDateMonth.Item1;
                                    if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                    }
                                }
                                else
                                {
                                    //offline
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        dtNewScheduleOn = nextDateMonth.Item1;
                                        if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                        }
                                    }
                                }
                                #endregion
                               
                                DateTime dtStart;
                                if (compliances.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(compliances.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }
                               
                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = dtNewScheduleOn; // nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                if (isdocflag == 1)
                                {
                                    complianceScheduleon.IsDocMan_NonMan = true;
                                }
                                else
                                {
                                    complianceScheduleon.IsDocMan_NonMan = false;
                                }
                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    nextDate = dtNewScheduleOn.AddDays(1);
                                }
                                else
                                {
                                    nextDate = nextDateMonth.Item1;
                                }

                               
                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                ComplianceManagement.CreateTransaction(transaction);
                              
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Frequancy 
                            if (compliances.SubComplianceType == 0)
                            {
                                nextDateMonth = new Tuple<DateTime, string, long>(nextDate.AddDays(Convert.ToDouble(compliances.DueDate)), "", 0);
                            }
                            else if (compliances.SubComplianceType == 1 || compliances.SubComplianceType == 2)
                            {
                                CreateScheduleOnErroComplianceid = compliances.ID;
                                nextDateMonth = GetNextDatePeriodicallyWithClientBased(nextDate, compliances.ID, complianceInstanceId, frequency);
                            }
                            else
                            {
                                CreateScheduleOnErroComplianceid = compliances.ID;
                                CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                                nextDateMonth = GetNextDateWithClientBased(nextDate, compliances.ID, complianceInstanceId, false, frequency, customerid);
                            }

                            string RLCSMonth = string.Empty;
                            string RLCSYear = string.Empty;

                            if (compliances.Frequency != null)
                            {
                                var tupleRLCSMonthYear = Get_RLCSMonthYear(Convert.ToInt32(compliances.Frequency), nextDateMonth.Item2);

                                if (tupleRLCSMonthYear != null)
                                {
                                    RLCSMonth = tupleRLCSMonthYear.Item1;
                                    RLCSYear = tupleRLCSMonthYear.Item2;
                                }
                            }
                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime ScheduleOn1;
                                if (onloff == 1) //online
                                {
                                    ScheduleOn1 = nextDateMonth.Item1;
                                }
                                else
                                {
                                    //offline                                    
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        ScheduleOn1 = nextDateMonth.Item1;
                                    }
                                }
                                #endregion
                                //Complinace Start Date logic
                                DateTime dtStart;
                                if (compliances.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(compliances.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }
                                //if (dtStart <= ScheduleOn1)
                                //{
                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = ScheduleOn1;// nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                if (!string.IsNullOrEmpty(RLCSMonth))
                                    complianceScheduleon.RLCS_PayrollMonth = RLCSMonth;

                                if (!string.IsNullOrEmpty(RLCSYear))
                                    complianceScheduleon.RLCS_PayrollYear = RLCSYear;

                                complianceScheduleon.RLCS_ActivityEndDate = nextDateMonth.Item1;
                                if (isdocflag == 1)
                                {
                                    complianceScheduleon.IsDocMan_NonMan = true;
                                }
                                else
                                {
                                    complianceScheduleon.IsDocMan_NonMan = false;
                                }
                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                nextDate = nextDateMonth.Item1;

                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                ComplianceManagement.CreateTransaction(transaction);

                                #region Task Scheduele
                                long CustomerBranchID = GetCustomerBranchID(complianceInstanceId);
                                var Taskapplicable = TaskManagment.TaskApplicable(CustomerBranchID);
                                if (Taskapplicable != null)
                                {

                                    if (Taskapplicable == 1)
                                    {
                                        var TaskIDList = (from row in entities.Tasks
                                                          join row1 in entities.TaskComplianceMappings
                                                          on row.ID equals row1.TaskID
                                                          where row1.ComplianceID == compliances.ID
                                                          && row.TaskType == 1 && row.ParentID == null
                                                          && (((row.Status != "A") || (row.Status == null))
                                                          || ((row.Status == "A") && (row.DeactivateOn > DateTime.Now)))
                                                          && row.Isdeleted == false && row.IsActive == true
                                                          select row.ID).ToList();

                                        if (TaskIDList.Count > 0)
                                        {
                                            TaskIDList.ForEach(entrytask =>
                                            {
                                                List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

                                                bool? IsAfter = TaskManagment.GetTaskBeforeAfter(entrytask);

                                                if (IsAfter == null)
                                                {
                                                    IsAfter = false;
                                                }

                                                var taskSubTaskList = (from row in entities.Tasks
                                                                       join row1 in entities.TaskInstances
                                                                       on row.ID equals row1.TaskId
                                                                       where row.MainTaskID == entrytask && row1.CustomerBranchID == CustomerBranchID
                                                                       select new { row.ID, row.ParentID, row.DueDays }).ToList();

                                                taskSubTaskList.ForEach(entrysubtask =>
                                                {
                                                    TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long)entrysubtask.ID, ParentID = (int?)entrysubtask.ParentID, ActualDueDays = (int)entrysubtask.DueDays });

                                                });

                                                TaskSubTaskList.ForEach(entryTaskSubTaskList =>
                                                {

                                                    var taskInstances = (from row in entities.TaskInstances
                                                                         where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
                                                                          && row.ScheduledOn <= nextDateMonth.Item1
                                                                          && row.CustomerBranchID == CustomerBranchID
                                                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                                                    DateTime schedueon;
                                                    if (IsAfter == null)
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }
                                                    else
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }

                                                    taskInstances.ForEach(entrytaskinstance =>
                                                    {
                                                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                                                        taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
                                                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleon.ID;
                                                        taskScheduleOn.ForMonth = nextDateMonth.Item2;
                                                        taskScheduleOn.ForPeriod = nextDateMonth.Item3;
                                                        taskScheduleOn.ScheduleOn = schedueon; // nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                        taskScheduleOn.IsActive = true;
                                                        taskScheduleOn.IsUpcomingNotDeleted = true;
                                                        entities.TaskScheduleOns.Add(taskScheduleOn);
                                                        entities.SaveChanges();

                                                        var AssignedTaskRole = TaskManagment.GetAssignedTaskUsers((int)entrytaskinstance.ID);
                                                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                        if (performerTaskRole != null)
                                                        {
                                                            var user = UserManagement.GetByID((int)performerTaskRole.UserID);
                                                            TaskTransaction tasktransaction = new TaskTransaction()
                                                            {
                                                                TaskInstanceId = entrytaskinstance.ID,
                                                                TaskScheduleOnID = taskScheduleOn.ID,
                                                                ComplianceScheduleOnID = complianceScheduleon.ID,
                                                                CreatedBy = performerTaskRole.UserID,
                                                                CreatedByText = user.FirstName + " " + user.LastName,
                                                                StatusId = 1,
                                                                Remarks = "New task assigned."
                                                            };

                                                            TaskManagment.CreateTaskTransaction(tasktransaction);

                                                            foreach (var roles in AssignedTaskRole)
                                                            {
                                                                if (roles.RoleID != 6)
                                                                {
                                                                    DateTime schdulon;
                                                                    if (IsAfter == true)
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    else
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    TaskManagment.CreateTaskReminders(entrytaskinstance.TaskId, compliances.ID, roles.ID, schdulon, complianceScheduleon.ID, IsAfter);
                                                                }
                                                            }
                                                        }

                                                    });
                                                });
                                            });
                                        }
                                    }
                                }
                                #endregion
                                //}
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);
                ComplianceManagement.SendgridSenEmail("Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);
            }
        }

        public static void CreateScheduleOnWithClientBasedDueDateFrequency(DateTime scheduledOn, long complianceInstanceId, CustomerSpecificCompliance compliances, long createdByID, string creatdByName, int frequency, long customerid, int isdocflag)
        {
            long CreateScheduleOnErroComplianceid = -1;
            long CreateScheduleOnErrocomplianceInstanceId = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var onloff = 1;
                    var beforafter = 0;

                    var ComplianceInfo = (from row in entities.Compliances
                                          where row.ID == compliances.ComplianceId
                                          select row).SingleOrDefault();

                    DateTime nextDate = scheduledOn;
                    DateTime curruntDate;
                    if (frequency == 7)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(1);
                    }
                    else if (frequency == 8)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(30);
                    }
                    else
                    {
                        curruntDate = DateTime.UtcNow;
                    }

                    while (nextDate < curruntDate) //Commented by rahul on 7 MARCH 2016 For Check Scheduled on
                    {
                        Tuple<DateTime, string, long> nextDateMonth;

                        if (frequency == 7)
                        {
                            #region Frequancy Daily                        
                            CreateScheduleOnErroComplianceid = compliances.ComplianceId;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateDaily(nextDate, compliances.ComplianceId, complianceInstanceId, false);

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                if (nextDateMonth.Item1.Date.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                    complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                    complianceScheduleon.ScheduleOn = nextDateMonth.Item1;
                                    complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                    complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                    complianceScheduleon.IsActive = true;
                                    complianceScheduleon.IsUpcomingNotDeleted = true;
                                    if (isdocflag == 1)
                                    {
                                        complianceScheduleon.IsDocMan_NonMan = true;
                                    }
                                    else
                                    {
                                        complianceScheduleon.IsDocMan_NonMan = false;
                                    }
                                    entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDateMonth.Item1;

                                    ComplianceTransaction transaction = new ComplianceTransaction()
                                    {
                                        ComplianceInstanceId = complianceInstanceId,
                                        ComplianceScheduleOnID = complianceScheduleon.ID,
                                        CreatedBy = createdByID,
                                        CreatedByText = creatdByName,
                                        StatusId = 1,
                                        Remarks = "New compliance assigned."
                                    };

                                    ComplianceManagement.CreateTransaction(transaction);
                                }
                                else
                                {
                                    if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        nextDate = nextDateMonth.Item1.AddDays(1);
                                    }
                                    else
                                    {
                                        nextDate = nextDateMonth.Item1;
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (frequency == 8)
                        {
                            #region Frequancy Weekly
                            CreateScheduleOnErroComplianceid = compliances.ComplianceId;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateWeekly(nextDate, compliances.ComplianceId, complianceInstanceId, Convert.ToInt32(ComplianceInfo.DueWeekDay));

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic 
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime dtNewScheduleOn;
                                if (onloff == 1) //online
                                {
                                    dtNewScheduleOn = nextDateMonth.Item1;
                                    if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                    }
                                }
                                else
                                {
                                    //offline
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        dtNewScheduleOn = nextDateMonth.Item1;
                                        if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                        }
                                    }
                                }
                                #endregion
                                //DateTime dtNewScheduleOn;
                                //if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                //{
                                //    dtNewScheduleOn = nextDateMonth.Item1.Date.AddDays(1);
                                //}
                                //else
                                //{
                                //    dtNewScheduleOn = nextDateMonth.Item1.Date;
                                //}
                                DateTime dtStart;
                                if (ComplianceInfo.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(ComplianceInfo.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }
                                //if (dtStart <= dtNewScheduleOn)
                                //{
                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = dtNewScheduleOn; // nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                if (isdocflag == 1)
                                {
                                    complianceScheduleon.IsDocMan_NonMan = true;
                                }
                                else
                                {
                                    complianceScheduleon.IsDocMan_NonMan = false;
                                }
                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    nextDate = dtNewScheduleOn.AddDays(1);
                                }
                                else
                                {
                                    nextDate = nextDateMonth.Item1;
                                }

                                //if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                //{
                                //    nextDate = nextDateMonth.Item1.AddDays(1);
                                //}
                                //else
                                //{
                                //    nextDate = nextDateMonth.Item1;
                                //}
                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                ComplianceManagement.CreateTransaction(transaction);
                                //}
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Frequancy 
                            if (compliances.SubComplianceType == 0)
                            {
                                nextDateMonth = new Tuple<DateTime, string, long>(nextDate.AddDays(Convert.ToDouble(compliances.DueDate)), "", 0);
                            }
                            else if (compliances.SubComplianceType == 1 || compliances.SubComplianceType == 2)
                            {
                                CreateScheduleOnErroComplianceid = compliances.ComplianceId;
                                //nextDateMonth = GetNextDatePeriodicallyWithClientBased(nextDate, compliances.ID, complianceInstanceId, frequency);
                                nextDateMonth = GetNextDatePeriodicallyWithClientBasedDueDateChange(nextDate, compliances.ComplianceId, complianceInstanceId, frequency, (compliances.CustomerID));
                            }
                            else
                            {
                                CreateScheduleOnErroComplianceid = compliances.ComplianceId;
                                CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                                nextDateMonth = GetNextDateWithClientBased(nextDate, compliances.ComplianceId, complianceInstanceId, false, frequency, customerid);
                            }

                            string RLCSMonth = string.Empty;
                            string RLCSYear = string.Empty;

                            if (compliances.Frequencyid != null)
                            {
                                var tupleRLCSMonthYear = Get_RLCSMonthYear(Convert.ToInt32(compliances.Frequencyid), nextDateMonth.Item2);

                                if (tupleRLCSMonthYear != null)
                                {
                                    RLCSMonth = tupleRLCSMonthYear.Item1;
                                    RLCSYear = tupleRLCSMonthYear.Item2;
                                }
                            }
                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime ScheduleOn1;
                                if (onloff == 1) //online
                                {
                                    ScheduleOn1 = nextDateMonth.Item1;
                                }
                                else
                                {
                                    //offline                                    
                                    var HolidayCount = CheckHoliday(compliances.ComplianceId, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        ScheduleOn1 = nextDateMonth.Item1;
                                    }
                                }
                                #endregion
                                //Complinace Start Date logic
                                DateTime dtStart;
                                if (ComplianceInfo.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(ComplianceInfo.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }
                                //if (dtStart <= ScheduleOn1)
                                //{
                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = ScheduleOn1;// nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                if (!string.IsNullOrEmpty(RLCSMonth))
                                    complianceScheduleon.RLCS_PayrollMonth = RLCSMonth;

                                if (!string.IsNullOrEmpty(RLCSYear))
                                    complianceScheduleon.RLCS_PayrollYear = RLCSYear;

                                complianceScheduleon.RLCS_ActivityEndDate = nextDateMonth.Item1;
                                if (isdocflag == 1)
                                {
                                    complianceScheduleon.IsDocMan_NonMan = true;
                                }
                                else
                                {
                                    complianceScheduleon.IsDocMan_NonMan = false;
                                }
                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                nextDate = nextDateMonth.Item1;

                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                ComplianceManagement.CreateTransaction(transaction);

                                #region Task Scheduele
                                long CustomerBranchID = GetCustomerBranchID(complianceInstanceId);
                                var Taskapplicable = TaskManagment.TaskApplicable(CustomerBranchID);
                                if (Taskapplicable != null)
                                {

                                    if (Taskapplicable == 1)
                                    {
                                        var TaskIDList = (from row in entities.Tasks
                                                          join row1 in entities.TaskComplianceMappings
                                                          on row.ID equals row1.TaskID
                                                          where row1.ComplianceID == compliances.ComplianceId
                                                          && row.TaskType == 1 && row.ParentID == null
                                                          && (((row.Status != "A") || (row.Status == null))
                                                          || ((row.Status == "A") && (row.DeactivateOn > DateTime.Now)))
                                                          && row.Isdeleted == false && row.IsActive == true
                                                          select row.ID).ToList();

                                        if (TaskIDList.Count > 0)
                                        {
                                            TaskIDList.ForEach(entrytask =>
                                            {
                                                List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

                                                bool? IsAfter = TaskManagment.GetTaskBeforeAfter(entrytask);

                                                if (IsAfter == null)
                                                {
                                                    IsAfter = false;
                                                }

                                                var taskSubTaskList = (from row in entities.Tasks
                                                                       join row1 in entities.TaskInstances
                                                                       on row.ID equals row1.TaskId
                                                                       where row.MainTaskID == entrytask && row1.CustomerBranchID == CustomerBranchID
                                                                       select new { row.ID, row.ParentID, row.DueDays }).ToList();

                                                taskSubTaskList.ForEach(entrysubtask =>
                                                {
                                                    TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long)entrysubtask.ID, ParentID = (int?)entrysubtask.ParentID, ActualDueDays = (int)entrysubtask.DueDays });

                                                });

                                                TaskSubTaskList.ForEach(entryTaskSubTaskList =>
                                                {

                                                    var taskInstances = (from row in entities.TaskInstances
                                                                         where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
                                                                          && row.ScheduledOn <= nextDateMonth.Item1
                                                                          && row.CustomerBranchID == CustomerBranchID
                                                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                                                    DateTime schedueon;
                                                    if (IsAfter == null)
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }
                                                    else
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }

                                                    taskInstances.ForEach(entrytaskinstance =>
                                                    {
                                                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                                                        taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
                                                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleon.ID;
                                                        taskScheduleOn.ForMonth = nextDateMonth.Item2;
                                                        taskScheduleOn.ForPeriod = nextDateMonth.Item3;
                                                        taskScheduleOn.ScheduleOn = schedueon; // nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                        taskScheduleOn.IsActive = true;
                                                        taskScheduleOn.IsUpcomingNotDeleted = true;
                                                        entities.TaskScheduleOns.Add(taskScheduleOn);
                                                        entities.SaveChanges();

                                                        var AssignedTaskRole = TaskManagment.GetAssignedTaskUsers((int)entrytaskinstance.ID);
                                                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                        if (performerTaskRole != null)
                                                        {
                                                            var user = UserManagement.GetByID((int)performerTaskRole.UserID);
                                                            TaskTransaction tasktransaction = new TaskTransaction()
                                                            {
                                                                TaskInstanceId = entrytaskinstance.ID,
                                                                TaskScheduleOnID = taskScheduleOn.ID,
                                                                ComplianceScheduleOnID = complianceScheduleon.ID,
                                                                CreatedBy = performerTaskRole.UserID,
                                                                CreatedByText = user.FirstName + " " + user.LastName,
                                                                StatusId = 1,
                                                                Remarks = "New task assigned."
                                                            };

                                                            TaskManagment.CreateTaskTransaction(tasktransaction);

                                                            foreach (var roles in AssignedTaskRole)
                                                            {
                                                                if (roles.RoleID != 6)
                                                                {
                                                                    DateTime schdulon;
                                                                    if (IsAfter == true)
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    else
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    TaskManagment.CreateTaskReminders(entrytaskinstance.TaskId, compliances.ComplianceId, roles.ID, schdulon, complianceScheduleon.ID, IsAfter);
                                                                }
                                                            }
                                                        }

                                                    });
                                                });
                                            });
                                        }
                                    }
                                }
                                #endregion
                                //}
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }

     
        public static int CheckHoliday(long ComplianceID, long complianceInstanceId, DateTime ScheduleOn, int duebefore, int isonlineoffline)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int cnt = 0;
                Boolean flag = false;

                //before
                if ((isonlineoffline == 0 || isonlineoffline == null) && duebefore == 1)
                {
                    while (flag == false)
                    {
                        var data = (from row in entities.Sp_CheckHoliday(ComplianceID, complianceInstanceId, ScheduleOn.Day, ScheduleOn.Month, ScheduleOn.Year)
                                    select row).ToList();
                        if (data.Count == 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            cnt += 1;
                            flag = false;
                            ScheduleOn = ScheduleOn.AddDays(-1);
                            if (ScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                            {
                                cnt += 1;
                                ScheduleOn = ScheduleOn.AddDays(-1);
                            }
                        }
                    }
                }
                else
                {
                    while (flag == false)
                    {
                        var data = (from row in entities.Sp_CheckHoliday(ComplianceID, complianceInstanceId, ScheduleOn.Day, ScheduleOn.Month, ScheduleOn.Year)
                                    select row).ToList();
                        if (data.Count == 0)
                        {
                            flag = true;
                        }
                        else
                        {
                            cnt += 1;
                            flag = false;
                            ScheduleOn = ScheduleOn.AddDays(1);
                            if (ScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                            {
                                cnt += 1;
                                ScheduleOn = ScheduleOn.AddDays(1);
                            }
                        }
                    }
                }

                return cnt;
            }
        }

        public static void CreateScheduleOn(DateTime scheduledOn, long complianceInstanceId, Compliance compliances, long createdByID, string creatdByName, int isdocflag)
        {
            long CreateScheduleOnErroComplianceid = -1;
            long CreateScheduleOnErrocomplianceInstanceId = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var onloff = 1;
                    var beforafter = 0;
                    if (compliances.duedatetype != null)
                    {
                        beforafter = (int)compliances.duedatetype;
                    }
                    if (compliances.onlineoffline != null)
                    {
                        onloff = Convert.ToByte(compliances.onlineoffline);
                    }

                    DateTime nextDate = scheduledOn;
                    DateTime curruntDate;
                    if (compliances.Frequency == 7)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(1);
                    }
                    else if (compliances.Frequency == 8)
                    {
                        curruntDate = DateTime.UtcNow.AddDays(30);
                    }
                    else
                    {
                        curruntDate = DateTime.UtcNow;
                    }

                    while (nextDate < curruntDate) //Commented by rahul on 7 MARCH 2016 For Check Scheduled on
                    {
                        Tuple<DateTime, string, long> nextDateMonth;

                        if (compliances.Frequency == 7)
                        {
                            #region Frequancy Daily                        
                            CreateScheduleOnErroComplianceid = compliances.ID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateDaily(nextDate, compliances.ID, complianceInstanceId, false);

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                if (nextDateMonth.Item1.Date.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                    complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                    complianceScheduleon.ScheduleOn = nextDateMonth.Item1;
                                    complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                    complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                    complianceScheduleon.IsActive = true;
                                    complianceScheduleon.IsUpcomingNotDeleted = true;
                                    if (isdocflag == 1)
                                    {
                                        complianceScheduleon.IsDocMan_NonMan = true;
                                    }
                                    else
                                    {
                                        complianceScheduleon.IsDocMan_NonMan = false;
                                    }
                                    entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                    entities.SaveChanges();
                                    nextDate = nextDateMonth.Item1;

                                    ComplianceTransaction transaction = new ComplianceTransaction()
                                    {
                                        ComplianceInstanceId = complianceInstanceId,
                                        ComplianceScheduleOnID = complianceScheduleon.ID,
                                        CreatedBy = createdByID,
                                        CreatedByText = creatdByName,
                                        StatusId = 1,
                                        Remarks = "New compliance assigned."
                                    };

                                    ComplianceManagement.CreateTransaction(transaction);
                                }
                                else
                                {
                                    if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        nextDate = nextDateMonth.Item1.AddDays(1);
                                    }
                                    else
                                    {
                                        nextDate = nextDateMonth.Item1;
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (compliances.Frequency == 8)
                        {
                            #region Frequancy Weekly
                            CreateScheduleOnErroComplianceid = compliances.ID;
                            CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                            nextDateMonth = GetNextDateWeekly(nextDate, compliances.ID, complianceInstanceId, Convert.ToInt32(compliances.DueWeekDay));

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic 
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime dtNewScheduleOn;
                                if (onloff == 1) //online
                                {
                                    dtNewScheduleOn = nextDateMonth.Item1;
                                    if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                    }
                                }
                                else
                                {
                                    //offline
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            dtNewScheduleOn = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        dtNewScheduleOn = nextDateMonth.Item1;
                                        if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            dtNewScheduleOn = dtNewScheduleOn.AddDays(1);
                                        }
                                    }
                                }
                                #endregion
                                //DateTime dtNewScheduleOn;
                                //if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                //{
                                //    dtNewScheduleOn = nextDateMonth.Item1.Date.AddDays(1);
                                //}
                                //else
                                //{
                                //    dtNewScheduleOn = nextDateMonth.Item1.Date;
                                //}

                                //Complinace Start Date logic
                                DateTime dtStart;
                                if (compliances.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(compliances.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }
                                //if (dtStart <= dtNewScheduleOn)
                                //{
                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = dtNewScheduleOn; // nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                if (isdocflag == 1)
                                {
                                    complianceScheduleon.IsDocMan_NonMan = true;
                                }
                                else
                                {
                                    complianceScheduleon.IsDocMan_NonMan = false;
                                }
                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                if (dtNewScheduleOn.Date.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    nextDate = dtNewScheduleOn.AddDays(1);
                                }
                                else
                                {
                                    nextDate = nextDateMonth.Item1;
                                }
                                //if (nextDateMonth.Item1.Date.DayOfWeek == DayOfWeek.Sunday)
                                //{
                                //    nextDate = nextDateMonth.Item1.AddDays(1);
                                //}
                                //else
                                //{
                                //    nextDate = nextDateMonth.Item1;
                                //}   


                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                ComplianceManagement.CreateTransaction(transaction);
                                //}
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Frequancy 
                            if (compliances.SubComplianceType == 0)
                            {
                                nextDateMonth = new Tuple<DateTime, string, long>(nextDate.AddDays(Convert.ToDouble(compliances.DueDate)), "", 0);
                            }
                            else if (compliances.SubComplianceType == 1 || compliances.SubComplianceType == 2)
                            {
                                CreateScheduleOnErroComplianceid = compliances.ID;
                                nextDateMonth = GetNextDatePeriodically(nextDate, compliances.ID, complianceInstanceId);
                            }
                            else
                            {
                                if (compliances.ScheduleType == 1) // Inbetween
                                {
                                    CreateScheduleOnErroComplianceid = compliances.ID;
                                    CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                                    var complianceSchedule = GetScheduleByComplianceID(compliances.ID);
                                    var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(complianceInstanceId);
                                    if (lastSchedule != null)
                                    {
                                        nextDateMonth = NewScheduleInbetweenLogicStatutory.GetNextDate(lastSchedule.ScheduleOn, compliances.ID, complianceInstanceId, complianceSchedule, (int)compliances.Frequency, lastSchedule, nextDate);
                                    }
                                    else
                                    {
                                        nextDateMonth = NewScheduleInbetweenLogicStatutory.GetNextDate(nextDate, compliances.ID, complianceInstanceId, complianceSchedule, (int)compliances.Frequency, lastSchedule, nextDate);
                                    }
                                }
                                else if (compliances.ScheduleType == 2) //Before
                                {
                                    CreateScheduleOnErroComplianceid = compliances.ID;
                                    CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                                    var complianceSchedule = GetScheduleByComplianceID(compliances.ID);
                                    var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(complianceInstanceId);
                                    if (lastSchedule != null)
                                    {
                                        nextDateMonth = NewSchedulePreLogicStatutory.GetNextDate(lastSchedule.ScheduleOn, compliances.ID, complianceInstanceId, complianceSchedule, (int)compliances.Frequency, lastSchedule, nextDate);
                                    }
                                    else
                                    {
                                        nextDateMonth = NewSchedulePreLogicStatutory.GetNextDate(nextDate, compliances.ID, complianceInstanceId, complianceSchedule, (int)compliances.Frequency, lastSchedule, nextDate);
                                    }
                                }
                                else
                                {
                                    CreateScheduleOnErroComplianceid = compliances.ID;
                                    CreateScheduleOnErrocomplianceInstanceId = complianceInstanceId;
                                    nextDateMonth = GetNextDate(nextDate, compliances.ID, complianceInstanceId, false);
                                }
                            }

                            string RLCSMonth = string.Empty;
                            string RLCSYear = string.Empty;

                            if (compliances.Frequency != null)
                            {
                                var tupleRLCSMonthYear = Get_RLCSMonthYear(Convert.ToInt32(compliances.Frequency), nextDateMonth.Item2);

                                if (tupleRLCSMonthYear != null)
                                {
                                    RLCSMonth = tupleRLCSMonthYear.Item1;
                                    RLCSYear = tupleRLCSMonthYear.Item2;
                                }
                            }

                            long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                                 where row.ComplianceInstanceID == complianceInstanceId && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                                 select row.ID).SingleOrDefault();

                            if (ScheduleOnID <= 0)
                            {
                                #region Holiday Logic
                                //DateTime ActuScheduleOn = new DateTime();
                                DateTime? ActuScheduleOn = null;
                                DateTime ScheduleOn1;
                                if (onloff == 1) //online
                                {
                                    ScheduleOn1 = nextDateMonth.Item1;
                                }
                                else
                                {
                                    //offline                                    
                                    var HolidayCount = CheckHoliday(compliances.ID, complianceInstanceId, nextDateMonth.Item1, beforafter, onloff);
                                    if (HolidayCount > 0)
                                    {
                                        if (beforafter == 1)
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(-HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                        else
                                        {
                                            ScheduleOn1 = nextDateMonth.Item1.AddDays(HolidayCount);
                                            ActuScheduleOn = nextDateMonth.Item1.Date;
                                        }
                                    }
                                    else
                                    {
                                        ScheduleOn1 = nextDateMonth.Item1;
                                    }
                                }
                                #endregion
                                //Complinace Start Date logic
                                DateTime dtStart;
                                if (compliances.StartDate != null)
                                {
                                    dtStart = Convert.ToDateTime(compliances.StartDate);
                                }
                                else
                                {
                                    dtStart = new DateTime(2018, 1, 1);
                                }

                                ComplianceScheduleOn complianceScheduleon = new ComplianceScheduleOn();
                                complianceScheduleon.ComplianceInstanceID = complianceInstanceId;
                                complianceScheduleon.ScheduleOn = ScheduleOn1;// nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.IsUpcomingNotDeleted = true;
                                complianceScheduleon.ActualScheduleon = ActuScheduleOn;
                                if (!string.IsNullOrEmpty(RLCSMonth))
                                    complianceScheduleon.RLCS_PayrollMonth = RLCSMonth;

                                if (!string.IsNullOrEmpty(RLCSYear))
                                    complianceScheduleon.RLCS_PayrollYear = RLCSYear;

                                complianceScheduleon.RLCS_ActivityEndDate = nextDateMonth.Item1;
                                if (isdocflag == 1)
                                {
                                    complianceScheduleon.IsDocMan_NonMan = true;
                                }
                                else
                                {
                                    complianceScheduleon.IsDocMan_NonMan = false;
                                }
                                entities.ComplianceScheduleOns.Add(complianceScheduleon);
                                entities.SaveChanges();

                                nextDate = nextDateMonth.Item1;

                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = complianceInstanceId,
                                    ComplianceScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                ComplianceManagement.CreateTransaction(transaction);

                                #region Task Scheduele
                                long CustomerBranchID = GetCustomerBranchID(complianceInstanceId);
                                var Taskapplicable = TaskManagment.TaskApplicable(CustomerBranchID);
                                if (Taskapplicable != null)
                                {

                                    if (Taskapplicable == 1)
                                    {
                                        var TaskIDList = (from row in entities.Tasks
                                                          join row1 in entities.TaskComplianceMappings
                                                          on row.ID equals row1.TaskID
                                                          where row1.ComplianceID == compliances.ID
                                                          && row.TaskType == 1 && row.ParentID == null
                                                          && (((row.Status != "A") || (row.Status == null))
                                                          || ((row.Status == "A") && (row.DeactivateOn > DateTime.Now)))
                                                          && row.Isdeleted == false && row.IsActive == true
                                                          select row.ID).ToList();

                                        if (TaskIDList.Count > 0)
                                        {
                                            TaskIDList.ForEach(entrytask =>
                                            {
                                                List<TaskNameValue> TaskSubTaskList = new List<TaskNameValue>();

                                                bool? IsAfter = TaskManagment.GetTaskBeforeAfter(entrytask);

                                                if (IsAfter == null)
                                                {
                                                    IsAfter = false;
                                                }

                                                var taskSubTaskList = (from row in entities.Tasks
                                                                       join row1 in entities.TaskInstances
                                                                       on row.ID equals row1.TaskId
                                                                       where row.MainTaskID == entrytask && row1.CustomerBranchID == CustomerBranchID
                                                                       select new { row.ID, row.ParentID, row.DueDays }).ToList();

                                                taskSubTaskList.ForEach(entrysubtask =>
                                                {
                                                    TaskSubTaskList.Add(new TaskNameValue() { TaskID = (long)entrysubtask.ID, ParentID = (int?)entrysubtask.ParentID, ActualDueDays = (int)entrysubtask.DueDays });

                                                });

                                                TaskSubTaskList.ForEach(entryTaskSubTaskList =>
                                                {

                                                    var taskInstances = (from row in entities.TaskInstances
                                                                         where row.IsDeleted == false && row.TaskId == entryTaskSubTaskList.TaskID
                                                                          && row.ScheduledOn <= nextDateMonth.Item1
                                                                          && row.CustomerBranchID == CustomerBranchID
                                                                         select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                                                    DateTime schedueon;
                                                    if (IsAfter == null)
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }
                                                    else
                                                    {
                                                        schedueon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                    }

                                                    taskInstances.ForEach(entrytaskinstance =>
                                                    {
                                                        TaskScheduleOn taskScheduleOn = new TaskScheduleOn();
                                                        taskScheduleOn.TaskInstanceID = entrytaskinstance.ID;
                                                        taskScheduleOn.ComplianceScheduleOnID = complianceScheduleon.ID;
                                                        taskScheduleOn.ForMonth = nextDateMonth.Item2;
                                                        taskScheduleOn.ForPeriod = nextDateMonth.Item3;
                                                        taskScheduleOn.ScheduleOn = schedueon; // nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                        taskScheduleOn.IsActive = true;
                                                        taskScheduleOn.IsUpcomingNotDeleted = true;
                                                        entities.TaskScheduleOns.Add(taskScheduleOn);
                                                        entities.SaveChanges();

                                                        var AssignedTaskRole = TaskManagment.GetAssignedTaskUsers((int)entrytaskinstance.ID);
                                                        var performerTaskRole = AssignedTaskRole.Where(en => en.RoleID == 3).FirstOrDefault();
                                                        if (performerTaskRole != null)
                                                        {
                                                            var user = UserManagement.GetByID((int)performerTaskRole.UserID);
                                                            TaskTransaction tasktransaction = new TaskTransaction()
                                                            {
                                                                TaskInstanceId = entrytaskinstance.ID,
                                                                TaskScheduleOnID = taskScheduleOn.ID,
                                                                ComplianceScheduleOnID = complianceScheduleon.ID,
                                                                CreatedBy = performerTaskRole.UserID,
                                                                CreatedByText = user.FirstName + " " + user.LastName,
                                                                StatusId = 1,
                                                                Remarks = "New task assigned."
                                                            };

                                                            TaskManagment.CreateTaskTransaction(tasktransaction);

                                                            foreach (var roles in AssignedTaskRole)
                                                            {
                                                                if (roles.RoleID != 6)
                                                                {
                                                                    DateTime schdulon;
                                                                    if (IsAfter == true)
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    else
                                                                    {
                                                                        schdulon = nextDateMonth.Item1.AddDays(-Convert.ToInt64(entryTaskSubTaskList.ActualDueDays));
                                                                    }
                                                                    TaskManagment.CreateTaskReminders(entrytaskinstance.TaskId, compliances.ID, roles.ID, schdulon, complianceScheduleon.ID, IsAfter);
                                                                }
                                                            }
                                                        }

                                                    });
                                                });
                                            });
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO), null,
                //    "Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);
                ComplianceManagement.SendgridSenEmail("Error Occured as CreateScheduleOn Function", "CreateScheduleOn" + "Complianceid=" + CreateScheduleOnErroComplianceid + "complianceInstanceId=" + CreateScheduleOnErrocomplianceInstanceId);

            }
        }
        public static bool ComplianceIsEventBased(long complianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.Compliances
                                      where row.ID == complianceId
                                      select row).FirstOrDefault();

                if (ComplianceData.EventFlag == null)
                    return true;
                else
                    return false;
            }
        }
        public static bool ComplianceIsActionable(long complianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceData = (from row in entities.Compliances
                                      where row.ID == complianceId
                                      select row).FirstOrDefault();

                if (ComplianceData.ComplinceVisible == true)
                    return true;
                else
                    return false;
            }
        }
      
        private static void CreateReminders(long complianceID, long ComplianceInstanceId, int ComplianceAssignmentId)
        {
            long CreateRemindersErrorcomplianceInstanceID = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var compliance = (from row in entities.Compliances
                                      where row.ID == complianceID
                                      select row).FirstOrDefault();

                    if (compliance.Frequency == 7)
                    {
                        #region Frequency Daily
                        List<ComplianceScheduleOn> ScheduledOnList = (from row in entities.ComplianceScheduleOns
                                                                      where row.ComplianceInstanceID == ComplianceInstanceId && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                      select row).ToList();

                        CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;

                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                        {
                            ComplianceReminder reminder = new ComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = sco.ScheduleOn,
                                RemindOn = sco.ScheduleOn.Date,
                            };
                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                            entities.ComplianceReminders.Add(reminder);
                        }
                        #endregion
                    }
                    else if (compliance.Frequency == 8)
                    {
                        #region Frequency Weekly
                        List<ComplianceScheduleOn> ScheduledOnList = (from row in entities.ComplianceScheduleOns
                                                                      where row.ComplianceInstanceID == ComplianceInstanceId
                                                                      && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                      select row).ToList();

                        CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;
                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                        {
                            ComplianceReminder reminder = new ComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = sco.ScheduleOn,
                                RemindOn = sco.ScheduleOn.Date,
                            };
                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                            entities.ComplianceReminders.Add(reminder);

                            ComplianceReminder reminder1 = new ComplianceReminder()
                            {
                                ComplianceAssignmentID = ComplianceAssignmentId,
                                ReminderTemplateID = 1,
                                ComplianceDueDate = sco.ScheduleOn,
                                RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 2),
                            };
                            reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled); //updated by Rahul on 26 April 2016
                            entities.ComplianceReminders.Add(reminder1);
                        }
                        #endregion
                    }
                    else
                    {
                        List<long> getdays = GetUpcomingReminderDaysDetail(ComplianceAssignmentId, Convert.ToInt32(compliance.Frequency), "S");

                        List<ComplianceScheduleOn> ScheduledOnList = (from row in entities.ComplianceScheduleOns
                                                                      where row.ComplianceInstanceID == ComplianceInstanceId && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                                      select row).ToList();


                        if (getdays.Count > 0)
                        {
                            foreach (ComplianceScheduleOn sco in ScheduledOnList)
                            {
                                foreach (var item in getdays)
                                {
                                    ComplianceReminder reminder1 = new ComplianceReminder()
                                    {
                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                        ReminderTemplateID = 1,
                                        ComplianceDueDate = sco.ScheduleOn,
                                        RemindOn = sco.ScheduleOn.Date.AddDays(-item),
                                    };
                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                    entities.ComplianceReminders.Add(reminder1);
                                }
                            }
                        }
                        else
                        {
                            #region Normal Frequency
                            //Time Based
                            if (compliance.SubComplianceType == 0)
                            {
                                //List<ComplianceScheduleOn> ScheduledOnList = (from row in entities.ComplianceScheduleOns
                                //                                              where row.ComplianceInstanceID == ComplianceInstanceId
                                //                                              && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                //                                              select row).ToList();

                                CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;
                                //Standard
                                if (compliance.ReminderType == 0)
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 2),
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);

                                            ComplianceReminder reminder1 = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 15),
                                            };
                                            reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled); //updated by Rahul on 26 April 2016
                                            entities.ComplianceReminders.Add(reminder1);
                                        }
                                    }
                                }
                                else//Custom
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        ScheduledOnList.ForEach(entry =>
                                        {
                                            DateTime TempScheduled = entry.ScheduleOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));

                                            while (TempScheduled.Date < entry.ScheduleOn)
                                            {
                                                ComplianceReminder reminder = new ComplianceReminder()
                                                {
                                                    ComplianceAssignmentID = ComplianceAssignmentId,
                                                    ReminderTemplateID = 1,
                                                    ComplianceDueDate = entry.ScheduleOn,
                                                    RemindOn = TempScheduled.Date,
                                                };

                                                reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                                entities.ComplianceReminders.Add(reminder);

                                                TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                            }
                                        });
                                    }
                                }

                            }//Function Based Start
                            else
                            {
                                CreateRemindersErrorcomplianceInstanceID = ComplianceInstanceId;
                                if (compliance.ReminderType == 0)
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 0)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 1)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = sco.ScheduleOn,
                                                RemindOn = sco.ScheduleOn.Date,
                                            };
                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                        }
                                    }
                                    else
                                    {
                                        if (compliance.Frequency.HasValue)
                                        {
                                            var reminders = (from row in entities.ReminderTemplates
                                                             where row.Frequency == compliance.Frequency.Value && row.IsSubscribed == true
                                                             select row).ToList();

                                            foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                            {
                                                reminders.ForEach(day =>
                                                {
                                                    ComplianceReminder reminder = new ComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = day.ID,
                                                        ComplianceDueDate = sco.ScheduleOn,
                                                        RemindOn = sco.ScheduleOn.Date.AddDays(-1 * day.TimeInDays),
                                                    };

                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                                    entities.ComplianceReminders.Add(reminder);

                                                });
                                            }
                                        }
                                        else //added by rahul on 16 FEB 2016 for Event
                                        {
                                            if (compliance.ComplianceType == 0 && compliance.EventID != null)
                                            {
                                                foreach (ComplianceScheduleOn sco in ScheduledOnList)
                                                {
                                                    ComplianceReminder reminder = new ComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = 1,
                                                        ComplianceDueDate = sco.ScheduleOn,
                                                        RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 2),
                                                    };
                                                    reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                    entities.ComplianceReminders.Add(reminder);

                                                    ComplianceReminder reminder1 = new ComplianceReminder()
                                                    {
                                                        ComplianceAssignmentID = ComplianceAssignmentId,
                                                        ReminderTemplateID = 1,
                                                        ComplianceDueDate = sco.ScheduleOn,
                                                        RemindOn = sco.ScheduleOn.Date.AddDays(-1 * 15),
                                                    };
                                                    reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                    entities.ComplianceReminders.Add(reminder1);
                                                }
                                            }
                                        } //added by rahul on 16 FEB 2016 for Event
                                    }//Added by rahul on 4 FEB 2016
                                }
                                else
                                {

                                    ScheduledOnList.ForEach(entry =>
                                    {
                                        DateTime TempScheduled = entry.ScheduleOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));

                                        while (TempScheduled.Date < entry.ScheduleOn)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = entry.ScheduleOn,
                                                RemindOn = TempScheduled.Date,
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                            entities.ComplianceReminders.Add(reminder);

                                            TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                        }

                                    });
                                }
                            }
                            #endregion
                        }
                    }
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateReminders Function", "CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateReminders Function", "CreateReminders" + "complianceInstanceID" + CreateRemindersErrorcomplianceInstanceID);


            }
        }
        public static List<long> GetUpcomingReminderDaysDetail(long ComplianceAssignmentId, int FreqID, string Type)
        {
            List<long> output = new List<long>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                User User = (from row in entities.Users
                             join row1 in entities.ComplianceAssignments
                             on row.ID equals row1.UserID
                             where row1.ID == ComplianceAssignmentId
                             && row.IsActive == true
                             && row.IsDeleted == false
                             select row).FirstOrDefault();
                if (User != null)
                {
                    long customerID = Convert.ToInt32(User.CustomerID);
                    long UserID = Convert.ToInt32(User.ID);

                    var UserObj = (from row in entities.ReminderTemplate_UserMapping
                                   where row.CustomerID == customerID
                                   && row.UserID == UserID
                                   && row.IsActive == true
                                   && row.Type == Type
                                   && row.Frequency == FreqID
                                   && row.Flag == "UN"
                                   select row).ToList();

                    if (UserObj.Count > 0)
                    {
                        output.Clear();
                        output = UserObj.Select(x => x.TimeInDays).ToList();
                    }
                    else
                    {
                        var CustObj = (from row in entities.ReminderTemplate_CustomerMapping
                                       where row.CustomerID == customerID
                                       && row.IsActive == true
                                       && row.Type == Type
                                       && row.Frequency == FreqID
                                       && row.Flag == "UN"
                                       select row).ToList();

                        if (CustObj.Count > 0)
                        {
                            output.Clear();
                            output = CustObj.Select(x => x.TimeInDays).ToList();
                        }
                        else
                        {
                            output.Clear();
                            //var StdObj = (from row in entities.ReminderTemplates
                            //              where row.Frequency == FreqID
                            //              && row.IsSubscribed == true
                            //              select row).ToList();

                            //if (StdObj.Count > 0)
                            //{
                            //    output.Clear();
                            //    output = StdObj.Select(x => Convert.ToInt64(x.TimeInDays)).ToList();
                            //}
                        }
                    }
                }
                return output;
            }
        }
        public static void CreateReminders(long complianceID, long ComplianceInstanceId, int ComplianceAssignmentId, DateTime scheduledOn)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    Compliance compliance = (from row in entities.Compliances
                                             where row.ID == complianceID
                                             select row).FirstOrDefault();

                    if (compliance.Frequency == 7)
                    {
                        #region Frequency Daily
                        ComplianceReminder reminder = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date,
                        };
                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder);
                        #endregion
                    }
                    else if (compliance.Frequency == 8)
                    {
                        #region Frequency Weekly
                        ComplianceReminder reminder = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date,
                        };
                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder);
                        ComplianceReminder reminder1 = new ComplianceReminder()
                        {
                            ComplianceAssignmentID = ComplianceAssignmentId,
                            ReminderTemplateID = 1,
                            ComplianceDueDate = scheduledOn,
                            RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                        };
                        reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                        entities.ComplianceReminders.Add(reminder1);
                        #endregion
                    }
                    else
                    {

                        List<long> getdays = GetUpcomingReminderDaysDetail(ComplianceAssignmentId, Convert.ToInt32(compliance.Frequency), "S");
                        if (getdays.Count > 0)
                        {
                            foreach (var item in getdays)
                            {
                                ComplianceReminder reminder1 = new ComplianceReminder()
                                {
                                    ComplianceAssignmentID = ComplianceAssignmentId,
                                    ReminderTemplateID = 1,
                                    ComplianceDueDate = scheduledOn,
                                    RemindOn = scheduledOn.Date.AddDays(-item),
                                };
                                reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                entities.ComplianceReminders.Add(reminder1);
                            }
                        }
                        else
                        {
                            #region Normal Frequency
                            if (compliance.SubComplianceType == 0)
                            {
                                if (compliance.ReminderType == 0)
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        ComplianceReminder reminder = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date,
                                        };
                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.ComplianceReminders.Add(reminder);
                                    }//added by rahul on 4 FEB 2016
                                    else
                                    {
                                        ComplianceReminder reminder = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                                        };
                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.ComplianceReminders.Add(reminder);


                                        ComplianceReminder reminder1 = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                                        };
                                        reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.ComplianceReminders.Add(reminder1);
                                    }
                                }
                                else
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {
                                        ComplianceReminder reminder = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date,
                                        };
                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.ComplianceReminders.Add(reminder);
                                    }//added by rahul on 4 FEB 2016
                                    else
                                    {
                                        DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));
                                        while (TempScheduled.Date < scheduledOn)
                                        {
                                            ComplianceReminder reminder = new ComplianceReminder()
                                            {
                                                ComplianceAssignmentID = ComplianceAssignmentId,
                                                ReminderTemplateID = 1,
                                                ComplianceDueDate = scheduledOn,
                                                RemindOn = TempScheduled.Date,
                                            };

                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                            entities.ComplianceReminders.Add(reminder);
                                            TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                        }//added by rahul on 8 FEB 2016
                                    }
                                }
                            }
                            else
                            {

                                if (compliance.ReminderType == 0)
                                {
                                    if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 0)
                                    {

                                        ComplianceReminder reminder = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date,
                                        };
                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.ComplianceReminders.Add(reminder);
                                    }
                                    else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 1)
                                    {

                                        ComplianceReminder reminder = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date,
                                        };
                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.ComplianceReminders.Add(reminder);

                                    }
                                    else if (compliance.ComplianceType == 1 && compliance.CheckListTypeID == 2)
                                    {

                                        ComplianceReminder reminder = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = scheduledOn.Date,
                                        };
                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                        entities.ComplianceReminders.Add(reminder);

                                    }
                                    else
                                    {
                                        if (compliance.Frequency.HasValue)
                                        {
                                            var reminders = (from row in entities.ReminderTemplates
                                                             where row.Frequency == compliance.Frequency.Value && row.IsSubscribed == true
                                                             select row).ToList();

                                            reminders.ForEach(day =>
                                            {
                                                ComplianceReminder reminder = new ComplianceReminder()
                                                {
                                                    ComplianceAssignmentID = ComplianceAssignmentId,
                                                    ReminderTemplateID = day.ID,
                                                    ComplianceDueDate = scheduledOn,
                                                    RemindOn = scheduledOn.Date.AddDays(-1 * day.TimeInDays),
                                                };

                                                reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                                entities.ComplianceReminders.Add(reminder);

                                            });
                                        }
                                        else//added by rahul on 16 FEB 2016 for Event
                                        {
                                            if (compliance.ComplianceType == 0 && compliance.EventID != null)
                                            {
                                                ComplianceReminder reminder = new ComplianceReminder()
                                                {
                                                    ComplianceAssignmentID = ComplianceAssignmentId,
                                                    ReminderTemplateID = 1,
                                                    ComplianceDueDate = scheduledOn,
                                                    RemindOn = scheduledOn.Date.AddDays(-1 * 2),
                                                };
                                                reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                entities.ComplianceReminders.Add(reminder);


                                                ComplianceReminder reminder1 = new ComplianceReminder()
                                                {
                                                    ComplianceAssignmentID = ComplianceAssignmentId,
                                                    ReminderTemplateID = 1,
                                                    ComplianceDueDate = scheduledOn,
                                                    RemindOn = scheduledOn.Date.AddDays(-1 * 15),
                                                };
                                                reminder1.Status = (byte)(reminder1.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                entities.ComplianceReminders.Add(reminder1);
                                            }
                                        }//added by rahul on 16 FEB 2016 for Event
                                    }
                                }
                                else
                                {
                                    DateTime TempScheduled = scheduledOn.AddDays(-(Convert.ToDouble(compliance.ReminderBefore)));
                                    while (TempScheduled.Date < scheduledOn)
                                    {
                                        ComplianceReminder reminder = new ComplianceReminder()
                                        {
                                            ComplianceAssignmentID = ComplianceAssignmentId,
                                            ReminderTemplateID = 1,
                                            ComplianceDueDate = scheduledOn,
                                            RemindOn = TempScheduled.Date,
                                        };

                                        reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);

                                        entities.ComplianceReminders.Add(reminder);

                                        TempScheduled = TempScheduled.AddDays(Convert.ToDouble(compliance.ReminderGap));
                                    }
                                }
                            }
                            #endregion
                        }

                    }
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();

                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                //msg.FunctionName = "CreateReminders";
                msg.FunctionName = "CreateReminders" + "ComplianceAssignmentId" + ComplianceAssignmentId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateReminders Function", "CreateReminders" + "ComplianceAssignmentId" + ComplianceAssignmentId);

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateReminders Function", "CreateReminders" + "ComplianceAssignmentId" + ComplianceAssignmentId);


            }
        }
        public static ComplianceInstance GetComplianceInstance(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.ComplianceInstances
                                   where row.ID == ComplianceInstanceID
                                   && row.IsDeleted == false
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }
        public static long GetScheduledOnPresentOrNot(long ComplianceInstanceID, DateTime ScheduledOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long ScheduleOnID = (from row in entities.ComplianceScheduleOns
                                     where row.ComplianceInstanceID == ComplianceInstanceID && row.ScheduleOn == ScheduledOn
                                     && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                     select row.ID).FirstOrDefault();
                return ScheduleOnID;
            }
        }
        public static DateTime? GetComplianceInstanceScheduledon(long ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime ScheduleOnID = (from row in entities.ComplianceInstances
                                         where row.ID == ComplianceInstanceID
                                         select row.ScheduledOn).FirstOrDefault();
                if (ScheduleOnID != null)
                {
                    return ScheduleOnID;
                }
                else
                {
                    return null;
                }

            }
        }
        public static List<ComplianceSchedule> GetScheduleByComplianceID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceSchedules
                                    where row.ComplianceID == complianceID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
        public static long GetNextMonthFromFrequency(long LastforMonth, byte? Frequency)
        {
            long NextPeriod;
            switch (Frequency)
            {
                case 0:
                    if (LastforMonth == 12)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 1;
                    break;

                case 1:
                    if (LastforMonth == 10)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 3;
                    break;

                case 2:
                    if (LastforMonth == 7)
                        NextPeriod = 1;
                    else if (LastforMonth == 10)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 6;
                    break;

                case 3:
                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 4:

                    if (LastforMonth == 9)
                        NextPeriod = 1;
                    else if (LastforMonth == 12)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 4;

                    break;
                case 5:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 6:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                default:
                    NextPeriod = 0;
                    break;
            }

            return NextPeriod;
        }
        public static long GetLastPeriodFromDate(int forMonth, byte? Frequency)
        {
            long forMonthName = 0;
            switch (Frequency)
            {
                case 0:
                    if (forMonth == 1)
                    {
                        forMonthName = 12;
                    }
                    else
                    {
                        forMonthName = forMonth - 1;
                    }
                    break;
                case 1:
                    if (forMonth == 1)
                    {
                        forMonthName = 10;
                    }
                    else
                    {
                        forMonthName = forMonth - 3;
                    }
                    break;
                case 2:
                    if (forMonth == 1)
                    {
                        forMonthName = 7;
                    }
                    else
                    {
                        forMonthName = forMonth - 6;
                    }
                    break;

                case 3:
                    forMonthName = 1;
                    break;

                case 4:

                    if (forMonth == 1)
                    {
                        forMonthName = 9;
                    }
                    else
                    {
                        forMonthName = forMonthName - 4;
                    }

                    break;
                case 5:
                    forMonthName = 1;
                    break;
                case 6:

                    forMonthName = 1;
                    break;

                default:
                    forMonthName = 0;
                    break;
            }

            return forMonthName;
        }
        public static Compliance GetByID(long complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var compliance = (from row in entities.Compliances.Include("ComplianceParameters")
                                  where row.ID == complianceID
                                  select row).SingleOrDefault();

                return compliance;
            }
        }
        public static Tuple<DateTime, string, long> GetEffectiveNextDate(DateTime scheduledOn, long complianceID, long ComplianceInstanceID)
        {
            var complianceSchedule = GetScheduleByComplianceID(complianceID);
            var objCompliance = GetByID(complianceID);
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduleOn.Month), objCompliance.Frequency);
                }
            }
            else
            {
                #region Monthly
                if (objCompliance.Frequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (objCompliance.Frequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (objCompliance.Frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (objCompliance.Frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (objCompliance.Frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (objCompliance.Frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            if (objCompliance.Frequency != 0 || objCompliance.Frequency != 1)
            {
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        //complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if (objCompliance.Frequency == 3)
                        //complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.Frequency == 0 || objCompliance.Frequency == 1)
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        else
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                }
                //Added by Rahul on 5 Jan 2015
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03" || row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.Frequency == 0 || objCompliance.Frequency == 1)
                        {
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                            {
                                if (objCompliance.Frequency == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03" || entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                //Added by Rahul on 15 Nov 2016
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" || row.SpecialDate.Substring(2, 2) == "11").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05" || entry.SpecialDate.Substring(2, 2) == "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.Frequency == 0 || objCompliance.Frequency == 1)
                        {
                            if (objCompliance.Frequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
            }
            else
            {
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();

                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                }
            }
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (objCompliance.Frequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    //ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.Frequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {

                    //ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);

                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.Frequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
                else
                {
                    //ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);

                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.Frequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    // ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);

                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.Frequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    // ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else if (objCompliance.Frequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }

                    }
                }
                else
                {
                    //ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                    if (complianceScheduleDates.Count > 0)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                    }
                }
            }
            else
            {
                //ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                // Comment by rahul on 6 JUNE 2017 In Case Of Effective Date Change
                if (complianceScheduleDates.Count > 0)
                {
                    complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                    complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                    ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                }
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (objCompliance.Frequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;

                        // complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        //date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList(); //orderby row.ScheduleOn descending
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        //complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.Frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.Frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.Frequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }
        public static Tuple<DateTime, string, long> GetNextDate(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, bool iseffectivedate)
        {
            var complianceSchedule = GetScheduleByComplianceID(complianceID);
            var objCompliance = GetByID(complianceID);
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    if (iseffectivedate)
                    {
                        #region Monthly
                        if (objCompliance.Frequency == 0)
                        {
                            lastPeriod = scheduledOn.Month - 1;
                        }
                        #endregion
                        #region   Half Yearly
                        else if (objCompliance.Frequency == 2)
                        {
                            if (complianceSchedule[0].ForMonth == 1)
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Annaully
                        else if (objCompliance.Frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {

                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 2)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 4;
                                    }
                                }

                            }
                        }
                        #endregion
                        #region Quarterly
                        else if (objCompliance.Frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region FourMonthly
                        else if (objCompliance.Frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Two Yearly
                        else if (objCompliance.Frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                }
                            }
                        }
                        #endregion
                        else
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {
                                    lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }

                    }
                    else
                    {
                        lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                    }
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduleOn.Month), objCompliance.Frequency);
                }
            }
            else
            {
                #region Monthly
                if (objCompliance.Frequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (objCompliance.Frequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (objCompliance.Frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (objCompliance.Frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (objCompliance.Frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (objCompliance.Frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
         
            if (objCompliance.Frequency != 0 || objCompliance.Frequency != 1)
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (objCompliance.Frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                #endregion

                #region Second Part
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (objCompliance.Frequency == 1)
                        {

                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 10).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" && entry.ForMonth == 10).Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            if (objCompliance.Frequency == 0)
                            {

                                if (lastPeriod == 11)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                if (lastPeriod == 7)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.SpecialDate.Substring(0, 2) == "10" && row.ForMonth == 8).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                            if (objCompliance.Frequency == 2)
                            {
                                if (lastPeriod == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else if (lastPeriod != 10)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "07" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        if (scheduledOn.Year <= DateTime.Today.Year - 1)
                                        {
                                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                        }
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {

                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                                    }
                                }
                            }
                            if (objCompliance.Frequency == 4)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 9).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                //if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                //{
                                //    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                //}
                                if (lastPeriod == 9)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }

                            }
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02"
                || row.SpecialDate.Substring(2, 2) == "03"
                || row.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "07"
                || row.SpecialDate.Substring(2, 2) == "08"
                || row.SpecialDate.Substring(2, 2) == "09"
                || row.SpecialDate.Substring(2, 2) == "10"
                || row.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 3)
                    {
                        complianceScheduleDates.Add(complianceSchedule.Where(entry =>
                        entry.SpecialDate.Substring(2, 2) == "02"
                        || entry.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "07"
                        || entry.SpecialDate.Substring(2, 2) == "08"
                        || entry.SpecialDate.Substring(2, 2) == "09"
                        || entry.SpecialDate.Substring(2, 2) == "10"
                        || entry.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                    if (objCompliance.Frequency == 2)
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "08").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "08").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                    else
                    {
                        if (objCompliance.Frequency == 0)
                        {
                            if (lastPeriod == 11)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else if (objCompliance.Frequency == 1)
                        {
                            if (objCompliance.Frequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && objCompliance.Frequency == 3).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && objCompliance.Frequency == 2).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (objCompliance.Frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (objCompliance.Frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (objCompliance.Frequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else if (objCompliance.Frequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, objCompliance.Frequency);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (objCompliance.Frequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (objCompliance.Frequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.Frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.Frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), objCompliance.Frequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }
        public static Tuple<DateTime, string, long> GetNextDateWithClientBased(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, bool iseffectivedate, int frequency, long customerid)
        {
            //var complianceSchedule = GetScheduleByClientFrequencyComplianceID(complianceID, customerid);
            var complianceSchedule = GetScheduleByCustomerwiseComplianceID(complianceID, customerid);
            //var objCompliance = GetByID(complianceID);
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            long lastPeriod = 0;
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    if (iseffectivedate)
                    {
                        #region Monthly
                        if (frequency == 0)
                        {
                            lastPeriod = scheduledOn.Month - 1;
                        }
                        #endregion
                        #region   Half Yearly
                        else if (frequency == 2)
                        {
                            if (complianceSchedule[0].ForMonth == 1)
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                        {
                                            lastPeriod = 1;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                        {
                                            lastPeriod = 7;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                        {
                                            lastPeriod = 4;
                                        }
                                        else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                        {
                                            lastPeriod = 10;
                                        }
                                        else
                                        {
                                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Annaully
                        else if (frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {

                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 2)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 4;
                                    }
                                }

                            }
                        }
                        #endregion
                        #region Quarterly
                        else if (frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 10;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                                    {
                                        lastPeriod = 7;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region FourMonthly
                        else if (frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {

                                    lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                }
                                else
                                {
                                    if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                                    {
                                        lastPeriod = 5;
                                    }
                                    else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                    {
                                        lastPeriod = 9;
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Two Yearly
                        else if (frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (complianceSchedule[0].ForMonth == 1)
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }
                                    if (lastPeriod == 4)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 10)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 11)
                                    {
                                        lastPeriod = 1;
                                    }
                                    if (lastPeriod == 12)
                                    {
                                        lastPeriod = 1;
                                    }
                                }
                                else
                                {
                                    if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                    {
                                        lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                                    }
                                    else
                                    {
                                        lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                    }

                                    if (lastPeriod == 1)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 3)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 5)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 6)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 7)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 8)
                                    {
                                        lastPeriod = 4;
                                    }
                                    if (lastPeriod == 9)
                                    {
                                        lastPeriod = 4;
                                    }
                                }
                            }
                        }
                        #endregion
                        else
                        {
                            if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                            {
                                if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                                {
                                    lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                    }
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(lastSchedule.ScheduleOn.Month), (byte)frequency);
                }
            }
            else
            {
                #region Monthly
                if (frequency == 0)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (frequency == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (frequency == 3)//updated by rahul on 22 april 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 2)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 4;
                            }
                        }

                    }
                }
                #endregion
                #region Quarterly
                else if (frequency == 1)//updated by rahul on 18 COT 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region FourMonthly
                else if (frequency == 4)//updated by rahul on 2 FEB 2017 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 4)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 6 && Convert.ToDateTime(getinstancescheduleondate).Month <= 8)
                            {
                                lastPeriod = 5;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 9;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Two Yearly
                else if (frequency == 5)//updated by rahul on 30 Nov 2016 for change in special date
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                            if (lastPeriod == 4)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 10)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 11)
                            {
                                lastPeriod = 1;
                            }
                            if (lastPeriod == 12)
                            {
                                lastPeriod = 1;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }

                            if (lastPeriod == 1)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 3)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 5)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 6)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 7)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 8)
                            {
                                lastPeriod = 4;
                            }
                            if (lastPeriod == 9)
                            {
                                lastPeriod = 4;
                            }
                        }
                    }
                }
                #endregion
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;//long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); comment by rahul on 5 Jan 2016
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            if (frequency != 0 || frequency != 1)
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if (frequency == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 0)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequency == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                #endregion

                #region Second Part
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (frequency == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (frequency == 1)
                        {

                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 10).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" && entry.ForMonth == 10).Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            if (frequency == 0)
                            {

                                if (lastPeriod == 11)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                if (lastPeriod == 7)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.SpecialDate.Substring(0, 2) == "10" && row.ForMonth == 8).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                            if (frequency == 2)
                            {
                                if (lastPeriod == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else if (lastPeriod != 10)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "07" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        if (scheduledOn.Year <= DateTime.Today.Year - 1)
                                        {
                                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                        }
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {

                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());

                                    }
                                }
                            }
                            if (frequency == 4)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 9).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                //if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                //{
                                //    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                //}
                                if (lastPeriod == 9)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }

                            }
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02"
                || row.SpecialDate.Substring(2, 2) == "03"
                || row.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "07"
                || row.SpecialDate.Substring(2, 2) == "08"
                || row.SpecialDate.Substring(2, 2) == "09"
                || row.SpecialDate.Substring(2, 2) == "10"
                || row.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (frequency == 3)
                    {
                        complianceScheduleDates.Add(complianceSchedule.Where(entry =>
                        entry.SpecialDate.Substring(2, 2) == "02"
                        || entry.SpecialDate.Substring(2, 2) == "06" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "07"
                        || entry.SpecialDate.Substring(2, 2) == "08"
                        || entry.SpecialDate.Substring(2, 2) == "09"
                        || entry.SpecialDate.Substring(2, 2) == "10"
                        || entry.SpecialDate.Substring(2, 2) == "11" // added by rahul on 11 June 2019
                        || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    }
                    if (frequency == 2)
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "08").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "08").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                    else
                    {
                        if (frequency == 0)
                        {
                            if (lastPeriod == 11)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else if (frequency == 1)
                        {
                            if (frequency == 1)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && frequency == 3).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && frequency == 2).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    if (frequency == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequency == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
          
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (frequency == 0)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else if (frequency == 5)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, (byte)frequency);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                //added by rahul on 7 Jan 2015 for Quarterly
                if (frequency == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 5)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequency == 0)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    //added by rahul on 6 OCT 2016
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), (byte)frequency);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }
        public static Tuple<DateTime, string, long> GetNextDatePeriodically(DateTime scheduledOn, long complianceID, long ComplianceInstanceID)
        {
            int formonthscheduleR = -1;
            var complianceSchedule = GetScheduleByComplianceID(complianceID);//.OrderBy(entry => entry.ForMonth).ToList();
            var objCompliance = GetByID(complianceID);
            DateTime date = complianceSchedule.Select(row => new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > scheduledOn).FirstOrDefault();
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            if (objCompliance.Frequency == 3 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (objCompliance.Frequency == 5 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }

            if (objCompliance.Frequency == 6 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (objCompliance.Frequency == 2)
            {
                List<Tuple<DateTime, long>> complianceScheduleDates;
                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "07").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "07").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" || row.SpecialDate.Substring(2, 2) == "10").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "10").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                long ActualForMonth = 0;
                if (objCompliance.Frequency == 2)
                {

                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(lastSchedule.ScheduleOn)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }

                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));

                    if (objCompliance.Frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }

                    if (objCompliance.Frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }
            string forMonth = string.Empty;

            if (complianceSchedule.Count > 0)
            {
                string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

                var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                formonthscheduleR = ForMonthSchedule.ForMonth;
            }


            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);


        }       
        public static Tuple<DateTime, string, long> GetNextDatePeriodicallyWithClientBased(DateTime scheduledOn, long complianceID, long ComplianceInstanceID, int frequency)
        {
            int formonthscheduleR = -1;
            var complianceSchedule = GetScheduleByComplianceID(complianceID);//.OrderBy(entry => entry.ForMonth).ToList();           
            DateTime date = complianceSchedule.Select(row => new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2)))).Where(row => row > scheduledOn).FirstOrDefault();
            var lastSchedule = ComplianceManagement.GetLastScheduleOnByInstance(ComplianceInstanceID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(ComplianceInstanceID);
            if (frequency == 3 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 5 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 6 && date != DateTime.MinValue)
            {
                date = new DateTime(scheduledOn.Year + 6, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
            }
            if (frequency == 2)
            {
                List<Tuple<DateTime, long>> complianceScheduleDates;
                complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "07").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "07").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" || row.SpecialDate.Substring(2, 2) == "10").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "10").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                long ActualForMonth = 0;
                if (frequency == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                            if (ActualForMonth != null)
                            {
                                date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                            }
                        }
                        else
                        {
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 > Convert.ToDateTime(lastSchedule.ScheduleOn)).ToList();
                        ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(ComplianceInstanceID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
            }
            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    if (frequency == 5)
                    {
                        date = new DateTime(scheduledOn.Year + 2, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                    if (frequency == 6)
                    {
                        date = new DateTime(scheduledOn.Year + 7, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                    }
                }
            }
            string forMonth = string.Empty;
            if (complianceSchedule.Count > 0)
            {
                string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");
                var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();
                forMonth = GetForMonthPeriodically(date, ForMonthSchedule.ForMonth, (byte)frequency);
                formonthscheduleR = ForMonthSchedule.ForMonth;
            }
            return new Tuple<DateTime, string, long>(date, forMonth, formonthscheduleR);
        }

        #endregion
    }
}
