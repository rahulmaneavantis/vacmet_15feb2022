﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
   public class RiskExportinfoReportNEW
    {
       public string ControlNo { get; set; }
        public long ID { get; set; }
        public long RiskCreationId { get; set; }
        public string Email { get; set; }
        public string ActivityDescription { get; set; }
        public string ControlObjective { get; set; }
        public long RiskCategory { get; set; }
        public long Industry { get; set; }
        public Nullable<int> Client { get; set; }
        public Nullable<int> LocationType { get; set; }
        public string Assertions { get; set; }
        public string ControlDescription { get; set; }
        public string MControlDescription { get; set; }
        public string PersonResponsible { get; set; }
        public Nullable<DateTime> EffectiveDate { get; set; }
        public string Key { get; set; }
        public string PreventiveControl { get; set; }
        public string AutomatedControl { get; set; }
        public string Frequency { get; set; }
        public Nullable<long> ProcessId { get; set; }
        public Nullable<long> SubProcessId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string GapDescription { get; set; }
        public string Recommendations { get; set; }
        public string ActionRemediationplan { get; set; }
        public string IPE { get; set; }
        public string ERPsystem { get; set; }
        public string FRC { get; set; }
        public string UniqueReferred { get; set; }
        public string TestStrategy { get; set; }
        public string DocumentsExamined { get; set; }
        public string ActivityTobeDone { get; set; }        
        public string IsInternalAudit { get; set; }
        public Nullable<int> RiskRating { get; set; }
        public Nullable<int> ControlRating { get; set; }
    }
}
