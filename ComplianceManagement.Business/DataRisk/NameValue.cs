﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    public class NameValue
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
