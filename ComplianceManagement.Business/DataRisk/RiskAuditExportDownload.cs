﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{   
    public class RiskAuditExportDownload
    {
        public long ID { get; set; }
        public long RiskCategoryCreationId { get; set; }
        public string ActivityDescription { get; set; }
        public string ControlObjective { get; set; }
        public string SampleDownloadFileName { get; set; }
        public string TestingDownloadFileName { get; set; }
        public Nullable<long> ProcessID { get; set; }
        public Nullable<long> SubProcessID { get; set; }
        public string KeyName { get; set; }
        
    }
}
