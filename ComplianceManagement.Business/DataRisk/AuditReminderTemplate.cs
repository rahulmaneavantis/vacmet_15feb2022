//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    using System;
    using System.Collections.Generic;
    
    public partial class AuditReminderTemplate
    {
        public int ID { get; set; }
        public long CustomerID { get; set; }
        public int TimeInDays { get; set; }
        public bool IsSubscribed { get; set; }
        public string IsEscalationReminder { get; set; }
        public Nullable<long> BeforeInDays { get; set; }
    }
}
