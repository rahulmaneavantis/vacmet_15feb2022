//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataRisk
{
    using System;
    
    public partial class usp_Pre_requisite_details_Result
    {
        public long checklistId { get; set; }
        public string checklistName { get; set; }
        public string FileName { get; set; }
        public long AuditID { get; set; }
        public string Remark { get; set; }
        public Nullable<long> AuditeeId { get; set; }
        public string AuditeeName { get; set; }
        public Nullable<long> BossId { get; set; }
        public string BossName { get; set; }
        public Nullable<System.DateTime> Timeline { get; set; }
        public Nullable<long> MAPId { get; set; }
    }
}
