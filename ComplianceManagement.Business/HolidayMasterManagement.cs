﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class HolidayMasterManagement
    {
        public static int CheckHoliday(long ComplianceID,long complianceInstanceId,DateTime ScheduleOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int cnt = 0;
                Boolean flag = false;
                while (flag ==false)
                {
                    var data = (from row in entities.Sp_CheckHoliday(ComplianceID, complianceInstanceId, ScheduleOn.Day, ScheduleOn.Month, ScheduleOn.Year)
                                select row).ToList();
                    if (data.Count == 0)
                    {
                        flag = true;
                    }
                    else
                    {
                        cnt += 1;
                        flag = false;
                        ScheduleOn = ScheduleOn.AddDays(1);
                    }
                }

                return cnt;
            }
        }

        public static int CheckHolidayInternal(long ComplianceID, long complianceInstanceId, DateTime ScheduleOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int cnt = 0;
                Boolean flag = false;
                while (flag == false)
                {
                    var data = (from row in entities.Sp_CheckHolidayInternal(ComplianceID, complianceInstanceId, ScheduleOn.Day, ScheduleOn.Month, ScheduleOn.Year)
                                select row).ToList();
                    if (data.Count == 0)
                    {
                        flag = true;
                    }
                    else
                    {
                        cnt += 1;
                        flag = false;
                        ScheduleOn = ScheduleOn.AddDays(1);
                    }
                }

                return cnt;
            }
        }

        public static List<object> GetAllHolidays(int compliancetypeID = -1, int stateID = -1,string filterText = "")
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                IQueryable<HolidayMaster> holidaylist = (from row in entities.HolidayMasters
                                                         where row.IsDeleted == false                                                         
                                                         select row);

                if (compliancetypeID != -1)
                    holidaylist = holidaylist.Where(entry => entry.ComplianceTypeID == compliancetypeID);

                if (stateID != -1)
                    holidaylist = holidaylist.Where(entry => entry.StateID == stateID);

                if (!string.IsNullOrEmpty(filterText))
                    holidaylist = holidaylist.Where(entry => entry.Name.Trim().ToUpper().Contains(filterText.Trim().ToUpper()));


                var holidayListFinal = (from row in holidaylist
                                        join type in entities.ComplianceTypes
                                        on row.ComplianceTypeID equals type.ID
                                        join states in entities.States
                                        on row.StateID equals states.ID
                                        into stateInfo from stateSummary in stateInfo.DefaultIfEmpty()
                                        select new
                                        {
                                            ID = row.ID,
                                            Name = row.Name,
                                            Day = row.Day,
                                            Month = row.Month,
                                            ComplianceTypeID = type.ID,
                                            ComplianceTypeName = type.Name,                                            
                                            StateID = (stateSummary != null ? stateSummary.ID : 0) ,
                                            State = (stateSummary != null ? stateSummary.Name : ""),
                                            IsDeleted = row.IsDeleted,
                                            CreatedBy = row.CreatedBy,
                                            CreatedOn = row.CreatedOn,
                                            UpdatedBy = row.UpdatedBy,
                                            UpdatedOn = row.UpdatedOn,
                                            HYear=row.HYear,
                                        }).OrderBy(entry => entry.ID).ToList<object>(); 
                    
                return holidayListFinal;
            }
        }
        public static HolidayMaster GetHolidayByID(long holidayID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                HolidayMaster holidayRecord = (from row in entities.HolidayMasters
                           where row.ID == holidayID && row.IsDeleted == false
                           select row).SingleOrDefault();

                return holidayRecord;
            }
        }
        public static void DeleteHoliday(int holidayID,long UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                HolidayMaster holidayToDelete = (from row in entities.HolidayMasters
                                   where row.ID == holidayID
                                   select row).FirstOrDefault();

                
                holidayToDelete.IsDeleted = true;
                holidayToDelete.UpdatedBy = UserID;
                holidayToDelete.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static bool Exists(HolidayMaster holiday)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.HolidayMasters
                             where row.IsDeleted == false 
                             && row.Name.Trim().ToUpper().Equals(holiday.Name.Trim().ToUpper())
                             select row).ToList();

                if (holiday.ComplianceTypeID != -1)
                    query = query.Where(entry => entry.ComplianceTypeID == holiday.ComplianceTypeID).ToList();

                if (holiday.StateID != -1 && holiday.StateID != 0)
                    query = query.Where(entry => entry.StateID == holiday.StateID).ToList();
                
                //for update 
                if (holiday.ID > 0)
                {
                    query = query.Where(entry => entry.ID != holiday.ID).ToList();
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static bool Create(HolidayMaster holiday)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.HolidayMasters.Add(holiday);
                entities.SaveChanges();
                return true;
            }
        }
        public static bool Update(HolidayMaster holiday)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                HolidayMaster holidayToUpdate = (from row in entities.HolidayMasters
                                   where row.ID == holiday.ID
                                   select row).FirstOrDefault();

                holidayToUpdate.Name = holiday.Name;
                holidayToUpdate.Day = holiday.Day;
                holidayToUpdate.Month = holiday.Month;
                holidayToUpdate.ComplianceTypeID = holiday.ComplianceTypeID;
                holidayToUpdate.StateID = holiday.StateID;
                holidayToUpdate.UpdatedBy = holiday.UpdatedBy;
                holidayToUpdate.UpdatedOn = DateTime.Now;              
                entities.SaveChanges();
                return true;
            }
        }
    }
}
