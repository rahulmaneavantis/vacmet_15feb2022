﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class  CustomerManagementRisk
    {
        public static List<int> GetAuditListRoleIMP(int AuditID, long ProcessID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.AuditImplementationAssignments
                             where row.AuditID == AuditID
                             && row.ProcessId == ProcessID
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }
                return AuditAssignedRoles;
            }
        }
        public static List<int> GetListOfRoleInAuditIMp(int AuditID, int UserID, long ProcessId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.AuditImplementationAssignments
                             where row.AuditID == AuditID
                             && row.UserID == UserID
                             && row.ProcessId == ProcessId
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }
                return AuditAssignedRoles;
            }
        }

        public static List<int> GetListOfRoleInAudit(int AuditID, long UserID, List<long> ProcessList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.InternalControlAuditAssignments
                             where row.AuditID == AuditID
                             && row.UserID == UserID
                             && ProcessList.Contains(row.ProcessId)
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }
                return AuditAssignedRoles;
            }
        }
        public static List<int> GetAuditListRole(int AuditID, List<long> ProcessList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.InternalControlAuditAssignments
                             where row.AuditID == AuditID
                             && ProcessList.Contains(row.ProcessId)
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }
                return AuditAssignedRoles;
            }
        }
        public static void UpdateCustomerStatus(int custID, int newStatus)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var custRecord = (from cust in entities.mst_Customer
                                  where cust.ID == custID
                                  select cust).FirstOrDefault();

                if (custRecord != null)
                {
                    custRecord.Status = newStatus;
                    entities.SaveChanges();
                }
            }
        }

        public static List<int> GetListOfRoleInAudit(int AuditID, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.InternalControlAuditAssignments
                             where row.AuditID == AuditID
                             && row.UserID == UserID
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }
                return AuditAssignedRoles;
            }
        }
        public static List<int> GetAuditListRole(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.InternalControlAuditAssignments
                             where row.AuditID == AuditID
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }
                return AuditAssignedRoles;
            }
        }
        public static int GetLatestAuditteeStatus(int AuditID, int ResultID)
        {
            int AuditteeResponse = -1;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Audit = (from row in entities.ImplementationAuditResults
                             where row.AuditID == AuditID
                             && row.ResultID == ResultID
                             select row).OrderByDescending(x => x.ID).FirstOrDefault();

                if (Audit != null)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Audit.Status)))
                    {
                        AuditteeResponse = (int)Audit.Status;
                    }
                }
                return AuditteeResponse;
            }
        }
        public static List<int> GetAuditListRoleIMP(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.AuditImplementationAssignments
                             where row.AuditID == AuditID
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }
                return AuditAssignedRoles;
            }
        }
        public static List<int> GetListOfRoleInAuditIMp(int AuditID, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.AuditImplementationAssignments
                             where row.AuditID == AuditID
                             && row.UserID == UserID
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }
                return AuditAssignedRoles;
            }
        }
        public static bool GetPreRequsite(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_PrequsiteCount_Result> Masterrecord = new List<SP_PrequsiteCount_Result>();
                entities.Database.CommandTimeout = 300;
                Masterrecord = (from row in entities.SP_PrequsiteCount(Userid)
                                select row).ToList();

                if (Masterrecord.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region ICFR

        public static List<int> GetAssignedRolesICFR(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();
                var Audit = (from row in entities.AuditAssignments
                             where row.UserID == Userid
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }

                return AuditAssignedRoles;
            }
        }
        public static long GetPersonResponsibleid(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.ActionPlanAssignments
                                where row.PersonResponsible == Userid
                                select row.PersonResponsible).ToList().Distinct().FirstOrDefault();
                return customer;
            }
        }

        public static bool GetDepartMentHeadId(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_User
                                where row.ID == Userid && row.IsHead == true
                                && row.IsDeleted == false && row.IsActive == true
                                select row.IsActive).FirstOrDefault();

                return customer;
            }
        }
        #endregion

        #region ARS

        public static string GetAuditHeadOrManagerid(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_User
                                where row.ID == Userid
                                && row.IsDeleted == false && row.IsActive == true
                                select row.IsAuditHeadOrMgr).Distinct().FirstOrDefault();
                return customer;
            }
        }
        public static int GetRoleid(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_User
                                where row.ID == Userid
                                && row.IsDeleted == false && row.IsActive == true
                                select row.RoleID).Distinct().FirstOrDefault();
                return customer;
            }
        }
        public static List<int> IMPGetAssignedRolesARS(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.AuditImplementationAssignments
                             where row.UserID == Userid
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }

                return AuditAssignedRoles;
            }
        }

        public static List<int> GetAssignedRolesARS(int Userid, int CustomerbranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.InternalControlAuditAssignments
                             where row.UserID == Userid && row.CustomerBranchID == CustomerbranchID
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }

                return AuditAssignedRoles;
            }
        }
        public static int CheckIsManagement(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.ID == Userid
                              && row.IsDeleted == false && row.IsActive == true
                             select row.RoleID).FirstOrDefault();
                return query;
            }
        }
        public static List<int> GetAssignedRolesARS(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> AuditAssignedRoles = new List<int>();

                List<int> roles = new List<int>();

                var Audit = (from row in entities.InternalControlAuditAssignments
                             where row.UserID == Userid
                             select row.RoleID).Distinct().ToList();

                if (Audit != null)
                {
                    AuditAssignedRoles = Audit.ToList();
                }

                return AuditAssignedRoles;
            }
        }
        public static List<int> ARSGetAssignedRolesBOTH(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int> ProcessAssignedRoles = new List<int>();
                List<int> ImplementationAssignedRoles = new List<int>();
                List<int> roles = new List<int>();

                var ProcessAssigned = (from row in entities.InternalControlAuditAssignments
                                       where row.UserID == Userid
                                       select row.RoleID).ToList().Distinct();

                if (ProcessAssigned != null)
                {
                    ProcessAssignedRoles = ProcessAssigned.ToList();
                }
                var ImplementationAssigned = (from row in entities.AuditImplementationAssignments
                                              where row.UserID == Userid
                                              select row.RoleID).ToList().Distinct();

                if (ImplementationAssigned != null)
                {
                    ImplementationAssignedRoles = ImplementationAssigned.ToList();
                }

                roles = ProcessAssignedRoles.Union(ImplementationAssignedRoles).ToList();
                return roles;
            }
        }
        public static long GetInternalPersonResponsibleid(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var checkProcessPersonResponsible = (from row in entities.InternalControlAuditResults
                                                     where row.PersonResponsible == Userid && row.IsDeleted == false
                                                     && row.PersonResponsible != null
                                                     select row.PersonResponsible).ToList().Distinct().FirstOrDefault();


                if (checkProcessPersonResponsible != null)
                {
                    return (long)checkProcessPersonResponsible;
                }
                else
                {
                    var checkIMPPersonResponsible = (from row in entities.ImplementationAuditResults
                                                     where row.PersonResponsible == Userid && row.IsDeleted == false
                                                     select row.PersonResponsible).ToList().Distinct().FirstOrDefault();

                    if (checkIMPPersonResponsible != null)
                    {
                        return (long)checkIMPPersonResponsible;
                    }
                    else
                    {
                        return 0;
                    }
                }

            }
        }
        public static bool CheckPersonResponsibleFlowApplicable(int UserID, long CBranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.InternalControlAuditResults
                             where row.PersonResponsible == UserID
                             && row.CustomerBranchId == CBranchID && row.IsDeleted == false
                             select row.PersonResponsible).ToList().Distinct().FirstOrDefault();

                //var query = (from row in entities.RiskActivityTransactions
                //             where row.IsDeleted == false
                //             && row.CustomerBranchId == CustomerbranchId
                //             && row.PersonResponsible == UserID
                //             select row).ToList().Distinct().FirstOrDefault();
                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static List<long> GetUsersApplicableBranch(int Userid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                List<long> ProcessBranches = new List<long>();
                List<long> ImplementationBranches = new List<long>();
                List<long> Branches = new List<long>();

                var processbranch = (from row in entities.InternalAuditTransactions
                                     where row.PersonResponsible == Userid
                                     select (long)row.CustomerBranchId).ToList().Distinct();

                if (processbranch != null)
                {
                    ProcessBranches = processbranch.ToList();
                }


                var implementationbranch = (from row in entities.AuditImplementationTransactions
                                            where row.PersonResponsible == Userid
                                            select (long)row.CustomerBranchId).ToList().Distinct();

                if (implementationbranch != null)
                {
                    ImplementationBranches = implementationbranch.ToList();
                }
                Branches = ProcessBranches.Union(ImplementationBranches).ToList();
                if (Branches != null)
                {
                    return Branches;
                }
                else
                {
                    return null;
                }
            }
        }
       
        #endregion
        public static mst_Customer GetByID(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customer = (from row in entities.mst_Customer
                                where row.ID == customerID
                                select row).SingleOrDefault();

                return customer;
            }
        }
        public static bool Create(mst_Customer customer)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                try
                {
                    customer.IsDeleted = false;
                    customer.CreatedOn = DateTime.UtcNow;
                    //customer.ServiceProviderID = 95;
                    //customer.IsServiceProvider = false;
                    entities.mst_Customer.Add(customer);
                    entities.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        
        public static void Update(mst_Customer customer)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Customer customerToUpdate = (from row in entities.mst_Customer
                                             where row.ID == customer.ID
                                             select row).FirstOrDefault();
                if (customerToUpdate != null)
                {
                    customerToUpdate.Name = customer.Name;
                    customerToUpdate.Address = customer.Address;
                    customerToUpdate.BuyerName = customer.BuyerName;
                    customerToUpdate.BuyerContactNumber = customer.BuyerContactNumber;
                    customerToUpdate.BuyerEmail = customer.BuyerEmail;
                    customerToUpdate.StartDate = customer.StartDate;
                    customerToUpdate.EndDate = customer.EndDate;
                    customerToUpdate.Status = customer.Status;
                    customerToUpdate.IComplianceApplicable = customer.IComplianceApplicable;
                    customerToUpdate.VerticalApplicable = customer.VerticalApplicable;
                    customerToUpdate.TaskApplicable = customer.TaskApplicable;
                    customerToUpdate.LocationType = customer.LocationType;
                    customerToUpdate.IsLabelApplicable = customer.IsLabelApplicable;
                    customerToUpdate.IsServiceProvider = customer.IsServiceProvider;
                    customerToUpdate.ServiceProviderID = customer.ServiceProviderID;
                    customerToUpdate.ComplianceProductType = customer.ComplianceProductType;
                    customerToUpdate.DocManNonMan = customer.DocManNonMan;
                    if (customer.IsDistributor != null)
                        customerToUpdate.IsDistributor = customer.IsDistributor;

                    if (customer.ParentID != null)
                        customerToUpdate.ParentID = customer.ParentID;

                    if (customer.CanCreateSubDist != null)
                        customerToUpdate.CanCreateSubDist = customer.CanCreateSubDist;

                    if (customer.IsPayment != null)
                        customerToUpdate.IsPayment = customer.IsPayment;

                    entities.SaveChanges();

                    bool userStatus = false;
                    if (customerToUpdate.Status == 1)
                        userStatus = true;

                    UserManagementRisk.UpdateUserStatus(customerToUpdate.ID, userStatus);
                }
            }
        }
        public static void Delete(int customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Customer customerToDelete = (from row in entities.mst_Customer
                                                 where row.ID == customerID
                                                 select row).FirstOrDefault();

                customerToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }
        public static bool Exists(mst_Customer customer)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Customer
                             where row.IsDeleted == false
                                  && row.Name.Equals(customer.Name)
                             select row);

                if (customer.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customer.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static InternalAuditTransactionView GetClosedTransaction(int ScheduledOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var statusList = (from row in entities.InternalAuditTransactionViews
                                  where row.AuditScheduleOnID == ScheduledOnID
                                  && (row.AuditStatusID == 4 || row.AuditStatusID == 5 || row.AuditStatusID == 7
                                  || row.AuditStatusID == 8 || row.AuditStatusID == 9)
                                  orderby row.AuditTransactionID descending
                                  select row).ToList();



                return statusList.FirstOrDefault();
            }
        }

        public static void UpdateCustomerPhoto(int ID, String FilePath)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Customer CustomerToUpdate = (from Row in entities.mst_Customer
                                                 where Row.ID == ID
                                                 select Row).FirstOrDefault();

                //CustomerToUpdate.LogoPath = FilePath;
                entities.SaveChanges();
            }
        }
    }
}
