﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Net.Mime;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class EmailManager
    {
        public static void SendMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<Stream, string>> attachment)
        {
            
            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId, "Teamlease Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                if (attachment.Count > 0)
                {
                    foreach (var attach in attachment)
                    {
                        Attachment data = new Attachment(attach.Item1, attach.Item2);
                        mailMessage.Attachments.Add(data);
                    }
                }
                if (to != null)
                    to.ForEach(entry => mailMessage.To.Add(entry));
                if (cc != null)
                    cc.ForEach(entry => mailMessage.CC.Add(entry));
                if (bcc != null)
                    bcc.ForEach(entry => mailMessage.Bcc.Add(entry));

                email.Send(mailMessage);
            }
        }       
        public static void SendMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message)
        {

            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId, "Teamlease Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                {
                    if (to != null)
                        to.ForEach(entry => mailMessage.To.Add(entry));
                    if (cc != null)
                        cc.ForEach(entry => mailMessage.CC.Add(entry));
                    if (bcc != null)
                        bcc.ForEach(entry => mailMessage.Bcc.Add(entry));
                }

                email.Send(mailMessage);
            }
        }
        public static void SendMail(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Attachment> attachment)
        {

            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId, "Teamlease Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                if (attachment.Count > 0)
                {
                    foreach (var attach in attachment)
                    {
                        mailMessage.Attachments.Add(attach);
                    }
                }
                if (to != null)
                    to.ForEach(entry => mailMessage.To.Add(entry));
                if (cc != null)
                    cc.ForEach(entry => mailMessage.CC.Add(entry));
                if (bcc != null)
                    bcc.ForEach(entry => mailMessage.Bcc.Add(entry));

                email.Send(mailMessage);
            }
        }
        public static bool SendMail_Secretarial(MailSettingViewModel mailSettings, string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Attachment> attachment)
        {
            var sendStatus = false;
            try
            {
                using (SmtpClient email = new SmtpClient())
                using (MailMessage mailMessage = new MailMessage())
                {
                    email.DeliveryMethod = SmtpDeliveryMethod.Network;
                    email.UseDefaultCredentials = false;

                    if (mailSettings == null)
                    {
                        NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                        email.Credentials = credential;
                        email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                        email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                        email.Timeout = 60000;
                        email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                        MailMessage oMessage = new MailMessage();
                        string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                        mailMessage.From = new MailAddress(FromEmailId, "Teamlease Regtech");
                        mailMessage.Subject = subject;
                        mailMessage.Body = message;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                        mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                    }
                    else
                    {
                        NetworkCredential credential = new NetworkCredential(mailSettings.Email_Entity, mailSettings.Password);
                        email.Credentials = credential;
                        email.Port = mailSettings.Port;
                        email.Host = mailSettings.Host;
                        email.Timeout = 60000;
                        email.EnableSsl = Convert.ToBoolean(mailSettings.EnableSsl);
                        MailMessage oMessage = new MailMessage();
                        //string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                        //mailMessage.From = new MailAddress(FromEmailId, "Avantis Regtech");
                        mailMessage.From = new MailAddress(mailSettings.Email_Entity, mailSettings.Email_Entity);
                        mailMessage.Subject = subject;
                        mailMessage.Body = message;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                        //mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                    }
                    if (attachment != null)
                    {
                        if (attachment.Count > 0)
                        {
                            foreach (var attach in attachment)
                            {
                                mailMessage.Attachments.Add(attach);
                            }
                        }
                    }

                    if (to != null)
                        to.ForEach(entry => mailMessage.To.Add(entry));
                    if (cc != null)
                        cc.ForEach(entry => mailMessage.CC.Add(entry));
                    if (bcc != null)
                        bcc.ForEach(entry => mailMessage.Bcc.Add(entry));

                    email.Send(mailMessage);
                    sendStatus = true;
                }
            }
            catch (Exception ex)
            {
                sendStatus = false;
            }
            return sendStatus;
        }

        public static void SendMailRahul(string from, List<string> to, List<string> cc, List<string> bcc, string subject, string message)
        {

            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId, "Teamlease Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                {
                    if (to != null)
                        to.ForEach(entry => mailMessage.To.Add(entry));
                    if (cc != null)
                        cc.ForEach(entry => mailMessage.CC.Add(entry));
                    if (bcc != null)
                        bcc.ForEach(entry => mailMessage.Bcc.Add(entry));
                }

                email.Send(mailMessage);
            }
        }
    }

    public class MailSettingViewModel
    {
        public long MailSettingId { get; set; }
        public string Email_Entity { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string Host { get; set; }
        public bool? EnableSsl { get; set; }
        public int? EntityID { get; set; }
    }

}
