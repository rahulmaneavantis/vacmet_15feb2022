//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.VirtuosoITech.ComplianceManagement.Business.DataContract
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cont_tbl_TemplateMaster_log
    {
        public long ID { get; set; }
        public int CustomerID { get; set; }
        public int TemplateID { get; set; }
        public string Version { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> ApproverCount { get; set; }
    }
}
