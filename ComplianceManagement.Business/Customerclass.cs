﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Threading;
using System.Net.Mail;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Web;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class Customerclass
    {

        public static List<IMPTUserView> GetAllIMPTUser(int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            { 
              
                    var users = (from row in entities.IMPTUserViews
                                 select row);
                    if (customerID != -1)
                    {
                        users = users.Where(entry => entry.CustomerID == customerID);
                    }

                    if (!string.IsNullOrEmpty(filter))
                    {
                        users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter));
                    }

                    return users.OrderBy(entry => entry.FirstName).ToList();
                } 
        }
        public static object GetAllCustomer(int userID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.CustomerAssignmentDetails
                             on row.ID equals row1.CustomerID
                             where row1.UserID == userID
                             && row.IsDeleted == false
                             && row1.IsDeleted == false 
                              && row.ComplianceProductType != 1
                              && row.Status == 1
                             select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.ToUpper().Contains(filter) || entry.BuyerName.ToUpper().Contains(filter) || entry.BuyerEmail.ToUpper().Contains(filter));
                }

                return users.Distinct().ToList();
            }
        }
    }
    
}
