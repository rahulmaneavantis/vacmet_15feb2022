﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Web;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class SendGridEmailManager
    {
        public static void SendGridMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message)
        {

            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait();  
        }

        public static void SendGridMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait();  
        }

        public static void SendGridNewsLetterMail(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            //Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            //task.Wait(); 


            using (SmtpClient email = new SmtpClient())
            using (MailMessage mailMessage = new MailMessage())
            {
                email.DeliveryMethod = SmtpDeliveryMethod.Network;
                email.UseDefaultCredentials = false;
                NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                email.Credentials = credential;
                email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                email.Timeout = 60000;
                email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                MailMessage oMessage = new MailMessage();
                string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                mailMessage.From = new MailAddress(FromEmailId, "Teamlease Regtech");
                mailMessage.Subject = subject;
                mailMessage.Body = message;
                mailMessage.IsBodyHtml = true;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));
                if (attachment.Count > 0)
                {
                    foreach (var attach in attachment)
                    {
                        mailMessage.Attachments.Add(attach);
                    }
                }
                if (to != null)
                    to.ForEach(entry => mailMessage.To.Add(entry));
                if (cc != null)
                    cc.ForEach(entry => mailMessage.CC.Add(entry));
                if (bcc != null)
                    bcc.ForEach(entry => mailMessage.Bcc.Add(entry));

                email.Send(mailMessage);
            }
        }

        public static void SendGridMailwithAttachment(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait();  
        }
        public static void SendGridNewsLetterMail1(string from, string fromName, List<string> to, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> attachment)
        {
            Task task = Execute(from, fromName, to, cc, bcc, subject, message, null);

            task.Wait();  
        }

        static async Task Execute(string From, string fromName, List<string> To, List<string> cc, List<string> bcc, string subject, string message, List<Tuple<string, string>> Attachment)
        {
            var sendGridAPIKey = ConfigurationManager.AppSettings["SendGridAPIKey"];

            var apiKey = sendGridAPIKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(From, fromName);
            var to = new EmailAddress(To.FirstOrDefault());
            var plainTextContent = message;
            var htmlContent = message;

            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

            await client.SendEmailAsync(msg);
        }

    }
}
