﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CustomerBranchManagementRisk
    {

     
        public static List<int> GetLocationHierarchy(int customerBranchID)
        {
            List<int> LocationList = new List<int>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                LocationList.Add(customerBranchID);

                var query = (from row in entities.mst_CustomerBranch
                             where row.ID == customerBranchID
                             select row.ParentID).FirstOrDefault();

                if (query != null)
                {
                    int? CustBranch;

                    LocationList.Add((int) query);

                    customerBranchID = (int) query;

                    CustBranch = (int) query;

                    while (CustBranch != null)
                    {
                        CustBranch = (from row in entities.mst_CustomerBranch
                                      where row.ID == customerBranchID
                                      select row.ParentID).FirstOrDefault();
                        if (CustBranch != null)
                        {
                            LocationList.Add((int) CustBranch);
                            customerBranchID = (int) CustBranch;
                        }
                    };
                }
            }

            return LocationList;
        }

        public static List<NameValueHierarchy> GetAllHierarchyARSAuditManager(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.EntitiesAssignmentAuditManagerRisks
                             join Cbranch in entities.mst_CustomerBranch
                             on row.BranchID equals Cbranch.ID
                             join Cust in entities.mst_Customer
                             on Cbranch.CustomerID equals Cust.ID
                             select Cust).Distinct();

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntitiesAM(item, true, entities);
                }
            }

            return hierarchy;
        }
        public static void LoadSubEntitiesAM(NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                where row.IsDeleted == false
                                                select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntitiesAM(item, false, entities);
            }
        }
        public static void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                BindBranchesHierarchy(node, item);
                parent.ChildNodes.Add(node);
            }
        }

      

  

        public static TMP_Assignment GetByID(int ProcessID, int Customerbranchid, string FinancialYear, string ForPeriod,int UserId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var tmpassign = (from row in entities.TMP_Assignment
                                 where row.ProcessId == ProcessID && row.CustomerBranchID == Customerbranchid
                                 && row.FinancialYear == FinancialYear
                                 && row.ForPeriod == ForPeriod && row.Status == 0 && row.PUserId == UserId
                                 select row).SingleOrDefault();

                return tmpassign;
            }
        }
        public static mst_CustomerBranch GetByID(long customerBranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var customerBranch = (from row in entities.mst_CustomerBranch
                                      where row.ID == customerBranchID
                                      select row).SingleOrDefault();

                return customerBranch;
            }
        }
        public static void Create(mst_CustomerBranch customerBranch)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                customerBranch.IsDeleted = false;
                customerBranch.CreatedOn = DateTime.UtcNow;

                entities.mst_CustomerBranch.Add(customerBranch);

                entities.SaveChanges();
            }
        }
        public static void Update(mst_CustomerBranch customerBranch)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_CustomerBranch customerBranchToUpdate = (from row in entities.mst_CustomerBranch
                                                         where row.ID == customerBranch.ID
                                                         select row).FirstOrDefault();

                customerBranchToUpdate.Name = customerBranch.Name;
                customerBranchToUpdate.Type = customerBranch.Type;
                customerBranchToUpdate.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                customerBranchToUpdate.AddressLine1 = customerBranch.AddressLine1;
                customerBranchToUpdate.AddressLine2 = customerBranch.AddressLine2;
                customerBranchToUpdate.StateID = customerBranch.StateID;
                customerBranchToUpdate.CityID = customerBranch.CityID;
                customerBranchToUpdate.Others = customerBranch.Others;
                customerBranchToUpdate.Industry = customerBranch.Industry;
                customerBranchToUpdate.ContactPerson = customerBranch.ContactPerson;
                customerBranchToUpdate.Landline = customerBranch.Landline;
                customerBranchToUpdate.Mobile = customerBranch.Mobile;
                customerBranchToUpdate.EmailID = customerBranch.EmailID;
                customerBranchToUpdate.PinCode = customerBranch.PinCode;
                customerBranchToUpdate.Status = customerBranch.Status;
                customerBranchToUpdate.LegalEntityTypeID = customerBranch.LegalEntityTypeID;//added by manisha

                entities.SaveChanges();
            }
        }
        public static void Delete(int customerBranchID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_CustomerBranch customerBranchToDelete = (from row in entities.mst_CustomerBranch
                                                         where row.ID == customerBranchID
                                                         select row).FirstOrDefault();

                customerBranchToDelete.IsDeleted = true;

                entities.SaveChanges();
            }
        }
        public static bool Exists(mst_CustomerBranch customerBranch, int CustomerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false
                             && row.Name.Equals(customerBranch.Name) && row.CustomerID == CustomerId
                             select row);

                if (customerBranch.ID > 0)
                {
                    query = query.Where(entry => entry.ID != customerBranch.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static List<NameValueHierarchy> GetAllHierarchy(int customerID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Customers
                             where row.IsDeleted == false
                             select row);

                if (customerID != -1)
                {
                    query = query.Where(entry => entry.ID == customerID);
                }

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

                foreach (var item in hierarchy)
                {
                    LoadSubEntities(item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                select row);

            if (isClient)
            {
                query = query.Where(entry => entry.CustomerID == nvp.ID && entry.ParentID == null);
            }
            else
            {
                query = query.Where(entry => entry.ParentID == nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID, Name = entry.Name }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                LoadSubEntities(item, false, entities);
            }
        }
        public static void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<int> LocationList)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                //if (FindNodeExists(item, LocationList))
                //{
                    BindBranchesHierarchy(node, item, LocationList);
                    parent.ChildNodes.Add(node);
                //}
            }
        }
        public static bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
       
        public static List<int> GetAssignedLocationList(int UserID, int custID, String Role)
        {
            List<int> LocationList = new List<int>();

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             select row).GroupBy(g => g.ID).Select(g => g.FirstOrDefault());

                if (query != null)
                    LocationList = query.Select(a => a.ID).Distinct().ToList();
                return LocationList;
            }
        }

        public static void Update1(mst_CustomerBranch customerBranch)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //CustomerBranch customerBranchToUpdate = new CustomerBranch() { ID = customerBranch.ID };
                //entities.CustomerBranches.Attach(customerBranchToUpdate);

                mst_CustomerBranch customerBranchToUpdate = (from row in entities.mst_CustomerBranch
                                                             where row.ID == customerBranch.ID
                                                             select row).FirstOrDefault();
                customerBranchToUpdate.Name = customerBranch.Name;
                customerBranchToUpdate.Type = customerBranch.Type;
                customerBranchToUpdate.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                customerBranchToUpdate.AddressLine1 = customerBranch.AddressLine1;
                customerBranchToUpdate.AddressLine2 = customerBranch.AddressLine2;
                customerBranchToUpdate.StateID = customerBranch.StateID;
                customerBranchToUpdate.CityID = customerBranch.CityID;
                customerBranchToUpdate.Others = customerBranch.Others;
                customerBranchToUpdate.Industry = customerBranch.Industry;
                customerBranchToUpdate.ContactPerson = customerBranch.ContactPerson;
                customerBranchToUpdate.Landline = customerBranch.Landline;
                customerBranchToUpdate.Mobile = customerBranch.Mobile;
                customerBranchToUpdate.EmailID = customerBranch.EmailID;
                customerBranchToUpdate.PinCode = customerBranch.PinCode;
                customerBranchToUpdate.Status = customerBranch.Status;
                customerBranchToUpdate.LegalEntityTypeID = customerBranch.LegalEntityTypeID;//added by manisha
                customerBranchToUpdate.AuditPR = customerBranch.AuditPR;
                customerBranchToUpdate.ComType = customerBranch.ComType;
                entities.SaveChanges();
            }
        }

        public static void Update1FromSecretarial(com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //CustomerBranch customerBranchToUpdate = new CustomerBranch() { ID = customerBranch.ID };
                //entities.CustomerBranches.Attach(customerBranchToUpdate);

                mst_CustomerBranch customerBranchToUpdate = (from row in entities.mst_CustomerBranch
                                                             where row.ID == customerBranch.ID
                                                             select row).FirstOrDefault();
                customerBranchToUpdate.Name = customerBranch.Name;
                //customerBranchToUpdate.Type = customerBranch.Type;
                //customerBranchToUpdate.LegalRelationShipOrStatus = customerBranch.LegalRelationShipOrStatus;
                customerBranchToUpdate.AddressLine1 = customerBranch.AddressLine1;
                customerBranchToUpdate.AddressLine2 = customerBranch.AddressLine2;
                customerBranchToUpdate.StateID = customerBranch.StateID;
                customerBranchToUpdate.CityID = customerBranch.CityID;
                //customerBranchToUpdate.Others = customerBranch.Others;
                //customerBranchToUpdate.Industry = customerBranch.Industry;
                //customerBranchToUpdate.ContactPerson = customerBranch.ContactPerson;
                //customerBranchToUpdate.Landline = customerBranch.Landline;
                //customerBranchToUpdate.Mobile = customerBranch.Mobile;
                customerBranchToUpdate.EmailID = customerBranch.EmailID;
                customerBranchToUpdate.PinCode = customerBranch.PinCode;
                //customerBranchToUpdate.Status = customerBranch.Status;
                //customerBranchToUpdate.LegalEntityTypeID = customerBranch.LegalEntityTypeID;//added by manisha
                //customerBranchToUpdate.AuditPR = customerBranch.AuditPR;
                //customerBranchToUpdate.ComType = customerBranch.ComType;
                entities.SaveChanges();
            }
        }

        /*Added by Madhur*/
        public static List<mst_CustomerBranch> GetcustomerBranchList(int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row).ToList();
                return query;
            }
        }

        public static List<CustomerBranch> GetcustomerBranchListComp(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row).ToList();
                return query;
            }
        }

        public static List<mst_Vertical> GetVerticalList(int CustomerID)
        {
            bool verticalexist = false;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var checkvertical = (from row in entities.mst_Vertical
                                     where row.CustomerID == CustomerID
                                     && row.VerticalName.ToLower() == "na"
                                     select row).ToList();

                if (checkvertical.Count > 0)
                {
                    verticalexist = true;
                }
                else
                {
                    mst_Vertical obj = new mst_Vertical();
                    obj.CustomerID = CustomerID;
                    obj.VerticalName = "NA";
                    entities.mst_Vertical.Add(obj);
                    entities.SaveChanges();
                }

                var query = (from row in entities.mst_Vertical
                             where row.CustomerID == CustomerID
                             select row).ToList();
                return query;
            }
        }

        public static List<Mst_Process> GetProcessList(int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_Process
                             where row.CustomerID == CustomerID
                             && row.IsDeleted == false
                             select row).ToList();
                return query;
            }
        }

        public static List<mst_Subprocess> GetSubProcessList(int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Subprocess
                             where row.IsDeleted == false
                             select row).ToList();
                return query;
            }
        }

        public static List<mst_Activity> GetActivityList(int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Activity
                             where row.IsDeleted == false
                             select row).ToList();
                return query;
            }
        }

        public static List<mst_User> GetUserList(int CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_User
                             where row.IsDeleted == false
                             && row.IsActive == true
                             && row.CustomerID == CustomerID
                             select row).ToList();
                return query;
            }
        }

        public static List<User> GetUserListComp(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Users
                             where row.IsDeleted == false
                             && row.IsActive == true
                             && row.CustomerID == CustomerID
                             select row).ToList();
                return query;
            }
        }
        /*End*/
    }
}
