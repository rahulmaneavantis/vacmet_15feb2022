﻿using System.Collections.Generic;
using System.Linq;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ComplianceStatusManagement
    {
        public static List<SP_GetComplianceStatusMaster_Result> GetStatusListByCustomer(bool IsCheck, string RoleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (IsCheck)
                {
                    var statusList = (from row in entities.SP_GetComplianceStatusMaster("True", RoleID)
                                      select row).ToList();

                    return statusList;
                }
                else
                {
                    var statusList = (from row in entities.SP_GetComplianceStatusMaster("False", RoleID)
                                      select row).ToList();

                    return statusList;
                }
            }
        }
        public static List<ComplianceStatu> GetStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  where row.ID != 16 && row.ID != 19 && row.ID != 20
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ComplianceStatu> GetNotCompliedStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  where row.ID != 19 && row.ID != 20
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ComplianceStatu> GetCompliedButDocumentPendingList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  where row.ID != 16
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ComplianceStatu> GetEscalationRevStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  where row.ID == 13 || row.ID == 14
                                  select row).ToList();

                return statusList;
            }
        }

        public static List<ComplianceStatu> GetEscalationStatusList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.ComplianceStatus
                                  where row.ID == 12
                                  select row).ToList();

                return statusList;
            }
        }
        public static List<tbl_ComplianceRemarkList> GetRemarkListByStatus(int RemarkID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.tbl_ComplianceRemarkList
                                  where row.Status == RemarkID
                                  && row.DepartmentID != 1
                                  && row.IsActive == true
                                  select row).ToList(); 
                return statusList;
            }
        }
        public static List<tbl_ComplianceRemarkList> GetRemarkListByCloseDelayed(int RemarkID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.tbl_ComplianceRemarkList
                                  where row.Status == RemarkID
                                  && row.DepartmentID == 0
                                  && row.IsActive == true
                                  select row).ToList();
                return statusList;
            }
        }
        public static List<tbl_ComplianceRemarkList> GetRemarkListByDepartment(int RemarkID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.tbl_ComplianceRemarkList
                                  where row.Status == RemarkID  
                                  && row.DepartmentID == 1
                                  || row.DepartmentID == 2
                                  && row.IsActive == true
                                  select row).ToList(); 
                return statusList;
            }
        }
        public static bool GetComplinceDeptID(long ComplianceInstanceID,int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceInstances
                            join row1 in entities.Departments
                            on row.DepartmentID equals row1.ID
                            where row.ID == ComplianceInstanceID
                            && row1.Name == "Secretarial"
                            && row1.CustomerID == CustomerID  
                            && row1.IsDeleted == false
                            && row.IsDeleted == false
                            select row.DepartmentID).FirstOrDefault();
                if (data != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool GetInternalComplinceDeptID(long ComplianceInstanceID, int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.InternalComplianceInstances
                            join row1 in entities.Departments
                            on row.DepartmentID equals row1.ID
                            where row.ID == ComplianceInstanceID
                            && row1.Name == "Secretarial"
                            && row1.CustomerID == CustomerID
                            && row1.IsDeleted == false
                            && row.IsDeleted == false
                            select row.DepartmentID).FirstOrDefault();
                if (data != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<ComplianceStatusTransition> GetStatusTransitionList()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transitionList = (from row in entities.ComplianceStatusTransitions
                                  select row).ToList();

                return transitionList;
            }
        }
        public static List<ComplianceStatusTransition> GetStatusTransitionListByInitialId(int initialId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var transitionList = (from row in entities.ComplianceStatusTransitions
                                      select row).ToList();

                if (initialId != 0)
                {
                    transitionList = transitionList.Where(row => row.InitialStateID == initialId).ToList();
                }

                return transitionList.ToList();
            }
        }
    }
}
