﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ComplinaceManagementNew
    {
     
        #region Excel Added by rahul
        public static void ModifyCompliancePenaltyDescription(long complianceid, string pdescription)
        {


            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                com.VirtuosoITech.ComplianceManagement.Business.Data.Compliance c = entities.Compliances.First(j => j.ID == complianceid);
                c.PenaltyDescription = pdescription;
                entities.SaveChanges();
            }
        }
        public static int GetIndustryIDByIndustryName(string Industryname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int resultset = (from x in entities.Industries
                                 where x.Name == Industryname
                                 select x.ID).SingleOrDefault();

                return resultset;
            }

        }

        public static int GetLegalEntityIDByLegalEntityName(string LegalEntityname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int resultset = (from x in entities.M_LegalEntityType
                                 where x.EntityTypeName == LegalEntityname
                                 select x.ID).SingleOrDefault();

                return resultset;
            }

        }
        public static void UpdateSecretarialMappedID(long ComplianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var ids = (from row in entities.Compliance_SecretarialTagMapping
                           where row.ComplianceID == ComplianceId
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    Compliance_SecretarialTagMapping prevmappedids = (from row in entities.Compliance_SecretarialTagMapping
                                                                      where row.ID == entry
                                                                      select row).FirstOrDefault();
                    entities.Compliance_SecretarialTagMapping.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }

        }
        public static void UpdateLegalEntityMappedID(long ComplianceId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {


                var ids = (from row in entities.LegalEntityTypeMappings
                           where row.ComplianceId == ComplianceId
                           select row.ID).ToList();

                ids.ForEach(entry =>
                {
                    LegalEntityTypeMapping prevmappedids = (from row in entities.LegalEntityTypeMappings
                                                            where row.ID == entry
                                                            select row).FirstOrDefault();
                    entities.LegalEntityTypeMappings.Remove(prevmappedids);

                });
                entities.SaveChanges();
            }

        }

        public static List<int> GetAllIndustryID()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllIndustryIDs = (from row in entities.Industries
                                      select row.ID).ToList();

                return AllIndustryIDs;
            }

        }


        public static List<int> GetAllLegalEntityTypeID()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AllLegalEntityTypeIDs = (from row in entities.M_LegalEntityType
                                             select row.ID).ToList();

                return AllLegalEntityTypeIDs;
            }

        }
        #endregion
    }
}
