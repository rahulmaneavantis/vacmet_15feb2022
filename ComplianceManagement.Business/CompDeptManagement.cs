﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class CompDeptManagement
    {
        public static int GetDepartmentIDByName1(string name, int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DepartmentID = (from row in entities.Departments
                                    where row.Name.ToUpper().Trim() == name.ToUpper().Trim()
                                    //&& row.CustomerID == CustomerId
                                    && row.IsDeleted == false
                                    select row.ID).FirstOrDefault();

                return Convert.ToInt32(DepartmentID);
            }
        }
        public static int GetContactPersonIDByEmail(string email, int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ContactPersonID = (from row in entities.Users
                                    where row.Email == email.ToUpper().Trim()
                                    && row.CustomerID == CustomerId
                                    && row.IsDeleted == false
                                    select row.ID).FirstOrDefault();

                return Convert.ToInt32(ContactPersonID);
            }
        }
        public static int GetPaymentTypeByID(string name, int CustomerId)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var PaymentTypeID = (from row in entities.tbl_PaymentType
                                     where row.TypeName.ToUpper().Trim() == name.ToUpper().Trim()
                                     && row.IsDeleted == false
                                     select row.ID).FirstOrDefault();

                return Convert.ToInt32(PaymentTypeID);
            }
        }
        #region compliance department    
        public static int GetDepartmentIDByName(string name)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DepartmentID = (from row in entities.Departments
                                    where row.Name.ToUpper().Trim() == name.ToUpper().Trim()
                                    && row.IsDeleted == false
                                    select row.ID).FirstOrDefault();

                return Convert.ToInt32(DepartmentID);
            }
        }
        public static int GetDepartmentIDByName(string name,int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DepartmentID = (from row in entities.Departments
                                    where row.Name.ToUpper().Trim() == name.ToUpper().Trim()
                                    && row.CustomerID == CustomerId
                                    && row.IsDeleted == false
                                    select row.ID).FirstOrDefault();

                return Convert.ToInt32(DepartmentID);
            }
        }
        public static object FillDepartment(int CustomerId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Departments
                             where row.IsDeleted == false
                             && row.CustomerID == CustomerId
                             select row);
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static void CreateDepartmentMaster(Department dept)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Departments.Add(dept);
                entities.SaveChanges();
            }
        }
        public static int CreateComplianceDepartmentMaster(Department dept)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Departments.Add(dept);
                    entities.SaveChanges();
                }

                return dept.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static void UpdateDepartmentMaster(Department dept)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Department deptMasterToUpdate = (from row in entities.Departments
                                                 where row.ID == dept.ID
                                                    && row.IsDeleted == false
                                                    && row.CustomerID == dept.CustomerID
                                                 select row).FirstOrDefault();

                deptMasterToUpdate.Name = dept.Name;
                deptMasterToUpdate.Color = dept.Color;
                entities.SaveChanges();
            }
        }
        public static Department DepartmentMasterGetByID(int deptID, long customerID) 
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MstDepartmentMaster = (from row in entities.Departments
                                           where row.ID == deptID
                                            && row.CustomerID== customerID
                                           select row).SingleOrDefault();

                return MstDepartmentMaster;
            }
        }     
        public static List<Department> GetAllDepartmentMasterList(long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var DepartmentMasterList = (from row in entities.Departments
                                            where row.IsDeleted == false
                                            && row.CustomerID == customerID
                                            select row);

                return DepartmentMasterList.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static void DeleteDepartmentMaster(int deptID, long customerID)//,long customerID
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                Department DepartmentMastertoDelete = (from row in entities.Departments
                                                       where row.ID == deptID
                                                       && row.CustomerID== customerID
                                                       select row).FirstOrDefault();

                DepartmentMastertoDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static bool DepartmentExists(Department deptmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Departments
                             where row.Name.ToUpper().Equals(deptmaster.Name.ToUpper()) && row.IsDeleted == false
                             && row.CustomerID==deptmaster.CustomerID
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        public static int GetDepartmentIDByName(List<Department> lstDepts, string deptName, int customerID)
        {
            var DepartmentID = (from row in lstDepts
                                where row.Name.Trim().ToUpper().Equals(deptName.Trim().ToUpper())
                                && row.IsDeleted == false
                                && row.CustomerID == customerID
                                select row.ID).FirstOrDefault();

            return Convert.ToInt32(DepartmentID);
        }
        #endregion



        #region Audit
        public static bool EntitiesAssignmentDepartMentHeadLocationDepartmentAssigned(int customerID, long Branchid, int departmentID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.RiskActivityToBeDoneMappings
                             join row1 in entities.Mst_Process
                             on row.ProcessId equals row1.Id
                             join row3 in entities.mst_Department
                             on row1.DepartmentID equals row3.ID
                             where row.CustomerBranchID == Branchid
                             && row1.CustomerID == customerID
                             && row3.ID == departmentID
                             && row1.IsDeleted == false
                             && row3.IsDeleted == false
                             select row3).FirstOrDefault();

                if (query != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static object FillDepartmentFromRATBDM(int customerID, List<long> branchList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                if (branchList.Count > 0)
                {
                    //var query = (from row in entities.RiskActivityToBeDoneMappings
                    //             join row1 in entities.Mst_Process
                    //             on row.ProcessId equals row1.Id
                    //             join row3 in entities.mst_Department
                    //             on row1.DepartmentID equals row3.ID
                    //             where branchList.Contains((long)row.CustomerBranchID)
                    //             && row1.CustomerID == customerID && row1.IsDeleted == false
                    //             && row3.IsDeleted == false
                    //             select row3).Distinct().ToList();

                    //var assigneddepartments = (from row in entities.EntitiesAssignmentDepartmentHeads
                    //                           where branchList.Contains((long)row.BranchID)
                    //                           && row.CustomerID == customerID
                    //                           && row.ISACTIVE == true
                    //                           select row.DepartmentID).Distinct().ToList();
                    //if (assigneddepartments.Count > 0)
                    //{
                    //    query = query.Where(en => assigneddepartments.Contains((int)en.ID)).ToList();
                    //}
                    var query = (from row in entities.mst_Department
                                 where row.CustomerID == customerID
                                 select row).ToList();

                    var users = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
                else
                {
                    //var query = (from row in entities.RiskActivityToBeDoneMappings
                    //             join row1 in entities.Mst_Process
                    //             on row.ProcessId equals row1.Id
                    //             join row3 in entities.mst_Department
                    //             on row1.DepartmentID equals row3.ID
                    //             where row1.CustomerID == customerID && row1.IsDeleted == false
                    //             && row3.IsDeleted == false
                    //             select row3).Distinct().ToList();

                    //var assigneddepartments = (from row in entities.EntitiesAssignmentDepartmentHeads
                    //                           where row.CustomerID == customerID
                    //                           && row.ISACTIVE == true
                    //                           select row.DepartmentID).Distinct().ToList();
                    //if (assigneddepartments.Count > 0)
                    //{
                    //    query = query.Where(en => !assigneddepartments.Contains((int)en.ID)).ToList();
                    //}

                    var query = (from row in entities.mst_Department
                                 where row.CustomerID == customerID
                                 select row).ToList();

                    var users = (from row in query
                                 select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    return users;
                }
            }
        }
        public static object FillDepartmentFromRATBDMEdit(int customerID, List<long> branchList, long UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (branchList.Count > 0)
                {
                    //var query = (from row in entities.EntitiesAssignmentDepartmentHeads
                    //             join row3 in entities.mst_Department
                    //             on row.DepartmentID equals row3.ID
                    //             where branchList.Contains((long)row.BranchID)
                    //             && row.CustomerID == customerID
                    //             && row.UserID == UserID
                    //             && row.ISACTIVE == true
                    //             && row3.IsDeleted == false
                    //             select row3).Distinct().ToList();
                    //var query = (from row in entities.RiskActivityToBeDoneMappings
                    //             join row1 in entities.Mst_Process
                    //             on row.ProcessId equals row1.Id
                    //             join row3 in entities.mst_Department
                    //             on row1.DepartmentID equals row3.ID
                    //             where branchList.Contains((long)row.CustomerBranchID)
                    //             && row1.CustomerID == customerID && row1.IsDeleted == false
                    //             && row3.IsDeleted == false
                    //             select row3).Distinct().ToList();

                    var query = (from row in entities.mst_Department
                                 where row.CustomerID == customerID
                                 select row).ToList();
                    if (query.Count > 0)
                    {
                        var users = (from row in query
                                     select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                        return users;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    //var query = (from row in entities.EntitiesAssignmentDepartmentHeads
                    //             join row3 in entities.mst_Department
                    //             on row.DepartmentID equals row3.ID
                    //             where row.UserID == UserID
                    //             && row.CustomerID == customerID
                    //             && row.ISACTIVE == true
                    //             && row3.IsDeleted == false
                    //             select row3).Distinct().ToList();
                    //var query = (from row in entities.RiskActivityToBeDoneMappings
                    //             join row1 in entities.Mst_Process
                    //             on row.ProcessId equals row1.Id
                    //             join row3 in entities.mst_Department
                    //             on row1.DepartmentID equals row3.ID
                    //             where row1.CustomerID == customerID && row1.IsDeleted == false
                    //             && row3.IsDeleted == false
                    //             select row3).Distinct().ToList();

                    var query = (from row in entities.mst_Department
                                 where row.CustomerID == customerID
                                 select row).ToList();
                    if (query.Count > 0)
                    {
                        var users = (from row in query
                                     select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                        return users;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public static string GetDepartmentName(int DeptId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = (from row in entities.mst_Department
                                         where
                                         row.ID == DeptId && row.IsDeleted == false
                                         select row.Name).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static object FillDepartmentAudit(int CustomerId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Department
                             where row.IsDeleted == false
                             && row.CustomerID == CustomerId
                             select row);
                var users = (from row in query
                             select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                return users;
            }
        }
        public static void DeleteDepartmentMasterAudit(int departmentid, long CustomerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                mst_Department DepartmentMastertoDelete = (from row in entities.mst_Department
                                                           where row.ID == departmentid
                                                           && row.CustomerID == CustomerID
                                                           select row).FirstOrDefault();

                DepartmentMastertoDelete.IsDeleted = true;
                entities.SaveChanges();
            }
        }
        public static bool DepartmentExistsAudit(mst_Department objDept)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Department
                             where row.Name.ToUpper().Equals(objDept.Name.ToUpper()) && row.IsDeleted == false
                             && row.CustomerID == objDept.CustomerID
                             select row);
                return query.Select(entry => true).SingleOrDefault();
            }
        }
        
        public static List<mst_Department> GetAllDepartmentMasterListAudit(long customerID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var DepartmentMasterList = (from row in entities.mst_Department
                                            where row.IsDeleted == false
                                            && row.CustomerID == customerID
                                            select row);
                return DepartmentMasterList.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static void CreateDepartmentMasterAudit(mst_Department mstdept)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                entities.mst_Department.Add(mstdept);
                entities.SaveChanges();
            }
        }
        public static void UpdateDepartmentMasterAudit(mst_Department mstdept)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                mst_Department mstdeptMasterToUpdate = (from row in entities.mst_Department
                                                        where row.ID == mstdept.ID
                                                        && row.IsDeleted == false
                                                        && row.CustomerID == mstdept.CustomerID
                                                        select row).FirstOrDefault();
                mstdeptMasterToUpdate.Name = mstdept.Name;
                mstdeptMasterToUpdate.Color = mstdept.Color;
                entities.SaveChanges();
            }
        }

        public static Department DepartmentMasterGetByIDAudit(int deptID, long customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var MstDepartmentMaster = (from row in entities.Departments
                                           where row.ID == deptID
                                            && row.CustomerID == customerID
                                           select row).FirstOrDefault();

                return MstDepartmentMaster;
            }
        }
        #endregion

        public static List<sp_ComplianceAssignedDepartment_Result> GetNewDEPTAll(int UserID, int CustomerBranchId, List<long> Branchlist, string isstatutoryinternal, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedDepartment_Result> complianceCategorys = new List<sp_ComplianceAssignedDepartment_Result>();
                if (approver == true)
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "MGMT", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "MGMT", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();
                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<sp_ComplianceAssignedDepartment_Result> GetNewAll(int UserID, int CustomerBranchId, List<long> Branchlist,string isstatutoryinternal, bool approver = false, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<sp_ComplianceAssignedDepartment_Result> complianceCategorys = new List<sp_ComplianceAssignedDepartment_Result>();
                if (approver == true)
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "APPR", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();

                    }
                }
                else
                {
                    if (Branchlist.Count > 0)
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "DEPT", isstatutoryinternal)
                                               where Branchlist.Contains((long)row.CustomerBranchID)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();
                    }
                    else
                    {
                        complianceCategorys = (from row in entities.sp_ComplianceAssignedDepartment(UserID, "DEPT", isstatutoryinternal)
                                               select row).GroupBy(g => g.Id).Select(a => a.FirstOrDefault()).ToList();


                    }
                    complianceCategorys = complianceCategorys.GroupBy(a => (int)a.Id).Select(a => a.FirstOrDefault()).ToList();

                }
                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static bool GetAuditDepartmentExists(mst_Department objDept)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_Department
                             where row.Name.ToUpper().Equals(objDept.Name.ToUpper()) && row.IsDeleted == false
                             && row.CustomerID == objDept.CustomerID
                               && row.Color == objDept.Color
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static bool GetDepartmentExists(Department deptmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Departments
                             where row.Name.ToUpper().Equals(deptmaster.Name.ToUpper()) && row.IsDeleted == false
                             && row.CustomerID == deptmaster.CustomerID
                             && row.Color == deptmaster.Color
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }
    }
}
