﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ProcessDetails
    {
        public long Id { get; set; }
        public string SubProcessName { get; set; }
        public long ProcessId { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
        public long? ParentID { get; set; }
        public decimal? Scores { get; set; }
    }

    public class ActivityDetails
    {        
        public string ActivityName { get; set; }        
        public string ProcessName { get; set; }
        public string SubProcessName { get; set; }
        public long ProcessId { get; set; }
                      
    }
}
