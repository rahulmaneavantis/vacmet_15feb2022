﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class InternalControlIFACTIVATEDManagementDashboardDepartMentHead
    {

        #region Audit Coverage
        public static DataTable GetAuditCoverageProcessWiseCustomer(int customerid,int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var transactionsQuery = (from row in entities.InternalControlAuditCoverageTransactionViews
                                         where row.CustomerId == customerid && row.DepartmentID == Departmentid
                                         select new
                                         {
                                             ProcessId = row.ProcessId,
                                             row.Rating,
                                         }).ToList();
                long highcount;
                long mediumCount;
                long lowcount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Total", typeof(long));
                var ProcessList = ProcessManagement.GetAllDepartmentProcess(Departmentid);
                foreach (Mst_Process cc in ProcessList)
                {
                    highcount = transactionsQuery.Where(entry => entry.Rating == 1 && entry.ProcessId == cc.Id).Count();
                    mediumCount = transactionsQuery.Where(entry => entry.Rating == 2 && entry.ProcessId == cc.Id).Count();
                    lowcount = transactionsQuery.Where(entry => entry.Rating == 3 && entry.ProcessId == cc.Id).Count();
                    totalcount = highcount + mediumCount + lowcount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, highcount, mediumCount, lowcount, totalcount);
                }
                return table;
            }
        }
        public static DataTable GetAuditCoverageProcessWiseCustomerBranch(int customerbranchid, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var transactionsQuery = (from row in entities.InternalControlAuditCoverageTransactionViews
                                         where row.CustomerBranchID == customerbranchid && row.DepartmentID == Departmentid
                                         select new
                                         {
                                             ProcessId = row.ProcessId,
                                             row.Rating,
                                         }).ToList();
                long highcount;
                long mediumCount;
                long lowcount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Total", typeof(long));
                var ProcessList = ProcessManagement.GetAllDepartmentProcess(Departmentid);
                foreach (Mst_Process cc in ProcessList)
                {
                    highcount = transactionsQuery.Where(entry => entry.Rating == 1 && entry.ProcessId == cc.Id).Count();
                    mediumCount = transactionsQuery.Where(entry => entry.Rating == 2 && entry.ProcessId == cc.Id).Count();
                    lowcount = transactionsQuery.Where(entry => entry.Rating == 3 && entry.ProcessId == cc.Id).Count();
                    totalcount = highcount + mediumCount + lowcount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, highcount, mediumCount, lowcount, totalcount);
                }
                return table;
            }
        }
        public static DataTable GetAuditCoverageSUBProcessWiseCustomer(int customerid, int ProcessId, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var transactionsQuery = (from row in entities.InternalControlAuditCoverageTransactionViews
                                         where row.CustomerId == customerid && row.ProcessId == ProcessId
                                         && row.DepartmentID == Departmentid
                                         select new
                                         {
                                             ProcessId = row.ProcessId,
                                             SubProcessId = row.SubProcessId,
                                             row.Rating,
                                         }).ToList();
                long highcount;
                long mediumCount;
                long lowcount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Total", typeof(long));

                //var ProcessList = GetProcessAll(ProcessId);
                var subprocessList = ProcessManagement.GetSubProcessAll(ProcessId, customerid);
                //foreach (Mst_Process cc in ProcessList)
                //{
                foreach (mst_Subprocess ss in subprocessList)
                {
                    highcount = transactionsQuery.Where(entry => entry.Rating == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == ss.ProcessId).Count();
                    mediumCount = transactionsQuery.Where(entry => entry.Rating == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == ss.ProcessId).Count();
                    lowcount = transactionsQuery.Where(entry => entry.Rating == 3 && entry.SubProcessId == ss.Id && entry.ProcessId == ss.ProcessId).Count();
                    totalcount = highcount + mediumCount + lowcount;

                    if (totalcount != 0)
                        table.Rows.Add(ss.Id, ss.Name, highcount, mediumCount, lowcount, totalcount);
                }
                //}        


                //var ProcessList = GetProcessAll();
                //var subprocessList = GetSubProcessAll();

                //foreach (Mst_Process cc in ProcessList)
                //{
                //    foreach (mst_Subprocess ss in subprocessList)
                //    {
                //        highcount = transactionsQuery.Where(entry => entry.Rating == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id).Count();
                //        mediumCount = transactionsQuery.Where(entry => entry.Rating == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id).Count();
                //        lowcount = transactionsQuery.Where(entry => entry.Rating == 3 && entry.SubProcessId == ss.Id && entry.ProcessId == cc.Id).Count();
                //        totalcount = highcount + mediumCount + lowcount;

                //        if (totalcount != 0)
                //            table.Rows.Add(cc.Id, ss.Name, highcount, mediumCount, lowcount, totalcount);
                //    }
                //}
                return table;
            }
        }
        public static DataTable GetAuditCoverageSUBProcessWiseCustomerBranch(int CustomerId,int customerbranchid, int ProcessId, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var transactionsQuery = (from row in entities.InternalControlAuditCoverageTransactionViews
                                         where row.CustomerBranchID == customerbranchid && row.ProcessId == ProcessId && row.DepartmentID == Departmentid
                                         select new
                                         {
                                             ProcessId = row.ProcessId,
                                             SubProcessId = row.SubProcessId,
                                             row.Rating,
                                         }).ToList();
                long highcount;
                long mediumCount;
                long lowcount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Total", typeof(long));
                
                var subprocessList = ProcessManagement.GetSubProcessAll(CustomerId,ProcessId);
                                
                foreach (mst_Subprocess ss in subprocessList)
                {
                    highcount = transactionsQuery.Where(entry => entry.Rating == 1 && entry.SubProcessId == ss.Id && entry.ProcessId == ss.ProcessId).Count();
                    mediumCount = transactionsQuery.Where(entry => entry.Rating == 2 && entry.SubProcessId == ss.Id && entry.ProcessId == ss.ProcessId).Count();
                    lowcount = transactionsQuery.Where(entry => entry.Rating == 3 && entry.SubProcessId == ss.Id && entry.ProcessId == ss.ProcessId).Count();
                    totalcount = highcount + mediumCount + lowcount;

                    if (totalcount != 0)
                        table.Rows.Add(ss.Id, ss.Name, highcount, mediumCount, lowcount, totalcount);
                }
                //}                
                return table;
            }
        }

        public static List<InternalControlAuditCoverageTransactionView> GetManagementDetailViewCustomer(int customerid, string financialyear, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, int Departmentid, int ProcessId)
        {
            List<InternalControlAuditCoverageTransactionView> detailView = new List<InternalControlAuditCoverageTransactionView>();
            if (filter.Equals("Process"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalControlAuditCoverageTransactionViews
                                      where (row.CustomerId == customerid && row.DepartmentID==Departmentid
                                      && statusIDs.Contains((int)row.Rating)) && row.ProcessId == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalControlAuditCoverageTransactionViews
                                      where (row.CustomerId == customerid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.Rating))
                                      select row).ToList();
                    }
                }
            }
            else if (filter.Equals("SubProcess"))
            {
                if (functionId != -1)
                {

                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalControlAuditCoverageTransactionViews
                                      where (row.CustomerId == customerid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.Rating)) && row.SubProcessId == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalControlAuditCoverageTransactionViews
                                      where (row.CustomerId == customerid && row.DepartmentID == Departmentid
                                       && row.ProcessId == ProcessId
                                      && statusIDs.Contains((int)row.Rating))
                                      select row).ToList();
                    }
                }
            }
            return detailView;
        }
        public static List<InternalControlAuditCoverageTransactionView> GetManagementDetailViewCustomerBranchId(int branchid, string financialyear, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, int Departmentid, int ProcessId)
        {
            List<InternalControlAuditCoverageTransactionView> detailView = new List<InternalControlAuditCoverageTransactionView>();
            if (filter.Equals("Process"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalControlAuditCoverageTransactionViews
                                      where (row.CustomerBranchID == branchid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.Rating)) && row.ProcessId == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalControlAuditCoverageTransactionViews
                                      where (row.CustomerBranchID == branchid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.Rating))
                                      select row).ToList();
                    }
                }
            }
            else if (filter.Equals("SubProcess"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalControlAuditCoverageTransactionViews
                                      where (row.CustomerBranchID == branchid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.Rating)) && row.SubProcessId == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalControlAuditCoverageTransactionViews
                                      where (row.CustomerBranchID == branchid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.Rating)) && row.ProcessId == ProcessId
                                      select row).ToList();
                    }                  
                }
            }

            return detailView;
        }
        #endregion

        
        
        public static List<Mst_ObservationCategory> GetObservationCategoryAll(int ProcessId, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var complianceCategorys = (from MOC in entities.Mst_ObservationCategory
                                           join IAITV in entities.InternalAuditInstanceTransactionViews
                                           on MOC.ID equals IAITV.ObservationCategory
                                           where IAITV.ProcessId == ProcessId && MOC.IsActive == false
                                           select MOC).ToList().Distinct();



                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }

        }
       
        //public static List<mst_Subprocess> GetSubProcessAll(string filter = null)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        var complianceCategorys = (from row in entities.mst_Subprocess
        //                                   //where row.IsDeleted == false
        //                                   select row);

        //        if (!string.IsNullOrEmpty(filter))
        //        {
        //            complianceCategorys = complianceCategorys.Where(entry => entry.Name.Contains(filter) || entry.Description.Contains(filter));
        //        }

        //        complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

        //        return complianceCategorys.OrderBy(entry => entry.Name).ToList();
        //    }
        //}

        #region Audit Status
        public static DataTable GetAuditStatusProcessCustomerWise(string FinancialYear, int customerid, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("Total", typeof(long));


                var ProcessList = ProcessManagement.GetAllDepartmentProcess(Departmentid);
                foreach (Mst_Process cc in ProcessList)
                {

                    NotDonecount = (from row in entities.GETNOTDONECOUNT_View
                                    where row.CustomerID == customerid && row.FinancialYear==FinancialYear && row.DepartmentID == Departmentid
                                          && row.ProcessId == cc.Id && row.AuditStatusID == null
                                    select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();


                    submitedcount = (from row in entities.GETNOTDONECOUNT_View
                                     where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 2
                                           && row.ProcessId == cc.Id && row.RoleID == 4 
                                     select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    TeamReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                       where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 4
                                              && row.ProcessId == cc.Id && row.RoleID == 4
                                        select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    AuditeeReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 6
                                             && row.ProcessId == cc.Id && row.RoleID == 4
                                       select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    FinalReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                        where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 5
                                             && row.ProcessId == cc.Id && row.RoleID == 4
                                       select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    closecount = (from row in entities.GETNOTDONECOUNT_View
                                  where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 3
                                        && row.ProcessId == cc.Id && row.RoleID == 4
                                  select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();


                    totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, totalcount);

                }

                return table;
            }
        }
        public static DataTable GetAuditStatusProcessBranchWise(string FinancialYear, int Branchid, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("Total", typeof(long));
                var ProcessList = ProcessManagement.GetAllDepartmentProcess(Departmentid);
                foreach (Mst_Process cc in ProcessList)
                {                   
                    NotDonecount = (from row in entities.GETNOTDONECOUNT_View
                                    where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && row.ProcessId == cc.Id && row.AuditStatusID == null
                                    select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();


                    submitedcount = (from row in entities.GETNOTDONECOUNT_View
                                     where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 2
                                           && row.ProcessId == cc.Id && row.RoleID == 4
                                     select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    TeamReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                       where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 4
                                              && row.ProcessId == cc.Id && row.RoleID == 4
                                        select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    AuditeeReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 6
                                             && row.ProcessId == cc.Id && row.RoleID == 4
                                       select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    FinalReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                        where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear  && row.DepartmentID == Departmentid && row.AuditStatusID == 5
                                             && row.ProcessId == cc.Id && row.RoleID == 4
                                       select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    closecount = (from row in entities.GETNOTDONECOUNT_View
                                  where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 3
                                        && row.ProcessId == cc.Id && row.RoleID == 4
                                  select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    totalcount = submitedcount + NotDonecount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, totalcount);
                }
                return table;
            }
        }
        public static DataTable GetAuditStatusSubProcessCustomerWise(string FinancialYear, int customerid, int ProcessId, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("Total", typeof(long));

                var subprocessList = ProcessManagement.GetSubProcessAll(customerid,ProcessId);
                foreach (mst_Subprocess cc in subprocessList)
                {                  
                    NotDonecount = (from row in entities.GETNOTDONECOUNT_View
                                    where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.AuditStatusID == null
                                    select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    submitedcount = (from row in entities.GETNOTDONECOUNT_View
                                     where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 2
                                           && row.ProcessId == cc.ProcessId && row.FinancialYear == FinancialYear && row.SubProcessId == cc.Id && row.RoleID == 4
                                     select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    TeamReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                       where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 4
                                              && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                        select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    AuditeeReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 6
                                             && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                       select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    FinalReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                        where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 5
                                             && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                       select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    closecount = (from row in entities.GETNOTDONECOUNT_View
                                  where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 3
                                        && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                  select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();


                    totalcount = submitedcount + NotDonecount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, totalcount);

                }
                return table;
            }
        }
        public static DataTable GetAuditStatusSubProcessBranchWise(int Customerid,string FinancialYear, int Branchid, int ProcessId, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("Total", typeof(long));

                var subprocessList = ProcessManagement.GetSubProcessAll(Customerid,ProcessId);
                foreach (mst_Subprocess cc in subprocessList)
                {
                    NotDonecount = (from row in entities.GETNOTDONECOUNT_View
                                    where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                           && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.AuditStatusID == null
                                    select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();


                    submitedcount = (from row in entities.GETNOTDONECOUNT_View
                                     where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 2
                                           && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                     select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    TeamReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                       where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 4
                                              && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                        select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    AuditeeReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 6
                                             && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                       select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();

                    FinalReviewcount = (from row in entities.GETNOTDONECOUNT_View
                                        where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 5
                                             && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                       select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();


                    closecount = (from row in entities.GETNOTDONECOUNT_View
                                  where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.AuditStatusID == 3
                                        && row.ProcessId == cc.ProcessId && row.SubProcessId == cc.Id && row.RoleID == 4
                                  select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).Count();
                    totalcount = submitedcount + NotDonecount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, totalcount);

                }
                return table;
            }
        }
        public static List<GETNOTDONECOUNT_View> GetAuditStatusDetailViewCustomer1(int customerid, string FinancialYear, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, string filter1, int Departmentid, int ProcessId)
        {
            List<GETNOTDONECOUNT_View> detailView = new List<GETNOTDONECOUNT_View>();
            if (filter.Equals("AuditStatus"))
            {
                if (filter1.Equals("NotDone"))
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && row.AuditStatusID == null && row.ProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                           && row.AuditStatusID == null
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }
                else if (filter1.Equals("Total"))
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID) || row.AuditStatusID == null) && row.ProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID) || row.AuditStatusID == null)
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }
                else
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID)) && row.ProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID))
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }

            }
            else if (filter.Equals("AuditStatusSupProcess"))
            {
                if (filter1.Equals("NotDone"))
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && row.AuditStatusID == null && row.SubProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.ProcessId == ProcessId && row.DepartmentID == Departmentid
                                          && row.AuditStatusID == null
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }
                else if (filter1.Equals("Total"))
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID) || row.AuditStatusID == null) && row.SubProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.ProcessId == ProcessId && row.DepartmentID == Departmentid
                                         && (statusIDs.Contains((int)row.AuditStatusID) || row.AuditStatusID == null)
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }
                else
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID)) && row.SubProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where (row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.ProcessId == ProcessId && row.DepartmentID == Departmentid
                                          && statusIDs.Contains((int)row.AuditStatusID))
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }

                }

            }
            return detailView;
        }
        public static List<GETNOTDONECOUNT_View> GetAuditStatusDetailViewCustomerBranchId1(int branchid, string FinancialYear, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, string filter1, int Departmentid, int ProcessId)
        {
            List<GETNOTDONECOUNT_View> detailView = new List<GETNOTDONECOUNT_View>();            
            if (filter.Equals("AuditStatus"))
            {
                if (filter1.Equals("NotDone"))
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && row.AuditStatusID == null && row.ProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                           && row.AuditStatusID == null
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }
                else if (filter1.Equals("Total"))
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID) || row.AuditStatusID == null) && row.ProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID) || row.AuditStatusID == null)
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }
                else
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID)) && row.ProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID))
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }

            }
            else if (filter.Equals("AuditStatusSupProcess"))
            {
                if (filter1.Equals("NotDone"))
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && row.AuditStatusID == null && row.SubProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.ProcessId == ProcessId && row.DepartmentID == Departmentid
                                          && row.AuditStatusID == null
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }
                else if (filter1.Equals("Total"))
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID) || row.AuditStatusID == null) && row.SubProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.ProcessId == ProcessId && row.DepartmentID == Departmentid
                                         && (statusIDs.Contains((int)row.AuditStatusID) || row.AuditStatusID == null)
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                }
                else
                {
                    if (functionId != -1)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                          && (statusIDs.Contains((int)row.AuditStatusID)) && row.SubProcessId == functionId
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            detailView = (from row in entities.GETNOTDONECOUNT_View
                                          where (row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.ProcessId == ProcessId && row.DepartmentID == Departmentid
                                          && statusIDs.Contains((int)row.AuditStatusID))
                                          select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }

                }

            }           
            return detailView;
        }
        public static List<InternalAuditInstanceTransactionView> GetAuditStatusDetailViewCustomer(int customerid, string FinancialYear, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, int Departmentid)
        {
            List<InternalAuditInstanceTransactionView> detailView = new List<InternalAuditInstanceTransactionView>();
            if (filter.Equals("AuditStatus"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalAuditInstanceTransactionViews
                                      where (row.CustomerID == customerid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.AuditStatusID)) && row.ProcessId == functionId
                                      select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalAuditInstanceTransactionViews
                                      where (row.CustomerID == customerid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.AuditStatusID))
                                      select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            else if (filter.Equals("AuditStatusSupProcess"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalAuditInstanceTransactionViews
                                      where (row.CustomerID == customerid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.AuditStatusID)) && row.SubProcessId == functionId
                                      select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalAuditInstanceTransactionViews
                                      where (row.CustomerID == customerid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.AuditStatusID))
                                      select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }

            return detailView;
        }
        public static List<InternalAuditInstanceTransactionView> GetAuditStatusDetailViewCustomerBranchId(int branchid, string FinancialYear, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, int Departmentid)
        {
            List<InternalAuditInstanceTransactionView> detailView = new List<InternalAuditInstanceTransactionView>();
            if (filter.Equals("AuditStatus"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalAuditInstanceTransactionViews
                                      where (row.CustomerBranchID == branchid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.AuditStatusID)) && row.ProcessId == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalAuditInstanceTransactionViews
                                      where (row.CustomerBranchID == branchid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.AuditStatusID))
                                      select row).ToList();
                    }
                }
            }
            else if (filter.Equals("AuditStatusSupProcess"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalAuditInstanceTransactionViews
                                      where (row.CustomerBranchID == branchid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.AuditStatusID)) && row.SubProcessId == functionId
                                      select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.InternalAuditInstanceTransactionViews
                                      where (row.CustomerBranchID == branchid && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.AuditStatusID))
                                      select row).ToList().GroupBy(entity => entity.ATBDId).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                }
            }
            return detailView;
        }
      
        #endregion

        #region Observation Status

        public static List<ObservationStatus_DisplayView> GetObservationDetailViewCustomer(int customerid, string FinancialYear, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, int Departmentid, int ProcessId)
        {
            List<ObservationStatus_DisplayView> detailView = new List<ObservationStatus_DisplayView>();
            if (filter.Equals("ObservationStatus"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.ObservationStatus_DisplayView
                                      where (row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.ObservatioRating)) && row.ProcessId == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.ObservationStatus_DisplayView
                                      where (row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.ObservatioRating))
                                      select row).ToList();
                    }
                }
            }
            else if (filter.Equals("ObservationStatusCategory"))
            {

                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.ObservationStatus_DisplayView
                                      where (row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.ObservatioRating)) && row.ObservationCategory == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.ObservationStatus_DisplayView
                                      where (row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.ObservatioRating))
                                      select row).ToList();
                    }
                }
            }
            return detailView;
        }
        public static List<ObservationStatus_DisplayView> GetObservationDetailViewCustomerBranch(int branchid, string FinancialYear, List<int> statusIDs, List<int?> statusNullableIDs, string filter, int? functionId, int Departmentid, int ProcessId)
        {
            List<ObservationStatus_DisplayView> detailView = new List<ObservationStatus_DisplayView>();
            if (filter.Equals("ObservationStatus"))
            {
                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.ObservationStatus_DisplayView
                                      where (row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.ObservatioRating)) && row.ProcessId == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.ObservationStatus_DisplayView
                                      where (row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.ObservatioRating))
                                      select row).ToList();
                    }
                }
            }
            else if (filter.Equals("ObservationStatusCategory"))
            {

                if (functionId != -1)
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.ObservationStatus_DisplayView
                                      where (row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.ObservatioRating)) && row.ObservationCategory == functionId
                                      select row).ToList();
                    }
                }
                else
                {
                    using (AuditControlEntities entities = new AuditControlEntities())
                    {
                        detailView = (from row in entities.ObservationStatus_DisplayView
                                      where (row.CustomerBranchID == branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid
                                      && statusIDs.Contains((int)row.ObservatioRating))
                                      select row).ToList();
                    }
                }
            }


            return detailView;
        }
        public static DataTable GetObservationStatusCustomerWise(string FinancialYear, int customerid, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long Highcount;
                long Mediumcount;
                long Lowcount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Total", typeof(long));
                var ProcessList = ProcessManagement.GetAllDepartmentProcess(Departmentid);
                foreach (Mst_Process cc in ProcessList)
                {
                    Highcount = (from row in entities.ObservationStatus_DisplayView
                                 where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 1
                                       && row.ProcessId == cc.Id
                                 select row).Count();

                    Mediumcount = (from row in entities.ObservationStatus_DisplayView
                                   where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 2
                                              && row.ProcessId == cc.Id
                                   select row).Count();

                    Lowcount = (from row in entities.ObservationStatus_DisplayView
                                where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 3
                                        && row.ProcessId == cc.Id
                                select row).Count();


                    totalcount = Highcount + Mediumcount + Lowcount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, Highcount, Mediumcount, Lowcount, totalcount);

                }
                return table;
            }
        }
        public static DataTable GetObservationStatusBranchWise(string FinancialYear, int Branchid, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                long Highcount;
                long Mediumcount;
                long Lowcount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Total", typeof(long));
                var ProcessList = ProcessManagement.GetAllDepartmentProcess(Departmentid);
                foreach (Mst_Process cc in ProcessList)
                {
                    Highcount = (from row in entities.ObservationStatus_DisplayView
                                 where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 1
                                       && row.ProcessId == cc.Id
                                 select row).Count();

                    Mediumcount = (from row in entities.ObservationStatus_DisplayView
                                   where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 2
                                              && row.ProcessId == cc.Id
                                   select row).Count();

                    Lowcount = (from row in entities.ObservationStatus_DisplayView
                                where row.CustomerBranchID == Branchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 3
                                        && row.ProcessId == cc.Id
                                select row).Count();

                   
                    totalcount = Highcount + Mediumcount + Lowcount;

                    if (totalcount != 0)
                        table.Rows.Add(cc.Id, cc.Name, Highcount, Mediumcount, Lowcount, totalcount);

                }
                return table;
            }
        }
        public static DataTable GetObservationStatusSUBProcessWiseCustomer(string FinancialYear, int customerid, int ProcessId, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long highcount;
                long mediumCount;
                long lowcount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Total", typeof(long));



                var subprocessList = GetObservationCategoryAll(ProcessId);
                foreach (Mst_ObservationCategory ss in subprocessList)
                {

                    highcount = (from row in entities.ObservationStatus_DisplayView
                                 where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 1
                                       && row.ProcessId == ProcessId && row.ObservationCategory == ss.ID
                                 select row).Count();

                    mediumCount = (from row in entities.ObservationStatus_DisplayView
                                   where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 2
                                              && row.ProcessId == ProcessId && row.ObservationCategory == ss.ID
                                   select row).Count();

                    lowcount = (from row in entities.ObservationStatus_DisplayView
                                where row.CustomerID == customerid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 3
                                        && row.ProcessId == ProcessId && row.ObservationCategory == ss.ID
                                select row).Count();

                    totalcount = highcount + mediumCount + lowcount;

                    if (totalcount != 0)
                        table.Rows.Add(ss.ID, ss.Name, highcount, mediumCount, lowcount, totalcount);
                }                    
                return table;
            }
        }
        public static DataTable GetObservationStatusSUBProcessWiseCustomerBranch(string FinancialYear, int customerbranchid, int ProcessId, int Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long highcount;
                long mediumCount;
                long lowcount;
                long totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Total", typeof(long));

                var subprocessList = GetObservationCategoryAll(ProcessId);
                foreach (Mst_ObservationCategory ss in subprocessList)
                {

                    highcount = (from row in entities.ObservationStatus_DisplayView
                                 where row.CustomerBranchID == customerbranchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 1
                                       && row.ProcessId == ProcessId && row.ObservationCategory == ss.ID
                                 select row).Count();

                    mediumCount = (from row in entities.ObservationStatus_DisplayView
                                   where row.CustomerBranchID == customerbranchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 2
                                              && row.ProcessId == ProcessId && row.ObservationCategory == ss.ID
                                   select row).Count();

                    lowcount = (from row in entities.ObservationStatus_DisplayView
                                where row.CustomerBranchID == customerbranchid && row.FinancialYear == FinancialYear && row.DepartmentID == Departmentid && row.ObservatioRating == 3
                                        && row.ProcessId == ProcessId && row.ObservationCategory == ss.ID
                                select row).Count();

                    totalcount = highcount + mediumCount + lowcount;

                    if (totalcount != 0)
                        table.Rows.Add(ss.ID, ss.Name, highcount, mediumCount, lowcount, totalcount);
                }                                  
                return table;
            }
        }
        #endregion
    }
}
