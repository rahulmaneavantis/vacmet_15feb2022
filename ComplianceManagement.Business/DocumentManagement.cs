﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.IO;
using System.Text.RegularExpressions;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class DocumentManagement
    {
        public static long CreateContractTrans(Cont_tbl_ContractTransaction ContractDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Cont_tbl_ContractTransaction.Add(ContractDocument);
                entities.SaveChanges();
                if (ContractDocument.ID > 0)
                    return ContractDocument.ID;
                else
                    return 0;
            }
        }
        public static long CreateContractAssignment(ContractAssignment ContractDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ContractAssignment fileData = (from row in entities.ContractAssignments
                                               where row.UserID == ContractDocument.UserID
                                                && row.RoleID == ContractDocument.RoleID
                                                && row.ContractInstanceID == ContractDocument.ContractInstanceID
                                               select row).FirstOrDefault();
                if (fileData == null)
                {
                    entities.ContractAssignments.Add(ContractDocument);
                    entities.SaveChanges();
                    if (ContractDocument.ID > 0)
                        return ContractDocument.ID;
                    else
                        return 0;
                }
                else
                    return 0;
            }
        }
        public static bool DeleteAllTempContractDocumentFile(long UserID, long CustID)
        {
            bool deleteSuccess = false;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objRecord = (from row in entities.TempContractDocuments
                                 where row.UserID == UserID
                                 && row.CustomerID == CustID
                                 select row).ToList();

                if (objRecord != null)
                {
                    objRecord.ForEach(entry =>
                            entities.TempContractDocuments.Remove(entry));
                    entities.SaveChanges();
                    deleteSuccess = true;
                }

                return deleteSuccess;
            }
        }
        public static long CreateContractDoc(ContractDocument ContractDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ContractDocuments.Add(ContractDocument);
                entities.SaveChanges();
                if (ContractDocument.ID > 0)
                {
                    return ContractDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static long CreateTempContractDocument(TempContractDocument tempComplianceDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TempContractDocuments.Add(tempComplianceDocument);
                entities.SaveChanges();
                if (tempComplianceDocument.ID > 0)
                {
                    return tempComplianceDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static long CreateContractDocument(ContractDocument tempComplianceDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ContractDocuments.Add(tempComplianceDocument);
                entities.SaveChanges();
                if (tempComplianceDocument.ID > 0)
                {
                    return tempComplianceDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static List<tbl_ContractTermSheetFileData> GetContractTermSheetDocumentData(long UserID, long CustID, long termsheetno)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.tbl_ContractTermSheetFileData
                                where row.TermSheetNo == termsheetno
                                && row.IsDeleted == false
                                select row).ToList();

                return fileData;
            }
        }
        public static List<ContractDocument> GetContractDocumentData(long UserID, long CustID, long ContId)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.ContractDocuments
                                where row.ContractID == ContId
                                && row.IsfromComment == false
                                select row).ToList();

                return fileData;
            }
        }
        public static List<TempContractDocument> GetTempContractDocumentData(long UserID, long CustID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.TempContractDocuments
                                where row.UserID == UserID
                                && row.CustomerID == CustID
                                select row).ToList();

                return fileData;
            }
        }
        /// <summary>
        /// ////////////////////////
        /// </summary>
        /// <param name="ScheduledOnID"></param>
        /// <param name="FileID"></param>
        /// <returns></returns>
        public static List<GetInternalComplianceDocumentsView> GetFileDataInternalNew(int ScheduledOnID, int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetInternalComplianceDocumentsViews
                                where row.InternalComplianceScheduledOnID == ScheduledOnID && row.FileID == FileID
                                select row).ToList();

                return fileData;
            }
        }

        public static List<GetInternalComplianceDocumentsView> GetInternalFileDataVersionWise(int ScheduledOnID, string Version)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetInternalComplianceDocumentsViews
                                where row.InternalComplianceScheduledOnID == ScheduledOnID
                                && row.Version == Version
                                select row).ToList();

                return fileData;
            }
        }
        public static List<CheckListInstanceTransactionView> NewGetPeriodBranchLocationChecklist(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.CheckListInstanceTransactionViews
                             where row.ScheduledOnID == scheduledOnID
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).ToList();

                return query;
            }
        }

        public static string GetSequenceID(int complianceInstanceID)
        {
            string ans = string.Empty;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                ans = (from row in entities.ComplianceInstances
                       where row.ID == complianceInstanceID
                       select row.SequenceID).FirstOrDefault();
            }
            return ans;

        }
        public static List<TempComplianceDocument> GetTempCheckListComplianceDocumentData(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.TempComplianceDocuments
                                where row.ScheduleOnID == ScheduledOnID
                                && (row.DocType == "C")
                                select row).ToList();

                return fileData;
            }
        }
        public static List<TempComplianceDocument> GetTempComplianceDocumentData(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.TempComplianceDocuments
                                where row.ScheduleOnID == ScheduledOnID
                                && (row.DocType == "S" || row.DocType == "W")
                                select row).ToList();

                return fileData;
            }
        }
        public static List<TempComplianceDocument> GetTempInternalCheckListDocumentData(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.TempComplianceDocuments
                                where row.ScheduleOnID == ScheduledOnID
                                && (row.DocType == "IC")
                                select row).ToList();

                return fileData;
            }
        }
        public static List<TempComplianceDocument> GetTempTaskDocumentData(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.TempComplianceDocuments
                                where row.ScheduleOnID == ScheduledOnID
                                && row.DocType == "T"
                                select row).ToList();

                return fileData;
            }
        }
        public static List<TempComplianceDocument> GetTempInternalComplianceDocumentData(long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.TempComplianceDocuments
                                where row.ScheduleOnID == ScheduledOnID
                                && (row.DocType == "IS" || row.DocType == "IW")
                                select row).ToList();

                return fileData;
            }
        }
        public static long CreateTempComplinaceDocument(TempComplianceDocument tempComplianceDocument)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.TempComplianceDocuments.Add(tempComplianceDocument);
                entities.SaveChanges();
                if (tempComplianceDocument.ID > 0)
                {
                    return tempComplianceDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "").Replace("(", "").Replace(")", "");

            return replace;
        }
        public static List<GetComplianceDocumentsView> GetFileDataView(int ScheduledOnID, int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetComplianceDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID && row.FileID == FileID
                                select row).ToList();

                return fileData;
            }
        }
        
        public static List<GetInternalComplianceDocumentsView> GetFileDataInternalView(int ScheduledOnID, int FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetInternalComplianceDocumentsViews
                                where row.InternalComplianceScheduledOnID == ScheduledOnID
                                && row.FileID == FileID
                                select row).ToList();

                return fileData;
            }
        }
        
        public static bool SaveReviseComplianceInternals(List<InternalFileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, byte[]>> filesList, int ScheduleOnID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                       
                        var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                        if (AWSData == null)
                        {
                            Internal_SaveDocFiles(filesList);
                        }


                        long transactionID = entities.InternalFileDataMappings.Where(entry => entry.InternalScheduledOnID == ScheduleOnID).FirstOrDefault().TransactionID;
                        foreach (InternalFileData fl in files)
                        {
                            fl.IsDeleted = false;
                            fl.EnType = "A";
                            entities.InternalFileDatas.Add(fl);
                            entities.SaveChanges();

                            InternalFileDataMapping fileMapping = new InternalFileDataMapping();
                            fileMapping.TransactionID = transactionID;
                            fileMapping.FileID = fl.ID;
                            fileMapping.InternalScheduledOnID = ScheduleOnID;
                            if (list != null)
                            {
                                fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                            }
                            entities.InternalFileDataMappings.Add(fileMapping);

                        }
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();                      
                        if (filesList != null)
                        {
                            foreach (var dfile in filesList)
                            {
                                if (System.IO.File.Exists(dfile.Key))
                                {
                                    System.IO.File.Delete(dfile.Key);
                                }
                            }
                        }
                        return false;
                    }
                }
            }
        }        
        public static string GetDocumnetVersionInternal(int InternalScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.InternalComplianceDocumentsViews
                                where row.InternalScheduledOnID == InternalScheduledOnID
                                select row).GroupBy(entry => entry.FileID).Select(en => en.FirstOrDefault()).ToList();

                if (fileData.Count == 0 || fileData == null)
                {
                    return "1.0";
                }
                else
                {
                    var version = fileData.OrderByDescending(entry => entry.FileID).Take(1).FirstOrDefault();
                    int lastVersion = 0;
                    if (version.Version == null)
                    {
                        return "2.0";
                    }
                    else
                    {
                        lastVersion = Convert.ToInt32(version.Version.Split('.')[0]);
                        lastVersion += 1;
                        return lastVersion + ".0";
                    }
                }
            }
        }      
        public static List<ComplianceDocumentsView> GetAll(int? customerID, string Role, int userID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var documentData = (from row in entities.ComplianceDocumentsViews
                                    where row.CustomerID == customerID
                                    select row).ToList();
                if (!Role.Equals("CADMN"))
                {
                    documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                }

                documentData = documentData.Where(entry => entry.RoleID == 3).ToList();

                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }
        public static List<SP_GetComplianceDocument_Result> GetFilteredComplianceDocuments(int Customerid, int userID, String Role, int Roleid, int Risk, DocumentFilterNewStatus status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, int SubTypeID, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetComplianceDocument(Customerid)
                                    select row).ToList();

                if (RoleID != 8 && RoleID != 9)
                {
                    var GetApprover = (from row in entities.ComplianceAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    if (Roleid != 0)
                        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignments
                                    on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                    where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();


                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                if (ComType != -1)
                    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                if (SubTypeID != -1)
                    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }


                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }
        public static List<SP_GetCheckListDocument_Result> GetFilteredCheckListDocuments(int Customerid, int userID, String Role, int Roleid, int Risk, DocumentFilterNewStatus status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetCheckListDocument(Customerid)
                                    select row).ToList();

                if (RoleID != 8 && RoleID != 9)
                {
                    var GetApprover = (from row in entities.ComplianceAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    if (Roleid != 0)
                        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignments
                                    on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                    where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();


                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                if (ComType != -1)
                    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                //if (SubTypeID != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }


                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }
        public static List<SP_GetCheckListReviewerDocument_Result> GetFilteredCheckListReviewerDocuments(int Customerid, int userID, String Role, int Roleid, int Risk, DocumentFilterNewStatus status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetCheckListReviewerDocument(Customerid)
                                    select row).ToList();

                if (RoleID != 8 && RoleID != 9)
                {
                    var GetApprover = (from row in entities.ComplianceAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    if (Roleid != 0)
                        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignments
                                    on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                    where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();


                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                if (ComType != -1)
                    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                //if (SubTypeID != -1)
                //    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }


                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }        
        public static List<SP_GetEventComplianceDocument_Result> GetFilteredEventComplianceDocuments(int Customerid, int userID, String Role, int Roleid, int Risk, DocumentFilterNewStatus status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, int EventID, int EventSchedueID, int SubTypeID, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetEventComplianceDocument(Customerid)
                                    select row).ToList();

                if (RoleID != 8 && RoleID != 9)
                {
                    var GetApprover = (from row in entities.ComplianceAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    if (Roleid != 0)
                        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignments
                                    on (long) row.ComplianceCategoryId equals row1.ComplianceCatagoryID
                                    where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }

                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();


                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.ComplianceStatusID == 6).ToList();
                        break;
                }

                if (ComType != -1)
                    documentData = documentData.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    documentData = documentData.Where(entry => entry.ActID == Act).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                if (EventID != -1)
                    documentData = documentData.Where(entry => entry.EventID == EventID).ToList();

                if (EventSchedueID != -1)
                    documentData = documentData.Where(entry => entry.EventScheduleOnID == EventSchedueID).ToList();

                if (SubTypeID != -1)
                    documentData = documentData.Where(entry => entry.ComplianceSubTypeID == SubTypeID).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }        
        public static List<SP_GetInternalComplianceDocument_Result> GetFilteredInternalComplianceDocuments(int Customerid, int userID, String Role, int Roleid, int Risk, DocumentFilterNewStatus status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetInternalComplianceDocument(Customerid)
                                    select row).ToList();


                if (RoleID != 8 && RoleID != 9)
                {
                    var GetApprover = (from row in entities.InternalComplianceAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    if (Roleid != 0)
                        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignmentInternals
                                    on (long) row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                    where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }


                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();

                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;
                }

                if (ComType != -1)
                    documentData = documentData.Where(entry => entry.IComplianceTypeID == ComType).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.IComplianceCategoryID == ComCategory).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.InternalScheduledOn >= StartDate && entry.InternalScheduledOn <= EndDate)).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }


                return documentData.GroupBy(entry => entry.InternalComplianceScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }
        public static List<SP_GetInternalCheckListComplianceDocument_Result> GetFilteredInternalCheckListComplianceDocuments(int Customerid, int userID, String Role, int Roleid, int Risk, DocumentFilterNewStatus status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate, string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long RoleID = (from row in entities.Users
                               where row.ID == userID
                               select row.RoleID).Single();

                var documentData = (from row in entities.SP_GetInternalCheckListComplianceDocument(Customerid)
                                    select row).ToList();


                if (RoleID != 8 && RoleID != 9)
                {
                    var GetApprover = (from row in entities.InternalComplianceAssignments
                                       where row.UserID == userID
                                       select row)
                                       .GroupBy(a => a.RoleID)
                                       .Select(a => a.FirstOrDefault())
                                       .Select(entry => entry.RoleID).ToList();

                    if (GetApprover.Count == 1 && GetApprover.Contains(6))
                        Roleid = 6;

                    if (!Role.Equals("CADMN"))
                    {
                        documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                    }

                    if (Roleid != 0)
                        documentData = documentData.Where(entry => entry.RoleID == Roleid).ToList();
                }
                else
                {
                    documentData = (from row in documentData
                                    join row1 in entities.EntitiesAssignmentInternals
                                    on (long) row.IComplianceCategoryID equals row1.ComplianceCatagoryID
                                    where row.CustomerID == Customerid && row.CustomerBranchID == row1.BranchID
                                    && row1.UserID == userID
                                    select row).Distinct().ToList();
                }


                if (Risk != -1)
                    documentData = documentData.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Entity/Sub-Entity/Location")
                    documentData = documentData.Where(entry => entry.Branch == location).ToList();

                switch (status)
                {
                    case DocumentFilterNewStatus.ClosedTimely:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatus.ClosedDelayed:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatus.PendingForReview:
                        documentData = documentData.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11)).ToList();
                        break;

                    case DocumentFilterNewStatus.Rejected:
                        documentData = documentData.Where(entry => entry.InternalComplianceStatusID == 6).ToList();
                        break;
                }

                if (ComType != -1)
                    documentData = documentData.Where(entry => entry.IComplianceTypeID == ComType).ToList();

                if (ComCategory != -1)
                    documentData = documentData.Where(entry => entry.IComplianceCategoryID == ComCategory).ToList();

                if (StartDate != null && EndDate != null)
                    documentData = documentData.Where(entry => (entry.InternalScheduledOn >= StartDate && entry.InternalScheduledOn <= EndDate)).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    documentData = documentData.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }


                return documentData.GroupBy(entry => entry.InternalComplianceScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }        
        public static string GetUserName(long complianceinstanceid,int roleID, string ComType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ComType == "S")
                {                   
                    var transactionsQuery = (from row in entities.ComplianceAssignments
                                             join cm in entities.Users on row.UserID equals cm.ID
                                             where row.ComplianceInstanceID == complianceinstanceid
                                             && row.RoleID == roleID
                                             select cm.FirstName + " " + cm.LastName).FirstOrDefault();
                    return transactionsQuery;
                }
                else if (ComType == "I")
                {                    
                    var transactionsQuery = (from row in entities.InternalComplianceAssignments
                                             join cm in entities.Users on row.UserID equals cm.ID
                                             where row.InternalComplianceInstanceID == complianceinstanceid
                                             && row.RoleID == roleID
                                             select cm.FirstName + " " + cm.LastName).FirstOrDefault();
                    return transactionsQuery;
                }
                else
                    return String.Empty;
            }
        }
        public static int GetCompliancetypeID(long complinaceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var transactionsQuery = (from row in entities.Compliances
                                         where row.ID == complinaceid  
                                         && row.IsDeleted==false
                                         && row.Status ==null                                       
                                         select (int)row.ComplianceType).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static string GetPeriod(long scheduledonid, long complianceinstanceid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var transactionsQuery = (from row in entities.ComplianceScheduleOns
                                         where
                                          row.ComplianceInstanceID == complianceinstanceid
                                         && row.ID == scheduledonid
                                         select row.ForMonth).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static List<SP_ReviseTransactionDocument_Result> GetFileData(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.SP_ReviseTransactionDocument(ScheduledOnID)                               
                                select row).ToList();

                return fileData;
            }
        }
        public static List<GetComplianceDocumentsView> GetFileData1(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetComplianceDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID
                                select row).ToList();

                return fileData;
            }
        }

        public static List<GetComplianceDocumentsView> GetFileDataVersionWise(int ScheduledOnID,string Version)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetComplianceDocumentsViews
                                where row.ScheduledOnID == ScheduledOnID
                                && row.Version == Version
                                select row).ToList();

                return fileData;
            }
        }
        public static List<GetInternalComplianceDocumentsView> GetInternalFileData1(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetInternalComplianceDocumentsViews
                                where row.InternalComplianceScheduledOnID == ScheduledOnID
                                select row).ToList();

                return fileData;
            }
        }
        public static ComplianceInstance GetComplianceInstanceData(int instanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.ComplianceInstances
                                    where row.ID == instanceID && row.IsDeleted == false
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }       
        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {                                   
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void Statutory_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void Vendor_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void Task_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void Critical_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void Internal_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void Contract_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void Litigation_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void Audit_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
        public static void IFC_SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        Writer.Write(CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }
                }
            }
        }
      

        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }
        public static void CreateDirectory(string directoryName)
        {
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);
        }     
        public static string GetDocumnetVersion(int scheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.ComplianceDocumentsViews
                                where row.ScheduledOnID == scheduledOnID
                                select row).GroupBy(entry => entry.FileID).Select(en => en.FirstOrDefault()).ToList();

                if (fileData.Count == 0 || fileData == null)
                {
                    return "1.0";
                }
                else
                {
                    var version = fileData.OrderByDescending(entry => entry.FileID).Take(1).FirstOrDefault();
                    int lastVersion = 0;
                    if (version.Version == null)
                    {
                        return "2.0";
                    }
                    else
                    {
                        lastVersion = Convert.ToInt32(version.Version.Split('.')[0]);
                        lastVersion += 1;
                        return lastVersion + ".0";
                    }
                }
            }


        }
    
        public static bool SaveReviseCompliances(List<FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, byte[]>> filesList, int ScheduleOnID,int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                        if (AWSData == null)
                        {
                            Statutory_SaveDocFiles(filesList);
                        }
                        long transactionID = entities.FileDataMappings.Where(entry => entry.ScheduledOnID == ScheduleOnID).FirstOrDefault().TransactionID;
                        foreach (FileData fl in files)
                        {
                            fl.IsDeleted = false;
                            fl.EnType = "A";
                            entities.FileDatas.Add(fl);
                            entities.SaveChanges();

                            FileDataMapping fileMapping = new FileDataMapping();
                            fileMapping.TransactionID = transactionID;
                            fileMapping.FileID = fl.ID;
                            fileMapping.ScheduledOnID = ScheduleOnID;
                            if (list != null)
                            {
                                fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                            }
                            entities.FileDataMappings.Add(fileMapping);

                        }
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;
                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        //entities.Connection.Close();

                        if (filesList != null)
                        {
                            foreach (var dfile in filesList)
                            {
                                if (System.IO.File.Exists(dfile.Key))
                                {
                                    System.IO.File.Delete(dfile.Key);
                                }
                            }

                        }
                        return false;
                    }
                }
            }
        }
        public static int GetComplianceTypeID(int ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.ComplianceInstances
                                      join row1 in entities.ComplianceScheduleOns
                                      on row.ID equals row1.ComplianceInstanceID
                                      join row3 in entities.Compliances 
                                      on row.ComplianceId equals row3.ID
                                      where row1.ID == ScheduleOnID
                                      select (int)row3.ComplianceType).Distinct().FirstOrDefault();
                if (ScheduleOnData != null)
                {
                    return ScheduleOnData;
                }
                else
                {
                    return  0;
                }
            }
        }

        public static ComplianceScheduleOn GetForMonthFromScheduleon(int SID)
        {
            ComplianceScheduleOn ScheduleOnData = new ComplianceScheduleOn();
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                     ScheduleOnData = (from row in entities.ComplianceScheduleOns
                                                           where row.ID == SID
                                                           select row).FirstOrDefault();
                    return ScheduleOnData;
                }
            }
            catch (Exception ex)
            {

                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "DocumentManagement";
                msg.FunctionName = "GetForMonthFromScheduleon";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);
                return ScheduleOnData;
            }
           
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();

                });
            }

        }
        public static ComplianceInstanceTransactionView GetForMonth(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceInstanceTransactionView ScheduleOnData = (from row in entities.ComplianceInstanceTransactionViews
                                                                    where row.ScheduledOnID == ScheduledOnID
                                                                    select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {
                    var complianceSchedule = ComplianceManagement.GetScheduleByComplianceID(ScheduleOnData.ComplianceID);

                    if (complianceSchedule.Count > 0)// added by rahul on 11 Jan 2016 for download one time document error
                    {
                        var objCompliance = ComplianceManagement.GetByID(ScheduleOnData.ComplianceID);

                        string spacialDate = ScheduleOnData.ScheduledOn.Day.ToString("00") + ScheduleOnData.ScheduledOn.Month.ToString("00");
                        var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                        string forMonth = string.Empty;
                        if (ForMonthSchedule != null && objCompliance.Frequency != null)// added by rahul on 3 May 2016 for download timebase document error
                        {
                            forMonth = ComplianceManagementComplianceScheduleon.GetForMonth(ScheduleOnData.ScheduledOn, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                        }

                        ScheduleOnData.ForMonth = forMonth;
                    }
                }

                return ScheduleOnData;
            }
        }
        public static string GetCheckListUserName(int compliancectatusid, long scheduledonid, long complianceinstanceid, int roleID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var transactionsQuery = (from CSO in entities.ComplianceScheduleOns
                                         join CA in entities.ComplianceAssignments
                                         on CSO.ComplianceInstanceID equals CA.ComplianceInstanceID
                                         join cm in entities.Users on CA.UserID equals cm.ID
                                         where CSO.ComplianceInstanceID == complianceinstanceid
                                         && CA.RoleID == roleID  && CSO.ID== scheduledonid
                                         && CSO.IsActive == true && CSO.IsUpcomingNotDeleted == true
                                         select cm.FirstName + " " + cm.LastName).FirstOrDefault();

                //var transactionsQuery = (from row in entities.CheckListInstanceTransactionViews
                //                         join cm in entities.Users on row.UserID equals cm.ID
                //                         where row.ComplianceInstanceID == complianceinstanceid
                //                         && row.RoleID == roleID
                //                         && row.ComplianceStatusID == compliancectatusid
                //                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                //                         select cm.FirstName + " " + cm.LastName).FirstOrDefault();
                return transactionsQuery;
            }
        }
        public static List<CheckListInstanceTransactionView> GetPeriodBranchLocationChecklistList(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.CheckListInstanceTransactionViews
                             where row.ScheduledOnID == scheduledOnID
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).Distinct().ToList();

                return query;
            }
        }

        public static CheckListInstanceTransactionView GetPeriodBranchLocationChecklist(int scheduledOnID, int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.CheckListInstanceTransactionViews
                             where row.ScheduledOnID == scheduledOnID
                             && row.ComplianceInstanceID == complianceInstanceID
                             select row).FirstOrDefault();

                return query;
            }
        }
        public static CheckListInstanceTransactionView GetForMonthChecklist(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.CheckListInstanceTransactionViews
                                                                    where row.ScheduledOnID == ScheduledOnID
                                                                    select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {
                    var complianceSchedule = ComplianceManagement.GetScheduleByComplianceID(ScheduleOnData.ComplianceID);

                    if (complianceSchedule.Count > 0)// added by rahul on 11 Jan 2016 for download one time document error
                    {
                        var objCompliance = ComplianceManagement.GetByID(ScheduleOnData.ComplianceID);

                        string spacialDate = ScheduleOnData.ScheduledOn.Day.ToString("00") + ScheduleOnData.ScheduledOn.Month.ToString("00");
                        var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                        string forMonth = string.Empty;
                        if (ForMonthSchedule != null && objCompliance.Frequency != null)// added by rahul on 3 May 2016 for download timebase document error
                        {
                            forMonth = ComplianceManagementComplianceScheduleon.GetForMonth(ScheduleOnData.ScheduledOn, ForMonthSchedule.ForMonth, objCompliance.Frequency);
                        }

                        ScheduleOnData.ForMonth = forMonth;
                    }
                }

                return ScheduleOnData;
            }
        }
        public static bool DeleteFile(string path, int file)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                FileData fileToUpdate = new FileData() { ID = file };
                entities.FileDatas.Attach(fileToUpdate);
                fileToUpdate.IsDeleted = true;
                entities.SaveChanges();
            }
            return true;
        }


        //Internal        
        public static List<GetInternalComplianceDocumentsView> GetFileDataInternal(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var fileData = (from row in entities.GetInternalComplianceDocumentsViews
                                where row.InternalComplianceScheduledOnID == ScheduledOnID
                                select row).ToList();

                return fileData;
            }
        }       
        public static InternalComplianceInstance GetInternalComplianceInstanceData(int instanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var instanceData = (from row in entities.InternalComplianceInstances
                                    where row.ID == instanceID
                                    select row).FirstOrDefault();

                return instanceData;
            }
        }
        public static InternalComplianceInstanceTransactionView GetForMonthInternal(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceInstanceTransactionView ScheduleOnData = (from row in entities.InternalComplianceInstanceTransactionViews
                                                                            where row.InternalScheduledOnID == ScheduledOnID
                                                                            select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {

                    var complianceSchedule = InternalComplianceManagement.GetScheduleByComplianceID((long) ScheduleOnData.InternalComplianceID);

                    if (complianceSchedule.Count > 0)// added by rahul on 11 Jan 2016 for download one time document error
                    {
                        var objCompliance = InternalComplianceManagement.GetByID((long) ScheduleOnData.InternalComplianceID);

                        string spacialDate = ScheduleOnData.InternalScheduledOn.Day.ToString("00") + ScheduleOnData.InternalScheduledOn.Month.ToString("00");
                        var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                        string forMonth = string.Empty;

                        if (ForMonthSchedule != null && objCompliance.IFrequency != null)// added by rahul on 3 May 2016 for download timebase document error
                        {
                            forMonth = InternalComplianceManagement.GetForMonth(ScheduleOnData.InternalScheduledOn, ForMonthSchedule.ForMonth, objCompliance.IFrequency);                            
                        }

                        ScheduleOnData.ForMonth = forMonth;
                    }
                }

                return ScheduleOnData;
            }
        }       
        public static InternalComplianceInstanceTransactionView InternalComplianceGetForMonth(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceInstanceTransactionView ScheduleOnData = (from row in entities.InternalComplianceInstanceTransactionViews
                                                                            where row.InternalScheduledOnID == ScheduledOnID
                                                                            select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {

                    var complianceSchedule = InternalComplianceManagement.GetScheduleByComplianceID((long) ScheduleOnData.InternalComplianceID);

                    if (complianceSchedule.Count > 0)//added by Manisha for One time condition
                    {

                        var objCompliance = ComplianceManagement.GetInternalComplianceByID((long) ScheduleOnData.InternalComplianceID);

                        string spacialDate = ScheduleOnData.InternalScheduledOn.Day.ToString("00") + ScheduleOnData.InternalScheduledOn.Month.ToString("00");
                        var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                        string forMonth = string.Empty;

                        forMonth = InternalComplianceManagement.GetForMonth(ScheduleOnData.InternalScheduledOn, ForMonthSchedule.ForMonth, objCompliance.IFrequency);
                        ScheduleOnData.ForMonth = forMonth;
                    }

                }

                return ScheduleOnData;
            }
        }       
        public static int GetInternalComplianceTypeID(int ScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ScheduleOnData = (from row in entities.InternalComplianceInstances
                                      join row1 in entities.InternalComplianceScheduledOns
                                      on row.ID equals row1.InternalComplianceInstanceID
                                      join row3 in entities.InternalCompliances
                                      on row.InternalComplianceID equals row3.ID
                                      where row1.ID == ScheduleOnID
                                      select (int)row3.IComplianceType).Distinct().FirstOrDefault();
                if (ScheduleOnData != null)
                {
                    return ScheduleOnData;
                }
                else
                {
                    return 0;
                }
            }
        }
        public static InternalComplianceInstanceCheckListTransactionView InternalComplianceCheckListGetForMonth(int ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceInstanceCheckListTransactionView ScheduleOnData = (from row in entities.InternalComplianceInstanceCheckListTransactionViews
                                                                            where row.InternalScheduledOnID == ScheduledOnID
                                                                            select row).FirstOrDefault();

                if (string.IsNullOrEmpty(ScheduleOnData.ForMonth))
                {

                    var complianceSchedule = InternalComplianceManagement.GetScheduleByComplianceID((long) ScheduleOnData.InternalComplianceID);

                    if (complianceSchedule.Count > 0)//added by Manisha for One time condition
                    {

                        var objCompliance = ComplianceManagement.GetInternalComplianceByID((long) ScheduleOnData.InternalComplianceID);

                        string spacialDate = ScheduleOnData.InternalScheduledOn.Day.ToString("00") + ScheduleOnData.InternalScheduledOn.Month.ToString("00");
                        var ForMonthSchedule = complianceSchedule.Where(row => row.SpecialDate.Equals(spacialDate)).FirstOrDefault();

                        string forMonth = string.Empty;

                        forMonth = InternalComplianceManagement.GetForMonth(ScheduleOnData.InternalScheduledOn, ForMonthSchedule.ForMonth, objCompliance.IFrequency);
                        ScheduleOnData.ForMonth = forMonth;
                    }

                }

                return ScheduleOnData;
            }
        }
        public static List<InternalComplianceDocumentsView> GetAllInternalCompliance(int? customerID, string Role, int userID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var documentData = (from row in entities.InternalComplianceDocumentsViews
                                    where row.CustomerID == customerID
                                    select row).ToList();
                if (!Role.Equals("CADMN"))
                {
                    documentData = documentData.Where(entry => entry.UserID == userID).ToList();
                }
                return documentData.GroupBy(entry => entry.InternalComplianceScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }

        public static string getFileName(string url)
        {
            if (url.Length > 30)
                return url.Substring(0, 30) + "...";
            else
                return url;
            //string[] arr = url.Split('/');
            //return arr[arr.Length - 1];
        }
    }
}
