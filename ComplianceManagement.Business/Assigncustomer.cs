﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
   public class Assigncustomer
    {
        public static object GetAssignCustomerData(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.ComplianceCertificateMappings
                             on row.ID equals row1.CustomerID
                             where row.IsDeleted == false
                             && row.ID == customerID
                             && row.Status == 1
                             select row);
                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        public static object GetAllCustomer(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var users = (from row in entities.CustomerAssignmentDetails
                             join row1 in entities.Customers
                             on row.CustomerID equals row1.ID 
                             where row.IsDeleted == false
                             && row.UserID == userID
                             && row1.IsDeleted == false
                             && row1.ComplianceProductType != 1
                             && row1.Status == 1
                             select row1);
               

                //if (!string.IsNullOrEmpty(filter))
                //{
                //    users = users.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter) || entry.BuyerContactNumber.Contains(filter));
                //}

                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        public static object GetAllCustomerData(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {    
                var users = (from row in entities.Customers
                             where row.IsDeleted == false
                             && row.ID == customerID
                             && row.IsDeleted == false
                             select row);  
                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        public static List<Customer> GetAllNewCustomer(int userid,int customerID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.CustomerAssignmentDetails
                             on row.ID equals row1.CustomerID
                             where row.IsDeleted == false
                             && row1.IsDeleted == false
                             && row1.UserID==userid
                             //&& row.ServiceProviderID == 95
                              && row.ComplianceProductType != 1
                              && row.Status == 1
                             select row);

                if (customerID != -1)
                {
                    users = users.Where(entry => entry.ID == customerID);
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.Contains(filter) || entry.BuyerName.Contains(filter) || entry.BuyerEmail.Contains(filter) || entry.BuyerContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.Name).Distinct().ToList();
            }
        }
        public static List<IMPTUserView> GetAllNewUser(int customerID,int userid, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.IMPTUserViews
                             where row.UserID==userid
                             select row);
                if (customerID != -1)
                {
                    users = users.Where(entry => entry.CustomerID == customerID);
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.FirstName.Contains(filter) || entry.LastName.Contains(filter) || entry.Email.Contains(filter) || entry.ContactNumber.Contains(filter));
                }

                return users.OrderBy(entry => entry.FirstName).Distinct().ToList();
            }
        }

    }
}
