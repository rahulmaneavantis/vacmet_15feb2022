﻿using System;
using System.Net;
using System.IO;
using System.Web;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ZohoCRMAPI
    {
        public static string zohocrmurl = "https://crm.zoho.com/crm/private/xml/";
        static bool invalid = false;
        //public static void Main(string[] args)
        //{      
        //    string result = APIMethod("Leads", "getRecords", "508020000000332001");//Change the id,method name, and module name here
        //    Console.Write(result);
        //}

        public static String APIMethod(string modulename, string methodname, string recordId, string fromRecordIndex, string toRecordIndex)
        {
            string uri = zohocrmurl + modulename + "/" + methodname + "?";
            /* Append your parameters here */
            string postContent = "scope=crmapi";
            postContent = postContent + "&authtoken=4ec79a6c5785c1d955f2c0c573b529aa";//Give your authtoken

            if (methodname.Equals("insertRecords") || methodname.Equals("updateRecords"))
            {
                postContent = postContent + "&xmlData=" + HttpUtility.UrlEncode("Your CompanyHannahSmithtesting@testing.com");
            }
            if (methodname.Equals("updateRecords") || methodname.Equals("deleteRecords") || methodname.Equals("getRecordById"))
            {
                postContent = postContent + "&id=" + recordId;
            }
            if (methodname.Equals("getRecords"))
            {
                postContent = postContent + "&selectColumns=Leads(Leads(First Name,Last Name,Email,Company)&fromIndex=" + fromRecordIndex + "&toIndex=" + toRecordIndex + "&version=1";
            }

            string result = AccessCRM(uri, postContent);
            return result;
        }
        public static string AccessCRM(string url, string postcontent)
        {
            try
            {
                string responseFromServer = string.Empty;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                byte[] byteArray = Encoding.UTF8.GetBytes(postcontent);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    responseFromServer = reader.ReadToEnd();
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                }
                return responseFromServer;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static bool CreateMailMasterLists(List<MailMasterList> lstMML)
        {
            try
            {
                int count = 0;
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    lstMML.ForEach(MML =>
                    {
                        entities.MailMasterLists.Add(MML);
                        count++;

                        if (count > 100)
                        {
                            entities.SaveChanges();
                            count = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ExistsMailMasterLists(MailMasterList MML)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.MailMasterLists
                             where row.Email.Equals(MML.Email)
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }

        public static string GetMailMasterLists(string Flag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<string> ValidMailList = new List<string>();

                var query = (from row in entities.MailMasterLists
                             where row.IsDeleted == false
                             && row.IsSubscribe == true
                             select row).ToList();

                if (Flag == "1") //Compliance Product User
                    query = query.Where(entry => entry.IsZoho == false).ToList();
                else if (Flag == "2") //Zoho
                    query = query.Where(entry => entry.IsZoho == true).ToList(); 

                if (query.Count > 0)
                {
                    query.ForEach(EachUser =>
                    {
                        if (IsValidEmail(EachUser.Email))
                            ValidMailList.Add(EachUser.Email);
                    });
                }

                if (ValidMailList.Count > 0)
                    return String.Join(", ", ValidMailList);
                else
                    return "";
            }
        }

        public static bool RefreshZohoRecords()
        {
            try
            {
                List<MailMasterList> lstMML = new List<MailMasterList>();

                int i = 0;
                string str = null;
                string result = null;

                string name = null;
                string company = null;
                string emailID = null;

                int fromIndex = 1;
                int toIndex = 200;

                bool ResponseOK = true;
                bool successGetRecords = false;

                do
                {
                    result = ZohoCRMAPI.APIMethod("Leads", "getRecords", "", fromIndex.ToString(), toIndex.ToString());//Change the id,method name, and module name here               

                    if (result != "")
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(result);
                        XmlNodeList xmlnode;
                        xmlnode = xmlDoc.GetElementsByTagName("Leads");

                        if (xmlnode.Count > 0)
                        {
                            for (i = 0; i <= xmlnode.Count - 1; i++)
                            {
                                foreach (XmlNode node in xmlnode[i].ChildNodes)
                                {
                                    name = emailID = company = "";

                                    if (node.ChildNodes.Item(1) != null)
                                    {
                                        company = node.ChildNodes.Item(1).InnerText.Trim();
                                    }

                                    if (node.ChildNodes.Item(2) != null)
                                    {
                                        name = node.ChildNodes.Item(2).InnerText.Trim();
                                    }
                                    else
                                    {
                                        name = "";
                                        emailID = "";
                                    }

                                    if (node.ChildNodes.Item(3) != null)
                                    {
                                        emailID = node.ChildNodes.Item(3).InnerText.Trim();
                                    }
                                    else
                                    {
                                        if (node.ChildNodes.Item(2) != null)
                                        {
                                            emailID = node.ChildNodes.Item(2).InnerText.Trim();
                                        }
                                    }

                                    using (AuditControlEntities entities = new AuditControlEntities())
                                    {
                                        MailMasterList newMML = new MailMasterList()
                                        {
                                            Name = name,
                                            Company = company,
                                            Email = emailID,
                                            IsZoho = true,
                                            IsSubscribe=true,
                                            IsDeleted = false
                                        };

                                        if (!ZohoCRMAPI.ExistsMailMasterLists(newMML))
                                            if (IsValidEmail(newMML.Email))
                                                lstMML.Add(newMML);
                                    }
                                }
                            }

                            fromIndex += 200;
                            toIndex += 200;
                        }
                        else
                            ResponseOK = false;
                    }
                    else
                        ResponseOK = false;

                } while (ResponseOK);

                if (lstMML.Count > 0)
                    successGetRecords = ZohoCRMAPI.CreateMailMasterLists(lstMML);

                return successGetRecords;
            }
            catch
            {
                return false;
            }
        }

        public static bool RefreshComplianceProductCustomerRecords()
        {
            try
            {
                List<MailMasterList> lstMML = new List<MailMasterList>();
                bool successGetRecords = false;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var CustomerRecords = (from UserData in entities.Users
                                           join PM in entities.ProductMappings
                                           on UserData.CustomerID equals PM.CustomerID
                                           join Cust in entities.Customers
                                           on UserData.CustomerID equals Cust.ID
                                           where UserData.CustomerID != null
                                           && UserData.CustomerID != 5
                                           && UserData.IsDeleted==false
                                           && PM.ProductID == 1
                                           && PM.IsActive==false
                                           && Cust.IsDeleted == false
                                           && Cust.Status == 1
                                           orderby UserData.CustomerID
                                           select new
                                           {
                                               Name= UserData.FirstName.Trim() +" "+ UserData.LastName.Trim(),
                                               Email= UserData.Email.Trim(),
                                               Company=Cust.Name
                                           }).ToList();

                    if (CustomerRecords.Count > 0)
                    {
                        CustomerRecords.ForEach(EachRecord =>
                        {
                            MailMasterList newMML = new MailMasterList()
                            {
                                Name = EachRecord.Name,
                                Company = EachRecord.Company,
                                Email = EachRecord.Email,
                                IsZoho = false,
                                IsSubscribe=true,
                                IsDeleted=false
                            };

                            if (!ZohoCRMAPI.ExistsMailMasterLists(newMML))
                                if (IsValidEmail(newMML.Email))
                                    lstMML.Add(newMML);
                        });                        
                    }
                }

                if (lstMML.Count > 0)
                    successGetRecords = ZohoCRMAPI.CreateMailMasterLists(lstMML);

                return successGetRecords;
            }
            catch
            {
                return false;
            }
        }

        //public static bool IsValidEmail(string email)
        //    {
        //        try
        //        {
        //            var emailChecked = new System.Net.Mail.MailAddress(email);
        //            return true;                
        //        }
        //        catch
        //        {
        //            return false;
        //        }
        //    }

        public static bool IsValidEmail(string strIn)
        {
            invalid = false;

            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }


    }
}

   
    

