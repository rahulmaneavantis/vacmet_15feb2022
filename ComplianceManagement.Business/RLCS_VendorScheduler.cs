﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business.RLCS
{
    public class RLCS_VendorScheduler
    {
        public static bool AuditStepMappingExists(RLCS_VendorAuditStepMapping ASM)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.RLCS_VendorAuditStepMapping
                             where row.AuditID == ASM.AuditID
                             && row.CheckListID == ASM.CheckListID
                             && row.VendorAuditScheduleOnID == ASM.VendorAuditScheduleOnID
                             select row).FirstOrDefault();

                if (query != null)
                    return true;
                else
                    return false;
            }
        }
        public static bool CreateAuditStepMapping(List<RLCS_VendorAuditStepMapping> AuditStepMappingList)
        {
            try
            {
                int saveCount = 0;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    AuditStepMappingList.ForEach(entry =>
                    {
                        entities.RLCS_VendorAuditStepMapping.Add(entry);
                        saveCount++;
                        if (saveCount > 100)
                        {
                            entities.SaveChanges();
                            saveCount = 0;
                        }
                    });

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool SaveAuditStepsDetails(List<long> ActiveAuditStepIDList, long auditID, long scheduleonid)
        {
            try
            {
                bool saveSuccess = false;                
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (ActiveAuditStepIDList.Count > 0)
                    {                      
                        //Save Each Step in AuditStepMapping
                        if (ActiveAuditStepIDList.Count > 0)
                        {
                            if (auditID != 0)
                            {
                                List<RLCS_VendorAuditStepMapping> AuditStepMappingList = new List<RLCS_VendorAuditStepMapping>();
                                ActiveAuditStepIDList.ForEach(EachAuditStep =>
                                {
                                    RLCS_VendorAuditStepMapping newASM = new RLCS_VendorAuditStepMapping()
                                    {
                                        AuditID = auditID,
                                        VendorAuditScheduleOnID = scheduleonid,
                                        CheckListID = EachAuditStep,
                                        IsActive = true,
                                    };

                                    if (!AuditStepMappingExists(newASM))
                                        AuditStepMappingList.Add(newASM);
                                });

                                if (AuditStepMappingList.Count > 0)
                                {
                                    saveSuccess = CreateAuditStepMapping(AuditStepMappingList);
                                }
                            } //AuditID is ZERO
                            else
                                saveSuccess = false;
                        } //No Audit Step
                        else
                            saveSuccess = false;
                    } //No Audit Step
                    else
                        saveSuccess = false;

                    return saveSuccess;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static List<RLCS_VendorFrquencySchedule> GetScheduleByComplianceID(long FrequnecyId,long AuditID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.RLCS_VendorFrquencySchedule
                                    where row.FrequnecyId == FrequnecyId
                                    && row.AuditID== AuditID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
        public static RLCS_VendorAuditScheduleOn GetLastScheduleOnByInstance(long auditID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleObj = (from row in entities.RLCS_VendorAuditScheduleOn
                                   where row.AuditID == auditID
                                   && row.IsActive == true
                                   orderby row.ScheduleOn descending, row.ID descending
                                   select row).FirstOrDefault();

                return scheduleObj;
            }
        }
        public static DateTime? GetComplianceInstanceScheduledon(long AuditID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime ScheduleOnID = (from row in entities.RLCS_VendorAuditInstance
                                         where row.ID == AuditID
                                         select (DateTime)row.CC_ContractFrom).FirstOrDefault();
                if (ScheduleOnID != null)
                {
                    return ScheduleOnID;
                }
                else
                {
                    return null;
                }
            }
        }
        public static long GetLastPeriodFromDate(int forMonth, int Frequency)
        {
            long forMonthName = 0;
            switch (Frequency)
            {
                case 1:
                    if (forMonth == 1)
                    {
                        forMonthName = 12;
                    }
                    else
                    {
                        forMonthName = forMonth - 1;
                    }
                    break;
                case 4:
                    if (forMonth == 1)
                    {
                        forMonthName = 10;
                    }
                    else
                    {
                        forMonthName = forMonth - 3;
                    }
                    break;
                case 2:
                    if (forMonth == 1)
                    {
                        forMonthName = 7;
                    }
                    else
                    {
                        forMonthName = forMonth - 6;
                    }
                    break;

                case 3:
                    forMonthName = 1;
                    break;
                case 5:
                    forMonthName = 1;
                    break;
                case 6:

                    forMonthName = 1;
                    break;

                default:
                    forMonthName = 0;
                    break;
            }

            return forMonthName;
        }
        public static long GetNextMonthFromFrequency(long LastforMonth, int Frequency)
        {
            long NextPeriod;
            switch (Frequency)
            {
                //1 Monthly
                case 1:
                    if (LastforMonth == 12)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 1;
                    break;
                //4 Quarterly
                case 4:
                    if (LastforMonth == 10)
                        NextPeriod = 1;
                    else
                        NextPeriod = LastforMonth + 3;
                    break;

                case 2:
                    if (LastforMonth == 7)
                        NextPeriod = 1;
                    else if (LastforMonth == 10)
                        NextPeriod = 4;
                    else
                        NextPeriod = LastforMonth + 6;
                    break;

                case 3:
                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;
                case 5:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                case 6:

                    if (LastforMonth == 4)
                    {
                        NextPeriod = 4;
                    }
                    else
                    {
                        NextPeriod = 1;
                    }
                    break;

                default:
                    NextPeriod = 0;
                    break;
            }

            return NextPeriod;
        }
        public static long GetScheduledOnPresentOrNot(long AuditID, DateTime ScheduledOn)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                long ScheduleOnID = (from row in entities.RLCS_VendorAuditScheduleOn
                                     where row.AuditID == AuditID && row.ScheduleOn == ScheduledOn
                                     && row.IsActive == true
                                     select row.ID).FirstOrDefault();
                return ScheduleOnID;
            }
        }
        public static string GetForMonth(DateTime date, int forMonth, int Frequency)
        {
            string forMonthName = string.Empty;
            switch (Frequency)
            {
                case 1:
                    string year = date.ToString("yy");
                    if (date > DateTime.Today.Date)
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            if (forMonth == 11)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else if (forMonth == 12)
                            {
                                year = (date.AddYears(-1)).ToString("yy");
                            }
                            else
                            {
                                year = (date).ToString("yy");
                            }
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    else
                    {
                        if (Convert.ToInt32(year) > Convert.ToInt32(DateTime.Today.Date.ToString("yy")))
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }
                        else if (forMonth == 12)
                        {
                            year = (date.AddYears(-1)).ToString("yy");
                        }//added by rahul on 18 OCT 2016   
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year;
                    break;
                case 4:
                    string yearq = date.ToString("yy");
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            yearq = (date).ToString("yy");
                        }
                        else
                        {
                            yearq = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + yearq +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 2)).Substring(0, 3) + " " + yearq;
                    break;
                case 2:
                    string year1 = date.ToString("yy");
                    if (forMonth == 7)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                    }
                    if (forMonth == 10)
                    {
                        if (date.ToString("MM") == "10")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "11")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else if (date.ToString("MM") == "12")
                        {
                            year1 = (date).ToString("yy");
                        }
                        else
                        {
                            year1 = (date.AddYears(-1)).ToString("yy");
                        }
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(3).Substring(0, 3) + " " + date.ToString("yy");
                    }
                    else
                    {
                        forMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth)).Substring(0, 3) + " " + year1 +
                                   " - " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName((forMonth + 5)).Substring(0, 3) + " " + year1;
                    }

                    break;

                case 3:

                    string startFinancial1Year = date.ToString("yy");
                    string endFinancial1Year = date.ToString("yy");
                    if (date.Month >= 4)
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }
                    else if (date.Month >= 3)
                    {
                        startFinancial1Year = (date.AddYears(-1)).ToString("yy");
                        endFinancial1Year = date.ToString("yy");
                    }
                    else
                    {
                        startFinancial1Year = (date.AddYears(-2)).ToString("yy");
                        endFinancial1Year = (date.AddYears(-1)).ToString("yy");
                    }

                    if (forMonth == 1)
                    {
                        forMonthName = "CY - " + endFinancial1Year;
                    }
                    else
                    {
                        forMonthName = "FY " + startFinancial1Year + " - " + endFinancial1Year;
                    }
                    break;
                default:
                    forMonthName = string.Empty;
                    break;
            }

            return forMonthName;
        }
        public static Tuple<DateTime, string, long> GetNextDate(DateTime scheduledOn, int frequencyID, long AuditID)
        {
            var complianceSchedule = GetScheduleByComplianceID(frequencyID, AuditID);
            var lastSchedule = GetLastScheduleOnByInstance(AuditID);
            DateTime? getinstancescheduleondate = GetComplianceInstanceScheduledon(AuditID);
            long lastPeriod = 0;
            DateTime ScheduleOndt = Convert.ToDateTime(lastSchedule.ScheduleOn);
            if (lastSchedule != null)
            {
                if (lastSchedule.ForPeriod != null)
                {
                    lastPeriod = Convert.ToInt64(lastSchedule.ForPeriod);
                }
                else
                {
                    lastPeriod = GetLastPeriodFromDate(Convert.ToInt32(ScheduleOndt.Month), frequencyID);
                }
            }
            else
            {
                #region Monthly
                if (frequencyID == 1)
                {
                    lastPeriod = scheduledOn.Month - 1;
                }
                #endregion
                #region   Half Yearly
                else if (frequencyID == 2)
                {
                    if (complianceSchedule[0].ForMonth == 1)
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 7)
                                {
                                    lastPeriod = 1;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 7 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                                {
                                    lastPeriod = 7;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                            }
                            else
                            {
                                if (Convert.ToDateTime(getinstancescheduleondate).Month >= 4 && Convert.ToDateTime(getinstancescheduleondate).Month <= 9)
                                {
                                    lastPeriod = 4;
                                }
                                else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 10)
                                {
                                    lastPeriod = 10;
                                }
                                else
                                {
                                    lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Annaully
                else if (frequencyID == 3)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {

                        if (complianceSchedule[0].ForMonth == 1)
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                        else
                        {
                            if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                            {
                                lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)); //comment by rahul on 5 Jan 2016
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion
                #region Quarterly
                else if (frequencyID == 4)
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {

                            lastPeriod = long.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2));// comment by rahul on 5 Jan 2016
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                        }
                        else
                        {
                            if (Convert.ToDateTime(getinstancescheduleondate).Month >= 1 && Convert.ToDateTime(getinstancescheduleondate).Month <= 3)
                            {
                                lastPeriod = 1;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 11 && Convert.ToDateTime(getinstancescheduleondate).Month <= 12)
                            {
                                lastPeriod = 10;
                            }
                            else if (Convert.ToDateTime(getinstancescheduleondate).Month >= 8 && Convert.ToDateTime(getinstancescheduleondate).Month <= 10)
                            {
                                lastPeriod = 7;
                            }
                            else
                            {
                                lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                            }
                        }
                    }
                }
                #endregion                                
                else
                {
                    if (complianceSchedule[0].SpecialDate != null && scheduledOn != null)
                    {
                        if (int.Parse(complianceSchedule[0].SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00")) == false)
                        {
                            lastPeriod = 1;
                        }
                        else
                        {
                            lastPeriod = complianceSchedule.Where(row => int.Parse(row.SpecialDate.Substring(2, 2)) >= int.Parse(scheduledOn.Month.ToString("00"))).FirstOrDefault().ForMonth;
                        }
                    }
                }
            }

            List<Tuple<DateTime, long>> complianceScheduleDates;
            if (frequencyID != 1 || frequencyID != 4)
            {
                #region  First Part
                if (complianceSchedule.Where(row => row.ForMonth == 1).FirstOrDefault() != null)
                {
                    if (frequencyID == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                else
                {
                    if (frequencyID == 3)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 5)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 6)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 4)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 1)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else if (frequencyID == 2)
                        complianceScheduleDates = complianceSchedule.Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                    else
                        complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                }
                #endregion

                #region Second Part
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" || row.SpecialDate.Substring(2, 2) == "04").FirstOrDefault() != null)
                {
                    if (frequencyID == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequencyID == 5)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 2, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else if (frequencyID == 6)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 7, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    else
                    {
                        if (frequencyID == 4)
                        {
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 10).FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" && entry.ForMonth == 10).Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            if (frequencyID == 1)
                            {
                                if (lastPeriod == 11)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" && row.ForMonth == 12).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                if (lastPeriod == 7)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.SpecialDate.Substring(0, 2) == "10" && row.ForMonth == 8).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                            if (frequencyID == 2)
                            {
                                if (lastPeriod == 1)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else if (lastPeriod != 10)
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 7).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                                else
                                {
                                    if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "07" && row.ForMonth == 1).FirstOrDefault() != null)
                                    {
                                        if (scheduledOn.Year <= DateTime.Today.Year - 1)
                                        {
                                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                        }
                                    }
                                    else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "04" && row.ForMonth == 10).FirstOrDefault() != null)
                                    {
                                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "04").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                    }
                                }
                            }
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02" || row.SpecialDate.Substring(2, 2) == "07" || row.SpecialDate.Substring(2, 2) == "08" || row.SpecialDate.Substring(2, 2) == "09" || row.SpecialDate.Substring(2, 2) == "10" || row.SpecialDate.Substring(2, 2) == "12").FirstOrDefault() != null)
                {
                    if (frequencyID == 3)
                        complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02" || entry.SpecialDate.Substring(2, 2) == "07" || entry.SpecialDate.Substring(2, 2) == "08" || entry.SpecialDate.Substring(2, 2) == "09" || entry.SpecialDate.Substring(2, 2) == "10" || entry.SpecialDate.Substring(2, 2) == "12").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                    if (frequencyID == 2)
                    {
                        if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "08").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "08").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                        else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "06").FirstOrDefault() != null)
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "06").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                    else
                    {
                        if (frequencyID == 1)
                        {
                            if (lastPeriod == 11)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && row.ForMonth == 12).FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                        }
                        else if (frequencyID == 4)
                        {
                            if (frequencyID == 4)
                            {
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "02").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "02").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "03").FirstOrDefault() != null)
                                {
                                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "03").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                                }
                            }
                            else
                            {
                                complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                            }
                        }
                        else
                        {
                            complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "11").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                        }
                    }
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && frequencyID == 3).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                else if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "05" && frequencyID == 2).FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01" || entry.SpecialDate.Substring(2, 2) == "04" || entry.SpecialDate.Substring(2, 2) == "05").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            else
            {
                #region Third Part
                complianceScheduleDates = complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) != "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).ToList();
                if (complianceSchedule.Where(row => row.SpecialDate.Substring(2, 2) == "01").FirstOrDefault() != null)
                {
                    complianceScheduleDates.Add(complianceSchedule.Where(entry => entry.SpecialDate.Substring(2, 2) == "01").Select(row => new Tuple<DateTime, long>(new DateTime(scheduledOn.Year + 1, Convert.ToInt32(row.SpecialDate.Substring(2, 2)), Convert.ToInt32(row.SpecialDate.Substring(0, 2))), row.ForMonth)).FirstOrDefault());
                }
                #endregion
            }
            complianceScheduleDates = complianceScheduleDates.Where(row => row != null).ToList();
            long ActualForMonth = 0;
            if (frequencyID == 1)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frequencyID);
                }
            }
            else if (frequencyID == 4)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frequencyID);
                }
            }
            else if (frequencyID == 2)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frequencyID);
                }
            }
            else if (frequencyID == 3)
            {
                if (lastSchedule == null)
                {
                    if (getinstancescheduleondate != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.OrderBy(t => t.Item1).ThenByDescending(t => t.Item2).ToList();
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                        if (complianceScheduleDates.Count > 0)
                        {
                            ActualForMonth = complianceScheduleDates.ToList().FirstOrDefault().Item2;
                        }
                    }
                }
                else
                {
                    ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frequencyID);
                }
            }
            else
            {
                ActualForMonth = GetNextMonthFromFrequency(lastPeriod, frequencyID);
            }
            DateTime date = new DateTime();
            if (complianceScheduleDates.Count > 0)
            {
                date = DateTime.UtcNow;
            }
            else
            {
                string ic = "1/1/0001 12:00:00 AM";
                date = Convert.ToDateTime(ic);
            }
            if (complianceScheduleDates.Count > 0)
            {
                if (frequencyID == 4)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(AuditID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequencyID == 2)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(AuditID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequencyID == 3)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(AuditID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else if (frequencyID == 1)
                {
                    if (lastSchedule == null)
                    {
                        if (getinstancescheduleondate != null)
                        {
                            complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(getinstancescheduleondate)).ToList();
                            date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                        }
                    }
                    else
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= Convert.ToDateTime(scheduledOn)).ToList();
                        complianceScheduleDates = complianceScheduleDates.OrderByDescending(x => x.Item1).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    long scheduledonidpresent = GetScheduledOnPresentOrNot(AuditID, date);
                    if (scheduledonidpresent > 0)
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).LastOrDefault().Item1;
                    }
                }
                else
                {
                    if (lastSchedule != null)
                    {
                        complianceScheduleDates = complianceScheduleDates.Where(row => row.Item1 >= lastSchedule.ScheduleOn).ToList();
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                    else
                    {
                        date = complianceScheduleDates.Where(row => row.Item2 == ActualForMonth).FirstOrDefault().Item1;
                    }
                }
            }

            if (date == DateTime.MinValue)
            {
                if (complianceSchedule.Count > 0)
                {
                    date = new DateTime(scheduledOn.Year + 1, Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(2, 2)), Convert.ToInt32(complianceSchedule[0].SpecialDate.Substring(0, 2)));
                }
            }

            string spacialDate = date.Day.ToString("00") + date.Month.ToString("00");

            string forMonth = string.Empty;
            forMonth = GetForMonth(date, Convert.ToInt32(ActualForMonth), frequencyID);
            return new Tuple<DateTime, string, long>(date, forMonth, ActualForMonth);
        }
        public static void CreateScheduleOn(DateTime scheduledOn,DateTime enddate, long auditID, int FrequnecyId, long createdByID, string creatdByName, List<long> auditstepdetails,long branchID)
        {
            long CreateScheduleOnErrocomplianceInstanceId = -1;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    DateTime nextDate = scheduledOn;
                    var compliances = (from row in entities.RLCS_VendorFrquencySchedule
                                       where row.FrequnecyId == FrequnecyId
                                       && row.AuditID == auditID
                                       select row).FirstOrDefault();
                    DateTime curruntDate;
                    curruntDate = DateTime.UtcNow;

                    while (nextDate.Date < curruntDate.Date)
                    {                        
                        Tuple<DateTime, string, long> nextDateMonth;
                        #region Normal Frequancy 

                        CreateScheduleOnErrocomplianceInstanceId = auditID;
                        nextDateMonth = GetNextDate(nextDate, FrequnecyId, auditID);

                        long ScheduleOnID = (from row in entities.RLCS_VendorAuditScheduleOn
                                             where row.AuditID == auditID
                                             && row.ScheduleOn == nextDateMonth.Item1 && row.ForPeriod == nextDateMonth.Item3
                                             select row.ID).SingleOrDefault();

                        if (ScheduleOnID <= 0)
                        {
                            if (nextDateMonth.Item1.Date < enddate.Date)
                            {
                                RLCS_VendorAuditScheduleOn complianceScheduleon = new RLCS_VendorAuditScheduleOn();
                                complianceScheduleon.AuditID = auditID;
                                complianceScheduleon.ScheduleOn = nextDateMonth.Item1;
                                complianceScheduleon.ForMonth = nextDateMonth.Item2;
                                complianceScheduleon.ForPeriod = nextDateMonth.Item3;
                                complianceScheduleon.IsActive = true;
                                complianceScheduleon.CustomerBranchID = branchID;
                                complianceScheduleon.AuditStatusID = 1;
                                entities.RLCS_VendorAuditScheduleOn.Add(complianceScheduleon);
                                entities.SaveChanges();
                                nextDate = nextDateMonth.Item1;

                                RLCS_VendorAuditTransaction transaction = new RLCS_VendorAuditTransaction()
                                {
                                    AuditID = auditID,
                                    VendorAuditScheduleOnID = complianceScheduleon.ID,
                                    CreatedBy = createdByID,
                                    CreatedByText = creatdByName,
                                    CustomerBranchId = branchID,
                                    StatusId = 1,
                                    Remarks = "New compliance assigned."
                                };

                                CreateTransaction(transaction);
                                //Save Total Active Ateps and Count 
                                SaveAuditStepsDetails(auditstepdetails, auditID, complianceScheduleon.ID);
                            }
                            else
                            {
                                break;
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "RLCSCreateInstances" + "AuditID" + CreateScheduleOnErrocomplianceInstanceId;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);
                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as RLCSCreateInstances Function", "RLCSCreateInstances" + "AuditID" + CreateScheduleOnErrocomplianceInstanceId);

                ComplianceManagement.SendgridSenEmail("Error Occured as RLCSCreateInstances Function", "RLCSCreateInstances" + "AuditID" + CreateScheduleOnErrocomplianceInstanceId);
            }
        }
        public static bool CreateTransaction(RLCS_VendorAuditTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.RLCS_VendorAuditTransaction.Add(transaction);
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");

                ComplianceManagement.SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }
        public static bool CreateInstances(List<Tuple<RLCS_VendorAuditInstance, RLCS_VendorAuditAssignment>> assignments, long createdByID, string creatdByName, List<long> asteplist,long branchID,int FrequencyID)
        {
            bool SaveFlag = false;
            long CreateInstancesErrorcomplianceInstanceID = -1;
            try
            {
                if (assignments.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {                        
                        assignments.ForEach(entry =>
                        {
                            long auditID = entry.Item1.ID;
                            CreateInstancesErrorcomplianceInstanceID = auditID;
                            var getpreviousschedule = GetLastScheduleOnByInstance(auditID);
                            if (getpreviousschedule != null)
                            {
                                CreateScheduleOn((DateTime)getpreviousschedule.ScheduleOn, (DateTime)entry.Item1.CC_ContractTo, auditID, Convert.ToInt32(FrequencyID), createdByID, creatdByName, asteplist, branchID);
                            }
                            else
                            {
                                CreateScheduleOn((DateTime)entry.Item1.CC_ContractFrom, (DateTime)entry.Item1.CC_ContractTo, auditID, Convert.ToInt32(FrequencyID), createdByID, creatdByName, asteplist, branchID);
                            }
                            long auditAssignment = (from row in entities.RLCS_VendorAuditAssignment
                                                    where row.AuditId == entry.Item1.ID && row.RoleID == entry.Item2.RoleID
                                                    select row.UserID).FirstOrDefault();

                            if (auditAssignment <= 0)
                            {
                                entry.Item2.AuditId = auditID;
                                entities.RLCS_VendorAuditAssignment.Add(entry.Item2);
                                entities.SaveChanges();
                                //CreateReminders(entry.Item1.ComplianceId, complianceInstanceID, entry.Item2.ID);
                            }
                            SaveFlag = true;
                        });
                    }                 
                }
                return SaveFlag;
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "RLCSCreateInstances" + "AuditID" + CreateInstancesErrorcomplianceInstanceID;
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                InsertLogToDatabase(ReminderUserList);
                //List<string> TO = new List<string>();
                //TO.Add("sachin@avantis.info");
                //TO.Add("narendra@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as RLCSCreateInstances Function", "RLCSCreateInstances" + "AuditID" + CreateInstancesErrorcomplianceInstanceID);

                ComplianceManagement.SendgridSenEmail("Error Occured as RLCSCreateInstances Function", "RLCSCreateInstances" + "AuditID" + CreateInstancesErrorcomplianceInstanceID);
                return false;
            }
        }
        public static void InsertLogToDatabase(List<LogMessage> objEscalation)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                objEscalation.ForEach(entry =>
                {
                    entities.LogMessages.Add(entry);
                    entities.SaveChanges();
                });
            }
        }
    }
}
