﻿using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class InternalControlManagementDashboardRisk
    {
        public static DataTable GetPersonResponsibleAuditTotalCountUserWiseIMP(int userid, int Roleid, int customerid, int StatusID, string FinYear, List<long> Branchlist, string Period, int verticalid, string tag)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int?> OpenImpStatusList = new List<int?>();
                OpenImpStatusList.Add(2);
                OpenImpStatusList.Add(3);
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long AuditeeSubmitcount;
                long AuditeeReviewCommentcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;
                string financialyear = string.Empty;
                string formonth = string.Empty;
                string branch = string.Empty;
                string AuditeeResponse = string.Empty;
                int VerticalID;
                string ProcessName = string.Empty;
                int totalLastcount = 0;
                int totalNotDoneLastcount = 0;
                int totalsubmitedLastcount = 0;
                int totalTeamReviewLastcount = 0;
                int totalAuditeeReviewLastcount = 0;
                int totalAuditeeSubmitLastcount = 0;
                int totalAuditeeReviewCommentLastcount = 0;
                int totalFinalReviewLastcount = 0;
                int totalcloseLastcount = 0;
                int InstanceIDLast = 0;
                int ScheduleOnIDLast = 0;
                int CustomerBranchIDLast = 0;
                int RoleidLast = 0;
                int VerticalIDLast = 0;
                string AuditName = string.Empty;


                DataTable table = new DataTable();
                table.Columns.Add("Branch", typeof(string));
                table.Columns.Add("FinancialYear", typeof(string));
                table.Columns.Add("ForMonth", typeof(string));
                table.Columns.Add("Total", typeof(long));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("AuditeeSubmit", typeof(long));
                table.Columns.Add("ReviewComment", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("VerticalID", typeof(int));
                table.Columns.Add("ProcessName", typeof(string));
                table.Columns.Add("AuditName", typeof(string));
                table.Columns.Add("AuditID", typeof(string));
                table.Columns.Add("AuditeeResponse", typeof(string));

                List<Sp_ImplementationAuditSummaryCountView_Result> ObjResultSet = new List<Sp_ImplementationAuditSummaryCountView_Result>();

                ObjResultSet = (from row in entities.Sp_ImplementationAuditSummaryCountView(customerid)
                                select row).ToList();

                List<long> AuditIDList = new List<long>();
                if (StatusID == 3)
                {
                    AuditIDList = (from row in ObjResultSet
                                   where row.PersonResponsibleOLD == userid
                                   && row.RoleID == 3
                                   && row.AuditStatusID == 3
                                   && row.FinancialYear == FinYear
                                   //&& row.AllClosed != null
                                   select (long)row.AuditID).Distinct().ToList();
                }
                else
                {
                    AuditIDList = (from row in ObjResultSet
                                   where row.PersonResponsibleOLD == userid
                                   && row.RoleID == 3
                                   && row.FinancialYear == FinYear
                                   //&& row.AllClosed == null
                                   //&& row.AuditID == auditID
                                   select (long)row.AuditID).Distinct().ToList();
                }


                foreach (var AudID in AuditIDList)
                {
                    List<Sp_ImplementationAuditSummaryCountView_Result> AuditSummaryRecords = new List<Sp_ImplementationAuditSummaryCountView_Result>();
                    if (!string.IsNullOrEmpty(tag))
                    {
                        if (tag == "Due")
                        {
                            AuditSummaryRecords = (from row in ObjResultSet
                                                   where row.PersonResponsibleOLD == userid
                                                    && row.RoleID == 3
                                                    && row.AuditID == AudID
                                                    && row.TimeLine > DateTime.Now
                                                   select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                        else if (tag == "OverDue")
                        {
                            AuditSummaryRecords = (from row in ObjResultSet
                                                   where row.PersonResponsibleOLD == userid
                                                    && row.RoleID == 3
                                                    && row.AuditID == AudID
                                                    && row.TimeLine <= DateTime.Now
                                                   select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        AuditSummaryRecords = (from row in ObjResultSet
                                               where row.PersonResponsibleOLD == userid
                                                && row.RoleID == 3
                                                && row.AuditID == AudID
                                               select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    if (Branchlist.Count > 0)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Branchlist.Contains((int)Entry.CustomerBranchID)).ToList();
                    if (verticalid != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.VerticalID == verticalid).ToList();
                    if (Period != "")
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.ForMonth == Period).ToList();

                    NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).Count();

                    submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2 && (entity.AuditeeResponse == "PS" || entity.AuditeeResponse == "R3" || entity.AuditeeResponse == "2R")).Count();

                    TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                    AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6 && entity.AuditeeResponse == "PA").Count();

                    AuditeeSubmitcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6 && (entity.AuditeeResponse == "AS" || entity.AuditeeResponse == "R2")).Count();

                    AuditeeReviewCommentcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6 && entity.AuditeeResponse == "AR").Count();

                    FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                    closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).Count();

                    totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + AuditeeSubmitcount + AuditeeReviewCommentcount + FinalReviewcount + closecount;

                    if (AuditSummaryRecords.Count > 0)
                    {
                        InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                        ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                        CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                        financialyear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                        branch = AuditSummaryRecords.GroupBy(g => g.Branch).Select(g => g.FirstOrDefault()).Select(entry => entry.Branch).FirstOrDefault();
                        VerticalID = AuditSummaryRecords.GroupBy(g => g.VerticalID).Select(g => g.FirstOrDefault()).Select(entry => (int)entry.VerticalID).FirstOrDefault();
                        AuditName = AuditSummaryRecords.GroupBy(g => g.AuditName).Select(g => g.FirstOrDefault()).Select(entry => entry.AuditName).FirstOrDefault();
                        AuditeeResponse = AuditSummaryRecords.GroupBy(g => g.AuditeeResponse).Select(g => g.FirstOrDefault()).Select(entry => entry.AuditeeResponse).FirstOrDefault();
                    }
                    else
                    {
                        InstanceID = 0;
                        ScheduleOnID = 0;
                        CustomerBranchID = 0;
                        VerticalID = 0;
                    }

                    if (totalcount != 0)
                    {
                        totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                        totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                        totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                        totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                        totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                        totalAuditeeSubmitLastcount = totalAuditeeSubmitLastcount + Convert.ToInt32(AuditeeSubmitcount);
                        totalAuditeeReviewCommentLastcount = totalAuditeeReviewCommentLastcount + Convert.ToInt32(AuditeeReviewCommentcount);
                        totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                        totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                        //ProcessIDLast = Convert.ToInt32(prc.Id);
                        //ProcessNameLast = prc.Name.ToString();
                        //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);    
                        InstanceIDLast = Convert.ToInt32(InstanceID);
                        ScheduleOnIDLast = -1;
                        CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                        RoleidLast = Convert.ToInt32(Roleid);
                        VerticalIDLast = Convert.ToInt32(VerticalID);
                    }
                    if (totalLastcount > 0)
                    {
                        table.Rows.Add(CustomerBranchIDLast, financialyear, formonth, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalAuditeeSubmitLastcount, totalAuditeeReviewCommentLastcount, totalFinalReviewLastcount, totalcloseLastcount, InstanceIDLast, ScheduleOnIDLast, CustomerBranchIDLast, RoleidLast, VerticalIDLast, "Total", AuditName, AudID, AuditeeResponse);
                    }

                    totalLastcount = 0;
                    totalNotDoneLastcount = 0;
                    totalsubmitedLastcount = 0;
                    totalTeamReviewLastcount = 0;
                    totalAuditeeReviewLastcount = 0;
                    totalAuditeeSubmitLastcount = 0;
                    totalAuditeeReviewCommentLastcount = 0;
                    totalFinalReviewLastcount = 0;
                    totalcloseLastcount = 0;
                }

                return table;
            }
        }
        public static DataTable GetAuditTotalCountUserWiseIMP(int userid, List<int> Roleid, int customerid, int StatusID, string FinYear, List<long> Branchlist, string Period, int verticalid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int?> OpenImpStatusList = new List<int?>();
                OpenImpStatusList.Add(2);
                OpenImpStatusList.Add(3);
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long AuditeeSubmitcount;
                long AuditeeReviewCommentcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;
                string financialyear = string.Empty;
                string formonth = string.Empty;
                string branch = string.Empty;
                int VerticalID;
                string ProcessName = string.Empty;
                int totalLastcount = 0;
                int totalNotDoneLastcount = 0;
                int totalsubmitedLastcount = 0;
                int totalTeamReviewLastcount = 0;
                int totalAuditeeReviewLastcount = 0;
                int totalAuditeeSubmitLastcount = 0;
                int totalAuditeeReviewCommentLastcount = 0;
                int totalFinalReviewLastcount = 0;
                int totalcloseLastcount = 0;
                //Defined by sushant
                //int ProcessIDLast = 0;
                //string ProcessNameLast = null;
                int InstanceIDLast = 0;
                int ScheduleOnIDLast = 0;
                int CustomerBranchIDLast = 0;
                int RoleidLast = 0;
                int VerticalIDLast = 0;
                string AuditName = string.Empty;


                DataTable table = new DataTable();
                table.Columns.Add("Branch", typeof(string));
                table.Columns.Add("FinancialYear", typeof(string));
                table.Columns.Add("ForMonth", typeof(string));
                table.Columns.Add("Total", typeof(long));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("AuditeeSubmit", typeof(long));
                table.Columns.Add("ReviewComment", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("VerticalID", typeof(int));
                table.Columns.Add("ProcessName", typeof(string));
                table.Columns.Add("AuditName", typeof(string));
                table.Columns.Add("AuditID", typeof(string));

                List<Sp_ImplementationAuditSummaryCountView_Result> ObjResultSet = new List<Sp_ImplementationAuditSummaryCountView_Result>();

                ObjResultSet = (from row in entities.Sp_ImplementationAuditSummaryCountView(customerid)
                                select row).ToList();

                List<long> AuditIDList = new List<long>();
                if (StatusID == 3)
                {
                    AuditIDList = (from row in ObjResultSet
                                   where row.UserID == userid
                                   && Roleid.Contains(row.RoleID)
                                   && row.AuditStatusID == 3
                                   && row.FinancialYear == FinYear
                                   //&& row.AllClosed != null
                                   select (long)row.AuditID).Distinct().ToList();
                }
                else
                {
                    AuditIDList = (from row in ObjResultSet
                                   where row.UserID == userid
                                   && Roleid.Contains(row.RoleID)
                                   && row.FinancialYear == FinYear
                                   //&& row.AllClosed == null
                                   //&& row.AuditID == auditID
                                   select (long)row.AuditID).Distinct().ToList();
                }


                foreach (var AudID in AuditIDList)
                {
                    var AuditSummaryRecords = (from row in ObjResultSet
                                               where row.UserID == userid
                                                && Roleid.Contains(row.RoleID)
                                                && row.AuditID == AudID
                                               select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                    if (Branchlist.Count > 0)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Branchlist.Contains((int)Entry.CustomerBranchID)).ToList();
                    if (verticalid != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.VerticalID == verticalid).ToList();
                    if (Period != "")
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.ForMonth == Period).ToList();

                    NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).Count();
                    //NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID == 3)).Count();

                    submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2 && (entity.AuditeeResponse == "PS" || entity.AuditeeResponse == "R3" || entity.AuditeeResponse == "2R")).Count();

                    TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                    AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6 && entity.AuditeeResponse == "PA").Count();

                    AuditeeSubmitcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6 && (entity.AuditeeResponse == "AS" || entity.AuditeeResponse == "R2")).Count();

                    AuditeeReviewCommentcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6 && entity.AuditeeResponse == "AR").Count();

                    FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                    closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).Count();
                    //closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID != 3).Count();

                    totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + AuditeeSubmitcount + AuditeeReviewCommentcount + FinalReviewcount + closecount;

                    if (AuditSummaryRecords.Count > 0)
                    {
                        InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                        ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                        CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                        financialyear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                        branch = AuditSummaryRecords.GroupBy(g => g.Branch).Select(g => g.FirstOrDefault()).Select(entry => entry.Branch).FirstOrDefault();
                        VerticalID = AuditSummaryRecords.GroupBy(g => g.VerticalID).Select(g => g.FirstOrDefault()).Select(entry => (int)entry.VerticalID).FirstOrDefault();
                        //ProcessName = AuditSummaryRecords.GroupBy(g => g.ProcessName).Select(g => g.FirstOrDefault()).Select(entry => entry.ProcessName).FirstOrDefault();
                        AuditName = AuditSummaryRecords.GroupBy(g => g.AuditName).Select(g => g.FirstOrDefault()).Select(entry => entry.AuditName).FirstOrDefault();
                    }
                    else
                    {
                        InstanceID = 0;
                        ScheduleOnID = 0;
                        CustomerBranchID = 0;
                        VerticalID = 0;
                    }

                    if (totalcount != 0)
                    {
                        totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                        totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                        totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                        totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                        totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                        totalAuditeeSubmitLastcount = totalAuditeeSubmitLastcount + Convert.ToInt32(AuditeeSubmitcount);
                        totalAuditeeReviewCommentLastcount = totalAuditeeReviewCommentLastcount + Convert.ToInt32(AuditeeReviewCommentcount);
                        totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                        totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);
                        InstanceIDLast = Convert.ToInt32(InstanceID);
                        ScheduleOnIDLast = -1;
                        CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                        if (Roleid.Contains(3))
                        {
                            RoleidLast = 3;
                        }
                        else if (Roleid.Contains(4) || Roleid.Contains(5))
                        {
                            RoleidLast = 4;
                        }

                        VerticalIDLast = Convert.ToInt32(VerticalID);
                        table.Rows.Add(CustomerBranchIDLast, financialyear, formonth, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalAuditeeSubmitLastcount, totalAuditeeReviewCommentLastcount, totalFinalReviewLastcount, totalcloseLastcount, InstanceIDLast, ScheduleOnIDLast, CustomerBranchIDLast, RoleidLast, VerticalIDLast, "Total", AuditName, AudID);
                    }
                    totalLastcount = 0;
                    totalNotDoneLastcount = 0;
                    totalsubmitedLastcount = 0;
                    totalTeamReviewLastcount = 0;
                    totalAuditeeReviewLastcount = 0;
                    totalAuditeeSubmitLastcount = 0;
                    totalAuditeeReviewCommentLastcount = 0;
                    totalFinalReviewLastcount = 0;
                    totalcloseLastcount = 0;
                }

                return table;
            }
        }
        public static DataTable getDataMasterReport(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, string Period, List<long> BranchList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                string IssueNumber = null;
                string AuditArea = "", Business_Vertical = "", Business_Vertical1 = "";
                string Grouping = "", Category = "", SubCategory = "", Observation = "";
                string StateShort = "", State = "", Hub = "", Location = "";
                string State_Branch = "", S_No = "", Report_No_Date = "";
                string Impact = "", Root_Cause = "", Audit_Recommendation = "", Action_Plan = "", Action_Plan1 = "", Owner = "", Owner_Name_detail = "";
                string Response = "", Closure_Status = "", Remarks = "", Reporting_AC_period = "", AC_Closure = "";
                string Audit_area = "", Report_no_State_Hub = "", Observation_Root_Cause = "", Risk_Level = "", Staff = "";
                DateTime? Due_Date = null;
                DateTime? Due_Date1 = null;

                DataTable table = new DataTable();

                table.Columns.Add("IssueNumber", typeof(string));
                table.Columns.Add("AuditArea", typeof(string));
                table.Columns.Add("Grouping", typeof(string));
                table.Columns.Add("StateShort", typeof(string));
                table.Columns.Add("State_Branch", typeof(string));
                table.Columns.Add("S_No", typeof(string));
                table.Columns.Add("Report_No_Date", typeof(string));
                table.Columns.Add("Business_Vertical", typeof(string));
                table.Columns.Add("State", typeof(string));
                table.Columns.Add("Hub", typeof(string));
                table.Columns.Add("Location", typeof(string));
                table.Columns.Add("Category", typeof(string));
                table.Columns.Add("SubCategory", typeof(string));
                table.Columns.Add("Observation", typeof(string));
                table.Columns.Add("Impact", typeof(string));
                table.Columns.Add("Root_Cause", typeof(string));
                table.Columns.Add("Audit_Recommendation", typeof(string));
                table.Columns.Add("Action_Plan", typeof(string));
                table.Columns.Add("Owner", typeof(string));
                table.Columns.Add("Owner_Name_detail", typeof(string));
                table.Columns.Add("Due_Date", typeof(string));
                table.Columns.Add("Response", typeof(string));
                table.Columns.Add("Closure_Status", typeof(string));
                table.Columns.Add("Remarks", typeof(string));
                table.Columns.Add("Reporting_AC_period", typeof(string));
                table.Columns.Add("AC_Closure", typeof(string));
                table.Columns.Add("Due_Date1", typeof(string));
                table.Columns.Add("Audit_area", typeof(string));
                table.Columns.Add("Report_no_State_Hub", typeof(string));
                table.Columns.Add("Business_Vertical1", typeof(string));
                table.Columns.Add("Observation_Root_Cause", typeof(string));
                table.Columns.Add("Action_Plan1", typeof(string));
                table.Columns.Add("Risk_Level", typeof(string));
                table.Columns.Add("Staff", typeof(string));

                var detailView = (from row in entities.ObservationStatusUserWiseNew_DisplayView_New
                                  where row.CustomerID == customerid
                                  select row).ToList();

                if (userID != -1)
                {

                    detailView = detailView.Where(entry => entry.UserID == userID).ToList();

                }
                if (RoleID != -1)
                {

                    detailView = detailView.Where(Entry => (Entry.RoleID == RoleID)).ToList();
                }
                if (BranchList.Count > 0)
                {
                    List<int?> BranchAssigned = BranchList.Select(x => (int?)x).ToList();
                    detailView = detailView.Where(Entry => BranchAssigned.Contains(Entry.CustomerBranchID)).ToList();
                }

                if (FinancialYear != "")
                    detailView = detailView.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (Period != "")
                    detailView = detailView.Where(entry => entry.ForMonth == Period).ToList();

                if (ProcessID != -1)
                    detailView = detailView.Where(entry => entry.ProcessId == ProcessID).ToList();

                if (VerticalID != -1)
                    detailView = detailView.Where(entry => entry.VerticalID == VerticalID).ToList();

                string stateVal = "";
                string HUBVal = "";
                string stateBarnch = "";
                int S_No_Count = 0;
                foreach (var item in detailView)
                {
                    if (item.State != null)
                    {
                        stateVal = item.State.ToString();
                        stateBarnch = stateVal + '/' + item.Branch.ToString();
                    }
                    else
                    {
                        stateBarnch = item.Branch.ToString();
                    }
                    if (item.HUB != null)
                        HUBVal = item.HUB.ToString();

                    int Rate = Convert.ToInt32(item.ObservatioRating);

                    if (Rate == 1)
                    {
                        Risk_Level = "High";
                    }
                    else if (Rate == 2)
                    {
                        Risk_Level = "Medium";
                    }
                    else if (Rate == 3)
                    {
                        Risk_Level = "Low";
                    }


                    if (item.ObservationSubCategoryName != null)
                    {
                        string s = item.ObservationSubCategoryName.ToString();
                        SubCategory = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
                    }
                    if (item.ManagementResponse != null)
                    {
                        string s = item.ManagementResponse.ToString();
                        Action_Plan = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
                        Action_Plan1 = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
                    }
                    if (item.Designation != null)
                    {
                        string s = item.Designation.ToString();
                        Owner = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
                    }
                    if (item.PersonResponsibleName.ToString() != null)
                    {
                        string s = item.PersonResponsibleName.ToString();
                        Owner_Name_detail = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
                    }
                    if (item.Recomendation != null)
                    {
                        string s = item.Recomendation.ToString();
                        Audit_Recommendation = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
                    }
                    if (item.Observation != null)
                    {
                        string s = item.Observation.ToString();
                        Observation = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
                    }

                    S_No_Count = S_No_Count + 1;

                    if (item.IssueNumber != null)
                    {
                        IssueNumber = item.IssueNumber.ToString();
                    }
                    if (item.ProcessName != null)
                    {
                        AuditArea = item.ProcessName.ToString();
                    }
                    if (item.SubProcessName != null)
                    {
                        Grouping = item.SubProcessName.ToString();
                    }
                    if (stateVal != null)
                    {
                        StateShort = stateVal;
                    }
                    if (stateBarnch != null)
                    {
                        State_Branch = stateBarnch;
                    }

                    S_No = S_No_Count.ToString();

                    if (item.ScheduledOnID != null)
                    {
                        Report_No_Date = item.ScheduledOnID.ToString();
                    }
                    if (item.VerticalName != null)
                    {
                        Business_Vertical = item.VerticalName.ToString();
                    }
                    if (stateVal != null)
                    {
                        State = stateVal;
                    }
                    if (HUBVal != null)
                    {
                        Hub = HUBVal;
                    }


                    if (item.Location != null)
                    {
                        Location = item.Location.ToString();
                    }
                    if (item.ObservationCategoryName != null)
                    {
                        Category = item.ObservationCategoryName.ToString();
                    }
                    if (item.Impact != null)
                    {
                        Impact = item.Impact.ToString();
                    }
                    if (item.RootCost != null)
                    {
                        Root_Cause = item.RootCost.ToString();
                    }
                    if (item.TimeLine != null)
                    {
                        Due_Date = item.TimeLine;
                    }
                    if (item.Status != null)
                    {
                        Closure_Status = item.Status.ToString();
                    }
                    if (item.Status != null)
                    {
                        AC_Closure = item.Status.ToString();
                    }
                    if (item.TimeLine != null)
                    {
                        Due_Date1 = item.TimeLine;
                    }
                    if (item.ProcessName != null)
                    {
                        Audit_area = item.ProcessName.ToString();
                    }

                    Report_no_State_Hub = item.ScheduledOnID.ToString() + '/' + stateVal + '/' + HUBVal;
                    if (item.VerticalName != null)
                    {
                        Business_Vertical1 = item.VerticalName.ToString();
                    }
                    if (item.RootCost != null)
                    {
                        Observation_Root_Cause = "Observartion:" + item.Observation.ToString() + '.' + "Root Cause:" + item.RootCost.ToString();
                    }

                    table.Rows.Add(IssueNumber, AuditArea, Grouping, StateShort, State_Branch, S_No, Report_No_Date, Business_Vertical, State, Hub, Location, Category, SubCategory,
                    Observation, Impact, Root_Cause, Audit_Recommendation, Action_Plan, Owner, Owner_Name_detail, Due_Date, Response, Closure_Status, Remarks, Reporting_AC_period, AC_Closure,
                    Due_Date1, Audit_area, Report_no_State_Hub, Business_Vertical1, Observation_Root_Cause, Action_Plan1, Risk_Level, Staff);

                }

                return table;
            }
        }
        public static List<ObservationStatusUserWiseNew_DisplayView_New> getDatatest(int RoleID, int customerid, int userID, int VerticalID, int ProcessID, string FinancialYear, int months, List<long> BranchList)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var detailView = (from row in entities.ObservationStatusUserWiseNew_DisplayView_New
                                  where row.CustomerID == customerid
                                  select row).ToList();

                if (userID != -1)
                {
                    detailView = detailView.Where(entry => entry.UserID == userID).ToList();
                }
                if (RoleID != -1)
                {
                    detailView = detailView.Where(entry => entry.RoleID == RoleID).ToList();
                }
                if (BranchList.Count > 0)
                {
                    List<int?> BranchAssigned = BranchList.Select(x => (int?)x).ToList();
                    detailView = detailView.Where(Entry => BranchAssigned.Contains(Entry.CustomerBranchID)).ToList();
                }

                if (FinancialYear != "")
                    detailView = detailView.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                if (months != -1)
                    detailView = detailView.Where(entry => entry.CreatedDateMonth == months).ToList();

                if (ProcessID != -1)
                    detailView = detailView.Where(entry => entry.ProcessId == ProcessID).ToList();

                if (VerticalID != -1)
                    detailView = detailView.Where(entry => entry.VerticalID == VerticalID).ToList();



                return detailView.ToList();
            }
        }
        public static InternalAuditInstanceData GetAuditinstanceData(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var datafetch = (from ACD in entities.AuditClosureDetails
                                 join MCB in entities.mst_CustomerBranch
                                 on ACD.BranchId equals MCB.ID
                                 join MV in entities.mst_Vertical
                                 on ACD.VerticalId equals MV.ID
                                 where MCB.IsDeleted == false
                                 && ACD.ID == AuditID
                                 select new InternalAuditInstanceData
                                 {
                                     location = MCB.Name,
                                     FinancialYear = ACD.FinancialYear,
                                     period = ACD.ForMonth,
                                     verticalName = MV.VerticalName
                                 }).FirstOrDefault();
                return datafetch;
            }
        }
        public static List<long> GetIMPDistinctProcessIDs(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.AuditImplementationAssignments
                                   where row.AuditID == AuditID
                                   select (long)row.ProcessId).Distinct().ToList();

                return ProcessList.ToList();
            }
        }
        //public static List<SP_AuditPlannedActualDetailDisplay_Result> GetAuditPlannedActualDisplaySP(String Type, int customerid, List<int?> BranchList, string FinancialYear, string Period, DateTime FromDate, DateTime ToDate, int VerticalID, int UserID, bool IsDepartmentHead)
        public static List<SP_AuditPlannedActualDetailDisplay_Result> GetAuditPlannedActualDisplaySP(String Type, int customerid, List<long> BranchList, List<string> FinancialYearList, List<string> PeriodList, DateTime FromDate, DateTime ToDate, List<int> VerticalIDList, int UserID, bool IsDepartmentHead)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<SP_AuditPlannedActualDetailDisplay_Result> transactionsQuery = new List<SP_AuditPlannedActualDetailDisplay_Result>();

                if (Type != "")
                {
                    if (Type == "Planned")
                    {
                        //transactionsQuery = (from row in entities.Sp_AuditPlannedActualDetailDisplayView_bkpMadhur(UserID)
                        transactionsQuery = (from row in entities.SP_AuditPlannedActualDetailDisplay(UserID)
                                             where FromDate.Date <= row.StartDate
                                             && row.StartDate <= ToDate.Date
                                             && row.CustomerID == customerid
                                             select row).ToList().GroupBy(entity => entity.AuditID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    else if (Type == "Actual")
                    {
                        //transactionsQuery = (from row in entities.SP_AuditPlannedActualDetailDisplay(UserID)
                        transactionsQuery = (from row in entities.SP_AuditPlannedActualDetailDisplay(UserID)
                                             where FromDate.Date <= row.ExpectedStartDate
                                             && row.ExpectedStartDate <= ToDate.Date
                                             && row.CustomerID == customerid
                                             select row).ToList().GroupBy(entity => entity.AuditID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    if (IsDepartmentHead)
                    {
                        List<long> branchids = AssignEntityManagementRisk.CheckDepartMentHeadLocation(UserID);
                        List<int?> BranchAssigned = branchids.Select(x => (int?)x).ToList();
                        if (BranchAssigned.Count > 0)
                        {
                            transactionsQuery = transactionsQuery.Where(entry => BranchAssigned.Contains(entry.CustomerBranchId)).ToList();
                        }

                    }

                    if (BranchList.Count > 0)
                        transactionsQuery = transactionsQuery.Where(entry => BranchList.Contains(entry.CustomerBranchId)).ToList();

                    //if (CustBranchID != -1)
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchId == CustBranchID).ToList();

                    //if (FinancialYear != "")
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.FinancialYear == FinancialYear).ToList();

                    //if (Period != "Period" && Period != "")
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();

                    //if (VerticalID != -1)
                    //    transactionsQuery = transactionsQuery.Where(entry => entry.VerticalID == VerticalID).ToList();

                    //if (transactionsQuery.Count > 0)
                    //    transactionsQuery = transactionsQuery.OrderBy(entry => entry.Branch)
                    //        .ThenBy(entry => entry.VerticalName).ToList();

                    if (FinancialYearList.Count > 0)
                        transactionsQuery = transactionsQuery.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();

                    if (PeriodList.Count > 0)
                        transactionsQuery = transactionsQuery.Where(entry => PeriodList.Contains(entry.ForMonth)).ToList();

                    if (VerticalIDList.Count > 0)
                        transactionsQuery = transactionsQuery.Where(entry => VerticalIDList.Contains((int)entry.VerticalID)).ToList();

                    if (transactionsQuery.Count > 0)
                        transactionsQuery = transactionsQuery.OrderBy(entry => entry.Branch)
                            .ThenBy(entry => entry.VerticalName).ToList();

                    //if (transactionsQuery.Count>0)
                    //{
                    //    transactionsQuery = transactionsQuery.OrderBy(entry=> entry.Branch).ThenBy().To
                    //}
                    transactionsQuery = transactionsQuery.Distinct().ToList();

                }

                return transactionsQuery;
            }
        }
        public static DateTime GetLastDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
        }
        #region ICFR
        public static DataTable GetInternalAuditStatusProcessSubProcessUserWise(List<long> instanceids, int userid, int Roleid, int customerid, int ProcessID, int SubProcessID, string ForMonth)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long Opencount;
                long submitedcount;
                long ReviewCommentcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;

                DataTable table = new DataTable();

                table.Columns.Add("ProcessId", typeof(int));
                table.Columns.Add("SubProcessId", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("SubProcess", typeof(string));
                table.Columns.Add("Total", typeof(long));
                table.Columns.Add("Open", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("ReviewComment", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("RoleID", typeof(int));

                if (ProcessID != -1 && SubProcessID != -1)
                {
                    var subprocessList = GetSubProcessDetails(ProcessID, customerid);

                    subprocessList = subprocessList.Where(Entry => Entry.SubProcessID == SubProcessID).ToList();

                    entities.Database.CommandTimeout = 300;
                    var MasterRecords = (from row in entities.AuditInstanceTransactionViews
                                         where row.CustomerID == customerid
                                         && row.UserID == userid && row.RoleID == Roleid
                                         select row).ToList();

                    foreach (ProcessSubProcessView cc in subprocessList)
                    {
                        var AuditSummaryRecords = (from row in MasterRecords
                                                   where row.ProcessId == cc.ProcessID
                                                   && row.SubProcessId == cc.SubProcessID
                                                   && row.UserID == userid
                                                   && row.RoleID == Roleid
                                                   && row.ForMonth == ForMonth
                                                   && instanceids.Contains(row.AuditInstanceID)
                                                   select row).ToList().GroupBy(entity => entity.RiskCreationId).Select(entity => entity.FirstOrDefault()).ToList();




                        Opencount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 1).Count();

                        submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                        ReviewCommentcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                        totalcount = Opencount + submitedcount + ReviewCommentcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = AuditSummaryRecords.GroupBy(g => g.AuditInstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.AuditInstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                        }

                        if (totalcount != 0)
                            table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, Opencount, submitedcount, ReviewCommentcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, Roleid);
                    }
                }
                else if (ProcessID != -1 && SubProcessID == -1)
                {
                    var subprocessList = GetSubProcessDetails(ProcessID, customerid);
                    entities.Database.CommandTimeout = 300;
                    var MasterRecords = (from row in entities.AuditInstanceTransactionViews
                                         where row.CustomerID == customerid
                                         select row).ToList();


                    foreach (ProcessSubProcessView cc in subprocessList)
                    {
                        var AuditSummaryRecords = (from row in MasterRecords
                                                   where row.ProcessId == cc.ProcessID
                                                   && row.SubProcessId == cc.SubProcessID
                                                   && row.UserID == userid
                                                   && row.RoleID == Roleid
                                                   && row.ForMonth == ForMonth
                                                   && instanceids.Contains(row.AuditInstanceID)
                                                   select row).ToList().GroupBy(entity => entity.RiskCreationId).Select(entity => entity.FirstOrDefault()).ToList();


                        Opencount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 1).Count();

                        submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                        ReviewCommentcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                        totalcount = Opencount + submitedcount + ReviewCommentcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = AuditSummaryRecords.GroupBy(g => g.AuditInstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.AuditInstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                        }

                        if (totalcount != 0)
                            table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, Opencount, submitedcount, ReviewCommentcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, Roleid);
                    }

                }

                else
                {
                    var processids = GetDistinctProcessIDsInternalAudit(instanceids);

                    if (processids.Count > 0)
                    {
                        processids.ForEach(ProcessId =>
                        {
                            var subprocessList = GetSubProcessDetails(ProcessId, customerid);
                            entities.Database.CommandTimeout = 300;
                            var MasterRecords = (from row in entities.AuditInstanceTransactionViews
                                                 where row.CustomerID == customerid
                                                 select row).ToList();

                            foreach (ProcessSubProcessView cc in subprocessList)
                            {
                                var AuditSummaryRecords = (from row in MasterRecords
                                                           where row.ProcessId == cc.ProcessID
                                                           && row.SubProcessId == cc.SubProcessID
                                                           && row.UserID == userid
                                                           && row.RoleID == Roleid
                                                           && row.ForMonth == ForMonth
                                                           && instanceids.Contains(row.AuditInstanceID)
                                                           select row).ToList().GroupBy(entity => entity.RiskCreationId).Select(entity => entity.FirstOrDefault()).ToList();
                                //if (!string.IsNullOrEmpty(ForMonth))
                                //    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => FrequencyIds.Contains((int) Entry.Frequency)).ToList();


                                Opencount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 1).Count();

                                submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                                ReviewCommentcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                                closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                                totalcount = Opencount + submitedcount + ReviewCommentcount + closecount;

                                if (AuditSummaryRecords.Count > 0)
                                {
                                    InstanceID = AuditSummaryRecords.GroupBy(g => g.AuditInstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.AuditInstanceID).FirstOrDefault();
                                    ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                                    CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                                }
                                else
                                {
                                    InstanceID = 0;
                                    ScheduleOnID = 0;
                                    CustomerBranchID = 0;
                                }

                                if (totalcount != 0)
                                    table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, Opencount, submitedcount, ReviewCommentcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, Roleid);
                            }
                        });
                    }
                }

                return table;
            }
        }
        #endregion
        #region ARS      
        //public static List<ObservationAging_DisplayView> GetOpenObservationsDetailView(String Type, long customerid, List<int?> BranchList, string FinancialYear, string Period, int ProcessID, int VerticalID, int UserID, bool IsAuditManager, bool IsManagement, bool IsDepartmentHead)
        //{
        //    List<ObservationAging_DisplayView> masterquery = new List<ObservationAging_DisplayView>();
        //    List<ObservationAging_DisplayView> transactionquery = new List<ObservationAging_DisplayView>();

        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        masterquery = (from row in entities.ObservationAging_DisplayView
        //                       where row.CustomerID == customerid
        //                       select row).ToList();


        //        if (IsAuditManager)
        //        {
        //            transactionquery = (from row in masterquery
        //                                join EAAR in entities.EntitiesAssignmentAuditManagerRisks
        //                                on row.ProcessId equals EAAR.ProcessId
        //                                //row.CustomerBranchID equals (int)EAAR.BranchID
        //                                where EAAR.UserID == UserID
        //                                && EAAR.ISACTIVE == true
        //                                select row).Distinct().ToList();
        //            //List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(UserID);
        //            //List<int?> BranchAssigned = branchids.Select(x => (int?)x).ToList();
        //            //transactionquery = transactionquery.Where(Entry => BranchAssigned.Contains(Entry.CustomerBranchID)).ToList();
        //        }
        //        if (IsDepartmentHead)
        //        {
        //            transactionquery = (from row in masterquery
        //                                join EAAR in entities.EntitiesAssignmentDepartmentHeads
        //                                on row.DepartmentID equals (int)EAAR.DepartmentID
        //                                join MSP in entities.Mst_Process
        //                                on row.ProcessId equals MSP.Id
        //                                where row.CustomerBranchID == EAAR.BranchID
        //                                && EAAR.UserID == UserID
        //                                && EAAR.ISACTIVE == true
        //                                select row).Distinct().ToList();

        //            //transactionquery = (from row in masterquery
        //            //                    join EAAR in entities.EntitiesAssignmentDepartmentHeads
        //            //                    on row.DepartmentID equals (int) EAAR.DepartmentID
        //            //                    join MSP in entities.Mst_Process
        //            //                    on row.ProcessId equals MSP.Id
        //            //                    where EAAR.UserID == UserID
        //            //                    && EAAR.ISACTIVE == true
        //            //                    select row).Distinct().ToList();

        //            //List<long> branchids = AssignEntityManagementRisk.CheckDepartMentHeadLocation(UserID);
        //            //List<long> DepartmentIds = AssignEntityManagementRisk.CheckDepartMentHeadDepartments(UserID);
        //            //List<int?> BranchAssigned = branchids.Select(x => (int?)x).ToList();

        //            //var ProcessList = GetAllDepartmentProcess(DepartmentIds);
        //            //if (BranchAssigned.Count > 0)
        //            //{
        //            //    transactionquery = transactionquery.Where(entry => BranchAssigned.Contains(entry.CustomerBranchID)).ToList();
        //            //}
        //            ////Process List
        //            //if (ProcessList.Count > 0)
        //            //{
        //            //    transactionquery = transactionquery.Where(entry => ProcessList.Contains(entry.ProcessId)).ToList();
        //            //}
        //        }
        //        if (IsManagement)
        //        {
        //            transactionquery = (from row in masterquery
        //                                join EAAR in entities.EntitiesAssignmentManagementRisks
        //                                on row.ProcessId equals EAAR.ProcessId
        //                                where EAAR.UserID == UserID
        //                                && EAAR.ISACTIVE == true
        //                                select row).Distinct().ToList();

        //            //List<long> branchids = AssignEntityManagementRisk.CheckManngementLocation(UserID);
        //            //List<int?> BranchAssigned = branchids.Select(x => (int?)x).ToList();
        //            //transactionquery = transactionquery.Where(Entry => BranchAssigned.Contains(Entry.CustomerBranchID)).ToList();
        //        }
        //        if (!String.IsNullOrEmpty(Type))
        //        {
        //            List<int?> ImplementedStatusList = new List<int?> { 1, 4, 5 };

        //            if (BranchList.Count > 0)
        //                transactionquery = transactionquery.Where(entry => BranchList.Contains(entry.CustomerBranchID)).ToList();

        //            if (FinancialYear != "")
        //                transactionquery = transactionquery.Where(entry => entry.FinancialYear == FinancialYear).ToList();

        //            if (Period != "")
        //                transactionquery = transactionquery.Where(entry => entry.ForMonth == Period).ToList();

        //            if (ProcessID != -1)
        //                transactionquery = transactionquery.Where(entry => entry.ProcessId == ProcessID).ToList();

        //            if (VerticalID != -1)
        //                transactionquery = transactionquery.Where(entry => entry.VerticalID == VerticalID).ToList();

        //            if (Type == "NotDue")
        //            {
        //                transactionquery = transactionquery.Where(Entry => Entry.TimeLine >= DateTime.Now.Date
        //                && (!ImplementedStatusList.Contains(Entry.ImplementationStatus) || Entry.ImplementationStatus != null)).ToList();
        //            }
        //            else if (Type == "LessThan45")
        //            {
        //                transactionquery = transactionquery.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-45) && Entry.TimeLine < DateTime.Now.Date
        //                && (!ImplementedStatusList.Contains(Entry.ImplementationStatus) || Entry.ImplementationStatus != null)).ToList();
        //            }
        //            else if (Type == "GreaterThan45")
        //            {
        //                transactionquery = transactionquery.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-90) && Entry.TimeLine < DateTime.Now.AddDays(-45)
        //                && (!ImplementedStatusList.Contains(Entry.ImplementationStatus) || Entry.ImplementationStatus != null)).ToList();
        //            }
        //            else if (Type == "GreaterThan90")
        //            {
        //                transactionquery = transactionquery.Where(Entry => Entry.TimeLine <= DateTime.Now.AddDays(-90)
        //                && (!ImplementedStatusList.Contains(Entry.ImplementationStatus) || Entry.ImplementationStatus != null)).ToList();
        //            }
        //        }
        //    }

        //    return transactionquery;
        //}

        public static List<SP_ObservationAging_DisplayView_Result> GetOpenObservationsDetailView(List<string> Type, int customerid, List<long> BranchList, List<string> FinancialYear, List<string> PeriodList, List<long> ProcessIDList, List<int> VerticalIDList, int UserID, bool IsAuditManager, bool IsManagement, bool IsDepartmentHead, List<long?> SubProcessIDList)
        {
            List<SP_ObservationAging_DisplayView_Result> masterquery = new List<SP_ObservationAging_DisplayView_Result>();
            List<SP_ObservationAging_DisplayView_Result> transactionquery = new List<SP_ObservationAging_DisplayView_Result>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                masterquery = (from row in entities.SP_ObservationAging_DisplayView(customerid)
                               select row).GroupBy(g => new { g.AuditID, g.ATBDId }).Select(g => g.FirstOrDefault()).ToList();

                if (IsAuditManager)
                {
                    transactionquery = (from row in masterquery
                                        join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                        on row.ProcessId equals EAAR.ProcessId
                                        //row.CustomerBranchID equals (int)EAAR.BranchID
                                        where EAAR.UserID == UserID
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();
                }
                if (IsDepartmentHead)
                {
                    transactionquery = (from row in masterquery
                                        join EAAR in entities.EntitiesAssignmentDepartmentHeads
                                        on row.DepartmentID equals (int)EAAR.DepartmentID
                                        join MSP in entities.Mst_Process
                                        on row.ProcessId equals MSP.Id
                                        where row.CustomerBranchID == EAAR.BranchID
                                        && EAAR.UserID == UserID
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();
                }
                if (IsManagement)
                {
                    transactionquery = (from row in masterquery
                                        join EAAR in entities.EntitiesAssignmentManagementRisks
                                        on row.ProcessId equals EAAR.ProcessId
                                        where EAAR.UserID == UserID
                                        && row.CustomerBranchID == EAAR.BranchID
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();
                }
                if (Type.Count > 0)
                {
                    List<int?> ImplementedStatusList = new List<int?> { 2, 4, 5 };

                    if (BranchList.Count > 0)
                        transactionquery = transactionquery.Where(entry => BranchList.Contains(entry.CustomerBranchID)).ToList();

                    if (FinancialYear.Count > 0)
                        transactionquery = transactionquery.Where(entry => FinancialYear.Contains(entry.FinancialYear)).ToList();

                    if (PeriodList.Count > 0)
                        transactionquery = transactionquery.Where(entry => PeriodList.Contains(entry.ForMonth)).ToList();

                    if (ProcessIDList.Count > 0)
                        transactionquery = transactionquery.Where(entry => ProcessIDList.Contains(entry.ProcessId)).ToList();

                    if (SubProcessIDList.Count > 0)
                        transactionquery = transactionquery.Where(entry => SubProcessIDList.Contains(entry.SubProcessId)).ToList();

                    if (VerticalIDList.Count > 0)
                        transactionquery = transactionquery.Where(entry => VerticalIDList.Contains((int)entry.VerticalID)).ToList();

                    if (Type.Contains("NotDue"))
                    {
                        transactionquery = transactionquery.Where(Entry => Entry.TimeLine >= DateTime.Now.Date
                        && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).ToList();
                    }
                    else if (Type.Contains("LessThan45"))
                    {
                        transactionquery = transactionquery.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-45) && Entry.TimeLine < DateTime.Now.Date
                        && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).ToList();
                    }
                    else if (Type.Contains("GreaterThan45"))
                    {
                        transactionquery = transactionquery.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-90) && Entry.TimeLine < DateTime.Now.AddDays(-45)
                        && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).ToList();
                    }
                    else if (Type.Contains("GreaterThan90"))
                    {
                        transactionquery = transactionquery.Where(Entry => Entry.TimeLine <= DateTime.Now.AddDays(-90)
                        && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).ToList();
                    }
                }

                transactionquery = transactionquery.Distinct().ToList();
            }
            return transactionquery;
        }

        public static List<SP_ObservationAging_DisplayView_Result> GetObservationDetailView(int customerid, List<long> BranchList, List<string> FinancialYearList, List<string> PeriodList, List<int> ObsRating, string ObjCatName, List<long> ProcessIDList, int UserID, bool IsAuditManager, bool IsManagement, bool IsDepartmentHead, List<long?> SubProcessIdList, List<int> VerticalIDList)
        {
            List<SP_ObservationAging_DisplayView_Result> masterquery = new List<SP_ObservationAging_DisplayView_Result>();
            List<SP_ObservationAging_DisplayView_Result> transactionquery = new List<SP_ObservationAging_DisplayView_Result>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                masterquery = (from row in entities.SP_ObservationAging_DisplayView(customerid)
                               where (row.CustomerID == customerid
                               && ObsRating.Contains((int)row.ObservatioRating))
                               select row).GroupBy(g => new { g.AuditID, g.ATBDId }).Select(g => g.FirstOrDefault()).ToList();

                if (IsAuditManager)
                {
                    transactionquery = (from row in masterquery
                                        join EAAR in entities.EntitiesAssignmentAuditManagerRisks
                                        on row.CustomerBranchID equals (int)EAAR.BranchID
                                        where EAAR.ProcessId == row.ProcessId
                                        && EAAR.UserID == UserID
                                        && EAAR.CustomerID == customerid
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();

                    //List<long> branchids = AssignEntityManagementRisk.CheckAuditManagerLocation(UserID);
                    //List<int?> BranchAssigned = branchids.Select(x => (int?) x).ToList();
                    //detailView = detailView.Where(Entry => BranchAssigned.Contains(Entry.CustomerBranchID)).ToList();
                }
                if (IsDepartmentHead)
                {
                    transactionquery = (from row in masterquery
                                        join EAAR in entities.EntitiesAssignmentDepartmentHeads
                                        on row.DepartmentID equals (int)EAAR.DepartmentID
                                        join MSP in entities.Mst_Process
                                        on row.ProcessId equals MSP.Id
                                        where row.CustomerBranchID == EAAR.BranchID
                                        && EAAR.UserID == UserID
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();


                    // List<long> branchids = AssignEntityManagementRisk.CheckDepartMentHeadLocation(UserID);
                    //List<long> DepartmentIds = AssignEntityManagementRisk.CheckDepartMentHeadDepartments(UserID);
                    //List<int?> BranchAssigned = branchids.Select(x => (int?) x).ToList();

                    //var ProcessList = GetAllDepartmentProcess(DepartmentIds);
                    //if (BranchAssigned.Count > 0)
                    //{
                    //    transactionquery = transactionquery.Where(entry => BranchAssigned.Contains(entry.CustomerBranchID)).ToList();
                    //}
                    ////Process List
                    //if (ProcessList.Count > 0)
                    //{
                    //    transactionquery = transactionquery.Where(entry => ProcessList.Contains(entry.ProcessId)).ToList();
                    //}                   
                }
                if (IsManagement)
                {
                    transactionquery = (from row in masterquery
                                        join EAAR in entities.EntitiesAssignmentManagementRisks
                                        on row.CustomerBranchID equals (int)EAAR.BranchID
                                        where EAAR.ProcessId == row.ProcessId
                                        && EAAR.UserID == UserID
                                        && EAAR.ISACTIVE == true
                                        select row).Distinct().ToList();

                    // List<long> branchids = AssignEntityManagementRisk.CheckManngementLocation(UserID);
                    //List<int?> BranchAssigned = branchids.Select(x => (int?) x).ToList();
                    //transactionquery = transactionquery.Where(Entry => BranchAssigned.Contains(Entry.CustomerBranchID)).ToList();
                }

                if (BranchList.Count > 0)
                    transactionquery = transactionquery.Where(entry => BranchList.Contains(entry.CustomerBranchID)).ToList();


                if (ProcessIDList.Count > 0)
                    transactionquery = transactionquery.Where(entry => ProcessIDList.Contains(entry.ProcessId)).ToList();

                if (SubProcessIdList.Count > 0)
                    transactionquery = transactionquery.Where(entry => SubProcessIdList.Contains(entry.SubProcessId)).ToList();

                if (!string.IsNullOrEmpty(ObjCatName))
                    transactionquery = transactionquery.Where(entry => entry.ObservationCategoryName == ObjCatName).ToList();

                if (FinancialYearList.Count > 0)
                    transactionquery = transactionquery.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();

                if (PeriodList.Count > 0)
                    transactionquery = transactionquery.Where(entry => PeriodList.Contains(entry.ForMonth)).ToList();

                if (transactionquery.Count > 0)
                {
                    transactionquery = transactionquery.OrderBy(entry => entry.Branch).ToList();
                }
                if (VerticalIDList.Count > 0)
                {
                    transactionquery = transactionquery.Where(entry => VerticalIDList.Contains((int)entry.VerticalID)).ToList();
                }
            }

            return transactionquery;
        }
        //public static List<Sp_AuditPlannedActualDetailDisplayView_bkpMadhur_Result> GetAuditPlannedActualDisplayView(String Type, int customerid, List<int?> BranchList, string FinancialYear, string Period, DateTime FromDate, DateTime ToDate, int VerticalID, int UserID, bool IsAuditManager, bool IsManagement)
        public static List<Sp_AuditPlannedActualDetailDisplayView_Result> GetAuditPlannedActualDisplayView(String Type, int customerId, List<long> BranchList, List<string> FinancialYearList, List<string> PeriodList, DateTime FromDate, DateTime ToDate, List<int> VerticalIDList, int UserID, bool IsAuditManager, bool IsManagement)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_AuditPlannedActualDetailDisplayView_Result> MastertransactionsQuery = new List<Sp_AuditPlannedActualDetailDisplayView_Result>();

                if (IsAuditManager)
                {
                    MastertransactionsQuery = (from row in entities.Sp_AuditPlannedActualDetailDisplayView(UserID, "AM")
                                               where row.CustomerID == customerId
                                               select row).ToList();

                }
                if (IsManagement)
                {
                    MastertransactionsQuery = (from row in entities.Sp_AuditPlannedActualDetailDisplayView(UserID, "MA")
                                               where row.CustomerID == customerId
                                               select row).ToList();

                }
                MastertransactionsQuery = MastertransactionsQuery.GroupBy(a => a.AuditID).Select(a => a.First()).ToList();
                if (Type != "")
                {
                    if (Type == "Planned")
                    {
                        MastertransactionsQuery = (from row in MastertransactionsQuery
                                                   where FromDate.Date <= row.StartDate
                                                   && row.StartDate <= ToDate.Date
                                                   select row).ToList();
                    }
                    else if (Type == "Actual")
                    {
                        MastertransactionsQuery = (from row in MastertransactionsQuery
                                                   where FromDate.Date <= row.ExpectedStartDate
                                                   && row.ExpectedStartDate <= ToDate.Date
                                                   select row).ToList();
                    }

                    if (BranchList.Count > 0)
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => BranchList.Contains(entry.CustomerBranchId)).ToList();

                    if (FinancialYearList.Count > 0)
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();

                    if (PeriodList.Count > 0)
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => PeriodList.Contains(entry.ForMonth)).ToList();

                    if (VerticalIDList.Count > 0)
                        MastertransactionsQuery = MastertransactionsQuery.Where(entry => VerticalIDList.Contains((int)entry.VerticalID)).ToList();

                    if (MastertransactionsQuery.Count > 0)
                        MastertransactionsQuery = MastertransactionsQuery.OrderBy(entry => entry.Branch)
                            .ThenBy(entry => entry.VerticalName).ToList();

                }
                return MastertransactionsQuery;
            }
        }

        public static List<long> GetAllDepartmentProcess(List<long> Departmentid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var complianceCategorys = (from row in entities.Mst_Process
                                           where row.IsDeleted == false && Departmentid.Contains((long)row.DepartmentID)
                                           select row.Id).ToList();

                if (complianceCategorys.Count > 0)
                {
                    return complianceCategorys;
                }
                else
                {
                    return null;
                }
            }
        }
        public static List<long> GetDistinctProcessIDsInternalAudit(List<long> InstanceIDs)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.AuditInstanceTransactionViews
                                   where InstanceIDs.Contains(row.AuditInstanceID)
                                   select (long)row.ProcessId).Distinct().ToList();

                return ProcessList.ToList();
            }
        }
        public static AuditScheduleOn GetInternalAuditScheduleOnDetails(int ScheduleOnID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.AuditScheduleOns
                                   where row.ID == ScheduleOnID
                                   select row).FirstOrDefault();

                return ProcessList;
            }
        }
        #endregion
        #region Person Responsilbe
        public static List<SP_PersonResponsible_AuditStatusSummary_Result> GetPersonResponsibleAuditSteps(int userid, int StatusID, string Status)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (StatusID == 1)
                {
                    var AuditSummaryMasterRecords = (from row in entities.SP_PersonResponsible_AuditStatusSummary(userid)
                                                     select row).ToList();

                    var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                               where row.AuditeeResponse == Status
                                               select row).Distinct().ToList();
                    var aa = AuditSummaryRecords.GroupBy(a => new { a.AuditID, a.ATBDID }).Select(entity => entity.FirstOrDefault()).ToList();
                    //var aa = AuditSummaryRecords.GroupBy(a => a.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();
                    return aa;
                }
                else
                {
                    var AuditSummaryMasterRecords = (from row in entities.SP_PersonResponsible_AuditStatusSummary(userid)
                                                     select row).ToList();

                    var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                               where row.PersonResponsible == userid
                                               && row.RoleID == 4
                                               && row.AuditeeResponse == Status
                                               select row).ToList();

                    var aa = AuditSummaryRecords.GroupBy(a => new { a.AuditID, a.ATBDID }).Select(entity => entity.FirstOrDefault()).ToList();
                    return aa;
                }
            }
        }
        public static List<Sp_ImplementationAuditSummaryCountView_Result> GetPersonResponsibleAuditStepsIMP(int customerid, int CustBranchid, int VerticalID, string FinYear, int Scheduleonid, string period, int userid, int roleid, List<int> statusIDs, List<int?> statusNullableIDs, string filter, long AuditID, string tag, List<string> AudResponse)
        {
            List<int?> OpenImpStatusList = new List<int?>();
            OpenImpStatusList.Add(2);
            OpenImpStatusList.Add(3);
            List<Sp_ImplementationAuditSummaryCountView_Result> AuditSummaryRecords = new List<Sp_ImplementationAuditSummaryCountView_Result>();
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (Scheduleonid == -1)
                {
                    if (!string.IsNullOrEmpty(tag))
                    {
                        if (tag == "Due")
                        {
                            AuditSummaryRecords = (from row in entities.Sp_ImplementationAuditSummaryCountView(customerid)
                                                   where row.PersonResponsibleOLD == userid
                                                   && row.RoleID == roleid
                                                   && row.FinancialYear == FinYear
                                                   && row.ForMonth == period
                                                   && row.AuditID == AuditID
                                                   && row.TimeLine > DateTime.Now
                                                   && row.AuditeeResponse == null
                                                   select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                        else if (tag == "OverDue")
                        {
                            AuditSummaryRecords = (from row in entities.Sp_ImplementationAuditSummaryCountView(customerid)
                                                   where row.PersonResponsibleOLD == userid
                                                   && row.RoleID == roleid
                                                   && row.FinancialYear == FinYear
                                                   && row.ForMonth == period
                                                   && row.AuditID == AuditID
                                                   && row.TimeLine <= DateTime.Now
                                                   && row.AuditeeResponse == null
                                                   select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                    }
                    else
                    {
                        AuditSummaryRecords = (from row in entities.Sp_ImplementationAuditSummaryCountView(customerid)
                                               where row.PersonResponsibleOLD == userid
                                               && row.RoleID == roleid
                                               && row.FinancialYear == FinYear
                                               && row.ForMonth == period
                                               && row.AuditID == AuditID
                                               && AudResponse.Contains(row.AuditeeResponse)
                                               select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    if (CustBranchid != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                    if (VerticalID != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                    if (filter.Equals("NotDone"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).ToList();

                    else if (filter.Equals("Total"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                    else if (filter.Equals("AuditeeReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && Entry.AuditeeResponse == "PA").ToList();

                    else if (filter.Equals("AuditeeSubmit"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && (Entry.AuditeeResponse == "AS" || Entry.AuditeeResponse == "R2")).ToList();

                    else if (filter.Equals("ReviewComment"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && Entry.AuditeeResponse == "AR").ToList();

                    else if (filter.Equals("Submitted"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2) && (Entry.AuditeeResponse == "PS" || Entry.AuditeeResponse == "R3" || Entry.AuditeeResponse == "2R")).ToList();

                    else if (filter.Equals("TeamReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                    else if (filter.Equals("FinalReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                    else if (filter.Equals("Closed"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).ToList();

                    return AuditSummaryRecords;
                }
                else
                {
                    AuditSummaryRecords = (from row in entities.Sp_ImplementationAuditSummaryCountView(customerid)
                                           where
                                           //&& row.UserID == userid
                                           row.RoleID == roleid
                                           && row.FinancialYear == FinYear
                                           && row.ForMonth == period && row.ScheduledOnID == Scheduleonid
                                           && row.AuditID == AuditID
                                           select row).ToList().GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();


                    if (CustBranchid != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                    if (VerticalID != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                    if (filter.Equals("NotDone"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).ToList();

                    else if (filter.Equals("Total"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                    else if (filter.Equals("AuditeeReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && Entry.AuditeeResponse == "PA").ToList();

                    else if (filter.Equals("AuditeeSubmit"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && (Entry.AuditeeResponse == "AS" || Entry.AuditeeResponse == "R2")).ToList();

                    else if (filter.Equals("ReviewComment"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && Entry.AuditeeResponse == "AR").ToList();

                    else if (filter.Equals("Submitted"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2) && (Entry.AuditeeResponse == "PS" || Entry.AuditeeResponse == "R3" || Entry.AuditeeResponse == "2R")).ToList();

                    else if (filter.Equals("TeamReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                    else if (filter.Equals("FinalReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                    else if (filter.Equals("Closed"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).ToList();

                    return AuditSummaryRecords;
                }
            }
        }
        public static List<PersonResponsibleImplementationAuditStatusDisplayView> GetPersonResponsibleAuditStepsIMP(int userid, int customerid, int CustBranchid, int VerticalID, string FinYear, int ScheduledonID, string forperiod, List<int> statusIDs, List<int?> statusNullableIDs, string filter, long AuditID)
        {
            List<PersonResponsibleImplementationAuditStatusDisplayView> detailView = new List<PersonResponsibleImplementationAuditStatusDisplayView>();

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditSummaryRecords = (from row in entities.PersonResponsibleImplementationAuditStatusDisplayViews
                                           where row.PersonResponsible == userid
                                           && row.CustomerBranchID == CustBranchid
                                           && row.VerticalID == VerticalID
                                          && row.AuditID == AuditID
                                           select row).ToList().GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();


                if (FinYear != "")
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                if (forperiod != "")
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.ForMonth == forperiod).ToList();
                if (filter.Equals("NotDone"))
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                else if (filter.Equals("Total"))
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                else if (filter.Equals("Submitted"))
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                else if (filter.Equals("TeamReview"))
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                else if (filter.Equals("AuditeeReview"))
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                else if (filter.Equals("FinalReview"))
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                else if (filter.Equals("Closed"))
                    AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                return AuditSummaryRecords;
            }
        }
        public static DataTable GetPersonResponsibleStatusProcessSubProcessUserWise(int userid, int customerid, int ProcessID, int SubProcessID, int custbranchid, int? verticalid, long auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;
                int VerticalID;

                int totalLastcount = 0;
                int totalAuditeeReviewLastcount = 0;
                int totalFinalReviewLastcount = 0;
                int totalcloseLastcount = 0;
                int CustomerBranchIDLast = 0;
                int VerticalIDLast = 0;
                string formonth = null;
                string FinancialYear = null;

                DataTable table = new DataTable();

                table.Columns.Add("ProcessId", typeof(int));
                table.Columns.Add("SubProcessId", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("SubProcess", typeof(string));
                table.Columns.Add("Total", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("VerticalID", typeof(int));
                table.Columns.Add("ForMonth", typeof(string));
                table.Columns.Add("FinancialYear", typeof(string));

                if (ProcessID != -1 && SubProcessID != -1)
                {
                    var subprocessList = GetSubProcessDetails(ProcessID, customerid);

                    subprocessList = subprocessList.Where(Entry => Entry.SubProcessID == SubProcessID).ToList();

                    var AuditSummaryMasterRecords = (from row in entities.SP_PersonResponsible_AuditStatusSummary(userid)
                                                     select row).GroupBy(a => a.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();


                    foreach (ProcessSubProcessView cc in subprocessList)
                    {

                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == cc.ProcessID
                                                   && row.SubProcessId == cc.SubProcessID
                                                   && row.RoleID == 4
                                                   && row.CustomerBranchID == custbranchid
                                                   && row.VerticalId == verticalid
                                                   && row.AuditID == auditID
                                                   && row.PersonResponsible == userid
                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();



                        AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                        FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                        totalcount = /*NotDonecount + submitedcount + TeamReviewcount +*/ AuditeeReviewcount + FinalReviewcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = (long)AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                            VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                            formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                            FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                            VerticalID = 0;
                        }

                        if (totalcount != 0)
                        {
                            totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                            totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                            totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                            totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                            table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID, formonth, FinancialYear);

                            CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                            VerticalIDLast = Convert.ToInt32(VerticalID);
                        }
                    }

                    table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, VerticalIDLast, formonth, FinancialYear);
                }
                else if (ProcessID != -1 && SubProcessID == -1)
                {
                    var subprocessList = GetSubProcessDetails(ProcessID, customerid);

                    var AuditSummaryMasterRecords = (from row in entities.SP_PersonResponsible_AuditStatusSummary(userid)
                                                     select row).GroupBy(a => a.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                    foreach (ProcessSubProcessView cc in subprocessList)
                    {
                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == cc.ProcessID
                                                   && row.SubProcessId == cc.SubProcessID
                                                   //&& instanceids.Contains((long) row.InstanceID)
                                                   && row.PersonResponsible == userid
                                                     && row.RoleID == 4
                                                     && row.CustomerBranchID == custbranchid
                                                   && row.VerticalId == verticalid
                                                   && row.AuditID == auditID
                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();


                        AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                        FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                        totalcount = /*NotDonecount + submitedcount + TeamReviewcount +*/ AuditeeReviewcount + FinalReviewcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = (long)AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                            VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                            formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                            FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                            VerticalID = 0;
                        }

                        if (totalcount != 0)
                        {
                            totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                            totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                            totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                            totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                            table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID, formonth, FinancialYear);

                            CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                            VerticalIDLast = Convert.ToInt32(VerticalID);
                        }
                    }
                    table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, VerticalIDLast, formonth, FinancialYear);
                }
                else
                {
                    var processids = GetDistinctProcessIDs(Convert.ToInt32(auditID));

                    if (processids.Count > 0)
                    {
                        var AuditSummaryMasterRecords = (from row in entities.SP_PersonResponsible_AuditStatusSummary(userid)
                                                         select row).GroupBy(a => a.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        processids.ForEach(ProcessId =>
                        {
                            var subprocessList = GetSubProcessDetails(ProcessId, customerid);

                            foreach (ProcessSubProcessView cc in subprocessList)
                            {
                                var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                           where row.ProcessId == cc.ProcessID
                                                           && row.SubProcessId == cc.SubProcessID
                                                           && row.RoleID == 4
                                                           && row.PersonResponsible == userid
                                                           && row.CustomerBranchID == custbranchid
                                                           && row.VerticalId == verticalid
                                                           && row.AuditID == auditID
                                                           select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();


                                AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                                FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                                closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                                totalcount = /*NotDonecount + submitedcount + TeamReviewcount +*/ AuditeeReviewcount + FinalReviewcount + closecount;

                                if (AuditSummaryRecords.Count > 0)
                                {
                                    InstanceID = (long)AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                                    ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                                    CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                                    VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                                    formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                                    FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                                }
                                else
                                {
                                    InstanceID = 0;
                                    ScheduleOnID = 0;
                                    CustomerBranchID = 0;
                                    VerticalID = 0;
                                }

                                if (totalcount != 0)
                                {
                                    totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                                    totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                                    totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                                    totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                                    table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID, formonth, FinancialYear);

                                    CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                                    VerticalIDLast = Convert.ToInt32(VerticalID);
                                }
                            }
                        });
                        table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, VerticalIDLast, formonth, FinancialYear);
                    }
                }

                return table;
            }
        }
        public static DataTable GetPersonResponsibleStatusProcessSubProcessUserWiseIMP(int userid, int customerid, int processid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                //long NotDonecount;
                //long submitedcount;
                //long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;
                int VerticalID;

                string financialyear = string.Empty;
                string formonth = string.Empty;
                string branch = string.Empty;
                string process = string.Empty;


                int totalLastcount = 0;
                int totalAuditeeReviewLastcount = 0;
                int totalFinalReviewLastcount = 0;
                int totalcloseLastcount = 0;
                int CustomerBranchIDLast = 0;
                int VerticalIDLast = 0;

                DataTable table = new DataTable();

                table.Columns.Add("Branch", typeof(string));
                table.Columns.Add("FinancialYear", typeof(string));
                table.Columns.Add("ForMonth", typeof(string));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("Total", typeof(long));

                //table.Columns.Add("NotDone", typeof(long));
                //table.Columns.Add("Submited", typeof(long));
                //table.Columns.Add("TeamReview", typeof(long));

                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("VerticalID", typeof(int));

                if (processid == -1)
                {
                    var processids = GetIMPDistinctProcessIDs(Convert.ToInt32(AuditID));

                    if (processids.Count > 0)
                    {
                        processids.ForEach(ProcessId =>
                        {
                            try
                            {
                                var AuditSummaryRecords = (from row in entities.PersonResponsibleImplementationAuditStatusDisplayViews
                                                           where row.CustomerID == customerid
                                                           // && instanceids.Contains((long)row.InternalAuditInstanceID)
                                                           && row.PersonResponsible == userid
                                                           && row.ProcessID == ProcessId
                                                           && row.AuditID == AuditID
                                                           select row).ToList().GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                                AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                                FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                                closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID != 3).Count();

                                totalcount = /*NotDonecount + submitedcount + TeamReviewcount +*/ AuditeeReviewcount + FinalReviewcount + closecount;

                                if (AuditSummaryRecords.Count > 0)
                                {
                                    InstanceID = (long)AuditSummaryRecords.GroupBy(g => g.InternalAuditInstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InternalAuditInstanceID).FirstOrDefault();
                                    ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                                    CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                                    VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalID).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalID).FirstOrDefault();

                                    financialyear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                                    formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                                    branch = AuditSummaryRecords.GroupBy(g => g.Branch).Select(g => g.FirstOrDefault()).Select(entry => entry.Branch).FirstOrDefault();
                                    process = AuditSummaryRecords.GroupBy(g => g.ProcessName).Select(g => g.FirstOrDefault()).Select(entry => entry.ProcessName).FirstOrDefault();
                                }
                                else
                                {
                                    InstanceID = 0;
                                    ScheduleOnID = 0;
                                    CustomerBranchID = 0;
                                    VerticalID = 0;
                                }

                                if (totalcount != 0)
                                {
                                    totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                                    totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                                    totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                                    totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);
                                    table.Rows.Add(branch, financialyear, formonth, process, totalcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID);
                                    CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                                    VerticalIDLast = Convert.ToInt32(VerticalID);
                                }
                            }
                            catch (Exception ex)
                            {

                                throw;
                            }
                        });
                        table.Rows.Add("", financialyear, formonth, "Total", totalLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, VerticalIDLast);
                    }
                }
                else
                {
                    var AuditSummaryRecords = (from row in entities.PersonResponsibleImplementationAuditStatusDisplayViews
                                               where row.CustomerID == customerid
                                               // && instanceids.Contains((long)row.InternalAuditInstanceID)
                                               && row.PersonResponsible == userid
                                               && row.ProcessID == processid
                                               && row.AuditID == AuditID
                                               select row).ToList().GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                    //NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null).Count();

                    //submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                    //TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                    AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                    FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                    closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID != 3).Count();

                    totalcount = /*NotDonecount + submitedcount + TeamReviewcount +*/ AuditeeReviewcount + FinalReviewcount + closecount;

                    if (AuditSummaryRecords.Count > 0)
                    {
                        InstanceID = (long)AuditSummaryRecords.GroupBy(g => g.InternalAuditInstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InternalAuditInstanceID).FirstOrDefault();
                        ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                        CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                        VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalID).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalID).FirstOrDefault();

                        financialyear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                        branch = AuditSummaryRecords.GroupBy(g => g.Branch).Select(g => g.FirstOrDefault()).Select(entry => entry.Branch).FirstOrDefault();
                        process = AuditSummaryRecords.GroupBy(g => g.ProcessName).Select(g => g.FirstOrDefault()).Select(entry => entry.ProcessName).FirstOrDefault();
                    }
                    else
                    {
                        InstanceID = 0;
                        ScheduleOnID = 0;
                        CustomerBranchID = 0;
                        VerticalID = 0;
                    }

                    if (totalcount != 0)
                    {
                        totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                        totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                        totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                        totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);
                        table.Rows.Add(branch, financialyear, formonth, process, totalcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID);
                        CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                        VerticalIDLast = Convert.ToInt32(VerticalID);
                    }
                    table.Rows.Add("", financialyear, formonth, "Total", totalLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, VerticalIDLast);
                }
                return table;
            }
        }

        #endregion
        #region Audit Manager
        public static DataTable GetAuditManagerStatusProcessSubProcessUserWise(int customerid, int ProcessID, int SubProcessID, int custbranchid, int? verticalid, long AuditID, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;
                int VerticalID;
                int totalLastcount = 0;
                int totalNotDoneLastcount = 0;
                int totalsubmitedLastcount = 0;
                int totalTeamReviewLastcount = 0;
                int totalAuditeeReviewLastcount = 0;
                int totalFinalReviewLastcount = 0;
                int totalcloseLastcount = 0;
                int ProcessIDLast = 0;
                int SubProcessIDLast = 0;
                string ProcessNameLast = null;
                string SubProcessNameLast = null;
                int InstanceIDLast = 0;
                int ScheduleOnIDLast = 0;
                int CustomerBranchIDLast = 0;
                int VerticalIDLast = 0;
                string formonth = null;
                string FinancialYear = null;
                DataTable table = new DataTable();

                table.Columns.Add("ProcessId", typeof(int));
                table.Columns.Add("SubProcessId", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("SubProcess", typeof(string));
                table.Columns.Add("Total", typeof(long));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("VerticalID", typeof(int));
                table.Columns.Add("ForMonth", typeof(string));
                table.Columns.Add("FinancialYear", typeof(string));

                if (ProcessID != -1 && SubProcessID != -1)
                {
                    var subprocessList = GetSubProcessDetails(ProcessID, customerid);

                    subprocessList = subprocessList.Where(Entry => Entry.SubProcessID == SubProcessID).ToList();

                    var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                     select row).ToList();
                    foreach (ProcessSubProcessView cc in subprocessList)
                    {
                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == cc.ProcessID
                                                   && row.SubProcessId == cc.SubProcessID
                                                   && row.RoleID == 4 && row.AuditID == AuditID
                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null).Count();

                        submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                        TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                        AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                        FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                        totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                            VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                            formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                            FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                            VerticalID = 0;
                        }

                        if (totalcount != 0)
                        {
                            totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                            totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                            totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                            totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                            totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                            totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                            totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                            table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID, formonth, FinancialYear);
                            ProcessIDLast = Convert.ToInt32(cc.ProcessID);
                            SubProcessIDLast = Convert.ToInt32(cc.SubProcessID);
                            ProcessNameLast = cc.ProcessName.ToString();
                            SubProcessNameLast = cc.SubProcessName.ToString();
                            InstanceIDLast = Convert.ToInt32(InstanceID);
                            ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                            CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                            VerticalIDLast = Convert.ToInt32(VerticalID);
                        }
                    }
                    table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, VerticalIDLast, formonth, FinancialYear);
                }
                else if (ProcessID != -1 && SubProcessID == -1)
                {
                    var subprocessList = GetSubProcessDetails(ProcessID, customerid);

                    var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                     select row).ToList();

                    foreach (ProcessSubProcessView cc in subprocessList)
                    {
                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == cc.ProcessID
                                                   && row.SubProcessId == cc.SubProcessID
                                                   && row.RoleID == 4 && row.AuditID == AuditID
                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null).Count();

                        submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                        TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                        AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                        FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                        totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                            VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                            formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                            FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                            VerticalID = 0;
                        }

                        if (totalcount != 0)
                        {
                            totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                            totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                            totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                            totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                            totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                            totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                            totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                            table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID, formonth, FinancialYear);
                            ProcessIDLast = Convert.ToInt32(cc.ProcessID);
                            SubProcessIDLast = Convert.ToInt32(cc.SubProcessID);
                            ProcessNameLast = cc.ProcessName.ToString();
                            SubProcessNameLast = cc.SubProcessName.ToString();
                            InstanceIDLast = Convert.ToInt32(InstanceID);
                            ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                            CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                            VerticalIDLast = Convert.ToInt32(VerticalID);

                        }
                    }
                    table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, VerticalIDLast, formonth, FinancialYear);
                }
                else
                {
                    var processids = GetDistinctAuditManagerProcessIDs(Convert.ToInt32(AuditID), UserID);
                    if (processids.Count > 0)
                    {
                        var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                         select row).ToList();

                        processids.ForEach(ProcessId =>
                        {
                            var subprocessList = GetSubProcessDetails(ProcessId, customerid);
                            foreach (ProcessSubProcessView cc in subprocessList)
                            {
                                var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                           where row.ProcessId == cc.ProcessID
                                                           && row.SubProcessId == cc.SubProcessID
                                                           && row.RoleID == 4 && row.AuditID == AuditID
                                                           select row).ToList();


                                NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null).Count();

                                submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                                TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                                AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                                FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                                closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                                totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                                if (AuditSummaryRecords.Count > 0)
                                {
                                    InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                                    ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                                    CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                                    VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                                    formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                                    FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                                }
                                else
                                {
                                    InstanceID = 0;
                                    ScheduleOnID = 0;
                                    CustomerBranchID = 0;
                                    VerticalID = 0;
                                }

                                if (totalcount != 0)
                                {
                                    totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                                    totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                                    totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                                    totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                                    totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                                    totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                                    totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                                    table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID, formonth, FinancialYear);
                                    ProcessIDLast = Convert.ToInt32(cc.ProcessID);
                                    SubProcessIDLast = Convert.ToInt32(cc.SubProcessID);
                                    ProcessNameLast = cc.ProcessName.ToString();
                                    SubProcessNameLast = cc.SubProcessName.ToString();
                                    InstanceIDLast = Convert.ToInt32(InstanceID);
                                    ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                                    CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                                    VerticalIDLast = Convert.ToInt32(VerticalID);

                                }
                            }
                        });
                        table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, VerticalIDLast, formonth, FinancialYear);
                    }
                }

                return table;
            }
        }
        public static DataTable GetAuditManagerStatusProcessSubProcessUserWiseIMP(int customerid, int Processid, string ProcessIdName, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int?> OpenImpStatusList = new List<int?>();
                OpenImpStatusList.Add(2);
                OpenImpStatusList.Add(3);
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;
                string financialyear = string.Empty;
                string formonth = string.Empty;
                string branch = string.Empty;
                int VerticalID;
                string SubProcessName = null;
                //~
                int totalLastcount = 0;
                int totalNotDoneLastcount = 0;
                int totalsubmitedLastcount = 0;
                int totalTeamReviewLastcount = 0;
                int totalAuditeeReviewLastcount = 0;
                int totalFinalReviewLastcount = 0;
                int totalcloseLastcount = 0;
                //Defined by sushant
                int ProcessIDLast = 0;
                string ProcessNameLast = null;
                string SubProcessNameLast = null;
                int InstanceIDLast = 0;
                int ScheduleOnIDLast = 0;
                int CustomerBranchIDLast = 0;
                int VerticalIDLast = 0;

                DataTable table = new DataTable();
                //CustomerBranchId,Branch,ForMonth,FinancialYear
                table.Columns.Add("Branch", typeof(string));
                table.Columns.Add("FinancialYear", typeof(string));
                table.Columns.Add("ForMonth", typeof(string));
                table.Columns.Add("Total", typeof(long));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("VerticalID", typeof(int));
                table.Columns.Add("ProcessID", typeof(int));
                table.Columns.Add("ProcessName", typeof(string));
                table.Columns.Add("SubProcess", typeof(string));
                if (Processid == -1)
                {
                    var processList = (from row in entities.SP_ImplementationAuditSummary(customerid)
                                       join row1 in entities.Mst_Process
                                       on row.ProcessID equals row1.Id
                                       where row.RoleID == 4
                                       //&& instanceids.Contains(row.InstanceID)
                                       && row.AuditID == AuditID
                                       select row1).Distinct().ToList();

                    foreach (var prc in processList)
                    {
                        var AuditSummaryRecords = (from row in entities.SP_ImplementationAuditSummary(customerid)
                                                   where row.RoleID == 4
                                                   // && instanceids.Contains(row.InstanceID)
                                                    && row.ProcessID == prc.Id
                                                    && row.AuditID == AuditID
                                                   select row).ToList().GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                        NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).Count();
                        //NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID == 3)).Count();

                        submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                        TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                        AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                        FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).Count();

                        totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                            financialyear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                            formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                            branch = AuditSummaryRecords.GroupBy(g => g.Branch).Select(g => g.FirstOrDefault()).Select(entry => entry.Branch).FirstOrDefault();
                            VerticalID = AuditSummaryRecords.GroupBy(g => g.VerticalID).Select(g => g.FirstOrDefault()).Select(entry => (int)entry.VerticalID).FirstOrDefault();
                            SubProcessName = AuditSummaryRecords.GroupBy(g => g.SubProcessName).Select(g => g.FirstOrDefault()).Select(entry => entry.SubProcessName).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                            VerticalID = 0;
                        }

                        if (totalcount != 0)
                        {
                            table.Rows.Add(branch, financialyear, formonth, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID, prc.Id, prc.Name, SubProcessName);

                            totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                            totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                            totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                            totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                            totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                            totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                            totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                            ProcessIDLast = Convert.ToInt32(prc.Id);
                            ProcessNameLast = prc.Name.ToString();
                            SubProcessNameLast = SubProcessName;
                            //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);    
                            InstanceIDLast = Convert.ToInt32(InstanceID);
                            ScheduleOnIDLast = -1;
                            CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                            // RoleidLast = Convert.ToInt32(Roleid);
                            VerticalIDLast = Convert.ToInt32(VerticalID);
                        }
                    }
                }
                else
                {
                    var AuditSummaryRecords = (from row in entities.SP_ImplementationAuditSummary(customerid)
                                               where row.RoleID == 4
                                                //&& instanceids.Contains(row.InstanceID)
                                                && row.ProcessID == Processid
                                                && row.AuditID == AuditID
                                               select row).ToList().GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                    NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).Count();
                    //NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID == 3)).Count();

                    submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                    TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                    AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                    FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                    closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).Count();
                    //closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID != 3).Count();

                    totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                    if (AuditSummaryRecords.Count > 0)
                    {
                        InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                        ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                        CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                        financialyear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                        branch = AuditSummaryRecords.GroupBy(g => g.Branch).Select(g => g.FirstOrDefault()).Select(entry => entry.Branch).FirstOrDefault();
                        VerticalID = AuditSummaryRecords.GroupBy(g => g.VerticalID).Select(g => g.FirstOrDefault()).Select(entry => (int)entry.VerticalID).FirstOrDefault();

                    }
                    else
                    {
                        InstanceID = 0;
                        ScheduleOnID = 0;
                        CustomerBranchID = 0;
                        VerticalID = 0;
                    }


                    if (totalcount != 0)
                    {
                        table.Rows.Add(branch, financialyear, formonth, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, VerticalID, Processid, ProcessIdName, SubProcessName);
                        totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                        totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                        totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                        totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                        totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                        totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                        totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                        ProcessIDLast = Convert.ToInt32(Processid);
                        ProcessNameLast = ProcessIdName;
                        SubProcessNameLast = SubProcessName;
                        //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);    
                        InstanceIDLast = Convert.ToInt32(InstanceID);
                        // ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                        ScheduleOnIDLast = -1;
                        CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                        // RoleidLast = Convert.ToInt32(Roleid);
                        VerticalIDLast = Convert.ToInt32(VerticalID);
                    }
                }
                table.Rows.Add(CustomerBranchIDLast, financialyear, formonth, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, InstanceIDLast, ScheduleOnIDLast, CustomerBranchIDLast, VerticalIDLast, ProcessIDLast, "Total", "");

                return table;
            }
        }
        public static List<SP_ImplementationAuditSummary_Result> GetAuditManagerDetailIMP(int customerid, int CustBranchid, int VerticalID, string FinYear, string period, int ScheduledOnID, List<int> statusIDs, List<int?> statusNullableIDs, string filter, long AuditID)
        {
            List<int?> OpenImpStatusList = new List<int?>();
            OpenImpStatusList.Add(2);
            OpenImpStatusList.Add(3);

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                if (ScheduledOnID == -1)
                {
                    var AuditSummaryRecords = (from row in entities.SP_ImplementationAuditSummary(customerid)
                                               where row.FinancialYear == FinYear
                                               && row.ForMonth == period
                                               && row.RoleID == 4
                                               && row.AuditID == AuditID
                                               select row).ToList();

                    if (CustBranchid != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                    if (VerticalID != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                    if (filter.Equals("NotDone"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null || (Entry.AuditStatusID == 3 &&  OpenImpStatusList.Contains(Entry.ImplementationAuditStatusID))).ToList();
                       //AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null || (Entry.AuditStatusID == 3 && Entry.ImplementationAuditStatusID == 3)).ToList();

                    else if (filter.Equals("Total"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                    else if (filter.Equals("Submitted"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                    else if (filter.Equals("TeamReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                    else if (filter.Equals("AuditeeReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                    else if (filter.Equals("FinalReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                    else if (filter.Equals("Closed"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3) && (Entry.ImplementationAuditStatusID != 3)).ToList();

                    return AuditSummaryRecords;
                }
                else
                {
                    var AuditSummaryRecords = (from row in entities.SP_ImplementationAuditSummary(customerid)
                                               where row.FinancialYear == FinYear
                                               && row.ForMonth == period
                                               && row.RoleID == 4 && row.ScheduledOnID == ScheduledOnID
                                               && row.AuditID == AuditID
                                               select row).ToList().GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                    if (CustBranchid != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                    if (VerticalID != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                    if (filter.Equals("NotDone"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null || (Entry.AuditStatusID == 3 && OpenImpStatusList.Contains(Entry.ImplementationAuditStatusID))).ToList();
                       //AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null || (Entry.AuditStatusID == 3 && Entry.ImplementationAuditStatusID == 3)).ToList();

                    else if (filter.Equals("Total"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                    else if (filter.Equals("Submitted"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                    else if (filter.Equals("TeamReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                    else if (filter.Equals("AuditeeReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                    else if (filter.Equals("FinalReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                    else if (filter.Equals("Closed"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3) && (Entry.ImplementationAuditStatusID != 3)).ToList();

                    return AuditSummaryRecords;
                }
            }
        }
        public static List<SP_AuditStatusSummary_Result> GetAuditManagerDetailUserWise(int customerid, int CustBranchid, int VerticalID, string FinYear, int ProcessId, int Subprocessid, int InstanceID, List<int> statusIDs, List<int?> statusNullableIDs, string filter, string period, int userid, long AuditID)
        {

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(CustBranchid, VerticalID, AuditID)
                                                 join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                                 on row.ProcessId equals row1.ProcessId
                                                 where row.CustomerBranchID == row1.BranchID && row1.UserID == userid
                                                 && row1.ISACTIVE == true
                                                 select row).ToList();

                if (ProcessId != -1 && ProcessId != 0)
                {
                    if (Subprocessid != -1 && Subprocessid != 0 && InstanceID != 0)
                    {

                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == ProcessId
                                                   && row.SubProcessId == Subprocessid
                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (FinYear != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                        if (filter.Equals("NotDone"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                        else if (filter.Equals("Total"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                        else if (filter.Equals("Submitted"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                        else if (filter.Equals("TeamReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                        else if (filter.Equals("AuditeeReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                        else if (filter.Equals("FinalReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                        else if (filter.Equals("Closed"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                        return AuditSummaryRecords;
                    }
                    else if (Subprocessid != -1 && Subprocessid != 0)
                    {

                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == ProcessId
                                                   && row.SubProcessId == Subprocessid
                                                   && row.UserID == userid
                                                   && row.RoleID == 4
                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();



                        if (FinYear != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                        if (filter.Equals("NotDone"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                        else if (filter.Equals("Total"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                        else if (filter.Equals("Submitted"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                        else if (filter.Equals("TeamReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                        else if (filter.Equals("AuditeeReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                        else if (filter.Equals("FinalReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                        else if (filter.Equals("Closed"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                        return AuditSummaryRecords;
                    }
                    else
                    {

                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.RoleID == 4

                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();


                        if (FinYear != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                        if (filter.Equals("NotDone"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                        else if (filter.Equals("Total"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                        else if (filter.Equals("Submitted"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                        else if (filter.Equals("TeamReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                        else if (filter.Equals("AuditeeReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                        else if (filter.Equals("FinalReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                        else if (filter.Equals("Closed"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                        return AuditSummaryRecords;
                    }

                }
                else
                {
                    var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                               where row.RoleID == 4
                                               select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();


                    if (filter.Equals("NotDone"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                    else if (filter.Equals("Total"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                    else if (filter.Equals("Submitted"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                    else if (filter.Equals("TeamReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                    else if (filter.Equals("AuditeeReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                    else if (filter.Equals("FinalReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                    else if (filter.Equals("Closed"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                    return AuditSummaryRecords;
                }
            }
        }
        #endregion

        #region Performer And Reviewer
        public static DataTable GetAuditStatusProcessSubProcessUserWise(int userid, int Roleid, int custbranchid, int? verticalid, int ProcessID, int SubProcessID, int customerid, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;
                int VerticalID;
                int totalLastcount = 0;
                int totalNotDoneLastcount = 0;
                int totalsubmitedLastcount = 0;
                int totalTeamReviewLastcount = 0;
                int totalAuditeeReviewLastcount = 0;
                int totalFinalReviewLastcount = 0;
                int totalcloseLastcount = 0;
                //string VerticalName;
                int ProcessIDLast = 0;
                int SubProcessIDLast = 0;
                string ProcessNameLast = null;
                string SubProcessNameLast = null;
                int InstanceIDLast = 0;
                int ScheduleOnIDLast = 0;
                int CustomerBranchIDLast = 0;
                int RoleidLast = 0;
                int VerticalIDLast = 0;
                string formonth = null;
                string FinancialYear = null;
                //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);

                DataTable table = new DataTable();

                table.Columns.Add("ProcessId", typeof(int));
                table.Columns.Add("SubProcessId", typeof(int));
                table.Columns.Add("Process", typeof(string));
                table.Columns.Add("SubProcess", typeof(string));
                table.Columns.Add("Total", typeof(long));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("VerticalID", typeof(int));
                table.Columns.Add("ForMonth", typeof(string));
                table.Columns.Add("FinancialYear", typeof(string));


                if (ProcessID != -1 && SubProcessID != -1)
                {
                    var subprocessList = GetSubProcessDetails(ProcessID, customerid);

                    subprocessList = subprocessList.Where(Entry => Entry.SubProcessID == SubProcessID).ToList();

                    //var AuditSummaryMasterRecords = (from row in entities.AuditSummaryCountViews
                    //                                 select row).ToList();

                    var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                     select row).ToList();

                    foreach (ProcessSubProcessView cc in subprocessList)
                    {
                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == cc.ProcessID
                                                   && row.SubProcessId == cc.SubProcessID
                                                   && row.UserID == userid
                                                   && row.RoleID == Roleid
                                                   //&& instanceids.Contains(row.InstanceID)
                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null).Count();

                        submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                        TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                        AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                        FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                        totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                            VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                            formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                            FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                            VerticalID = 0;
                        }

                        if (totalcount != 0)
                        {
                            totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                            totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                            totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                            totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                            totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                            totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                            totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                            table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID, formonth, FinancialYear);


                            ProcessIDLast = Convert.ToInt32(cc.ProcessID);
                            SubProcessIDLast = Convert.ToInt32(cc.SubProcessID);
                            ProcessNameLast = cc.ProcessName.ToString();
                            SubProcessNameLast = cc.SubProcessName.ToString();
                            //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);    
                            InstanceIDLast = Convert.ToInt32(InstanceID);
                            ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                            CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                            RoleidLast = Convert.ToInt32(Roleid);
                            VerticalIDLast = Convert.ToInt32(VerticalID);
                        }
                    }
                    table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, RoleidLast, VerticalIDLast, formonth, FinancialYear);
                }

                else if (ProcessID != -1 && SubProcessID == -1)
                {
                    var subprocessList = GetSubProcessDetails(ProcessID, customerid);

                    //var AuditSummaryMasterRecords = (from row in entities.AuditSummaryCountViews
                    //                                 select row).ToList();

                    var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                     select row).ToList();

                    foreach (ProcessSubProcessView cc in subprocessList)
                    {
                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == cc.ProcessID
                                                   && row.SubProcessId == cc.SubProcessID
                                                   && row.UserID == userid
                                                   && row.RoleID == Roleid
                                                   //&& instanceids.Contains(row.InstanceID)
                                                   select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null).Count();

                        submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                        TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                        AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                        FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                        totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                            VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                            formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                            FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                            VerticalID = 0;
                        }

                        if (totalcount != 0)
                        {
                            totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                            totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                            totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                            totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                            totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                            totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                            totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                            table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID, formonth, FinancialYear);

                            ProcessIDLast = Convert.ToInt32(cc.ProcessID);
                            SubProcessIDLast = Convert.ToInt32(cc.SubProcessID);
                            ProcessNameLast = cc.ProcessName.ToString();
                            SubProcessNameLast = cc.SubProcessName.ToString();
                            //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);    
                            InstanceIDLast = Convert.ToInt32(InstanceID);
                            ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                            CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                            RoleidLast = Convert.ToInt32(Roleid);
                            VerticalIDLast = Convert.ToInt32(VerticalID);
                        }
                    }

                    table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, RoleidLast, VerticalIDLast, formonth, FinancialYear);
                }
                else
                {
                    var processids = GetDistinctAssignedProcessIDs(Convert.ToInt32(AuditID), userid);
                    int i = 0;
                    if (processids.Count > 0)
                    {

                        var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(custbranchid, verticalid, AuditID)
                                                         select row).ToList();
                        processids.ForEach(ProcessId =>
                        {
                            var subprocessList = GetAssignedSubProcessDetails(ProcessId, customerid, userid, AuditID);

                            foreach (Sp_GetAssignedSubprocessList_Result cc in subprocessList)
                            {
                                var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                           where row.ProcessId == cc.ProcessID
                                                        && row.SubProcessId == cc.SubProcessID
                                                        && row.UserID == userid
                                                        && row.RoleID == Roleid
                                                           //&& instanceids.Contains(row.InstanceID)
                                                           select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                                NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null).Count();

                                submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                                TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                                AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                                FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                                closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3).Count();

                                totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                                if (AuditSummaryRecords.Count > 0)
                                {
                                    InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                                    ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                                    CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                                    VerticalID = (int)AuditSummaryRecords.GroupBy(g => g.VerticalId).Select(g => g.FirstOrDefault()).Select(entry => entry.VerticalId).FirstOrDefault();
                                    formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                                    FinancialYear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                                }
                                else
                                {
                                    InstanceID = 0;
                                    ScheduleOnID = 0;
                                    CustomerBranchID = 0;
                                    VerticalID = 0;
                                }

                                if (totalcount != 0)
                                {
                                    i = i + 1;
                                    if (i > 35)
                                    {
                                        string chks = "asd";
                                    }
                                    if (i > 70)
                                    {
                                        string chk = "asd";
                                    }

                                    totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                                    totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                                    totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                                    totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                                    totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                                    totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                                    totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);


                                    //cc.ForMonth, cc.FinancialYear);

                                    table.Rows.Add(cc.ProcessID, cc.SubProcessID, cc.ProcessName, cc.SubProcessName, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID, formonth, FinancialYear);

                                    ProcessIDLast = Convert.ToInt32(cc.ProcessID);
                                    SubProcessIDLast = Convert.ToInt32(cc.SubProcessID);
                                    ProcessNameLast = cc.ProcessName.ToString();
                                    SubProcessNameLast = cc.SubProcessName.ToString();
                                    //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);    
                                    InstanceIDLast = Convert.ToInt32(InstanceID);
                                    ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                                    CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                                    RoleidLast = Convert.ToInt32(Roleid);
                                    VerticalIDLast = Convert.ToInt32(VerticalID);
                                }
                            }
                        });
                        //table.Rows.Add(0, 0, null, null, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, 0, 0, 0); 
                        table.Rows.Add(-1, -1, "Total", null, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, 0, 0, CustomerBranchIDLast, RoleidLast, VerticalIDLast, formonth, FinancialYear);
                    }
                }

                return table;
            }
        }
        #endregion

        public static List<ProcessSubProcessView> GetSubProcessDetails(long ProcessId, int customerid)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var SubProcessList = (from row in entities.ProcessSubProcessViews
                                      where row.ProcessID == ProcessId && row.CustomerID == customerid
                                      select row);

                SubProcessList = SubProcessList.OrderBy(entry => entry.SubProcessName);

                return SubProcessList.ToList();
            }
        }

        public static List<Sp_GetAssignedSubprocessList_Result> GetAssignedSubProcessDetails(long ProcessId, int customerid, int UserID, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.Sp_GetAssignedSubprocessList(Convert.ToInt32(AuditID), UserID, (int)ProcessId)
                                   select row).Distinct().ToList();

                return ProcessList.ToList();
            }
        }

        public static List<long> GetDistinctAuditManagerProcessIDs(int AuditID, int UserID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.InternalControlAuditAssignments
                                   join row1 in entities.EntitiesAssignmentAuditManagerRisks
                                   on row.ProcessId equals row1.ProcessId
                                   where row.CustomerBranchID == row1.BranchID
                                   && row.AuditID == AuditID && row1.UserID == UserID
                                  && row1.ISACTIVE == true
                                   select row.ProcessId).Distinct().ToList();

                return ProcessList.ToList();
            }
        }
        public static List<long> GetDistinctProcessIDs(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.InternalControlAuditAssignments
                                   where row.AuditID == AuditID
                                   select row.ProcessId).Distinct().ToList();

                return ProcessList.ToList();
            }
        }

        public static List<long> GetDistinctAssignedProcessIDs(int AuditID, int UserId)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var ProcessList = (from row in entities.InternalControlAuditAssignments
                                   where row.AuditID == AuditID && row.UserID == UserId
                                   select row.ProcessId).Distinct().ToList();

                return ProcessList.ToList();
            }
        }
        public static List<SP_AuditStatusSummary_Result> GetAuditDetailUserWise(int customerid, int CustBranchid, string FinYear, int ProcessId, int Subprocessid, int userid, int roleid, int Verticalid, int InstanceID, List<int> statusIDs, List<int?> statusNullableIDs, string filter, string period, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var AuditSummaryMasterRecords = (from row in entities.SP_AuditStatusSummary(CustBranchid, Verticalid, AuditID)
                                                 select row).ToList();

                if (ProcessId != -1 && ProcessId != 0)
                {
                    if (Subprocessid != -1 && Subprocessid != 0 && InstanceID != 0)
                    {

                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == ProcessId
                                                   && row.SubProcessId == Subprocessid
                                                   && row.UserID == userid
                                                   && row.RoleID == roleid
                                                   //&& instanceids.Contains(row.InstanceID)
                                                   select row).ToList().
                                                   GroupBy(entity => new { entity.ATBDID, entity.AuditID }).Select(entity => entity.FirstOrDefault()).ToList();


                        //var AuditSummaryRecords = (from row in entities.AuditSummaryCountViews
                        //                           where row.ProcessId == ProcessId
                        //                           && row.SubProcessId == Subprocessid
                        //                           && row.UserID == userid
                        //                           && row.RoleID == roleid
                        //                           && row.CustomerBranchID == CustBranchid
                        //                           && row.VerticalId == Verticalid
                        //                           && row.InstanceID == InstanceID
                        //                           select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (FinYear != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                        if (period != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.ForMonth == period).ToList();

                        if (filter.Equals("NotDone"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                        else if (filter.Equals("Total"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                        else if (filter.Equals("Submitted"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                        else if (filter.Equals("TeamReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                        else if (filter.Equals("AuditeeReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                        else if (filter.Equals("FinalReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                        else if (filter.Equals("Closed"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                        return AuditSummaryRecords;

                    }
                    else if (Subprocessid != -1 && Subprocessid != 0)
                    {

                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == ProcessId
                                                   && row.SubProcessId == Subprocessid
                                                   && row.UserID == userid
                                                   && row.RoleID == roleid
                                                   //&& instanceids.Contains(row.InstanceID)
                                                   select row).ToList().
                                                   GroupBy(entity => new { entity.ATBDID, entity.AuditID }).Select(entity => entity.FirstOrDefault()).ToList();



                        //var AuditSummaryRecords = (from row in entities.AuditSummaryCountViews
                        //                           where row.ProcessId == ProcessId
                        //                           && row.SubProcessId == Subprocessid
                        //                           && row.UserID == userid
                        //                           && row.RoleID == roleid
                        //                           && row.CustomerBranchID == CustBranchid
                        //                           && row.VerticalId == Verticalid
                        //                           select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (FinYear != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                        if (period != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.ForMonth == period).ToList();

                        if (filter.Equals("NotDone"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                        else if (filter.Equals("Total"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                        else if (filter.Equals("Submitted"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                        else if (filter.Equals("TeamReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                        else if (filter.Equals("AuditeeReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                        else if (filter.Equals("FinalReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                        else if (filter.Equals("Closed"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                        return AuditSummaryRecords;
                    }
                    else
                    {
                        var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                                   where row.ProcessId == ProcessId
                                                   && row.UserID == userid
                                                   && row.RoleID == roleid
                                                   //&& instanceids.Contains(row.InstanceID)
                                                   select row).ToList().
                                                   GroupBy(entity => new { entity.ATBDID, entity.AuditID }).Select(entity => entity.FirstOrDefault()).ToList();



                        //var AuditSummaryRecords = (from row in entities.AuditSummaryCountViews
                        //                           where row.ProcessId == ProcessId
                        //                           && row.UserID == userid
                        //                           && row.RoleID == roleid
                        //                           && row.CustomerBranchID == CustBranchid
                        //                           && row.VerticalId == Verticalid
                        //                           select row).ToList().GroupBy(entity => entity.ATBDID).Select(entity => entity.FirstOrDefault()).ToList();

                        if (FinYear != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.FinancialYear == FinYear).ToList();

                        if (period != "")
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.ForMonth == period).ToList();

                        if (filter.Equals("NotDone"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                        else if (filter.Equals("Total"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                        else if (filter.Equals("Submitted"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                        else if (filter.Equals("TeamReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                        else if (filter.Equals("AuditeeReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                        else if (filter.Equals("FinalReview"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                        else if (filter.Equals("Closed"))
                            AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                        return AuditSummaryRecords;
                    }

                }
                else
                {

                    var AuditSummaryRecords = (from row in AuditSummaryMasterRecords
                                               where row.UserID == userid
                                               && row.RoleID == roleid
                                               //&& instanceids.Contains(row.InstanceID)
                                               select row).ToList().
                                               GroupBy(entity => new { entity.ATBDID, entity.AuditID }).Select(entity => entity.FirstOrDefault()).ToList();



                    //var AuditSummaryRecords = (from row in entities.AuditSummaryCountViews
                    //                           where row.UserID == userid
                    //                           && row.RoleID == roleid
                    //                           && row.CustomerBranchID == CustBranchid
                    //                           && row.VerticalId == Verticalid
                    //                           && row.FinancialYear == FinYear
                    //                           && row.ForMonth == period
                    //                           select row).ToList().GroupBy(entity => new {
                    //                               entity.InstanceID,
                    //                               entity.ProcessId,
                    //                               entity.ATBDID
                    //                           }).Select(entity => entity.FirstOrDefault()).ToList();

                    if (filter.Equals("NotDone"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null).ToList();

                    else if (filter.Equals("Total"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                    else if (filter.Equals("Submitted"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                    else if (filter.Equals("TeamReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                    else if (filter.Equals("AuditeeReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                    else if (filter.Equals("FinalReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                    else if (filter.Equals("Closed"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3)).ToList();

                    return AuditSummaryRecords;
                }

            }
        }

        public static List<Mst_ObservationCategory> GetObservationCategoryAll(int ProcessId, string filter = null)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var complianceCategorys = (from MOC in entities.Mst_ObservationCategory
                                           join IAITV in entities.InternalAuditInstanceTransactionViews
                                           on MOC.ID equals IAITV.ObservationCategory
                                           where IAITV.ProcessId == ProcessId && MOC.IsActive == false
                                           select MOC).ToList().Distinct();



                complianceCategorys = complianceCategorys.OrderBy(entry => entry.Name);

                return complianceCategorys.OrderBy(entry => entry.Name).ToList();
            }

        }
        //30 Jan
        public static DataTable GetAuditStatusProcessSubProcessUserWiseIMP(int userid, int Roleid, int customerid, int Processid, string ProcessIdName, long auditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int?> OpenImpStatusList = new List<int?>();
                OpenImpStatusList.Add(2);
                OpenImpStatusList.Add(3);
                long NotDonecount;
                long submitedcount;
                long TeamReviewcount;
                long AuditeeReviewcount;
                long FinalReviewcount;
                long closecount;
                long totalcount;
                long InstanceID;
                long ScheduleOnID;
                long CustomerBranchID;
                string financialyear = string.Empty;
                string formonth = string.Empty;
                string branch = string.Empty;
                int VerticalID;
                string ProcessName = string.Empty;
                int totalLastcount = 0;
                int totalNotDoneLastcount = 0;
                int totalsubmitedLastcount = 0;
                int totalTeamReviewLastcount = 0;
                int totalAuditeeReviewLastcount = 0;
                int totalFinalReviewLastcount = 0;
                int totalcloseLastcount = 0;
                //Defined by sushant
                int ProcessIDLast = 0;
                string ProcessNameLast = null;
                int InstanceIDLast = 0;
                int ScheduleOnIDLast = 0;
                int CustomerBranchIDLast = 0;
                int RoleidLast = 0;
                int VerticalIDLast = 0;

                DataTable table = new DataTable();
                table.Columns.Add("Branch", typeof(string));
                table.Columns.Add("FinancialYear", typeof(string));
                table.Columns.Add("ForMonth", typeof(string));
                table.Columns.Add("Total", typeof(long));
                table.Columns.Add("NotDone", typeof(long));
                table.Columns.Add("Submited", typeof(long));
                table.Columns.Add("TeamReview", typeof(long));
                table.Columns.Add("AuditeeReview", typeof(long));
                table.Columns.Add("FinalReview", typeof(long));
                table.Columns.Add("Closed", typeof(long));
                table.Columns.Add("InstanceID", typeof(long));
                table.Columns.Add("ScheduleOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(long));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("VerticalID", typeof(int));
                table.Columns.Add("ProcessID", typeof(int));
                table.Columns.Add("ProcessName", typeof(string));
                if (Processid == -1)
                {
                    var processList = (from row in entities.SP_ImplementationAuditSummary(customerid)
                                       join row1 in entities.Mst_Process
                                       on row.ProcessID equals row1.Id
                                       where row.UserID == userid
                                       && row.RoleID == Roleid
                                       //&& instanceids.Contains(row.InstanceID)
                                       && row.AuditID == auditID
                                       select row1).Distinct().ToList();

                    foreach (var prc in processList)
                    {
                        var AuditSummaryRecords = (from row in entities.SP_ImplementationAuditSummary(customerid)
                                                   where
                                                    //&& row.UserID == userid
                                                    row.RoleID == Roleid
                                                   // && instanceids.Contains(row.InstanceID)
                                                    && row.ProcessID == prc.Id
                                                    && row.AuditID == auditID
                                                   select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                        NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).Count();
                        //NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID == 3)).Count();

                        submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                        TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                        AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                        FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                        closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).Count();
                        //closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID != 3).Count();

                        totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                        if (AuditSummaryRecords.Count > 0)
                        {
                            InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                            ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                            CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                            financialyear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                            formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                            branch = AuditSummaryRecords.GroupBy(g => g.Branch).Select(g => g.FirstOrDefault()).Select(entry => entry.Branch).FirstOrDefault();
                            VerticalID = AuditSummaryRecords.GroupBy(g => g.VerticalID).Select(g => g.FirstOrDefault()).Select(entry => (int)entry.VerticalID).FirstOrDefault();
                            //ProcessName = AuditSummaryRecords.GroupBy(g => g.ProcessName).Select(g => g.FirstOrDefault()).Select(entry => entry.ProcessName).FirstOrDefault();

                        }
                        else
                        {
                            InstanceID = 0;
                            ScheduleOnID = 0;
                            CustomerBranchID = 0;
                            VerticalID = 0;
                        }

                        if (totalcount != 0)
                        {
                            table.Rows.Add(branch, financialyear, formonth, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID, prc.Id, prc.Name);

                            totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                            totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                            totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                            totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                            totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                            totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                            totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                            ProcessIDLast = Convert.ToInt32(prc.Id);
                            ProcessNameLast = prc.Name.ToString();
                            //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);    
                            InstanceIDLast = Convert.ToInt32(InstanceID);
                            ScheduleOnIDLast = -1;
                            CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                            RoleidLast = Convert.ToInt32(Roleid);
                            VerticalIDLast = Convert.ToInt32(VerticalID);
                        }
                    }
                }
                else
                {
                    var AuditSummaryRecords = (from row in entities.SP_ImplementationAuditSummary(customerid)
                                               where row.UserID == userid
                                                && row.RoleID == Roleid
                                                //  && instanceids.Contains(row.InstanceID)
                                                && row.ProcessID == Processid
                                               select row).GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                    NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).Count();
                    //NotDonecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID == 3)).Count();

                    submitedcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 2).Count();

                    TeamReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 4).Count();

                    AuditeeReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 6).Count();

                    FinalReviewcount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 5).Count();

                    closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).Count();
                    //closecount = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && entity.ImplementationAuditStatusID != 3).Count();

                    totalcount = NotDonecount + submitedcount + TeamReviewcount + AuditeeReviewcount + FinalReviewcount + closecount;

                    if (AuditSummaryRecords.Count > 0)
                    {
                        InstanceID = AuditSummaryRecords.GroupBy(g => g.InstanceID).Select(g => g.FirstOrDefault()).Select(entry => entry.InstanceID).FirstOrDefault();
                        ScheduleOnID = (long)AuditSummaryRecords.GroupBy(g => g.ScheduledOnID).Select(g => g.FirstOrDefault()).Select(entry => entry.ScheduledOnID).FirstOrDefault();
                        CustomerBranchID = (long)AuditSummaryRecords.GroupBy(g => g.CustomerBranchID).Select(g => g.FirstOrDefault()).Select(entry => entry.CustomerBranchID).FirstOrDefault();
                        financialyear = AuditSummaryRecords.GroupBy(g => g.FinancialYear).Select(g => g.FirstOrDefault()).Select(entry => entry.FinancialYear).FirstOrDefault();
                        formonth = AuditSummaryRecords.GroupBy(g => g.ForMonth).Select(g => g.FirstOrDefault()).Select(entry => entry.ForMonth).FirstOrDefault();
                        branch = AuditSummaryRecords.GroupBy(g => g.Branch).Select(g => g.FirstOrDefault()).Select(entry => entry.Branch).FirstOrDefault();
                        VerticalID = AuditSummaryRecords.GroupBy(g => g.VerticalID).Select(g => g.FirstOrDefault()).Select(entry => (int)entry.VerticalID).FirstOrDefault();
                        //ProcessName = AuditSummaryRecords.GroupBy(g => g.ProcessName).Select(g => g.FirstOrDefault()).Select(entry => entry.ProcessName).FirstOrDefault();

                    }
                    else
                    {
                        InstanceID = 0;
                        ScheduleOnID = 0;
                        CustomerBranchID = 0;
                        VerticalID = 0;
                    }

                    if (totalcount != 0)
                    {
                        table.Rows.Add(branch, financialyear, formonth, totalcount, NotDonecount, submitedcount, TeamReviewcount, AuditeeReviewcount, FinalReviewcount, closecount, InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID, Processid, ProcessIdName);
                        totalLastcount = totalLastcount + Convert.ToInt32(totalcount);
                        totalNotDoneLastcount = totalNotDoneLastcount + Convert.ToInt32(NotDonecount);
                        totalsubmitedLastcount = totalsubmitedLastcount + Convert.ToInt32(submitedcount);

                        totalTeamReviewLastcount = totalTeamReviewLastcount + Convert.ToInt32(TeamReviewcount);
                        totalAuditeeReviewLastcount = totalAuditeeReviewLastcount + Convert.ToInt32(AuditeeReviewcount);
                        totalFinalReviewLastcount = totalFinalReviewLastcount + Convert.ToInt32(FinalReviewcount);
                        totalcloseLastcount = totalcloseLastcount + Convert.ToInt32(closecount);

                        ProcessIDLast = Convert.ToInt32(Processid);
                        ProcessNameLast = ProcessIdName;
                        //InstanceID, ScheduleOnID, CustomerBranchID, Roleid, VerticalID);    
                        InstanceIDLast = Convert.ToInt32(InstanceID);
                        // ScheduleOnIDLast = Convert.ToInt32(ScheduleOnID);
                        ScheduleOnIDLast = -1;
                        CustomerBranchIDLast = Convert.ToInt32(CustomerBranchID);
                        RoleidLast = Convert.ToInt32(Roleid);
                        VerticalIDLast = Convert.ToInt32(VerticalID);
                    }
                }
                table.Rows.Add(CustomerBranchIDLast, financialyear, formonth, totalLastcount, totalNotDoneLastcount, totalsubmitedLastcount, totalTeamReviewLastcount, totalAuditeeReviewLastcount, totalFinalReviewLastcount, totalcloseLastcount, InstanceIDLast, ScheduleOnIDLast, CustomerBranchIDLast, RoleidLast, VerticalIDLast, ProcessIDLast, "Total");

                return table;
            }
        }
        public static List<Sp_ImplementationAuditSummaryCountView_Result> GetAuditDetailUserWiseIMP(int customerid, int CustBranchid, int VerticalID, string FinYear, string period, int userid, List<int> roleid, int Scheduleonid, List<int> statusIDs, List<int?> statusNullableIDs, string filter, long AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<int?> OpenImpStatusList = new List<int?>();
                OpenImpStatusList.Add(2);
                OpenImpStatusList.Add(3);
                if (Scheduleonid == -1)
                {
                    var AuditSummaryRecords = (from row in entities.Sp_ImplementationAuditSummaryCountView(customerid)
                                               where roleid.Contains(row.RoleID)
                                               && row.FinancialYear == FinYear
                                               && row.ForMonth == period
                                               && row.AuditID == AuditID
                                               && row.UserID == userid
                                               select row).Distinct().ToList();

                    if (CustBranchid != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                    if (VerticalID != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                    if (filter.Equals("NotDone"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).ToList();

                    else if (filter.Equals("Total"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                    else if (filter.Equals("AuditeeReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && Entry.AuditeeResponse == "PA").ToList();

                    else if (filter.Equals("AuditeeSubmit"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && (Entry.AuditeeResponse == "AS" || Entry.AuditeeResponse == "R2")).ToList();

                    else if (filter.Equals("ReviewComment"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6) && Entry.AuditeeResponse == "AR").ToList();

                    else if (filter.Equals("Submitted"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2) && (Entry.AuditeeResponse == "PS" || Entry.AuditeeResponse == "R3" || Entry.AuditeeResponse == "2R")).ToList();

                    else if (filter.Equals("TeamReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                    else if (filter.Equals("FinalReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                    else if (filter.Equals("Closed"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).ToList();

                    return AuditSummaryRecords;
                }
                else
                {
                    var AuditSummaryRecords = (from row in entities.Sp_ImplementationAuditSummaryCountView(CustBranchid)
                                               where roleid.Contains(row.RoleID)
                                               && row.FinancialYear == FinYear
                                               && row.ForMonth == period && row.ScheduledOnID == Scheduleonid
                                               && row.AuditID == AuditID
                                               && row.UserID == userid
                                               select row).ToList().GroupBy(entity => entity.ResultID).Select(entity => entity.FirstOrDefault()).ToList();

                    if (CustBranchid != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.CustomerBranchID == CustBranchid).ToList();

                    if (VerticalID != -1)
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                    if (filter.Equals("NotDone"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(entity => entity.AuditStatusID == null || (entity.AuditStatusID == 3 && OpenImpStatusList.Contains(entity.ImplementationAuditStatusID))).ToList();
                    // AuditSummaryRecords = AuditSummaryRecords.Where(Entry => Entry.AuditStatusID == null || (Entry.AuditStatusID == 3 && Entry.ImplementationAuditStatusID == 3)).ToList();

                    else if (filter.Equals("Total"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == null || statusIDs.Contains((int)Entry.AuditStatusID))).ToList();

                    else if (filter.Equals("Submitted"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 2)).ToList();

                    else if (filter.Equals("TeamReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 4)).ToList();

                    else if (filter.Equals("AuditeeReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 6)).ToList();

                    else if (filter.Equals("FinalReview"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 5)).ToList();

                    else if (filter.Equals("Closed"))
                        AuditSummaryRecords = AuditSummaryRecords.Where(entity => entity.AuditStatusID == 3 && !OpenImpStatusList.Contains(entity.ImplementationAuditStatusID)).ToList();
                    //AuditSummaryRecords = AuditSummaryRecords.Where(Entry => (Entry.AuditStatusID == 3) && (Entry.ImplementationAuditStatusID != 3)).ToList();

                    return AuditSummaryRecords;
                }
            }
        }
    }
}
