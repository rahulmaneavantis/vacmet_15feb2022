﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Net.Mail;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Business
{
    public class ConfigurationManagement
    {
        public static void CreateUserReminder(ReminderTemplate_UserMapping obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ReminderTemplate_UserMapping.Add(obj);

                entities.SaveChanges();
                #region update Repeat value to all also

                List<ReminderTemplate_UserMapping> query = (from row in entities.ReminderTemplate_UserMapping
                                                            where row.Frequency == obj.Frequency
                                                            && row.CustomerID == obj.CustomerID
                                                            && row.IsActive == true
                                                             && row.Flag == obj.Flag
                                                             && row.Type == obj.Type
                                                            select row).ToList();
                if (query.Count > 0)
                {
                    var chkRepeatValue = query.Where(x => x.RepeatEveryDays != null && x.RepeatEveryDays > 0).OrderByDescending(x => x.CreatedOn).Select(x => x.RepeatEveryDays).FirstOrDefault();
                    if (chkRepeatValue > 0)
                    {
                        foreach (var item in query)
                        {
                            ReminderTemplate_UserMapping query1 = (from row in entities.ReminderTemplate_UserMapping
                                                                   where row.ID == item.ID
                                                                   select row).FirstOrDefault();

                            if (query1 != null)
                            {
                                query1.RepeatEveryDays = chkRepeatValue;
                                entities.SaveChanges();
                            }
                        }
                    }
                }
                #endregion
            }
        }
        //public static void CreateUserReminder(ReminderTemplate_UserMapping obj)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        entities.ReminderTemplate_UserMapping.Add(obj);

        //        entities.SaveChanges();
        //    }
        //}
        public static void DeleteFreqDays(int ID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ReminderTemplate_CustomerMapping obj = (from row in entities.ReminderTemplate_CustomerMapping
                                                        where row.ID == ID
                                                        select row).FirstOrDefault();
                if (obj != null)
                {
                    obj.IsActive = false;
                    obj.UpdatedBy = UserID;
                    obj.UpdateOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
        }
        public static List<SP_ReminderTemplate_UserMapping_Result> GetAllUserReminderTemplate(int Customer, int UserID, string ComplianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Detail = (from row in entities.SP_ReminderTemplate_UserMapping(Customer, UserID, ComplianceType)
                              where row.IsActive == true
                              select row).ToList();

                return Detail.ToList();
            }
        }
        public static void DeleteUserFreqDays(int ID, int UserID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ReminderTemplate_UserMapping obj = (from row in entities.ReminderTemplate_UserMapping
                                                    where row.ID == ID
                                                    select row).FirstOrDefault();
                if (obj != null)
                {
                    obj.IsActive = false;
                    obj.UpdatedBy = UserID;
                    obj.UpdateOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
        }
        public static bool ReminderTemplate_UserMappingExist(ReminderTemplate_UserMapping obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ReminderTemplate_UserMapping
                             where row.Frequency == obj.Frequency
                             && row.CustomerID == obj.CustomerID
                               && row.UserID == obj.UserID
                             && row.IsActive == true
                             && row.TimeInDays == obj.TimeInDays
                              && row.Flag == obj.Flag
                              && row.Type == obj.Type
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        public static List<SP_ReminderTemplate_CustomerMapping_Result> GetAllReminderTemplate(int Customer, string ComplianceType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Detail = (from row in entities.SP_ReminderTemplate_CustomerMapping(Customer, ComplianceType)
                              where row.IsActive == true
                              select row).ToList();

                return Detail.ToList();
            }
        }
        public static List<ReminderTemplate_CustomerMapping> GetAllReminderCustomerTemplates()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                return entities.ReminderTemplate_CustomerMapping.ToList();
            }
        }
        public static bool ReminderTemplate_CustomerMappingExist(ReminderTemplate_CustomerMapping obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ReminderTemplate_CustomerMapping
                             where row.Frequency == obj.Frequency
                             && row.CustomerID == obj.CustomerID
                             && row.IsActive == true
                             && row.TimeInDays == obj.TimeInDays
                              && row.Flag == obj.Flag
                              && row.Type == obj.Type
                             select row);

                return query.Select(entry => true).FirstOrDefault();
            }
        }
        //public static void CreateReminder(ReminderTemplate_CustomerMapping obj)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        entities.ReminderTemplate_CustomerMapping.Add(obj);

        //        entities.SaveChanges();
        //    }
        //}

        public static void CreateReminder(ReminderTemplate_CustomerMapping obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ReminderTemplate_CustomerMapping.Add(obj);

                entities.SaveChanges();

                #region update Repeat value to all also

                List<ReminderTemplate_CustomerMapping> query = (from row in entities.ReminderTemplate_CustomerMapping
                                                                where row.Frequency == obj.Frequency
                                                                && row.CustomerID == obj.CustomerID
                                                                && row.IsActive == true
                                                                 && row.Flag == obj.Flag
                                                                 && row.Type == obj.Type
                                                                select row).ToList();
                if (query.Count > 0)
                {
                    var chkRepeatValue = query.Where(x => x.RepeatEveryDays != null && x.RepeatEveryDays > 0).OrderByDescending(x => x.CreatedOn).Select(x => x.RepeatEveryDays).FirstOrDefault();
                    if (chkRepeatValue > 0)
                    {
                        foreach (var item in query)
                        {
                            ReminderTemplate_CustomerMapping query1 = (from row in entities.ReminderTemplate_CustomerMapping
                                                                       where row.ID == item.ID
                                                                       select row).FirstOrDefault();

                            if (query1 != null)
                            {
                                query1.RepeatEveryDays = chkRepeatValue;
                                entities.SaveChanges();
                            }
                        }
                    }
                }
                #endregion
            }
        }
        public static void UpdateReminder(ReminderTemplate_CustomerMapping obj)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ReminderTemplate_CustomerMapping FUpdate = (from row in entities.ReminderTemplate_CustomerMapping
                                                            where row.Frequency == obj.Frequency
                                                            && row.CustomerID == obj.CustomerID
                                                            && row.IsActive == true
                                                            select row).FirstOrDefault();

                FUpdate.TimeInDays = obj.TimeInDays;
                FUpdate.RepeatEveryDays = obj.RepeatEveryDays;

                entities.SaveChanges();
            }
        }
        public static List<FinancialYear> GetAll(string filter)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var financialYears = (from row in entities.FinancialYears
                                      //where row.IsDeleted == false
                                      select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    financialYears = financialYears.Where(entry => entry.Name.Contains(filter));
                }

                return financialYears.ToList();
            }
        }

        public static FinancialYear GetByID(int financialYearID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var financialYear = (from row in entities.FinancialYears
                                     where row.ID == financialYearID
                                     select row).SingleOrDefault();

                return financialYear;
            }
        }

        public static void Delete(int financialYearID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //FinancialYear financialYearToDelete = new FinancialYear() { ID = financialYearID };
                //entities.FinancialYears.Attach(financialYearToDelete);

                FinancialYear financialYearToDelete = (from row in entities.FinancialYears
                                                       where row.ID == financialYearID
                                                       select row).FirstOrDefault();

                entities.FinancialYears.Remove(financialYearToDelete);

                entities.SaveChanges();
            }
        }

        public static bool Exists(FinancialYear financialYear)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.FinancialYears
                             where row.Name.Equals(financialYear.Name)
                             select row);

                if (financialYear.ID > 0)
                {
                    query = query.Where(entry => entry.ID != financialYear.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void Update(FinancialYear financialYear)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //FinancialYear FinancialYearToUpdate = new FinancialYear() { ID = financialYear.ID };
                //entities.FinancialYears.Attach(FinancialYearToUpdate);
                FinancialYear FinancialYearToUpdate = (from row in entities.FinancialYears
                                                       where row.ID == financialYear.ID
                                                       select row).FirstOrDefault();

                FinancialYearToUpdate.Name = financialYear.Name;

                entities.SaveChanges();
            }
        }

        public static void Create(FinancialYear financialYear)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.FinancialYears.Add(financialYear);

                entities.SaveChanges();
            }
        }

        #region Reminder Templates
        public static List<ReminderTemplate> GetAllReminderTemplates()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                return entities.ReminderTemplates.ToList();
            }
        }

        public static void UpdateReminderTemplates(List<ReminderTemplate> templates)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                templates.ForEach(entry => {
                    //ReminderTemplate templateToUpdate = new ReminderTemplate() { ID = entry.ID };
                    //entities.ReminderTemplates.Attach(templateToUpdate);

                    ReminderTemplate templateToUpdate = (from row in entities.ReminderTemplates
                                                         where row.ID == entry.ID
                                                         select row).FirstOrDefault();
                    templateToUpdate.IsSubscribed = entry.IsSubscribed;
                });
                
                entities.SaveChanges();
            }
        }
        #endregion

        #region

        public static void SendMassEmail(string subject, string message, int role, List<Tuple<Stream, string>> attachment)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = UserManagement.GetAll(-1).Where(entry=>entry.RoleID!=1).ToList();
                if (role != -1)
                    data = data.Where(entry => entry.RoleID == role).ToList();

                foreach (var em in data)
                {
                    try
                    {
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { em.Email }), null, null, subject, message, attachment);
                    }
                    catch (Exception ex)
                    {
                        MassEmail massemail = new MassEmail();
                        massemail.Subject = subject;
                        massemail.Message = message;
                        massemail.SentOn = DateTime.UtcNow;
                        massemail.Status = false;
                        massemail.SenTo = em.Email;
                        int id = Create(massemail);
                        if (id != 0 && attachment.Count > 0)
                        {
                            foreach (var attach in attachment)
                            {
                                MassEmailAttachment mea = new MassEmailAttachment();
                                BinaryReader br = new BinaryReader(attach.Item1);
                                mea.Attachment = br.ReadBytes((Int32)attach.Item1.Length);
                                mea.Name = attach.Item2;
                                mea.MassEmailID = id;
                                entities.MassEmailAttachments.Add(mea);
                            }

                            entities.SaveChanges();

                        }

                    }
                }
                
            }
            
        }

        public static int Create(MassEmail massemail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.MassEmails.Add(massemail);

                entities.SaveChanges();
                return massemail.ID;
            }
        }

        public static void DeleteMassEmail(List<int> massemail)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                massemail.ForEach(en =>
                {
                        List<MassEmailAttachment> AttaMassItem = (from Aitem in entities.MassEmailAttachments
                                                                  where Aitem.MassEmailID == en
                                                                  select Aitem).ToList();

                        AttaMassItem.ForEach(entry =>
                        {
                            entities.MassEmailAttachments.Remove(entry);
                        });

                        entities.SaveChanges();

                        MassEmail massitem = (from item in entities.MassEmails
                                              where item.ID == en
                                              select item).SingleOrDefault();

                        entities.MassEmails.Remove(massitem);
                        entities.SaveChanges();
                });
            }
        }

        public static void SendFailedMail()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var data = (from row in entities.MassEmails
                            select row).ToList();

                List<int> IdsforDelete = new List<int>();

                foreach (MassEmail row in data)
                {
                    try
                    {
                        List<Tuple<Stream, string>> attachment = new List<Tuple<Stream, string>>();
                        List<MassEmailAttachment> EmailAttachment = (from emattach in entities.MassEmailAttachments
                                                                     select emattach).ToList();

                        foreach(MassEmailAttachment mea in EmailAttachment)
                        {
                           Stream stream = new MemoryStream(mea.Attachment);
                           attachment.Add(new Tuple<Stream,string>(stream,mea.Name));
                        }

                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { row.SenTo }), null, null, row.Subject, row.Message, attachment);
                        IdsforDelete.Add(row.ID);
                    }
                    catch (Exception)
                    {
                        throw;

                    }
                }

                DeleteMassEmail(IdsforDelete);
               
            }
        }

        #endregion

    }
}
