﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Business.ComplianceFund
{
    public class FundMasterManagement
    {
        public static List<CompFund_tbl_ComplianceStatus> getStatus()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objStatusList = (from row in entities.CompFund_tbl_ComplianceStatus
                                     orderby row.Name
                                     select row).ToList();

                return objStatusList;
            }
        }

        public static CompFund_tbl_FileData GetFile(long FileID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CompFund_tbl_FileData file = (from row in entities.CompFund_tbl_FileData
                                              where row.IsDeleted == false
                                           && row.ID == FileID
                                           select row).FirstOrDefault();

                return file;
            }
        }


        public static bool DeleteFile(long FileID, int userID)
        {
            bool result = false;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CompFund_tbl_FileData objDeletelist = (from row in entities.CompFund_tbl_FileData
                                                       where row.IsDeleted == false
                                                    && row.ID == FileID
                                                    select row).FirstOrDefault();
                if (objDeletelist != null)
                {
                    objDeletelist.IsDeleted = true;
                    objDeletelist.UpdatedOn = DateTime.Now;
                    objDeletelist.UpdatedBy = userID;
                    entities.SaveChanges();
                    result = true;
                }
                return result;
            }
        }

        public static bool CreateTransaction(CompFund_tbl_ChecklistAuditTransaction transaction)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            transaction.Dated = DateTime.Now;
                            entities.CompFund_tbl_ChecklistAuditTransaction.Add(transaction);
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                LogMessage msg = new LogMessage();
                msg.ClassName = "ComplianceManagement";
                msg.FunctionName = "CreateTransaction";
                msg.CreatedOn = DateTime.Now;
                msg.Message = ex.Message + "----\r\n" + ex.InnerException;
                msg.StackTrace = ex.StackTrace;
                msg.LogLevel = (byte)com.VirtuosoITech.Logger.Loglevel.Error;
                ReminderUserList.Add(msg);
                //InsertLogToDatabase(ReminderUserList);

                //List<string> TO = new List<string>();
                //TO.Add("amol@avantis.info");
                //TO.Add("rahul@avantis.co.in");
                //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(TO), new List<String>(TO),
                //    null, "Error Occured as CreateTransaction Function", "CreateTransaction");


                ComplianceManagement.SendgridSenEmail("Error Occured as CreateTransaction Function", "CreateTransaction");

                return false;
            }
        }

        public static bool FileUpload(long checklistID, long ChecklistScheduleOnID, List<CompFund_tbl_FileData> files, List<KeyValuePair<string, int>> list, List<KeyValuePair<string, Byte[]>> filesList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            DocumentManagement.Vendor_SaveDocFiles(filesList);
                            if (files != null)
                            {
                                foreach (CompFund_tbl_FileData fl in files)
                                {
                                    fl.IsDeleted = false;
                                    fl.EnType = "A";
                                    entities.CompFund_tbl_FileData.Add(fl);
                                    entities.SaveChanges();

                                    CompFund_tbl_FileDataMapping fileMapping = new CompFund_tbl_FileDataMapping();
                                    fileMapping.ChecklistID = checklistID;
                                    fileMapping.FileID = fl.ID;
                                    fileMapping.ScheduledOnID = ChecklistScheduleOnID;
                                    if (list != null)
                                    {
                                        fileMapping.FileType = Convert.ToInt16(list.Where(ent => ent.Key.Equals(fl.Name)).FirstOrDefault().Value);
                                    }
                                    entities.CompFund_tbl_FileDataMapping.Add(fileMapping);
                                }
                            }
                            entities.SaveChanges();
                            dbtransaction.Commit();
                            return true;
                        }
                        catch (Exception)
                        {
                            dbtransaction.Rollback();
                            if (filesList != null)
                            {
                                foreach (var dfile in filesList)
                                {
                                    if (System.IO.File.Exists(dfile.Key))
                                    {
                                        System.IO.File.Delete(dfile.Key);
                                    }
                                }
                            }
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage> ReminderUserList = new List<com.VirtuosoITech.ComplianceManagement.Business.Data.LogMessage>();
                return false;
            }
        }


        public static List<CompFund_SP_Checklist_Status_Result> GetStatusDetails(int ChecklistID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_SP_Checklist_Status(ChecklistID, customerID)
                            select row).ToList();
                return Data;
            }
        }

        public static List<CompFund_tbl_FileData> getDocumentData(long CheckListID, long ScheduledOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objChecklist = (from row in entities.CompFund_tbl_FileData
                                    join row1 in entities.CompFund_tbl_FileDataMapping
                                    on row.ID equals row1.FileID
                                    where row.IsDeleted == false
                                          && row1.ChecklistID == CheckListID
                                          && row1.ScheduledOnID == ScheduledOnID
                                    select row).ToList();

                return objChecklist;
            }
        }

        public static List<CompFund_SP_ChecklistGetAuditLogTransaction_Result> GetAllTransactionLog(int ScheduledOnID, long checklistID,long AuditchecklistID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var statusList = (from row in entities.CompFund_SP_ChecklistGetAuditLogTransaction(ScheduledOnID, checklistID, AuditchecklistID)
                                  orderby row.Dated descending
                                  select row).ToList();

                return statusList;
            }
        }
        public static bool UpdateInvestorDetails(CompFund_tbl_InvestorMaster _objDoctype)
        {
            bool updateSuccess = false;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CompFund_tbl_InvestorMaster objRecord = (from row in entities.CompFund_tbl_InvestorMaster
                                                             where row.ID == _objDoctype.ID
                                                             && row.CustomerID == _objDoctype.CustomerID
                                                             select row).FirstOrDefault();
                    if (objRecord != null)
                    {
                        objRecord.InvestorName = _objDoctype.InvestorName;
                        objRecord.UpdatedBy = _objDoctype.UpdatedBy;
                        objRecord.UpdatedOn = _objDoctype.UpdatedOn;

                        entities.SaveChanges();

                        updateSuccess = true;
                    }
                }

                return updateSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static long CreateInvestorName(CompFund_tbl_InvestorMaster _objDoctype)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.CompFund_tbl_InvestorMaster.Add(_objDoctype);
                    entities.SaveChanges();

                    return _objDoctype.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public static bool ExistsInvestorName(CompFund_tbl_InvestorMaster _objDoctype)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objCase = (from row in entities.CompFund_tbl_InvestorMaster
                               where row.InvestorName.Equals(_objDoctype.InvestorName)
                               && row.CustomerID == _objDoctype.CustomerID
                               select row);

                return objCase.Select(entry => true).FirstOrDefault();
            }
        }
        public static CompFund_tbl_InvestorMaster GetInvestorDetailsByID(long ID, int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Objcase = (from row in entities.CompFund_tbl_InvestorMaster
                               where row.ID == ID
                               && row.CustomerID == customerID
                               select row).FirstOrDefault();
                return Objcase;
            }
        }
        public static bool DeleteInvestors(long ID, long customerID)
        {
            bool deleteSuccess = false;
            try
            {

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CompFund_tbl_InvestorMaster objCase = (from row in entities.CompFund_tbl_InvestorMaster
                                                           where row.ID == ID
                                                           && row.CustomerID == customerID
                                                           select row).FirstOrDefault();
                    if (objCase != null)
                    {
                        objCase.IsDeleted = true;
                        entities.SaveChanges();

                        deleteSuccess = true;
                    }
                }

                return deleteSuccess;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return deleteSuccess;
            }
        }
        public static List<CompFund_tbl_InvestorMaster> GetInvestors(long cutomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_tbl_InvestorMaster
                                  where row.CustomerID== cutomerID
                                  && row.IsDeleted == false
                                  select row).ToList();
                
                return Data;
            }
        }
        public static List<CompFund_SP_GetALLChecklist_Result> GetChecklist(int Dtype,int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_SP_GetALLChecklist(Dtype, CID)
                            select row).ToList();

                return Data;
            }
        }

        public static List<CompFund_SP_GetALLChecklistNew_Result> GetChecklistNew(int Dtype, int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Data = (from row in entities.CompFund_SP_GetALLChecklistNew(Dtype, CID)
                            select row).ToList();

                return Data;
            }
        }

        public static long saveCheckListDetail(CompFund_tbl_Checklist objchklst)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.CompFund_tbl_Checklist.Add(objchklst);
                    entities.SaveChanges();

                    return objchklst.Id;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static bool ExistsChecklistStatusName(CompFund_tbl_Checklist_Status _objDoctype)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objCase = (from row in entities.CompFund_tbl_Checklist_Status
                               where row.Status.Equals(_objDoctype.Status)
                               && row.CustomerId == _objDoctype.CustomerId
                                && row.ChecklistID == _objDoctype.ChecklistID
                               && row.Remark == _objDoctype.Remark
                                 && row.IsDeleted == false                                 
                               select row);

                return objCase.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool ExistsChecklistMapping(int CID, int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.CompFund_tbl_ChecklistMapping
                                 where row.IsDeleted == false
                                 && row.CustomerID == CID
                                 && row.CheckListId == ID
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }
        public static void DeleteCheckListDetailByID(int objchklstStatusID, int UID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                CompFund_tbl_Checklist Data = (from row in entities.CompFund_tbl_Checklist
                                               where row.Id == objchklstStatusID
                                               select row).FirstOrDefault();

                if (Data != null)
                {
                    Data.IsDeleted = true;
                    Data.UpdatedBy = UID;
                    Data.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                }
            }
        }
        public static void DeleteCheckListStatusDetailByID(int objchklstStatusID,int UID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CompFund_tbl_Checklist_Status objStatus = (from row in entities.CompFund_tbl_Checklist_Status
                                                               where row.ID == objchklstStatusID
                                                               select row).FirstOrDefault();
                    if (objStatus != null)
                    {
                        objStatus.IsDeleted = true;
                        objStatus.UpdatedBy = UID;
                        objStatus.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void saveCheckListStatusDetail(CompFund_tbl_Checklist_Status objchklst)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    CompFund_tbl_Checklist_Status objStatus = (from row in entities.CompFund_tbl_Checklist_Status
                                                             where row.Status.Equals(objchklst.Status)
                                                             && row.CustomerId == objchklst.CustomerId
                                                              && row.ChecklistID == objchklst.ChecklistID
                                                               select row).FirstOrDefault();
                    if (objStatus != null)
                    {
                        objStatus.Remark = objchklst.Remark;
                        objStatus.UpdatedBy = objchklst.CreatedBy;
                        objchklst.UpdatedOn = DateTime.Now;
                        objStatus.IsDeleted = false;
                        entities.SaveChanges();
                    }
                    else
                    {
                        entities.CompFund_tbl_Checklist_Status.Add(objchklst);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        public static bool ExistsChecklistData(string Clause_ref,string Comp_Que,bool ISFundDoc,int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.CompFund_tbl_Checklist
                                 where row.Clause_ref_num == Clause_ref
                                 && row.Comp_Que == Comp_Que
                                 && row.IsDeleted == false
                                 && row.IsFundDocument == ISFundDoc
                                 && row.CustomerId == CID
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool ExistsSideLetterChecklistData(string Clause_ref, string Comp_Que,string InvestorName, bool ISFundDoc,int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var obj = (from row in entities.CompFund_tbl_InvestorMaster
                                 where row.InvestorName.Trim() == InvestorName.Trim()
                                 && row.IsDeleted == false
                                 && row.CustomerID == CID
                                 select row);
                if (obj != null)
                {
                    var objVendor = (from row in entities.CompFund_tbl_Checklist
                                     where row.Clause_ref_num == Clause_ref
                                     && row.Comp_Que == Comp_Que
                                     && row.IsDeleted == false
                                     && row.IsFundDocument == ISFundDoc
                                       && row.CustomerId == CID
                                     select row);

                    return objVendor.Select(entry => true).FirstOrDefault();
                }
                else
                {
                    return obj.Select(entry => true).FirstOrDefault();
                }
            }
        }
        public static bool ExistsInvestorName(string Name,int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.CompFund_tbl_InvestorMaster
                                 where row.InvestorName.Trim() == Name.Trim()
                                 && row.IsDeleted == false
                                 && row.CustomerID == CID
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }
        public static bool ExistsInvestorNameforupdate(string Name, int CID,int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.CompFund_tbl_InvestorMaster
                                 where row.InvestorName.Trim() == Name.Trim()
                                 && row.IsDeleted == false
                                 && row.CustomerID == CID
                                 && row.ID != ID
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }
        public static long GetInvestorNameID(string Name, int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objinvestorid = (from row in entities.CompFund_tbl_InvestorMaster
                                 where row.InvestorName.Trim() == Name.Trim()
                                 && row.IsDeleted == false
                                 && row.CustomerID == CID
                                 select row).FirstOrDefault();
                if (objinvestorid != null)
                {
                    return objinvestorid.ID;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static bool ExistsInvestorNameMapping(int CID, int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var objVendor = (from row in entities.CompFund_tbl_Checklist
                                 where row.IsDeleted == false
                                 && row.CustomerId == CID
                                 && row.InvestorID == ID
                                 select row);

                return objVendor.Select(entry => true).FirstOrDefault();
            }
        }
    }
}
