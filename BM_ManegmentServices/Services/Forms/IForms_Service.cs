﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Forms
{
    public interface IForms_Service
    {
        List<FormsVM> GetMeetingFormList(long meetingId, long meetingAgendaMappingId, long complianceId);
        FormsDocumentVM GenerateMeetingFormData(long meetingAgendaMappingId, long formMappingId);

        #region Generate E-Forms
        EFormVM GenerateForms(long meetingFormMappingId, long meetingId);

        EFormVM GenerateFormsforDirector(long DirectorID);
        EFormVM GenerateFormforDirectorDIR3(long directorId);
        #endregion

        #region Generate E-Form DPT-3
        EFormVM GenerateDPT3(DPT3EFormVM obj, int entityId);
        #endregion

        #region Generate E-Form AOC-4
        EFormVM GenerateAOC4(long AOC4Id, int customerId);
        #endregion
    }
}
