﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.AOC4;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Forms
{
    public class Forms_Service : IForms_Service
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public List<FormsVM> GetMeetingFormList(long meetingId, long meetingAgendaMappingId, long complianceId)
        {
            try
            {
                var result = (from mapping in entities.BM_MeetingFormMapping
                              join forms in entities.BM_FormMaster on mapping.FormID equals forms.FormID
                              join agendaMapping in entities.BM_MeetingAgendaMapping on mapping.MeetingAgendaMappingId equals agendaMapping.MeetingAgendaMappingID
                              where mapping.MeetingId == meetingId && mapping.IsDeleted == false && agendaMapping.IsDeleted == false && agendaMapping.IsActive == true
                              select new FormsVM
                              {
                                  MeetingFormMappingId = mapping.MeetingFormMappingID,
                                  MeetingId = meetingId,
                                  MeetingAgendaMappingId = mapping.MeetingAgendaMappingId,
                                  ComplianceId = (long)mapping.ComplianceId,

                                  FormId = (long)mapping.FormID,
                                  EForm = forms.EForm,
                                  FormName = mapping.FormName,
                                  IsEForm = forms.IsEForm,
                                  CanGenerate = agendaMapping.Result == SecretarialConst.AgendaResult.APPROVED ? true : false,
                                  ForMultipleEvents = forms.ForMultipleEvents,
                                  EventCount = 1
                              });

                if (meetingAgendaMappingId > 0)
                {
                    result = (from row in result
                              where row.MeetingAgendaMappingId == meetingAgendaMappingId
                              select row);
                }

                if (complianceId > 0)
                {
                    result = (from row in result
                              where row.ComplianceId == complianceId
                              select row);
                }

                var lst = result.ToList();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.ForMultipleEvents == true)
                        {
                            item.EventCount = (from row in entities.BM_ComplianceScheduleOnNew
                                               where row.MeetingID == item.MeetingId && row.ComplianceID == item.ComplianceId && row.IsActive == true && row.IsDeleted == false
                                               select row.ComplianceID
                                               ).Count();

                            if (item.EForm == "MGT-14" && item.EventCount > 1)
                            {
                                //item.EventMessage = "The form will be generated for " + item.EventCount + " resolution(s).";
                                item.EventMessage = "After generating the form type " + item.EventCount + " instead of 10 in point Number 6.";
                            }
                            else if (item.EForm == "DIR-12New" && item.EventCount > 1)
                            {
                                var directorCount = 0; var kmpCount = 0;
                                var lstUIForms = (from row in entities.BM_ComplianceScheduleOnNew
                                                  join agenda in entities.BM_AgendaMaster on row.AgendaMasterID equals agenda.BM_AgendaMasterId
                                                  join forms in entities.BM_UIForm on agenda.UIFormID equals forms.UIFormID
                                                  where row.MeetingID == item.MeetingId && row.ComplianceID == item.ComplianceId && row.IsActive == true && row.IsDeleted == false
                                                  select new
                                                  {
                                                      row.ComplianceID,
                                                      agenda.UIFormID
                                                  }
                                               ).ToList();
                                if (lstUIForms.Count > 0)
                                {
                                    directorCount = lstUIForms.Where(k => k.UIFormID > 10).Select(k => k.UIFormID).Count();
                                    kmpCount = lstUIForms.Where(k => k.UIFormID < 10 && k.UIFormID > 0).Select(k => k.UIFormID).Count();

                                    item.EventMessage = "The form will be generated for";
                                    if (directorCount > 0)
                                    {
                                        item.EventMessage += " " + directorCount + " MD/Director(s)";
                                    }

                                    if (kmpCount > 0)
                                    {
                                        if (directorCount > 0)
                                        {
                                            item.EventMessage += " &";
                                        }

                                        item.EventMessage += " " + kmpCount + "  Manager/CFO/CEO/CS.";
                                    }
                                }
                            }
                        }
                    }
                }
                return lst;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public FormsDocumentVM GenerateMeetingFormData(long meetingAgendaMappingId, long formMappingId)
        {
            var result = new FormsDocumentVM();
            try
            {
                using (Compliance_SecretarialEntities entities1 = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities1.BM_SP_MeetingFormGenerate(meetingAgendaMappingId, formMappingId)
                              select new FormsDocumentVM
                              {
                                  EntityId = (int)row.EntityId,
                                  FormFormat = row.FormFormat,
                                  LetterHead = row.LetterHead
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            if(result == null)
            {
                result = new FormsDocumentVM();
            }
            return result;
        }

        #region Generate E-Forms
        public EFormVM GenerateForm(long meetingFormMappingId, long meetingId)
        {
            var result = new EFormVM() { MeetingFormMappingId = meetingFormMappingId };
            var lstFields = new List<EFormFieldVM>();
            try
            {
                var obj = (from formMapping in entities.BM_MeetingFormMapping
                           join forms in entities.BM_FormMaster on formMapping.FormID equals forms.FormID
                           join agendaMapping in entities.BM_MeetingAgendaMapping on formMapping.MeetingAgendaMappingId equals agendaMapping.MeetingAgendaMappingID
                           join agenda in entities.BM_AgendaMaster on agendaMapping.AgendaID equals agenda.BM_AgendaMasterId
                           join meeting in entities.BM_Meetings on agendaMapping.MeetingID equals meeting.MeetingID
                           join entity in entities.BM_EntityMaster on meeting.EntityId equals entity.Id
                           where formMapping.MeetingFormMappingID == meetingFormMappingId
                           select new
                           {
                               entity.CIN_LLPIN,
                               meeting.MeetingDate,
                               meeting.SendDateNotice,

                               forms.EForm,
                               forms.FormPath,

                               agenda.UIFormID,
                               agendaMapping.RefMasterID,

                               agendaMapping.MeetingAgendaMappingID
                           }).FirstOrDefault();


                //result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR-12.pdf";
                //result.FormName = "DIR-12.pdf";

                if (obj != null)
                {
                    if (obj.RefMasterID > 0)
                    {
                        result.FullFileName = obj.FormPath;
                        result.FormName = obj.EForm + ".pdf";
                        if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                        {
                            switch (obj.UIFormID)
                            {
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CFO:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CS:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_CEO:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CFO:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CS:
                                case SecretarialConst.UIForms.RESIGNATION_OF_CEO:
                                case SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER:
                                    #region E-forms
                                    if (obj.EForm == "DIR-12")
                                    {
                                        #region DIR-12
                                        var cfo = (from row in entities.BM_DirectorMaster
                                                   where row.Id == (long)obj.RefMasterID
                                                   select new
                                                   {
                                                       row.FirstName,
                                                       row.LastName,
                                                       row.MiddleName,

                                                       row.Father,
                                                       row.FatherMiddleName,
                                                       row.FatherLastName,

                                                       row.EmailId_Personal,
                                                       row.PAN,
                                                       row.Present_Address_Line1,
                                                       row.Present_Address_Line2,
                                                       row.Present_PINCode,
                                                       row.MobileNo,

                                                       row.DesignationId,
                                                       row.CS_MemberNo,
                                                       row.Kmp_appointment_Date,
                                                       row.cessation_Date,

                                                       row.Present_CityId

                                                   }).FirstOrDefault();

                                        if (cfo != null)
                                        {
                                            result.FormName = "DIR-12_" + (string.IsNullOrEmpty(cfo.FirstName) ? "" : cfo.FirstName) + (string.IsNullOrEmpty(cfo.LastName) ? "" : "_" + cfo.LastName) + ".pdf";

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", Value = obj.CIN_LLPIN });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DIR_12[0].NUM_M_S_CFO_CEO[0]", Value = "4" });

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].PAN[0]", Value = cfo.PAN });
                                            if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CFO || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CS || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CEO || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].RB_A_C[0]", Value = "APPN" });
                                                if (cfo.Kmp_appointment_Date != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                                }
                                            }
                                            else
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].RB_A_C[0]", Value = "RESG" });
                                                if (cfo.cessation_Date != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.cessation_Date).ToString("yyyy-MM-dd") });
                                                }
                                            }
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].FIRST_NAME[0]", Value = cfo.FirstName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].MIDDLE_NAME[0]", Value = cfo.MiddleName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].LAST_NAME[0]", Value = cfo.LastName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].FIRST_NAME1[0]", Value = cfo.Father });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].MIDDLE_NAME1[0]", Value = cfo.FatherMiddleName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].LAST_NAME1[0]", Value = cfo.FatherLastName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].ADDRESS_LINE1[0]", Value = cfo.Present_Address_Line1 });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].ADDRESS_LINE2[0]", Value = cfo.Present_Address_Line2 });
                                            if (cfo.Present_CityId > 0)
                                            {
                                                var cityName = (from city in entities.Cities
                                                                where city.ID == cfo.Present_CityId
                                                                select city.Name).FirstOrDefault();
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].CITY[0]", Value = cityName });
                                            }

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].STATE[0]", Value = "MH" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].PIN_CODE[0]", Value = cfo.Present_PINCode });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].ISO_COUNTRY_CODE[0]", Value = "IN" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].PHONE[0]", Value = cfo.MobileNo });
                                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_OF_BIRTH[0]", Value = "" });

                                            switch (cfo.DesignationId)
                                            {
                                                case 2:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DESIGNATION2[0]", Value = "MNGR" });
                                                    break;
                                                case 3:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DESIGNATION2[0]", Value = "SCTY" });
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].MEMBERSHIP_NUM[0]", Value = cfo.CS_MemberNo });
                                                    break;
                                                case 4:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DESIGNATION2[0]", Value = "CEO" });
                                                    break;
                                                case 5:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DESIGNATION2[0]", Value = "CFO" });
                                                    break;
                                                default:
                                                    break;
                                            }


                                            //if (cfo.Kmp_appointment_Date != null)
                                            //{
                                            //    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                            //}
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].EMAIL_ID[0]", Value = cfo.EmailId_Personal });
                                            //lstFields.Add(new EFormFieldVM() { Name = "", Value = "" });
                                        }
                                        #endregion
                                    }
                                    else if (obj.EForm == "MGT-14")
                                    {
                                        #region MGT-14
                                        var agendaDetails = (from row in entities.BM_SP_GetAgendaTemplate(obj.MeetingAgendaMappingID, null)
                                                             select row).FirstOrDefault();

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].CIN[0]", Value = obj.CIN_LLPIN });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].CB1_REGISTRATION[0]", Value = "RESL" });
                                        if (obj.SendDateNotice != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].RESOLUTION[0]", Value = Convert.ToDateTime(obj.SendDateNotice).ToString("yyyy-MM-dd") });
                                        }
                                        if (obj.MeetingDate != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].RESOLUTION1[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].NO_OF_RESOLUTION[0]", Value = "1" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].SECTION_PASSED[0]", Value = "179(3)" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].SECTION_PASSED1[0]", Value = "" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].PASSING_RESLTN[0]", Value = "C1" });

                                        if (agendaDetails != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].SUBJECT_RES[0]", Value = System.Text.RegularExpressions.Regex.Replace(agendaDetails.MinutesFormatHeading, "<.*?>", String.Empty) });
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].AUTHRTY_PASSING[0]", Value = "BDIR" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[0].RB_RESOLUTION[0]", Value = "WRQM" });

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].AUTHORIZED[0]", Value = "12" }); //Director
                                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DATE_DECLARATION[0]", Value = "" }); // Date of Declaration
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DESIGNATION[0]", Value = "DIRT" }); //Director
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DIN_PAN[0]", Value = "" }); //DIN
                                        #endregion
                                    }
                                    else if (obj.EForm == "MR-1")
                                    {
                                        #region MR-1
                                        var cfo = (from row in entities.BM_DirectorMaster
                                                   where row.Id == (long)obj.RefMasterID
                                                   select new
                                                   {
                                                       row.FirstName,
                                                       row.LastName,
                                                       row.MiddleName,

                                                       row.Father,
                                                       row.FatherMiddleName,
                                                       row.FatherLastName,

                                                       row.EmailId_Personal,
                                                       row.PAN,
                                                       row.Present_Address_Line1,
                                                       row.Present_Address_Line2,
                                                       row.Present_PINCode,
                                                       row.MobileNo,

                                                       row.DesignationId,
                                                       row.CS_MemberNo,
                                                       row.Kmp_appointment_Date,
                                                       row.cessation_Date,

                                                       row.Present_CityId

                                                   }).FirstOrDefault();
                                        if (cfo != null)
                                        {
                                            var fullName = (string.IsNullOrEmpty(cfo.FirstName) ? "" : cfo.FirstName) + (string.IsNullOrEmpty(cfo.MiddleName) ? "" : " " + cfo.MiddleName) + (string.IsNullOrEmpty(cfo.LastName) ? "" : " " + cfo.LastName);


                                            result.FormName = "MR-1_" + (string.IsNullOrEmpty(fullName) ? "" : fullName.Replace(" ", "_")) + ".pdf";

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].CIN[0]", Value = obj.CIN_LLPIN });

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DIN[0]", Value = cfo.PAN });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].NAME[0]", Value = fullName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DESIGNATION_OPT[0]", Value = "MANG" });

                                            if (obj.MeetingDate != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RESOLUTION_DATE[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                            }

                                            if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER)
                                            {
                                                if (cfo.Kmp_appointment_Date != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].EFFECT_DATE_APP[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].FROM_DATE[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                                }

                                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].TO_DATE[0]", Value = "" });
                                            }


                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RB_AGE_APPOINTEE[0]", Value = "NO" });
                                        }
                                        #endregion
                                    }
                                    #endregion
                                    break;

                                default:
                                    break;
                            }
                            SetData(result, lstFields);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public EFormVM GenerateForms(long meetingFormMappingId, long meetingId)
        {
            var result = new EFormVM() { MeetingFormMappingId = meetingFormMappingId };
            var lstFields = new List<EFormFieldVM>();
            var lstFormsVM = new List<MultipleFormsVM>();

            try
            {
                var obj = (from formMapping in entities.BM_MeetingFormMapping
                           join forms in entities.BM_FormMaster on formMapping.FormID equals forms.FormID
                           join agendaMapping in entities.BM_MeetingAgendaMapping on formMapping.MeetingAgendaMappingId equals agendaMapping.MeetingAgendaMappingID
                           join agenda in entities.BM_AgendaMaster on agendaMapping.AgendaID equals agenda.BM_AgendaMasterId
                           join meeting in entities.BM_Meetings on agendaMapping.MeetingID equals meeting.MeetingID
                           join entity in entities.BM_EntityMaster on meeting.EntityId equals entity.Id
                           where formMapping.MeetingFormMappingID == meetingFormMappingId
                           select new
                           {
                               entity.CIN_LLPIN,
                               meeting.MeetingDate,
                               meeting.SendDateNotice,
                               meeting.MeetingTypeId,

                               forms.FormID,
                               forms.EForm,
                               forms.ForMultipleEvents,
                               forms.FormPath,
                               forms.MultipleFormPath,

                               agenda.UIFormID,
                               agendaMapping.RefMasterID,
                               agendaMapping.ItemNo,

                               agendaMapping.MeetingID,
                               agendaMapping.MeetingAgendaMappingID,

                               formMapping.MeetingFormMappingID,
                               formMapping.ComplianceId
                           }).FirstOrDefault();

                //result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR-12.pdf";
                //result.FormName = "DIR-12.pdf";

                if (obj != null)
                {
                    if (obj.ForMultipleEvents == true)
                    {
                        lstFormsVM = (from formMapping in entities.BM_MeetingFormMapping
                                      join forms in entities.BM_FormMaster on formMapping.FormID equals forms.FormID
                                      join agendaMapping in entities.BM_MeetingAgendaMapping on formMapping.MeetingAgendaMappingId equals agendaMapping.MeetingAgendaMappingID
                                      join agenda in entities.BM_AgendaMaster on agendaMapping.AgendaID equals agenda.BM_AgendaMasterId
                                      where formMapping.MeetingId == obj.MeetingID && formMapping.FormID == obj.FormID && formMapping.ComplianceId == obj.ComplianceId &&
                                      agendaMapping.IsActive == true && agendaMapping.IsDeleted == false
                                      select new MultipleFormsVM
                                      {
                                          //forms.EForm,
                                          //forms.ForMultipleEvents,
                                          //forms.FormPath,
                                          //forms.MultipleFormPath,
                                          UIFormID = agenda.UIFormID,
                                          RefMasterID = agendaMapping.RefMasterID,
                                          ItemNo = agendaMapping.ItemNo,

                                          MeetingId = agendaMapping.MeetingID,
                                          MeetingAgendaMappingId = agendaMapping.MeetingAgendaMappingID,

                                          MeetingFormMappingId = formMapping.MeetingFormMappingID
                                      }).ToList();
                    }
                    else
                    {
                        lstFormsVM.Add(new MultipleFormsVM()
                        {
                            MeetingId = obj.MeetingID,
                            MeetingAgendaMappingId = obj.MeetingAgendaMappingID,
                            MeetingFormMappingId = obj.MeetingFormMappingID,
                            RefMasterID = obj.RefMasterID,
                            ItemNo = obj.ItemNo,
                            UIFormID = obj.UIFormID
                        });
                    }

                    result.FormName = obj.EForm + ".pdf";

                    var index = -1;
                    var indexDirector = -1;
                    if (obj.EForm == "DIR-12")
                    {
                        #region DIR-12
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", Value = obj.CIN_LLPIN });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DIR_12[0].NUM_M_S_CFO_CEO[0]", Value = "4" });

                        foreach (var item in lstFormsVM)
                        {
                            if (item.RefMasterID > 0)
                            {
                                switch (item.UIFormID)
                                {
                                    #region KMP
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_CFO:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_CS:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_CEO:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_CFO:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_CS:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_CEO:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_MANAGER:

                                        var cfo = (from details in entities.BM_Directors_DetailsOfInterest
                                                   join row in entities.BM_DirectorMaster on details.Director_Id equals row.Id
                                                   where details.Id == (long)item.RefMasterID
                                                   select new
                                                   {
                                                       row.Id,
                                                       row.DIN,
                                                       row.FirstName,
                                                       row.LastName,
                                                       row.MiddleName,

                                                       row.Father,
                                                       row.FatherMiddleName,
                                                       row.FatherLastName,

                                                       row.EmailId_Personal,
                                                       row.PAN,
                                                       row.Present_Address_Line1,
                                                       row.Present_Address_Line2,
                                                       row.Present_PINCode,
                                                       row.MobileNo,
                                                       row.DOB,

                                                       row.CS_MemberNo,
                                                       Kmp_appointment_Date = details.DateOfAppointment,
                                                       cessation_Date = details.CessationEffectFrom,

                                                       row.Present_CityId,

                                                       details.TenureOfAppointment,
                                                       details.DirectorDesignationId,
                                                   }).FirstOrDefault();

                                        if (cfo != null)
                                        {
                                            index++;

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].PAN[0]", Value = cfo.PAN });
                                            if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CFO || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CS || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CEO || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].RB_A_C[0]", Value = "APPN" });
                                                if (cfo.Kmp_appointment_Date != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                                }
                                            }
                                            else
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].RB_A_C[0]", Value = "RESG" });
                                                if (cfo.cessation_Date != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.cessation_Date).ToString("yyyy-MM-dd") });
                                                }
                                            }
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].FIRST_NAME[0]", Value = cfo.FirstName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].MIDDLE_NAME[0]", Value = cfo.MiddleName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].LAST_NAME[0]", Value = cfo.LastName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].FIRST_NAME1[0]", Value = cfo.Father });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].MIDDLE_NAME1[0]", Value = cfo.FatherMiddleName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].LAST_NAME1[0]", Value = cfo.FatherLastName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].ADDRESS_LINE1[0]", Value = cfo.Present_Address_Line1 });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].ADDRESS_LINE2[0]", Value = cfo.Present_Address_Line2 });
                                            if (cfo.Present_CityId > 0)
                                            {
                                                var cityName = (from city in entities.Cities
                                                                where city.ID == cfo.Present_CityId
                                                                select city.Name).FirstOrDefault();
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].CITY[0]", Value = cityName });
                                            }

                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].STATE[0]", Value = "MH" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].PIN_CODE[0]", Value = cfo.Present_PINCode });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].ISO_COUNTRY_CODE[0]", Value = "IN" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].PHONE[0]", Value = cfo.MobileNo });
                                            if (cfo.DOB != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DATE_OF_BIRTH[0]", Value = Convert.ToDateTime(cfo.DOB).ToString("yyyy-MM-dd") });
                                            }

                                            switch (cfo.DirectorDesignationId)
                                            {
                                                case 2:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DESIGNATION2[0]", Value = "MNGR" });
                                                    break;
                                                case 3:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DESIGNATION2[0]", Value = "SCTY" });
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].MEMBERSHIP_NUM[0]", Value = cfo.CS_MemberNo });
                                                    break;
                                                case 4:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DESIGNATION2[0]", Value = "CEO" });
                                                    break;
                                                case 5:
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].DESIGNATION2[0]", Value = "CFO" });
                                                    break;
                                                default:
                                                    break;
                                            }


                                            //if (cfo.Kmp_appointment_Date != null)
                                            //{
                                            //    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].DATE_APP_CESSATN[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                            //}
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_S[0].DATA[" + index + "].EMAIL_ID[0]", Value = cfo.EmailId_Personal });
                                            //lstFields.Add(new EFormFieldVM() { Name = "", Value = "" });

                                        }

                                        break;
                                    #endregion

                                    #region Director
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_MD:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_WTD:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_ADDD:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_ADDD_INDEPENDANT:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_CAVD:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_NOMD:
                                    case SecretarialConst.UIForms.APPOINTMENT_OF_ALTD:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_MD:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_WTD:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_ADDD:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_ADDD_INDEPENDANT:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_CAVD:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_NOMD:
                                    case SecretarialConst.UIForms.RESIGNATION_OF_ALTD:
                                        var director = (from details in entities.BM_Directors_DetailsOfInterest
                                                        join row in entities.BM_DirectorMaster on details.Director_Id equals row.Id
                                                        where details.Id == (long)item.RefMasterID
                                                        select new
                                                        {
                                                            row.Id,
                                                            row.DIN,
                                                            row.FirstName,
                                                            row.LastName,
                                                            row.MiddleName,

                                                            row.Father,
                                                            row.FatherMiddleName,
                                                            row.FatherLastName,

                                                            row.EmailId_Personal,
                                                            row.PAN,
                                                            row.Present_Address_Line1,
                                                            row.Present_Address_Line2,
                                                            row.Present_PINCode,
                                                            row.MobileNo,

                                                            row.DesignationId,
                                                            row.CS_MemberNo,
                                                            row.Kmp_appointment_Date,
                                                            row.cessation_Date,

                                                            row.Present_CityId,

                                                            details.DateOfAppointment,
                                                            details.TenureOfAppointment,
                                                            details.DirectorDesignationId,
                                                            details.IfDirector,

                                                            details.CompanyOrInstitutionForNominee,
                                                            details.EmailIdOfDirectorForNominee,
                                                            details.CessationEffectFrom,

                                                            details.OriginalDirector_Id

                                                        }).FirstOrDefault();

                                        if (director != null)
                                        {
                                            indexDirector++;
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DIN[0]", Value = director.DIN });

                                            if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_WTD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ADDD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ADDD_INDEPENDANT || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CAVD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_NOMD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ALTD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].RB_A_C_CD[0]", Value = "APPN" });

                                                if (director.DateOfAppointment != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DATE_APP_CD[0]", Value = Convert.ToDateTime(director.DateOfAppointment).ToString("yyyy-MM-dd") });
                                                }

                                                if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "MAND" }); //Managing Director
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_WTD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "WHTD" }); //Whole Time Director
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ADDD || item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ADDD_INDEPENDANT)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "ADDD" }); //Additional Director
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_CAVD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "CAVD" }); //Casual vacancy
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_NOMD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "NOMD" }); //Appointment of Nominee director
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].NAME_COMPANY[0]", Value = director.CompanyOrInstitutionForNominee });
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].EMAIL_ID_DIR[0]", Value = director.EmailIdOfDirectorForNominee });
                                                    //data[0].T_ZMCA_NCA_DIR12_M[0].DATA[0].NAME_COMPANY[0]
                                                    //data[0].T_ZMCA_NCA_DIR12_M[0].DATA[0].EMAIL_ID_DIR[0]
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_ALTD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "ALTD" }); //Appointment of Alternate director
                                                    if (director.OriginalDirector_Id > 0)
                                                    {
                                                        var OriginalDirector_DIN = (from row in entities.BM_DirectorMaster
                                                                                    where row.Id == director.OriginalDirector_Id
                                                                                    select row.DIN
                                                                                   ).FirstOrDefault();
                                                        if (OriginalDirector_DIN != null)
                                                        {
                                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DIN1[0]", Value = OriginalDirector_DIN });
                                                        }
                                                    }
                                                }

                                                #region Directorship details
                                                string category = "PROF", chairman = "NONE", executive = "NONE", nonExecutive = "NONE";
                                                switch (director.IfDirector)
                                                {
                                                    case 1:
                                                        chairman = "CHRM";
                                                        executive = "EXEC";
                                                        break;
                                                    case 4:
                                                        chairman = "CHRM";
                                                        nonExecutive = "NEXE";
                                                        break;
                                                    case 5:
                                                        executive = "EXEC";
                                                        break;
                                                    case 6:
                                                        nonExecutive = "NEXE";
                                                        break;
                                                    case 7:
                                                        nonExecutive = "NEXE";
                                                        category = "INDP";
                                                        break;
                                                    case 9:
                                                        chairman = "CHRM";
                                                        nonExecutive = "NEXE";
                                                        category = "INDP";
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                #endregion

                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CATEGORY[0]", Value = category });

                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_CHAIRMAN[0]", Value = chairman });
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_EXEC_DIRECTOR[0]", Value = executive });
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_N_EXE_DIR[0]", Value = nonExecutive }); //NEXE

                                                //lstFields.Add(new EFormFieldVM() { Name = "", Value = director.DIN });

                                                var detailsOfInterest = (from details in entities.BM_Directors_DetailsOfInterest
                                                                         join entity in entities.BM_EntityMaster on details.EntityId equals entity.Id
                                                                         join designation in entities.BM_Directors_Designation on details.DirectorDesignationId equals designation.Id
                                                                         where details.Director_Id == director.Id && details.Id != (long)item.RefMasterID &&
                                                                             details.IsDeleted == false && details.IsActive == true
                                                                         select new
                                                                         {
                                                                             entity.CIN_LLPIN,
                                                                             designation.Name,
                                                                             details.PercentagesOfShareHolding,
                                                                             details.DirectorDesignationId
                                                                         }).ToList();

                                                if (detailsOfInterest.Count > 0)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].NUM_SUCH_ENTITIE[0]", Value = detailsOfInterest.Count.ToString() }); //NEXE
                                                    var entitydetails = detailsOfInterest.FirstOrDefault();

                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CIN_LLPIN_FCRN_R[0]", Value = entitydetails.CIN_LLPIN });
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION1[0]", Value = entitydetails.Name });
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].PERCENTAGE_OF_SH[0]", Value = entitydetails.PercentagesOfShareHolding.ToString() });
                                                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[0].AMOUNT[0]", Value = "" });
                                                }

                                            }
                                            else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_MD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_WTD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ADDD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ADDD_INDEPENDANT || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_CAVD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_NOMD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ALTD)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].RB_A_C_CD[0]", Value = "RESG" });
                                                lstFields.Add(new EFormFieldVM() { Name = " data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].REASON[0]", Value = "RESG" });

                                                if (director.CessationEffectFrom != null)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DATE_CONFIRMATN[0]", Value = Convert.ToDateTime(director.CessationEffectFrom).ToString("yyyy-MM-dd") });
                                                }

                                                if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_DIRECTOR_NONEXE_INDEPENDANT)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "DIRT" }); //Director
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_MD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "MAND" }); //Managing Director
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_WTD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "WHTD" }); //Whole Time Director
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ADDD || item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ADDD_INDEPENDANT)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "ADDD" }); //Additional Director
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_CAVD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "CAVD" }); //Casual vacancy
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_NOMD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "NOMD" }); //
                                                }
                                                else if (item.UIFormID == SecretarialConst.UIForms.RESIGNATION_OF_ALTD)
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].DESIGNATION[0]", Value = "ALTD" }); //
                                                }

                                                #region Directorship details
                                                string category = "PROF", chairman = "NONE", executive = "NONE", nonExecutive = "NONE";
                                                switch (director.IfDirector)
                                                {
                                                    case 1:
                                                        chairman = "CHRM";
                                                        executive = "EXEC";
                                                        break;
                                                    case 4:
                                                        chairman = "CHRM";
                                                        nonExecutive = "NEXE";
                                                        break;
                                                    case 5:
                                                        executive = "EXEC";
                                                        break;
                                                    case 6:
                                                        nonExecutive = "NEXE";
                                                        break;
                                                    case 7:
                                                        nonExecutive = "NEXE";
                                                        category = "INDP";
                                                        break;
                                                    case 9:
                                                        chairman = "CHRM";
                                                        nonExecutive = "NEXE";
                                                        category = "INDP";
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                #endregion

                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_CHAIRMAN[0]", Value = chairman });
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_EXEC_DIRECTOR[0]", Value = executive });
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_DIR12_M[0].DATA[" + indexDirector + "].CB_N_EXE_DIR[0]", Value = nonExecutive }); //NEXE
                                            }
                                        }
                                        #endregion
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        #endregion
                    }
                    else if (obj.EForm == "MGT-14")
                    {
                        #region MGT-14

                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].CIN[0]", Value = obj.CIN_LLPIN });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].CB1_REGISTRATION[0]", Value = "RESL" });
                        if (obj.SendDateNotice != null)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].RESOLUTION[0]", Value = Convert.ToDateTime(obj.SendDateNotice).ToString("yyyy-MM-dd") });
                        }
                        if (obj.MeetingDate != null)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].RESOLUTION1[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                        }

                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].NO_OF_RESOLUTION[0]", Value = "10" });

                        foreach (var item in lstFormsVM)
                        {
                            index++;
                            var agendaDetails = (from row in entities.BM_SP_GetAgendaTemplate(item.MeetingAgendaMappingId, null)
                                                 select row).FirstOrDefault();


                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].SECTION_PASSED[0]", Value = "179(3)" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].SECTION_PASSED1[0]", Value = "" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].PASSING_RESLTN[0]", Value = "C1" });

                            if (agendaDetails != null)
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].SUBJECT_RES[0]", Value = System.Text.RegularExpressions.Regex.Replace(agendaDetails.MinutesFormatHeading, "<.*?>", String.Empty) });
                            }

                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].AUTHRTY_PASSING[0]", Value = "BDIR" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_MGT14_R[0].DATA[" + index + "].RB_RESOLUTION[0]", Value = "WRQM" });
                        }

                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].AUTHORIZED[0]", Value = "12" }); //Director
                                                                                                                               //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DATE_DECLARATION[0]", Value = "" }); // Date of Declaration
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DESIGNATION[0]", Value = "DIRT" }); //Director
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MGT_14[0].DIN_PAN[0]", Value = "" }); //DIN
                        #endregion
                    }
                    else if (obj.EForm == "MR-1" && obj.RefMasterID > 0)
                    {
                        #region MR-1
                        if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MANAGER)
                        {
                            #region Manager
                            var cfo = (from row in entities.BM_DirectorMaster
                                       where row.Id == (long)obj.RefMasterID
                                       select new
                                       {
                                           row.FirstName,
                                           row.LastName,
                                           row.MiddleName,

                                           row.Father,
                                           row.FatherMiddleName,
                                           row.FatherLastName,

                                           row.EmailId_Personal,
                                           row.PAN,
                                           row.Present_Address_Line1,
                                           row.Present_Address_Line2,
                                           row.Present_PINCode,
                                           row.MobileNo,

                                           row.DesignationId,
                                           row.CS_MemberNo,
                                           row.Kmp_appointment_Date,
                                           row.cessation_Date,

                                           row.Present_CityId

                                       }).FirstOrDefault();
                            if (cfo != null)
                            {
                                var fullName = (string.IsNullOrEmpty(cfo.FirstName) ? "" : cfo.FirstName) + (string.IsNullOrEmpty(cfo.MiddleName) ? "" : " " + cfo.MiddleName) + (string.IsNullOrEmpty(cfo.LastName) ? "" : " " + cfo.LastName);


                                result.FormName = "MR-1_" + (string.IsNullOrEmpty(fullName) ? "" : fullName.Replace(" ", "_")) + ".pdf";

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].CIN[0]", Value = obj.CIN_LLPIN });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DIN[0]", Value = cfo.PAN });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].NAME[0]", Value = fullName });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DESIGNATION_OPT[0]", Value = "MANG" });

                                if (obj.MeetingDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RESOLUTION_DATE[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                }

                                if (cfo.Kmp_appointment_Date != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].EFFECT_DATE_APP[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].FROM_DATE[0]", Value = Convert.ToDateTime(cfo.Kmp_appointment_Date).ToString("yyyy-MM-dd") });
                                }

                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].TO_DATE[0]", Value = "" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RB_AGE_APPOINTEE[0]", Value = "NO" });
                            }
                            #endregion
                        }
                        else if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_MD || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_WTD)
                        {
                            #region MD/WTD
                            var director = (from details in entities.BM_Directors_DetailsOfInterest
                                            join row in entities.BM_DirectorMaster on details.Director_Id equals row.Id
                                            where details.Id == (long)obj.RefMasterID
                                            select new
                                            {
                                                row.DIN,
                                                row.FirstName,
                                                row.LastName,
                                                row.MiddleName,

                                                row.Father,
                                                row.FatherMiddleName,
                                                row.FatherLastName,

                                                row.EmailId_Personal,
                                                row.PAN,
                                                row.Present_Address_Line1,
                                                row.Present_Address_Line2,
                                                row.Present_PINCode,
                                                row.MobileNo,

                                                row.DesignationId,
                                                row.CS_MemberNo,
                                                row.Kmp_appointment_Date,
                                                row.cessation_Date,

                                                row.Present_CityId,

                                                details.DateOfAppointment,
                                                details.TenureOfAppointment,
                                                details.DirectorDesignationId,

                                            }).FirstOrDefault();
                            if (director != null)
                            {
                                var fullName = (string.IsNullOrEmpty(director.FirstName) ? "" : director.FirstName) + (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.MiddleName) + (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName);

                                result.FormName = "MR-1_" + (string.IsNullOrEmpty(fullName) ? "" : fullName.Replace(" ", "_")) + ".pdf";

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].CIN[0]", Value = obj.CIN_LLPIN });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DIN[0]", Value = director.PAN });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].NAME[0]", Value = fullName });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].DESIGNATION_OPT[0]", Value = "MDIR" });

                                if (obj.MeetingDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RESOLUTION_DATE[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                }

                                if (director.DateOfAppointment != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].EFFECT_DATE_APP[0]", Value = Convert.ToDateTime(director.DateOfAppointment).ToString("yyyy-MM-dd") });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].FROM_DATE[0]", Value = Convert.ToDateTime(director.DateOfAppointment).ToString("yyyy-MM-dd") });
                                }

                                if (director.TenureOfAppointment != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].TO_DATE[0]", Value = Convert.ToDateTime(director.TenureOfAppointment).ToString("yyyy-MM-dd") });
                                }
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_MR_1[0].RB_AGE_APPOINTEE[0]", Value = "NO" });
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else if (obj.EForm == "ADT-1" && obj.RefMasterID > 0)
                    {
                        #region ADT-1
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].CIN[0]", Value = obj.CIN_LLPIN });

                        //Changed on 06 Sep 2021
                        //if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OR_REAPPOINTMENT_OF_STATUTORY_AUDITOR_IN_AGM) 
                        if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OR_REAPPOINTMENT_OF_STATUTORY_AUDITOR_IN_AGM || obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OR_REAPPOINTMENT_OF_STATUTORY_AUDITOR_IN_AGM_SUB_SEQUENT)
                        {
                            #region Auditor
                            #region Statutory Auditor Master Data
                            var auditor = (from row in entities.BM_StatutoryAuditor
                                           where row.Id == obj.RefMasterID
                                           && row.Flag == "SA"
                                           select new VMStatutory_Auditor
                                           {
                                               Id = row.Id,
                                               Class_of_Company_sec139 = row.Class_of_Company_sec139,
                                               Nature_of_Appointment = row.Nature_of_Appointment,
                                               IsJoin_Auditor_Appointed = row.IsJoin_Auditor_Appointed,
                                               Auditor_Number = row.Auditor_Number,
                                               Auditor_AGM = (bool)row.Auditor_AGM,
                                               Date_AGM = row.Date_AGM,
                                               Date_Appointment = row.Date_Appointment,
                                               IsAuditor_Appointed_casualvacancy = (bool)row.IsAuditor_Appointed_casualvacancy,
                                               SRN_relevent_form = row.SRN_relevent_form,
                                               Person_vacated_office = row.Person_vacated_office,
                                               Membership_Number_or_Registrationfirm = row.Membership_Number_or_Registrationfirm,
                                               Date_vacancy = row.Date_vacancy,
                                               Reasons_casual_vacancy = row.Reasons_casual_vacancy,

                                               AGMNo_TillAppointmentTerminate = row.AGMNo_TillAppointmentTerminate

                                           }).FirstOrDefault();
                            if (auditor != null)
                            {
                                auditor.AuditDeatilsList = (from x in entities.BM_StatutoryAuditor_Mapping
                                                            where x.Statutory_AuditorId == obj.RefMasterID && x.IsActive == true && x.flag == "SA"
                                                            select new VMAuditorDetails

                                                            {

                                                                Auditor_CriteriaId = x.CategoryId,
                                                                Auditor_Id = x.Auditor_ID,
                                                                Id = x.Id,
                                                                AppointedFromdate = x.Appointed_FromDate,
                                                                dueDate = x.Appointed_DueDate,
                                                                AppointedFirstFYDate = x.StartFY,
                                                                AppointedLastFYDate = x.EndFY,
                                                                Number_of_appointedFY = x.Appointed_No_of_FY,

                                                            }).ToList();
                            }
                            #endregion

                            if (auditor != null)
                            {
                                if (auditor.AuditDeatilsList != null)
                                {
                                    var auditor1 = auditor.AuditDeatilsList.FirstOrDefault();
                                    var auditorPersonalDetails = (from row in entities.BM_Auditor_Master
                                                                  from city in entities.Cities.Where(k => k.ID == row.city_Id).DefaultIfEmpty()
                                                                  from state in entities.States.Where(k => k.ID == row.state_Id).DefaultIfEmpty()
                                                                  where row.Id == auditor1.Auditor_Id
                                                                  select new
                                                                  {
                                                                      row.PAN,
                                                                      row.FullName_FirmName,
                                                                      row.Reg_MemberNumber,
                                                                      row.AddressLine1,
                                                                      row.AddressLine2,
                                                                      row.city_Id,
                                                                      cityName = city.Name,
                                                                      row.state_Id,
                                                                      row.Country_Id,
                                                                      row.Pin_Number,
                                                                      row.Email_Id
                                                                  }).FirstOrDefault();

                                    if (auditor.Class_of_Company_sec139 == true)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "YES" });
                                    }
                                    else
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "NO" });
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NATURE_APPOINT[0]", Value = "ARGM" });

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_JOINT_AUDITOR[0]", Value = "NO" });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NUM_AUDITORS[0]", Value = "1" });

                                    if (obj.MeetingTypeId == 11)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AGM[0]", Value = "YES" });
                                        if (auditor.Date_AGM != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].DATE_OF_AGM[0]", Value = Convert.ToDateTime(auditor.Date_AGM).ToString("yyyy-MM-dd") });
                                        }
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AUDITOR_APPNT[0]", Value = "NO" });

                                    if (auditor1.Auditor_CriteriaId == 2)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "AFRM" }); //Auditor's Firm
                                    }
                                    else
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "INDI" }); //Individual Auditor
                                    }

                                    if (auditorPersonalDetails != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PAN_AUDITOR[0]", Value = auditorPersonalDetails.PAN }); //PAN
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NAME_AUDITOR[0]", Value = auditorPersonalDetails.FullName_FirmName });//Name
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].MEMBERSHIP_NUM[0]", Value = auditorPersonalDetails.Reg_MemberNumber });//Membership No
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_I[0]", Value = auditorPersonalDetails.AddressLine1 });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_II[0]", Value = auditorPersonalDetails.AddressLine2 });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].CITY[0]", Value = auditorPersonalDetails.cityName });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].STATE[0]", Value = "" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].COUNTRY[0]", Value = "" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PIN_CODE[0]", Value = Convert.ToString(auditorPersonalDetails.Pin_Number) });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].EMAIL_ID_A[0]", Value = auditorPersonalDetails.Email_Id });
                                    }

                                    if (auditor1.AppointedFirstFYDate != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].FROM_DATE[0]", Value = Convert.ToDateTime(auditor1.AppointedFirstFYDate).ToString("yyyy-MM-dd") });
                                    }
                                    if (auditor1.AppointedLastFYDate != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].TO_DATE[0]", Value = Convert.ToDateTime(auditor1.AppointedLastFYDate).ToString("yyyy-MM-dd") });
                                    }

                                    if (auditor1.Number_of_appointedFY != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NUM_F_YEARS[0]", Value = Convert.ToString(auditor1.Number_of_appointedFY) });
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_APPOINTMENT[0]", Value = "YES" });
                                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NUM_F_YEARS1[0]", Value = "" });
                                }
                            }
                            #endregion
                        }
                        else if (obj.UIFormID == SecretarialConst.UIForms.AUDITOR_APPOINTED_IN_CASE_OF_CASUAL_VACANCY_RATIFICATION_IN_EGM)
                        {
                            #region Auditor
                            var auditorAppointedInCV = (from x in entities.BM_StatutoryAuditor_Mapping
                                                        where x.Id == obj.RefMasterID && //x.IsActive == true &&
                                                        x.flag == "SA"
                                                        select new VMAuditorDetails
                                                        {
                                                            Id = x.Id,
                                                            Statutory_AuditorId_ = x.Statutory_AuditorId,
                                                            Auditor_CriteriaId = x.CategoryId,
                                                            Auditor_Id = x.Auditor_ID,
                                                            AppointedFromdate = x.Appointed_FromDate,
                                                            dueDate = x.Appointed_DueDate,
                                                            AppointedFirstFYDate = x.StartFY,
                                                            AppointedLastFYDate = x.EndFY,
                                                            Number_of_appointedFY = x.Appointed_No_of_FY
                                                        }).FirstOrDefault();

                            if (auditorAppointedInCV != null)
                            {


                                #region Statutory Auditor Master Data
                                var auditor = (from row in entities.BM_StatutoryAuditor
                                               where row.Id == auditorAppointedInCV.Statutory_AuditorId_ //obj.RefMasterID
                                               && row.Flag == "SA"
                                               select new VMStatutory_Auditor
                                               {
                                                   Id = row.Id,
                                                   Class_of_Company_sec139 = row.Class_of_Company_sec139,
                                                   Nature_of_Appointment = row.Nature_of_Appointment,
                                                   IsJoin_Auditor_Appointed = row.IsJoin_Auditor_Appointed,
                                                   Auditor_Number = row.Auditor_Number,
                                                   Auditor_AGM = (bool)row.Auditor_AGM,
                                                   Date_AGM = row.Date_AGM,
                                                   Date_Appointment = row.Date_Appointment,
                                                   IsAuditor_Appointed_casualvacancy = (bool)row.IsAuditor_Appointed_casualvacancy,
                                                   SRN_relevent_form = row.SRN_relevent_form,
                                                   Person_vacated_office = row.Person_vacated_office,
                                                   Membership_Number_or_Registrationfirm = row.Membership_Number_or_Registrationfirm,
                                                   Date_vacancy = row.Date_vacancy,
                                                   Reasons_casual_vacancy = row.Reasons_casual_vacancy,

                                                   AGMNo_TillAppointmentTerminate = row.AGMNo_TillAppointmentTerminate

                                               }).FirstOrDefault();
                                if (auditor != null)
                                {
                                    auditor.AuditDeatilsList = new List<VMAuditorDetails>() { auditorAppointedInCV };
                                }
                                #endregion

                                if (auditor != null)
                                {
                                    if (auditor.AuditDeatilsList != null)
                                    {
                                        var auditor1 = auditor.AuditDeatilsList.FirstOrDefault();
                                        var auditorPersonalDetails = (from row in entities.BM_Auditor_Master
                                                                      from city in entities.Cities.Where(k => k.ID == row.city_Id).DefaultIfEmpty()
                                                                      from state in entities.States.Where(k => k.ID == row.state_Id).DefaultIfEmpty()
                                                                      where row.Id == auditor1.Auditor_Id
                                                                      select new
                                                                      {
                                                                          row.PAN,
                                                                          row.FullName_FirmName,
                                                                          row.Reg_MemberNumber,
                                                                          row.AddressLine1,
                                                                          row.AddressLine2,
                                                                          row.city_Id,
                                                                          cityName = city.Name,
                                                                          row.state_Id,
                                                                          row.Country_Id,
                                                                          row.Pin_Number,
                                                                          row.Email_Id
                                                                      }).FirstOrDefault();

                                        if (auditor.Class_of_Company_sec139 == true)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "YES" });
                                        }
                                        else
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "NO" });
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NATURE_APPOINT[0]", Value = "AACV" });

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_JOINT_AUDITOR[0]", Value = "NO" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NUM_AUDITORS[0]", Value = "1" });

                                        if (obj.MeetingTypeId == 11)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AGM[0]", Value = "NO" });
                                            //if (auditor.Date_AGM != null)
                                            //{
                                            //    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].DATE_OF_AGM[0]", Value = Convert.ToDateTime(auditor.Date_AGM).ToString("yyyy-MM-dd") });
                                            //}
                                        }

                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AUDITOR_APPNT[0]", Value = "NO" });

                                        if (auditor1.Auditor_CriteriaId == 2)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "AFRM" }); //Auditor's Firm
                                        }
                                        else
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "INDI" }); //Individual Auditor
                                        }

                                        if (auditorPersonalDetails != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PAN_AUDITOR[0]", Value = auditorPersonalDetails.PAN }); //PAN
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NAME_AUDITOR[0]", Value = auditorPersonalDetails.FullName_FirmName });//Name
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].MEMBERSHIP_NUM[0]", Value = auditorPersonalDetails.Reg_MemberNumber });//Membership No
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_I[0]", Value = auditorPersonalDetails.AddressLine1 });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_II[0]", Value = auditorPersonalDetails.AddressLine2 });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].CITY[0]", Value = auditorPersonalDetails.cityName });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].STATE[0]", Value = "" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].COUNTRY[0]", Value = "" });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PIN_CODE[0]", Value = Convert.ToString(auditorPersonalDetails.Pin_Number) });
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].EMAIL_ID_A[0]", Value = auditorPersonalDetails.Email_Id });
                                        }

                                        if (auditor1.AppointedFromdate != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].FROM_DATE[0]", Value = Convert.ToDateTime(auditor1.AppointedFromdate).ToString("yyyy-MM-dd") });
                                        }
                                        if (auditor1.dueDate != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].TO_DATE[0]", Value = Convert.ToDateTime(auditor1.dueDate).ToString("yyyy-MM-dd") });
                                        }

                                        if (auditor1.Number_of_appointedFY != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NUM_F_YEARS[0]", Value = Convert.ToString(auditor1.Number_of_appointedFY) });
                                        }

                                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_APPOINTMENT[0]", Value = "NO" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AUDITOR_APPNT[0]", Value = "YES" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].SRN_RELEVNT_FORM[0]", Value = auditor.SRN_relevent_form });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].PERSON_VACATED[0]", Value = "AFRM" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].MEM_OR_REG_NUM[0]", Value = auditor.Membership_Number_or_Registrationfirm });
                                        if (auditor.Date_vacancy != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].DATE_VACANCY[0]", Value = Convert.ToDateTime(auditor.Date_vacancy).ToString("yyyy-MM-dd") });
                                        }
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].REASONS_CASUAL_V[0]", Value = auditor.Reasons_casual_vacancy });
                                    }
                                }

                            }

                            #endregion
                        }
                        else if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_FIRST_STATUTORY_AUDITOR)
                        {
                            #region Auditor
                            #region Statutory Auditor Master Data
                            var auditor = (from row in entities.BM_StatutoryAuditor
                                           where row.Id == obj.RefMasterID
                                           && row.Flag == "SA"
                                           select new VMStatutory_Auditor
                                           {
                                               Id = row.Id,
                                               Class_of_Company_sec139 = row.Class_of_Company_sec139,
                                               Nature_of_Appointment = row.Nature_of_Appointment,
                                               IsJoin_Auditor_Appointed = row.IsJoin_Auditor_Appointed,
                                               Auditor_Number = row.Auditor_Number,
                                               Auditor_AGM = (bool)row.Auditor_AGM,
                                               Date_AGM = row.Date_AGM,
                                               Date_Appointment = row.Date_Appointment,
                                               IsAuditor_Appointed_casualvacancy = (bool)row.IsAuditor_Appointed_casualvacancy,
                                               SRN_relevent_form = row.SRN_relevent_form,
                                               Person_vacated_office = row.Person_vacated_office,
                                               Membership_Number_or_Registrationfirm = row.Membership_Number_or_Registrationfirm,
                                               Date_vacancy = row.Date_vacancy,
                                               Reasons_casual_vacancy = row.Reasons_casual_vacancy,

                                               AGMNo_TillAppointmentTerminate = row.AGMNo_TillAppointmentTerminate

                                           }).FirstOrDefault();
                            if (auditor != null)
                            {
                                auditor.AuditDeatilsList = (from x in entities.BM_StatutoryAuditor_Mapping
                                                            where x.Statutory_AuditorId == obj.RefMasterID && x.IsActive == true && x.flag == "SA"
                                                            select new VMAuditorDetails

                                                            {

                                                                Auditor_CriteriaId = x.CategoryId,
                                                                Auditor_Id = x.Auditor_ID,
                                                                Id = x.Id,
                                                                AppointedFromdate = x.Appointed_FromDate,
                                                                dueDate = x.Appointed_DueDate,
                                                                AppointedFirstFYDate = x.StartFY,
                                                                AppointedLastFYDate = x.EndFY,
                                                                Number_of_appointedFY = x.Appointed_No_of_FY,

                                                            }).ToList();
                            }
                            #endregion

                            if (auditor != null)
                            {
                                if (auditor.AuditDeatilsList != null)
                                {
                                    var auditor1 = auditor.AuditDeatilsList.FirstOrDefault();
                                    var auditorPersonalDetails = (from row in entities.BM_Auditor_Master
                                                                  from city in entities.Cities.Where(k => k.ID == row.city_Id).DefaultIfEmpty()
                                                                  from state in entities.States.Where(k => k.ID == row.state_Id).DefaultIfEmpty()
                                                                  where row.Id == auditor1.Auditor_Id
                                                                  select new
                                                                  {
                                                                      row.PAN,
                                                                      row.FullName_FirmName,
                                                                      row.Reg_MemberNumber,
                                                                      row.AddressLine1,
                                                                      row.AddressLine2,
                                                                      row.city_Id,
                                                                      cityName = city.Name,
                                                                      row.state_Id,
                                                                      row.Country_Id,
                                                                      row.Pin_Number,
                                                                      row.Email_Id
                                                                  }).FirstOrDefault();

                                    if (auditor.Class_of_Company_sec139 == true)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "YES" });
                                    }
                                    else
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_COMPANY_139_2[0]", Value = "NO" });
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NATURE_APPOINT[0]", Value = "FABD" });

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_JOINT_AUDITOR[0]", Value = "NO" });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].NUM_AUDITORS[0]", Value = "1" });

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AGM[0]", Value = "NO" });
                                    if (obj.MeetingTypeId == 11)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AGM[0]", Value = "YES" });
                                        if (auditor.Date_AGM != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].DATE_OF_AGM[0]", Value = Convert.ToDateTime(auditor.Date_AGM).ToString("yyyy-MM-dd") });
                                        }
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].RB_AUDITOR_APPNT[0]", Value = "NO" });

                                    if (auditor1.Auditor_CriteriaId == 2)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "AFRM" }); //Auditor's Firm
                                    }
                                    else
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_CATEGORY_A[0]", Value = "INDI" }); //Individual Auditor
                                    }

                                    if (auditorPersonalDetails != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PAN_AUDITOR[0]", Value = auditorPersonalDetails.PAN }); //PAN
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NAME_AUDITOR[0]", Value = auditorPersonalDetails.FullName_FirmName });//Name
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].MEMBERSHIP_NUM[0]", Value = auditorPersonalDetails.Reg_MemberNumber });//Membership No
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_I[0]", Value = auditorPersonalDetails.AddressLine1 });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].ADDRESS_LINE_II[0]", Value = auditorPersonalDetails.AddressLine2 });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].CITY[0]", Value = auditorPersonalDetails.cityName });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].STATE[0]", Value = "" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].COUNTRY[0]", Value = "" });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].PIN_CODE[0]", Value = Convert.ToString(auditorPersonalDetails.Pin_Number) });
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].EMAIL_ID_A[0]", Value = auditorPersonalDetails.Email_Id });
                                    }

                                    if (auditor1.AppointedFirstFYDate != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].FROM_DATE[0]", Value = Convert.ToDateTime(auditor1.AppointedFirstFYDate).ToString("yyyy-MM-dd") });
                                    }
                                    if (auditor1.AppointedLastFYDate != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].TO_DATE[0]", Value = Convert.ToDateTime(auditor1.AppointedLastFYDate).ToString("yyyy-MM-dd") });
                                    }

                                    if (auditor.Date_Appointment != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_ADT_1[0].DATE_APPOINTMENT[0]", Value = Convert.ToDateTime(auditor.Date_Appointment).ToString("yyyy-MM-dd") });
                                    }

                                    if (auditor1.Number_of_appointedFY != null)
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].NUM_F_YEARS[0]", Value = Convert.ToString(auditor1.Number_of_appointedFY) });
                                    }

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_ADT_1_S4[0].DATA[0].RB_APPOINTMENT[0]", Value = "YES" });
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else if (obj.EForm == "CRA-2" && obj.RefMasterID > 0)
                    {
                        #region CRA-2
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].CIN_OR_FCRN[0]", Value = obj.CIN_LLPIN });
                        if (obj.UIFormID == SecretarialConst.UIForms.APPOINTMENT_OF_COST_AUDITOR)
                        {
                            #region Cost Auditor Master Data
                            var auditor = (from row in entities.BM_StatutoryAuditor
                                           join mapping in entities.BM_StatutoryAuditor_Mapping on row.Id equals mapping.Statutory_AuditorId
                                           where row.Id == obj.RefMasterID
                                           && row.Flag == "CA"
                                           select new VM_CostAuditor
                                           {
                                               AuditorMappingId = mapping.Id,
                                               Id = row.Id,
                                               EntityId = row.EntityId,
                                               AuditorId = mapping.Auditor_ID,
                                               categoryId = mapping.CategoryId,
                                               Nature_of_intimation_cost = row.Nature_of_Intimatecost,
                                               Cost_auditor_firm_Original = row.CoustAuditor_FirmOrigine,
                                               dateofBordMeeting = row.BoardMeeting_date,
                                               ResulationNumber = row.Resulation_Number
                                           }).FirstOrDefault();

                            if (auditor != null)
                            {
                                var auditorPersonalDetails = (from row in entities.BM_Auditor_Master
                                                              from city in entities.Cities.Where(k => k.ID == row.city_Id).DefaultIfEmpty()
                                                              from state in entities.States.Where(k => k.ID == row.state_Id).DefaultIfEmpty()
                                                              where row.Id == auditor.AuditorId
                                                              select new
                                                              {
                                                                  row.PAN,
                                                                  row.FullName_FirmName,
                                                                  row.Reg_MemberNumber,
                                                                  row.AddressLine1,
                                                                  row.AddressLine2,
                                                                  row.city_Id,
                                                                  cityName = city.Name,
                                                                  row.state_Id,
                                                                  row.Country_Id,
                                                                  row.Pin_Number,
                                                                  row.Email_Id
                                                              }).FirstOrDefault();

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].NATURE_INTIMATN[0]", Value = "ORGN" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].NUM_COST_AUDITOR[0]", Value = "1" });

                                if (auditor.categoryId == 1)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].RB_CATEGORY_ADTR[0]", Value = "INDVDL" }); //Individual Auditor
                                }
                                else
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].RB_CATEGORY_ADTR[0]", Value = "PRTNR" }); //Partner Firm
                                }

                                if (auditorPersonalDetails != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].MEMBERSHP_NO_COA[0]", Value = auditorPersonalDetails.Reg_MemberNumber });//Membership No
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].NAME_COST_AUDITR[0]", Value = auditorPersonalDetails.FullName_FirmName });//Name
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].FIRM_REGSTRN_NUM[0]", Value = auditor.Cost_auditor_firm_Original }); // Cost_auditor_firm_Original
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].NAME_COSTA_FIRM[0]", Value = "" }); // Name of LLP firm
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].ADDRESS_LINE_1[0]", Value = auditorPersonalDetails.AddressLine1 });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].ADDRESS_LINE_2[0]", Value = auditorPersonalDetails.AddressLine2 });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].CITY[0]", Value = auditorPersonalDetails.cityName });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].STATE[0]", Value = "" });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].COUNTRY[0]", Value = "" });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].PIN_CODE[0]", Value = Convert.ToString(auditorPersonalDetails.Pin_Number) });
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].EMAIL_ID[0]", Value = auditorPersonalDetails.Email_Id });
                                }

                                if (obj.MeetingDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].DATE_BOARD_MEETN[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                }
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZMCA_NCA_CRA2_S4[0].DATA[0].TYPE_APPOINTMENT[0]", Value = "1" }); // Original

                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].F_YEAR_FROM_DATE[0]", Value = "1" });
                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].F_YEAR_END_DATE[0]", Value = "1" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_CRA_2[0].RB_CHANGE_COST_A[0]", Value = "NO" });
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else if (obj.EForm == "AOC-5" && obj.RefMasterID > 0)
                    {
                        #region AOC-5
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_5[0].CIN[0]", Value = obj.CIN_LLPIN });
                        if (obj.UIFormID == SecretarialConst.UIForms.KEEPING_OF_BOOKS_OF_ACCOUNTS)
                        {
                            var booksDetails = (from row in entities.BM_EntityMasterBooksOfAccounts
                                                where row.Id == obj.RefMasterID
                                                select new
                                                {
                                                    row.AddressLine1,
                                                    row.AddressLine2,
                                                    row.CityName,
                                                    row.StateId,
                                                    row.CountyId,
                                                    row.Pincode
                                                }).FirstOrDefault();

                            if (booksDetails != null)
                            {
                                if (obj.MeetingDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_5[0].DATE_BOARD_RES[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                                }
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_5[0].ADDRESS_LINE_1[0]", Value = booksDetails.AddressLine1 });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_5[0].ADDRESS_LINE_2[0]", Value = booksDetails.AddressLine2 });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_5[0].CITY[0]", Value = booksDetails.CityName });
                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_5[0].PS_ADDRESS_STATE[0]", Value = booksDetails.CityName });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_5[0].COUNTRY[0]", Value = "IN" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_5[0].PINCODE[0]", Value = booksDetails.Pincode });
                            }
                        }
                        #endregion
                    }
                    else if (obj.EForm == "INC-22" && obj.RefMasterID > 0)
                    {
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].CIN[0]", Value = obj.CIN_LLPIN });
                        #region Within Local Limit
                        if (obj.UIFormID == SecretarialConst.UIForms.SHIFTING_RO_WITHIN_LOCAL_LIMIT)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].PURPOSE_FORM[0]", Value = "1" });

                            var objShiftingDetails = (from row in entities.BM_EntityShiftingRO_History
                                                      from state in entities.States.Where(k => k.ID == row.Regi_StateIdNew).DefaultIfEmpty()
                                                      from city in entities.Cities.Where(k => k.ID == row.Regi_CityIdNew).DefaultIfEmpty()
                                                      where row.Id == obj.RefMasterID
                                                      select new
                                                      {
                                                          row.EffectFrom,
                                                          row.Regi_Address_Line1New,
                                                          row.Regi_Address_Line2New,
                                                          CityName = city.Name,
                                                          StateName = state.Name,
                                                          row.Regi_PINCodeNew,
                                                      }).FirstOrDefault();

                            if (objShiftingDetails != null)
                            {
                                if (objShiftingDetails.EffectFrom != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].DATE2[0]", Value = Convert.ToDateTime(objShiftingDetails.EffectFrom).ToString("yyyy-MM-dd") });
                                }

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].STREET[0]", Value = objShiftingDetails.Regi_Address_Line1New });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].HOUSE_NUM[0]", Value = objShiftingDetails.Regi_Address_Line2New });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].CITY[0]", Value = objShiftingDetails.CityName });
                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].STATE[0]", Value = booksDetails.CityName });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].COUNTRY[0]", Value = "INDIA" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].PIN_CODE[0]", Value = objShiftingDetails.Regi_PINCodeNew });

                                #region Police Station details
                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].STREET1[0]", Value = objShiftingDetails.Regi_Address_Line1New });
                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].HOUSE_NUM1[0]", Value = objShiftingDetails.Regi_Address_Line2New });
                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].CITY1[0]", Value = objShiftingDetails.CityName });
                                ////lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].STATE[0]", Value = booksDetails.CityName });
                                //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].PIN_CODE1[0]", Value = objShiftingDetails.Regi_PINCodeNew });
                                #endregion

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].REGIS_OFFICE[0]", Value = "2" });
                                
                                if (obj.ItemNo > 0)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].RESOLUTION_NO[0]", Value = Convert.ToString(obj.ItemNo) });
                                }
                            }

                            if (obj.MeetingDate != null)
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].DATE1[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                            }
                        }

                        #endregion
                    }
                    else if (obj.EForm == "SH-7" && obj.RefMasterID > 0)
                    {
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].CIN[0]", Value = obj.CIN_LLPIN });
                        #region Increased by Company
                        if (obj.UIFormID == SecretarialConst.UIForms.INCREASE_IN_AUTHORISED_CAPITAL_GM)
                        {
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].FORM_PURPOSE[0]", Value = "ISCI" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].RESOLUTION[0]", Value = "ODRS" });
                            if (obj.MeetingDate != null)
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].DATE1[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                            }

                            var objAuthorizedCapital = (from row in entities.BM_CapitalMaster_RevisedHistory
                                                      where row.IdRevised == obj.RefMasterID
                                                      select new
                                                      {
                                                          AuthorizedCapval = row.AuthorizedCapital,
                                                          AuthorisedId = (long)row.CapitalId,
                                                          Entity_Id = row.Entity_Id,

                                                          TnAuthorisedCapital = row.TotNoAuthorizedCapita,
                                                          TaAuthorisedCapital = row.TotamtAuthorizedCapita,

                                                          TnAuthorisedCapitalRevised = row.TotNoAuthorizedCapitaRevised,
                                                          TaAuthorisedCapitalRevised = row.TotamtAuthorizedCapitaRevised,

                                                          //Pre
                                                          pTnAuthorisedCapital = row.Pri_TotNoAuthorizedCapita,
                                                          pTaAuthorisedCapital = row.Pri_TotamtAuthorizedCapita,

                                                          IsPrefrence = row.IsPrefrence,

                                                          pTnAuthorisedCapitalRevised = row.Pri_TotNoAuthorizedCapitaRevised,
                                                          pTaAuthorisedCapitalRevised = row.Pri_TotamtAuthorizedCapitaRevised,
                                                          //End

                                                          //EQ Existing issued, subscribed, paidup
                                                          TotNoIssuedCapital = row.TotNoIssuedCapital,
                                                          TotamtNoIssuedCapital = row.TotamtNoIssuedCapital,

                                                          TotNoSubscribCapital = row.TotNoSubscribCapital,
                                                          TotamtNoSubscribCapital = row.TotamtNoSubscribCapital,

                                                          TotNoPaidupCapital = row.TotNoPaidupCapital,
                                                          TotamtNoPaidupCapital = row.TotamtNoPaidupCapital,
                                                          //End

                                                          //PRE Existing issued, subscribed, paidup
                                                          Pri_TotNoIssuedCapital = row.Pri_TotNoIssuedCapital,
                                                          Pri_TotamtNoIssuedCapital = row.Pri_TotamtNoIssuedCapital,

                                                          Pri_TotNoSubscribCapital = row.Pri_TotNoSubscribCapital,
                                                          Pri_TotamtNoSubscribCapital = row.Pri_TotamtNoSubscribCapital,

                                                          Pri_TotNoPaidupCapital = row.Pri_TotNoPaidupCapital,
                                                          Pri_TotamtNoPaidupCapital = row.Pri_TotamtNoPaidupCapital,
                                                          //End
                                                          AuthorisedIdRevised = row.IdRevised
                                                      }).FirstOrDefault();

                            if (objAuthorizedCapital != null)
                            {

                                var additionEQNo = (objAuthorizedCapital.TnAuthorisedCapitalRevised - objAuthorizedCapital.TnAuthorisedCapital);
                                var additionEQAmout = (objAuthorizedCapital.TaAuthorisedCapitalRevised - objAuthorizedCapital.TaAuthorisedCapital);

                                var additionPRENo = (objAuthorizedCapital.pTnAuthorisedCapitalRevised - objAuthorizedCapital.pTnAuthorisedCapital);
                                var additionPREAmout = (objAuthorizedCapital.pTaAuthorisedCapitalRevised - objAuthorizedCapital.pTaAuthorisedCapital);

                                var authorizedCapvalDiffernce = additionEQAmout + additionPREAmout;
                                var authorizedCapvalRevised = objAuthorizedCapital.AuthorizedCapval + authorizedCapvalDiffernce;

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].INC_REVISED[0]", Value = Convert.ToString(authorizedCapvalRevised) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].INC_DIFF[0]", Value = Convert.ToString(authorizedCapvalDiffernce) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_EQUITY[0]", Value = Convert.ToString(additionEQNo) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_EQUITY[0]", Value = Convert.ToString(additionEQAmout) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_PREFERENCE[0]", Value = Convert.ToString(additionPRENo) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_PREFRENCE[0]", Value = Convert.ToString(additionPREAmout) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].TOTAL_SHARES[0]", Value = Convert.ToString(authorizedCapvalDiffernce) });

                                #region Nominal value per Share
                                var FaceValueOfEquityShare = (from row in entities.BM_Share_RevisedHistory
                                                              where row.CapitalMasterIdRevised == obj.RefMasterID
                                                              && row.IsActive == true && row.Sharetype == "E"
                                                              && row.Nominalper_AuhorisedCapitalRevised > 0
                                                              select row.Nominalper_AuhorisedCapitalRevised).FirstOrDefault();

                                var FaceValueOfPreferenceShare = (from row in entities.BM_Share_RevisedHistory
                                                                  where row.CapitalMasterIdRevised == obj.RefMasterID
                                                                  && row.IsActive == true && row.Sharetype == "P"
                                                                  && row.Nominalper_AuhorisedCapitalRevised > 0
                                                                  select row.Nominalper_AuhorisedCapitalRevised).FirstOrDefault();
                                #endregion

                                #region Revised Capital structure
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AUTH_CAPI_COMP[0]", Value = Convert.ToString(authorizedCapvalRevised) });

                                //Authorised
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_EQUITY_SHARE[0]", Value = Convert.ToString(objAuthorizedCapital.TnAuthorisedCapitalRevised) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_EQUITY1[0]", Value = Convert.ToString(objAuthorizedCapital.TaAuthorisedCapitalRevised) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NOMINAL_AMOUNT[0]", Value = Convert.ToString(FaceValueOfEquityShare) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_PREF_SHARES1[0]", Value = Convert.ToString(objAuthorizedCapital.pTnAuthorisedCapitalRevised) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_PREF[0]", Value = Convert.ToString(objAuthorizedCapital.pTaAuthorisedCapitalRevised) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NOMINAL_AMT_PREF[0]", Value = Convert.ToString(FaceValueOfPreferenceShare) });

                                //End

                                //Issued
                                var TotAmtIssuedCapitalRevised = objAuthorizedCapital.TotamtNoIssuedCapital + objAuthorizedCapital.Pri_TotamtNoIssuedCapital;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].CAPITAL_COMP[0]", Value = Convert.ToString(TotAmtIssuedCapitalRevised) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_EQ_SHARES[0]", Value = Convert.ToString(objAuthorizedCapital.TotNoIssuedCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_EQUITY2[0]", Value = Convert.ToString(objAuthorizedCapital.TotamtNoIssuedCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NOMINAL_AMOUNT1[0]", Value = Convert.ToString(FaceValueOfEquityShare) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_PREF_SHARES2[0]", Value = Convert.ToString(objAuthorizedCapital.Pri_TotNoIssuedCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_PREF1[0]", Value = Convert.ToString(objAuthorizedCapital.Pri_TotamtNoIssuedCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NOMINL_AMT_PREF1[0]", Value = Convert.ToString(FaceValueOfPreferenceShare) });
                                //End

                                //Subscribed
                                var TotamtNoSubscribCapital = objAuthorizedCapital.TotamtNoSubscribCapital + objAuthorizedCapital.Pri_TotamtNoSubscribCapital;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].SUBSCRIBD_CAPTL[0]", Value = Convert.ToString(TotamtNoSubscribCapital) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_EQ_SHARES1[0]", Value = Convert.ToString(objAuthorizedCapital.TotNoSubscribCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_EQUITY3[0]", Value = Convert.ToString(objAuthorizedCapital.TotamtNoSubscribCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NOMINAL_AMOUNT2[0]", Value = Convert.ToString(FaceValueOfEquityShare) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_PREF_SHARES3[0]", Value = Convert.ToString(objAuthorizedCapital.Pri_TotNoSubscribCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_PREF2[0]", Value = Convert.ToString(objAuthorizedCapital.Pri_TotamtNoSubscribCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NOMINL_AMT_PREF2[0]", Value = Convert.ToString(FaceValueOfPreferenceShare) });
                                //End

                                //Paidup
                                var TotamtNoPaidupCapital = objAuthorizedCapital.TotamtNoPaidupCapital + objAuthorizedCapital.Pri_TotamtNoPaidupCapital;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].PAID_CAPITAL[0]", Value = Convert.ToString(TotamtNoPaidupCapital) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_EQ_SHARES2[0]", Value = Convert.ToString(objAuthorizedCapital.TotNoPaidupCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_EQUITY4[0]", Value = Convert.ToString(objAuthorizedCapital.TotamtNoPaidupCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NOMINAL_AMOUNT3[0]", Value = Convert.ToString(FaceValueOfEquityShare) });

                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NUM_PREF_SHARES4[0]", Value = Convert.ToString(objAuthorizedCapital.Pri_TotNoPaidupCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].AMOUNT_PREF3[0]", Value = Convert.ToString(objAuthorizedCapital.Pri_TotamtNoPaidupCapital) });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_SH_7[0].NOMINL_AMT_PREF3[0]", Value = Convert.ToString(FaceValueOfPreferenceShare) });
                                //End

                                //if (obj.ItemNo > 0)
                                //{
                                //    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].RESOLUTION_NO[0]", Value = Convert.ToString(obj.ItemNo) });
                                //}
                                #endregion

                            }

                            //if (obj.MeetingDate != null)
                            //{
                            //    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_INC_22[0].DATE1[0]", Value = Convert.ToDateTime(obj.MeetingDate).ToString("yyyy-MM-dd") });
                            //}
                        }

                        #endregion
                    }

                    if (lstFormsVM.Count > 1 || obj.EForm == "INC-22")
                    {
                        result.FullFileName = (from row in entities.BM_SP_FormPath(obj.EForm, index, indexDirector, (int?)obj.UIFormID)
                                               select row).FirstOrDefault();
                    }
                    else
                    {
                        result.FullFileName = obj.FormPath;
                    }

                    if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                    {
                        SetData(result, lstFields);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        private void SetData(EFormVM obj, List<EFormFieldVM> lstFields)
        {
            try
            {
                obj.FormData = null;

                var form = System.Web.Hosting.HostingEnvironment.MapPath(obj.FullFileName);
                PdfReader pdfReader = new PdfReader(form);

                using (MemoryStream ms = new MemoryStream())
                {
                    PdfStamper stamper = new PdfStamper(pdfReader, ms, '\0', true);

                    AcroFields pdfFormFields = stamper.AcroFields;

                    foreach (var item in lstFields)
                    {
                        pdfFormFields.SetField(item.Name, item.Value);
                    }
                    stamper.Close();
                    obj.FormData = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SetDataFileStream(EFormVM obj, List<EFormFieldVM> lstFields)
        {
            try
            {
                obj.FormData = null;

                var form = System.Web.Hosting.HostingEnvironment.MapPath(obj.FullFileName);
                var formNew = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/BM_Management/Documents/Forms/Form_DIR-12_Amol_Dere.pdf");
                PdfReader pdfReader = new PdfReader(form);

                PdfStamper stamper = new PdfStamper(pdfReader, new FileStream(formNew, FileMode.Create), pdfReader.PdfVersion, true);
                AcroFields pdfFormFields = stamper.AcroFields;

                //foreach (var item in lstFields)
                //{
                //    pdfFormFields.SetField(item.Name, item.Value);
                //}
                stamper.AcroFields.SetField("data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", "L74140KA2000PLC118395");
                stamper.AcroFields.SetField("data[0].ZMCA_NCA_DIR_12[0].NUM_M_S_CFO_CEO[0]", "1");
                stamper.AcroFields.SetField("data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].PAN[0]", "ASNPP3939M");
                stamper.AcroFields.SetField("data[0].T_ZMCA_NCA_DIR12_S[0].DATA[0].RB_A_C[0]", "APPN");
                stamper.FormFlattening = false;
                stamper.Close();


                //
                var formNew1 = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/BM_Management/Documents/Forms/Form_DIR-12_Amol_Dere1.pdf");
                pdfReader = new PdfReader(form);
                PdfStamper stamper1 = new PdfStamper(pdfReader, new FileStream(formNew1, FileMode.Create), pdfReader.PdfVersion, true);

                //foreach (var item in lstFields)
                //{
                //    pdfFormFields.SetField(item.Name, item.Value);
                //}
                stamper1.AcroFields.SetField("data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", "L74140KA2000PLC118395");
                stamper1.Close();
                //


                //
                var formNew2 = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/BM_Management/Documents/Forms/Form_DIR-12_Amol_Dere2.pdf");
                pdfReader = new PdfReader(form);
                PdfStamper stamper2 = new PdfStamper(pdfReader, new FileStream(formNew2, FileMode.Create), pdfReader.PdfVersion, false);

                //foreach (var item in lstFields)
                //{
                //    pdfFormFields.SetField(item.Name, item.Value);
                //}
                stamper2.AcroFields.SetField("data[0].ZMCA_NCA_DIR_12[0].SRN_2_1_OR_CIN[0]", "L74140KA2000PLC118395");
                stamper2.Close();
                //

                using (FileStream fs = File.OpenRead(formNew))
                {
                    int length = (int)fs.Length;
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        obj.FormData = br.ReadBytes(length);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Generate E-Forms (Director)
        public EFormVM GenerateFormsforDirector(long DirectorID)
        {
            string city = string.Empty;
            var result = new EFormVM() { DirectorID = DirectorID };
            var lstFields = new List<EFormFieldVM>();
            var lstFormsVM = new List<MultipleFormsVM>();

            try
            {
                var obj = (from dir in entities.BM_DirectorMaster
                           join rows in entities.BM_DirectorTypeOfChanges
                           on dir.Id equals rows.DirectorId
                           //join row in entities.BM_DirectorTypeChangesMapping
                           //on rows.DirectorId equals row.DirectorId
                           where dir.Id == DirectorID
                           && rows.IsActive == true
                           // && row.IsActive==true
                           && rows.IsApproved == true
                           select new DirectorDir3anddir6VM
                           {
                               dirId = dir.Id,
                               FirstName = dir.FirstName,
                               MiddleName = dir.MiddleName,
                               LastName = dir.LastName,
                               din = dir.DIN,
                               PAN = dir.PAN,
                               FatherFirstName = dir.Father,
                               FatherMiddleName = dir.FatherMiddleName,
                               FatherLastName = dir.FatherLastName,
                               Nationality = dir.Nationality,
                               DOB = dir.DOB,
                               Gender = dir.Gender,
                               EmailId_Official = dir.EmailId_Official,
                               EmailId_Personal = dir.EmailId_Personal,
                               Permenant_Address_Line1 = dir.Permenant_Address_Line1,
                               Permenant_Address_Line2 = dir.Permenant_Address_Line2,
                               Present_Address_Line1 = dir.Present_Address_Line1,
                               Present_Address_Line2 = dir.Present_Address_Line2,
                               IsName_of_Director = rows.Name_of_Director,
                               IsFatherName = rows.FatherName,
                               IsMobile = rows.Mobile,
                               IsEmailID = rows.EmailID,
                               IsGender = rows.Gender,
                               IsNationality = rows.Nationality,
                               IsPAN = rows.PAN,
                               IsPassport = rows.Passport,
                               IsParmanent_Address = rows.Parmanent_Address,
                               IsPresent_Address = rows.Present_Address,
                               IsAadharNumber = rows.AadharNumber,
                               IsDateofBirth = rows.DateofBirth,

                           }).FirstOrDefault();

                //result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR-12.pdf";
                //result.FormName = "DIR-12.pdf";

                if (obj != null)
                {
                    #region Dir-6

                    result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR-6.pdf";
                    result.FormName = "DIR6.pdf";

                    if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                    {
                        #region DIR-6

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZMCA_NCA_DIR_6[0].DIN[0]", Value = obj.din });
                        if (obj.IsName_of_Director)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB1_DIRECTOR[0]", Value = "APPN" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB1_DIRECTOR[0]", Value = "" });
                        }
                        if (obj.IsFatherName)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB2_FATHER_NAME[0]", Value = "FATH" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB2_FATHER_NAME[0]", Value = "" });
                        }
                        if (obj.IsNationality)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB3_NATIONALITY[0]", Value = "NATY" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB3_NATIONALITY[0]", Value = "" });
                        }
                        if (obj.IsDateofBirth)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB4_DATE_OF_BIRT[0]", Value = "DDOB" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB4_DATE_OF_BIRT[0]", Value = "" });
                        }
                        if (obj.IsGender)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB5_GENDER[0]", Value = "GEND" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB5_GENDER[0]", Value = "" });
                        }
                        if (obj.IsPAN)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB6_IT_PAN[0]", Value = "IPAN" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB6_IT_PAN[0]", Value = "" });
                        }
                        if (obj.IsPassport)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB8_PASSPORT_NUM[0]", Value = "PASS" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB8_PASSPORT_NUM[0]", Value = "" });
                        }
                        if (obj.IsAadharNumber)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB15_AADHAR_NUM[0]", Value = "AADH" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB15_AADHAR_NUM[0]", Value = "" });
                        }
                        if (obj.IsParmanent_Address)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB11_PERM_ADD[0]", Value = "PMRA" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB11_PERM_ADD[0]", Value = "" });
                        }
                        if (obj.IsPresent_Address)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB12_PRESNT_ADD[0]", Value = "PRRA" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB12_PRESNT_ADD[0]", Value = "" });
                        }
                        if (obj.IsRasidential_Status)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB14_RESDN_STATU[0]", Value = "WRII" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB14_RESDN_STATU[0]", Value = "" });
                        }
                        if (obj.IsPhotoGraph_of_Director)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB13_PHOTO_DIR[0]", Value = "PICS" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZMCA_NCA_DIR_6[0].CB13_PHOTO_DIR[0]", Value = "" });
                        }
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZMCA_NCA_DIR_6[0].CB7_VOTER_ID[0]", Value = "" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZMCA_NCA_DIR_6[0].CB9_DRIVN_LIC_NO[0]", Value = "" });
                        if (obj.IsName_of_Director)
                        {
                            if (obj.FirstName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].FIRST_NAME[0]", Value = obj.FirstName });
                            }
                            if (obj.LastName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].LAST_NAME[0]", Value = obj.LastName });
                            }
                            if (obj.MiddleName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].MIDDLE_NAME[0]", Value = obj.MiddleName });
                            }
                        }
                        if (obj.IsFatherName)
                        {
                            if (obj.FatherFirstName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].FATH_FIRST_NAME[0]", Value = obj.FatherFirstName });
                            }
                            if (obj.FatherLastName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].FATH_LAST_NAME[0]", Value = obj.FatherLastName });
                            }
                            if (obj.FatherMiddleName != null)
                            {
                                lstFields.Add(new EFormFieldVM()
                                { Name = "data[0].ZMCA_NCA_DIR_6[0].FATH_MIDDLE_NAME[0]", Value = obj.FatherMiddleName });
                            }
                        }
                        if (obj.IsDateofBirth)
                        {
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].DATE_OF_BIRTH[0]",
                                Value = Convert.ToDateTime(obj.DOB).ToString("yyyy-MM-dd")

                            });
                        }


                        if (obj.IsPAN)
                        {
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].IT_PAN[0]",
                                Value = obj.PAN

                            });
                        }

                        if (obj.IsAadharNumber)
                        {
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].AADHAR_NUMBER[0]",
                                Value = obj.Aadhaar

                            });
                        }
                        if (obj.IsNationality)
                        {
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].NATIONALITY[0]",
                                Value = "IN"

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].RB_CITIZEN_IND[0]",
                                Value = "YES"

                            });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].RB_CITIZEN_IND[0]",
                                Value = "NO"

                            });
                        }
                        if (obj.IsRasidential_Status)
                        {
                            if (obj.ResidentInIndia == true)
                            {
                                lstFields.Add(new EFormFieldVM()
                                {
                                    Name = "data[0].ZMCA_NCA_DIR_6[0].RB_RESIDENT_IND[0]",
                                    Value = "YES"

                                });
                            }
                            else
                            {
                                lstFields.Add(new EFormFieldVM()
                                {
                                    Name = "data[0].ZMCA_NCA_DIR_6[0].RB_RESIDENT_IND[0]",
                                    Value = "NO"

                                });
                            }

                        }

                        if (obj.IsGender)
                        {

                            if (obj.Gender == 2)
                            {
                                lstFields.Add(new EFormFieldVM()
                                {
                                    Name = "data[0].ZMCA_NCA_DIR_6[0].RB_GENDER[0]",
                                    Value = "FEML"
                                });
                            }
                            else if (obj.Gender == 1)
                            {
                                lstFields.Add(new EFormFieldVM()
                                {
                                    Name = "data[0].ZMCA_NCA_DIR_6[0].RB_GENDER[0]",
                                    Value = "MALE"
                                });
                            }
                        }

                        if (obj.IsPresent_Address)
                        {
                            city = GetcityId(obj.Present_CityId);
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_ADD_LINE1[0]",
                                Value = obj.Present_Address_Line1

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_ADD_LINE2[0]",
                                Value = obj.Present_Address_Line2

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_ADD_LINE2[0]",
                                Value = obj.Present_Address_Line2

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_CITY[0]",
                                Value = city

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_PINCODE[0]",
                                Value = obj.Present_PINCode

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_COUNTRY_COD[0]",
                                Value = "IN"

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_PHONE[0]",
                                Value = obj.MobileNo

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PERM_COUNTRY[0]",
                                Value = "INDIA"

                            });
                        }

                        if (obj.IsParmanent_Address)
                        {
                            city = GetcityId(obj.Permenant_CityId);
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_ADD_LINE1[0]",
                                Value = obj.Permenant_Address_Line1

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_ADD_LINE2[0]",
                                Value = obj.Permenant_Address_Line2

                            });

                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_CITY[0]",
                                Value = city

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_PINCODE[0]",
                                Value = obj.Present_PINCode

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_COUNTRY_COD[0]",
                                Value = "IN"

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_PHONE[0]",
                                Value = obj.MobileNo

                            });
                            lstFields.Add(new EFormFieldVM()
                            {
                                Name = "data[0].ZMCA_NCA_DIR_6[0].PRES_COUNTRY[0]",
                                Value = "INDIA"

                            });
                        }

                        #endregion DIR-6
                    }
                    #endregion

                    SetData(result, lstFields);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public string GetcityId(int? cityId)
        {
            string city = "";
            try
            {

                city = (from row in entities.Cities where row.ID == cityId select row.Name).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return city;
        }
        public EFormVM GenerateFormforDirectorDIR3(long DirectorID)
        {
            var result = new EFormVM() { DirectorID = DirectorID };
            var lstFields = new List<EFormFieldVM>();
            var lstFormsVM = new List<MultipleFormsVM>();
            string Gender = string.Empty;
            try
            {
                var obj = (from dir in entities.BM_DirectorMaster
                           join rows in entities.BM_DirectorTypeOfChanges
                           on dir.Id equals rows.DirectorId
                           join row in entities.BM_DirectorTypeChangesMapping
                           on rows.DirectorId equals row.DirectorId
                           where dir.Id == DirectorID
                             && rows.IsActive == true
                           && row.IsActive == true
                           && rows.IsApproved == true
                           select new DirectorDir3anddir6VM
                           {
                               dirId = dir.Id,
                               FirstName = dir.FirstName,
                               MiddleName = dir.MiddleName,
                               LastName = dir.LastName,
                               din = dir.DIN,
                               PAN = dir.PAN,
                               FatherFirstName = dir.Father,
                               FatherMiddleName = dir.FatherMiddleName,
                               FatherLastName = dir.FatherLastName,
                               Nationality = dir.Nationality,
                               DOB = dir.DOB,
                               Gender = dir.Gender,
                               EmailId_Official = dir.EmailId_Official,
                               EmailId_Personal = dir.EmailId_Personal,
                               Permenant_Address_Line1 = dir.Permenant_Address_Line1,
                               Permenant_Address_Line2 = dir.Permenant_Address_Line2,
                               Present_Address_Line1 = dir.Present_Address_Line1,
                               Present_Address_Line2 = dir.Present_Address_Line2,
                               IsName_of_Director = rows.Name_of_Director,
                               IsFatherName = rows.FatherName,
                               IsMobile = rows.Mobile,
                               IsEmailID = rows.EmailID,
                               IsGender = rows.Gender,
                               IsNationality = rows.Nationality,
                               IsPAN = rows.PAN,
                               IsPassport = rows.Passport,
                               IsParmanent_Address = rows.Parmanent_Address,
                               IsPresent_Address = rows.Present_Address,
                               IsAadharNumber = rows.AadharNumber,
                               IsDateofBirth = rows.DateofBirth,

                           }).FirstOrDefault();


                if (obj != null)
                {


                    result.FullFileName = "~/Areas/BM_Management/Documents/Forms/Form_DIR3-KYC.pdf";
                    result.FormName = "Form_DIR-3_KYC.pdf";


                    if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                    {


                        #region DIR-3 KYC

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].DIN[0]", Value = obj.din });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].FIRST_NAME[0]", Value = obj.FirstName });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].LAST_NAME[0]", Value = obj.LastName });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].MIDDLE_NAME[0]", Value = obj.MiddleName });

                        if (obj.FatherFirstName != null)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZNCA_DIR3_KYC[0].FATH_FIRST_NAME[0]", Value = obj.FatherFirstName });
                        }
                        if (obj.FatherLastName != null)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZNCA_DIR3_KYC[0].FATH_LAST_NAME[0]", Value = obj.FatherLastName });
                        }
                        if (obj.FatherMiddleName != null)
                        {
                            lstFields.Add(new EFormFieldVM()
                            { Name = "data[0].ZNCA_DIR3_KYC[0].FATH_MIDDLE_NAME[0]", Value = obj.FatherMiddleName });
                        }
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_CITIZEN_INDIA[0]", Value = "YES" });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].NATIONALITY[0]", Value = "IN" });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_RESIDENT_IND[0]", Value = "YES" });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].DATE_OF_BIRTH[0]", Value = Convert.ToDateTime(obj.DOB).ToString("yyyy-MM-dd") });
                        if (obj.Gender == 1)
                        {
                            Gender = "MALE";
                        }
                        else if (obj.Gender == 2)
                        {
                            Gender = "FEMALE";
                        }
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_GENDER[0]", Value = Gender });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].IT_PAN[0]", Value = obj.PAN });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_AADHAR_NUMBER[0]", Value = "Yes" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].AADHAR_NUMBER[0]", Value = obj.Aadhaar });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERSONAL_MOB_NUM[0]", Value = obj.MobileNo });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERSONAL_EMAILID[0]", Value = obj.EmailId_Personal });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERSONAL_EMAILID[0]", Value = obj.EmailId_Personal });


                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_LINE1[0]", Value = obj.Permenant_Address_Line1 });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_LINE2[0]", Value = obj.Permenant_Address_Line2 });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_CITY[0]", Value = "" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_PINCODE[0]", Value = "" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERM_COUNTRY_COD[0]", Value = "IN" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_COUNTRY[0]", Value = "INDIA" });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PERM_ADD_PINCODE[0]", Value = obj.Permenant_PINCode });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].RB_PRES_PERM_ADD[0]", Value = "YES" });

                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_LINE1[0]", Value = obj.Present_Address_Line1 });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_LINE2[0]", Value = obj.Present_Address_Line2 });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_CITY[0]", Value = "" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_PINCODE[0]", Value = "" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PRES_COUNTRY_COD[0]", Value = "IN" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_COUNTRY[0]", Value = "INDIA" });
                        lstFields.Add(new EFormFieldVM()
                        { Name = "data[0].ZNCA_DIR3_KYC[0].PRES_ADD_PINCODE[0]", Value = obj.Present_PINCode });
                    }
                    #endregion DIR-3 KYC

                    SetData(result, lstFields);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Generate E-Form DPT-3
        public EFormVM GenerateDPT3(DPT3EFormVM obj, int entityId)
        {
            var result = new EFormVM();
            var form = (from row in entities.BM_FormMaster
                        where row.EForm == "DPT-3" && row.IsDeleted == false
                        select new
                        {
                            row.EForm,
                            row.FormPath
                        }).FirstOrDefault();
            if (form != null)
            {
                result.FormName = form.EForm + ".pdf";
                result.FullFileName = form.FormPath;
            }

            var lstFields = new List<EFormFieldVM>();
            try
            {
                var entityDetails = (from entity in entities.BM_EntityMaster
                                     where entity.Id == entityId
                                     select new
                                     {
                                         entity.CompanyName,
                                         entity.CIN_LLPIN,
                                         entity.Entity_Type,
                                         entity.Email_Id
                                     }).FirstOrDefault();

                if (entityDetails != null)
                {
                    if (form != null)
                    {
                        var fileNameTemp = string.IsNullOrEmpty(entityDetails.CompanyName) ? "" : entityDetails.CompanyName;
                        result.FormName = form.EForm + "_" + fileNameTemp + ".pdf";
                    }
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].CIN[0]", Value = entityDetails.CIN_LLPIN });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].EMAIL_ID[0]", Value = entityDetails.Email_Id });

                    //if(entityDetails.Entity_Type == SecretarialConst.EntityTypeID.LISTED || entityDetails.Entity_Type == SecretarialConst.EntityTypeID.PUBLIC)
                    //{
                    //    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].RB_PUBC_PRIV_COM[0]", Value = "PRIV" });
                    //}
                    //else
                    //{
                    //    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].RB_PUBC_PRIV_COM[0]", Value = "PRIV" });
                    //}
                }


                if (obj != null)
                {

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].PURPOSE_OF_FORM[0]", Value = "PTC" });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].RB_GOVT_COMP[0]", Value = "NO" });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].PAID_UP_SHARE_CA[0]", Value = obj._08_NetWorth.PaidUpShareCapital });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].FREE_RESERVES[0]", Value = obj._08_NetWorth.FreeReserves });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].SECURITIES_P_ACC[0]", Value = obj._08_NetWorth.SecuritiesPremium });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ACCUMULATED_LOSS[0]", Value = obj._08_NetWorth.BalanceOfDeferred });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].BALANCE_DEFER_RE[0]", Value = obj._08_NetWorth.AccumulatedLoss });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ACCUMULATED_UD[0]", Value = obj._08_NetWorth.AccumulatedUnprovidedDep });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].MISCELLANEOUS_E[0]", Value = obj._08_NetWorth.MiscellaneousExpense });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].OTHER_INTANGIBLE[0]", Value = obj._08_NetWorth.OtherIntangible });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].NET_WORTH[0]", Value = obj._08_NetWorth.NetWorth });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC1_I[0]", Value = obj._15_A_ReceiptOfMoney.CentralGovernment });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC1_II[0]", Value = obj._15_A_ReceiptOfMoney.stateGovernment });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC1_III[0]", Value = obj._15_A_ReceiptOfMoney.LocalAuthority });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC1_IV[0]", Value = obj._15_A_ReceiptOfMoney.StatutoryAuthority });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_I[0]", Value = obj._15_B_AmountReceivedFrom.ForeignGovernments });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_II[0]", Value = obj._15_B_AmountReceivedFrom.ForeignInternationalBanks });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_III[0]", Value = obj._15_B_AmountReceivedFrom.MultilateralFI });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_IV[0]", Value = obj._15_B_AmountReceivedFrom.ForeignGODFI });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_V[0]", Value = obj._15_B_AmountReceivedFrom.ForeignECA });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_VI[0]", Value = obj._15_B_AmountReceivedFrom.ForeignCO });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_VII[0]", Value = obj._15_B_AmountReceivedFrom.ForeignBodyCor });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2VIII[0]", Value = obj._15_B_AmountReceivedFrom.ForeignCitizens });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_IX[0]", Value = obj._15_B_AmountReceivedFrom.ForeignAuthorities });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC2_X[0]", Value = obj._15_B_AmountReceivedFrom.PersonsResidentsOutsideIndia });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC3_I[0]", Value = obj._15_C_AnyAmountReceipt.BankingCompany });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC3_II[0]", Value = obj._15_C_AnyAmountReceipt.SBI });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC3_III[0]", Value = obj._15_C_AnyAmountReceipt.NotifiedByTheCentralGovernment });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC3_IV[0]", Value = obj._15_C_AnyAmountReceipt.CorrespondingNewBank });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC3_V[0]", Value = obj._15_C_AnyAmountReceipt.CooperativeBank });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC4_I[0]", Value = obj._15_D_AnyAmountReceiptAsLoan.PublicFinancialInstitutions });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC4_II[0]", Value = obj._15_D_AnyAmountReceiptAsLoan.AnyRegionalFI });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC4_III[0]", Value = obj._15_D_AnyAmountReceiptAsLoan.InsuranceCompanies });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC4_IV[0]", Value = obj._15_D_AnyAmountReceiptAsLoan.ScheduledBanks });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC5[0]", Value = obj._15_E_IssueOfCommercial });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC6[0]", Value = obj._15_F_FromOtherCompany });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC7[0]", Value = obj._15_G_IssueOfCommercial });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC8[0]", Value = obj._15_H_FromPerson });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC9_A[0]", Value = obj._15_I_AnyAmountReceiptAsLoan.AmountRaisedBy });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC9_B[0]", Value = obj._15_I_AnyAmountReceiptAsLoan.BondsOrDebentures });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC10[0]", Value = obj._15_J_NonConvertibleDebentures });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC11[0]", Value = obj._15_K_EmployeeOfCompany });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC12[0]", Value = obj._15_L_NonInterest });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC13_I[0]", Value = obj._15_M_AnyAmountReceiptAsLoan.AdvanceForSupply });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC13_II[0]", Value = obj._15_M_AnyAmountReceiptAsLoan.AdvanceAccounted });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC13III[0]", Value = obj._15_M_AnyAmountReceiptAsLoan.SecurityDeposit });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC13_IV[0]", Value = obj._15_M_AnyAmountReceiptAsLoan.LongTermProjects });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC13_V[0]", Value = obj._15_M_AnyAmountReceiptAsLoan.TowardsConsideration });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC13_VI[0]", Value = obj._15_M_AnyAmountReceiptAsLoan.SectoralRegulator });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC13VII[0]", Value = obj._15_M_AnyAmountReceiptAsLoan.Publication });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC14[0]", Value = obj._15_N_ByPromoters });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC15[0]", Value = obj._15_O_ByNidhiCompnay });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC16[0]", Value = obj._15_P_ByChitFund });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC17[0]", Value = obj._15_Q_SEBI });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC18[0]", Value = obj._15_R_ByStartUp });

                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC19_I[0]", Value = obj._15_S_AnyAmountReceiptAsLoan.InvestmentFund });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC19_II[0]", Value = obj._15_S_AnyAmountReceiptAsLoan.CapitalFund });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC19III[0]", Value = obj._15_S_AnyAmountReceiptAsLoan.InfraTrust });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC19_IV[0]", Value = obj._15_S_AnyAmountReceiptAsLoan.RealEstateTrust });
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_DPT_3[0].ANY_AMT_REC19_V[0]", Value = obj._15_S_AnyAmountReceiptAsLoan.MutualFund });
                }

                if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                {
                    SetData(result, lstFields);
                    result.Success = true;
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Generate E-Form AOC4
        public EFormVM GenerateAOC4(long AOC4Id, int customerId)
        {
            var result = new EFormVM();
            IAOC4Service objIAOC4Service = new AOC4Service();
            var eform = (from row in entities.BM_SP_GetAOC4EformById((int)AOC4Id, customerId)
                         select row).FirstOrDefault();

            if (!string.IsNullOrEmpty(eform))
            {
                result.FormName = "AOC-4.pdf";
                result.FullFileName = eform;
            }

            var lstFields = new List<EFormFieldVM>();
            try
            {
                var AOC4Details = (from row in entities.BM_Form_AOC4
                                   join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                   where row.Id == AOC4Id
                                   select new
                                   {
                                       row.EntityId,
                                       entity.CompanyName,
                                       entity.CIN_LLPIN,
                                       entity.Entity_Type,
                                       entity.Email_Id,

                                       AOC4Id = row.Id
                                   }).FirstOrDefault();
                if (AOC4Details != null)
                {
                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CIN[0]", Value = AOC4Details.CIN_LLPIN });

                    #region Company Info
                    var companyInfo = (from row in entities.BM_Form_AOC4_1_CompanyInfo
                                       join a in entities.BM_Form_AOC4 on row.AOC4Id equals a.Id
                                       join e in entities.BM_EntityMaster on a.EntityId equals e.Id
                                       where row.AOC4Id == AOC4Details.AOC4Id && row.IsActive == true && a.IsActive == true
                                       select new AOC4_CompanyInfoVM
                                       {
                                           CompanyInfoId = row.Id,
                                           CompanyInfoAOC4Id = row.AOC4Id,
                                           EntityId = e.Id,
                                           CompanyName = e.CompanyName,
                                           FYStart = row.FYStart,
                                           FYEnd = row.FYEnd,
                                           DateOfBM_FRApproved = row.DateOfBM_FRApproved,
                                           DateOfBM_FRSec134 = row.DateOfBM_FRSec134,
                                           DateOfBM_FRSignAuditor = row.DateOfBM_FRSignAuditor,
                                           NoOfAuditors = row.NoOfAuditors,
                                           RecordsInEForm = row.RecordsInEForm
                                       }).FirstOrDefault();

                    if (companyInfo != null)
                    {
                        if (companyInfo.FYStart != null)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].FY_START_DATE[0]", Value = Convert.ToDateTime(companyInfo.FYStart).ToString("yyyy-MM-dd") });
                        }

                        if (companyInfo.FYEnd != null)
                        {
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].FY_END_DATE[0]", Value = Convert.ToDateTime(companyInfo.FYEnd).ToString("yyyy-MM-dd") });
                        }

                        if (companyInfo.DateOfBM_FRApproved != null)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DATE_BOD_MEETING[0]", Value = Convert.ToDateTime(companyInfo.DateOfBM_FRApproved).ToString("yyyy-MM-dd") });
                        }

                        if (companyInfo.DateOfBM_FRSec134 != null)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DATE_SIGNING_RFS[0]", Value = Convert.ToDateTime(companyInfo.DateOfBM_FRSec134).ToString("yyyy-MM-dd") });
                        }

                        if (companyInfo.DateOfBM_FRSignAuditor != null)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DATE_SIGNING_RFS[0]", Value = Convert.ToDateTime(companyInfo.DateOfBM_FRSignAuditor).ToString("yyyy-MM-dd") });
                        }

                        if (companyInfo.RecordsInEForm == true)
                        {
                            //lstFields.Add(new EFormFieldVM() { Name = "", Value = Convert.ToDateTime(companyInfo.DateOfBM_FRSignAuditor).ToString("yyyy-MM-dd") });
                        }

                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NATURE_OF_FS[0]", Value = "OAFS" });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_PROV_FIN_STMT[0]", Value = "NO" });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_ADOPTED_ADJOU[0]", Value = "NO" });

                        //Subsidary
                        var holdingDetails =  objIAOC4Service.GetCompanyHolding(AOC4Details.EntityId);
                        if(holdingDetails != null)
                        {
                            if(!string.IsNullOrEmpty(holdingDetails.CIN_HOLDING_COMP))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMP_SUBSIDRY[0]", Value = "YES" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CIN_HOLDING_COMP[0]", Value = holdingDetails.CIN_HOLDING_COMP });
                            }
                            else
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMP_SUBSIDRY[0]", Value = "NO" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CIN_HOLDING_COMP[0]", Value = "" });
                            }
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMP_SUBSIDRY[0]", Value = "NO" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CIN_HOLDING_COMP[0]", Value = "" });
                        }
                        
                        //End

                        if (companyInfo.NoOfAuditors > 0)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUM_OF_AUDITORS[0]", Value = Convert.ToString(companyInfo.NoOfAuditors) });
                        }

                        var scheduleIII = ""; // "YES";
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_SCHEDULE_III[0]", Value = scheduleIII });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TYPE_OF_INDUSTRY[0]", Value = "COIC" });

                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_CONSOLIDATED[0]", Value = "NO" });

                        #region Signing Details
                        companyInfo.objSignedDetails = (from row in entities.BM_Form_AOC4_1_SignedDetails
                                                        where row.AOC4Id == AOC4Details.AOC4Id && row.IsActive == true
                                                        select new AOC4_SignedDetailsVM
                                                        {
                                                            DINPAN_1 = row.DINPAN_1,
                                                            Name_1 = row.Name_1,
                                                            Date_1 = row.Date_1,
                                                            DINPAN_2 = row.DINPAN_2,
                                                            Name_2 = row.Name_2,
                                                            Date_2 = row.Date_2,
                                                            DINPAN_3 = row.DINPAN_3,
                                                            Name_3 = row.Name_3,
                                                            Date_3 = row.Date_3,
                                                            DINPAN_4 = row.DINPAN_4,
                                                            Name_4 = row.Name_4,
                                                            Date_4 = row.Date_4,
                                                            DINPAN_5 = row.DINPAN_5,
                                                            Name_5 = row.Name_5,
                                                            Date_5 = row.Date_5,
                                                            DIN_1 = row.DIN_1,
                                                            DName_1 = row.DName_1,
                                                            DDate_1 = row.DDate_1,
                                                            DIN_2 = row.DIN_2,
                                                            DName_2 = row.DName_2,
                                                            DDate_2 = row.DDate_2,
                                                            DIN_3 = row.DIN_3,
                                                            DName_3 = row.DName_3,
                                                            DDate_3 = row.DDate_3
                                                        }).FirstOrDefault();

                        if (companyInfo.objSignedDetails != null)
                        {
                            DateTime? signDate = null;
                            var DINPAN = companyInfo.objSignedDetails.DINPAN_1;
                            if (!string.IsNullOrEmpty(DINPAN))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN1[0]", Value = DINPAN });
                                signDate = companyInfo.objSignedDetails.Date_1;
                                if (signDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS1[0]", Value = Convert.ToDateTime(signDate).ToString("yyyy-MM-dd") });
                                }
                            }

                            DINPAN = companyInfo.objSignedDetails.DINPAN_2;
                            if (!string.IsNullOrEmpty(DINPAN))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN2[0]", Value = DINPAN });
                                signDate = companyInfo.objSignedDetails.Date_2;
                                if (signDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS2[0]", Value = Convert.ToDateTime(signDate).ToString("yyyy-MM-dd") });
                                }
                            }

                            DINPAN = companyInfo.objSignedDetails.DINPAN_3;
                            if (!string.IsNullOrEmpty(DINPAN))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN3[0]", Value = DINPAN });
                                signDate = companyInfo.objSignedDetails.Date_3;
                                if (signDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS3[0]", Value = Convert.ToDateTime(signDate).ToString("yyyy-MM-dd") });
                                }
                            }

                            DINPAN = companyInfo.objSignedDetails.DINPAN_4;
                            if (!string.IsNullOrEmpty(DINPAN))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN4[0]", Value = DINPAN });
                                signDate = companyInfo.objSignedDetails.Date_4;
                                if (signDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS4[0]", Value = Convert.ToDateTime(signDate).ToString("yyyy-MM-dd") });
                                }
                            }

                            DINPAN = companyInfo.objSignedDetails.DINPAN_5;
                            if (!string.IsNullOrEmpty(DINPAN))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN5[0]", Value = DINPAN });
                                signDate = companyInfo.objSignedDetails.Date_4;
                                if (signDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS5[0]", Value = Convert.ToDateTime(signDate).ToString("yyyy-MM-dd") });
                                }
                            }


                            //BR
                            DINPAN = companyInfo.objSignedDetails.DIN_1;
                            if (!string.IsNullOrEmpty(DINPAN))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIN1[0]", Value = DINPAN });
                                signDate = companyInfo.objSignedDetails.DDate_1;
                                if (signDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_BR1[0]", Value = Convert.ToDateTime(signDate).ToString("yyyy-MM-dd") });
                                }
                            }

                            DINPAN = companyInfo.objSignedDetails.DIN_2;
                            if (!string.IsNullOrEmpty(DINPAN))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIN2[0]", Value = DINPAN });
                                signDate = companyInfo.objSignedDetails.DDate_2;
                                if (signDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_BR2[0]", Value = Convert.ToDateTime(signDate).ToString("yyyy-MM-dd") });
                                }
                            }

                            DINPAN = companyInfo.objSignedDetails.DIN_3;
                            if (!string.IsNullOrEmpty(DINPAN))
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIN3[0]", Value = DINPAN });
                                signDate = companyInfo.objSignedDetails.DDate_3;
                                if (signDate != null)
                                {
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_BR3[0]", Value = Convert.ToDateTime(signDate).ToString("yyyy-MM-dd") });
                                }
                            }
                        }
                        #endregion

                        #region Auditor's Details
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUM_OF_AUDITORS[0]", Value = "1" });
                        if (companyInfo.FYStart != null)
                        {
                            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                            {

                                var lstStatutotyAuditor = (from row in entities.BM_SP_StatutoryAuditorDetails(AOC4Details.EntityId)
                                                           select new VMStatutory_Auditor
                                                           {
                                                               Id = row.Id,
                                                               AuditorMappingId = row.MappingId,
                                                               Nature_of_Appointment = row.Nature_of_Appointment,
                                                               AuditorId = row.Auditor_ID,
                                                               appointed_FromDate = row.Appointed_FromDate,
                                                               appointed_ToDate = row.Appointed_DueDate,
                                                               jointAuditor = row.IsJoin_Auditor_Appointed == null ? "NO" : row.IsJoin_Auditor_Appointed == true ? "YES" : "No",
                                                               AudtitorCriteria = row.Category,
                                                               AuditorNames = row.FullName_FirmName,
                                                               IsMappingActive = row.IsMappingActive,
                                                               IsTenureCompleted = row.IsTenureCompleted
                                                           }).ToList();
                                if (lstStatutotyAuditor != null)
                                {
                                    var statAuditor = (from row in lstStatutotyAuditor
                                                       where row.appointed_FromDate <= companyInfo.FYStart && row.appointed_ToDate >= companyInfo.FYStart
                                                       select row).FirstOrDefault();

                                    if(statAuditor != null)
                                    {
                                        var auditor = (from row in entities.BM_Auditor_Master
                                                       where row.Id == statAuditor.AuditorId
                                                       select row).FirstOrDefault();

                                        if(auditor != null)
                                        {
                                            lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].IT_PAN[0]", Value = auditor.PAN });

                                            if(auditor.Category_Id == 1)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].RB_CATEGORY_AUDT[0]", Value = "INDI" });
                                            }
                                            else if (auditor.Category_Id == 2)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].RB_CATEGORY_AUDT[0]", Value = "AFRM" });
                                            }

                                            if(!String.IsNullOrEmpty(auditor.Reg_MemberNumber))
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].MEMBERSHIP_NUM_A[0]", Value = auditor.Reg_MemberNumber });
                                            }

                                            var auditorDetails = (from row in entities.BM_StatutoryAuditor
                                                           where row.Id == statAuditor.Id
                                                           select new
                                                           {
                                                               row.SRN_relevent_form
                                                           }).FirstOrDefault();
                                            if(auditorDetails != null)
                                            {
                                                if (!String.IsNullOrEmpty(auditorDetails.SRN_relevent_form))
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].SRN_ADT_1[0]", Value = auditorDetails.SRN_relevent_form });
                                                }
                                            }

                                            if(auditor.FullName_FirmName != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].NAME_AUDT_AUDTRF[0]", Value = auditor.FullName_FirmName });
                                            }


                                            if (auditor.AddressLine1 != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].ADDRESS_LINE_I[0]", Value = auditor.AddressLine1 });
                                            }

                                            if (auditor.AddressLine2 != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].ADDRESS_LINE_II[0]", Value = auditor.AddressLine2 });
                                            }

                                            if (auditor.city_Id > 0 )
                                            {
                                                var city = (from row in entities.Cities
                                                            where row.ID == auditor.city_Id
                                                            select row.Name).FirstOrDefault();

                                                if(!string.IsNullOrEmpty(city))
                                                {
                                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].CITY[0]", Value = city });
                                                }
                                            }

                                            if (auditor.Pin_Number != null)
                                            {
                                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_S10[0].DATA[0].PIN_CODE[0]", Value = auditor.Pin_Number.ToString() });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                    }
                    #endregion

                    #region Balance Sheet
                    var objBalanceSheet = objIAOC4Service.GetBalanceSheet(AOC4Id, customerId);
                    if (objBalanceSheet != null)
                    {
                        #region Current Period
                        if (objBalanceSheet.Current != null)
                        {
                            var strValue = string.Empty;

                            if(objBalanceSheet.Current.EndDate != null)
                            {
                                strValue = Convert.ToDateTime(objBalanceSheet.Current.EndDate).ToString("yyyy-MM-dd");
                            }
                            else
                            {
                                strValue = "";
                            }
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DATE_CURR_REP[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.Capital);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHARE_CAPITAL_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.Reserve);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RESERVE_SURPLUS1[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.MoneyRecived);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.MoneyPending);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MON_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.LongTerm);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_BORR_C[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.Deferred);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TL_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.OtherLongTerm);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_LNG_TRM_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.LongTermProvision);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_PROV_C[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.ShortTerm);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_BOR_C[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.TradePayables);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TRADE_PAYABLES_C[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.OtherCL);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_LIA_C[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.ShortTermProvision);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_PRO_C[0]", Value = strValue });

                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4 [0]", Value = strValue });
                            //strValue = Convert.ToString(objBalanceSheet.Current.Total_I);

                            strValue = Convert.ToString(objBalanceSheet.Current.TangibleA);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TANGIBLE_ASSET_C[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.IntangibleA);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AST_C[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.CapitalWIP);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_WIP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.IntangibleA_UD);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AUD_C[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.NonCI);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NON_CURR_INV_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.DeferredTA);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TA_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.Lt_LoanAndAdv);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LT_LOANS_ADV_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.OtherNonCA);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_NON_CA_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.CurrentInvestment);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CURRENT_INV_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.Invetories);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.TradeReceivable);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TRADE_RECEIV_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.CashEquivalents);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CASH_AND_EQU_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.ShortTermLoan);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHORT_TRM_LOA_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Current.OtherCurrentAsset);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_CA_CR[0]", Value = strValue });
                        }
                        #endregion

                        #region Previous Period
                        if (objBalanceSheet.Previous != null)
                        {
                            var strValue = string.Empty;

                            if (objBalanceSheet.Previous.EndDate != null)
                            {
                                strValue = Convert.ToDateTime(objBalanceSheet.Previous.EndDate).ToString("yyyy-MM-dd");
                            }
                            else
                            {
                                strValue = "";
                            }
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DATE_PREV_REP[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.Capital);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHARE_CAPITAL_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.Reserve);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RESERVE_SURPLUS2[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.MoneyRecived);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.MoneyPending);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MON_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.LongTerm);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_BORR_P[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.Deferred);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TL_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.OtherLongTerm);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_LNG_TRM_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.LongTermProvision);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_PROV_P[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.ShortTerm);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_BOR_P[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.TradePayables);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TRADE_PAYABLES_P[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.OtherCL);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_LIA_P[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.ShortTermProvision);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_PRO_P[0]", Value = strValue });

                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4 [0]", Value = strValue });
                            //strValue = Convert.ToString(objBalanceSheet.Previous.Total_I);

                            strValue = Convert.ToString(objBalanceSheet.Previous.TangibleA);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TANGIBLE_ASSET_P[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.IntangibleA);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AST_P[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.CapitalWIP);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_WIP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.IntangibleA_UD);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AUD_P[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.NonCI);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NON_CURR_INV_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.DeferredTA);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TA_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.Lt_LoanAndAdv);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LT_LOANS_ADV_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.OtherNonCA);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_NON_CA_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.CurrentInvestment);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CURRENT_INV_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.Invetories);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.TradeReceivable);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TRADE_RECEIV_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.CashEquivalents);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CASH_AND_EQU_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.ShortTermLoan);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHORT_TRM_LOA_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objBalanceSheet.Previous.OtherCurrentAsset);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_CA_PR[0]", Value = strValue });
                        }
                        #endregion
                    }
                    #endregion

                    #region Detailed Balance sheet
                    var objDetailedBalanceSheet = objIAOC4Service.GetDetailedBalanceSheet(AOC4Id, customerId);
                    if (objDetailedBalanceSheet != null)
                    {
                        #region Current
                        if (objDetailedBalanceSheet.Current != null)
                        {
                            var strValue = string.Empty;

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Bonds);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].BONDS_DEBS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_LoanBank);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FB_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_LoanOtherParties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FO_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Deferred_pay_liabilities);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_PL_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Deposits);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Loans_adv_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_RP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_maturities);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_MAT_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Otherloans_advances);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOA_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LT_Aggregate_Amt);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREGATE_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_LoanBank);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_B_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_LoanOtherParties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_OP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Loans_advances_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_A_RP_ST_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Deposits);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_ST_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Loans_adv_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTH_LOANS_ADV_CR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Tot_borrowings);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOTAL_ST_BORR_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.ST_Aggregate_Amt);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREG_ST_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Capital_adv);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CAPT_ADVANCES_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Security_Deposits);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Loans_adv_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_ORP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Otherloans_advances);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_A_CR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_TotLoan_Adv);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_A_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_FromRelated_Parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_RP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_FromOthers);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_OT_CR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Net_loan_Adv);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4 [0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTA_Loans_Adv_dueByDir);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_DUE_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Capital_adv);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4CAPT_ADVANC_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Security_Deposits);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Loans_adv_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_OR_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Otherloans_advances);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_CR1[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_TotLoan_Adv);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_FromRelated_Parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_RP_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_FromOthers);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_OT_CR1[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Net_loan_Adv);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NET_LT_LOA_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.LTAD_Loans_Adv_dueByDir);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOAN_ADV_DUE_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Secured_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_ES_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Secured_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_WS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Unsecured_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_ES_C[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Unsecured_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_WS_C[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Doubtful_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_ES_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Doubtful_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_WS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_TotalTrade_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_ES_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_TotalTrade_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_WS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Provision_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_ES_C[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_Provision_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_WS_C[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_NetTrade_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_ES_C[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_NetTrade_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_WS_C[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_DebtDue_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_ES_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Current.TR_DebtDue_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_WS_CR[0]", Value = strValue });

                        }
                        #endregion

                        #region Previous
                        if (objDetailedBalanceSheet.Previous != null)
                        {
                            var strValue = string.Empty;

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Bonds);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].BONDS_DEBS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_LoanBank);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FB_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_LoanOtherParties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FO_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Deferred_pay_liabilities);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_PL_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Deposits);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Loans_adv_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_RP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_maturities);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_MAT_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Otherloans_advances);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOA_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LT_Aggregate_Amt);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREGATE_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_LoanBank);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_B_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_LoanOtherParties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_OP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Loans_advances_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_A_RP_ST_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Deposits);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_ST_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Loans_adv_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTH_LOANS_ADV_PR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Tot_borrowings);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOTAL_ST_BORR_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.ST_Aggregate_Amt);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREG_ST_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Capital_adv);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CAPT_ADVANCES_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Security_Deposits);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Loans_adv_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_ORP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Otherloans_advances);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_A_PR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_TotLoan_Adv);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_A_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_FromRelated_Parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_RP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_FromOthers);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_OT_PR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Net_loan_Adv);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4 [0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTA_Loans_Adv_dueByDir);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_DUE_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Capital_adv);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4CAPT_ADVANC_PR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Security_Deposits);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_PR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Loans_adv_related_parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_OR_PR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Otherloans_advances);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_PR1[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_TotLoan_Adv);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_FromRelated_Parties);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_RP_PR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_FromOthers);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_OT_PR1[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Net_loan_Adv);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NET_LT_LOA_CR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.LTAD_Loans_Adv_dueByDir);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOAN_ADV_DUE_PR1[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Secured_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_ES_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Secured_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_WS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Unsecured_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_ES_P[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Unsecured_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_WS_P[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Doubtful_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_ES_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Doubtful_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_WS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_TotalTrade_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_ES_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_TotalTrade_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_WS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Provision_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_ES_P[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_Provision_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_WS_P[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_NetTrade_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_ES_P[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_NetTrade_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_WS_P[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_DebtDue_ESM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_ES_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedBalanceSheet.Previous.TR_DebtDue_WSM);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_WS_PR[0]", Value = strValue });

                        }
                        #endregion
                    }
                    #endregion

                    #region Financial Parameters
                    var objFinancialPara = objIAOC4Service.GetFinancialParameteritems(AOC4Id, customerId);
                    if (objFinancialPara != null)
                    {
                        var strValue = string.Empty;

                        strValue = Convert.ToString(objFinancialPara.Amt_Of_issue_Alloted);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].AMOUNT_ISSUE_ALL[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Share_app_Money_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Share_app_Money_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY1[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Share_app_Money_3);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY2[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Share_app_Money_4);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY3[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Paid_up_capital_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PAID_UP_CAPT_FC[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Paid_up_capital_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PAID_UP_CAPT_FC1[0]", Value = strValue });

                        //strValue = Convert.ToString(objFinancialPara.NoOfShares);
                        //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUM_SHARES_BB[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Deposit_Accepted);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEP_ACCEPTED_REN[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Deposit_Matured_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLAI[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Deposit_Matured_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLA1[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Deposit_Matured_3);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLA2[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Unclaimed_Debentures);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].UNCLAIMED_M_DEB[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Claimed_Debentures);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEBENTURES_CLAIM[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.InterestOnDepositAccrued);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INTEREST_DEPOSIT[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Unpaid_divedend);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].UNPAID_DIVIDEND[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.InvestmentInCompany_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INVESTMENT_SUBSD[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.InvestmentInCompany_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INVESTMENT_GOVT[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Capital_Reserves);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_RESERVES[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Amt_TransferforIEPF);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].AMT_DUE_TRANSFER[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Inter_Corp_Deposit);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INTER_CORPORATE[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.GrossValueOfTransaction);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].GROSS_VALUE[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.CapitalSubsidies);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_SUBSDIES[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.CallsUnpaidByDirector);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CALLS_UNPAID_DIR[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.CallsUnpaidByOthers);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CALLS_UNPAID_OTH[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Forfeited_Shares_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].FORFEITED_SHARES[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Forfeited_Shares_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].FORFEITED_SHAR_R[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Borrowing_foreign_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].BORROWING_FIA[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Borrowing_foreign_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].BORROWING_FC[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.InterCorporate_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INTER_CORP_BORR[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.InterCorporate_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INTER_CORP_BORR1[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.CommercialPaper);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].COMMERCIAL_PAPER[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.ConversionOfWarrant_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_ES[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.ConversionOfWarrant_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_PS[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.ConversionOfWarrant_3);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_DE[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.WarrantsIssueInForeignCurrency);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].WARRANTS_ISSUED[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.WarrantsIssueInRupees);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].WARRANTS_ISSUED1[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.DefaultInPayment_1);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEFAULT_PAYMENT[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.DefaultInPayment_2);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEFAULT_PAYMENT1[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.WheatheOperatingLease);
                        strValue = string.IsNullOrEmpty(strValue) ? "NO" : "YES";
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_OPERATING_LEA[0]", Value = strValue });

                        if (objFinancialPara.WheatheOperatingLease == true)
                        {
                            strValue = Convert.ToString(objFinancialPara.ProvideDetailsOfConversion);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DETAILS_CONVERSN[0]", Value = strValue });
                        }

                        strValue = Convert.ToString(objFinancialPara.NetWorthOfComp);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NET_WORTH_COMPAN[0]", Value = strValue });

                        //strValue = Convert.ToString(objFinancialPara.NoOfShareHolders);
                        //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUM_SHARE_HOLDRS[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.SecuredLoan);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].SECURED_LOAN[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.GrossFixedAssets);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].GROSS_FIXD_ASSET[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Depreciation_Amortization);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DEPRECIATN_AMORT[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.Misc_Expenditure);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].MISC_EXPENDITURE[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialPara.UnhedgedForeign);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].UNHEDGED_FE_EXP[0]", Value = strValue });

                    }
                    #endregion

                    #region Share Capital
                    var objSharedCapital = objIAOC4Service.GetSharedCapitalitems(AOC4Id, customerId);

                    if (objSharedCapital != null)
                    {
                        var strValue = string.Empty;

                        strValue = Convert.ToString(objSharedCapital.PublicIssue_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PUBLIC_ISSUE_ES[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.PublicIssue_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PUBLIC_ISSUE_PS[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.BonusIssue_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].BONUS_ISSUE_ES[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.BonusIssue_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].BONUS_ISSUE_PS[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.RightIssue_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RIGHTS_ISSUE_ES[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.RightIssue_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RIGHTS_ISSUE_PS[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.PvtPlacementarising_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PRIV_PLACEMENT_E[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.PvtPlacementarising_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PRIV_PLACEMENT_P[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.OtherPvtPlacement_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHR_PRI_PLAC_ES[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.OtherPvtPlacement_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHR_PRI_PLAC_PS[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.PrferentialAllotment_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PREF_ALLOTMENT_E[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.PrferentialAllotment_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].PREF_ALLOTMENT_P[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.OtherPreferentialallotment_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHR_PREF_ALL_ES[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.OtherPreferentialallotment_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHR_PREF_ALL_PS[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.EmpStockOptionPlan_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].ESOP_ES[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.EmpStockOptionPlan_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].ESOP_PS[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.Others_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_ES[0]", Value = strValue });

                        strValue = Convert.ToString(objSharedCapital.Others_P);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_PS[0]", Value = strValue });
                    }
                    #endregion

                    #region Cost Record
                    var objCostRecord = objIAOC4Service.GetDetailsOfCostRecords(AOC4Id);
                    if (objCostRecord != null)
                    {
                        if (objCostRecord.MaintenanceOfCR)
                        {
                            var strValue = string.Empty;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS[0]", Value = "YES" });

                            strValue = string.IsNullOrEmpty(objCostRecord.CentralExcise) ? "" : objCostRecord.CentralExcise;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE[0]", Value = strValue });

                            if (objCostRecord.AuditOfCR)
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS1[0]", Value = "YES" });
                                strValue = string.IsNullOrEmpty(objCostRecord.CentralExcise1) ? "" : objCostRecord.CentralExcise1;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE1[0]", Value = strValue });
                            }
                            else
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS1[0]", Value = "NO" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE1[0]", Value = "" });
                            }
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS[0]", Value = "NO" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE[0]", Value = "" });

                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS1[0]", Value = "" });
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE1[0]", Value = "" });
                        }
                    }
                    else
                    {
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS[0]", Value = "NO" });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE[0]", Value = "" });

                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS1[0]", Value = "" });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE1[0]", Value = "" });
                    }
                    #endregion

                    #region Profit & Loss
                    var objPL = objIAOC4Service.GetProfitLoss(AOC4Id, customerId);
                    if (objPL != null)
                    {
                        #region Current
                        if (objPL.Current != null)
                        {
                            var strValue = string.Empty;

                            if (objPL.Current.FROM_DATE_CR != null)
                            {
                                strValue = Convert.ToDateTime(objPL.Current.FROM_DATE_CR).ToString("yyyy-MM-dd");
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].FROM_DATE_CR[0]", Value = strValue });
                            }

                            if (objPL.Current.TO_DATE_CR != null)
                            {
                                strValue = Convert.ToDateTime(objPL.Current.TO_DATE_CR).ToString("yyyy-MM-dd");
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TO_DATE_CR[0]", Value = strValue });
                            }

                            strValue = Convert.ToString(objPL.Current.SALES_GOODS_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.SALES_GOODS_T_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_T_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.SALES_SUPPLY_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.SALES_GOODS1_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS1_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.SALE_GOODS_T1_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALE_GOODS_T1_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.SALES_SUPPLY1_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY1_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.OTHER_INCOME_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].OTHER_INCOME_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.TOTAL_REVENUE_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_REVENUE_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.COST_MATERIAL_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].COST_MATERIAL_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PURCHASE_STOCK_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PURCHASE_STOCK_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.FINISHED_GOODS_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].FINISHED_GOODS_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.WORK_IN_PROG_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].WORK_IN_PROG_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.STOCK_IN_TRADE_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].STOCK_IN_TRADE_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.EMP_BENEFIT_EX_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EMP_BENEFIT_EX_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.MANGERIAL_REM_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].MANGERIAL_REM_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PAYMENT_AUDTRS_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PAYMENT_AUDTRS_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.INSURANCE_EXP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].INSURANCE_EXP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.POWER_FUEL_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].POWER_FUEL_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.FINANCE_COST_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].FINANCE_COST_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.DEPRECTN_AMORT_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DEPRECTN_AMORT_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.OTHER_EXPENSES_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].OTHER_EXPENSES_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.TOTAL_EXPENSES_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EXPENSES_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PROFIT_BEFORE_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEFORE_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.EXCEPTIONL_ITM_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EXCEPTIONL_ITM_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PROFIT_BEF_TAX_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEF_TAX_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.EXTRAORDINARY_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EXTRAORDINARY_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PROF_B_TAX_7_8_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_B_TAX_7_8_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.CURRENT_TAX_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].CURRENT_TAX_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.DEFERRED_TAX_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DEFERRED_TAX_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PROF_LOSS_OPER_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_OPER_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PROF_LOSS_DO_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_DO_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.TAX_EXPNS_DIS_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TAX_EXPNS_DIS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PROF_LOS_12_13_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_12_13_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.PROF_LOS_11_14_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_11_14_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.BASIC_BEFR_EI_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].BASIC_BEFR_EI_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.DILUTED_BEF_EI_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_BEF_EI_C[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.BASIC_AFTR_EI_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].BASIC_AFTR_EI_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Current.DILUTED_AFT_EI_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_AFT_EI_C[0]", Value = strValue });

                        }
                        #endregion

                        #region Previous
                        if (objPL.Previous != null)
                        {
                            var strValue = string.Empty;

                            if (objPL.Previous.FROM_DATE_CR != null)
                            {
                                strValue = Convert.ToDateTime(objPL.Previous.FROM_DATE_CR).ToString("yyyy-MM-dd");
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].FROM_DATE_PR[0]", Value = strValue });
                            }

                            if (objPL.Previous.TO_DATE_CR != null)
                            {
                                strValue = Convert.ToDateTime(objPL.Previous.TO_DATE_CR).ToString("yyyy-MM-dd");
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TO_DATE_PR[0]", Value = strValue });
                            }

                            strValue = Convert.ToString(objPL.Previous.SALES_GOODS_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.SALES_GOODS_T_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_T_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.SALES_SUPPLY_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.SALES_GOODS1_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS1_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.SALE_GOODS_T1_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALE_GOODS_T1_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.SALES_SUPPLY1_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY1_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.OTHER_INCOME_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].OTHER_INCOME_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.TOTAL_REVENUE_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_REVENUE_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.COST_MATERIAL_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].COST_MATERIAL_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PURCHASE_STOCK_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PURCHASE_STOCK_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.FINISHED_GOODS_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].FINISHED_GOODS_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.WORK_IN_PROG_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].WORK_IN_PROG_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.STOCK_IN_TRADE_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].STOCK_IN_TRADE_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.EMP_BENEFIT_EX_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EMP_BENEFIT_EX_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.MANGERIAL_REM_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].MANGERIAL_REM_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PAYMENT_AUDTRS_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PAYMENT_AUDTRS_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.INSURANCE_EXP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].INSURANCE_EXP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.POWER_FUEL_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].POWER_FUEL_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.FINANCE_COST_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].FINANCE_COST_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.DEPRECTN_AMORT_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DEPRECTN_AMORT_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.OTHER_EXPENSES_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].OTHER_EXPENSES_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.TOTAL_EXPENSES_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EXPENSES_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PROFIT_BEFORE_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEFORE_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.EXCEPTIONL_ITM_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EXCEPTIONL_ITM_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PROFIT_BEF_TAX_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEF_TAX_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.EXTRAORDINARY_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EXTRAORDINARY_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PROF_B_TAX_7_8_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_B_TAX_7_8_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.CURRENT_TAX_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].CURRENT_TAX_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.DEFERRED_TAX_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DEFERRED_TAX_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PROF_LOSS_OPER_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_OPER_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PROF_LOSS_DO_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_DO_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.TAX_EXPNS_DIS_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TAX_EXPNS_DIS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PROF_LOS_12_13_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_12_13_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.PROF_LOS_11_14_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_11_14_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.BASIC_BEFR_EI_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].BASIC_BEFR_EI_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.DILUTED_BEF_EI_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_BEF_EI_P[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.BASIC_AFTR_EI_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].BASIC_AFTR_EI_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objPL.Previous.DILUTED_AFT_EI_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_AFT_EI_P[0]", Value = strValue });

                        }
                        #endregion
                    }
                    #endregion

                    #region Detailed Profit & Loss
                    var objDetailedPL = objIAOC4Service.GetDetailedProfitLossitems(AOC4Id, customerId);
                    if (objDetailedPL != null)
                    {
                        #region Current
                        if (objDetailedPL.Current != null)
                        {
                            var strValue = string.Empty;

                            strValue = Convert.ToString(objDetailedPL.Current.EXP_GOODS_FOB_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EXP_GOODS_FOB_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.INTEREST_DIVD_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_DIVD_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.ROYALTY_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.KNOW_HOW_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.PROF_CONS_FEE_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_CONS_FEE_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.OTHR_INCOME_E_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].OTHR_INCOME_E_CR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedPL.Current.TOTAL_EARNG_FE_C);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EARNG_FE_C[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.RAW_MATERIAL_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].RAW_MATERIAL_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.COMPONENT_SP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].COMPONENT_SP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.CAPITAL_GOODS_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].CAPITAL_GOODS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.ROYALTY_EXP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_EXP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.KNOW_HOW_EXP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_EXP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.PROF_CON_FEE_E_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_CON_FEE_E_C[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.INTEREST_EXP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_EXP_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.OTHER_MATTERS_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].OTHER_MATTERS_CR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Current.DIVIDEND_PAID_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIVIDEND_PAID_CR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedPL.Current.TOT_EXP_FE_CR);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOT_EXP_FE_CR[0]", Value = strValue });
                        }
                        #endregion

                        #region Previous
                        if (objDetailedPL.Previous != null)
                        {
                            var strValue = string.Empty;

                            strValue = Convert.ToString(objDetailedPL.Previous.EXP_GOODS_FOB_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EXP_GOODS_FOB_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.INTEREST_DIVD_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_DIVD_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.ROYALTY_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.KNOW_HOW_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.PROF_CONS_FEE_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_CONS_FEE_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.OTHR_INCOME_E_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].OTHR_INCOME_E_PR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedPL.Previous.TOTAL_EARNG_FE_C);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EARNG_FE_P[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.RAW_MATERIAL_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].RAW_MATERIAL_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.COMPONENT_SP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].COMPONENT_SP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.CAPITAL_GOODS_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].CAPITAL_GOODS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.ROYALTY_EXP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_EXP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.KNOW_HOW_EXP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_EXP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.PROF_CON_FEE_E_C);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROF_CON_FEE_E_P[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.INTEREST_EXP_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_EXP_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.OTHER_MATTERS_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].OTHER_MATTERS_PR[0]", Value = strValue });

                            strValue = Convert.ToString(objDetailedPL.Previous.DIVIDEND_PAID_CR);
                            strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DIVIDEND_PAID_PR[0]", Value = strValue });

                            //strValue = Convert.ToString(objDetailedPL.Previous.TOT_EXP_FE_CR);
                            //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                            //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOT_EXP_FE_PR[0]", Value = strValue });
                        }
                        #endregion
                    }
                    #endregion

                    #region Financial Parameters PL Items

                    var objFinancialParaItem = objIAOC4Service.GetFinancialParameter_PLDefaultData(AOC4Id, customerId);
                    if (objFinancialParaItem != null)
                    {
                        var strValue = String.Empty;

                        strValue = Convert.ToString(objFinancialParaItem.PROPOSED_DIVIDND);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROPOSED_DIVIDND[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialParaItem.PROPOSED_DIV_PER);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PROPOSED_DIV_PER[0]", Value = strValue });

                        //strValue = Convert.ToString(objFinancialParaItem.BASIC_EARNING_PS);
                        //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].BASIC_EARNING_PS[0]", Value = strValue });

                        //strValue = Convert.ToString(objFinancialParaItem.DILUTED_EARN_PS);
                        //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_EARN_PS[0]", Value = strValue });

                        //strValue = Convert.ToString(objFinancialParaItem.INCOME_FOREIGN);
                        //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].INCOME_FOREIGN[0]", Value = strValue });

                        //strValue = Convert.ToString(objFinancialParaItem.EXPENDIT_FOREIGN);
                        //strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].EXPENDIT_FOREIGN[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialParaItem.REVENUE_SUBSIDIE);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].REVENUE_SUBSIDIE[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialParaItem.RENT_PAID);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].RENT_PAID[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialParaItem.CONSUMPTION_STOR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].CONSUMPTION_STOR[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialParaItem.GROSS_VALUE_TRAN);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].GROSS_VALUE_TRAN[0]", Value = strValue });

                        strValue = Convert.ToString(objFinancialParaItem.BAD_DEBTS_RP);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].BAD_DEBTS_RP[0]", Value = strValue });

                    }

                    #endregion

                    #region CSR
                    var objCSR = objIAOC4Service.GetReportingCSRWithDetails(AOC4Id);
                    if (objCSR != null)
                    {
                        var strValue = string.Empty;

                        #region CSR Header
                        if(objCSR.RB_CSR_APPLICABL == true)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].RB_CSR_APPLICABL[0]", Value = "YES" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].RB_CSR_APPLICABL[0]", Value = "NO" });
                        }

                        strValue = Convert.ToString(objCSR.TURNOVER);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TURNOVER[0]", Value = strValue });

                        strValue = Convert.ToString(objCSR.NET_WORTH);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].NET_WORTH[0]", Value = strValue });

                        strValue = Convert.ToString(objCSR.PRESCRIBED_CSR_E);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].PRESCRIBED_CSR_E[0]", Value = strValue });

                        strValue = Convert.ToString(objCSR.TOTAL_AMT_SPENT);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_AMT_SPENT[0]", Value = strValue });

                        strValue = Convert.ToString(objCSR.AMOUNT_SPENT_LA);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].AMOUNT_SPENT_LA[0]", Value = strValue });

                        strValue = Convert.ToString(objCSR.NUM_CSR_ACTIVITY);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].NUM_CSR_ACTIVITY[0]", Value = strValue });
                        #endregion

                        #region CSR Details
                        var count_CSRActivity = 0;
                        if (objCSR.lstDetails != null)
                        {
                            var index = -1;
                            foreach (var item in objCSR.lstDetails)
                            {
                                strValue = item.CSR_PROJECT_ACTV;
                                if(!string.IsNullOrEmpty(strValue))
                                {
                                    index++;

                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].CSR_PROJECT_ACTV[0]", Value = strValue });

                                    strValue = Convert.ToString(item.Sector_Code);
                                    strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].SECTOR_PROJ_COVR[0]", Value = strValue });

                                    strValue = Convert.ToString(item.State_UT_Code);
                                    strValue = string.IsNullOrEmpty(strValue) ? "ALL" : strValue;
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].STATE_UT[0]", Value = strValue });

                                    if (strValue == "ALL")
                                    {
                                        lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].DISTRICT[0]", Value = "Multiple Districts" });
                                    }

                                    strValue = Convert.ToString(item.AMOUNT_OUTLAY);
                                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].AMOUNT_OUTLAY[0]", Value = strValue });

                                    strValue = Convert.ToString(item.AMOUNT_SPENT_PRJ);
                                    strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].AMOUNT_SPENT_PRJ[0]", Value = strValue });

                                    strValue = Convert.ToString(item.Mode_Amount_Code);
                                    strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].MODE_AMOUNT_SPNT[0]", Value = strValue });
                                }

                            }

                            count_CSRActivity = index + 1;
                        }
                        if(count_CSRActivity == 0)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].NUM_CSR_ACTIVITY[0]", Value = "" });

                            for (int index = 0; index < 3; index++)
                            {
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].CSR_PROJECT_ACTV[0]", Value = "" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].SECTOR_PROJ_COVR[0]", Value = "" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].STATE_UT[0]", Value = "" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].DISTRICT[0]", Value = "" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].AMOUNT_OUTLAY[0]", Value = "" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].AMOUNT_SPENT_PRJ[0]", Value = "" });
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + index + "].MODE_AMOUNT_SPNT[0]", Value = "" });
                            }
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC4_II[0].NUM_CSR_ACTIVITY[0]", Value = count_CSRActivity.ToString() });
                        }


                        #endregion
                    }
                    #endregion

                    #region RPT
                    var objRPT = objIAOC4Service.GetRPTDetails(AOC4Id, customerId);
                    if (objRPT != null)
                    {
                        var strValue = string.Empty;
                        var index = 0;
                        var countContracts = 0;
                        var countMatContracts = 0;
                        #region Contracts
                        if (objRPT.lstContracts != null)
                        {
                            index = -1;
                            foreach (var item in objRPT.lstContracts)
                            {
                                index++;
                                strValue = Convert.ToString(item.NAME_RELATED_PAR);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[" + index + "].NAME_RELATED_PAR[0]", Value = strValue });

                                strValue = Convert.ToString(item.NATURE_OF_RELATN);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[" + index + "].NATURE_OF_RELATN[0]", Value = strValue });

                                strValue = Convert.ToString(item.NATURE_OF_CONTRA);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[" + index + "].NATURE_OF_CONTRA[0]", Value = strValue });

                                //index
                                strValue = Convert.ToString(item.DURATION_OF_CONT);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[" + index + "].DURATION_OF_CONT[0]", Value = strValue });

                                if (item.DATE_OF_APPROVAL != null)
                                {
                                    strValue = Convert.ToDateTime(item.DATE_OF_APPROVAL).ToString("yyyy-MM-dd");
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[" + index + "].DATE_OF_APPROVAL[0]", Value = strValue });
                                }

                                strValue = Convert.ToString(item.AMOUNT_PAID);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[" + index + "].AMOUNT_PAID[0]", Value = strValue });

                                if (item.DATE_SPCL_RESOLT != null)
                                {
                                    strValue = Convert.ToDateTime(item.DATE_OF_APPROVAL).ToString("yyyy-MM-dd");
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[" + index + "].DATE_SPCL_RESOLT[0]", Value = strValue });
                                }
                            }
                            countContracts = index + 1;
                        }
                        #endregion

                        #region Material Contracts
                        if (objRPT.lstMaterialContracts != null)
                        {
                            index = -1;
                            foreach (var item in objRPT.lstMaterialContracts)
                            {
                                index++;

                                strValue = Convert.ToString(item.NAME_RELATED_PAR);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[" + index + "].NAME_RELATED_PAR[0]", Value = strValue });

                                strValue = Convert.ToString(item.NATURE_OF_RELATN);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[" + index + "].NATURE_OF_RELATN[0]", Value = strValue });

                                strValue = Convert.ToString(item.NATURE_OF_CONTRA);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[" + index + "].NATURE_OF_CONTRA[0]", Value = strValue });

                                strValue = Convert.ToString(item.DURATION_OF_CONT);
                                strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_4[0].DATA[" + index + "].DURATION_OF_CONT[0]", Value = strValue });

                                if (item.DATE_OF_APPROVAL != null)
                                {
                                    strValue = Convert.ToDateTime(item.DATE_OF_APPROVAL).ToString("yyyy-MM-dd");
                                    lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_4[0].DATA[" + index + "].DATE_OF_APPROVAL[0]", Value = strValue });
                                }

                                strValue = Convert.ToString(item.AMOUNT_PAID);
                                strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                                lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_4[0].DATA[" + index + "].AMOUNT_PAID[0]", Value = strValue });
                            }
                            countMatContracts = index + 1;
                        }
                        #endregion

                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUM_CONTRACTS[0]", Value = countContracts.ToString() });
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUM_MAT_CONTRACT[0]", Value = countMatContracts.ToString() });
                    }
                    #endregion

                    #region Auditors Details
                    var objAuditors = objIAOC4Service.GetAuditorsReport(AOC4Id);
                    if (objAuditors != null)
                    {
                        var strValue = string.Empty;

                        if (objAuditors.RB_COMPTRL_AUDTR == true)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMPTRL_AUDTR[0]", Value = "YES" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMPTRL_AUDTR[0]", Value = "NO" });
                        }

                        if (objAuditors.RB_AUDTR_REPORT == true)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]", Value = "YES" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]", Value = "NO" });
                        }

                        if (objAuditors.RB_COMP_AUDT_REP == true)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMP_AUDT_REP[0]", Value = "YES" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMP_AUDT_REP[0]", Value = "NO" });
                        }

                        strValue = Convert.ToString(objAuditors.FIXED_ASSETS);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].FIXED_ASSETS[0]", Value = strValue });

                        strValue = Convert.ToString(objAuditors.INVENTORIES);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES[0]", Value = strValue });

                        strValue = Convert.ToString(objAuditors.LOANS_GIVEN_COMP);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_GIVEN_COMP[0]", Value = strValue });

                        strValue = Convert.ToString(objAuditors.ACCEPTANCE_PUB_D);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].ACCEPTANCE_PUB_D[0]", Value = strValue });

                        strValue = Convert.ToString(objAuditors.MAINTENANCE_CR);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].MAINTENANCE_CR[0]", Value = strValue });

                        strValue = Convert.ToString(objAuditors.STATUTORY_DUES);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].STATUTORY_DUES[0]", Value = strValue });

                        strValue = Convert.ToString(objAuditors.TERM_LOANS);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS[0]", Value = strValue });

                        strValue = Convert.ToString(objAuditors.FRAUD_NOTICED);
                        strValue = string.IsNullOrEmpty(strValue) ? "0.00" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].FRAUD_NOTICED[0]", Value = strValue });

                        strValue = Convert.ToString(objAuditors.OTHER_COMMENTS);
                        strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_COMMENTS[0]", Value = strValue });
                    }
                    #endregion

                    #region Misc Authorization
                    var objMisc = objIAOC4Service.GetMiscAuth(AOC4Id);
                    if (objMisc != null)
                    {
                        var strValue = string.Empty;

                        if (objMisc.RB_SECRETARIAL_A == true)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_SECRETARIAL_A[0]", Value = "YES" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_SECRETARIAL_A[0]", Value = "NO" });
                        }

                        if (objMisc.RB_AUDTR_REPORT == true)
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]", Value = "YES" });
                        }
                        else
                        {
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]", Value = "NO" });
                        }

                        strValue = Convert.ToString(objMisc.CVRN);
                        strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CVRN[0]", Value = strValue });

                        if (objMisc.DECLARATION_DATE != null)
                        {
                            strValue = Convert.ToDateTime(objMisc.DECLARATION_DATE).ToString("yyyy-MM-dd");
                            lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DECLARATION_DATE[0]", Value = strValue });
                        }

                        //strValue = Convert.ToString(objMisc.DESIGNATION);
                        //strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DESIGNATION[0]", Value = strValue });

                        strValue = Convert.ToString(objMisc.DIN_PAN_MEM_NUM);
                        strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].DIN_PAN_MEM_NUM[0]", Value = strValue });

                        //strValue = Convert.ToString(objMisc.RB_CA_COA_CS);
                        //strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        ////switch (strValue)
                        ////{
                        ////    case "CA":

                        ////        break;
                        ////    default:
                        ////        break;
                        ////}
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_CA_COA_CS[0]", Value = strValue });

                        //if (objMisc.RB_ASSOC_FELLOW == true)
                        //{
                        //    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_ASSOC_FELLOW[0]", Value = "YES" });
                        //}
                        //else
                        //{
                        //    lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_ASSOC_FELLOW[0]", Value = "NO" });
                        //}
                        //strValue = Convert.ToString(objMisc.MEMBERSHIP_NUM);
                        //strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].MEMBERSHIP_NUM[0]", Value = strValue });

                        //strValue = Convert.ToString(objMisc.CERT_PRACTICE_NO);
                        //strValue = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].CERT_PRACTICE_NO[0]", Value = strValue });
                    }
                    #endregion

                    #region Test
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUM_CONTRACTS[0]", Value = "5" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUM_MAT_CONTRACT[0]", Value = "4" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[0].NAME_RELATED_PAR[0]", Value = "" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[0].NATURE_OF_RELATN[0]", Value = "brother" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[0].NATURE_OF_CONTRA[0]", Value = "buy" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[0].DURATION_OF_CONT[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[0].DATE_OF_APPROVAL[0]", Value = "2021-11-30" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[0].AMOUNT_PAID[0]", Value = "121.00" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[0].DATE_SPCL_RESOLT[0]", Value = "2021-11-30" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[0].NAME_RELATED_PAR[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[0].NATURE_OF_RELATN[0]", Value = "aa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[0].NATURE_OF_CONTRA[0]", Value = "aaa" });
                    //lstFields.Add(new EFormFieldVM() { Name =  "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[0].DETAILS_COMMENTS[0]", Value = "asdf" });
                    //lstFields.Add(new EFormFieldVM() { Name =  "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[0].BOD_DIR_REPLY[0]", Value = "asd" });

                    ////
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[1].NAME_RELATED_PAR[0]", Value = "" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[1].NATURE_OF_RELATN[0]", Value = "brother" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[1].NATURE_OF_CONTRA[0]", Value = "buy" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[1].DURATION_OF_CONT[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[1].DATE_OF_APPROVAL[0]", Value = "2021-11-30" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[1].AMOUNT_PAID[0]", Value = "121.00" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[1].DATE_SPCL_RESOLT[0]", Value = "2021-11-30" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[1].NAME_RELATED_PAR[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[1].NATURE_OF_RELATN[0]", Value = "aa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[1].NATURE_OF_CONTRA[0]", Value = "aaa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[1].DETAILS_COMMENTS[0]", Value = "asdf" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[1].BOD_DIR_REPLY[0]", Value = "asd" });


                    ////
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[2].NAME_RELATED_PAR[0]", Value = "" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[2].NATURE_OF_RELATN[0]", Value = "brother" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[2].NATURE_OF_CONTRA[0]", Value = "buy" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[2].DURATION_OF_CONT[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[2].DATE_OF_APPROVAL[0]", Value = "2021-11-30" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[2].AMOUNT_PAID[0]", Value = "121.00" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[2].DATE_SPCL_RESOLT[0]", Value = "2021-11-30" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[2].NAME_RELATED_PAR[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[2].NATURE_OF_RELATN[0]", Value = "aa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[2].NATURE_OF_CONTRA[0]", Value = "aaa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[2].DETAILS_COMMENTS[0]", Value = "asdf" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[2].BOD_DIR_REPLY[0]", Value = "asd" });

                    ////
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[3].NAME_RELATED_PAR[0]", Value = "" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[3].NATURE_OF_RELATN[0]", Value = "brother" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[3].NATURE_OF_CONTRA[0]", Value = "buy" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[3].DURATION_OF_CONT[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[3].DATE_OF_APPROVAL[0]", Value = "2021-11-30" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[3].AMOUNT_PAID[0]", Value = "121.00" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[3].DATE_SPCL_RESOLT[0]", Value = "2021-11-30" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[3].NAME_RELATED_PAR[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[3].NATURE_OF_RELATN[0]", Value = "aa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[3].NATURE_OF_CONTRA[0]", Value = "aaa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[3].DETAILS_COMMENTS[0]", Value = "asdf" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[3].BOD_DIR_REPLY[0]", Value = "asd" });


                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[4].NAME_RELATED_PAR[0]", Value = "" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[4].NATURE_OF_RELATN[0]", Value = "brother" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[4].NATURE_OF_CONTRA[0]", Value = "buy" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[4].DURATION_OF_CONT[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[4].DATE_OF_APPROVAL[0]", Value = "2021-11-30" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[4].AMOUNT_PAID[0]", Value = "121.00" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[4].DATE_SPCL_RESOLT[0]", Value = "2021-11-30" });

                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[4].NAME_RELATED_PAR[0]", Value = "a" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[4].NATURE_OF_RELATN[0]", Value = "aa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[4].NATURE_OF_CONTRA[0]", Value = "aaa" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[4].DETAILS_COMMENTS[0]", Value = "asdf" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].T_ZNCA_AOC_4_SV_I[0].DATA[4].BOD_DIR_REPLY[0]", Value = "asd" });
                    #endregion

                    #region Test
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMPTRL_AUDTR[0]", Value = "NO" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_CAG_CONDUCTED[0]", Value = "" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]", Value = "NO" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].NUMBER_OF_QUALIF[0]", Value = "" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_COMP_AUDT_REP[0]", Value = "YES" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].FIXED_ASSETS[0]", Value = "CNTA" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES[0]", Value = "CNTA" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].LOANS_GIVEN_COMP[0]", Value = "CNTA" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].ACCEPTANCE_PUB_D[0]", Value = "CNTA" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].MAINTENANCE_CR[0]", Value = "CNTA" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].STATUTORY_DUES[0]", Value = "FAVO" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS[0]", Value = "FAVO" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].FRAUD_NOTICED[0]", Value = "FAVO" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].OTHER_COMMENTS[0]", Value = "FAVO" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_SECRETARIAL_A[0]", Value = "NO" });
                    //lstFields.Add(new EFormFieldVM() { Name = "data[0].ZMCA_NCA_AOC_4[0].RB_DETAILED_DISC[0]", Value = "YES" });
                    #endregion
                }

                if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.MapPath(result.FullFileName)))
                {
                    SetData(result, lstFields);
                    result.Success = true;
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }
        #endregion
    }
}