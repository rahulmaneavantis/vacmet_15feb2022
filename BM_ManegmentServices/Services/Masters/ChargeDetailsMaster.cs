﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using System.Reflection;
using BM_ManegmentServices.Data;
using iTextSharp.text.pdf;
using System.Web.Hosting;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.IO;

namespace BM_ManegmentServices.Services.Masters
{
    public class ChargeDetailsMaster : IChargeDetailsMaster
    {

        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public List<ChargeDetails_VM> GetCharge(long chargeHeaderId)
        {
            List<ChargeDetails_VM> _objaddicomp = new List<ChargeDetails_VM>();
            try
            {
                _objaddicomp = (from row in entities.BM_chargedetails
                                join rows in entities.BM_ChargeType on row.Charge_TypeId equals rows.Id
                                where row.ChargeHeaderId == chargeHeaderId && row.IsActive == true
                                select new ChargeDetails_VM
                                {
                                    ChargeId = row.Charge_Id,
                                    ChargeTypeDesc = rows.Charge_TypeDesc,
                                    EntityId = row.Entity_Id,
                                    ChargeTypeId = row.Charge_TypeId,
                                    ChargeAmount = row.Charge_Amount,
                                    
                                    TermsconditionsOfcharge = row.Terms_conditionsOfcharge,
                                    Descinstrument = row.Desc_instrument,
                                    Descinstrumentchargemodify = row.Desc_instrument_chargemodify,
                                    ShortDescPropertyCharged = row.ShortDesc_PropertyCharged,
                                    Namesaddresseschargeholder = row.Names_addresses_chargeholder,
                                    Reasonsdelayfiling = row.Reasons_delay_filing,
                                    RegistrationChargeCreateDate = row.Registration_ChargeCreateDate,
                                    RegistrationChargeModificationDate = row.Registration_ChargeModificationDate,
                                    RegistrationsatisfactionDate = row.Registration_satisfactionDate,
                                    Particularsmodification = row.Particulars_modification,
                                    CreationDate = row.Charge_CreationDate,
                                    ModificationDate = row.Charge_Modification_Date,
                                    SatisfactionDate = row.Satisfaction_Date,
                                    InFavour = row.In_Favour,
                                    Status = row.Status,
                                    IsActive = row.IsActive,
                                    ChargeTerm = row.ChargeTerm,
                                    AddressOfChargeHolder = row.AddressOfChargeHolder,
                                    ChargeHeaderId = row.ChargeHeaderId,
                                }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objaddicomp;
        }

        public IEnumerable<ChargeTypeList_VM> GetAllChargeDetails()

        {
            List<ChargeTypeList_VM> ListofCharge = new List<ChargeTypeList_VM>();
            try
            {
                ListofCharge = (from row in entities.BM_ChargeType
                                select new ChargeTypeList_VM
                                {
                                    Id = row.Id,
                                    ChargeTypeDesc = row.Charge_TypeDesc
                                }).ToList();
                return ListofCharge;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return null;
        }

        public ChargeDetails_VM AddChargeDetails(ChargeDetails_VM _objadditionalcomp, int userId)
        {
            ChargeDetails_VM objadditioncom = new ChargeDetails_VM();
            try
            {
                bool sucess = false;
                BM_chargedetails _objcomp = new BM_chargedetails();


                //_objcomp.Charge_Id = _objadditionalcomp.ChargeId;
                _objcomp.Entity_Id = _objadditionalcomp.EntityId;
                _objcomp.Charge_TypeId = _objadditionalcomp.ChargeTypeId;
                _objcomp.Charge_Amount = _objadditionalcomp.ChargeAmount;
                _objcomp.ShortDesc_PropertyCharged = _objadditionalcomp.ShortDescPropertyCharged;
                _objcomp.Names_addresses_chargeholder = _objadditionalcomp.Namesaddresseschargeholder;
                _objcomp.Charge_Modification_Date = _objadditionalcomp.ModificationDate;
                _objcomp.Satisfaction_Date = _objadditionalcomp.SatisfactionDate;
                _objcomp.Charge_CreationDate = _objadditionalcomp.CreationDate;
                _objcomp.In_Favour = _objadditionalcomp.InFavour;
                _objcomp.Status = _objadditionalcomp.Status;
                _objcomp.IsActive = true;
                _objcomp.CreatedBy = userId;


                entities.BM_chargedetails.Add(_objcomp);
                entities.SaveChanges();

                sucess = true;


                if (sucess)
                {
                    objadditioncom.Success = true;
                    objadditioncom.Message = SecretarialConst.Messages.saveSuccess;
                }
                else
                {
                    objadditioncom.Success = false;
                    objadditioncom.Message = SecretarialConst.Messages.alreadyExist;
                }
            }

            catch (Exception ex)
            {
                objadditioncom.Error = true;
                objadditioncom.Message = SecretarialConst.Messages.serverError;
            }
            return objadditioncom;
        }



        public ChargeDetails_VM EditChargeDetails(int Id, int entityId)
        {
            ChargeDetails_VM objadditcom = new ChargeDetails_VM();
            try
            {
                objadditcom = (from row in entities.BM_chargedetails
                               where row.Charge_Id == Id
                               select new ChargeDetails_VM
                               {
                                   ChargeId = row.Charge_Id,
                                   EntityId = row.Entity_Id,
                                   ChargeTypeId = row.Charge_TypeId,
                                   ChargeAmount = row.Charge_Amount,
                                   CreationDate = row.Charge_CreationDate,
                                   ModificationDate = row.Charge_Modification_Date,
                                   SatisfactionDate = row.Satisfaction_Date,
                                   InFavour = row.In_Favour,
                                   Status = row.Status,
                                   ShortDescPropertyCharged = row.ShortDesc_PropertyCharged,
                                   Namesaddresseschargeholder = row.Names_addresses_chargeholder,
                                   TermsconditionsOfcharge = row.Terms_conditionsOfcharge,
                                   Descinstrument = row.Desc_instrument,
                                   RegistrationChargeModificationDate = row.Registration_ChargeModificationDate,
                                   Descinstrumentchargemodify = row.Desc_instrument_chargemodify,
                                   Particularsmodification = row.Particulars_modification,
                                   RegistrationsatisfactionDate = row.Registration_satisfactionDate,
                                   FactsDelaycondonationDate = row.Facts_Delaycondonation_Date,
                                   Reasonsdelayfiling = row.Reasons_delay_filing,
                                   ChargeHeaderId = row.ChargeHeaderId,
                                   IsActive = row.IsActive,
                                   CreatedBy = row.CreatedBy,
                                   ChargeTerm = row.ChargeTerm,
                                   AddressOfChargeHolder = row.AddressOfChargeHolder,
                                   RegistrationChargeCreateDate = row.Registration_ChargeCreateDate,

                               }).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return objadditcom;
        }

        public ChargeDetails_VM UpdateChargeDetails(ChargeDetails_VM _objadditionalcomp, int userId)
        {
            try
            {
                BM_chargedetails _objcomp = new BM_chargedetails();
                var checkChargeexist = (from row in entities.BM_chargedetails where row.Charge_Id == _objadditionalcomp.ChargeId select row).FirstOrDefault();

                if (checkChargeexist != null)
                {
                    checkChargeexist.Charge_Id = _objadditionalcomp.ChargeId;
                    //checkChargeexist.Entity_Id= _objadditionalcomp.EntityId;
                    checkChargeexist.Charge_TypeId = _objadditionalcomp.ChargeTypeId;
                    checkChargeexist.Charge_Amount = _objadditionalcomp.ChargeAmount;
                    checkChargeexist.ShortDesc_PropertyCharged = _objadditionalcomp.ShortDescPropertyCharged;
                    checkChargeexist.Terms_conditionsOfcharge = _objadditionalcomp.TermsconditionsOfcharge;
                    checkChargeexist.Desc_instrument = _objadditionalcomp.Descinstrument;
                    checkChargeexist.Registration_ChargeModificationDate = _objadditionalcomp.RegistrationChargeModificationDate;
                    checkChargeexist.Desc_instrument_chargemodify = _objadditionalcomp.Descinstrumentchargemodify;
                    checkChargeexist.Particulars_modification = _objadditionalcomp.Particularsmodification;
                    checkChargeexist.Registration_satisfactionDate = _objadditionalcomp.RegistrationsatisfactionDate;
                    checkChargeexist.Facts_Delaycondonation_Date = _objadditionalcomp.FactsDelaycondonationDate;
                    checkChargeexist.Reasons_delay_filing = _objadditionalcomp.Reasonsdelayfiling;
                    checkChargeexist.Charge_CreationDate = _objadditionalcomp.CreationDate;
                    checkChargeexist.Charge_Modification_Date = _objadditionalcomp.ModificationDate;
                    checkChargeexist.Satisfaction_Date = _objadditionalcomp.SatisfactionDate;
                    checkChargeexist.In_Favour = _objadditionalcomp.InFavour;
                    checkChargeexist.Status = _objadditionalcomp.Status;
                    checkChargeexist.UpdatedOn = DateTime.Now;
                    checkChargeexist.UpdatedBy = userId;
                    checkChargeexist.IsActive = true;
                    checkChargeexist.Registration_ChargeCreateDate = _objadditionalcomp.RegistrationChargeCreateDate;
                    checkChargeexist.ChargeTerm = _objadditionalcomp.ChargeTerm;
                    checkChargeexist.Names_addresses_chargeholder = _objadditionalcomp.Namesaddresseschargeholder;
                    checkChargeexist.AddressOfChargeHolder = _objadditionalcomp.AddressOfChargeHolder;
                    checkChargeexist.Particulars_modification = _objadditionalcomp.Particularsmodification;
                    checkChargeexist.Registration_satisfactionDate = _objadditionalcomp.RegistrationsatisfactionDate;

                    entities.SaveChanges();
                    _objadditionalcomp.Success = true;
                    _objadditionalcomp.Message = SecretarialConst.Messages.updateSuccess;
                }
                else
                {
                    entities.BM_chargedetails.Add(_objcomp);
                    entities.SaveChanges();
                    _objadditionalcomp.Error = true;
                    _objadditionalcomp.Message = SecretarialConst.Messages.noRecordFound;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objadditionalcomp;
        }
        public Message ChecksaveUpdateForChargeMaster(string chargeId, string Charge_TypeDesc, int entityId, BM_chargedetails objauditor)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var chargeHeader = (from row in entities.BM_ChargeHeader
                                     where row.EntityId == entityId && row.ChargeID == chargeId && 
                                     row.IsDeleted == false && row.IsActive == true
                                     select row).FirstOrDefault();
                    if(chargeHeader == null)
                    {
                        chargeHeader = new BM_ChargeHeader();
                        chargeHeader.ChargeID = chargeId;
                        chargeHeader.EntityId = entityId;
                        chargeHeader.CreationDate = objauditor.Charge_CreationDate;

                        chargeHeader.IsActive = true;
                        chargeHeader.IsDeleted = false;
                        chargeHeader.CreatedBy = objauditor.CreatedBy;
                        chargeHeader.CreatedOn = DateTime.Now;
                        chargeHeader.Charge_Amount = "0";

                        chargeHeader.StatusId = 1;

                        entities.BM_ChargeHeader.Add(chargeHeader);
                        entities.SaveChanges();
                    }

                    if(chargeHeader.Id > 0 && chargeHeader.StatusId != 3)
                    {
                        BM_chargedetails objauditors = new BM_chargedetails();

                        var ChargeTypeId = (from row in entities.BM_ChargeType
                                            where row.Charge_TypeDesc == Charge_TypeDesc
                                            select row.Id).FirstOrDefault();

                        objauditors.ChargeHeaderId = chargeHeader.Id;
                        objauditors.Charge_TypeId = ChargeTypeId;
                        objauditors.Charge_CreationDate = objauditor.Charge_CreationDate;
                        objauditors.Registration_ChargeCreateDate = objauditor.Registration_ChargeCreateDate;
                        objauditors.Charge_Amount = objauditor.Charge_Amount;
                        objauditors.ShortDesc_PropertyCharged = objauditor.ShortDesc_PropertyCharged;
                        objauditors.Names_addresses_chargeholder = objauditor.Names_addresses_chargeholder;
                        objauditors.Terms_conditionsOfcharge = objauditor.Terms_conditionsOfcharge;
                        objauditors.Desc_instrument = objauditor.Desc_instrument;
                        objauditors.Charge_Modification_Date = objauditor.Charge_Modification_Date;
                        objauditors.Registration_ChargeModificationDate = objauditor.Registration_ChargeModificationDate;
                        objauditors.Desc_instrument_chargemodify = objauditor.Desc_instrument_chargemodify;
                        objauditors.Particulars_modification = objauditor.Particulars_modification;
                        objauditors.Satisfaction_Date = objauditor.Satisfaction_Date;
                        objauditors.Registration_satisfactionDate = objauditor.Registration_satisfactionDate;
                        objauditors.Facts_Delaycondonation_Date = objauditor.Facts_Delaycondonation_Date;
                        objauditors.Reasons_delay_filing = objauditor.Reasons_delay_filing;
                        objauditors.In_Favour = objauditor.In_Favour;

                        objauditors.ChargeTerm = objauditor.ChargeTerm;
                        objauditors.AddressOfChargeHolder = objauditor.AddressOfChargeHolder;

                        objauditors.Charge_Id = objauditor.Charge_Id;
                        objauditors.Entity_Id = objauditor.Entity_Id;
                        objauditors.IsActive = true;
                        objauditors.CreatedBy = objauditor.CreatedBy;
                        objauditors.Entity_Id = entityId;

                        entities.BM_chargedetails.Add(objauditors);
                        entities.SaveChanges();

                        int StatusIDNew = objauditors.Satisfaction_Date != null ? 3 : (objauditors.Charge_Modification_Date != null ? 2 : 1);

                        var objBM_ChargeHeader = (from row in entities.BM_ChargeHeader
                                            where row.Id == chargeHeader.Id
                                            select row).FirstOrDefault();
                        if(objBM_ChargeHeader != null)
                        {
                            if(objBM_ChargeHeader.StatusId != 3)
                            {
                                if(StatusIDNew >= objBM_ChargeHeader.StatusId)
                                {
                                    objBM_ChargeHeader.StatusId = StatusIDNew;
                                    objBM_ChargeHeader.Charge_Amount = objauditors.Charge_Amount;
                                    objBM_ChargeHeader.Names_addresses_chargeholder = objauditors.Names_addresses_chargeholder;
                                }

                                if (StatusIDNew == 2 && !string.IsNullOrEmpty(objauditor.Charge_Amount))
                                {
                                    objBM_ChargeHeader.Charge_Amount = objauditor.Charge_Amount;
                                }

                                if (StatusIDNew == 2)
                                {
                                    objBM_ChargeHeader.ModificationDate = objauditor.Charge_Modification_Date;
                                }
                                else if (StatusIDNew == 3)
                                {
                                    objBM_ChargeHeader.SatisficationDate = objauditors.Satisfaction_Date;
                                }

                                result.Success = true;
                                result.Message = SecretarialConst.Messages.saveSuccess;
                            }
                            else
                            {
                                result.Error = true;
                                result.Message = "Satisfication data already exists for " + chargeId +" Charge Id";
                            }

                            objBM_ChargeHeader.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public bool DeleteChargeDetails(int id, int userId)
        {
            bool success = false;
            try
            {

                var checkChargeexist = (from row in entities.BM_chargedetails where row.Charge_Id == id select row).FirstOrDefault();

                if (checkChargeexist != null)
                {
                    checkChargeexist.IsActive = false;
                    checkChargeexist.UpdatedOn = DateTime.Now;
                    checkChargeexist.UpdatedBy = userId;
                    entities.SaveChanges();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        public ChargeDetails_VM DetailsChargeDetails(int Id, int ChargeId)
        {
            ChargeDetails_VM objadditcom = new ChargeDetails_VM();
            try
            {
                objadditcom = (from row in entities.BM_chargedetails
                               where row.Charge_Id == Id
                               select new ChargeDetails_VM
                               {
                                   ChargeId = row.Charge_Id,
                                   //EntityId = row.Entity_Id,
                                   //ChargeTypeId = row.Charge_TypeId,
                                   //ChargeAmount = row.Charge_Amount,
                                   CreationDate = row.Charge_CreationDate,
                                   ModificationDate = row.Charge_Modification_Date,
                                   SatisfactionDate = row.Satisfaction_Date,
                                   //InFavour = row.In_Favour,

                                   //Status = row.Status,

                               }).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return objadditcom;
        }

        #region Upload Eform
        public ChargeUploadEFormVM UploadChargeEform(ChargeUploadEFormVM obj, int userId)
        {
            try
            {
                if (obj.File != null)
                {
                    if (obj.File.ContentLength > 0)
                    {
                        string myFilePath = obj.File.FileName;
                        string ext = System.IO.Path.GetExtension(myFilePath);
                        if (ext == ".pdf")
                        {
                            //string _path;
                            //_path = GetFormsPath(objfileupload);
                            //if (!(string.IsNullOrEmpty(_path)))
                            //{
                            //    PdfReader pdfReader = new PdfReader(_path);
                            //    if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                            //    {
                            //        foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                            //        {

                            //        }
                            //    }
                            //}

                            PdfReader pdfReader = new PdfReader(obj.File.InputStream);
                            if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                            {
                                string mode = string.Empty;
                                string chargeId = string.Empty;
                                string shortDescription = string.Empty;

                                string amount = string.Empty;
                                string termsInMonth = string.Empty;

                                string nameOfChargeHolder = string.Empty;
                                string addressOfChargeHolder = string.Empty;
                                string addressLine1 = string.Empty;
                                string addressLine2 = string.Empty;
                                string City = string.Empty;
                                string State = string.Empty;
                                string pin = string.Empty;
                                string termsAndConditions = string.Empty;

                                DateTime? modificationDate = null;
                                string purticularsOfModification = string.Empty;
                                string desc_instrument_chargemodify = string.Empty;

                                DateTime? satisficationDate = null;
                                string reasonForDelay = string.Empty;

                                #region SHG -1 
                                if (obj.fromTypeCharge == "SHG1")
                                {
                                    foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                    {

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].RB_FORM_FOR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                mode = (de.Value.InnerText).Trim();
                                            }
                                        }

                                        //Charge Id
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].CHARGE_ID[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                chargeId = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        //shortDescription
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].SHORT_PARTICLRS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                shortDescription = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        //Amount
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].AMT_SECURD_CHARG[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                amount = (de.Value.InnerText).Trim();
                                            }
                                        }

                                        //terms in Months
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG1_II[0].REPAYMENT_TERM[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                termsInMonth = (de.Value.InnerText).Trim();
                                            }
                                        }

                                        //Name of Charge holder
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].NAME_CHARGE_HOLD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                nameOfChargeHolder = (de.Value.InnerText).Trim();
                                            }
                                        }

                                        //addressOfChargeHolder
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].ADDRESS_LINE1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                addressLine1 = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].ADDRESS_LINE2[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                addressLine2 = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].CITY[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                City = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].STATE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                State = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].PIN_CODE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                pin = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        //

                                        //terms & condition
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].EXTENT_OPERTN_CH[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                termsAndConditions = (de.Value.InnerText).Trim();
                                            }
                                        }

                                        //modification date
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].DATE_INSTRUMENT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                modificationDate = Convert.ToDateTime(de.Value.InnerText);
                                            }
                                        }

                                        //purticulars of modification
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].PARTICLRS_PRES_M[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                purticularsOfModification = (de.Value.InnerText).Trim();
                                            }
                                        }

                                        //Description of the instrument modifying the charge
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_1[0].DESC_INSTRUMENT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                desc_instrument_chargemodify = (de.Value.InnerText).Trim();
                                            }
                                        }

                                    }

                                    if (mode != "MDFN") //CRTN - Creation
                                    {
                                        obj.Error = true;
                                        obj.Message = "CHG-1 modification data not found";
                                        return obj;
                                    }
                                }
                                #endregion

                                #region SHG -4
                                if (obj.fromTypeCharge == "SHG4")
                                {
                                    foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                    {

                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_4[0].RB_REGISTRATION[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                mode = (de.Value.InnerText).Trim();
                                            }
                                        }

                                        //Charge Id
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_4[0].CHARGE_ID[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                chargeId = (de.Value.InnerText).Trim();
                                            }
                                        }
                                        //Date Of Satisfication
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_4[0].DATE_DECLARATION[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                satisficationDate = Convert.ToDateTime(de.Value.InnerText);
                                            }
                                        }
                                        //reson for delay
                                        if (de.Key.ToString() == "data[0].ZMCA_NCA_CHG_4[0].REASONS_FOR_DLAY[0]")
                                        {
                                            if (!string.IsNullOrEmpty(de.Value.InnerText))
                                            {
                                                reasonForDelay = (de.Value.InnerText).Trim();
                                            }
                                        }
                                    }

                                    if (mode != "SRTN") // Satisfication
                                    {
                                        obj.Error = true;
                                        obj.Message = "CHG-4 satisfication data not found";
                                        return obj;
                                    }

                                }
                                #endregion
                                
                                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                                {
                                    #region check charge Id exists or not & save modification & satisfication
                                    var objBM_ChargeHeader = (from row in entities.BM_ChargeHeader
                                                              where row.EntityId == obj.EntityId && row.ChargeID == chargeId &&
                                                              row.IsDeleted == false && row.IsActive == true
                                                              select row).FirstOrDefault();
                                    if (objBM_ChargeHeader != null)
                                    {
                                        var objChargeDetailstemp = (from row in entities.BM_chargedetails
                                                                    where row.ChargeHeaderId == objBM_ChargeHeader.Id && row.IsActive == true
                                                                    select new
                                                                    {
                                                                        row.Charge_Id,
                                                                        row.ChargeHeaderId,
                                                                        row.Entity_Id,
                                                                        row.Charge_TypeId,
                                                                        row.Charge_CreationDate,
                                                                        row.Registration_ChargeCreateDate,
                                                                        row.ShortDesc_PropertyCharged,
                                                                        row.Charge_Amount,
                                                                        row.Names_addresses_chargeholder,
                                                                        row.Terms_conditionsOfcharge,
                                                                        row.Desc_instrument,
                                                                        row.Charge_Modification_Date,
                                                                        row.Registration_ChargeModificationDate,
                                                                        row.Desc_instrument_chargemodify,
                                                                        row.Particulars_modification,
                                                                        row.Satisfaction_Date,
                                                                        row.Registration_satisfactionDate,
                                                                        row.Facts_Delaycondonation_Date,
                                                                        row.Reasons_delay_filing,
                                                                        row.In_Favour,
                                                                        row.Status,
                                                                        row.IsActive,
                                                                        row.CreatedBy
                                                                    }).OrderByDescending(row => row.Charge_Id).FirstOrDefault();

                                        if (objChargeDetailstemp != null)
                                        {
                                            #region Charge Details
                                            BM_chargedetails objChargeDetails = new BM_chargedetails();

                                            objChargeDetails.Charge_Id = 0;
                                            objChargeDetails.ChargeHeaderId = objBM_ChargeHeader.Id;

                                            objChargeDetails.Entity_Id = objChargeDetailstemp.Entity_Id;
                                            objChargeDetails.Charge_TypeId = objChargeDetailstemp.Charge_TypeId;
                                            objChargeDetails.Charge_CreationDate = objChargeDetailstemp.Charge_CreationDate;
                                            objChargeDetails.Registration_ChargeCreateDate = objChargeDetailstemp.Registration_ChargeCreateDate;
                                            objChargeDetails.ShortDesc_PropertyCharged = objChargeDetailstemp.ShortDesc_PropertyCharged;
                                            objChargeDetails.Charge_Amount = objChargeDetailstemp.Charge_Amount;
                                            objChargeDetails.Names_addresses_chargeholder = objChargeDetailstemp.Names_addresses_chargeholder;
                                            objChargeDetails.Terms_conditionsOfcharge = objChargeDetailstemp.Terms_conditionsOfcharge;
                                            objChargeDetails.Desc_instrument = objChargeDetailstemp.Desc_instrument;
                                            objChargeDetails.Charge_Modification_Date = objChargeDetailstemp.Charge_Modification_Date;
                                            objChargeDetails.Registration_ChargeModificationDate = objChargeDetailstemp.Registration_ChargeModificationDate;
                                            objChargeDetails.Desc_instrument_chargemodify = objChargeDetailstemp.Desc_instrument_chargemodify;
                                            objChargeDetails.Particulars_modification = objChargeDetailstemp.Particulars_modification;

                                            objChargeDetails.In_Favour = objChargeDetailstemp.In_Favour;
                                            objChargeDetails.Status = objChargeDetailstemp.Status;
                                            objChargeDetails.IsActive = true;
                                            objChargeDetails.CreatedBy = userId;


                                            int statuIdNew = 0;
                                            //Modification form
                                            if (mode == "MDFN") //CRTN - Creation
                                            {
                                                objChargeDetails.ShortDesc_PropertyCharged = shortDescription;
                                                objChargeDetails.ChargeTerm = termsInMonth;
                                                objChargeDetails.Charge_Amount = amount;

                                                objChargeDetails.Names_addresses_chargeholder = nameOfChargeHolder;
                                                objChargeDetails.AddressOfChargeHolder = addressOfChargeHolder;
                                                objChargeDetails.Terms_conditionsOfcharge = termsAndConditions;

                                                objChargeDetails.Charge_Modification_Date = modificationDate;
                                                objChargeDetails.Particulars_modification = purticularsOfModification;
                                                objChargeDetails.Desc_instrument_chargemodify = desc_instrument_chargemodify;
                                                objChargeDetails.Status = true;

                                                addressOfChargeHolder = (string.IsNullOrEmpty(addressLine1) ? "" : addressLine1 + ", ") +
                                                                        (string.IsNullOrEmpty(addressLine2) ? "" : addressLine2 + ", ") +
                                                                        (string.IsNullOrEmpty(State) ? "" : State + ", ") +
                                                                        (string.IsNullOrEmpty(City) ? "" : City + " ") +
                                                                        (string.IsNullOrEmpty(pin) ? "" : "-"+pin);

                                                objChargeDetails.AddressOfChargeHolder = addressOfChargeHolder;
                                                statuIdNew = 2;
                                            }
                                            else if (mode == "SRTN") // Satisfication
                                            {
                                                objChargeDetails.Status = false;

                                                objChargeDetails.Satisfaction_Date = satisficationDate;
                                                objChargeDetails.Registration_satisfactionDate = satisficationDate;
                                                objChargeDetails.Facts_Delaycondonation_Date = objChargeDetailstemp.Facts_Delaycondonation_Date;
                                                objChargeDetails.Reasons_delay_filing = reasonForDelay;

                                                statuIdNew = 3;
                                            }

                                            var addChargeDetails = false;

                                            if (objBM_ChargeHeader.StatusId != 3)
                                            {
                                                if (statuIdNew >= objBM_ChargeHeader.StatusId)
                                                {
                                                    objBM_ChargeHeader.StatusId = statuIdNew;
                                                    addChargeDetails = true;
                                                }

                                                if(statuIdNew == 2 && !string.IsNullOrEmpty(amount))
                                                {
                                                    objBM_ChargeHeader.Charge_Amount = amount;
                                                }

                                                if (statuIdNew == 2)
                                                {
                                                    objBM_ChargeHeader.ModificationDate = modificationDate;
                                                }
                                                else if (statuIdNew == 3)
                                                {
                                                    objBM_ChargeHeader.SatisficationDate = satisficationDate;
                                                }

                                                objBM_ChargeHeader.UpdatedBy = userId;
                                                objBM_ChargeHeader.UpdatedOn = DateTime.Now;

                                                entities.SaveChanges();

                                                if (addChargeDetails)
                                                {
                                                    entities.BM_chargedetails.Add(objChargeDetails);
                                                    entities.SaveChanges();
                                                }

                                                obj.Success = true;
                                                obj.Message = SecretarialConst.Messages.saveSuccess;
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        public List<ChargeHeaderVM> GetChargeIndex(int entityId)
        {
            var result = new List<ChargeHeaderVM>();
            try
            {
                result = (from row in entities.BM_ChargeHeader
                                where row.EntityId == entityId && row.IsActive == true && row.IsDeleted == false
                                select new ChargeHeaderVM
                                {
                                    ChargeHeaderId = row.Id,
                                    ChargeId = row.ChargeID,
                                    CreationDate = row.CreationDate,
                                    ModificationDate = row.ModificationDate,
                                    SatisficationDate = row.SatisficationDate,
                                    ChargeAmount = row.Charge_Amount,
                                    ChargeHolder = row.Names_addresses_chargeholder,
                                    StatusId = row.StatusId,
                                }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public EntityDocumentVM Savechargedocument(EntityDocumentVM _objdoc, int userId, int customerId)
        {
            try
            {
                if (_objdoc.files != null)
                {                   
                        BM_EntityDocuments _objdocdetails = new BM_EntityDocuments
                        {
                            Description = _objdoc.Discription,
                            License_RegNumber = _objdoc.LicenceNo,
                            FYID = _objdoc.FYID,
                            StartTimeAGM = _objdoc.startTiming,
                            EndTimeAGM = _objdoc.EndTiming,
                            BMDate_LastRenew = _objdoc.BMDate,
                            GMDate_UpcomingRenew = _objdoc.GMDate,

                            IsActive = true,
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now,
                            CustomerId = customerId,
                            EntityId = _objdoc.EntityId,
                            ChargeHeaderId = _objdoc.ChargeHeaderId,
                            ChargeDetailId = _objdoc.ChargeDetailId,
                            ParticularChargeId = _objdoc.PId,                         
                        };
                        entities.BM_EntityDocuments.Add(_objdocdetails);
                        entities.SaveChanges();
                    if (_objdocdetails.Id > 0 && _objdoc.files != null)
                    {
                        if (_objdoc.files.Count() > 0)
                        {
                            bool savechange = FileUpload.SaveEntityDocument(_objdoc.files, _objdocdetails.Id, userId, customerId, "CHG", _objdoc.EntityId); _objdoc.Success = true;
                            _objdoc.Message = "Record saved successfully";
                        }
                    }
                }
                else
                {
                    _objdoc.Error = true;
                    _objdoc.Message = "Please select File";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return _objdoc;
        }
        public List<BM_SP_GetEntityChargeDocuments_Result> GetChargeDocuments(long chargeDetailsID, int entityId)
        {
            List<BM_ManegmentServices.Data.BM_SP_GetEntityChargeDocuments_Result> _obj = new List<BM_ManegmentServices.Data.BM_SP_GetEntityChargeDocuments_Result>();
            _obj = (from x in entities.BM_SP_GetEntityChargeDocuments(chargeDetailsID, entityId)
                    select x).ToList();
            return _obj;
        }

        public IEnumerable<EntityDocumentVM> GetParticularDocument()
        {
            IEnumerable<EntityDocumentVM> _objparticular = new List<EntityDocumentVM>();
             _objparticular = (from row in entities.BM_ParticularCharge
                             select new EntityDocumentVM
                             {
                               PId = row.ID,
                               ParticularChargeName = row.ParticularChargeName
                            }).ToList();

            return _objparticular;
        }

    }
}