﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IRegister
    {
        List<VMRegisters> GetRegisterList();
        VMRegisters getRegisterbyId(int id);
        IEnumerable<EntityTypeVM> GetEntityType();
        VMRegisters AddRegister(VMRegisters _objregister);
        VMRegisters UpdateRegister(VMRegisters _objregister);
        List<RegistersLists> GetRegisterList(long entityId);
    }
}
