﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public class MBP2TransactionService : IMBP2TransactionService
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public List<MBP2TransactionNatureVM> GetNatureOfTransactions()
        {
            List<MBP2TransactionNatureVM> result = null;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Entity_MBP2_TransactionNature 
                              where row.IsActive == true
                              select new MBP2TransactionNatureVM
                              {
                                  Id = (int)row.Id,
                                  NatureOfTransaction = row.Name 
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<MBP2TransactionVM> GetTransactions(int entityId, string view)
        {
            List<MBP2TransactionVM> result = null;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var result1 = (from row in entities.BM_Entity_MBP2_Transaction
                                  join n in entities.BM_Entity_MBP2_TransactionNature on (int)row.NatureOfTransactionId equals n.Id
                                  where row.EntityId == entityId && row.IsDeleted == false && row.IsActive
                                  select new MBP2TransactionVM
                                  {
                                      MBP2TransId = row.Id,
                                      EntityId = row.EntityId,
                                      NatureOfTransactionId = row.NatureOfTransactionId,
                                      NatureOfTransaction = n.Name,
                                      DateOfMaking = row.DateOfMaking,
                                      Name_ = row.Name_,
                                      Address_ = row.Address_,
                                      Amount = row.Amount,
                                      TimePeriod = row.TimePeriod,
                                      Purpose = row.Purpose,
                                      PercentageOfReserve = row.PercentageOfReserve,
                                      DateOfBoardResolution = row.DateOfBoardResolution,
                                      DateOfPassingResolution = row.DateOfPassingResolution,
                                      RateOfInterest = row.RateOfInterest,
                                      DateOfMaturity = row.DateOfMaturity,
                                      NoOfSecurity = row.NoOfSecurity,
                                      KindOfSecurity = row.KindOfSecurity,
                                      NominalValue = row.NominalValue,
                                      PaidUp = row.PaidUp,
                                      CostOfAuisition = row.CostOfAuisition,
                                      DateOfSelling = row.DateOfSelling,
                                      SellingPrice = row.SellingPrice,
                                      Remarks = row.Remarks,
                                      LastModified = row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn
                                  });

                    if(view == "Transaction")
                    {
                        result = result1.OrderByDescending(k => k.LastModified).ToList();
                    }
                    else
                    {
                        result = result1.OrderBy(k => k.DateOfMaking).ThenBy(k => k.LastModified).ToList();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public MBP2TransactionVM GetTransaction(long id, int entityId)
        {
            var result = new MBP2TransactionVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Entity_MBP2_Transaction
                              where row.Id == id && row.EntityId == entityId && row.IsDeleted == false && row.IsActive == true
                              select new MBP2TransactionVM
                              {
                                  MBP2TransId = row.Id,
                                  EntityId = row.EntityId,
                                  NatureOfTransactionId = row.NatureOfTransactionId,
                                  DateOfMaking = row.DateOfMaking,
                                  Name_ = row.Name_,
                                  Address_ = row.Address_,
                                  Amount = row.Amount,
                                  TimePeriod = row.TimePeriod,
                                  Purpose = row.Purpose,
                                  PercentageOfReserve = row.PercentageOfReserve,
                                  DateOfBoardResolution = row.DateOfBoardResolution,
                                  DateOfPassingResolution = row.DateOfPassingResolution,
                                  RateOfInterest = row.RateOfInterest,
                                  DateOfMaturity = row.DateOfMaturity,
                                  NoOfSecurity = row.NoOfSecurity,
                                  KindOfSecurity = row.KindOfSecurity,
                                  NominalValue = row.NominalValue,
                                  PaidUp = row.PaidUp,
                                  CostOfAuisition = row.CostOfAuisition,
                                  DateOfSelling = row.DateOfSelling,
                                  SellingPrice = row.SellingPrice,
                                  Remarks = row.Remarks
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public EntityMasterVM GetCompanyName(int EntityId)
        {
            EntityMasterVM result = null;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_EntityMaster
                              where row.Id == EntityId
                              select new EntityMasterVM
                              {
                                  EntityName = row.CompanyName,
                                  CIN = row.CIN_LLPIN,
                                  RegisterAddress = row.Regi_Address_Line1 + " " + row.Regi_Address_Line2,
                                  emailId = row.Email_Id

                              }                            
                              ).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public MBP2TransactionVM CreateUpdateTransaction(MBP2TransactionVM obj, int createdBy)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if(obj.MBP2TransId > 0)
                    {
                        #region Update
                        var _obj = (from row in entities.BM_Entity_MBP2_Transaction
                                    where row.Id == obj.MBP2TransId && row.IsDeleted == false
                                    select row).FirstOrDefault();
                        if(_obj != null)
                        {
                            _obj.Id = obj.MBP2TransId;
                            _obj.EntityId = obj.EntityId;
                            _obj.NatureOfTransactionId = obj.NatureOfTransactionId;
                            _obj.DateOfMaking = obj.DateOfMaking;
                            _obj.Name_ = obj.Name_;
                            _obj.Address_ = obj.Address_;
                            _obj.Amount = obj.Amount;
                            _obj.TimePeriod = obj.TimePeriod;
                            _obj.Purpose = obj.Purpose;
                            _obj.PercentageOfReserve = obj.PercentageOfReserve;
                            _obj.DateOfBoardResolution = obj.DateOfBoardResolution;
                            _obj.DateOfPassingResolution = obj.DateOfPassingResolution;
                            _obj.RateOfInterest = obj.RateOfInterest;
                            _obj.DateOfMaturity = obj.DateOfMaturity;
                            _obj.NoOfSecurity = obj.NoOfSecurity;
                            _obj.KindOfSecurity = obj.KindOfSecurity;
                            _obj.NominalValue = obj.NominalValue;
                            _obj.PaidUp = obj.PaidUp;
                            _obj.CostOfAuisition = obj.CostOfAuisition;
                            _obj.DateOfSelling = obj.DateOfSelling;
                            _obj.SellingPrice = obj.SellingPrice;
                            _obj.Remarks = obj.Remarks;

                            _obj.IsActive = true;
                            _obj.IsDeleted = false;
                            _obj.UpdatedBy = createdBy;
                            _obj.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();

                            obj.Success = true;
                            obj.Message = SecretarialConst.Messages.updateSuccess;
                        }
                        else
                        {
                            obj.Error = true;
                            obj.Message = SecretarialConst.Messages.serverError;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Create
                        var _obj = new BM_Entity_MBP2_Transaction();
                        _obj.Id = obj.MBP2TransId;
                        _obj.EntityId = obj.EntityId;
                        _obj.NatureOfTransactionId = obj.NatureOfTransactionId;
                        _obj.DateOfMaking = obj.DateOfMaking;
                        _obj.Name_ = obj.Name_;
                        _obj.Address_ = obj.Address_;
                        _obj.Amount = obj.Amount;
                        _obj.TimePeriod = obj.TimePeriod;
                        _obj.Purpose = obj.Purpose;
                        _obj.PercentageOfReserve = obj.PercentageOfReserve;
                        _obj.DateOfBoardResolution = obj.DateOfBoardResolution;
                        _obj.DateOfPassingResolution = obj.DateOfPassingResolution;
                        _obj.RateOfInterest = obj.RateOfInterest;
                        _obj.DateOfMaturity = obj.DateOfMaturity;
                        _obj.NoOfSecurity = obj.NoOfSecurity;
                        _obj.KindOfSecurity = obj.KindOfSecurity;
                        _obj.NominalValue = obj.NominalValue;
                        _obj.PaidUp = obj.PaidUp;
                        _obj.CostOfAuisition = obj.CostOfAuisition;
                        _obj.DateOfSelling = obj.DateOfSelling;
                        _obj.SellingPrice = obj.SellingPrice;
                        _obj.Remarks = obj.Remarks;

                        _obj.IsActive = true;
                        _obj.IsDeleted = false;
                        _obj.CreatedBy = createdBy;
                        _obj.CreatedOn = DateTime.Now;

                        entities.BM_Entity_MBP2_Transaction.Add(_obj);
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Message DeleteTransaction(int id, int entityId, int deletedBy)
        {
            var obj = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    #region set deleted
                    var _obj = (from row in entities.BM_Entity_MBP2_Transaction
                                where row.Id == id && row.EntityId == entityId && row.IsDeleted == false
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.IsActive = false;
                        _obj.IsDeleted = true;
                        _obj.UpdatedBy = deletedBy;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.deleteSuccess;
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = SecretarialConst.Messages.serverError;
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        #region Import Excel (Private, Public, Listed)
        public MBP2TransactionUploadVM UploadExcel(MBP2TransactionUploadVM objfileupload, int customerId, int userId)
        {
            objfileupload.Success = true;

            List<string> message;
            string Message;
            if (objfileupload.File != null)
            {
                if (objfileupload.File.ContentLength > 0)
                {
                    string excelfileName = string.Empty;
                    string path = "~/Areas/BM_Management/Documents/" + customerId + "/EntityMaster/";
                    string _file_Name = DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss_") + System.IO.Path.GetFileName(objfileupload.File.FileName);
                    string _path = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(path), _file_Name);
                    string ext = System.IO.Path.GetExtension(_file_Name);
                    if (ext == ".xlsx")
                    {
                        bool exists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(path));
                        if (!exists)
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
                        objfileupload.File.SaveAs(_path);

                        FileInfo excelfile = new FileInfo(_path);

                        if (excelfile != null)
                        {
                            using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                            {
                                bool flagPublic = false;
                                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                                {
                                    if (sheet.Name.Trim().Equals("MBP-2 Transaction"))
                                    {
                                        flagPublic = true;
                                    }
                                }

                                if (flagPublic == true)
                                {
                                    var result = UploadMBP2Excel(xlWorkbook, objfileupload.EntityId, customerId, userId, out message);
                                    objfileupload.Message = result.Message;
                                    objfileupload.Success = result.Success;
                                    objfileupload.Error = result.Error;
                                    objfileupload.lstMessage = message;
                                }
                                else
                                {
                                    Message = "No Data Found in Excel Document or Sheet Name must be 'Entity Master'";
                                    objfileupload.Message = Message;
                                }
                            }
                        }
                    }
                    else
                    {
                        Message = "File is only in Excel Format'";
                        objfileupload.Message = Message;
                    }
                }
            }
            return objfileupload;
        }
        private Message UploadMBP2Excel(ExcelPackage xlWorkbook, int entityid, int customerId, int userId, out List<string> errorMessage)
        {
            errorMessage = new List<string>();
            var result = new Message();
            var lstObj = new List<MBP2TransactionVM>();
            try
            {
                ExcelWorksheet excelMBP2Data = xlWorkbook.Workbook.Worksheets["MBP-2 Transaction"];

                if (excelMBP2Data != null)
                {
                    int xlrow2 = excelMBP2Data.Dimension.End.Row;

                    #region Validations for Entity Master
                    for (int i = 2; i <= xlrow2; i++)
                    {
                        var obj = new MBP2TransactionVM() { EntityId = entityid};
                        int colIndex = 1;

                        #region Transaction Details
                        #region Nature of Transaction
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            var nature = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                            obj.NatureOfTransaction = nature;
                            obj.NatureOfTransactionId = GetNatureOfTransaction(nature);
                        }
                        else
                        {
                            errorMessage.Add("Please check the Nature of transaction can not be empty at row - " + i + "");
                        }

                        if (obj.NatureOfTransactionId == 0)
                        {
                            errorMessage.Add("Please check the Nature of transaction at row - " + i + "");
                        }
                        #endregion

                        #region Date of transaction
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            string dateofTrans = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text).Trim();
                            DateTime d = new DateTime();
                            if (DateTime.TryParse(dateofTrans, out d))
                            {
                                obj.DateOfMaking = d;
                            }
                            else
                            {
                                errorMessage.Add("Please enter valid date of transaction at row - " + i + "");
                            }
                        }
                        else
                        {
                            errorMessage.Add("Please Check the date of transaction can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region Name
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            obj.Name_ = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                        }
                        else
                        {
                            errorMessage.Add("Please Name can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region Address
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            obj.Address_ = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                        }
                        #endregion

                        #region Amount
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            var amt = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim()).Replace(",","");
                            obj.Amount = Convert.ToDecimal(amt);
                        }
                        else
                        {
                            errorMessage.Add("Please check Amount can not be empty at row - " + i + "");
                        }
                        #endregion

                        #region Time Period
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            obj.TimePeriod = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                        }
                        #endregion

                        #region Purpose
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            obj.Purpose = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                        }
                        #endregion

                        #region Percentages
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            obj.PercentageOfReserve = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                        }
                        #endregion

                        #region Date of Board
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            string dateOfBoard = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text).Trim();
                            DateTime d = new DateTime();
                            if (DateTime.TryParse(dateOfBoard, out d))
                            {
                                obj.DateOfBoardResolution = d;
                            }
                        }
                        #endregion

                        #region Date of Resolution
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            string dateofPassing = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text).Trim();
                            DateTime d = new DateTime();
                            if (DateTime.TryParse(dateofPassing, out d))
                            {
                                obj.DateOfPassingResolution = d;
                            }
                        }
                        #endregion

                        #region Rate Of Interest
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            var interest = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim()).Replace(",","");
                            obj.RateOfInterest = Convert.ToDecimal(interest);
                        }
                        #endregion

                        #region Date of Maturity
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            string dateofMaturity = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text).Trim();
                            DateTime d = new DateTime();
                            if (DateTime.TryParse(dateofMaturity, out d))
                            {
                                obj.DateOfMaturity = d;
                            }
                        }
                        #endregion

                        #region No of Security
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            obj.NoOfSecurity = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                        }
                        #endregion

                        #region Kind of Security
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            obj.KindOfSecurity = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                        }
                        #endregion

                        #region Nominal
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            var nominal = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim()).Replace(",", "");
                            obj.NominalValue = Convert.ToDecimal(nominal);
                        }
                        #endregion

                        #region Paidup
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            var paidup = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim()).Replace(",", "");
                            obj.PaidUp = Convert.ToDecimal(paidup);
                        }
                        #endregion

                        #region Cost of Acquisition
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            var cost = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim()).Replace(",", "");
                            obj.CostOfAuisition = Convert.ToDecimal(cost);
                        }
                        #endregion

                        #region Date of Selling
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            string dateofSelling = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text).Trim();
                            DateTime d = new DateTime();
                            if (DateTime.TryParse(dateofSelling, out d))
                            {
                                obj.DateOfSelling = d;
                            }
                        }
                        #endregion

                        #region Price
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            var price = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim()).Replace(",", "");
                            obj.SellingPrice = Convert.ToDecimal(price);
                        }
                        #endregion

                        #region Remark
                        colIndex++;
                        if (!String.IsNullOrEmpty(Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim())))
                        {
                            obj.Remarks = Convert.ToString(excelMBP2Data.Cells[i, colIndex].Text.Trim());
                        }
                        #endregion

                        #endregion

                        lstObj.Add(obj);
                    }
                    #endregion

                    if (errorMessage.Count == 0)
                    {
                        int count = 0;
                        foreach (var item in lstObj)
                        {
                            var temp = CreateUpdateTransaction(item, userId);
                            if (temp.Error)
                            {
                                errorMessage.Add("Error in creating " + item.NatureOfTransaction + " - " + temp.Message);
                            }
                            else if (temp.Success)
                            {
                                count++;
                            }
                        }
                        if (errorMessage.Count == 0)
                        {
                            result.Success = true;
                            result.Message = SecretarialConst.Messages.saveSuccess;
                        }
                        else
                        {
                            if (count > 0)
                            {
                                result.Success = true;
                                result.Message = count + " Records saved successfully.";
                            }
                        }
                    }
                    else
                    {
                        result.Error = true;
                    }
                }
                else
                {
                    result.Error = true;
                    result.Message = "Data Not Found";
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public int GetNatureOfTransaction(string nature)
        {
            long result = 0;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Entity_MBP2_TransactionNature
                            where row.Name == nature
                            select row.Id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                result = 0;
            }
            return (int) result;
        }
        #endregion

        public MBP2TransactionLimitVM GetLimit(int entityId)
        {
            var result = new MBP2TransactionLimitVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Entity_MBP2_Limit
                              where row.EntityId == entityId && row.IsDeleted == false && row.IsActive == true
                              select new MBP2TransactionLimitVM
                              {
                                  EntityId = (int) row.EntityId,
                                  ApprovedLimit = row.ApproveLimit,
                                  UsedLimit = row.UsedLimit,
                                  DateOfBoardResolution = row.DateOfBoardResolution,
                                  DateOfPassingResolution = row.DateOfPassingResolution,
                              }).FirstOrDefault();

                    if(result == null)
                    {
                        result = new MBP2TransactionLimitVM() { EntityId = entityId};
                    }

                    result.DateOfBoardResolutionStr = result.DateOfBoardResolution == null ? "" : Convert.ToDateTime(result.DateOfBoardResolution).ToString("dd/MM/yyy");
                    result.DateOfPassingResolutionStr = result.DateOfPassingResolution == null ? "" : Convert.ToDateTime(result.DateOfPassingResolution).ToString("dd/MM/yyy");
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public MBP2TransactionLimitVM SetLimit(MBP2TransactionLimitVM obj, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Entity_MBP2_Limit
                              where row.EntityId == obj.EntityId
                              select row).FirstOrDefault();

                    if(_obj == null)
                    {
                        _obj = new BM_Entity_MBP2_Limit();
                        _obj.EntityId = obj.EntityId;
                        _obj.ApproveLimit = obj.ApprovedLimit;
                        _obj.UsedLimit = obj.UsedLimit;
                        _obj.DateOfBoardResolution = obj.DateOfBoardResolution;
                        _obj.DateOfPassingResolution = obj.DateOfPassingResolution;
                        _obj.IsActive = true;
                        _obj.IsDeleted = false;
                        _obj.CreatedBy = userId;
                        _obj.CreatedOn = DateTime.Now;

                        entities.BM_Entity_MBP2_Limit.Add(_obj);
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        _obj.ApproveLimit = obj.ApprovedLimit;
                        _obj.UsedLimit = obj.UsedLimit;
                        _obj.DateOfBoardResolution = obj.DateOfBoardResolution;
                        _obj.DateOfPassingResolution = obj.DateOfPassingResolution;
                        _obj.IsActive = true;
                        _obj.IsDeleted = false;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public EntityDocumentVM Savechargedocument(EntityDocumentVM _objdoc, int userId, int customerId)
        {
            try
            {
                if (_objdoc.files != null)
                {
                    BM_EntityDocuments _objdocdetails = new BM_EntityDocuments
                    {
                        Description = _objdoc.Discription,
                        License_RegNumber = _objdoc.LicenceNo,
                        FYID = _objdoc.FYID,
                        StartTimeAGM = _objdoc.startTiming,
                        EndTimeAGM = _objdoc.EndTiming,
                        BMDate_LastRenew = _objdoc.BMDate,
                        GMDate_UpcomingRenew = _objdoc.GMDate,

                        IsActive = true,
                        CreatedBy = userId,
                        CreatedOn = DateTime.Now,
                        CustomerId = customerId,
                        EntityId = _objdoc.EntityId,
                        NatureOfTransactionId = _objdoc.NatureOfTransactionId,
                    };
                    entities.BM_EntityDocuments.Add(_objdocdetails);
                    entities.SaveChanges();
                    if (_objdocdetails.Id > 0 && _objdoc.files != null)
                    {
                        if (_objdoc.files.Count() > 0)
                        {
                            bool savechange = FileUpload.SaveEntityDocument(_objdoc.files, _objdocdetails.Id, userId, customerId, "LISEC", _objdoc.EntityId); _objdoc.Success = true;
                            _objdoc.Message = "Record saved successfully";
                        }
                    }
                }
                else
                {
                    _objdoc.Error = true;
                    _objdoc.Message = "Please select File";
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return _objdoc;
        }
        public List<BM_SP_GetMBP2Documents_Result> GetMBP2Documents(int entityId, int NatureOfTransactionId)
        {
            List<BM_SP_GetMBP2Documents_Result> _obj = new List<BM_SP_GetMBP2Documents_Result>();
            _obj = (from x in entities.BM_SP_GetMBP2Documents(NatureOfTransactionId, entityId)
                    select x).ToList();
            return _obj;
        }

        public List<MBP2TransactionNatureVM> GetNatureType()
        {
            List<MBP2TransactionNatureVM> result = null;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Entity_MBP2_TransactionNature
                              where row.IsActive == true
                              select new MBP2TransactionNatureVM
                              {
                                  Id = (int)row.Id,
                                  NatureOfTransaction = row.Name
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<MBP2TransactionVM> GetTransactionByNature(int entityId, string view, int NatureOfTransactionId)
        {
            List<MBP2TransactionVM> result = null;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (NatureOfTransactionId > 0)
                    {

                         var result1 = (from row in entities.BM_Entity_MBP2_Transaction
                                       join n in entities.BM_Entity_MBP2_TransactionNature on (int)row.NatureOfTransactionId equals n.Id
                                       where row.EntityId == entityId && row.IsDeleted == false && row.IsActive && row.NatureOfTransactionId == NatureOfTransactionId
                                       select new MBP2TransactionVM
                                       {
                                           MBP2TransId = row.Id,
                                           EntityId = row.EntityId,
                                           NatureOfTransactionId = row.NatureOfTransactionId,
                                           NatureOfTransaction = n.Name,
                                           DateOfMaking = row.DateOfMaking,
                                           Name_ = row.Name_,
                                           Address_ = row.Address_,
                                           Amount = row.Amount,
                                           TimePeriod = row.TimePeriod,
                                           Purpose = row.Purpose,
                                           PercentageOfReserve = row.PercentageOfReserve,
                                           DateOfBoardResolution = row.DateOfBoardResolution,
                                           DateOfPassingResolution = row.DateOfPassingResolution,
                                           RateOfInterest = row.RateOfInterest,
                                           DateOfMaturity = row.DateOfMaturity,
                                           NoOfSecurity = row.NoOfSecurity,
                                           KindOfSecurity = row.KindOfSecurity,
                                           NominalValue = row.NominalValue,
                                           PaidUp = row.PaidUp,
                                           CostOfAuisition = row.CostOfAuisition,
                                           DateOfSelling = row.DateOfSelling,
                                           SellingPrice = row.SellingPrice,
                                           Remarks = row.Remarks,
                                           LastModified = row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn
                                       });
                        if (view == "Transaction")
                        {
                            result = result1.OrderByDescending(k => k.LastModified).ToList();
                        }
                        else
                        {
                            result = result1.OrderBy(k => k.DateOfMaking).ThenBy(k => k.LastModified).ToList();
                        }

                    }
                    else
                    {
                         var result1 = (from row in entities.BM_Entity_MBP2_Transaction
                                       join n in entities.BM_Entity_MBP2_TransactionNature on (int)row.NatureOfTransactionId equals n.Id
                                       where row.EntityId == entityId && row.IsDeleted == false && row.IsActive
                                       select new MBP2TransactionVM
                                       {
                                           MBP2TransId = row.Id,
                                           EntityId = row.EntityId,
                                           NatureOfTransactionId = row.NatureOfTransactionId,
                                           NatureOfTransaction = n.Name,
                                           DateOfMaking = row.DateOfMaking,
                                           Name_ = row.Name_,
                                           Address_ = row.Address_,
                                           Amount = row.Amount,
                                           TimePeriod = row.TimePeriod,
                                           Purpose = row.Purpose,
                                           PercentageOfReserve = row.PercentageOfReserve,
                                           DateOfBoardResolution = row.DateOfBoardResolution,
                                           DateOfPassingResolution = row.DateOfPassingResolution,
                                           RateOfInterest = row.RateOfInterest,
                                           DateOfMaturity = row.DateOfMaturity,
                                           NoOfSecurity = row.NoOfSecurity,
                                           KindOfSecurity = row.KindOfSecurity,
                                           NominalValue = row.NominalValue,
                                           PaidUp = row.PaidUp,
                                           CostOfAuisition = row.CostOfAuisition,
                                           DateOfSelling = row.DateOfSelling,
                                           SellingPrice = row.SellingPrice,
                                           Remarks = row.Remarks,
                                           LastModified = row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn
                                       });
                        if (view == "Transaction")
                        {
                            result = result1.OrderByDescending(k => k.LastModified).ToList();
                        }
                        else
                        {
                            result = result1.OrderBy(k => k.DateOfMaking).ThenBy(k => k.LastModified).ToList();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

    }
}