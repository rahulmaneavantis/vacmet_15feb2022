﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IMeetingModule
    {
        List<VM_NewMeetingModule> GetAllMeeting(int customerId);
        VM_NewMeetingModule getMettingbyId(int id);
        VM_NewMeetingModule AddMeeting(VM_NewMeetingModule _objvmmeeting);
        VM_NewMeetingModule UpdateMeeting(VM_NewMeetingModule _objvmmeeting);
    }
}
