﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public class BankAccDetailsMaster : IBankAccDetailsMaster
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public List<BankAccDetails_VM> GetBankDetails(int entityId)
        {
            List<BankAccDetails_VM> _objaddicust = new List<BankAccDetails_VM>();
            try
            {
                var today = DateTime.Today;
                _objaddicust = (from row in entities.BM_BankAccountDetails
                                join rows in entities.BM_BankAccountType
                                      on new { Source = row.AccTypeId }
                                      equals new { Source = rows.Id }
                                where row.EntityId == entityId && row.IsActive == true
                                select new BankAccDetails_VM
                                {
                                    AccType =rows.AccType,
                                    Id = row.Id,
                                    EntityId = row.EntityId,
                                    BankName = row.BankName,
                                    AccNo = row.AccNo,
                                    AccTypeId = row.AccTypeId,
                                    Ifsc = row.Ifsc,
                                    Address = row.Address,
                                    OpenDate = row.OpenDate,
                                    CloseDate = row.CloseDate,
                                    Status = row.CloseDate == null ? true : (row.CloseDate < today) ? false : true,
                                    IsActive = row.IsActive
                                }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objaddicust;
        }
        public IEnumerable<BankTypeList_VM> GetBankType()
        {
            List<BankTypeList_VM> Listofstate = new List<BankTypeList_VM>();
            try
            {
                Listofstate = (from row in entities.BM_BankAccountType
                               select new BankTypeList_VM
                               {
                                   Id = row.Id,
                                   Acctype = row.AccType
                               }).ToList();
                return Listofstate;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public BankAccDetails_VM AddBankDetails(BankAccDetails_VM _objadditionalcomp, int userId)
        {
            BankAccDetails_VM objadditioncom = new BankAccDetails_VM();
            try
            {
                bool sucess = false;
                BM_BankAccountDetails _objcomp = new BM_BankAccountDetails();
                _objcomp.AccNo = _objadditionalcomp.AccNo;
                _objcomp.BankName = _objadditionalcomp.BankName;
                _objcomp.AccTypeId = _objadditionalcomp.AccTypeId;
                _objcomp.Ifsc = _objadditionalcomp.Ifsc;
                _objcomp.Address = _objadditionalcomp.Address;
                _objcomp.OpenDate = _objadditionalcomp.OpenDate;
                _objcomp.CloseDate = _objadditionalcomp.CloseDate;
                _objcomp.Status = _objadditionalcomp.Status;
                _objcomp.EntityId = _objadditionalcomp.EntityId;
                _objcomp.CreatedBy = userId;
                _objcomp.CreatedOn = DateTime.Now;
                _objcomp.IsActive = true;
                entities.BM_BankAccountDetails.Add(_objcomp);
                entities.SaveChanges();

                _objadditionalcomp.Id = _objcomp.Id;

                objadditioncom.Success = true;
                objadditioncom.Message = SecretarialConst.Messages.saveSuccess;
            }

            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objadditioncom.Error = true;
                objadditioncom.Message = SecretarialConst.Messages.serverError;
            }
            return objadditioncom;
        }

        public BankAccDetails_VM EditBankDetails( int Id, int entityId)
        {
            BankAccDetails_VM objadditcom = new BankAccDetails_VM();
            try
            {
                objadditcom = (from row in entities.BM_BankAccountDetails
                               where row.Id == Id
                               
                               select new BankAccDetails_VM
                               {
                                   Id = row.Id,
                                   EntityId =row.EntityId,
                                   BankName = row.BankName,
                                   AccNo = row.AccNo,
                                   AccTypeId = row.AccTypeId,
                                   Ifsc = row.Ifsc,
                                   Address = row.Address,
                                   OpenDate = row.OpenDate,
                                   CloseDate = (DateTime)row.CloseDate,
                                   //Status = row.Status,
                               }).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return objadditcom;
        }
        public BankAccDetails_VM UpdateBankDetails(BankAccDetails_VM _objadditionalcomp, int userId)
        {
            try
            {
                BM_BankAccountDetails _objcomp = new BM_BankAccountDetails();
                {
                    var checkcomplianceexist = (from row in entities.BM_BankAccountDetails where row.Id == _objadditionalcomp.Id select row).FirstOrDefault();

                    if (checkcomplianceexist != null)
                    {
                        checkcomplianceexist.AccNo = _objadditionalcomp.AccNo;
                        checkcomplianceexist.BankName = _objadditionalcomp.BankName;
                        checkcomplianceexist.AccTypeId = _objadditionalcomp.AccTypeId;
                        checkcomplianceexist.Ifsc = _objadditionalcomp.Ifsc;
                        checkcomplianceexist.Address = _objadditionalcomp.Address;
                        checkcomplianceexist.OpenDate = _objadditionalcomp.OpenDate;
                        checkcomplianceexist.CloseDate = _objadditionalcomp.CloseDate;
                        if (_objadditionalcomp.CloseDate != null)
                        {
                            //checkcomplianceexist.IsActive = false;
                            if(_objadditionalcomp.CloseDate <= DateTime.Now.Date)
                            {
                                checkcomplianceexist.Status = false;
                            }
                            else
                            {
                                checkcomplianceexist.Status = true;
                            }
                        }
                        else
                        {
                            checkcomplianceexist.Status = true;
                        }
                        checkcomplianceexist.EntityId = _objadditionalcomp.EntityId;
                        checkcomplianceexist.UpdatedBy = userId;
                        checkcomplianceexist.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();

                        _objadditionalcomp.Success = true;
                        _objadditionalcomp.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        _objadditionalcomp.Error = true;
                        _objadditionalcomp.Message = SecretarialConst.Messages.serverError;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objadditionalcomp.Error = true;
                _objadditionalcomp.Message = SecretarialConst.Messages.serverError;
            }
            return _objadditionalcomp;
        }
        public bool DeleteAdditionalBank(int id, int userId)
        {
            bool success = false;
            try
            {
                var checkcomp = (from row in entities.BM_BankAccountDetails where row.Id == id select row).FirstOrDefault();
                if (checkcomp != null)
                {
                    checkcomp.IsActive = false;
                    checkcomp.UpdatedOn = DateTime.Now;
                    checkcomp.UpdatedBy = userId;
                    entities.SaveChanges();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }
    }
}