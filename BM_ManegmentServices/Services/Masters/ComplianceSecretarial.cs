﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;
using BM_ManegmentServices.VM.Compliance;

namespace BM_ManegmentServices.Services.Masters
{
    public class ComplianceSecretarial : ICompliance
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public void AddFormDetailsItem(FormDetails complianceItem, int userID, int customerID)
        {
            throw new NotImplementedException();
        }

        public void AddFormitem(FormDetails complianceItem, int userID, int customerID)
        {
            throw new NotImplementedException();
        }

        public void DeleteFormComlianceDelete(FormDetails formItem, int userID, int customerID)
        {
            throw new NotImplementedException();
        }

        public List<ComplianceVM> GetAllSecretarialCompliances()
        {
            try
            {
                var result = (from row in entities.BM_SP_GetComplianceForAgendaItem()
                              select new ComplianceVM
                              {
                                  ComplianceId = row.ID,
                                  Description = row.ShortDescription
                              }
                          ).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<ComplianceStatusVM> GetComplianceUpdateStatus(long scheduleOnID, int userID)
        {
            try
            {
                var roleID = entities.BM_ComplianceScheduleAssignment.Where(k => k.ScheduleOnID == scheduleOnID && k.UserID == userID).Select(k => (int?)k.RoleID).Max();
                return GetComplianceUpdateStatus(scheduleOnID, userID, roleID);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<ComplianceStatusVM> GetComplianceUpdateStatus(long scheduleOnID, int userID, int? roleID)
        {
            try
            {
                var initialStautID = entities.BM_ComplianceScheduleOn.Where(k => k.ScheduleOnID == scheduleOnID).Select(k => k.StatusId).FirstOrDefault();
                var isComplianceAssinged = entities.BM_ComplianceScheduleAssignment.Where(k => k.ScheduleOnID == scheduleOnID && k.UserID == userID && k.RoleID == roleID).Any();

                if (roleID > 0 && initialStautID > 0 && isComplianceAssinged == true)
                {
                    var result = (from row in entities.ComplianceStatusTransitions
                                  join status in entities.ComplianceStatus on row.FinalStateID equals status.ID
                                  where row.RoleID == roleID && row.InitialStateID == initialStautID
                                  select new ComplianceStatusVM
                                  {
                                      StatusId = status.ID,
                                      StatusName = status.Name
                                  }
                          ).Distinct().ToList();
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<FormDetails> GetFormComplianceMapping(long complianceID)
        {
            throw new NotImplementedException();
        }
    }
}