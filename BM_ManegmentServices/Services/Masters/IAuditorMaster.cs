﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IAuditorMaster
    {
        List<VMAuditor> GetAuditorDetails(int customerId);
        IEnumerable<BM_EntityMaster> GetDropDownforEntityType();
        VMAuditor AddAuditData(VMAuditor _vmauditor);

        VMAuditor UpdateAuditData(VMAuditor _vmauditor);
        VMAuditor GetAuditorbyId(int id);
        IEnumerable<BM_Nature_of_Appointment> GetDropDownNatureofAppointment();
        IEnumerable<VMAuditorforDropdown> GetDropDownAuditors(int criteriaId, int type, int EntityId, int CustomerId);
        List<VMStatutory_Auditor> GetStatutoryAuditor(int customerId,long EntityId);
        VMStatutory_Auditor AddStatutoryAuditor(VMStatutory_Auditor _objstatutory_auditor);
        VMStatutory_Auditor UpdateStatutoryAuditor(VMStatutory_Auditor _objstatutory_auditor);
        VMStatutory_Auditor GetStatutorybyId(int id, int customer_Id);
        IEnumerable<BM_AuditorType> GetDropDownAuditors_Type();
        VMInternalAuditor AddInternalAuditor(VMInternalAuditor _objinternal_auditor);
        VMInternalAuditor UpdateInternalAuditor(VMInternalAuditor _objinternal_auditor);
        string getAuditorName(int auditorId);
        VMInternalAuditor GetInternalAuditorbyId(long id, int entityId);
        List<VMInternalAuditor> GetInternalAuditor(int customerId,long EntityId);
        VM_SecreterialAuditor updateSecreterialDate(VM_SecreterialAuditor _objsecreterial);
        VM_SecreterialAuditor AddSecreterialDate(VM_SecreterialAuditor _objsecreterial);
        VM_SecreterialAuditor getSecreterialAuditorbyId(long id, int entityId);
        List<VM_SecreterialAuditor> getSecreterialAuditor(int customerId,long EntityID);
        VM_CostAuditor getCostAuditorbyId(long id, int entityId);
        VM_CostAuditor updateCostDate(VM_CostAuditor _objcost);
        VM_CostAuditor AddCostDate(VM_CostAuditor _objcost);
        string getEntityName(int entity_ID);
        List<VM_CostAuditor> getCostAuditor(int customerId,long EntityId);
        int Getcountry(string country);
        int getStateId(string state);
        int getCityId(int stateId, string city);
        bool ChecksaveUpdateForAuditorMaster(BM_Auditor_Master objauditor);

        #region Template field Generation
        IEnumerable<VMAuditorforDropdown> GetAuditorsByType(int type, int customerID);
        IEnumerable<BM_Auditor_Master> GetAuditorwhoresigned(long entityId);
        #endregion

        #region Auditor Resignation
        Auditor_ResignationVM UpdateAuditorResignation(Auditor_ResignationVM obj);
        Auditor_ResignationVM SaveAuditorResignation(Auditor_ResignationVM obj, int customerId,int userId);
        Auditor_ResignationVM GetAuditorResignationDetails(int Statutory_AuditorId, int AuditorId, int EntityId, string AuditorCategory);
        Message RevertAuditorResignation(int statutory_AuditorId, int auditorId, int userId);
        Auditor_ResignationReviewVM ResignationLetter(long AuditorMappingId);
        IEnumerable<VMAuditorforDropdown> GetAuditorListForApptCess(int entityId, string auditorType, long? statutoryMappingId);
        string DeleteAuditor(int id, int customerId,int UserId);
        #endregion

        List<VMAuditor> GetAuditorsForGM(int customerId, long EntityId);
        DateTime? GetEntityIncorporationDate(int entityId);
    }
}
