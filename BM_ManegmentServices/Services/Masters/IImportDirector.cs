﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IImportDirector
    {
        List<ImportDirectorFromCustomerVM> GetDirectorListForImport(int customerId);
        Message ImportDirectorFromOtherCustomer(IEnumerable<ImportDirectorFromCustomerVM> lstDirectors, int customer, int createdBy);
    }
}
