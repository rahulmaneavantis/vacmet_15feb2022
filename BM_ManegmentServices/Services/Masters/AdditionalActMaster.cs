﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public class AdditionalActMaster : IAdditionalActMaster
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        #region GetAdditional Act Added by Rohit on 21 of Oct. 2021
        public List<AddtionalAct_VM> GetAdditionalAct()
        {
            List<AddtionalAct_VM> _objaddiact = new List<AddtionalAct_VM>();
            try
            {
                _objaddiact = (from row in entities.BM_SP_GetAdditionalAct()
                               select new AddtionalAct_VM
                               {
                                   ActId = row.AdditonalActId,
                                   ActName = row.ActName
                               }).ToList();
                //_objaddiact = (from row in entities.BM_AdditionalAct
                //               join rows in entities.Acts on row.ActId equals rows.ID
                //               where row.IsActive == true
                //               select new AddtionalAct_VM
                //               {
                //                   ActId = row.Id,
                //                   ActName = rows.Name
                //               }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objaddiact;
        }

        public AddtionalAct_VM AddAdditionalAct(AddtionalAct_VM _objadditionalact, int userId)
        {
            AddtionalAct_VM objadditionact = new AddtionalAct_VM();
            try
            {
                bool sucess = false;
                int actId = 0;
                BM_AdditionalAct _objact = new BM_AdditionalAct();

                if (_objadditionalact.LSTAct != null)
                {

                    string s = _objadditionalact.LSTAct;
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = values[i].Trim();
                        if (values[i] != null)
                            actId = Convert.ToInt32(values[i]);

                        var checkactexist = (from row in entities.BM_AdditionalAct where row.ActId == actId select row).FirstOrDefault();
                        if (checkactexist == null)
                        {
                            _objact.ActId = actId;
                            _objact.IsActive = true;
                            _objact.Createdby = userId;
                            _objact.CreatedOn = DateTime.Now;

                            entities.BM_AdditionalAct.Add(_objact);
                            entities.SaveChanges();

                            sucess = true;
                        }
                        else
                        {
                            checkactexist.IsActive = true;
                            checkactexist.Updatedby = userId;
                            checkactexist.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();

                            sucess = true;
                        }
                    }

                    if (sucess)
                    {
                        objadditionact.Success = true;
                        objadditionact.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        objadditionact.Success = false;
                        objadditionact.Message = SecretarialConst.Messages.alreadyExist;
                    }
                }
                else
                {
                    objadditionact.Success = false;
                    objadditionact.Message = "Please enter at least one ActID";
                }
            }
            catch (Exception ex)
            {
                objadditionact.Error = true;
                objadditionact.Message = SecretarialConst.Messages.serverError;
            }
            return objadditionact;
        }

        public AddtionalAct_VM EditAdditionalAct(long Id)
        {
            AddtionalAct_VM _objadditionalact = new AddtionalAct_VM();
            try
            {
                _objadditionalact = (from row in entities.BM_AdditionalAct
                                     where row.Id == Id
                               select new AddtionalAct_VM
                               {
                                   Id = row.Id,
                                   AdditionalActId = row.ActId
                               }).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objadditionalact;
        }

        public AddtionalAct_VM UpdateAdditionalAct(AddtionalAct_VM _objadditionalact, int userId)
        {
            try
            {
                int actId = 0;
                BM_AdditionalAct _objAct = new BM_AdditionalAct();
                string s = _objadditionalact.LSTAct;
                string[] values = s.Split(',');
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = values[i].Trim();
                    if (values[i] != null)
                        actId = Convert.ToInt32(values[i]);


                    var checkActexist = (from row in entities.BM_AdditionalAct where row.Id == _objadditionalact.Id select row).FirstOrDefault();

                    if (checkActexist != null)
                    {
                        checkActexist.ActId = actId;
                        checkActexist.IsActive = true;
                        checkActexist.Createdby = userId;
                        checkActexist.CreatedOn = DateTime.Now;
                        entities.BM_AdditionalAct.Add(_objAct);
                        entities.SaveChanges();
                        _objadditionalact.Success = true;
                        _objadditionalact.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        _objadditionalact.Error = true;
                        _objadditionalact.Message = SecretarialConst.Messages.noRecordFound;
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objadditionalact.Error = true;
                _objadditionalact.Message = SecretarialConst.Messages.serverError;
            }
            return _objadditionalact;
        }

        public bool DeleteAdditionalAct(long id, int userId)
        {
            bool success = false;
            try
            {
                var checkcomp = (from row in entities.BM_AdditionalAct where row.ActId == id select row).FirstOrDefault();
                if (checkcomp != null)
                {
                    checkcomp.IsActive = false;
                    checkcomp.UpdatedOn = DateTime.Now;
                    checkcomp.Updatedby = userId;
                    entities.SaveChanges();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }
        #endregion

    }
}