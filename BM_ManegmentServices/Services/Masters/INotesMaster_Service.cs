﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface INotesMaster_Service
    {
        NotesMasterVM Save(NotesMasterVM obj, int customerId, int createdBy);
        NotesMasterVM GetNotesMaster(NotesMasterVM obj, int customerId);
    }
}
