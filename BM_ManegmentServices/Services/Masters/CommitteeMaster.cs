﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class CommitteeMaster : ICommitteeMaster
    {    
        public List<Composition> CheckIsValidCommittee(int CommitteeId, int EntityId, int CustomerId)
        {
            try
            {
                var result = new List<Composition>();
                int? CustomerIdTemp = null;
                ICommitteeComp objICommitteeComp = new CommitteeComp();

                int EntityType = 0;

                var asPerStandard = objICommitteeComp.CheckAsPerStandard(CommitteeId, CustomerId, EntityId);

                if (!asPerStandard)
                {
                    CustomerIdTemp = CustomerId;
                }

                #region Get Entity Type
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var entity = entities.BM_EntityMaster.Where(k => k.Id == EntityId && k.Is_Deleted == false).FirstOrDefault();
                    if (entity != null)
                    {
                        //if (entity.Entity_Type == 1)
                        //{
                        //    EntityType = 1;
                        //}
                        //else if (entity.Entity_Type == 2 && entity.IS_Listed == false)
                        //{
                        //    EntityType = 2;
                        //}
                        //else if (entity.Entity_Type == 2 && entity.IS_Listed == true)
                        //{
                        //    EntityType = 3;
                        //}

                        if (entity.Entity_Type == 1 || entity.Entity_Type == 2 || entity.Entity_Type == 9 || entity.Entity_Type == 10)
                        {
                            EntityType = entity.Entity_Type;
                        }
                    }
                }
                #endregion

                var CompositionRules = objICommitteeComp.GetAllComposition(CommitteeId, EntityType, "D", CustomerIdTemp, EntityId);

                #region Check Rules
                // BM_Directors_DetailsOfCommiteePosition   Committee_Type column contains Designation Id
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    //Get total Members
                    var totalMember = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.Committee_Id == CommitteeId && k.EntityId == EntityId && k.IsDeleted == false).Count();

                    if (totalMember == 0)
                    {
                        result = CompositionRules;
                    }
                    else
                    {
                        foreach (var item in CompositionRules)
                        {
                            //Minimum Count
                            if (item.DesignationId == null && item.DesignationCount > 0)
                            {
                                if (totalMember < item.DesignationCount)
                                {
                                    result.Add(item);
                                }
                            }
                            //Minumum Designation Count
                            else if (item.DesignationId != null && item.DesignationCount > 0 && item.DesignationNumerator == null && item.DesignationDenominator == null)
                            {
                                var DesignationCount = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.Committee_Id == CommitteeId && k.EntityId == EntityId && k.Committee_Type == item.DesignationId && k.IsDeleted == false).Count();
                                if (DesignationCount < item.DesignationCount)
                                {
                                    result.Add(item);
                                }
                            }
                            //Minumum Designation Count with total strength
                            else if (item.DesignationId != null && item.DesignationNumerator != null && item.DesignationDenominator != null)
                            {
                                decimal minimum = 0;
                                var DesignationCount = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.Committee_Id == CommitteeId && k.EntityId == EntityId && k.Committee_Type == item.DesignationId && k.IsDeleted == false).Count();

                                if (item.DesignationCount > minimum)
                                {
                                    minimum = (int)item.DesignationCount;
                                }

                                if (item.DesignationDenominator > 0)
                                {
                                    decimal n = Math.Ceiling(((decimal)item.DesignationNumerator / (decimal)item.DesignationDenominator) * totalMember);

                                    if (n > minimum)
                                    {
                                        minimum = n;
                                    }

                                    if (DesignationCount < minimum)
                                    {
                                        result.Add(item);
                                    }
                                }
                            }

                        }
                    }
                }
                #endregion

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<string> getMandatoryCommittee(int EntityId)
        {
            var result = new List<string>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var r = (from row in entities.BM_Applicability
                             join e in entities.BM_Applicability_Entity on row.Id equals e.ApplicabilityId
                             join c in entities.BM_CommitteeComp on row.CommitteeId equals c.Id
                             where e.EntityId == EntityId && e.ApplicabilityValue == true
                             select new
                             {
                                 CommitteId = row.CommitteeId,
                                 Name = c.Name
                             }).Distinct();

                    foreach (var item in r)
                    {
                        var check = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.EntityId == EntityId && k.Committee_Id == item.CommitteId && k.IsDeleted == false).Any();
                        if (!check)
                        {
                            result.Add(item.Name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //obj.Message.Error = true;
                //obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<CommitteeMatrixVM> GetCommitteeMatrix(int entityId)
        {
            var result = new List<CommitteeMatrixVM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SP_GetCommitteeMatrix(entityId)
                             select new CommitteeMatrixVM
                             {
                                 Id = row.Id,
                                 CommitteeId = row.CommitteeId,
                                 CommitteeName = row.CommitteeName,
                                 CommitteeType = row.CommitteeType,
                                 DirectorId = row.DirectorId,
                                 UserId = row.UserId,
                                 ParticipantType = row.ParticipantType,
                                 FirstName = row.FirstName,
                                 FullName = row.FullName,
                                 Gender = row.Gender,
                                 DIN_PAN = row.DIN_PAN,
                                 DesignationId = row.DesignationId,
                                 Designation = row.Designation,
                                 DirectorShipIdForQuorum = row.DirectorShipIdForQuorum,
                                 DirectorShip = row.DirectorShip,
                                 ParticipantDesignation = row.ParticipantDesignation,
                                 PositionId = row.PositionId,
                                 Position = row.Position,
                                 DateOfAppointment = row.DateOfAppointment
                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}