﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IDesignationMaster
    {
        List<VMDesignation> GetDesignationData();
        VMDesignation SaveUpdateDesignationDetials(VMDesignation objVMDesignation);
        VMDesignation GetDesignationMaster(int id);
        bool DeleteDesignationData(int id);//, int UserID

        List<Director_DesignationVM> GetDirectorDesignation();
        List<VMDesignation> GetDesignationForDirector(string insertSelect);
        List<VMDesignation> GetDesignation(bool isDirector, bool isMNGT, string insertSelect,bool IsforLLP);
        List<DirectorTypeOfMembershipVM> GetDirectorTypeOfMembership(int designationId, string insertSelect);
    }
}
