﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IVideoMeeting
    {
       VMeetingVM GetMeetingDetail(int CustomerId, long MeetingId);

        VideoMeetingVM SaveVideoMeeting(VideoMeetingVM MeetingDetails,long MeetingId,int userId);
        string GetVMeetingId(long meetingId);
        void Dispose();

        List<FileDataVM> GetvideoConfrencingDetails(long meetingID, int userID, int customerId);
        string GetZoomMeetingRecording(int meetingId, int customerID);
        string GetProviderName(int customerID);
        string GetCiscoMeetingRecording(int meetingId, int customerID);
    }
}
