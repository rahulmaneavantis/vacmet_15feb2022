﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IAdditionalActMaster
    {
        AddtionalAct_VM AddAdditionalAct(AddtionalAct_VM _objadditionalact, int UserId);
        List<AddtionalAct_VM> GetAdditionalAct();
        AddtionalAct_VM EditAdditionalAct(long Id);
        AddtionalAct_VM UpdateAdditionalAct(AddtionalAct_VM _objadditionalact, int UserId);
        bool DeleteAdditionalAct(long id, int UserId);
    }
}