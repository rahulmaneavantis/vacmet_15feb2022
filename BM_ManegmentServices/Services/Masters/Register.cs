﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class Register : IRegister
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public VMRegisters getRegisterbyId(int id)
        {
            try
            {

                var getRegister = (from row in entities.BM_Register
                                   where row.Id == id && row.isActive == true
                                   select new VMRegisters
                                   {
                                       EntityType = (from x in entities.BM_RegisterTypeMapping where x.RegisterId == id && x.IsActive == true select x.EntityTypeId).ToList(),
                                       Register_Name = row.Name
                                   }).FirstOrDefault();
                return getRegister;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMRegisters> GetRegisterList()
        {
            try
            {

                var getRegister = (from row in entities.BM_Register
                                   where row.isActive == true
                                   select new VMRegisters
                                   {
                                       Id = row.Id,
                                       EntityTypeName = (from x in entities.BM_RegisterTypeMapping join y in entities.BM_EntityType on x.EntityTypeId equals y.Id where x.IsActive == true && x.RegisterId == row.Id select new VMEntityType { Id = y.Id, EntityType = y.EntityName }).ToList(),
                                       Register_Name = row.Name

                                   }).ToList();
                return getRegister;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<EntityTypeVM> GetEntityType()
        {
            try
            {

                var ListofCompType = (from row in entities.BM_EntityType
                                      where row.ForRegister == true
                                      select new EntityTypeVM
                                      {
                                          EntityTypeId = row.Id,
                                          EntityTypeName = row.EntityName
                                      }).ToList();

                return ListofCompType;


            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VMRegisters AddRegister(VMRegisters _objregister)
        {
            try
            {

                var _objcheckexist = (from row in entities.BM_Register where row.Name == _objregister.Register_Name && row.isActive == true select row).FirstOrDefault();

                if (_objcheckexist == null)
                {
                    BM_Register _objRegister = new BM_Register();
                    _objRegister.Name = _objregister.Register_Name;
                    _objRegister.isActive = true;
                    _objRegister.Createdon = DateTime.Now;
                    entities.BM_Register.Add(_objRegister);
                    entities.SaveChanges();
                    if (_objRegister.Id > 0 && _objregister.EntityType.Count > 0)
                    {
                        foreach (var item in _objregister.EntityType)
                        {
                            var _bjcheck = (from row in entities.BM_RegisterTypeMapping where row.RegisterId == _objRegister.Id && row.EntityTypeId == item && row.IsActive == true select row).FirstOrDefault();

                            if (_bjcheck == null)
                            {
                                BM_RegisterTypeMapping obj = new BM_RegisterTypeMapping();
                                obj.RegisterId = _objRegister.Id;
                                obj.EntityTypeId = item;
                                obj.IsActive = true;
                                entities.BM_RegisterTypeMapping.Add(obj);
                                entities.SaveChanges();
                            }
                        }
                    }
                    _objregister.Successmessage = true;
                    _objregister.successErrorMessage = "Saved Successfully.";
                    return _objregister;
                }
                else
                {
                    _objregister.Errormessage = true;
                    _objregister.successErrorMessage = "Somthing wents wrong";
                    return _objregister;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objregister.Errormessage = true;
                _objregister.successErrorMessage = "Server error occured";
                return _objregister;
            }
        }

        public VMRegisters UpdateRegister(VMRegisters _objregister)
        {
            try
            {

                var _objcheckexist = (from row in entities.BM_Register where row.Name == _objregister.Register_Name && row.isActive == true select row).FirstOrDefault();

                if (_objcheckexist != null)
                {
                    _objcheckexist.Name = _objregister.Register_Name;
                    _objcheckexist.isActive = true;
                    _objcheckexist.Createdon = DateTime.Now;
                    //entities.BM_Register.Add(_objcheckexist);
                    entities.SaveChanges();
                    if (_objcheckexist.Id > 0 && _objregister.EntityType.Count > 0)
                    {
                        //foreach (var item in _objregister.EntityType)
                        //{
                        var _bjchecks = (from row in entities.BM_RegisterTypeMapping where row.RegisterId == _objcheckexist.Id && row.IsActive == true select row).FirstOrDefault();

                        if (_bjchecks != null)
                        {
                            _bjchecks.IsActive = false;
                            entities.SaveChanges();
                        }
                        //}
                        foreach (var items in _objregister.EntityType)
                        {
                            var _bjcheck = (from row in entities.BM_RegisterTypeMapping where row.RegisterId == _objcheckexist.Id && row.EntityTypeId == items select row).FirstOrDefault();
                            if (_bjcheck != null)
                            {
                                _bjcheck.RegisterId = _objcheckexist.Id;
                                _bjcheck.EntityTypeId = items;
                                _bjcheck.IsActive = true;
                                entities.SaveChanges();
                            }
                            else
                            {
                                BM_RegisterTypeMapping objmapping = new BM_RegisterTypeMapping();
                                objmapping.RegisterId = _objcheckexist.Id;
                                objmapping.EntityTypeId = items;
                                objmapping.IsActive = true;
                                entities.BM_RegisterTypeMapping.Add(objmapping);
                                entities.SaveChanges();
                            }
                        }
                    }
                    _objregister.Successmessage = true;
                    _objregister.successErrorMessage = "Updated Successfully.";
                    return _objregister;
                }
                else
                {
                    _objregister.Errormessage = true;
                    _objregister.successErrorMessage = "Somthing wents wrong";
                    return _objregister;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objregister.Errormessage = true;
                _objregister.successErrorMessage = "Server error occured";
                return _objregister;
            }
        }

        public List<RegistersLists> GetRegisterList(long entityId)
        {
            var getResultofRegisters = (dynamic)null;
            try
            {

                int EntityType = (from row in entities.BM_EntityMaster where row.Id == entityId select row.Entity_Type).FirstOrDefault();
                if (EntityType > 0)
                {
                    getResultofRegisters = (from RTM in entities.BM_RegisterTypeMapping
                                            join R in entities.BM_Register on RTM.RegisterId equals R.Id
                                            where RTM.EntityTypeId == EntityType
                                            && RTM.IsActive == true
                                            select new RegistersLists
                                            {
                                                Id = R.Id,
                                                Name = R.Name
                                            }).ToList();
                                    }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                
            }
            return getResultofRegisters;
        }
    }
}