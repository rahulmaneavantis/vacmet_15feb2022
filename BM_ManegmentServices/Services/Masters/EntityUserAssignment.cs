﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class EntityUserAssignment : IEntityuserAssignment
    {
        public VM_EntityuserAssignment AddUserAssignment(VM_EntityuserAssignment _objentityassignment, int createdBy)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkentity = (from row in entities.BM_EntityUserAssignment where row.UserID == _objentityassignment.user && row.IsActive == true select row).FirstOrDefault();
                    if (checkentity == null)
                    {
                        BM_EntityUserAssignment _objuser = new BM_EntityUserAssignment();
                        _objuser.UserID = _objentityassignment.user;
                        _objuser.CreatedOn = DateTime.Now;
                        _objuser.CreatedBy = createdBy;
                        _objuser.IsActive = true;
                        entities.BM_EntityUserAssignment.Add(_objuser);
                        entities.SaveChanges();
                        if (_objuser.ID > 0 && _objentityassignment.EntityId.Count > 0)
                        {
                            foreach (var item in _objentityassignment.EntityId)
                            {
                                var checkmapping = (from row in entities.BM_EntityUserAssignmentMapping where row.AssignmentId == _objuser.ID && row.EntityId == item select row).FirstOrDefault();
                                if (checkmapping == null)
                                {
                                    BM_EntityUserAssignmentMapping _obj = new BM_EntityUserAssignmentMapping();
                                    _obj.AssignmentId = (int)_objuser.ID;
                                    _obj.EntityId = item;
                                    _obj.IsActive = true;
                                    _obj.Createdon = DateTime.Now;
                                    _obj.Createdby = createdBy;
                                    entities.BM_EntityUserAssignmentMapping.Add(_obj);
                                    entities.SaveChanges();
                                }
                            }
                        }
                        _objentityassignment.successMessage = true;
                        _objentityassignment.successerrorMessage = "Saved Successfully.";

                    }
                    else if (checkentity.ID > 0 && _objentityassignment.EntityId.Count > 0)
                    {

                        foreach (var item in _objentityassignment.EntityId)
                        {
                            var checkmapping = (from row in entities.BM_EntityUserAssignmentMapping where row.AssignmentId == checkentity.ID && row.EntityId == item select row).FirstOrDefault();
                            if (checkmapping == null)
                            {
                                BM_EntityUserAssignmentMapping _obj = new BM_EntityUserAssignmentMapping();
                                _obj.AssignmentId = (int)checkentity.ID;
                                _obj.EntityId = item;
                                _obj.IsActive = true;
                                _obj.Createdon = DateTime.Now;
                                _obj.Createdby = createdBy;
                                entities.BM_EntityUserAssignmentMapping.Add(_obj);
                                entities.SaveChanges();
                            }
                            else
                            {
                                checkmapping.AssignmentId = (int)checkentity.ID;
                                checkmapping.EntityId = item;
                                checkmapping.IsActive = true;
                                checkmapping.Updatedon = DateTime.Now;
                                checkmapping.Updatedby = createdBy;

                                entities.SaveChanges();
                            }
                        }
                        _objentityassignment.successMessage = true;
                        _objentityassignment.successerrorMessage = "Saved Successfully.";
                    }

                    else
                    {
                        _objentityassignment.errorMessage = true;
                        _objentityassignment.successerrorMessage = "Data already exist";
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                _objentityassignment.errorMessage = true;
                _objentityassignment.successerrorMessage = "Server error occurred";

            }
            return _objentityassignment;
        }

        public List<VM_EntityuserAssignment> GetEntityAssignment(int customerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    //var getuserassignment = (from row in entities.BM_EntityuserAssinment
                    //                         where row.IsActive == true
                    //                         select new VM_EntityuserAssignment
                    //                         {
                    //                             Entity = (from x in entities.BM_EntityuserAssinmentMapping join y in entities.BM_EntityMaster on x.EntityId equals y.Id where x.AssintmentId == row.Id && x.IsActive == true select y.CompanyName).ToList(),
                    //                             UserName = (from u in entities.Users join e in entities.BM_EntityuserAssinment on u.ID equals e.userId where u.ID == row.userId select u.FirstName + " " + u.LastName).FirstOrDefault(),
                    //                             Id = row.Id
                    //                         }).ToList();

                    //return getuserassignment;
                    var getuserAssignmentdetails = (from row in entities.BM_SP_GetEntityAssignUser(customerId)
                                                    orderby row.UserName
                                                    select new VM_EntityuserAssignment
                                                    {
                                                        CompanyName = row.CompanyName,
                                                        UserName = row.UserName,
                                                        Id = row.Id,
                                                        CompanyId = row.EntityId,
                                                        MeetingTypeName = (from r in entities.BM_EntityUserCommitteeAssignmentMapping join r1 in entities.BM_CommitteeComp on r.MeetingTypeId equals r1.Id where r.AssignmentId == row.Id && r.EntityId == row.EntityId && r.IsDeleted == false && r.IsActive == true select r1.MeetingTypeName).ToList()
                                                    }).ToList();
                    return getuserAssignmentdetails;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<UserforDropdown> GetUser(string role, int customerId)
        {
            try
            {
                List<string> lstExcludedUsers = new List<string> { "CSIMP", "DRCTR", "HDCS", "CADMN", "DADMN", "CSMGR" };
                return GetUser(customerId, lstExcludedUsers);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<UserforDropdown> GetUserForEntityAssignment(string role, int customerId)
        {
            try
            {
                List<string> lstExcludedUsers = new List<string> { "CSIMP", "HDCS", "CADMN", "DADMN", "CSMGR" };
                return GetUser(customerId, lstExcludedUsers);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public IEnumerable<UserforDropdown> GetUser(int customerId, List<string> lstExcludedUsers)
        {
            try
            {
                List<UserforDropdown> getUser = new List<UserforDropdown>();
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    int? parentID = entities.Customers.Where(k => k.ID == customerId).Select(row => row.ParentID).FirstOrDefault();

                    if (parentID != null)
                    {
                        getUser = (from row in entities.Users
                                   join rows in entities.Roles
                                   on row.SecretarialRoleID equals rows.ID
                                   where (row.CustomerID == customerId || row.CustomerID == parentID)
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                   && !lstExcludedUsers.Contains(rows.Code)
                                   orderby row.FirstName
                                   select new UserforDropdown
                                   {
                                       Id = row.ID,
                                       Name = row.FirstName + " " + row.LastName,
                                   }).ToList();
                    }
                    else
                    {
                        getUser = (from row in entities.Users
                                   join rows in entities.Roles
                                   on row.SecretarialRoleID equals rows.ID
                                   where row.CustomerID == customerId
                                   && row.IsActive == true
                                   && row.IsDeleted == false
                                   && !lstExcludedUsers.Contains(rows.Code)
                                   orderby row.FirstName
                                   select new UserforDropdown
                                   {
                                       Id = row.ID,
                                       Name = row.FirstName + " " + row.LastName,
                                   }).ToList();
                    }
                    return getUser;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<UserforDropdown> GetUserforTask(string role, int CustomerId, int userId)
        {
            List<UserforDropdown> getUser = new List<UserforDropdown>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (role == SecretarialConst.Roles.DRCTR || role == SecretarialConst.Roles.SMNGT)
                    {
                        getUser = (from row in entities.Users
                                   join rows in entities.Roles
                                  on row.SecretarialRoleID equals rows.ID
                                   where
                                    row.IsActive == true && row.IsDeleted == false
                                     && row.CustomerID == CustomerId
                                     && row.ID != userId
                                   select new UserforDropdown
                                   {
                                       Id = row.ID,
                                       Name = row.FirstName + " " + row.LastName,
                                   }).OrderBy(row => row.Name).ToList();
                    }
                    else if (role == SecretarialConst.Roles.CS || role == SecretarialConst.Roles.HDCS)
                    {
                        int? parentID = entities.Customers.Where(k => k.ID == CustomerId).Select(row => row.ParentID).FirstOrDefault();

                        if (parentID != null)
                        {
                            getUser = (from row in entities.Users
                                       join rows in entities.Roles
                                       on row.SecretarialRoleID equals rows.ID
                                       where (rows.Code == SecretarialConst.Roles.CSMGR || rows.Code == SecretarialConst.Roles.CEXCT || rows.Code == SecretarialConst.Roles.DADMN
                                       || rows.Code == SecretarialConst.Roles.CS || rows.Code == SecretarialConst.Roles.HDCS || rows.Code == SecretarialConst.Roles.DRCTR || rows.Code == SecretarialConst.Roles.SMNGT)
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                       && (row.CustomerID == CustomerId || row.CustomerID == parentID)
                                       && row.ID != userId
                                       select new UserforDropdown
                                       {
                                           Id = row.ID,
                                           Name = row.FirstName + " " + row.LastName,
                                       }).OrderBy(row => row.Name).ToList();
                        }
                        else
                        {
                            getUser = (from row in entities.Users
                                       join rows in entities.Roles
                                       on row.SecretarialRoleID equals rows.ID
                                       where (rows.Code == SecretarialConst.Roles.CS || rows.Code == SecretarialConst.Roles.HDCS || rows.Code == SecretarialConst.Roles.DRCTR || rows.Code == SecretarialConst.Roles.SMNGT) 
                                       && row.IsActive == true
                                       && row.IsDeleted == false
                                       && row.CustomerID == CustomerId
                                       && row.ID != userId
                                       select new UserforDropdown
                                       {
                                           Id = row.ID,
                                           Name = row.FirstName + " " + row.LastName,
                                       }).OrderBy(row => row.Name).ToList();
                        }

                    }
                    return getUser;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return getUser;
            }
        }


        public VM_EntityuserAssignment GetUserAssignmentbyId(int id)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getuserassignment = (from row in entities.BM_EntityUserAssignment
                                             where row.ID == id && row.IsActive == true
                                             select new VM_EntityuserAssignment
                                             {

                                                 EntityId = (from x in entities.BM_EntityUserAssignmentMapping where x.AssignmentId == id && x.IsActive == true select x.EntityId).ToList(),
                                                 user = row.UserID,
                                                 Id = row.ID

                                             }).FirstOrDefault();

                    return getuserassignment;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public VM_EntityuserAssignment UpdateUserAssignment(VM_EntityuserAssignment _objentityassignment, int updatedBy, int customerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkentity = (from row in entities.BM_EntityUserAssignment where row.UserID == _objentityassignment.user && row.IsActive == true select row).FirstOrDefault();
                    if (checkentity != null)
                    {
                        checkentity.UpdatedOn = DateTime.Now;
                        checkentity.UpdatedBy = updatedBy;
                        checkentity.IsActive = true;

                        entities.Entry(checkentity).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        var entityList = (from row in entities.BM_EntityUserAssignmentMapping
                                          join entity in entities.BM_EntityMaster on row.EntityId equals entity.Id
                                          where row.AssignmentId == checkentity.ID && row.IsActive == true && entity.Customer_Id == customerId
                                          select entity.Id).ToList();
                        if (entityList != null)
                        {
                            foreach (var item in entityList)
                            {
                                entities.BM_EntityUserAssignmentMapping.Where(k => k.AssignmentId == checkentity.ID && k.EntityId == item && k.IsActive == true).ToList().ForEach(k => { k.IsActive = false; k.Updatedby = updatedBy; k.Updatedon = DateTime.Now; });
                                entities.SaveChanges();
                            }
                        }


                        if (checkentity.ID > 0 && _objentityassignment.EntityId.Count > 0)
                        {
                            foreach (var items in _objentityassignment.EntityId)
                            {
                                var checkmapping = (from row in entities.BM_EntityUserAssignmentMapping where row.AssignmentId == checkentity.ID && row.EntityId == items select row).FirstOrDefault();
                                if (checkmapping == null)
                                {

                                    BM_EntityUserAssignmentMapping _obj = new BM_EntityUserAssignmentMapping();
                                    _obj.AssignmentId = (int)checkentity.ID;
                                    _obj.EntityId = items;
                                    _obj.IsActive = true;
                                    _obj.Createdon = DateTime.Now;
                                    _obj.Createdby = updatedBy;
                                    entities.BM_EntityUserAssignmentMapping.Add(_obj);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    checkmapping.IsActive = true;
                                    checkmapping.Updatedon = DateTime.Now;
                                    checkentity.UpdatedBy = updatedBy;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        _objentityassignment.successMessage = true;
                        _objentityassignment.successerrorMessage = "Updated Successfully.";
                        return _objentityassignment;
                    }
                    else
                    {
                        _objentityassignment.errorMessage = true;
                        _objentityassignment.successerrorMessage = "Data not found";
                        return _objentityassignment;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objentityassignment.errorMessage = true;
                _objentityassignment.successerrorMessage = "Server error occured";
                return _objentityassignment;
            }
        }

        public bool DeleteEntityAssignment(int Id, int EntityId)
        {
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                try
                {
                    var checkexist = (from row in entities.BM_EntityUserAssignmentMapping
                                      where row.AssignmentId == Id && row.EntityId == EntityId
                                      select row).FirstOrDefault();
                    if (checkexist != null)
                    {
                        checkexist.IsActive = false;
                        entities.SaveChanges();
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public UserMeetingTypeAssignmentVM GetUserMeetingTypeAssignmentById(int assignmentId, int entityId)
        {
            var result = new UserMeetingTypeAssignmentVM() { AssignmentId = assignmentId, EntityId = entityId};
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result.ListMeetingTypeId = (from row in entities.BM_EntityUserCommitteeAssignmentMapping
                                            where row.AssignmentId == assignmentId && row.EntityId == entityId &&
                                            row.IsActive == true && row.IsDeleted == false
                                            select row.MeetingTypeId).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public UserMeetingTypeAssignmentVM SaveMeetingTypeAssignment(UserMeetingTypeAssignmentVM obj, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    entities.BM_EntityUserCommitteeAssignmentMapping.Where(k => k.AssignmentId == obj.AssignmentId && k.EntityId == obj.EntityId && k.IsActive == true).ToList().ForEach(k => { k.IsActive = false; k.IsDeleted = true; k.UpdatedBy = userId; k.UpdatedOn = DateTime.Now; });
                    entities.SaveChanges();

                    foreach (var item in obj.ListMeetingTypeId)
                    {
                        var result = (from row in entities.BM_EntityUserCommitteeAssignmentMapping
                                      where row.AssignmentId == obj.AssignmentId && row.EntityId == obj.EntityId && row.MeetingTypeId == item
                                      select row).FirstOrDefault();
                        if(result != null)
                        {
                            result.IsDeleted = false;
                            result.IsActive = true;
                            result.UpdatedBy = userId;
                            result.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }
                        else
                        {
                            result = new BM_EntityUserCommitteeAssignmentMapping();
                            result.AssignmentId = obj.AssignmentId;
                            result.EntityId = obj.EntityId;
                            result.MeetingTypeId = item;
                            result.IsDeleted = false;
                            result.IsActive = true;
                            result.CreatedBy = userId;
                            result.CreatedOn = DateTime.Now;

                            entities.BM_EntityUserCommitteeAssignmentMapping.Add(result);
                            entities.SaveChanges();
                        }
                    }

                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.saveSuccess;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
    }
}