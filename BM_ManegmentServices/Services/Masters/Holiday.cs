﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Data.Entity.SqlServer;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class Holiday : IHoliday
    {
        public VMHolidayMaster CreateHoliday(VMHolidayMaster _objholiday)
        {
            try
            {
                if (_objholiday.HolidaysDate != null)
                {
                    DateTime Hdate = Convert.ToDateTime(_objholiday.HolidaysDate);
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        var obj_IsExsist = (from row in entities.BM_HolidayMaster
                                            where row.IsActive == true
                                            //&& row.State == _objholiday.StateId
                                            //&& row.Stockexchange == _objholiday.StockExchangeId
                                            && row.HolidaysDate == Hdate
                                            select row).FirstOrDefault();

                        if (obj_IsExsist == null)
                        {
                            BM_HolidayMaster obj = new BM_HolidayMaster();
                            obj.HolidaysDate = Convert.ToDateTime(_objholiday.HolidaysDate);
                            if (_objholiday.IsStateorStockex)
                            {
                                obj.State = _objholiday.StateId;
                            }
                            else
                            {
                                obj.Stockexchange = _objholiday.StockExchangeId;
                            }
                            obj.IsSataeorSX = _objholiday.IsStateorStockex;
                            obj.Holiday_Discription = _objholiday.Holiday_Discription;
                            obj.IsActive = true;
                            obj.CreatedOn = DateTime.Now;
                            entities.BM_HolidayMaster.Add(obj);
                            entities.SaveChanges();
                            _objholiday.Message = true;
                            _objholiday.SuccessErrorMsg = "Saved Successfully.";
                            return _objholiday;
                        }
                        else
                        {
                            _objholiday.errorMessage = true;
                            _objholiday.SuccessErrorMsg = "Data allready exsist";
                            return _objholiday;
                        }
                    }
                }
                else
                {
                    _objholiday.errorMessage = true;
                    _objholiday.SuccessErrorMsg = "Please select Holiday Date";
                    return _objholiday;
                }
            }
            catch (Exception ex)
            {
                _objholiday.errorMessage = true;
                _objholiday.SuccessErrorMsg = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objholiday;
            }
        }

        public VMHolidayMaster UpdateHoliday(VMHolidayMaster _objholiday)
        {
            try
            {
                if (_objholiday.HolidaysDate != null)
                {
                    DateTime Hdate = Convert.ToDateTime(_objholiday.HolidaysDate);
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        var obj_IsExsist = (from row in entities.BM_HolidayMaster
                                            where
                                          row.IsActive == true
                                          && row.State == _objholiday.StateId
                                           && row.HolidaysDate == Hdate
                                           && row.Id == _objholiday.Id
                                            select row).FirstOrDefault();
                        if (obj_IsExsist != null)
                        {
                            obj_IsExsist.HolidaysDate = Convert.ToDateTime(_objholiday.HolidaysDate);
                            if (_objholiday.IsStateorStockex)
                            {
                                obj_IsExsist.State = _objholiday.StateId;
                            }
                            else
                            {
                                obj_IsExsist.Stockexchange = _objholiday.StockExchangeId;
                            }
                            obj_IsExsist.IsSataeorSX = _objholiday.IsStateorStockex;
                            obj_IsExsist.IsActive = true;
                            obj_IsExsist.UpdatedOn = DateTime.Now;
                            obj_IsExsist.Holiday_Discription = _objholiday.Holiday_Discription;
                            entities.SaveChanges();
                            _objholiday.Message = true;
                            _objholiday.SuccessErrorMsg = "Updated Successfully.";
                            return _objholiday;
                        }
                        else
                        {
                            _objholiday.errorMessage = true;
                            _objholiday.SuccessErrorMsg = "Data not found";
                            return _objholiday;
                        }
                    }
                }
                else
                {
                    _objholiday.errorMessage = true;
                    _objholiday.SuccessErrorMsg = "Please select Holiday Date";
                    return _objholiday;
                }

            }
            catch (Exception ex)
            {
                _objholiday.errorMessage = true;
                _objholiday.SuccessErrorMsg = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objholiday;
            }
        }

        public List<VMHolidayMaster> GetHolidayList()
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _objListofHoliday = (from row in entities.BM_HolidayMaster.Where(x => x.IsActive == true)
                                             from rows in entities.States.Where(k => k.ID == row.State).DefaultIfEmpty()
                                             from rowss in entities.BM_StockExchange.Where(S => S.Id == row.Stockexchange).DefaultIfEmpty()
                                             select new VMHolidayMaster
                                             {
                                                 Id = row.Id,
                                                 HolidaysDate = SqlFunctions.DateName("day", row.HolidaysDate) + " " + SqlFunctions.DateName("month", row.HolidaysDate) + " " + SqlFunctions.DateName("year", row.HolidaysDate),
                                                 stateName_StockExchangeName = row.State != null ? rows.Name : rowss.Name,
                                                 StateName = rows.Name,
                                                 StockExchange = rowss.Name,
                                                 IsStateorStockex = row.IsSataeorSX,
                                                 Holiday_Discription = row.Holiday_Discription,

                                             }).ToList();
                    return _objListofHoliday;
                }
            }
            catch (Exception ex)
            {
                List<VMHolidayMaster> obj = new List<VMHolidayMaster>();
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }

        public VMHolidayMaster GetHolidaybyId(int id)
        {
            VMHolidayMaster _objListofHoliday = new VMHolidayMaster();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    _objListofHoliday = (from row in entities.BM_HolidayMaster.
                                      Where(x => x.IsActive == true && x.Id == id)
                                         from rows in entities.States.Where(k => k.ID == row.State).DefaultIfEmpty()
                                         from rowss in entities.BM_StockExchange.Where(S => S.Id == row.Stockexchange).DefaultIfEmpty()
                                         select new VMHolidayMaster
                                         {
                                             Id = row.Id,
                                             HolidaysDate = SqlFunctions.DateName("day", row.HolidaysDate) + "/" + SqlFunctions.DateName("month", row.HolidaysDate) + "/" + SqlFunctions.DateName("year", row.HolidaysDate),
                                             StateId = row.State,
                                             stateName_StockExchangeName = row.State != null ? rows.Name : rowss.Name,
                                             StockExchangeId = row.Stockexchange,
                                             IsStateorStockex = row.IsSataeorSX,
                                             Holiday_Discription = row.Holiday_Discription,
                                         }).FirstOrDefault();
                    return _objListofHoliday;
                }
            }
            catch (Exception ex)
            {
                _objListofHoliday.errorMessage = true;
                _objListofHoliday.SuccessErrorMsg = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objListofHoliday;
            }
        }

        public int GetStockExchangevalue(string stockexchange)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getdata = (from row in entities.BM_StockExchange
                                   where row.isActive == true
                                   &&  row.Name.ToLower()== stockexchange.ToLower()
                                   select row).FirstOrDefault();
                    return Convert.ToInt32(getdata.Id);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }

        }

        public int GetStatevalue(string state)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var getstate = (from row in entities.States
                                    where row.IsDeleted == false
                                    && row.Name.ToLower() == state.ToLower()
                                   select row).FirstOrDefault();
                                    
                    return getstate.ID;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public bool CheckSaveorUpdate(BM_HolidayMaster objholiday)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkdata = (from row in entities.BM_HolidayMaster
                                     where row.HolidaysDate == objholiday.HolidaysDate
                                     //&& row.State == objholiday.State
                                     select row).FirstOrDefault();
                    if(checkdata!=null)
                    {
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        entities.BM_HolidayMaster.Add(objholiday);
                        entities.SaveChanges();
                        return true;
                    }
                        }
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool CheckSaveorUpdateforstockexchange(BM_HolidayMaster objholiday)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var checkdata = (from row in entities.BM_HolidayMaster
                                     where row.HolidaysDate == objholiday.HolidaysDate
                                     //&& row.Stockexchange == objholiday.Stockexchange
                                     select row).FirstOrDefault();
                    if (checkdata != null)
                    {
                        objholiday.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        return true;
                    }
                    else
                    {
                        objholiday.CreatedOn = DateTime.Now;
                        entities.BM_HolidayMaster.Add(objholiday);
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
    }
}