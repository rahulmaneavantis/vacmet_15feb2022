﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IDocumentFormat
    {
        List<BM_DocumentFormat> GetDocumentFormat(string TemplateFormate,int CustomerId);

        void SaveDocumentFormat(BM_DocumentFormat FormatDetails);
    }
}