﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public class MeetingModule : IMeetingModule
    {
        public List<VM_NewMeetingModule> GetAllMeeting(int customerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var get_Meetingdate = (from row in entities.BM_MeetingModule
                                           where row.CustomerId == customerId
                                           && row.IsActive == true
                                           select new VM_NewMeetingModule
                                           {
                                               Id = row.Id,
                                               MeetingType = (from x in entities.BM_CommitteeComp where x.Id == row.BoardMeetingId && x.IsDeleted == false select x.MeetingTypeName).FirstOrDefault(),
                                               Entity_Name = (from y in entities.BM_EntityMaster where y.Id == row.EntityId && y.Is_Deleted == false select y.CompanyName).FirstOrDefault(),
                                               FY = row.FY,
                                               Type = row.Type,
                                           }).ToList();
                    return get_Meetingdate;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public VM_NewMeetingModule getMettingbyId(int id)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var _objgetmeeting = (from row in entities.BM_MeetingModule where row.Id == id && row.IsActive == true select new VM_NewMeetingModule { Id = row.Id, Type = row.Type, BoardMeeting = row.BoardMeetingId, Entity = row.EntityId, FY = row.FY }).FirstOrDefault();
                    return _objgetmeeting;
                }
            }
            catch (Exception ex)
            {
                VM_NewMeetingModule obj = new VM_NewMeetingModule();
                obj.errorMessage = true;
                obj.Message = "Server error occured";
                return obj;
            }
        }

        public VM_NewMeetingModule UpdateMeeting(VM_NewMeetingModule _objvmmeeting)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var _objupdatedate = (from row in entities.BM_MeetingModule where row.Id == _objvmmeeting.Id && row.IsActive == true && row.CustomerId == _objvmmeeting.customerId select row).FirstOrDefault();
                    if (_objupdatedate != null)
                    {
                        _objupdatedate.CustomerId = _objvmmeeting.customerId;
                        _objupdatedate.EntityId = _objvmmeeting.Entity;
                        _objupdatedate.FY = _objvmmeeting.FY;
                        _objupdatedate.BoardMeetingId = _objvmmeeting.BoardMeeting;
                        if (_objvmmeeting.BoardMeeting == 10)
                        {
                            _objupdatedate.Type = _objvmmeeting.Type;
                        }
                        else
                        {
                            _objupdatedate.Type = "";
                        }
                        _objupdatedate.IsActive = true;
                        _objupdatedate.Updatedon = DateTime.Now;
                         entities.SaveChanges();
                        _objvmmeeting.successMessage = true;
                        _objvmmeeting.Message = "Meeting update successfully";
                        return _objvmmeeting;
                    }
                    else
                    {
                        _objvmmeeting.errorMessage = true;
                        _objvmmeeting.Message = "Somthing wents wrong";
                        return _objvmmeeting;
                    }
                }
            }
            catch (Exception ex)
            {
                _objvmmeeting.errorMessage = true;
                _objvmmeeting.Message = "Server error occured";
                return _objvmmeeting;
            }
        }

        public VM_NewMeetingModule AddMeeting(VM_NewMeetingModule _objvmmeeting)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var _objupdatedate = (from row in entities.BM_MeetingModule
                                          where row.IsActive == true 
                                          && row.CustomerId == _objvmmeeting.customerId 
                                          && row.EntityId == _objvmmeeting.Entity
                                          select row).FirstOrDefault();
                    if (_objupdatedate == null)
                    {
                        BM_MeetingModule obj = new BM_MeetingModule();
                        obj.EntityId = _objvmmeeting.Entity;
                        obj.CustomerId = _objvmmeeting.customerId;
                        obj.FY = _objvmmeeting.FY;
                        obj.BoardMeetingId = _objvmmeeting.BoardMeeting;
                        if (_objvmmeeting.BoardMeeting == 10)
                        {
                            obj.Type = _objvmmeeting.Type;
                        }
                        else
                        {
                            obj.Type = "";
                        }
                        obj.IsActive = true;
                        obj.CreatedOn = DateTime.Now;
                        entities.BM_MeetingModule.Add(obj);
                        entities.SaveChanges();
                        _objvmmeeting.successMessage = true;
                        _objvmmeeting.Message = "Meeting save successfully";
                        return _objvmmeeting;
                    }
                    else
                    {
                        _objvmmeeting.errorMessage = true;
                        _objvmmeeting.Message = "Somthing wents wrong";
                        return _objvmmeeting;
                    }

                }
            }
            catch (Exception ex)
            {
                _objvmmeeting.errorMessage = true;
                _objvmmeeting.Message = "Server error occured";
                return _objvmmeeting;
            }
        }
    }
}