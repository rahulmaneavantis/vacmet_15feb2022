﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;
using Microsoft.Ajax.Utilities;

namespace BM_ManegmentServices.Services.Masters
{
    public class AgendaMaster : IAgendaMaster
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        #region Role-based Agenda list
        public List<AgendaMasterVM> GetAll(int customerId, string role)
        {
            try
            {
                var result = (from row in entities.BM_AgendaMaster
                              where row.Parent_Id == null && row.IsDeleted == false
                              select new AgendaMasterVM
                              {
                                  BM_AgendaMasterId = row.BM_AgendaMasterId,
                                  AgendaHeading = row.AgendaItemHeading,
                                  //Agenda = row.Agenda,
                                  EntityType = row.EntityType,
                                  MeetingTypeId = row.MeetingTypeId,
                                  EntityTypeName = row.BM_EntityType.EntityName,
                                  MeetingTypeName = row.BM_CommitteeComp.MeetingTypeName,
                                  PartId = row.PartId,
                                  Customer_Id = row.Customer_Id,
                                  IsApproved = row.IsApproved,
                                  AgendaInfomative = row.InfoText,
                                  HasInfo = row.HasInfo,
                                  IsMultiStageAgenda = row.IsMultiStageAgenda == null ? false : row.IsMultiStageAgenda,
                                  LastUpdatedOn = row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn,
                                  IsLive = row.IsLive
                              });

                if (role == SecretarialConst.Roles.CSIMP)
                {
                    return (from row in result
                            where row.Customer_Id == null
                            select row).ToList();
                }
                else
                {
                    return (from row in result
                            where ((row.Customer_Id == null && row.IsLive == true) || row.Customer_Id == customerId)
                            select row).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<AgendaMasterVM> GetAll_CustomerEntityTypeWise(int customerId, string role, string productApplicableLogin)
        {
            List<AgendaMasterVM> lstAgendaMaster = new List<AgendaMasterVM>();
            try
            {
                var query = (from row in entities.BM_AgendaMaster
                            join com in entities.BM_CommitteeComp on row.MeetingTypeId equals com.Id
                            where row.Parent_Id == null && row.IsDeleted == false
                            && row.IsCreatedInMeeting == false  // Added on 30 Dec 2021
                            select new AgendaMasterVM
                            {
                                BM_AgendaMasterId = row.BM_AgendaMasterId,
                                AgendaHeading = row.AgendaItemHeading,
                                //Agenda = row.Agenda,
                                EntityType = row.EntityType,
                                MeetingTypeId = row.MeetingTypeId,
                                EntityTypeName = row.BM_EntityType.EntityName,
                                MeetingTypeName = com.MeetingTypeName,
                                PartId = row.PartId,
                                Customer_Id = row.Customer_Id,
                                IsApproved = row.IsApproved,
                                AgendaInfomative = row.InfoText,
                                HasInfo = row.HasInfo,
                                IsMultiStageAgenda = row.IsMultiStageAgenda == null ? false : row.IsMultiStageAgenda,
                                LastUpdatedOn = row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn,
                                IsOrdinaryBusiness = row.IsOrdinaryBusiness,
                                IsLive = row.IsLive,
Doctype=row.DocumentStatus,
                                DocumentType = new DocumentTypeViewdata
                                {
                                    Type = row.DocumentStatus,
                                    TypeName = row.DocumentStatus!=null? row.DocumentStatus.Trim():"",
                                },
                            });

                if (role == SecretarialConst.Roles.CSIMP)
                {
                    lstAgendaMaster = (from row in query
                                       where row.Customer_Id == null
                                       select row).ToList();
                }
                else
                {
                    lstAgendaMaster = (from row in query
                                       where ((row.Customer_Id == null  && row.IsLive == true)|| row.Customer_Id == customerId)
                                       select row).ToList();

                    if (lstAgendaMaster.Count > 0)
                    {
                        var lstEntityTypeCustWise = new List<int>();
                        if(productApplicableLogin == "DFM")
                        {
                            lstEntityTypeCustWise = (from row in entities.Customers
                                                    join entity in entities.BM_EntityMaster on row.ID equals entity.Customer_Id
                                                    where (row.ID == customerId || row.ParentID == customerId) && row.IsDeleted == false && entity.Is_Deleted == false
                                                    select entity.Entity_Type).Distinct().ToList();
                        }
                        else
                        {
                            lstEntityTypeCustWise = entities.BM_EntityMaster.Where(row => row.Customer_Id == customerId && row.Entity_Type != null).Select(row => row.Entity_Type).Distinct().ToList();
                        }
                        

                        if (lstEntityTypeCustWise.Count > 0)
                        {
                            lstAgendaMaster = lstAgendaMaster.Where(row => row.EntityType != null && lstEntityTypeCustWise.Contains((int)row.EntityType)).ToList();
                        }
                    }
                }

                return lstAgendaMaster;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        public List<AgendaMasterVM> GetAll_Agendadetils(int customerId, string role, string productApplicableLogin)
        {
            List<AgendaMasterVM> lstAgendaMaster = new List<AgendaMasterVM>();
            try
            {
                var query = (from row in entities.BM_AgendaMaster
                             join com in entities.BM_CommitteeComp on row.MeetingTypeId equals com.Id
                             where row.Parent_Id == null && row.IsDeleted == false
                             select new AgendaMasterVM
                             {
                                 BM_AgendaMasterId = row.BM_AgendaMasterId,
                                 AgendaHeading = row.AgendaItemHeading,
                                 Agenda = row.Agenda,
                                 EntityType = row.EntityType,
                                 MeetingTypeId = row.MeetingTypeId,
                                 EntityTypeName = row.BM_EntityType.EntityName,
                                 MeetingTypeName = com.MeetingTypeName,
                                 PartId = row.PartId,
                                 Customer_Id = row.Customer_Id,
                                 IsApproved = row.IsApproved,
                               //  AgendaInfomative = row.InfoText,
                                 HasInfo = row.HasInfo,
                                 IsMultiStageAgenda = row.IsMultiStageAgenda == null ? false : row.IsMultiStageAgenda,
                                 LastUpdatedOn = row.UpdatedOn == null ? row.CreatedOn : row.UpdatedOn,
                                 IsOrdinaryBusiness = row.IsOrdinaryBusiness,
                                 IsLive = row.IsLive,
                                 DocumentType = new DocumentTypeViewdata
                                 {
                                     Type = row.DocumentStatus,
                                     TypeName = row.DocumentStatus != null ? row.DocumentStatus.Trim() : "",
                                 },
                             });

                if (role == SecretarialConst.Roles.CSIMP)
                {
                    lstAgendaMaster = (from row in query
                                       where row.Customer_Id == null
                                       select row).ToList();
                }
                else
                {
                    lstAgendaMaster = (from row in query
                                       where ((row.Customer_Id == null && row.IsLive == true) || row.Customer_Id == customerId)
                                       select row).ToList();

                    if (lstAgendaMaster.Count > 0)
                    {
                        var lstEntityTypeCustWise = new List<int>();
                        if (productApplicableLogin == "DFM")
                        {
                            lstEntityTypeCustWise = (from row in entities.Customers
                                                     join entity in entities.BM_EntityMaster on row.ID equals entity.Customer_Id
                                                     where (row.ID == customerId || row.ParentID == customerId) && row.IsDeleted == false && entity.Is_Deleted == false
                                                     select entity.Entity_Type).Distinct().ToList();
                        }
                        else
                        {
                            lstEntityTypeCustWise = entities.BM_EntityMaster.Where(row => row.Customer_Id == customerId && row.Entity_Type != null).Select(row => row.Entity_Type).Distinct().ToList();
                        }


                        if (lstEntityTypeCustWise.Count > 0)
                        {
                            lstAgendaMaster = lstAgendaMaster.Where(row => row.EntityType != null && lstEntityTypeCustWise.Contains((int)row.EntityType)).ToList();
                        }
                    }
                }

                return lstAgendaMaster;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Customer-wise Standard Agenda 
        public StandardAgendaVM GetStadardAgenda(int customerId, long id)
        {
            try
            {
                var result = (from row in entities.BM_AgendaMaster
                              where row.BM_AgendaMasterId == id && row.Parent_Id == null && row.IsDeleted == false
                              select new StandardAgendaVM
                              {
                                  AgendaHeading = row.AgendaItemHeading,
                                  EntityType = row.EntityType,
                                  MeetingTypeId = row.MeetingTypeId,
                                  EntityTypeName = row.BM_EntityType.EntityName,
                                  MeetingTypeName = row.BM_CommitteeComp.MeetingTypeName
                              }).FirstOrDefault();

                if (result != null)
                {
                    result.formates = (from row in entities.BM_SP_AgendaFormatesByCustomerId(customerId, id)
                                       select new StandardAgendaFormatesVM
                                       {
                                           AgendaMasterId = row.BM_AgendaMasterId,
                                           StandardAgendaMasterId = row.ID,

                                           AgendaItemText = row.AgendaItemText,
                                           AgendaFormat = row.AgendaFormat,

                                           ResolutionFormatHeading = row.ResolutionFormatHeading,
                                           ResolutionFormat = row.ResolutionFormat,

                                           MinutesFormatHeading = row.MinutesFormatHeading,
                                           MinutesFormat = row.MinutesFormat,
                                           MinutesDisApproveFormat = row.MinutesDisApproveFormat,
                                           MinutesDifferFormat = row.MinutesDifferFormat,

                                           SEBI_IntimationFormat = row.SEBI_IntimationFormat,
                                           SEBI_DisclosureFormat = row.SEBI_DisclosureFormat
                                       }).FirstOrDefault();
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public StandardAgendaFormates_SubVM GetPreCommitteeStadardAgenda(int customerId, long id)
        {
            try
            {
                var result = (from row in entities.BM_SP_AgendaFormatesByCustomerId(customerId, id)
                              select new StandardAgendaFormates_SubVM
                              {
                                  AgendaMasterIdSub = row.BM_AgendaMasterId,
                                  StandardAgendaMasterIdSub = row.ID,

                                  AgendaItemTextSub = row.AgendaItemText,
                                  AgendaFormatSub = row.AgendaFormat,

                                  ResolutionFormatHeadingSub = row.ResolutionFormatHeading,
                                  ResolutionFormatSub = row.ResolutionFormat,

                                  MinutesFormatHeadingSub = row.MinutesFormatHeading,
                                  MinutesFormatSub = row.MinutesFormat,
                                  MinutesDisApproveFormatSub = row.MinutesDisApproveFormat,
                                  MinutesDifferFormatSub = row.MinutesDifferFormat
                              }).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public StandardAgendaFormatesVM SaveAgendaFormates(StandardAgendaFormatesVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_AgendaMasterFormates
                            where row.AgendaId == obj.AgendaMasterId && row.CustomerID == obj.CustomerID
                            select row).FirstOrDefault();

                if (_obj == null)
                {
                    _obj = new BM_AgendaMasterFormates() { ID = 0 };

                    _obj.AgendaId = obj.AgendaMasterId;
                    _obj.CustomerID = obj.CustomerID;

                    _obj.AgendaItemText = obj.AgendaItemText;
                    _obj.AgendaFormat = obj.AgendaFormat;

                    _obj.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                    _obj.ResolutionFormat = obj.ResolutionFormat;

                    _obj.MinutesFormatHeading = obj.MinutesFormatHeading;
                    _obj.MinutesFormat = obj.MinutesFormat;
                    _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormat;
                    _obj.MinutesDifferFormat = obj.MinutesDifferFormat;

                    _obj.SEBI_IntimationFormat = obj.SEBI_IntimationFormat;
                    _obj.SEBI_DisclosureFormat = obj.SEBI_DisclosureFormat;

                    _obj.IsDeleted = false;
                    _obj.CreatedBy = userId;
                    _obj.CreatedOn = DateTime.Now;

                    entities.BM_AgendaMasterFormates.Add(_obj);
                    entities.SaveChanges();

                    obj.StandardAgendaMasterId = _obj.ID;
                }
                else
                {
                    _obj.AgendaItemText = obj.AgendaItemText;
                    _obj.AgendaFormat = obj.AgendaFormat;

                    _obj.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                    _obj.ResolutionFormat = obj.ResolutionFormat;

                    _obj.MinutesFormatHeading = obj.MinutesFormatHeading;
                    _obj.MinutesFormat = obj.MinutesFormat;
                    _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormat;
                    _obj.MinutesDifferFormat = obj.MinutesDifferFormat;

                    _obj.SEBI_IntimationFormat = obj.SEBI_IntimationFormat;
                    _obj.SEBI_DisclosureFormat = obj.SEBI_DisclosureFormat;

                    _obj.IsDeleted = false;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                }

                obj.Success = true;
                obj.Message = "Update successfully.";
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public StandardAgendaFormates_SubVM SaveAgendaFormates(StandardAgendaFormates_SubVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_AgendaMasterFormates
                            where row.AgendaId == obj.AgendaMasterIdSub && row.CustomerID == obj.CustomerID
                            select row).FirstOrDefault();

                if (_obj == null)
                {
                    _obj = new BM_AgendaMasterFormates() { ID = 0 };

                    _obj.AgendaId = obj.AgendaMasterIdSub;
                    _obj.CustomerID = obj.CustomerID;

                    _obj.AgendaItemText = obj.AgendaItemTextSub;
                    _obj.AgendaFormat = obj.AgendaFormatSub;

                    _obj.ResolutionFormatHeading = obj.ResolutionFormatHeadingSub;
                    _obj.ResolutionFormat = obj.ResolutionFormatSub;

                    _obj.MinutesFormatHeading = obj.MinutesFormatHeadingSub;
                    _obj.MinutesFormat = obj.MinutesFormatSub;
                    _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormatSub;
                    _obj.MinutesDifferFormat = obj.MinutesDifferFormatSub;

                    _obj.IsDeleted = false;
                    _obj.CreatedBy = userId;
                    _obj.CreatedOn = DateTime.Now;

                    entities.BM_AgendaMasterFormates.Add(_obj);
                    entities.SaveChanges();

                    obj.StandardAgendaMasterIdSub = _obj.ID;
                }
                else
                {
                    _obj.AgendaItemText = obj.AgendaItemTextSub;
                    _obj.AgendaFormat = obj.AgendaFormatSub;

                    _obj.ResolutionFormatHeading = obj.ResolutionFormatHeadingSub;
                    _obj.ResolutionFormat = obj.ResolutionFormatSub;

                    _obj.MinutesFormatHeading = obj.MinutesFormatHeadingSub;
                    _obj.MinutesFormat = obj.MinutesFormatSub;
                    _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormatSub;
                    _obj.MinutesDifferFormat = obj.MinutesDifferFormatSub;

                    _obj.IsDeleted = false;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                }

                obj.Success = true;
                obj.Message = "Update successfully.";
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Agenda

        public List<AgendaMasterVM> GetAll()
        {
            try
            {
                var result = (from row in entities.BM_AgendaMaster
                              where row.Parent_Id == null && row.IsDeleted == false
                              select new AgendaMasterVM
                              {
                                  BM_AgendaMasterId = row.BM_AgendaMasterId,
                                  AgendaHeading = row.AgendaItemHeading,
                                  Agenda = row.Agenda,
                                  EntityType = row.EntityType,
                                  MeetingTypeId = row.MeetingTypeId,
                                  EntityTypeName = row.BM_EntityType.EntityName,//  e.EntityName,
                                  MeetingTypeName = row.BM_CommitteeComp.MeetingTypeName,
                                  IsForAllEntityType = row.IsForAllEntityType,
                                  FrequencyId = row.FrequencyId,
                                  PreCommitteeMeetingTypeId = row.PreCommitteeMeetingTypeId,
                                  IsForAllMeetingType = row.IsForAllMeetingType,
                                  IsVoting = row.IsVoting,
                                  IsNoting = row.IsNoting,
                                  PartId = row.PartId,
                                  IsMultiStageAgenda = row.IsMultiStageAgenda,
                                  IsPostBallotRequired = row.IsPostBallotRequired,
                                  CanPassedByCircularResolution = row.CanPassedByCircularResolution,
                                  IsSubAgenda = row.IsSubAgenda,
                                  //BM_SubAgendaMaster = entities.BM_AgendaMaster.Where(k => k.Parent_Id == row.BM_AgendaMasterId ).Select(k => new SubAgendaMasterVM {BM_AgendaMasterId = (long) k.Parent_Id , BM_SubAgendaMasterId = k.BM_AgendaMasterId, SubAgenda = k.Agenda }).ToList()

                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public AgendaMasterVM GetAgenda(long Id)
        {
            try
            {
                var result = (from row in entities.BM_AgendaMaster
                              where row.BM_AgendaMasterId == Id && row.Parent_Id == null && row.IsDeleted == false
                              select new AgendaMasterVM
                              {
                                  BM_AgendaMasterId = row.BM_AgendaMasterId,
                                  AgendaHeading = row.AgendaItemHeading,
                                  Agenda = row.Agenda,
                                  EntityType = row.EntityType,
                                  MeetingTypeId = row.MeetingTypeId,
                                  DefaultAgendaFor = row.DefaultAgendaFor,
                                  UIFormID = row.UIFormID,
                                  EntityTypeName = row.BM_EntityType.EntityName,//  e.EntityName,
                                  MeetingTypeName = row.BM_CommitteeComp.MeetingTypeName,
                                  IsForAllEntityType = row.IsForAllEntityType,
                                  FrequencyId = row.FrequencyId,
                                  PreCommitteeMeetingTypeId = row.PreCommitteeMeetingTypeId,
                                  IsForAllMeetingType = row.IsForAllMeetingType,
                                  IsVoting = row.IsVoting,
                                  IsNoting = row.IsNoting,
                                  CanMultiple = row.CanMultiple,
                                  PartId = row.PartId,
                                  IsMultiStageAgenda = row.IsMultiStageAgenda,
                                  IsPostBallotRequired = row.IsPostBallotRequired,
                                  CanPassedByCircularResolution = row.CanPassedByCircularResolution,
                                  AgendaFormat = row.AgendaFormat,
                                  ResolutionFormat = row.ResolutionFormat,
                                  MinutesFormat = row.MinutesFormat,
                                  ResolutionFormatHeading = row.ResolutionFormatHeading,
                                  MinutesFormatHeading = row.MinutesFormatHeading,
                                  MinutesDisApproveFormat = row.MinutesDisApproveFormat,
                                  MinutesDifferFormat = row.MinutesDifferFormat,
                                  SEBI_IntimationFormat = row.SEBI_IntimationFormat,
                                  SEBI_DisclosureFormat = row.SEBI_DisclosureFormat,

                                  IsSubAgenda = row.IsSubAgenda,

                                  AgendaInfomative = row.InfoText,
                                  HasInfo = row.HasInfo,
                                  IsOrdinaryBusiness = row.IsOrdinaryBusiness,
                                  IsSpecialResolution = row.IsSpecialResolution

                              }).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public AgendaMasterVM Create(AgendaMasterVM obj, int customerId, string role)
        {
            try
            {
                obj.Message = new Response();

                BM_AgendaMaster _obj = new BM_AgendaMaster();
                _obj.AgendaItemHeading = obj.AgendaHeading;
                _obj.Agenda = obj.Agenda;
                _obj.EntityType = obj.EntityType;
                _obj.MeetingTypeId = obj.MeetingTypeId;

                if (role == BM_ManegmentServices.SecretarialConst.Roles.CSIMP)
                {
                    obj.Customer_Id = null;
                    if (!string.IsNullOrEmpty(obj.DefaultAgendaFor))
                    {
                        var DefaultAgendaExists = (from row in entities.BM_AgendaMaster
                                                   where row.EntityType == obj.EntityType && row.MeetingTypeId == obj.MeetingTypeId &&
                                                   row.DefaultAgendaFor == obj.DefaultAgendaFor && row.IsDeleted == false
                                                   select row.BM_AgendaMasterId).Any();
                        if (DefaultAgendaExists == true)
                        {
                            obj.Message.Error = true;
                            obj.Message.Message = "Default Agenda allready set for other Agenda";
                            return obj;
                        }
                    }
                    _obj.DefaultAgendaFor = obj.DefaultAgendaFor;

                    _obj.UIFormID = obj.UIFormID;
                }
                else
                {
                    obj.Customer_Id = customerId;
                    obj.IsLive = true;
                }

                _obj.Customer_Id = obj.Customer_Id;

                _obj.FrequencyId = obj.FrequencyId;
                _obj.PreCommitteeMeetingTypeId = obj.PreCommitteeMeetingTypeId;
                _obj.IsVoting = obj.IsVoting;
                _obj.IsNoting = obj.IsNoting;
                _obj.CanMultiple = obj.CanMultiple;
                _obj.PartId = obj.PartId;
                _obj.IsMultiStageAgenda = obj.IsMultiStageAgenda;
                if (obj.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || obj.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    _obj.IsPostBallotRequired = obj.IsPostBallotRequired;
                    _obj.CanPassedByCircularResolution = obj.IsPostBallotRequired;
                    _obj.IsOrdinaryBusiness = obj.IsOrdinaryBusiness;
                    _obj.IsSpecialResolution = obj.IsSpecialResolution;
                    if (obj.IsOrdinaryBusiness == true)
                    {
                        _obj.PartId = 1;
                        obj.PartId = 1;
                    }
                    else
                    {
                        _obj.PartId = 2;
                        obj.PartId = 2;
                    }
                }
                else
                {
                    _obj.IsPostBallotRequired = false;
                    _obj.CanPassedByCircularResolution = obj.CanPassedByCircularResolution;
                    _obj.IsOrdinaryBusiness = false;
                    _obj.IsSpecialResolution = false;
                }

                _obj.IsSubAgenda = obj.IsSubAgenda;

                _obj.IsDeleted = false;
                _obj.CreatedBy = obj.UserId;
                _obj.CreatedOn = DateTime.Now;

                _obj.AgendaFormat = obj.AgendaFormat;
                _obj.ResolutionFormat = obj.ResolutionFormat;
                _obj.MinutesFormat = obj.MinutesFormat;
                _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormat;
                _obj.MinutesDifferFormat = obj.MinutesDifferFormat;

                _obj.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                _obj.MinutesFormatHeading = obj.MinutesFormatHeading;

                _obj.SEBI_IntimationFormat = obj.SEBI_IntimationFormat;
                _obj.SEBI_DisclosureFormat = obj.SEBI_DisclosureFormat;

                if (string.IsNullOrEmpty(obj.Agenda) || string.IsNullOrEmpty(obj.AgendaFormat) || string.IsNullOrEmpty(obj.MinutesFormatHeading) || string.IsNullOrEmpty(obj.MinutesFormat) || string.IsNullOrEmpty(obj.MinutesDisApproveFormat))
                {
                    _obj.IsApproved = false;
                }
                else
                {
                    _obj.IsApproved = true;
                }
                _obj.HasCompliance = false;
                _obj.HasTemplate = false;

                _obj.Parent_Id = obj.Parent_Id;
                entities.BM_AgendaMaster.Add(_obj);
                entities.SaveChanges();

                if (_obj.BM_AgendaMasterId > 0 && obj.UIFormID > 0)
                {
                    entities.BM_SP_AgendaMasterSetUIForm(_obj.BM_AgendaMasterId, obj.UserId);
                    entities.SaveChanges();
                }
                obj.BM_AgendaMasterId = _obj.BM_AgendaMasterId;
                obj.Message.Success = true;
                obj.Message.Message = "Saved Successfully.";
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public AgendaMasterVM Update(AgendaMasterVM obj, string role)
        {
            try
            {
                obj.Message = new Response();
                var IsUIFormChanged = false;

                if (role == BM_ManegmentServices.SecretarialConst.Roles.CSIMP)
                {
                    if (!string.IsNullOrEmpty(obj.DefaultAgendaFor))
                    {
                        var DefaultAgendaExists = (from row in entities.BM_AgendaMaster
                                                   where row.BM_AgendaMasterId != obj.BM_AgendaMasterId && row.EntityType == obj.EntityType && row.MeetingTypeId == obj.MeetingTypeId &&
                                                   row.DefaultAgendaFor == obj.DefaultAgendaFor && row.IsDeleted == false
                                                   select row.BM_AgendaMasterId).Any();

                        if (DefaultAgendaExists == true)
                        {
                            obj.Message.Error = true;
                            obj.Message.Message = "Default Agenda allready set for other Agenda";
                            return obj;
                        }
                    }
                }

                var isUsedAsMultistage = entities.BM_AgendaMaster_SubAgendaMappging.Where(k => k.AgendaMasterId == obj.BM_AgendaMasterId && k.IsDeleted == false).Any();
                var isUsed = entities.BM_AgendaMaster_SubAgendaMappging.Where(k => k.SubAgendaMasterId == obj.BM_AgendaMasterId && k.IsDeleted == false).Any();

                BM_AgendaMaster _obj = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == obj.BM_AgendaMasterId && k.IsDeleted == false).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.AgendaItemHeading = obj.AgendaHeading;
                    _obj.Agenda = obj.Agenda;
                    _obj.EntityType = obj.EntityType;
                    //_obj.MeetingTypeId = obj.MeetingTypeId;
                    obj.MeetingTypeId = _obj.MeetingTypeId;

                    if (role == BM_ManegmentServices.SecretarialConst.Roles.CSIMP)
                    {
                        _obj.DefaultAgendaFor = obj.DefaultAgendaFor;
                        if (_obj.UIFormID != obj.UIFormID)
                        {
                            _obj.UIFormID = obj.UIFormID;
                            IsUIFormChanged = true;
                        }
                    }
                    else
                    {
                        _obj.IsLive = true;
                    }

                    _obj.FrequencyId = obj.FrequencyId;
                    _obj.PreCommitteeMeetingTypeId = obj.PreCommitteeMeetingTypeId;
                    _obj.IsVoting = obj.IsVoting;
                    _obj.CanMultiple = obj.CanMultiple;
                    _obj.IsNoting = obj.IsNoting;
                    _obj.PartId = obj.PartId;

                    if (isUsed == true)
                    {
                        obj.IsMultiStageAgenda = false;
                    }
                    else if (isUsedAsMultistage == true)
                    {
                        obj.IsMultiStageAgenda = true;
                    }
                    _obj.IsMultiStageAgenda = obj.IsMultiStageAgenda;

                    if (obj.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || obj.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                    {
                        _obj.IsPostBallotRequired = obj.IsPostBallotRequired;
                        _obj.CanPassedByCircularResolution = obj.IsPostBallotRequired;
                        _obj.IsOrdinaryBusiness = obj.IsOrdinaryBusiness;
                        _obj.IsSpecialResolution = obj.IsSpecialResolution;

                        if (obj.IsOrdinaryBusiness == true)
                        {
                            _obj.PartId = 1;
                            obj.PartId = 1;
                        }
                        else
                        {
                            _obj.PartId = 2;
                            obj.PartId = 2;
                        }
                    }
                    else
                    {
                        _obj.IsPostBallotRequired = false;
                        _obj.CanPassedByCircularResolution = obj.CanPassedByCircularResolution;
                        _obj.IsOrdinaryBusiness = false;
                        _obj.IsSpecialResolution = false;
                    }

                    _obj.IsSubAgenda = obj.IsSubAgenda;

                    _obj.IsDeleted = false;
                    _obj.UpdatedBy = obj.UserId;
                    _obj.UpdatedOn = DateTime.Now;


                    _obj.AgendaFormat = obj.AgendaFormat;
                    _obj.ResolutionFormat = obj.ResolutionFormat;
                    _obj.MinutesFormat = obj.MinutesFormat;
                    _obj.MinutesDisApproveFormat = obj.MinutesDisApproveFormat;
                    _obj.MinutesDifferFormat = obj.MinutesDifferFormat;

                    _obj.ResolutionFormatHeading = obj.ResolutionFormatHeading;
                    _obj.MinutesFormatHeading = obj.MinutesFormatHeading;


                    _obj.SEBI_IntimationFormat = obj.SEBI_IntimationFormat;
                    _obj.SEBI_DisclosureFormat = obj.SEBI_DisclosureFormat;

                    if (string.IsNullOrEmpty(obj.Agenda) || string.IsNullOrEmpty(obj.AgendaFormat) || string.IsNullOrEmpty(obj.MinutesFormatHeading) || string.IsNullOrEmpty(obj.MinutesFormat) || string.IsNullOrEmpty(obj.MinutesDisApproveFormat))
                    {
                        _obj.IsApproved = false;
                    }
                    else
                    {
                        _obj.IsApproved = true;
                    }

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    if (IsUIFormChanged)
                    {
                        entities.BM_SP_AgendaMasterSetUIForm(_obj.BM_AgendaMasterId, obj.UserId);
                        entities.SaveChanges();
                    }

                    #region Set Deleted Flag
                    //entities.BM_AgendaMaster_EntityTypeMapping.Where(k => k.AgendaMasterId == _obj.BM_AgendaMasterId && k.IsDeleted == false).ToList().ForEach(k => k.IsDeleted = true);
                    //entities.SaveChanges();

                    //entities.BM_AgendaMaster_MeetingTypeMapping.Where(k => k.AgendaMasterId == _obj.BM_AgendaMasterId && k.IsDeleted == false).ToList().ForEach(k => k.IsDeleted = true);
                    //entities.SaveChanges();
                    #endregion

                    obj.Message.Success = true;
                    obj.Message.Message = "Updated Successfully.";
                }
                else
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        #endregion

        #region Sub Agenda (Pre-Committee)
        public List<SubAgendaMasterVM> GetAllSubAgendas(long ParentId)
        {
            try
            {
                var IsExists = (from row in entities.BM_AgendaMaster
                                where row.BM_AgendaMasterId == ParentId && row.Parent_Id == null && row.IsDeleted == false
                                select true).FirstOrDefault();

                if (IsExists == true)
                {
                    var result = (from row in entities.BM_AgendaMaster
                                  from meeting in entities.BM_CommitteeComp.Where(k => k.Id == row.MeetingTypeId).DefaultIfEmpty()
                                  where row.Parent_Id == ParentId && row.IsDeleted == false
                                  select new SubAgendaMasterVM
                                  {
                                      BM_AgendaMasterId = (long)row.Parent_Id,
                                      BM_SubAgendaMasterId = row.BM_AgendaMasterId,
                                      SubAgendaHeading = row.AgendaItemHeading,
                                      SubAgenda = row.Agenda,
                                      MeetingTypeIdSub = row.MeetingTypeId,
                                      Sequence = row.SequenceNo,
                                      StageId = row.StageId,
                                      CanPassedByCircularResolutionSub = row.CanPassedByCircularResolution,
                                      IsPostBallotRequiredSub = row.IsPostBallotRequired,
                                      IsVotingSub = row.IsVoting,

                                      AgendaFormatSub = row.AgendaFormat,
                                      ResolutionFormatSub = row.ResolutionFormat,
                                      MinutesFormatSub = row.MinutesFormat,
                                      MeetingTypeNameSub = meeting.MeetingTypeName,
                                      SubHasInfo = row.HasInfo,
                                      SubAgendaInfomative = row.InfoText
                                  }).ToList();

                    return result;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public List<AgendaMasterVM> GetAllSubAgenda(long id)
        {
            try
            {
                var result = (from row in entities.BM_AgendaMaster
                                  //from e in entities.BM_EntityType.Where( k=> k.Id == row.EntityType).DefaultIfEmpty()
                                  //from c in entities.BM_CommitteeComp.Where(k => k.Id == row.MeetingTypeId).DefaultIfEmpty()
                              where row.Parent_Id == id && row.IsDeleted == false
                              select new AgendaMasterVM
                              {
                                  BM_AgendaMasterId = row.BM_AgendaMasterId,
                                  AgendaHeading = row.AgendaItemHeading,
                                  Agenda = row.Agenda,
                                  EntityType = row.EntityType,
                                  MeetingTypeId = row.MeetingTypeId,
                                  EntityTypeName = row.BM_EntityType.EntityName,//  e.EntityName,
                                  MeetingTypeName = row.BM_CommitteeComp.MeetingTypeName,
                                  IsForAllEntityType = row.IsForAllEntityType,
                                  FrequencyId = row.FrequencyId,
                                  PreCommitteeMeetingTypeId = row.PreCommitteeMeetingTypeId,
                                  IsForAllMeetingType = row.IsForAllMeetingType,
                                  IsVoting = row.IsVoting,
                                  IsNoting = row.IsNoting,
                                  PartId = row.PartId,
                                  IsMultiStageAgenda = row.IsMultiStageAgenda,
                                  IsPostBallotRequired = row.IsPostBallotRequired,
                                  CanPassedByCircularResolution = row.CanPassedByCircularResolution,

                                  IsSubAgenda = row.IsSubAgenda,
                                  Parent_Id = row.Parent_Id
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public SubAgendaMasterVM Create(SubAgendaMasterVM obj)
        {
            try
            {
                var parentAgenda = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == obj.BM_AgendaMasterId && k.IsDeleted == false).FirstOrDefault();
                obj.Message = new Response();
                BM_AgendaMaster subAgenda = new BM_AgendaMaster();
                subAgenda.BM_AgendaMasterId = 0;
                subAgenda.Parent_Id = obj.BM_AgendaMasterId;
                //subAgenda.FrequencyId = obj.FrequencyId;
                //subAgenda.EntityType = obj.EntityType;

                if (parentAgenda != null)
                {
                    subAgenda.EntityType = parentAgenda.EntityType;
                    subAgenda.PartId = 2;
                }

                subAgenda.AgendaItemHeading = obj.SubAgendaHeading;
                subAgenda.Agenda = obj.SubAgenda;
                subAgenda.MeetingTypeId = obj.MeetingTypeIdSub;
                subAgenda.StageId = obj.StageId;
                subAgenda.SequenceNo = obj.Sequence;
                subAgenda.CanPassedByCircularResolution = obj.CanPassedByCircularResolutionSub;
                subAgenda.IsPostBallotRequired = obj.IsPostBallotRequiredSub;
                subAgenda.IsVoting = obj.IsVotingSub;

                subAgenda.AgendaFormat = obj.AgendaFormatSub;
                subAgenda.ResolutionFormatHeading = obj.ResolutionFormatHeadingSub;
                subAgenda.ResolutionFormat = obj.ResolutionFormatSub;
                subAgenda.MinutesFormatHeading = obj.MinutesFormatHeadingSub;
                subAgenda.MinutesFormat = obj.MinutesFormatSub;
                subAgenda.MinutesDisApproveFormat = obj.MinutesDisApproveFormatSub;
                subAgenda.MinutesDifferFormat = obj.MinutesDifferFormatSub;


                subAgenda.IsSubAgenda = obj.IsSubAgenda;
                subAgenda.IsDeleted = false;
                subAgenda.CreatedOn = DateTime.Now;
                subAgenda.CreatedBy = obj.UserId;

                subAgenda.IsApproved = true;
                subAgenda.HasCompliance = false;
                subAgenda.HasTemplate = false;


                subAgenda.InfoText = obj.SubAgendaInfomative;
                if (!String.IsNullOrEmpty(obj.SubAgendaInfomative))
                {
                    subAgenda.HasInfo = true;
                }
                else
                {
                    subAgenda.HasInfo = true;
                }


                entities.BM_AgendaMaster.Add(subAgenda);
                entities.SaveChanges();

                obj.BM_SubAgendaMasterId = subAgenda.BM_AgendaMasterId;
                obj.Message.Success = true;
                obj.Message.Message = "Saved Successfully.";
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public SubAgendaMasterVM Update(SubAgendaMasterVM obj)
        {
            try
            {
                obj.Message = new Response();
                BM_AgendaMaster subAgenda = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == obj.BM_SubAgendaMasterId && k.IsDeleted == false).FirstOrDefault();
                if (subAgenda != null)
                {
                    //subAgenda.FrequencyId = obj.FrequencyId;
                    //subAgenda.EntityType = obj.EntityType;
                    subAgenda.AgendaItemHeading = obj.SubAgendaHeading;
                    subAgenda.Agenda = obj.SubAgenda;
                    subAgenda.MeetingTypeId = obj.MeetingTypeIdSub;
                    subAgenda.StageId = obj.StageId;
                    subAgenda.SequenceNo = obj.Sequence;
                    subAgenda.CanPassedByCircularResolution = obj.CanPassedByCircularResolutionSub;
                    subAgenda.IsPostBallotRequired = obj.IsPostBallotRequiredSub;
                    subAgenda.IsVoting = obj.IsVotingSub;

                    subAgenda.AgendaFormat = obj.AgendaFormatSub;
                    subAgenda.ResolutionFormatHeading = obj.ResolutionFormatHeadingSub;
                    subAgenda.ResolutionFormat = obj.ResolutionFormatSub;
                    subAgenda.MinutesFormatHeading = obj.MinutesFormatHeadingSub;
                    subAgenda.MinutesFormat = obj.MinutesFormatSub;
                    subAgenda.MinutesDisApproveFormat = obj.MinutesDisApproveFormatSub;
                    subAgenda.MinutesDifferFormat = obj.MinutesDifferFormatSub;


                    subAgenda.IsSubAgenda = obj.IsSubAgenda;

                    subAgenda.UpdatedOn = DateTime.Now;
                    subAgenda.UpdatedBy = obj.UserId;

                    subAgenda.InfoText = obj.SubAgendaInfomative;
                    if (!String.IsNullOrEmpty(obj.SubAgendaInfomative))
                    {
                        subAgenda.HasInfo = true;
                    }
                    else
                    {
                        subAgenda.HasInfo = true;
                    }

                    subAgenda.IsApproved = true;
                    entities.Entry(subAgenda).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    obj.Message.Success = true;
                    obj.Message.Message = "Updated Successfully.";
                }
                else
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }

        public SubAgendaMasterVM GetSubAgenda(long Id)
        {
            try
            {
                var result = (from row in entities.BM_AgendaMaster
                              where row.BM_AgendaMasterId == Id && row.IsDeleted == false
                              select new SubAgendaMasterVM
                              {
                                  BM_AgendaMasterId = (long)row.Parent_Id,
                                  BM_SubAgendaMasterId = row.BM_AgendaMasterId,
                                  SubAgendaHeading = row.AgendaItemHeading,
                                  SubAgenda = row.Agenda,
                                  MeetingTypeIdSub = row.MeetingTypeId,
                                  Sequence = row.SequenceNo,
                                  StageId = row.StageId,
                                  CanPassedByCircularResolutionSub = row.CanPassedByCircularResolution,
                                  IsPostBallotRequiredSub = row.IsPostBallotRequired,
                                  IsVotingSub = row.IsVoting,

                                  AgendaFormatSub = row.AgendaFormat,
                                  ResolutionFormatHeadingSub = row.ResolutionFormatHeading,
                                  ResolutionFormatSub = row.ResolutionFormat,
                                  MinutesFormatHeadingSub = row.MinutesFormatHeading,
                                  MinutesFormatSub = row.MinutesFormat,
                                  MinutesDisApproveFormatSub = row.MinutesDisApproveFormat,
                                  MinutesDifferFormatSub = row.MinutesDifferFormat,
                                  IsSubAgenda = row.IsSubAgenda,
                                  SubAgendaInfomative = row.InfoText,
                                  SubHasInfo = row.HasInfo
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }
        #endregion

        #region Agenda Informative Text
        public AgendaInfoVM GetInfomativeText(long agendaId)
        {
            var result = new AgendaInfoVM() { AgendaInfoId = agendaId };
            try
            {
                var obj = (from row in entities.BM_AgendaMaster
                           where row.BM_AgendaMasterId == agendaId && row.IsDeleted == false
                           select new
                           {
                               AgendaInfo = row.InfoText
                           }).FirstOrDefault();
                if (obj != null)
                {
                    result.Success = true;
                    result.AgendaInfo = obj.AgendaInfo;
                }
                else
                {
                    result.Error = true;
                    result.Message = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AgendaInfoVM SaveInfomativeText(AgendaInfoVM obj, int userId)
        {
            try
            {
                var _obj = (from row in entities.BM_AgendaMaster
                            where row.BM_AgendaMasterId == obj.AgendaInfoId && row.IsDeleted == false
                            select row).FirstOrDefault();
                if (_obj != null)
                {
                    if (!string.IsNullOrEmpty(obj.AgendaInfo))
                    {
                        _obj.HasInfo = true;
                    }
                    else
                    {
                        _obj.HasInfo = false;
                    }
                    _obj.InfoText = obj.AgendaInfo;
                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = "Updated Successfully.";
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Sub Agenda New (Mapping)
        public List<SubAgendaMasterVM> GetAllSubAgendaNew(int MeetingTypeId, long Parent_AgendaId)
        {
            try
            {
                var entityTypeOfParentAgenda = (from row in entities.BM_AgendaMaster
                                                where row.BM_AgendaMasterId == Parent_AgendaId && row.IsDeleted == false
                                                select row.EntityType
                                               ).FirstOrDefault();

                var result = (from subAgenda in entities.BM_AgendaMaster
                              from meeting in entities.BM_CommitteeComp.Where(k => k.Id == subAgenda.MeetingTypeId).DefaultIfEmpty()
                              where subAgenda.MeetingTypeId == MeetingTypeId && subAgenda.EntityType == entityTypeOfParentAgenda && subAgenda.DefaultAgendaFor == null &&
                                    subAgenda.IsMultiStageAgenda == false && subAgenda.IsDeleted == false && meeting.IsDeleted == false
                              select new SubAgendaMasterVM
                              {
                                  BM_AgendaMasterId = subAgenda.BM_AgendaMasterId,
                                  SubAgenda = subAgenda.AgendaItemHeading,
                                  MeetingTypeNameSub = meeting.MeetingTypeName
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<AgendaMaster_SubAgendaMappging> GetAllSubAgendasNew(long ParentId)
        {
            try
            {
                var IsExists = (from row in entities.BM_AgendaMaster
                                where row.BM_AgendaMasterId == ParentId && row.IsMultiStageAgenda == true && row.Parent_Id == null && row.IsDeleted == false
                                select true).FirstOrDefault();

                if (IsExists == true)
                {
                    var result = (from subAgenda in entities.BM_AgendaMaster
                                  join mapping in entities.BM_AgendaMaster_SubAgendaMappging on subAgenda.BM_AgendaMasterId equals mapping.SubAgendaMasterId
                                  from meeting in entities.BM_CommitteeComp.Where(k => k.Id == subAgenda.MeetingTypeId).DefaultIfEmpty()
                                  where mapping.AgendaMasterId == ParentId && subAgenda.IsMultiStageAgenda == false && subAgenda.IsDeleted == false && mapping.IsDeleted == false && meeting.IsDeleted == false
                                  select new AgendaMaster_SubAgendaMappging
                                  {
                                      Agenda_SubAgendaMappgingID = mapping.Agenda_SubAgendaMappgingID,
                                      SubAgendaMasterId = mapping.SubAgendaMasterId,
                                      Parent_AgendaMasterId = mapping.AgendaMasterId,
                                      SubAgenda = subAgenda.AgendaItemHeading,
                                      Sequence = mapping.SequenceNo,
                                      StageId = mapping.StageId,
                                      MeetingTypeNameSub = meeting.MeetingTypeName
                                  }).ToList();

                    return result;
                }
                else
                {
                    return new List<AgendaMaster_SubAgendaMappging>();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public AgendaMaster_SubAgendaMappging Create(AgendaMaster_SubAgendaMappging obj)
        {
            try
            {
                obj.Message = new Response();
                var ISExists = entities.BM_AgendaMaster_SubAgendaMappging.Where(k => k.AgendaMasterId == obj.Parent_AgendaMasterId && k.SubAgendaMasterId == obj.SubAgendaMasterId && k.IsDeleted == false).Any();
                if (ISExists)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Sub Agenda allready exists.";
                    return obj;
                }

                BM_AgendaMaster_SubAgendaMappging subAgenda = new BM_AgendaMaster_SubAgendaMappging();
                subAgenda.Agenda_SubAgendaMappgingID = 0;
                subAgenda.AgendaMasterId = obj.Parent_AgendaMasterId;
                subAgenda.SubAgendaMasterId = obj.SubAgendaMasterId;

                subAgenda.SequenceNo = obj.Sequence;
                subAgenda.StageId = obj.StageId;

                subAgenda.IsDeleted = false;
                subAgenda.CreatedOn = DateTime.Now;
                subAgenda.CreatedBy = obj.UserId;
                entities.BM_AgendaMaster_SubAgendaMappging.Add(subAgenda);
                entities.SaveChanges();

                obj.Agenda_SubAgendaMappgingID = subAgenda.Agenda_SubAgendaMappgingID;
                obj.Message.Success = true;
                obj.Message.Message = "Saved Successfully.";
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public AgendaMaster_SubAgendaMappging Update(AgendaMaster_SubAgendaMappging obj)
        {
            try
            {
                obj.Message = new Response();
                var ISExists = entities.BM_AgendaMaster_SubAgendaMappging
                                .Where(k => k.AgendaMasterId == obj.Parent_AgendaMasterId
                                && k.SubAgendaMasterId == obj.SubAgendaMasterId
                                && k.Agenda_SubAgendaMappgingID != obj.Agenda_SubAgendaMappgingID
                                && k.IsDeleted == false
                            ).Any();
                if (ISExists)
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Sub Agenda allready exists.";
                    return obj;
                }

                BM_AgendaMaster_SubAgendaMappging _obj = entities.BM_AgendaMaster_SubAgendaMappging.Where(k => k.Agenda_SubAgendaMappgingID == obj.Agenda_SubAgendaMappgingID && k.IsDeleted == false).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.SubAgendaMasterId = obj.SubAgendaMasterId;
                    _obj.SequenceNo = obj.Sequence;
                    _obj.StageId = obj.StageId;

                    _obj.IsDeleted = false;
                    _obj.UpdatedOn = DateTime.Now;
                    _obj.UpdatedBy = obj.UserId;
                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    obj.Message.Success = true;
                    obj.Message.Message = "Updated Successfully.";
                }
                else
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public AgendaMaster_SubAgendaMappging GetSubAgendaNew(long Id)
        {
            try
            {
                var result = (from subAgenda in entities.BM_AgendaMaster
                              join mapping in entities.BM_AgendaMaster_SubAgendaMappging on subAgenda.BM_AgendaMasterId equals mapping.SubAgendaMasterId
                              from meeting in entities.BM_CommitteeComp.Where(k => k.Id == subAgenda.MeetingTypeId).DefaultIfEmpty()
                              where mapping.Agenda_SubAgendaMappgingID == Id && subAgenda.IsMultiStageAgenda == false && subAgenda.IsDeleted == false && mapping.IsDeleted == false && meeting.IsDeleted == false
                              select new AgendaMaster_SubAgendaMappging
                              {
                                  Agenda_SubAgendaMappgingID = mapping.Agenda_SubAgendaMappgingID,
                                  SubAgendaMasterId = mapping.SubAgendaMasterId,
                                  Parent_AgendaMasterId = mapping.AgendaMasterId,
                                  SubAgenda = subAgenda.AgendaItemHeading,
                                  Sequence = mapping.SequenceNo,
                                  StageId = mapping.StageId,
                                  MeetingTypeIDSub = subAgenda.MeetingTypeId,
                                  MeetingTypeNameSub = meeting.MeetingTypeName
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Agenda details for Meeting Module
        public IEnumerable<SubAgendaMasterVM> GetAgendaItems(int EntityId, int MeetingTypeId)
        {
            try
            {
                var EntityType = entities.BM_EntityMaster.Where(k => k.Id == EntityId && k.Is_Deleted == false).Select(k => k.Entity_Type).FirstOrDefault();

                return (from Agenda in entities.BM_AgendaMaster
                        where Agenda.MeetingTypeId == MeetingTypeId && Agenda.EntityType == EntityType && Agenda.Parent_Id == null && Agenda.IsDeleted == false
                        orderby Agenda.AgendaItemHeading
                        select new SubAgendaMasterVM
                        {
                            BM_AgendaMasterId = Agenda.BM_AgendaMasterId,
                            SubAgendaHeading = Agenda.AgendaItemHeading,
                            SubAgenda = Agenda.Agenda,
                        });
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<AgendaItemSelect> GetAgendaItemsSelectList(long MeetingId, string Part, int CustomerId)
        {
            try
            {
                var PartId = 2;
                var PartId_temp = 2;
                if (!String.IsNullOrEmpty(Part))
                {
                    if (Part == "A")
                    {
                        PartId = 1;
                        PartId_temp = 1;
                    }
                    else if (Part == "C")
                    {
                        PartId_temp = 3;
                    }
                }
                return (from Agenda in entities.BM_SP_GetMeetingAgendaItemList(MeetingId, CustomerId, PartId)
                        orderby Agenda.AgendaItemHeading
                        select new AgendaItemSelect
                        {
                            BM_AgendaMasterId = Agenda.AgendaID,
                            AgendaHeading = Agenda.AgendaItemHeading,
                            Agenda = Agenda.Agenda,
                            PartID = PartId_temp,
                            IsCheked = false,
                            Meeting_Id = MeetingId,
                            FrequencyID = Agenda.FrequencyId,
                            RefPendingMappingID = Agenda.MeetingAgendaMappingID,
                            RefMeeting = Agenda.Meeting,

                            StartMeetingId = Agenda.StartMeetingId,
                            StartAgendaId = Agenda.StartAgendaId,
                            StartMappingId = Agenda.StartMappingId,

                            HasCompliance = Agenda.HasCompliance,
                            HasInfo = Agenda.HasInfo
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<AgendaItemSelect> GetPendingAgendaItemsSelectList(long MeetingId, string Part, int CustomerId)
        {
            try
            {
                var PartId = 2;
                if (!String.IsNullOrEmpty(Part))
                {
                    if (Part == "A")
                    {
                        PartId = 1;
                    }
                }
                return (from Agenda in entities.BM_SP_GetMeetingPendingAgendaItemList(MeetingId, CustomerId, PartId)
                        orderby Agenda.AgendaItemHeading
                        select new AgendaItemSelect
                        {
                            BM_AgendaMasterId = Agenda.AgendaID,
                            AgendaHeading = Agenda.AgendaItemHeading,
                            Agenda = Agenda.Agenda,
                            //Commented on 28 Apr 2021
                            //PartID = PartId,
                            PartID = Agenda.PartId,
                            //End
                            IsCheked = false,
                            Meeting_Id = MeetingId,
                            FrequencyID = Agenda.FrequencyId,
                            RefPendingMappingID = Agenda.MeetingAgendaMappingID,
                            RefMeeting = Agenda.Meeting,
                            RefResult = Agenda.ResultRemark,

                            StartMeetingId = Agenda.StartMeetingId,
                            StartAgendaId = Agenda.StartAgendaId,
                            StartMappingId = Agenda.StartMappingId,

                            SequenceNo = Agenda.SequenceNo,
                            IsNewStage = Agenda.IsNewStage,

                            HasCompliance = Agenda.HasCompliance,
                            HasInfo = Agenda.HasInfo,
                            ScheduleOnID = Agenda.ScheduleOnID,

                            OpenAgendaId = Agenda.OpenAgendaId,
                            RefMasterName = Agenda.RefMasterName,
                            RefMasterID = Agenda.RefMasterID,
                            RefMasterIDOriginal = Agenda.RefMasterIDOriginal
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<AgendaItemSelect> GetMeetingMultiStageAgendaItemHistory(long StartMeetingId, long StartAgendaId, int EntityId, int CustomerId)
        {
            try
            {
                return (from row in entities.BM_SP_GetMeetingMultiStageAgendaItemHistory(StartMeetingId, StartAgendaId, EntityId, CustomerId)
                        select new AgendaItemSelect
                        {
                            BM_AgendaMasterId = row.AgendaID,
                            AgendaHeading = row.AgendaItemHeading,
                            Agenda = row.AgendaFormat,
                            IsCheked = false,
                            //FrequencyID = row.FrequencyId,
                            RefPendingMappingID = row.MeetingAgendaMappingID,
                            RefMeeting = row.Meeting,
                            RefResult = row.ResultRemark,

                            StartMeetingId = StartMeetingId,
                            StartAgendaId = StartAgendaId,
                            SequenceNo = row.SequenceNo,
                            //IsNewStage = row.IsNewStage
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<OpenAgendaDetailsVM> GetDetailsOfOpenAgendaItem(long MappingId)
        {
            try
            {
                return (from row in entities.BM_SP_MeetingOpenAgendaDetails(MappingId)
                        select new OpenAgendaDetailsVM
                        {
                            SrNo = row.SrNo,
                            MeetingId = row.MeetingId,
                            MeetingSrNo = row.MeetingSrNo,
                            FYText = row.FYText,
                            MeetingTypeName = row.MeetingTypeName,
                            MeetingCircular = row.MeetingCircular,
                            MeetingDate = row.MeetingDate,
                            AgendaID = row.AgendaID,
                            MeetingAgendaMappingID = row.MeetingAgendaMappingID,

                            ResultRemark = row.ResultRemark,
                            AgendaItemText = row.AgendaItemText,
                            AgendaFormat = row.AgendaFormat
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region UI Form
        public IEnumerable<UIFormsVM> GetUIForms()
        {
            try
            {
                return (from form in entities.BM_UIForm
                        where form.IsActive == true && form.IsDeleted == false
                        orderby form.UIFormName
                        select new UIFormsVM
                        {
                            UIFormID = form.UIFormID,
                            UIFormName = form.UIFormName
                        });
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion
        public AgendaMasterVM GetAgendaTemplate(long Id)
        {
            try
            {
                var result = (from row in entities.BM_AgendaMaster
                              where row.BM_AgendaMasterId == Id && row.IsDeleted == false
                              select new AgendaMasterVM
                              {
                                  BM_AgendaMasterId = row.BM_AgendaMasterId,
                                  Agenda = row.Agenda,
                                  EntityType = row.EntityType,
                                  MeetingTypeId = row.MeetingTypeId,
                                  IsForAllEntityType = row.IsForAllEntityType,
                                  FrequencyId = row.FrequencyId,
                                  PreCommitteeMeetingTypeId = row.PreCommitteeMeetingTypeId,
                                  IsForAllMeetingType = row.IsForAllMeetingType,
                                  IsVoting = row.IsVoting,
                                  IsNoting = row.IsNoting,
                                  AgendaFormat = row.AgendaFormat,
                                  ResolutionFormat = row.ResolutionFormat,
                                  MinutesFormat = row.MinutesFormat
                              }).FirstOrDefault();

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }

        public string Delete(long id, int userId)
        {
            var result = "";
            try
            {
                BM_AgendaMaster _obj = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == id && k.IsDeleted == false).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.IsDeleted = true;
                    _obj.UpdatedBy = userId;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();

                    result = "Delete successfully";
                }
                else
                {
                    result = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                result = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AgendaMasterVM UpdateTemplate(AgendaMasterVM obj)
        {
            try
            {
                obj.Message = new Response();
                BM_AgendaMaster _obj = entities.BM_AgendaMaster.Where(k => k.BM_AgendaMasterId == obj.BM_AgendaMasterId && k.IsDeleted == false).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.AgendaFormat = obj.AgendaFormat;
                    _obj.ResolutionFormat = obj.ResolutionFormat;
                    _obj.MinutesFormat = obj.MinutesFormat;

                    entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                    obj.Message.Success = true;
                    obj.Message.Message = "Saved Successfully.";
                }
                else
                {
                    obj.Message.Error = true;
                    obj.Message.Message = "Something wents wrong";
                }
            }
            catch (Exception ex)
            {
                obj.Message.Error = true;
                obj.Message.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public IEnumerable<AgendaFrequencyVM> GetAgendaFrequency()
        {
            var result = from row in entities.BM_AgendaFrequency
                         select new AgendaFrequencyVM
                         {
                             FrequencyId = row.FrequencyId,
                             FrequencyName = row.FrequencyName
                         };

            return result;
        }

        public bool DeleteAgendaComplianceMapping(int Id)
        {

            try
            {
                if (Id > 0)
                {
                    var _objmappingdetails = (from row in entities.BM_ComplianceMapping
                                              where row.MCM_ID == Id && row.IsDeleted == false
                                              //&& row.CustomerID == customerId
                                              select row).FirstOrDefault();
                    if (_objmappingdetails != null)
                    {
                        _objmappingdetails.IsDeleted = true;
                        _objmappingdetails.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        //obj.Message = true;
                        //obj.successErrorMessage = "Deleted Successfully";
                        return true;
                    }
                    else
                    {
                        //obj.errorMessage = true;
                        //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                        return false;

                    }
                }
                else
                {
                    //obj.errorMessage = true;
                    //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                    return false;
                }
            }
            catch (Exception ex)
            {
                //obj.successErrorMessage = "Some error Occure in Deleting Fields";
                //obj.errorMessage = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }

        }

        public bool UpdateAgendaDetails(AgendaMasterVM agendaItem, int userID, int? customerID)
        {
            bool result = false;
            try
            {
                var prevAgendaItem = (from x in entities.BM_AgendaMaster
                                      where x.BM_AgendaMasterId == agendaItem.BM_AgendaMasterId
                                      //&& x.Customer_Id == null
                                      //&& x.IsDeleted == false
                                      select x).FirstOrDefault();
                if (prevAgendaItem != null)
                {
                    prevAgendaItem.IsLive = agendaItem.IsLive;
                    prevAgendaItem.DocumentStatus = agendaItem.DocumentType.Type;
                    prevAgendaItem.UpdatedBy = userID;
                    prevAgendaItem.UpdatedOn = DateTime.Now;

                    entities.SaveChanges();

                    //Added 08 Dec 2021
                    if(prevAgendaItem.Parent_Id == null)
                    {
                        entities.BM_AgendaMaster.Where(k => k.Parent_Id == agendaItem.BM_AgendaMasterId && k.IsDeleted == false).ToList().ForEach(k => k.IsLive = agendaItem.IsLive);
                        entities.SaveChanges();
                    }
                    //End

                    result = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                result = false;
            }
            return result;
        }
    }
}