﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public interface ICircularResolution
    {
        List<VM_CircularResolution> GetAll_CircularResolution(int customerID);

        VM_CircularResolution GetCircularDetailByID(int ID);
    }
}
