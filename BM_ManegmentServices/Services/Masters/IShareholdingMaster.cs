﻿using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IShareholdingMaster
    {
        EntityMasterVM getEntitIdbyId(int entity_Id, int customerId);
        List<VMShareholding> GetShareholdingdata(int EntityId, int CustomerId);
        ShareHoldings AddShareholdingdata(ShareHoldings _objshareholder, int customerId,int UserId);
        string UpdateShareholdingdtls(VMShareholding _objshareholding, int customerId);
    
        ShareHoldings getShareHoldingData(long shareholdingId, int customerId);
     

        bool CreatePublicPrivateListedData(BM_ShareholderListedEntityData _ObjshareholderListedEntityData);

        bool CreateDmatData(BM_DematForm obj_BM_DematForm);

        List<BM_ShareholderListedEntityData> GetShareDetailsForListedCompay(int entityId, int customerId);
        List<BM_DematForm> GetShareDetailsForDemateForm(int entityId, int customerId);
        ShareHoldings UpdateShareholdingdata(ShareHoldings _objdtls, int customerId,int userId);
        string getEntityName(int entity_Id);
        List<ClassesOfShare> GetDropDownClassesOfShares(int entityId);
        decimal gettotalprefrenceCapital(int entityID, int customerId);
        List<VMShareholding> GetShareholdingMember(int entityId, int customerId);
        bool DeleteShareHolding(int id, int customerId, int UserId);
      
        ShareHoldingDetails AddShareholderDetails(ShareHoldingDetails _objDetails);
        ShareHoldingDetails UpdateShareholderDetails(ShareHoldingDetails _objDetails);
        List<ShareHoldingDetails> GetShareholdingDetails(int id,int CustomerId);
        ShareHoldingDetails EditSHDetails(int id, int sharesHoldingId);
        string DeleteShareHoldingDetails(int id);
        decimal gettotalpaidupcapitalinamCapital(int entityId, int customerId);
        List<VMShareholding> GetPartnerDetails(int customerId, int EntityID);
    }
}
