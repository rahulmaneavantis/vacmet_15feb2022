﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.Compliance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IChargeDetailsMaster
    {       
        List<ChargeDetails_VM> GetCharge(long chargeHeaderId);
        ChargeDetails_VM AddChargeDetails(ChargeDetails_VM _objadditionalcomp, int userId);
        ChargeDetails_VM EditChargeDetails(int id, int entityId);
        ChargeDetails_VM UpdateChargeDetails(ChargeDetails_VM _objadditionalcomp, int userId);
        IEnumerable<ChargeTypeList_VM> GetAllChargeDetails();
        Message ChecksaveUpdateForChargeMaster(string chargeId, string Charge_TypeDesc, int entityId, BM_chargedetails objauditor);
        bool DeleteChargeDetails(int id, int UserId);

        #region Upload Eform
        ChargeUploadEFormVM UploadChargeEform(ChargeUploadEFormVM obj, int userId);
        #endregion

        List<ChargeHeaderVM> GetChargeIndex(int entityId);

        EntityDocumentVM Savechargedocument(EntityDocumentVM _objdoc, int userId, int customerId);
        List<BM_SP_GetEntityChargeDocuments_Result> GetChargeDocuments(long chargeDetailsID, int entityId);

        IEnumerable<EntityDocumentVM> GetParticularDocument();

    }
}
