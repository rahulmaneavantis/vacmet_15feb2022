﻿using BM_ManegmentServices.VM;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public interface ITemplateservice
    {
        Template_MasterVM Add(Template_MasterVM _objTemplate);
        Template_MasterVM GetTemplatemaster(Template_MasterVM _ObjTemplate);

        string JoinMeetingTemplate();

        string TestMailTemplate();
    }
}
