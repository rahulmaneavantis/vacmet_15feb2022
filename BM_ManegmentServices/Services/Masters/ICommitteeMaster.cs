﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface ICommitteeMaster
    {
        List<Composition> CheckIsValidCommittee(int CommitteeId, int EntityId, int CustomerId);

        List<string> getMandatoryCommittee(int EntityId);

        List<CommitteeMatrixVM> GetCommitteeMatrix(int entityId);
    }
}
