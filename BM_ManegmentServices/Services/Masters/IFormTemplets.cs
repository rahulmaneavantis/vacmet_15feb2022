﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IFormTemplets
    {
        List<VM_FormTemplet> GetFormTempletDetails(int customerId, string role);
        List<VM_FormTemplet> GetFormTempletDetails(long complianceId);
        IEnumerable<MasterDetails> GetMasterForm();
        IEnumerable<MasterFormates> GetTemplateFormate(long masterId);
        FormTemplateDetails AddFormTemplet(FormTemplateDetails objformtemplate);
        FormTemplateDetails UpdateFormTemplet(FormTemplateDetails objformtemplate);
        FormTemplateDetails getFormFormateById(long formId);
        List<VM_FormTemplet> GetMeetingFormTemplate(long meetingId);
        VM_FormTemplet getFileFormateforPreview(long formId);
    }
}
