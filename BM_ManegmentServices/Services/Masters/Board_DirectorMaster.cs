﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using static BM_ManegmentServices.VM.VMBoard_Director;

namespace BM_ManegmentServices.Services.Masters
{
    public class Board_DirectorMaster : IBoard_DirectorMaster
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        #region Create and Update Directors Rules
        public VMBoard_Director Create_DirectorsRules(VMBoard_Director _objBoardDirector, int CustomerId)
        {
            _objBoardDirector.Messages = new Response();
            try
            {
                
                var _objcheckDirectordtls = (from row in entities.BM_BoardDirector_Rule
                                             where row.Entity_Type == _objBoardDirector.EntityType
                                             && row.RuleType == "DD"
                                             && row.Gender == _objBoardDirector.Gender
                                             //&& row.DesignationId == _objBoardDirector.Designation_dirId
                                             select row).FirstOrDefault();
                if (_objcheckDirectordtls != null)
                {
                    _objBoardDirector.Messages.Error = true;
                   _objBoardDirector.Messages.Message = "Field already exist";
                    return _objBoardDirector;
                }
                else
                {
                    BM_BoardDirector_Rule obj = new BM_BoardDirector_Rule();
                    obj.Entity_Type = _objBoardDirector.EntityType;
                    obj.DesignationId = _objBoardDirector.Designation_dirId;
                    obj.Max_Director = _objBoardDirector.Max;
                    obj.Min_Director = _objBoardDirector.Min;
                    obj.RuleType = "DD";
                    obj.Gender = _objBoardDirector.Gender;
                    obj.IsDeleted = false;
                    obj.Customer_Id = CustomerId;
                    obj.CreatedOn = DateTime.Now;
                    obj.CreatedBy = CustomerId;
                    entities.BM_BoardDirector_Rule.Add(obj);
                    entities.SaveChanges();
                    _objBoardDirector.Messages.Success = true;
                    _objBoardDirector.Messages.Message = "Saved Successfully.";
                    return _objBoardDirector;
                }

            }
            catch (Exception ex)
            {
                _objBoardDirector.Messages.Error = true;
                _objBoardDirector.Messages.Message = "Server error occurred";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objBoardDirector;
            }
        }

        public VMBoard_Director Update_DirectorsRules(VMBoard_Director _objBoardDirector)
        {
            _objBoardDirector.Messages = new Response();
            try
            {
                var _objcheckDirectordtls = (from row in entities.BM_BoardDirector_Rule
                                             where row.Entity_Type == _objBoardDirector.EntityType
                                             && row.Id == _objBoardDirector.BDId
                                             && row.RuleType == "DD"
                                             //&& row.Gender == _objBoardDirector.Gender
                                             //&& row.DesignationId == _objBoardDirector.Designation_dirId
                                             select row).FirstOrDefault();
                if (_objcheckDirectordtls != null)
                {
                    _objcheckDirectordtls.Entity_Type = _objBoardDirector.EntityType;
                    _objcheckDirectordtls.DesignationId = _objBoardDirector.Designation_dirId;
                    _objcheckDirectordtls.Max_Director = _objBoardDirector.Max;
                    _objcheckDirectordtls.Min_Director = _objBoardDirector.Min;
                    _objcheckDirectordtls.RuleType = "DD";
                    _objcheckDirectordtls.Gender = _objBoardDirector.Gender;
                    _objcheckDirectordtls.IsDeleted = false;

                    _objcheckDirectordtls.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                    _objBoardDirector.Messages.Success = true;
                    _objBoardDirector.Messages.Message = "Updated Successfully.";
                    return _objBoardDirector;

                }
                else
                {
                    _objBoardDirector.Messages.Error = true;
                    _objBoardDirector.Messages.Message = "Something went wrong";
                    return _objBoardDirector;

                }

            }
            catch (Exception ex)
            {
                _objBoardDirector.Messages.Error = true;
                _objBoardDirector.Messages.Message = "Server error occurred";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objBoardDirector;
            }
        }
        #endregion

        #region List of DirectorAge
        public List<Director_Age> GetAllDirectorAge(int customerId, int entityType)
        {
            try
            {
                var getagedata = (from row in entities.BM_BoardDirector_Rule
                                  from rows in entities.BM_Directors_Designation.Where(x => x.Id == row.DesignationId).DefaultIfEmpty()
                                  where row.RuleType == "A"
                                 // && row.Customer_Id == customerId
                                  && row.Entity_Type == entityType
                                  && row.IsDeleted == false
                                  //&& row.DesignationId == 
                                  select new Director_Age
                                  {
                                      Min_age = row.Min_Director,
                                      Max_age = row.Max_Director,
                                      Id = row.Id,
                                      DesignationName = rows.Name,
                                      Entity_type = (int)row.Entity_Type,
                                      DegignationId = row.DesignationId,

                                  }).ToList();
                #region Generate Rules

                foreach (var item in getagedata)
                {
                    string rule1, rule2;
                    if ((item.Min_age != null || item.Max_age != null) && (item.DesignationName == null))
                    {
                        if (item.Min_age != null)
                        {
                            rule1 = "Minimum" + "<b>" + item.Min_age + "</b>" + "years for all Directors";
                        }
                        else
                        {
                            rule1 = "";
                        }
                        if (item.Max_age != null)
                        {
                            rule2 = "Maximum" + "<b>" + item.Max_age + "</b>" + "years for all Directors";
                        }
                        else
                        {
                            rule2 = "";
                        }
                        item.Rules = rule1 + "   " + rule2;
                    }
                    else if (item.Max_age != null && item.DesignationName != null)
                    {

                        if (item.Min_age != null)
                        {
                            rule1 = "Minimum" + "<b>" + item.Min_age + "</b>" + "years for" + item.DesignationName;
                        }
                        else
                        {
                            rule1 = "";
                        }
                        if (item.Max_age != null)
                        {
                            rule2 = "Maximum" + "<b>" + item.Max_age + "</b>" + "years for" + item.DesignationName;
                        }
                        else
                        {
                            rule2 = "";
                        }

                        item.Rules = rule1 + "   " + rule2;
                    }
                    else
                    {
                        item.Rules = "";
                    }
                }
                #endregion

                return getagedata;
            }
            catch (Exception ex)
            {
                List<Director_Age> obj = new List<Director_Age>();

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }
        #endregion

        #region List of Directors in Company
        public List<Director_Rule> GetAllDirectorDiscription(int customerId, int entityType)
        {
            try
            {
                var getagedata = (from row in entities.BM_BoardDirector_Rule
                                  from rows in entities.BM_Directors_Designation.Where(x => x.Id == row.DesignationId).DefaultIfEmpty()
                                  where row.RuleType == "DD"
                                 // && row.Customer_Id == customerId
                                  && row.Entity_Type == entityType
                                  select new Director_Rule
                                  {
                                      gender = row.Gender,
                                      Min = row.Min_Director,
                                      Max = row.Max_Director,
                                      Designation = row.DesignationId,
                                      DesignatonName = rows.Name,
                                      Entity_type = (int)row.Entity_Type,
                                      
                                      Id = row.Id,
                                  }).ToList();
                #region Generate Rules
                foreach (var item in getagedata)
                {
                    string rules1, rules2;
                    if (item.Max > 0 || item.Min > 0 && item.DesignatonName == null && (item.gender == 2 || item.gender == null))
                    {
                        item.Rules = "Minimum" + "<b> " + item.Min + " </b> " + " Directors" + "   \n\n " + "Maximum" + "<b>" + item.Max + "</b>" + " Directors";
                    }
                    else if (item.Max > 0 || item.Min > 0 && item.DesignatonName != null && (item.gender == 2 || item.gender == null))
                    {
                        item.Rules = "Minimum" + "<b> " + item.Min + " </b> " + item.DesignatonName + "   \n\n " + "Maximum" + "<b>" + item.Max + "</b>" + item.DesignatonName;
                    }
                    else if ((item.Max > 0 || item.Min > 0) && item.DesignatonName == null && item.gender == 1)
                    {
                        if (item.Min != null)
                        {
                            rules1 = "Minimum" + "<b> " + item.Min + " </b> " + "Women Director";
                        }
                        else
                        {
                            rules1 = "";
                        }
                        if (item.Max != null)
                        {
                            rules2 = "Maximum" + "<b> " + item.Max + " </b> " + "Women Director";
                        }
                        else
                        {
                            rules2 = "";
                        }
                        item.Rules = rules1 + "<br/>" + "" + rules2;
                    }
                    else
                    {
                        item.Rules = "";
                    }
                }
                #endregion

                return getagedata;
            }
            catch (Exception ex)
            {
                List<Director_Rule> obj = new List<Director_Rule>();

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }
        #endregion

        #region Edit Directors
        public VMBoard_Director GetDirector(int id, int entity_Type, int designationId)
        {
            try
            {
                var objgetDirdtlsbyId = (from row in entities.BM_BoardDirector_Rule
                                         where row.Id == id && row.Entity_Type == entity_Type
                                         select new VMBoard_Director
                                         {
                                             Max = row.Max_Director,
                                             Min = row.Min_Director,
                                             BDId = row.Id,
                                             EntityType = (int)row.Entity_Type,
                                             Designation_dirId = row.DesignationId,
                                             Gender = row.Gender,
                                         }).FirstOrDefault();
                return objgetDirdtlsbyId;
            }
            catch (Exception ex)
            {
                VMBoard_Director obj = new VMBoard_Director();
                obj.Messages = new Response();
                obj.Messages.Error = true;
                obj.Messages.Message = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;
            }
        }
        #endregion

        #region Update_Create Director_ruleAge
        public VMBoard_Director Update_DirectorAge(VMBoard_Director _objBoardDirector, int customerId)
        {
           

            try
            {
                _objBoardDirector.Messages = new Response();
                var _objcheckDirectordtls = (from row in entities.BM_BoardDirector_Rule

                                             where row.Entity_Type == _objBoardDirector.EntityType
                                             && row.Id == _objBoardDirector.BDId
                                             //&& row.DesignationId == _objBoardDirector.Designation_dirId
                                             select row).FirstOrDefault();
                if (_objcheckDirectordtls != null)
                {
                    _objcheckDirectordtls.Entity_Type = _objBoardDirector.EntityType;
                    _objcheckDirectordtls.DesignationId = _objBoardDirector.Designation_dirId;
                    _objcheckDirectordtls.Max_Director = _objBoardDirector.Max;
                    _objcheckDirectordtls.Min_Director = _objBoardDirector.Min;
                    _objcheckDirectordtls.RuleType = "A";
                    _objcheckDirectordtls.IsDeleted = false;
                    _objcheckDirectordtls.Customer_Id = customerId;
                    _objcheckDirectordtls.CreatedOn = DateTime.Now;
                    _objcheckDirectordtls.CreatedBy = customerId;
                    entities.SaveChanges();
                    _objBoardDirector.Messages.Success = true;
                    _objBoardDirector.Messages.Message = "Updated Successfully.";
                }
                else
                {
                    _objBoardDirector.Messages.Error = true;
                    _objBoardDirector.Messages.Message = "Something went wrong";
                }
                return _objBoardDirector;
            }
            catch (Exception ex)
            {
                _objBoardDirector.Messages.Error = true;
                _objBoardDirector.Messages.Message = "Server error occurred";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objBoardDirector;
            }
        }

        public VMBoard_Director Create_DirectorAge(VMBoard_Director _objBoardDirector, int customerId)
        {
            try
            {
                _objBoardDirector.Messages = new Response();
                var _objcheckDirectordtls = (from row in entities.BM_BoardDirector_Rule
                                             where row.Entity_Type == _objBoardDirector.EntityType
                                             && row.RuleType == "A"
                                             && row.DesignationId == _objBoardDirector.Designation_ageId
                                             select row).FirstOrDefault();
                if (_objcheckDirectordtls != null)
                {
                    _objBoardDirector.Messages.Error = true;
                    _objBoardDirector.Messages.Message = "Field already exist";
                    return _objBoardDirector;
                }
                else
                {
                    BM_BoardDirector_Rule obj = new BM_BoardDirector_Rule();
                    obj.Entity_Type = _objBoardDirector.EntityType;
                    obj.DesignationId = _objBoardDirector.Designation_ageId;
                    obj.Max_Director = _objBoardDirector.Max;
                    obj.Min_Director = _objBoardDirector.Min;
                    obj.RuleType = "A";
                    obj.IsDeleted = false;
                    obj.Customer_Id = customerId;
                    obj.CreatedOn = DateTime.Now;
                    obj.CreatedBy = customerId;
                    entities.BM_BoardDirector_Rule.Add(obj);
                    entities.SaveChanges();
                    _objBoardDirector.Messages.Success = true;
                    _objBoardDirector.Messages.Message = "Saved Successfully";
                    return _objBoardDirector;
                }
                
            }
            catch (Exception ex)
            {
                _objBoardDirector.Messages.Error = true;
                _objBoardDirector.Messages.Message = "Server error occurred";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objBoardDirector;
            }
        }

        public VMBoard_Director GetDirectorAge(int id, int entity_Type)
        {
            try
            {
                
                
                var objgetDirdtlsbyId = (from row in entities.BM_BoardDirector_Rule
                                         where row.Id == id && row.Entity_Type == entity_Type
                                         select new VMBoard_Director
                                         {
                                             Max = row.Max_Director,
                                             Min = row.Min_Director,
                                             BDId = row.Id,
                                             EntityType = (int)row.Entity_Type,
                                             Designation_ageId= row.DesignationId,
                                             Gender = row.Gender,
                                         }).FirstOrDefault();
                return objgetDirdtlsbyId;
            }
            catch (Exception ex)
            {
                VMBoard_Director obj = new VMBoard_Director();
                obj.Messages = new Response();
                obj.Messages.Error = true;
                obj.Messages.Message = "Server error occurred";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return obj;

            }
        }

        public int GetEntityType(int EntityId,int CustomerId)
        {
            int EntityTypeId = 0;
            try
            {
                EntityTypeId = (from x in entities.BM_EntityMaster where x.Id == EntityId && x.Customer_Id == CustomerId && x.Is_Deleted==false  select x.Entity_Type).FirstOrDefault();
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return EntityTypeId;
        }
        #endregion
    }
}