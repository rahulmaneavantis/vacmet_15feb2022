﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{

    public class FormTemplet : IFormTemplets
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public List<VM_FormTemplet> GetFormTempletDetails(int customerId, string role)
        {
            var result = new List<VM_FormTemplet>();
            try
            {
                //if(role == SecretarialConst.Roles.CSIMP)
                //{

                //}
                result = (from row in entities.BM_FormMaster
                          where row.IsActive == true
                          orderby row.FormName
                          select new VM_FormTemplet
                          {
                              FormId = row.FormID,
                              FormName = row.FormName,
                              Details = row.Details,
                              StartDate = row.startDate,
                              IsEForm = row.IsEForm,
                              IsReadonly = row.IsReadonly
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<VM_FormTemplet> GetFormTempletDetails(long complianceId)
        {
            try
            {
                var getformTemple = (from row in entities.BM_FormMaster
                                     where row.IsActive == true
                                     select new VM_FormTemplet
                                     {
                                         FormId = row.FormID,
                                         FormName = row.FormName,
                                         Details = row.Details,
                                         StartDate = row.startDate,
                                         IsEForm = row.IsEForm,
                                         ComplianceId = complianceId
                                     }).ToList();
                return getformTemple;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<MasterDetails> GetMasterForm()
        {
            try
            {
                var getMasterdata = (from row in entities.BM_FormTemplateFieldMaster
                                     where row.IsActive == true
                                     select new MasterDetails
                                     {
                                         ID = row.MasterID,
                                         MasterName = row.MasterName
                                     }
                                     ).ToList();
                return getMasterdata;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<MasterFormates> GetTemplateFormate(long masterId)
        {
            try
            {
                var getFormTempletFieldData = (from row in entities.BM_FormTemplateFieldDetails
                                               where row.MasterID == masterId && row.IsActive == true
                                               select new MasterFormates
                                               {
                                                   ID = row.ID,
                                                   FieldName = row.FieldName,
                                                   FieldValue = row.FieldValue,
                                               }
              ).ToList();
                return getFormTempletFieldData;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public FormTemplateDetails AddFormTemplet(FormTemplateDetails objformtemplate)
        {
            try
            {
                //var IsExist = (from row in entities.BM_FormMaster where row.FormName == objformtemplate.FormName && row.FormFormat == objformtemplate.FormDetails select row).Any();
                //var IsExist = (from row in entities.BM_FormMaster where row.FormName == objformtemplate.FormName && row.IsEForm == false select row).Any();
                var IsExist = (from row in entities.BM_FormMaster where row.FormName == objformtemplate.FormName && row.IsActive == true select row).Any();
                if (IsExist == false)
                {
                    var objformMaster = new BM_FormMaster();

                    objformMaster.IsEForm = false;
                    objformMaster.EForm = "";
                    objformMaster.FormName = objformtemplate.FormName;
                    objformMaster.Details = objformtemplate.Details;
                    objformMaster.FormFormat = objformtemplate.FormFormat;
                    objformMaster.startDate = objformtemplate.StartDate;
                    objformMaster.EndDate = objformtemplate.EndDate;
                    objformMaster.IsActive = true;
                    objformMaster.ForMultipleEvents = false;
                    objformMaster.IsDeleted = false;
                    objformMaster.CreatedOn = DateTime.Now;
                    objformMaster.CreatedBy = objformtemplate.UserId;
                    entities.BM_FormMaster.Add(objformMaster);
                    entities.SaveChanges();
                    objformtemplate.FormId = objformMaster.FormID;
                    objformtemplate.Success = true;
                    objformtemplate.Message = "Saved Successfully.";

                }
                else
                {
                    objformtemplate.Error = true;
                    objformtemplate.Message = "Form name allready exists.";
                }
            }
            catch (Exception ex)
            {
                objformtemplate.Error = true;
                objformtemplate.Message = "Server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objformtemplate;
        }

        public FormTemplateDetails UpdateFormTemplet(FormTemplateDetails objformtemplate)
        {
            try
            {
                var IsFormNameExist = (from row in entities.BM_FormMaster where row.FormID != objformtemplate.FormId && row.FormName == objformtemplate.FormName && row.IsActive == true select row).Any();
                if(IsFormNameExist == true)
                {
                    objformtemplate.Error = true;
                    objformtemplate.Message = "Form name allready exists.";
                    return objformtemplate;
                }

                var IsExist = (from row in entities.BM_FormMaster where row.FormID == objformtemplate.FormId select row).FirstOrDefault();
                if (IsExist != null)
                {
                    IsExist.FormName = objformtemplate.FormName;
                    IsExist.Details = objformtemplate.Details;
                    IsExist.FormFormat = objformtemplate.FormFormat;
                    IsExist.startDate = objformtemplate.StartDate;
                    IsExist.EndDate = objformtemplate.EndDate;
                    IsExist.IsActive = true;
                    IsExist.IsDeleted = false;
                    IsExist.UpdatedOn = DateTime.Now;
                    IsExist.UpdatedBy = objformtemplate.UserId;
                    entities.SaveChanges();
                    objformtemplate.Success = true;
                    objformtemplate.Message = "Updated Successfully.";
                }
                else
                {
                    objformtemplate.Error = true;
                    objformtemplate.Message = "Somthing wents wrong";
                }
            }
            catch (Exception ex)
            {
                objformtemplate.Error = true;
                objformtemplate.Message = "Server error occuerd";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objformtemplate;
        }

        public FormTemplateDetails getFormFormateById(long formId)
        {
            try
            {
                var getFormFormate = (from row in entities.BM_FormMaster
                                      where row.FormID == formId
                                      select new FormTemplateDetails
                                      {
                                          FormId = row.FormID,
                                          FormName = row.FormName,
                                          Details = row.Details,
                                          FormFormat = row.FormFormat,
                                          StartDate = row.startDate,
                                      }).FirstOrDefault();
                return getFormFormate;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VM_FormTemplet> GetMeetingFormTemplate(long meetingId)
        {
            try
            {

                var getMeetingForm = (from row in entities.BM_MeetingFormMapping
                                      where row.MeetingId == meetingId
                                      select new VM_FormTemplet
                                      {
                                          FormId = row.MeetingFormMappingID,
                                          FormName = row.FormName,
                                          MeetingId = meetingId,
                                          MappingAgendaId = row.MeetingAgendaMappingId

                                          // FormFormate=(from x in entities.BM_SP_MeetingFormGenerate(row.MeetingAgendaMappingId,row.MeetingFormMappingID) select x).FirstOrDefault(),
                                      }
                                    ).ToList();

                return getMeetingForm;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<VM_FormTemplet> obj = new List<VM_FormTemplet>();
                return obj;
            }
        }

        public VM_FormTemplet getFileFormateforPreview(long formId)
        {
            try
            {
                var getFormPreview = (from row in entities.BM_FormMaster
                                      where row.FormID == formId
                                      select new VM_FormTemplet
                                      {
                                          FormFormate = row.FormFormat,
                                          FormId = row.FormID
                                      }
                                      ).FirstOrDefault();
                return getFormPreview;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

        }
    }
}