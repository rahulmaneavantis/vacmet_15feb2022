﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IAdditionalComplianceMaster
    {
        AdditionalCompliances_VM AddAdditionalCompliances(AdditionalCompliances_VM _objadditionalcomp, int userId);
        List<ComplianceList_VM> GetAdditionalCompliances();
        AdditionalCompliances_VM EditAdditionalCompliances(long id);
        AdditionalCompliances_VM UpdateAdditionalCompliances(AdditionalCompliances_VM _objadditionalcomp, int userId);
        bool DeleteAdditionalComp(long id,int UserId);
    }
}
