﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IMBP2TransactionService
    {
        List<MBP2TransactionNatureVM> GetNatureOfTransactions();
        List<MBP2TransactionVM> GetTransactions(int entityId, string view);
        MBP2TransactionVM GetTransaction(long id, int entityId);
        MBP2TransactionVM CreateUpdateTransaction(MBP2TransactionVM obj, int createdBy);
        Message DeleteTransaction(int id, int entityId, int deletedBy);
        MBP2TransactionUploadVM UploadExcel(MBP2TransactionUploadVM objfileupload, int customerId, int userId);
        EntityMasterVM GetCompanyName(int EntityId);
        MBP2TransactionLimitVM GetLimit(int entityId);
        MBP2TransactionLimitVM SetLimit(MBP2TransactionLimitVM obj, int userId);
        EntityDocumentVM Savechargedocument(EntityDocumentVM _objdoc, int userId, int customerId);
        List<BM_SP_GetMBP2Documents_Result> GetMBP2Documents(int entityId, int NatureOfTransactionId);
        List<MBP2TransactionNatureVM> GetNatureType();
        List<MBP2TransactionVM> GetTransactionByNature(int entityId, string view, int NatureOfTransactionId);


        
    }
}
