﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using System.Reflection;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Meetings;

namespace BM_ManegmentServices.Services.Masters
{

    public class Template_Services : ITemplateservice
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public Template_MasterVM Add(Template_MasterVM _objTemplate)
        {
            try
            {
                bool? IsQuarter = false;

                if (_objTemplate.MeetingTypeId == 10 && (_objTemplate.TemplateType == "A" || _objTemplate.TemplateType == "AN" || _objTemplate.TemplateType == "N"))
                {
                    if (!string.IsNullOrEmpty(_objTemplate.Quarter_))
                    {
                        IsQuarter = true;
                    }
                    else
                    {
                        IsQuarter = false;
                    }
                }

                var CheckforTemplate = (from row in entities.BM_TemplateMaster
                                        where row.MeetingTypeId == _objTemplate.MeetingTypeId
                                        && row.TemplateType == _objTemplate.TemplateType
                                        && row.IsQuarter == IsQuarter
                                        && row.IsShorter == _objTemplate.IsShorter 
                                        && row.IsDeleted == false 
                                        select row).FirstOrDefault();

                if (CheckforTemplate == null)
                {
                    BM_TemplateMaster obj = new BM_TemplateMaster();
                    obj.MeetingTypeId = _objTemplate.MeetingTypeId;
                    obj.TemplateType = _objTemplate.TemplateType;
                    obj.IsQuarter = _objTemplate.IsQuarter;
                    obj.MailTemplate = _objTemplate.MailTemplate;
                    obj.SMSTemplate = _objTemplate.SMSTemplate;
                    obj.IsDeleted = _objTemplate.IsDeleted;
                    obj.CreatedOn = DateTime.Now;
                    obj.CreatedBy = _objTemplate.userId;

                    obj.IsDeleted = false;
                    obj.IsQuarter = IsQuarter;
                    obj.IsShorter = _objTemplate.IsShorter;
                    entities.BM_TemplateMaster.Add(obj);
                    entities.SaveChanges();
                    _objTemplate.Success = true;
                    _objTemplate.Message = "Saved Successfully.";
                }
                else
                {
                    CheckforTemplate.MailTemplate = _objTemplate.MailTemplate;

                    _objTemplate.UpdatedOn = DateTime.Now;
                    _objTemplate.UpdatedBy = _objTemplate.userId;

                    entities.SaveChanges();
                    _objTemplate.Success = true;
                    _objTemplate.Message = "Updated Successfully.";
                }

            }
            catch (Exception ex)
            {
                _objTemplate.Error = true;
                _objTemplate.Message = "Server error occured";

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return _objTemplate;
        }
        public Template_MasterVM GetTemplatemaster(Template_MasterVM _ObjTemplate)
        {
            try
            {
                bool? IsQuarter = false;

                if (_ObjTemplate.MeetingTypeId == 10 && (_ObjTemplate.TemplateType == "A" || _ObjTemplate.TemplateType == "AN" || _ObjTemplate.TemplateType == "N"))
                {
                    if(!string.IsNullOrEmpty(_ObjTemplate.Quarter_))
                    {
                        IsQuarter = true;
                    }
                    else
                    {
                        IsQuarter = false;
                    }
                }

                var NoticeFormat = (from row in entities.BM_TemplateMaster
                                    where row.MeetingTypeId == _ObjTemplate.MeetingTypeId && 
                                            row.TemplateType == _ObjTemplate.TemplateType &&  
                                            row.IsQuarter == IsQuarter && 
                                            row.IsShorter == _ObjTemplate.IsShorter &&
                                            row.IsDeleted == false
                                    select row.MailTemplate).FirstOrDefault();

                if (NoticeFormat != null)
                {
                    _ObjTemplate.MailTemplate = NoticeFormat;
                    _ObjTemplate.Success = true;
                    _ObjTemplate.Message = "success";
                }
                else
                {
                    _ObjTemplate.Error = true;
                    _ObjTemplate.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                _ObjTemplate.Error = true;
                _ObjTemplate.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _ObjTemplate;
        }

        public string JoinMeetingTemplate()
        {
            try
            {
                return entities.BM_TemplateMaster.Where(k => k.MeetingTypeId == -1 && k.TemplateType == SecretarialConst.MeetingTemplatType.JOINMEETING).Select(k => k.MailTemplate).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public string TestMailTemplate()
        {
            try
            {
                return entities.BM_TemplateMaster.Where(k => k.MeetingTypeId == -1 && k.TemplateType == SecretarialConst.MeetingTemplatType.TESTMAIL).Select(k => k.MailTemplate).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
    }
}
