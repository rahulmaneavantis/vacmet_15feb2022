﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.VM;
using System.Data.Entity.SqlServer;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.IO;
using System.Web.Hosting;

using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using BM_ManegmentServices.Services.Setting;
using System.Globalization;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace BM_ManegmentServices.Services.Masters
{
    public class VideoMeeting : IVideoMeeting
    {
        private Compliance_SecretarialEntities entity = new Compliance_SecretarialEntities();

        public VMeetingVM GetMeetingDetail(int CustomerId, long MeetingId)
        {
            VMeetingVM _objvmeeting = new VMeetingVM();
            try
            {
                string VmeetingId = (from x in entity.BM_VideoMeeting
                                     where x.MeetingID == MeetingId
                                     select x.V_MeetingId).FirstOrDefault();
                                
                var meeting = (from M in entity.BM_Meetings
                               from E in entity.BM_EntityMaster
                               where M.EntityId == E.Id 
                               && M.MeetingID == MeetingId
                               select new
                               {
                                   MeetingID = M.MeetingID,
                                   CompanyName = E.CompanyName,
                                   MeetingDate = M.MeetingDate,
                                   MeetingTime = M.MeetingTime,
                                   MeetingTitle = M.MeetingTitle,
                                   MeetingVenue = M.MeetingVenue,
                               }).FirstOrDefault();

                if (meeting.MeetingDate != null && meeting.MeetingTime != null)
                {
                    _objvmeeting = new VMeetingVM()
                    {
                        VMeetingId = VmeetingId,
                        MeetingID = meeting.MeetingID,
                        MeetingTitle = meeting.MeetingTitle + "-" + meeting.CompanyName,
                        MeetingDate1 = Convert.ToDateTime(meeting.MeetingDate).ToString("dd/MMM/yyyy"),
                        MeetingTime = meeting.MeetingTime,
                        EntityName = meeting.CompanyName,
                        Venue = meeting.MeetingVenue,                        
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
            return _objvmeeting;
        }

        public string GetVMeetingId(long meetingId)
        {
            string VMeetingId = string.Empty;

            try
            {
                VMeetingId = (from x in entity.BM_VideoMeeting
                              where x.MeetingID == meetingId
                              && x.IsActive == true
                              select x.V_MeetingId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return VMeetingId;
        }

        public VideoMeetingVM SaveVideoMeeting(VideoMeetingVM MeetingDetails, long MeetingId, int userId)
        {
            try
            {
                var CheckFomateDetails = (from Value in entity.BM_VideoMeeting
                                          where Value.MeetingID == MeetingId
                                          select Value).FirstOrDefault();

                if (CheckFomateDetails == null && !string.IsNullOrEmpty(MeetingDetails.Id))
                {
                    BM_VideoMeeting Meeting = new BM_VideoMeeting();
                    Meeting.MeetingID = MeetingId;
                    Meeting.V_MeetingId = MeetingDetails.Id;
                    Meeting.MType = MeetingDetails.MType;
                    Meeting.Password = MeetingDetails.Password;
                    Meeting.encrypted_password = MeetingDetails.encrypted_password;
                    Meeting.Start_Url = MeetingDetails.Start_Url;
                    Meeting.Join_Url = MeetingDetails.Join_Url;
                    //Meeting.host_video = MeetingDetails.host_video;
                    //Meeting.participant_video = MeetingDetails.participant_video;
                    Meeting.join_before_host = MeetingDetails.join_before_host;
                    Meeting.mute_upon_entry = MeetingDetails.mute_upon_entry;
                    Meeting.approval_type = MeetingDetails.approval_type;
                    Meeting.audio = MeetingDetails.audio;
                    Meeting.auto_recording = MeetingDetails.auto_recording;
                    Meeting.enforce_login = MeetingDetails.enforce_login;
                    Meeting.close_registration = MeetingDetails.close_registration;
                    Meeting.allow_multiple_devices = MeetingDetails.allow_multiple_devices;
                    Meeting.registrants_confirmation_email = MeetingDetails.registrants_confirmation_email;
                    Meeting.waiting_room = MeetingDetails.waiting_room;
                    Meeting.request_permission_to_unmute_participants = MeetingDetails.request_permission_to_unmute_participants;
                    Meeting.registrants_email_notification = MeetingDetails.registrants_email_notification;
                    Meeting.meeting_authentication = MeetingDetails.meeting_authentication;
                    Meeting.Status = MeetingDetails.Status;
                    Meeting.CrBy = userId;
                    Meeting.CrDate = DateTime.Now;
                    Meeting.IsActive = true;

                    entity.BM_VideoMeeting.Add(Meeting);
                    entity.SaveChanges();
                }
                else if (!string.IsNullOrEmpty(MeetingDetails.Id))
                {
                    CheckFomateDetails.MeetingID = MeetingId;
                    CheckFomateDetails.V_MeetingId = MeetingDetails.Id;
                    CheckFomateDetails.MType = MeetingDetails.MType;
                    CheckFomateDetails.Password = MeetingDetails.Password;
                    CheckFomateDetails.encrypted_password = MeetingDetails.encrypted_password;
                    CheckFomateDetails.Start_Url = MeetingDetails.Start_Url;
                    CheckFomateDetails.Join_Url = MeetingDetails.Join_Url;
                    //CheckFomateDetails.host_video = MeetingDetails.host_video;
                    //CheckFomateDetails.participant_video = MeetingDetails.participant_video;
                    CheckFomateDetails.join_before_host = MeetingDetails.join_before_host;
                    CheckFomateDetails.mute_upon_entry = MeetingDetails.mute_upon_entry;
                    CheckFomateDetails.approval_type = MeetingDetails.approval_type;
                    CheckFomateDetails.audio = MeetingDetails.audio;
                    CheckFomateDetails.auto_recording = MeetingDetails.auto_recording;
                    CheckFomateDetails.enforce_login = MeetingDetails.enforce_login;
                    CheckFomateDetails.close_registration = MeetingDetails.close_registration;
                    CheckFomateDetails.allow_multiple_devices = MeetingDetails.allow_multiple_devices;
                    CheckFomateDetails.registrants_confirmation_email = MeetingDetails.registrants_confirmation_email;
                    CheckFomateDetails.waiting_room = MeetingDetails.waiting_room;
                    CheckFomateDetails.request_permission_to_unmute_participants = MeetingDetails.request_permission_to_unmute_participants;
                    CheckFomateDetails.registrants_email_notification = MeetingDetails.registrants_email_notification;
                    CheckFomateDetails.meeting_authentication = MeetingDetails.meeting_authentication;
                    CheckFomateDetails.Status = MeetingDetails.Status;
                    CheckFomateDetails.ModBy = userId;
                    CheckFomateDetails.ModDate = DateTime.Now;

                    CheckFomateDetails.IsActive = true;
                    entity.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return MeetingDetails;
        }


        public VideoMeetingVM DeleteVMeeting(VideoMeetingVM account, long meetingId)
        {
            try
            {
                var checkvmeeting = (from x in entity.BM_VideoMeeting where x.MeetingID == meetingId select x).FirstOrDefault();
                if (checkvmeeting != null)
                {
                    checkvmeeting.V_MeetingId = null;
                    checkvmeeting.IsActive = false;
                    entity.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return account;
        }

        public zoomtokenVM GetToken_Zoom(int customerId)
        {
            var checktokencustomerwise = (from x in entity.BM_CustomerConfiguration
                                          where x.CustomerID == customerId
                                          && x.IsActive == true
                                          select new zoomtokenVM
                                          {
                                              token = x.Zoomtoken,
                                              ExpiryDate = x.ZoomtokenExpiryDate,
                                              ClientId = x.ClientId,
                                              ClientSecret = x.ClientSecret,
                                              refreshToken = x.Refresh_Token,
                                              expiryDateofRefreshToken = x.Refresh_TokenExpiryDate,
                                          }).FirstOrDefault();

            return checktokencustomerwise;
        }

        public CiscoWebexVM SaveVideoMeetingcisco(CiscoWebexVM account, long meetingId, int UserId)
        {
            try
            {
                var CheckFomateDetails =
                (from Value in entity.BM_VideoMeeting
                 where Value.MeetingID == meetingId
                 select Value).FirstOrDefault();

                if (CheckFomateDetails == null && account.id != null)
                {
                    BM_VideoMeeting Meeting = new BM_VideoMeeting();
                    Meeting.MeetingID = meetingId;
                    Meeting.V_MeetingId = account.id;
                    //Meeting.me = Convert.ToInt32(account.meetingNumber);
                    //Meeting.MType = account.meetingType;
                    Meeting.Password = account.password;
                    //Meeting.encrypted_password = MeetingDetails.encrypted_password;
                    Meeting.Start_Url = account.webLink;
                    Meeting.Join_Url = account.siteUrl;

                    Meeting.Status = account.state;
                    Meeting.CrBy = UserId;
                    Meeting.CrDate = DateTime.Now;
                    Meeting.IsActive = true;

                    entity.BM_VideoMeeting.Add(Meeting);
                    entity.SaveChanges();
                }
                else if (account.id != null)
                {
                    CheckFomateDetails.MeetingID = meetingId;
                    CheckFomateDetails.V_MeetingId = account.id;

                    CheckFomateDetails.Password = account.password;

                    CheckFomateDetails.Start_Url = account.webLink;
                    CheckFomateDetails.Join_Url = account.siteUrl;

                    CheckFomateDetails.Status = account.state;
                    CheckFomateDetails.ModBy = UserId;
                    CheckFomateDetails.ModDate = DateTime.Now;
                    CheckFomateDetails.IsActive = true;
                    entity.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return account;
        }

        public CiscoWebexVM DeleteVMeetingCisco(CiscoWebexVM _objciscowebex, long meetingId)
        {
            try
            {
                var CheckFomateDetails = (from Value in entity.BM_VideoMeeting
                                          where Value.MeetingID == meetingId
                                          select Value).FirstOrDefault();

                if (CheckFomateDetails != null)
                {
                    CheckFomateDetails.IsActive = false;
                    entity.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objciscowebex;
        }

        public CiscowebexRecVM SaveCiscowebexRecording(CiscowebexRecVM _objciscorecdtls, int meetingId)
        {
            var checkforRecording = (from row in entity.BM_VideoMeeting_RecordingDetails
                                     where row.V_MeetingId == _objciscorecdtls.meetingSeriesId
                                     && row.MeetingID == meetingId
                                     && row.RecordingId == _objciscorecdtls.id
                                     //&& row.IsActive == true
                                     select row).FirstOrDefault();
            if (checkforRecording == null)
            {
                BM_VideoMeeting_RecordingDetails _objrecor = new BM_VideoMeeting_RecordingDetails()
                {
                    RecordingId = _objciscorecdtls.id,
                    MeetingID = meetingId,
                    V_MeetingId = _objciscorecdtls.meetingId,
                    Downloadurl = _objciscorecdtls.temporaryDirectDownloadLinks.recordingDownloadLink,
                };
                entity.BM_VideoMeeting_RecordingDetails.Add(_objrecor);
                entity.SaveChanges();
            }
            else
            {
                checkforRecording.RecordingId = _objciscorecdtls.id;
                checkforRecording.MeetingID = meetingId;
                checkforRecording.V_MeetingId = _objciscorecdtls.meetingId;
                checkforRecording.Downloadurl = _objciscorecdtls.temporaryDirectDownloadLinks.recordingDownloadLink;
                entity.SaveChanges();
            }
            return _objciscorecdtls;
        }

        public bool SaveZoomRecording(RecordingFile _objzoomrecdtls, int meetingId)
        {
            bool success = false;
            try
            {
                var checkrecordingdtls = (from x in entity.BM_VideoMeeting_RecordingDetails
                                          where x.MeetingID == meetingId
                                          && x.V_MeetingId == _objzoomrecdtls.meeting_id
                                          && x.RecordingId == _objzoomrecdtls.id
                                          select x).FirstOrDefault();

                if (checkrecordingdtls == null)
                {
                    BM_VideoMeeting_RecordingDetails _objrecording = new BM_VideoMeeting_RecordingDetails()
                    {
                        RecordingId = _objzoomrecdtls.id,
                        MeetingID = meetingId,
                        V_MeetingId = _objzoomrecdtls.meeting_id,
                        Downloadurl = _objzoomrecdtls.download_url,
                        Status = false,
                    };
                    entity.BM_VideoMeeting_RecordingDetails.Add(_objrecording);
                    entity.SaveChanges();

                    success = true;
                }
                else
                {
                    checkrecordingdtls.RecordingId = _objzoomrecdtls.id;
                    checkrecordingdtls.MeetingID = meetingId;
                    checkrecordingdtls.V_MeetingId = _objzoomrecdtls.meeting_id;
                    checkrecordingdtls.Downloadurl = _objzoomrecdtls.download_url;
                    checkrecordingdtls.Status = false;
                    entity.SaveChanges();

                    success = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        public void Dispose()
        {

            entity.Dispose();

        }

        public List<FileDataVM> GetvideoConfrencingDetails(long meetingID, int userID, int customerId)
        {
            List<FileDataVM> _objvideomeetin = new List<FileDataVM>();
            try
            {
                _objvideomeetin = (from x in entity.BM_FileData
                                   where x.MeetingId == meetingID
                                   && x.IsDeleted == false
                                   && x.DocType.Trim() == "VMR"
                                   select new FileDataVM
                                   {
                                       FileID = x.Id,
                                       FileName = x.FileName
                                   }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objvideomeetin;
        }

        #region Create Zoom Meeting
        public string CreateVideoMeeting(long MeetingId, int CustomerId, string token, int UserId)
        {

            // HttpResponseMessage responses = Request.CreateResponse(HttpStatusCode.OK);
            VMeetingVM Data = new VMeetingVM();
            VideoMeetingVM objmeetingVM = new VideoMeetingVM();
            string message = string.Empty;
            BM_VideoMeeting Value = new BM_VideoMeeting();

            try
            {

                Data = GetMeetingDetail(CustomerId, MeetingId);

                if (Data.MeetingDate1 != null)
                {
                    CultureInfo culture = new CultureInfo("en-US");
                    DateTime starttime = Convert.ToDateTime(Data.MeetingDate1 + " " + Data.MeetingTime, culture);


                    var obj1 = new
                    {
                        topic = Data.MeetingTitle,
                        type = " 2",//1. for Instant 2. for Schedule Meeting  3. for Recurring Meeting
                        start_time = starttime.ToString("yyyy-MM-ddTHH:mm:ss"),
                        duration = "60",
                        timezone = "Asia/Calcutta",
                        password = "password",
                        agenda = Data.MeetingTitle,


                        settings = new
                        {
                            host_video = " true",
                            participant_video = "true",
                            cn_meeting = " false",
                            in_meeting = " true",
                            join_before_host = "true",
                            mute_upon_entry = " false",
                            jbh_time = 5,

                            watermark = " true",
                            use_pmi = " false",
                            approval_type = "0",
                            registration_type = "3",
                            audio = "both",
                            waiting_room = "false",
                            meeting_authentication="false",
                            auto_recording = "cloud",
                            enforce_login = " false",
                            alternative_hosts = " ",
                            allow_multipul_device = "true,"
                        }
                    };

                    //"{\"topic\":\"Test Meeting\",\"type\":2,\"start_time\":\"2020-11-04T05:35:01\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"Agenda\",\"recurrence\":{\"type\":1,\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":true,\"mute_upon_entry\":false,\"watermark\":false,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}"
                    var client = new RestClient("https://api.zoom.us/v2/users/me/meetings");
                    var request = new RestRequest(RestSharp.Method.POST);
                    request.AddHeader("content-type", "application/json");
                    request.AddHeader("authorization", "Bearer" + token);
                    request.AddParameter("application/json", "{\"topic\":\"" + obj1.topic + "\",\"type\":" + obj1.type + ",\"start_time\":\"" + obj1.start_time + "\",\"duration\":60,\"timezone\":\"" + obj1.timezone + "\",\"password\":\"" + obj1.password + "\",\"agenda\":\"" + obj1.agenda + "\",\"recurrence\":{\"type\":" + obj1.type + ",\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":" + obj1.settings.host_video + ",\"participant_video\":" + obj1.settings.participant_video + ",\"cn_meeting\":" + obj1.settings.cn_meeting + ",\"in_meeting\":" + obj1.settings.in_meeting + ",\"join_before_host\":" + obj1.settings.join_before_host + ",\"mute_upon_entry\":" + obj1.settings.mute_upon_entry + ",\"watermark\":" + obj1.settings.watermark + ",\"use_pmi\":" + obj1.settings.use_pmi + ",\"approval_type\":" + obj1.settings.approval_type + ",\"registration_type\":" + obj1.settings.registration_type + ",\"audio\":\"" + obj1.settings.audio + "\",\"waiting_room\":\"" + obj1.settings.waiting_room + "\",\"auto_recording\":\"" + obj1.settings.auto_recording + "\",\"enforce_login\":" + obj1.settings.enforce_login + ",\"alternative_hosts\":\"\"}}", RestSharp.ParameterType.RequestBody);
                    //request.AddParameter("application/json",obj1, RestSharp.ParameterType.RequestBody);
                    ServicePointManager.MaxServicePointIdleTime = 1000;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                           SecurityProtocolType.Tls11 |
                                           SecurityProtocolType.Tls12;
                    RestSharp.IRestResponse response = client.Execute(request);
                    //  string Check = obj1.ToJSON();
                    message = response.StatusCode.ToString().ToLower();
                    if (response.StatusCode.ToString().ToLower() == "created")
                    {

                        VideoMeetingVM _objvideomeeting = JsonConvert.DeserializeObject<VideoMeetingVM>(response.Content);

                        if (_objvideomeeting != null && !string.IsNullOrEmpty(_objvideomeeting.Id))
                        {
                            objmeetingVM = SaveVideoMeeting(_objvideomeeting, MeetingId, UserId);
                            message = "Video conferencing created successfully";
                        }
                        else
                        {
                            message = "Something went wrong,Video conferencing is not created";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                message = "Server error occured";
            }
            return message;
        }

        #endregion

        #region Update Zoom Meeting
        public string UpdateVideoMeeting(long MeetingId, int CustomerId, string Token, int UserID)
        {
            string message = string.Empty;
            try
            {

                VMeetingVM Data = new VMeetingVM();
                VideoMeetingVM objmeetingVM = new VideoMeetingVM();

                string MeetingVId = GetVMeetingId(MeetingId);
                BM_VideoMeeting Value = new BM_VideoMeeting();
                Data = GetMeetingDetail(CustomerId, MeetingId);
                if (Data.MeetingDate1 != null)
                {
                    CultureInfo culture = new CultureInfo("en-US");
                    DateTime starttime = Convert.ToDateTime(Data.MeetingDate1 + " " + Data.MeetingTime, culture);
                    var obj1 = new
                    {
                        topic = Data.MeetingTitle,
                        type = " 2",//1. for Instant 2. for Schedule Meeting  3. for Recurring Meeting
                        start_time = starttime.ToString("yyyy-MM-dd T HH:mm:ss"),
                        duration = "60",
                        timezone = "Asia/Calcutta",
                        password = "password",
                        agenda = Data.MeetingTitle,


                        settings = new
                        {
                            host_video = " true",
                            participant_video = "true",
                            cn_meeting = " false",
                            in_meeting = " true",
                            join_before_host = "true",
                            mute_upon_entry = " false",
                            jbh_time = 0,

                            watermark = " true",
                            use_pmi = " false",
                            approval_type = "2",
                            registration_type = "3",
                            audio = "both",
                            waiting_room = "true",
                            auto_recording = "cloud",
                            enforce_login = " false",
                            alternative_hosts = " ",
                            allow_multipul_device = "true,"
                        }
                    };

                    var client = new RestSharp.RestClient("https://api.zoom.us/v2/meetings/" + MeetingVId);
                    var request = new RestSharp.RestRequest(RestSharp.Method.PATCH);
                    request.AddHeader("content-type", "application/json");
                    request.AddHeader("authorization", "Bearer" + Token);
                    request.AddParameter("application/json", "{\"topic\":\"" + obj1.topic + "\",\"type\":" + obj1.type + ",\"start_time\":\"" + obj1.start_time + "\",\"duration\":60,\"timezone\":\"" + obj1.timezone + "\",\"password\":\"" + obj1.password + "\",\"agenda\":\"" + obj1.agenda + "\",\"settings\":{\"host_video\":" + obj1.settings.host_video + ",\"participant_video\":" + obj1.settings.participant_video + ",\"cn_meeting\":" + obj1.settings.cn_meeting + ",\"in_meeting\":" + obj1.settings.in_meeting + ",\"join_before_host\":" + obj1.settings.join_before_host + ",\"mute_upon_entry\":" + obj1.settings.mute_upon_entry + ",\"watermark\":" + obj1.settings.watermark + ",\"use_pmi\":" + obj1.settings.use_pmi + ",\"approval_type\":" + obj1.settings.approval_type + ",\"registration_type\":" + obj1.settings.registration_type + ",\"audio\":\"" + obj1.settings.audio + "\",\"waiting_room\":\"" + obj1.settings.waiting_room + "\",\"auto_recording\":\"" + obj1.settings.auto_recording + "\",\"enforce_login\":" + obj1.settings.enforce_login + ",\"alternative_hosts\":\"\"}}", RestSharp.ParameterType.RequestBody);

                    ServicePointManager.MaxServicePointIdleTime = 1000;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                           SecurityProtocolType.Tls11 |
                                           SecurityProtocolType.Tls12;
                    RestSharp.IRestResponse response = client.Execute(request);

                    if (response.ErrorMessage != "BAD REQUEST")
                    {
                        VideoMeetingVM _objvideomeeting = JsonConvert.DeserializeObject<VideoMeetingVM>(response.Content);

                        if (_objvideomeeting != null)
                        {
                            objmeetingVM = SaveVideoMeeting(_objvideomeeting, MeetingId, UserID);
                            message = "Video conferencing is updated successfully";
                        }
                      
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                message = "Server error occurred";
            }

            return message;
        }
        #endregion

        #region Delete Video Meeting
        public string DeleteVideoMeeting(long MeetingId, int CustomerId, string Token, int userId)
        {
            string message = string.Empty;


            VideoMeetingVM objmeetingVM = new VideoMeetingVM();
            string MeetingVId = GetVMeetingId(MeetingId);
            if (!string.IsNullOrEmpty(MeetingVId))
            {

                //"{\"topic\":\"Test Meeting\",\"type\":2,\"start_time\":\"2020-11-04T05:35:01\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"Agenda\",\"recurrence\":{\"type\":1,\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":true,\"mute_upon_entry\":false,\"watermark\":false,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}"
                var client = new RestSharp.RestClient("https://api.zoom.us/v2/meetings/" + MeetingVId);
                var request = new RestSharp.RestRequest(RestSharp.Method.DELETE);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer" + Token);
                // request.AddParameter("application/json", "{\"topic\":\"" + obj1.topic + "\",\"type\":" + obj1.type + ",\"start_time\":\"" + obj1.start_time + "\",\"duration\":60,\"timezone\":\"Asia/India\",\"password\":\"password\",\"agenda\":\"" + obj1.agenda + "\",\"recurrence\":{\"type\":" + obj1.type + ",\"repeat_interval\":1,\"weekly_days\":\"1,2,3,4,5,6\",\"end_times\":50},\"settings\":{\"host_video\":true,\"participant_video\":true,\"cn_meeting\":false,\"in_meeting\":true,\"join_before_host\":true,\"mute_upon_entry\":false,\"watermark\":false,\"use_pmi\":false,\"approval_type\":0,\"registration_type\":3,\"audio\":\"both\",\"waiting_room\":\"false\",\"auto_recording\":\"Local\",\"enforce_login\":false,\"alternative_hosts\":\"\"}}", RestSharp.ParameterType.RequestBody);
                //request.AddParameter("application/json",obj1, RestSharp.ParameterType.RequestBody);
                ServicePointManager.MaxServicePointIdleTime = 1000;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                       SecurityProtocolType.Tls11 |
                                       SecurityProtocolType.Tls12;
                RestSharp.IRestResponse response = client.Execute(request);

                if (response.ErrorMessage != "BAD REQUEST")
                {
                    VideoMeetingVM _objvideomeeting = JsonConvert.DeserializeObject<VideoMeetingVM>(response.Content);

                    message = "Video Conferencing deleted successfully";
                    //if (account != null)

                    //{
                    objmeetingVM = DeleteVMeeting(_objvideomeeting, MeetingId);

                    // }
                }
            }
            else
            {
                message = "Something went wrong";
            }
            return message;
        }

        #endregion

        #region Generate zoom Jwt Created by Ruchi on 04-01-2020
        public string GenerateZoomjwtoken(int CustomerId)
        {

            string ApiKey = "";
            string ApiSecret = "";
            var gettokendetails = GetToken_Zoom(CustomerId);
            if (!string.IsNullOrEmpty(gettokendetails.ClientId) && !string.IsNullOrEmpty(gettokendetails.ClientSecret))
            {
                DateTime Expiry = DateTime.UtcNow.AddDays(1);

                ApiKey = gettokendetails.ClientId;
                ApiSecret = gettokendetails.ClientSecret;


                int ts = (int)(Expiry - new DateTime(1970, 1, 1)).TotalSeconds;

                // Create Security key  using private key above:
                var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(ApiSecret));

                // length should be >256b
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                //Finally create a Token
                var header = new JwtHeader(credentials);

                //Zoom Required Payload
                var payload = new JwtPayload
              {
            { "iss", ApiKey},
            { "exp", ts },
        };

                var secToken = new JwtSecurityToken(header, payload);
                var handler = new JwtSecurityTokenHandler();

                // Token to String so you can use it in your client
                var tokenString = handler.WriteToken(secToken);

                var checkcustomer = (from x in entity.BM_CustomerConfiguration
                                     where x.CustomerID == CustomerId
                                     && x.IsActive == true
                                     select x).FirstOrDefault();
                if (checkcustomer != null)
                {
                    checkcustomer.Zoomtoken = tokenString.ToString();
                    checkcustomer.ZoomtokenExpiryDate = Expiry;
                    entity.SaveChanges();
                }
            }
            return "true";
        }

        #endregion

        #region Create Cisco webex Meeting
        public string CreateVideoMeetingCisco(long MeetingId, int CustomerId, string token, int UserId)
        {


            VMeetingVM Data = new VMeetingVM();
            CiscoWebexVM objmeetingVM = new CiscoWebexVM();
            string message = string.Empty;
            BM_VideoMeeting Value = new BM_VideoMeeting();

            try
            {

                Data = GetMeetingDetail(CustomerId, MeetingId);

                if (Data.MeetingDate1 != null && Data.MeetingTime != null)
                {
                    CultureInfo culture = new CultureInfo("en-US");
                    DateTime starttime = Convert.ToDateTime(Data.MeetingDate1 + " " + Data.MeetingTime, culture).AddHours(-5).AddMinutes(-30);
                    //  DateTime starttime= DateTime.ParseExact(Data.MeetingDate1 + " " + Data.MeetingTime, "MM/dd/yyyy HH:mm:ss", culture).AddHours(-5).AddMinutes(-30); ;

                    DateTime enddate = Convert.ToDateTime(starttime, culture).AddHours(4);
                    var obj1 = new
                    {
                        title = Data.MeetingTitle,
                        agenda = Data.MeetingTitle,
                        password = CreatePassword(6),
                        start = starttime.ToString("yyyy-MM-dd HH:mm:ss"),
                        end = enddate.ToString("yyyy-MM-dd HH:mm:ss"),
                        timezone = "UTC",
                        enabledAutoRecordMeeting = true,
                        allowAnyUserToBeCoHost = false
                        //.ToString("dd-MM-yyyy hh:mm")
                    };

                    var client = new RestClient("https://webexapis.com/v1/meetings");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Authorization", "Bearer  " + token);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", "{\r\n    \"title\": \""+obj1.title+"\",\r\n    \"agenda\": \""+obj1.agenda+"\",\r\n    \"password\": \""+obj1.password+"\",\r\n    \"start\": \""+obj1.start+"\",\r\n    \"end\": \""+obj1.end+"\",\r\n    \"timezone\": \"UTC\",\r\n   \r\n    \"enabledAutoRecordMeeting\": true,\r\n    \"allowAnyUserToBeCoHost\": true\r\n   \r\n}", ParameterType.RequestBody);
                    
                    ServicePointManager.MaxServicePointIdleTime = 1000;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                           SecurityProtocolType.Tls11 |
                                           SecurityProtocolType.Tls12;
                    IRestResponse response = client.Execute(request);

                    message = response.StatusCode.ToString().ToLower();
                    if (response.StatusCode.ToString().ToLower() == "ok")
                    {

                        CiscoWebexVM _objciscowebex = JsonConvert.DeserializeObject<CiscoWebexVM>(response.Content);

                        if (_objciscowebex != null && !(string.IsNullOrEmpty(_objciscowebex.id)))
                        {
                            objmeetingVM = SaveVideoMeetingcisco(_objciscowebex, MeetingId, UserId);
                            message = "Video conferencing created successfully";
                        }
                        else
                        {
                            message = "Something went wrong in creating video conferencing";
                        }

                    }
                    else
                    {
                        message = "Something went wrong";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                message = "Server error occurred";
            }
            return message;
        }
        #endregion

        #region  Update Cisco webex Meeting
        public string UpdateVideoMeetingCisco(long MeetingId, int CustomerId, string token,int UserId)
        {
        
            VMeetingVM Data = new VMeetingVM();
            CiscoWebexVM objmeetingVM = new CiscoWebexVM();

            string MeetingVId = GetVMeetingId(MeetingId);
            string message = string.Empty;

            

            try
            {

                Data = GetMeetingDetail(CustomerId, MeetingId);
                if (Data.MeetingDate1 != null && Data.MeetingTime != null)
                {
                    CultureInfo culture = new CultureInfo("en-US");
                    DateTime starttime = Convert.ToDateTime(Data.MeetingDate1 + " " + Data.MeetingTime, culture).AddHours(-5).AddMinutes(-30);
                    DateTime enddate = Convert.ToDateTime(starttime, culture).AddHours(4);
                    if (Data.MeetingDate1 != null)
                    {
                        var obj1 = new
                        {
                            title = Data.MeetingTitle,
                            agenda = Data.MeetingTitle,
                            password = CreatePassword(6),
                            start = starttime.ToString("yyyy-MM-dd HH:mm:ss"),
                            end = enddate.ToString("yyyy-MM-dd HH:mm:ss"),
                            timezone = "UTC",
                            enabledAutoRecordMeeting = true,
                            allowAnyUserToBeCoHost = true
                        };

                        var client = new RestClient("https://webexapis.com/v1/meetings/" + MeetingVId);
                        ////client.Timeout = -1;
                        var request = new RestRequest(Method.PUT);
                        request.AddHeader("Authorization", "Bearer  " + token);
                        request.AddHeader("Content-Type", "application/json");
                        request.AddParameter("application/json", "{\r\n    \"title\": \"" + obj1.title + "\",\r\n    \"agenda\": \"" + obj1.agenda + "\",\r\n    \"password\": \"" + obj1.password + "\",\r\n    \"start\": \"" + obj1.start + "\",\r\n    \"end\": \"" + obj1.end + "\",\r\n    \"timezone\": \"UTC\",\r\n   \r\n    \"enabledAutoRecordMeeting\": true,\r\n    \"allowAnyUserToBeCoHost\": true\r\n   \r\n}", ParameterType.RequestBody);

                        ServicePointManager.MaxServicePointIdleTime = 1000;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                               SecurityProtocolType.Tls11 |
                                               SecurityProtocolType.Tls12;
                        IRestResponse response = client.Execute(request);


                        //  string Check = obj1.ToJSON();
                        message = response.StatusCode.ToString().ToLower();
                        if (response.StatusCode.ToString().ToLower() == "ok")
                        {
                            message = "Video conferencing created successfully";
                            CiscoWebexVM account = JsonConvert.DeserializeObject<CiscoWebexVM>(response.Content);

                            if (account != null)
                            {
                                objmeetingVM = SaveVideoMeetingcisco(account, MeetingId, UserId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return message;
        }


        #endregion

        #region Delete Cisco webex Meeting
        public string DeleteVideoMeetingCisco(long MeetingId, int CustomerId, string token,int userId)
        {
           
            // HttpResponseMessage responses = Request.CreateResponse(HttpStatusCode.OK);
            VMeetingVM Data = new VMeetingVM();
            CiscoWebexVM objmeetingVM = new CiscoWebexVM();
            string message = string.Empty;
            BM_VideoMeeting Value = new BM_VideoMeeting();

            try
            {

                Data = GetMeetingDetail(CustomerId, MeetingId);

                if (Data.MeetingDate1 != null)
                {
                    var obj1 = new
                    {
                        title = Data.MeetingTitle,
                        agenda = Data.MeetingTitle,
                        password = CreatePassword(6),
                        start = "2021-02-20 9:10:00",
                        end = "2021-02-20 10:10:00",
                        timezone = "UTC",
                        enabledAutoRecordMeeting = true,
                        allowAnyUserToBeCoHost = false
                    };

                    var client = new RestClient("https://webexapis.com/v1/meetings/dd9b37fb674f44b4af76249895c916aa");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.DELETE);
                    request.AddHeader("Authorization", "Bearer NzQ4NDllZmYtM2YxMy00NDY1LWEzMDgtZmExZGFjODQwYmQ5Mjk2NzZiN2ItZjkw_P0A1_fff8f2b4-6d3d-477f-882d-6fee59a6f298");
                    request.AddParameter("text/plain", "", ParameterType.RequestBody);
                    ServicePointManager.MaxServicePointIdleTime = 1000;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                           SecurityProtocolType.Tls11 |
                                           SecurityProtocolType.Tls12;
                    IRestResponse response = client.Execute(request);


                    //  string Check = obj1.ToJSON();
                    message = response.StatusCode.ToString().ToLower();
                    //if (response.StatusCode.ToString().ToLower() == "ok")
                    //{
                    message = "Video conferencing deleted successfully";
                    CiscoWebexVM account = JsonConvert.DeserializeObject<CiscoWebexVM>(response.Content);

                 
                       objmeetingVM = DeleteVMeetingCisco(account, MeetingId);
               
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return message;
        }
        #endregion

        #region generate Token using Refresh token(Ruchi on 17 feb 2021)
        public string GenerateCiscoToken(int CustomerId)
        {
            var gettokendetails = GetToken_Zoom(CustomerId);
            var client = new RestClient("https://webexapis.com/v1/access_token");
            //client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "{\r\n\"grant_type\":\"refresh_token\",\r\n\"client_id\":\"" + gettokendetails.ClientId + "\",\r\n\"client_secret\":\"" + gettokendetails.ClientSecret + "\",\r\n\"refresh_token\":\"" + gettokendetails.refreshToken + "\"\r\n}", ParameterType.RequestBody);
            ServicePointManager.MaxServicePointIdleTime = 1000;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                   SecurityProtocolType.Tls11 |
                                   SecurityProtocolType.Tls12;
            IRestResponse response = client.Execute(request);
            if (response.StatusCode.ToString().ToLower() == "ok")
            {
                CiscoTokenVM _objciscotoken = JsonConvert.DeserializeObject<CiscoTokenVM>(response.Content);
                if(_objciscotoken!=null)
                {
                    bool success = SaveCiscotokenDetails(CustomerId, _objciscotoken);
                }
            }

            return "true";
        }

        private bool SaveCiscotokenDetails(int customerId, CiscoTokenVM _objciscotoken)
        {
            bool success = false;
            try
            {
                DateTime tokenexp =DateTime.Now;
                DateTime ref_tokenexp = DateTime.Now;

                tokenexp = tokenexp.AddSeconds(_objciscotoken.expires_in);
                ref_tokenexp = ref_tokenexp.AddSeconds(_objciscotoken.refresh_token_expires_in);
               

                var checkCustomer = (from x in entity.BM_CustomerConfiguration
                                     where x.CustomerID == customerId
                                     && x.IsActive == true
                                     select x).FirstOrDefault();
                if(checkCustomer!=null)
                {
                    checkCustomer.Zoomtoken = _objciscotoken.access_token;
                    checkCustomer.ZoomtokenExpiryDate = tokenexp;
                    checkCustomer.Refresh_Token = _objciscotoken.refresh_token;
                    checkCustomer.Refresh_TokenExpiryDate = ref_tokenexp;
                    entity.SaveChanges();
                }
                success = true;
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }
        #endregion

        #region generate new  Token  token(Ruchi on 17 feb 2021)
        //public string GenerateCiscoToken(int CustomerID)
        //{
        //    var client = new RestClient("https://webexapis.com/v1/access_token");
        //    client.Timeout = -1;
        //    var request = new RestRequest(Method.POST);
        //    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        //    request.AddParameter("grant_type", "authorization_code");
        //    request.AddParameter("client_id", "");
        //    request.AddParameter("client_secret", "");
        //    request.AddParameter("code", "NGVmNzk3NzYtZWM1ZS00MGRjLWI0YzItNzU3YWI5OTIzZmVjMmE1ODMyNWItYWM1_P0A1_fff8f2b4-6d3d-477f-882d-6fee59a6f298");
        //    request.AddParameter("redirect_uri", "https://470650ef4818.ngrok.io/Meeting/test");
        //    IRestResponse response = client.Execute(request);
        //    Console.WriteLine(response.Content);
        //    return "true";
        //}
        #endregion

        #region Generate Randome Password
        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
        #endregion

        #region Cisco webex Meeting Recording(Downloding,Deleting)

        public string GetCiscoMeetingRecordingList(int MeetingId, int CustomerId)
        {
            var checkforzoomtoken = GetToken_Zoom(CustomerId);
            string VMeetingId = GetVMeetingId(MeetingId);
            string token = checkforzoomtoken.token;
            CiscowebexRecVM objmeetingVM = new CiscowebexRecVM();

            var client = new RestClient("https://webexapis.com/v1/recordings");

            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + token);
            ServicePointManager.MaxServicePointIdleTime = 1000;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                   SecurityProtocolType.Tls11 |
                                   SecurityProtocolType.Tls12;
            IRestResponse response = client.Execute(request);

            CiscoRecordingDetailsVM _objciscorecdtls = JsonConvert.DeserializeObject<CiscoRecordingDetailsVM>(response.Content);
            if (_objciscorecdtls != null)
            {
                if (_objciscorecdtls.items.Count > 0)
                {
                    bool success = SaveCiscowebexRecordingdtls(_objciscorecdtls.items, MeetingId);
                }
               
            }
            return "true";
        }

        private bool SaveCiscowebexRecordingdtls(List<RecordingDetlsVM> recordingdetails, int meetingId)
        {
            foreach (var item in recordingdetails)
            {
                var checkforRecording = (from row in entity.BM_VideoMeeting_RecordingDetails
                                         where row.V_MeetingId == item.meetingSeriesId
                                         && row.MeetingID == meetingId
                                         && row.RecordingId == item.id
                                         //&& row.IsActive == true
                                         select row).FirstOrDefault();
                if (checkforRecording == null)
                {
                    BM_VideoMeeting_RecordingDetails _objrecor = new BM_VideoMeeting_RecordingDetails()
                    {
                        RecordingId = item.id,
                        MeetingID = meetingId,
                        V_MeetingId = item.meetingSeriesId,
                      
                    };
                    entity.BM_VideoMeeting_RecordingDetails.Add(_objrecor);
                    entity.SaveChanges();
                }
                else
                {
                    checkforRecording.RecordingId = item.id;
                    checkforRecording.MeetingID = meetingId;
                    checkforRecording.V_MeetingId = item.meetingSeriesId;
                 
                    entity.SaveChanges();
                }
            }
            return true;
        }

        public string GetCiscoMeetingRecording(int MeetingId,int CustomerId)
        {
            string message = "";
            try
            {
                bool checkrecording = checkrecordingdtls(MeetingId);
                if (checkrecording)
                {
                    var checkforzoomtoken = GetToken_Zoom(CustomerId);
                    string recording = GetCiscoMeetingRecordingList(MeetingId, CustomerId);
                    string VMeetingId = GetVMeetingId(MeetingId);
                    string token = checkforzoomtoken.token;
                    string recordingID = GetCiscoRecordingId(MeetingId, CustomerId, VMeetingId);


                    if (!string.IsNullOrEmpty(recordingID))
                    {
                        CiscowebexRecVM objmeetingVM = new CiscowebexRecVM();

                        var client = new RestClient("https://webexapis.com/v1/recordings/" + recordingID);

                        var request = new RestRequest(Method.GET);
                        request.AddHeader("Authorization", "Bearer " + token);
                        ServicePointManager.MaxServicePointIdleTime = 1000;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                               SecurityProtocolType.Tls11 |
                                               SecurityProtocolType.Tls12;
                        IRestResponse response = client.Execute(request);

                        CiscowebexRecVM _objciscorecdtls = JsonConvert.DeserializeObject<CiscowebexRecVM>(response.Content);
                        if (_objciscorecdtls != null)
                        {
                            objmeetingVM = SaveCiscowebexRecording(_objciscorecdtls, MeetingId);

                            DownloadProversRecording(_objciscorecdtls.temporaryDirectDownloadLinks.recordingDownloadLink, MeetingId, CustomerId, VMeetingId, 1);
                        }
                        message = "";
                    }
                    else
                    {
                        message = "Recording not found";
                    }
                }
                else
                {
                    message = "No more recording found for this meeting";
                }
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                message = "Server error occured";
            }

            return message;
        }

        private bool checkrecordingdtls(int meetingId)
        {
            bool success = false;
            try
            {
                var checkfile = (from x in entity.BM_FileData
                                 where x.MeetingId == meetingId
                                 && x.DocType.Trim() == "VMR"
                                 && x.IsDeleted == false
                                 select x).ToList();
                if(checkfile!=null)
                {
                    if(checkfile.Count>0)
                    {
                        success = false;
                    }
                    else
                    {
                        success = true;
                    }
                }
                else
                {
                    success = true;
                }
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        private string GetCiscoRecordingId(int meetingId, int customerId,string VmeetingId)
        {
            string recordingId = "";
            try
            {
                recordingId = (from x in entity.BM_VideoMeeting_RecordingDetails
                               where x.MeetingID == meetingId
                                && x.V_MeetingId == VmeetingId
                               select x.RecordingId).FirstOrDefault();
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return recordingId;
        }

    
        #endregion

        #region Get zoom Meeting Recording
        public string GetZoomMeetingRecording(int MeetingId, int CustomerId)
        {
            string message = string.Empty;
            bool checkrecording = checkrecordingdtls(MeetingId);
            if (checkrecording)
            {
                var checkforzoomtoken = GetToken_Zoom(CustomerId);
                string VMeetingId = GetVMeetingId(MeetingId);
                ZoomRecordingDetailsVM objmeetingVM = new ZoomRecordingDetailsVM();
                string token = checkforzoomtoken.token;
                var client = new RestClient("https://api.zoom.us/v2/meetings/" + VMeetingId + "/recordings");

                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Bearer" + token);
                request.AddHeader("Cookie", "cred=B8440D5FB1A6E73C89185DC2976F61C5");
                request.AddParameter("text/plain", "", ParameterType.RequestBody);

                ServicePointManager.MaxServicePointIdleTime = 1000;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                ZoomRecordingDetailsVM _objzoomrecdtls = JsonConvert.DeserializeObject<ZoomRecordingDetailsVM>(response.Content);
                if (_objzoomrecdtls != null)
                {
                    if (_objzoomrecdtls.recording_files != null)
                    {
                        if (_objzoomrecdtls.recording_files.Count > 0)
                        {

                            int i = 0;
                            foreach (var item in _objzoomrecdtls.recording_files)
                            {
                                if (item.recording_type == "shared_screen_with_speaker_view")
                                {
                                    bool success = SaveZoomRecording(item, MeetingId);

                                    DownloadProversRecording(item.download_url, MeetingId, CustomerId, item.meeting_id, i);
                                    i++;
                                }
                            }
                        }
                    }
                }
                message = " ";
            }
            else
            {
                message = "No more recording found";
            }
            return message;
        }

        private void DownloadProversRecording(string Zoomdownloadurl, int meetingId, int CustomerId, string VMeetingId, int count)
        {
            VMeetingVM Data = new VMeetingVM();
            Data = GetMeetingDetail(CustomerId, meetingId);

            string directoryPath = "";
            string FileName = "";
            string FullPath = "";
            string remoteUri = "";
            VideoMeeting _objvm = new VideoMeeting();
            var checkforzoomtoken = _objvm.GetToken_Zoom(CustomerId);
            string providerName = GetProviderName(CustomerId);
            string token = checkforzoomtoken.token;
            string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
            if (providerName.ToLower() == SecretarialConst.Providers.Zoom)
            {
                remoteUri = Zoomdownloadurl + "?access_token=" + token;
            }
            else if(providerName.ToLower() == SecretarialConst.Providers.Webex)
            {
                remoteUri = Zoomdownloadurl;
            }

            //string fileName = "D:\\GITLAB\\icai_ui_workingsolution_live\\ComplianceManagement.Portal\\Areas\\Document\\SampleDocument\\Zoom\\file.mp4";

            directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/Document/" + CustomerId + "/"+ providerName + "/" + meetingId + "/");

            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            FileName = Data.MeetingTitle + "-Recording-" + count + ".mp4";
            FullPath = System.IO.Path.Combine(directoryPath, FileName);

            WebClient myWebClient = new WebClient();
            myWebClient.DownloadFile(remoteUri, FullPath);

            UploadfileinCloude(FileName, FullPath, meetingId, CustomerId, Data);
        }

        public void UploadfileinCloude(string fileName, string filePath, int meetingId, int customerId, VMeetingVM Data)
        {
            try
            {
                //VMeetingVM Data = new VMeetingVM();
                VideoMeeting objvmeeting = new VideoMeeting();

                if (Data == null)
                    Data = objvmeeting.GetMeetingDetail(customerId, meetingId);

                string fileKey = Convert.ToString(Guid.NewGuid());
                string fileExtension = System.IO.Path.GetExtension(filePath);
                string FileData = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                //string fileName = "ZoomRecording" + count + FileData + Data.MeetingTitle;
                string fileNameNew = fileKey + fileExtension;
                string fulfilename = fileName;

                Stream fileStream = File.OpenRead(filePath);
                using (fileStream)
                {
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    var directoryPath = "testsecretarial/" + customerId + "/Recording/" + meetingId + "/";
                    CloudBlobContainer container = blobClient.GetContainerReference(directoryPath);
                    container.CreateIfNotExistsAsync();

                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileNameNew);
                    blockBlob.UploadFromStream(fileStream);

                    BM_FileData _obj = new BM_FileData()
                    {
                        FilePath = directoryPath,
                        FileKey = fileKey,
                        FileName = fulfilename,
                        Version = "",
                        IsDeleted = false,
                        EnType = "",
                        OnFileStorage = true,
                        MeetingId = meetingId,
                        VersionDate = DateTime.Now,
                        VersionComment = "",
                        UploadedBy = 1,
                        UploadedOn = DateTime.Now,
                        DocType = "VMR"
                    };

                    if (fileStream.Length != null)
                        _obj.FileSize = fileStream.Length.ToString();

                    entity.BM_FileData.Add(_obj);
                    entity.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string GetProviderName(int customerID)
        {
            string providerName = "";
            try
            {
                providerName = (from x in entity.BM_CustomerConfiguration
                                where x.CustomerID == customerID
                                select x.ProviderName).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return providerName;
        }


        #endregion
    }
}