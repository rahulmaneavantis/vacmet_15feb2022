﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IAgendaMaster
    {
        #region Role-based Agenda list
        List<AgendaMasterVM> GetAll(int customerId, string role);
        List<AgendaMasterVM> GetAll_CustomerEntityTypeWise(int customerId, string role, string productApplicableLogin);

        List<AgendaMasterVM> GetAll_Agendadetils(int customerId, string role, string productApplicableLogin);
        #endregion

        #region Customer-wise Standard Agenda
        StandardAgendaVM GetStadardAgenda(int customerId, long id);
        StandardAgendaFormates_SubVM GetPreCommitteeStadardAgenda(int customerId, long id);
        StandardAgendaFormatesVM SaveAgendaFormates(StandardAgendaFormatesVM obj, int userId);
        StandardAgendaFormates_SubVM SaveAgendaFormates(StandardAgendaFormates_SubVM obj, int userId);
        #endregion
        List<AgendaMasterVM> GetAll();
        AgendaMasterVM GetAgenda(long Id);
        List<SubAgendaMasterVM> GetAllSubAgendas(long ParentId);
        List<AgendaMasterVM> GetAllSubAgenda(long id);
        AgendaMasterVM GetAgendaTemplate(long Id);
        AgendaMasterVM Create(AgendaMasterVM obj, int customerId,string role);
        AgendaMasterVM Update(AgendaMasterVM obj, string role);

        SubAgendaMasterVM Create(SubAgendaMasterVM obj);
        SubAgendaMasterVM Update(SubAgendaMasterVM obj);
        SubAgendaMasterVM GetSubAgenda(long Id);
        string Delete(long id, int userId);

        #region Agenda Informative Text
        AgendaInfoVM GetInfomativeText(long agendaId);
        AgendaInfoVM SaveInfomativeText(AgendaInfoVM obj, int userId);
        #endregion

        #region Sub Agenda New
        List<SubAgendaMasterVM> GetAllSubAgendaNew(int MeetingTypeId, long Parent_AgendaId);
        List<AgendaMaster_SubAgendaMappging> GetAllSubAgendasNew(long ParentId);
        AgendaMaster_SubAgendaMappging Create(AgendaMaster_SubAgendaMappging obj);
        AgendaMaster_SubAgendaMappging Update(AgendaMaster_SubAgendaMappging obj);
        AgendaMaster_SubAgendaMappging GetSubAgendaNew(long Id);
        #endregion

        #region Agenda details for Meeting Module
        //
        // Summary:
        //     Computes the average of a sequence of System.Int32 values.
        //
        // Parameters:
        //   EntityId:
        //     Entity Id is a sequence of System.Int32 value.
        //
        //   MeetingTypeId:
        //     Meeting Type Id is a sequence of System.Int32 value.
        //
        // Returns:
        //     An IEnumerable that represents the Agenda Item
        //
        IEnumerable<SubAgendaMasterVM> GetAgendaItems(int EntityId, int MeetingTypeId);

        IEnumerable<AgendaItemSelect> GetAgendaItemsSelectList(long MeetingId, string Part, int CustomerId);
        IEnumerable<AgendaItemSelect> GetPendingAgendaItemsSelectList(long MeetingId, string Part, int CustomerId);
        IEnumerable<AgendaItemSelect> GetMeetingMultiStageAgendaItemHistory(long StartMeetingId, long StartAgendaId, int EntityId, int CustomerId);
        IEnumerable<OpenAgendaDetailsVM> GetDetailsOfOpenAgendaItem(long MappingId);
        #endregion

        #region UI Form
        IEnumerable<UIFormsVM> GetUIForms();
        #endregion

        AgendaMasterVM UpdateTemplate(AgendaMasterVM obj);
        IEnumerable<AgendaFrequencyVM> GetAgendaFrequency();
        bool DeleteAgendaComplianceMapping(int Id);
        bool UpdateAgendaDetails(AgendaMasterVM agendadoctypeItem, int userID, int? customerID);

        
    }
}
