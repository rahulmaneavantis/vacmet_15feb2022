﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IEntityMaster
    {
        string GetEntityName(int EntityID, int CustomerID);
        List<Customer_VM> GetCustomers();
        EntityMaster GetEntityMaster(int id);
        #region Role based entity Details
        List<EntityMasterVM> GetAllEntityMaster(int customerID);
        List<EntityMasterVM> GetAllEntityMaster(int customerID, int userID, string role);
        List<EntityMasterVM> GetAllEntityMasterForMeeting(int customerID);
        List<EntityMasterVM> GetAllEntityMasterForMeeting(int customerID, int userID, string role);
        List<EntityMasterVM> GetAllEntityMasterForMeetingNew(int customerID, int userID, string role);
        #endregion
        List<EntityAddressVM> GetEntityAddressForMeeting(int Entity_Id, int customerID);

        string AddEntityMasterdtls(EntityMasterVM _objentity, int CustomerId);

        IEnumerable<BM_EntityType> GetCompanyType();
        IEnumerable<EntityTypeVM> GetCompanyTypeVM();
        IEnumerable<EntityTypeVM> EntityTypeVMForMeeting();
        IEnumerable<VMState> GetStateList();
        IEnumerable<VMCity> GetCityList(int? stateId);
        IEnumerable<BM_CompanyCategory> GetCompanyCategory();
        IEnumerable<BM_ROC_code> GetRocCode();
        string UpdateEntity(EntityMasterVM _objentity);
        void Dispose();
        IEnumerable<BM_StockExchange> GetAllStockExchange(bool addNew);
        IEnumerable<BM_BusinessActivity> GetBusinessActivityCode();
        IEnumerable<BM_BusinessActivity> GetBusinessActivDesc(int businessId);
        List<EntityMasterVM> GetAllEntityMasterForCommittee(long Director_Id);
        Pravate_PublicVM CreatePublicPrivate(Pravate_PublicVM _objentity, int customerId, int UserId);
        LLPVM CreateLLP(LLPVM _objentity, int customerId, int UserId);
        TrustVM Createtrusts(TrustVM _objentity, int customerId, int UserId);
        TrustVM Updatetrusts(TrustVM _objentity, int customerId, int UserId);
        BodyCorporateVM Createbody_Corporate(BodyCorporateVM _objentity, int customerId, int UserId);
        List<EntityBranchDetailsVM> GetEntityBranchList(int customerId, int userID, string role, int entityId);
        IEnumerable<VMCountry> GetAllCountryDtls();
        List<EntityMasterVM> GetAllEntityMasterbyid(int customerId, int userID, string role, long entityId);
        List<EntityMasterVM> GetAllEntityForBOD(int customerId, int userID);
        Pravate_PublicVM GetprivatepublicDtls(int Id, int EntityId);
        LLPVM GetLLPDtls(int id, int entityId);
        IEnumerable<VMCity> GetAllCountryCitydtlsForBodyCorporate(int? CountryId);
        TrustVM GetTustDtls(int Id, int EntityId);
        BodyCorporateVM GetBodyCorporate(int id, int entityTypeId);
        #region Applicability 
        decimal GetCapital(int Entity_Id, int customerId);
        BM_Rule GetBM_Applicability(int Entity_Id, decimal PreferenceShare, int customerId);
        BM_Rule GetBM_Applicability(BM_Rule objBM_Rule, int customerId);
        bool UpdateBM_Applicability(BM_Rule objBM_Rule);
        bool DeleteEntityInfo(int Id, int customerId);
        IEnumerable<VMState> GetStateListforMaster(int countryId);
        IEnumerable<BM_Nationality> GetNationality();
        #endregion

        bool CreateEntityFromLegal(CustomerBranch obj);

        bool UpdateEntityFromLegal(CustomerBranch obj);

        string GetFY(long entityId);
        IEnumerable<EntityMasterVM> EntityListdirectorwise(int userID);
        List<EntityMasterVM> GetEntityForTask(int customerId, int userID, string role);
        BodyCorporateVM UpdateBody_Corporate(BodyCorporateVM _objentity, int customerId, int userId);
        LLPVM UpdateLLP(LLPVM _objentity, int customerId, int UserId);
        VM_QuestionForShareholding SaveSharesDetails(VM_QuestionForShareholding objsharesholder);
        IEnumerable<ListofstockExchange> readListedStockExchangeData(int entityID);
        ListofstockExchange Create(ListofstockExchange objlistofstockexc);
        Pravate_PublicVM UpdatePublicPrivateEntity(Pravate_PublicVM _objentity, int customerId, int UserId);
        ListofstockExchange Update(ListofstockExchange objlistofstockexc);
        ListofstockExchange Destroy(ListofstockExchange objlistofstockexc);
        VMEntityFileUpload ImportDatafrom_MGT7(VMEntityFileUpload objfileupload);
        IEnumerable<BM_CompanySubCategory> GetDropDownforCompanySubCategory();

        List<SubCompanyType> GetSubEntityDetails(long entityID, long customerID);
        List<AssosiateType> GetAllEntityMaster();
        void addCompanySubType(SubCompanyType items);
        void DeleteSubEntityMapping(SubCompanyType items);
        SubCompanyType getDetailsofentity(long entityId, string cIN, int CustomerId);

        List<EntityMasterVM> GetAssignedEntities(int custID, int userID, string userRole);
        List<EntityMasterVM> GetEntitiesDropDown(int custID, int userID, string userRole);

        #region Import Entity from AVACOM
        List<ParentEntitySelectVM> GetEntityListForImport(int customerId);
        IEnumerable<ParentEntitySelectVM> ImportEntityFromAVACOM(IEnumerable<ParentEntitySelectVM> lstEntities, int createdBy);
        EntityDocumentVM CreateEntityDocuments(EntityDocumentVM _objdoc, int userId, int customerId);
        List<BM_SP_GetEntityDocuments_Result> GetEntityDocuments(int UserId, int customerId, string Role, int entityId, string doctype);
        
        string Viewdocument(long id);
        bool Downloadrecords(long id);
        string GetFilePath(long id);
        bool DeleteDocdat(int id, int UserId);
        EntityBranchDetailsVM GetBranchDetails(int entityID, int branchId);
        VMEntityFileUpload ImportDatafrom_LLP11(VMEntityFileUpload objfileupload,int CustomerId,int USerId);
        VMEntityFileUpload ImportDatafrom_LLPFliip(VMEntityFileUpload objfileupload);
        //IEnumerable<EntityDocTypeVM> GetDocumentType();
        IEnumerable<EntityDocTypeVM> GetDocumentType(int entityType);
        IEnumerable<EntityDocTypeVM> UploadChargeDocument(int entityType);

        IEnumerable<SubIndustriesVM> GetRegulatoryAuthority();
        List<Body_CorporateVM> GetBodyCorporateNomnee(int customerId);
        List<Body_CorporateVM> GetBodyCorporateMaster(int customerId);
        Body_CorporateVM GetBodyCorporateMaster(int id, int customerId);
        Body_CorporateVM CreateUpdatebodycorporate(Body_CorporateVM _objbodycorporate, int userId, int customerId);
		#endregion
        #region Copy Meetings
        List<EntityMasterVM> GetEntitiesListForCopyMeeting(int entityId, int entityTypeID, int customerID);
        #endregion

        #region Import Excel (Private, Public, Listed)
        VMEntityFileUpload ImportDataEntityMasterExcel(VMEntityFileUpload objfileupload, int CustomerID, int UserId);
        #endregion
    }
}