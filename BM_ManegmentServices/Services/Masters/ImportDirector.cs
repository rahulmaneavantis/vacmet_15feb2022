﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class ImportDirector : IImportDirector
    {
        #region Import Director
        public List<ImportDirectorFromCustomerVM> GetDirectorListForImport(int customerId)
        {
            var result = new List<ImportDirectorFromCustomerVM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SP_DirectorListForImportFromOtherCust(customerId)
                              select new ImportDirectorFromCustomerVM
                              {
                                  CustId = row.CustId,
                                  DirectorId = row.DirectorId,
                                  Name = row.DirectorName,
                                  DIN = row.DIN,
                                  PAN = row.PAN,
                                  Email = row.EmailId,
                                  IsCheked = false
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public Message ImportDirectorFromOtherCustomer(IEnumerable<ImportDirectorFromCustomerVM> lstDirectors, int customerId, int createdBy)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    foreach (var item in lstDirectors)
                    {
                        var _obj = (from row in entities.BM_DirectorMasterFromOtherCustomer
                                                where row.CustomerId == customerId && row.DirectorId == item.DirectorId
                                                select row).FirstOrDefault();
                        if (_obj != null)
                        {
                            _obj.IsActive = true;
                            _obj.IsDeleted = false;
                            _obj.UpdatedBy = createdBy;
                            _obj.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }
                        else
                        {
                            _obj = new BM_DirectorMasterFromOtherCustomer();
                            _obj.CustomerId = customerId;
                            _obj.DirectorId = item.DirectorId;

                            _obj.IsActive = true;
                            _obj.IsDeleted = false;
                            _obj.CreatedBy = createdBy;
                            _obj.CreatedOn = DateTime.Now;

                            entities.BM_DirectorMasterFromOtherCustomer.Add(_obj);
                            entities.SaveChanges();

                            var spResult = (from row in entities.BM_SP_DirectorListOnImportFromOtherCust(item.DirectorId, customerId, createdBy)
                                      select row).FirstOrDefault();

                        }
                    }
                }
                result.Success = true;
                result.Message = SecretarialConst.Messages.saveSuccess;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion
    }
}
