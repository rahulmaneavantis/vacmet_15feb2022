﻿using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class ShareholdingMastre : IShareholdingMaster
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public EntityMasterVM getEntitIdbyId(int entity_Id, int customerId)
        {
            try
            {
                var getShareholdingdtlsbyId = (from row in entities.BM_EntityMaster
                                               where row.Id == entity_Id
                                               && row.Customer_Id == customerId
                                               select new EntityMasterVM
                                               {
                                                   Id = row.Id,
                                                   isDemate = row.IsDematerialisedForm,
                                                   IsPartialorComplited = row.SharesIsComplitedorPatial
                                               }).FirstOrDefault();

                return getShareholdingdtlsbyId;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #region Shareholding Details

        public string UpdateShareholdingdtls(VMShareholding _objshareholder, int customerId)
        {
            try
            {
                //var _objupdateshareholder = (from row in entities.BM_ShareHolding
                //                             where row.Id == _objshareholder.Id
                //                             && row.Customer_Id == _objshareholder.Customer_Id
                //                             && row.IsActive == true
                //                             && row.Follio_No == _objshareholder.Follio_No
                //                             select row).FirstOrDefault();
                //if (_objupdateshareholder != null)
                //{
                //_objupdateshareholder.Customer_Id = customerId;
                //_objupdateshareholder.becomingmember_Date = _objshareholder.becomingmember_Date;
                //_objupdateshareholder.bodycorporate_Address = _objshareholder.bodycorporate_Address;
                //_objupdateshareholder.Cessation_Membership_Date = _objshareholder.Cessation_Membership_Date;
                //_objupdateshareholder.CIN = _objshareholder.CIN;
                //_objupdateshareholder.declaration_Date_under89 = _objshareholder.declaration_Date_under89;
                //_objupdateshareholder.DOB_minor = _objshareholder.DOB_minor;
                //_objupdateshareholder.Email_Id = _objshareholder.Email_Id;
                //_objupdateshareholder.Father_Mother_Spouse_Name = _objshareholder.Father_Mother_Spouse_Name;
                //_objupdateshareholder.Follio_No = _objshareholder.Follio_No;
                //_objupdateshareholder.Guardian_Name = _objshareholder.Guardian_Name;
                //_objupdateshareholder.JointHolder_Name = _objshareholder.JointHolder_Name;
                //_objupdateshareholder.lienonshares = _objshareholder.lienonshares;
                //_objupdateshareholder.Marital_Status = _objshareholder.Marital_Status;
                //_objupdateshareholder.Member_Name = _objshareholder.Member_Name;
                //_objupdateshareholder.Name_and_Address_ben = _objshareholder.Name_and_Address_ben;
                //_objupdateshareholder.Nationality = _objshareholder.Nationality;
                //_objupdateshareholder.Nomination_Date_Receipt = _objshareholder.Nomination_Date_Receipt;
                //_objupdateshareholder.NominaVal_per_Shares = _objshareholder.NominaVal_per_Shares;
                //_objupdateshareholder.NomineeName_Address = _objshareholder.NomineeName_Address;
                //_objupdateshareholder.Occupation = _objshareholder.Occupation;
                //_objupdateshareholder.PAN = _objshareholder.PAN;
                //_objupdateshareholder.Sendingnotices_Instruction = _objshareholder.Sendingnotices_Instruction;
                //_objupdateshareholder.shares_abeyance = _objshareholder.shares_abeyance;
                //_objupdateshareholder.Shares_Class = _objshareholder.Shares_Class;
                //_objupdateshareholder.Tot_SharesHeld = _objshareholder.Tot_SharesHeld;
                //_objupdateshareholder.IsActive = true;
                ////objshareholding.Createdby = "";
                //_objupdateshareholder.CreatedOn = DateTime.Now;
                ////objshareholding.Updatedby=
                //_objupdateshareholder.UpdatedOn = DateTime.Now;
                //entities.SaveChanges();
                return "Updated Successfully.";
                //}
                //else
                //{
                //    return "data not found";
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "something went wrong";
            }
        }


        #endregion

        #region ShareHolding Data

        public ShareHoldings getShareHoldingData(long shareholdingId, int customerId)
        {
            try
            {
                var getSHDtls = (from row in entities.BM_ShareHolding
                                 where row.IsActive == true
                                 && row.Id == shareholdingId
                                 select new ShareHoldings
                                 {
                                     Id = row.Id,
                                     Follio_No = row.Follio_No,
                                     Type = (int)row.Type,
                                     CIN = row.CIN,
                                     Shares_Class = row.Shares_Class,
                                     NominaVal_per_Shares = row.NominaVal_per_Shares,
                                     Tot_SharesHeld = (long)row.Tot_SharesHeld,
                                     Member_Name = row.Member_Name,
                                     JointHolder_Name = row.JointHolder_Name,
                                     Name_and_Address_ben = row.Name_and_Address_ben,
                                     Email_Id = row.Email_Id,
                                     Father_Mother_Spouse_Name = row.Father_Mother_Spouse_Name,
                                     Marital_Status = row.Marital_Status,
                                     Occupation = row.Occupation,
                                     PAN = row.PAN,
                                     Nationality = row.Nationality,
                                     Guardian_Name = row.Guardian_Name,
                                     IsMemberMinnor = row.IsMemberMinnor,
                                     DOB_minor = row.DOB_minor,
                                     becomingmember_Date = row.becomingmember_Date,
                                     declaration_Date_under89 = row.declaration_Date_under89,
                                     bodycorporate_Address = row.bodycorporate_Address,
                                     Nomination_Date_Receipt = row.Nomination_Date_Receipt,
                                     shares_abeyance = row.shares_abeyance,
                                     lienonshares = row.lienonshares,
                                     Cessation_Membership_Date = row.Cessation_Membership_Date,
                                     Sendingnotices_Instruction = row.Sendingnotices_Instruction,
                                     ISPramoter = row.IsPramoter,
                                     //IsDirector = row.IsDirector,
                                     //DirectorId = row.DirectorId

                                 }).FirstOrDefault();
                return getSHDtls;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public ShareHoldings UpdateShareholdingdata(ShareHoldings _objshareholder, int customerId, int userId)
        {
            _objshareholder.Message = new Response();
            try
            {
                var CheckFollioNumber = (from row in entities.BM_ShareHolding
                                         where row.Id != _objshareholder.Id
                                         && row.IsActive == true
                                         && row.Customer_Id == customerId
                                         && row.Entity_Id == _objshareholder.Entity_Id
                                         && row.Follio_No == _objshareholder.Follio_No
                                         select row).ToList();

                if (CheckFollioNumber.Count == 0)
                {
                    if (_objshareholder.Entity_Id > 0)
                    {
                        var _objupdateshareholder = (from row in entities.BM_ShareHolding
                                                     where row.Customer_Id == customerId
                                                     && row.Entity_Id == _objshareholder.Entity_Id
                                                     && row.Id == _objshareholder.Id
                                                     && row.IsActive == true
                                                     select row).FirstOrDefault();

                        if (_objupdateshareholder != null)
                        {
                            if (_objshareholder.becomingmember_Date != null)
                            {
                                _objupdateshareholder.becomingmember_Date = _objshareholder.becomingmember_Date;
                            }

                            if (_objshareholder.IsMemberMinnor)
                            {
                                _objupdateshareholder.Guardian_Name = _objshareholder.Guardian_Name;
                                _objupdateshareholder.DOB_minor = _objshareholder.DOB_minor;
                                _objupdateshareholder.IsMemberMinnor = _objshareholder.IsMemberMinnor;
                            }
                            else
                            {
                                _objupdateshareholder.Guardian_Name = "";
                                _objupdateshareholder.DOB_minor = null;
                                _objupdateshareholder.IsMemberMinnor = _objshareholder.IsMemberMinnor;
                            }

                            _objupdateshareholder.IsPramoter = _objshareholder.ISPramoter;
                            _objupdateshareholder.Member_Name = _objshareholder.Member_Name;
                            _objupdateshareholder.bodycorporate_Address = _objshareholder.bodycorporate_Address;
                            _objupdateshareholder.Cessation_Membership_Date = _objshareholder.Cessation_Membership_Date;
                            _objupdateshareholder.Type = _objshareholder.Type;
                            _objupdateshareholder.CIN = _objshareholder.CIN;
                            _objupdateshareholder.declaration_Date_under89 = _objshareholder.declaration_Date_under89;
                            _objupdateshareholder.DOB_minor = _objshareholder.DOB_minor;
                            if (string.IsNullOrEmpty(_objshareholder.Email_Id))
                            {
                                _objupdateshareholder.Email_Id = "";
                            }
                            else
                            {
                                _objupdateshareholder.Email_Id = _objshareholder.Email_Id;
                            }
                            _objupdateshareholder.Father_Mother_Spouse_Name = _objshareholder.Father_Mother_Spouse_Name;
                            _objupdateshareholder.Follio_No = _objshareholder.Follio_No;
                            _objupdateshareholder.Guardian_Name = _objshareholder.Guardian_Name;
                            _objupdateshareholder.JointHolder_Name = _objshareholder.JointHolder_Name;
                            _objupdateshareholder.lienonshares = _objshareholder.lienonshares;
                            _objupdateshareholder.Marital_Status = _objshareholder.Marital_Status;

                            _objupdateshareholder.Name_and_Address_ben = _objshareholder.Name_and_Address_ben;
                            if (_objshareholder.Nationality > 0)
                            {
                                _objupdateshareholder.Nationality = _objshareholder.Nationality;
                            }
                            else
                            {
                                _objupdateshareholder.Nationality = -1;
                            }
                            _objupdateshareholder.Nomination_Date_Receipt = _objshareholder.Nomination_Date_Receipt;
                            _objupdateshareholder.NominaVal_per_Shares = _objshareholder.NominaVal_per_Shares;
                            _objupdateshareholder.NomineeName_Address = _objshareholder.NomineeName_Address;
                            _objupdateshareholder.Occupation = _objshareholder.Occupation;

                            if (string.IsNullOrEmpty(_objshareholder.PAN))
                            {
                                _objupdateshareholder.PAN = "";
                            }
                            else
                            {
                                _objupdateshareholder.PAN = _objshareholder.PAN;
                            }
                            _objupdateshareholder.Sendingnotices_Instruction = _objshareholder.Sendingnotices_Instruction;
                            _objupdateshareholder.shares_abeyance = _objshareholder.shares_abeyance;
                            _objupdateshareholder.Shares_Class = _objshareholder.Shares_Class;
                            _objupdateshareholder.Tot_SharesHeld = _objshareholder.Tot_SharesHeld;
                            _objupdateshareholder.IsActive = true;
                            _objupdateshareholder.UpdatedOn = DateTime.Now;
                            _objupdateshareholder.Updatedby = userId;
                            entities.SaveChanges();

                            _objshareholder.Id = _objupdateshareholder.Id;
                            _objshareholder.Message.Success = true;
                            _objshareholder.Message.Message = "Updated Successfully.";
                            return _objshareholder;
                        }
                        else
                        {
                            _objshareholder.Message.Error = true;
                            _objshareholder.Message.Message = "Data not found";
                            return _objshareholder;
                        }
                    }
                    else
                    {
                        _objshareholder.Message.Error = true;
                        _objshareholder.Message.Message = "Something went wrong";
                        return _objshareholder;
                    }
                }
                else
                {
                    _objshareholder.Message.Error = true;
                    _objshareholder.Message.Message = "Record with same Folio Number already exists";
                    return _objshareholder;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                _objshareholder.Message.Error = true;
                _objshareholder.Message.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                return _objshareholder;
            }
            catch (Exception ex)
            {
                _objshareholder.Message.Error = true;
                _objshareholder.Message.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objshareholder;
            }
        }

        public ShareHoldings AddShareholdingdata(ShareHoldings _objshareholder, int customerId, int userId)
        {
            try
            {
                _objshareholder.Message = new Response();
                if (_objshareholder.Entity_Id > 0)
                {
                    var _objupdateshareholder = (from row in entities.BM_ShareHolding
                                                 where row.Customer_Id == customerId
                                                  && row.Entity_Id == _objshareholder.Entity_Id
                                                  && row.Follio_No == _objshareholder.Follio_No
                                                  && row.IsActive == true
                                                 select row).FirstOrDefault();

                    if (_objupdateshareholder == null)
                    {
                        BM_ShareHolding objshareholding = new BM_ShareHolding();
                        objshareholding.Customer_Id = customerId;
                        objshareholding.Entity_Id = _objshareholder.Entity_Id;

                        if (_objshareholder.becomingmember_Date != null)
                        {
                            objshareholding.becomingmember_Date = _objshareholder.becomingmember_Date;
                        }

                        objshareholding.Member_Name = _objshareholder.Member_Name;

                        if (_objshareholder.IsMemberMinnor)
                        {
                            objshareholding.Guardian_Name = _objshareholder.Guardian_Name;
                            objshareholding.DOB_minor = _objshareholder.DOB_minor;
                            objshareholding.IsMemberMinnor = _objshareholder.IsMemberMinnor;
                        }
                        else
                        {
                            objshareholding.Guardian_Name = "";
                            objshareholding.DOB_minor = null;
                            objshareholding.IsMemberMinnor = _objshareholder.IsMemberMinnor;
                        }

                        objshareholding.IsPramoter = _objshareholder.ISPramoter;
                        objshareholding.bodycorporate_Address = _objshareholder.bodycorporate_Address;
                        objshareholding.Cessation_Membership_Date = _objshareholder.Cessation_Membership_Date;
                        objshareholding.Type = _objshareholder.Type;
                        objshareholding.CIN = _objshareholder.CIN;
                        objshareholding.declaration_Date_under89 = _objshareholder.declaration_Date_under89;
                        objshareholding.DOB_minor = _objshareholder.DOB_minor;
                        if (string.IsNullOrEmpty(_objshareholder.Email_Id))
                        {
                            objshareholding.Email_Id = "";
                        }
                        else
                        {
                            objshareholding.Email_Id = _objshareholder.Email_Id;
                        }
                        objshareholding.Father_Mother_Spouse_Name = _objshareholder.Father_Mother_Spouse_Name;
                        objshareholding.Follio_No = _objshareholder.Follio_No;

                        objshareholding.JointHolder_Name = _objshareholder.JointHolder_Name;
                        objshareholding.lienonshares = _objshareholder.lienonshares;
                        objshareholding.Marital_Status = _objshareholder.Marital_Status;

                        objshareholding.Name_and_Address_ben = _objshareholder.Name_and_Address_ben;
                        if (_objshareholder.Nationality > 0)
                        {
                            objshareholding.Nationality = _objshareholder.Nationality;
                        }
                        else
                        {
                            objshareholding.Nationality =-1;
                        }
                        objshareholding.Nomination_Date_Receipt = _objshareholder.Nomination_Date_Receipt;
                        objshareholding.NominaVal_per_Shares = _objshareholder.NominaVal_per_Shares;
                        objshareholding.NomineeName_Address = _objshareholder.NomineeName_Address;
                        objshareholding.Occupation = _objshareholder.Occupation;
                        if (string.IsNullOrEmpty(_objshareholder.PAN))
                        {
                            objshareholding.PAN ="";
                        }
                        else
                        {
                            objshareholding.PAN = _objshareholder.PAN;
                        }
                        objshareholding.Sendingnotices_Instruction = _objshareholder.Sendingnotices_Instruction;
                        objshareholding.shares_abeyance = _objshareholder.shares_abeyance;
                        objshareholding.Shares_Class = _objshareholder.Shares_Class;
                        objshareholding.Tot_SharesHeld = _objshareholder.Tot_SharesHeld;
                        objshareholding.IsActive = true;
                        objshareholding.Createdby = userId;
                        objshareholding.CreatedOn = DateTime.Now;
                        //objshareholding.Updatedby=
                        objshareholding.UpdatedOn = DateTime.Now;
                        entities.BM_ShareHolding.Add(objshareholding);
                        entities.SaveChanges();
                        _objshareholder.Id = objshareholding.Id;
                        _objshareholder.Message.Success = true;
                        _objshareholder.Message.Message = "Saved Successfully.";
                        return _objshareholder;
                    }
                    else
                    {
                        //_objshareholder.Id = _objupdateshareholder.Id;
                        _objshareholder.Message.Error = true;
                        _objshareholder.Message.Message = "Record with same Folio Number alraedy exists";
                        return _objshareholder;
                    }
                }
                else
                {
                    _objshareholder.Message.Error = true;
                    _objshareholder.Message.Message = "Something wents wrong";
                    return _objshareholder;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                _objshareholder.Message.Error = true;
                _objshareholder.Message.Message = SecretarialConst.Messages.validationError;

                string errMsg = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    //LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    errMsg = string.Empty;
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errMsg = String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State) + String.Format("Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        LoggerMessage_AVASEC.InsertErrorMsg_DBLog(errMsg, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                return _objshareholder;
            }
            catch (Exception ex)
            {
                _objshareholder.Message.Error = true;
                _objshareholder.Message.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return _objshareholder;
            }
        }

        public List<VMShareholding> GetShareholdingdata(int EntityId, int CustomerId)
        {
            try
            {
                var getShareholdingData = (from row in entities.BM_SP_ShareHolderDetails(EntityId, CustomerId)
                                           select new VMShareholding
                                           {
                                               Id = row.Id,
                                               Follio_No = row.Follio_No,
                                               Tot_SharesHeld = row.Tot_SharesHeld,
                                               Member_Name = row.Member_Name,
                                               EmailId = row.Email_Id,
                                               equity_PaidUp = row.TotamtNoPaidupCapital,
                                               Prifrencepaidup = row.Pri_TotamtNoPaidupCapital,

                                               paidupCapital = Convert.ToDecimal(row.Pri_TotamtNoPaidupCapital + row.TotamtNoPaidupCapital),
                                               //perofTotShareHolding= Math.Round(Convert.ToDecimal((row.Tot_SharesHeld)/(row.Pri_TotamtNoPaidupCapital + row.TotamtNoPaidupCapital) * 100)),
                                           }).ToList();
                if (getShareholdingData.Count > 0)
                {
                    foreach (var item in getShareholdingData)
                    {
                        if ((item.equity_PaidUp + item.Prifrencepaidup) != 0)
                            item.perofTotShareHolding = Math.Round(Convert.ToDecimal((item.Tot_SharesHeld) / (item.equity_PaidUp + item.Prifrencepaidup) * 100), 2);
                    }
                }
                return getShareholdingData;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<VMShareholding> GetShareholdingMember(int EntityId, int CustomerId)
        {
            try
            {
                var getShareholdingData = (from row in entities.BM_SP_ShareHolderDetails(EntityId, CustomerId)
                                           select new VMShareholding
                                           {
                                               Id = row.Id,
                                               Member_Name = row.Member_Name,
                                               Follio_No = row.Follio_No
                                           }).ToList();

                getShareholdingData.Insert(0, new VMShareholding { Id = 0, Member_Name = "All" ,Follio_No =""});
                return getShareholdingData;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion

        #region Excel Upload
        public bool CreatePublicPrivateListedData(BM_ShareholderListedEntityData _objBenposdata)
        {
            bool success = false;
            try
            {

                BM_ShareholderListedEntityData _objBanpos = new BM_ShareholderListedEntityData();
                if (_objBenposdata != null)
                {
                    var CheckListedShareholdiingdtls = (from row in entities.BM_ShareholderListedEntityData
                                                        where row.EntityId == _objBenposdata.EntityId
                                                         && row.FOLIO_NO == _objBenposdata.FOLIO_NO
                                                        select row).FirstOrDefault();
                    if (CheckListedShareholdiingdtls == null)
                    {
                        _objBanpos.FOLIO_NO = _objBenposdata.FOLIO_NO;
                        _objBanpos.SHARE_HOLDER_NAME = _objBenposdata.SHARE_HOLDER_NAME;
                        _objBanpos.SHARES = _objBenposdata.SHARES;
                        _objBanpos.JOINT_HOLDER1 = _objBenposdata.JOINT_HOLDER1;
                        _objBanpos.JOINT_HOLDER2 = _objBenposdata.JOINT_HOLDER2;
                        _objBanpos.JOINT_HOLDER3 = _objBenposdata.JOINT_HOLDER3;
                        _objBanpos.FATHER_HUSBANDNAME = _objBenposdata.FATHER_HUSBANDNAME;
                        _objBanpos.ADDRESSLINE1 = _objBenposdata.ADDRESSLINE1;
                        _objBanpos.ADDRESSLINE2 = _objBenposdata.ADDRESSLINE2;
                        _objBanpos.ADDRESSLINE3 = _objBenposdata.ADDRESSLINE3;
                        _objBanpos.ADDRESSLINE4 = _objBenposdata.ADDRESSLINE4;
                        _objBanpos.PINCODE = _objBenposdata.PINCODE;
                        _objBanpos.EMAILID = _objBenposdata.EMAILID;
                        _objBanpos.PHONENO = _objBenposdata.PHONENO;
                        _objBanpos.PANCARDNO = _objBenposdata.PANCARDNO;
                        _objBanpos.SecondHolderPAN_No = _objBenposdata.SecondHolderPAN_No;
                        _objBanpos.ThirdHolderPAN_No = _objBenposdata.ThirdHolderPAN_No;
                        _objBanpos.CATEGORY = _objBenposdata.CATEGORY;
                        _objBanpos.STATUS = _objBenposdata.STATUS;
                        _objBanpos.OCCUPATION = _objBenposdata.OCCUPATION;
                        _objBanpos.BANK_AC_NO = _objBenposdata.BANK_AC_NO;
                        _objBanpos.BANKNAME = _objBenposdata.BANKNAME;
                        _objBanpos.BANK_ADDRESS_LINE1 = _objBenposdata.BANK_ADDRESS_LINE1;
                        _objBanpos.BANK_ADDRESS_LINE2 = _objBenposdata.BANK_ADDRESS_LINE2;
                        _objBanpos.BANK_ADDRESS_LINE3 = _objBenposdata.BANK_ADDRESS_LINE3;
                        _objBanpos.BANK_ADDRESS_LINE4 = _objBenposdata.BANK_ADDRESS_LINE4;
                        _objBanpos.BANK_PINCODE = _objBenposdata.BANK_PINCODE;
                        _objBanpos.BANK_AC_TYPE = _objBenposdata.BANK_AC_TYPE;
                        _objBanpos.MICR_CODE = _objBenposdata.MICR_CODE;
                        _objBanpos.IFSC = _objBenposdata.IFSC;
                        _objBanpos.NOM_NAME = _objBenposdata.NOM_NAME;
                        _objBanpos.GAUARIDAN_NM = _objBenposdata.GAUARIDAN_NM;
                        _objBanpos.EntityId = _objBenposdata.EntityId;
                        _objBanpos.allotment_Date = _objBenposdata.allotment_Date;
                        _objBanpos.Createdby = _objBenposdata.Createdby;
                        _objBanpos.Createdon = DateTime.Now;
                        entities.BM_ShareholderListedEntityData.Add(_objBanpos);
                        entities.SaveChanges();
                        success = true;
                    }
                    else
                    {
                        CheckListedShareholdiingdtls.FOLIO_NO = _objBenposdata.FOLIO_NO;
                        CheckListedShareholdiingdtls.SHARE_HOLDER_NAME = _objBenposdata.SHARE_HOLDER_NAME;
                        CheckListedShareholdiingdtls.SHARES = _objBenposdata.SHARES;
                        CheckListedShareholdiingdtls.JOINT_HOLDER1 = _objBenposdata.JOINT_HOLDER1;
                        CheckListedShareholdiingdtls.JOINT_HOLDER2 = _objBenposdata.JOINT_HOLDER2;
                        CheckListedShareholdiingdtls.JOINT_HOLDER3 = _objBenposdata.JOINT_HOLDER3;
                        CheckListedShareholdiingdtls.FATHER_HUSBANDNAME = _objBenposdata.FATHER_HUSBANDNAME;
                        CheckListedShareholdiingdtls.ADDRESSLINE1 = _objBenposdata.ADDRESSLINE1;
                        CheckListedShareholdiingdtls.ADDRESSLINE2 = _objBenposdata.ADDRESSLINE2;
                        CheckListedShareholdiingdtls.ADDRESSLINE3 = _objBenposdata.ADDRESSLINE3;
                        CheckListedShareholdiingdtls.ADDRESSLINE4 = _objBenposdata.ADDRESSLINE4;
                        CheckListedShareholdiingdtls.PINCODE = _objBenposdata.PINCODE;
                        CheckListedShareholdiingdtls.EMAILID = _objBenposdata.EMAILID;
                        CheckListedShareholdiingdtls.PHONENO = _objBenposdata.PHONENO;
                        CheckListedShareholdiingdtls.PANCARDNO = _objBenposdata.PANCARDNO;
                        CheckListedShareholdiingdtls.SecondHolderPAN_No = _objBenposdata.SecondHolderPAN_No;
                        CheckListedShareholdiingdtls.ThirdHolderPAN_No = _objBenposdata.ThirdHolderPAN_No;
                        CheckListedShareholdiingdtls.CATEGORY = _objBenposdata.CATEGORY;
                        CheckListedShareholdiingdtls.STATUS = _objBenposdata.STATUS;
                        CheckListedShareholdiingdtls.OCCUPATION = _objBenposdata.OCCUPATION;
                        CheckListedShareholdiingdtls.BANK_AC_NO = _objBenposdata.BANK_AC_NO;
                        CheckListedShareholdiingdtls.BANKNAME = _objBenposdata.BANKNAME;
                        CheckListedShareholdiingdtls.BANK_ADDRESS_LINE1 = _objBenposdata.BANK_ADDRESS_LINE1;
                        CheckListedShareholdiingdtls.BANK_ADDRESS_LINE2 = _objBenposdata.BANK_ADDRESS_LINE2;
                        CheckListedShareholdiingdtls.BANK_ADDRESS_LINE3 = _objBenposdata.BANK_ADDRESS_LINE3;
                        CheckListedShareholdiingdtls.BANK_ADDRESS_LINE4 = _objBenposdata.BANK_ADDRESS_LINE4;
                        CheckListedShareholdiingdtls.BANK_PINCODE = _objBenposdata.BANK_PINCODE;
                        CheckListedShareholdiingdtls.BANK_AC_TYPE = _objBenposdata.BANK_AC_TYPE;
                        CheckListedShareholdiingdtls.MICR_CODE = _objBenposdata.MICR_CODE;
                        CheckListedShareholdiingdtls.IFSC = _objBenposdata.IFSC;
                        CheckListedShareholdiingdtls.NOM_NAME = _objBenposdata.NOM_NAME;
                        CheckListedShareholdiingdtls.GAUARIDAN_NM = _objBenposdata.GAUARIDAN_NM;
                        CheckListedShareholdiingdtls.EntityId = _objBenposdata.EntityId;
                        CheckListedShareholdiingdtls.Updateon = _objBenposdata.Createdby;
                        CheckListedShareholdiingdtls.allotment_Date = _objBenposdata.allotment_Date;
                        CheckListedShareholdiingdtls.Updatedby = DateTime.Now;

                        entities.SaveChanges();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        public bool CreateDmatData(BM_DematForm obj_BM_DematForm)
        {
            try
            {
                if (obj_BM_DematForm != null)
                {
                    entities.BM_DematForm.Add(obj_BM_DematForm);
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public List<BM_ShareholderListedEntityData> GetShareDetailsForListedCompay(int entityId, int customerId)
        {
            try
            {
                var _objgetListedCompany = (from row in entities.BM_ShareholderListedEntityData
                                            where row.EntityId == entityId
                                            select row).ToList();
                return _objgetListedCompany;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_DematForm> GetShareDetailsForDemateForm(int entityId, int customerId)
        {
            try
            {
                var _objGetDematList = (from row in entities.BM_DematForm where row.Entity_Id == entityId select row
                    ).ToList();
                return _objGetDematList;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public string getEntityName(int entity_Id)
        {
            try
            {
                var _objgetentityName = (from row in entities.BM_EntityMaster
                                         where row.Id == entity_Id && row.Is_Deleted == false
                                         select row.CompanyName

                                       ).FirstOrDefault();
                return _objgetentityName;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "server error occurred";
            }
        }

        #endregion

        public List<ClassesOfShare> GetDropDownClassesOfShares(int entityId)
        {
            List<ClassesOfShare> lstClassesOfShare = new List<ClassesOfShare>();
            try
            {
                var result = (from row in entities.BM_CapitalMaster
                              where row.Entity_Id == entityId && row.IsActive == true
                              select row).FirstOrDefault();

                if (result != null)
                {
                    if (result.TotNoAuthorizedCapita > 0 && result.TotamtAuthorizedCapita > 0)
                    {
                        var NominalValue = Math.Round(result.TotamtAuthorizedCapita / result.TotNoAuthorizedCapita, 4);
                        if (NominalValue > 0)

                            lstClassesOfShare.Add(new ClassesOfShare() { ClassesOfShareID = 1, ClassesOfShareName = "Equity Shares", NominalValuePerShare = NominalValue });
                    }

                    if (result.Pri_TotNoAuthorizedCapita > 0 && result.Pri_TotamtAuthorizedCapita > 0)
                    {
                        var NominalValue = Math.Round(result.Pri_TotamtAuthorizedCapita / result.Pri_TotNoAuthorizedCapita, 4);
                        if (NominalValue > 0)

                            lstClassesOfShare.Add(new ClassesOfShare() { ClassesOfShareID = 2, ClassesOfShareName = "Preference Shares", NominalValuePerShare = NominalValue });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }

            return lstClassesOfShare;
        }

        public decimal gettotalprefrenceCapital(int entityID, int customerId)
        {
            decimal totalPrefrence = 0;
            decimal paidupCapital = 0;
            decimal equietyPaidupCapital = 0;
            try
            {
                var gettotalprefrenceCapital = (from row in entities.BM_CapitalMaster
                                                //join rows in entities.BM_Share on row.Id equals rows.CapitalMasterId
                                                where row.Entity_Id == entityID && row.CustomerId == customerId
                                                && row.IsActive==true
                                                select row).FirstOrDefault();
                if (gettotalprefrenceCapital != null)
                {
                    equietyPaidupCapital  = gettotalprefrenceCapital.TotamtNoPaidupCapital;
                    if (gettotalprefrenceCapital.IsPrefrence)
                    {
                        paidupCapital = gettotalprefrenceCapital.Pri_TotamtNoPaidupCapital;
                    }
                    else
                    {
                        paidupCapital = 0;
                    }

                    totalPrefrence = paidupCapital + equietyPaidupCapital;
                }
                if (totalPrefrence > 0)
                {
                    return totalPrefrence;
                }
                else
                {
                    return totalPrefrence;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        #region created by Ruchi on 27th-jan-2021 paidup capital returns in amount
        public decimal gettotalpaidupcapitalinamCapital(int entityID, int customerId)
        {
            decimal totalPrefrence = 0;
            decimal paidupCapital = 0;
            decimal equietyPaidupCapital = 0;
            try
            {
                var gettotalprefrenceCapital = (from row in entities.BM_CapitalMaster
                                                //join rows in entities.BM_Share on row.Id equals rows.CapitalMasterId
                                                where row.Entity_Id == entityID 
                                                && row.CustomerId == customerId
                                                && row.IsActive==true
                                                select row).FirstOrDefault();
                if (gettotalprefrenceCapital != null)
                {
                    equietyPaidupCapital  = gettotalprefrenceCapital.TotamtNoPaidupCapital;
                    if (gettotalprefrenceCapital.IsPrefrence)
                    {
                        paidupCapital = gettotalprefrenceCapital.Pri_TotamtNoPaidupCapital;
                    }
                    else
                    {
                        paidupCapital = 0;
                    }

                    totalPrefrence = paidupCapital + equietyPaidupCapital;
                }
                if (totalPrefrence > 0)
                {
                    return totalPrefrence;
                }
                else
                {
                    return totalPrefrence;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        #endregion

        public bool DeleteShareHolding(int id, int customerId, int UserId)
        {
            bool success;
            try
            {
                var CheckShareHolding = (from row in entities.BM_ShareHolding
                                         where row.Id == id && row.Customer_Id == customerId
                                         select row).FirstOrDefault();
                if (CheckShareHolding != null)
                {
                    CheckShareHolding.IsActive = false;
                    CheckShareHolding.UpdatedOn = DateTime.Now;
                    CheckShareHolding.Updatedby = UserId;
                    entities.SaveChanges();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        public List<ShareHoldingDetails> GetShareholdingDetails(int sharesHoldingId, int customerId)
        {
            List<ShareHoldingDetails> objSharesDetails = new List<ShareHoldingDetails>();
            try
            {
                objSharesDetails = (from row in entities.BM_ShareHoldingDetails
                                    where row.ShareHoldingId == sharesHoldingId
                                     && row.IsActive == true
                                    select new ShareHoldingDetails
                                    {
                                        Id = row.Id,
                                        AllotmentNo = row.AllotmentNo,
                                        Dated = row.allotmentDate,
                                        NoOfShareTransfered = row.NoOfShareTransfered,
                                        DistinctiveFrom = row.DistinctiveFrom,
                                        DistinctiveTo = row.DistinctiveTo,
                                        FolioOfTransferor = row.FolioOfTransferor,
                                        NameOfTransferor = row.NameOfTransferor,
                                        DateOfIssued = row.DateOfIssued,
                                        CertificateNo = row.CertificateNo,
                                        LockInPeriod = row.LockInPeriod,
                                        PayableAmount = row.PayableAmount,
                                        PaidOrToBePaidAmount = row.PaidOrToBePaidAmount,
                                        DueAmount = row.DueAmount,
                                        Thereof = row.Thereof,
                                        DateOfTransfer = row.DateOfTransfer,
                                        BalanceShare = row.BalanceShare,
                                        DistinctiveFrom1 = row.DistinctiveFrom1,
                                        DistinctiveTo1 = row.DistinctiveTo1,
                                        FolioOfTransferee = row.FolioOfTransferee,
                                        NameOfTransferee = row.NameOfTransferee,
                                        NumberOfShares = row.NumberOfShares,
                                        Remarks = row.Remarks
                                    }).ToList();
                return objSharesDetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return objSharesDetails;
        }

        public ShareHoldingDetails AddShareholderDetails(ShareHoldingDetails item)
        {
            try
            {
                #region Check Constraints
                var entityId = (from row in entities.BM_ShareHolding
                                where row.Id == item.parentID && row.IsActive == true
                                select row.Entity_Id
                                    ).FirstOrDefault();

                //if (!string.IsNullOrEmpty(item.CertificateNo))
                //{
                //    var checkCeritificateExists = (from row in entities.BM_ShareHolding
                //                                   join details in entities.BM_ShareHoldingDetails on row.Id equals details.ShareHoldingId
                //                                   where row.Entity_Id == entityId && row.IsActive == true && details.IsActive == true && details.CertificateNo == item.CertificateNo
                //                                   select row.Id
                //                                   ).Any();

                //    if(checkCeritificateExists)
                //    {
                //        item.Error = true;
                //        item.Message = "Certificate number already exists.";
                //        return item;
                //    }

                //}

                var checkDistinctive = (from row in entities.BM_SP_CheckShareHoldingConstraint(entityId, item.Id, item.DistinctiveFrom, item.DistinctiveTo)
                                        select row.Id).Any();
                if(checkDistinctive)
                {
                    item.Error = true;
                    item.Message = "Distinctive numbers already exists";
                    return item;
                }
                #endregion

                BM_ShareHoldingDetails _bm_shareholdingdtls = new BM_ShareHoldingDetails();
                if (item.parentID > 0)
                    _bm_shareholdingdtls.ShareHoldingId = (int)item.parentID;
                _bm_shareholdingdtls.AllotmentNo = item.AllotmentNo;
                _bm_shareholdingdtls.allotmentDate = item.Dated;
                _bm_shareholdingdtls.NumberOfShares = item.NumberOfShares;
                _bm_shareholdingdtls.DistinctiveFrom = item.DistinctiveFrom;
                _bm_shareholdingdtls.DistinctiveTo = item.DistinctiveTo;
                _bm_shareholdingdtls.FolioOfTransferor = item.FolioOfTransferor;
                _bm_shareholdingdtls.NameOfTransferor = item.NameOfTransferor;
                _bm_shareholdingdtls.DateOfIssued = item.DateOfIssued;
                _bm_shareholdingdtls.CertificateNo = item.CertificateNo;
                _bm_shareholdingdtls.LockInPeriod = item.LockInPeriod;
                _bm_shareholdingdtls.PayableAmount = item.PayableAmount;
                _bm_shareholdingdtls.PaidOrToBePaidAmount = item.PaidOrToBePaidAmount;
                _bm_shareholdingdtls.DueAmount = item.DueAmount;
                _bm_shareholdingdtls.Thereof = item.Thereof;
                _bm_shareholdingdtls.DateOfTransfer = item.DateOfTransfer;
                _bm_shareholdingdtls.NoOfShareTransfered = item.NoOfShareTransfered;
                _bm_shareholdingdtls.DistinctiveFrom1 = item.DistinctiveFrom1;
                _bm_shareholdingdtls.DistinctiveTo1 = item.DistinctiveTo1;
                _bm_shareholdingdtls.FolioOfTransferee = item.FolioOfTransferee;
                _bm_shareholdingdtls.NameOfTransferee = item.NameOfTransferee;
                _bm_shareholdingdtls.BalanceShare = item.BalanceShare;
                _bm_shareholdingdtls.Remarks = item.Remarks;
                _bm_shareholdingdtls.Authentication_ = item.Authentication_;
                _bm_shareholdingdtls.IsActive = true;
                entities.BM_ShareHoldingDetails.Add(_bm_shareholdingdtls);
                entities.SaveChanges();
                item.Id = _bm_shareholdingdtls.Id;
                item.Success = true;
                item.Message = "Saved Successfully.";

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                item.Error = true;
                item.Message = "Server error occurred";

            }
            return item;
        }

        public ShareHoldingDetails UpdateShareholderDetails(ShareHoldingDetails item)
        {
            try
            {
                #region Check Constraints
                var entityId = (from row in entities.BM_ShareHolding
                                where row.Id == item.parentID && row.IsActive == true
                                select row.Entity_Id
                                    ).FirstOrDefault();

                //if (!string.IsNullOrEmpty(item.CertificateNo))
                //{
                //    var checkCeritificateExists = (from row in entities.BM_ShareHolding
                //                                   join details in entities.BM_ShareHoldingDetails on row.Id equals details.ShareHoldingId
                //                                   where row.Entity_Id == entityId && row.IsActive == true && details.IsActive == true && details.CertificateNo == item.CertificateNo &&
                //                                   details.Id != item.Id
                //                                   select row.Id
                //                                   ).Any();

                //    if (checkCeritificateExists)
                //    {
                //        item.Error = true;
                //        item.Message = "Certificate number already exists.";
                //        return item;
                //    }

                //}

                var checkDistinctive = (from row in entities.BM_SP_CheckShareHoldingConstraint(entityId, item.Id, item.DistinctiveFrom, item.DistinctiveTo)
                                        select row.Id).Any();
                if (checkDistinctive)
                {
                    item.Error = true;
                    item.Message = "Please Distinctive From and To already exists.";
                    return item;
                }
                #endregion

                var _bm_shareholdingdtls = (from x in entities.BM_ShareHoldingDetails where x.Id == item.Id && x.ShareHoldingId == item.parentID && x.IsActive == true select x).FirstOrDefault();
                if (_bm_shareholdingdtls != null)
                {
                    if (item.parentID > 0)
                        _bm_shareholdingdtls.ShareHoldingId = (int)item.parentID;
                    _bm_shareholdingdtls.AllotmentNo = item.AllotmentNo;
                    _bm_shareholdingdtls.allotmentDate = item.Dated;
                    _bm_shareholdingdtls.NumberOfShares = item.NumberOfShares;
                    _bm_shareholdingdtls.DistinctiveFrom = item.DistinctiveFrom;
                    _bm_shareholdingdtls.DistinctiveTo = item.DistinctiveTo;
                    _bm_shareholdingdtls.FolioOfTransferor = item.FolioOfTransferor;
                    _bm_shareholdingdtls.NameOfTransferor = item.NameOfTransferor;
                    _bm_shareholdingdtls.DateOfIssued = item.DateOfIssued;
                    _bm_shareholdingdtls.CertificateNo = item.CertificateNo;
                    _bm_shareholdingdtls.LockInPeriod = item.LockInPeriod;
                    _bm_shareholdingdtls.PayableAmount = item.PayableAmount;
                    _bm_shareholdingdtls.PaidOrToBePaidAmount = item.PaidOrToBePaidAmount;
                    _bm_shareholdingdtls.DueAmount = item.DueAmount;
                    _bm_shareholdingdtls.Thereof = item.Thereof;
                    _bm_shareholdingdtls.DateOfTransfer = item.DateOfTransfer;
                    _bm_shareholdingdtls.NoOfShareTransfered = item.NoOfShareTransfered;
                    _bm_shareholdingdtls.DistinctiveFrom1 = item.DistinctiveFrom1;
                    _bm_shareholdingdtls.DistinctiveTo1 = item.DistinctiveTo1;
                    _bm_shareholdingdtls.FolioOfTransferee = item.FolioOfTransferee;
                    _bm_shareholdingdtls.NameOfTransferee = item.NameOfTransferee;
                    _bm_shareholdingdtls.BalanceShare = item.BalanceShare;
                    _bm_shareholdingdtls.Remarks = item.Remarks;
                    _bm_shareholdingdtls.Authentication_ = item.Authentication_;
                    _bm_shareholdingdtls.IsActive = true;
                    entities.SaveChanges();
                    item.Success = true;
                    item.Message = "Updated Successfully.";
                }
                else
                {
                    item.Success = true;
                    item.Message = "Something went wrong.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                item.Error = true;
                item.Message = "Server error occurred";
            }
            return item;
        }

        public ShareHoldingDetails EditSHDetails(int id, int sharesHoldingId)
        {
            ShareHoldingDetails objgetdetails = new ShareHoldingDetails();
            try
            {
                objgetdetails = (from row in entities.BM_ShareHoldingDetails
                                 where row.ShareHoldingId == sharesHoldingId
                                 && row.Id == id
                                 && row.IsActive == true
                                 select new ShareHoldingDetails
                                 {
                                     Id = row.Id,
                                     parentID = sharesHoldingId,
                                     AllotmentNo = row.AllotmentNo,
                                     Dated = row.allotmentDate,
                                     NoOfShareTransfered = row.NoOfShareTransfered,
                                     DistinctiveFrom = row.DistinctiveFrom,
                                     DistinctiveTo = row.DistinctiveTo,
                                     FolioOfTransferor = row.FolioOfTransferor,
                                     NameOfTransferor = row.NameOfTransferor,
                                     DateOfIssued = row.DateOfIssued,
                                     CertificateNo = row.CertificateNo,
                                     LockInPeriod = row.LockInPeriod,
                                     PayableAmount = row.PayableAmount,
                                     PaidOrToBePaidAmount = row.PaidOrToBePaidAmount,
                                     DueAmount = row.DueAmount,
                                     Thereof = row.Thereof,
                                     DateOfTransfer = row.DateOfTransfer,
                                     BalanceShare = row.BalanceShare,
                                     DistinctiveFrom1 = row.DistinctiveFrom1,
                                     DistinctiveTo1 = row.DistinctiveTo1,
                                     FolioOfTransferee = row.FolioOfTransferee,
                                     NameOfTransferee = row.NameOfTransferee,
                                     NumberOfShares = row.NumberOfShares,
                                     Remarks = row.Remarks
                                 }).FirstOrDefault();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return objgetdetails;
        }

        public string DeleteShareHoldingDetails(int id)
        {
            string Message = string.Empty;
            try
            {
                var checkdetails = (from x in entities.BM_ShareHoldingDetails where x.Id == id && x.IsActive == true select x).FirstOrDefault();
                if (checkdetails != null)
                {
                    checkdetails.IsActive = false;
                    entities.SaveChanges();
                    Message = "Deleted successfully";
                }
                else
                {
                    Message = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                Message = "Server error occurred";
            }
            return Message;
        }

        public List<VMShareholding> GetPartnerDetails(int customerId, int EntityID)
        {
            List<VMShareholding> _objSharesDetails = new List<VMShareholding>();
            try
            {
                _objSharesDetails = (from row in entities.BM_SP_GetPartnerSharesDetails(EntityID, customerId)
                                     select new VMShareholding
                                     {
                                         designationId=row.designationId,
                                         DirectorId=row.DirectorId,
                                         Member_Name = row.PartnerName,
                                         DPIN = row.DIN,
                                         PAN = row.PAN,
                                         Tot_SharesHeld = row.Contribution_Obligation,
                                         perofHolding = row.PercentagesOfShareHolding,
                                         PartnerId = row.DirectorId,
                                         partnerShersID = row.sharesID,
                                         NatureofIntrest = row.Name,
                                         Designation = row.DesignationName,
                                         BCID = row.BodyCorporateID,
                                         BodyCorpAsPartnerId = row.BodyCorpAsPartnerId,
                                         Contribution_Obligation = row.Contribution_Obligation,
                                         Contribution_Received = row.Contribution_Received,
                                     }).ToList();
                if (_objSharesDetails != null)
                {
                    string BCN = "";
                    if (_objSharesDetails.Count > 0)
                    {
                        foreach (var item in _objSharesDetails)
                        {
                            if (item.Designation == "Nominee of Body Corporates")
                            {
                                if (item.BCID > 0)
                                {
                                    BCN = (from x in entities.BM_EntityMaster where x.Id == item.BCID select x.CompanyName).FirstOrDefault();
                                }
                                if (!string.IsNullOrEmpty(BCN))
                                {
                                    item.Designation = item.Designation + "(" + BCN + ")";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objSharesDetails;
        }
    }
}