﻿using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
   public interface ITask
    {
        IEnumerable<BM_MeetingAgendaMapping> GetAgendaList(int MeetingId);
        IEnumerable<TaskMeeting>  GetMeetingList(long EntityId, int loggedInUserID, string userRole);
        //IEnumerable<TaskMeeting> GetMeetingList(long EntityId);
        VMTask UpdateTask(VMTask vmtask);
        VMTask CreateTask(VMTask vmtask);
        Message DeleteTask(long id, int userId);
        VMTask CreateTaskAssinment(VMTask vmtask);
        List<VMTask> ReadData(int TaskId);
        List<VMTask> GetTaskDetails(int customerId,int UserId,int Role, bool? loadTask, bool? loadReview);
        VMTask gettaskbyId(int id,int userId,int customerId,string Role);
        VMTask UpdateAssigTask(VMTask _objentity);
        bool DownloadTaskFile(int id);
        bool ViewTaskFile(int id);
        List<TaskRemarks> GetPerformerDetails(int customerId, int userId, int taskId);
        VMTask UpdateTaskPerformer(VMTask vmtask);
        VMTask CreateTaskPerformer(VMTask vmtask);
        bool ExportExcel(byte[] fileBytes);
        List<BM_Task_SP_Status_Result> GetPerformerstatus(int customerId, int userId, int taskId);
        VMTask GetTask(int taskId);
        List<VMTask> GetTaskType(int taskId,int UserId);
        TaskTypeVM getTaskTypeDetails(int taskId);
        bool DownloadTaskFile1(int id);
        List<string> GetTaskFile(long id);
   
        List<TaskFile> GetTaskFileDetails(long taskId, int userId, int customerId);
        bool DeleteTask(long fileId);
        string getAgendatitle(int? agendaId);
        #region Agenda/Minutes review
        VMTask SaveTask(VMTask obj, int createdBy, int customerId, out string mailFormat);
        List<VMTask> GetTaskDetails(long meetingId, int entityId, int taskType);
        VMTask GetTaskDetails(long meetingId, int entityId, int taskId, int taskType);
        List<ReviewerRemark> GetReviewerTaskDetails(long taskId);
        List<ReviewerRemark> GetReviewerTaskDetails(long entityId, long meetingId, int taskType);
        ReviewerRemark GetAgendaMinutesReview(long taskId, long assignmentId, int userId, bool canAcceptChanges, bool allowFinalizeReview);
        ReviewerRemark SaveAgendaMinutesReview(ReviewerRemark obj, int userId, out string mailFormat);
        Message CloseAgendaMinutesReview(long taskId, long meetingId, int userId);
        #endregion

        #region Get Performer/Reviewer
        List<TaskAssignmentUserVM> GetUserListByTaskId(long taskId);
        List<TaskAssignmentUserVM> GetReviewerListByTaskAssignmentId(long taskAssignmentId);
        TaskAssignmentUserVM GetPerformerByTaskAssignmentId(long taskAssignmentId);
        #endregion

        #region Review Log
        List<ReviewTransactionVM> GetReviewLog(long taskId, int customerId);
        List<TimelineEventModel> GetReviewLogs(long taskId, int customerId);
        #endregion
    }
}
