﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System.Reflection;

namespace BM_ManegmentServices.Services.Masters
{
    public class PageAuthentication : IPageAuthentication
    {
        public IEnumerable<EntityMasterVM> GetEntityuserWise(int UserId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    //var _Entitylist = (from row in entities.BM_SP_EntityUser_wise(UserId)
                    //                   select new EntityMasterVM
                    //                   {
                    //                       Id = row.Id,
                    //                       EntityName = row.CompanyName
                    //                   }
                    //                          ).ToList();

                    return null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        public List<VM_pageAuthentication> GetPageAuthentication(int userId, int customerId, int EntityId, string Role)
        {
            List<VM_pageAuthentication> _pageAutenUserList = new List<VM_pageAuthentication>();
            try
            {

                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (Role == "SMNGT")
                    {
                        _pageAutenUserList = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, EntityId)
                                              where row.Role == Role
                                              select new VM_pageAuthentication
                                              {
                                                  UserId = userId,
                                                  parentId = row.ParentId,
                                                  Id = row.ID,
                                                  PageName = row.Name,
                                                  Editview = row.CanEdit,
                                                  DeleteView = row.CanDelete,
                                                  Addview = row.CanAdd,
                                                  Pageview = row.CanView,
                                              }).ToList();
                    }
                    else if (Role == "DRCTR")
                    {
                        _pageAutenUserList = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, EntityId)
                                              where row.Role == Role
                                              select new VM_pageAuthentication
                                              {
                                                  UserId = userId,
                                                  parentId = row.ParentId,
                                                  Id = row.ID,
                                                  PageName = row.Name,
                                                  Editview = row.CanEdit,
                                                  DeleteView = row.CanDelete,
                                                  Addview = row.CanAdd,
                                                  Pageview = row.CanView,
                                              }).ToList();
                    }
                    else if (Role == "CS")
                    {
                        _pageAutenUserList = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, EntityId)
                                              where row.Role != "SMNGT" && row.Role != "DRCTR"
                                              select new VM_pageAuthentication
                                              {
                                                  UserId = userId,
                                                  parentId = row.ParentId,
                                                  Id = row.ID,
                                                  PageName = row.Name,
                                                  Editview = row.CanEdit,
                                                  DeleteView = row.CanDelete,
                                                  Addview = row.CanAdd,
                                                  Pageview = row.CanView,
                                              }).ToList();
                    }
                    else
                    {
                        _pageAutenUserList = (from row in entities.BM_SP_GetUserWisePageAuthorization(userId, 0, 0, EntityId)
                                              where row.Role != "SMNGT" && row.Role != "DRCTR"
                                              select new VM_pageAuthentication
                                              {
                                                  UserId = userId,
                                                  parentId = row.ParentId,
                                                  Id = row.ID,
                                                  PageName = row.Name,
                                                  Editview = row.CanEdit,
                                                  DeleteView = row.CanDelete,
                                                  Addview = row.CanAdd,
                                                  Pageview = row.CanView,
                                              }).ToList();
                    }
                    return _pageAutenUserList;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        public List<VM_pageAuthentication> UpdateAuthentication(List<VM_pageAuthentication> newComponentTypes, int CustomerId, int loggedInUserID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    foreach (var item in newComponentTypes)
                    {
                        var checkexistAuthorization = (from row in entities.BM_PageAuthorizationMaster
                                                       where row.UserID == item.UserId
                                                       && row.PageID == item.Id
                                                       && row.IsActive == true
                                                       select row).FirstOrDefault();

                        if (checkexistAuthorization == null)
                        {
                            BM_PageAuthorizationMaster _objpageAuthentication = new BM_PageAuthorizationMaster();
                            _objpageAuthentication.CanAdd = item.Addview;
                            _objpageAuthentication.CanView = item.Pageview;
                            _objpageAuthentication.CanEdit = item.Editview;
                            _objpageAuthentication.CanDelete = item.DeleteView;
                            _objpageAuthentication.IsActive = true;
                            _objpageAuthentication.PageID = (int)item.Id;
                            _objpageAuthentication.UserID = item.UserId;
                            _objpageAuthentication.EntityID = item.EntityId;
                            _objpageAuthentication.CustomerID = 0;

                            _objpageAuthentication.CreatedBy = loggedInUserID;
                            _objpageAuthentication.CreatedOn = DateTime.Now;
                            //saveSuccess = SavePageAutorizedval(_objpageAuthentication);
                            entities.BM_PageAuthorizationMaster.Add(_objpageAuthentication);
                            entities.SaveChanges();
                        }
                        else
                        {
                            checkexistAuthorization.CanAdd = item.Addview;
                            checkexistAuthorization.CanView = item.Pageview;
                            checkexistAuthorization.CanEdit = item.Editview;
                            checkexistAuthorization.CanDelete = item.DeleteView;
                            checkexistAuthorization.IsActive = true;
                            checkexistAuthorization.PageID = (int)item.Id;
                            checkexistAuthorization.UserID = item.UserId;
                            checkexistAuthorization.EntityID = item.EntityId;
                            checkexistAuthorization.CustomerID = 0;

                            checkexistAuthorization.UpdatedBy = loggedInUserID;
                            checkexistAuthorization.UpdatedOn = DateTime.Now;

                            // saveSuccess = SavePageAutorizedval(_objpageAuthentication);
                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return newComponentTypes;
        }

    }
}