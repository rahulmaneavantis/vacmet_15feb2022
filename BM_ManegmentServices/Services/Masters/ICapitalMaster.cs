﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public interface ICapitalMaster
    {
        AuthorizedCapital SaveAuthorisedCapital(AuthorizedCapital objauhorizedCapital, int userID, int CustomerId);
        VMCapitalData AddEquietydtls(VMCapitalData obj, int userID, int CustomerId);
        //VMPrifrenscShare AddPrefrence(VMPrifrenscShare objPrefrence, int userID, int CustomerId);
        VMPrifrenscShare AddPrefrenceMultiple(VMPrifrenscShare objPrefrence, int userID, int CustomerId);
        VMUnclassified SaveUnClassified(VMUnclassified objUnclassified, int userID, int CustomerId);
        VMDebentures SaveDebengerdtls(VMDebentures objDebenture, int userID, int CustomerId);
        AuthorizedCapital GetAuthorisedCapital(int userID, int CustomerId, int entityId);
        List<VMllpCapital> GetCapitalLLPList(int Entity_Id);
        VMCapitalData GetEquityCapital(int userID, int v, int entityId, long authorisedId);
        List<Shares> GetEquityCapital_subdtls(int userID, int CustomerId, long equityId,int EntityId);
        VMPrifrenscShare GetPrifrenceCapital(int userID, int CustomerId, int entityId, long capitalID);
        List<pShareCouponRate> GetPrifrenceCapitalList(int userID, int CustomerId, int entityId, long capitalID);
        VMUnclassified GetUnclassified(int userID, int CustomerId, int entityId, long authorisedId);
        VMDebentures GetDebenger(int userID, int CustomerId, int entityId, long authorisedId);
        VMllpCapital AddLLpCapitalMaster(VMllpCapital _objllp, int customerId);
        VMllpCapital UpdateLLpCapitalMaster(VMllpCapital _objllp, int customerId);
        List<VMBodyCorporate> GetCapitalBodyCorporateList(int EntityId);
        VMBodyCorporate AddBodyCorporateCapitalMaster(VMBodyCorporate _objbodyCorporate, int customerId);
        string UpdatebodyCorporateCapitalMaster(VMBodyCorporate _objbodyCorporate, int customerId);
        VMllpCapital EditCapitalLLP(int Id, int EntityId);
        VMBodyCorporate EditCapitalBodyCorporate(int id, int entityId);
        IEnumerable<VMllpCapital> GetAllDINNumber(int customerId,int EntityId);
        VMBodyCorporate Update_BodyCorporateCapitalMaster(VMBodyCorporate _objbodycorporate, int customerId);
        string getEntityName(int entityId);
        //VMCapitalData UpdateEquietydtls(VMCapitalData objEquity, int userID, int customerId);

        string CheckValidCapital(decimal? currentValue, string CapitalType, long CapitalId);
        List<EntityCpitalDetails> GetEntityShareCapitaldetails(int EntityId);
    }
}
