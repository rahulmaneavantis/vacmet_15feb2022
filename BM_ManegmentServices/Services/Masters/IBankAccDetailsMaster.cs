﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IBankAccDetailsMaster
    {
        List<BankAccDetails_VM> GetBankDetails(int entityId);
        BankAccDetails_VM EditBankDetails(int Id, int entityId);
        BankAccDetails_VM AddBankDetails(BankAccDetails_VM _objadditionalcomp, int userId);
        BankAccDetails_VM UpdateBankDetails(BankAccDetails_VM _objadditionalcomp, int userId);
        bool DeleteAdditionalBank(int id, int userId);
        IEnumerable<BankTypeList_VM> GetBankType();
    }
}
