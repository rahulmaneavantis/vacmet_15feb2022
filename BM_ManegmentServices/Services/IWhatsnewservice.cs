﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.Services.Masters
{
    public interface IWhatsnewservice
    {
        WhatsNewVM Savewhatsnewdocument(IEnumerable<HttpPostedFileBase> files, int userId, WhatsNewVM _objdoc);
        List<WhatsNewVM> GetWhatsnewdata();
        bool DeleteWhatsNew(long id, int UserId);
    }
}