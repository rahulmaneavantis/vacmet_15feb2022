﻿using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Reports
{
    public interface IReportPolices
    {
        List<BM_ReportPolices> GetReportsDetails(int customerId, int userID, string role, long entityId);
        ReportVM uploadFile(ReportVM objreport);
        bool Downloadrecords(long id);
        string Viewdocument(long id);
        ReportVM GetReportPolices(int iD);
    }
}
