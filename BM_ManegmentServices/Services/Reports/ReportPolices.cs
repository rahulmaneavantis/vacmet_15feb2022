﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.Data;
using System.Reflection;
using BM_ManegmentServices.VM;
using System.IO;
using Ionic.Zip;
using System.Web.Hosting;
using System.Net;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.Services.Masters;

namespace BM_ManegmentServices.Services.Reports
{
    public class ReportPolices : IReportPolices
    {
        private Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public List<BM_ReportPolices> GetReportsDetails(int customerId, int userID, string role, long entityId)
        {
            try
            {
                var getreportdetails = (from row in entities.BM_ReportPolices where row.EntityId == entityId && row.CustomerId == customerId select row).ToList();
                return getreportdetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public ReportVM uploadFile(ReportVM objreport)
        {
            try
            {
                objreport.Response = new Response();
                string fileName = string.Empty;
                string directoryPath = string.Empty;

                var checkFileexist = (from row in entities.BM_ReportPolices
                                      where row.PolicesName == objreport.ReportName
                                      && row.EntityId == objreport.EntityId
                                      && row.IsActive == true
                                      select row).FirstOrDefault();

                if (checkFileexist == null)
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                    if (objreport.File.ContentLength > 0)
                    {
                        HttpPostedFileBase uploadfile1 = objreport.File;

                        directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/Document/" + objreport.CustomerId + "/Report/");

                        if (!Directory.Exists(directoryPath))
                            Directory.CreateDirectory(directoryPath);

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(objreport.File.FileName));
                        Stream fs = objreport.File.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                        fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                        SaveDocFiles(fileList);

                        BM_ReportPolices _objreportpolices = new BM_ReportPolices();
                        _objreportpolices.PolicesName = objreport.ReportName;
                        _objreportpolices.Filesize = objreport.File.ContentLength;
                        _objreportpolices.EntityId = objreport.EntityId;
                        _objreportpolices.CustomerId = objreport.CustomerId;
                        _objreportpolices.Createdon = DateTime.Now;
                        _objreportpolices.IsActive = true;
                        _objreportpolices.Createdby = objreport.UserId;
                        entities.BM_ReportPolices.Add(_objreportpolices);
                        entities.SaveChanges();

                        if (_objreportpolices.Id > 0)
                        {
                            BM_FileData objFileData = new BM_FileData();
                            objFileData.FileName = objreport.File.FileName;
                            objFileData.FileSize = Convert.ToString(objreport.File.ContentLength);
                            objFileData.FileKey = fileKey1.ToString();
                            objFileData.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            objFileData.ReportPolicesId = _objreportpolices.Id;
                            objFileData.IsDeleted = false;
                            objFileData.UploadedBy = objreport.UserId;
                            objFileData.UploadedOn = DateTime.Now;
                            entities.BM_FileData.Add(objFileData);
                            entities.SaveChanges();
                            objreport.Response.Success = true;
                            objreport.Response.Message = "Report save successfully";
                        }
                    }
                }
                else
                {
                    objreport.Response.Error = true;
                    objreport.Response.Message = "Report already exist";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                objreport.Response.Error = true;
                objreport.Response.Message = "Server error occured";
            }
            return objreport;
        }

        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(Masters.CryptographyHandler.AESEncrypt(file.Value));
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }

        #region Download Records
        public bool Downloadrecords(long id)
        {
            try
            {
                var CheckData = (from row in entities.BM_ReportPolices
                                 join rows in entities.BM_FileData on row.Id equals rows.ReportPolicesId
                                 where row.Id == id
                                 select rows).ToList();
                if (CheckData.Count > 0)
                {
                    bool IsDocumentDownload = downloadTaskData(CheckData);
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;

            }
        }

        public static bool downloadTaskData(List<BM_FileData> checkData)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile TaskDocument = new ZipFile())
                {
                    foreach (var files in checkData)
                    {
                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            documentSuccess = true;
                            int idx = files.FileName.LastIndexOf('.');
                            string str = files.FileName.Substring(0, idx) + "_" + "." + files.FileName.Substring(idx + 1);
                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                            if (!TaskDocument.ContainsEntry("Document" + "/" + str))
                                TaskDocument.AddEntry("Document" + "/" + str, Masters.CryptographyHandler.AESDecrypt(ReadDocFiles(filePath)));
                        }
                    }
                    if (documentSuccess)
                    {
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        TaskDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + "RecordsDocuments.zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }

        public string Viewdocument(long id)
        {
            string PolicesDocpath = "";

            var CheckData = (from row in entities.BM_ReportPolices
                             join rows in entities.BM_FileData on row.Id equals rows.ReportPolicesId
                             where row.Id == id
                             select rows).FirstOrDefault();

            if (CheckData != null)
            {
                string filePath = Path.Combine(HostingEnvironment.MapPath(CheckData.FilePath), CheckData.FileKey + Path.GetExtension(CheckData.FileName));
                if (CheckData.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles/Records";
                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + File;

                    string extension = System.IO.Path.GetExtension(filePath);

                    //Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(HostingEnvironment.MapPath(DateFolder));
                    }

                    //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = 36 + "" + 5 + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(HostingEnvironment.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(Masters.CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    bw.Close();

                    PolicesDocpath = FileName;
                    PolicesDocpath = PolicesDocpath.Substring(2, PolicesDocpath.Length - 2);

                }
            }
            return PolicesDocpath;
        }

        public ReportVM GetReportPolices(int iD)
        {
            ReportVM _objgetpolices = new ReportVM();
            try
            {
                _objgetpolices = (from x in entities.BM_ReportPolices
                                  where x.Id == iD
                                  && x.IsActive == true
                                  select new ReportVM
                                  {
                                      PolicesID = x.Id,
                                      EntityId = x.EntityId,
                                      ReportName = x.PolicesName
                                  }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objgetpolices;
        }

        #endregion Download Records
    }
}