﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Registers
{
    public interface IRegisterDetails
    {
        Entity getEntityDetails(int entityID);
        //List<Directors> getDirectorDetails(int entityID, int directorId,int CustomerId);
        List<Directors> GetDirectorKMPDetails(int entityID, int detailsofInterestID, int customerID);
        List<DirectorSecurity> getDirectorSecurityDetails(int directorId, int customerID,long EntityID);
        List<MemberRegister> GetMemberofRegisterDetails(long entityID, int memberId, int customerID);
        List<ChargeRegister> GetChargeRegisterDetails(long entityID, int memberId, int customerID);
        List<MemberSecurities> GetMemberSecurities(long entityID, int customerID, int memberId);
    }
}
