﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Registers
{
    public class RegisterDetails : IRegisterDetails
    {
        public Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        //public List<Directors> getDirectorDetails(int entityID, int directorId, int CustomerId)
        public List<Directors> GetDirectorKMPDetails(int entityID, int detailsofInterestID, int customerID)
        {
            try
            {
                var getDirectorDetailsforRegister = (from row in entities.BM_SP_DirectorDetails(entityID, detailsofInterestID, customerID)
                                                     select new Directors
                                                     {
                                                         DirectorId = row.DirectorId,
                                                         DIN = row.DIN,
                                                         FullName = row.DirectorFullName,
                                                         FatherName = row.FatherName,
                                                         MotherName = row.Mother,
                                                         //MotherName = (from r in entities.BM_Directors_Relatives where r.Director_Id == row.DirectorId && r.Relation == "Mother" select r.Name).FirstOrDefault(),
                                                         SpouseName = (from r in entities.BM_Directors_Relatives where r.Director_Id == row.DirectorId && r.Relation == "Spouse" select r.Name).FirstOrDefault(),
                                                         DateofBirth = row.DOB,
                                                         PresentAddress = row.PresentAddress,
                                                         ParmanentAddress = row.PermanentAddress,
                                                         //Nationalality = "Indian",
                                                         Nationalality = row.NationalityName,
                                                         Occupation = row.Occupation,
                                                         DateofCessation = row.cessiondateandReasons,
                                                         PAN = row.PAN!=null?row.PAN.ToUpper():"",
                                                         DateofBoardResolution = row.DateOfResolution,
                                                         DateofAppointmentandReappointment = row.DateOfAppointment,
                                                         IsDirector = row.IsDirector,
                                                         IsMNGT = row.IsMNGT,
                                                         ICSNo = row.CS_MemberNo
                                                     }).ToList();
                return getDirectorDetailsforRegister;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<DirectorSecurity> getDirectorSecurityDetails(int directorId, int customerID, long EntityId)
        {
            try
            {
                var getDirectorSecurities = (from row in entities.BM_SP_DirectorSecuritiesDetails(directorId, customerID, EntityId)
                                             select new DirectorSecurity
                                             {
                                                 CIN = row.CIN,
                                                 CompanyName = row.CompanyName,
                                                 NoofSecurities = row.securities_No,
                                                 DescriptionofSecurity = row.Dis_of_securities,
                                                 NominalvalueofSecurities = row.Nominam_valSecurities,
                                                 DateofAccusation = row.acquisition_Date != null ? Convert.ToDateTime(row.acquisition_Date).ToString("dd/MM/yyyy") : "",
                                                 PricePaidforaccusation = row.acquisition_pricePaid,
                                                 otherPricePaidforaccusation = row.Other_ConsiPaid_acquisition,
                                                 DateofDesposal = row.Date_disposal != null ? Convert.ToDateTime(row.Date_disposal).ToString("dd/MM/yyyy") : "",
                                                 Pricerecivefordisposal = row.Price_Received_disposal,
                                                 OtherPriceforDisposal = row.otherconsideration_disposal_RescPrice,
                                                 CumaltiveBalance = row.Cbal_No_securities_afterTransaction,
                                                 ModeofAccusation = row.Mode_of_acquisitionSecurities,
                                                 ModeOfHolding = row.DematerializedorPhysical,
                                                 SecurityHasbeenpledge = row.Securities_encumbrance
                                             }).ToList();

                getDirectorSecurities.ForEach(u => { (from y in entities.BM_SubEntityMapping where y.CIN == u.CIN select y).ToList(); });

                if (getDirectorSecurities.Count > 0)
                {
                    getDirectorSecurities.ForEach(u => { (from y in entities.BM_EntityMaster where y.CIN_LLPIN == u.CIN select y).ToList(); });
                }
                return getDirectorSecurities;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                List<DirectorSecurity> obj = new List<DirectorSecurity>();
                return obj;
            }
        }

        public Entity getEntityDetails(int entityID)
        {
            try
            {
                var result = new Entity() { EntityId = entityID};
                var getEntitydetailsbyId = (from row in entities.BM_EntityMaster
                                            from c in entities.Cities.Where(k => k.ID == row.Regi_CityId).Select( k=> k.Name).DefaultIfEmpty()
                                            from s in entities.States.Where(k => k.ID == row.Regi_StateId).Select(k => k.Name).DefaultIfEmpty()
                                            where row.Id == entityID
                                            select new
                                            {
                                                row.CompanyName,
                                                row.Regi_Address_Line1,
                                                row.Regi_Address_Line2,
                                                city = c,
                                                state = s,
                                                row.Regi_PINCode
                                            }).FirstOrDefault();

                if(getEntitydetailsbyId != null)
                {
                    result.EntityName = getEntitydetailsbyId.CompanyName;
                    var address = (string.IsNullOrEmpty(getEntitydetailsbyId.Regi_Address_Line1) ? "" : getEntitydetailsbyId.Regi_Address_Line1) +
                        (string.IsNullOrEmpty(getEntitydetailsbyId.Regi_Address_Line2) ? "" : ", " +getEntitydetailsbyId.Regi_Address_Line2) +
                        (string.IsNullOrEmpty(getEntitydetailsbyId.state) ? "" : " " + getEntitydetailsbyId.state) +
                        (string.IsNullOrEmpty(getEntitydetailsbyId.city) ? "" : " " +getEntitydetailsbyId.city) +
                        (string.IsNullOrEmpty(getEntitydetailsbyId.Regi_PINCode) ? "" : " " +getEntitydetailsbyId.Regi_PINCode);

                    result.Address = address;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #region Register of Member
        public List<MemberRegister> GetMemberofRegisterDetails(long entityID, int memberId, int customerID)
        {
            try
            {
                var getMemberOFRegisterDetails = (from row in entities.BM_SP_RegisterofMember(memberId, entityID, customerID)
                                                  select new MemberRegister
                                                  {
                                                      MemberId = (int)row.Id,
                                                      EntityName = row.CompanyName,
                                                      EntityAddress = row.CompanyAddress,
                                                      FolioNumber = row.Follio_No,
                                                      ClassofShares = row.ClassofShares,
                                                      Nominalvaluepershares = row.NominaVal_per_Shares,
                                                      TotalSharesHeld = row.Tot_SharesHeld,
                                                      MemberName = row.MemberName,
                                                      JointHolderName = row.JointHolder_Name,
                                                      Address = row.bodycorporate_Address,
                                                      EmailId = row.Email_Id,
                                                      CIN = row.CIN,
                                                      UIN = "",
                                                      Father_Mother_SpouseName = row.Father_Mother_Spouse_Name,
                                                      Status = row.Status,
                                                      PAN = row.PAN!=null?row.PAN.ToUpper():"",
                                                      GuardianName = row.Guardian_Name,
                                                      DateofBirth = row.DOB_minor != null ? Convert.ToDateTime(row.DOB_minor).ToString("dd-MM-yyyy") : "",
                                                      DateofBecomingMember = row.becomingmember_Date != null ? Convert.ToDateTime(row.becomingmember_Date).ToString("dd-MM-yyyy") : "",
                                                      Dateunder89 = row.declaration_Date_under89 != null ? Convert.ToDateTime(row.declaration_Date_under89).ToString("dd-MM-yyyy") : "",
                                                      BenificialNameandAddress = row.Name_and_Address_ben,
                                                      DateofNomination = row.Nomination_Date_Receipt != null ? Convert.ToDateTime(row.Nomination_Date_Receipt).ToString("dd-MM-yyyy") : "",
                                                      NameandAddressofNomminee = row.NomineeName_Address,
                                                      sharesinAbayes = row.shares_abeyance,
                                                      DateofCessation = row.Cessation_Membership_Date != null ? Convert.ToDateTime(row.Cessation_Membership_Date).ToString("dd-MM-yyyy") : "",
                                                      Occupation = row.Occupation,
                                                      Nationality = row.Nationality
                                                  }).ToList();
                return getMemberOFRegisterDetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<MemberSecurities> GetMemberSecurities(long entityID, int customerID, int memberId)
        {
            try
            {
                var getMemberOfSecurities = (from row in entities.BM_SP_MemberSecurityDetails(entityID, memberId, customerID)
                                             select new MemberSecurities
                                             {
                                                 AllotmentNo = row.AllotmentNo,
                                                 DateofAllotment = row.allotmentDate != null ? Convert.ToDateTime(row.allotmentDate).ToString("dd-MM-yyyy") : "",
                                                 NumberofShares = row.NumberOfShares,
                                                 DistinctiveNoofShares_From = row.DistinctiveFrom,
                                                 DistinctiveNoofShares_To = row.DistinctiveTo,
                                                 TransferorFolioNo = row.FolioOfTransferor,
                                                 NameOfTransferror=row.NameOfTransferor,
                                                 DateofIssues = row.DateOfIssued != null ? Convert.ToDateTime(row.DateOfIssued).ToString("dd-MM-yyyy") : "",
                                                 CertificateNumber = row.CertificateNo,
                                                 LockinPeriod = row.LockInPeriod,
                                                 AmountPayable = row.PayableAmount,
                                                 AmountPaid = row.PaidOrToBePaidAmount,
                                                 AmountDue = row.DueAmount,
                                                 DescofConsidration = row.Thereof,
                                                 DateofTransfer = row.DateOfTransfer != null ? Convert.ToDateTime(row.DateOfTransfer).ToString("dd-MM-yyyy") : "",
                                                 NoofSharesTransfered = row.NoOfShareTransfered,
                                                 DistinctiveNo_Form = row.DistinctiveFrom1,
                                                 DistinctiveNo_To = row.DistinctiveTo1,
                                                 TransfreeFolio = row.FolioOfTransferee,
                                                 NameofTransfaree = row.NameOfTransferee,
                                                 BalanceofSharesHeld = row.BalanceShare,
                                                 Remark = row.Remarks,
                                                 Authentication = row.Authentication_
                                             }).ToList();
                return getMemberOfSecurities;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        //Added for Charge Register 28 Jul 2021
        #region Register of Charge
        public List<ChargeRegister> GetChargeRegisterDetails(long entityID, int ChargeHeaderId, int customerID)
        {
            try
            {
                var getChargeRegisterDetails = (from row in entities.BM_SP_RegisterofCharge(ChargeHeaderId, entityID, customerID)
                                                select new ChargeRegister
                                                {
                                                    Charge_Id = row.Charge_Id,
                                                    Entity_Id = row.Entity_Id,
                                                    ChargeTypeId = row.Charge_TypeId,
                                                    ChargeIdMannual = row.ChargeIdMannual,
                                                    Charge_TypeDesc = row.Charge_TypeDesc,
                                                    ChargeAmount = row.Charge_Amount,
                                                    CreationDate = row.Charge_CreationDate,
                                                    RegistrationChargeCreateDate = row.Registration_ChargeCreateDate,
                                                    ModificationDate = row.Charge_Modification_Date,
                                                    RegistrationModificationDate = row.Registration_ChargeModificationDate,
                                                    SatisfactionDate = row.Satisfaction_Date,
                                                    InFavour = row.In_Favour,
                                                    
                                                    ShortDescPropertyCharged = row.ShortDesc_PropertyCharged,
                                                    Namesaddresseschargeholder = row.Names_addresses_chargeholder,
                                                    TermsconditionsOfcharge = row.Terms_conditionsOfcharge,
                                                    Descinstrument = row.Desc_instrument,

                                                    Descinstrumentchargemodify = row.Desc_instrument_chargemodify,
                                                    Particularsmodification = row.Particulars_modification,
                                                    RegistrationsatisfactionDate = row.Registration_satisfactionDate,
                                                    FactsDelaycondonationDate = row.Facts_Delaycondonation_Date,
                                                    Reasonsdelayfiling = row.Reasons_delay_filing,
                                                    Status = row.Status
                                                }).ToList(); 
                return getChargeRegisterDetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        
        #endregion


    }
}