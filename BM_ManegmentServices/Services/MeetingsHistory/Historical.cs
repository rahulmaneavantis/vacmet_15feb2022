﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using BM_ManegmentServices.Data;
using System.IO;
using System.Web.Hosting;
using com.VirtuosoITech.ComplianceManagement.Business;
using Ionic.Zip;
using System.Net;

namespace BM_ManegmentServices.Services.MeetingsHistory
{
    public class Historical : IHistorical
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public VM_HistoricalMeetings CreateHistoricalData(VM_HistoricalMeetings _objHistoricalMeetings, int customerId, int userId)
        {
            int? NumberofMeetings = 0;
            try
            {
                int i = 0;
                if (_objHistoricalMeetings.MeetingNo > 0)
                {
                    NumberofMeetings = _objHistoricalMeetings.MeetingNo;
                }
                else
                {
                    NumberofMeetings = 1;
                }

                for (i = 1; i <= NumberofMeetings; i++)
                {
                    if (_objHistoricalMeetings.MeetingTypeId != null)
                    {
                        if (_objHistoricalMeetings.MeetingTypeId.Count > 0 && _objHistoricalMeetings.Circular_Meeting != "O")
                        {
                            foreach (var item in _objHistoricalMeetings.MeetingTypeId)
                            {
                                if (NumberofMeetings > 1)
                                {
                                    BM_HistoricalData _objdata = new BM_HistoricalData();

                                    _objdata.Meeting_Circular = _objHistoricalMeetings.Circular_Meeting;

                                    _objdata.EntityId = _objHistoricalMeetings.EntityId;
                                    _objdata.MeetingTypeID = item;

                                    _objdata.FY = _objHistoricalMeetings.FYID;
                                    _objdata.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;
                                    _objdata.MeetingDate = _objHistoricalMeetings.MeetingDate;

                                    _objdata.IsActive = true;
                                    _objdata.CustomerId = customerId;
                                    _objdata.CreatedOn = DateTime.Now;
                                    _objdata.CreatedBy = userId;

                                    entities.BM_HistoricalData.Add(_objdata);
                                    entities.SaveChanges();

                                    _objHistoricalMeetings.ID = _objdata.ID;
                                    _objHistoricalMeetings.Success = true;
                                    _objHistoricalMeetings.Message = "Saved successfully";
                                }
                                else
                                {
                                    var checkdata = (from row in entities.BM_HistoricalData
                                                     where row.IsActive == true
                                                        && row.EntityId == _objHistoricalMeetings.EntityId
                                                        && row.MeetingTypeID == item
                                                        && row.FY == _objHistoricalMeetings.FYID
                                                        && row.CustomerId == customerId
                                                     select row).FirstOrDefault();
                                    if(checkdata!=null && _objHistoricalMeetings.MeetingSRNo>0)
                                    {
                                        checkdata= (from row in entities.BM_HistoricalData
                                                    where row.IsActive == true
                                                       && row.EntityId == _objHistoricalMeetings.EntityId
                                                       && row.MeetingTypeID == item
                                                       && row.FY == _objHistoricalMeetings.FYID
                                                       && row.MeetingSrNo== _objHistoricalMeetings.MeetingSRNo
                                                       && row.CustomerId == customerId
                                                       && row.Meeting_Circular== _objHistoricalMeetings.Circular_Meeting
                                                    select row).FirstOrDefault();
                                    }

                                    if (checkdata == null)
                                    {
                                        BM_HistoricalData _objdata = new BM_HistoricalData();

                                        _objdata.Meeting_Circular = _objHistoricalMeetings.Circular_Meeting;

                                        _objdata.EntityId = _objHistoricalMeetings.EntityId;
                                        _objdata.MeetingTypeID = item;

                                        _objdata.FY = _objHistoricalMeetings.FYID;
                                        _objdata.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;
                                        _objdata.MeetingDate = _objHistoricalMeetings.MeetingDate;

                                        _objdata.IsActive = true;
                                        _objdata.CustomerId = customerId;
                                        _objdata.CreatedOn = DateTime.Now;
                                        _objdata.CreatedBy = userId;

                                        entities.BM_HistoricalData.Add(_objdata);
                                        entities.SaveChanges();

                                        _objHistoricalMeetings.ID = _objdata.ID;
                                        _objHistoricalMeetings.Success = true;
                                        _objHistoricalMeetings.Message = "Saved successfully";
                                    }
                                    else
                                    {
                                        _objHistoricalMeetings.Error = true;
                                        _objHistoricalMeetings.Message = "Same record already exists";
                                    }
                                }
                            }
                        }
                        else
                        {
                            var checkdata = (from row in entities.BM_HistoricalData
                                             where row.EntityId == _objHistoricalMeetings.EntityId
                                             && row.FY == _objHistoricalMeetings.FYID
                                             && row.CustomerId == customerId
                                             && row.IsActive == true
                                             select row).FirstOrDefault();

                            if (checkdata == null)
                            {
                                BM_HistoricalData _objdata = new BM_HistoricalData();
                                _objdata.EntityId = _objHistoricalMeetings.EntityId;
                                // _objdata.MeetingTypeID = item;
                                _objdata.Meeting_Circular = _objHistoricalMeetings.Circular_Meeting;
                                _objdata.IsActive = true;
                                _objdata.FY = _objHistoricalMeetings.FYID;
                                _objdata.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;
                                _objdata.CustomerId = customerId;
                                _objdata.CreatedOn = DateTime.Now;
                                _objdata.CreatedBy = userId;
                                _objdata.MeetingDate = _objHistoricalMeetings.MeetingDate;
                                entities.BM_HistoricalData.Add(_objdata);
                                entities.SaveChanges();
                                _objHistoricalMeetings.ID = _objdata.ID;
                                _objHistoricalMeetings.Success = true;
                                _objHistoricalMeetings.Message = "Saved successfully";
                            }
                            else
                            {
                                _objHistoricalMeetings.Error = true;
                                _objHistoricalMeetings.Message = "Same record already exists";
                            }
                        }
                    }
                    else
                    {
                        var checkdataforothers = (from row in entities.BM_HistoricalData
                                                  where row.EntityId == _objHistoricalMeetings.EntityId
                                                  && row.CustomerId == customerId
                                                  && row.FY== _objHistoricalMeetings.FYID
                                                  && row.Meeting_Circular== _objHistoricalMeetings.Circular_Meeting
                                                  && row.IsActive == true
                                                  select row).FirstOrDefault();

                        if (checkdataforothers == null)
                        {
                            BM_HistoricalData _objdata = new BM_HistoricalData();
                            _objdata.EntityId = _objHistoricalMeetings.EntityId;
                            // _objdata.MeetingTypeID = item;
                            _objdata.Meeting_Circular = _objHistoricalMeetings.Circular_Meeting;

                            _objdata.FY = _objHistoricalMeetings.FYID;
                            _objdata.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;
                            _objdata.MeetingDate = _objHistoricalMeetings.MeetingDate;

                            _objdata.IsActive = true;
                            _objdata.CustomerId = customerId;
                            _objdata.CreatedOn = DateTime.Now;
                            _objdata.CreatedBy = userId;

                            entities.BM_HistoricalData.Add(_objdata);
                            entities.SaveChanges();

                            _objHistoricalMeetings.ID = _objdata.ID;
                            _objHistoricalMeetings.Success = true;
                            _objHistoricalMeetings.Message = "Saved Successfully";
                        }
                        else
                        {
                            _objHistoricalMeetings.Error = true;
                            _objHistoricalMeetings.Message = "Same record already exists";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objHistoricalMeetings.Error = true;
                _objHistoricalMeetings.Message = SecretarialConst.Messages.serverError;
            }
            return _objHistoricalMeetings;
        }

        //public VM_HistoricalMeetings CreateHistoricalData(VM_HistoricalMeetings _objHistoricalMeetings, int customerId, int userId)
        //{
        //    int NumberofMeetings = 0;
        //    try
        //    {
        //        int i = 0;
        //        if (_objHistoricalMeetings.MeetingNo > 0)
        //        {
        //            NumberofMeetings = _objHistoricalMeetings.MeetingNo;
        //        }
        //        else
        //        {
        //            NumberofMeetings = 1;
        //        }

        //        for (i = 1; i <= NumberofMeetings; i++)
        //        {
        //            if (_objHistoricalMeetings.MeetingTypeId != null)
        //            {
        //                if (_objHistoricalMeetings.MeetingTypeId.Count > 0 && _objHistoricalMeetings.Circular_Meeting != "O")
        //                {
        //                    foreach (var item in _objHistoricalMeetings.MeetingTypeId)
        //                    {
        //                        var checkdata = (from row in entities.BM_HistoricalData
        //                                         where row.IsActive == true
        //                                            && row.EntityId == _objHistoricalMeetings.EntityId
        //                                            && row.MeetingTypeID == item
        //                                            && row.FY == _objHistoricalMeetings.FYID
        //                                            && row.CustomerId == customerId
        //                                         select row).FirstOrDefault();

        //                        if (checkdata == null)
        //                        {
        //                            BM_HistoricalData _objdata = new BM_HistoricalData();
        //                            _objdata.EntityId = _objHistoricalMeetings.EntityId;
        //                            _objdata.MeetingTypeID = item;
        //                            _objdata.Meeting_Circular = _objHistoricalMeetings.Circular_Meeting;
        //                            _objdata.IsActive = true;
        //                            _objdata.FY = _objHistoricalMeetings.FYID;
        //                            _objdata.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;
        //                            _objdata.MeetingDate = _objHistoricalMeetings.MeetingDate;
        //                            _objdata.CustomerId = customerId;
        //                            _objdata.CreatedOn = DateTime.Now;
        //                            _objdata.CreatedBy = userId;

        //                            entities.BM_HistoricalData.Add(_objdata);
        //                            entities.SaveChanges();

        //                            _objHistoricalMeetings.ID = _objdata.ID;
        //                            _objHistoricalMeetings.Success = true;
        //                            _objHistoricalMeetings.Message = "Saved successfully";

        //                        }
        //                        else
        //                        {
        //                            _objHistoricalMeetings.Error = true;
        //                            _objHistoricalMeetings.Message = "Data already exist";
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    var checkdata = (from row in entities.BM_HistoricalData
        //                                     where
        //                                    row.IsActive == true
        //                                    && row.EntityId == _objHistoricalMeetings.EntityId
        //                                    // && row.MeetingTypeID == item
        //                                    && row.FY == _objHistoricalMeetings.FYID
        //                                    && row.CustomerId == customerId
        //                                     select row).FirstOrDefault();
        //                    if (checkdata == null)
        //                    {
        //                        BM_HistoricalData _objdata = new BM_HistoricalData();
        //                        _objdata.EntityId = _objHistoricalMeetings.EntityId;
        //                        // _objdata.MeetingTypeID = item;
        //                        _objdata.Meeting_Circular = _objHistoricalMeetings.Circular_Meeting;
        //                        _objdata.IsActive = true;
        //                        _objdata.FY = _objHistoricalMeetings.FYID;
        //                        _objdata.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;
        //                        _objdata.CustomerId = customerId;
        //                        _objdata.CreatedOn = DateTime.Now;
        //                        _objdata.CreatedBy = userId;
        //                        _objdata.MeetingDate = _objHistoricalMeetings.MeetingDate;
        //                        entities.BM_HistoricalData.Add(_objdata);
        //                        entities.SaveChanges();
        //                        _objHistoricalMeetings.ID = _objdata.ID;
        //                        _objHistoricalMeetings.Success = true;
        //                        _objHistoricalMeetings.Message = "Saved successfully";
        //                    }
        //                    else
        //                    {
        //                        _objHistoricalMeetings.Error = true;
        //                        _objHistoricalMeetings.Message = "Data already exist";
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                var checkdataforothers = (from row in entities.BM_HistoricalData
        //                                          where
        //                                             row.IsActive == true
        //                                             && row.EntityId == _objHistoricalMeetings.EntityId
        //                                             //&& row.MeetingTypeID == item
        //                                             //&& row.FY == _objHistoricalMeetings.FYID
        //                                             && row.CustomerId == customerId
        //                                          select row).FirstOrDefault();
        //                if (checkdataforothers == null)
        //                {
        //                    BM_HistoricalData _objdata = new BM_HistoricalData();
        //                    _objdata.EntityId = _objHistoricalMeetings.EntityId;
        //                    // _objdata.MeetingTypeID = item;
        //                    _objdata.Meeting_Circular = _objHistoricalMeetings.Circular_Meeting;
        //                    _objdata.IsActive = true;
        //                    _objdata.FY = _objHistoricalMeetings.FYID;
        //                    _objdata.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;
        //                    _objdata.CustomerId = customerId;
        //                    _objdata.MeetingDate = _objHistoricalMeetings.MeetingDate;
        //                    _objdata.CreatedOn = DateTime.Now;
        //                    _objdata.CreatedBy = userId;
        //                    entities.BM_HistoricalData.Add(_objdata);
        //                    entities.SaveChanges();
        //                    _objHistoricalMeetings.ID = _objdata.ID;
        //                    _objHistoricalMeetings.Success = true;
        //                    _objHistoricalMeetings.Message = "Saved Successfully";
        //                }
        //                else
        //                {
        //                    _objHistoricalMeetings.Error = true;
        //                    _objHistoricalMeetings.Message = "Data already exist";
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        _objHistoricalMeetings.Error = true;
        //        _objHistoricalMeetings.Message = "server error occured";
        //    }
        //    return _objHistoricalMeetings;
        //}

        public List<VM_HistoricalMeetings> GetHistoricalData(int customerID, int userID, string roleCode)
        {
            List<VM_HistoricalMeetings> _objgetHisdata = new List<VM_HistoricalMeetings>();
            try
            {
                _objgetHisdata = (from x in entities.BM_SP_GetHistoricalData(customerID, userID, roleCode)
                                  select new VM_HistoricalMeetings
                                  {
                                      ID = x.MeetingId,
                                      MeetingTypeName = x.MeetingTypeName,
                                      Circular_Meeting = x.MeetingCircular,
                                      EntityId = x.EntityId,
                                      EntityName = x.EntityName,
                                      FYID = x.FYID,
                                      FY = x.FYText,
                                      MeetingSRNo = x.MeetingSrNo,
                                      //FileTagging=(from y in entities.BM_HistoricalFileTagMapping where y.HistoricalID==x.MeetingId && y.IsActive==true select y.TaggInputes).ToList(),
                                      MeetingDate = x.MeetingDate,
                                  }).ToList();

                if (_objgetHisdata.Count > 0)
                {
                    foreach (var item in _objgetHisdata)
                    {
                        var getfile = (from y in entities.BM_HistoricalFileTagMapping 
                                       where y.HistoricalID == item.ID 
                                       && y.IsActive == true 
                                       select new VM_HistoricalMeetings { FileTags = y.TaggInputes }).ToList();

                        if (getfile.Count > 0)
                        {
                            item.FileTags = string.Join<string>(",", getfile.Select(p => p.FileTags));
                        }
                        else
                        {
                            item.FileTags = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objgetHisdata;
        }

        public VM_HistoricalMeetings GetHistoricalDataByID(int iD)
        {
            VM_HistoricalMeetings _objdata = new VM_HistoricalMeetings();
            try
            {
                _objdata = (from x in entities.BM_HistoricalData
                            where x.ID == iD
                            && x.IsActive==true
                            select new VM_HistoricalMeetings
                            {
                                ID = x.ID,
                                Circular_Meeting = x.Meeting_Circular,
                                EntityId = x.EntityId,
                                MeetingTypeId = (from y in entities.BM_HistoricalData where y.ID == x.ID select y.MeetingTypeID).ToList(),
                                FYID = x.FY,
                                MeetingDate = x.MeetingDate,
                                MeetingSRNo = x.MeetingSrNo,     
                                IsOther = x.Meeting_Circular == "O" ? false : true,                                
                                StartMeetingTime = x.StartMeetingTime,
                                EndMeetingTime = x.EndMeetingTime,
                            }).FirstOrDefault();

                if (_objdata != null)
                {
                    if (_objdata.Circular_Meeting == "O")
                    {
                        _objdata.IsComcheck = true;
                        _objdata.Isdefaultcheck = false;
                    }
                    else
                    {
                        _objdata.Isdefaultcheck = true;
                        _objdata.IsComcheck = false;
                    }

                    var getfile = (from y in entities.BM_HistoricalFileTagMapping 
                                   where y.HistoricalID == _objdata.ID 
                                   && y.IsActive == true 
                                   select new VM_HistoricalMeetings { FileTags = y.TaggInputes }).ToList();

                    if (getfile.Count > 0)
                    {
                        _objdata.FileTags = string.Join<string>(",", getfile.Select(p => p.FileTags));
                    }
                    else
                    {
                        _objdata.FileTags = "";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objdata;

        }

        public List<BM_SP_GetHistoricalFileData_Result> GetHistoricalFileData(int customerId, int HistoricalID, string status)
        {
            List<BM_SP_GetHistoricalFileData_Result> _objFile = new List<BM_SP_GetHistoricalFileData_Result>();
            try
            {
                _objFile = (from x in entities.BM_SP_GetHistoricalFileData(customerId)
                            where x.ID == HistoricalID
                            && x.DocType == status
                            select x).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objFile;
        }

        public VM_HistoricalMeetings UpdateHistoricalData(VM_HistoricalMeetings _objHistoricalMeetings, int customerId, int userId)
        {
            VM_HistoricalMeetings _objHisData = new VM_HistoricalMeetings();
            try
            {
                if (_objHistoricalMeetings.MeetingTypeId != null)
                {
                    foreach (var item in _objHistoricalMeetings.MeetingTypeId)
                    {
                        bool srNumberExists = false;
                        if (_objHistoricalMeetings.MeetingSRNo != null)
                        {
                            var prevRecordExists = (from x in entities.BM_HistoricalData
                                                    where x.CustomerId == customerId
                                                       && x.Meeting_Circular == _objHistoricalMeetings.Circular_Meeting
                                                       && x.MeetingTypeID == item
                                                       && x.EntityId == _objHistoricalMeetings.EntityId
                                                       && x.MeetingSrNo == _objHistoricalMeetings.MeetingSRNo
                                                       && x.FY== _objHistoricalMeetings.FYID
                                                       && x.IsActive==true
                                                    select x).FirstOrDefault();

                            if (prevRecordExists != null)
                            {
                                if (prevRecordExists.ID != _objHistoricalMeetings.ID)
                                    srNumberExists = true;
                            }

                        }

                        if (!srNumberExists)
                        {
                            var prevRecord = (from x in entities.BM_HistoricalData
                                              where x.CustomerId == customerId
                                                 && x.ID == _objHistoricalMeetings.ID
                                                 && x.IsActive==true
                                              //&& x.EntityId == _objHistoricalMeetings.EntityId
                                              //&& x.MeetingSrNo == _objHistoricalMeetings.MeetingSRNo
                                              select x).FirstOrDefault();

                            if (prevRecord != null)
                            {
                                prevRecord.Meeting_Circular = _objHistoricalMeetings.Circular_Meeting;
                                prevRecord.EntityId = _objHistoricalMeetings.EntityId;
                                prevRecord.MeetingTypeID = item;
                                prevRecord.FY = _objHistoricalMeetings.FYID;
                                prevRecord.MeetingDate = _objHistoricalMeetings.MeetingDate;
                                prevRecord.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;

                                prevRecord.StartMeetingTime = _objHistoricalMeetings.StartMeetingTime;
                                prevRecord.EndMeetingTime = _objHistoricalMeetings.EndMeetingTime;
                                //checkhisdata.UploadedType = _objHistoricalMeetings.status;
                                entities.SaveChanges();

                                _objHistoricalMeetings.Success = true;
                                _objHistoricalMeetings.Message = SecretarialConst.Messages.updateSuccess;
                            }

                            #region Upload Document

                            if (_objHistoricalMeetings.files != null && prevRecord.ID > 0)
                            {
                                if (_objHistoricalMeetings.files.Count() > 0)
                                {
                                    bool savechange = FileUpload.SaveHistoricalFile(_objHistoricalMeetings.files, prevRecord.ID, userId, customerId, _objHistoricalMeetings.status);
                                }
                            }

                            #endregion

                            #region Add Tagging

                            if (_objHistoricalMeetings.FileTags != null)
                            {
                                string Tags = Convert.ToString(_objHistoricalMeetings.FileTags);
                                string[] tagList = Convert.ToString(Tags).Split(',');

                                //Deactivate Prev Tagging
                                if (tagList != null)
                                {
                                    if (tagList.Length > 0)
                                    {
                                        var prevTags = (from row in entities.BM_HistoricalFileTagMapping
                                                        where row.HistoricalID == _objHistoricalMeetings.ID
                                                        && row.IsActive == true
                                                        select row).ToList();

                                        if (prevTags != null)
                                        {
                                            if (prevTags.Count > 0)
                                            {

                                            }
                                        }
                                    }
                                }


                                foreach (string filetaginputes in tagList)
                                {
                                    var checkforTagging = (from row in entities.BM_HistoricalFileTagMapping
                                                           where row.TaggInputes == filetaginputes
                                                           && row.HistoricalID == _objHistoricalMeetings.ID
                                                           //&& row.IsActive == true 
                                                           && row.CustomerId == customerId
                                                           select row).FirstOrDefault();

                                    if (checkforTagging == null)
                                    {
                                        BM_HistoricalFileTagMapping _objtaginputes = new BM_HistoricalFileTagMapping();
                                        _objtaginputes.HistoricalID = (int)_objHistoricalMeetings.ID;
                                        _objtaginputes.TaggInputes = filetaginputes;
                                        _objtaginputes.DocType = _objHistoricalMeetings.status;
                                        _objtaginputes.IsActive = true;
                                        _objtaginputes.Createdby = userId;
                                        _objtaginputes.CreatedOn = DateTime.Now;
                                        _objtaginputes.CustomerId = customerId;

                                        entities.BM_HistoricalFileTagMapping.Add(_objtaginputes);
                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        checkforTagging.HistoricalID = (int)_objHistoricalMeetings.ID;
                                        checkforTagging.TaggInputes = filetaginputes;
                                        checkforTagging.DocType = _objHistoricalMeetings.status;
                                        checkforTagging.IsActive = true;
                                        checkforTagging.UpdatedBy = userId;
                                        checkforTagging.UpdatedOn = DateTime.Now;
                                        checkforTagging.CustomerId = customerId;

                                        entities.SaveChanges();
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            _objHistoricalMeetings.Error = true;
                            _objHistoricalMeetings.Message = "Record with Same Sr.No already exits";
                        }
                    }
                }
                else
                {
                    var checkhisdatawithoutserialno = (from x in entities.BM_HistoricalData
                                                       where x.CustomerId == customerId
                        && x.ID == _objHistoricalMeetings.ID
                        && x.EntityId == _objHistoricalMeetings.EntityId
                        && x.MeetingSrNo == _objHistoricalMeetings.MeetingSRNo
                         && x.FY == _objHistoricalMeetings.FYID
                         && x.IsActive==true

                                                       select x).FirstOrDefault();
                    if (checkhisdatawithoutserialno != null)
                    {
                        checkhisdatawithoutserialno.MeetingSrNo = _objHistoricalMeetings.MeetingSRNo;
                        checkhisdatawithoutserialno.EntityId = _objHistoricalMeetings.EntityId;
                        checkhisdatawithoutserialno.FY = _objHistoricalMeetings.FYID;

                        //checkhisdatawithoutserialno.MeetingTypeID = item;
                        checkhisdatawithoutserialno.MeetingDate = _objHistoricalMeetings.MeetingDate;
                        checkhisdatawithoutserialno.StartMeetingTime = _objHistoricalMeetings.StartMeetingTime;
                        checkhisdatawithoutserialno.EndMeetingTime = _objHistoricalMeetings.EndMeetingTime;
                        //checkhisdatawithoutserialno.UploadedType = _objHistoricalMeetings.status;
                        entities.SaveChanges();
                        _objHistoricalMeetings.Success = true;
                        _objHistoricalMeetings.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    if (_objHistoricalMeetings.files != null)
                        if (_objHistoricalMeetings.files.Count() > 0)
                        {
                            bool savechange = FileUpload.SaveHistoricalFile(_objHistoricalMeetings.files, checkhisdatawithoutserialno.ID, userId, customerId, _objHistoricalMeetings.status);
                        }
                }
                if (_objHistoricalMeetings.Circular_Meeting == "O")
                {
                    _objHistoricalMeetings.IsComcheck = true;
                    _objHistoricalMeetings.Isdefaultcheck = false;
                    _objHistoricalMeetings.IsOther = false;
                }
                else
                {
                    _objHistoricalMeetings.Isdefaultcheck = true;
                    _objHistoricalMeetings.IsComcheck = false;
                    _objHistoricalMeetings.IsOther = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objHistoricalMeetings.Error = true;
                _objHistoricalMeetings.Message = SecretarialConst.Messages.serverError;
            }
            return _objHistoricalMeetings;
        }

        public static void SaveHistoricalFileDataMapping(string doctype, long historicalID, long FileID)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    #region Save Update Doctype
                    if (doctype != null)
                    {
                        var checkforDoctype = (from doc in entities.BM_HistoricalDoctypeMapping
                                               where doc.HistoricalId == historicalID
                                               && doc.DocType == doctype
                                               && doc.FileId == FileID
                                               && doc.IsActive==true
                                               select doc).FirstOrDefault();
                        if (checkforDoctype == null)
                        {
                            BM_HistoricalDoctypeMapping _objdoctype = new BM_HistoricalDoctypeMapping()
                            {
                                DocType = doctype,
                                HistoricalId = (int)historicalID,
                                FileId = (int)FileID,
                                IsActive = true,
                            };
                            entities.BM_HistoricalDoctypeMapping.Add(_objdoctype);
                            entities.SaveChanges();

                        }
                        else
                        {
                            checkforDoctype.DocType = doctype;
                            checkforDoctype.HistoricalId = (int)historicalID;
                            checkforDoctype.FileId = (int)FileID;
                            checkforDoctype.IsActive = true;
                            entities.SaveChanges();
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string Viewdocument(long id)
        {
            string Historicaldata = "";
            var CheckData = (from row in entities.BM_FileData

                             where row.Id == id
                             && row.IsDeleted==false
                             select row).FirstOrDefault();

            if (CheckData != null)
            {
                string filePath = Path.Combine(HostingEnvironment.MapPath(CheckData.FilePath), CheckData.FileKey + Path.GetExtension(CheckData.FileName));
                if (CheckData.FilePath != null && File.Exists(filePath))
                {
                    string Folder = "~/TempFiles/HistoricalData";
                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    string DateFolder = Folder + "/" + File;

                    string extension = System.IO.Path.GetExtension(filePath);

                    //Directory.CreateDirectory(Server.MapPath(DateFolder));

                    if (!Directory.Exists(DateFolder))
                    {
                        Directory.CreateDirectory(HostingEnvironment.MapPath(DateFolder));
                    }

                    //string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    string User = 36 + "" + 5 + "" + FileDate;

                    string FileName = DateFolder + "/" + User + "" + extension;

                    FileStream fs = new FileStream(HostingEnvironment.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(Masters.CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                    bw.Close();

                    Historicaldata = FileName;
                    Historicaldata = Historicaldata.Substring(2, Historicaldata.Length - 2);

                }
            }
            return Historicaldata;
        }


        public bool Downloadrecords(long id)
        {
            try
            {
                var CheckData = (from row in entities.BM_FileData
                                 where row.Id == id
                                 && row.IsDeleted==false
                                 select row).ToList();

                if (CheckData.Count > 0)
                {
                    bool IsDocumentDownload = downloadHistoricalData(CheckData);
                }
                return true;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;

            }
        }

        private bool downloadHistoricalData(List<BM_FileData> checkData)
        {
            try
            {
                bool documentSuccess = false;

                using (ZipFile TaskDocument = new ZipFile())
                {
                    foreach (var files in checkData)
                    {
                        string filePath = Path.Combine(HostingEnvironment.MapPath(files.FilePath), files.FileKey + Path.GetExtension(files.FileName));

                        if (files.FilePath != null && File.Exists(filePath))
                        {
                            documentSuccess = true;
                            int idx = files.FileName.LastIndexOf('.');
                            string str = files.FileName.Substring(0, idx) + "_" + "." + files.FileName.Substring(idx + 1);
                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                            if (!TaskDocument.ContainsEntry("Document" + "/" + str))
                                TaskDocument.AddEntry("Document" + "/" + str, Masters.CryptographyHandler.AESDecrypt(ReadDocFiles(filePath)));
                        }
                    }
                    if (documentSuccess)
                    {
                        string fileNAme = "HistoricaDocuments" + DateTime.Now;
                        var zipMs = new MemoryStream();
                        WebClient req = new WebClient();
                        HttpResponse response = HttpContext.Current.Response;
                        TaskDocument.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] Filedata = zipMs.ToArray();
                        response.Buffer = true;
                        response.ClearContent();
                        response.ClearHeaders();
                        response.Clear();
                        response.ContentType = "application/zip";
                        response.AddHeader("content-disposition", "attachment; filename=" + fileNAme + ".zip");
                        response.BinaryWrite(Filedata);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    return documentSuccess;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }

        public List<FYearVM> BindFY()
        {
            List<FYearVM> _objFY = new List<FYearVM>();
            try
            {
                _objFY = (from row in entities.BM_YearMaster
                          where row.IsDeleted == false || row.IsForHistorical == true
                          select new FYearVM
                          {
                              Id = row.FYID,
                              FYText = row.FYText
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return _objFY;
        }

        public bool DeleteHisData(int iD, int UserId)
        {
            bool success = false;
            try
            {
                var checkData = (from x in entities.BM_HistoricalData where x.ID == iD select x).FirstOrDefault();
                //var checkData = (from x in entities.BM_HistoricalData where x.ID == iD && x.IsActive == true select x).FirstOrDefault();
                if (checkData != null)
                {
                    checkData.IsActive = false;

                    checkData.UpdatedBy = UserId;
                    checkData.UpdatedOn = DateTime.Now;
                    
                    entities.SaveChanges();
                    
                    success = true;
                }
            }
            catch (Exception ex)
            {
                success = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return success;
        }

        public bool DeleteHisFile(int iD, int userId)
        {
            bool success = false;
            try
            {
                var checkData = (from x in entities.BM_FileData where x.Id == iD select x).FirstOrDefault();
                //var checkData = (from x in entities.BM_HistoricalData where x.ID == iD && x.IsActive == true select x).FirstOrDefault();
                if (checkData != null)
                {
                    checkData.IsDeleted = true;
                    checkData.UploadedBy = userId;
                    checkData.UploadedOn = DateTime.Now;

                    entities.SaveChanges();

                    success = true;
                }
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

        public List<HistoricalDataAgendaVM> GetAgendaItem(long historicalId)
        {
            var result = new List<HistoricalDataAgendaVM>();
            try
            {
                result = (from row in entities.BM_HistoricalDataAgenda
                          where row.HistoricalId == historicalId && row.IsDeleted == false
                          select new HistoricalDataAgendaVM
                          {
                              ID = row.Id,
                              HistoricalId = row.HistoricalId,
                              ItemNo = row.SrNo,
                              AgendaItemText = row.AgendaItemText,
                              Result = row.Result,
                              t = row.SrNo == null ? (int) row.Id : row.SrNo
                          }).OrderBy( k=> k.t).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public Message SaveAgendaItem(long historicalId, List<HistoricalDataAgendaVM> lst, int userId)
        {
            var result = new Message();
            try
            {
                if(historicalId > 0)
                {
                    entities.BM_HistoricalDataAgenda.Where(k => k.HistoricalId == historicalId && k.IsDeleted == false).ToList().ForEach(k => k.IsDeleted = true);
                    entities.SaveChanges();
                    if(lst != null)
                    {
                        foreach (var item in lst)
                        {
                            var _obj = (from row in entities.BM_HistoricalDataAgenda
                                        where row.HistoricalId == historicalId && row.Id == item.ID
                                        select row).FirstOrDefault();
                            if(_obj != null)
                            {
                                _obj.SrNo = item.ItemNo;
                                _obj.Result = item.Result;
                                _obj.AgendaItemText = item.AgendaItemText;
                                _obj.UpdatedBy = userId;
                                _obj.UpdatedOn = DateTime.Now;
                                _obj.IsDeleted = false;
                                entities.SaveChanges();
                            }
                            else
                            {
                                _obj = new BM_HistoricalDataAgenda();
                                _obj.HistoricalId = historicalId;
                                _obj.SrNo = item.ItemNo;
                                _obj.Result = item.Result;
                                _obj.AgendaItemText = item.AgendaItemText;
                                _obj.CreatedBy = userId;
                                _obj.CreatedOn = DateTime.Now;
                                _obj.IsDeleted = false;
                                entities.BM_HistoricalDataAgenda.Add(_obj);
                                entities.SaveChanges();
                            }
                        }
                    }
                    result.Success = true;
                    result.Message = SecretarialConst.Messages.saveSuccess;
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<MeetingAgendaSummary> GetAgendaItemForConcludedMeeting(long historicalId)
        {
            var result = new List<MeetingAgendaSummary>();
            try
            {
                result = (from row in entities.BM_HistoricalDataAgenda
                          where row.HistoricalId == historicalId && row.IsDeleted == false
                          select new MeetingAgendaSummary
                          {
                              HMappingId = row.Id,
                              HId = row.HistoricalId,
                              SrNo = row.SrNo,
                              AgendaFormate = row.AgendaItemText,
                              Result = row.Result,
                              ParticipantId = 0,
                              MeetingId = 0,
                              MeetingAgendaMappingId = 0,
                              t = row.SrNo == null ? (int)row.Id : row.SrNo
                          }).OrderBy(k => k.t).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
    }
}