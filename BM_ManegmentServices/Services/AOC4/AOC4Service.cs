﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.HelpVideo;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using iTextSharp.text.pdf;

namespace BM_ManegmentServices.Services.AOC4
{
    public class AOC4Service : IAOC4Service
    {
        public List<AOC4_List_VM> GetAOC4List(int customerId)
        {
            var result = new List<AOC4_List_VM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SP_GetAOC4ByCustomer(customerId)
                              select new AOC4_List_VM
                              {
                                  AOC4Id = row.Id,
                                  EntityId = row.EntityId,
                                  Company = row.CompanyName,
                                  FYId = row.FYId,
                                  FY = row.FYText,
                                  Status_ = row.Status_
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #region Master
        public AOC4_List_VM CreateAOC4(AOC4_List_VM obj, int userId, int customerId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var isExists = (from row in entities.BM_Form_AOC4
                                    where row.EntityId == obj.EntityId && row.FYId == obj.FYId && row.IsActive == true && row.IsDeleted == false
                                    select row).Any();

                    if (isExists)
                    {
                        obj.Error = true;
                        obj.Message = "Record Allready exists...!";
                    }
                    else
                    {
                        var CIN = (from row in entities.BM_EntityMaster
                                   where row.Id == obj.EntityId
                                   select row.CIN_LLPIN).FirstOrDefault();

                        var _obj = new BM_Form_AOC4()
                        {
                            CompanyInfo = false,
                            BalanceSheet = false,
                            Detailed_A = false,
                            Detailed_B = false,
                            Detailed_C = false,
                            Detailed_D = false,
                            Detailed_E = false,
                            Financial_Parameters = false,
                            ShareCapital = false,
                            Detailed_CostRecord = false,
                            P_And_L = false,
                            Detailed_PL_A = false,
                            Detailed_PL_B = false,
                            Financial_Parameters_PL = false,
                            Principal_Or_Services = false,
                            CSR = false,
                            RelatedParty = false,
                            AuditorsReport = false,
                            Misc = false,
                            Auth = false
                        };

                        _obj.IsActive = true;
                        _obj.IsDeleted = false;

                        _obj.EntityId = obj.EntityId;
                        _obj.CustomerId = customerId;
                        _obj.FYId = obj.FYId;
                        _obj.CIN = CIN;

                        _obj.FormVersion = 1;


                        _obj.CreatedBy = userId;
                        _obj.CreatedOn = DateTime.Now;
                        entities.BM_Form_AOC4.Add(_obj);
                        entities.SaveChanges();
                        obj.AOC4Id = _obj.Id;

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public AOC4_List_VM GetAOC4Master(long id, int customerId)
        {
            var result = new AOC4_List_VM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SP_GetAOC4ByCustomer(customerId)
                              where row.Id == id
                              select new AOC4_List_VM
                              {
                                  AOC4Id = row.Id,
                                  EntityId = row.EntityId,
                                  Company = row.CompanyName,
                                  FYId = row.FYId,
                                  FY = row.FYText
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<AOC4DetailsVM> GetAOC4DetailsById(int id, int customerId)
        {
            var result = new List<AOC4DetailsVM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SP_GetAOC4DetailsById(id, customerId)
                              select new AOC4DetailsVM
                              {
                                  Aoc4Id = row.Id,
                                  EntityId = row.EntityId,
                                  Seg = row.Seg,
                                  Details = row.Details,
                                  Status_ = row.Status_,
                                  EditAction = row.EditAction,
                                  PrevAction = row.PrevAction,
                                  wndTitle = row.wndTitle
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Company Info
        public AOC4_CompanyInfoVM GetCompanyInfo(long id, int entityId)
        {
            var result = new AOC4_CompanyInfoVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_1_CompanyInfo
                              join a in entities.BM_Form_AOC4 on row.AOC4Id equals a.Id
                              join e in entities.BM_EntityMaster on a.EntityId equals e.Id
                              where row.AOC4Id == id && row.IsActive == true && a.IsActive == true
                              select new AOC4_CompanyInfoVM
                              {
                                  CompanyInfoId = row.Id,
                                  CompanyInfoAOC4Id = row.AOC4Id,
                                  EntityId = e.Id,
                                  CompanyName = e.CompanyName,
                                  FYStart = row.FYStart,
                                  FYEnd = row.FYEnd,
                                  DateOfBM_FRApproved = row.DateOfBM_FRApproved,
                                  DateOfBM_FRSec134 = row.DateOfBM_FRSec134,
                                  DateOfBM_FRSignAuditor = row.DateOfBM_FRSignAuditor,
                                  NoOfAuditors = row.NoOfAuditors,
                                  RecordsInEForm = row.RecordsInEForm,
                                  ShareCapital = row.ShareCapital,
                                  DetailedPL = row.DetailedPL,
                                  RPT = row.RPT
                              }).FirstOrDefault();

                    if (result == null)
                    {
                        result = new AOC4_CompanyInfoVM() { CompanyInfoAOC4Id = id, EntityId = entityId, objSignedDetails = new AOC4_SignedDetailsVM() };
                        var objAOC4 = (from row in entities.BM_Form_AOC4
                                       join y in entities.BM_YearMaster on row.FYId equals y.FYID
                                       where row.Id == id
                                       select new
                                       {
                                           y.FromDate,
                                           y.ToDate
                                       }).FirstOrDefault();

                        if (objAOC4 != null)
                        {
                            result.FYStart = objAOC4.FromDate;
                            result.FYEnd = objAOC4.ToDate;
                        }
                    }

                    #region Signed Details
                    result.objSignedDetails = (from row in entities.BM_Form_AOC4_1_SignedDetails
                                               where row.AOC4Id == id && row.IsActive == true
                                               select new AOC4_SignedDetailsVM
                                               {
                                                   DINPAN_1 = row.DINPAN_1,
                                                   Name_1 = row.Name_1,
                                                   Date_1 = row.Date_1,
                                                   DINPAN_2 = row.DINPAN_2,
                                                   Name_2 = row.Name_2,
                                                   Date_2 = row.Date_2,
                                                   DINPAN_3 = row.DINPAN_3,
                                                   Name_3 = row.Name_3,
                                                   Date_3 = row.Date_3,
                                                   DINPAN_4 = row.DINPAN_4,
                                                   Name_4 = row.Name_4,
                                                   Date_4 = row.Date_4,
                                                   DINPAN_5 = row.DINPAN_5,
                                                   Name_5 = row.Name_5,
                                                   Date_5 = row.Date_5,
                                                   DIN_1 = row.DIN_1,
                                                   DName_1 = row.DName_1,
                                                   DDate_1 = row.DDate_1,
                                                   DIN_2 = row.DIN_2,
                                                   DName_2 = row.DName_2,
                                                   DDate_2 = row.DDate_2,
                                                   DIN_3 = row.DIN_3,
                                                   DName_3 = row.DName_3,
                                                   DDate_3 = row.DDate_3
                                               }).FirstOrDefault();

                    if (result.objSignedDetails == null)
                    {
                        result.objSignedDetails = new AOC4_SignedDetailsVM();
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AOC4_CompanyInfoVM SaveCompanyInfo(AOC4_CompanyInfoVM obj, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Form_AOC4_1_CompanyInfo
                                where row.Id == obj.CompanyInfoId && row.AOC4Id == obj.CompanyInfoAOC4Id
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.FYStart = obj.FYStart;
                        _obj.FYEnd = obj.FYEnd;
                        _obj.DateOfBM_FRApproved = obj.DateOfBM_FRApproved;
                        _obj.DateOfBM_FRSec134 = obj.DateOfBM_FRSec134;
                        _obj.DateOfBM_FRSignAuditor = obj.DateOfBM_FRSignAuditor;
                        _obj.NoOfAuditors = obj.NoOfAuditors;
                        _obj.RecordsInEForm = obj.RecordsInEForm;

                        _obj.ShareCapital = obj.ShareCapital;
                        _obj.DetailedPL = obj.DetailedPL;
                        _obj.RPT = obj.RPT;

                        _obj.IsActive = true;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        _obj = new BM_Form_AOC4_1_CompanyInfo();
                        _obj.AOC4Id = obj.CompanyInfoAOC4Id;
                        _obj.FYStart = obj.FYStart;
                        _obj.FYEnd = obj.FYEnd;
                        _obj.DateOfBM_FRApproved = obj.DateOfBM_FRApproved;
                        _obj.DateOfBM_FRSec134 = obj.DateOfBM_FRSec134;
                        _obj.DateOfBM_FRSignAuditor = obj.DateOfBM_FRSignAuditor;
                        _obj.NoOfAuditors = obj.NoOfAuditors;
                        _obj.RecordsInEForm = obj.RecordsInEForm;

                        _obj.ShareCapital = obj.ShareCapital;
                        _obj.DetailedPL = obj.DetailedPL;
                        _obj.RPT = obj.RPT;

                        _obj.IsActive = true;
                        _obj.CreatedBy = userId;
                        _obj.CreatedOn = DateTime.Now;

                        entities.BM_Form_AOC4_1_CompanyInfo.Add(_obj);
                        entities.SaveChanges();

                        var objAOCForm = (from row in entities.BM_Form_AOC4
                                          where row.Id == obj.CompanyInfoAOC4Id
                                          select row).FirstOrDefault();
                        if (objAOCForm != null)
                        {
                            objAOCForm.CompanyInfo = true;

                            objAOCForm.UpdatedBy = userId;
                            objAOCForm.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }

                        obj.CompanyInfoId = _obj.Id;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;

                    }

                    #region
                    if (obj.Success)
                    {
                        bool ShareCapital = false, DetailedPL = false, RPT = false;
                        if (obj.ShareCapital == false)
                        {
                            ShareCapital = true;
                        }
                        else
                        {
                            ShareCapital = (from row in entities.BM_Form_AOC4_4_ShareCapitalRaised
                                            where row.AOC4Id == obj.CompanyInfoAOC4Id && row.IsActive == true
                                            select row.Id).Any();
                        }

                        if (obj.DetailedPL == false)
                        {
                            DetailedPL = true;
                        }
                        else
                        {
                            DetailedPL = (from row in entities.BM_Form_AOC4_6_P_And_L_Detailed
                                          where row.AOC4Id == obj.CompanyInfoAOC4Id && row.IsActive == true
                                          select row.Id).Any();
                        }

                        if (obj.RPT == false)
                        {
                            RPT = true;
                        }
                        else
                        {
                            RPT = (from row in entities.BM_Form_AOC4_10_RPT_Contracts
                                   where row.AOC4Id == obj.CompanyInfoAOC4Id && row.IsActive == true
                                   select row.Id).Any();

                            if (RPT == false)
                            {
                                RPT = (from row in entities.BM_Form_AOC4_10_RPT_MaterialContracts
                                       where row.AOC4Id == obj.CompanyInfoAOC4Id && row.IsActive == true
                                       select row.Id).Any();
                            }
                        }

                        var objAOCForm = (from row in entities.BM_Form_AOC4
                                          where row.Id == obj.CompanyInfoAOC4Id
                                          select row).FirstOrDefault();
                        if (objAOCForm != null)
                        {
                            objAOCForm.ShareCapital = ShareCapital;
                            objAOCForm.Detailed_PL_A = DetailedPL;
                            objAOCForm.RelatedParty = RPT;

                            objAOCForm.UpdatedBy = userId;
                            objAOCForm.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }
                    }
                    #endregion

                    #region Save Signed Details
                    if (obj.Success)
                    {
                        if (obj.objSignedDetails != null)
                        {
                            var _objSignedDetails = (from row in entities.BM_Form_AOC4_1_SignedDetails
                                                     where row.AOC4Id == obj.CompanyInfoAOC4Id
                                                     select row).FirstOrDefault();

                            if (_objSignedDetails != null)
                            {
                                _objSignedDetails.AOC4Id = obj.CompanyInfoAOC4Id;
                                _objSignedDetails.DINPAN_1 = obj.objSignedDetails.DINPAN_1;
                                _objSignedDetails.Name_1 = obj.objSignedDetails.Name_1;
                                _objSignedDetails.Date_1 = obj.objSignedDetails.Date_1;
                                _objSignedDetails.DINPAN_2 = obj.objSignedDetails.DINPAN_2;
                                _objSignedDetails.Name_2 = obj.objSignedDetails.Name_2;
                                _objSignedDetails.Date_2 = obj.objSignedDetails.Date_2;
                                _objSignedDetails.DINPAN_3 = obj.objSignedDetails.DINPAN_3;
                                _objSignedDetails.Name_3 = obj.objSignedDetails.Name_3;
                                _objSignedDetails.Date_3 = obj.objSignedDetails.Date_3;
                                _objSignedDetails.DINPAN_4 = obj.objSignedDetails.DINPAN_4;
                                _objSignedDetails.Name_4 = obj.objSignedDetails.Name_4;
                                _objSignedDetails.Date_4 = obj.objSignedDetails.Date_4;
                                _objSignedDetails.DINPAN_5 = obj.objSignedDetails.DINPAN_5;
                                _objSignedDetails.Name_5 = obj.objSignedDetails.Name_5;
                                _objSignedDetails.Date_5 = obj.objSignedDetails.Date_5;
                                _objSignedDetails.DIN_1 = obj.objSignedDetails.DIN_1;
                                _objSignedDetails.DName_1 = obj.objSignedDetails.DName_1;
                                _objSignedDetails.DDate_1 = obj.objSignedDetails.DDate_1;
                                _objSignedDetails.DIN_2 = obj.objSignedDetails.DIN_2;
                                _objSignedDetails.DName_2 = obj.objSignedDetails.DName_2;
                                _objSignedDetails.DDate_2 = obj.objSignedDetails.DDate_2;
                                _objSignedDetails.DIN_3 = obj.objSignedDetails.DIN_3;
                                _objSignedDetails.DName_3 = obj.objSignedDetails.DName_3;
                                _objSignedDetails.DDate_3 = obj.objSignedDetails.DDate_3;
                                _objSignedDetails.IsActive = true;
                                _objSignedDetails.UpdatedBy = userId;
                                _objSignedDetails.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();
                            }
                            else
                            {
                                _objSignedDetails = new BM_Form_AOC4_1_SignedDetails();
                                _objSignedDetails.AOC4Id = obj.CompanyInfoAOC4Id;
                                _objSignedDetails.DINPAN_1 = obj.objSignedDetails.DINPAN_1;
                                _objSignedDetails.Name_1 = obj.objSignedDetails.Name_1;
                                _objSignedDetails.Date_1 = obj.objSignedDetails.Date_1;
                                _objSignedDetails.DINPAN_2 = obj.objSignedDetails.DINPAN_2;
                                _objSignedDetails.Name_2 = obj.objSignedDetails.Name_2;
                                _objSignedDetails.Date_2 = obj.objSignedDetails.Date_2;
                                _objSignedDetails.DINPAN_3 = obj.objSignedDetails.DINPAN_3;
                                _objSignedDetails.Name_3 = obj.objSignedDetails.Name_3;
                                _objSignedDetails.Date_3 = obj.objSignedDetails.Date_3;
                                _objSignedDetails.DINPAN_4 = obj.objSignedDetails.DINPAN_4;
                                _objSignedDetails.Name_4 = obj.objSignedDetails.Name_4;
                                _objSignedDetails.Date_4 = obj.objSignedDetails.Date_4;
                                _objSignedDetails.DINPAN_5 = obj.objSignedDetails.DINPAN_5;
                                _objSignedDetails.Name_5 = obj.objSignedDetails.Name_5;
                                _objSignedDetails.Date_5 = obj.objSignedDetails.Date_5;
                                _objSignedDetails.DIN_1 = obj.objSignedDetails.DIN_1;
                                _objSignedDetails.DName_1 = obj.objSignedDetails.DName_1;
                                _objSignedDetails.DDate_1 = obj.objSignedDetails.DDate_1;
                                _objSignedDetails.DIN_2 = obj.objSignedDetails.DIN_2;
                                _objSignedDetails.DName_2 = obj.objSignedDetails.DName_2;
                                _objSignedDetails.DDate_2 = obj.objSignedDetails.DDate_2;
                                _objSignedDetails.DIN_3 = obj.objSignedDetails.DIN_3;
                                _objSignedDetails.DName_3 = obj.objSignedDetails.DName_3;
                                _objSignedDetails.DDate_3 = obj.objSignedDetails.DDate_3;
                                _objSignedDetails.IsActive = true;
                                _objSignedDetails.CreatedBy = userId;
                                _objSignedDetails.CreatedOn = DateTime.Now;

                                entities.BM_Form_AOC4_1_SignedDetails.Add(_objSignedDetails);
                                entities.SaveChanges();

                                obj.objSignedDetails.SignedId = _objSignedDetails.Id;

                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public AOC4_CompanyHoldingVM GetCompanyHolding(int entityId)
        {
            var result = new AOC4_CompanyHoldingVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SubEntityMapping
                              where row.EntityId == entityId && row.isActive == true && row.CompanysubType == 2
                              select new AOC4_CompanyHoldingVM
                              {
                                  CIN_HOLDING_COMP = row.CIN
                              }).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        #endregion

        #region Balance Sheet
        public Message SaveBalanceSheet(List<AOC4_BalanceSheetVM> objList, int userId)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (objList != null)
                    {
                        foreach (var obj in objList)
                        {

                            var _obj = (from row in entities.BM_Form_AOC4_2_BalanceSheet
                                        where row.AOC4Id == obj.BalanceSheetAOC4Id && row.ReportingPeriod == obj.ReportingPeriod
                                        select row).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.ReportingPeriod = obj.ReportingPeriod;
                                _obj.EndDate = obj.EndDate;
                                _obj.Capital = obj.Capital;
                                _obj.Reserve = obj.Reserve;
                                _obj.MoneyRecived = obj.MoneyRecived;
                                _obj.MoneyPending = obj.MoneyPending;
                                _obj.LongTerm = obj.LongTerm;
                                _obj.Deferred = obj.Deferred;
                                _obj.OtherLongTerm = obj.OtherLongTerm;
                                _obj.LongTermProvision = obj.LongTermProvision;
                                _obj.ShortTerm = obj.ShortTerm;
                                _obj.TradePayables = obj.TradePayables;
                                _obj.OtherCL = obj.OtherCL;
                                _obj.ShortTermProvision = obj.ShortTermProvision;
                                _obj.Total_I = obj.Total_I;
                                _obj.TangibleA = obj.TangibleA;
                                _obj.IntangibleA = obj.IntangibleA;
                                _obj.CapitalWIP = obj.CapitalWIP;
                                _obj.IntangibleA_UD = obj.IntangibleA_UD;
                                _obj.NonCI = obj.NonCI;
                                _obj.DeferredTA = obj.DeferredTA;
                                _obj.Lt_LoanAndAdv = obj.Lt_LoanAndAdv;
                                _obj.OtherNonCA = obj.OtherNonCA;
                                _obj.CurrentInvestment = obj.CurrentInvestment;
                                _obj.Invetories = obj.Invetories;
                                _obj.TradeReceivable = obj.TradeReceivable;
                                _obj.CashEquivalents = obj.CashEquivalents;
                                _obj.ShortTermLoan = obj.ShortTermLoan;
                                _obj.OtherCurrentAsset = obj.OtherCurrentAsset;
                                _obj.Total_II = obj.Total_II;

                                _obj.IsActive = true;
                                _obj.UpdatedBy = userId;
                                _obj.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();

                                result.Success = true;
                                result.Message = SecretarialConst.Messages.updateSuccess;
                            }
                            else
                            {
                                _obj = new BM_Form_AOC4_2_BalanceSheet();

                                _obj.AOC4Id = obj.BalanceSheetAOC4Id;
                                _obj.ReportingPeriod = obj.ReportingPeriod;
                                _obj.EndDate = obj.EndDate;
                                _obj.Capital = obj.Capital;
                                _obj.Reserve = obj.Reserve;
                                _obj.MoneyRecived = obj.MoneyRecived;
                                _obj.MoneyPending = obj.MoneyPending;
                                _obj.LongTerm = obj.LongTerm;
                                _obj.Deferred = obj.Deferred;
                                _obj.OtherLongTerm = obj.OtherLongTerm;
                                _obj.LongTermProvision = obj.LongTermProvision;
                                _obj.ShortTerm = obj.ShortTerm;
                                _obj.TradePayables = obj.TradePayables;
                                _obj.OtherCL = obj.OtherCL;
                                _obj.ShortTermProvision = obj.ShortTermProvision;
                                _obj.Total_I = obj.Total_I;
                                _obj.TangibleA = obj.TangibleA;
                                _obj.IntangibleA = obj.IntangibleA;
                                _obj.CapitalWIP = obj.CapitalWIP;
                                _obj.IntangibleA_UD = obj.IntangibleA_UD;
                                _obj.NonCI = obj.NonCI;
                                _obj.DeferredTA = obj.DeferredTA;
                                _obj.Lt_LoanAndAdv = obj.Lt_LoanAndAdv;
                                _obj.OtherNonCA = obj.OtherNonCA;
                                _obj.CurrentInvestment = obj.CurrentInvestment;
                                _obj.Invetories = obj.Invetories;
                                _obj.TradeReceivable = obj.TradeReceivable;
                                _obj.CashEquivalents = obj.CashEquivalents;
                                _obj.ShortTermLoan = obj.ShortTermLoan;
                                _obj.OtherCurrentAsset = obj.OtherCurrentAsset;
                                _obj.Total_II = obj.Total_II;

                                _obj.IsActive = true;
                                _obj.CreatedBy = userId;
                                _obj.CreatedOn = DateTime.Now;

                                entities.BM_Form_AOC4_2_BalanceSheet.Add(_obj);
                                entities.SaveChanges();

                                var objAOCForm = (from row in entities.BM_Form_AOC4
                                                  where row.Id == obj.BalanceSheetAOC4Id
                                                  select row).FirstOrDefault();
                                if (objAOCForm != null)
                                {
                                    objAOCForm.BalanceSheet = true;

                                    objAOCForm.UpdatedBy = userId;
                                    objAOCForm.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }

                                obj.BalanceSheetId = _obj.Id;
                                result.Success = true;
                                result.Message = SecretarialConst.Messages.saveSuccess;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AOC4_BalanceSheetPrevVM GetBalanceSheet(long id, int customerId)
        {
            var result = new AOC4_BalanceSheetPrevVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Form_AOC4_2_BalanceSheet
                                where row.AOC4Id == id && row.IsActive == true
                                select new AOC4_BalanceSheetVM
                                {
                                    BalanceSheetId = row.Id,
                                    BalanceSheetAOC4Id = row.AOC4Id,
                                    ReportingPeriod = row.ReportingPeriod,
                                    EndDate = row.EndDate,
                                    Capital = row.Capital,
                                    Reserve = row.Reserve,
                                    MoneyRecived = row.MoneyRecived,
                                    MoneyPending = row.MoneyPending,
                                    LongTerm = row.LongTerm,
                                    Deferred = row.Deferred,
                                    OtherLongTerm = row.OtherLongTerm,
                                    LongTermProvision = row.LongTermProvision,
                                    ShortTerm = row.ShortTerm,
                                    TradePayables = row.TradePayables,
                                    OtherCL = row.OtherCL,
                                    ShortTermProvision = row.ShortTermProvision,
                                    Total_I = row.Total_I,
                                    TangibleA = row.TangibleA,
                                    IntangibleA = row.IntangibleA,
                                    CapitalWIP = row.CapitalWIP,
                                    IntangibleA_UD = row.IntangibleA_UD,
                                    NonCI = row.NonCI,
                                    DeferredTA = row.DeferredTA,
                                    Lt_LoanAndAdv = row.Lt_LoanAndAdv,
                                    OtherNonCA = row.OtherNonCA,
                                    CurrentInvestment = row.CurrentInvestment,
                                    Invetories = row.Invetories,
                                    TradeReceivable = row.TradeReceivable,
                                    CashEquivalents = row.CashEquivalents,
                                    ShortTermLoan = row.ShortTermLoan,
                                    OtherCurrentAsset = row.OtherCurrentAsset,
                                    Total_II = row.Total_II,
                                }).ToList();

                    if (_obj != null)
                    {
                        result.Current = new AOC4_BalanceSheetVM();
                        result.Current = (from row in _obj
                                          where row.ReportingPeriod == "C"
                                          select row).FirstOrDefault();

                        result.Previous = new AOC4_BalanceSheetVM();
                        result.Previous = (from row in _obj
                                           where row.ReportingPeriod == "P"
                                           select row).FirstOrDefault();
                    }

                    #region Set Default Values
                    if (result.Current == null)
                    {
                        result.Current = new AOC4_BalanceSheetVM()
                        {
                            BalanceSheetId = 0,
                            BalanceSheetAOC4Id = 0,
                            ReportingPeriod = "C",
                            EndDate = null,
                            Capital = 0,
                            Reserve = 0,
                            MoneyRecived = 0,
                            MoneyPending = 0,
                            LongTerm = 0,
                            Deferred = 0,
                            OtherLongTerm = 0,
                            LongTermProvision = 0,
                            ShortTerm = 0,
                            TradePayables = 0,
                            OtherCL = 0,
                            ShortTermProvision = 0,
                            Total_I = 0,
                            TangibleA = 0,
                            IntangibleA = 0,
                            CapitalWIP = 0,
                            IntangibleA_UD = 0,
                            NonCI = 0,
                            DeferredTA = 0,
                            Lt_LoanAndAdv = 0,
                            OtherNonCA = 0,
                            CurrentInvestment = 0,
                            Invetories = 0,
                            TradeReceivable = 0,
                            CashEquivalents = 0,
                            ShortTermLoan = 0,
                            OtherCurrentAsset = 0,
                            Total_II = 0,
                        };
                    }

                    if (result.Previous == null)
                    {
                        result.Previous = new AOC4_BalanceSheetVM()
                        {
                            BalanceSheetId = 0,
                            BalanceSheetAOC4Id = 0,
                            ReportingPeriod = "P",
                            EndDate = null,
                            Capital = 0,
                            Reserve = 0,
                            MoneyRecived = 0,
                            MoneyPending = 0,
                            LongTerm = 0,
                            Deferred = 0,
                            OtherLongTerm = 0,
                            LongTermProvision = 0,
                            ShortTerm = 0,
                            TradePayables = 0,
                            OtherCL = 0,
                            ShortTermProvision = 0,
                            Total_I = 0,
                            TangibleA = 0,
                            IntangibleA = 0,
                            CapitalWIP = 0,
                            IntangibleA_UD = 0,
                            NonCI = 0,
                            DeferredTA = 0,
                            Lt_LoanAndAdv = 0,
                            OtherNonCA = 0,
                            CurrentInvestment = 0,
                            Invetories = 0,
                            TradeReceivable = 0,
                            CashEquivalents = 0,
                            ShortTermLoan = 0,
                            OtherCurrentAsset = 0,
                            Total_II = 0,
                        };
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public long GetBalanceSheetIdByAOC4Id(long AOC4Id, int customerId)
        {
            long result = 0;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var isExists = (from row in entities.BM_Form_AOC4_2_BalanceSheet
                                    where row.AOC4Id == AOC4Id && row.IsActive == true
                                    select row.Id).Any();

                    if (isExists)
                    {
                        result = AOC4Id;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public long GetPreviousPeriodIdByAOC4Id(long AOC4Id, int customerId)
        {
            long result = 0;
            DateTime lastFYEndDate;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var details = (from row in entities.BM_Form_AOC4
                                   join e in entities.BM_EntityMaster on row.EntityId equals e.Id
                                   join y in entities.BM_YearMaster on row.FYId equals y.FYID
                                   where row.Id == AOC4Id && row.IsActive == true
                                   select new
                                   {
                                       AOC4Id = row.Id,
                                       EntityId = e.Id,
                                       FYCY = y.Type,
                                       FY = y.FYID,
                                       FromDate = y.FromDate
                                   }).FirstOrDefault();
                    if (details != null)
                    {
                        if (details.FromDate != null)
                        {
                            lastFYEndDate = Convert.ToDateTime(details.FromDate).AddDays(-30);

                            var fyId = (from row in entities.BM_YearMaster
                                        where row.FromDate <= lastFYEndDate && row.ToDate >= lastFYEndDate &&
                                        row.Type == details.FYCY && row.IsDeleted == false
                                        select row.FYID).FirstOrDefault();
                            if (fyId > 0)
                            {
                                var lastAOC4Id = (from row in entities.BM_Form_AOC4
                                                  join e in entities.BM_EntityMaster on row.EntityId equals e.Id
                                                  join y in entities.BM_YearMaster on row.FYId equals y.FYID
                                                  where row.EntityId == details.EntityId && row.FYId == fyId && row.IsActive == true
                                                  select row.Id).FirstOrDefault();

                                if (lastAOC4Id > 0)
                                {
                                    result = lastAOC4Id;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AOC4_BalanceSheetPrevVM GetBalanceSheetDefaultDataForExcel(long id, int customerId)
        {
            AOC4_BalanceSheetPrevVM result = null;
            bool lastYearflag = false;
            //Get Current AOC4 id if balance sheet data Exists
            var AOC4Id = GetBalanceSheetIdByAOC4Id(id, customerId);

            if (AOC4Id > 0)
            {
                lastYearflag = false;
            }
            else
            {
                //Get last Year AOC4 id if balance sheet data Exists
                AOC4Id = GetPreviousPeriodIdByAOC4Id(id, customerId);
                if(AOC4Id > 0)
                {
                    lastYearflag = true;
                }
                else
                {
                    lastYearflag = false;
                }

            }

            if (lastYearflag == false)
            {
                result = GetBalanceSheet(AOC4Id, customerId);
            }
            else
            {
                var result1 = GetBalanceSheet(AOC4Id, customerId);
                result = new AOC4_BalanceSheetPrevVM();
                result.Current = new AOC4_BalanceSheetVM() { ReportingPeriod = "C" };

                result.Previous = new AOC4_BalanceSheetVM();
                if(result1 != null)
                {
                    if(result1.Current != null)
                    {
                        result.Previous = result1.Current;
                        result.Previous.ReportingPeriod = "P";
                    }
                }
            }

            return result;
        }
        #endregion

        #region Detailed Balance sheet
        public Message SaveDetailedBalanceSheet(List<AOC4_DetailedBalanceSheetVM> objList, int userId)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (objList != null)
                    {
                        foreach (var obj in objList)
                        {

                            var _obj = (from row in entities.BM_Form_AOC4_2_DetailedBalanceSheet
                                        where row.AOC4Id == obj.BalanceSheetAOC4Id && row.LT_ReportingPeriod == obj.LT_ReportingPeriod
                                        select row).FirstOrDefault();
                            if (_obj != null)
                            {
                                // Section A 
                                _obj.LT_ReportingPeriod = obj.LT_ReportingPeriod;
                                _obj.LT_Bonds = obj.LT_Bonds;
                                _obj.LT_LoanBank = obj.LT_LoanBank;
                                _obj.LT_LoanOtherParties = obj.LT_LoanOtherParties;
                                _obj.LT_Deferred_pay_liabilities = obj.LT_Deferred_pay_liabilities;
                                _obj.LT_Deposits = obj.LT_Deposits;
                                _obj.LT_Loans_adv_related_parties = obj.LT_Loans_adv_related_parties;
                                _obj.LT_maturities = obj.LT_maturities;
                                _obj.LT_Otherloans_advances = obj.LT_Otherloans_advances;
                                _obj.LT_Tot_borrowings = obj.LT_Tot_borrowings;
                                _obj.LT_Aggregate_Amt = obj.LT_Aggregate_Amt;

                                // Section B
                                _obj.ST_LoanBank = obj.ST_LoanBank;
                                _obj.ST_LoanOtherParties = obj.ST_LoanOtherParties;
                                _obj.ST_Loans_advances_related_parties = obj.ST_Loans_advances_related_parties;
                                _obj.ST_Deposits = obj.ST_Deposits;
                                _obj.ST_Loans_adv_related_parties = obj.ST_Loans_adv_related_parties;
                                _obj.ST_Tot_borrowings = obj.ST_Tot_borrowings;
                                _obj.ST_Aggregate_Amt = obj.ST_Aggregate_Amt;

                                // Section C
                                _obj.LTA_Capital_adv = obj.LTA_Capital_adv;
                                _obj.LTA_Security_Deposits = obj.LTA_Security_Deposits;
                                _obj.LTA_Loans_adv_related_parties = obj.LTA_Loans_adv_related_parties;
                                _obj.LTA_Otherloans_advances = obj.LTA_Otherloans_advances;
                                _obj.LTA_TotLoan_Adv = obj.LTA_TotLoan_Adv;
                                _obj.LTA_FromRelated_Parties = obj.LTA_FromRelated_Parties;
                                _obj.LTA_FromOthers = obj.LTA_FromOthers;
                                _obj.LTA_Net_loan_Adv = obj.LTA_Net_loan_Adv;
                                _obj.LTA_Loans_Adv_dueByDir = obj.LTA_Loans_Adv_dueByDir;

                                // Section D
                                _obj.LTAD_Capital_adv = obj.LTAD_Capital_adv;
                                _obj.LTAD_Security_Deposits = obj.LTAD_Security_Deposits;
                                _obj.LTAD_Loans_adv_related_parties = obj.LTAD_Loans_adv_related_parties;
                                _obj.LTAD_Otherloans_advances = obj.LTAD_Otherloans_advances;
                                _obj.LTAD_TotLoan_Adv = obj.LTAD_TotLoan_Adv;
                                _obj.LTAD_FromRelated_Parties = obj.LTAD_FromRelated_Parties;
                                _obj.LTAD_FromOthers = obj.LTAD_FromOthers;
                                _obj.LTAD_Net_loan_Adv = obj.LTAD_Net_loan_Adv;
                                _obj.LTAD_Loans_Adv_dueByDir = obj.LTAD_Loans_Adv_dueByDir;

                                // Section E 
                                _obj.TR_Secured_ESM = obj.TR_Secured_ESM;
                                _obj.TR_Secured_WSM = obj.TR_Secured_WSM;
                                _obj.TR_Unsecured_ESM = obj.TR_Unsecured_ESM;
                                _obj.TR_Unsecured_WSM = obj.TR_Unsecured_WSM;
                                _obj.TR_Doubtful_ESM = obj.TR_Doubtful_ESM;
                                _obj.TR_Doubtful_WSM = obj.TR_Doubtful_WSM;
                                _obj.TR_TotalTrade_ESM = obj.TR_TotalTrade_ESM;
                                _obj.TR_TotalTrade_WSM = obj.TR_TotalTrade_WSM;
                                _obj.TR_Provision_ESM = obj.TR_Provision_ESM;
                                _obj.TR_Provision_WSM = obj.TR_Provision_WSM;
                                _obj.TR_NetTrade_ESM = obj.TR_NetTrade_ESM;
                                _obj.TR_NetTrade_WSM = obj.TR_NetTrade_WSM;
                                _obj.TR_DebtDue_ESM = obj.TR_DebtDue_ESM;
                                _obj.TR_DebtDue_WSM = obj.TR_DebtDue_WSM;

                                entities.SaveChanges();

                                result.Success = true;
                                result.Message = SecretarialConst.Messages.updateSuccess;
                            }
                            else
                            {
                                _obj = new BM_Form_AOC4_2_DetailedBalanceSheet();

                                //Section A
                                _obj.AOC4Id = obj.BalanceSheetAOC4Id;
                                _obj.LT_ReportingPeriod = obj.LT_ReportingPeriod;
                                _obj.LT_Bonds = obj.LT_Bonds;
                                _obj.LT_LoanBank = obj.LT_LoanBank;
                                _obj.LT_LoanOtherParties = obj.LT_LoanOtherParties;
                                _obj.LT_Deferred_pay_liabilities = obj.LT_Deferred_pay_liabilities;
                                _obj.LT_Deposits = obj.LT_Deposits;
                                _obj.LT_Loans_adv_related_parties = obj.LT_Loans_adv_related_parties;
                                _obj.LT_maturities = obj.LT_maturities;
                                _obj.LT_Otherloans_advances = obj.LT_Otherloans_advances;
                                _obj.LT_Tot_borrowings = obj.LT_Tot_borrowings;
                                _obj.LT_Aggregate_Amt = obj.LT_Aggregate_Amt;

                                //Section B
                                _obj.ST_LoanBank = obj.ST_LoanBank;
                                _obj.ST_LoanOtherParties = obj.ST_LoanOtherParties;
                                _obj.ST_Loans_advances_related_parties = obj.ST_Loans_advances_related_parties;
                                _obj.ST_Deposits = obj.ST_Deposits;
                                _obj.ST_Loans_adv_related_parties = obj.ST_Loans_adv_related_parties;
                                _obj.ST_Tot_borrowings = obj.ST_Tot_borrowings;
                                _obj.ST_Aggregate_Amt = obj.ST_Aggregate_Amt;

                                //Section C
                                _obj.LTA_Capital_adv = obj.LTA_Capital_adv;
                                _obj.LTA_Security_Deposits = obj.LTA_Security_Deposits;
                                _obj.LTA_Loans_adv_related_parties = obj.LTA_Loans_adv_related_parties;
                                _obj.LTA_Otherloans_advances = obj.LTA_Otherloans_advances;
                                _obj.LTA_TotLoan_Adv = obj.LTA_TotLoan_Adv;
                                _obj.LTA_FromRelated_Parties = obj.LTA_FromRelated_Parties;
                                _obj.LTA_FromOthers = obj.LTA_FromOthers;
                                _obj.LTA_Net_loan_Adv = obj.LTA_Net_loan_Adv;
                                _obj.LTA_Loans_Adv_dueByDir = obj.LTA_Loans_Adv_dueByDir;

                                //Section D
                                _obj.LTAD_Capital_adv = obj.LTAD_Capital_adv;
                                _obj.LTAD_Security_Deposits = obj.LTAD_Security_Deposits;
                                _obj.LTAD_Loans_adv_related_parties = obj.LTAD_Loans_adv_related_parties;
                                _obj.LTAD_Otherloans_advances = obj.LTAD_Otherloans_advances;
                                _obj.LTAD_TotLoan_Adv = obj.LTAD_TotLoan_Adv;
                                _obj.LTAD_FromRelated_Parties = obj.LTAD_FromRelated_Parties;
                                _obj.LTAD_FromOthers = obj.LTAD_FromOthers;
                                _obj.LTAD_Net_loan_Adv = obj.LTAD_Net_loan_Adv;
                                _obj.LTAD_Loans_Adv_dueByDir = obj.LTAD_Loans_Adv_dueByDir;

                                // Section E 
                                _obj.TR_Secured_ESM = obj.TR_Secured_ESM;
                                _obj.TR_Secured_WSM = obj.TR_Secured_WSM;
                                _obj.TR_Unsecured_ESM = obj.TR_Unsecured_ESM;
                                _obj.TR_Unsecured_WSM = obj.TR_Unsecured_WSM;
                                _obj.TR_Doubtful_ESM = obj.TR_Doubtful_ESM;
                                _obj.TR_Doubtful_WSM = obj.TR_Doubtful_WSM;
                                _obj.TR_TotalTrade_ESM = obj.TR_TotalTrade_ESM;
                                _obj.TR_TotalTrade_WSM = obj.TR_TotalTrade_WSM;
                                _obj.TR_Provision_ESM = obj.TR_Provision_ESM;
                                _obj.TR_Provision_WSM = obj.TR_Provision_WSM;
                                _obj.TR_NetTrade_ESM = obj.TR_NetTrade_ESM;
                                _obj.TR_NetTrade_WSM = obj.TR_NetTrade_WSM;
                                _obj.TR_DebtDue_ESM = obj.TR_DebtDue_ESM;
                                _obj.TR_DebtDue_WSM = obj.TR_DebtDue_WSM;

                                entities.BM_Form_AOC4_2_DetailedBalanceSheet.Add(_obj);
                                entities.SaveChanges();

                                var objAOCForm = (from row in entities.BM_Form_AOC4
                                                  where row.Id == obj.BalanceSheetAOC4Id
                                                  select row).FirstOrDefault();
                                if (objAOCForm != null)
                                {
                                    objAOCForm.Detailed_A = true;
                                    objAOCForm.Detailed_B = true;
                                    objAOCForm.Detailed_C = true;
                                    objAOCForm.Detailed_D = true;
                                    objAOCForm.Detailed_E = true;

                                    objAOCForm.UpdatedBy = userId;
                                    objAOCForm.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }

                                obj.DetailedBalanceSheetId = _obj.Id;
                                result.Success = true;
                                result.Message = SecretarialConst.Messages.saveSuccess;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public AOC4_DetailedBalanceSheetPrevVM GetDetailedBalanceSheet(long id, int customerId)
        {
            var result = new AOC4_DetailedBalanceSheetPrevVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Form_AOC4_2_DetailedBalanceSheet
                                where row.AOC4Id == id
                                select new AOC4_DetailedBalanceSheetVM
                                {
                                    DetailedBalanceSheetId = row.Id,
                                    BalanceSheetAOC4Id = row.AOC4Id,
                                    LT_ReportingPeriod = row.LT_ReportingPeriod,
                                    LT_Bonds = row.LT_Bonds,
                                    LT_LoanBank = row.LT_LoanBank,
                                    LT_LoanOtherParties = row.LT_LoanOtherParties,
                                    LT_Deferred_pay_liabilities = row.LT_Deferred_pay_liabilities,
                                    LT_Deposits = row.LT_Deposits,
                                    LT_Loans_adv_related_parties = row.LT_Loans_adv_related_parties,
                                    LT_maturities = row.LT_maturities,
                                    LT_Otherloans_advances = row.LT_Otherloans_advances,
                                    LT_Tot_borrowings = row.LT_Tot_borrowings,
                                    //LT_Tot_borrowings = (row.LT_Bonds + row.LT_LoanBank + row.LT_LoanOtherParties + row.LT_Deferred_pay_liabilities + row.LT_Deposits + row.LT_Loans_adv_related_parties + row.LT_maturities + row.LT_Otherloans_advances),
                                    LT_Aggregate_Amt = row.LT_Aggregate_Amt,
                                    ST_LoanBank = row.ST_LoanBank,
                                    ST_LoanOtherParties = row.ST_LoanOtherParties,
                                    ST_Loans_advances_related_parties = row.ST_Loans_advances_related_parties,
                                    ST_Deposits = row.ST_Deposits,
                                    ST_Loans_adv_related_parties = row.ST_Loans_adv_related_parties,
                                    ST_Tot_borrowings = row.ST_Tot_borrowings,
                                    ST_Aggregate_Amt = row.ST_Aggregate_Amt,
                                    LTA_Capital_adv = row.LTA_Capital_adv,
                                    LTA_Security_Deposits = row.LTA_Security_Deposits,
                                    LTA_Loans_adv_related_parties = row.LTA_Loans_adv_related_parties,
                                    LTA_Otherloans_advances = row.LTA_Otherloans_advances,
                                    LTA_TotLoan_Adv = row.LTA_TotLoan_Adv,
                                    LTA_FromRelated_Parties = row.LTA_FromRelated_Parties,
                                    LTA_FromOthers = row.LTA_FromOthers,
                                    LTA_Net_loan_Adv = row.LTA_Net_loan_Adv,
                                    LTA_Loans_Adv_dueByDir = row.LTA_Loans_Adv_dueByDir,
                                    LTAD_Capital_adv = row.LTAD_Capital_adv,
                                    LTAD_Security_Deposits = row.LTAD_Security_Deposits,
                                    LTAD_Loans_adv_related_parties = row.LTAD_Loans_adv_related_parties,
                                    LTAD_Otherloans_advances = row.LTAD_Otherloans_advances,
                                    LTAD_TotLoan_Adv = row.LTAD_TotLoan_Adv,
                                    LTAD_FromRelated_Parties = row.LTAD_FromRelated_Parties,
                                    LTAD_FromOthers = row.LTAD_FromOthers,
                                    LTAD_Net_loan_Adv = row.LTAD_Net_loan_Adv,
                                    LTAD_Loans_Adv_dueByDir = row.LTAD_Loans_Adv_dueByDir,
                                    TR_Secured_ESM = row.TR_Secured_ESM,
                                    TR_Secured_WSM = row.TR_Secured_WSM,
                                    TR_Unsecured_ESM = row.TR_Unsecured_ESM,
                                    TR_Unsecured_WSM = row.TR_Unsecured_WSM,
                                    TR_Doubtful_ESM = row.TR_Doubtful_ESM,
                                    TR_Doubtful_WSM = row.TR_Doubtful_WSM,
                                    TR_TotalTrade_ESM = row.TR_TotalTrade_ESM,
                                    TR_TotalTrade_WSM = row.TR_TotalTrade_WSM,
                                    TR_Provision_ESM = row.TR_Provision_ESM,
                                    TR_Provision_WSM = row.TR_Provision_WSM,
                                    TR_NetTrade_ESM = row.TR_NetTrade_ESM,
                                    TR_NetTrade_WSM = row.TR_NetTrade_WSM,
                                    TR_DebtDue_ESM = row.TR_DebtDue_ESM,
                                    TR_DebtDue_WSM = row.TR_DebtDue_WSM,

                                }).ToList();

                    if (_obj != null)
                    {
                        result.Current = new AOC4_DetailedBalanceSheetVM();
                        result.Current = (from row in _obj
                                          where row.LT_ReportingPeriod == "C"
                                          select row).FirstOrDefault();

                        result.Previous = new AOC4_DetailedBalanceSheetVM();
                        result.Previous = (from row in _obj
                                           where row.LT_ReportingPeriod == "P"
                                           select row).FirstOrDefault();
                    }

                    #region Set Default Values
                    if (result.Current == null)
                    {
                        result.Current = new AOC4_DetailedBalanceSheetVM()
                        {
                            DetailedBalanceSheetId = 0,
                            BalanceSheetAOC4Id = 0,
                            LT_ReportingPeriod = "C",
                            LT_Bonds = 0,
                            LT_LoanBank = 0,
                            LT_LoanOtherParties = 0,
                            LT_Deferred_pay_liabilities = 0,
                            LT_Deposits = 0,
                            LT_Loans_adv_related_parties = 0,
                            LT_maturities = 0,
                            LT_Otherloans_advances = 0,
                            LT_Tot_borrowings = 0,
                            LT_Aggregate_Amt = 0,
                            ST_LoanBank = 0,
                            ST_LoanOtherParties = 0,
                            ST_Loans_advances_related_parties = 0,
                            ST_Deposits = 0,
                            ST_Loans_adv_related_parties = 0,
                            ST_Tot_borrowings = 0,
                            ST_Aggregate_Amt = 0,
                            LTA_Capital_adv = 0,
                            LTA_Security_Deposits = 0,
                            LTA_Loans_adv_related_parties = 0,
                            LTA_Otherloans_advances = 0,
                            LTA_TotLoan_Adv = 0,
                            LTA_FromRelated_Parties = 0,
                            LTA_FromOthers = 0,
                            LTA_Net_loan_Adv = 0,
                            LTA_Loans_Adv_dueByDir = 0,
                            LTAD_Capital_adv = 0,
                            LTAD_Security_Deposits = 0,
                            LTAD_Loans_adv_related_parties = 0,
                            LTAD_Otherloans_advances = 0,
                            LTAD_TotLoan_Adv = 0,
                            LTAD_FromRelated_Parties = 0,
                            LTAD_FromOthers = 0,
                            LTAD_Net_loan_Adv = 0,
                            LTAD_Loans_Adv_dueByDir = 0,
                            TR_Secured_ESM = 0,
                            TR_Secured_WSM = 0,
                            TR_Unsecured_ESM = 0,
                            TR_Unsecured_WSM = 0,
                            TR_Doubtful_ESM = 0,
                            TR_Doubtful_WSM = 0,
                            TR_TotalTrade_ESM = 0,
                            TR_TotalTrade_WSM = 0,
                            TR_Provision_ESM = 0,
                            TR_Provision_WSM = 0,
                            TR_NetTrade_ESM = 0,
                            TR_NetTrade_WSM = 0,
                            TR_DebtDue_ESM = 0,
                            TR_DebtDue_WSM = 0,
                        };
                    }

                    if (result.Previous == null)
                    {
                        result.Previous = new AOC4_DetailedBalanceSheetVM()
                        {
                            DetailedBalanceSheetId = 0,
                            BalanceSheetAOC4Id = 0,
                            LT_ReportingPeriod = "P",
                            LT_Bonds = 0,
                            LT_LoanBank = 0,
                            LT_LoanOtherParties = 0,
                            LT_Deferred_pay_liabilities = 0,
                            LT_Deposits = 0,
                            LT_Loans_adv_related_parties = 0,
                            LT_maturities = 0,
                            LT_Otherloans_advances = 0,
                            LT_Tot_borrowings = 0,
                            LT_Aggregate_Amt = 0,
                            ST_LoanBank = 0,
                            ST_LoanOtherParties = 0,
                            ST_Loans_advances_related_parties = 0,
                            ST_Deposits = 0,
                            ST_Loans_adv_related_parties = 0,
                            ST_Tot_borrowings = 0,
                            ST_Aggregate_Amt = 0,
                            LTA_Capital_adv = 0,
                            LTA_Security_Deposits = 0,
                            LTA_Loans_adv_related_parties = 0,
                            LTA_Otherloans_advances = 0,
                            LTA_TotLoan_Adv = 0,
                            LTA_FromRelated_Parties = 0,
                            LTA_FromOthers = 0,
                            LTA_Net_loan_Adv = 0,
                            LTA_Loans_Adv_dueByDir = 0,
                            LTAD_Capital_adv = 0,
                            LTAD_Security_Deposits = 0,
                            LTAD_Loans_adv_related_parties = 0,
                            LTAD_Otherloans_advances = 0,
                            LTAD_TotLoan_Adv = 0,
                            LTAD_FromRelated_Parties = 0,
                            LTAD_FromOthers = 0,
                            LTAD_Net_loan_Adv = 0,
                            LTAD_Loans_Adv_dueByDir = 0,
                            TR_Secured_ESM = 0,
                            TR_Secured_WSM = 0,
                            TR_Unsecured_ESM = 0,
                            TR_Unsecured_WSM = 0,
                            TR_Doubtful_ESM = 0,
                            TR_Doubtful_WSM = 0,
                            TR_TotalTrade_ESM = 0,
                            TR_TotalTrade_WSM = 0,
                            TR_Provision_ESM = 0,
                            TR_Provision_WSM = 0,
                            TR_NetTrade_ESM = 0,
                            TR_NetTrade_WSM = 0,
                            TR_DebtDue_ESM = 0,
                            TR_DebtDue_WSM = 0,
                        };
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public long GetDetailedBalanceSheetIdByAOC4Id(long AOC4Id, int customerId)
        {
            long result = 0;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var isExists = (from row in entities.BM_Form_AOC4_2_DetailedBalanceSheet
                                    where row.AOC4Id == AOC4Id
                                    select row.Id).Any();

                    if (isExists)
                    {
                        result = AOC4Id;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public AOC4_DetailedBalanceSheetPrevVM GetDetailedBalanceSheetDefaultDataForExcel(long id, int customerId)
        {
            AOC4_DetailedBalanceSheetPrevVM result = null;
            bool lastYearflag = false;
            //Get Current AOC4 id if balance sheet data Exists
            var AOC4Id = GetDetailedBalanceSheetIdByAOC4Id(id, customerId);

            if (AOC4Id > 0)
            {
                lastYearflag = false;
            }
            else
            {
                //Get last Year AOC4 id if balance sheet data Exists
                AOC4Id = GetPreviousPeriodIdByAOC4Id(id, customerId);
                if (AOC4Id > 0)
                {
                    lastYearflag = true;
                }
                else
                {
                    lastYearflag = false;
                }

            }

            if (lastYearflag == false)
            {
                result = GetDetailedBalanceSheet(AOC4Id, customerId);
            }
            else
            {
                var result1 = GetDetailedBalanceSheet(AOC4Id, customerId);
                result = new AOC4_DetailedBalanceSheetPrevVM();
                result.Current = new AOC4_DetailedBalanceSheetVM() { LT_ReportingPeriod = "C" };

                result.Previous = new AOC4_DetailedBalanceSheetVM();
                if (result1 != null)
                {
                    if (result1.Current != null)
                    {
                        result.Previous = result1.Current;
                        result.Previous.LT_ReportingPeriod = "P";
                    }
                }
            }

            return result;
        }

        #endregion

        #region Financial parameters
        public Message SaveFinancialParameter(AOC4_FinancialParameterVM _objaocExcel, int userID)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _objExcel = (from row in entities.BM_Form_AOC4_3_FinancialParameters
                                     where row.AOC4Id == _objaocExcel.FinancialParameterAOC4Id
                                     select row).FirstOrDefault();

                    if (_objExcel != null)
                    {
                        _objExcel.AOC4Id = _objaocExcel.FinancialParameterAOC4Id;
                        _objExcel.Amt_Of_issue_Alloted = _objaocExcel.Amt_Of_issue_Alloted;
                        _objExcel.Share_app_Money_1 = _objaocExcel.Share_app_Money_1;
                        _objExcel.Share_app_Money_2 = _objaocExcel.Share_app_Money_2;
                        _objExcel.Share_app_Money_3 = _objaocExcel.Share_app_Money_3;
                        _objExcel.Share_app_Money_4 = _objaocExcel.Share_app_Money_4;
                        _objExcel.Paid_up_capital_1 = _objaocExcel.Paid_up_capital_1;
                        _objExcel.Paid_up_capital_2 = _objaocExcel.Paid_up_capital_2;
                        _objExcel.NoOfShares = _objaocExcel.NoOfShares;
                        _objExcel.Deposit_Accepted = _objaocExcel.Deposit_Accepted;
                        _objExcel.Deposit_Matured_1 = _objaocExcel.Deposit_Matured_1;
                        _objExcel.Deposit_Matured_2 = _objaocExcel.Deposit_Matured_2;
                        _objExcel.Deposit_Matured_3 = _objaocExcel.Deposit_Matured_3;
                        _objExcel.Unclaimed_Debentures = _objaocExcel.Unclaimed_Debentures;
                        _objExcel.Claimed_Debentures = _objaocExcel.Claimed_Debentures;
                        _objExcel.InterestOnDepositAccrued = _objaocExcel.InterestOnDepositAccrued;
                        _objExcel.Unpaid_divedend = _objaocExcel.Unpaid_divedend;
                        _objExcel.InvestmentInCompany_1 = _objaocExcel.InvestmentInCompany_1;
                        _objExcel.InvestmentInCompany_2 = _objaocExcel.InvestmentInCompany_2;
                        _objExcel.Capital_Reserves = _objaocExcel.Capital_Reserves;
                        _objExcel.Amt_TransferforIEPF = _objaocExcel.Amt_TransferforIEPF;
                        _objExcel.Inter_Corp_Deposit = _objaocExcel.Inter_Corp_Deposit;
                        _objExcel.GrossValueOfTransaction = _objaocExcel.GrossValueOfTransaction;
                        _objExcel.CapitalSubsidies = _objaocExcel.CapitalSubsidies;
                        _objExcel.CallsUnpaidByDirector = _objaocExcel.CallsUnpaidByDirector;
                        _objExcel.CallsUnpaidByOthers = _objaocExcel.CallsUnpaidByOthers;
                        _objExcel.Forfeited_Shares_1 = _objaocExcel.Forfeited_Shares_1;
                        _objExcel.Forfeited_Shares_2 = _objaocExcel.Forfeited_Shares_2;
                        _objExcel.Borrowing_foreign_1 = _objaocExcel.Borrowing_foreign_1;
                        _objExcel.Borrowing_foreign_2 = _objaocExcel.Borrowing_foreign_2;
                        _objExcel.InterCorporate_1 = _objaocExcel.InterCorporate_1;
                        _objExcel.InterCorporate_2 = _objaocExcel.InterCorporate_2;
                        _objExcel.CommercialPaper = _objaocExcel.CommercialPaper;
                        _objExcel.ConversionOfWarrant_1 = _objaocExcel.ConversionOfWarrant_1;
                        _objExcel.ConversionOfWarrant_2 = _objaocExcel.ConversionOfWarrant_2;
                        _objExcel.ConversionOfWarrant_3 = _objaocExcel.ConversionOfWarrant_3;
                        _objExcel.WarrantsIssueInForeignCurrency = _objaocExcel.WarrantsIssueInForeignCurrency;
                        _objExcel.WarrantsIssueInRupees = _objaocExcel.WarrantsIssueInRupees;
                        _objExcel.DefaultInPayment_1 = _objaocExcel.DefaultInPayment_1;
                        _objExcel.DefaultInPayment_2 = _objaocExcel.DefaultInPayment_2;
                        _objExcel.WheatheOperatingLease = _objaocExcel.WheatheOperatingLease;
                        _objExcel.ProvideDetailsOfConversion = _objaocExcel.ProvideDetailsOfConversion;
                        _objExcel.NetWorthOfComp = _objaocExcel.NetWorthOfComp;
                        _objExcel.NoOfShareHolders = _objaocExcel.NoOfShareHolders;
                        _objExcel.SecuredLoan = _objaocExcel.SecuredLoan;
                        _objExcel.GrossFixedAssets = _objaocExcel.GrossFixedAssets;
                        _objExcel.Depreciation_Amortization = _objaocExcel.Depreciation_Amortization;
                        _objExcel.Misc_Expenditure = _objaocExcel.Misc_Expenditure;
                        _objExcel.UnhedgedForeign = _objaocExcel.UnhedgedForeign;

                        _objExcel.IsActive = true;
                        _objExcel.UpdatedBy = userID;
                        _objExcel.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        result.Success = true;
                        result.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        _objExcel = new BM_Form_AOC4_3_FinancialParameters();

                        _objExcel.AOC4Id = _objaocExcel.FinancialParameterAOC4Id;
                        _objExcel.Amt_Of_issue_Alloted = _objaocExcel.Amt_Of_issue_Alloted;
                        _objExcel.Share_app_Money_1 = _objaocExcel.Share_app_Money_1;
                        _objExcel.Share_app_Money_2 = _objaocExcel.Share_app_Money_2;
                        _objExcel.Share_app_Money_3 = _objaocExcel.Share_app_Money_3;
                        _objExcel.Share_app_Money_4 = _objaocExcel.Share_app_Money_4;
                        _objExcel.Paid_up_capital_1 = _objaocExcel.Paid_up_capital_1;
                        _objExcel.Paid_up_capital_2 = _objaocExcel.Paid_up_capital_2;
                        _objExcel.NoOfShares = _objaocExcel.NoOfShares;
                        _objExcel.Deposit_Accepted = _objaocExcel.Deposit_Accepted;
                        _objExcel.Deposit_Matured_1 = _objaocExcel.Deposit_Matured_1;
                        _objExcel.Deposit_Matured_2 = _objaocExcel.Deposit_Matured_2;
                        _objExcel.Deposit_Matured_3 = _objaocExcel.Deposit_Matured_3;
                        _objExcel.Unclaimed_Debentures = _objaocExcel.Unclaimed_Debentures;
                        _objExcel.Claimed_Debentures = _objaocExcel.Claimed_Debentures;
                        _objExcel.InterestOnDepositAccrued = _objaocExcel.InterestOnDepositAccrued;
                        _objExcel.Unpaid_divedend = _objaocExcel.Unpaid_divedend;
                        _objExcel.InvestmentInCompany_1 = _objaocExcel.InvestmentInCompany_1;
                        _objExcel.InvestmentInCompany_2 = _objaocExcel.InvestmentInCompany_2;
                        _objExcel.Capital_Reserves = _objaocExcel.Capital_Reserves;
                        _objExcel.Amt_TransferforIEPF = _objaocExcel.Amt_TransferforIEPF;
                        _objExcel.Inter_Corp_Deposit = _objaocExcel.Inter_Corp_Deposit;
                        _objExcel.GrossValueOfTransaction = _objaocExcel.GrossValueOfTransaction;
                        _objExcel.CapitalSubsidies = _objaocExcel.CapitalSubsidies;
                        _objExcel.CallsUnpaidByDirector = _objaocExcel.CallsUnpaidByDirector;
                        _objExcel.CallsUnpaidByOthers = _objaocExcel.CallsUnpaidByOthers;
                        _objExcel.Forfeited_Shares_1 = _objaocExcel.Forfeited_Shares_1;
                        _objExcel.Forfeited_Shares_2 = _objaocExcel.Forfeited_Shares_2;
                        _objExcel.Borrowing_foreign_1 = _objaocExcel.Borrowing_foreign_1;
                        _objExcel.Borrowing_foreign_2 = _objaocExcel.Borrowing_foreign_2;
                        _objExcel.InterCorporate_1 = _objaocExcel.InterCorporate_1;
                        _objExcel.InterCorporate_2 = _objaocExcel.InterCorporate_2;
                        _objExcel.CommercialPaper = _objaocExcel.CommercialPaper;
                        _objExcel.ConversionOfWarrant_1 = _objaocExcel.ConversionOfWarrant_1;
                        _objExcel.ConversionOfWarrant_2 = _objaocExcel.ConversionOfWarrant_2;
                        _objExcel.ConversionOfWarrant_3 = _objaocExcel.ConversionOfWarrant_3;
                        _objExcel.WarrantsIssueInForeignCurrency = _objaocExcel.WarrantsIssueInForeignCurrency;
                        _objExcel.WarrantsIssueInRupees = _objaocExcel.WarrantsIssueInRupees;
                        _objExcel.DefaultInPayment_1 = _objaocExcel.DefaultInPayment_1;
                        _objExcel.DefaultInPayment_2 = _objaocExcel.DefaultInPayment_2;
                        _objExcel.WheatheOperatingLease = _objaocExcel.WheatheOperatingLease;
                        _objExcel.ProvideDetailsOfConversion = _objaocExcel.ProvideDetailsOfConversion;
                        _objExcel.NetWorthOfComp = _objaocExcel.NetWorthOfComp;
                        _objExcel.NoOfShareHolders = _objaocExcel.NoOfShareHolders;
                        _objExcel.SecuredLoan = _objaocExcel.SecuredLoan;
                        _objExcel.GrossFixedAssets = _objaocExcel.GrossFixedAssets;
                        _objExcel.Depreciation_Amortization = _objaocExcel.Depreciation_Amortization;
                        _objExcel.Misc_Expenditure = _objaocExcel.Misc_Expenditure;
                        _objExcel.UnhedgedForeign = _objaocExcel.UnhedgedForeign;
                        _objExcel.IsActive = true;
                        _objExcel.CreatedBy = userID;
                        _objExcel.CreatedOn = DateTime.Now;

                        entities.BM_Form_AOC4_3_FinancialParameters.Add(_objExcel);
                        entities.SaveChanges();

                        var objAOCForm = (from row in entities.BM_Form_AOC4
                                          where row.Id == _objaocExcel.FinancialParameterAOC4Id
                                          select row).FirstOrDefault();
                        if (objAOCForm != null)
                        {
                            objAOCForm.Financial_Parameters = true;

                            objAOCForm.UpdatedBy = userID;
                            objAOCForm.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }

                        result.Success = true;
                        result.Message = SecretarialConst.Messages.saveSuccess;

                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AOC4_FinancialParameterVM GetFinancialParameteritems(long id, int customerId)
        {
            var result = new AOC4_FinancialParameterVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_3_FinancialParameters
                              where row.AOC4Id == id && row.IsActive == true
                              select new AOC4_FinancialParameterVM
                              {
                                  FinancialParameterAOC4Id = (long) row.AOC4Id,
                                  Amt_Of_issue_Alloted = row.Amt_Of_issue_Alloted,
                                  Share_app_Money_1 = row.Share_app_Money_1,
                                  Share_app_Money_2 = row.Share_app_Money_2,
                                  Share_app_Money_3 = row.Share_app_Money_3,
                                  Share_app_Money_4 = row.Share_app_Money_4,
                                  Paid_up_capital_1 = row.Paid_up_capital_1,
                                  Paid_up_capital_2 = row.Paid_up_capital_2,
                                  NoOfShares = row.NoOfShares,
                                  Deposit_Accepted = row.Deposit_Accepted,
                                  Deposit_Matured_1 = row.Deposit_Matured_1,
                                  Deposit_Matured_2 = row.Deposit_Matured_2,
                                  Deposit_Matured_3 = row.Deposit_Matured_3,
                                  Unclaimed_Debentures = row.Unclaimed_Debentures,
                                  Claimed_Debentures = row.Claimed_Debentures,
                                  InterestOnDepositAccrued = row.InterestOnDepositAccrued,
                                  Unpaid_divedend = row.Unpaid_divedend,
                                  InvestmentInCompany_1 = row.InvestmentInCompany_1,
                                  InvestmentInCompany_2 = row.InvestmentInCompany_2,
                                  Capital_Reserves = row.Capital_Reserves,
                                  Amt_TransferforIEPF = row.Amt_TransferforIEPF,
                                  Inter_Corp_Deposit = row.Inter_Corp_Deposit,
                                  GrossValueOfTransaction = row.GrossValueOfTransaction,
                                  CapitalSubsidies = row.CapitalSubsidies,
                                  CallsUnpaidByDirector = row.CallsUnpaidByDirector,
                                  CallsUnpaidByOthers = row.CallsUnpaidByOthers,
                                  Forfeited_Shares_1 = row.Forfeited_Shares_1,
                                  Forfeited_Shares_2 = row.Forfeited_Shares_2,
                                  Borrowing_foreign_1 = row.Borrowing_foreign_1,
                                  Borrowing_foreign_2 = row.Borrowing_foreign_2,
                                  InterCorporate_1 = row.InterCorporate_1,
                                  InterCorporate_2 = row.InterCorporate_2,
                                  CommercialPaper = row.CommercialPaper,
                                  ConversionOfWarrant_1 = row.ConversionOfWarrant_1,
                                  ConversionOfWarrant_2 = row.ConversionOfWarrant_2,
                                  ConversionOfWarrant_3 = row.ConversionOfWarrant_3,
                                  WarrantsIssueInForeignCurrency = row.WarrantsIssueInForeignCurrency,
                                  WarrantsIssueInRupees = row.WarrantsIssueInRupees,
                                  DefaultInPayment_1 = row.DefaultInPayment_1,
                                  DefaultInPayment_2 = row.DefaultInPayment_2,
                                  WheatheOperatingLease = row.WheatheOperatingLease,
                                  ProvideDetailsOfConversion = row.ProvideDetailsOfConversion,
                                  NetWorthOfComp = row.NetWorthOfComp,
                                  NoOfShareHolders = row.NoOfShareHolders,
                                  SecuredLoan = row.SecuredLoan,
                                  GrossFixedAssets = row.GrossFixedAssets,
                                  Depreciation_Amortization = row.Depreciation_Amortization,
                                  Misc_Expenditure = row.Misc_Expenditure,
                                  UnhedgedForeign = row.UnhedgedForeign,
                              }).FirstOrDefault();

                    #region Set Default Values
                    if (result == null)
                    {
                        result = new AOC4_FinancialParameterVM()
                        {
                            FinancialParameterAOC4Id = 0,
                            Amt_Of_issue_Alloted = 0,
                            Share_app_Money_1 = 0,
                            Share_app_Money_2 = 0,
                            Share_app_Money_3 = 0,
                            Share_app_Money_4 = 0,
                            Paid_up_capital_1 = 0,
                            Paid_up_capital_2 = 0,
                            NoOfShares = 0,
                            Deposit_Accepted = 0,
                            Deposit_Matured_1 = 0,
                            Deposit_Matured_2 = 0,
                            Deposit_Matured_3 = 0,
                            Unclaimed_Debentures = 0,
                            Claimed_Debentures = 0,
                            InterestOnDepositAccrued = 0,
                            Unpaid_divedend = 0,
                            InvestmentInCompany_1 = 0,
                            InvestmentInCompany_2 = 0,
                            Capital_Reserves = 0,
                            Amt_TransferforIEPF = 0,
                            Inter_Corp_Deposit = 0,
                            GrossValueOfTransaction = 0,
                            CapitalSubsidies = 0,
                            CallsUnpaidByDirector = 0,
                            CallsUnpaidByOthers = 0,
                            Forfeited_Shares_1 = 0,
                            Forfeited_Shares_2 = 0,
                            Borrowing_foreign_1 = 0,
                            Borrowing_foreign_2 = 0,
                            InterCorporate_1 = 0,
                            InterCorporate_2 = 0,
                            CommercialPaper = 0,
                            ConversionOfWarrant_1 = 0,
                            ConversionOfWarrant_2 = 0,
                            ConversionOfWarrant_3 = 0,
                            WarrantsIssueInForeignCurrency = 0,
                            WarrantsIssueInRupees = 0,
                            DefaultInPayment_1 = 0,
                            DefaultInPayment_2 = 0,
                            //WheatheOperatingLease = 0,
                            ProvideDetailsOfConversion = "",
                            NetWorthOfComp = 0,
                            NoOfShareHolders = 0,
                            SecuredLoan = 0,
                            GrossFixedAssets = 0,
                            Depreciation_Amortization = 0,
                            Misc_Expenditure = 0,
                            UnhedgedForeign = 0,

                        };
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Shared Capital Raised
        public Message SaveSharedCapitalRaised(ShareCapitalRaisedVM _objSharecapital, int userID)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _objCapital = (from row in entities.BM_Form_AOC4_4_ShareCapitalRaised
                                       where row.AOC4Id == _objSharecapital.ShareCapitalAOC4Id
                                       select row).FirstOrDefault();

                    if (_objCapital != null)
                    {
                        _objCapital.AOC4Id = _objSharecapital.ShareCapitalAOC4Id;
                        _objCapital.PublicIssue_E = _objSharecapital.PublicIssue_E;
                        _objCapital.PublicIssue_P = _objSharecapital.PublicIssue_P;
                        _objCapital.PublicIssue_T = _objSharecapital.PublicIssue_T;
                        _objCapital.BonusIssue_E = _objSharecapital.BonusIssue_E;
                        _objCapital.BonusIssue_P = _objSharecapital.BonusIssue_P;
                        _objCapital.BonusIssue_T = _objSharecapital.BonusIssue_T;
                        _objCapital.RightIssue_E = _objSharecapital.RightIssue_E;
                        _objCapital.RightIssue_P = _objSharecapital.RightIssue_P;
                        _objCapital.RightIssue_T = _objSharecapital.RightIssue_T;
                        _objCapital.PvtPlacementarising_E = _objSharecapital.PvtPlacementarising_E;
                        _objCapital.PvtPlacementarising_P = _objSharecapital.PvtPlacementarising_P;
                        _objCapital.PvtPlacementarising_T = _objSharecapital.PvtPlacementarising_T;
                        _objCapital.OtherPvtPlacement_E = _objSharecapital.OtherPvtPlacement_E;
                        _objCapital.OtherPvtPlacement_P = _objSharecapital.OtherPvtPlacement_P;
                        _objCapital.OtherPvtPlacement_T = _objSharecapital.OtherPvtPlacement_T;
                        _objCapital.PrferentialAllotment_E = _objSharecapital.PrferentialAllotment_E;
                        _objCapital.PrferentialAllotment_P = _objSharecapital.PrferentialAllotment_P;
                        _objCapital.PrferentialAllotment_T = _objSharecapital.PrferentialAllotment_T;
                        _objCapital.OtherPreferentialallotment_E = _objSharecapital.OtherPreferentialallotment_E;
                        _objCapital.OtherPreferentialallotment_P = _objSharecapital.OtherPreferentialallotment_P;
                        _objCapital.OtherPreferentialallotment_T = _objSharecapital.OtherPreferentialallotment_T;
                        _objCapital.EmpStockOptionPlan_E = _objSharecapital.EmpStockOptionPlan_E;
                        _objCapital.EmpStockOptionPlan_P = _objSharecapital.EmpStockOptionPlan_P;
                        _objCapital.EmpStockOptionPlan_T = _objSharecapital.EmpStockOptionPlan_T;
                        _objCapital.Others_E = _objSharecapital.Others_E;
                        _objCapital.Others_P = _objSharecapital.Others_P;
                        _objCapital.Others_T = _objSharecapital.Others_T;
                        _objCapital.TotalAmtShares_E = _objSharecapital.TotalAmtShares_E;
                        _objCapital.TotalAmtShares_P = _objSharecapital.TotalAmtShares_P;
                        _objCapital.TotalAmtShares_T = _objSharecapital.TotalAmtShares_T;

                        _objCapital.IsActive = true;
                        _objCapital.UpdatedBy = userID;
                        _objCapital.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        result.Success = true;
                        result.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        _objCapital = new BM_Form_AOC4_4_ShareCapitalRaised();
                        _objCapital.AOC4Id = _objSharecapital.ShareCapitalAOC4Id;
                        _objCapital.PublicIssue_E = _objSharecapital.PublicIssue_E;
                        _objCapital.PublicIssue_P = _objSharecapital.PublicIssue_P;
                        _objCapital.PublicIssue_T = _objSharecapital.PublicIssue_T;
                        _objCapital.BonusIssue_E = _objSharecapital.BonusIssue_E;
                        _objCapital.BonusIssue_P = _objSharecapital.BonusIssue_P;
                        _objCapital.BonusIssue_T = _objSharecapital.BonusIssue_T;
                        _objCapital.RightIssue_E = _objSharecapital.RightIssue_E;
                        _objCapital.RightIssue_P = _objSharecapital.RightIssue_P;
                        _objCapital.RightIssue_T = _objSharecapital.RightIssue_T;
                        _objCapital.PvtPlacementarising_E = _objSharecapital.PvtPlacementarising_E;
                        _objCapital.PvtPlacementarising_P = _objSharecapital.PvtPlacementarising_P;
                        _objCapital.PvtPlacementarising_T = _objSharecapital.PvtPlacementarising_T;
                        _objCapital.OtherPvtPlacement_E = _objSharecapital.OtherPvtPlacement_E;
                        _objCapital.OtherPvtPlacement_P = _objSharecapital.OtherPvtPlacement_P;
                        _objCapital.OtherPvtPlacement_T = _objSharecapital.OtherPvtPlacement_T;
                        _objCapital.PrferentialAllotment_E = _objSharecapital.PrferentialAllotment_E;
                        _objCapital.PrferentialAllotment_P = _objSharecapital.PrferentialAllotment_P;
                        _objCapital.PrferentialAllotment_T = _objSharecapital.PrferentialAllotment_T;
                        _objCapital.OtherPreferentialallotment_E = _objSharecapital.OtherPreferentialallotment_E;
                        _objCapital.OtherPreferentialallotment_P = _objSharecapital.OtherPreferentialallotment_P;
                        _objCapital.OtherPreferentialallotment_T = _objSharecapital.OtherPreferentialallotment_T;
                        _objCapital.EmpStockOptionPlan_E = _objSharecapital.EmpStockOptionPlan_E;
                        _objCapital.EmpStockOptionPlan_P = _objSharecapital.EmpStockOptionPlan_P;
                        _objCapital.EmpStockOptionPlan_T = _objSharecapital.EmpStockOptionPlan_T;
                        _objCapital.Others_E = _objSharecapital.Others_E;
                        _objCapital.Others_P = _objSharecapital.Others_P;
                        _objCapital.Others_T = _objSharecapital.Others_T;
                        _objCapital.TotalAmtShares_E = _objSharecapital.TotalAmtShares_E;
                        _objCapital.TotalAmtShares_P = _objSharecapital.TotalAmtShares_P;
                        _objCapital.TotalAmtShares_T = _objSharecapital.TotalAmtShares_T;

                        _objCapital.IsActive = true;
                        _objCapital.CreatedBy = userID;
                        _objCapital.CreatedOn = DateTime.Now;

                        entities.BM_Form_AOC4_4_ShareCapitalRaised.Add(_objCapital);
                        entities.SaveChanges();

                        var objAOCForm = (from row in entities.BM_Form_AOC4
                                          where row.Id == _objSharecapital.ShareCapitalAOC4Id
                                          select row).FirstOrDefault();
                        if (objAOCForm != null)
                        {
                            objAOCForm.ShareCapital = true;

                            objAOCForm.UpdatedBy = userID;
                            objAOCForm.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }

                        _objSharecapital.ShareCapitalRaiseId = _objCapital.Id;
                        result.Success = true;
                        result.Message = SecretarialConst.Messages.saveSuccess;

                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public ShareCapitalRaisedVM GetSharedCapitalitems(long id, int customerId)
        {
            var result = new ShareCapitalRaisedVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_4_ShareCapitalRaised
                              where row.AOC4Id == id && row.IsActive == true
                              select new ShareCapitalRaisedVM
                              {
                                  ShareCapitalAOC4Id = (long)row.AOC4Id,
                                  PublicIssue_E = row.PublicIssue_E,
                                  PublicIssue_P = row.PublicIssue_P,
                                  PublicIssue_T = row.PublicIssue_T,
                                  BonusIssue_E = row.BonusIssue_E,
                                  BonusIssue_P = row.BonusIssue_P,
                                  BonusIssue_T = row.BonusIssue_T,
                                  RightIssue_E = row.RightIssue_E,
                                  RightIssue_P = row.RightIssue_P,
                                  RightIssue_T = row.RightIssue_T,
                                  PvtPlacementarising_E = row.PvtPlacementarising_E,
                                  PvtPlacementarising_P = row.PvtPlacementarising_P,
                                  PvtPlacementarising_T = row.PvtPlacementarising_T,
                                  OtherPvtPlacement_E = row.OtherPvtPlacement_E,
                                  OtherPvtPlacement_P = row.OtherPvtPlacement_P,
                                  OtherPvtPlacement_T = row.OtherPvtPlacement_T,
                                  PrferentialAllotment_E = row.PrferentialAllotment_E,
                                  PrferentialAllotment_P = row.PrferentialAllotment_P,
                                  PrferentialAllotment_T = row.PrferentialAllotment_T,
                                  OtherPreferentialallotment_E = row.OtherPreferentialallotment_E,
                                  OtherPreferentialallotment_P = row.OtherPreferentialallotment_P,
                                  OtherPreferentialallotment_T = row.OtherPreferentialallotment_T,
                                  EmpStockOptionPlan_E = row.EmpStockOptionPlan_E,
                                  EmpStockOptionPlan_P = row.EmpStockOptionPlan_P,
                                  EmpStockOptionPlan_T = row.EmpStockOptionPlan_T,
                                  Others_E = row.Others_E,
                                  Others_P = row.Others_P,
                                  Others_T = row.Others_T,
                                  TotalAmtShares_E = row.TotalAmtShares_E,
                                  TotalAmtShares_P = row.TotalAmtShares_P,
                                  TotalAmtShares_T = row.TotalAmtShares_T,

                              }).FirstOrDefault();

                    #region Set Default Values
                    if (result == null)
                    {
                        result = new ShareCapitalRaisedVM()
                        {
                            ShareCapitalAOC4Id = 0,
                            PublicIssue_E = 0,
                            PublicIssue_P = 0,
                            PublicIssue_T = 0,
                            BonusIssue_E = 0,
                            BonusIssue_P = 0,
                            BonusIssue_T = 0,
                            RightIssue_E = 0,
                            RightIssue_P = 0,
                            RightIssue_T = 0,
                            PvtPlacementarising_E = 0,
                            PvtPlacementarising_P = 0,
                            PvtPlacementarising_T = 0,
                            OtherPvtPlacement_E = 0,
                            OtherPvtPlacement_P = 0,
                            OtherPvtPlacement_T = 0,
                            PrferentialAllotment_E = 0,
                            PrferentialAllotment_P = 0,
                            PrferentialAllotment_T = 0,
                            OtherPreferentialallotment_E = 0,
                            OtherPreferentialallotment_P = 0,
                            OtherPreferentialallotment_T = 0,
                            EmpStockOptionPlan_E = 0,
                            EmpStockOptionPlan_P = 0,
                            EmpStockOptionPlan_T = 0,
                            Others_E = 0,
                            Others_P = 0,
                            Others_T = 0,
                            TotalAmtShares_E = 0,
                            TotalAmtShares_P = 0,
                            TotalAmtShares_T = 0,
                        };
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Cost Records and Cost Audit
        public DetailsOfCostRecordsVM GetDetailsOfCostRecords(long id)
        {
            var result = new DetailsOfCostRecordsVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_5_DetailsOfCostRecords
                              where row.AOC4Id == id && row.IsActive == true
                              select new DetailsOfCostRecordsVM
                              {
                                  DetailsOfCostRecordsId = row.Id,
                                  DetailsOfCostRecordsAOC4Id = row.AOC4Id,
                                  MaintenanceOfCR = row.MaintenanceOfCR,
                                  CentralExcise = row.CentralExcise,
                                  AuditOfCR = row.AuditOfCR,
                                  CentralExcise1 = row.CentralExcise1,
                              }).FirstOrDefault();
                }

                if (result == null)
                {
                    result = new DetailsOfCostRecordsVM() { DetailsOfCostRecordsAOC4Id = id };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public DetailsOfCostRecordsVM SaveDetailsOfCostRecords(DetailsOfCostRecordsVM obj, int userId)
        {
            try
            {
                if(obj.MaintenanceOfCR == false)
                {
                    obj.CentralExcise = "";
                    obj.AuditOfCR = false;
                }

                if(obj.AuditOfCR == false)
                {
                    obj.CentralExcise1 = "";
                }
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Form_AOC4_5_DetailsOfCostRecords
                                where row.Id == obj.DetailsOfCostRecordsId && row.AOC4Id == obj.DetailsOfCostRecordsAOC4Id
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.AOC4Id = obj.DetailsOfCostRecordsId;
                        _obj.MaintenanceOfCR = obj.MaintenanceOfCR;
                        _obj.CentralExcise = obj.CentralExcise;
                        _obj.AuditOfCR = obj.AuditOfCR;
                        _obj.CentralExcise1 = obj.CentralExcise1;

                        _obj.IsActive = true;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        _obj = new BM_Form_AOC4_5_DetailsOfCostRecords();
                        _obj.AOC4Id = obj.DetailsOfCostRecordsAOC4Id;
                        _obj.MaintenanceOfCR = obj.MaintenanceOfCR;
                        _obj.CentralExcise = obj.CentralExcise;
                        _obj.AuditOfCR = obj.AuditOfCR;
                        _obj.CentralExcise1 = obj.CentralExcise1;

                        _obj.IsActive = true;
                        _obj.CreatedBy = userId;
                        _obj.CreatedOn = DateTime.Now;

                        entities.BM_Form_AOC4_5_DetailsOfCostRecords.Add(_obj);
                        entities.SaveChanges();

                        var objAOCForm = (from row in entities.BM_Form_AOC4
                                          where row.Id == obj.DetailsOfCostRecordsAOC4Id
                                          select row).FirstOrDefault();
                        if (objAOCForm != null)
                        {
                            objAOCForm.Detailed_CostRecord = true;

                            objAOCForm.UpdatedBy = userId;
                            objAOCForm.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }

                        obj.DetailsOfCostRecordsId = _obj.Id;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Profit & Loss
        public Message SaveProfitLoss(List<AOC4_ProfitLossVM> objList, int userId)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (objList != null)
                    {
                        foreach (var obj in objList)
                        {

                            var _obj = (from row in entities.BM_Form_AOC4_6_P_And_L
                                        where row.AOC4Id == obj.Aoc4Id && row.ReportingPeriod == obj.ReportingPeriod
                                        select row).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.AOC4Id = obj.Aoc4Id;
                                _obj.ReportingPeriod = obj.ReportingPeriod;
                                _obj.FROM_DATE_CR = obj.FROM_DATE_CR;
                                _obj.TO_DATE_CR = obj.TO_DATE_CR;
                                _obj.SALES_GOODS_CR = obj.SALES_GOODS_CR;
                                _obj.SALES_GOODS_T_CR = obj.SALES_GOODS_T_CR;
                                _obj.SALES_SUPPLY_CR = obj.SALES_SUPPLY_CR;
                                _obj.SALES_GOODS1_CR = obj.SALES_GOODS1_CR;
                                _obj.SALE_GOODS_T1_CR = obj.SALE_GOODS_T1_CR;
                                _obj.SALES_SUPPLY1_CR = obj.SALES_SUPPLY1_CR;
                                _obj.OTHER_INCOME_CR = obj.OTHER_INCOME_CR;
                                _obj.TOTAL_REVENUE_CR = obj.TOTAL_REVENUE_CR;
                                _obj.COST_MATERIAL_CR = obj.COST_MATERIAL_CR;
                                _obj.PURCHASE_STOCK_C = obj.PURCHASE_STOCK_C;
                                _obj.FINISHED_GOODS_C = obj.FINISHED_GOODS_C;
                                _obj.WORK_IN_PROG_CR = obj.WORK_IN_PROG_CR;
                                _obj.STOCK_IN_TRADE_C = obj.STOCK_IN_TRADE_C;
                                _obj.EMP_BENEFIT_EX_C = obj.EMP_BENEFIT_EX_C;
                                _obj.MANGERIAL_REM_CR = obj.MANGERIAL_REM_CR;
                                _obj.PAYMENT_AUDTRS_C = obj.PAYMENT_AUDTRS_C;
                                _obj.INSURANCE_EXP_CR = obj.INSURANCE_EXP_CR;
                                _obj.POWER_FUEL_CR = obj.POWER_FUEL_CR;
                                _obj.FINANCE_COST_CR = obj.FINANCE_COST_CR;
                                _obj.DEPRECTN_AMORT_C = obj.DEPRECTN_AMORT_C;
                                _obj.OTHER_EXPENSES_C = obj.OTHER_EXPENSES_C;
                                _obj.TOTAL_EXPENSES_C = obj.TOTAL_EXPENSES_C;
                                _obj.PROFIT_BEFORE_CR = obj.PROFIT_BEFORE_CR;
                                _obj.EXCEPTIONL_ITM_C = obj.EXCEPTIONL_ITM_C;
                                _obj.PROFIT_BEF_TAX_C = obj.PROFIT_BEF_TAX_C;
                                _obj.EXTRAORDINARY_CR = obj.EXTRAORDINARY_CR;
                                _obj.PROF_B_TAX_7_8_C = obj.PROF_B_TAX_7_8_C;
                                _obj.CURRENT_TAX_CR = obj.CURRENT_TAX_CR;
                                _obj.DEFERRED_TAX_CR = obj.DEFERRED_TAX_CR;
                                _obj.PROF_LOSS_OPER_C = obj.PROF_LOSS_OPER_C;
                                _obj.PROF_LOSS_DO_CR = obj.PROF_LOSS_DO_CR;
                                _obj.TAX_EXPNS_DIS_CR = obj.TAX_EXPNS_DIS_CR;
                                _obj.PROF_LOS_12_13_C = obj.PROF_LOS_12_13_C;
                                _obj.PROF_LOS_11_14_C = obj.PROF_LOS_11_14_C;
                                _obj.BASIC_BEFR_EI_CR = obj.BASIC_BEFR_EI_CR;
                                _obj.DILUTED_BEF_EI_C = obj.DILUTED_BEF_EI_C;
                                _obj.BASIC_AFTR_EI_CR = obj.BASIC_AFTR_EI_CR;
                                _obj.DILUTED_AFT_EI_C = obj.DILUTED_AFT_EI_C;


                                _obj.IsActive = true;
                                _obj.UpdatedBy = userId;
                                _obj.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();

                                result.Success = true;
                                result.Message = SecretarialConst.Messages.updateSuccess;
                            }
                            else
                            {
                                _obj = new BM_Form_AOC4_6_P_And_L();

                                _obj.AOC4Id = obj.Aoc4Id;
                                _obj.ReportingPeriod = obj.ReportingPeriod;
                                _obj.FROM_DATE_CR = obj.FROM_DATE_CR;
                                _obj.TO_DATE_CR = obj.TO_DATE_CR;
                                _obj.SALES_GOODS_CR = obj.SALES_GOODS_CR;
                                _obj.SALES_GOODS_T_CR = obj.SALES_GOODS_T_CR;
                                _obj.SALES_SUPPLY_CR = obj.SALES_SUPPLY_CR;
                                _obj.SALES_GOODS1_CR = obj.SALES_GOODS1_CR;
                                _obj.SALE_GOODS_T1_CR = obj.SALE_GOODS_T1_CR;
                                _obj.SALES_SUPPLY1_CR = obj.SALES_SUPPLY1_CR;
                                _obj.OTHER_INCOME_CR = obj.OTHER_INCOME_CR;
                                _obj.TOTAL_REVENUE_CR = obj.TOTAL_REVENUE_CR;
                                _obj.COST_MATERIAL_CR = obj.COST_MATERIAL_CR;
                                _obj.PURCHASE_STOCK_C = obj.PURCHASE_STOCK_C;
                                _obj.FINISHED_GOODS_C = obj.FINISHED_GOODS_C;
                                _obj.WORK_IN_PROG_CR = obj.WORK_IN_PROG_CR;
                                _obj.STOCK_IN_TRADE_C = obj.STOCK_IN_TRADE_C;
                                _obj.EMP_BENEFIT_EX_C = obj.EMP_BENEFIT_EX_C;
                                _obj.MANGERIAL_REM_CR = obj.MANGERIAL_REM_CR;
                                _obj.PAYMENT_AUDTRS_C = obj.PAYMENT_AUDTRS_C;
                                _obj.INSURANCE_EXP_CR = obj.INSURANCE_EXP_CR;
                                _obj.POWER_FUEL_CR = obj.POWER_FUEL_CR;
                                _obj.FINANCE_COST_CR = obj.FINANCE_COST_CR;
                                _obj.DEPRECTN_AMORT_C = obj.DEPRECTN_AMORT_C;
                                _obj.OTHER_EXPENSES_C = obj.OTHER_EXPENSES_C;
                                _obj.TOTAL_EXPENSES_C = obj.TOTAL_EXPENSES_C;
                                _obj.PROFIT_BEFORE_CR = obj.PROFIT_BEFORE_CR;
                                _obj.EXCEPTIONL_ITM_C = obj.EXCEPTIONL_ITM_C;
                                _obj.PROFIT_BEF_TAX_C = obj.PROFIT_BEF_TAX_C;
                                _obj.EXTRAORDINARY_CR = obj.EXTRAORDINARY_CR;
                                _obj.PROF_B_TAX_7_8_C = obj.PROF_B_TAX_7_8_C;
                                _obj.CURRENT_TAX_CR = obj.CURRENT_TAX_CR;
                                _obj.DEFERRED_TAX_CR = obj.DEFERRED_TAX_CR;
                                _obj.PROF_LOSS_OPER_C = obj.PROF_LOSS_OPER_C;
                                _obj.PROF_LOSS_DO_CR = obj.PROF_LOSS_DO_CR;
                                _obj.TAX_EXPNS_DIS_CR = obj.TAX_EXPNS_DIS_CR;
                                _obj.PROF_LOS_12_13_C = obj.PROF_LOS_12_13_C;
                                _obj.PROF_LOS_11_14_C = obj.PROF_LOS_11_14_C;
                                _obj.BASIC_BEFR_EI_CR = obj.BASIC_BEFR_EI_CR;
                                _obj.DILUTED_BEF_EI_C = obj.DILUTED_BEF_EI_C;
                                _obj.BASIC_AFTR_EI_CR = obj.BASIC_AFTR_EI_CR;
                                _obj.DILUTED_AFT_EI_C = obj.DILUTED_AFT_EI_C;

                                _obj.IsActive = true;
                                _obj.CreatedBy = userId;
                                _obj.CreatedOn = DateTime.Now;

                                entities.BM_Form_AOC4_6_P_And_L.Add(_obj);
                                entities.SaveChanges();

                                var objAOCForm = (from row in entities.BM_Form_AOC4
                                                  where row.Id == obj.Aoc4Id
                                                  select row).FirstOrDefault();
                                if (objAOCForm != null)
                                {
                                    objAOCForm.P_And_L = true;

                                    objAOCForm.UpdatedBy = userId;
                                    objAOCForm.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }

                                obj.ProfitLossId = _obj.Id;
                                result.Success = true;
                                result.Message = SecretarialConst.Messages.saveSuccess;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AOC4_ProfitLossPrevVM GetProfitLoss(long id, int customerId)
        {
            var result = new AOC4_ProfitLossPrevVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Form_AOC4_6_P_And_L
                                where row.AOC4Id == id && row.IsActive == true
                                select new AOC4_ProfitLossVM
                                {
                                    ProfitLossId = row.Id,
                                    Aoc4Id = row.AOC4Id,
                                    ReportingPeriod = row.ReportingPeriod,
                                    FROM_DATE_CR = row.FROM_DATE_CR,
                                    TO_DATE_CR = row.TO_DATE_CR,
                                    SALES_GOODS_CR = row.SALES_GOODS_CR,
                                    SALES_GOODS_T_CR = row.SALES_GOODS_T_CR,
                                    SALES_SUPPLY_CR = row.SALES_SUPPLY_CR,
                                    SALES_GOODS1_CR = row.SALES_GOODS1_CR,
                                    SALE_GOODS_T1_CR = row.SALE_GOODS_T1_CR,
                                    SALES_SUPPLY1_CR = row.SALES_SUPPLY1_CR,
                                    OTHER_INCOME_CR = row.OTHER_INCOME_CR,
                                    TOTAL_REVENUE_CR = row.TOTAL_REVENUE_CR,
                                    COST_MATERIAL_CR = row.COST_MATERIAL_CR,
                                    PURCHASE_STOCK_C = row.PURCHASE_STOCK_C,
                                    FINISHED_GOODS_C = row.FINISHED_GOODS_C,
                                    WORK_IN_PROG_CR = row.WORK_IN_PROG_CR,
                                    STOCK_IN_TRADE_C = row.STOCK_IN_TRADE_C,
                                    EMP_BENEFIT_EX_C = row.EMP_BENEFIT_EX_C,
                                    MANGERIAL_REM_CR = row.MANGERIAL_REM_CR,
                                    PAYMENT_AUDTRS_C = row.PAYMENT_AUDTRS_C,
                                    INSURANCE_EXP_CR = row.INSURANCE_EXP_CR,
                                    POWER_FUEL_CR = row.POWER_FUEL_CR,
                                    FINANCE_COST_CR = row.FINANCE_COST_CR,
                                    DEPRECTN_AMORT_C = row.DEPRECTN_AMORT_C,
                                    OTHER_EXPENSES_C = row.OTHER_EXPENSES_C,
                                    TOTAL_EXPENSES_C = row.TOTAL_EXPENSES_C,
                                    PROFIT_BEFORE_CR = row.PROFIT_BEFORE_CR,
                                    EXCEPTIONL_ITM_C = row.EXCEPTIONL_ITM_C,
                                    PROFIT_BEF_TAX_C = row.PROFIT_BEF_TAX_C,
                                    EXTRAORDINARY_CR = row.EXTRAORDINARY_CR,
                                    PROF_B_TAX_7_8_C = row.PROF_B_TAX_7_8_C,
                                    CURRENT_TAX_CR = row.CURRENT_TAX_CR,
                                    DEFERRED_TAX_CR = row.DEFERRED_TAX_CR,
                                    PROF_LOSS_OPER_C = row.PROF_LOSS_OPER_C,
                                    PROF_LOSS_DO_CR = row.PROF_LOSS_DO_CR,
                                    TAX_EXPNS_DIS_CR = row.TAX_EXPNS_DIS_CR,
                                    PROF_LOS_12_13_C = row.PROF_LOS_12_13_C,
                                    PROF_LOS_11_14_C = row.PROF_LOS_11_14_C,
                                    BASIC_BEFR_EI_CR = row.BASIC_BEFR_EI_CR,
                                    DILUTED_BEF_EI_C = row.DILUTED_BEF_EI_C,
                                    BASIC_AFTR_EI_CR = row.BASIC_AFTR_EI_CR,
                                    DILUTED_AFT_EI_C = row.DILUTED_AFT_EI_C,
                                    IsActive = row.IsActive,
                                    CreatedBy = row.CreatedBy,
                                    CreatedOn = row.CreatedOn,
                                    UpdatedBy = row.UpdatedBy,
                                    UpdatedOn = row.UpdatedOn,

                                }).ToList();

                    if (_obj != null)
                    {
                        result.Current = new AOC4_ProfitLossVM();
                        result.Current = (from row in _obj
                                          where row.ReportingPeriod == "C"
                                          select row).FirstOrDefault();

                        result.Previous = new AOC4_ProfitLossVM();
                        result.Previous = (from row in _obj
                                           where row.ReportingPeriod == "P"
                                           select row).FirstOrDefault();
                    }

                    #region Set Default Values
                    if (result.Current == null)
                    {
                        result.Current = new AOC4_ProfitLossVM()
                        {
                            ProfitLossId = 0,
                            Aoc4Id = 0,
                            ReportingPeriod = null,
                            FROM_DATE_CR = null,
                            TO_DATE_CR = null,
                            SALES_GOODS_CR = 0,
                            SALES_GOODS_T_CR = 0,
                            SALES_SUPPLY_CR = 0,
                            SALES_GOODS1_CR = 0,
                            SALE_GOODS_T1_CR = 0,
                            SALES_SUPPLY1_CR = 0,
                            OTHER_INCOME_CR = 0,
                            TOTAL_REVENUE_CR = 0,
                            COST_MATERIAL_CR = 0,
                            PURCHASE_STOCK_C = 0,
                            FINISHED_GOODS_C = 0,
                            WORK_IN_PROG_CR = 0,
                            STOCK_IN_TRADE_C = 0,
                            EMP_BENEFIT_EX_C = 0,
                            MANGERIAL_REM_CR = 0,
                            PAYMENT_AUDTRS_C = 0,
                            INSURANCE_EXP_CR = 0,
                            POWER_FUEL_CR = 0,
                            FINANCE_COST_CR = 0,
                            DEPRECTN_AMORT_C = 0,
                            OTHER_EXPENSES_C = 0,
                            TOTAL_EXPENSES_C = 0,
                            PROFIT_BEFORE_CR = 0,
                            EXCEPTIONL_ITM_C = 0,
                            PROFIT_BEF_TAX_C = 0,
                            EXTRAORDINARY_CR = 0,
                            PROF_B_TAX_7_8_C = 0,
                            CURRENT_TAX_CR = 0,
                            DEFERRED_TAX_CR = 0,
                            PROF_LOSS_OPER_C = 0,
                            PROF_LOSS_DO_CR = 0,
                            TAX_EXPNS_DIS_CR = 0,
                            PROF_LOS_12_13_C = 0,
                            PROF_LOS_11_14_C = 0,
                            BASIC_BEFR_EI_CR = 0,
                            DILUTED_BEF_EI_C = 0,
                            BASIC_AFTR_EI_CR = 0,
                            DILUTED_AFT_EI_C = 0,
                            IsActive = true,
                            CreatedBy = 0,
                            CreatedOn = null,
                            UpdatedBy = 0,
                            UpdatedOn = null,
                        };
                    }

                    if (result.Previous == null)
                    {
                        result.Previous = new AOC4_ProfitLossVM()
                        {
                            ProfitLossId = 0,
                            Aoc4Id = 0,
                            ReportingPeriod = null,
                            FROM_DATE_CR = null,
                            TO_DATE_CR = null,
                            SALES_GOODS_CR = 0,
                            SALES_GOODS_T_CR = 0,
                            SALES_SUPPLY_CR = 0,
                            SALES_GOODS1_CR = 0,
                            SALE_GOODS_T1_CR = 0,
                            SALES_SUPPLY1_CR = 0,
                            OTHER_INCOME_CR = 0,
                            TOTAL_REVENUE_CR = 0,
                            COST_MATERIAL_CR = 0,
                            PURCHASE_STOCK_C = 0,
                            FINISHED_GOODS_C = 0,
                            WORK_IN_PROG_CR = 0,
                            STOCK_IN_TRADE_C = 0,
                            EMP_BENEFIT_EX_C = 0,
                            MANGERIAL_REM_CR = 0,
                            PAYMENT_AUDTRS_C = 0,
                            INSURANCE_EXP_CR = 0,
                            POWER_FUEL_CR = 0,
                            FINANCE_COST_CR = 0,
                            DEPRECTN_AMORT_C = 0,
                            OTHER_EXPENSES_C = 0,
                            TOTAL_EXPENSES_C = 0,
                            PROFIT_BEFORE_CR = 0,
                            EXCEPTIONL_ITM_C = 0,
                            PROFIT_BEF_TAX_C = 0,
                            EXTRAORDINARY_CR = 0,
                            PROF_B_TAX_7_8_C = 0,
                            CURRENT_TAX_CR = 0,
                            DEFERRED_TAX_CR = 0,
                            PROF_LOSS_OPER_C = 0,
                            PROF_LOSS_DO_CR = 0,
                            TAX_EXPNS_DIS_CR = 0,
                            PROF_LOS_12_13_C = 0,
                            PROF_LOS_11_14_C = 0,
                            BASIC_BEFR_EI_CR = 0,
                            DILUTED_BEF_EI_C = 0,
                            BASIC_AFTR_EI_CR = 0,
                            DILUTED_AFT_EI_C = 0,
                            IsActive = true,
                            CreatedBy = 0,
                            CreatedOn = null,
                            UpdatedBy = 0,
                            UpdatedOn = null,
                        };
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public long GetProfitLossIdByAOC4Id(long AOC4Id, int customerId)
        {
            long result = 0;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var isExists = (from row in entities.BM_Form_AOC4_6_P_And_L
                                    where row.AOC4Id == AOC4Id && row.IsActive == true
                                    select row.Id).Any();

                    if (isExists)
                    {
                        result = AOC4Id;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public AOC4_ProfitLossPrevVM GetProfitLossDefaultDataForExcel(long id, int customerId)
        {
            AOC4_ProfitLossPrevVM result = null;
            bool lastYearflag = false;
            //Get Current AOC4 id if Profit & Loss data Exists
            var AOC4Id = GetProfitLossIdByAOC4Id(id, customerId);

            if (AOC4Id > 0)
            {
                lastYearflag = false;
            }
            else
            {
                //Get last Year AOC4 id if Profit & Loss data Exists
                AOC4Id = GetPreviousPeriodIdByAOC4Id(id, customerId);
                if (AOC4Id > 0)
                {
                    lastYearflag = true;
                }
                else
                {
                    lastYearflag = false;
                }

            }

            if (lastYearflag == false)
            {
                result = GetProfitLoss(AOC4Id, customerId);
            }
            else
            {
                var result1 = GetProfitLoss(AOC4Id, customerId);
                result = new AOC4_ProfitLossPrevVM();
                result.Current = new AOC4_ProfitLossVM() { ReportingPeriod = "C" };

                result.Previous = new AOC4_ProfitLossVM();
                if (result1 != null)
                {
                    if (result1.Current != null)
                    {
                        result.Previous = result1.Current;
                        result.Previous.ReportingPeriod = "P";
                    }
                }
            }

            return result;
        }
        #endregion

        #region Detailed Profit and Loss items 
        public Message SaveDetailedProfitLoss(List<DetailedProfitLossVM> objList, int userId)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (objList != null)
                    {
                        foreach (var obj in objList)
                        {

                            var _obj = (from row in entities.BM_Form_AOC4_6_P_And_L_Detailed
                                        where row.AOC4Id == obj.DetailedOfPLAOC4Id && row.ReportingPeriod == obj.ReportingPeriod
                                        select row).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.AOC4Id = obj.DetailedOfPLAOC4Id;
                                _obj.ReportingPeriod = obj.ReportingPeriod;
                                _obj.EXP_GOODS_FOB_CR = obj.EXP_GOODS_FOB_CR;
                                _obj.INTEREST_DIVD_CR = obj.INTEREST_DIVD_CR;
                                _obj.ROYALTY_CR = obj.ROYALTY_CR;
                                _obj.KNOW_HOW_CR = obj.KNOW_HOW_CR;
                                _obj.PROF_CONS_FEE_CR = obj.PROF_CONS_FEE_CR;
                                _obj.OTHR_INCOME_E_CR = obj.OTHR_INCOME_E_CR;
                                _obj.TOTAL_EARNG_FE_C = obj.TOTAL_EARNG_FE_C;
                                _obj.RAW_MATERIAL_CR = obj.RAW_MATERIAL_CR;
                                _obj.COMPONENT_SP_CR = obj.COMPONENT_SP_CR;
                                _obj.CAPITAL_GOODS_CR = obj.CAPITAL_GOODS_CR;
                                _obj.ROYALTY_EXP_CR = obj.ROYALTY_EXP_CR;
                                _obj.KNOW_HOW_EXP_CR = obj.KNOW_HOW_EXP_CR;
                                _obj.PROF_CON_FEE_E_C = obj.PROF_CON_FEE_E_C;
                                _obj.INTEREST_EXP_CR = obj.INTEREST_EXP_CR;
                                _obj.OTHER_MATTERS_CR = obj.OTHER_MATTERS_CR;
                                _obj.DIVIDEND_PAID_CR = obj.DIVIDEND_PAID_CR;
                                _obj.TOT_EXP_FE_CR = obj.TOT_EXP_FE_CR;

                                _obj.IsActive = true;
                                _obj.UpdatedBy = userId;
                                _obj.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();

                                result.Success = true;
                                result.Message = SecretarialConst.Messages.updateSuccess;
                            }
                            else
                            {
                                _obj = new BM_Form_AOC4_6_P_And_L_Detailed();

                                _obj.AOC4Id = obj.DetailedOfPLAOC4Id;
                                _obj.ReportingPeriod = obj.ReportingPeriod;
                                _obj.EXP_GOODS_FOB_CR = obj.EXP_GOODS_FOB_CR;
                                _obj.INTEREST_DIVD_CR = obj.INTEREST_DIVD_CR;
                                _obj.ROYALTY_CR = obj.ROYALTY_CR;
                                _obj.KNOW_HOW_CR = obj.KNOW_HOW_CR;
                                _obj.PROF_CONS_FEE_CR = obj.PROF_CONS_FEE_CR;
                                _obj.OTHR_INCOME_E_CR = obj.OTHR_INCOME_E_CR;
                                _obj.TOTAL_EARNG_FE_C = obj.TOTAL_EARNG_FE_C;
                                _obj.RAW_MATERIAL_CR = obj.RAW_MATERIAL_CR;
                                _obj.COMPONENT_SP_CR = obj.COMPONENT_SP_CR;
                                _obj.CAPITAL_GOODS_CR = obj.CAPITAL_GOODS_CR;
                                _obj.ROYALTY_EXP_CR = obj.ROYALTY_EXP_CR;
                                _obj.KNOW_HOW_EXP_CR = obj.KNOW_HOW_EXP_CR;
                                _obj.PROF_CON_FEE_E_C = obj.PROF_CON_FEE_E_C;
                                _obj.INTEREST_EXP_CR = obj.INTEREST_EXP_CR;
                                _obj.OTHER_MATTERS_CR = obj.OTHER_MATTERS_CR;
                                _obj.DIVIDEND_PAID_CR = obj.DIVIDEND_PAID_CR;
                                _obj.TOT_EXP_FE_CR = obj.TOT_EXP_FE_CR;

                                _obj.IsActive = true;
                                _obj.CreatedBy = userId;
                                _obj.CreatedOn = DateTime.Now;

                                entities.BM_Form_AOC4_6_P_And_L_Detailed.Add(_obj);
                                entities.SaveChanges();

                                var objAOCForm = (from row in entities.BM_Form_AOC4
                                                  where row.Id == obj.DetailedOfPLAOC4Id
                                                  select row).FirstOrDefault();
                                if (objAOCForm != null)
                                {
                                    objAOCForm.Detailed_PL_A = true;

                                    objAOCForm.UpdatedBy = userId;
                                    objAOCForm.UpdatedOn = DateTime.Now;

                                    entities.SaveChanges();
                                }

                                obj.DetailedOfPLId = _obj.Id;
                                result.Success = true;
                                result.Message = SecretarialConst.Messages.saveSuccess;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public long GetDetailedPLIdByAOC4Id(long AOC4Id, int customerId)
        {
            long result = 0;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var isExists = (from row in entities.BM_Form_AOC4_6_P_And_L_Detailed
                                    where row.AOC4Id == AOC4Id && row.IsActive == true
                                    select row.Id).Any();

                    if (isExists)
                    {
                        result = AOC4Id;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public DetailedProfitLossPrevVM GetDetailedProfitLossitems(long id, int customerId)
        {
            var result = new DetailedProfitLossPrevVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Form_AOC4_6_P_And_L_Detailed
                                where row.AOC4Id == id && row.IsActive == true
                                select new DetailedProfitLossVM
                                {
                                    DetailedOfPLAOC4Id = row.AOC4Id,
                                    ReportingPeriod = row.ReportingPeriod,
                                    EXP_GOODS_FOB_CR = row.EXP_GOODS_FOB_CR,
                                    INTEREST_DIVD_CR = row.INTEREST_DIVD_CR,
                                    ROYALTY_CR = row.ROYALTY_CR,
                                    KNOW_HOW_CR = row.KNOW_HOW_CR,
                                    PROF_CONS_FEE_CR = row.PROF_CONS_FEE_CR,
                                    OTHR_INCOME_E_CR = row.OTHR_INCOME_E_CR,
                                    TOTAL_EARNG_FE_C = row.TOTAL_EARNG_FE_C,
                                    RAW_MATERIAL_CR = row.RAW_MATERIAL_CR,
                                    COMPONENT_SP_CR = row.COMPONENT_SP_CR,
                                    CAPITAL_GOODS_CR = row.CAPITAL_GOODS_CR,
                                    ROYALTY_EXP_CR = row.ROYALTY_EXP_CR,
                                    KNOW_HOW_EXP_CR = row.KNOW_HOW_EXP_CR,
                                    PROF_CON_FEE_E_C = row.PROF_CON_FEE_E_C,
                                    INTEREST_EXP_CR = row.INTEREST_EXP_CR,
                                    OTHER_MATTERS_CR = row.OTHER_MATTERS_CR,
                                    DIVIDEND_PAID_CR = row.DIVIDEND_PAID_CR,
                                    TOT_EXP_FE_CR = row.TOT_EXP_FE_CR,
                                }).ToList();

                    if (_obj != null)
                    {
                        result.Current = new DetailedProfitLossVM();
                        result.Current = (from row in _obj
                                          where row.ReportingPeriod == "C"
                                          select row).FirstOrDefault();

                        result.Previous = new DetailedProfitLossVM();
                        result.Previous = (from row in _obj
                                           where row.ReportingPeriod == "P"
                                           select row).FirstOrDefault();
                    }

                    #region Set Default Values
                    if (result.Current == null)
                    {
                        result.Current = new DetailedProfitLossVM()
                        {
                            ReportingPeriod = "C",
                            EXP_GOODS_FOB_CR = 0,
                            INTEREST_DIVD_CR = 0,
                            ROYALTY_CR = 0,
                            KNOW_HOW_CR = 0,
                            PROF_CONS_FEE_CR = 0,
                            OTHR_INCOME_E_CR = 0,
                            TOTAL_EARNG_FE_C = 0,
                            RAW_MATERIAL_CR = 0,
                            COMPONENT_SP_CR = 0,
                            CAPITAL_GOODS_CR = 0,
                            ROYALTY_EXP_CR = 0,
                            KNOW_HOW_EXP_CR = 0,
                            PROF_CON_FEE_E_C = 0,
                            INTEREST_EXP_CR = 0,
                            OTHER_MATTERS_CR = 0,
                            DIVIDEND_PAID_CR = 0,
                            TOT_EXP_FE_CR = 0,
                        };
                    }

                    if (result.Previous == null)
                    {
                        result.Previous = new DetailedProfitLossVM()
                        {
                            ReportingPeriod = "P",
                            EXP_GOODS_FOB_CR = 0,
                            INTEREST_DIVD_CR = 0,
                            ROYALTY_CR = 0,
                            KNOW_HOW_CR = 0,
                            PROF_CONS_FEE_CR = 0,
                            OTHR_INCOME_E_CR = 0,
                            TOTAL_EARNG_FE_C = 0,
                            RAW_MATERIAL_CR = 0,
                            COMPONENT_SP_CR = 0,
                            CAPITAL_GOODS_CR = 0,
                            ROYALTY_EXP_CR = 0,
                            KNOW_HOW_EXP_CR = 0,
                            PROF_CON_FEE_E_C = 0,
                            INTEREST_EXP_CR = 0,
                            OTHER_MATTERS_CR = 0,
                            DIVIDEND_PAID_CR = 0,
                            TOT_EXP_FE_CR = 0,
                        };
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public DetailedProfitLossPrevVM GetDetailedProfiLossDefaultData(long id, int customerId)
        {
            DetailedProfitLossPrevVM result = null;
            bool lastYearflag = false;
            //Get Current AOC4 id if Detailed Profit and Loss items data Exists
            
            var AOC4Id = GetDetailedPLIdByAOC4Id(id, customerId);
            if (AOC4Id > 0)
            {
                lastYearflag = false;
            }
            else
            {
                //Get last Year AOC4 id if Detailed Profit and Loss items data Exists
                AOC4Id = GetPreviousPeriodIdByAOC4Id(id, customerId);
                if (AOC4Id > 0)
                {
                    lastYearflag = true;
                }
                else
                {
                    lastYearflag = false;
                }

            }

            if (lastYearflag == false)
            {
                result = GetDetailedProfitLossitems(AOC4Id, customerId);
            }
            else
            {
                var result1 = GetDetailedProfitLossitems(AOC4Id, customerId);
                result = new DetailedProfitLossPrevVM();
                result.Current = new DetailedProfitLossVM() { ReportingPeriod = "C" };

                result.Previous = new DetailedProfitLossVM();
                if (result1 != null)
                {
                    if (result1.Current != null)
                    {
                        result.Previous = result1.Current;
                        result.Previous.ReportingPeriod = "P";
                    }
                }
            }

            return result;
        }
        #endregion

        #region Financial parameters - Profit and loss account items    
        public Message SaveFinancialParameter_PL(FinancialParameter_PLVM _objFPL, int userID)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (_objFPL != null)
                    {
                        var _objFinancialPL = (from row in entities.BM_Form_AOC4_7_P_ANd_L_FinancialPara
                                               where row.AOC4Id == _objFPL.FinancialParameterPLAOC4Id
                                               select row).FirstOrDefault();

                        if (_objFinancialPL != null)
                        {
                            _objFinancialPL.AOC4Id = _objFPL.FinancialParameterPLAOC4Id;
                            _objFinancialPL.PROPOSED_DIVIDND = _objFPL.PROPOSED_DIVIDND;
                            _objFinancialPL.PROPOSED_DIV_PER = _objFPL.PROPOSED_DIV_PER;
                            _objFinancialPL.BASIC_EARNING_PS = _objFPL.BASIC_EARNING_PS;
                            _objFinancialPL.DILUTED_EARN_PS = _objFPL.DILUTED_EARN_PS;
                            _objFinancialPL.INCOME_FOREIGN = _objFPL.INCOME_FOREIGN;
                            _objFinancialPL.EXPENDIT_FOREIGN = _objFPL.EXPENDIT_FOREIGN;
                            _objFinancialPL.REVENUE_SUBSIDIE = _objFPL.REVENUE_SUBSIDIE;
                            _objFinancialPL.RENT_PAID = _objFPL.RENT_PAID;
                            _objFinancialPL.CONSUMPTION_STOR = _objFPL.CONSUMPTION_STOR;
                            _objFinancialPL.GROSS_VALUE_TRAN = _objFPL.GROSS_VALUE_TRAN;
                            _objFinancialPL.BAD_DEBTS_RP = _objFPL.BAD_DEBTS_RP;

                            _objFinancialPL.IsActive = true;
                            _objFinancialPL.UpdatedBy = userID;
                            _objFinancialPL.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();

                            result.Success = true;
                            result.Message = SecretarialConst.Messages.updateSuccess;
                        }
                        else
                        {
                            _objFinancialPL = new BM_Form_AOC4_7_P_ANd_L_FinancialPara();

                            _objFinancialPL.AOC4Id = _objFPL.FinancialParameterPLAOC4Id;
                            _objFinancialPL.PROPOSED_DIVIDND = _objFPL.PROPOSED_DIVIDND;
                            _objFinancialPL.PROPOSED_DIV_PER = _objFPL.PROPOSED_DIV_PER;
                            _objFinancialPL.BASIC_EARNING_PS = _objFPL.BASIC_EARNING_PS;
                            _objFinancialPL.DILUTED_EARN_PS = _objFPL.DILUTED_EARN_PS;
                            _objFinancialPL.INCOME_FOREIGN = _objFPL.INCOME_FOREIGN;
                            _objFinancialPL.EXPENDIT_FOREIGN = _objFPL.EXPENDIT_FOREIGN;
                            _objFinancialPL.REVENUE_SUBSIDIE = _objFPL.REVENUE_SUBSIDIE;
                            _objFinancialPL.RENT_PAID = _objFPL.RENT_PAID;
                            _objFinancialPL.CONSUMPTION_STOR = _objFPL.CONSUMPTION_STOR;
                            _objFinancialPL.GROSS_VALUE_TRAN = _objFPL.GROSS_VALUE_TRAN;
                            _objFinancialPL.BAD_DEBTS_RP = _objFPL.BAD_DEBTS_RP;

                            _objFinancialPL.IsActive = true;
                            _objFinancialPL.CreatedBy = userID;
                            _objFinancialPL.CreatedOn = DateTime.Now;

                            entities.BM_Form_AOC4_7_P_ANd_L_FinancialPara.Add(_objFinancialPL);
                            entities.SaveChanges();

                            var objAOCForm = (from row in entities.BM_Form_AOC4
                                              where row.Id == _objFPL.FinancialParameterPLAOC4Id
                                              select row).FirstOrDefault();
                            if (objAOCForm != null)
                            {
                                objAOCForm.Financial_Parameters_PL = true;

                                objAOCForm.UpdatedBy = userID;
                                objAOCForm.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();
                            }

                            _objFPL.FinancialParameterPLId = _objFinancialPL.Id;
                            result.Success = true;
                            result.Message = SecretarialConst.Messages.saveSuccess;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public FinancialParameter_PLVM GetFinancialParameter_PLDefaultData(long id, int customerId)
        {
            var result = new FinancialParameter_PLVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_7_P_ANd_L_FinancialPara
                              where row.AOC4Id == id && row.IsActive == true
                              select new FinancialParameter_PLVM
                              {
                                  FinancialParameterPLAOC4Id = row.AOC4Id,
                                  PROPOSED_DIVIDND = row.PROPOSED_DIVIDND,
                                  PROPOSED_DIV_PER = row.PROPOSED_DIV_PER,
                                  BASIC_EARNING_PS = row.BASIC_EARNING_PS,
                                  DILUTED_EARN_PS = row.DILUTED_EARN_PS,
                                  INCOME_FOREIGN = row.INCOME_FOREIGN,
                                  EXPENDIT_FOREIGN = row.EXPENDIT_FOREIGN,
                                  REVENUE_SUBSIDIE = row.REVENUE_SUBSIDIE,
                                  RENT_PAID = row.RENT_PAID,
                                  CONSUMPTION_STOR = row.CONSUMPTION_STOR,
                                  GROSS_VALUE_TRAN = row.GROSS_VALUE_TRAN,
                                  BAD_DEBTS_RP = row.BAD_DEBTS_RP,
                              }).FirstOrDefault();

                    #region Set Default Values
                    if (result == null)
                    {
                        result = new FinancialParameter_PLVM()
                        {
                            FinancialParameterPLAOC4Id = 0,
                            PROPOSED_DIVIDND = 0,
                            PROPOSED_DIV_PER = 0,
                            BASIC_EARNING_PS = 0,
                            DILUTED_EARN_PS = 0,
                            INCOME_FOREIGN = 0,
                            EXPENDIT_FOREIGN = 0,
                            REVENUE_SUBSIDIE = 0,
                            RENT_PAID = 0,
                            CONSUMPTION_STOR = 0,
                            GROSS_VALUE_TRAN = 0,
                            BAD_DEBTS_RP = 0,
                        };
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Related Party Transaction
        public RPT_VM GetRPTDetails(long id, int entityId)
        {
            var result = new RPT_VM() { RPTAOC4ID = id };
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result.lstContracts = (from row in entities.BM_Form_AOC4_10_RPT_Contracts
                                           where row.AOC4Id == id && row.IsActive == true
                                           select new RPT_Contracts_VM
                                           {
                                               NAME_RELATED_PAR = row.NAME_RELATED_PAR,
                                               NATURE_OF_RELATN = row.NATURE_OF_RELATN,
                                               NATURE_OF_CONTRA = row.NATURE_OF_CONTRA,
                                               DURATION_OF_CONT = row.DURATION_OF_CONT,
                                               DATE_OF_APPROVAL = row.DATE_OF_APPROVAL,
                                               AMOUNT_PAID = row.AMOUNT_PAID,
                                               DATE_SPCL_RESOLT = row.DATE_SPCL_RESOLT
                                           }).ToList();

                    if (result.lstContracts == null)
                    {
                        result.lstContracts = new List<RPT_Contracts_VM>();
                    }

                    result.lstMaterialContracts = (from row in entities.BM_Form_AOC4_10_RPT_MaterialContracts
                                                   where row.AOC4Id == id && row.IsActive == true
                                                   select new RPT_MaterialContracts_VM
                                                   {
                                                       NAME_RELATED_PAR = row.NAME_RELATED_PAR,
                                                       NATURE_OF_RELATN = row.NATURE_OF_RELATN,
                                                       NATURE_OF_CONTRA = row.NATURE_OF_CONTRA,
                                                       DURATION_OF_CONT = row.DURATION_OF_CONT,
                                                       DATE_OF_APPROVAL = row.DATE_OF_APPROVAL,
                                                       AMOUNT_PAID = row.AMOUNT_PAID
                                                   }).ToList();

                    if (result.lstMaterialContracts == null)
                    {
                        result.lstMaterialContracts = new List<RPT_MaterialContracts_VM>();
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public RPT_VM SaveRPT(RPT_VM obj, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (obj != null)
                    {
                        if (obj.lstContracts != null)
                        {
                            entities.BM_Form_AOC4_10_RPT_Contracts.Where(k => k.AOC4Id == obj.RPTAOC4ID && k.IsActive == true).ToList().ForEach(k => { k.IsActive = false; k.UpdatedBy = userId; k.UpdatedOn = DateTime.Now; });
                            entities.SaveChanges();

                            foreach (var item in obj.lstContracts)
                            {
                                if (!string.IsNullOrEmpty(item.NAME_RELATED_PAR)) 
                                {

                                    var _obj = new BM_Form_AOC4_10_RPT_Contracts();
                                    _obj.AOC4Id = obj.RPTAOC4ID;

                                    _obj.NAME_RELATED_PAR = item.NAME_RELATED_PAR;
                                    _obj.NATURE_OF_RELATN = item.NATURE_OF_RELATN;
                                    _obj.NATURE_OF_CONTRA = item.NATURE_OF_CONTRA;
                                    _obj.DURATION_OF_CONT = item.DURATION_OF_CONT;
                                    _obj.DATE_OF_APPROVAL = item.DATE_OF_APPROVAL;
                                    _obj.AMOUNT_PAID = item.AMOUNT_PAID;
                                    _obj.DATE_SPCL_RESOLT = item.DATE_SPCL_RESOLT;

                                    _obj.IsActive = true;
                                    _obj.CreatedBy = userId;
                                    _obj.CreatedOn = DateTime.Now;

                                    entities.BM_Form_AOC4_10_RPT_Contracts.Add(_obj);
                                    entities.SaveChanges();

                                }
                                
                            }
                        }

                        if (obj.lstMaterialContracts != null)
                        {
                            entities.BM_Form_AOC4_10_RPT_MaterialContracts.Where(k => k.AOC4Id == obj.RPTAOC4ID && k.IsActive == true).ToList().ForEach(k => { k.IsActive = false; k.UpdatedBy = userId; k.UpdatedOn = DateTime.Now; });
                            entities.SaveChanges();

                            foreach (var item in obj.lstMaterialContracts)
                            {
                                if (!string.IsNullOrEmpty(item.NAME_RELATED_PAR))
                                {
                                    var _obj = new BM_Form_AOC4_10_RPT_MaterialContracts();
                                    _obj.AOC4Id = obj.RPTAOC4ID;
                                    _obj.NAME_RELATED_PAR = item.NAME_RELATED_PAR;
                                    _obj.NATURE_OF_RELATN = item.NATURE_OF_RELATN;
                                    _obj.NATURE_OF_CONTRA = item.NATURE_OF_CONTRA;
                                    _obj.DURATION_OF_CONT = item.DURATION_OF_CONT;
                                    _obj.DATE_OF_APPROVAL = item.DATE_OF_APPROVAL;
                                    _obj.AMOUNT_PAID = item.AMOUNT_PAID;

                                    _obj.IsActive = true;
                                    _obj.CreatedBy = userId;
                                    _obj.CreatedOn = DateTime.Now;

                                    entities.BM_Form_AOC4_10_RPT_MaterialContracts.Add(_obj);
                                    entities.SaveChanges();
                                }
                              
                            }
                        }

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;

                        var objAOCForm = (from row in entities.BM_Form_AOC4
                                          where row.Id == obj.RPTAOC4ID
                                          select row).FirstOrDefault();
                        if (objAOCForm != null)
                        {
                            objAOCForm.RelatedParty = true;

                            objAOCForm.UpdatedBy = userId;
                            objAOCForm.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion



        #region Misc & Auth
        public MiscellaneousAuthorizationVM GetMiscAuth(long id)
        {
            var result = new MiscellaneousAuthorizationVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_12_MiscAndAuth
                              where row.AOC4Id == id && row.IsActive == true
                              select new MiscellaneousAuthorizationVM
                              {
                                  MiscId = row.Id,
                                  Aoc4Id = row.AOC4Id,
                                  RB_SECRETARIAL_A = row.RB_SECRETARIAL_A,
                                  RB_AUDTR_REPORT = row.RB_AUDTR_REPORT,
                                  CVRN = row.CVRN,
                                  DECLARATION_DATE = row.DECLARATION_DATE,
                                  DESIGNATION = row.DESIGNATION,
                                  DIN_PAN_MEM_NUM = row.DIN_PAN_MEM_NUM,
                                  RB_CA_COA_CS = row.RB_CA_COA_CS,
                                  RB_ASSOC_FELLOW = row.RB_ASSOC_FELLOW,
                                  MEMBERSHIP_NUM = row.MEMBERSHIP_NUM,
                                  CERT_PRACTICE_NO = row.CERT_PRACTICE_NO

                              }).FirstOrDefault();
                }

                if (result == null)
                {
                    result = new MiscellaneousAuthorizationVM() { Aoc4Id = id , RB_AUDTR_REPORT = true };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public MiscellaneousAuthorizationVM SaveMiscAuth(MiscellaneousAuthorizationVM obj, int userId)
        {
            try
            {
                
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Form_AOC4_12_MiscAndAuth
                                where row.Id == obj.MiscId && row.AOC4Id == obj.Aoc4Id
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.AOC4Id = obj.Aoc4Id;
                        _obj.RB_SECRETARIAL_A = obj.RB_SECRETARIAL_A;
                        _obj.RB_AUDTR_REPORT = obj.RB_AUDTR_REPORT;
                        _obj.CVRN = obj.CVRN;
                        _obj.DECLARATION_DATE = obj.DECLARATION_DATE;
                        _obj.DESIGNATION = obj.DESIGNATION;
                        _obj.DIN_PAN_MEM_NUM = obj.DIN_PAN_MEM_NUM;
                        _obj.RB_CA_COA_CS = obj.RB_CA_COA_CS;
                        _obj.RB_ASSOC_FELLOW = obj.RB_ASSOC_FELLOW;
                        _obj.MEMBERSHIP_NUM = obj.MEMBERSHIP_NUM;
                        _obj.CERT_PRACTICE_NO = obj.CERT_PRACTICE_NO;

                        _obj.IsActive = true;
                        _obj.UpdatedBy = userId;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        _obj = new BM_Form_AOC4_12_MiscAndAuth();
                        _obj.AOC4Id = obj.Aoc4Id;
                        _obj.RB_SECRETARIAL_A = obj.RB_SECRETARIAL_A;
                        _obj.RB_AUDTR_REPORT = obj.RB_AUDTR_REPORT;
                        _obj.CVRN = obj.CVRN;
                        _obj.DECLARATION_DATE = obj.DECLARATION_DATE;
                        _obj.DESIGNATION = obj.DESIGNATION;
                        _obj.DIN_PAN_MEM_NUM = obj.DIN_PAN_MEM_NUM;
                        _obj.RB_CA_COA_CS = obj.RB_CA_COA_CS;
                        _obj.RB_ASSOC_FELLOW = obj.RB_ASSOC_FELLOW;
                        _obj.MEMBERSHIP_NUM = obj.MEMBERSHIP_NUM;
                        _obj.CERT_PRACTICE_NO = obj.CERT_PRACTICE_NO;

                        _obj.IsActive = true;
                        _obj.CreatedBy = userId;
                        _obj.CreatedOn = DateTime.Now;

                        entities.BM_Form_AOC4_12_MiscAndAuth.Add(_obj);
                        entities.SaveChanges();

                        var objAOCForm = (from row in entities.BM_Form_AOC4
                                          where row.Id == obj.Aoc4Id
                                          select row).FirstOrDefault();
                        if (objAOCForm != null)
                        {
                            objAOCForm.Misc = true;

                            objAOCForm.UpdatedBy = userId;
                            objAOCForm.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }

                        obj.MiscId = _obj.Id;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }


        #endregion

        #region Reporting CSR 

        public Message SaveReportingCSR(CSR_VM _objCSR, int userID)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    if (_objCSR != null)
                    {
                        #region Master
                        var _obj = (from row in entities.BM_Form_AOC4_9_CSR
                                    where row.AOC4Id == _objCSR.CSRAOC4Id
                                    select row).FirstOrDefault();

                        if (_obj != null)
                        {
                            _obj.AOC4Id = _objCSR.CSRAOC4Id;
                            _obj.RB_CSR_APPLICABL = _objCSR.RB_CSR_APPLICABL;
                            _obj.TURNOVER = _objCSR.TURNOVER;
                            _obj.NET_WORTH = _objCSR.NET_WORTH;
                            _obj.PRESCRIBED_CSR_E = _objCSR.PRESCRIBED_CSR_E;
                            _obj.TOTAL_AMT_SPENT = _objCSR.TOTAL_AMT_SPENT;
                            _obj.AMOUNT_SPENT_LA = _objCSR.AMOUNT_SPENT_LA;
                            _obj.NUM_CSR_ACTIVITY = _objCSR.NUM_CSR_ACTIVITY;
                            _obj.DETAILS_IMPLEMEN = _objCSR.DETAILS_IMPLEMEN;

                            _obj.IsActive = true;
                            _obj.UpdatedBy = userID;
                            _obj.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();

                            result.Success = true;
                            result.Message = SecretarialConst.Messages.updateSuccess;
                        }
                        else
                        {
                            _obj = new BM_Form_AOC4_9_CSR();

                            _obj.AOC4Id = _objCSR.CSRAOC4Id;
                            _obj.RB_CSR_APPLICABL = _objCSR.RB_CSR_APPLICABL;
                            _obj.TURNOVER = _objCSR.TURNOVER;
                            _obj.NET_WORTH = _objCSR.NET_WORTH;
                            _obj.PRESCRIBED_CSR_E = _objCSR.PRESCRIBED_CSR_E;
                            _obj.TOTAL_AMT_SPENT = _objCSR.TOTAL_AMT_SPENT;
                            _obj.AMOUNT_SPENT_LA = _objCSR.AMOUNT_SPENT_LA;
                            _obj.NUM_CSR_ACTIVITY = _objCSR.NUM_CSR_ACTIVITY;
                            _obj.DETAILS_IMPLEMEN = _objCSR.DETAILS_IMPLEMEN;

                            _obj.IsActive = true;
                            _obj.CreatedBy = userID;
                            _obj.CreatedOn = DateTime.Now;

                            entities.BM_Form_AOC4_9_CSR.Add(_obj);
                            entities.SaveChanges();

                            var objAOCForm = (from row in entities.BM_Form_AOC4
                                              where row.Id == _objCSR.CSRAOC4Id
                                              select row).FirstOrDefault();
                            if (objAOCForm != null)
                            {
                                objAOCForm.CSR = true;

                                objAOCForm.UpdatedBy = userID;
                                objAOCForm.UpdatedOn = DateTime.Now;

                                entities.SaveChanges();
                            }

                            _objCSR.CSRAOC4Id = _obj.AOC4Id;
                            result.Success = true;
                            result.Message = SecretarialConst.Messages.saveSuccess;
                        }
                        #endregion

                        #region Details
                        if (result.Success)
                        {
                            if (_objCSR.lstDetails != null)
                            {
                                //Delete Existing Data
                                entities.BM_Form_AOC4_9_CSR_Details.Where(k => k.AOC4Id == _objCSR.CSRAOC4Id && k.IsActive == true).ToList().ForEach(k => { k.IsActive = false; k.UpdatedBy = userID; k.UpdatedOn = DateTime.Now; });
                                entities.SaveChanges();
                                //

                                foreach (var _objdetails in _objCSR.lstDetails)
                                {
                                    if (!string.IsNullOrEmpty(_objdetails.CSR_PROJECT_ACTV))
                                    {
                                        var _objReport = new BM_Form_AOC4_9_CSR_Details();
                                        _objReport.AOC4Id = _objCSR.CSRAOC4Id;
                                        _objReport.CSR_PROJECT_ACTV = _objdetails.CSR_PROJECT_ACTV;
                                        var id = GetCSRProject(_objdetails.SECTOR_PROJ_COVR_Str);
                                        _objReport.SECTOR_PROJ_COVR = id;
                                        id = GetCSRStateId(_objdetails.STATE_UT_Str);
                                        _objReport.STATE_UT = id;
                                        _objReport.AMOUNT_OUTLAY = _objdetails.AMOUNT_OUTLAY;
                                        _objReport.AMOUNT_SPENT_PRJ = _objdetails.AMOUNT_SPENT_PRJ;
                                        id = GetCSRModeOfAmt(_objdetails.MODE_AMOUNT_SPNT_Str);
                                        _objReport.MODE_AMOUNT_SPNT = id;

                                        _objReport.IsActive = true;
                                        _objReport.CreatedBy = userID;
                                        _objReport.CreatedOn = DateTime.Now;

                                        entities.BM_Form_AOC4_9_CSR_Details.Add(_objReport);
                                        entities.SaveChanges();
                                    }
                                    
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public int GetCSRProject(string str)
        {
            int result = 0;
            try
            {
                if (!string.IsNullOrEmpty(str))
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        result = (from x in entities.BM_Form_AOC4_CSR_SectorMaster
                                  where x.SectorName == str
                                  select x.Id).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public int GetCSRStateId(string str)
        {
            int result = 0;
            try
            {
                if (!string.IsNullOrEmpty(str))
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        result = (from x in entities.States
                                  where x.Name == str
                                  select x.ID).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public int GetCSRModeOfAmt(string str)
        {
            int result = 0;
            try
            {
                if (!string.IsNullOrEmpty(str))
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        result = (from x in entities.BM_Form_AOC4_CSR_ModeOfAmt
                                  where x.ModeName == str
                                  select x.Id).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }
        public CSR_VM GetReportingCSR(long id)
        {
            var result = new CSR_VM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_9_CSR
                              where row.AOC4Id == id && row.IsActive == true
                              select new CSR_VM
                              {
                                  CSRAOC4Id = row.AOC4Id,
                                  RB_CSR_APPLICABL = row.RB_CSR_APPLICABL,
                                  TURNOVER = row.TURNOVER,
                                  NET_WORTH = row.NET_WORTH,
                                  PRESCRIBED_CSR_E = row.PRESCRIBED_CSR_E,
                                  TOTAL_AMT_SPENT = row.TOTAL_AMT_SPENT,
                                  AMOUNT_SPENT_LA = row.AMOUNT_SPENT_LA,
                                  NUM_CSR_ACTIVITY = row.NUM_CSR_ACTIVITY,
                                  DETAILS_IMPLEMEN = row.DETAILS_IMPLEMEN
                }).FirstOrDefault();

                }

                if (result == null)
                {
                    result = new CSR_VM() { CSRAOC4Id = id };
                }

                if (result.lstDetails == null)
                {
                    result.lstDetails = new List<CSRDetails_VM>();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public CSR_VM GetReportingCSRWithDetails(long id)
        {
            var result = new CSR_VM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_9_CSR
                              where row.AOC4Id == id && row.IsActive == true
                              select new CSR_VM
                              {
                                  CSRAOC4Id = row.AOC4Id,
                                  RB_CSR_APPLICABL = row.RB_CSR_APPLICABL,
                                  TURNOVER = row.TURNOVER,
                                  NET_WORTH = row.NET_WORTH,
                                  PRESCRIBED_CSR_E = row.PRESCRIBED_CSR_E,
                                  TOTAL_AMT_SPENT = row.TOTAL_AMT_SPENT,
                                  AMOUNT_SPENT_LA = row.AMOUNT_SPENT_LA,
                                  NUM_CSR_ACTIVITY = row.NUM_CSR_ACTIVITY,
                                  DETAILS_IMPLEMEN = row.DETAILS_IMPLEMEN,
                              }).FirstOrDefault();

                    if (result != null)
                    {
                        result.lstDetails = (from row in entities.BM_Form_AOC4_9_CSR_Details
                                             from a in entities.BM_Form_AOC4_CSR_SectorMaster.Where(k => k.Id == row.SECTOR_PROJ_COVR).DefaultIfEmpty()
                                             from b in entities.States.Where(k => k.ID == row.STATE_UT).DefaultIfEmpty()
                                             from c in entities.BM_Form_AOC4_CSR_ModeOfAmt.Where(k => k.Id == row.MODE_AMOUNT_SPNT).DefaultIfEmpty()
                                             where row.AOC4Id == id && row.IsActive == true
                                             select new CSRDetails_VM
                                             {
                                                 CSRDetailsId = row.Id,
                                                 CSRDetailsAOC4Id = row.AOC4Id,
                                                 CSR_PROJECT_ACTV = row.CSR_PROJECT_ACTV,
                                                 SECTOR_PROJ_COVR = row.SECTOR_PROJ_COVR,
                                                 SECTOR_PROJ_COVR_Str = a.SectorName,
                                                 STATE_UT = row.STATE_UT,
                                                 STATE_UT_Str = b.Name,
                                                 AMOUNT_OUTLAY = row.AMOUNT_OUTLAY,
                                                 AMOUNT_SPENT_PRJ = row.AMOUNT_SPENT_PRJ,
                                                 MODE_AMOUNT_SPNT = row.MODE_AMOUNT_SPNT,
                                                 MODE_AMOUNT_SPNT_Str = c.ModeName,
                                             }).ToList();
                    }
                }

                if (result == null)
                {
                    result = new CSR_VM() { CSRAOC4Id = id };
                }

                if (result.lstDetails == null)
                {
                    result.lstDetails = new List<CSRDetails_VM>();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<CSRDetails_VM> GetDefaultCSRExcelData(long id, int customerID)
        {
            var result = new List<CSRDetails_VM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_9_CSR_Details
                              from a in entities.BM_Form_AOC4_CSR_SectorMaster.Where(k => k.Id == row.SECTOR_PROJ_COVR).DefaultIfEmpty()
                              from b in entities.States.Where(k => k.ID == row.STATE_UT).DefaultIfEmpty()
                              from c in entities.BM_Form_AOC4_CSR_ModeOfAmt.Where(k => k.Id == row.MODE_AMOUNT_SPNT).DefaultIfEmpty()
                              where row.AOC4Id == id && row.IsActive == true
                              select new CSRDetails_VM
                              {
                                  CSRDetailsId = row.Id,
                                  CSRDetailsAOC4Id = row.AOC4Id,
                                  CSR_PROJECT_ACTV = row.CSR_PROJECT_ACTV,
                                  SECTOR_PROJ_COVR = row.SECTOR_PROJ_COVR,
                                  SECTOR_PROJ_COVR_Str = a.SectorName,
                                  STATE_UT = row.STATE_UT,
                                  STATE_UT_Str = b.Name,
                                  AMOUNT_OUTLAY = row.AMOUNT_OUTLAY,
                                  AMOUNT_SPENT_PRJ = row.AMOUNT_SPENT_PRJ,
                                  MODE_AMOUNT_SPNT = row.MODE_AMOUNT_SPNT,
                                  MODE_AMOUNT_SPNT_Str = c.ModeName,
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region  Auditor's Report
        public AuditorsReport_VM SaveAuditReport(AuditorsReport_VM obj, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.BM_Form_AOC4_11_AuditorsReport
                                where row.Id == obj.AuditorReportId && row.AOC4Id == obj.AuditorReportAOC4Id
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.RB_COMPTRL_AUDTR = obj.RB_COMPTRL_AUDTR;
                        _obj.RB_AUDTR_REPORT = obj.RB_AUDTR_REPORT;
                        _obj.RB_COMP_AUDT_REP = obj.RB_COMP_AUDT_REP;
                        _obj.FIXED_ASSETS = obj.FIXED_ASSETS;
                        _obj.INVENTORIES = obj.INVENTORIES;
                        _obj.LOANS_GIVEN_COMP = obj.LOANS_GIVEN_COMP;
                        _obj.ACCEPTANCE_PUB_D = obj.ACCEPTANCE_PUB_D;
                        _obj.MAINTENANCE_CR = obj.MAINTENANCE_CR;
                        _obj.STATUTORY_DUES = obj.STATUTORY_DUES;
                        _obj.TERM_LOANS = obj.TERM_LOANS;
                        _obj.FRAUD_NOTICED = obj.FRAUD_NOTICED;
                        _obj.OTHER_COMMENTS = obj.OTHER_COMMENTS;
                        _obj.RB_SECRETARIAL_A = obj.RB_SECRETARIAL_A;
                        _obj.RB_DETAILED_DISC = obj.RB_DETAILED_DISC;
                        _obj.INVENTORIES = obj.INVENTORIES;

                        _obj.UpdatedBy = obj.UpdatedBy;
                        _obj.UpdatedOn = DateTime.Now;

                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.updateSuccess;
                    }
                    else
                    {
                        _obj = new BM_Form_AOC4_11_AuditorsReport();

                        _obj.AOC4Id = obj.AuditorReportAOC4Id;
                        _obj.RB_COMPTRL_AUDTR = obj.RB_COMPTRL_AUDTR;
                        _obj.RB_AUDTR_REPORT = obj.RB_AUDTR_REPORT;
                        _obj.RB_COMP_AUDT_REP = obj.RB_COMP_AUDT_REP;
                        _obj.FIXED_ASSETS = obj.FIXED_ASSETS;
                        _obj.INVENTORIES = obj.INVENTORIES;
                        _obj.LOANS_GIVEN_COMP = obj.LOANS_GIVEN_COMP;
                        _obj.ACCEPTANCE_PUB_D = obj.ACCEPTANCE_PUB_D;
                        _obj.MAINTENANCE_CR = obj.MAINTENANCE_CR;
                        _obj.STATUTORY_DUES = obj.STATUTORY_DUES;
                        _obj.TERM_LOANS = obj.TERM_LOANS;
                        _obj.FRAUD_NOTICED = obj.FRAUD_NOTICED;
                        _obj.OTHER_COMMENTS = obj.OTHER_COMMENTS;
                        _obj.RB_SECRETARIAL_A = obj.RB_SECRETARIAL_A;
                        _obj.RB_DETAILED_DISC = obj.RB_DETAILED_DISC;
                        _obj.INVENTORIES = obj.INVENTORIES;

                        _obj.IsActive = true;
                        _obj.CreatedBy = userId;
                        _obj.CreatedOn = DateTime.Now;

                        entities.BM_Form_AOC4_11_AuditorsReport.Add(_obj);
                        entities.SaveChanges();

                        var objAOCForm = (from row in entities.BM_Form_AOC4
                                          where row.Id == obj.AuditorReportAOC4Id
                                          select row).FirstOrDefault();
                        if (objAOCForm != null)
                        {
                            objAOCForm.AuditorsReport = true;

                            objAOCForm.UpdatedBy = userId;
                            objAOCForm.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();
                        }

                        obj.AuditorReportId = _obj.Id;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public AuditorsReport_VM GetAuditorsReport(long id)
        {
            var result = new AuditorsReport_VM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_Form_AOC4_11_AuditorsReport
                              where row.AOC4Id == id && row.IsActive == true
                              select new AuditorsReport_VM
                              {
                                  AuditorReportId = row.Id,
                                  AuditorReportAOC4Id = row.AOC4Id,
                                  RB_COMPTRL_AUDTR = row.RB_COMPTRL_AUDTR,
                                  RB_AUDTR_REPORT = row.RB_AUDTR_REPORT,
                                  RB_COMP_AUDT_REP = row.RB_COMP_AUDT_REP,
                                  FIXED_ASSETS = row.FIXED_ASSETS,
                                  INVENTORIES = row.INVENTORIES,
                                  LOANS_GIVEN_COMP = row.LOANS_GIVEN_COMP,
                                  ACCEPTANCE_PUB_D = row.ACCEPTANCE_PUB_D,
                                  MAINTENANCE_CR = row.MAINTENANCE_CR,
                                  STATUTORY_DUES = row.STATUTORY_DUES,
                                  TERM_LOANS = row.TERM_LOANS,
                                  FRAUD_NOTICED = row.FRAUD_NOTICED,
                                  OTHER_COMMENTS = row.OTHER_COMMENTS,
                              }).FirstOrDefault();

                    if (result == null)
                    {
                        result = new AuditorsReport_VM() { AuditorReportAOC4Id = id };
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Import AOC4 Unused
        public AOC4FormUploadVM UploadAOC4(AOC4FormUploadVM obj, int custId, int userId)
        {
            var AOC4Id = 1;
            try
            {
                if (obj.File != null)
                {
                    if (obj.File.ContentLength > 0)
                    {
                        string myFilePath = obj.File.FileName;
                        string ext = Path.GetExtension(myFilePath);
                        if (ext == ".pdf")
                        {
                            string excelfileName = string.Empty;
                            string path = "~/Areas/BM_Management/Documents/" + custId + "/DirectorMaster";
                            string _file_Name = System.IO.Path.GetFileName(obj.File.FileName);
                            string _path = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(path), _file_Name);
                            bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            if (!exists)
                            {
                                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            }
                            DirectoryInfo di = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            FileInfo[] TXTFiles = di.GetFiles(_file_Name);
                            if (TXTFiles.Length == 0)
                            {
                                obj.File.SaveAs(_path);
                            }
                            PdfReader pdfReader = new PdfReader(_path);

                            var objCompanyInfo = new AOC4_CompanyInfoVM() { CompanyInfoAOC4Id = AOC4Id };
                            var objBalanceSheet_CR = new AOC4_BalanceSheetVM() { BalanceSheetAOC4Id = AOC4Id };
                            var objBalanceSheet_PR = new AOC4_BalanceSheetVM() { BalanceSheetAOC4Id = AOC4Id };

                            var objDetailedBalanceSheet_CR = new AOC4_DetailedBalanceSheetVM { BalanceSheetAOC4Id = AOC4Id };
                            var objDetailedBalanceSheet_PR = new AOC4_DetailedBalanceSheetVM { BalanceSheetAOC4Id = AOC4Id };

                            var objProfitLoss_CR = new AOC4_ProfitLossVM { ProfitLossId = AOC4Id };
                            var objProfitLoss_PR = new AOC4_ProfitLossVM { ProfitLossId = AOC4Id };

                            if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                            {
                                foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                {
                                    var key = de.Key.ToString();
                                    var value = (de.Value.InnerText).Trim();
                                    decimal actualVal = 0;

                                    #region Company Info
                                    if (key == "data[0].ZMCA_NCA_AOC_4[0].FY_START_DATE[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objCompanyInfo.FYStart = Convert.ToDateTime(key);
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].FY_END_DATE[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objCompanyInfo.FYEnd = Convert.ToDateTime(key);
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DATE_BOD_MEETING[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objCompanyInfo.DateOfBM_FRApproved = Convert.ToDateTime(key);
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DATE_SIGNING_RFS[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objCompanyInfo.DateOfBM_FRSec134 = Convert.ToDateTime(key);
                                        }
                                    }
                                    #endregion

                                    #region Balance Sheet
                                    //Decimal import sample

                                    #region Current period
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_CAPITAL_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.Capital = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].RESERVE_SURPLUS1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.Reserve = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.MoneyRecived = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.MoneyRecived = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MON_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.MoneyPending = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_BORR_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.LongTerm = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TL_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.Deferred = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LNG_TRM_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.OtherLongTerm = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_PROV_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.LongTermProvision = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_BOR_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.ShortTerm = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_PAYABLES_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.TradePayables = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_LIA_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.OtherCL = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_PRO_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.ShortTermProvision = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.Total_I = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TANGIBLE_ASSET_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.TangibleA = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AST_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.IntangibleA = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_WIP_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.CapitalWIP = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AUD_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.IntangibleA_UD = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].NON_CURR_INV_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.NonCI = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TA_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.DeferredTA = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LT_LOANS_ADV_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.Lt_LoanAndAdv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_NON_CA_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.OtherNonCA = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].CURRENT_INV_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.CurrentInvestment = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.Invetories = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_RECEIV_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.TradeReceivable = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].CASH_AND_EQU_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.CashEquivalents = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TRM_LOA_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.ShortTermLoan = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_CA_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_CR.OtherCurrentAsset = actualVal;
                                        }
                                    }
                                    //else if (key == "Total")
                                    //{
                                    //    if (!(string.IsNullOrEmpty(value)))
                                    //    {
                                    //        objBalanceSheet_CR.Total_II = actualVal;
                                    //    }
                                    //}
                                    #endregion

                                    #region Previous period                                   

                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_CAPITAL_PR[0]")                                   
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.Capital = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].RESERVE_SURPLUS2[0]")                                        
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.Reserve = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.MoneyRecived = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.MoneyRecived = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MON_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.MoneyPending = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_BORR_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.LongTerm = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TL_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.Deferred = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LNG_TRM_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.OtherLongTerm = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_PROV_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.LongTermProvision = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_BOR_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.ShortTerm = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_PAYABLES_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.TradePayables = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_LIA_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.OtherCL = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_PRO_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.ShortTermProvision = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.Total_I = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TANGIBLE_ASSET_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.TangibleA = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AST_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.IntangibleA = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_WIP_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.CapitalWIP = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AUD_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.IntangibleA_UD = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].NON_CURR_INV_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.NonCI = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TA_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.DeferredTA = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LT_LOANS_ADV_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.Lt_LoanAndAdv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_NON_CA_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.OtherNonCA = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].CURRENT_INV_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.CurrentInvestment = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.Invetories = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_RECEIV_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.TradeReceivable = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].CASH_AND_EQU_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.CashEquivalents = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TRM_LOA_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.ShortTermLoan = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_CA_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objBalanceSheet_PR.OtherCurrentAsset = actualVal;
                                        }
                                    }
                                    //else if (key == "Total")
                                    //{
                                    //    if (!(string.IsNullOrEmpty(value)))
                                    //    {
                                    //        objBalanceSheet_PR.Total_II = actualVal;
                                    //    }
                                    //}

                                    #endregion

                                    //End 
                                    #endregion

                                    #region Detailed Balance Sheet

                                    #region Current period
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONDS_DEBS_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_Bonds = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FB_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_LoanBank = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FO_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_LoanOtherParties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_PL_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_Deferred_pay_liabilities = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_Deposits = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_RP_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_Loans_adv_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_MAT_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_maturities = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOA_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_Otherloans_advances = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREGATE_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LT_Aggregate_Amt = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_B_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.ST_LoanBank = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_OP_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.ST_LoanOtherParties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_A_RP_ST_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.ST_Loans_advances_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_ST_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.ST_Deposits = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTH_LOANS_ADV_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.ST_Loans_adv_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_ST_BORR_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.ST_Tot_borrowings = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREG_ST_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.ST_Aggregate_Amt = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPT_ADVANCES_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_Capital_adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_Security_Deposits = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_ORP_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_Loans_adv_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_A_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_Otherloans_advances = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_A_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_TotLoan_Adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_RP_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_FromRelated_Parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_OT_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_FromOthers = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_Net_loan_Adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_DUE_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTA_Loans_Adv_dueByDir = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4CAPT_ADVANC_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_Capital_adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_Security_Deposits = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_OR_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_Loans_adv_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_Otherloans_advances = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_TotLoan_Adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_RP_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_FromRelated_Parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_OT_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_FromOthers = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_LT_LOA_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_Net_loan_Adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_ADV_DUE_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.LTAD_Loans_Adv_dueByDir = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_ES_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_Secured_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_WS_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_Secured_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_ES_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_Unsecured_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_WS_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_Unsecured_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_ES_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_Doubtful_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_WS_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_Doubtful_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_ES_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_TotalTrade_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_WS_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_TotalTrade_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_ES_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_Provision_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_WS_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_Provision_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_ES_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_NetTrade_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_WS_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_NetTrade_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_ES_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_DebtDue_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_WS_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_CR.TR_DebtDue_WSM = actualVal;
                                        }
                                    }
                                    #endregion

                                    #region Previous period

                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONDS_DEBS_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_Bonds = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FB_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_LoanBank = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FO_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_LoanOtherParties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_PL_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_Deferred_pay_liabilities = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_Deposits = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_RP_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_Loans_adv_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_MAT_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_maturities = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOA_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_Otherloans_advances = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREGATE_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LT_Aggregate_Amt = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_B_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.ST_LoanBank = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_OP_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.ST_LoanOtherParties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_A_RP_ST_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.ST_Loans_advances_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_ST_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.ST_Deposits = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTH_LOANS_ADV_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.ST_Loans_adv_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_ST_BORR_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.ST_Tot_borrowings = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREG_ST_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.ST_Aggregate_Amt = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPT_ADVANCES_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_Capital_adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_Security_Deposits = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_ORP_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_Loans_adv_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_A_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_Otherloans_advances = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_A_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_TotLoan_Adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_RP_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_FromRelated_Parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_OT_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_FromOthers = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_Net_loan_Adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_DUE_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTA_Loans_Adv_dueByDir = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4CAPT_ADVANC_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_Capital_adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_Security_Deposits = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_OR_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_Loans_adv_related_parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_Otherloans_advances = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_TotLoan_Adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_RP_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_FromRelated_Parties = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_OT_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_FromOthers = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_LT_LOA_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_Net_loan_Adv = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_ADV_DUE_CR1[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.LTAD_Loans_Adv_dueByDir = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_ES_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_Secured_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_WS_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_Secured_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_ES_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_Unsecured_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_WS_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_Unsecured_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_ES_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_Doubtful_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_WS_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_Doubtful_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_ES_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_TotalTrade_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_WS_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_TotalTrade_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_ES_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_Provision_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_WS_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_Provision_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_ES_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_NetTrade_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_WS_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_NetTrade_WSM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_ES_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_DebtDue_ESM = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_WS_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objDetailedBalanceSheet_PR.TR_DebtDue_WSM = actualVal;
                                        }
                                    }
                                    #endregion

                                    #endregion

                                    #region Profit & Loss

                                    #region Current Period
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FROM_DATE_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.FROM_DATE_CR = Convert.ToDateTime(key);
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TO_DATE_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.TO_DATE_CR = Convert.ToDateTime(key);
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.SALES_GOODS_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_T_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.SALES_GOODS_T_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.SALES_SUPPLY_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS1_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.SALES_GOODS1_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALE_GOODS_T1_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.SALE_GOODS_T1_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY1_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.SALES_SUPPLY1_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_INCOME_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.OTHER_INCOME_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_REVENUE_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.TOTAL_REVENUE_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COST_MATERIAL_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.COST_MATERIAL_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PURCHASE_STOCK_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PURCHASE_STOCK_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINISHED_GOODS_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.FINISHED_GOODS_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].WORK_IN_PROG_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.WORK_IN_PROG_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].STOCK_IN_TRADE_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.STOCK_IN_TRADE_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EMP_BENEFIT_EX_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.EMP_BENEFIT_EX_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].MANGERIAL_REM_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.MANGERIAL_REM_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PAYMENT_AUDTRS_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PAYMENT_AUDTRS_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INSURANCE_EXP_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.INSURANCE_EXP_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].POWER_FUEL_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.POWER_FUEL_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINANCE_COST_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.FINANCE_COST_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEPRECTN_AMORT_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.DEPRECTN_AMORT_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_EXPENSES_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.OTHER_EXPENSES_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EXPENSES_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.TOTAL_EXPENSES_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEFORE_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PROFIT_BEFORE_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXCEPTIONL_ITM_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.EXCEPTIONL_ITM_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEF_TAX_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PROFIT_BEF_TAX_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXTRAORDINARY_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.EXTRAORDINARY_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_B_TAX_7_8_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PROF_B_TAX_7_8_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CURRENT_TAX_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.CURRENT_TAX_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEFERRED_TAX_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.DEFERRED_TAX_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_OPER_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PROF_LOSS_OPER_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_DO_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PROF_LOSS_DO_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TAX_EXPNS_DIS_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.TAX_EXPNS_DIS_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_12_13_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PROF_LOS_12_13_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_11_14_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.PROF_LOS_11_14_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_BEFR_EI_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.BASIC_BEFR_EI_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_BEF_EI_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.DILUTED_BEF_EI_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_AFTR_EI_CR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.BASIC_AFTR_EI_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_AFT_EI_C[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_CR.DILUTED_AFT_EI_C = actualVal;
                                        }
                                    }

                                    #endregion

                                    #region Previos Period

                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FROM_DATE_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.FROM_DATE_CR = Convert.ToDateTime(key);
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TO_DATE_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.TO_DATE_CR = Convert.ToDateTime(key);
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.SALES_GOODS_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_T_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.SALES_GOODS_T_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.SALES_SUPPLY_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS1_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.SALES_GOODS1_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALE_GOODS_T1_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.SALE_GOODS_T1_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY1_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.SALES_SUPPLY1_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_INCOME_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.OTHER_INCOME_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_REVENUE_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.TOTAL_REVENUE_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COST_MATERIAL_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.COST_MATERIAL_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PURCHASE_STOCK_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PURCHASE_STOCK_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINISHED_GOODS_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.FINISHED_GOODS_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].WORK_IN_PROG_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.WORK_IN_PROG_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].STOCK_IN_TRADE_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.STOCK_IN_TRADE_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EMP_BENEFIT_EX_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.EMP_BENEFIT_EX_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].MANGERIAL_REM_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.MANGERIAL_REM_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PAYMENT_AUDTRS_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PAYMENT_AUDTRS_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INSURANCE_EXP_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.INSURANCE_EXP_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].POWER_FUEL_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.POWER_FUEL_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINANCE_COST_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.FINANCE_COST_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEPRECTN_AMORT_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.DEPRECTN_AMORT_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_EXPENSES_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.OTHER_EXPENSES_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EXPENSES_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.TOTAL_EXPENSES_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEFORE_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PROFIT_BEFORE_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXCEPTIONL_ITM_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.EXCEPTIONL_ITM_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEF_TAX_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PROFIT_BEF_TAX_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXTRAORDINARY_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.EXTRAORDINARY_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_B_TAX_7_8_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PROF_B_TAX_7_8_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CURRENT_TAX_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.CURRENT_TAX_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEFERRED_TAX_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.DEFERRED_TAX_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_OPER_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PROF_LOSS_OPER_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_DO_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PROF_LOSS_DO_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TAX_EXPNS_DIS_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.TAX_EXPNS_DIS_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_12_13_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PROF_LOS_12_13_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_11_14_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.PROF_LOS_11_14_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_BEFR_EI_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.BASIC_BEFR_EI_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_BEF_EI_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.DILUTED_BEF_EI_C = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_AFTR_EI_PR[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.BASIC_AFTR_EI_CR = actualVal;
                                        }
                                    }
                                    else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_AFT_EI_P[0]")
                                    {
                                        if (!(string.IsNullOrEmpty(value)))
                                        {
                                            objProfitLoss_PR.DILUTED_AFT_EI_C = actualVal;
                                        }
                                    }

                                    #endregion

                                    #endregion
                                }
                            }


                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return obj;
        }
        #endregion

        #region Import AOC4 old
        public AOC4FormUploadVM UpoadAOC4_old(AOC4FormUploadVM obj, int custId, int userId)
        {
            try
            {
                if (obj.File != null)
                {
                    if (obj.File.ContentLength > 0)
                    {
                        string myFilePath = obj.File.FileName;
                        string ext = Path.GetExtension(myFilePath);
                        if (ext == ".pdf")
                        {
                            string excelfileName = string.Empty;
                            string path = "~/Areas/BM_Management/Documents/" + custId + "/DirectorMaster";
                            string _file_Name = System.IO.Path.GetFileName(obj.File.FileName);
                            string _path = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(path), _file_Name);
                            bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            if (!exists)
                            {
                                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            }
                            DirectoryInfo di = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            FileInfo[] TXTFiles = di.GetFiles(_file_Name);
                            if (TXTFiles.Length == 0)
                            {
                                obj.File.SaveAs(_path);
                            }
                            PdfReader pdfReader = new PdfReader(_path);

                            var fycy = string.Empty;
                            var objAOC4 = new AOC4_List_VM();

                            if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                            {
                                var CIN = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].ZMCA_NCA_AOC_4[0].CIN[0]").FirstOrDefault().Value.InnerText).Trim();

                                if (!string.IsNullOrEmpty(CIN))
                                {
                                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                                    {
                                        var entityId = 0;

                                        var entity = (from row in entities.BM_EntityMaster
                                                      where row.Customer_Id == custId && row.CIN_LLPIN == CIN && row.Is_Deleted == false
                                                      select new
                                                      {
                                                          row.Id,
                                                          row.FY_CY
                                                      }).FirstOrDefault();

                                        if (entity != null)
                                        {
                                            entityId = entity.Id;
                                            fycy = entity.FY_CY;
                                        }

                                        if (entityId > 0)
                                        {
                                            objAOC4.EntityId = entityId;
                                        }
                                        else
                                        {
                                            obj.Error = true;
                                            obj.Message = "CIN not found in Entity Master";
                                            return obj;
                                        }
                                    }
                                }
                                else
                                {
                                    obj.Error = true;
                                    obj.Message = "Unable to read CIN";
                                    return obj;
                                }

                                var FYStart = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].ZMCA_NCA_AOC_4[0].FY_START_DATE[0]").FirstOrDefault().Value.InnerText).Trim();
                                var FYEnd = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].ZMCA_NCA_AOC_4[0].FY_END_DATE[0]").FirstOrDefault().Value.InnerText).Trim();

                                DateTime sDate;
                                DateTime eDate;

                                if (string.IsNullOrEmpty(FYStart) || string.IsNullOrEmpty(FYEnd))
                                {
                                    obj.Error = true;
                                    obj.Message = "Unable to read Financial year Data.";
                                    return obj;
                                }
                                else
                                {
                                    if (!DateTime.TryParseExact(FYStart, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out sDate))
                                    {
                                        obj.Error = true;
                                        obj.Message = "Unable to read Financial year Data.";
                                        return obj;
                                    }

                                    if (!DateTime.TryParseExact(FYEnd, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out eDate))
                                    {
                                        obj.Error = true;
                                        obj.Message = "Unable to read Financial year Data.";
                                        return obj;
                                    }
                                }

                                if (!string.IsNullOrEmpty(fycy))
                                {
                                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                                    {
                                        var FYID = (from row in entities.BM_YearMaster
                                                    where row.Type == fycy && row.FromDate <= sDate && row.ToDate >= sDate
                                                    select row.FYID).FirstOrDefault();

                                        if (FYID > 0)
                                        {
                                            objAOC4.FYId = (int)FYID;
                                        }
                                        else
                                        {
                                            obj.Error = true;
                                            obj.Message = "Financial year not found.";
                                            return obj;
                                        }
                                    }
                                }
                                else
                                {
                                    obj.Error = true;
                                    obj.Message = "Please set Financial Year in Entity Master";
                                    return obj;
                                }

                                objAOC4 = CreateAOC4(objAOC4, userId, custId);

                                if (objAOC4.Success)
                                {
                                    var AOC4Id = objAOC4.AOC4Id;

                                    #region objects
                                    var objCompanyInfo = new AOC4_CompanyInfoVM() { CompanyInfoAOC4Id = AOC4Id };

                                    var objBalanceSheet_CR = new AOC4_BalanceSheetVM() { BalanceSheetAOC4Id = AOC4Id, ReportingPeriod="C" };
                                    var objBalanceSheet_PR = new AOC4_BalanceSheetVM() { BalanceSheetAOC4Id = AOC4Id, ReportingPeriod = "P" };

                                    var objDetailedBalanceSheet_CR = new AOC4_DetailedBalanceSheetVM { BalanceSheetAOC4Id = AOC4Id , LT_ReportingPeriod = "C" };
                                    var objDetailedBalanceSheet_PR = new AOC4_DetailedBalanceSheetVM { BalanceSheetAOC4Id = AOC4Id , LT_ReportingPeriod = "P" };                                 

                                    var objProfitLoss_CR = new AOC4_ProfitLossVM { ProfitLossId = AOC4Id, ReportingPeriod = "C" };
                                    var objProfitLoss_PR = new AOC4_ProfitLossVM { ProfitLossId = AOC4Id, ReportingPeriod = "P" };

                                    var objFinancialParameter = new AOC4_FinancialParameterVM { FinancialParameterAOC4Id = AOC4Id };
                                    var objSharedCapital = new ShareCapitalRaisedVM { ShareCapitalAOC4Id = AOC4Id };
                                    var objCostRecords = new DetailsOfCostRecordsVM { DetailsOfCostRecordsAOC4Id = AOC4Id };
                                    var objAuditorReport = new AuditorsReport_VM { AuditorReportAOC4Id = AOC4Id };

                                    var objDetailedPL_CR = new DetailedProfitLossVM { DetailedOfPLAOC4Id = AOC4Id, ReportingPeriod = "C" };
                                    var objDetailedPL_PR = new DetailedProfitLossVM { DetailedOfPLAOC4Id = AOC4Id, ReportingPeriod = "P" };

                                    var objFinancialParaPL = new FinancialParameter_PLVM { FinancialParameterPLAOC4Id = AOC4Id };
                                    var objCSRHeader = new CSR_VM { CSRAOC4Id = AOC4Id };
                                    var objMiscAuth = new MiscellaneousAuthorizationVM { MiscId = AOC4Id };

                                    #endregion

                                    foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                    {
                                        var key = de.Key.ToString();
                                        var value = (de.Value.InnerText).Trim();
                                        decimal actualVal = 0;

                                        //Add your code
                                        DateTime tempDate;

                                        #region Company Info
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].FY_START_DATE[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.FYStart = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FY_END_DATE[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.FYEnd = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DATE_BOD_MEETING[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.DateOfBM_FRApproved = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DATE_SIGNING_RFS[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.DateOfBM_FRSec134 = tempDate;
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Balance Sheet
                                        //Decimal import sample

                                        #region Current period
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_CAPITAL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.Capital = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RESERVE_SURPLUS1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.Reserve = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.MoneyRecived = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.MoneyRecived = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MON_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.MoneyPending = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_BORR_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.LongTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.Deferred = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LNG_TRM_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.OtherLongTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_PROV_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.LongTermProvision = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_BOR_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.ShortTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_PAYABLES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.TradePayables = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_LIA_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.OtherCL = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_PRO_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.ShortTermProvision = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.Total_I = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TANGIBLE_ASSET_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.TangibleA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AST_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.IntangibleA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_WIP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.CapitalWIP = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AUD_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.IntangibleA_UD = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NON_CURR_INV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.NonCI = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.DeferredTA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LT_LOANS_ADV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_CR.Lt_LoanAndAdv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_NON_CA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.OtherNonCA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CURRENT_INV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.CurrentInvestment = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.Invetories = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_RECEIV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.TradeReceivable = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CASH_AND_EQU_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.CashEquivalents = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TRM_LOA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.ShortTermLoan = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_CA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_CR.OtherCurrentAsset = actualVal;
                                        }
                                        //else if (key == "Total")
                                        //{
                                        //    if (!(string.IsNullOrEmpty(value)))
                                        //    {
                                        //        objBalanceSheet_CR.Total_II = actualVal;
                                        //    }
                                        //}
                                        #endregion

                                        #region Previous period                                   

                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_CAPITAL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.Capital = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RESERVE_SURPLUS2[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_PR.Reserve = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.MoneyRecived = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.MoneyRecived = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MON_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.MoneyPending = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_BORR_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                                objBalanceSheet_PR.LongTerm = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.Deferred = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LNG_TRM_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.OtherLongTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_PROV_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.LongTermProvision = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_BOR_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_PR.ShortTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_PAYABLES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.TradePayables = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_LIA_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.OtherCL = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_PRO_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.ShortTermProvision = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.Total_I = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TANGIBLE_ASSET_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.TangibleA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AST_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.IntangibleA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_WIP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.CapitalWIP = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AUD_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.IntangibleA_UD = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NON_CURR_INV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.NonCI = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.DeferredTA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LT_LOANS_ADV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.Lt_LoanAndAdv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_NON_CA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.OtherNonCA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CURRENT_INV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.CurrentInvestment = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.Invetories = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_RECEIV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.TradeReceivable = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CASH_AND_EQU_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objBalanceSheet_PR.CashEquivalents = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TRM_LOA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.ShortTermLoan = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_CA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objBalanceSheet_PR.OtherCurrentAsset = actualVal;
                                        }
                                        //else if (key == "Total")
                                        //{
                                        //    if (!(string.IsNullOrEmpty(value)))
                                        //    {
                                        //        objBalanceSheet_PR.Total_II = actualVal;
                                        //    }
                                        //}

                                        #endregion

                                        //End 
                                        #endregion

                                        #region Detailed Balance Sheet

                                        #region Current period
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONDS_DEBS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LT_Bonds = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FB_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LT_LoanBank = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FO_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objDetailedBalanceSheet_CR.LT_LoanOtherParties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_PL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LT_Deferred_pay_liabilities = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LT_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_RP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LT_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_MAT_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objDetailedBalanceSheet_CR.LT_maturities = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objDetailedBalanceSheet_CR.LT_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREGATE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LT_Aggregate_Amt = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_B_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.ST_LoanBank = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_OP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.ST_LoanOtherParties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_A_RP_ST_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.ST_Loans_advances_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_ST_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.ST_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTH_LOANS_ADV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.ST_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_ST_BORR_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.ST_Tot_borrowings = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREG_ST_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.ST_Aggregate_Amt = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPT_ADVANCES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Capital_adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Security_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_ORP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_A_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_A_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objDetailedBalanceSheet_CR.LTA_TotLoan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_RP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTA_FromRelated_Parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_OT_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTA_FromOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Net_loan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_DUE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Loans_Adv_dueByDir = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4CAPT_ADVANC_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Capital_adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Security_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_OR_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_TotLoan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_RP_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_FromRelated_Parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_OT_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_FromOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_LT_LOA_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Net_loan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_ADV_DUE_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Loans_Adv_dueByDir = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_ES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_Secured_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_WS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_Secured_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_ES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_Unsecured_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_WS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_Unsecured_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_ES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_Doubtful_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_WS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_Doubtful_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_ES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_TotalTrade_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_WS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_TotalTrade_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_ES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_Provision_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_WS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_Provision_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_ES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_NetTrade_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_WS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_NetTrade_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_ES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_CR.TR_DebtDue_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_WS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objDetailedBalanceSheet_CR.TR_DebtDue_WSM = actualVal;
                                        }
                                        #endregion

                                        #region Previous period

                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONDS_DEBS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LT_Bonds = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FB_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LT_LoanBank = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FO_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objDetailedBalanceSheet_PR.LT_LoanOtherParties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_PL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LT_Deferred_pay_liabilities = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LT_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_RP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LT_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_MAT_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LT_maturities = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LT_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREGATE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LT_Aggregate_Amt = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_B_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.ST_LoanBank = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_OP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.ST_LoanOtherParties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_A_RP_ST_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.ST_Loans_advances_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_ST_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.ST_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTH_LOANS_ADV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.ST_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_ST_BORR_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.ST_Tot_borrowings = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREG_ST_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.ST_Aggregate_Amt = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPT_ADVANCES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Capital_adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Security_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_ORP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_A_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_A_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_TotLoan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_RP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_FromRelated_Parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_OT_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_FromOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Net_loan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_DUE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Loans_Adv_dueByDir = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4CAPT_ADVANC_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Capital_adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Security_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_OR_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_TotLoan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_RP_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_FromRelated_Parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_OT_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_FromOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_LT_LOA_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                               
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Net_loan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_ADV_DUE_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Loans_Adv_dueByDir = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_ES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_Secured_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_WS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_Secured_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_ES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_Unsecured_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_WS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_Unsecured_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_ES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_Doubtful_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_WS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_Doubtful_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_ES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_TotalTrade_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_WS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_TotalTrade_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_ES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_Provision_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_WS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_Provision_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_ES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_NetTrade_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_WS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_NetTrade_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_ES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_DebtDue_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_WS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }                                                
                                            }
                                            objDetailedBalanceSheet_PR.TR_DebtDue_WSM = actualVal;
                                        }
                                        #endregion

                                        #endregion

                                        #region Financial Parameter
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].AMOUNT_ISSUE_ALL[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                                objFinancialParameter.Amt_Of_issue_Alloted = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Share_app_Money_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Share_app_Money_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY2[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Share_app_Money_3 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY3[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Share_app_Money_4 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PAID_UP_CAPT_FC[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Paid_up_capital_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PAID_UP_CAPT_FC1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Paid_up_capital_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NUM_SHARES_BB[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.NoOfShares = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEP_ACCEPTED_REN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Deposit_Accepted = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLAI[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Deposit_Matured_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLA1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Deposit_Matured_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLA2[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Deposit_Matured_3 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNCLAIMED_M_DEB[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Unclaimed_Debentures = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBENTURES_CLAIM[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Claimed_Debentures = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTEREST_DEPOSIT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InterestOnDepositAccrued = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNPAID_DIVIDEND[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Unpaid_divedend = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVESTMENT_SUBSD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InvestmentInCompany_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVESTMENT_GOVT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InvestmentInCompany_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_RESERVES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Capital_Reserves = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].AMT_DUE_TRANSFER[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Amt_TransferforIEPF = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTER_CORPORATE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Inter_Corp_Deposit = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].GROSS_VALUE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.GrossValueOfTransaction = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_SUBSDIES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.CapitalSubsidies = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CALLS_UNPAID_DIR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.CallsUnpaidByDirector = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CALLS_UNPAID_OTH[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.CallsUnpaidByOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FORFEITED_SHARES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Forfeited_Shares_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FORFEITED_SHAR_R[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Forfeited_Shares_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BORROWING_FIA[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Borrowing_foreign_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BORROWING_FC[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Borrowing_foreign_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTER_CORP_BORR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InterCorporate_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTER_CORP_BORR1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InterCorporate_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].COMMERCIAL_PAPER[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.CommercialPaper = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.ConversionOfWarrant_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.ConversionOfWarrant_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_DE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.ConversionOfWarrant_3 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].WARRANTS_ISSUED[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.WarrantsIssueInForeignCurrency = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].WARRANTS_ISSUED1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.WarrantsIssueInRupees = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFAULT_PAYMENT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.DefaultInPayment_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFAULT_PAYMENT1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.DefaultInPayment_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_OPERATING_LEA[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objFinancialParameter.WheatheOperatingLease = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DETAILS_CONVERSN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objFinancialParameter.ProvideDetailsOfConversion = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_WORTH_COMPAN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.NetWorthOfComp = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NUM_SHARE_HOLDRS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.NoOfShareHolders = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_LOAN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.SecuredLoan = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].GROSS_FIXD_ASSET[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.GrossFixedAssets = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPRECIATN_AMORT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Depreciation_Amortization = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MISC_EXPENDITURE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Misc_Expenditure = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].UNHEDGED_FE_EXP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.UnhedgedForeign = actualVal;
                                        }

                                        #endregion

                                        #region Shared Capital Raised
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].PUBLIC_ISSUE_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                                objSharedCapital.PublicIssue_E = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PUBLIC_ISSUE_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PublicIssue_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONUS_ISSUE_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.BonusIssue_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONUS_ISSUE_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.BonusIssue_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RIGHTS_ISSUE_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.RightIssue_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RIGHTS_ISSUE_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.RightIssue_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PRIV_PLACEMENT_E[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PvtPlacementarising_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PRIV_PLACEMENT_P[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PvtPlacementarising_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHR_PRI_PLAC_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.OtherPvtPlacement_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHR_PRI_PLAC_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.OtherPvtPlacement_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PREF_ALLOTMENT_E[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PrferentialAllotment_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PREF_ALLOTMENT_P[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PrferentialAllotment_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PUBLIC_ISSUE_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PrferentialAllotment_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHR_PREF_ALL_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.OtherPreferentialallotment_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHR_PREF_ALL_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.OtherPreferentialallotment_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].ESOP_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.EmpStockOptionPlan_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].ESOP_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.EmpStockOptionPlan_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.Others_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.Others_P = actualVal;
                                        }

                                        #endregion

                                        #region Cost Records
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].AMOUNT_ISSUE_ALL[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objCostRecords.MaintenanceOfCR = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objCostRecords.CentralExcise = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS1[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objCostRecords.AuditOfCR = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objCostRecords.CentralExcise1 = value;
                                            }
                                        }
                                        #endregion

                                        #region Profit & Loss

                                        #region Current Period
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FROM_DATE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objProfitLoss_CR.FROM_DATE_CR = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TO_DATE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objProfitLoss_CR.TO_DATE_CR = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.SALES_GOODS_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_T_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.SALES_GOODS_T_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.SALES_SUPPLY_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS1_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.SALES_GOODS1_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALE_GOODS_T1_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.SALE_GOODS_T1_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY1_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.SALES_SUPPLY1_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_INCOME_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.OTHER_INCOME_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_REVENUE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.TOTAL_REVENUE_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COST_MATERIAL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.COST_MATERIAL_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PURCHASE_STOCK_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PURCHASE_STOCK_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINISHED_GOODS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.FINISHED_GOODS_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].WORK_IN_PROG_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.WORK_IN_PROG_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].STOCK_IN_TRADE_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.STOCK_IN_TRADE_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EMP_BENEFIT_EX_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.EMP_BENEFIT_EX_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].MANGERIAL_REM_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.MANGERIAL_REM_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PAYMENT_AUDTRS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PAYMENT_AUDTRS_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INSURANCE_EXP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.INSURANCE_EXP_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].POWER_FUEL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.POWER_FUEL_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINANCE_COST_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.FINANCE_COST_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEPRECTN_AMORT_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.DEPRECTN_AMORT_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_EXPENSES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.OTHER_EXPENSES_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EXPENSES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.TOTAL_EXPENSES_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEFORE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PROFIT_BEFORE_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXCEPTIONL_ITM_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.EXCEPTIONL_ITM_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEF_TAX_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PROFIT_BEF_TAX_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXTRAORDINARY_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.EXTRAORDINARY_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_B_TAX_7_8_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PROF_B_TAX_7_8_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CURRENT_TAX_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.CURRENT_TAX_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEFERRED_TAX_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.DEFERRED_TAX_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_OPER_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PROF_LOSS_OPER_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_DO_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PROF_LOSS_DO_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TAX_EXPNS_DIS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.TAX_EXPNS_DIS_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_12_13_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PROF_LOS_12_13_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_11_14_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.PROF_LOS_11_14_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_BEFR_EI_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.BASIC_BEFR_EI_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_BEF_EI_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.DILUTED_BEF_EI_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_AFTR_EI_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.BASIC_AFTR_EI_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_AFT_EI_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_CR.DILUTED_AFT_EI_C = actualVal;
                                            }
                                        }

                                        #endregion

                                        #region Previos Period

                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FROM_DATE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objProfitLoss_PR.FROM_DATE_CR = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TO_DATE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objProfitLoss_PR.TO_DATE_CR = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.SALES_GOODS_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_T_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.SALES_GOODS_T_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.SALES_SUPPLY_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS1_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.SALES_GOODS1_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALE_GOODS_T1_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.SALE_GOODS_T1_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY1_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.SALES_SUPPLY1_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_INCOME_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.OTHER_INCOME_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_REVENUE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.TOTAL_REVENUE_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COST_MATERIAL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.COST_MATERIAL_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PURCHASE_STOCK_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PURCHASE_STOCK_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINISHED_GOODS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.FINISHED_GOODS_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].WORK_IN_PROG_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.WORK_IN_PROG_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].STOCK_IN_TRADE_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.STOCK_IN_TRADE_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EMP_BENEFIT_EX_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.EMP_BENEFIT_EX_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].MANGERIAL_REM_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.MANGERIAL_REM_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PAYMENT_AUDTRS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PAYMENT_AUDTRS_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INSURANCE_EXP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.INSURANCE_EXP_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].POWER_FUEL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.POWER_FUEL_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINANCE_COST_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.FINANCE_COST_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEPRECTN_AMORT_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.DEPRECTN_AMORT_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_EXPENSES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.OTHER_EXPENSES_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EXPENSES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.TOTAL_EXPENSES_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEFORE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PROFIT_BEFORE_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXCEPTIONL_ITM_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.EXCEPTIONL_ITM_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEF_TAX_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PROFIT_BEF_TAX_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXTRAORDINARY_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.EXTRAORDINARY_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_B_TAX_7_8_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PROF_B_TAX_7_8_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CURRENT_TAX_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.CURRENT_TAX_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEFERRED_TAX_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.DEFERRED_TAX_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_OPER_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PROF_LOSS_OPER_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_DO_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PROF_LOSS_DO_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TAX_EXPNS_DIS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.TAX_EXPNS_DIS_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_12_13_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PROF_LOS_12_13_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_11_14_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.PROF_LOS_11_14_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_BEFR_EI_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.BASIC_BEFR_EI_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_BEF_EI_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.DILUTED_BEF_EI_C = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_AFTR_EI_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.BASIC_AFTR_EI_CR = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_AFT_EI_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objProfitLoss_PR.DILUTED_AFT_EI_C = actualVal;
                                            }
                                        }

                                        #endregion

                                        #endregion

                                        #region Detailed Profit and Loss items

                                        #region Current
                                        if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXP_GOODS_FOB_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.EXP_GOODS_FOB_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_DIVD_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.INTEREST_DIVD_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.ROYALTY_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.KNOW_HOW_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_CONS_FEE_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.PROF_CONS_FEE_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHR_INCOME_E_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.OTHR_INCOME_E_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EARNG_FE_C[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.TOTAL_EARNG_FE_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].RAW_MATERIAL_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.RAW_MATERIAL_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COMPONENT_SP_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.COMPONENT_SP_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CAPITAL_GOODS_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.CAPITAL_GOODS_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_EXP_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.ROYALTY_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_EXP_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.KNOW_HOW_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_CON_FEE_E_C[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.PROF_CON_FEE_E_C = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_EXP_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.INTEREST_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_MATTERS_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.OTHER_MATTERS_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIVIDEND_PAID_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.DIVIDEND_PAID_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOT_EXP_FE_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.TOT_EXP_FE_CR = actualVal;

                                        }
                                        #endregion

                                        #region Previous
                                        if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXP_GOODS_FOB_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.EXP_GOODS_FOB_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_DIVD_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.INTEREST_DIVD_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.ROYALTY_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.KNOW_HOW_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_CONS_FEE_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.PROF_CONS_FEE_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHR_INCOME_E_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.OTHR_INCOME_E_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EARNG_FE_C[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.TOTAL_EARNG_FE_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].RAW_MATERIAL_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.RAW_MATERIAL_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COMPONENT_SP_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.COMPONENT_SP_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CAPITAL_GOODS_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.CAPITAL_GOODS_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_EXP_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.ROYALTY_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_EXP_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.KNOW_HOW_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_CON_FEE_E_C[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.PROF_CON_FEE_E_C = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_EXP_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.INTEREST_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_MATTERS_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.OTHER_MATTERS_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIVIDEND_PAID_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.DIVIDEND_PAID_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOT_EXP_FE_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.TOT_EXP_FE_CR = actualVal;

                                        }
                                        #endregion

                                        #endregion

                                        #region Financial Parameters PL Items
                                        if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROPOSED_DIVIDND[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.PROPOSED_DIVIDND = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROPOSED_DIV_PER[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.PROPOSED_DIV_PER = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_EARNING_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.BASIC_EARNING_PS = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_EARN_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.DILUTED_EARN_PS = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROPOSED_DIVIDND[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.PROPOSED_DIVIDND = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INCOME_FOREIGN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.INCOME_FOREIGN = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXPENDIT_FOREIGN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.EXPENDIT_FOREIGN = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].REVENUE_SUBSIDIE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.REVENUE_SUBSIDIE = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].RENT_PAID[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.RENT_PAID = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CONSUMPTION_STOR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.CONSUMPTION_STOR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].GROSS_VALUE_TRAN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.GROSS_VALUE_TRAN = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BAD_DEBTS_RP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.BAD_DEBTS_RP = actualVal;
                                        }

                                        #endregion

                                        #region CSR

                                        #region CSR Header
                                        if (key == "data[0].ZMCA_NCA_AOC4_II[0].RB_CSR_APPLICABL[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objCSRHeader.RB_CSR_APPLICABL = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TURNOVER[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.TURNOVER = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].NET_WORTH[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.NET_WORTH = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PRESCRIBED_CSR_E[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.PRESCRIBED_CSR_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_AMT_SPENT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.TOTAL_AMT_SPENT = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].AMOUNT_SPENT_LA[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.AMOUNT_SPENT_LA = actualVal;
                                        }
                                        //else if (key == "data[0].ZMCA_NCA_AOC4_II[0].NUM_CSR_ACTIVITY[0]")
                                        //{
                                        //    if (!string.IsNullOrEmpty(value))
                                        //    {
                                        //        _objCSRHeader.NUM_CSR_ACTIVITY = Convert.ToInt32(value);
                                        //    }                                        
                                        //}
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DETAILS_IMPLEMEN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objCSRHeader.DETAILS_IMPLEMEN = value;
                                            }
                                        }
                                        #endregion


                                        #endregion

                                        #region Auditors Report
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_COMPTRL_AUDTR[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objAuditorReport.RB_COMPTRL_AUDTR = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objAuditorReport.RB_AUDTR_REPORT = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_COMP_AUDT_REP[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objAuditorReport.RB_COMP_AUDT_REP = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FIXED_ASSETS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.FIXED_ASSETS = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.INVENTORIES = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_GIVEN_COMP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.LOANS_GIVEN_COMP = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].ACCEPTANCE_PUB_D[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.ACCEPTANCE_PUB_D = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MAINTENANCE_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.MAINTENANCE_CR = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].STATUTORY_DUES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.STATUTORY_DUES = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.TERM_LOANS = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FRAUD_NOTICED[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.FRAUD_NOTICED = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_COMMENTS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.OTHER_COMMENTS = value;
                                            }
                                        }
                                        #endregion

                                        #region Misc Authorization                                       

                                        GetMiscAuth(AOC4Id);

                                        if (objMiscAuth != null)
                                        {
                                            var strValue = string.Empty;

                                            if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_COMPTRL_AUDTR[0]")
                                            {
                                                if (value == "YES")
                                                {
                                                    objMiscAuth.RB_SECRETARIAL_A = true;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]")
                                            {
                                                if (value == "YES")
                                                {
                                                    objMiscAuth.RB_AUDTR_REPORT = true;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].DESIGNATION[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.DESIGNATION = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].DIN_PAN_MEM_NUM[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.DIN_PAN_MEM_NUM = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_CA_COA_CS[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.RB_CA_COA_CS = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_ASSOC_FELLOW[0]")
                                            {
                                                if (value == "YES")
                                                {
                                                    objMiscAuth.RB_ASSOC_FELLOW = true;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].MEMBERSHIP_NUM[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.MEMBERSHIP_NUM = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].CERT_PRACTICE_NO[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.CERT_PRACTICE_NO = value;
                                                }
                                            }

                                        }
                                        #endregion

                                        //End


                                    }

                                    SaveCompanyInfo(objCompanyInfo, userId);

                                    List<AOC4_BalanceSheetVM> objList = new List<AOC4_BalanceSheetVM>();
                                    objList.Add(objBalanceSheet_CR);
                                    objList.Add(objBalanceSheet_PR);
                                    SaveBalanceSheet(objList, userId);

                                    List<AOC4_DetailedBalanceSheetVM> objListDbs = new List<AOC4_DetailedBalanceSheetVM>();
                                    objListDbs.Add(objDetailedBalanceSheet_CR);
                                    objListDbs.Add(objDetailedBalanceSheet_PR);
                                    SaveDetailedBalanceSheet(objListDbs, userId);

                                    List<AOC4_ProfitLossVM> objListPL = new List<AOC4_ProfitLossVM>();
                                    objListPL.Add(objProfitLoss_CR);
                                    objListPL.Add(objProfitLoss_PR);
                                    SaveProfitLoss(objListPL,userId);

                                    List<DetailedProfitLossVM> objListDPL = new List<DetailedProfitLossVM>();
                                    objListDPL.Add(objDetailedPL_CR);
                                    objListDPL.Add(objDetailedPL_PR);
                                    SaveDetailedProfitLoss(objListDPL,userId);

                                    SaveFinancialParameter(objFinancialParameter, userId);
                                    SaveSharedCapitalRaised(objSharedCapital, userId);
                                    SaveFinancialParameter_PL(objFinancialParaPL, userId);
                                    SaveAuditReport(objAuditorReport, userId);
                                    SaveDetailsOfCostRecords(objCostRecords, userId);
                                    SaveMiscAuth(objMiscAuth,userId);

                                }                                
                            }                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Import AOC4
        public int GetCSRProjectIdByCode(string str)
        {
            int result = 0;
            try
            {
                if (!string.IsNullOrEmpty(str))
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        result = (from x in entities.BM_Form_AOC4_CSR_SectorMaster
                                  where x.SectorCode == str
                                  select x.Id).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public int GetCSRStateIdByCode(string str)
        {
            int result = 0;
            try
            {
                if (!string.IsNullOrEmpty(str))
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        var tempresult = (from x in entities.BM_StateMaster_Code
                                          where x.AOC4Code == str
                                          select x.StateId).FirstOrDefault();
                        if (tempresult > 0)
                        {
                            result = (int)tempresult;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }

        public int GetCSRModeOfAmtIdByCode(string str)
        {
            int result = 0;
            try
            {
                if (!string.IsNullOrEmpty(str))
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        result = (from x in entities.BM_Form_AOC4_CSR_ModeOfAmt
                                  where x.ModeCode == str
                                  select x.Id).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return result;
        }
        public AOC4FormUploadVM UpoadAOC4(AOC4FormUploadVM obj, int custId, int userId)
        {
            try
            {
                if (obj.File != null)
                {
                    if (obj.File.ContentLength > 0)
                    {
                        string myFilePath = obj.File.FileName;
                        string ext = Path.GetExtension(myFilePath);
                        if (ext == ".pdf")
                        {
                            string excelfileName = string.Empty;
                            string path = "~/Areas/BM_Management/Documents/" + custId + "/AOC4";
                            string _file_Name = System.IO.Path.GetFileName(obj.File.FileName);
                            string _path = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(path), _file_Name);
                            bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            if (!exists)
                            {
                                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            }
                            DirectoryInfo di = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(path));
                            FileInfo[] TXTFiles = di.GetFiles(_file_Name);
                            if (TXTFiles.Length == 0)
                            {
                                obj.File.SaveAs(_path);
                            }
                            PdfReader pdfReader = new PdfReader(_path);

                            var fycy = string.Empty;
                            var objAOC4 = new AOC4_List_VM();

                            if (pdfReader.AcroFields.Xfa.DatasetsSom != null)
                            {
                                var CIN = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].ZMCA_NCA_AOC_4[0].CIN[0]").FirstOrDefault().Value.InnerText).Trim();

                                if (!string.IsNullOrEmpty(CIN))
                                {
                                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                                    {
                                        var entityId = 0;

                                        var entity = (from row in entities.BM_EntityMaster
                                                      where row.Customer_Id == custId && row.CIN_LLPIN == CIN && row.Is_Deleted == false
                                                      select new
                                                      {
                                                          row.Id,
                                                          row.FY_CY
                                                      }).FirstOrDefault();

                                        if (entity != null)
                                        {
                                            entityId = entity.Id;
                                            fycy = entity.FY_CY;
                                        }

                                        if (entityId > 0)
                                        {
                                            objAOC4.EntityId = entityId;
                                        }
                                        else
                                        {
                                            obj.Error = true;
                                            obj.Message = "The CIN entered by you was not found in our entity master directory. Request you to upload/add your CIN at entity master and try again.";
                                            return obj;
                                        }
                                    }
                                }
                                else
                                {
                                    obj.Error = true;
                                    obj.Message = "Unable to read CIN";
                                    return obj;
                                }

                                var FYStart = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].ZMCA_NCA_AOC_4[0].FY_START_DATE[0]").FirstOrDefault().Value.InnerText).Trim();
                                var FYEnd = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].ZMCA_NCA_AOC_4[0].FY_END_DATE[0]").FirstOrDefault().Value.InnerText).Trim();

                                DateTime sDate;
                                DateTime eDate;

                                if (string.IsNullOrEmpty(FYStart) || string.IsNullOrEmpty(FYEnd))
                                {
                                    obj.Error = true;
                                    obj.Message = "Unable to read Financial year Data.";
                                    return obj;
                                }
                                else
                                {
                                    if (!DateTime.TryParseExact(FYStart, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out sDate))
                                    {
                                        obj.Error = true;
                                        obj.Message = "Unable to read Financial year Data.";
                                        return obj;
                                    }

                                    if (!DateTime.TryParseExact(FYEnd, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out eDate))
                                    {
                                        obj.Error = true;
                                        obj.Message = "Unable to read Financial year Data.";
                                        return obj;
                                    }
                                }

                                if (!string.IsNullOrEmpty(fycy))
                                {
                                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                                    {
                                        var FYID = (from row in entities.BM_YearMaster
                                                    where row.Type == fycy && row.FromDate <= sDate && row.ToDate >= sDate
                                                    select row.FYID).FirstOrDefault();

                                        if (FYID > 0)
                                        {
                                            objAOC4.FYId = (int)FYID;
                                        }
                                        else
                                        {
                                            obj.Error = true;
                                            obj.Message = "Financial year not found.";
                                            return obj;
                                        }
                                    }
                                }
                                else
                                {
                                    obj.Error = true;
                                    obj.Message = "Please set Financial Year in Entity Master";
                                    return obj;
                                }

                                objAOC4 = CreateAOC4(objAOC4, userId, custId);

                                if (objAOC4.Success)
                                {
                                    var AOC4Id = objAOC4.AOC4Id;
                                    obj.Success = true;
                                    obj.Message = SecretarialConst.Messages.saveSuccess;

                                    #region objects
                                    var objCompanyInfo = new AOC4_CompanyInfoVM() { CompanyInfoAOC4Id = AOC4Id, objSignedDetails = new AOC4_SignedDetailsVM() };

                                    var objBalanceSheet_CR = new AOC4_BalanceSheetVM() { BalanceSheetAOC4Id = AOC4Id, ReportingPeriod = "C" };
                                    var objBalanceSheet_PR = new AOC4_BalanceSheetVM() { BalanceSheetAOC4Id = AOC4Id, ReportingPeriod = "P" };

                                    var objDetailedBalanceSheet_CR = new AOC4_DetailedBalanceSheetVM { BalanceSheetAOC4Id = AOC4Id, LT_ReportingPeriod = "C" };
                                    var objDetailedBalanceSheet_PR = new AOC4_DetailedBalanceSheetVM { BalanceSheetAOC4Id = AOC4Id, LT_ReportingPeriod = "P" };

                                    var objProfitLoss_CR = new AOC4_ProfitLossVM { Aoc4Id = AOC4Id, ReportingPeriod = "C" };
                                    var objProfitLoss_PR = new AOC4_ProfitLossVM { Aoc4Id = AOC4Id, ReportingPeriod = "P" };

                                    var objFinancialParameter = new AOC4_FinancialParameterVM { FinancialParameterAOC4Id = AOC4Id };
                                    var objSharedCapital = new ShareCapitalRaisedVM { ShareCapitalAOC4Id = AOC4Id };
                                    var objCostRecords = new DetailsOfCostRecordsVM { DetailsOfCostRecordsAOC4Id = AOC4Id };
                                    var objAuditorReport = new AuditorsReport_VM { AuditorReportAOC4Id = AOC4Id };

                                    var objDetailedPL_CR = new DetailedProfitLossVM { DetailedOfPLAOC4Id = AOC4Id, ReportingPeriod = "C" };
                                    var objDetailedPL_PR = new DetailedProfitLossVM { DetailedOfPLAOC4Id = AOC4Id, ReportingPeriod = "P" };

                                    var objFinancialParaPL = new FinancialParameter_PLVM { FinancialParameterPLAOC4Id = AOC4Id };
                                    var objCSRHeader = new CSR_VM { CSRAOC4Id = AOC4Id };
                                    var objRPT = new RPT_VM { RPTAOC4ID = AOC4Id };
                                    var objMiscAuth = new MiscellaneousAuthorizationVM { Aoc4Id = AOC4Id };

                                    #endregion

                                    foreach (var de in pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node)
                                    {
                                        var key = de.Key.ToString();
                                        var value = (de.Value.InnerText).Trim();
                                        decimal actualVal = 0;

                                        //Add your code
                                        DateTime tempDate;

                                        #region Company Info
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].FY_START_DATE[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.FYStart = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FY_END_DATE[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.FYEnd = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DATE_BOD_MEETING[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.DateOfBM_FRApproved = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DATE_SIGNING_RFS[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.DateOfBM_FRSec134 = tempDate;
                                                }
                                            }
                                        }

                                        #region SIgning Details
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objCompanyInfo.objSignedDetails.DINPAN_1 = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.objSignedDetails.Date_1 = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN2[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objCompanyInfo.objSignedDetails.DINPAN_2 = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS2[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.objSignedDetails.Date_2 = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN3[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objCompanyInfo.objSignedDetails.DINPAN_3 = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS3[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.objSignedDetails.Date_3 = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN4[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objCompanyInfo.objSignedDetails.DINPAN_4 = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS4[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.objSignedDetails.Date_4 = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIN_PAN5[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objCompanyInfo.objSignedDetails.DINPAN_5 = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_FS5[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.objSignedDetails.Date_5 = tempDate;
                                                }
                                            }
                                        }

                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIN1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objCompanyInfo.objSignedDetails.DIN_1 = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_BR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.objSignedDetails.DDate_1 = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIN2[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objCompanyInfo.objSignedDetails.DIN_2 = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_BR2[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.objSignedDetails.DDate_2 = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIN3[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                objCompanyInfo.objSignedDetails.DIN_3 = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DATE_SIGNING_BR3[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objCompanyInfo.objSignedDetails.DDate_3 = tempDate;
                                                }
                                            }
                                        }
                                        #endregion

                                        #endregion

                                        #region Balance Sheet
                                        //Decimal import sample

                                        #region Current period
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_CAPITAL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.Capital = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RESERVE_SURPLUS1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.Reserve = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.MoneyRecived = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.MoneyRecived = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MON_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.MoneyPending = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_BORR_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.LongTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.Deferred = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LNG_TRM_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.OtherLongTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_PROV_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.LongTermProvision = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_BOR_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.ShortTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_PAYABLES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.TradePayables = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_LIA_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.OtherCL = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_PRO_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.ShortTermProvision = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.Total_I = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TANGIBLE_ASSET_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.TangibleA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AST_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.IntangibleA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_WIP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.CapitalWIP = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AUD_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.IntangibleA_UD = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NON_CURR_INV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.NonCI = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.DeferredTA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LT_LOANS_ADV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.Lt_LoanAndAdv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_NON_CA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.OtherNonCA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CURRENT_INV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.CurrentInvestment = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.Invetories = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_RECEIV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.TradeReceivable = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CASH_AND_EQU_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.CashEquivalents = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TRM_LOA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.ShortTermLoan = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_CA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_CR.OtherCurrentAsset = actualVal;
                                        }
                                        //else if (key == "Total")
                                        //{
                                        //    if (!(string.IsNullOrEmpty(value)))
                                        //    {
                                        //        objBalanceSheet_CR.Total_II = actualVal;
                                        //    }
                                        //}
                                        #endregion

                                        #region Previous period                                   

                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_CAPITAL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.Capital = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RESERVE_SURPLUS2[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.Reserve = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.MoneyRecived = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MONEY_RECEIVD_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.MoneyRecived = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MON_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.MoneyPending = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_BORR_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                                objBalanceSheet_PR.LongTerm = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.Deferred = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LNG_TRM_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.OtherLongTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_PROV_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.LongTermProvision = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_BOR_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.ShortTerm = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_PAYABLES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.TradePayables = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_LIA_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.OtherCL = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TERM_PRO_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.ShortTermProvision = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.Total_I = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TANGIBLE_ASSET_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.TangibleA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AST_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.IntangibleA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_WIP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.CapitalWIP = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTANGIBLE_AUD_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.IntangibleA_UD = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NON_CURR_INV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.NonCI = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_TA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.DeferredTA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LT_LOANS_ADV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.Lt_LoanAndAdv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_NON_CA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.OtherNonCA = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CURRENT_INV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.CurrentInvestment = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.Invetories = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TRADE_RECEIV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.TradeReceivable = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CASH_AND_EQU_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.CashEquivalents = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHORT_TRM_LOA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.ShortTermLoan = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_CURR_CA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objBalanceSheet_PR.OtherCurrentAsset = actualVal;
                                        }
                                        //else if (key == "Total")
                                        //{
                                        //    if (!(string.IsNullOrEmpty(value)))
                                        //    {
                                        //        objBalanceSheet_PR.Total_II = actualVal;
                                        //    }
                                        //}

                                        #endregion

                                        //End 
                                        #endregion

                                        #region Detailed Balance Sheet

                                        #region Current period
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONDS_DEBS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_Bonds = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FB_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_LoanBank = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FO_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_LoanOtherParties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_PL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_Deferred_pay_liabilities = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_RP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_MAT_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_maturities = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOA_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREGATE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LT_Aggregate_Amt = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_B_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.ST_LoanBank = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_OP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.ST_LoanOtherParties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_A_RP_ST_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.ST_Loans_advances_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_ST_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.ST_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTH_LOANS_ADV_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.ST_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_ST_BORR_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.ST_Tot_borrowings = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREG_ST_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.ST_Aggregate_Amt = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPT_ADVANCES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Capital_adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Security_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_ORP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_A_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_A_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_TotLoan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_RP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_FromRelated_Parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_OT_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_FromOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Net_loan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_DUE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTA_Loans_Adv_dueByDir = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4CAPT_ADVANC_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Capital_adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Security_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_OR_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_TotLoan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_RP_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_FromRelated_Parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_OT_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_FromOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_LT_LOA_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Net_loan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_ADV_DUE_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.LTAD_Loans_Adv_dueByDir = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_ES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_Secured_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_WS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_Secured_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_ES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_Unsecured_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_WS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_Unsecured_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_ES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_Doubtful_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_WS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_Doubtful_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_ES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_TotalTrade_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_WS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_TotalTrade_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_ES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_Provision_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_WS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_Provision_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_ES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_NetTrade_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_WS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_NetTrade_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_ES_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_DebtDue_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_WS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_CR.TR_DebtDue_WSM = actualVal;
                                        }
                                        #endregion

                                        #region Previous period

                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONDS_DEBS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_Bonds = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FB_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_LoanBank = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS_FO_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_LoanOtherParties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFERRED_PL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_Deferred_pay_liabilities = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_RP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LONG_TERM_MAT_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_maturities = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOA_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREGATE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LT_Aggregate_Amt = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_B_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.ST_LoanBank = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_REPAY_OP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.ST_LoanOtherParties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_A_RP_ST_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.ST_Loans_advances_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPOSITS_ST_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.ST_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTH_LOANS_ADV_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.ST_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_ST_BORR_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.ST_Tot_borrowings = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_AGGREG_ST_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.ST_Aggregate_Amt = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPT_ADVANCES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Capital_adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Security_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_ORP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_A_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_A_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_TotLoan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_RP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_FromRelated_Parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLOW_OT_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_FromOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4 [0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Net_loan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_DUE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTA_Loans_Adv_dueByDir = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4CAPT_ADVANC_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Capital_adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURITY_DEP_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Security_Deposits = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_ADV_OR_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Loans_adv_related_parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_LOANS_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Otherloans_advances = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOT_LT_LOAN_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_TotLoan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_RP_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_FromRelated_Parties = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PROV_ALLW_OT_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_FromOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_LT_LOA_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Net_loan_Adv = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOAN_ADV_DUE_CR1[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.LTAD_Loans_Adv_dueByDir = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_ES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_Secured_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_CG_WS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_Secured_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_ES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_Unsecured_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNSECURD_CG_WS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_Unsecured_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_ES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_Doubtful_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DOUBTFUL_WS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_Doubtful_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_ES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_TotalTrade_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TOTAL_TR_WS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_TotalTrade_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_ES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_Provision_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LESS_PA_BAD_WS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_Provision_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_ES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_NetTrade_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_TRADE_R_WS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_NetTrade_WSM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_ES_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_DebtDue_ESM = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBTS_DUE_WS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objDetailedBalanceSheet_PR.TR_DebtDue_WSM = actualVal;
                                        }
                                        #endregion

                                        #endregion

                                        #region Financial Parameter
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].AMOUNT_ISSUE_ALL[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                                objFinancialParameter.Amt_Of_issue_Alloted = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Share_app_Money_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Share_app_Money_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY2[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Share_app_Money_3 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SHARE_APP_MONEY3[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Share_app_Money_4 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PAID_UP_CAPT_FC[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Paid_up_capital_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PAID_UP_CAPT_FC1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Paid_up_capital_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NUM_SHARES_BB[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.NoOfShares = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEP_ACCEPTED_REN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Deposit_Accepted = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLAI[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Deposit_Matured_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLA1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Deposit_Matured_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEP_MATURED_CLA2[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Deposit_Matured_3 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNCLAIMED_M_DEB[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Unclaimed_Debentures = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEBENTURES_CLAIM[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Claimed_Debentures = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTEREST_DEPOSIT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InterestOnDepositAccrued = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].UNPAID_DIVIDEND[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Unpaid_divedend = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVESTMENT_SUBSD[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InvestmentInCompany_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVESTMENT_GOVT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InvestmentInCompany_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_RESERVES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Capital_Reserves = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].AMT_DUE_TRANSFER[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Amt_TransferforIEPF = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTER_CORPORATE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Inter_Corp_Deposit = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].GROSS_VALUE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.GrossValueOfTransaction = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CAPITAL_SUBSDIES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.CapitalSubsidies = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CALLS_UNPAID_DIR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.CallsUnpaidByDirector = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CALLS_UNPAID_OTH[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.CallsUnpaidByOthers = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FORFEITED_SHARES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Forfeited_Shares_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FORFEITED_SHAR_R[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Forfeited_Shares_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BORROWING_FIA[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Borrowing_foreign_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BORROWING_FC[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Borrowing_foreign_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTER_CORP_BORR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InterCorporate_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INTER_CORP_BORR1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.InterCorporate_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].COMMERCIAL_PAPER[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.CommercialPaper = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.ConversionOfWarrant_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.ConversionOfWarrant_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CONVERSION_WR_DE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.ConversionOfWarrant_3 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].WARRANTS_ISSUED[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.WarrantsIssueInForeignCurrency = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].WARRANTS_ISSUED1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.WarrantsIssueInRupees = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFAULT_PAYMENT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.DefaultInPayment_1 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEFAULT_PAYMENT1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.DefaultInPayment_2 = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_OPERATING_LEA[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objFinancialParameter.WheatheOperatingLease = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DETAILS_CONVERSN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objFinancialParameter.ProvideDetailsOfConversion = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NET_WORTH_COMPAN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.NetWorthOfComp = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NUM_SHARE_HOLDRS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.NoOfShareHolders = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].SECURED_LOAN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.SecuredLoan = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].GROSS_FIXD_ASSET[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.GrossFixedAssets = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].DEPRECIATN_AMORT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Depreciation_Amortization = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MISC_EXPENDITURE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.Misc_Expenditure = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].UNHEDGED_FE_EXP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParameter.UnhedgedForeign = actualVal;
                                        }

                                        #endregion

                                        #region Shared Capital Raised
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].PUBLIC_ISSUE_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                                objSharedCapital.PublicIssue_E = actualVal;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PUBLIC_ISSUE_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PublicIssue_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONUS_ISSUE_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.BonusIssue_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].BONUS_ISSUE_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.BonusIssue_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RIGHTS_ISSUE_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.RightIssue_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RIGHTS_ISSUE_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.RightIssue_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PRIV_PLACEMENT_E[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PvtPlacementarising_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PRIV_PLACEMENT_P[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PvtPlacementarising_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHR_PRI_PLAC_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.OtherPvtPlacement_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHR_PRI_PLAC_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.OtherPvtPlacement_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PREF_ALLOTMENT_E[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PrferentialAllotment_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PREF_ALLOTMENT_P[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PrferentialAllotment_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].PUBLIC_ISSUE_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.PrferentialAllotment_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHR_PREF_ALL_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.OtherPreferentialallotment_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHR_PREF_ALL_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.OtherPreferentialallotment_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].ESOP_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.EmpStockOptionPlan_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].ESOP_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.EmpStockOptionPlan_P = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_ES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.Others_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objSharedCapital.Others_P = actualVal;
                                        }

                                        #endregion

                                        #region Cost Records
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].AMOUNT_ISSUE_ALL[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objCostRecords.MaintenanceOfCR = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objCostRecords.CentralExcise = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_COST_RECORDS1[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objCostRecords.AuditOfCR = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].CENTRAL_EXCISE1[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objCostRecords.CentralExcise1 = value;
                                            }
                                        }
                                        #endregion

                                        #region Profit & Loss

                                        #region Current Period
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FROM_DATE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objProfitLoss_CR.FROM_DATE_CR = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TO_DATE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objProfitLoss_CR.TO_DATE_CR = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.SALES_GOODS_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_T_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.SALES_GOODS_T_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.SALES_SUPPLY_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS1_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.SALES_GOODS1_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALE_GOODS_T1_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.SALE_GOODS_T1_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY1_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.SALES_SUPPLY1_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_INCOME_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.OTHER_INCOME_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_REVENUE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.TOTAL_REVENUE_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COST_MATERIAL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.COST_MATERIAL_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PURCHASE_STOCK_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PURCHASE_STOCK_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINISHED_GOODS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.FINISHED_GOODS_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].WORK_IN_PROG_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.WORK_IN_PROG_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].STOCK_IN_TRADE_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.STOCK_IN_TRADE_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EMP_BENEFIT_EX_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.EMP_BENEFIT_EX_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].MANGERIAL_REM_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.MANGERIAL_REM_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PAYMENT_AUDTRS_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PAYMENT_AUDTRS_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INSURANCE_EXP_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.INSURANCE_EXP_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].POWER_FUEL_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.POWER_FUEL_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINANCE_COST_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.FINANCE_COST_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEPRECTN_AMORT_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.DEPRECTN_AMORT_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_EXPENSES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.OTHER_EXPENSES_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EXPENSES_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.TOTAL_EXPENSES_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEFORE_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PROFIT_BEFORE_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXCEPTIONL_ITM_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.EXCEPTIONL_ITM_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEF_TAX_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PROFIT_BEF_TAX_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXTRAORDINARY_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.EXTRAORDINARY_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_B_TAX_7_8_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PROF_B_TAX_7_8_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CURRENT_TAX_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.CURRENT_TAX_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEFERRED_TAX_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.DEFERRED_TAX_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_OPER_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PROF_LOSS_OPER_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_DO_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PROF_LOSS_DO_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TAX_EXPNS_DIS_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.TAX_EXPNS_DIS_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_12_13_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PROF_LOS_12_13_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_11_14_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.PROF_LOS_11_14_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_BEFR_EI_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.BASIC_BEFR_EI_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_BEF_EI_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.DILUTED_BEF_EI_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_AFTR_EI_CR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.BASIC_AFTR_EI_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_AFT_EI_C[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_CR.DILUTED_AFT_EI_C = actualVal;
                                        }

                                        #endregion

                                        #region Previos Period

                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FROM_DATE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objProfitLoss_PR.FROM_DATE_CR = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TO_DATE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                {
                                                    objProfitLoss_PR.TO_DATE_CR = tempDate;
                                                }
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.SALES_GOODS_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS_T_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.SALES_GOODS_T_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.SALES_SUPPLY_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_GOODS1_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.SALES_GOODS1_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALE_GOODS_T1_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.SALE_GOODS_T1_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].SALES_SUPPLY1_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.SALES_SUPPLY1_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_INCOME_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.OTHER_INCOME_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_REVENUE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.TOTAL_REVENUE_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COST_MATERIAL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.COST_MATERIAL_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PURCHASE_STOCK_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PURCHASE_STOCK_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINISHED_GOODS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.FINISHED_GOODS_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].WORK_IN_PROG_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.WORK_IN_PROG_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].STOCK_IN_TRADE_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.STOCK_IN_TRADE_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EMP_BENEFIT_EX_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.EMP_BENEFIT_EX_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].MANGERIAL_REM_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.MANGERIAL_REM_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PAYMENT_AUDTRS_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PAYMENT_AUDTRS_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INSURANCE_EXP_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.INSURANCE_EXP_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].POWER_FUEL_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.POWER_FUEL_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].FINANCE_COST_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.FINANCE_COST_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEPRECTN_AMORT_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.DEPRECTN_AMORT_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_EXPENSES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.OTHER_EXPENSES_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EXPENSES_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.TOTAL_EXPENSES_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEFORE_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PROFIT_BEFORE_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXCEPTIONL_ITM_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.EXCEPTIONL_ITM_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROFIT_BEF_TAX_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PROFIT_BEF_TAX_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXTRAORDINARY_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.EXTRAORDINARY_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_B_TAX_7_8_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PROF_B_TAX_7_8_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CURRENT_TAX_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.CURRENT_TAX_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DEFERRED_TAX_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.DEFERRED_TAX_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_OPER_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PROF_LOSS_OPER_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOSS_DO_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PROF_LOSS_DO_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TAX_EXPNS_DIS_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.TAX_EXPNS_DIS_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_12_13_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PROF_LOS_12_13_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_LOS_11_14_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.PROF_LOS_11_14_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_BEFR_EI_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.BASIC_BEFR_EI_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_BEF_EI_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.DILUTED_BEF_EI_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_AFTR_EI_PR[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.BASIC_AFTR_EI_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_AFT_EI_P[0]")
                                        {
                                            if (!(string.IsNullOrEmpty(value)))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objProfitLoss_PR.DILUTED_AFT_EI_C = actualVal;
                                        }

                                        #endregion

                                        #endregion

                                        #region Detailed Profit and Loss items

                                        #region Current
                                        if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXP_GOODS_FOB_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.EXP_GOODS_FOB_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_DIVD_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.INTEREST_DIVD_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.ROYALTY_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.KNOW_HOW_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_CONS_FEE_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.PROF_CONS_FEE_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHR_INCOME_E_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.OTHR_INCOME_E_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EARNG_FE_C[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.TOTAL_EARNG_FE_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].RAW_MATERIAL_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.RAW_MATERIAL_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COMPONENT_SP_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.COMPONENT_SP_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CAPITAL_GOODS_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.CAPITAL_GOODS_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_EXP_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.ROYALTY_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_EXP_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.KNOW_HOW_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_CON_FEE_E_C[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.PROF_CON_FEE_E_C = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_EXP_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.INTEREST_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_MATTERS_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.OTHER_MATTERS_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIVIDEND_PAID_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.DIVIDEND_PAID_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOT_EXP_FE_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_CR.TOT_EXP_FE_CR = actualVal;

                                        }
                                        #endregion

                                        #region Previous
                                        if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXP_GOODS_FOB_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.EXP_GOODS_FOB_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_DIVD_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.INTEREST_DIVD_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.ROYALTY_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.KNOW_HOW_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_CONS_FEE_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.PROF_CONS_FEE_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHR_INCOME_E_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.OTHR_INCOME_E_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_EARNG_FE_C[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.TOTAL_EARNG_FE_C = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].RAW_MATERIAL_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.RAW_MATERIAL_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].COMPONENT_SP_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.COMPONENT_SP_CR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CAPITAL_GOODS_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.CAPITAL_GOODS_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].ROYALTY_EXP_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.ROYALTY_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].KNOW_HOW_EXP_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.KNOW_HOW_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROF_CON_FEE_E_C[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.PROF_CON_FEE_E_C = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INTEREST_EXP_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.INTEREST_EXP_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].OTHER_MATTERS_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.OTHER_MATTERS_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DIVIDEND_PAID_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.DIVIDEND_PAID_CR = actualVal;

                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOT_EXP_FE_PR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objDetailedPL_PR.TOT_EXP_FE_CR = actualVal;

                                        }
                                        #endregion

                                        #endregion

                                        #region Financial Parameters PL Items
                                        if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROPOSED_DIVIDND[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.PROPOSED_DIVIDND = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROPOSED_DIV_PER[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.PROPOSED_DIV_PER = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BASIC_EARNING_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.BASIC_EARNING_PS = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].DILUTED_EARN_PS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.DILUTED_EARN_PS = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PROPOSED_DIVIDND[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.PROPOSED_DIVIDND = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].INCOME_FOREIGN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.INCOME_FOREIGN = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].EXPENDIT_FOREIGN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.EXPENDIT_FOREIGN = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].REVENUE_SUBSIDIE[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.REVENUE_SUBSIDIE = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].RENT_PAID[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.RENT_PAID = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].CONSUMPTION_STOR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.CONSUMPTION_STOR = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].GROSS_VALUE_TRAN[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.GROSS_VALUE_TRAN = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].BAD_DEBTS_RP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objFinancialParaPL.BAD_DEBTS_RP = actualVal;
                                        }

                                        #endregion

                                        #region CSR

                                        #region CSR Header
                                        if (key == "data[0].ZMCA_NCA_AOC4_II[0].RB_CSR_APPLICABL[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objCSRHeader.RB_CSR_APPLICABL = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TURNOVER[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.TURNOVER = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].NET_WORTH[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }
                                            }
                                            objCSRHeader.NET_WORTH = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].PRESCRIBED_CSR_E[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.PRESCRIBED_CSR_E = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].TOTAL_AMT_SPENT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.TOTAL_AMT_SPENT = actualVal;
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].AMOUNT_SPENT_LA[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                if (!decimal.TryParse(value.Replace(",", ""), out actualVal))
                                                {
                                                    actualVal = 0;
                                                }

                                            }
                                            objCSRHeader.AMOUNT_SPENT_LA = actualVal;
                                        }
                                        #endregion

                                        #region CSR Details
                                        else if (key == "data[0].ZMCA_NCA_AOC4_II[0].NUM_CSR_ACTIVITY[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                var countCSR = 0;
                                                if (int.TryParse(value, out countCSR))
                                                {
                                                    if (countCSR > 0)
                                                    {
                                                        objCSRHeader.lstDetails = new List<CSRDetails_VM>();
                                                        var strTemp = string.Empty;
                                                        for (int i = 0; i < countCSR; i++)
                                                        {
                                                            var objCSRDetails = new CSRDetails_VM();
                                                            try
                                                            {
                                                                objCSRDetails.CSR_PROJECT_ACTV = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + i + "].CSR_PROJECT_ACTV[0]").FirstOrDefault().Value.InnerText).Trim();

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + i + "].SECTOR_PROJ_COVR[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                objCSRDetails.SECTOR_PROJ_COVR = GetCSRProjectIdByCode(strTemp);

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + i + "].STATE_UT[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                objCSRDetails.STATE_UT = GetCSRStateIdByCode(strTemp);

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + i + "].AMOUNT_OUTLAY[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                if (!string.IsNullOrEmpty(strTemp))
                                                                {
                                                                    if (!decimal.TryParse(strTemp.Replace(",", ""), out actualVal))
                                                                    {
                                                                        actualVal = 0;
                                                                    }
                                                                    objCSRDetails.AMOUNT_OUTLAY = actualVal;
                                                                }

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + i + "].AMOUNT_SPENT_PRJ[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                if (!string.IsNullOrEmpty(strTemp))
                                                                {
                                                                    if (!decimal.TryParse(strTemp.Replace(",", ""), out actualVal))
                                                                    {
                                                                        actualVal = 0;
                                                                    }
                                                                    objCSRDetails.AMOUNT_SPENT_PRJ = actualVal;
                                                                }

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIII[0].DATA[" + i + "].MODE_AMOUNT_SPNT[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                if (!string.IsNullOrEmpty(strTemp))
                                                                {
                                                                    objCSRDetails.MODE_AMOUNT_SPNT = GetCSRModeOfAmtIdByCode(strTemp);
                                                                }

                                                                objCSRHeader.lstDetails.Add(objCSRDetails);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #endregion

                                        #region RPT

                                        #region Contracts
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NUM_CONTRACTS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                var countCSR = 0;
                                                if (int.TryParse(value, out countCSR))
                                                {
                                                    if (countCSR > 0)
                                                    {
                                                        objRPT.lstContracts = new List<RPT_Contracts_VM>();
                                                        var strTemp = string.Empty;
                                                        for (int i = 0; i < countCSR; i++)
                                                        {
                                                            var objContracts = new RPT_Contracts_VM();
                                                            try
                                                            {
                                                                objContracts.NAME_RELATED_PAR = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[" + i + "].NAME_RELATED_PAR[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                objContracts.NATURE_OF_RELATN = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[" + i + "].NATURE_OF_RELATN[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                objContracts.NATURE_OF_CONTRA = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_1[0].DATA[" + i + "].NATURE_OF_CONTRA[0]").FirstOrDefault().Value.InnerText).Trim();

                                                                objContracts.DURATION_OF_CONT = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[" + i + "].DURATION_OF_CONT[0]").FirstOrDefault().Value.InnerText).Trim();

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[" + i + "].DATE_OF_APPROVAL[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                if (!(string.IsNullOrEmpty(strTemp)))
                                                                {
                                                                    if (DateTime.TryParseExact(strTemp, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                                    {
                                                                        objContracts.DATE_OF_APPROVAL = tempDate;
                                                                    }
                                                                }

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[" + i + "].AMOUNT_PAID[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                if (!string.IsNullOrEmpty(strTemp))
                                                                {
                                                                    if (!decimal.TryParse(strTemp.Replace(",", ""), out actualVal))
                                                                    {
                                                                        actualVal = 0;
                                                                    }
                                                                    objContracts.AMOUNT_PAID = actualVal;
                                                                }

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_2[0].DATA[" + i + "].DATE_SPCL_RESOLT[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                if (!(string.IsNullOrEmpty(strTemp)))
                                                                {
                                                                    if (DateTime.TryParseExact(strTemp, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                                    {
                                                                        objContracts.DATE_SPCL_RESOLT = tempDate;
                                                                    }
                                                                }
                                                                objRPT.lstContracts.Add(objContracts);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Material Contracts
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].NUM_MAT_CONTRACT[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                var countCSR = 0;
                                                if (int.TryParse(value, out countCSR))
                                                {
                                                    if (countCSR > 0)
                                                    {
                                                        objRPT.lstMaterialContracts = new List<RPT_MaterialContracts_VM>();
                                                        var strTemp = string.Empty;
                                                        for (int i = 0; i < countCSR; i++)
                                                        {
                                                            var objContracts = new RPT_MaterialContracts_VM();
                                                            try
                                                            {
                                                                objContracts.NAME_RELATED_PAR = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[" + i + "].NAME_RELATED_PAR[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                objContracts.NATURE_OF_RELATN = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[" + i + "].NATURE_OF_RELATN[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                objContracts.NATURE_OF_CONTRA = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_3[0].DATA[" + i + "].NATURE_OF_CONTRA[0]").FirstOrDefault().Value.InnerText).Trim();

                                                                objContracts.DURATION_OF_CONT = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_4[0].DATA[" + i + "].DURATION_OF_CONT[0]").FirstOrDefault().Value.InnerText).Trim();

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_4[0].DATA[" + i + "].DATE_OF_APPROVAL[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                if (!(string.IsNullOrEmpty(strTemp)))
                                                                {
                                                                    if (DateTime.TryParseExact(strTemp, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                                    {
                                                                        objContracts.DATE_OF_APPROVAL = tempDate;
                                                                    }
                                                                }

                                                                strTemp = (pdfReader.AcroFields.Xfa.DatasetsSom.Name2Node.ToList().Where(k => k.Key == "data[0].T_ZNCA_AOC_4_SIV_4[0].DATA[" + i + "].AMOUNT_PAID[0]").FirstOrDefault().Value.InnerText).Trim();
                                                                if (!string.IsNullOrEmpty(strTemp))
                                                                {
                                                                    if (!decimal.TryParse(strTemp.Replace(",", ""), out actualVal))
                                                                    {
                                                                        actualVal = 0;
                                                                    }
                                                                    objContracts.AMOUNT_PAID = actualVal;
                                                                }
                                                                objRPT.lstMaterialContracts.Add(objContracts);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #endregion

                                        #region Auditors Report
                                        if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_COMPTRL_AUDTR[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objAuditorReport.RB_COMPTRL_AUDTR = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objAuditorReport.RB_AUDTR_REPORT = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_COMP_AUDT_REP[0]")
                                        {
                                            if (value == "YES")
                                            {
                                                objAuditorReport.RB_COMP_AUDT_REP = true;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FIXED_ASSETS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.FIXED_ASSETS = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].INVENTORIES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.INVENTORIES = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].LOANS_GIVEN_COMP[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.LOANS_GIVEN_COMP = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].ACCEPTANCE_PUB_D[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.ACCEPTANCE_PUB_D = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].MAINTENANCE_CR[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.MAINTENANCE_CR = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].STATUTORY_DUES[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.STATUTORY_DUES = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].TERM_LOANS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.TERM_LOANS = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].FRAUD_NOTICED[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.FRAUD_NOTICED = value;
                                            }
                                        }
                                        else if (key == "data[0].ZMCA_NCA_AOC_4[0].OTHER_COMMENTS[0]")
                                        {
                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                objAuditorReport.OTHER_COMMENTS = value;
                                            }
                                        }
                                        #endregion

                                        #region Misc Authorization
                                        if (objMiscAuth != null)
                                        {
                                            var strValue = string.Empty;

                                            if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_SECRETARIAL_A[0]")
                                            {
                                                if (value == "YES")
                                                {
                                                    objMiscAuth.RB_SECRETARIAL_A = true;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_AUDTR_REPORT[0]")
                                            {
                                                if (value == "YES")
                                                {
                                                    objMiscAuth.RB_AUDTR_REPORT = true;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].CVRN[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.CVRN = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].DECLARATION_DATE[0]")
                                            {
                                                if (!(string.IsNullOrEmpty(value)))
                                                {
                                                    if (DateTime.TryParseExact(value, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None, out tempDate))
                                                    {
                                                        objMiscAuth.DECLARATION_DATE = tempDate;
                                                    }
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].DESIGNATION[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.DESIGNATION = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].DIN_PAN_MEM_NUM[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.DIN_PAN_MEM_NUM = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_CA_COA_CS[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.RB_CA_COA_CS = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].RB_ASSOC_FELLOW[0]")
                                            {
                                                if (value == "YES")
                                                {
                                                    objMiscAuth.RB_ASSOC_FELLOW = true;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].MEMBERSHIP_NUM[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.MEMBERSHIP_NUM = value;
                                                }
                                            }
                                            else if (key == "data[0].ZMCA_NCA_AOC_4[0].CERT_PRACTICE_NO[0]")
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    objMiscAuth.CERT_PRACTICE_NO = value;
                                                }
                                            }
                                        }
                                        #endregion

                                        //End
                                    }

                                    SaveCompanyInfo(objCompanyInfo, userId);

                                    List<AOC4_BalanceSheetVM> objList = new List<AOC4_BalanceSheetVM>();
                                    objList.Add(objBalanceSheet_CR);
                                    objList.Add(objBalanceSheet_PR);
                                    SaveBalanceSheet(objList, userId);

                                    List<AOC4_DetailedBalanceSheetVM> objListDbs = new List<AOC4_DetailedBalanceSheetVM>();
                                    objListDbs.Add(objDetailedBalanceSheet_CR);
                                    objListDbs.Add(objDetailedBalanceSheet_PR);
                                    SaveDetailedBalanceSheet(objListDbs, userId);

                                    List<AOC4_ProfitLossVM> objListPL = new List<AOC4_ProfitLossVM>();
                                    objListPL.Add(objProfitLoss_CR);
                                    objListPL.Add(objProfitLoss_PR);
                                    SaveProfitLoss(objListPL, userId);

                                    List<DetailedProfitLossVM> objListDPL = new List<DetailedProfitLossVM>();
                                    objListDPL.Add(objDetailedPL_CR);
                                    objListDPL.Add(objDetailedPL_PR);
                                    SaveDetailedProfitLoss(objListDPL, userId);

                                    SaveFinancialParameter(objFinancialParameter, userId);
                                    SaveSharedCapitalRaised(objSharedCapital, userId);
                                    SaveFinancialParameter_PL(objFinancialParaPL, userId);
                                    SaveAuditReport(objAuditorReport, userId);
                                    SaveDetailsOfCostRecords(objCostRecords, userId);
                                    //
                                    SaveReportingCSR(objCSRHeader, userId);
                                    SaveRPT(objRPT, userId);
                                    //
                                    SaveMiscAuth(objMiscAuth, userId);

                                }
                                else
                                {
                                    obj.Error = objAOC4.Error;
                                    obj.Message = objAOC4.Message;
                                }
                            }
                            else
                            {
                                obj.Error = true;
                                obj.Message = "Unable to read data from uploaded file";
                            }
                        }
                        else
                        {
                            obj.Error = true;
                            obj.Message = "Please upload .pdf file";
                        }
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Unable to read data from uploaded file";
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Please upload file";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

    }
}