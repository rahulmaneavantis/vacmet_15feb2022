﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.HelpVideo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.AOC4
{
    public interface IAOC4Service
    {
        #region Master
        List<AOC4_List_VM> GetAOC4List(int customerId);
        AOC4_List_VM CreateAOC4(AOC4_List_VM obj, int userId, int customerId);
        AOC4_List_VM GetAOC4Master(long id, int customerId);
        List<AOC4DetailsVM> GetAOC4DetailsById(int id, int customerId);
        AOC4_CompanyInfoVM GetCompanyInfo(long id, int entityId);
        AOC4_CompanyInfoVM SaveCompanyInfo(AOC4_CompanyInfoVM obj, int userId);
        AOC4_CompanyHoldingVM GetCompanyHolding(int entityId);

        #endregion

        #region Balance Sheet
        Message SaveBalanceSheet(List<AOC4_BalanceSheetVM> objList, int userId);
        AOC4_BalanceSheetPrevVM GetBalanceSheet(long id, int customerId);
        AOC4_BalanceSheetPrevVM GetBalanceSheetDefaultDataForExcel(long id, int customerId);
        #endregion

        #region Detailed Balance sheet
        Message SaveDetailedBalanceSheet(List<AOC4_DetailedBalanceSheetVM> objList, int userId);
        AOC4_DetailedBalanceSheetPrevVM GetDetailedBalanceSheet(long id, int customerId);
        AOC4_DetailedBalanceSheetPrevVM GetDetailedBalanceSheetDefaultDataForExcel(long id, int customerId);
        #endregion

        #region Financial Parameter
        Message SaveFinancialParameter(AOC4_FinancialParameterVM _objaocExcel, int userID);
        AOC4_FinancialParameterVM GetFinancialParameteritems(long id, int customerId);
        #endregion

        #region Shared Capital Raised
        Message SaveSharedCapitalRaised(ShareCapitalRaisedVM _objSharecapital, int userID);
        ShareCapitalRaisedVM GetSharedCapitalitems(long id, int customerId);
        #endregion

        #region Cost Records and Cost Audit
        DetailsOfCostRecordsVM GetDetailsOfCostRecords(long id);
        DetailsOfCostRecordsVM SaveDetailsOfCostRecords(DetailsOfCostRecordsVM obj, int userId);
        #endregion

        #region Profit & Loss
        Message SaveProfitLoss(List<AOC4_ProfitLossVM> objList, int userId);
        AOC4_ProfitLossPrevVM GetProfitLoss(long id, int customerId);
        AOC4_ProfitLossPrevVM GetProfitLossDefaultDataForExcel(long id, int customerId);
        #endregion

        #region Detailed Profit and Loss items 
        Message SaveDetailedProfitLoss(List<DetailedProfitLossVM> objList, int userId);
        DetailedProfitLossPrevVM GetDetailedProfitLossitems(long id, int customerId);
        DetailedProfitLossPrevVM GetDetailedProfiLossDefaultData(long id, int customerId);
        #endregion

        #region FinancialParameter Profit and Loss Items
        Message SaveFinancialParameter_PL(FinancialParameter_PLVM _objFPL, int userID);
        FinancialParameter_PLVM GetFinancialParameter_PLDefaultData(long id, int customerId);
        #endregion

        #region Related Party Transaction
        RPT_VM GetRPTDetails(long id, int entityId);
        RPT_VM SaveRPT(RPT_VM obj, int userId);
        #endregion


        #region MiscAuth
        MiscellaneousAuthorizationVM GetMiscAuth(long id);
        MiscellaneousAuthorizationVM SaveMiscAuth(MiscellaneousAuthorizationVM obj, int userId);

        #endregion

        #region CSR
        Message SaveReportingCSR(CSR_VM _objCSR, int userID);
        CSR_VM GetReportingCSR(long id);
        List<CSRDetails_VM> GetDefaultCSRExcelData(long id, int customerID);
        CSR_VM GetReportingCSRWithDetails(long id);
        #endregion

        #region  Auditor's Report
        AuditorsReport_VM SaveAuditReport(AuditorsReport_VM obj, int userId);
        AuditorsReport_VM GetAuditorsReport(long id);
        #endregion

        AOC4FormUploadVM UpoadAOC4(AOC4FormUploadVM obj, int custId, int userId);

    }
}
