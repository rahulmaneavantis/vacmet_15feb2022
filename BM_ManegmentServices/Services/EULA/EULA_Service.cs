﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.EULA;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.EULA
{
    public class EULA_Service : IEULA_Service
    {
        public List<EULA_DetailsVM> GetEULADetails(int userId)
        {
            List<EULA_DetailsVM> result = null;
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_SP_GetEULAList(userId)
                              orderby row.UploadedOn descending, row.AcceptedOn descending
                              select new EULA_DetailsVM
                              {
                                  CustomerName = row.Customer,
                                  UserID = (int)row.UserID,
                                  UserName = row.UserName,
                                  MobileNo = row.ContactNumber,
                                  EmailID = row.Email,
                                  EULA_ID = row.EULA_ID,
                                  EULA = row.EULA,
                                  AcceptedOn = row.AcceptedOn,
                                  UploadedOn = row.UploadedOn,
                                  FilePath = row.FilePath,
                                  IsDocUploaded = row.FilePath == null ? false : true,
                                  StatusId = row.StatusId,
                                  Status = row.StatusName,
                                  //MembershipNumber = row.MembershipNumber,
                                  LastLoginTime = row.LastLoginTime,
                                  LastLoginBefore = row.LastLoginBefore,
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public EULA_DetailsVM GetEULADetailsById(int id, string userMode)
        {
            var result = new EULA_DetailsVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.EULADetails
                              join u in entities.Users on row.UserID equals u.ID
                              join c in entities.Customers on u.CustomerID equals c.ID
                              where row.ID == id && row.IsActive == true
                              select new EULA_DetailsVM
                              {
                                  CustomerID = c.ID,
                                  CustomerName = c.Name,
                                  UserID = (int)row.UserID,
                                  UserName = u.FirstName + " " + u.LastName,
                                  MobileNo = u.ContactNumber,
                                  EmailID = u.Email,
                                  EULA_ID = row.ID,
                                  EULA = row.EULA,
                                  AcceptedOn = row.AcceptedOn,
                                  UploadedOn = row.UploadedOn,
                                  FilePath = row.FilePath,
                                  StatusId = row.StatusId
                              }).FirstOrDefault();

                    if(result != null)
                    {
                        result.UserMode = userMode;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public EULA_DetailsVM GetEULADetailsByUserId(int userId)
        {
            var result = new EULA_DetailsVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.EULADetails
                              join u in entities.Users on row.UserID equals u.ID
                              join c in entities.Customers on u.CustomerID equals c.ID
                              where u.ID == userId && row.IsActive == true
                              select new EULA_DetailsVM
                              {
                                  CustomerID = c.ID,
                                  CustomerName = c.Name,
                                  UserID = (int)row.UserID,
                                  UserName = u.FirstName + " " + u.LastName,
                                  MobileNo = u.ContactNumber,
                                  EmailID = u.Email,
                                  EULA_ID = row.ID,
                                  EULA = row.EULA,
                                  AcceptedOn = row.AcceptedOn,
                                  UploadedOn = row.UploadedOn,
                                  FilePath = row.FilePath,
                                  StatusId = row.StatusId
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public EULA_DetailsVM SaveEULAStatus(EULA_DetailsVM obj, byte statusId, int userId)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    var _obj = (from row in entities.EULADetails
                                where row.ID == obj.EULA_ID && row.IsActive == true
                                select row).FirstOrDefault();
                    if (_obj != null)
                    {
                        _obj.StatusId = statusId;
                        _obj.StatusUpdatedBy = userId;
                        _obj.StatusUpdatedOn = DateTime.Now;
                        entities.SaveChanges();

                        obj.StatusId = statusId;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = SecretarialConst.Messages.serverError;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public string GetEULADocById(int id)
        {
            var fileName = "";
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                fileName = (from row in entities.EULADetails
                            where row.ID == id && row.IsActive == true
                            select row.FilePath).FirstOrDefault();
            }
            return fileName;
        }
        public void SendCrendentialsPost(int UserId, string email, int sendBy)
        {
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    BM_EULAResetPasswordLog obj = new BM_EULAResetPasswordLog();
                    obj.UserId = UserId;
                    obj.EmailId = email;
                    obj.SendOn = DateTime.Now;
                    obj.SendBy = sendBy;
                    entities.BM_EULAResetPasswordLog.Add(obj);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public UserVM GetUser(int userId)
        {
            var obj = new UserVM();
            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
            {
                obj = (from row in entities.Users
                            where row.ID == userId
                            select new UserVM
                            {
                                UserID = row.ID,
                                FirstName = row.FirstName,
                                LastName = row.LastName,
                                Email = row.Email,
                            }).FirstOrDefault();
            }
            return obj;
        }

        public bool ChangePassword(long userId, string encryPassword)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        var userToUpdate = (from entry in entities.Users
                                        where entry.ID == userId
                                        select entry).FirstOrDefault();

                        userToUpdate.Password = encryPassword;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = null;
                        userToUpdate.EnType = "A";
                        userToUpdate.ChangePasswordFlag = false;

                        entities.SaveChanges();

                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        public bool ChangePasswordAudit(long userId, string encryPassword)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                using (System.Data.Entity.DbContextTransaction dbtransaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        mst_User userToUpdate = new mst_User();
                        userToUpdate = (from entry in entities.mst_User
                                        where entry.ID == userId
                                        select entry).FirstOrDefault();

                        userToUpdate.Password = encryPassword;
                        userToUpdate.ChangPasswordDate = DateTime.UtcNow;
                        userToUpdate.LastLoginTime = null;
                        userToUpdate.ChangePasswordFlag = false;
                        userToUpdate.EnType = "A";
                        entities.SaveChanges();
                        dbtransaction.Commit();
                        return true;

                    }
                    catch (Exception)
                    {
                        dbtransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        public void WrongAttemptCountUpdate(long userID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var userToUpdate = (from Row in entities.Users
                                        where Row.ID == userID && Row.IsDeleted == false && Row.IsActive == true
                                        select Row).FirstOrDefault();

                    if (userToUpdate != null)
                    {
                        userToUpdate.WrongAttempt = 0;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }
    }
}