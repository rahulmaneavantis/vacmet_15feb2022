﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using BM_ManegmentServices.Services.Forms;
using BM_ManegmentServices.Services.DocumentManagenemt;
using BM_ManegmentServices.Services.Compliance;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Data.Entity.SqlServer;

namespace BM_ManegmentServices.Services.Profile
{
    public class MyProfile : IMyProfile
    {
        IForms_Service objIForms_Service;
        IFileData_Service objIFileData_Service;
        IDirectorCompliances objIDirectorCompliances;

        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public MyProfile(IForms_Service objForms_Service, IFileData_Service objFileData_Service, IDirectorCompliances objDirectorCompliances)
        {
            objIForms_Service = objForms_Service;
            objIFileData_Service = objFileData_Service;
            objIDirectorCompliances = objDirectorCompliances;
        }

        public bool Canceleditchange(long directorID, int userID)
        {
            try
            {
                var checkedittype = (from row in entities.BM_DirectorTypeOfChanges where row.DirectorId == directorID && row.UserID == userID && row.IsActive == true select row).FirstOrDefault();
                if (checkedittype != null)
                {
                    checkedittype.IsActive = false;
                    entities.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public ResignationofDirector GetDetailsofIntrest(long? iD, long? entityId)
        {
            try
            {
                var getdetails = (from row in entities.BM_Directors_DetailsOfInterest
                                  where row.Director_Id == iD
                                  && row.EntityId == entityId
                                  select new ResignationofDirector
                                  {
                                      DirectorID = iD,
                                      EntityID = entityId,
                                      ResignationDate = row.ResignedDate,
                                      CESSATION = row.CESSATION,
                                      isResign = row.IsResigned != null ? (bool)row.IsResigned : false
                                  }).FirstOrDefault();
                return getdetails;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Director_MasterVM GetDirectorDetails(int userId, int customerID)
        {
            List<string> a = new List<string>();
            Director_MasterVM getDirectorDetails = new Director_MasterVM();
            try
            {
                var checkdirectorDetails = (from Dirs in entities.BM_DirectorMaster
                                            where Dirs.UserID == userId
                                            select Dirs).FirstOrDefault();

                getDirectorDetails = (from Dir in entities.BM_DirectorMaster
                                      where Dir.UserID == userId && Dir.Customer_Id == customerID
                                      select new Director_MasterVM
                                      {
                                          Salutation = Dir.Salutation,
                                          FirstName = Dir.FirstName,
                                          MiddleName = Dir.MiddleName,
                                          LastName = Dir.LastName,
                                          DOB = SqlFunctions.DateName("day", Dir.DOB) + "/" + SqlFunctions.DateName("month", Dir.DOB) + "/" + SqlFunctions.DateName("year", Dir.DOB),
                                          DESC_ExpiryDateprofile= SqlFunctions.DateName("day", Dir.DSC_ExpiryDate) + "/" + SqlFunctions.DateName("month", Dir.DSC_ExpiryDate) + "/" + SqlFunctions.DateName("year", Dir.DSC_ExpiryDate),
                                          Gender = Dir.Gender,
                                          DIN = Dir.DIN,
                                          MobileNo = Dir.MobileNo,
                                          EmailId = Dir.EmailId_Personal,
                                          EmailId_Official = Dir.EmailId_Personal,
                                          FatherFirstName = Dir.Father,
                                          FatherMiddleName = Dir.FatherMiddleName,
                                          FatherLastName = Dir.FatherLastName,
                                          Permenant_Address_Line1 = Dir.Permenant_Address_Line1,
                                          Permenant_Address_Line2 = Dir.Permenant_Address_Line2,
                                          Permenant_StateId = Dir.Permenant_StateId,
                                          Permenant_CityId = Dir.Permenant_CityId,
                                          Permenant_PINCode = Dir.Permenant_PINCode,
                                          Present_Address_Line1 = Dir.Present_Address_Line1,
                                          Present_Address_Line2 = Dir.Present_Address_Line2,
                                          Present_StateId = Dir.Present_StateId,
                                          Present_CityId = Dir.Present_CityId,
                                          Present_PINCode = Dir.Present_PINCode,
                                          IsSameAddress = Dir.IsSameAddress,
                                          //EducationalQualification = Dir.EducationalQualification,
                                          //Occupation = Dir.Occupation,
                                          //AreaOfOccupation = Dir.AreaOfOccupation,
                                          EducationalQualification = Dir.EducationalQualification,
                                          OtherQualification = Dir.OtherQualification,
                                          Occupation = Dir.Occupation,
                                          AreaOfOccupation = Dir.AreaOfOccupation,

                                          OtherOccupation = Dir.OtherOccupation,
                                          ResidentInIndia = Dir.ResidentInIndia,
                                          Nationality = Dir.Nationality,
                                          PAN = Dir.PAN,
                                          Adhaar = Dir.Aadhaar,
                                          PassportNo = Dir.PassportNo,
                                          DESC_ExpiryDate = Dir.DSC_ExpiryDate,
                                          ID = Dir.Id,
                                          Aadhaar_Doc_Name = Dir.Aadhar_Doc,
                                          Photo_Doc_Name = Dir.Photo_Doc,
                                          Passport_Doc_Name = Dir.Passport_Doc,
                                          PAN_Doc_Name = Dir.PAN_Doc,
                                          MotherName = Dir.Mother,
                                          CS_Membersip = Dir.CS_MemberNo


                                      }).FirstOrDefault();
                if (getDirectorDetails != null)
                {
                    if (getDirectorDetails != null)
                    {
                        getDirectorDetails.IsCAmember = true;
                    }
                }

                if (checkdirectorDetails.NumberofChanges > 0)
                {
                    var checkfordetailsedit = (from x in entities.BM_DirectorTypeOfChanges
                                               join
    y in entities.BM_DirectorTypeChangesMapping on x.Id equals y.TypeChangeMaapingID
                                               where x.DirectorId == getDirectorDetails.ID
                                               && y.IsActive == true
                                               && x.IsActive == true
                                               select new
                                               {
                                                   x,
                                                   y
                                               }).FirstOrDefault();

                    if (checkfordetailsedit.x.Name_of_Director)
                    {
                        getDirectorDetails.Salutation = checkfordetailsedit.y.Salutation;
                        getDirectorDetails.FirstName = checkfordetailsedit.y.FirstName;
                        getDirectorDetails.MiddleName = checkfordetailsedit.y.MiddleName;
                        getDirectorDetails.LastName = checkfordetailsedit.y.LastName;
                        a.Add("Name");
                    }
                    if (checkfordetailsedit.x.FatherName)
                    {
                        getDirectorDetails.FatherFirstName = checkfordetailsedit.y.FatherFirstName;
                        getDirectorDetails.FatherMiddleName = checkfordetailsedit.y.FatherMiddleName;
                        getDirectorDetails.FatherLastName = checkfordetailsedit.y.FatherLastName;
                        a.Add("Father's Name");
                    }
                    if (checkfordetailsedit.x.DateofBirth)
                    {
                        string date1 = string.Empty;
                        // getDirectorDetails.DOB
                        // date1= SqlFunctions.DateName("day", checkfordetailsedit.y.DOB) + "/" + SqlFunctions.DateName("month", checkfordetailsedit.y.DOB) + "/" + SqlFunctions.DateName("year", checkfordetailsedit.y.DOB);
                        getDirectorDetails.DOB = Convert.ToDateTime(checkfordetailsedit.y.DOB).ToString("dd/MM/yyyy");
                        // getDirectorDetails.DOB = checkfordetailsedit.y.DOB;
                        a.Add("Date of Birth");
                    }
                    if (checkfordetailsedit.x.Gender)
                    {
                        getDirectorDetails.Gender = checkfordetailsedit.y.Gender;
                        a.Add("Gender");
                    }
                    if (checkfordetailsedit.x.PAN)
                    {
                        getDirectorDetails.PAN = checkfordetailsedit.y.PAN;
                        a.Add("PAN");
                    }
                    if (checkfordetailsedit.x.EmailID)
                    {
                        getDirectorDetails.EmailId = checkfordetailsedit.y.EmailId_Personal;
                        getDirectorDetails.EmailId_Official = checkfordetailsedit.y.EmailId_Official;
                        a.Add("Email");
                    }
                    if (checkfordetailsedit.x.Mobile)
                    {
                        getDirectorDetails.MobileNo = checkfordetailsedit.y.MobileNo;

                        a.Add("Mobile");
                    }
                    if (checkfordetailsedit.x.AadharNumber)
                    {
                        getDirectorDetails.Adhaar = checkfordetailsedit.y.Aadhaar;

                        a.Add("Aadhar");
                    }
                    if (checkfordetailsedit.x.Parmanent_Address)
                    {
                        getDirectorDetails.Permenant_Address_Line1 = checkfordetailsedit.y.Permenant_Address_Line1;
                        getDirectorDetails.Permenant_Address_Line2 = checkfordetailsedit.y.Permenant_Address_Line1;
                        getDirectorDetails.Permenant_StateId = checkfordetailsedit.y.Permenant_StateId;
                        getDirectorDetails.Permenant_CityId = checkfordetailsedit.y.Permenant_CityId;
                        getDirectorDetails.Permenant_PINCode = checkfordetailsedit.y.Permenant_PINCode;
                        a.Add("Permanent Address");
                    }
                    if (checkfordetailsedit.x.Present_Address)
                    {
                        getDirectorDetails.Present_Address_Line1 = checkfordetailsedit.y.Present_Address_Line1;
                        getDirectorDetails.Present_Address_Line2 = checkfordetailsedit.y.Present_Address_Line2;
                        getDirectorDetails.Present_StateId = checkfordetailsedit.y.Present_StateId;
                        getDirectorDetails.Present_CityId = checkfordetailsedit.y.Present_CityId;
                        getDirectorDetails.Present_PINCode = checkfordetailsedit.y.Present_PINCode;
                        a.Add("Present Address");
                    }
                    if (checkfordetailsedit.x.Passport)
                    {
                        getDirectorDetails.PassportNo = checkfordetailsedit.y.PassportNo;

                        a.Add("Passport Number");
                    }
                    getDirectorDetails.ChangedBy = (from u in entities.Users where u.ID == checkfordetailsedit.x.Changedby select u.FirstName + " " + u.LastName).FirstOrDefault();

                    if (checkdirectorDetails.NumberofChanges != null)
                        getDirectorDetails.NumberofTypeChange = Convert.ToInt32(checkdirectorDetails.NumberofChanges);

                    getDirectorDetails.MessageforDirector = a;

                }
                return getDirectorDetails;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return getDirectorDetails;
            }
        }

        public OnEditingDirector getOnEditChanges(long iD)
        {
            try
            {
                var getoneditChage = (from row in entities.BM_DirectorTypeOfChanges
                                      where row.DirectorId == iD && row.IsApproved == true
                                      select new OnEditingDirector
                                      {
                                          Id = row.Id,
                                          MobileNumber = row.Mobile,
                                          EmailId = row.EmailID,
                                          DirectorID = row.DirectorId,
                                      }
                                    ).OrderByDescending(x => x.Id).First();

                if (getoneditChage != null)
                {
                    var userIdPerformerDIR6 = objIDirectorCompliances.GetPerformer(getoneditChage.DirectorID, "TypeOfChange-DIR-6");
                    if (userIdPerformerDIR6 > 0)
                    {
                        getoneditChage.userIdPerformerDIR6 = (int)userIdPerformerDIR6;
                    }

                    if (getoneditChage.MobileNumber || getoneditChage.EmailId)
                    {
                        var userIdPerformerDIR3 = objIDirectorCompliances.GetPerformer(getoneditChage.DirectorID, "TypeOfChange-DIR-3");
                        if (userIdPerformerDIR3 > 0)
                        {
                            getoneditChage.userIdPerformerDIR3 = (int)userIdPerformerDIR3;
                        }
                    }
                }
                return getoneditChage;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Director_MasterVM updateDirectorProfile(Director_MasterVM obj)
        {
            obj.Response = new Response();
            try
            {
                var getdetailsfromDirector = (from row in entities.BM_DirectorMaster where row.UserID == obj.UserID select row).FirstOrDefault();

                if (getdetailsfromDirector != null)
                {
                    var getdetailsofEditing = (from x in entities.BM_DirectorTypeOfChanges
                                               join y in entities.BM_DirectorTypeChangesMapping on x.Id equals y.TypeChangeMaapingID
                                               where x.DirectorId == getdetailsfromDirector.Id && x.IsActive == true && y.IsActive == true
                                               select new
                                               {
                                                   x,
                                                   y
                                               }
                                             ).FirstOrDefault();
                    if (getdetailsofEditing != null)
                    {
                        if (obj.Approve_disapprove == "Approve")
                        {
                            getdetailsofEditing.x.IsApproved = true;
                            //getdetailsofEditing.x.IsPending = false;
                            //getdetailsofEditing.x.IsActive = false;
                            entities.SaveChanges();
                            if (getdetailsfromDirector != null)
                            {
                                if (getdetailsofEditing.x.Name_of_Director)
                                {
                                    getdetailsfromDirector.FirstName = getdetailsofEditing.y.FirstName;
                                    getdetailsfromDirector.LastName = getdetailsofEditing.y.LastName;
                                    getdetailsfromDirector.MiddleName = getdetailsofEditing.y.MiddleName;

                                }
                                if (getdetailsofEditing.x.FatherName)
                                {
                                    getdetailsfromDirector.Father = getdetailsofEditing.y.FatherFirstName;
                                    getdetailsfromDirector.FatherMiddleName = getdetailsofEditing.y.FatherMiddleName;
                                    getdetailsfromDirector.FatherLastName = getdetailsofEditing.y.FatherMiddleName;
                                }
                                if (getdetailsofEditing.x.EmailID)
                                {
                                    getdetailsfromDirector.EmailId_Official = getdetailsofEditing.y.EmailId_Official;
                                    getdetailsfromDirector.EmailId_Personal = getdetailsofEditing.y.EmailId_Personal;

                                }
                                if (getdetailsofEditing.x.Mobile)
                                {
                                    getdetailsfromDirector.MobileNo = getdetailsofEditing.y.MobileNo;

                                }
                                if (getdetailsofEditing.x.DateofBirth)
                                {
                                    getdetailsfromDirector.DOB = Convert.ToDateTime(getdetailsofEditing.y.DOB);

                                }
                                if (getdetailsofEditing.x.PAN)
                                {
                                    getdetailsfromDirector.PAN = getdetailsofEditing.y.PAN;

                                }
                                if (getdetailsofEditing.x.AadharNumber)
                                {
                                    getdetailsfromDirector.Aadhaar = getdetailsofEditing.y.Aadhaar;
                                }
                                if (getdetailsofEditing.x.Parmanent_Address)
                                {
                                    getdetailsfromDirector.Permenant_Address_Line1 = getdetailsofEditing.y.Permenant_Address_Line1;
                                    getdetailsfromDirector.Permenant_Address_Line2 = getdetailsofEditing.y.Permenant_Address_Line2;
                                    getdetailsfromDirector.Permenant_StateId = getdetailsofEditing.y.Permenant_StateId;
                                    getdetailsfromDirector.Permenant_CityId = (int)getdetailsofEditing.y.Permenant_CityId;
                                    getdetailsfromDirector.Permenant_PINCode = getdetailsofEditing.y.Permenant_PINCode;

                                }
                                if (getdetailsofEditing.x.Present_Address)
                                {
                                    getdetailsfromDirector.Present_Address_Line1 = getdetailsofEditing.y.Present_Address_Line1;
                                    getdetailsfromDirector.Present_Address_Line2 = getdetailsofEditing.y.Present_Address_Line2;
                                    getdetailsfromDirector.Present_StateId = (int)getdetailsofEditing.y.Present_StateId;
                                    getdetailsfromDirector.Present_CityId = (int)getdetailsofEditing.y.Present_CityId;
                                    getdetailsfromDirector.Present_PINCode = getdetailsofEditing.y.Present_PINCode;

                                }
                                if (getdetailsofEditing.x.Passport)
                                {
                                    getdetailsfromDirector.PassportNo = getdetailsofEditing.y.PassportNo;
                                }


                                if (getdetailsofEditing.x.PhotoGraph_of_Director)
                                {
                                    getdetailsfromDirector.Photo_Doc = getdetailsofEditing.y.Photo_Doc;
                                }
                                //getdetailsfromDirector.isChangesDone = false;
                                //getdetailsfromDirector.NumberofChanges = 0;
                                entities.SaveChanges();
                                //var checkTypeofChanges = (from row in entities.BM_DirectorTypeChangesMapping where row.DirectorId == getdetailsfromDirector.Id && row.IsActive == true select row).FirstOrDefault();
                                //if (checkTypeofChanges != null)
                                //{
                                //    checkTypeofChanges.IsActive = false;
                                //    entities.SaveChanges();
                                //}

                            }
                            //obj.NumberofTypeChange = 0;
                            obj.Approve_disapprove = "Approved";
                            obj.Response.Success = true;
                            obj.Response.Message = "Approved Successfully";
                        }
                        else if (obj.Approve_disapprove == "Reject")
                        {
                            getdetailsofEditing.x.IsDisapproved = true;
                            getdetailsofEditing.x.IsPending = false;
                            getdetailsofEditing.x.IsActive = false;
                            entities.SaveChanges();
                            obj.Response.Success = true;
                            obj.Response.Message = "Reject Successfully";
                            obj.NumberofTypeChange = 0;
                            obj.Approve_disapprove = "Reject";
                        }
                    }
                    else if (obj.Approve_disapprove == "Save")
                    {

                        if (getdetailsfromDirector != null)
                        {
                            var checkTypeofchanges = (from typechange in entities.BM_DirectorTypeOfChanges where typechange.UserID == getdetailsfromDirector.UserID && typechange.DirectorId == getdetailsfromDirector.Id && typechange.IsActive == true select typechange).FirstOrDefault();
                            if (checkTypeofchanges != null)
                            {
                                if (checkTypeofchanges.Name_of_Director)
                                {
                                    getdetailsfromDirector.Salutation = obj.Salutation;
                                    getdetailsfromDirector.FirstName = obj.FirstName;
                                    getdetailsfromDirector.LastName = obj.LastName;
                                    getdetailsfromDirector.MiddleName = obj.MiddleName;
                                }
                                if (checkTypeofchanges.FatherName)
                                {
                                    getdetailsfromDirector.FSalutations = obj.FSalutations;
                                    getdetailsfromDirector.Father = obj.FatherFirstName;
                                    getdetailsfromDirector.FatherMiddleName = obj.FatherMiddleName;
                                    getdetailsfromDirector.FatherLastName = obj.FatherLastName;
                                }

                                if (checkTypeofchanges.Nationality)
                                {
                                    getdetailsfromDirector.Nationality = (int)obj.Nationality;
                                }
                                if (checkTypeofchanges.DateofBirth)
                                {
                                   
                                    getdetailsfromDirector.DOB = DateTimeExtensions.GetDate(obj.DOB);
                                }
                                if (checkTypeofchanges.Gender)
                                {
                                    getdetailsfromDirector.Gender = (int)obj.Gender;
                                }
                                if (checkTypeofchanges.PAN)
                                {
                                    getdetailsfromDirector.PAN = obj.PAN;
                                }
                                if (checkTypeofchanges.Passport)
                                {
                                    getdetailsfromDirector.PassportNo = obj.PassportNo;
                                }
                                if (checkTypeofchanges.EmailID)
                                {
                                    getdetailsfromDirector.EmailId_Official = obj.EmailId_Official;
                                    getdetailsfromDirector.EmailId_Personal = obj.EmailId;
                                }
                                if (checkTypeofchanges.Mobile)
                                {
                                    getdetailsfromDirector.MobileNo = obj.MobileNo;
                                }
                                if (checkTypeofchanges.Parmanent_Address)
                                {
                                    getdetailsfromDirector.Permenant_Address_Line1 = obj.Permenant_Address_Line1;
                                    getdetailsfromDirector.Permenant_Address_Line2 = obj.Permenant_Address_Line2;
                                    if (obj.Permenant_StateId > 0)
                                    {
                                        getdetailsfromDirector.Permenant_StateId = (int)obj.Permenant_StateId;
                                    }
                                    else
                                    {
                                        getdetailsfromDirector.Permenant_StateId = -1;
                                    }
                                    if (obj.Permenant_CityId>0)
                                    {
                                        getdetailsfromDirector.Permenant_CityId = (int)obj.Permenant_CityId;
                                    }
                                    else
                                    {
                                        getdetailsfromDirector.Permenant_CityId = -1;
                                    }
                                    getdetailsfromDirector.Permenant_CountryId = obj.Permenant_CountryID;
                                    getdetailsfromDirector.Permenant_PINCode = obj.Permenant_PINCode;
                                }
                                if (checkTypeofchanges.Present_Address)
                                {
                                    getdetailsfromDirector.Present_Address_Line1 = obj.Present_Address_Line1;
                                    getdetailsfromDirector.Present_Address_Line2 = obj.Present_Address_Line2;
                                    getdetailsfromDirector.Present_StateId = obj.Present_StateId > 0 ? (int)obj.Present_StateId : 0;
                                    getdetailsfromDirector.Present_CityId = obj.Permenant_CityId > 0 ? (int)obj.Present_CityId : 0;
                                    getdetailsfromDirector.Present_CountryId = obj.Permenant_CountryID;
                                    getdetailsfromDirector.Permenant_PINCode = obj.Permenant_PINCode;
                                }
                                if (checkTypeofchanges.Rasidential_Status)
                                {
                                    getdetailsfromDirector.ResidentInIndia = obj.ResidentInIndia;
                                }
                                if (checkTypeofchanges.AadharNumber)
                                {
                                    getdetailsfromDirector.Aadhaar = obj.Adhaar;
                                }
                                if (checkTypeofchanges.PhotoGraph_of_Director)
                                {
                                    getdetailsfromDirector.Photo_Doc = obj.Photo_Doc_Name;
                                }
                                entities.SaveChanges();

                                //checkTypeofchanges.IsPending = false;
                                checkTypeofchanges.IsApproved = true;
                               // checkTypeofchanges.IsActive = false;
                                entities.SaveChanges();

                                obj.Approve_disapprove = "Save";
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                obj.Response.Error = true;
                obj.Response.Message = "server error occurred";
            }
            return obj;
        }

        public OnEditingDirector SaveDirectorTypeOfChange(OnEditingDirector obj)
        {
            Message message = new Message();
            try
            {
                var dir6 = objIForms_Service.GenerateFormsforDirector(obj.DirectorID);
                int? fileId_DIR6 = null;
                var fileData_Dir6 = new FileDataVM();
                if (dir6 != null)
                {
                    string filePath = string.Empty;

                    if (obj.CustomerId != null && obj.CustomerId != 0)
                        filePath = "~/Areas/BM_Management/Documents/" + obj.CustomerId + "/Director/" + obj.DirectorID;
                    else
                        filePath = "~/Areas/BM_Management/Documents/Director/" + obj.DirectorID;

                    if (!string.IsNullOrEmpty(filePath))
                    {
                        fileData_Dir6.FileData = dir6.FormData;
                        fileData_Dir6.FileName = "DIR-6-" + DateTime.Now.ToString("ddMMyyyy") + ".pdf";
                        fileData_Dir6.Version = "1.0";
                        fileData_Dir6.FilePath = filePath;
                        fileData_Dir6 = objIFileData_Service.Save(fileData_Dir6, 1);

                        if (fileData_Dir6.FileID != 0)
                            fileId_DIR6 = Convert.ToInt32(fileData_Dir6.FileID);
                    }
                }

                message = objIDirectorCompliances.GenerateDirectorScheduleOn(obj.CustomerId, obj.DirectorID, "TypeOfChange-DIR-6", DateTime.Now, 1, "Admin", fileId_DIR6, obj.userIdPerformerDIR6, DateTime.Now);

                int? fileId_DIR3 = null;

                if (dir6 != null)
                {
                    if (obj.MobileNumber || obj.EmailId)
                    {
                        var dir3 = objIForms_Service.GenerateFormsforDirector(obj.DirectorID);

                        var fileData_Dir3 = new FileDataVM();

                        string filePath = string.Empty;

                        if (obj.CustomerId != null && obj.CustomerId != 0)
                            filePath = "~/Areas/BM_Management/Documents/" + obj.CustomerId + "/Director/" + obj.DirectorID;
                        else
                            filePath = "~/Areas/BM_Management/Documents/Director/" + obj.DirectorID;

                        if (!string.IsNullOrEmpty(filePath))
                        {
                            fileData_Dir3.FileData = dir6.FormData;
                            fileData_Dir3.FileName = "DIR-3-" + DateTime.Now.ToString("ddMMyyyy") + ".pdf";
                            fileData_Dir3.Version = "1.0";
                            fileData_Dir3.FilePath = filePath;
                            fileData_Dir3 = objIFileData_Service.Save(fileData_Dir3, 1);

                            if (fileData_Dir3.FileID != 0)
                                fileId_DIR3 = Convert.ToInt32(fileData_Dir3.FileID);
                        }

                        //objIDirectorCompliances.GenerateDirectorScheduleOn(obj.DirectorID, "TypeOfChange-DIR-3", DateTime.Now, 1, "Admin", fileId_DIR3, obj.userIdPerformerDIR3);
                        message = objIDirectorCompliances.GenerateDirectorScheduleOn(obj.CustomerId, obj.DirectorID, "TypeOfChange-DIR-3", DateTime.Now, 1, "Admin", fileId_DIR3, obj.userIdPerformerDIR3, DateTime.Now);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            if (message.Success)
            {
                obj.Message = SecretarialConst.Messages.saveSuccess;
                //obj.Message = "Saved successfully;";
                obj.Success = true;

            }
            else
            {
                obj.Message = SecretarialConst.Messages.serverError;
                //obj.Message = "Something wends wrongs";
                obj.Error = true;
            }

            return obj;
        }

        public bool CloseProfileEditing(OnEditingDirector obj)
        {
            try
            {
                var getdetailsfromDirector = (from row in entities.BM_DirectorMaster where row.Id == obj.DirectorID select row).FirstOrDefault();

                if (getdetailsfromDirector != null)
                {
                    var getdetailsofEditing = (from x in entities.BM_DirectorTypeOfChanges
                                               join y in entities.BM_DirectorTypeChangesMapping on x.Id equals y.TypeChangeMaapingID
                                               where x.DirectorId == getdetailsfromDirector.Id && x.IsActive == true && y.IsActive == true
                                               && x.IsApproved == true
                                               select new
                                               {
                                                   x,
                                                   y
                                               }
                                             ).FirstOrDefault();
                    if (getdetailsofEditing != null)
                    {
                        getdetailsofEditing.x.IsPending = false;
                        getdetailsofEditing.x.IsActive = false;
                        entities.SaveChanges();
                        if (getdetailsfromDirector != null)
                        {
                            getdetailsfromDirector.isChangesDone = false;
                            getdetailsfromDirector.NumberofChanges = 0;
                            entities.SaveChanges();
                            var checkTypeofChanges = (from row in entities.BM_DirectorTypeChangesMapping where row.DirectorId == getdetailsfromDirector.Id && row.IsActive == true select row).FirstOrDefault();
                            if (checkTypeofChanges != null)
                            {
                                checkTypeofChanges.IsActive = false;
                                entities.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        var gettypeofchange = (from x in entities.BM_DirectorTypeOfChanges
                                                   
                                                   where x.DirectorId == getdetailsfromDirector.Id && x.IsActive == true
                                                   && x.IsApproved == true
                                                   select x
                                                  
                                            ).FirstOrDefault();
                        if(gettypeofchange!=null)
                        {
                            gettypeofchange.IsPending = false;
                            gettypeofchange.IsActive = false;
                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return true;
        }
    }
}