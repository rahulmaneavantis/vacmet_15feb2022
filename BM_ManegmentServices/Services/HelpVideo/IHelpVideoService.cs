﻿using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.HelpVideo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.HelpVideo
{
    public interface IHelpVideoService
    {
        List<HelpVideoVM> GetHelpListImplementation();
        List<HelpVideoVM> GetHelpList();
        HelpVideoVM CreateHelpLink(HelpVideoVM obj, int userId);
        Message UpdateHelpLink(int id, string directoryPath, string fileName, string fileNameNew, long fileSize,int userId);
        Message UpdateSerialNumbers(List<HelpVideoSRVM> lstItems, int userId);
        Message DeleteHelpLink(int id, int userId);
        HelpVideoVM GetHelpLink(int id);
        List<HelpVideoVM> GetHelpParentList();
    }
}
