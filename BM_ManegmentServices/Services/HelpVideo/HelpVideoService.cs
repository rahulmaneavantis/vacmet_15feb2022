﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.HelpVideo;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.HelpVideo
{
    public class HelpVideoService : IHelpVideoService
    {
        public List<HelpVideoVM> GetHelpListImplementation()
        {
            var result = new List<HelpVideoVM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_HelpVideoMaster
                              where row.IsDeleted == false
                              orderby row.SrNo
                              select new HelpVideoVM
                              {
                                  HelpVideoId = row.Id,
                                  IsLinkActive = row.IsActive,
                                  HelpName = row.Name_,
                                  Description_ = row.Description_,
                                  ParentId = row.ParentId,
                                  SrNo = row.SrNo == null ? (100 + row.Id) : row.SrNo,
                                  FileType = row.FileType,
                                  FileURL = row.FileURL
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }
        public List<HelpVideoVM> GetHelpList()
        {
            var result = new List<HelpVideoVM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_HelpVideoMaster
                              where row.IsActive == true && row.IsDeleted == false
                              orderby row.SrNo
                              select new HelpVideoVM
                              {
                                  HelpVideoId = row.Id,
                                  IsLinkActive = row.IsActive,
                                  HelpName = row.Name_,
                                  Description_ = row.Description_,
                                  ParentId = row.ParentId,
                                  SrNo = row.SrNo == null ? (100 + row.Id) : row.SrNo,
                                  FileType = row.FileType,
                                  FileURL = row.FileURL
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public HelpVideoVM CreateHelpLink(HelpVideoVM obj, int userId)
        {
            try
            {
                if (obj.HelpVideoId > 0)
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        //if(obj.ParentId == null)
                        //{
                        //    var result = from row in entities.BM_HelpVideoMaster
                        //                 where row.ParentId == obj.HelpVideoId && row.IsActive == true
                        //}

                        var _obj = (from row in entities.BM_HelpVideoMaster
                                    where row.Id == obj.HelpVideoId
                                    select row).FirstOrDefault();

                        if (_obj != null)
                        {
                            _obj.Name_ = obj.HelpName;
                            _obj.Description_ = obj.Description_;
                            _obj.IsActive = obj.IsLinkActive;
                            _obj.UpdatedOn = DateTime.Now;
                            _obj.UpdatedBy = userId;
                            entities.SaveChanges();

                            obj.Success = true;
                            obj.Message = SecretarialConst.Messages.updateSuccess;
                        }
                        else
                        {
                            obj.Error = true;
                            obj.Message = SecretarialConst.Messages.serverError;
                        }
                    }
                }
                else
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        var maxSrNo = entities.BM_HelpVideoMaster.Where(k => k.ParentId == obj.ParentId && k.IsDeleted == false).Max(k => k.SrNo);
                        if(maxSrNo == null)
                        {
                            maxSrNo = 0;
                        }
                        maxSrNo++;

                        var _obj = new BM_HelpVideoMaster();
                        _obj.Id = obj.HelpVideoId;
                        _obj.SrNo = maxSrNo;
                        _obj.Name_ = obj.HelpName;
                        _obj.Description_ = obj.Description_;
                        _obj.ParentId = obj.ParentId;
                        _obj.IsActive = false;
                        _obj.OnStorage = false;
                        _obj.IsDeleted = false;
                        _obj.CreatedOn = DateTime.Now;
                        _obj.CreatedBy = userId;

                        entities.BM_HelpVideoMaster.Add(_obj);
                        entities.SaveChanges();

                        obj.HelpVideoId = _obj.Id;
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                }

                #region
                //if(obj.Success)
                //{
                //    if(obj.Files != null)
                //    {
                //        string fileKey = Convert.ToString(Guid.NewGuid());
                //        string fileExtension = Path.GetExtension(obj.Files.FileName);
                //        string fileName = obj.Files.FileName;
                //        string fileNameNew = fileKey + fileExtension;

                //        Stream fileStream = obj.Files.InputStream;
                //        using (fileStream)
                //        {
                //            var pathDetails = "DIY";
                //            if(obj.HelpMode == "DFM")
                //            {
                //                pathDetails = "DFM";
                //            }
                //            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                //            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                //            var directoryPath = "testsecretarial/" + pathDetails +"/" + obj.HelpVideoId;
                //            CloudBlobContainer container = blobClient.GetContainerReference(directoryPath);
                //            container.CreateIfNotExistsAsync();

                //            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileNameNew);
                //            blockBlob.UploadFromStream(fileStream);

                //            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                //            {
                //                var _objFileDetails = (from row in entities.BM_HelpVideoMaster
                //                                       where row.Id == obj.HelpVideoId
                //                                       select row).FirstOrDefault();

                //                if (_objFileDetails != null)
                //                {
                //                    _objFileDetails.FileURL = directoryPath +"/"+ fileNameNew;
                //                    _objFileDetails.FileSize = obj.Files.ContentLength;
                //                    _objFileDetails.FileName_ = obj.Files.FileName;
                //                    _objFileDetails.OnStorage = true;
                //                    entities.SaveChanges();
                //                }
                //            }
                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public Message UpdateHelpLink(int id, string directoryPath, string fileName, string fileNameNew, long fileSize, int userId)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {

                    var _objFileDetails = (from row in entities.BM_HelpVideoMaster
                                            where row.Id == id
                                            select row).FirstOrDefault();

                    if (_objFileDetails != null)
                    {
                        _objFileDetails.FileURL = directoryPath + "/" + fileNameNew;
                        _objFileDetails.FileSize = fileSize;
                        _objFileDetails.FileName_ = fileName;
                        _objFileDetails.OnStorage = true;
                        entities.SaveChanges();

                        result.Success = true;
                        result.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        result.Error = true;
                        result.Message = SecretarialConst.Messages.serverError;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public Message UpdateSerialNumbers(List<HelpVideoSRVM> lstItems, int userId)
        {
            var result = new Message();
            try
            {
                if(lstItems != null)
                {
                    using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                    {
                        foreach (var item in lstItems)
                        {
                            var _obj = (from row in entities.BM_HelpVideoMaster
                                        where row.Id == item.HelpVideoId
                                        select row).FirstOrDefault();
                            if (_obj != null)
                            {
                                _obj.SrNo = item.SrNoNew;
                                _obj.UpdatedBy = userId;
                                _obj.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();
                            }
                        }
                    }
                    result.Success = true;
                    result.Message = SecretarialConst.Messages.saveSuccess;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public Message DeleteHelpLink(int id, int userId)
        {
            var result = new Message();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {

                    var _obj = (from row in entities.BM_HelpVideoMaster
                                where row.Id == id
                                select row).FirstOrDefault();

                    if (_obj != null)
                    {
                        _obj.IsActive = false;
                        _obj.IsDeleted = true;
                        _obj.UpdatedOn = DateTime.Now;
                        _obj.UpdatedBy = userId;
                        entities.SaveChanges();

                        result.Success = true;
                        result.Message = SecretarialConst.Messages.deleteSuccess;
                    }
                    else
                    {
                        result.Error = true;
                        result.Message = SecretarialConst.Messages.serverError;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public HelpVideoVM GetHelpLink(int id)
        {
            var result = new HelpVideoVM();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_HelpVideoMaster
                              where row.Id == id && row.IsDeleted == false
                              select new HelpVideoVM
                              {
                                  HelpVideoId = row.Id,
                                  HelpName = row.Name_,
                                  Description_ = row.Description_,
                                  ParentId = row.ParentId,
                                  SrNo = row.SrNo,
                                  FileType = row.FileType,
                                  FileURL = row.FileURL,
                                  IsLinkActive = row.IsActive
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public List<HelpVideoVM> GetHelpParentList()
        {
            var result = new List<HelpVideoVM>();
            try
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    result = (from row in entities.BM_HelpVideoMaster
                              where row.ParentId == null && row.IsActive == true
                              orderby row.Name_
                              select new HelpVideoVM
                              {
                                  HelpVideoId = row.Id,
                                  HelpName = row.Name_,
                                  Description_ = row.Description_,
                                  ParentId = row.ParentId,
                                  SrNo = row.SrNo,
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }
    }
}