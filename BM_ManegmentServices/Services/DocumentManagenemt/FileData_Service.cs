﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public class FileData_Service : IFileData_Service
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public int GetFileVersion(long scheduleOnID, string fileName)
        {
            int version = 0;
            try
            {
                var result = (from row in entities.FileDataMappings
                              join fileData in entities.FileDatas on row.FileID equals fileData.ID
                              orderby row.ID descending
                              where row.ScheduledOnID == scheduleOnID 
                              && fileData.Name == fileName
                              select fileData.Version).FirstOrDefault();

                int.TryParse(result, out version);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return version;
        }

        //Generate Key for FileData if Key is Empty
        public FileDataVM Save(FileDataVM obj, int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(obj.FileKey))
                {
                    obj.FileKey = Convert.ToString(Guid.NewGuid());
                }

                string path = System.Web.Hosting.HostingEnvironment.MapPath(obj.FilePath);
                string fileExtension = Path.GetExtension(obj.FileName);
                string fileName = obj.FileKey + fileExtension;
                string filePath = Path.Combine(path, fileName);

                if (!Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);

                SaveDocFile(filePath, obj.FileData);

                if (File.Exists(filePath))
                {
                    FileData _obj = new FileData()
                    {
                        FilePath = obj.FilePath,
                        FileKey = obj.FileKey,
                        Name = obj.FileName,
                        //FileSize = Convert.ToString(obj.FileData.LongLength),
                        Version = obj.Version,
                        VersionDate = DateTime.Now,
                        IsDeleted = false,
                        EnType = "A",
                        //UploadedBy = userId,
                        //UploadedOn = DateTime.Now
                    };

                    if (obj.FileData.Length != null)
                        _obj.FileSize = obj.FileData.Length;

                    entities.FileDatas.Add(_obj);
                    entities.SaveChanges();
                    obj.FileID = _obj.ID;
                }

                obj.Success = true;
                obj.Message = "Saved successfully";
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Message CreateFileDataMapping(long transactionID, long scheduleOnID, long fileID, Int16 fileType)
        {
            var obj = new Message();
            try
            {
                var _obj = new FileDataMapping()
                {
                    TransactionID = transactionID,
                    ScheduledOnID = scheduleOnID,
                    FileID = (int)fileID,
                    FileType = fileType
                };
                entities.FileDataMappings.Add(_obj);
                entities.SaveChanges();
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public FileDataVM GetFile(long fileID, int userId)
        {
            var obj = new FileDataVM();
            try
            {
                var result = (from row in entities.FileDatas
                              where row.ID == fileID
                              select new FileDataVM
                              {
                                  FileName = row.Name,
                                  FileKey = row.FileKey,
                                  FilePath = row.FilePath,
                                  Version = row.Version
                              }).FirstOrDefault();

                if (result != null)
                {
                    string path = System.Web.Hosting.HostingEnvironment.MapPath(result.FilePath);
                    string fileExtension = Path.GetExtension(result.FileName);
                    string fileName = result.FileKey + fileExtension;

                    string filePath = Path.Combine(path, fileName);

                    if (File.Exists(filePath))
                    {
                        obj.FileKey = result.FileKey;
                        obj.FileName = result.FileName;
                        //obj.FileData = ReadDocFiles(filePath);
                        obj.FileData = CryptographyHandler.AESDecrypt(ReadDocFiles(filePath));
                        obj.Success = true;
                    }
                    else
                    {
                        obj.Error = true;
                    }
                }
                else
                {
                    obj.Error = true;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        void SaveDocFile(string fileName, Byte[] data)
        {
            using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(fileName)))
            {
                // Writer raw data                
                Writer.Write(CryptographyHandler.AESEncrypt(data));
                //Writer.Write(data);
                Writer.Flush();
                Writer.Close();
            }
        }
        void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    SaveDocFile(file.Key, file.Value);
                }
            }
        }
        public byte[] ReadDocFiles(string filePath)
        {
            using (FileStream fs = File.OpenRead(filePath))
            {
                int length = (int)fs.Length;
                byte[] buffer;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    buffer = br.ReadBytes(length);
                }
                return buffer;
            }
        }

        public static List<BM_SP_ComplianceDocuments_Result> GetComplianceDocumentsByScheduleOnID(int scheduleOnID)
        {
            try
            {
                using (Compliance_SecretarialEntities dbContext = new Compliance_SecretarialEntities())
                {
                    var lstComplianceDocs = dbContext.BM_SP_ComplianceDocuments().Where(row => row.ScheduleOnID == scheduleOnID).ToList();

                    return lstComplianceDocs;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public FileDataVM SaveSecretarialDocs(FileDataVM obj, int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(obj.FileKey))
                {
                    obj.FileKey = Convert.ToString(Guid.NewGuid());
                }

                string path = System.Web.Hosting.HostingEnvironment.MapPath(obj.FilePath);
                string fileExtension = Path.GetExtension(obj.FileName);
                string fileName = obj.FileKey + fileExtension;
                string filePath = Path.Combine(path, fileName);

                if (!Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);

                SaveDocFile(filePath, obj.FileData);

                if (File.Exists(filePath))
                {
                    BM_FileData _obj = new BM_FileData()
                    {
                        FilePath = obj.FilePath,
                        FileKey = obj.FileKey,
                        FileName = obj.FileName,
                        FileSize = Convert.ToString(obj.FileData.LongLength),
                        Version = obj.Version,
                        VersionDate = DateTime.Now,
                        IsDeleted = false,
                        EnType = "A",
                        MeetingId = obj.MeetingId,
                        DocType = obj.MeetingDoc,
                        UploadedBy = userId,
                        UploadedOn = DateTime.Now
                    };

                    entities.BM_FileData.Add(_obj);
                    entities.SaveChanges();
                    obj.FileID = _obj.Id;
                }
                obj.Success = true;
                obj.Message = "Saved successfully";
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public List<FileDataVM> GetAllSecretarialDocs(long meetingId, string docs)
        {
            var obj = new List<FileDataVM>();
            try
            {
                obj = (from row in entities.BM_FileData
                        where row.MeetingId == meetingId && row.DocType == docs && row.IsDeleted == false
                        select new FileDataVM
                        {
                            FileID = row.Id,
                            FileName = row.FileName,
                            Version = row.Version
                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public FileDataVM GetSecretarialDocs(long fileID)
        {
            var obj = new FileDataVM();
            try
            {
                var result = (from row in entities.BM_FileData
                              where row.Id == fileID
                              select new FileDataVM
                              {
                                  FileName = row.FileName,
                                  FileKey = row.FileKey,
                                  FilePath = row.FilePath,
                                  Version = row.Version
                              }).FirstOrDefault();

                if (result != null)
                {
                    string path = System.Web.Hosting.HostingEnvironment.MapPath(result.FilePath);
                    string fileExtension = Path.GetExtension(result.FileName);
                    string fileName = result.FileKey + fileExtension;

                    string filePath = Path.Combine(path, fileName);

                    if (File.Exists(filePath))
                    {
                        obj.FileKey = result.FileKey;
                        obj.FileName = result.FileName;
                        obj.FileData = CryptographyHandler.AESDecrypt(ReadDocFiles(filePath));
                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = SecretarialConst.Messages.serverError;
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = SecretarialConst.Messages.serverError;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public Message DeleteSecretarialDocs(long fileId, long meetingId, string docs, int userId)
        {
            var obj = new Message();
            try
            {
                var result = (from row in entities.BM_FileData
                              where row.Id == fileId && row.MeetingId == meetingId && row.DocType == docs && row.IsDeleted == false
                              select row).FirstOrDefault();

                if (result != null)
                {
                    result.IsDeleted = true;
                    result.UploadedBy = userId;
                    result.UploadedOn = DateTime.Now;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.deleteSuccess;
                }
                else
                {
                    obj.Error = true;
                    obj.Message = SecretarialConst.Messages.serverError;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Message DeleteSecretarialDocs(long fileId, long meetingAgendaMappingId, long meetingId, int userId)
        {
            var obj = new Message();
            try
            {
                var result = (from row in entities.BM_FileData
                              where row.Id == fileId && row.AgendaMappingId == meetingAgendaMappingId && row.IsDeleted == false
                              select row).FirstOrDefault();

                if (result != null)
                {
                    result.IsDeleted = true;
                    result.UploadedBy = userId;
                    result.UploadedOn = DateTime.Now;
                    entities.SaveChanges();
                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.deleteSuccess;

                    var flag = (from row in entities.BM_SP_MeetingAgendaSetGenerationFlag(meetingId)
                                  select row);
                }
                else
                {
                    obj.Error = true;
                    obj.Message = SecretarialConst.Messages.serverError;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public string GetSecretarialFile(long fileID, int userId, int customerId)
        {
            var actualFileName = "";
            var obj = new FileDataVM();
            try
            {
                var result = (from row in entities.BM_FileData
                              where row.Id == fileID
                              select new FileDataVM
                              {
                                  FileName = row.FileName,
                                  FileKey = row.FileKey,
                                  FilePath = row.FilePath,
                                  Version = row.Version
                              }).FirstOrDefault();

                if (result != null)
                {
                    string path = System.Web.Hosting.HostingEnvironment.MapPath(result.FilePath);
                    string fileExtension = Path.GetExtension(result.FileName);
                    string fileName = result.FileKey + fileExtension;

                    string filePath = Path.Combine(path, fileName);

                    if (File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                        string DateFolder = Folder + "/" + File;
                        string extension = System.IO.Path.GetExtension(filePath);
                        Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                        }

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                        string User = userId + "" + customerId + "" + FileDate;
                        string FileName = DateFolder + "/" + User + "" + extension;
                        FileStream fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        bw.Write(CryptographyHandler.AESDecrypt(ReadDocFiles(filePath)));
                        bw.Close();

                        actualFileName = FileName.Substring(2, FileName.Length - 2);
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = SecretarialConst.Messages.serverError;
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = SecretarialConst.Messages.serverError;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return actualFileName;
        }

        public byte[] GetEULAFile(string filePath)
        {
            return CryptographyHandler.AESDecrypt(ReadDocFiles(filePath));
        }

        public VM.EULA.EULA_DetailsVM SaveEULAFile(VM.EULA.EULA_DetailsVM obj)
        {
            try
            {
                string fileName = string.Empty;
                string directoryPath = string.Empty;
                if (obj.files != null)
                {
                    foreach (var file in obj.files)
                    {
                        if (file.ContentLength > 0)
                        {
                            var virtualPath = "~/Areas/Document/EULA/" + obj.CustomerID + "/" + obj.UserID + "/";
                            directoryPath = System.Web.Hosting.HostingEnvironment.MapPath(virtualPath);

                            if (!Directory.Exists(directoryPath))
                                Directory.CreateDirectory(directoryPath);

                            Guid fileKey1 = Guid.NewGuid();
                            string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(file.FileName));
                            Stream fs = file.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            SaveDocFile(finalPath1, bytes);
                            var objEULADetails = (from row in entities.EULADetails
                                                  where row.ID == obj.EULA_ID && row.IsActive == true
                                                  select row).FirstOrDefault();
                            if(objEULADetails != null)
                            {
                                objEULADetails.FilePath = virtualPath + fileKey1 + Path.GetExtension(file.FileName);
                                objEULADetails.StatusId = 1;
                                objEULADetails.UploadedOn = DateTime.Now;
                                entities.SaveChanges();

                                obj.StatusId = 1;
                            }
                            obj.Success = true;
                            obj.Message = SecretarialConst.Messages.saveSuccess;
                        }
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Please upload document";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        #region Get Historical file data list by ID
        public List<FileDataVM> FileDataListByHistoricalID(long historicalId, string doc)
        {
            var result = new List<FileDataVM>();
            try
            {
                result = (from row in entities.BM_HistoricalData
                          join file in entities.BM_FileData on row.ID equals (int) file.HistoricalDtataID
                          where row.ID == historicalId && file.DocType == doc && file.IsDeleted == false
                          select new FileDataVM
                          {
                              FileID = file.Id,
                              FileName = file.FileName,
                              Version = file.Version
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion
    }
}