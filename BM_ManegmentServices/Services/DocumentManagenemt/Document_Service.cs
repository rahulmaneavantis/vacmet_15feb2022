﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Services.Meetings;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.DocumentManagenemt;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.Pdf.Parsing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.DocumentManagenemt
{
    public class Document_Service : IDocument_Service
    {
        IAgendaMinutesReviewService objIAgendaMinutesReviewService;
        IDocumentSetting_Service objIDocumentSetting_Service;
        IFileData_Service objIFileData_Service;
        public Document_Service(IAgendaMinutesReviewService objAgendaMinutesReviewService, IDocumentSetting_Service objDocumentSetting_Service, IFileData_Service objFileData_Service)
        {
            objIAgendaMinutesReviewService = objAgendaMinutesReviewService;
            objIDocumentSetting_Service = objDocumentSetting_Service;
            objIFileData_Service = objFileData_Service;
        }
        public byte[] GenerateAgendaDocument(long meetingId, long? taskAssignmentId, int userId, int customerId, bool generateNew)
        {
            var result = GenerateAgendaDocument(meetingId, taskAssignmentId, userId, customerId, generateNew, false);
            return result.FileContent;
        }

        public string GenerateAgendaDocument1(long meetingId, long? taskAssignmentId, int userId, int customerId, bool generateNew)
        {
            var result = GenerateAgendaDocument(meetingId, taskAssignmentId, userId, customerId, generateNew, true);
            return result.FileName;
        }

        public DocumentVM GenerateAgendaDocument(long meetingId, long? taskAssignmentId, int userId, int customerId, bool forceGenerateNewDocument, bool generatePDF)
        {
            var result = new DocumentVM();
            #region Get File
            #region Check Document already generated or not on Review
            string agendaReviewFileName = null;

            if (taskAssignmentId > 0 && forceGenerateNewDocument == false)
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    agendaReviewFileName = (from row in entities.BM_TaskAssignment
                                            where row.ID == taskAssignmentId
                                            select row.AgendaFilePath).FirstOrDefault();
                }
            }
            else if (meetingId > 0 && taskAssignmentId == 0)
            {
                using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                {
                    agendaReviewFileName = (from row in entities.BM_Meetings
                                            where row.MeetingID == meetingId && row.GenerateAgendaDocument == false
                                            select row.AgendaFilePath).FirstOrDefault();
                }
            }
            #endregion

            #region Get Generated File
            if (!string.IsNullOrEmpty(agendaReviewFileName))
            {
                if (generatePDF == false)
                {
                    int index = agendaReviewFileName.LastIndexOf(".pdf");
                    agendaReviewFileName = agendaReviewFileName.Substring(0, index) + ".docx";
                }

                var fileName = System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewFileName);
                if (File.Exists(fileName))
                {
                    if (generatePDF)
                    {
                        string DateFolder = "~/TempFiles" + "/" + Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                        if (!System.IO.Directory.Exists(DateFolder))
                        {
                            System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                        }
                        string User = userId + "" + customerId + "" + Convert.ToString(DateTime.Now.ToString("HHmmss"));
                        string FileName = System.Web.Hosting.HostingEnvironment.MapPath(DateFolder + "/" + User + Path.GetExtension(fileName));

                        FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (true)
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(objIFileData_Service.ReadDocFiles(fileName)));
                        }
                        bw.Close();

                        result.FileName = DateFolder + "/" + User + Path.GetExtension(fileName);
                        result.IsFileGenerated = true;
                    }
                    else
                    {
                        result.FileContent = CryptographyHandler.AESDecrypt(objIFileData_Service.ReadDocFiles(fileName));
                    }
                    return result;
                }
            }
            #endregion
            #endregion

            #region Generate Agenda Document
            //Process proc = Process.GetCurrentProcess();
            //var watchMain = new System.Diagnostics.Stopwatch();
            //var watch = new System.Diagnostics.Stopwatch();
            //var strWatchDetails = "";
            //watchMain.Start();
            //strWatchDetails += "Start : " + Environment.NewLine;
            //strWatchDetails += $"Execution Time: {watchMain.ElapsedMilliseconds} ms" + Environment.NewLine + $"Memory Usage: {proc.PrivateMemorySize64}" + Environment.NewLine;
            TableOfContent toc = null;
            var agendaCount = 0;
            var hasCustomAgendaTable = false;
            using (WordDocument doc = new WordDocument())
            {
                var docSetting = objIDocumentSetting_Service.GetDocumentSetting(customerId, SecretarialConst.MeetingDocumentTypes.AGENDA);
                var defaultParagraphFont = "Bookman Old Style";
                doc.EnsureMinimal();

                WParagraphStyle myParagraphStyle = (WParagraphStyle)doc.AddParagraphStyle("MyParagraphStyle");
                myParagraphStyle.CharacterFormat.FontName = "Bookman Old Style";
                myParagraphStyle.CharacterFormat.FontSize = 12;
                var objStyleVM = new VM.StyleVM();
                if (docSetting != null)
                {
                    if (docSetting.Styles != null)
                    {
                        objStyleVM = docSetting.Styles.Where(k => k.StyleName == "Paragraph").Select(k => k).FirstOrDefault();
                        if (objStyleVM != null)
                        {
                            myParagraphStyle.CharacterFormat.FontName = objStyleVM.FontName;
                            defaultParagraphFont = objStyleVM.FontName;
                            float fontSize = 12;
                            if (!float.TryParse(objStyleVM.FontSize, out fontSize))
                            {
                                fontSize = 12;
                            }
                            myParagraphStyle.CharacterFormat.FontSize = fontSize;
                        }
                    }
                }

                #region Meeting & Agenda details
                var agenda = objIAgendaMinutesReviewService.PreviewAgenda(meetingId, customerId, taskAssignmentId);
                var meetingSrNo = "";
                var meetingTypeName = "";
                if (agenda.MeetingSrNo.HasValue)
                {
                    meetingSrNo = agenda.MeetingSrNo + GetSuffix(Convert.ToInt32(agenda.MeetingSrNo));
                }

                if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                {
                    meetingTypeName = "Annual General Meeting";
                }
                else if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    meetingTypeName = "Extra Ordinary General Meeting";
                }

                string mDate = string.Empty;
                string mDay = string.Empty;
                string mTime = string.Empty;
                string mVenue = string.Empty;
                string cDate = string.Empty;

                if (agenda.MeetingDate != null)
                {
                    var date = Convert.ToDateTime(agenda.MeetingDate);
                    mDate = date.ToString("MMMM dd, yyyy");
                    mDay = date.ToString("dddd");
                }
                if (agenda.CircularDate != null)
                {
                    cDate = Convert.ToDateTime(agenda.CircularDate).ToString("MMMM dd, yyyy");
                }
                if (!string.IsNullOrEmpty(agenda.MeetingTime))
                {
                    mTime = agenda.MeetingTime;
                }
                if (!string.IsNullOrEmpty(agenda.MeetingVenue))
                {
                    mVenue = agenda.MeetingVenue;
                }
                #endregion

                Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlFormatProvider htmlProvider = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlFormatProvider();
                Telerik.Windows.Documents.Flow.Model.RadFlowDocument htmlDocument = new Telerik.Windows.Documents.Flow.Model.RadFlowDocument();

                #region Page Headers / Footer
                var hasPageHeader = false;
                var letterHead = objIDocumentSetting_Service.GetPageHeaderFooter(agenda.Entityt_Id);
                if(letterHead != null)
                {
                    if(letterHead.UsedInAgenda)
                    {
                        if (!string.IsNullOrEmpty(letterHead.PageHeaderFormat))
                        {
                            #region Page Header
                            doc.LastSection.PageSetup.DifferentFirstPage = true;
                            IWParagraph paragraphHeader = doc.LastSection.HeadersFooters.FirstPageHeader.AddParagraph();

                            doc.LastSection.PageSetup.HeaderDistance = 0f;

                            htmlDocument = htmlProvider.Import(letterHead.PageHeaderFormat);
                            Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings exportSettings1 = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings();

                            string htmlstringHeader = htmlProvider.Export(htmlDocument);

                            var isValidHtml = doc.LastSection.Body.IsValidXHTML(htmlstringHeader, XHTMLValidationType.None);
                            if (isValidHtml)
                            {
                                paragraphHeader.AppendHTML(htmlstringHeader);
                                hasPageHeader = true;
                            }
                            #endregion

                            #region Footer
                            if (hasPageHeader && !string.IsNullOrEmpty(letterHead.PageFooterFormat))
                            {
                                IWParagraph paragraphFooter = doc.LastSection.HeadersFooters.FirstPageFooter.AddParagraph();

                                doc.LastSection.PageSetup.FooterDistance = 0f;

                                htmlDocument = htmlProvider.Import(letterHead.PageFooterFormat);
                                string htmlstringFooter = htmlProvider.Export(htmlDocument);

                                var isValidHtmlFooter = doc.LastSection.Body.IsValidXHTML(htmlstringFooter, XHTMLValidationType.None);
                                if (isValidHtmlFooter)
                                {
                                    paragraphFooter.AppendHTML(htmlstringFooter);
                                }
                            }
                            #endregion
                        }
                    }
                }
                #endregion

                IWTable agendaTable = null;
                if (agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.AGM && agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.EGM)
                {
                    #region Agenda Document generation for Board & Committee
                    #region Header
                    var paragraph = doc.LastParagraph;
                    if(hasPageHeader == false)
                    {
                        var headerTextMain = paragraph.AppendText(agenda.EntityName) as WTextRange;
                        paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        headerTextMain.CharacterFormat.FontSize = 18;
                        headerTextMain.CharacterFormat.Bold = true;

                        paragraph.AppendText(Environment.NewLine);

                        headerTextMain = paragraph.AppendText("Registered Office: "+agenda.EntityAddressLine1) as WTextRange;
                        headerTextMain.CharacterFormat.FontSize = 10;
                        headerTextMain.CharacterFormat.Bold = true;

                        headerTextMain = paragraph.AppendText(agenda.EntityAddressLine2) as WTextRange;
                        headerTextMain.CharacterFormat.FontSize = 10;
                        headerTextMain.CharacterFormat.Bold = true;

                        paragraph.AppendText(Environment.NewLine);

                        headerTextMain = paragraph.AppendText("CIN: "+agenda.EntityCIN_LLPIN) as WTextRange;
                        headerTextMain.CharacterFormat.FontSize = 12;
                        headerTextMain.CharacterFormat.Bold = true;

                        paragraph = doc.LastSection.AddParagraph() as WParagraph;
                        paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        paragraph.AppendText(Environment.NewLine);
                    }

                    var agendaMainHeading = "AGENDA FOR THE " + meetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "") + " " + agenda.MeetingTypeName.ToUpper() + (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING ? " MEETING" : " CIRCULAR");
                    var headerText = paragraph.AppendText(agendaMainHeading) as WTextRange;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    headerText.CharacterFormat.FontSize = 14;
                    headerText.CharacterFormat.Bold = true;
                    #endregion

                    #region Meeting details table
                    if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING)
                    {
                        #region Table
                        paragraph = doc.LastSection.AddParagraph() as WParagraph;
                        IWTable table = doc.LastSection.AddTable();
                        table.ResetCells(4, 3);
                        //Specifies the horizontal alignment of the table
                        table.TableFormat.HorizontalAlignment = RowAlignment.Center;

                        float tableWidth = doc.LastSection.PageSetup.PageSize.Width - (doc.LastSection.PageSetup.Margins.Left + doc.LastSection.PageSetup.Margins.Right);

                        tableWidth = (tableWidth / 100) * 80;
                        //float individualCellWidth = tableWidth / 3;
                        float individualCellWidth = tableWidth / 100;

                        for (int i = 0; i < 4; i++)
                        {
                            table.Rows[i].Cells[0].Width = 18 * individualCellWidth;
                            table.Rows[i].Cells[1].Width = 2 * individualCellWidth;
                            table.Rows[i].Cells[2].Width = 80 * individualCellWidth;
                        }

                        var cellRun = table.Rows[0].Cells[0].AddParagraph().AppendText("DATE") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[0].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[0].Cells[2].AddParagraph().AppendText(mDate) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        //Day
                        cellRun = table.Rows[1].Cells[0].AddParagraph().AppendText("DAY") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[2].AddParagraph().AppendText(mDay) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;
                        //End

                        //Time
                        cellRun = table.Rows[2].Cells[0].AddParagraph().AppendText("TIME") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[2].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[2].Cells[2].AddParagraph().AppendText(mTime) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;
                        //End

                        //Venue
                        cellRun = table.Rows[3].Cells[0].AddParagraph().AppendText("VENUE") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[3].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[3].Cells[2].AddParagraph().AppendText(mVenue) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;
                        //End
                        #endregion
                    }
                    else
                    {
                        #region Table
                        paragraph = doc.LastSection.AddParagraph() as WParagraph;
                        IWTable table = doc.LastSection.AddTable();
                        table.ResetCells(2, 3);
                        //Specifies the horizontal alignment of the table
                        table.TableFormat.HorizontalAlignment = RowAlignment.Center;
                        float tableWidth = doc.LastSection.PageSetup.PageSize.Width - (doc.LastSection.PageSetup.Margins.Left + doc.LastSection.PageSetup.Margins.Right);

                        tableWidth = (tableWidth / 100) * 80;
                        //float individualCellWidth = tableWidth / 3;
                        float individualCellWidth = tableWidth / 100;

                        for (int i = 0; i < 2; i++)
                        {
                            table.Rows[i].Cells[0].Width = 22 * individualCellWidth;
                            table.Rows[i].Cells[1].Width = 2 * individualCellWidth;
                            table.Rows[i].Cells[2].Width = 76 * individualCellWidth;
                        }

                        var cellRun = table.Rows[0].Cells[0].AddParagraph().AppendText("CIRCULAR DATE") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[0].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[0].Cells[2].AddParagraph().AppendText(cDate) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[0].AddParagraph().AppendText("DUE DATE") as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[1].AddParagraph().AppendText(":") as WTextRange;
                        cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;

                        cellRun = table.Rows[1].Cells[2].AddParagraph().AppendText(mDate) as WTextRange;
                        cellRun.CharacterFormat.Bold = true;
                        cellRun.CharacterFormat.FontSize = 12;
                        #endregion
                    }
                    #endregion

                    #region TOC
                    WParagraph para = doc.LastSection.AddParagraph() as WParagraph;
                    //Insert TOC
                    //TableOfContent toc = null;
                    //Commented on 22 Oct 2021
                    //if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING)
                    if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING || agenda.MeetingCircular == SecretarialConst.MeetingCircular.CIRCULAR)
                    {
                        para.AppendText("Agenda Items");
                        para.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                        para.ApplyStyle(BuiltinStyle.Heading1);

                        //Commented on 16 Nov 2021
                        //para = doc.LastSection.AddParagraph() as WParagraph;
                        //para.AppendText(Environment.NewLine);

                        //Commented on 20 Apr 2021
                        //toc = para.AppendTOC(1, 3);
                        //Added Bellow code
                        //Commented on 16 Nov 2021
                        //if (hasPageHeader)
                        //{
                        //    para = doc.LastSection.AddParagraph() as WParagraph;
                        //    IWTable table = doc.LastSection.AddTable();
                        //    table.ResetCells(1, 2);

                        //    float tableWidth = doc.LastSection.PageSetup.PageSize.Width - (doc.LastSection.PageSetup.Margins.Left + doc.LastSection.PageSetup.Margins.Right);
                        //    table.Rows[0].Cells[0].Width = 70;
                        //    table.Rows[0].Cells[1].Width = tableWidth - 70;
                        //    table.TableFormat.Borders.BorderType = BorderStyle.None;

                        //    toc = table.Rows[0].Cells[1].AddParagraph().AppendTOC(1, 3);

                        //    doc.LastSection.PageSetup.Margins.Left = 0;
                        //}
                        //else
                        //{
                        //    toc = para.AppendTOC(1, 3);
                        //}
                        //Added on 16 Nov 2021
                        if (hasPageHeader)
                        {
                            doc.LastSection.PageSetup.Margins.Left = 0;
                        }
                        if (agenda.lstAgendaItems != null)
                        {
                            agendaCount = agenda.lstAgendaItems.Count;
                            if (agendaCount > 0)
                            {
                                paragraph = doc.LastSection.AddParagraph() as WParagraph;
                                agendaTable = doc.LastSection.AddTable();
                                agendaTable.ResetCells((agendaCount + 1), 3);
                                //Specifies the horizontal alignment of the table
                                agendaTable.TableFormat.HorizontalAlignment = RowAlignment.Center;

                                float tableWidth = doc.LastSection.PageSetup.PageSize.Width - (doc.LastSection.PageSetup.Margins.Left + doc.LastSection.PageSetup.Margins.Right);

                                tableWidth = (tableWidth / 100) * 90;
                                float individualCellWidth = tableWidth / 100;

                                agendaTable.Rows[0].Cells[0].Width = 13 * individualCellWidth;
                                agendaTable.Rows[0].Cells[1].Width = 72 * individualCellWidth;
                                agendaTable.Rows[0].Cells[2].Width = 15 * individualCellWidth;

                                var cellRunTbl = agendaTable.Rows[0].Cells[0].AddParagraph().AppendText("Sr. No.") as WTextRange;
                                cellRunTbl.CharacterFormat.Bold = true;
                                cellRunTbl.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                                cellRunTbl.CharacterFormat.FontSize = 12;

                                cellRunTbl = agendaTable.Rows[0].Cells[1].AddParagraph().AppendText("Agenda") as WTextRange;
                                cellRunTbl.CharacterFormat.Bold = true;
                                cellRunTbl.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                                cellRunTbl.CharacterFormat.FontSize = 12;

                                cellRunTbl = agendaTable.Rows[0].Cells[2].AddParagraph().AppendText("Page No.") as WTextRange;
                                cellRunTbl.CharacterFormat.Bold = true;
                                cellRunTbl.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                                cellRunTbl.CharacterFormat.FontSize = 12;

                                for (int i = 0; i < agendaCount; i++)
                                {
                                    agendaTable.Rows[i + 1].Cells[0].Width = 13 * individualCellWidth;
                                    agendaTable.Rows[i + 1].Cells[1].Width = 72 * individualCellWidth;
                                    agendaTable.Rows[i + 1].Cells[2].Width = 15 * individualCellWidth;

                                    cellRunTbl = agendaTable.Rows[i + 1].Cells[0].AddParagraph().AppendText((i+1).ToString()) as WTextRange;
                                    cellRunTbl.CharacterFormat.FontSize = 12;

                                    //var htmlstring = "Item No. " + (i + 1) + " - " + (string.IsNullOrEmpty(agenda.lstAgendaItems[i].AgendaItemText) ? "" : agenda.lstAgendaItems[i].AgendaItemText.ToUpper());
                                    var htmlstring = (string.IsNullOrEmpty(agenda.lstAgendaItems[i].AgendaItemText) ? "" : agenda.lstAgendaItems[i].AgendaItemText.ToUpper());
                                    htmlstring = htmlstring.Replace("<B>", "");
                                    htmlstring = htmlstring.Replace("</B>", "");

                                    cellRunTbl = agendaTable.Rows[i + 1].Cells[1].AddParagraph().AppendText(htmlstring) as WTextRange;
                                    cellRunTbl.CharacterFormat.FontSize = 12;
                                }

                                hasCustomAgendaTable = true;
                            }
                        }
                        //End
                        //End
                    }
                    WSection section = doc.LastSection;
                    #endregion

                    #region Agenda Items
                    if (agenda.lstAgendaItems != null)
                    {
                        WParagraph newPara = section.AddParagraph() as WParagraph;
                        var SrNo = 0;
                        var docCount = 1;
                        foreach (var item in agenda.lstAgendaItems)
                        {
                            SrNo++;

                            //Commented on 19 Feb 2021 for testing
                            //newPara = section.AddParagraph() as WParagraph;
                            //newPara.AppendBreak(Syncfusion.DocIO.DLS.BreakType.PageBreak);

                            section = doc.AddSection() as WSection;
                            #region Page Setup
                            if (docSetting != null)
                            {
                                if (docSetting.PageSetting != null)
                                {
                                    switch (docSetting.PageSetting.PageSize)
                                    {
                                        case "A3":
                                            section.PageSetup.PageSize = PageSize.A3;
                                            break;
                                        case "A4":
                                            section.PageSetup.PageSize = PageSize.A4;
                                            break;
                                        case "A5":
                                            section.PageSetup.PageSize = PageSize.A5;
                                            break;
                                        case "A6":
                                            section.PageSetup.PageSize = PageSize.A6;
                                            break;
                                        case "Legal":
                                            section.PageSetup.PageSize = PageSize.Legal;
                                            break;
                                        case "Letter":
                                            section.PageSetup.PageSize = PageSize.Letter;
                                            break;
                                        default:
                                            section.PageSetup.PageSize = PageSize.A4;
                                            break;
                                    }
                                }

                                section.PageSetup.Margins.Top = (float)docSetting.PageSetting.MarginTop;
                                section.PageSetup.Margins.Left = (float)docSetting.PageSetting.MarginLeft;
                                section.PageSetup.Margins.Right = (float)docSetting.PageSetting.MarginRight;
                                section.PageSetup.Margins.Bottom = (float)docSetting.PageSetting.MarginBottom;
                            }
                            #endregion
                            //newPara = section.AddParagraph() as WParagraph;
                            //newPara.AppendBreak(Syncfusion.DocIO.DLS.BreakType.PageBreak);

                            //newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Right;
                            //WTextRange text = newPara.AppendText("Item No. " + SrNo) as WTextRange;
                            //text.CharacterFormat.UnderlineStyle = UnderlineStyle.Single;
                            //text.CharacterFormat.Bold = true;
                            //text.CharacterFormat.Italic = true;
                            //text.CharacterFormat.FontSize = 12;

                            newPara = section.AddParagraph() as WParagraph;

                            #region Convert html format in XHTML 1.0
                            var htmlstring = "Item No. " + SrNo + " - " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper());
                            htmlstring = htmlstring.Replace("<B>", "<b>");
                            htmlstring = htmlstring.Replace("</B>", "</b>");
                            #endregion

                            //Added on 16 Nov 2021
                            if (hasCustomAgendaTable)
                            {
                                if(agendaTable != null)
                                {
                                    doc.UpdateWordCount(true);
                                    var p = doc.BuiltinDocumentProperties.PageCount;

                                    var cellRunTbl = agendaTable.Rows[SrNo].Cells[2].AddParagraph().AppendText(p.ToString()) as WTextRange;
                                    cellRunTbl.CharacterFormat.FontSize = 12;
                                }
                            }
                            //End
                            bool isValidHtml = false;
                            newPara.AppendHTML(htmlstring);
                            newPara.ParagraphFormat.Borders.Bottom.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Single;
                            newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                            newPara.ApplyStyle(BuiltinStyle.Heading2);
                            //

                            #region Convert html format in XHTML 1.0
                            htmlDocument = htmlProvider.Import(item.AgendaFormat);
                            Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings exportSettings = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings();

                            htmlstring = htmlProvider.Export(htmlDocument);
                            #endregion

                            isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                            if (isValidHtml)
                            {
                                newPara = section.AddParagraph() as WParagraph;
                                newPara.AppendHTML(htmlstring);

                                var ss = newPara.StyleName;
                                newPara.ApplyStyle("MyParagraphStyle");
                            }

                            #region Agenda Documents
                            var docs = objIAgendaMinutesReviewService.GetAgendaDocument(item.MeetingAgendaMappingID, customerId);
                            if (docs != null)
                            {
                                foreach (var annexure in docs)
                                {
                                    //Commented on 19 Feb 2021 for testing
                                    //section.BreakCode = SectionBreakCode.NewPage;
                                    //newPara = section.AddParagraph() as WParagraph;

                                    section = doc.AddSection() as WSection;
                                    section.PageSetup.PageSize = PageSize.A4;
                                    section.PageSetup.Margins = new MarginsF(0, 0, 0, 30);
                                    newPara = section.AddParagraph() as WParagraph;

                                    //var actualFileName = objIFileData_Service.GetSecretarialDocs(annexure.ID);
                                    var fileName = annexure.FileName;
                                    var extension = System.IO.Path.GetExtension(fileName);
                                    switch (extension)
                                    {
                                        case ".pdf1":
                                            newPara.AppendText("Annexure " + docCount);
                                            newPara.ApplyStyle(BuiltinStyle.Heading3);
                                            newPara = section.AddParagraph() as WParagraph;

                                            string DateFolder = "~/TempFiles/" + Convert.ToString(DateTime.Now.ToString("ddMMyy")) + "/" + userId;
                                            //Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                                            }

                                            //var actualFileName = objIFileData_Service.GetSecretarialDocs(annexure.ID);
                                            //PdfLoadedDocument loadedDocument = new PdfLoadedDocument(actualFileName.FileData);
                                            var tempFilePDF = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                            if (!string.IsNullOrEmpty(tempFilePDF))
                                            {
                                                var aa = GleamTech.DocumentUltimate.DocumentConverter.Convert(System.Web.Hosting.HostingEnvironment.MapPath(@"~\" + tempFilePDF), System.Web.Hosting.HostingEnvironment.MapPath(DateFolder + "/Sample.png"), GleamTech.DocumentUltimate.DocumentFormat.Png);

                                                float clientWidth = section.PageSetup.ClientWidth;
                                                float clientHeight = section.PageSetup.PageSize.Height - section.PageSetup.Margins.Top - section.PageSetup.Margins.Bottom;

                                                foreach (var fNames in aa.OutputFiles)
                                                {
                                                    //var image = System.Drawing.Image.FromFile(Server.MapPath("~/pdf/Temp/"+ item));
                                                    using (var image = System.Drawing.Image.FromFile(fNames))
                                                    {
                                                        //para1.AppendPicture(image);

                                                        float scalePer = 100;
                                                        float scalePerH = 100;

                                                        var picture = newPara.AppendPicture(image) as WPicture;
                                                        if (picture.Width > clientWidth)
                                                        {
                                                            scalePer = clientWidth / image.Width * 100;
                                                        }
                                                        else if (picture.Height > clientHeight)
                                                        {
                                                            scalePerH = clientHeight / image.Height * 100;
                                                        }
                                                        // This will resizes the width and height.
                                                        picture.WidthScale = scalePer;
                                                        picture.HeightScale = scalePerH;
                                                    }
                                                }
                                                docCount++;
                                                DirectoryInfo dir = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                                                foreach (FileInfo fi in dir.GetFiles())
                                                {
                                                    fi.Delete();
                                                }
                                            }
                                            break;
                                        case ".pdf":
                                            //var actualFileName = objIFileData_Service.GetSecretarialDocs(annexure.ID);
                                            //PdfLoadedDocument loadedDocument = new PdfLoadedDocument(actualFileName.FileData);
                                            var tempFile = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                            if (!string.IsNullOrEmpty(tempFile))
                                            {
                                                newPara = section.AddParagraph() as WParagraph; // Added on 31 Dec 2021
                                                newPara.AppendText("\t Annexure " + docCount);
                                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                                                newPara.ApplyStyle(BuiltinStyle.Heading3);
                                                newPara = section.AddParagraph() as WParagraph;

                                                newPara.ApplyStyle(BuiltinStyle.Normal); //Added on 28 Dec 2021
                                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                                                using (PdfLoadedDocument loadedDocument = new PdfLoadedDocument(System.Web.Hosting.HostingEnvironment.MapPath(@"~\" + tempFile)))
                                                {
                                                    //Get the PDF page count
                                                    int count = loadedDocument.Pages.Count;
                                                    //Convert each page to image file
                                                    if (count > 0)
                                                    {
                                                        for (var p = 0; p < count; p++)
                                                        {
                                                            using (var image = loadedDocument.ExportAsImage(p))
                                                            {
                                                                //Added on 28 Dec 2021
                                                                if(p > 0)
                                                                {
                                                                    section = doc.AddSection() as WSection;
                                                                    section.PageSetup.PageSize = PageSize.A4;
                                                                    section.PageSetup.Margins = new MarginsF(0, 0, 0, 30);
                                                                    newPara = section.AddParagraph() as WParagraph;
                                                                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                                                                }
                                                                //End

                                                                var picture = newPara.AppendPicture(image) as WPicture;
                                                                float clientWidth = section.PageSetup.ClientWidth;
                                                                float clientHeight = section.PageSetup.PageSize.Height - section.PageSetup.Margins.Top - section.PageSetup.Margins.Bottom;
                                                                float scalePer = 100;
                                                                float scalePerH = 100;
                                                                clientHeight -= 100;

                                                                if (picture.Width > clientWidth && picture.Height <= clientHeight)
                                                                {
                                                                    scalePer = 90;
                                                                    scalePerH = 100;
                                                                }
                                                                else if (picture.Width > clientWidth)
                                                                {
                                                                    scalePer = 90;
                                                                    scalePerH = 90;
                                                                }

                                                                if (picture.Height > clientHeight && scalePerH > 85)
                                                                {
                                                                    scalePer = 90;
                                                                    scalePerH = 85;
                                                                }

                                                                // This will resizes the width and height.
                                                                picture.WidthScale = scalePer;
                                                                picture.HeightScale = scalePerH;
                                                            }
                                                        }

                                                    }
                                                    loadedDocument.Close();
                                                    docCount++;
                                                }
                                            }
                                            break;
                                        //case ".pdf":
                                        //    newPara.AppendText("Annexure " + docCount);
                                        //    newPara.ApplyStyle(BuiltinStyle.Heading3);
                                        //    newPara = section.AddParagraph() as WParagraph;

                                        //    //var actualFileName = objIFileData_Service.GetSecretarialDocs(annexure.ID);
                                        //    //PdfLoadedDocument loadedDocument = new PdfLoadedDocument(actualFileName.FileData);
                                        //    var tempFile = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                        //    if(!string.IsNullOrEmpty(tempFile))
                                        //    {
                                        //        using (PdfLoadedDocument loadedDocument = new PdfLoadedDocument(System.Web.Hosting.HostingEnvironment.MapPath(@"~\" + tempFile)))
                                        //        {
                                        //            //Get the PDF page count
                                        //            int count = loadedDocument.Pages.Count;
                                        //            //Convert each page to image file
                                        //            if (count > 0)
                                        //            {
                                        //                for (var  p =0; p < count; p++)
                                        //                {
                                        //                    using (var image = loadedDocument.ExportAsImage(p))
                                        //                    {
                                        //                        var picture = newPara.AppendPicture(image) as WPicture;
                                        //                        float clientWidth = section.PageSetup.ClientWidth;
                                        //                        float clientHeight = section.PageSetup.PageSize.Height - section.PageSetup.Margins.Top - section.PageSetup.Margins.Bottom;
                                        //                        float scalePer = 100;
                                        //                        float scalePerH = 100;

                                        //                        if (picture.Width > clientWidth)
                                        //                        {
                                        //                            scalePer = clientWidth / image.Width * 100;
                                        //                        }
                                        //                        else if (picture.Height > clientHeight)
                                        //                        {
                                        //                            scalePerH = clientHeight / image.Height * 100;
                                        //                        }
                                        //                        // This will resizes the width and height.
                                        //                        picture.WidthScale = scalePer;
                                        //                        picture.HeightScale = scalePerH;
                                        //                    }       
                                        //                }
                                        //            }
                                        //            loadedDocument.Close();
                                        //            docCount++;
                                        //        }
                                        //    }
                                        //    break;
                                        case ".docx":
                                        case ".doc":
                                            var tempFile1 = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                            if (!string.IsNullOrEmpty(tempFile1))
                                            {
                                                newPara = section.AddParagraph() as WParagraph; // Added on 31 Dec 2021
                                                newPara.AppendText("\t Annexure " + docCount);
                                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                                                newPara.ApplyStyle(BuiltinStyle.Heading3);
                                                newPara = section.AddParagraph() as WParagraph;

                                                newPara.ApplyStyle(BuiltinStyle.Normal); //Added on 28 Dec 2021
                                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;

                                                #region Word to image
                                                var formatType = FormatType.Docx;
                                                if (extension == ".doc")
                                                {
                                                    formatType = FormatType.Doc;
                                                }

                                                //Loads an existing Word document
                                                using (WordDocument wordDocument = new WordDocument(System.Web.Hosting.HostingEnvironment.MapPath(@"~\" + tempFile1), formatType))
                                                {
                                                    //Initializes the ChartToImageConverter for converting charts during Word to image conversion
                                                    //wordDocument.ChartToImageConverter = new ChartToImageConverter();
                                                    //Sets the scaling mode for charts (Normal mode reduces the file size)
                                                    //wordDocument.ChartToImageConverter.ScalingMode = ScalingMode.Normal;
                                                    //Converts word document to image
                                                    var images = wordDocument.RenderAsImages(ImageType.Metafile);
                                                    //var images = wordDocument.RenderAsImages(ImageType.Bitmap);
                                                    var imgC = 0;
                                                    foreach (var image in images)
                                                    {
                                                        //Added on 28 Dec 2021
                                                        if (imgC > 0)
                                                        {
                                                            section = doc.AddSection() as WSection;
                                                            section.PageSetup.PageSize = PageSize.A4;
                                                            section.PageSetup.Margins = new MarginsF(0, 0, 0, 30);
                                                            newPara = section.AddParagraph() as WParagraph;
                                                            newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                                                        }
                                                        imgC++;
                                                        //End

                                                        float clientWidth = section.PageSetup.ClientWidth;
                                                        float clientHeight = section.PageSetup.PageSize.Height - section.PageSetup.Margins.Top - section.PageSetup.Margins.Bottom - 60;
                                                        float scalePer = 95, scalePerH = 95;

                                                        var picture = newPara.AppendPicture(image) as WPicture;

                                                        if (picture.Width > clientWidth && picture.Height <= clientHeight)
                                                        {
                                                            scalePer = 90;
                                                            scalePerH = 95;
                                                        }
                                                        else if (picture.Width > clientWidth)
                                                        {
                                                            scalePer = 90;
                                                            scalePerH = 90;
                                                        }

                                                        if (picture.Height > clientHeight && scalePerH > 85)
                                                        {
                                                            scalePer = 90;
                                                            scalePerH = 85;
                                                        }

                                                        // This will resizes the width and height.
                                                        picture.WidthScale = scalePer;
                                                        picture.HeightScale = scalePerH;
                                                        //End

                                                        //Sets horizontal and vertical alignments
                                                        picture.HorizontalAlignment = ShapeHorizontalAlignment.Center;
                                                        picture.VerticalAlignment = ShapeVerticalAlignment.Bottom;
                                                    }
                                                    wordDocument.Close();
                                                }

                                                #endregion
                                                docCount++;
                                            }

                                            #region insert word
                                            //var tempFile = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                            //if(!string.IsNullOrEmpty(tempFile))
                                            //{
                                            //    newPara.AppendText("Annexure " + docCount);
                                            //    newPara.ApplyStyle(BuiltinStyle.Heading3);
                                            //    newPara = section.AddParagraph() as WParagraph;

                                            //    WordDocument wordDocument = new WordDocument(Server.MapPath(@"~\" + tempFile), FormatType.Docx);
                                            //    doc.ImportContent(wordDocument, ImportOptions.KeepSourceFormatting);
                                            //    //wordDocument.ChartToImageConverter = new ChartToImageConverter();
                                            //    wordDocument.RenderAsImages(ImageType.Metafile);

                                            //    wordDocument.Close();
                                            //    docCount++;
                                            //}
                                            #endregion
                                            break;
                                        //case ".doc":
                                        //    #region insert word
                                        //    var tempFiledoc = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                        //    if (!string.IsNullOrEmpty(tempFiledoc))
                                        //    {
                                        //        newPara.AppendText("Annexure " + docCount);
                                        //        newPara.ApplyStyle(BuiltinStyle.Heading3);
                                        //        newPara = section.AddParagraph() as WParagraph;
                                        //        WordDocument wordDocument = new WordDocument(Server.MapPath(@"~\" + tempFiledoc), FormatType.Doc);
                                        //        doc.ImportContent(wordDocument, ImportOptions.UseDestinationStyles);
                                        //        wordDocument.Close();

                                        //        docCount++;
                                        //    }
                                        //    #endregion
                                        //    break;
                                        case ".bmp":
                                        case ".jpeg":
                                        case ".jpg":
                                        case ".png":
                                        case ".tiff":
                                        case ".ico":
                                        case ".icon":
                                            var tempFileimg = objIFileData_Service.GetSecretarialFile(annexure.ID, userId, customerId);
                                            if (!string.IsNullOrEmpty(tempFileimg))
                                            {
                                                newPara = section.AddParagraph() as WParagraph; // Added on 31 Dec 2021
                                                newPara.AppendText("\t Annexure " + docCount);
                                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                                                newPara.ApplyStyle(BuiltinStyle.Heading3);
                                                newPara = section.AddParagraph() as WParagraph;

                                                newPara.ApplyStyle(BuiltinStyle.Normal); //Added on 28 Dec 2021
                                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;

                                                System.Drawing.Image img = System.Drawing.Image.FromFile(System.Web.Hosting.HostingEnvironment.MapPath(@"~\" + tempFileimg));
                                                //newPara.AppendPicture(img);

                                                var picture = newPara.AppendPicture(img) as WPicture;
                                                float clientWidth = section.PageSetup.ClientWidth;
                                                float clientHeight = section.PageSetup.PageSize.Height - section.PageSetup.Margins.Top - section.PageSetup.Margins.Bottom;
                                                float scalePer = 100;
                                                float scalePerH = 100;
                                                clientHeight -= 100;

                                                if (picture.Width > clientWidth && picture.Height <= clientHeight)
                                                {
                                                    scalePer = 90;
                                                    scalePerH = 100;
                                                }
                                                else if (picture.Width > clientWidth)
                                                {
                                                    scalePer = 90;
                                                    scalePerH = 90;
                                                }

                                                if (picture.Height > clientHeight && scalePerH > 85)
                                                {
                                                    scalePer = 90;
                                                    scalePerH = 85;
                                                }
                                                // This will resizes the width and height.
                                                picture.WidthScale = scalePer;
                                                picture.HeightScale = scalePerH;


                                                docCount++;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion

                    #region Page Numbers
                    //IWParagraph paragraphFooter = section.HeadersFooters.Footer.AddParagraph();
                    //paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    //var pageNum = paragraphFooter.AppendField("Page", FieldType.FieldPage) as WField;
                    //pageNum.CharacterFormat.FontSize = 10;
                    ////paragraphFooter = section.AddParagraph();
                    #endregion

                    #region Update table of content
                    //if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING && toc != null)
                    //{
                    //    toc.IncludePageNumbers = true;
                    //    toc.RightAlignPageNumbers = true;
                    //    toc.UseHyperlinks = true;
                    //    toc.LowerHeadingLevel = Convert.ToInt32(1);
                    //    toc.UpperHeadingLevel = Convert.ToInt32(3);
                    //    toc.UseOutlineLevels = true;
                    //    toc.UseTableEntryFields = true;

                    //    #region TOC Styles
                    //    //Style MyStyleTOC1 = (WParagraphStyle)doc.AddParagraphStyle("MyStyleTOC1");
                    //    //MyStyleTOC1.CharacterFormat.Bold = true;
                    //    //MyStyleTOC1.CharacterFormat.FontName = defaultParagraphFont;
                    //    //MyStyleTOC1.CharacterFormat.FontSize = 16;
                    //    //toc.SetTOCLevelStyle(1, MyStyleTOC1.Name);

                    //    //Style MyStyleTOC2 = (WParagraphStyle)doc.AddParagraphStyle("MyStyleTOC2");
                    //    //MyStyleTOC2.CharacterFormat.Bold = true;
                    //    //MyStyleTOC2.CharacterFormat.FontName = defaultParagraphFont;
                    //    //MyStyleTOC2.CharacterFormat.FontSize = 14;
                    //    //toc.SetTOCLevelStyle(2, MyStyleTOC2.Name);
                    //    #endregion

                    //    doc.UpdateTableOfContents();
                    //}
                    #endregion

                    #region Page Setup
                    //if (docSetting != null)
                    //{
                    //    if (docSetting.PageSetting != null)
                    //    {
                    //        switch (docSetting.PageSetting.PageSize)
                    //        {
                    //            case "A3":
                    //                section.PageSetup.PageSize = PageSize.A3;
                    //                break;
                    //            case "A4":
                    //                section.PageSetup.PageSize = PageSize.A4;
                    //                break;
                    //            case "A5":
                    //                section.PageSetup.PageSize = PageSize.A5;
                    //                break;
                    //            case "A6":
                    //                section.PageSetup.PageSize = PageSize.A6;
                    //                break;
                    //            case "Legal":
                    //                section.PageSetup.PageSize = PageSize.Legal;
                    //                break;
                    //            case "Letter":
                    //                section.PageSetup.PageSize = PageSize.Letter;
                    //                break;
                    //            default:
                    //                section.PageSetup.PageSize = PageSize.A4;
                    //                break;
                    //        }
                    //    }

                    //    section.PageSetup.Margins.Top = (float)docSetting.PageSetting.MarginTop;
                    //    section.PageSetup.Margins.Left = (float)docSetting.PageSetting.MarginLeft;
                    //    section.PageSetup.Margins.Right = (float)docSetting.PageSetting.MarginRight;
                    //    section.PageSetup.Margins.Bottom = (float)docSetting.PageSetting.MarginBottom;
                    //}
                    #endregion
                    #endregion
                }
                else
                {
                    #region Agenda Document generation for AGM/EGM
                    #region Header
                    var paragraph = doc.LastParagraph;
                    var headerText = paragraph.AppendText(agenda.EntityName) as WTextRange;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    headerText.CharacterFormat.FontSize = 18;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    headerText = paragraph.AppendText("Registered Office: "+agenda.EntityAddressLine1) as WTextRange;
                    headerText.CharacterFormat.FontSize = 10;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    headerText = paragraph.AppendText(agenda.EntityAddressLine2) as WTextRange;
                    headerText.CharacterFormat.FontSize = 10;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    headerText = paragraph.AppendText("CIN: "+agenda.EntityCIN_LLPIN) as WTextRange;
                    headerText.CharacterFormat.FontSize = 11;
                    headerText.CharacterFormat.Bold = true;

                    paragraph.AppendText(Environment.NewLine);

                    paragraph = doc.LastSection.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;

                    headerText = paragraph.AppendText("NOTICE OF " + meetingSrNo + " " + meetingTypeName.ToUpper() + "") as WTextRange;
                    headerText.CharacterFormat.FontSize = 14;
                    headerText.CharacterFormat.Bold = true;
                    paragraph.AppendText(Environment.NewLine);

                    paragraph = doc.LastSection.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Justify;
                    var agendaMainHeading = "Notice is hereby given that the " + meetingSrNo + " " + meetingTypeName + " of the members of " + agenda.EntityName + " will be held on " + mDay + " " + mDate + " at " + mTime + " at " + mVenue + " to transact following business:";
                    headerText = paragraph.AppendText(agendaMainHeading) as WTextRange;
                    headerText.CharacterFormat.FontSize = 12;
                    //headerText.CharacterFormat.Bold = true;

                    WSection section = doc.LastSection;
                    #endregion

                    #region Agenda Items
                    if (agenda.lstAgendaItems != null)
                    {
                        IWParagraph newPara;
                        var SrNo = 0;
                        string business_ = "", businessLast_ = "";
                        foreach (var item in agenda.lstAgendaItems)
                        {
                            SrNo++;
                            businessLast_ = item.IsOrdinaryBusiness ? "Ordinary Business" : "Special Business";

                            if (business_ != businessLast_)
                            {
                                business_ = businessLast_;
                                newPara = section.AddParagraph() as WParagraph;
                                var headingTextRange = newPara.AppendText(business_) as WTextRange;
                                headingTextRange.CharacterFormat.FontSize = 14;
                                newPara.ApplyStyle(BuiltinStyle.Heading1);
                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                            }

                            newPara = section.AddParagraph() as WParagraph;

                            #region Convert html format in XHTML 1.0
                            //var htmlstring = SrNo + ". " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper());
                            var htmlstring = SrNo + ". " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText);
                            htmlstring = htmlstring.Replace("<B>", "<b>");
                            htmlstring = htmlstring.Replace("</B>", "</b>");
                            #endregion

                            bool isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                            if (isValidHtml)
                            {
                                newPara.AppendHTML(htmlstring);
                                newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                                newPara.ApplyStyle(BuiltinStyle.Heading2);
                            }

                            if (!item.IsOrdinaryBusiness)
                            {
                                htmlstring = item.AgendaFormat == null ? " " : item.AgendaFormat;

                                htmlDocument = htmlProvider.Import(htmlstring);
                                htmlstring = htmlProvider.Export(htmlDocument);

                                newPara = section.AddParagraph() as WParagraph;

                                isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                                if (isValidHtml)
                                {
                                    newPara.AppendHTML(htmlstring);
                                }
                            }
                            #region Page Numbers
                            //IWParagraph paragraphFooter = section.HeadersFooters.Footer.AddParagraph();
                            //paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                            //paragraphFooter.AppendField("Page", FieldType.FieldPage);
                            //paragraphFooter = section.AddParagraph();
                            #endregion
                        }
                    }
                    #endregion

                    #region Page Numbers
                    IWParagraph paragraphFooter = section.HeadersFooters.Footer.AddParagraph();
                    paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    var pageNum = paragraphFooter.AppendField("Page", FieldType.FieldPage) as WField;
                    pageNum.CharacterFormat.FontSize = 10;
                    #endregion

                    #region Signing Authority
                    paragraph = section.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;

                    paragraph.AppendText(Environment.NewLine);
                    var textRange = paragraph.AppendText("By Order of the Board of Director") as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    textRange = paragraph.AppendText("For " + agenda.EntityName) as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;

                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    paragraph = section.AddParagraph() as WParagraph;
                    if (agenda.MeetingSigningAuthorityForAGM != null)
                    {
                        textRange = paragraph.AppendText(agenda.MeetingSigningAuthorityForAGM.AuthorityName) as WTextRange;
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                        textRange = paragraph.AppendText("(" + agenda.MeetingSigningAuthorityForAGM.Designation + ")") as WTextRange;
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                        if (agenda.MeetingSigningAuthorityForAGM.IsCS == true)
                        {
                            textRange = paragraph.AppendText("Membership No. " + agenda.MeetingSigningAuthorityForAGM.MembershipNo) as WTextRange;
                            textRange.CharacterFormat.FontSize = 12;
                            textRange.CharacterFormat.Bold = true;
                            paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                        }
                        else
                        {
                            textRange = paragraph.AppendText("DIN : " + agenda.MeetingSigningAuthorityForAGM.DIN_PAN) as WTextRange;
                            textRange.CharacterFormat.FontSize = 12;
                            textRange.CharacterFormat.Bold = true;
                            paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                        }
                    }

                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    textRange = paragraph.AppendText("Date") as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);


                    textRange = paragraph.AppendText("Place : " + agenda.EntityCity) as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    #endregion

                    #region Notes
                    if (!string.IsNullOrEmpty(agenda.GM_Notes))
                    {
                        paragraph = section.AddParagraph() as WParagraph;
                        var notesTextRange = paragraph.AppendText("Notes for Members' Attention") as WTextRange;
                        notesTextRange.CharacterFormat.FontSize = 14;
                        paragraph.ApplyStyle(BuiltinStyle.Heading1);

                        var htmlstring = agenda.GM_Notes == null ? " " : agenda.GM_Notes;
                        htmlDocument = htmlProvider.Import(htmlstring);
                        htmlstring = htmlProvider.Export(htmlDocument);

                        paragraph = section.AddParagraph() as WParagraph;

                        var isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                        if (isValidHtml)
                        {
                            paragraph.AppendHTML(htmlstring);
                            paragraph.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Justify;
                        }
                    }
                    #endregion

                    #region Explanatory statements
                    var hasExplanatorystatements = false;
                    if (agenda.lstAgendaItems != null)
                    {
                        foreach (var item in agenda.lstAgendaItems)
                        {
                            if (!string.IsNullOrEmpty(item.ExplanatoryStatement))
                            {
                                hasExplanatorystatements = true;
                                break;
                            }
                        }

                        if (hasExplanatorystatements)
                        {
                            paragraph = section.AddParagraph() as WParagraph;
                            paragraph.AppendText("Explanatory Statement pursuant to Section 102 (1) of the companies act, 2013");
                            paragraph.ApplyStyle(BuiltinStyle.Heading1);

                            paragraph = section.AddParagraph() as WParagraph;
                            paragraph.AppendText("The following statement sets out all material facts relating to special business mentioned in the notice:");

                            var SrNo = 0;
                            string resolutionType = "";
                            foreach (var item in agenda.lstAgendaItems)
                            {
                                SrNo++;
                                if (!string.IsNullOrEmpty(item.ExplanatoryStatement))
                                {
                                    resolutionType = item.IsSpecialResolution ? "Special resolution" : "Ordinary resolution";
                                    string AgendaItem = SrNo + ". " + (string.IsNullOrEmpty(item.AgendaItemText) ? "" : item.AgendaItemText.ToUpper());
                                    paragraph = section.AddParagraph() as WParagraph;
                                    paragraph.AppendText(AgendaItem);
                                    paragraph.ApplyStyle(BuiltinStyle.Heading2);

                                    paragraph = section.AddParagraph() as WParagraph;
                                    var htmlstring = item.ExplanatoryStatement == null ? " " : item.ExplanatoryStatement;
                                    htmlDocument = htmlProvider.Import(htmlstring);
                                    htmlstring = htmlProvider.Export(htmlDocument);

                                    var isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                                    if (isValidHtml)
                                    {
                                        paragraph.AppendHTML(htmlstring);
                                    }

                                    htmlstring = "<p></p>" +
                                                        "None of the directors of the Company, Key Managerial Personnel and their relatives is in any way concerned or interested financially or otherwise in the resolution." +
                                                        "<p></p>" +
                                                        "The Board of Directors accordingly recommends the <strong>" + resolutionType + "</strong> set out at ITEM NO. <strong>" + SrNo + "</strong> of the notice for the approval of the members.";
                                    htmlDocument = htmlProvider.Import(htmlstring);
                                    htmlstring = htmlProvider.Export(htmlDocument);

                                    isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                                    if (isValidHtml)
                                    {
                                        paragraph = section.AddParagraph() as WParagraph;
                                        paragraph.AppendHTML(htmlstring);
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region Signing Authority
                    paragraph = section.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;

                    paragraph.AppendText(Environment.NewLine);
                    textRange = paragraph.AppendText("By Order of the Board of Director") as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    textRange = paragraph.AppendText("For " + agenda.EntityName) as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;

                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    paragraph = section.AddParagraph() as WParagraph;
                    if (agenda.MeetingSigningAuthorityForAGM != null)
                    {
                        textRange = paragraph.AppendText(agenda.MeetingSigningAuthorityForAGM.AuthorityName) as WTextRange;
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                        textRange = paragraph.AppendText("(" + agenda.MeetingSigningAuthorityForAGM.Designation + ")") as WTextRange;
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                        if (agenda.MeetingSigningAuthorityForAGM.IsCS == true)
                        {
                            textRange = paragraph.AppendText("Membership No. " + agenda.MeetingSigningAuthorityForAGM.MembershipNo) as WTextRange;
                            textRange.CharacterFormat.FontSize = 12;
                            textRange.CharacterFormat.Bold = true;
                            paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                        }
                        else
                        {
                            textRange = paragraph.AppendText("DIN : " + agenda.MeetingSigningAuthorityForAGM.DIN_PAN) as WTextRange;
                            textRange.CharacterFormat.FontSize = 12;
                            textRange.CharacterFormat.Bold = true;
                            paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                        }
                    }

                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);

                    textRange = paragraph.AppendText("Date") as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);


                    textRange = paragraph.AppendText("Place : " + agenda.EntityCity) as WTextRange;
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    paragraph.AppendBreak(Syncfusion.DocIO.DLS.BreakType.LineBreak);
                    #endregion

                    #region Page Setup
                    if (docSetting != null)
                    {
                        if (docSetting.PageSetting != null)
                        {
                            switch (docSetting.PageSetting.PageSize)
                            {
                                case "A3":
                                    section.PageSetup.PageSize = PageSize.A3;
                                    break;
                                case "A4":
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                                case "A5":
                                    section.PageSetup.PageSize = PageSize.A5;
                                    break;
                                case "A6":
                                    section.PageSetup.PageSize = PageSize.A6;
                                    break;
                                case "Legal":
                                    section.PageSetup.PageSize = PageSize.Legal;
                                    break;
                                case "Letter":
                                    section.PageSetup.PageSize = PageSize.Letter;
                                    break;
                                default:
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                            }
                        }

                        section.PageSetup.Margins.Top = (float)docSetting.PageSetting.MarginTop;
                        section.PageSetup.Margins.Left = (float)docSetting.PageSetting.MarginLeft;
                        section.PageSetup.Margins.Right = (float)docSetting.PageSetting.MarginRight;
                        section.PageSetup.Margins.Bottom = (float)docSetting.PageSetting.MarginBottom;
                    }
                    #endregion
                    #endregion
                }

                #region Page Numbers
                foreach (var s in doc.Sections)
                {
                    IWParagraph paragraphFooter = (s as WSection).HeadersFooters.Footer.AddParagraph();
                    paragraphFooter.ParagraphFormat.HorizontalAlignment = Syncfusion.DocIO.DLS.HorizontalAlignment.Center;
                    var pageNum = paragraphFooter.AppendField("Page", FieldType.FieldPage) as WField;
                    pageNum.CharacterFormat.FontSize = 10;
                    //paragraphFooter = section.AddParagraph();
                }

                #endregion

                #region Document style setting
                if (docSetting != null)
                {
                    if (docSetting.Styles != null)
                    {
                        foreach (var style in docSetting.Styles)
                        {
                            WParagraphStyle docStyle = doc.Styles.FindByName(style.StyleName, Syncfusion.DocIO.DLS.StyleType.ParagraphStyle) as WParagraphStyle;
                            if (docStyle != null)
                            {
                                if (!string.IsNullOrEmpty(style.FontName))
                                {
                                    docStyle.CharacterFormat.FontName = style.FontName;
                                }

                                float fontSize = 10;
                                if (!float.TryParse(style.FontSize, out fontSize))
                                {
                                    fontSize = 10;
                                }

                                if (fontSize > 0)
                                {
                                    docStyle.CharacterFormat.FontSize = fontSize;
                                }

                                //docStyle.CharacterFormat.TextColor = Color.FromRgb(20, 50,230);

                                docStyle.CharacterFormat.Bold = style.IsBold;
                                docStyle.CharacterFormat.Italic = style.IsItalic;
                                if (style.IsUnderline)
                                {
                                    docStyle.CharacterFormat.UnderlineStyle = UnderlineStyle.Single;
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Update table of content
                //Commented on 22 Oct 2021
                //if (agenda.MeetingCircular == SecretarialConst.MeetingCircular.MEETING && toc != null)
                if (toc != null)
                {
                    toc.IncludePageNumbers = true;
                    toc.RightAlignPageNumbers = true;
                    toc.UseHyperlinks = true;
                    toc.LowerHeadingLevel = Convert.ToInt32(1);
                    toc.UpperHeadingLevel = Convert.ToInt32(3);
                    toc.UseOutlineLevels = true;
                    toc.UseTableEntryFields = true;

                    #region TOC Styles
                    //Style MyStyleTOC1 = (WParagraphStyle)doc.AddParagraphStyle("MyStyleTOC1");
                    //MyStyleTOC1.CharacterFormat.Bold = true;
                    //MyStyleTOC1.CharacterFormat.FontName = defaultParagraphFont;
                    //MyStyleTOC1.CharacterFormat.FontSize = 16;
                    //toc.SetTOCLevelStyle(1, MyStyleTOC1.Name);

                    //Style MyStyleTOC2 = (WParagraphStyle)doc.AddParagraphStyle("MyStyleTOC2");
                    //MyStyleTOC2.CharacterFormat.Bold = true;
                    //MyStyleTOC2.CharacterFormat.FontName = defaultParagraphFont;
                    //MyStyleTOC2.CharacterFormat.FontSize = 14;
                    //toc.SetTOCLevelStyle(2, MyStyleTOC2.Name);
                    #endregion

                    doc.UpdateTableOfContents();
                    IncreaseSpacingsInTOC(toc, 10, 10);
                }
                #endregion

                try
                {
                    //doccontrol.DocumentSource = new DocumentSource(di, bytes);
                    //doc.Save("Sample.docx", FormatType.Docx, Response, HttpContentDisposition.Attachment);

                    //if(generatePDF)
                    //{
                    #region save temp File
                    string Folder = "~/TempFiles";
                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                    string DateFolder = Folder + "/" + File;
                    System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                    if (!System.IO.Directory.Exists(DateFolder))
                    {
                        System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                    }

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                    string User = userId + "" + customerId + "" + FileDate;
                    //string FileName = DateFolder + "/" + User + ".docx";
                    //doc.Save(System.Web.Hosting.HostingEnvironment.MapPath(FileName), FormatType.Docx);
                    string FileName = DateFolder + "/" + User + ".pdf";
                    //doc.Save(System.Web.Hosting.HostingEnvironment.MapPath(FileName), FormatType.Docx);

                    #region Generate PDF
                    //Creates an instance of the DocToPDFConverter - responsible for Word to PDF conversion
                    DocToPDFConverter converter = new DocToPDFConverter();

                    //Added on 16 Nov 2021
                    converter.Settings.EnableFastRendering = true;

                    //Sets ExportBookmarks for preserving Word document headings as PDF bookmarks
                    converter.Settings.ExportBookmarks = Syncfusion.DocIO.ExportBookmarkType.Headings;
                    //Converts Word document into PDF document

                    //Sets true to enable the fast rendering using direct PDF conversion.
                    //converter.Settings.EnableFastRendering = true;

                    Syncfusion.Pdf.PdfDocument pdfDocument = converter.ConvertToPDF(doc);
                    //Saves the PDF file to file system
                    pdfDocument.Save(System.Web.Hosting.HostingEnvironment.MapPath(FileName));

                    //Closes the instance of document objects
                    pdfDocument.Close(true);

                    ////doc.Close();
                    //doccontrol.Document = FileName;
                    if (generatePDF)
                    {
                        result.FileName = FileName;
                        result.IsFileGenerated = true;
                    }
                    #endregion

                    #region Save Generated Doc

                    string agendaReviewPath = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + meetingId + "/Agenda";
                    //string agendaReviewFile = agendaReviewPath + "/Agenda.pdf";
                    //string agendaReviewFileDocx = agendaReviewPath + "/Agenda.docx";
                    var guid = Convert.ToString(Guid.NewGuid());
                    string agendaReviewFile = agendaReviewPath + "/Agenda_" + guid + ".pdf";
                    string agendaReviewFileDocx = agendaReviewPath + "/Agenda_" + guid + ".docx";

                    if (taskAssignmentId > 0)
                    {
                        agendaReviewPath = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + meetingId + "/Review/" + taskAssignmentId;
                        agendaReviewFile = agendaReviewPath + "/ReviewAgenda.pdf";
                        agendaReviewFileDocx = agendaReviewPath + "/ReviewAgenda.docx";
                    }

                    if (!System.IO.Directory.Exists(agendaReviewPath))
                    {
                        System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewPath));
                    }

                    //Generate Pdf
                    using (FileStream fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewFile), FileMode.Create, FileAccess.ReadWrite))
                    {
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (true)
                        {
                            bw.Write(CryptographyHandler.AESEncrypt(objIFileData_Service.ReadDocFiles(System.Web.Hosting.HostingEnvironment.MapPath(FileName))));
                        }
                        bw.Close();
                    }

                    //Generate Docx
                    using (FileStream fsDocx = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewFileDocx), FileMode.Create, FileAccess.ReadWrite))
                    {
                        BinaryWriter bwDocx = new BinaryWriter(fsDocx);
                        if (true)
                        {
                            using (var stream = new MemoryStream())
                            {
                                doc.Save(stream, FormatType.Docx);
                                bwDocx.Write(CryptographyHandler.AESEncrypt(stream.ToArray()));
                                if (generatePDF == false)
                                {
                                    result.FileContent = stream.ToArray();
                                }
                            }
                        }
                        bwDocx.Close();
                    }

                    ////Generate Docx
                    //using (FileStream fsDocx = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewFileDocx), FileMode.Create, FileAccess.ReadWrite))
                    //{
                    //    BinaryWriter bwDocx = new BinaryWriter(fsDocx);
                    //    if (true)
                    //    {
                    //        using (var stream = new MemoryStream())
                    //        {
                    //            doc.Save(stream, FormatType.Docx);
                    //            bwDocx.Write(CryptographyHandler.AESEncrypt(stream.ToArray()));
                    //            if (generatePDF == false)
                    //            {
                    //                result.FileContent = stream.ToArray();
                    //            }

                    //            #region Generate PDF
                    //            //Creates an instance of the DocToPDFConverter - responsible for Word to PDF conversion
                    //            DocToPDFConverter converter = new DocToPDFConverter();
                    //            //Sets ExportBookmarks for preserving Word document headings as PDF bookmarks
                    //            converter.Settings.ExportBookmarks = Syncfusion.DocIO.ExportBookmarkType.Headings;
                    //            //Converts Word document into PDF document

                    //            //Sets true to enable the fast rendering using direct PDF conversion.
                    //            //converter.Settings.EnableFastRendering = true;

                    //            using (Syncfusion.Pdf.PdfDocument pdfDocument = converter.ConvertToPDF(new WordDocument(stream, FormatType.Docx)))
                    //            {
                    //                //Saves the PDF file to file system
                    //                pdfDocument.Save(System.Web.Hosting.HostingEnvironment.MapPath(FileName));

                    //                //Closes the instance of document objects
                    //                pdfDocument.Close(true);
                    //            }

                    //            //Generate Pdf
                    //            using (FileStream fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewFile), FileMode.Create, FileAccess.ReadWrite))
                    //            {
                    //                BinaryWriter bw = new BinaryWriter(fs);
                    //                if (true)
                    //                {
                    //                    bw.Write(CryptographyHandler.AESEncrypt(objIFileData_Service.ReadDocFiles(System.Web.Hosting.HostingEnvironment.MapPath(FileName))));
                    //                }
                    //                bw.Close();
                    //            }

                    //            ////doc.Close();
                    //            //doccontrol.Document = FileName;
                    //            if (generatePDF)
                    //            {
                    //                result.FileName = FileName;
                    //                result.IsFileGenerated = true;
                    //            }
                    //            #endregion


                    //        }
                    //    }
                    //    bwDocx.Close();
                    //}

                    if (taskAssignmentId > 0)
                    {
                        using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                        {
                            var agendaFilePath = (from row in entities.BM_TaskAssignment
                                                  where row.ID == taskAssignmentId
                                                  select row).FirstOrDefault();
                            if (agendaFilePath != null)
                            {
                                agendaFilePath.AgendaFilePath = agendaReviewFile;
                                entities.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                        {
                            var agendaFilePath = (from row in entities.BM_Meetings
                                                  where row.MeetingID == meetingId
                                                  select row).FirstOrDefault();
                            if (agendaFilePath != null)
                            {
                                agendaFilePath.AgendaFilePath = agendaReviewFile;
                                agendaFilePath.GenerateAgendaDocument = false;
                                agendaFilePath.GenerateAgendaDocumentOnNotice = false;
                                agendaFilePath.EnType = "A";
                                entities.SaveChanges();

                                entities.BM_MeetingAgendaMinutesComments.Where(k => k.MeetingId == meetingId && k.IsActive == true && k.DocType == "A").ToList().ForEach(k => { k.IsActive = false; k.UpdatedBy = userId; k.UpdatedOn = DateTime.Now; });
                                entities.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #endregion
                    //}
                    //else
                    //{
                    //    MemoryStream stream = new MemoryStream();
                    //    doc.Save(stream, FormatType.Docx);
                    //    result.FileContent = stream.ToArray();
                    //}
                }
                catch (Exception ex)
                {
                    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            #endregion
            return result;
        }

        private Syncfusion.DocIO.DLS.Entity IncreaseSpacingsInTOC(TableOfContent toc, float beforeSpacing, float afterSpacing)
        {
            int tocIndex = toc.OwnerParagraph.Items.IndexOf(toc);
            //TOC may contains nested fields and each fields has its owner field end mark 
            //so to indentify the TOC Field end mark (WFieldMark instance) used the stack.
            Stack<Syncfusion.DocIO.DLS.Entity> fieldStack = new Stack<Syncfusion.DocIO.DLS.Entity>();
            fieldStack.Push(toc);

            //Finds whether TOC end item is exist in same paragraph.
            for (int i = tocIndex + 1; i < toc.OwnerParagraph.Items.Count; i++)
            {
                Syncfusion.DocIO.DLS.Entity item = toc.OwnerParagraph.Items[i];

                if (item is WField)
                    fieldStack.Push(item);
                else if (item is WFieldMark && (item as WFieldMark).Type == FieldMarkType.FieldEnd)
                {
                    if (fieldStack.Count == 1)
                    {
                        fieldStack.Clear();
                        return item;
                    }
                    else
                        fieldStack.Pop();
                }

            }

            return FindLastItemInTextBody(toc, fieldStack, beforeSpacing, afterSpacing);
        }

        private Syncfusion.DocIO.DLS.Entity FindLastItemInTextBody(TableOfContent toc, Stack<Syncfusion.DocIO.DLS.Entity> fieldStack, float beforeSpacing, float afterSpacing)
        {
            WTextBody tBody = toc.OwnerParagraph.OwnerTextBody;

            for (int i = tBody.ChildEntities.IndexOf(toc.OwnerParagraph) + 1; i < tBody.ChildEntities.Count; i++)
            {
                WParagraph paragraph = tBody.ChildEntities[i] as WParagraph;

                //Set after spacings for paragraphs inside TOC
                paragraph.ParagraphFormat.AfterSpacing = afterSpacing;
                //Set before spacings for paragraphs inside TOC
                paragraph.ParagraphFormat.BeforeSpacing = beforeSpacing;

                foreach (Syncfusion.DocIO.DLS.Entity item in paragraph.Items)
                {
                    if (item is WField)
                        fieldStack.Push(item);
                    else if (item is WFieldMark && (item as WFieldMark).Type == FieldMarkType.FieldEnd)
                    {
                        if (fieldStack.Count == 1)
                        {
                            fieldStack.Clear();
                            return item;
                        }
                        else
                            fieldStack.Pop();
                    }
                }
            }
            return null;
        }

        private string GetSuffix(int day)
        {
            string suffix = "th";

            if (day < 11 || day > 20)
            {
                day = day % 10;
                switch (day)
                {
                    case 1:
                        suffix = "st";
                        break;
                    case 2:
                        suffix = "nd";
                        break;
                    case 3:
                        suffix = "rd";
                        break;
                }
            }

            return suffix;
        }

        public string GetMinutesDocumentFile(long meetingId, long? taskAssignmentId, int userId, int customerId, bool forceGenerateNewDocument, bool generatePDF, string generateFor)
        {
            var result = GenerateMinutesDocument(meetingId, taskAssignmentId, userId, customerId, forceGenerateNewDocument, generatePDF, generateFor);
            return result.FileName;
        }
        public byte[] GetMinutesDocumentData(long meetingId, long? taskAssignmentId, int userId, int customerId, bool forceGenerateNewDocument, bool generatePDF, string generateFor)
        {
            var result = GenerateMinutesDocument(meetingId, taskAssignmentId, userId, customerId, forceGenerateNewDocument, generatePDF, generateFor);
            return result.FileContent;
        }
        public DocumentVM GenerateMinutesDocument(long meetingId, long? taskAssignmentId, int userId, int customerId, bool forceGenerateNewDocument, bool generatePDF, string generateFor)
        {
            var result = new DocumentVM();
            #region Generate Minutes Document
            using (WordDocument doc = new WordDocument())
            {
                var docSetting = objIDocumentSetting_Service.GetDocumentSetting(customerId, SecretarialConst.MeetingDocumentTypes.MINUTES);
                var defaultParagraphFont = "Bookman Old Style";
                doc.EnsureMinimal();

                WParagraphStyle myParagraphStyle = (WParagraphStyle)doc.AddParagraphStyle("MyParagraphStyle");
                myParagraphStyle.CharacterFormat.FontName = "Bookman Old Style";
                myParagraphStyle.CharacterFormat.FontSize = 12;
                var objStyleVM = new VM.StyleVM();
                if (docSetting != null)
                {
                    if (docSetting.Styles != null)
                    {
                        objStyleVM = docSetting.Styles.Where(k => k.StyleName == "Paragraph").Select(k => k).FirstOrDefault();
                        if (objStyleVM != null)
                        {
                            myParagraphStyle.CharacterFormat.FontName = objStyleVM.FontName;
                            defaultParagraphFont = objStyleVM.FontName;
                            float fontSize = 12;
                            if (!float.TryParse(objStyleVM.FontSize, out fontSize))
                            {
                                fontSize = 12;
                            }
                            myParagraphStyle.CharacterFormat.FontSize = fontSize;
                        }
                    }
                }

                #region Meeting & Agenda details
                var agenda = objIAgendaMinutesReviewService.PreviewMOM(meetingId, customerId);
                if (agenda != null)
                {
                    agenda.lstMeetingAttendance = objIAgendaMinutesReviewService.GetPerticipenforAttendenceCS(meetingId, customerId, null);
                    agenda.lstOtherParticipantAttendance = objIAgendaMinutesReviewService.GetOtherParticipentAttendence(meetingId);
                }
                var meetingSrNo = "";
                var meetingTypeName = "";
                if (agenda.MeetingSrNo.HasValue)
                {
                    meetingSrNo = agenda.MeetingSrNo + GetSuffix(Convert.ToInt32(agenda.MeetingSrNo));
                }

                if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                {
                    meetingTypeName = "Annual General Meeting";
                }
                else if (agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                {
                    meetingTypeName = "Extra Ordinary General Meeting";
                }

                string mDate = string.Empty;
                string mDay = string.Empty;
                string mTime = string.Empty;
                string mVenue = string.Empty;
                string cDate = string.Empty;

                if (agenda.MeetingDate != null)
                {
                    var date = Convert.ToDateTime(agenda.MeetingDate);
                    mDate = date.ToString("MMMM dd yyyy");
                    mDay = date.ToString("dddd");
                }
                if (agenda.CircularDate != null)
                {
                    cDate = Convert.ToDateTime(agenda.CircularDate).ToString("MMMM dd, yyyy");
                }
                if (!string.IsNullOrEmpty(agenda.MeetingTime))
                {
                    mTime = agenda.MeetingTime;
                }
                if (!string.IsNullOrEmpty(agenda.MeetingVenue))
                {
                    mVenue = agenda.MeetingVenue;
                }
                #endregion

                Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlFormatProvider htmlProvider = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlFormatProvider();
                Telerik.Windows.Documents.Flow.Model.RadFlowDocument htmlDocument = new Telerik.Windows.Documents.Flow.Model.RadFlowDocument();

                if (agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.AGM && agenda.MeetingTypeId != SecretarialConst.MeetingTypeID.EGM)
                {
                    #region Header
                    var meetingTitle = "MINUTES OF THE " + meetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "") + " MEETING OF THE " + agenda.MeetingTypeName.ToUpper() + " OF " + agenda.EntityName.ToUpper() + " HELD ON " + mDay.ToUpper()+", "+ mDate.ToUpper() + ", AT " + mTime.ToUpper() + " AT " + mVenue.ToUpper();
                    WParagraph paragraph = doc.LastSection.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.Borders.Bottom.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Single;
                    paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    paragraph.ApplyStyle(BuiltinStyle.Heading1);
                    var headerText = paragraph.AppendText(meetingTitle) as WTextRange;
                    headerText.CharacterFormat.FontSize = 14;
                    headerText.CharacterFormat.Bold = true;

                    //paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    #endregion

                    #region Attendance
                    if (agenda.lstMeetingAttendance.Where(k => (k.Attendance == "P" || k.Attendance == "VC") && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER)).Count() > 0)
                    {
                        var newPara = doc.LastSection.AddParagraph() as WParagraph;
                        newPara = doc.LastSection.AddParagraph() as WParagraph;
                        var t = newPara.AppendText("PRESENT") as WTextRange;
                        t.CharacterFormat.Bold = true;
                        newPara.AppendText(Environment.NewLine);

                        foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "P" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER)))
                        {
                            t = newPara.AppendText(item.MeetingParticipant_Name + " : " + item.Designation) as WTextRange;
                            newPara.AppendText(Environment.NewLine);
                        }

                        if(agenda.lstMeetingAttendance.Where(k => k.Attendance == "VC" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER)).Count() > 0)
                        {
                            newPara.AppendText(Environment.NewLine);
                            t = newPara.AppendText("Present through VC/OAVM") as WTextRange;
                            t.CharacterFormat.Bold = true;
                            newPara.AppendText(Environment.NewLine);

                            foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "VC" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER)))
                            {
                                t = newPara.AppendText(item.MeetingParticipant_Name + " : " + item.Designation) as WTextRange;
                                newPara.AppendText(Environment.NewLine);
                            }
                        }

                        if (agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.INVITEE).Count() > 0)
                        {
                            newPara = doc.LastSection.AddParagraph() as WParagraph;
                            t = newPara.AppendText("Special Invitees") as WTextRange;
                            //t.CharacterFormat.FontSize = 15;
                            t.CharacterFormat.Bold = true;
                            newPara.AppendText(Environment.NewLine);
                            foreach (var item in agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.INVITEE))
                            {
                                t = newPara.AppendText(item.MeetingParticipant_Name + " " + item.Designation) as WTextRange;
                                //t.CharacterFormat.FontSize = 15;
                                newPara.AppendText(Environment.NewLine);
                            }
                        }

                        newPara = doc.LastSection.AddParagraph() as WParagraph;
                        t = newPara.AppendText("The meeting commenced at " + agenda.MeetingStartTime) as WTextRange;
                        newPara = doc.LastSection.AddParagraph() as WParagraph;

                        if (agenda.lstMeetingAttendance.Where(k => (k.Attendance == "P" || k.Attendance == "VC") && k.Position == 1 && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR).Count() > 0)
                        {
                            var chairman = agenda.lstMeetingAttendance.Where(k => (k.Attendance == "P" || k.Attendance == "VC") && k.Position == 1).FirstOrDefault();
                            if (chairman != null)
                            {
                                newPara = doc.LastSection.AddParagraph() as WParagraph;
                                t = newPara.AppendText("Chairman") as WTextRange;
                                t.CharacterFormat.Bold = true;

                                newPara.AppendText(Environment.NewLine);

                                t = newPara.AppendText(chairman.MeetingParticipant_Name) as WTextRange;
                                t.CharacterFormat.Bold = true;
                                t = newPara.AppendText(" presided as the Chairman of the meeting.") as WTextRange;
                            }
                        }
                    }
                    #endregion

                    #region Agenda Items
                    bool anyOtherBusinessFlag = false;
                    var section = doc.LastSection as WSection;
                    if (agenda.lstAgendaItems != null)
                    {
                        var SrNo = 0;
                        foreach (var item in agenda.lstAgendaItems)
                        {
                            SrNo++;
                            WParagraph newPara = section.AddParagraph() as WParagraph;
                            if (item.PartId == 3)
                            {
                                if (anyOtherBusinessFlag == false)
                                {
                                    newPara.AppendText("Any other business item");
                                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                                    newPara.ApplyStyle(BuiltinStyle.Heading2);

                                    newPara = section.AddParagraph() as WParagraph;
                                    var anyOtherText = "The Chairman informed the board with the consultation of majority of members present at the meeting to consider the following agenda";
                                    newPara.AppendText(anyOtherText);

                                    newPara = section.AddParagraph() as WParagraph;

                                    anyOtherBusinessFlag = true;
                                }
                            }


                            string htmlstring = SrNo + ". " + item.AgendaItemText;
                            htmlstring = htmlstring.Replace("<B>", "<b>");
                            htmlstring = htmlstring.Replace("</B>", "</b>");

                            bool isValidHtml = false;
                            newPara.AppendHTML(htmlstring);
                            newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                            newPara.ApplyStyle(BuiltinStyle.Heading2);

                            #region Convert html format in XHTML 1.0
                            htmlDocument = htmlProvider.Import(string.IsNullOrEmpty(item.AgendaFormat) ? "" : item.AgendaFormat);
                            Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings exportSettings = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings();

                            htmlstring = htmlProvider.Export(htmlDocument);
                            #endregion

                            isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                            if (isValidHtml)
                            {
                                newPara = section.AddParagraph() as WParagraph;
                                newPara.AppendHTML(htmlstring);
                            }
                        }
                    }
                    #endregion

                    #region Vote of Thanks
                    var voteForThanks = "Votes of Thanks";

                    paragraph = section.AddParagraph() as WParagraph;
                    paragraph.AppendText(voteForThanks);
                    paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                    paragraph.ApplyStyle(BuiltinStyle.Heading2);


                    var voteForThanksBody = "There being no other matter to be transacted, the meeting concluded at " + agenda.MeetingEndTime + ". with a vote of thanks to the Chair.";
                    paragraph = section.AddParagraph() as WParagraph;
                    paragraph.AppendText(voteForThanksBody);

                    if (generateFor != "Draft Minutes")
                    {
                        var minutesDetails = objIAgendaMinutesReviewService.GetMinutesDetails(meetingId);

                        if (minutesDetails != null)
                        {
                            #region Table
                            paragraph = doc.LastSection.AddParagraph() as WParagraph;
                            IWTable table = doc.LastSection.AddTable();
                            table.TableFormat.HorizontalAlignment = RowAlignment.Center;
                            table.TableFormat.Borders.BorderType = BorderStyle.None;
                            table.ResetCells(3, 2);

                            float tableWidth = doc.LastSection.PageSetup.PageSize.Width - (doc.LastSection.PageSetup.Margins.Left + doc.LastSection.PageSetup.Margins.Right) - 100;
                            float individualCellWidth = tableWidth / 3;

                            for (int i = 0; i < 3; i++)
                            {
                                table.Rows[i].Cells[0].Width = individualCellWidth * 2;
                                table.Rows[i].Cells[1].Width = individualCellWidth;
                            }

                            WTextRange cellRun = new WTextRange(doc);


                            if (string.IsNullOrEmpty(minutesDetails.Place))
                            {
                                cellRun = table.Rows[0].Cells[0].AddParagraph().AppendText("Place") as WTextRange;
                                cellRun.CharacterFormat.FontSize = 12;
                            }
                            else
                            {
                                cellRun = table.Rows[0].Cells[0].AddParagraph().AppendText("Place : " + minutesDetails.Place) as WTextRange;
                                cellRun.CharacterFormat.FontSize = 12;
                            }

                            cellRun = table.Rows[2].Cells[1].AddParagraph().AppendText("Chairman") as WTextRange;
                            cellRun.OwnerParagraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                            cellRun.CharacterFormat.Bold = true;
                            cellRun.CharacterFormat.FontSize = 12;

                            if (minutesDetails.DateOfEntering != null)
                            {
                                cellRun = table.Rows[1].Cells[0].AddParagraph().AppendText("Date of entering minutes : " + Convert.ToDateTime(minutesDetails.DateOfEntering).ToString("MMM dd, yyyy")) as WTextRange;
                                cellRun.CharacterFormat.FontSize = 12;
                            }
                            else
                            {
                                cellRun = table.Rows[1].Cells[0].AddParagraph().AppendText("Date of entering minutes") as WTextRange;
                                cellRun.CharacterFormat.FontSize = 12;
                            }

                            if (minutesDetails.DateOfSigning != null)
                            {
                                cellRun = table.Rows[2].Cells[0].AddParagraph().AppendText("Date of signing minutes : " + Convert.ToDateTime(minutesDetails.DateOfSigning).ToString("MMM dd, yyyy")) as WTextRange;
                                cellRun.CharacterFormat.FontSize = 12;
                            }
                            else
                            {
                                cellRun = table.Rows[2].Cells[0].AddParagraph().AppendText("Date of signing minutes") as WTextRange;
                                cellRun.CharacterFormat.FontSize = 12;
                            }
                            #endregion
                        }
                    }
                    #endregion

                    #region Page Setup
                    if (docSetting != null)
                    {
                        if (docSetting.PageSetting != null)
                        {
                            switch (docSetting.PageSetting.PageSize)
                            {
                                case "A3":
                                    section.PageSetup.PageSize = PageSize.A3;
                                    break;
                                case "A4":
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                                case "A5":
                                    section.PageSetup.PageSize = PageSize.A5;
                                    break;
                                case "A6":
                                    section.PageSetup.PageSize = PageSize.A6;
                                    break;
                                case "Legal":
                                    section.PageSetup.PageSize = PageSize.Legal;
                                    break;
                                case "Letter":
                                    section.PageSetup.PageSize = PageSize.Letter;
                                    break;
                                default:
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                            }
                        }

                        section.PageSetup.Margins.Top = (float)docSetting.PageSetting.MarginTop;
                        section.PageSetup.Margins.Left = (float)docSetting.PageSetting.MarginLeft;
                        section.PageSetup.Margins.Right = (float)docSetting.PageSetting.MarginRight;
                        section.PageSetup.Margins.Bottom = (float)docSetting.PageSetting.MarginBottom;
                    }
                    #endregion
                }
                else
                {
                    #region Header
                    var chairman = new MeetingAttendance_VM();

                    var meetingTitle = "MINUTES OF THE " + meetingSrNo + (agenda.IsAdjourned == true ? " (Adjourned)" : "") + " " + meetingTypeName.ToUpper() + " OF THE MEMBERS OF " + agenda.EntityName.ToUpper() + " HELD ON " + mDay.ToUpper() + ", " + mDate.ToUpper() + ", AT " + mTime.ToUpper() + " AT " + mVenue.ToUpper();
                    WParagraph paragraph = doc.LastSection.AddParagraph() as WParagraph;
                    paragraph.ParagraphFormat.Borders.Bottom.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Single;
                    paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    paragraph.ApplyStyle(BuiltinStyle.Heading1);
                    var headerText = paragraph.AppendText(meetingTitle) as WTextRange;
                    headerText.CharacterFormat.FontSize = 14;
                    headerText.CharacterFormat.Bold = true;
                    //paragraph.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    #endregion

                    #region Attendance
                    var newPara = doc.LastSection.AddParagraph() as WParagraph;
                    var isLeave = false;
                    if (agenda.lstMeetingAttendance.Where(k => (k.Attendance == "P" || k.Attendance == "VC")).Count() > 0)
                    {
                        chairman = agenda.lstMeetingAttendance.Where(k => (k.Attendance == "P" || k.Attendance == "VC") && k.Position == 1 && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR).FirstOrDefault();

                        newPara = doc.LastSection.AddParagraph() as WParagraph;
                        var t = newPara.AppendText("MEMBER’S PRESENT") as WTextRange;
                        t.CharacterFormat.Bold = true;
                        newPara.AppendText(Environment.NewLine);

                        t = newPara.AppendText(" " + (agenda.TotalMemberPresentInAGM != null ? Convert.ToString(agenda.TotalMemberPresentInAGM) : "{{TOTAL_NUMBER_OF_MEMBERS_PRESENT}}") + " members were present in person or by proxy.") as WTextRange;
                        newPara.AppendText(Environment.NewLine);

                        newPara = doc.LastSection.AddParagraph() as WParagraph;
                        t = newPara.AppendText("DIRECTOR’S PRESENT") as WTextRange;
                        t.CharacterFormat.Bold = true;
                        newPara.AppendText(Environment.NewLine);

                        foreach (var item in agenda.lstMeetingAttendance.Where(k => (k.Attendance == "P" || k.Attendance == "VC") && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER)))
                        {
                            t = newPara.AppendText(item.MeetingParticipant_Name + " : " + item.Designation) as WTextRange;
                            newPara.AppendText(Environment.NewLine);
                        }


                        if (agenda.lstOtherParticipantAttendance != null)
                        {
                            if (agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP).Count() > 0)
                            {
                                newPara = doc.LastSection.AddParagraph() as WParagraph;
                                t = newPara.AppendText("OFFICERS IN PRESENCE") as WTextRange;
                                t.CharacterFormat.Bold = true;
                                newPara.AppendText(Environment.NewLine);

                                foreach (var item in agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP))
                                {
                                    t = newPara.AppendText(item.MeetingParticipant_Name + " : " + item.Designation) as WTextRange;
                                    newPara.AppendText(Environment.NewLine);
                                }
                            }

                            if (agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.AUDITOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.INVITEE)).Count() > 0)
                            {
                                newPara = doc.LastSection.AddParagraph() as WParagraph;
                                t = newPara.AppendText("SPECIAL INVITEES") as WTextRange;
                                t.CharacterFormat.Bold = true;
                                newPara.AppendText(Environment.NewLine);

                                foreach (var item in agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "P" && (k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.AUDITOR || k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.INVITEE)))
                                {
                                    if (string.IsNullOrEmpty(item.Representative))
                                    {
                                        t = newPara.AppendText(item.MeetingParticipant_Name + " " + item.Designation) as WTextRange;
                                        newPara.AppendText(Environment.NewLine);
                                    }
                                    else
                                    {
                                        t = newPara.AppendText(item.Representative + " representing " + item.MeetingParticipant_Name + ", the Company’s " + item.Designation) as WTextRange;
                                        newPara.AppendText(Environment.NewLine);
                                    }
                                }
                            }

                            if (agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "A" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP).Count() > 0)
                            {
                                isLeave = true;
                            }
                        }

                        #region Leave of absens
                        if (agenda.lstMeetingAttendance.Where(k => k.Attendance == "A" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR).Count() > 0)
                        {
                            isLeave = true;
                        }

                        if (isLeave)
                        {
                            newPara = doc.LastSection.AddParagraph() as WParagraph;
                            t = newPara.AppendText("Leave of Absences") as WTextRange;
                            newPara.AppendText(Environment.NewLine);
                            t.CharacterFormat.Bold = true;

                            t = newPara.AppendText("Leave of absence was granted to") as WTextRange;
                            newPara.AppendText(Environment.NewLine);
                            t.CharacterFormat.Bold = true;

                            foreach (var item in agenda.lstMeetingAttendance.Where(k => k.Attendance == "A" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR))
                            {
                                t = newPara.AppendText(item.MeetingParticipant_Name + " " + item.Designation) as WTextRange;
                                newPara.AppendText(Environment.NewLine);
                            }

                            if (agenda.lstOtherParticipantAttendance != null)
                            {
                                foreach (var item in agenda.lstOtherParticipantAttendance.Where(k => k.Attendance == "A" && k.ParticipantType == SecretarialConst.MeetingParticipantsTypes.KMP))
                                {
                                    t = newPara.AppendText(item.MeetingParticipant_Name + " " + item.Designation) as WTextRange;
                                    newPara.AppendText(Environment.NewLine);
                                }
                            }
                        }
                        #endregion

                        if (chairman != null)
                        {
                            newPara = doc.LastSection.AddParagraph() as WParagraph;
                            t = newPara.AppendText("CHAIRMAN") as WTextRange;
                            t.CharacterFormat.Bold = true;

                            newPara.AppendText(Environment.NewLine);
                            t = newPara.AppendText(chairman.MeetingParticipant_Name + ", in his capacity as the chairman of the board of directors of the company, occupied the chair and presided over the meeting.") as WTextRange;
                            newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                            newPara.AppendText(Environment.NewLine);
                        }

                    }
                    #endregion

                    #region Register. Documents & Report
                    newPara = doc.LastSection.AddParagraph() as WParagraph;

                    var txtRegister = newPara.AppendText("REGISTER, DOCUMENTS, REPORTS") as WTextRange;
                    txtRegister.CharacterFormat.Bold = true;
                    newPara.AppendText(Environment.NewLine);

                    if(agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                    {
                        newPara.AppendText("The Chairman informed the Members that the following documents and registers as required under the Companies Act, 2013 and other applicable laws were open for inspection by the Members at the Meeting:");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("1. Notice convening the " + meetingSrNo + " " + meetingTypeName);
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("2. Directors’ Report of the company for the financial year " + agenda.FYText + " along with its annexures. ");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("3. Audited Financial Statements(Standalone) " + (agenda.HasConsolidatedFinancial ? "and Audited Consolidated Financial Statements " : "") + "for the financial year " + agenda.FYText + " along with respective Auditor’s Reports.");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("4. Secretarial Audit Report for the financial year " + agenda.FYText);
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("5. The Register of Proxies with the proxies lodged with the Company in connection with " + meetingSrNo + " " + meetingTypeName);
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("6. Register of Directors and Key Managerial Personnel and their Shareholding.");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("7. Register of Contracts or Arrangements in which the Directors were interested.");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                    }
                    else
                    {
                        newPara.AppendText("The Chairman informed the Members that the following documents and registers as required under the Companies Act, 2013 and other applicable laws were open for inspection by the Members at the Meeting:");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("1. Notice convening the " + meetingSrNo + " " + meetingTypeName);
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("2. The Register of Proxies with the proxies lodged with the Company in connection with " + meetingSrNo + " " + meetingTypeName);
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("3. Register of Directors and Key Managerial Personnel and their Shareholding.");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                        newPara.AppendText("4. Register of Contracts or Arrangements in which the Directors were interested.");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                    }
                    #endregion

                    #region  Quorum
                    newPara = doc.LastSection.AddParagraph() as WParagraph;

                    var txtQuorum = newPara.AppendText("Quorum") as WTextRange;
                    txtQuorum.CharacterFormat.Bold = true;
                    newPara.AppendText(Environment.NewLine);
                    newPara.AppendText("At " + agenda.MeetingStartTime + ", the Chairman welcomed the Members present at the venue of the "+ meetingTypeName+". Thereafter, the Chairman announced that the requisite quorum was present and called the Meeting to order. ");
                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    newPara.AppendText(Environment.NewLine);

                    if(agenda.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM)
                    {
                        newPara = doc.LastSection.AddParagraph() as WParagraph;
                        newPara.AppendText("The Chairman requested the members present to take the notice convening the "+ meetingTypeName + ", the auditor’s Report and the annexure thereto as read, to which the members agreed. As there were, no qualifications in the auditor’s report as also in Secretarial Audit report, the same were not required to read.");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                    }
                    else
                    {
                        newPara = doc.LastSection.AddParagraph() as WParagraph;
                        newPara.AppendText("The Chairman requested the members present to take the notice convening the "+ meetingTypeName + ", to which the members agreed.");
                        newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                        newPara.AppendText(Environment.NewLine);
                    }
                    
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    newPara.AppendText("With the permission of the members, the Chairman then commenced reading out the summary of the resolution to provide sufficient opportunity to the members to propose and second the same and vote on them. He drew attention of members to the Explanatory Statement forming part of Notice convening the " + meetingTypeName + ", which explained the objectives and implications of the resolutions.");
                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    #endregion

                    #region Agenda Items
                    var section = doc.LastSection as WSection;
                    if (agenda.lstAgendaItems != null)
                    {
                        var SrNo = 0;
                        foreach (var item in agenda.lstAgendaItems)
                        {
                            SrNo++;
                            newPara = doc.LastSection.AddParagraph() as WParagraph;
                            newPara.AppendText("ITEM NO. ");
                            var agendaText = newPara.AppendText(SrNo + "") as WTextRange;
                            agendaText.CharacterFormat.Bold = true;

                            newPara = doc.LastSection.AddParagraph() as WParagraph;
                            //newPara.AppendText(item.AgendaItemText);
                            var htmlstring = item.AgendaItemText.Replace("<B>", "<b>");
                            htmlstring = htmlstring.Replace("</B>", "</b>");
                            newPara.AppendHTML(htmlstring);
                            newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                            newPara.ApplyStyle(BuiltinStyle.Heading2);

                            bool isValidHtml = false;
                            #region Convert html format in XHTML 1.0
                            htmlDocument = htmlProvider.Import(string.IsNullOrEmpty(item.AgendaFormat) ? "" : item.AgendaFormat);
                            Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings exportSettings = new Telerik.Windows.Documents.Flow.FormatProviders.Html.HtmlExportSettings();

                            htmlstring = htmlProvider.Export(htmlDocument);
                            isValidHtml = section.Body.IsValidXHTML(htmlstring, XHTMLValidationType.None);
                            if (isValidHtml)
                            {
                                newPara = section.AddParagraph() as WParagraph;
                                newPara.AppendHTML(htmlstring);
                            }

                            #endregion
                        }
                    }
                    #endregion

                    #region Invited Questions
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    var invitedQuestions = "The chairman then invited questions from the members. The following members offered comments/raised queries.";

                    newPara.AppendText(invitedQuestions);
                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;

                    var invitedQuestionsBody = "The Chairman thanked the members for all questions and concerned raised.";
                    newPara.AppendText(invitedQuestionsBody);
                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;

                    invitedQuestionsBody = "The chairman then authorized the company secretary to carry out the voting process.";
                    if (agenda.IsEVoting)
                    {
                        invitedQuestionsBody += Environment.NewLine + "He further stated that the combined results of the remote e-voting and the voting at the meeting would be announced by Company Secretary and the same would be displayed on the Company’s website, the website of NSDL/CDSL, will be uploaded on the stock exchanges in within two days.";
                    }
                    newPara.AppendText(invitedQuestionsBody);
                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;


                    invitedQuestionsBody = "The Chairman thanked the members and extended his good wishes for supporting the company.";
                    newPara.AppendText(invitedQuestionsBody);
                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;


                    var voteForThanks = "Votes of Thanks";

                    //paragraph = section.AddParagraph() as WParagraph;
                    var voteTextRange = newPara.AppendText(voteForThanks) as WTextRange;
                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                    voteTextRange.CharacterFormat.FontSize = 14;
                    voteTextRange.CharacterFormat.Bold = true;

                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    invitedQuestionsBody = "On behalf of the members, one of them proposed a vote of thanks to the chair.";
                    newPara.AppendText(invitedQuestionsBody);
                    newPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Justify;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    newPara = doc.LastSection.AddParagraph() as WParagraph;
                    #endregion

                    #region Table
                    paragraph = doc.LastSection.AddParagraph() as WParagraph;
                    IWTable table = doc.LastSection.AddTable();
                    table.TableFormat.HorizontalAlignment = RowAlignment.Center;
                    table.TableFormat.Borders.BorderType = BorderStyle.None;
                    table.ResetCells(3, 2);

                    float tableWidth = doc.LastSection.PageSetup.PageSize.Width - (doc.LastSection.PageSetup.Margins.Left + doc.LastSection.PageSetup.Margins.Right) - 100;

                    float individualCellWidth = tableWidth / 2;

                    for (int i = 0; i < 3; i++)
                    {
                        table.Rows[i].Cells[0].Width = individualCellWidth;
                        table.Rows[i].Cells[1].Width = individualCellWidth;
                    }

                    var isTableAdded = false;
                    if (generateFor != "Draft Minutes")
                    {
                        var minutesDetails = objIAgendaMinutesReviewService.GetMinutesDetails(meetingId);

                        if (minutesDetails != null)
                        {
                            #region Table
                            WTextRange cellRun = new WTextRange(doc);
                            cellRun = table.Rows[0].Cells[0].AddParagraph().AppendText("DATE: " + (minutesDetails.DateOfSigning != null ? Convert.ToDateTime(minutesDetails.DateOfSigning).ToString("dd / MM / yyyy") : "")) as WTextRange;
                            cellRun = table.Rows[1].Cells[0].AddParagraph().AppendText((chairman != null ? chairman.MeetingParticipant_Name : "{{NAME_OF_CHAIRMAN}}")) as WTextRange;
                            cellRun = table.Rows[2].Cells[0].AddParagraph().AppendText("PLACE : " + (String.IsNullOrEmpty(minutesDetails.Place) ? "" : minutesDetails.Place)) as WTextRange;
                            cellRun = table.Rows[1].Cells[1].AddParagraph().AppendText("Chairman") as WTextRange;
                            isTableAdded = true;
                            #endregion
                        }
                    }

                    if (!isTableAdded)
                    {
                        table.Rows[0].Cells[0].AddParagraph().AppendText("DATE: ");
                        table.Rows[1].Cells[0].AddParagraph().AppendText((chairman != null ? chairman.MeetingParticipant_Name : "{{NAME_OF_CHAIRMAN}}"));
                        table.Rows[2].Cells[0].AddParagraph().AppendText("PLACE : ");
                        table.Rows[1].Cells[1].AddParagraph().AppendText("Chairman");
                    }
                    #endregion

                    #region Page Setup
                    if (docSetting != null)
                    {
                        if (docSetting.PageSetting != null)
                        {
                            switch (docSetting.PageSetting.PageSize)
                            {
                                case "A3":
                                    section.PageSetup.PageSize = PageSize.A3;
                                    break;
                                case "A4":
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                                case "A5":
                                    section.PageSetup.PageSize = PageSize.A5;
                                    break;
                                case "A6":
                                    section.PageSetup.PageSize = PageSize.A6;
                                    break;
                                case "Legal":
                                    section.PageSetup.PageSize = PageSize.Legal;
                                    break;
                                case "Letter":
                                    section.PageSetup.PageSize = PageSize.Letter;
                                    break;
                                default:
                                    section.PageSetup.PageSize = PageSize.A4;
                                    break;
                            }
                        }

                        section.PageSetup.Margins.Top = (float)docSetting.PageSetting.MarginTop;
                        section.PageSetup.Margins.Left = (float)docSetting.PageSetting.MarginLeft;
                        section.PageSetup.Margins.Right = (float)docSetting.PageSetting.MarginRight;
                        section.PageSetup.Margins.Bottom = (float)docSetting.PageSetting.MarginBottom;
                    }
                    #endregion
                }

                #region Document style setting
                if (docSetting != null)
                {
                    if (docSetting.Styles != null)
                    {
                        foreach (var style in docSetting.Styles)
                        {
                            WParagraphStyle docStyle = doc.Styles.FindByName(style.StyleName, Syncfusion.DocIO.DLS.StyleType.ParagraphStyle) as WParagraphStyle;
                            if (docStyle != null)
                            {
                                if (!string.IsNullOrEmpty(style.FontName))
                                {
                                    docStyle.CharacterFormat.FontName = style.FontName;
                                }

                                float fontSize = 10;
                                if (!float.TryParse(style.FontSize, out fontSize))
                                {
                                    fontSize = 10;
                                }

                                if (fontSize > 0)
                                {
                                    docStyle.CharacterFormat.FontSize = fontSize;
                                }

                                //docStyle.CharacterFormat.TextColor = Color.FromRgb(20, 50,230);

                                docStyle.CharacterFormat.Bold = style.IsBold;
                                docStyle.CharacterFormat.Italic = style.IsItalic;
                                if (style.IsUnderline)
                                {
                                    docStyle.CharacterFormat.UnderlineStyle = UnderlineStyle.Single;
                                }
                            }
                        }
                    }
                }
                #endregion

                try
                {
                    //doccontrol.DocumentSource = new DocumentSource(di, bytes);
                    //doc.Save("Sample.docx", FormatType.Docx, Response, HttpContentDisposition.Attachment);

                    //if(generatePDF)
                    //{
                    #region save temp File
                    string Folder = "~/TempFiles";
                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                    string DateFolder = Folder + "/" + File;
                    System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                    if (!System.IO.Directory.Exists(DateFolder))
                    {
                        System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(DateFolder));
                    }

                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                    string User = userId + "" + customerId + "" + FileDate;
                    //string FileName = DateFolder + "/" + User + ".docx";
                    //doc.Save(System.Web.Hosting.HostingEnvironment.MapPath(FileName), FormatType.Docx);
                    string FileName = DateFolder + "/" + User + ".pdf";
                    //doc.Save(System.Web.Hosting.HostingEnvironment.MapPath(FileName), FormatType.Docx);

                    #region Generate PDF
                    //Creates an instance of the DocToPDFConverter - responsible for Word to PDF conversion
                    DocToPDFConverter converter = new DocToPDFConverter();
                    //Sets ExportBookmarks for preserving Word document headings as PDF bookmarks
                    converter.Settings.ExportBookmarks = Syncfusion.DocIO.ExportBookmarkType.Headings;
                    //Converts Word document into PDF document

                    //Sets true to enable the fast rendering using direct PDF conversion.
                    //converter.Settings.EnableFastRendering = true;

                    Syncfusion.Pdf.PdfDocument pdfDocument = converter.ConvertToPDF(doc);
                    //Saves the PDF file to file system
                    pdfDocument.Save(System.Web.Hosting.HostingEnvironment.MapPath(FileName));

                    //Closes the instance of document objects
                    pdfDocument.Close(true);

                    ////doc.Close();
                    //doccontrol.Document = FileName;
                    if (generatePDF)
                    {
                        result.FileName = FileName;
                        result.IsFileGenerated = true;
                    }
                    else
                    {
                        using (var stream = new MemoryStream())
                        {
                            doc.Save(stream, FormatType.Docx);
                            result.FileContent = stream.ToArray();
                        }
                    }
                    #endregion

                    #region Save Generated Doc Added on 28 July 2021
                    if(generateFor == "Draft Minutes")
                    {
                        string agendaReviewPath = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + meetingId + "/Agenda";
                        //string agendaReviewFile = agendaReviewPath + "/Agenda.pdf";
                        //string agendaReviewFileDocx = agendaReviewPath + "/Agenda.docx";
                        var guid = Convert.ToString(Guid.NewGuid());
                        string agendaReviewFile = agendaReviewPath + "/DraftMinute_" + guid + ".pdf";
                        string agendaReviewFileDocx = agendaReviewPath + "/DraftMinute_" + guid + ".docx";

                        if (taskAssignmentId > 0)
                        {
                            agendaReviewPath = "~/Areas/BM_Management/Documents/" + customerId + "/Meeting/" + meetingId + "/Review/" + taskAssignmentId;
                            agendaReviewFile = agendaReviewPath + "/ReviewDraftMinute.pdf";
                            agendaReviewFileDocx = agendaReviewPath + "/ReviewDraftMinute.docx";
                        }

                        if (!System.IO.Directory.Exists(agendaReviewPath))
                        {
                            System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewPath));
                        }

                        //Generate Pdf
                        using (FileStream fs = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewFile), FileMode.Create, FileAccess.ReadWrite))
                        {
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (true)
                            {
                                bw.Write(CryptographyHandler.AESEncrypt(objIFileData_Service.ReadDocFiles(System.Web.Hosting.HostingEnvironment.MapPath(FileName))));
                            }
                            bw.Close();
                        }

                        //Generate Docx
                        using (FileStream fsDocx = new FileStream(System.Web.Hosting.HostingEnvironment.MapPath(agendaReviewFileDocx), FileMode.Create, FileAccess.ReadWrite))
                        {
                            BinaryWriter bwDocx = new BinaryWriter(fsDocx);
                            if (true)
                            {
                                using (var stream = new MemoryStream())
                                {
                                    doc.Save(stream, FormatType.Docx);
                                    bwDocx.Write(CryptographyHandler.AESEncrypt(stream.ToArray()));
                                    if (generatePDF == false)
                                    {
                                        result.FileContent = stream.ToArray();
                                    }
                                }
                            }
                            bwDocx.Close();
                        }

                        if (taskAssignmentId > 0)
                        {
                            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                            {
                                var agendaFilePath = (from row in entities.BM_TaskAssignment
                                                      where row.ID == taskAssignmentId
                                                      select row).FirstOrDefault();
                                if (agendaFilePath != null)
                                {
                                    agendaFilePath.AgendaFilePath = agendaReviewFile;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                            {
                                var agendaFilePath = (from row in entities.BM_Meetings
                                                      where row.MeetingID == meetingId
                                                      select row).FirstOrDefault();
                                if (agendaFilePath != null)
                                {
                                    agendaFilePath.DraftMinutesFilePath = agendaReviewFile;
                                    entities.SaveChanges();

                                    entities.BM_MeetingAgendaMinutesComments.Where(k => k.MeetingId == meetingId && k.IsActive == true && k.DocType == "DM").ToList().ForEach(k => { k.IsActive = false; k.UpdatedBy = userId; k.UpdatedOn = DateTime.Now; });
                                    entities.SaveChanges();
                                }
                            }
                        }
                    }
                    #endregion

                    #endregion
                    //}
                    //else
                    //{
                    //MemoryStream stream = new MemoryStream();
                    //doc.Save(stream, FormatType.Docx);
                    //result.FileContent = stream.ToArray();
                    //}
                }
                catch (Exception ex)
                {
                    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            #endregion
            return result;
        }

    }
}