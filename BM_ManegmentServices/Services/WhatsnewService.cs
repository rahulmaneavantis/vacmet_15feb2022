﻿using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;

namespace BM_ManegmentServices.Services.Masters
{
    public class WhatsnewService : IWhatsnewservice
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public WhatsNewVM Savewhatsnewdocument(IEnumerable<HttpPostedFileBase> files, int userId, WhatsNewVM _objdoc)
        {
            
            try
            {
                bool success = false;
                string fileName = string.Empty;
                string directoryPath = string.Empty;

                foreach (var file in files)
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                    if (file.ContentLength > 0)
                    {
                        IEnumerable<HttpPostedFileBase> uploadfile1 = files;

                        directoryPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Areas/WhatsNew/");

                        if (!Directory.Exists(directoryPath))
                            Directory.CreateDirectory(directoryPath);

                        Guid fileKey1 = Guid.NewGuid();
                        string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(file.FileName));
                        string filepathvirtual = Path.Combine("~/Areas/WhatsNew/", fileKey1 + Path.GetExtension(file.FileName));
                        Stream fs = file.InputStream;
                        BinaryReader br = new BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                        fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));
                        SaveDocFiles(fileList);

                        BM_WhatsNew _objdocdetails = new BM_WhatsNew
                        {
                            Id = _objdoc.Id,
                            Title = _objdoc.Title,
                            FileName = file.FileName,
                            FilePath = filepathvirtual,
                            UploadedBy = userId,
                            UploadedOn = _objdoc.UploadedOn,
                            IsDeleted = true
                        };
                        entities.BM_WhatsNew.Add(_objdocdetails);
                        entities.SaveChanges();
                        success = true;
                    }
                    if (success)
                    {
                        _objdoc.Success = true;
                        _objdoc.Message = SecretarialConst.Messages.saveSuccess;
                    }
                    else
                    {
                        _objdoc.Success = false;
                        _objdoc.Message = SecretarialConst.Messages.alreadyExist;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objdoc.Message = SecretarialConst.Messages.serverError;
            }

            return _objdoc;
        }

        public static void SaveDocFiles(List<KeyValuePair<string, Byte[]>> filesList)
        {
            if (filesList != null)
            {
                foreach (var file in filesList)
                {
                    using (BinaryWriter Writer = new BinaryWriter(File.OpenWrite(file.Key)))
                    {
                        // Writer raw data                
                        Writer.Write(file.Value);
                        Writer.Flush();
                        Writer.Close();
                    }

                }
            }
        }

        public List<WhatsNewVM> GetWhatsnewdata()
        {
            var result = new List<WhatsNewVM>();
            try
            {
                result = (from row in entities.BM_WhatsNew
                          where row.IsDeleted == true
                          select new WhatsNewVM
                          {
                              Id = row.Id,
                              Title = row.Title,
                              UploadedOn = row.UploadedOn,
                              FilePath = row.FilePath
                          }).ToList();

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result.OrderByDescending(k => k.UploadedOn).ToList();

        }
        public bool DeleteWhatsNew(long id, int userId)
        {
            bool success = false;
            try
            {
                var checkcomp = (from row in entities.BM_WhatsNew where row.Id == id select row).FirstOrDefault();
                if (checkcomp != null)
                {
                    checkcomp.IsDeleted = false;
                    //checkcomp.UploadedOn = DateTime.Now;
                    checkcomp.UploadedBy = userId;
                    entities.SaveChanges();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                success = false;
            }
            return success;
        }

    }
}