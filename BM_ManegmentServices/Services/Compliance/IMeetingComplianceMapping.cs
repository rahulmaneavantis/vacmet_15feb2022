﻿using BM_ManegmentServices.VM.Compliance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Compliance
{
    public interface IMeetingComplianceMapping
    {
        List<VM_MeetingCamplianceMapping> GetMeetingType();
        List<AgendaComplianceEditorVM> GetComplianceMappingDetails(int entityType, long meetingTypeID);
        List<VMCompliences> BindCompliancesNew(int meetingTypeId, int entityTypeId);
        void AddMeetingItem(VMCompliences complianceItem, int userID, int customerID);
        IEnumerable<DayTypeViewModel> getDayType(string dayhoureId);
        IEnumerable<DaysOrHoursViewModel> GetHoureMinutesDay();
        void AddMeetingMappingDetails(AgendaComplianceEditorVM complianceItem, int userID, int customerID);
    }
}
