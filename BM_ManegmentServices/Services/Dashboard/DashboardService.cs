﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.VM.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Dashboard
{
    public class DashboardService : IDashboardService
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public object GetDashboardCount(int CustomerId, int UserID, string Role)
        {
            try
            {
                return (from row in entities.BM_SP_GetDashboardCount(CustomerId, UserID, Role)
                        select new
                        {
                            Draft = row.Draft,
                            Completed = row.Completed
                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public object GetDashboardCountForDirector(int CustomerId, int UserID)
        {
            try
            {
                return (from row in entities.BM_SP_GetDashboardCountForDirector(CustomerId, UserID)
                        select new
                        {
                            Entities = row.Entities,
                            Directors = row.Directors,
                            Meetings = row.Meetings,
                            Agenda = row.Agenda,
                            Drafts = row.Drafts
                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Dashboard_CompliancesCountVM GetComplianceCount(int CustomerId, int UserID)
        {
            var result = new Dashboard_CompliancesCountVM() { Performer = new PerformerCountVM(), Reviewer = new ReviewerCountVM() };
            try
            {
                var countResult = (from row in entities.BM_SP_Compliance_MyCompliancesNewCount(UserID, CustomerId)
                              select row).ToList();

                if(countResult != null)
                {
                    #region Performer
                    result.Performer.OverDue_Statutory = GetCount(countResult, 3, "S", "Overdue");
                    result.Performer.OverDue_Meeting = GetCount(countResult, 3, "M", "Overdue");
                    result.Performer.OverDue_Director = GetCount(countResult, 3, "D", "Overdue");

                    result.Performer.Upcoming_Statutory = GetCount(countResult, 3, "S", "Upcoming");
                    result.Performer.Upcoming_Meeting = GetCount(countResult, 3, "M", "Upcoming");
                    result.Performer.Upcoming_Director = GetCount(countResult, 3, "D", "Upcoming");

                    result.Performer.PendingForReview_Statutory = GetCount(countResult, 3, "S", "Pending For Review");
                    result.Performer.PendingForReview_Meeting = GetCount(countResult, 3, "M", "Pending For Review");
                    result.Performer.PendingForReview_Director = GetCount(countResult, 3, "D", "Pending For Review");

                    result.Performer.Rejected_Statutory = GetCount(countResult, 3, "S", "Rejected");
                    result.Performer.Rejected_Meeting = GetCount(countResult, 3, "M", "Rejected");
                    result.Performer.Rejected_Director = GetCount(countResult, 3, "D", "Rejected");
                    #endregion

                    #region Performer
                    result.Reviewer.DueButNotSubmitted_Statutory = GetCount(countResult, 4, "S", "Due But Not Submitted");
                    result.Reviewer.DueButNotSubmitted_Meeting = GetCount(countResult, 4, "M", "Due But Not Submitted");
                    result.Reviewer.DueButNotSubmitted_Director = GetCount(countResult, 4, "D", "Due But Not Submitted");

                    result.Reviewer.PendingForReview_Statutory = GetCount(countResult, 4, "S", "Pending For Review");
                    result.Reviewer.PendingForReview_Meeting = GetCount(countResult, 4, "M", "Pending For Review");
                    result.Reviewer.PendingForReview_Director = GetCount(countResult, 4, "D", "Pending For Review");

                    result.Reviewer.Rejected_Statutory = GetCount(countResult, 4, "S", "Rejected");
                    result.Reviewer.Rejected_Meeting = GetCount(countResult, 4, "M", "Rejected");
                    result.Reviewer.Rejected_Director = GetCount(countResult, 4, "D", "Rejected");
                    #endregion
                }
            }
            catch (Exception ex)
            {
                //obj.Error = true;
                //obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public int GetCount (List<BM_SP_Compliance_MyCompliancesNewCount_Result> obj, int RoleId, string mappingType, string status)
        {
            int result = 0;
            try
            {
                var count = obj.Where(k => k.RoleID == RoleId && k.MappingType == mappingType && k.Name == status).Select(k => k.Total).FirstOrDefault();
                if(count > 0)
                {
                    result = (int)count;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }


        #region Compliance
        public List<MyCompliance_Director_ResultVM> MyCompliances_Director(int customerId, int userId, int roleId)
        {
            try
            {
                return (from row in entities.BM_SP_MyCompliance_Director(userId, customerId, 1, roleId, 2, null)
                        select new MyCompliance_Director_ResultVM
                        {
                            EntityId = row.EntityId,
                            CompanyName = row.CompanyName,
                            DirectorId = row.DirectorId,
                            DirectorName = row.DirectorName,
                            ScheduleOnID = row.ScheduleOnID,

                            RoleID = row.RoleID,
                            MappingType = row.MappingType,

                            MeetingID = row.MeetingID,
                            MeetingTitle = row.MeetingTitle,

                            ShortForm = row.ShortForm,
                            ShortDescription = row.ShortDescription,
                            Description = row.Description,
                            RequiredForms = row.RequiredForms,

                            ScheduleOn = row.ScheduleOn,
                            ScheduleOnInt = row.ScheduleOnInt,
                            ScheduleOnTime = row.ScheduleOnTime,
                            ComplianceStatus = row.ComplianceStatus,

                            Frequency = row.Frequency,
                            ForMonth = row.ForMonth,
                            ForPeriod = row.ForPeriod,
                            Risk = row.Risk

                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public List<MyCompliance_Director_ResultVM> MyCompliances_Director(int customerId, int userId, int roleId, string FY_Year)
        {
            try
            {
                entities.Database.CommandTimeout = 180;                
                return (from row in entities.BM_SP_MyCompliance_Director(userId, customerId, 1, roleId, 2, FY_Year)
                        select new MyCompliance_Director_ResultVM
                        {
                            EntityId = row.EntityId,
                            CompanyName = row.CompanyName,
                            DirectorId = row.DirectorId,
                            DirectorName = row.DirectorName,
                            ScheduleOnID = row.ScheduleOnID,

                            //RoleID = row.RoleID,
                            MappingType = row.MappingType,

                            MeetingID = row.MeetingID,
                            MeetingTitle = row.MeetingTitle,

                            ShortForm = row.ShortForm,
                            ShortDescription = row.ShortDescription,
                            Description = row.Description,
                            RequiredForms = row.RequiredForms,

                            ScheduleOn = row.ScheduleOn,
                            ScheduleOnInt = row.ScheduleOnInt,
                            ScheduleOnTime = row.ScheduleOnTime,
                            ComplianceStatus = row.ComplianceStatus,

                            Frequency = row.Frequency,
                            ForMonth = row.ForMonth,
                            ForPeriod = row.ForPeriod,
                            Risk = row.Risk

                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<BM_SP_DashboardCompliances_Result> GetDashboardCompliances(int userID, int customerID,  string FY_Year, string userRole)
        {
            try
            {
                using (Compliance_SecretarialEntities context = new Compliance_SecretarialEntities())
                {
                    //     string sql = string.Format("EXEC [dbo].[BM_SP_DashboardCompliances] @UserID = N'{0}', @CustomerID = N'{1}', @FY_Year = N'{2}', @Role = N'{3}'", userID, customerID, FY_Year, userRole);

                    //     List<BM_SP_DashboardCompliances_Result> duplicateCheckResults =
                    //context.Database.SqlQuery<BM_SP_DashboardCompliances_Result>(sql).ToList();

                    //     return duplicateCheckResults;
                    context.Database.CommandTimeout = 360;
                    return (from row in context.BM_SP_DashboardCompliances(userID, customerID, FY_Year, userRole)
                            select row).ToList();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<MyCompliance_Director_ResultVM> MyCompliances_Dashboard(int customerId, int userId, int roleId, string FY_Year, string role)
        {
            try
            {
                return (from row in entities.BM_SP_DashboardCompliances(userId, customerId, FY_Year, role)
                        select new MyCompliance_Director_ResultVM
                        {
                            EntityId = row.EntityId,
                            CompanyName = row.CompanyName,
                            DirectorId = row.DirectorId,
                            DirectorName = row.DirectorName,
                            ScheduleOnID = row.ScheduleOnID,

                            RoleID = (int)row.UserRoleID,
                            MappingType = row.MappingType,

                            MeetingID = row.MeetingID,
                            MeetingTitle = row.MeetingTitle,

                            ShortForm = row.ShortForm,
                            ShortDescription = row.ShortDescription,
                            Description = row.Description,
                            RequiredForms = row.RequiredForms,

                            ScheduleOn = row.ScheduleOn,
                            ScheduleOnInt = row.ScheduleOnInt,
                            ScheduleOnTime = row.ScheduleOnTime,
                            ComplianceStatus = row.ComplianceStatus,

                            Frequency = row.Frequency,
                            ForMonth = row.ForMonth,
                            ForPeriod = row.ForPeriod,
                            Risk = row.Risk,
                            PerformerName = row.PerformerName

                        }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion
    }
}