﻿using BM_ManegmentServices.VM.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.Data;

namespace BM_ManegmentServices.Services.Dashboard
{
    public interface IDashboardService
    {
        object GetDashboardCount(int CustomerId, int UserID, string Role);

        object GetDashboardCountForDirector(int CustomerId, int UserID);

        Dashboard_CompliancesCountVM GetComplianceCount(int CustomerId, int UserID);

        #region Compliance
        List<MyCompliance_Director_ResultVM> MyCompliances_Director(int customerId, int userId, int roleId);

        List<MyCompliance_Director_ResultVM> MyCompliances_Director(int customerId, int userId, int roleId, string FY_Year);

        List<BM_SP_DashboardCompliances_Result> GetDashboardCompliances(int userID, int customerID, string FY_Year, string userRole);

        List<MyCompliance_Director_ResultVM> MyCompliances_Dashboard(int customerId, int userId, int roleId, string FY_Year, string role);
        #endregion
    }
}
