﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.VM.UIForms;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.UIForm
{
    public class UIFormData<T>
    {
        public T MapData(T param, List<MeetingAgendaTemplateListVM> values)
        {
            if (values != null)
            {
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    var value = values.Where(k => k.TemplateName == property.Name).Select(k => k.TemplateValue).FirstOrDefault();
                    property.SetValue(param, value);
                }
            }
            return param;
        }
        public List<MeetingAgendaTemplateListVM> MapData(List<MeetingAgendaTemplateListVM> values, T param)
        {
            if (values != null && param != null)
            {
                foreach (var item in values)
                {
                    var property = typeof(T).GetProperties().Where(k => k.Name == item.TemplateName).FirstOrDefault();
                    if (property != null)
                    {
                        item.TemplateValue = Convert.ToString(property.GetValue(param));
                    }
                }
            }
            return values;
        }
    }
    public class UIFormService : IUIFormService
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        IMeeting_Service objIMeeting_Service;
        IKMP_Master objIKMP_Master;
        IDirectorMaster objIDirectorMaster;
        IAuditorMaster objIAuditorMaster;
        ICapitalMaster objICapitalMaster;
        public UIFormService(IMeeting_Service objMeeting_Service, IKMP_Master objKMP_Master, IDirectorMaster objDirectorMaster, IAuditorMaster objAuditorMaster, ICapitalMaster objCapitalMaster)
        {
            objIMeeting_Service = objMeeting_Service;
            objIKMP_Master = objKMP_Master;
            objIDirectorMaster = objDirectorMaster;

            objIAuditorMaster = objAuditorMaster;

            objICapitalMaster = objCapitalMaster;
        }

        public List<MeetingAgendaTemplateListVM> GetTemplateFields(long meetingId, long agendaId, long mappingId)
        {
            try
            {
                var result = (from row in entities.BM_MeetingAgendaTemplateList
                              join template in entities.BM_AgendaMasterTemplate on row.TemplateID equals template.TemplateID
                              join form in entities.BM_UIFormColumns on template.UIFormColumnsID equals form.UIFormColumnsID
                              where row.MeetingAgendaMappingID == mappingId && template.IsUIFormColumn == true && row.IsDeleted == false
                              select new MeetingAgendaTemplateListVM
                              {
                                  TemplateID = template.TemplateID,
                                  CategoryID = template.CategoryID,
                                  TemplateName = form.RefColumn,
                                  TemplateValue = row.TemplateListValue,
                                  MeetingAgendaMapping_ID = mappingId
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<MeetingAgendaTemplateListVM> GetUIFormColumnsData(long agendaId, long mappingId)
        {
            try
            {
                var result = (from agendaFields in entities.BM_AgendaMasterTemplate
                              join form in entities.BM_UIFormColumns on agendaFields.UIFormColumnsID equals form.UIFormColumnsID
                              from template in entities.BM_MeetingAgendaTemplateList.Where(k => k.MeetingAgendaMappingID == mappingId && k.TemplateID == agendaFields.TemplateID && k.IsDeleted == false).DefaultIfEmpty()
                              where agendaFields.AgendaID == agendaId && agendaFields.IsUIFormColumn == true && agendaFields.IsDeleted == false
                              select new MeetingAgendaTemplateListVM
                              {
                                  TemplateID = agendaFields.TemplateID,
                                  TemplateName = form.RefColumn,
                                  TemplateListID = template == null ? 0 : template.TemplateListID,
                                  TemplateValue = template == null ? "" : template.TemplateListValue,
                                  MeetingAgendaMapping_ID = mappingId
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #region Appointment of CEO/CFO/CS/Manager 
        public UIForm_DirectorMasterVM AppointmentOfManager(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 2;
            var typeOfDirectorShipId = 0;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfCS(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 3;
            var typeOfDirectorShipId = 0;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfCEO(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 4;
            var typeOfDirectorShipId = 0;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfCFO(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 5;
            var typeOfDirectorShipId = 0;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }

        private UIForm_KMPMasterVM AppointmentOfKMP(long meetingId, long agendaId, long mappingId, int designationId)
        {
            var obj = new UIForm_KMPMasterVM();
            try
            {
                //obj.AgendaID = agendaId;
                //obj.MappingID = mappingId;
                //obj.DesignationId = designationId;

                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<UIForm_KMPMasterVM> mapper = new UIFormData<UIForm_KMPMasterVM>();
                obj = mapper.MapData(obj, result);

                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                obj.DesignationId = designationId;

                if (refMasterId > 0)
                {
                    obj.RefMasterID = (long)refMasterId;
                    var address = (from row in entities.BM_DirectorMaster
                                   from state in entities.States.Where(k => k.ID == row.Present_StateId).Select(k => k.Name).DefaultIfEmpty()
                                   from city in entities.Cities.Where(k => k.ID == row.Present_CityId).Select(k => k.Name).DefaultIfEmpty()
                                   where row.Id == refMasterId
                                   select new
                                   {
                                       row.Present_Address_Line1,
                                       row.Present_Address_Line2,
                                       row.Present_StateId,
                                       row.Present_CityId,
                                       state,
                                       city,
                                       row.Present_PINCode,
                                       row.Salutation,
                                       row.FSalutations
                                   }).FirstOrDefault();

                    if (address != null)
                    {
                        obj.Present_Address_Line1 = address.Present_Address_Line1;
                        obj.Present_Address_Line2 = address.Present_Address_Line2;

                        obj.Present_StateId = address.Present_StateId;
                        obj.Present_CityId = address.Present_CityId;
                        obj.Present_PINCode = address.Present_PINCode;
                        obj.Salutation = address.Salutation;
                        obj.FSalutations = address.FSalutations;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public UIForm_KMPMasterVM SaveAppointmentOfCFO(UIForm_KMPMasterVM obj, int userId, int customerId)
        {
            try
            {
                #region Appointment of Manager validation
                if (obj.DesignationId == 2)
                {
                    DateTime Kmp_appointment_Date = DateTime.Now;
                    DateTime Kmp_appointment_Date_To = DateTime.Now;
                    bool flag = true;

                    if (!string.IsNullOrEmpty(obj.Kmp_appointment_Date))
                    {
                        if (!DateTime.TryParseExact(obj.Kmp_appointment_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Kmp_appointment_Date))
                        {
                            flag = false;
                        }
                    }
                    else
                    {
                        flag = false;
                    }

                    if (!string.IsNullOrEmpty(obj.Kmp_appointment_Date_To))
                    {
                        if (!DateTime.TryParseExact(obj.Kmp_appointment_Date_To, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Kmp_appointment_Date_To))
                        {
                            flag = false;
                        }
                    }
                    else
                    {
                        flag = false;
                    }

                    if (flag)
                    {
                        if (Kmp_appointment_Date.AddYears(5) < Kmp_appointment_Date_To)
                        {
                            obj.Success = false;
                            obj.Error = true;
                            obj.Message = "Please check tenure of appointment to date";
                            return obj;
                        }
                    }
                }
                #endregion

                #region Consent date shall not after meeting date

                DateTime dateOfConsent_ = DateTime.Now;
                DateTime Kmp_appointment_Date1 = DateTime.Now;
                var dateOfConsent_Flag = false;
                var Kmp_appointment_Date_Flag = false;

                if (DateTime.TryParseExact(obj.DateOfConsent_, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateOfConsent_))
                {
                    dateOfConsent_Flag = true;
                }

                //if (!string.IsNullOrEmpty(obj.Kmp_appointment_Date))
                //{
                if (DateTime.TryParseExact(obj.Kmp_appointment_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Kmp_appointment_Date1))
                {
                    Kmp_appointment_Date_Flag = true;
                }
                //}

                var meetingdate = (from mapping in entities.BM_MeetingAgendaMapping
                                   join meeting in entities.BM_Meetings on mapping.MeetingID equals meeting.MeetingID
                                   where mapping.MeetingAgendaMappingID == obj.MappingID && mapping.IsDeleted == false
                                   select meeting.MeetingDate
                                  ).FirstOrDefault();

                if (meetingdate != null && dateOfConsent_Flag)
                {
                    if (dateOfConsent_ > meetingdate)
                    {
                        obj.Error = true;
                        obj.Message = "Date of consent shall not be after the date of Meeting";
                        return obj;
                    }
                }

                if (dateOfConsent_Flag == true && Kmp_appointment_Date_Flag == true)
                {
                    if (dateOfConsent_ > Kmp_appointment_Date1)
                    {
                        obj.Error = true;
                        obj.Message = "Date of consent shall not be after the date of appointment";
                        return obj;
                    }
                }
                #endregion

                #region Commented on 12 Sep 2020
                //var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                //obj.RefMasterID = (from mapping in entities.BM_MeetingAgendaMapping
                //                   where mapping.MeetingAgendaMappingID == obj.MappingID && mapping.IsDeleted == false
                //                   select mapping.RefMasterID).FirstOrDefault();

                //UIFormData<UIForm_KMPMasterVM> mapper = new UIFormData<UIForm_KMPMasterVM>();
                //result = mapper.MapData(result, obj);

                //var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                //objMeetingAgendaTemplateVM.lstControls = result;

                //objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                //obj.Success = objMeetingAgendaTemplateVM.Success;
                //obj.Error = objMeetingAgendaTemplateVM.Error;
                //obj.Message = objMeetingAgendaTemplateVM.Message;

                //obj = objIKMP_Master.CreateUpdateKMP(obj, userId, customerId);
                #endregion
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                obj.RefMasterID = (from mapping in entities.BM_MeetingAgendaMapping
                                   where mapping.MeetingAgendaMappingID == obj.MappingID && mapping.IsDeleted == false
                                   select mapping.RefMasterID).FirstOrDefault();
                obj = objIKMP_Master.CreateUpdateKMP(obj, userId, customerId);

                if (obj.Success)
                {
                    var presentState = (from row in entities.States
                                        where row.ID == obj.Present_StateId
                                        select row.Name).FirstOrDefault();

                    var presentCity = (from row in entities.Cities
                                       where row.ID == obj.Present_CityId
                                       select row.Name).FirstOrDefault();

                    obj.Present_Address_Line1 = (string.IsNullOrEmpty(obj.Present_Address_Line1) ? "" : obj.Present_Address_Line1) +
                            (string.IsNullOrEmpty(obj.Present_Address_Line2) ? "" : " " + obj.Present_Address_Line2) +
                            (string.IsNullOrEmpty(presentCity) ? "" : ", " + presentCity) +
                            (string.IsNullOrEmpty(presentState) ? "" : ", " + presentState) +
                            (string.IsNullOrEmpty(obj.Present_PINCode) ? "" : "-" + obj.Present_PINCode);

                    UIFormData<UIForm_KMPMasterVM> mapper = new UIFormData<UIForm_KMPMasterVM>();
                    result = mapper.MapData(result, obj);

                    var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                    objMeetingAgendaTemplateVM.lstControls = result;

                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    obj.Success = objMeetingAgendaTemplateVM.Success;
                    obj.Error = objMeetingAgendaTemplateVM.Error;
                    obj.Message = objMeetingAgendaTemplateVM.Message;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Cessation of CEO/CFO/CS/Manager
        public UIForm_DirectorMasterCessationVM CessationOfManager(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 2;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        public UIForm_DirectorMasterCessationVM CessationOfCS(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 3;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        public UIForm_DirectorMasterCessationVM CessationOfCEO(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 4;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        public UIForm_DirectorMasterCessationVM CessationOfCFO(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 5;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        private UIForm_KMPMasterCessationVM CessationOfKMP(long meetingId, long agendaId, long mappingId, int designationId)
        {
            var obj = new UIForm_KMPMasterCessationVM();
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<UIForm_KMPMasterCessationVM> mapper = new UIFormData<UIForm_KMPMasterCessationVM>();
                obj = mapper.MapData(obj, result);

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                obj.DesignationId = designationId;
                if (refMasterId > 0)
                {
                    obj.RefMasterID = (long)refMasterId;
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_KMPMasterCessationVM SaveCessationOfKMP(UIForm_KMPMasterCessationVM obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                if (obj.RefMasterID > 0)
                {
                    var kmp = (from row in entities.BM_DirectorMaster
                               where row.Id == obj.RefMasterID
                               select new
                               {
                                   row.FirstName,
                                   row.MiddleName,
                                   row.LastName,

                                   row.Present_Address_Line1,
                                   row.Present_Address_Line2,

                                   row.PAN,

                                   row.cessation_Date
                               }).FirstOrDefault();

                    if (kmp != null)
                    {
                        obj.FirstName = kmp.FirstName;
                        obj.MiddleName = kmp.MiddleName;
                        obj.LastName = kmp.LastName;

                        obj.Present_Address_Line1 = kmp.Present_Address_Line1;
                        obj.Present_Address_Line2 = kmp.Present_Address_Line2;

                        obj.PAN = kmp.PAN;
                    }

                    UIFormData<UIForm_KMPMasterCessationVM> mapper = new UIFormData<UIForm_KMPMasterCessationVM>();
                    result = mapper.MapData(result, obj);

                    var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                    objMeetingAgendaTemplateVM.lstControls = result;

                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.RefMasterID;
                        mapping.UpdatedBy = userId;
                        mapping.UpdatedOn = DateTime.Now;

                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }

                    obj.Success = objMeetingAgendaTemplateVM.Success;
                    obj.Error = objMeetingAgendaTemplateVM.Error;
                    obj.Message = objMeetingAgendaTemplateVM.Message;

                    objIKMP_Master.ResignationOfKMP(obj, userId, customerId);
                }

            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Appointment of Director
        private UIForm_DirectorMasterVM AppointmentOfDirector(long meetingId, long agendaId, long mappingId, int designationId, int typeOfDirectorShipId)
        {
            var obj = new UIForm_DirectorMasterVM();
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<UIForm_DirectorMasterVM> mapper = new UIFormData<UIForm_DirectorMasterVM>();
                obj = mapper.MapData(obj, result);

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                obj.DirectorDesignationId = designationId;
                obj.TypeOfDirectorShipId = typeOfDirectorShipId;
                if (refMasterId > 0)
                {
                    obj.RefMasterID = (long)refMasterId;
                    obj.Director_Id = entities.BM_Directors_DetailsOfInterest.Where(k => k.Id == refMasterId).Select(k => k.Director_Id).FirstOrDefault();
                    if (designationId == 11)
                    {
                        obj.OriginalDirector_Id = entities.BM_Directors_DetailsOfInterest.Where(k => k.Id == refMasterId).Select(k => k.OriginalDirector_Id).FirstOrDefault();
                    }
                    else
                    {
                        obj.OriginalDirector_Id = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_DirectorMasterVM AppointmentOfMD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 6;
            var typeOfDirectorShipId = 5;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfWTD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 7;
            var typeOfDirectorShipId = 5;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfADDD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 8;
            var typeOfDirectorShipId = 6;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfADDD_Independant(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 8;
            var typeOfDirectorShipId = 7;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfCAVD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 9;
            var typeOfDirectorShipId = 5;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfNOMD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 10;
            var typeOfDirectorShipId = 6;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM AppointmentOfALTD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 11;
            var typeOfDirectorShipId = 5;
            return AppointmentOfDirector(meetingId, agendaId, mappingId, designationId, typeOfDirectorShipId);
        }
        public UIForm_DirectorMasterVM SaveAppointmentOfDirector(UIForm_DirectorMasterVM obj, int userId, int customerId)
        {
            try
            {
                #region WTD,MD,Manager - term of appointment should not exceed 5 years
                if (obj.DirectorDesignationId == 2 || obj.DirectorDesignationId == 6 || obj.DirectorDesignationId == 7)
                {
                    DateTime appointment_Date = DateTime.Now, TenureOfAppointment = DateTime.Now;
                    var flag = true;
                    if (!string.IsNullOrEmpty(obj.DateOfAppointment))
                    {
                        if (!DateTime.TryParseExact(obj.DateOfAppointment, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out appointment_Date))
                        {
                            flag = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(obj.TenureOfAppointment))
                    {
                        if (!DateTime.TryParseExact(obj.TenureOfAppointment, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TenureOfAppointment))
                        {
                            flag = false;
                        }
                    }

                    if (flag)
                    {
                        if (appointment_Date.AddYears(5) < TenureOfAppointment)
                        {
                            obj.Error = true;
                            obj.Message = "The term of appointment shall not exceed 5 years.";
                            return obj;
                        }
                    }
                }
                #endregion

                #region Director to whom such appointee is alternate
                if (obj.DirectorDesignationId == 11 && obj.OriginalDirector_Id <= 0)
                {
                    obj.Error = true;
                    obj.Message = "Select Director to whom such appointee is alternate";
                }
                #endregion 

                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                if (obj.Director_Id > 0)
                {
                    var directorDetails = (from director in entities.BM_DirectorMaster
                                           from present_state in entities.States.Where(k => k.ID == director.Present_StateId).Select(k => k.Name).DefaultIfEmpty()
                                           from present_city in entities.Cities.Where(k => k.ID == director.Present_CityId).Select(k => k.Name).DefaultIfEmpty()
                                           where director.Id == obj.Director_Id
                                           select new
                                           {
                                               director.DIN,

                                               director.Salutation,
                                               director.FirstName,
                                               director.MiddleName,
                                               director.LastName,

                                               director.FSalutations,
                                               director.Father,
                                               director.FatherMiddleName,
                                               director.FatherLastName,

                                               director.Present_Address_Line1,
                                               director.Present_Address_Line2,
                                               director.Present_StateId,
                                               director.Present_CityId,
                                               present_state,
                                               present_city,
                                               director.Present_PINCode,

                                               director.PAN,
                                               director.CS_MemberNo
                                           }).FirstOrDefault();

                    if (directorDetails != null)
                    {
                        obj.DIN = directorDetails.DIN;

                        obj.FirstName = (string.IsNullOrEmpty(directorDetails.Salutation) ? "" : directorDetails.Salutation) +
                                        (string.IsNullOrEmpty(directorDetails.FirstName) ? "" : " " + directorDetails.FirstName) +
                                        (string.IsNullOrEmpty(directorDetails.MiddleName) ? "" : " " + directorDetails.MiddleName) +
                                        (string.IsNullOrEmpty(directorDetails.LastName) ? "" : " " + directorDetails.LastName);

                        obj.Father = (string.IsNullOrEmpty(directorDetails.FSalutations) ? "" : directorDetails.FSalutations) +
                                     (string.IsNullOrEmpty(directorDetails.Father) ? "" : directorDetails.Father) +
                                     (string.IsNullOrEmpty(directorDetails.FatherMiddleName) ? "" : " " + directorDetails.FatherMiddleName) +
                                     (string.IsNullOrEmpty(directorDetails.FatherLastName) ? "" : " " + directorDetails.FatherLastName);


                        obj.Present_Address_Line1 = (string.IsNullOrEmpty(directorDetails.Present_Address_Line1) ? "" : directorDetails.Present_Address_Line1) +
                            (string.IsNullOrEmpty(directorDetails.Present_Address_Line2) ? "" : " " + directorDetails.Present_Address_Line2) +
                            (string.IsNullOrEmpty(directorDetails.present_state) ? "" : ", " + directorDetails.present_state) +
                            (string.IsNullOrEmpty(directorDetails.present_city) ? "" : ", " + directorDetails.present_city) +
                            (string.IsNullOrEmpty(directorDetails.Present_PINCode) ? "" : "-" + directorDetails.Present_PINCode);

                        obj.PAN = directorDetails.PAN;
                        obj.CS_MemberNo = directorDetails.CS_MemberNo;
                    }

                    if (obj.DirectorDesignationId == 11)
                    {
                        if (obj.OriginalDirector_Id > 0)
                        {
                            var originaldirectorDetails = (from director in entities.BM_DirectorMaster
                                                           where director.Id == obj.OriginalDirector_Id
                                                           select new
                                                           {
                                                               director.DIN,

                                                               director.Salutation,
                                                               director.FirstName,
                                                               director.MiddleName,
                                                               director.LastName
                                                           }).FirstOrDefault();

                            if (originaldirectorDetails != null)
                            {
                                obj.OriginalDirectorDIN = directorDetails.DIN;

                                obj.OriginalDirectorName = (string.IsNullOrEmpty(originaldirectorDetails.Salutation) ? "" : originaldirectorDetails.Salutation) +
                                                (string.IsNullOrEmpty(originaldirectorDetails.FirstName) ? "" : " " + originaldirectorDetails.FirstName) +
                                                (string.IsNullOrEmpty(originaldirectorDetails.MiddleName) ? "" : " " + originaldirectorDetails.MiddleName) +
                                                (string.IsNullOrEmpty(originaldirectorDetails.LastName) ? "" : " " + originaldirectorDetails.LastName);
                            }
                            else
                            {
                                obj.OriginalDirectorDIN = "";
                                obj.OriginalDirectorName = "";
                            }
                        }
                        else
                        {
                            obj.OriginalDirectorDIN = "";
                            obj.OriginalDirectorName = "";
                        }
                    }

                    UIFormData<UIForm_DirectorMasterVM> mapper = new UIFormData<UIForm_DirectorMasterVM>();
                    result = mapper.MapData(result, obj);

                    #region MD term of appointment shall not exceed 5 years
                    if (obj.DirectorDesignationId == 6)
                    {
                        if (!String.IsNullOrEmpty(obj.TermOfAppointment))
                        {
                            int termOfAppointment = 0;
                            if (Int32.TryParse(obj.TermOfAppointment, out termOfAppointment))
                            {
                                if (termOfAppointment > 5)
                                {
                                    obj.Error = true;
                                    obj.Message = "The term of appointment shall not exceed 5 years.";
                                    return obj;
                                }
                            }
                        }
                    }
                    #endregion

                    var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                    objMeetingAgendaTemplateVM.lstControls = result;

                    //Commented on 17 Sep 2020
                    //objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    //obj.Success = objMeetingAgendaTemplateVM.Success;
                    //obj.Error = objMeetingAgendaTemplateVM.Error;
                    //obj.Message = objMeetingAgendaTemplateVM.Message;

                    obj = objIDirectorMaster.AppointmentOfDirector(obj, userId, customerId);

                    if (obj.RefMasterID > 0 && obj.Success)
                    {
                        //Added on 17 Sep 2020
                        objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                        var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MappingID && k.IsDeleted == false).FirstOrDefault();
                        if (mapping != null)
                        {
                            mapping.RefMasterID = obj.RefMasterID;
                            mapping.UpdatedBy = userId;
                            mapping.UpdatedOn = DateTime.Now;

                            entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();
                        }
                    }
                }
                else
                {
                    obj.Error = true;
                    obj.Message = "Select Director from list.";
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Cessation Of Director
        public UIForm_DirectorMasterCessationVM CessationOfMD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 6;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        public UIForm_DirectorMasterCessationVM CessationOfWTD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 7;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        public UIForm_DirectorMasterCessationVM CessationOfADDD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 8;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        public UIForm_DirectorMasterCessationVM CessationOfCAVD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 9;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        public UIForm_DirectorMasterCessationVM CessationOfNOMD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 10;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        public UIForm_DirectorMasterCessationVM CessationOfALTD(long meetingId, long agendaId, long mappingId)
        {
            var designationId = 11;
            return CessationOfDirector(meetingId, agendaId, mappingId, designationId);
        }
        private UIForm_DirectorMasterCessationVM CessationOfDirector(long meetingId, long agendaId, long mappingId, int designationId)
        {
            var obj = new UIForm_DirectorMasterCessationVM();
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<UIForm_DirectorMasterCessationVM> mapper = new UIFormData<UIForm_DirectorMasterCessationVM>();
                obj = mapper.MapData(obj, result);

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                obj.DirectorDesignationId = designationId;
                if (refMasterId > 0)
                {
                    obj.RefMasterID = (long)refMasterId;
                    //obj.Director_Id = entities.BM_Directors_DetailsOfInterest.Where(k => k.Id == refMasterId).Select(k => k.Director_Id).FirstOrDefault();
                    var director = (from row in entities.BM_Directors_DetailsOfInterest
                                    join dir in entities.BM_DirectorMaster on row.Director_Id equals dir.Id
                                    where row.Id == refMasterId
                                    select new
                                    {
                                        row.Director_Id,
                                        dir.Salutation,
                                        dir.FirstName,
                                        dir.MiddleName,
                                        dir.LastName,
                                        row.CessationEffectFrom,
                                        row.ResignedDate
                                    }).FirstOrDefault();
                    if (director != null)
                    {
                        obj.Director_Id = director.Director_Id;
                        obj.FullNameOfDirector = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation) +
                                                (string.IsNullOrEmpty(director.FirstName) ? "" : " " + director.FirstName) +
                                                (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.MiddleName) +
                                                (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName);

                        if (director.CessationEffectFrom != null)
                        {
                            obj.CessationEffectFrom = Convert.ToDateTime(director.CessationEffectFrom).ToString("dd/MM/yyyy");
                        }

                        if (director.ResignedDate != null)
                        {
                            obj.ResignedDate = Convert.ToDateTime(director.ResignedDate).ToString("dd/MM/yyyy");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_DirectorMasterCessationVM SaveCessationOfDirector(UIForm_DirectorMasterCessationVM obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                if (obj.Director_Id > 0)
                {
                    var directorDetails = (from director in entities.BM_DirectorMaster
                                           from present_state in entities.States.Where(k => k.ID == director.Present_StateId).Select(k => k.Name).DefaultIfEmpty()
                                           from present_city in entities.Cities.Where(k => k.ID == director.Present_CityId).Select(k => k.Name).DefaultIfEmpty()
                                           where director.Id == obj.Director_Id
                                           select new
                                           {
                                               director.DIN,
                                               director.PAN,

                                               director.Salutation,
                                               director.FirstName,
                                               director.MiddleName,
                                               director.LastName,

                                               director.Present_Address_Line1,
                                               director.Present_Address_Line2,
                                               present_state,
                                               present_city,
                                               director.Present_PINCode


                                           }).FirstOrDefault();

                    if (directorDetails != null)
                    {
                        obj.DIN = directorDetails.DIN;
                        obj.PAN = directorDetails.PAN;

                        obj.FirstName = (string.IsNullOrEmpty(directorDetails.Salutation) ? "" : directorDetails.Salutation) +
                                        (string.IsNullOrEmpty(directorDetails.FirstName) ? "" : " " + directorDetails.FirstName) +
                                        (string.IsNullOrEmpty(directorDetails.MiddleName) ? "" : " " + directorDetails.MiddleName) +
                                        (string.IsNullOrEmpty(directorDetails.LastName) ? "" : " " + directorDetails.LastName);

                        obj.Present_Address_Line1 = (string.IsNullOrEmpty(directorDetails.Present_Address_Line1) ? "" : directorDetails.Present_Address_Line1) +
                            (string.IsNullOrEmpty(directorDetails.Present_Address_Line2) ? "" : " " + directorDetails.Present_Address_Line2) +
                            (string.IsNullOrEmpty(directorDetails.present_state) ? "" : ", " + directorDetails.present_state) +
                            (string.IsNullOrEmpty(directorDetails.present_city) ? "" : ", " + directorDetails.present_city) +
                            (string.IsNullOrEmpty(directorDetails.Present_PINCode) ? "" : "-" + directorDetails.Present_PINCode);

                    }

                    UIFormData<UIForm_DirectorMasterCessationVM> mapper = new UIFormData<UIForm_DirectorMasterCessationVM>();
                    result = mapper.MapData(result, obj);

                    var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                    objMeetingAgendaTemplateVM.lstControls = result;

                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.RefMasterID;
                        mapping.UpdatedBy = userId;
                        mapping.UpdatedOn = DateTime.Now;

                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }

                    obj.Success = objMeetingAgendaTemplateVM.Success;
                    obj.Error = objMeetingAgendaTemplateVM.Error;
                    obj.Message = objMeetingAgendaTemplateVM.Message;

                    //Commented on 08 Jul 2020 due to Compliance activation
                    //objIDirectorMaster.CessationOfDirector(obj, userId, customerId);
                    //End
                }

            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Appointment of Auditor
        public VMInternalAuditor AppointmentOFInternalAuditor(long meetingId, long agendaId, long mappingId)
        {
            var obj = new VMInternalAuditor();
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                //var result = GetTemplateFields(meetingId, agendaId, mappingId);

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                if (refMasterId > 0)
                {
                    obj = objIAuditorMaster.GetInternalAuditorbyId((long)refMasterId, obj.EntityId);
                }

                //UIFormData<VMInternalAuditor> mapper = new UIFormData<VMInternalAuditor>();
                //var objTempAuditor = new VMInternalAuditor();
                //objTempAuditor = mapper.MapData(objTempAuditor, result);
                //if(objTempAuditor!= null)
                //{
                //    obj.AuditorAppointedForFY = objTempAuditor.AuditorAppointedForFY;
                //}

                obj.MeetingAgengaMappingID = mappingId;
                obj.AgendaID = agendaId;
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.successerrorMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public VMInternalAuditor SaveAppointmentOfInternalAuditor(VMInternalAuditor obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MeetingAgengaMappingID);

                if (obj.Auditor_Id > 0)
                {
                    var auditor = (from row in entities.BM_Auditor_Master
                                   from present_state in entities.States.Where(k => k.ID == row.state_Id).Select(k => k.Name).DefaultIfEmpty()
                                   from present_city in entities.Cities.Where(k => k.ID == row.city_Id).Select(k => k.Name).DefaultIfEmpty()
                                   where row.Id == obj.Auditor_Id && row.IsActive == true
                                   select new
                                   {
                                       row.FullName_FirmName,
                                       row.Reg_MemberNumber,
                                       row.AddressLine1,
                                       row.AddressLine2,
                                       present_state,
                                       present_city,
                                       row.Pin_Number
                                   }).FirstOrDefault();
                    if (auditor != null)
                    {
                        obj.AuditorName = auditor.FullName_FirmName;
                        obj.RegistrationNo = auditor.Reg_MemberNumber;

                        obj.AddressOfAuditor = (string.IsNullOrEmpty(auditor.AddressLine1) ? "" : auditor.AddressLine1) +
                            (string.IsNullOrEmpty(auditor.AddressLine2) ? "" : " " + auditor.AddressLine2) +
                            (string.IsNullOrEmpty(auditor.present_state) ? "" : ", " + auditor.present_state) +
                            (string.IsNullOrEmpty(auditor.present_city) ? "" : ", " + auditor.present_city) +
                            (auditor.Pin_Number == null ? "" : "-" + auditor.Pin_Number);
                    }
                }

                if (obj.Date_of_appointment != null)
                {
                    obj.DateOfAppointment = Convert.ToDateTime(obj.Date_of_appointment).ToString("dd/MM/yyyy");
                }

                obj.AuditorAppointedForFY = (from row in entities.BM_YearMaster
                                             where row.FYID == obj.FYID
                                             select row.FYText).FirstOrDefault();

                UIFormData<VMInternalAuditor> mapper = new UIFormData<VMInternalAuditor>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                //Commented on 11 Sep 2020
                //objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.CustomerId = customerId;
                obj.UserId = userId;

                if (obj.Id == 0)
                {
                    obj = objIAuditorMaster.AddInternalAuditor(obj);
                }
                else
                {
                    obj = objIAuditorMaster.UpdateInternalAuditor(obj);
                }

                if (obj.Id > 0 && obj.successMessage)
                {
                    //Added on 11 Sep 2020
                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MeetingAgengaMappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.Id;
                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.successerrorMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public VM_SecreterialAuditor AppointmentOfSecretarialAuditor(long meetingId, long agendaId, long mappingId)
        {
            var obj = new VM_SecreterialAuditor();
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                //var result = GetTemplateFields(meetingId, agendaId, mappingId);

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                if (refMasterId > 0)
                {
                    obj = objIAuditorMaster.getSecreterialAuditorbyId((long)refMasterId, obj.EntityId);
                }

                //UIFormData<VM_SecreterialAuditor> mapper = new UIFormData<VM_SecreterialAuditor>();
                //var objTempAuditor = new VM_SecreterialAuditor();
                //objTempAuditor = mapper.MapData(objTempAuditor, result);
                //if (objTempAuditor != null)
                //{
                //    obj.AuditorAppointedForFY = objTempAuditor.AuditorAppointedForFY;
                //}

                obj.MeetingAgengaMappingID = mappingId;
                obj.AgendaID = agendaId;
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.successerrorMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public VM_SecreterialAuditor SaveAppointmentOfSecretarialAuditor(VM_SecreterialAuditor obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MeetingAgengaMappingID);

                if (obj.AuditorID > 0)
                {
                    var auditor = (from row in entities.BM_Auditor_Master
                                   from present_state in entities.States.Where(k => k.ID == row.state_Id).Select(k => k.Name).DefaultIfEmpty()
                                   from present_city in entities.Cities.Where(k => k.ID == row.city_Id).Select(k => k.Name).DefaultIfEmpty()
                                   where row.Id == obj.AuditorID && row.IsActive == true
                                   select new
                                   {
                                       row.FullName_FirmName,
                                       row.Reg_MemberNumber,
                                       row.AddressLine1,
                                       row.AddressLine2,
                                       present_state,
                                       present_city,
                                       row.Pin_Number
                                   }).FirstOrDefault();

                    if (auditor != null)
                    {
                        obj.AuditorName = auditor.FullName_FirmName;
                        obj.RegistrationNo = auditor.Reg_MemberNumber;


                        obj.AddressOfAuditor = (string.IsNullOrEmpty(auditor.AddressLine1) ? "" : auditor.AddressLine1) +
                                                    (string.IsNullOrEmpty(auditor.AddressLine2) ? "" : " " + auditor.AddressLine2) +
                                                    (string.IsNullOrEmpty(auditor.present_state) ? "" : ", " + auditor.present_state) +
                                                    (string.IsNullOrEmpty(auditor.present_city) ? "" : ", " + auditor.present_city) +
                                                    (auditor.Pin_Number == null ? "" : "-" + auditor.Pin_Number);
                    }
                }

                if (obj.Date_of_appointment != null)
                {
                    obj.DateOfAppointment = Convert.ToDateTime(obj.Date_of_appointment).ToString("dd/MM/yyyy");
                }
                if (obj.Period_of_AppointmentFromDate != null)
                {
                    obj.Appointed_FromDate = Convert.ToDateTime(obj.Period_of_AppointmentFromDate).ToString("dd/MM/yyyy");
                }
                if (obj.Period_of_AppointmentToDate != null)
                {
                    obj.Appointed_DueDate = Convert.ToDateTime(obj.Period_of_AppointmentToDate).ToString("dd/MM/yyyy");
                }

                obj.AuditorAppointedForFY = (from row in entities.BM_YearMaster
                                             where row.FYID == obj.FYID
                                             select row.FYText).FirstOrDefault();

                UIFormData<VM_SecreterialAuditor> mapper = new UIFormData<VM_SecreterialAuditor>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                //Commented on 11 Sep 2020
                //objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.CustomerId = customerId;
                //obj.UserId = userId;

                if (obj.Id == 0)
                {
                    obj = objIAuditorMaster.AddSecreterialDate(obj);
                }
                else
                {
                    obj = objIAuditorMaster.updateSecreterialDate(obj);
                }

                if (obj.Id > 0 && obj.successMessage)
                {
                    //Added on 11 Sep 2020
                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MeetingAgengaMappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.Id;
                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.successerrorMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Statutoy Auditor
        public VMStatutory_Auditor AppointmentOfFirstStatutoryAuditor(long meetingId, long agendaId, long mappingId, int customerId)
        {
            int natureOfAppointment = 1; // First auditor by Board of directors/members
            return AppointmentOfStatutoryAuditor(meetingId, agendaId, mappingId, natureOfAppointment, customerId);
        }

        public VMStatutory_Auditor AppointmentOfStatutoryAuditorInCaseOfCasualVacancy(long meetingId, long agendaId, long mappingId, int customerId)
        {
            int natureOfAppointment = 4; //Auditor appointed in case of casual vacancy
            return AppointmentOfStatutoryAuditor(meetingId, agendaId, mappingId, natureOfAppointment, customerId);
        }
        public VMStatutory_Auditor AppointmentOfStatutoryAuditor(long meetingId, long agendaId, long mappingId, int natureOfAppointment, int customerId)
        {
            var obj = new VMStatutory_Auditor();
            obj.AuditDeatilsList = new List<VMAuditorDetails> { new VMAuditorDetails() };
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                if (refMasterId > 0)
                {
                    obj = objIAuditorMaster.GetStatutorybyId((int)refMasterId, customerId);
                }
                else
                {
                    if (natureOfAppointment == 1)
                    {
                        obj.IsJoin_Auditor_Appointed = false;
                        obj.Class_of_Company_sec139 = true;
                        obj.AGMNo_TillAppointmentTerminate = "1";
                        obj.EntityIncorporationDate = objIAuditorMaster.GetEntityIncorporationDate(obj.EntityId);
                        obj.AuditDeatilsList[0].AppointedFirstFYDate = obj.EntityIncorporationDate;
                        obj.AuditDeatilsList[0].Number_of_appointedFY = 1;
                    }
                }

                if (natureOfAppointment == 4)//
                {
                    var refMasterIDOriginal = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterIDOriginal).FirstOrDefault();
                    //obj.OriginalAuditorMappingId = refMasterIDOriginal;

                    obj.Class_of_Company_sec139 = true;
                    obj.IsAuditor_Appointed_casualvacancy = true;

                    if (refMasterIDOriginal > 0)
                    {
                        var prevAuditorDetails = (from row in entities.BM_StatutoryAuditor_Mapping
                                                  join rows in entities.BM_Auditor_Master on row.Auditor_ID equals rows.Id
                                                  where row.Id == (int)refMasterIDOriginal
                                                  select new
                                                  {
                                                      row.Id,
                                                      row.Auditor_ID,
                                                      rows.FullName_FirmName,
                                                      rows.Reg_MemberNumber,
                                                      row.Leave_Date,
                                                      row.DateOfResignationSubmit,
                                                      row.ResignforLeave
                                                  }).FirstOrDefault();
                        if (prevAuditorDetails != null)
                        {
                            obj.Person_vacated_office = prevAuditorDetails.Auditor_ID;
                            obj.PreviousAuditorName = prevAuditorDetails.FullName_FirmName;
                            obj.Date_vacancy = prevAuditorDetails.Leave_Date;
                            obj.Membership_Number_or_Registrationfirm = prevAuditorDetails.Reg_MemberNumber;
                            obj.PreviousRegistrationNo = prevAuditorDetails.Reg_MemberNumber;
                            obj.Reasons_casual_vacancy = prevAuditorDetails.ResignforLeave;
                        }
                    }
                }

                if (obj.AuditDeatilsList.Count > 0)
                {
                    obj.Auditor_Number = obj.AuditDeatilsList.Count;
                }

                //
                //var result = GetTemplateFields(meetingId, agendaId, mappingId);
                //UIFormData<VMStatutory_Auditor> mapper = new UIFormData<VMStatutory_Auditor>();
                //obj = mapper.MapData(obj, result);
                //
                obj.MeetingAgengaMappingID = mappingId;
                obj.AgendaID = agendaId;
                obj.Nature_of_Appointment = natureOfAppointment;
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.showMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public VMStatutory_Auditor AppointmentOfStatutoryAuditorRecommendationInBorad(long meetingId, long agendaId, long mappingId, int customerId, int uIFormID)
        {
            int natureOfAppointment = 2; //Auditor appointed in case of casual vacancy
            return AppointmentOfSubSequentStatutoryAuditor(meetingId, agendaId, mappingId, natureOfAppointment, customerId, uIFormID);
        }
        public VMStatutory_Auditor AppointmentOfSubSequentStatutoryAuditor(long meetingId, long agendaId, long mappingId, int natureOfAppointment, int customerId, int uiFormId)
        {
            var obj = new VMStatutory_Auditor();
            obj.AuditDeatilsList = new List<VMAuditorDetails> { new VMAuditorDetails() };
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();

                var EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                if (refMasterId > 0)
                {
                    obj = objIAuditorMaster.GetStatutorybyId((int)refMasterId, customerId);
                }
                else
                {
                    //if (natureOfAppointment == 1)
                    //{
                    //    obj.IsJoin_Auditor_Appointed = false;
                    //    obj.Class_of_Company_sec139 = true;
                    //    obj.AGMNo_TillAppointmentTerminate = "1";
                    //    obj.EntityIncorporationDate = objIAuditorMaster.GetEntityIncorporationDate(obj.EntityId);
                    //    obj.AuditDeatilsList[0].AppointedFirstFYDate = obj.EntityIncorporationDate;
                    //    obj.AuditDeatilsList[0].Number_of_appointedFY = 1;
                    //}

                    obj.IsJoin_Auditor_Appointed = false;
                }

                if (obj.AuditDeatilsList.Count > 0)
                {
                    obj.Auditor_Number = obj.AuditDeatilsList.Count;
                }

                //Fetch Term
                var objTemp = new VMStatutory_Auditor();
                var result = GetTemplateFields(meetingId, agendaId, mappingId);
                UIFormData<VMStatutory_Auditor> mapper = new UIFormData<VMStatutory_Auditor>();
                objTemp = mapper.MapData(objTemp, result);
                obj.Term = objTemp.Term;
                //End

                obj.MeetingAgengaMappingID = mappingId;
                obj.AgendaID = agendaId;
                obj.Nature_of_Appointment = natureOfAppointment;
                obj.UIFormID = uiFormId;
                obj.EntityId = EntityId;
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.showMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public VMStatutory_Auditor SaveAppointmentOfStatutoryAuditor(VMStatutory_Auditor obj, int userId, int customerId)
        {
            var uiformIdTemp = obj.UIFormID;
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MeetingAgengaMappingID);

                //if (obj.AuditorId > 0)
                //{
                //    var auditor = (from row in entities.BM_Auditor_Master
                //                   where row.Id == obj.AuditorId && row.IsActive == true
                //                   select new
                //                   {
                //                       row.FullName_FirmName,
                //                       row.Reg_MemberNumber
                //                   }).FirstOrDefault();
                //    if (auditor != null)
                //    {
                //        obj.AuditorName = auditor.FullName_FirmName;
                //        obj.RegistrationNo = auditor.Reg_MemberNumber;
                //    }
                //}

                if (obj.AuditDeatilsList != null)
                {
                    var auditor_Id = obj.AuditDeatilsList.FirstOrDefault().Auditor_Id;
                    var auditor = (from row in entities.BM_Auditor_Master
                                   from present_state in entities.States.Where(k => k.ID == row.state_Id).Select(k => k.Name).DefaultIfEmpty()
                                   from present_city in entities.Cities.Where(k => k.ID == row.city_Id).Select(k => k.Name).DefaultIfEmpty()
                                   where row.Id == auditor_Id && row.IsActive == true
                                   select new
                                   {
                                       row.FullName_FirmName,
                                       row.Reg_MemberNumber,
                                       row.AddressLine1,
                                       row.AddressLine2,
                                       present_state,
                                       present_city,
                                       row.Pin_Number
                                   }).FirstOrDefault();
                    if (auditor != null)
                    {
                        obj.AuditorName = auditor.FullName_FirmName;
                        obj.RegistrationNo = auditor.Reg_MemberNumber;

                        obj.AddressOfAuditor = (string.IsNullOrEmpty(auditor.AddressLine1) ? "" : auditor.AddressLine1) +
                                                    (string.IsNullOrEmpty(auditor.AddressLine2) ? "" : " " + auditor.AddressLine2) +
                                                    (string.IsNullOrEmpty(auditor.present_state) ? "" : ", " + auditor.present_state) +
                                                    (string.IsNullOrEmpty(auditor.present_city) ? "" : ", " + auditor.present_city) +
                                                    (auditor.Pin_Number == null ? "" : "-" + auditor.Pin_Number);
                    }
                    if (obj.Nature_of_Appointment == 1 || uiformIdTemp == 45)
                    {
                        var firstRow = obj.AuditDeatilsList.FirstOrDefault();
                        if(firstRow != null)
                        {
                            if(firstRow.AppointedFirstFYDate!= null)
                            {
                                obj.FYStartDateStr = Convert.ToDateTime(firstRow.AppointedFirstFYDate).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                obj.FYStartDateStr = "";
                            }

                            if (firstRow.AppointedLastFYDate != null)
                            {
                                obj.FYLastDateStr = Convert.ToDateTime(firstRow.AppointedLastFYDate).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                obj.FYLastDateStr = "";
                            }
                        }
                    }

                    if (obj.Nature_of_Appointment == 4)
                    {
                        var refMasterIDOriginal = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MeetingAgengaMappingID).Select(k => k.RefMasterIDOriginal).FirstOrDefault();
                        obj.OriginalAuditorMappingId = refMasterIDOriginal;

                        #region Previous auditor details
                        if (refMasterIDOriginal > 0)
                        {
                            var auditorPrev = (from row in entities.BM_StatutoryAuditor_Mapping
                                               join rows in entities.BM_Auditor_Master on row.Auditor_ID equals rows.Id
                                               where row.Id == refMasterIDOriginal //&& row.IsActive == true
                                               select new
                                               {
                                                   row.Auditor_ID,
                                                   rows.FullName_FirmName,
                                                   rows.Reg_MemberNumber,
                                                   row.Leave_Date,
                                                   row.ResignforLeave
                                               }).FirstOrDefault();

                            if (auditorPrev != null)
                            {
                                obj.PreviousAuditorName = auditorPrev.FullName_FirmName;
                                obj.Membership_Number_or_Registrationfirm = auditorPrev.Reg_MemberNumber;
                                obj.PreviousRegistrationNo = auditorPrev.Reg_MemberNumber;

                                obj.Person_vacated_office = auditorPrev.Auditor_ID;
                                obj.Date_vacancy = auditorPrev.Leave_Date;
                                obj.Reasons_casual_vacancy = auditorPrev.ResignforLeave;
                            }
                        }
                        #endregion
                    }

                    if(uiformIdTemp == SecretarialConst.UIForms.AUDITOR_RECOMMANDATION_IN_CASE_OF_APPOINTMENT_OF_STAT_IN_BOARD || uiformIdTemp == SecretarialConst.UIForms.APPOINTMENT_OR_REAPPOINTMENT_OF_STATUTORY_AUDITOR_IN_AGM_SUB_SEQUENT)
                    {
                        var firstRow = obj.AuditDeatilsList.FirstOrDefault();
                        if (firstRow != null)
                        {
                            obj.AppointedForNoOfYears = firstRow.Number_of_appointedFY == null ? " " : Convert.ToString(firstRow.Number_of_appointedFY);
                        }
                    }
                }

                if (obj.Id > 0)
                {
                    var dateOfBoardMeeting = entities.BM_StatutoryAuditor_Mapping.Where(k => k.Id == obj.Id).Select(k => k.DateOfBoardMeeting).FirstOrDefault();
                    if (dateOfBoardMeeting != null)
                    {
                        obj.DateOfBoardMeeting = Convert.ToDateTime(dateOfBoardMeeting).ToString("dd/MM/yyyy");
                    }
                }
                if (obj.Date_Appointment != null)
                {
                    obj.DateOfAppointment = Convert.ToDateTime(obj.Date_Appointment).ToString("dd/MM/yyyy");
                }
                //if (obj.Period_of_AppointmentFromDate != null)
                //{
                //    obj.Appointed_FromDate = Convert.ToDateTime(obj.Period_of_AppointmentFromDate).ToString("dd/MM/yyyy");
                //}
                //if (obj.Period_of_AppointmentToDate != null)
                //{
                //    obj.Appointed_DueDate = Convert.ToDateTime(obj.Period_of_AppointmentToDate).ToString("dd/MM/yyyy");
                //}

                UIFormData<VMStatutory_Auditor> mapper = new UIFormData<VMStatutory_Auditor>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                //Commented on 16 Sep 2020
                //objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.CustomerId = customerId;
                //obj.UserId = userId;

                if (obj.Id == 0)
                {
                    obj = objIAuditorMaster.AddStatutoryAuditor(obj);
                }
                else
                {
                    obj = objIAuditorMaster.UpdateStatutoryAuditor(obj);
                }

                if (obj.Id > 0)
                {
                    //Addded on 16 Sep 2020
                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MeetingAgengaMappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.Id;
                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.showMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            obj.UIFormID = uiformIdTemp;
            return obj;
        }
        #endregion

        #region Cost Auditor
        public VM_CostAuditor AppointmentOfOriginalCostAuditor(long meetingId, long agendaId, long mappingId)
        {
            int natureOfIntimation = 1; //Original Appointment
            return AppointmentOfCostAuditor(meetingId, agendaId, mappingId, natureOfIntimation);
        }

        public VM_CostAuditor AppointmentOfCostAuditor(long meetingId, long agendaId, long mappingId, int natureOfIntimation)
        {
            var obj = new VM_CostAuditor();
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                //var result = GetTemplateFields(meetingId, agendaId, mappingId);

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                obj.ResulationNumber = "0";

                if (refMasterId > 0)
                {
                    obj = objIAuditorMaster.getCostAuditorbyId((long)refMasterId, obj.EntityId);
                }

                //UIFormData<VM_CostAuditor> mapper = new UIFormData<VM_CostAuditor>();
                //var objTempAuditor = new VM_CostAuditor();
                //objTempAuditor = mapper.MapData(objTempAuditor, result);
                //if (objTempAuditor != null)
                //{
                //    obj.AuditorAppointedForFY = objTempAuditor.AuditorAppointedForFY;
                //}

                obj.MeetingAgengaMappingID = mappingId;
                obj.AgendaID = agendaId;

                obj.Nature_of_intimation_cost = natureOfIntimation;
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.SuccesserrorMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public VM_CostAuditor SaveAppointmentOfCostAuditor(VM_CostAuditor obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MeetingAgengaMappingID);

                if (obj.AuditorId > 0)
                {
                    var auditor = (from row in entities.BM_Auditor_Master
                                   from present_state in entities.States.Where(k => k.ID == row.state_Id).Select(k => k.Name).DefaultIfEmpty()
                                   from present_city in entities.Cities.Where(k => k.ID == row.city_Id).Select(k => k.Name).DefaultIfEmpty()
                                   where row.Id == obj.AuditorId && row.IsActive == true
                                   select new
                                   {
                                       row.FullName_FirmName,
                                       row.Reg_MemberNumber,
                                       row.AddressLine1,
                                       row.AddressLine2,
                                       present_state,
                                       present_city,
                                       row.Pin_Number
                                   }).FirstOrDefault();

                    if (auditor != null)
                    {
                        obj.Auditor_Name = auditor.FullName_FirmName;
                        obj.RegistrationNo = auditor.Reg_MemberNumber;

                        obj.AddressOfAuditor = (string.IsNullOrEmpty(auditor.AddressLine1) ? "" : auditor.AddressLine1) +
                                                    (string.IsNullOrEmpty(auditor.AddressLine2) ? "" : " " + auditor.AddressLine2) +
                                                    (string.IsNullOrEmpty(auditor.present_state) ? "" : ", " + auditor.present_state) +
                                                    (string.IsNullOrEmpty(auditor.present_city) ? "" : ", " + auditor.present_city) +
                                                    (auditor.Pin_Number == null ? "" : "-" + auditor.Pin_Number);
                    }
                }

                //if (obj.Date_of_appointment != null)
                //{
                //    obj.DateOfAppointment = Convert.ToDateTime(obj.Date_of_appointment).ToString("dd/MM/yyyy");
                //}
                //if (obj.Period_of_AppointmentFromDate != null)
                //{
                //    obj.Appointed_FromDate = Convert.ToDateTime(obj.Period_of_AppointmentFromDate).ToString("dd/MM/yyyy");
                //}
                //if (obj.Period_of_AppointmentToDate != null)
                //{
                //    obj.Appointed_DueDate = Convert.ToDateTime(obj.Period_of_AppointmentToDate).ToString("dd/MM/yyyy");
                //}
                obj.AuditorAppointedForFY = (from row in entities.BM_YearMaster
                                             where row.FYID == obj.FYID
                                             select row.FYText).FirstOrDefault();

                UIFormData<VM_CostAuditor> mapper = new UIFormData<VM_CostAuditor>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                //Commented on 11 Sep 2020
                //objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.CustomerId = customerId;
                //obj.UserId = userId;

                if (obj.Id == 0)
                {
                    obj = objIAuditorMaster.AddCostDate(obj);
                }
                else
                {
                    obj = objIAuditorMaster.updateCostDate(obj);
                }

                if (obj.Id > 0 && obj.successMessage)
                {
                    //Added on 11 Sep 2020
                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MeetingAgengaMappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.Id;
                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                obj.errorMessage = true;
                obj.SuccesserrorMessage = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public VM_CostAuditorRatification RatificationCostAuditor(long meetingId, long agendaId, long mappingId)
        {
            var obj = new VM_CostAuditorRatification();
            try
            {
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<VM_CostAuditorRatification> mapper = new UIFormData<VM_CostAuditorRatification>();
                obj = mapper.MapData(obj, result);

                if (!string.IsNullOrEmpty(obj.YearEndDate))
                {
                    DateTime yearEndDate = DateTime.Now;
                    if (!DateTime.TryParseExact(obj.YearEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out yearEndDate))
                    {
                        obj.YearEndDateOfAuditor = yearEndDate;
                    }
                }
                obj.AgendaID = agendaId;
                obj.MeetingAgengaMappingID = mappingId;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return obj;
        }
        public VM_CostAuditorRatification SaveRatificationCostAuditor(VM_CostAuditorRatification obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MeetingAgengaMappingID);

                if (obj.YearEndDateOfAuditor != null)
                {
                    obj.YearEndDate = Convert.ToDateTime(obj.YearEndDateOfAuditor).ToString("dd/MM/yyyy");
                }

                UIFormData<VM_CostAuditorRatification> mapper = new UIFormData<VM_CostAuditorRatification>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Resignation of Auditor
        public Auditor_ResignationVM ResignationOfAuditor(long meetingId, long agendaId, long mappingId, long uiFormId)
        {
            var obj = new Auditor_ResignationVM();
            try
            {
                switch (uiFormId)
                {
                    case SecretarialConst.UIForms.RESIGNATION_OF_SECRETARIAL_AUDITOR:
                        obj.AuditorType = "SCA";
                        break;
                    case SecretarialConst.UIForms.RESIGNATION_OF_INTERNAL_AUDITOR:
                        obj.AuditorType = "IA";
                        break;
                    case SecretarialConst.UIForms.RESIGNATION_OF_STATUTORY_AUDITOR:
                        obj.AuditorType = "SA";
                        break;
                    case SecretarialConst.UIForms.RESIGNATION_OF_COST_AUDITOR:
                        obj.AuditorType = "CA";
                        break;

                    default:
                        break;
                }
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();

                if (refMasterId > 0)
                {
                    obj.AuditorMappingId = (int)refMasterId;
                    obj._AuditorId = (from row in entities.BM_StatutoryAuditor_Mapping
                                      where row.Id == refMasterId
                                      select row.Auditor_ID
                                     ).FirstOrDefault();
                }

                //var result = GetTemplateFields(meetingId, agendaId, mappingId);
                //UIFormData<Auditor_ResignationVM> mapper = new UIFormData<Auditor_ResignationVM>();
                //obj = mapper.MapData(obj, result);

                DateTime dateOfResignation = DateTime.Now;
                DateTime dateOfResignationSubmit = DateTime.Now;
                if (!string.IsNullOrEmpty(obj.Date_Of_Resignation))
                {
                    DateTime yearEndDate = DateTime.Now;
                    if (!DateTime.TryParseExact(obj.Date_Of_Resignation, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateOfResignation))
                    {
                        obj.DateOfResignation = dateOfResignation;
                    }
                }

                if (!string.IsNullOrEmpty(obj.Date_Of_Resignation_Submit))
                {
                    DateTime yearEndDate = DateTime.Now;
                    if (!DateTime.TryParseExact(obj.Date_Of_Resignation_Submit, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateOfResignationSubmit))
                    {
                        obj.DateOfResignationSubmit = dateOfResignationSubmit;
                    }
                }

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                //if (refMasterId > 0)
                //{
                //    obj.AuditorMappingId = (int) refMasterId;
                //    obj._AuditorId = entities.BM_StatutoryAuditor_Mapping.Where(k => k.Id == refMasterId).Select(k => k.Auditor_ID).FirstOrDefault();
                //}
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public Auditor_ResignationVM SaveResignationOfAuditor(Auditor_ResignationVM obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                if (obj.DateOfResignation != null)
                {
                    obj.Date_Of_Resignation = Convert.ToDateTime(obj.DateOfResignation).ToString("dd/MM/yyyy");
                }

                if (obj.DateOfResignationSubmit != null)
                {
                    obj.Date_Of_Resignation_Submit = Convert.ToDateTime(obj.DateOfResignationSubmit).ToString("dd/MM/yyyy");
                }

                if (obj._AuditorId > 0)
                {
                    var auditorDetails = (from auditor in entities.BM_Auditor_Master
                                          where auditor.Id == obj._AuditorId
                                          select new
                                          {
                                              auditor.FullName_FirmName,
                                              auditor.Reg_MemberNumber
                                          }).FirstOrDefault();

                    if (auditorDetails != null)
                    {
                        obj.Auditor_Name = auditorDetails.FullName_FirmName;
                        obj.RegistrationNo = auditorDetails.Reg_MemberNumber;

                        var detailsId = (from details in entities.BM_StatutoryAuditor_Mapping
                                         where details.Auditor_ID == obj._AuditorId && details.EntityId == obj.EntityId && details.flag == obj.AuditorType &&
                                            details.IsDeleted == false
                                         select details.Id).FirstOrDefault();

                        obj.AuditorMappingId = detailsId;
                    }

                    UIFormData<Auditor_ResignationVM> mapper = new UIFormData<Auditor_ResignationVM>();
                    result = mapper.MapData(result, obj);

                    var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                    objMeetingAgendaTemplateVM.lstControls = result;

                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.AuditorMappingId;
                        mapping.UpdatedBy = userId;
                        mapping.UpdatedOn = DateTime.Now;

                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }

                    obj.Success = objMeetingAgendaTemplateVM.Success;
                    obj.Error = objMeetingAgendaTemplateVM.Error;
                    obj.Message = objMeetingAgendaTemplateVM.Message;

                    //objIDirectorMaster.CessationOfDirector(obj, userId, customerId);
                }

            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_AuditorResignationVM ResignationOfAuditorNew(long meetingId, long agendaId, long mappingId, long uiFormId)
        {
            var obj = new UIForm_AuditorResignationVM();
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();

                if (refMasterId > 0)
                {
                    obj.RefMasterID = (long)refMasterId;
                    var auditor = (from row in entities.BM_StatutoryAuditor_Mapping
                                   join rows in entities.BM_Auditor_Master on row.Auditor_ID equals rows.Id
                                   from present_state in entities.States.Where(k => k.ID == rows.state_Id).Select(k => k.Name).DefaultIfEmpty()
                                   from present_city in entities.Cities.Where(k => k.ID == rows.city_Id).Select(k => k.Name).DefaultIfEmpty()
                                   where row.Id == refMasterId
                                   select new
                                   {
                                       row.Id,
                                       rows.FullName_FirmName,
                                       rows.Reg_MemberNumber,
                                       row.Leave_Date,
                                       row.DateOfResignationSubmit,
                                       row.ResignforLeave,
                                       row.FileDataId,

                                       rows.AddressLine1,
                                       rows.AddressLine2,
                                       present_state,
                                       present_city,
                                       rows.Pin_Number
                                   }
                                     ).FirstOrDefault();

                    if (auditor != null)
                    {
                        obj.AuditorName = auditor.FullName_FirmName;
                        obj.RegistrationNo = auditor.Reg_MemberNumber;
                        obj.DateOfResignation = auditor.Leave_Date;
                        obj.DateOfResignationSubmit = auditor.DateOfResignationSubmit;
                        obj.ResonOfResignation = auditor.ResignforLeave;
                        obj.FileDataId = auditor.FileDataId;

                        obj.AddressOfAuditor = (string.IsNullOrEmpty(auditor.AddressLine1) ? "" : auditor.AddressLine1) +
                            (string.IsNullOrEmpty(auditor.AddressLine2) ? "" : " " + auditor.AddressLine2) +
                            (string.IsNullOrEmpty(auditor.present_state) ? "" : ", " + auditor.present_state) +
                            (string.IsNullOrEmpty(auditor.present_city) ? "" : ", " + auditor.present_city) +
                            (auditor.Pin_Number == null ? "" : "-" + auditor.Pin_Number);
                    }
                }

                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public UIForm_AuditorResignationVM SaveResignationDetailsOfAuditor(UIForm_AuditorResignationVM obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                if (obj.DateOfResignation != null)
                {
                    obj.Date_Of_Resignation = Convert.ToDateTime(obj.DateOfResignation).ToString("dd/MM/yyyy");
                }

                if (obj.DateOfResignationSubmit != null)
                {
                    obj.Date_Of_Resignation_Submit = Convert.ToDateTime(obj.DateOfResignationSubmit).ToString("dd/MM/yyyy");
                }

                UIFormData<UIForm_AuditorResignationVM> mapper = new UIFormData<UIForm_AuditorResignationVM>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.Success = objMeetingAgendaTemplateVM.Success;
                obj.Error = objMeetingAgendaTemplateVM.Error;
                obj.Message = objMeetingAgendaTemplateVM.Message;
                //objIDirectorMaster.CessationOfDirector(obj, userId, customerId);

            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Financial Result
        public UIForm_FinancialResult_ListedVM FinancialResultListed(long meetingId, long agendaId, long mappingId, int entityTypeId)
        {
            var obj = new UIForm_FinancialResult_ListedVM();
            try
            {
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<UIForm_FinancialResult_ListedVM> mapper = new UIFormData<UIForm_FinancialResult_ListedVM>();
                obj = mapper.MapData(obj, result);

                if (entityTypeId == SecretarialConst.EntityTypeID.PRIVATE)
                {
                    obj.EntityId = (from meeting in entities.BM_Meetings
                                    where meeting.MeetingID == meetingId
                                    select meeting.EntityId
                                    ).FirstOrDefault();
                }
                else
                {
                    #region Generate quarter ended for Listed & Public Company
                    var details = (from mapping in entities.BM_MeetingAgendaMapping
                                   join meeting in entities.BM_Meetings on mapping.MeetingID equals meeting.MeetingID
                                   join fy in entities.BM_YearMaster on meeting.FY equals fy.FYID
                                   where mapping.MeetingAgendaMappingID == mappingId && mapping.IsDeleted == false && mapping.IsActive == true
                                   select new
                                   {
                                       meeting.EntityId,
                                       meeting.Customer_Id,
                                       meeting.Quarter_,
                                       fy.FromDate,
                                   }).FirstOrDefault();

                    if (details != null)
                    {
                        obj.EntityId = details.EntityId;

                        if (details.FromDate.HasValue)
                        {
                            DateTime fyStartDate = Convert.ToDateTime(details.FromDate);
                            DateTime quarterEndDate = DateTime.Now;

                            int quarter = 0;
                            switch (details.Quarter_)
                            {
                                case "Q1":
                                    quarter = 1;
                                    break;
                                case "Q2":
                                    quarter = 2;
                                    break;
                                case "Q3":
                                    quarter = 3;
                                    break;
                                case "Q4":
                                    quarter = 4;
                                    break;
                                default:
                                    break;
                            }

                            if (quarter > 0)
                            {
                                quarterEndDate = fyStartDate.AddMonths(quarter * 3).AddDays(-1);
                                obj.QuarterEndedForTheMeeting = quarterEndDate.ToString("dd MMMM yyyy");
                            }

                            #region Chairman details
                            var lstDir = objIDirectorMaster.GetDirectorListOfEntity((int)obj.EntityId, SecretarialConst.MeetingTypeID.BOARD);
                            if (lstDir != null)
                            {
                                var chairman = lstDir.Where(k => k.PositionId == 1).FirstOrDefault();
                                if (chairman != null)
                                {
                                    obj.NameOfAuthorisedDirector = chairman.FullName;
                                    obj.DesignationOfAuthorisedDirector = chairman.Designation;
                                    obj.DINOfAuthorisedDirector = chairman.DIN;
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }

                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                obj.EntityTypeId = entityTypeId;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_FinancialResult_ListedVM SaveFinancialResultListed(UIForm_FinancialResult_ListedVM obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                UIFormData<UIForm_FinancialResult_ListedVM> mapper = new UIFormData<UIForm_FinancialResult_ListedVM>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.Success = objMeetingAgendaTemplateVM.Success;
                obj.Error = objMeetingAgendaTemplateVM.Error;
                obj.Message = objMeetingAgendaTemplateVM.Message;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_FinancialResultYearEnded_PrivateVM FinancialResultForYearEnded_Private(long meetingId, long agendaId, long mappingId, int entityTypeId)
        {
            var obj = new UIForm_FinancialResultYearEnded_PrivateVM();
            try
            {
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<UIForm_FinancialResultYearEnded_PrivateVM> mapper = new UIFormData<UIForm_FinancialResultYearEnded_PrivateVM>();
                obj = mapper.MapData(obj, result);

                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                obj.EntityTypeId = entityTypeId;
                obj.EntityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId).Select(k => k.EntityId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public UIForm_FinancialResultYearEnded_PrivateVM SaveFinancialResultForYearEnded_Private(UIForm_FinancialResultYearEnded_PrivateVM obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                #region Director Details
                if (!string.IsNullOrEmpty(obj.Id_of_1st_director))
                {
                    long directorId = 0;
                    if (!long.TryParse(obj.Id_of_1st_director, out directorId))
                    {
                        directorId = 0;
                    }

                    if (directorId > 0)
                    {
                        var director = (from dir in entities.BM_DirectorMaster
                                        where dir.Id == directorId
                                        select new
                                        {
                                            dir.DIN,
                                            dir.Salutation,
                                            dir.FirstName,
                                            dir.MiddleName,
                                            dir.LastName,
                                        }).FirstOrDefault();

                        if (director != null)
                        {
                            obj.DIN_of_1st_director = director.DIN;
                            obj.Name_of_1st_director = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation) +
                                                (string.IsNullOrEmpty(director.FirstName) ? "" : " " + director.FirstName) +
                                                (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.MiddleName) +
                                                (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName);
                        }
                    }
                    else
                    {
                        obj.Name_of_1st_director = "";
                        obj.DIN_of_1st_director = "";
                    }
                }
                else
                {
                    obj.Name_of_1st_director = "";
                    obj.DIN_of_1st_director = "";
                }

                if (!string.IsNullOrEmpty(obj.Id_of_2nd_director))
                {
                    long directorId = 0;
                    if (!long.TryParse(obj.Id_of_2nd_director, out directorId))
                    {
                        directorId = 0;
                    }

                    if (directorId > 0)
                    {
                        var director = (from dir in entities.BM_DirectorMaster
                                        where dir.Id == directorId
                                        select new
                                        {
                                            dir.DIN,
                                            dir.Salutation,
                                            dir.FirstName,
                                            dir.MiddleName,
                                            dir.LastName,
                                        }).FirstOrDefault();

                        if (director != null)
                        {
                            obj.DIN_of_2nd_director = director.DIN;
                            obj.Name_of_2nd_director = (string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation) +
                                                (string.IsNullOrEmpty(director.FirstName) ? "" : " " + director.FirstName) +
                                                (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.MiddleName) +
                                                (string.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName);
                        }
                    }
                    else
                    {
                        obj.Name_of_2nd_director = "";
                        obj.DIN_of_2nd_director = "";
                    }
                }
                else
                {
                    obj.Name_of_2nd_director = "";
                    obj.DIN_of_2nd_director = "";
                }
                #endregion
                UIFormData<UIForm_FinancialResultYearEnded_PrivateVM> mapper = new UIFormData<UIForm_FinancialResultYearEnded_PrivateVM>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.Success = objMeetingAgendaTemplateVM.Success;
                obj.Error = objMeetingAgendaTemplateVM.Error;
                obj.Message = objMeetingAgendaTemplateVM.Message;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_FinancialResultYearEnded_ListedVM FinancialResultForYearEnded_Listed(long meetingId, long agendaId, long mappingId, int entityTypeId)
        {
            var obj = new UIForm_FinancialResultYearEnded_ListedVM();
            try
            {
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<UIForm_FinancialResultYearEnded_ListedVM> mapper = new UIFormData<UIForm_FinancialResultYearEnded_ListedVM>();
                obj = mapper.MapData(obj, result);

                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                obj.EntityTypeId = entityTypeId;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public UIForm_FinancialResultYearEnded_ListedVM SaveFinancialResultForYearEnded_Listed(UIForm_FinancialResultYearEnded_ListedVM obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                UIFormData<UIForm_FinancialResultYearEnded_ListedVM> mapper = new UIFormData<UIForm_FinancialResultYearEnded_ListedVM>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.Success = objMeetingAgendaTemplateVM.Success;
                obj.Error = objMeetingAgendaTemplateVM.Error;
                obj.Message = objMeetingAgendaTemplateVM.Message;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = "Server Error Occure";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Calling GM
        public UIForm_Calling_GeneralMeetingVM CallingGeneralMeeting(long meetingId, long agendaId, long mappingId, long uIFormId)
        {
            var obj = new UIForm_Calling_GeneralMeetingVM();
            try
            {
                var result = GetTemplateFields(meetingId, agendaId, mappingId);

                UIFormData<UIForm_Calling_GeneralMeetingVM> mapper = new UIFormData<UIForm_Calling_GeneralMeetingVM>();
                obj = mapper.MapData(obj, result);

                var meeting = (from row in entities.BM_Meetings
                               where row.MeetingID == meetingId
                               select new
                               {
                                   row.EntityId,
                                   row.Entity_Type,
                                   row.MeetingTypeId,
                                   row.MeetingCircular,
                                   row.FY
                               }).FirstOrDefault();
                if (meeting != null)
                {
                    obj.EntityId = meeting.EntityId;
                    obj.EntityTypeId = meeting.Entity_Type;

                    if(uIFormId == SecretarialConst.UIForms.CALLING_OF_EGM)
                    {
                        obj.GMTypeId = SecretarialConst.MeetingTypeID.EGM;
                    }
                    else
                    {
                        obj.GMTypeId = SecretarialConst.MeetingTypeID.AGM;
                    }

                    //Set default Serial Number
                    if (string.IsNullOrEmpty(obj.SerialNo_))
                    {
                        //var srNo = objIMeeting_Service.GetNextMeetingSrNo((int)obj.EntityId, meeting.MeetingTypeId, meeting.MeetingCircular, (int)meeting.FY);
                        var srNo = objIMeeting_Service.GetNextMeetingSrNo((int)obj.EntityId, obj.GMTypeId, meeting.MeetingCircular, (int)meeting.FY);
                        obj.SerialNo_ = srNo == null ? "1" : Convert.ToString(srNo);
                    }
                }

                obj.Meeting_ID_ = meetingId;
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public UIForm_Calling_GeneralMeetingVM SaveCallingGeneralMeeting(UIForm_Calling_GeneralMeetingVM obj, int userId, int customerId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);

                if (string.IsNullOrEmpty(obj.SerialNo_))
                {
                    obj.Error = true;
                    obj.Message = "Please enter serial number.";
                    return obj;
                }
                var srNo = Convert.ToInt32(obj.SerialNo_);
                var fyId = Convert.ToInt16(obj.FYID);
                var fyText = (from row in entities.BM_YearMaster
                              where row.FYID == fyId
                              select row.FYText).FirstOrDefault();

                obj.FYText = fyText;

                if (obj.GMTypeId == SecretarialConst.MeetingTypeID.AGM)
                {
                    var isAGMExists = objIMeeting_Service.CheckAGMExistsOrNotByFY((int)obj.EntityId, fyId);

                    if (isAGMExists)
                    {
                        obj.Error = true;
                        obj.Message = "AGM already exists for " + fyText;
                        return obj;
                    }

                    var isAGMSrNoExists = objIMeeting_Service.CheckAGMExistsOrNotBySrNo((int)obj.EntityId, srNo);
                    if (isAGMSrNoExists)
                    {
                        obj.Error = true;
                        obj.Message = "AGM Serial Number already exists.";
                        return obj;
                    }
                }

                if (obj.EntityTypeId == SecretarialConst.EntityTypeID.PRIVATE)
                {
                    obj.IsEVoting = "No";
                }
                else if (obj.EntityTypeId == SecretarialConst.EntityTypeID.LISTED)
                {
                    obj.IsEVoting = "Yes";
                }
                UIFormData<UIForm_Calling_GeneralMeetingVM> mapper = new UIFormData<UIForm_Calling_GeneralMeetingVM>();
                result = mapper.MapData(result, obj);

                var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                objMeetingAgendaTemplateVM.lstControls = result;

                objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                obj.Success = objMeetingAgendaTemplateVM.Success;
                obj.Error = objMeetingAgendaTemplateVM.Error;
                obj.Message = objMeetingAgendaTemplateVM.Message;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Books of Accoutns
        public EntityMasterBooksOfAccountsVM BooksOfAccoutns(long meetingId, long agendaId, long mappingId)
        {
            var obj = new EntityMasterBooksOfAccountsVM();
            try
            {
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                if (refMasterId > 0)
                {
                    obj.BOA_Id = (int)refMasterId;
                    var details = (from row in entities.BM_EntityMasterBooksOfAccounts
                                   from state in entities.States.Where(k => k.ID == row.StateId).DefaultIfEmpty()
                                   where row.Id == refMasterId
                                   select new
                                   {
                                       row.Id,
                                       row.AddressLine1,
                                       row.AddressLine2,
                                       row.CityName,
                                       row.StateId,
                                       state.Name,
                                       row.CountyId,
                                       row.Pincode
                                   }).FirstOrDefault();

                    if (details != null)
                    {
                        obj.BOA_AddressLine1 = details.AddressLine1;
                        obj.BOA_AddressLine2 = details.AddressLine2;
                        obj.BOA_CityName = details.CityName;
                        obj.BOA_StateId = details.StateId;
                        obj.BOA_StateName = details.Name;
                        obj.BOA_Pincode = details.Pincode;
                    }
                }

                obj.Meeting_ID_ = meetingId;
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public EntityMasterBooksOfAccountsVM SaveBooksOfAccoutns(EntityMasterBooksOfAccountsVM obj, int userId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);
                var objEntityMasterBooksOfAccountsTemplateVM = new EntityMasterBooksOfAccountsTemplateVM();

                if (obj.BOA_Id > 0)
                {
                    var _booksOfAccDetails = (from row in entities.BM_EntityMasterBooksOfAccounts
                                              where row.Id == obj.BOA_Id
                                              select row).FirstOrDefault();

                    if (_booksOfAccDetails != null)
                    {
                        _booksOfAccDetails.AddressLine1 = obj.BOA_AddressLine1;
                        _booksOfAccDetails.AddressLine2 = obj.BOA_AddressLine2;
                        _booksOfAccDetails.CityName = obj.BOA_CityName;
                        _booksOfAccDetails.StateId = obj.BOA_StateId;
                        _booksOfAccDetails.Pincode = obj.BOA_Pincode;

                        _booksOfAccDetails.UpdatedBy = userId;
                        _booksOfAccDetails.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.updateSuccess;
                    }
                }
                else
                {
                    var _booksOfAccDetails = new BM_EntityMasterBooksOfAccounts();
                    _booksOfAccDetails.AddressLine1 = obj.BOA_AddressLine1;
                    _booksOfAccDetails.AddressLine2 = obj.BOA_AddressLine2;
                    _booksOfAccDetails.CityName = obj.BOA_CityName;
                    _booksOfAccDetails.StateId = obj.BOA_StateId;
                    _booksOfAccDetails.Pincode = obj.BOA_Pincode;
                    _booksOfAccDetails.IsActive = false;
                    _booksOfAccDetails.IsDeleted = false;

                    _booksOfAccDetails.CreatedBy = userId;
                    _booksOfAccDetails.CreatedOn = DateTime.Now;
                    entities.BM_EntityMasterBooksOfAccounts.Add(_booksOfAccDetails);
                    entities.SaveChanges();

                    obj.BOA_Id = _booksOfAccDetails.Id;
                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.saveSuccess;
                }

                if (obj.BOA_Id > 0 && obj.Success)
                {
                    if (obj.BOA_StateId > 0)
                    {
                        obj.BOA_StateName = (from row in entities.States
                                             where row.ID == obj.BOA_StateId
                                             select row.Name).FirstOrDefault();
                    }

                    objEntityMasterBooksOfAccountsTemplateVM.AddressAtWhichBooksToBeKept = (string.IsNullOrEmpty(obj.BOA_AddressLine1) ? "" : obj.BOA_AddressLine1) +
                                                                                            (string.IsNullOrEmpty(obj.BOA_AddressLine2) ? "" : " " + obj.BOA_AddressLine2) +
                                                                                            (string.IsNullOrEmpty(obj.BOA_StateName) ? "" : ", " + obj.BOA_StateName) +
                                                                                            (string.IsNullOrEmpty(obj.BOA_CityName) ? "" : ", " + obj.BOA_CityName) +
                                                                                            (obj.BOA_Pincode == null ? "" : "-" + obj.BOA_Pincode);


                    UIFormData<EntityMasterBooksOfAccountsTemplateVM> mapper = new UIFormData<EntityMasterBooksOfAccountsTemplateVM>();
                    result = mapper.MapData(result, objEntityMasterBooksOfAccountsTemplateVM);

                    var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                    objMeetingAgendaTemplateVM.lstControls = result;

                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.BOA_Id;
                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Shifting of Register Office
        public ShiftingRegisterOfficeVM WithinLocalLimit(long meetingId, long agendaId, long mappingId)
        {
            var obj = new ShiftingRegisterOfficeVM();
            try
            {
                var entityId = (from row in entities.BM_Meetings
                                where row.MeetingID == meetingId
                                select row.EntityId).FirstOrDefault();
                var OldAddressFlag = false;
                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                if (refMasterId > 0)
                {
                    obj = (from row in entities.BM_EntityShiftingRO_History
                           from state in entities.States.Where(k => k.ID == row.Regi_StateId).DefaultIfEmpty()
                           from city in entities.States.Where(k => k.ID == row.Regi_CityId).DefaultIfEmpty()
                           where row.Id == refMasterId
                           select new ShiftingRegisterOfficeVM
                           {
                               Shifting_Id = row.Id,
                               EffectFrom = row.EffectFrom,
                               Regi_Address_Line1 = row.Regi_Address_Line1,
                               Regi_Address_Line2 = row.Regi_Address_Line2,
                               Regi_CityId = row.Regi_CityId,
                               Regi_StateId = row.Regi_StateId,
                               Regi_PINCode = row.Regi_PINCode,
                               RegisteredOfficeAddress = row.Regi_Address_Old,

                               Regi_Address_Line1New = row.Regi_Address_Line1New,
                               Regi_Address_Line2New = row.Regi_Address_Line2New,
                               Regi_CityIdNew = row.Regi_CityIdNew,
                               Regi_StateIdNew = row.Regi_StateIdNew,
                               Regi_PINCodeNew = row.Regi_PINCodeNew
                           }).FirstOrDefault();

                    if (obj == null)
                    {
                        obj = new ShiftingRegisterOfficeVM();
                    }
                    else
                    {
                        OldAddressFlag = true;
                    }

                    obj.Shifting_Id = (int)refMasterId;
                }

                if (OldAddressFlag == false)
                {
                    #region set Old Address
                    if (entityId > 0)
                    {
                        var _objEntityMaster = (from row in entities.BM_EntityMaster
                                                from state in entities.States.Where(k => k.ID == row.Regi_StateId).DefaultIfEmpty()
                                                from city in entities.Cities.Where(k => k.ID == row.Regi_CityId).DefaultIfEmpty()
                                                where row.Id == entityId
                                                select new
                                                {
                                                    row.Id,
                                                    row.Regi_Address_Line1,
                                                    row.Regi_Address_Line2,
                                                    row.Regi_StateId,
                                                    StateName = state.Name,
                                                    row.Regi_CityId,
                                                    CityName = city.Name,
                                                    row.Regi_PINCode
                                                }).FirstOrDefault();
                        if (_objEntityMaster != null)
                        {
                            obj.Regi_CityIdNew = _objEntityMaster.Regi_CityId;
                            obj.Regi_StateIdNew = _objEntityMaster.Regi_StateId;

                            obj.RegisteredOfficeAddress = (string.IsNullOrEmpty(_objEntityMaster.Regi_Address_Line1) ? "" : _objEntityMaster.Regi_Address_Line1) +
                                                                (string.IsNullOrEmpty(_objEntityMaster.Regi_Address_Line2) ? "" : " " + _objEntityMaster.Regi_Address_Line2) +
                                                                (string.IsNullOrEmpty(_objEntityMaster.StateName) ? "" : ", " + _objEntityMaster.StateName) +
                                                                (string.IsNullOrEmpty(_objEntityMaster.CityName) ? "" : ", " + _objEntityMaster.CityName) +
                                                                (_objEntityMaster.Regi_PINCode == null ? "" : "-" + _objEntityMaster.Regi_PINCode);
                        }
                    }
                    #endregion
                }

                obj.Meeting_ID_ = meetingId;
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
                obj.Shifting_EntityId = entityId;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public ShiftingRegisterOfficeVM SaveWithinLocalLimit(ShiftingRegisterOfficeVM obj, int userId)
        {
            try
            {
                var result = GetUIFormColumnsData(obj.AgendaID, obj.MappingID);
                var objShiftingRegisterOfficeTemplateVM = new ShiftingRegisterOfficeTemplateVM();

                if (obj.Shifting_Id > 0)
                {
                    var _shiftingHistory = (from row in entities.BM_EntityShiftingRO_History
                                            where row.Id == obj.Shifting_Id
                                            select row).FirstOrDefault();

                    if (_shiftingHistory != null)
                    {
                        _shiftingHistory.EffectFrom = obj.EffectFrom;
                        _shiftingHistory.Regi_Address_Line1New = obj.Regi_Address_Line1New;
                        _shiftingHistory.Regi_Address_Line2New = obj.Regi_Address_Line2New;
                        _shiftingHistory.Regi_CityIdNew = obj.Regi_CityIdNew;
                        _shiftingHistory.Regi_StateIdNew = obj.Regi_StateIdNew;
                        _shiftingHistory.Regi_PINCodeNew = obj.Regi_PINCodeNew;

                        _shiftingHistory.UpdatedBy = userId;
                        _shiftingHistory.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();

                        obj.Success = true;
                        obj.Message = SecretarialConst.Messages.updateSuccess;

                        #region set Old Address
                        var stateOld = (from row in entities.States
                                        where row.ID == _shiftingHistory.Regi_StateId
                                        select row.Name).FirstOrDefault();

                        var cityOld = (from row in entities.Cities
                                       where row.ID == _shiftingHistory.Regi_CityId
                                       select row.Name).FirstOrDefault();

                        objShiftingRegisterOfficeTemplateVM.RegisteredOfficeAddress = (string.IsNullOrEmpty(_shiftingHistory.Regi_Address_Line1) ? "" : _shiftingHistory.Regi_Address_Line1) +
                                                                                            (string.IsNullOrEmpty(_shiftingHistory.Regi_Address_Line2) ? "" : " " + _shiftingHistory.Regi_Address_Line2) +
                                                                                            (string.IsNullOrEmpty(stateOld) ? "" : ", " + stateOld) +
                                                                                            (string.IsNullOrEmpty(cityOld) ? "" : ", " + cityOld) +
                                                                                            (_shiftingHistory.Regi_PINCode == null ? "" : "-" + _shiftingHistory.Regi_PINCode);
                        #endregion
                    }
                }
                else
                {
                    var _shiftingHistory = new BM_EntityShiftingRO_History();

                    #region set Old Address
                    if (obj.Shifting_EntityId > 0)
                    {
                        var _objEntityMaster = (from row in entities.BM_EntityMaster
                                                from state in entities.States.Where(k => k.ID == row.Regi_StateId).DefaultIfEmpty()
                                                from city in entities.Cities.Where(k => k.ID == row.Regi_CityId).DefaultIfEmpty()
                                                where row.Id == obj.Shifting_EntityId
                                                select new
                                                {
                                                    row.Id,
                                                    row.Regi_Address_Line1,
                                                    row.Regi_Address_Line2,
                                                    row.Regi_StateId,
                                                    StateName = state.Name,
                                                    row.Regi_CityId,
                                                    CityName = city.Name,
                                                    row.Regi_PINCode
                                                }).FirstOrDefault();
                        if (_objEntityMaster != null)
                        {
                            objShiftingRegisterOfficeTemplateVM.RegisteredOfficeAddress = (string.IsNullOrEmpty(_objEntityMaster.Regi_Address_Line1) ? "" : _objEntityMaster.Regi_Address_Line1) +
                                                                (string.IsNullOrEmpty(_objEntityMaster.Regi_Address_Line2) ? "" : " " + _objEntityMaster.Regi_Address_Line2) +
                                                                (string.IsNullOrEmpty(_objEntityMaster.StateName) ? "" : ", " + _objEntityMaster.StateName) +
                                                                (string.IsNullOrEmpty(_objEntityMaster.CityName) ? "" : ", " + _objEntityMaster.CityName) +
                                                                (_objEntityMaster.Regi_PINCode == null ? "" : "-" + _objEntityMaster.Regi_PINCode);

                            _shiftingHistory.Regi_Address_Line1 = (string.IsNullOrEmpty(_objEntityMaster.Regi_Address_Line1) ? "" : _objEntityMaster.Regi_Address_Line1);
                            _shiftingHistory.Regi_Address_Line2 = (string.IsNullOrEmpty(_objEntityMaster.Regi_Address_Line2) ? "" : _objEntityMaster.Regi_Address_Line2);
                            _shiftingHistory.Regi_StateId = _objEntityMaster.Regi_StateId;
                            _shiftingHistory.Regi_CityId = _objEntityMaster.Regi_CityId;
                            _shiftingHistory.Regi_PINCode = _objEntityMaster.Regi_PINCode;

                            _shiftingHistory.Regi_Address_Old = objShiftingRegisterOfficeTemplateVM.RegisteredOfficeAddress;
                        }
                    }
                    #endregion

                    _shiftingHistory.EntityId = obj.Shifting_EntityId;
                    _shiftingHistory.EffectFrom = obj.EffectFrom;
                    _shiftingHistory.Regi_Address_Line1New = obj.Regi_Address_Line1New;
                    _shiftingHistory.Regi_Address_Line2New = obj.Regi_Address_Line2New;
                    _shiftingHistory.Regi_CityIdNew = obj.Regi_CityIdNew;
                    _shiftingHistory.Regi_StateIdNew = obj.Regi_StateIdNew;
                    _shiftingHistory.Regi_PINCodeNew = obj.Regi_PINCodeNew;

                    _shiftingHistory.CreatedBy = userId;
                    _shiftingHistory.CreatedOn = DateTime.Now;
                    _shiftingHistory.IsActive = true;
                    _shiftingHistory.IsDeleted = false;
                    entities.BM_EntityShiftingRO_History.Add(_shiftingHistory);
                    entities.SaveChanges();

                    obj.Shifting_Id = _shiftingHistory.Id;
                    obj.Success = true;
                    obj.Message = SecretarialConst.Messages.saveSuccess;

                }

                if (obj.Shifting_Id > 0 && obj.Success)
                {
                    if (obj.EffectFrom != null)
                    {
                        objShiftingRegisterOfficeTemplateVM.DateOfShifting = Convert.ToDateTime(obj.EffectFrom).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        objShiftingRegisterOfficeTemplateVM.DateOfShifting = "";
                    }

                    var stateNew = "";
                    if (obj.Regi_StateIdNew > 0)
                    {
                        stateNew = (from row in entities.States
                                    where row.ID == obj.Regi_StateIdNew
                                    select row.Name).FirstOrDefault();
                    }


                    var cityNew = "";
                    if (obj.Regi_CityIdNew > 0)
                    {
                        cityNew = (from row in entities.Cities
                                   where row.ID == obj.Regi_CityIdNew
                                   select row.Name).FirstOrDefault();
                    }

                    objShiftingRegisterOfficeTemplateVM.NewRegisteredOfficeAddress = (string.IsNullOrEmpty(obj.Regi_Address_Line1New) ? "" : obj.Regi_Address_Line1New) +
                                                                                        (string.IsNullOrEmpty(obj.Regi_Address_Line2New) ? "" : " " + obj.Regi_Address_Line2New) +
                                                                                        (string.IsNullOrEmpty(stateNew) ? "" : ", " + stateNew) +
                                                                                        (string.IsNullOrEmpty(cityNew) ? "" : ", " + cityNew) +
                                                                                        (obj.Regi_PINCodeNew == null ? "" : "-" + obj.Regi_PINCodeNew);


                    UIFormData<ShiftingRegisterOfficeTemplateVM> mapper = new UIFormData<ShiftingRegisterOfficeTemplateVM>();
                    result = mapper.MapData(result, objShiftingRegisterOfficeTemplateVM);

                    var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                    objMeetingAgendaTemplateVM.lstControls = result;

                    objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userId);

                    var mapping = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == obj.MappingID && k.IsDeleted == false).FirstOrDefault();
                    if (mapping != null)
                    {
                        mapping.RefMasterID = obj.Shifting_Id;
                        entities.Entry(mapping).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        #endregion

        #region Share Capital - Increase in Authorised Capital
        public IncreaseInAuthorisedCapitalVM IncreaseInAuthorisedCapital(long meetingId, long agendaId, long mappingId, int customerId, int userId)
        {
            var obj = new IncreaseInAuthorisedCapitalVM();
            try
            {
                var entityId = (from row in entities.BM_Meetings
                                where row.MeetingID == meetingId
                                select row.EntityId).FirstOrDefault();

                obj = new IncreaseInAuthorisedCapitalVM()
                {
                    AuthorizedCapital = new AuthorizedCapital() { Entity_Id = entityId },
                    VMCapitalData = new VMCapitalData() { shares = new List<Shares>() },
                    VMPrifrenscShare = new VMPrifrenscShare() { pshares = new pShares() },

                    VMUnclassified = new VMUnclassified(),
                    VMDebentures = new VMDebentures()
                };


                var refMasterId = entities.BM_MeetingAgendaMapping.Where(k => k.MeetingAgendaMappingID == mappingId && k.MeetingID == meetingId).Select(k => k.RefMasterID).FirstOrDefault();
                if (refMasterId > 0)
                {
                    var objAuthorizedCapital = (from row in entities.BM_CapitalMaster_RevisedHistory
                                             where row.IdRevised == refMasterId
                                             select new
                                             {
                                                 AuthorizedCapval = row.AuthorizedCapital,
                                                 AuthorisedId = (long) row.CapitalId,
                                                 Entity_Id = row.Entity_Id,

                                                 TnAuthorisedCapital = row.TotNoAuthorizedCapita,
                                                 TaAuthorisedCapital = row.TotamtAuthorizedCapita,

                                                 TnAuthorisedCapitalRevised = row.TotNoAuthorizedCapitaRevised,
                                                 TaAuthorisedCapitalRevised = row.TotamtAuthorizedCapitaRevised,

                                                 //Pre
                                                 pTnAuthorisedCapital = row.Pri_TotNoAuthorizedCapita,
                                                 pTaAuthorisedCapital = row.Pri_TotamtAuthorizedCapita,

                                                 IsPrefrence = row.IsPrefrence,

                                                 pTnAuthorisedCapitalRevised = row.Pri_TotNoAuthorizedCapitaRevised,
                                                 pTaAuthorisedCapitalRevised = row.Pri_TotamtAuthorizedCapitaRevised,
                                                 //End
                                                 AuthorisedIdRevised = row.IdRevised
                                             }).FirstOrDefault();

                    if(objAuthorizedCapital != null)
                    {
                        obj.AuthorizedCapital = new AuthorizedCapital()
                        {
                            AuthorizedCapval = objAuthorizedCapital.AuthorizedCapval,
                            AuthorisedId = objAuthorizedCapital.AuthorisedId,
                            Entity_Id = objAuthorizedCapital.Entity_Id,

                            AuthorisedIdRevised = objAuthorizedCapital.AuthorisedIdRevised,
                        };

                        obj.AuthorizedCapital.AdditionEQNo = (objAuthorizedCapital.TnAuthorisedCapitalRevised - objAuthorizedCapital.TnAuthorisedCapital);
                        obj.AuthorizedCapital.AdditionEQAmout = (objAuthorizedCapital.TaAuthorisedCapitalRevised - objAuthorizedCapital.TaAuthorisedCapital);

                        obj.AuthorizedCapital.AdditionPRENo = (objAuthorizedCapital.pTnAuthorisedCapitalRevised - objAuthorizedCapital.pTnAuthorisedCapital);
                        obj.AuthorizedCapital.AdditionPREAmout = (objAuthorizedCapital.pTaAuthorisedCapitalRevised - objAuthorizedCapital.pTaAuthorisedCapital);

                        obj.AuthorizedCapital.AuthorizedCapvalDiffernce = obj.AuthorizedCapital.AdditionEQAmout + obj.AuthorizedCapital.AdditionPREAmout;
                        obj.AuthorizedCapital.AuthorizedCapvalRevised = objAuthorizedCapital.AuthorizedCapval + obj.AuthorizedCapital.AuthorizedCapvalDiffernce;

                        obj.VMCapitalData.Id = obj.AuthorizedCapital.AuthorisedId;
                        obj.VMPrifrenscShare.Id = obj.AuthorizedCapital.AuthorisedId;
                        obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                        obj.VMDebentures.Id = obj.AuthorizedCapital.AuthorisedId;

                        obj.VMCapitalData.IdRevised = obj.AuthorizedCapital.AuthorisedIdRevised;
                        obj.VMPrifrenscShare.IdRevised = obj.AuthorizedCapital.AuthorisedIdRevised;

                        #region EQ
                        obj.VMCapitalData.EntityId = objAuthorizedCapital.Entity_Id;
                        obj.VMCapitalData.TnAuthorisedCapital = objAuthorizedCapital.TnAuthorisedCapital;
                        obj.VMCapitalData.TaAuthorisedCapital = objAuthorizedCapital.TaAuthorisedCapital;

                        obj.VMCapitalData.TnAuthorisedCapitalRevised = objAuthorizedCapital.TnAuthorisedCapitalRevised;
                        obj.VMCapitalData.TaAuthorisedCapitalRevised = objAuthorizedCapital.TaAuthorisedCapitalRevised;
                        obj.VMCapitalData.IdRevised = objAuthorizedCapital.AuthorisedIdRevised;
                        #endregion

                        if (obj.VMCapitalData != null)
                        {
                            #region EQ Details
                            obj.VMCapitalData.EntityId = entityId;
                            obj.VMCapitalData.MeetingAgengaMappingIDEQ = mappingId;
                            obj.VMCapitalData.AgengaIDEQ = agendaId;
                            if (obj.VMCapitalData.IdRevised > 0)
                            {
                                var _objEquietysub_Dtls = (from row in entities.BM_Share_RevisedHistory
                                                           where row.CapitalMasterIdRevised == obj.VMCapitalData.IdRevised
                                                             && row.Sharetype == "E"
                                                             && row.IsActive == true
                                                           select new Shares
                                                           {
                                                               Equ_sharesID = (long) row.ShareId,
                                                               AuthorisedCapital1 = row.No_AuhorisedCapital,
                                                               AuthorisedCapital2 = row.Nominalper_AuhorisedCapital,
                                                               AuthorisedCapital3 = row.totamt_AuhorisedCapital,
                                                               
                                                               sharesID = (int)row.Shares_Class,

                                                               AuthorisedCapital1Revised = row.No_AuhorisedCapitalRevised,
                                                               AuthorisedCapital2Revised = row.Nominalper_AuhorisedCapitalRevised,
                                                               AuthorisedCapital3Revised = row.totamt_AuhorisedCapitalRevised,
                                                               Equ_sharesIDRevised = row.IdRevised
                                                           }).ToList();

                                obj.VMCapitalData.shares = new List<Shares>();

                                if (_objEquietysub_Dtls.Count > 0)
                                {
                                    obj.VMCapitalData.count = _objEquietysub_Dtls.Count;
                                    obj.VMCapitalData.shares.AddRange(_objEquietysub_Dtls);
                                }
                                else
                                {
                                    obj.VMCapitalData.shares.Add(new Shares());
                                }
                            }
                            else
                            {
                                obj.VMCapitalData = new VMCapitalData();
                                obj.VMCapitalData.shares = new List<Shares>();
                                obj.VMCapitalData.shares.Add(new Shares());
                            }
                            #endregion

                            #region PRE
                            obj.VMPrifrenscShare.pTnAuthorisedCapital = objAuthorizedCapital.pTnAuthorisedCapital;
                            obj.VMPrifrenscShare.pTaAuthorisedCapital = objAuthorizedCapital.pTaAuthorisedCapital;

                            obj.VMPrifrenscShare.IsPrefrence = objAuthorizedCapital.IsPrefrence;

                            obj.VMPrifrenscShare.pTnAuthorisedCapitalRevised = objAuthorizedCapital.pTnAuthorisedCapitalRevised;
                            obj.VMPrifrenscShare.pTaAuthorisedCapitalRevised = objAuthorizedCapital.pTaAuthorisedCapitalRevised;
                            obj.VMPrifrenscShare.IdRevised = objAuthorizedCapital.AuthorisedIdRevised;
                            #region Pre Details
                            if (obj.VMPrifrenscShare.IdRevised > 0)
                            {
                                obj.VMPrifrenscShare.EntityIdPref = entityId;
                                obj.VMPrifrenscShare.MeetingAgengaMappingIDPref = mappingId;
                                obj.VMPrifrenscShare.AgengaIDPref = agendaId;
                                obj.VMPrifrenscShare.PsharesCoupons = new List<pShareCouponRate>();

                                var _objPrifrencelist = (from row in entities.BM_Share_RevisedHistory
                                                         where row.CapitalMasterIdRevised == obj.VMPrifrenscShare.IdRevised && row.Sharetype == "P" && row.IsActive == true
                                                         select new pShareCouponRate
                                                         {
                                                             psharesID = (long) row.ShareId,
                                                             pAuthorisedCapital1 = row.No_AuhorisedCapital,
                                                             pAuthorisedCapital2 = row.Nominalper_AuhorisedCapital,
                                                             pAuthorisedCapital3 = row.totamt_AuhorisedCapital,

                                                             //Set Revised Coupen rate & capital type - Don't change
                                                             Pref_CapitalType = row.Pref_CapitalTypeRevised,
                                                             Pref_CoupenRate = row.Pref_CoupenRateRevised,
                                                             //End

                                                             pAuthorisedCapital1Revised = row.No_AuhorisedCapitalRevised,
                                                             pAuthorisedCapital2Revised = row.Nominalper_AuhorisedCapitalRevised,
                                                             pAuthorisedCapital3Revised = row.totamt_AuhorisedCapitalRevised,
                                                             psharesIDRevised = row.IdRevised,
                                                         }).ToList();

                                if (_objPrifrencelist != null)
                                {
                                    obj.VMPrifrenscShare.pcount = _objPrifrencelist.Count;
                                    obj.VMPrifrenscShare.PsharesCoupons.AddRange(_objPrifrencelist);
                                }
                            }
                            else
                            {
                                obj.VMPrifrenscShare = new VMPrifrenscShare();
                                obj.VMPrifrenscShare.PsharesCoupons = new List<pShareCouponRate>();
                            }
                            #endregion
                            #endregion

                            #region Unclassified
                            //obj.VMUnclassified = objCapitalMaster.GetUnclassified(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, obj.AuthorizedCapital.AuthorisedId);

                            //if (obj.VMUnclassified != null)
                            //{
                            //    obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                            //}
                            //else
                            //{
                            //    obj.VMUnclassified = new VMUnclassified();
                            //    obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                            //}

                            //obj.VMDebentures = objCapitalMaster.GetDebenger(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, obj.AuthorizedCapital.AuthorisedId);

                            //if (obj.VMDebentures == null)
                            //{

                            //    obj.VMDebentures = new VMDebentures();
                            //    obj.VMDebentures.Id = obj.AuthorizedCapital.AuthorisedId;
                            //}
                            #endregion
                        }

                    }
                }
                else
                {
                    obj.AuthorizedCapital = objICapitalMaster.GetAuthorisedCapital(userId, customerId, entityId);
                    if (obj.AuthorizedCapital != null)
                    {
                        //CapitalID = obj.AuthorizedCapital.AuthorisedId;
                        obj.VMCapitalData.Id = obj.AuthorizedCapital.AuthorisedId;
                        obj.VMPrifrenscShare.Id = obj.AuthorizedCapital.AuthorisedId;
                        obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                        obj.VMDebentures.Id = obj.AuthorizedCapital.AuthorisedId;

                        obj.AuthorizedCapital.AdditionEQNo = 0;
                        obj.AuthorizedCapital.AdditionEQAmout = 0;

                        obj.AuthorizedCapital.AdditionPRENo = 0;
                        obj.AuthorizedCapital.AdditionPREAmout = 0;

                        obj.AuthorizedCapital.AuthorizedCapvalDiffernce = 0;
                        obj.AuthorizedCapital.AuthorizedCapvalRevised = obj.AuthorizedCapital.AuthorizedCapval;


                        obj.VMCapitalData = objICapitalMaster.GetEquityCapital(userId, customerId, entityId, obj.AuthorizedCapital.AuthorisedId);
                        if (obj.VMCapitalData != null)
                        {
                            obj.VMCapitalData.EntityId = entityId;
                            obj.VMCapitalData.MeetingAgengaMappingIDEQ = mappingId;
                            if (obj.VMCapitalData.Id > 0)
                            {
                                var _objEquietysub_Dtls = objICapitalMaster.GetEquityCapital_subdtls(userId, customerId, obj.VMCapitalData.Id, entityId);
                                obj.VMCapitalData.shares = new List<Shares>();

                                if (_objEquietysub_Dtls.Count > 0)
                                {
                                    obj.VMCapitalData.count = _objEquietysub_Dtls.Count;
                                    obj.VMCapitalData.shares.AddRange(_objEquietysub_Dtls);
                                }
                                else
                                {
                                    obj.VMCapitalData.shares.Add(new Shares());
                                }
                            }
                            else
                            {
                                obj.VMCapitalData = new VMCapitalData();
                                obj.VMCapitalData.shares = new List<Shares>();
                                obj.VMCapitalData.shares.Add(new Shares());
                            }

                            obj.VMPrifrenscShare = objICapitalMaster.GetPrifrenceCapital(userId, customerId, entityId, obj.VMCapitalData.Id);
                            if (obj.VMPrifrenscShare != null)
                            {
                                obj.VMPrifrenscShare.EntityIdPref = entityId;
                                obj.VMPrifrenscShare.MeetingAgengaMappingIDPref = mappingId;
                                obj.VMPrifrenscShare.PsharesCoupons = new List<pShareCouponRate>();
                                var _objPrifrencelist = objICapitalMaster.GetPrifrenceCapitalList(userId, customerId, entityId, obj.VMCapitalData.Id);
                                if (_objPrifrencelist != null)
                                {
                                    obj.VMPrifrenscShare.pcount = _objPrifrencelist.Count;
                                    obj.VMPrifrenscShare.PsharesCoupons.AddRange(_objPrifrencelist);
                                }
                            }
                            else
                            {
                                obj.VMPrifrenscShare = new VMPrifrenscShare();
                                obj.VMPrifrenscShare.PsharesCoupons = new List<pShareCouponRate>();
                            }

                            #region Unclassified
                            //obj.VMUnclassified = objCapitalMaster.GetUnclassified(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, obj.AuthorizedCapital.AuthorisedId);

                            //if (obj.VMUnclassified != null)
                            //{
                            //    obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                            //}
                            //else
                            //{
                            //    obj.VMUnclassified = new VMUnclassified();
                            //    obj.VMUnclassified.Id = obj.AuthorizedCapital.AuthorisedId;
                            //}

                            //obj.VMDebentures = objCapitalMaster.GetDebenger(AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID), EntityId, obj.AuthorizedCapital.AuthorisedId);

                            //if (obj.VMDebentures == null)
                            //{

                            //    obj.VMDebentures = new VMDebentures();
                            //    obj.VMDebentures.Id = obj.AuthorizedCapital.AuthorisedId;
                            //}
                            #endregion
                        }
                    }
                    else
                    {
                        obj.Error = true;
                        obj.Message = "Please fill capital master.";
                    }
                }

                obj.Meeting_ID_ = meetingId;
                obj.AgendaID = agendaId;
                obj.MappingID = mappingId;
            }
            catch (Exception ex)
            {
                obj.Error = true;
                obj.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public Message GetRevisedHistoryId(int entityId, long meetingAgendaMappingId, int userId, out long capitalMasterIdRevised)
        {
            var result = new Message();
            capitalMasterIdRevised = 0;
            try
            {
                var refMasterId = (from row in entities.BM_MeetingAgendaMapping
                                   where row.MeetingAgendaMappingID == meetingAgendaMappingId
                                   select row.RefMasterID).FirstOrDefault();
                if(refMasterId > 0)
                {
                    capitalMasterIdRevised = (long)refMasterId;
                    result.Success = true;
                }

                var capitalId = (from row in entities.BM_CapitalMaster_RevisedHistory
                                 where row.Entity_Id == entityId && row.MeetingAgendaMappingId == meetingAgendaMappingId &&
                                 row.IsDeleted == false
                                 select row.IdRevised).FirstOrDefault();
                if (capitalId > 0)
                {
                    capitalMasterIdRevised = capitalId;
                    result.Success = true;
                }
                else
                {
                    #region Set Default data
                    var objCapitalMaster = (from row in entities.BM_CapitalMaster
                                            where row.Entity_Id == entityId && row.IsActive == true
                                            select row).FirstOrDefault();
                    if (objCapitalMaster != null)
                    {
                        #region Capital Master Data
                        var objCapitalMasterRevised = new BM_CapitalMaster_RevisedHistory()
                        {
                            IdRevised = 0,
                            CapitalId = objCapitalMaster.Id,
                            MeetingAgendaMappingId = meetingAgendaMappingId,
                            Entity_Id = objCapitalMaster.Entity_Id,
                            CustomerId = objCapitalMaster.CustomerId,
                            //MeetingDate = objCapitalMaster.MeetingDate,
                            //GMDate = objCapitalMaster.GMDate,
                            AuthorizedCapital = objCapitalMaster.AuthorizedCapital,
                            AuthorizedCapitalRevised = objCapitalMaster.AuthorizedCapital,
                            IsDebenger = objCapitalMaster.IsDebenger,
                            IsPrefrence = objCapitalMaster.IsPrefrence,
                            IsPrefrenceRevised = objCapitalMaster.IsPrefrence,
                            IsUnclassified = objCapitalMaster.IsUnclassified,
                            TotNoAuthorizedCapita = objCapitalMaster.TotNoAuthorizedCapita,
                            TotNoAuthorizedCapitaRevised = objCapitalMaster.TotNoAuthorizedCapita,
                            TotNoIssuedCapital = objCapitalMaster.TotNoIssuedCapital,
                            TotNoSubscribCapital = objCapitalMaster.TotNoSubscribCapital,
                            TotNoPaidupCapital = objCapitalMaster.TotNoPaidupCapital,
                            TotamtAuthorizedCapita = objCapitalMaster.TotamtAuthorizedCapita,
                            TotamtAuthorizedCapitaRevised = objCapitalMaster.TotamtAuthorizedCapita,
                            TotamtNoIssuedCapital = objCapitalMaster.TotamtNoIssuedCapital,
                            TotamtNoSubscribCapital = objCapitalMaster.TotamtNoSubscribCapital,
                            TotamtNoPaidupCapital = objCapitalMaster.TotamtNoPaidupCapital,
                            Pri_TotNoAuthorizedCapita = objCapitalMaster.Pri_TotNoAuthorizedCapita,
                            Pri_TotNoAuthorizedCapitaRevised = objCapitalMaster.Pri_TotNoAuthorizedCapita,
                            Pri_TotNoIssuedCapital = objCapitalMaster.Pri_TotNoIssuedCapital,
                            Pri_TotNoSubscribCapital = objCapitalMaster.Pri_TotNoSubscribCapital,
                            Pri_TotNoPaidupCapital = objCapitalMaster.Pri_TotNoPaidupCapital,
                            Pri_TotamtAuthorizedCapita = objCapitalMaster.Pri_TotamtAuthorizedCapita,
                            Pri_TotamtAuthorizedCapitaRevised = objCapitalMaster.Pri_TotamtAuthorizedCapita,
                            Pri_TotamtNoIssuedCapital = objCapitalMaster.Pri_TotamtNoIssuedCapital,
                            Pri_TotamtNoSubscribCapital = objCapitalMaster.Pri_TotamtNoSubscribCapital,
                            Pri_TotamtNoPaidupCapital = objCapitalMaster.Pri_TotamtNoPaidupCapital,
                            Pri_CapitalType = objCapitalMaster.Pri_CapitalType,
                            Pri_CoupenRate = objCapitalMaster.Pri_CoupenRate,
                            Unclassified_AuhorisedCapital = objCapitalMaster.Unclassified_AuhorisedCapital,
                            Unclassified_AuhorisedCapitalRevised = objCapitalMaster.Unclassified_AuhorisedCapital,
                            ECountOld = 0,
                            PCountOld = 0,
                            IsActive = false,
                            IsDeleted = false,
                            Createdby = userId,
                            CreatedOn = DateTime.Now
                        };
                        entities.BM_CapitalMaster_RevisedHistory.Add(objCapitalMasterRevised);
                        entities.SaveChanges();
                        #endregion

                        #region Class Details
                        if (objCapitalMasterRevised.IdRevised > 0)
                        {
                            var objShareDetailsList = (from row in entities.BM_Share
                                                       where row.CapitalMasterId == objCapitalMaster.Id && row.IsActive == true
                                                       select row).ToList();
                            if (objShareDetailsList != null)
                            {
                                foreach (var objShareDetails in objShareDetailsList)
                                {
                                    var objShareDetailsRevised = new BM_Share_RevisedHistory()
                                    {
                                        IdRevised = 0,
                                        ShareId = objShareDetails.Id,
                                        CapitalMasterIdRevised = (int)objCapitalMasterRevised.IdRevised,
                                        CapitalMasterId = objShareDetails.CapitalMasterId,
                                        CustomerId = objShareDetails.CustomerId,
                                        Sharetype = objShareDetails.Sharetype,
                                        Shares_Class = objShareDetails.Shares_Class,
                                        No_AuhorisedCapital = objShareDetails.No_AuhorisedCapital,
                                        No_AuhorisedCapitalRevised = objShareDetails.No_AuhorisedCapital,
                                        No_IssuedCapital = objShareDetails.No_IssuedCapital,
                                        No_SubscribedCapital = objShareDetails.No_SubscribedCapital,
                                        No_Paid_upCapital = objShareDetails.No_Paid_upCapital,
                                        Nominalper_AuhorisedCapital = objShareDetails.Nominalper_AuhorisedCapital,
                                        Nominalper_AuhorisedCapitalRevised = objShareDetails.Nominalper_AuhorisedCapital,
                                        Nominalper_IssuedCapital = objShareDetails.Nominalper_IssuedCapital,
                                        Nominalper_SubscribedCapital = objShareDetails.Nominalper_SubscribedCapital,
                                        Nominalper_Paid_upCapital = objShareDetails.Nominalper_Paid_upCapital,
                                        totamt_AuhorisedCapital = objShareDetails.totamt_AuhorisedCapital,
                                        totamt_AuhorisedCapitalRevised = objShareDetails.totamt_AuhorisedCapital,
                                        totamt_IssuedCapital = objShareDetails.totamt_IssuedCapital,
                                        totamt_SubscribedCapital = objShareDetails.totamt_SubscribedCapital,
                                        totamt_upCapital = objShareDetails.totamt_upCapital,
                                        IsActive = true,
                                        Createdby = userId,
                                        Createdon = DateTime.Now,
                                        Pref_CapitalType = objShareDetails.Pref_CapitalType,
                                        Pref_CoupenRate = objShareDetails.Pref_CoupenRate,
                                        Pref_CapitalTypeRevised = objShareDetails.Pref_CapitalType,
                                        Pref_CoupenRateRevised = objShareDetails.Pref_CoupenRate,
                                    };

                                    entities.BM_Share_RevisedHistory.Add(objShareDetailsRevised);
                                    entities.SaveChanges();
                                }
                            }

                            #region set RefMaster in MeetingAgendaMappingId
                            var objMeetingAgendaMappingId = (from row in entities.BM_MeetingAgendaMapping
                                                             where row.MeetingAgendaMappingID == meetingAgendaMappingId
                                                             select row).FirstOrDefault();
                            if(objMeetingAgendaMappingId != null)
                            {
                                objMeetingAgendaMappingId.RefMasterID = objCapitalMasterRevised.IdRevised;
                                entities.SaveChanges();
                            }
                            #endregion

                            capitalMasterIdRevised = objCapitalMasterRevised.IdRevised;
                        }
                        #endregion

                        result.Success = true;
                    }
                    else
                    {
                        result.Error = true;
                        result.Message = "";
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
            }
            return result;
        }
        public VMCapitalData AddEquietydtls(VMCapitalData _objequity, int userID, int CustomerId)
        {
            decimal TAOEquityAuth = 0;
            decimal TNOEquityAuth = 0;


            decimal TNOEquityAuthRevised = 0;
            decimal TAOEquityAuthRevised = 0;
            try
            {
                #region get Revised Id
                if (_objequity.IdRevised <= 0)
                {
                    long capitalIdRevisedTemp = 0;
                    var checkResult = GetRevisedHistoryId(_objequity.EntityId, _objequity.MeetingAgengaMappingIDEQ, userID, out capitalIdRevisedTemp);
                    if (checkResult.Error)
                    {
                        _objequity.errorMessage = true;
                        return _objequity;
                    }

                    _objequity.IdRevised = capitalIdRevisedTemp;
                }
                #endregion

                if (_objequity.IdRevised > 0)
                {
                    var objdeleteshares = (from row in entities.BM_Share_RevisedHistory
                                           where row.CapitalMasterIdRevised == _objequity.IdRevised
                                           && row.IsActive == true && row.Sharetype == "E"
                                           && row.ShareId == 0
                                           select row).ToList();

                    if (objdeleteshares.Count > 0)
                    {
                        foreach (var items in objdeleteshares)
                        {
                            items.IsActive = false;
                            entities.SaveChanges();
                        }
                    }

                    foreach (var item in _objequity.shares)
                    {
                        var objshares = (from row in entities.BM_Share_RevisedHistory
                                         where (row.IdRevised == item.Equ_sharesIDRevised || (row.ShareId == item.Equ_sharesID && row.ShareId > 0))
                                         && row.CapitalMasterIdRevised == _objequity.IdRevised
                                         select row).FirstOrDefault();

                        if (objshares != null)
                        {
                            objshares.Shares_Class = item.sharesID;
                            objshares.No_AuhorisedCapitalRevised = item.AuthorisedCapital1Revised;
                            objshares.Nominalper_AuhorisedCapitalRevised = item.AuthorisedCapital2Revised;
                            objshares.totamt_AuhorisedCapitalRevised = item.AuthorisedCapital3Revised;

                            TNOEquityAuth = TNOEquityAuth + (item.AuthorisedCapital1);
                            TAOEquityAuth = TAOEquityAuth + item.AuthorisedCapital3;

                            TNOEquityAuthRevised = TNOEquityAuthRevised + (item.AuthorisedCapital1Revised);
                            TAOEquityAuthRevised = TAOEquityAuthRevised + item.AuthorisedCapital3Revised;


                            objshares.Sharetype = "E";
                            objshares.IsActive = true;

                            objshares.Updatedby = userID;
                            objshares.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }
                        else
                        {
                            BM_Share_RevisedHistory obj = new BM_Share_RevisedHistory();
                            obj.ShareId = 0;
                            obj.Shares_Class = item.sharesID;
                            obj.CapitalMasterId = 0;
                            obj.CapitalMasterIdRevised = (int) _objequity.IdRevised;
                            obj.No_AuhorisedCapital = item.AuthorisedCapital1;
                            obj.No_AuhorisedCapitalRevised = item.AuthorisedCapital1Revised;
                            obj.No_IssuedCapital = 0;
                            obj.No_Paid_upCapital = 0;
                            obj.No_SubscribedCapital = 0;
                            obj.Nominalper_AuhorisedCapital = item.AuthorisedCapital2;
                            obj.Nominalper_AuhorisedCapitalRevised = item.AuthorisedCapital2Revised;
                            obj.Nominalper_IssuedCapital = 0;
                            obj.Nominalper_Paid_upCapital = 0;
                            obj.Nominalper_SubscribedCapital = 0;
                            obj.totamt_AuhorisedCapitalRevised = item.AuthorisedCapital3Revised;
                            obj.totamt_IssuedCapital = 0;
                            obj.totamt_upCapital = 0;
                            obj.totamt_SubscribedCapital = 0;

                            TNOEquityAuth = TNOEquityAuth + (item.AuthorisedCapital1);
                            TAOEquityAuth = TAOEquityAuth + item.AuthorisedCapital3;

                            TNOEquityAuthRevised = TNOEquityAuthRevised + (item.AuthorisedCapital1Revised);
                            TAOEquityAuthRevised = TAOEquityAuthRevised + item.AuthorisedCapital3Revised;

                            obj.Sharetype = "E";
                            obj.IsActive = true;
                            obj.CustomerId = CustomerId;

                            obj.Createdby = userID;
                            obj.Createdon = DateTime.Now;

                            entities.BM_Share_RevisedHistory.Add(obj);
                            entities.SaveChanges();
                        }
                    }

                    _objequity.TnAuthorisedCapital = TNOEquityAuth;
                    _objequity.TaAuthorisedCapital = TAOEquityAuth;

                    _objequity.TnAuthorisedCapitalRevised = TNOEquityAuthRevised;
                    _objequity.TaAuthorisedCapitalRevised = TAOEquityAuthRevised;

                    _objequity.SuccessErrorMsg = "Saved successfully";
                    _objequity.Message = true;

                    var capitalDtls = (from row in entities.BM_CapitalMaster_RevisedHistory
                                       where row.IdRevised == _objequity.IdRevised
                                       select row).FirstOrDefault();
                    if (capitalDtls != null)
                    {
                        capitalDtls.TotNoAuthorizedCapitaRevised = _objequity.TnAuthorisedCapitalRevised;
                        capitalDtls.TotamtAuthorizedCapitaRevised = _objequity.TaAuthorisedCapitalRevised;

                        var authorisedCapitalIncreased =  (capitalDtls.TotamtAuthorizedCapitaRevised - capitalDtls.TotamtAuthorizedCapita) + (capitalDtls.Pri_TotamtAuthorizedCapitaRevised - capitalDtls.Pri_TotamtAuthorizedCapita);
                        var authorisedCapitalRevised = capitalDtls.AuthorizedCapital +  (authorisedCapitalIncreased > 0 ? authorisedCapitalIncreased : 0 );
                        capitalDtls.AuthorizedCapitalRevised = authorisedCapitalRevised;

                        entities.SaveChanges();

                        #region Save Template Fields
                        var objIncreaseInAuthCapitalTemplateVM = new IncreaseInAuthCapitalTemplateVM();

                        var FaceValueOfEquityShare = (from row in entities.BM_Share_RevisedHistory
                                                      where row.CapitalMasterIdRevised == _objequity.IdRevised
                                                      && row.IsActive == true && row.Sharetype == "E"
                                                      && row.Nominalper_AuhorisedCapitalRevised > 0
                                                      select row.Nominalper_AuhorisedCapitalRevised).FirstOrDefault();

                        var FaceValueOfPreferenceShare = (from row in entities.BM_Share_RevisedHistory
                                                          where row.CapitalMasterIdRevised == _objequity.IdRevised
                                                          && row.IsActive == true && row.Sharetype == "P"
                                                          && row.Nominalper_AuhorisedCapitalRevised > 0
                                                          select row.Nominalper_AuhorisedCapitalRevised).FirstOrDefault();

                        objIncreaseInAuthCapitalTemplateVM.ExistingAuthorisedShareCapital = capitalDtls.AuthorizedCapital;
                        objIncreaseInAuthCapitalTemplateVM.IncreasedAuthorisedShareCapital = capitalDtls.AuthorizedCapitalRevised;
                        objIncreaseInAuthCapitalTemplateVM.NumberOfEquityShares = capitalDtls.TotNoAuthorizedCapita;
                        objIncreaseInAuthCapitalTemplateVM.FaceValueOfEquityShare = FaceValueOfEquityShare;
                        objIncreaseInAuthCapitalTemplateVM.EquityShareCapital = capitalDtls.TotamtAuthorizedCapita;

                        objIncreaseInAuthCapitalTemplateVM.IncreasedNumberOfEquityShares = capitalDtls.TotNoAuthorizedCapitaRevised;
                        objIncreaseInAuthCapitalTemplateVM.IncreasedEquityShareCapital = capitalDtls.TotamtAuthorizedCapitaRevised;

                        objIncreaseInAuthCapitalTemplateVM.NumberOfPreferenceShares = capitalDtls.Pri_TotNoAuthorizedCapita;
                        objIncreaseInAuthCapitalTemplateVM.FaceValueOfPreferenceShare = FaceValueOfPreferenceShare;
                        objIncreaseInAuthCapitalTemplateVM.PreferenceShareCapital = capitalDtls.Pri_TotamtAuthorizedCapita;
                        objIncreaseInAuthCapitalTemplateVM.IncreasedNumberOfPreferenceShare = capitalDtls.Pri_TotNoAuthorizedCapitaRevised;
                        objIncreaseInAuthCapitalTemplateVM.IncreasedPreferenceShareCapital = capitalDtls.Pri_TotamtAuthorizedCapitaRevised;

                        var result = GetUIFormColumnsData(_objequity.AgengaIDEQ, _objequity.MeetingAgengaMappingIDEQ);
                        UIFormData<IncreaseInAuthCapitalTemplateVM> mapper = new UIFormData<IncreaseInAuthCapitalTemplateVM>();
                        result = mapper.MapData(result, objIncreaseInAuthCapitalTemplateVM);

                        var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                        objMeetingAgendaTemplateVM.lstControls = result;

                        objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userID);
                        #endregion
                    }
                    else
                    {
                        _objequity.SuccessErrorMsg = "Something went wrong";
                        _objequity.errorMessage = true;
                    }
                }
                else
                {
                    _objequity.SuccessErrorMsg = "Something went wrong";
                    _objequity.errorMessage = true;

                }
            }
            catch (Exception ex)
            {
                _objequity.SuccessErrorMsg = "Sarver error occured";
                _objequity.errorMessage = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objequity;
        }

        public VMPrifrenscShare AddPrefrenceMultiple(VMPrifrenscShare _objequity, int userID, int CustomerId)
        {
            decimal TAOEquityAuth = 0;
            decimal TNOEquityAuth = 0;


            decimal TNOEquityAuthRevised = 0;
            decimal TAOEquityAuthRevised = 0;
            try
            {
                #region get Revised Id
                if (_objequity.IdRevised <= 0)
                {
                    long capitalIdRevisedTemp = 0;
                    var checkResult = GetRevisedHistoryId(_objequity.EntityIdPref, _objequity.MeetingAgengaMappingIDPref, userID, out capitalIdRevisedTemp);
                    if (checkResult.Error)
                    {
                        _objequity.errorMessage = true;
                        return _objequity;
                    }

                    _objequity.IdRevised = capitalIdRevisedTemp;
                }
                #endregion

                if (_objequity.IdRevised > 0)
                {
                    var objdeleteshares = (from row in entities.BM_Share_RevisedHistory
                                           where row.CapitalMasterIdRevised == _objequity.IdRevised
                                           && row.IsActive == true && row.Sharetype == "P"
                                           && row.ShareId == 0
                                           select row).ToList();

                    if (objdeleteshares.Count > 0)
                    {
                        foreach (var items in objdeleteshares)
                        {
                            items.IsActive = false;
                            entities.SaveChanges();
                        }
                    }

                    foreach (var item in _objequity.PsharesCoupons)
                    {
                        var objshares = (from row in entities.BM_Share_RevisedHistory
                                         where (row.IdRevised == item.psharesIDRevised || ( row.ShareId == item.psharesID && row.ShareId > 0))
                                         && row.CapitalMasterIdRevised == _objequity.IdRevised
                                         select row).FirstOrDefault();

                        if (objshares != null)
                        {
                            objshares.No_AuhorisedCapitalRevised = item.pAuthorisedCapital1Revised;
                            objshares.Nominalper_AuhorisedCapital = item.pAuthorisedCapital2Revised;
                            objshares.totamt_AuhorisedCapitalRevised = item.pAuthorisedCapital3Revised;

                            objshares.Pref_CapitalTypeRevised = item.Pref_CapitalType;
                            objshares.Pref_CoupenRateRevised = item.Pref_CoupenRate;

                            TNOEquityAuth = TNOEquityAuth + (item.pAuthorisedCapital1);
                            TAOEquityAuth = TAOEquityAuth + item.pAuthorisedCapital3;

                            TNOEquityAuthRevised = TNOEquityAuthRevised + (item.pAuthorisedCapital1Revised);
                            TAOEquityAuthRevised = TAOEquityAuthRevised + item.pAuthorisedCapital3Revised;

                            objshares.Sharetype = "P";
                            objshares.IsActive = true;

                            objshares.Updatedby = userID;
                            objshares.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();
                        }
                        else
                        {
                            BM_Share_RevisedHistory obj = new BM_Share_RevisedHistory();
                            obj.ShareId = 0;
                            obj.Shares_Class = 0;
                            obj.CapitalMasterId = 0;
                            obj.CapitalMasterIdRevised = (int)_objequity.IdRevised;
                            obj.No_AuhorisedCapital = 0;
                            obj.No_AuhorisedCapitalRevised = item.pAuthorisedCapital1Revised;
                            obj.No_IssuedCapital = 0;
                            obj.No_Paid_upCapital = 0;
                            obj.No_SubscribedCapital = 0;
                            obj.Nominalper_AuhorisedCapital = 0;
                            obj.Nominalper_AuhorisedCapitalRevised = item.pAuthorisedCapital2Revised;
                            obj.Nominalper_IssuedCapital = 0;
                            obj.Nominalper_Paid_upCapital = 0;
                            obj.Nominalper_SubscribedCapital = 0;
                            obj.totamt_AuhorisedCapitalRevised = item.pAuthorisedCapital3Revised;
                            obj.totamt_IssuedCapital = 0;
                            obj.totamt_upCapital = 0;
                            obj.totamt_SubscribedCapital = 0;

                            obj.Pref_CapitalType = item.Pref_CapitalType;
                            obj.Pref_CoupenRate = item.Pref_CoupenRate;

                            obj.Pref_CapitalTypeRevised = item.Pref_CapitalType;
                            obj.Pref_CoupenRateRevised = item.Pref_CoupenRate;

                            TNOEquityAuth = TNOEquityAuth + (item.pAuthorisedCapital1);
                            TAOEquityAuth = TAOEquityAuth + item.pAuthorisedCapital3;

                            TNOEquityAuthRevised = TNOEquityAuthRevised + (item.pAuthorisedCapital1Revised);
                            TAOEquityAuthRevised = TAOEquityAuthRevised + item.pAuthorisedCapital3Revised;

                            obj.Sharetype = "P";
                            obj.IsActive = true;
                            obj.CustomerId = CustomerId;

                            obj.Createdby = userID;
                            obj.Createdon = DateTime.Now;

                            entities.BM_Share_RevisedHistory.Add(obj);
                            entities.SaveChanges();

                            item.psharesIDRevised = obj.IdRevised;
                        }
                    }

                    _objequity.pTnAuthorisedCapital = TNOEquityAuth;
                    _objequity.pTaAuthorisedCapital = TAOEquityAuth;

                    _objequity.pTnAuthorisedCapitalRevised = TNOEquityAuthRevised;
                    _objequity.pTaAuthorisedCapitalRevised = TAOEquityAuthRevised;

                    _objequity.SuccessErrorMsg = "Saved successfully";
                    _objequity.Message = true;

                    var capitalDtls = (from row in entities.BM_CapitalMaster_RevisedHistory
                                       where row.IdRevised == _objequity.IdRevised
                                       select row).FirstOrDefault();
                    if (capitalDtls != null)
                    {
                        //capitalDtls.Pri_TotNoAuthorizedCapita = _objequity.pTnAuthorisedCapital;
                        //capitalDtls.Pri_TotamtAuthorizedCapita = _objequity.pTaAuthorisedCapital;

                        capitalDtls.Pri_TotNoAuthorizedCapitaRevised = _objequity.pTnAuthorisedCapitalRevised;
                        capitalDtls.Pri_TotamtAuthorizedCapitaRevised = _objequity.pTaAuthorisedCapitalRevised;

                        var authorisedCapitalIncreased = (capitalDtls.TotamtAuthorizedCapitaRevised - capitalDtls.TotamtAuthorizedCapita) + (capitalDtls.Pri_TotamtAuthorizedCapitaRevised - capitalDtls.Pri_TotamtAuthorizedCapita);
                        var authorisedCapitalRevised = capitalDtls.AuthorizedCapital + (authorisedCapitalIncreased > 0 ? authorisedCapitalIncreased : 0);
                        capitalDtls.AuthorizedCapitalRevised = authorisedCapitalRevised;

                        entities.SaveChanges();

                        #region Save Template Fields
                        var objIncreaseInAuthCapitalTemplateVM = new IncreaseInAuthCapitalTemplateVM();

                        var FaceValueOfEquityShare = (from row in entities.BM_Share_RevisedHistory
                                               where row.CapitalMasterIdRevised == _objequity.IdRevised
                                               && row.IsActive == true && row.Sharetype == "E"
                                               && row.Nominalper_AuhorisedCapitalRevised > 0
                                               select row.Nominalper_AuhorisedCapitalRevised).FirstOrDefault();

                        var FaceValueOfPreferenceShare = (from row in entities.BM_Share_RevisedHistory
                                                      where row.CapitalMasterIdRevised == _objequity.IdRevised
                                                      && row.IsActive == true && row.Sharetype == "P"
                                                      && row.Nominalper_AuhorisedCapitalRevised > 0
                                                      select row.Nominalper_AuhorisedCapitalRevised).FirstOrDefault();

                        objIncreaseInAuthCapitalTemplateVM.NumberOfEquityShares = capitalDtls.TotNoAuthorizedCapita;
                        objIncreaseInAuthCapitalTemplateVM.FaceValueOfEquityShare = FaceValueOfEquityShare;
                        objIncreaseInAuthCapitalTemplateVM.EquityShareCapital = capitalDtls.TotamtAuthorizedCapita;

                        objIncreaseInAuthCapitalTemplateVM.IncreasedNumberOfEquityShares = capitalDtls.TotNoAuthorizedCapitaRevised;
                        objIncreaseInAuthCapitalTemplateVM.IncreasedEquityShareCapital = capitalDtls.TotamtAuthorizedCapitaRevised;

                        objIncreaseInAuthCapitalTemplateVM.NumberOfPreferenceShares = capitalDtls.Pri_TotNoAuthorizedCapita;
                        objIncreaseInAuthCapitalTemplateVM.FaceValueOfPreferenceShare = FaceValueOfPreferenceShare;
                        objIncreaseInAuthCapitalTemplateVM.PreferenceShareCapital = capitalDtls.Pri_TotamtAuthorizedCapita;
                        objIncreaseInAuthCapitalTemplateVM.IncreasedNumberOfPreferenceShare = capitalDtls.Pri_TotNoAuthorizedCapitaRevised;
                        objIncreaseInAuthCapitalTemplateVM.IncreasedPreferenceShareCapital = capitalDtls.Pri_TotamtAuthorizedCapitaRevised;

                        //
                        objIncreaseInAuthCapitalTemplateVM.ExistingAuthorisedShareCapital = capitalDtls.AuthorizedCapital;
                        objIncreaseInAuthCapitalTemplateVM.IncreasedAuthorisedShareCapital = capitalDtls.AuthorizedCapitalRevised;

                        var result = GetUIFormColumnsData(_objequity.AgengaIDPref, _objequity.MeetingAgengaMappingIDPref);
                        UIFormData<IncreaseInAuthCapitalTemplateVM> mapper = new UIFormData<IncreaseInAuthCapitalTemplateVM>();
                        result = mapper.MapData(result, objIncreaseInAuthCapitalTemplateVM);

                        var objMeetingAgendaTemplateVM = new MeetingAgendaTemplateVM();
                        objMeetingAgendaTemplateVM.lstControls = result;

                        objIMeeting_Service.SaveUpdateAgendaItemTemplateList(objMeetingAgendaTemplateVM, userID);
                        #endregion
                    }
                    else
                    {
                        _objequity.SuccessErrorMsg = "Something went wrong";
                        _objequity.errorMessage = true;
                    }
                }
                else
                {
                    _objequity.SuccessErrorMsg = "Something went wrong";
                    _objequity.errorMessage = true;

                }
            }
            catch (Exception ex)
            {
                _objequity.SuccessErrorMsg = "Sarver error occured";
                _objequity.errorMessage = true;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objequity;
        }
        #endregion
    }
}