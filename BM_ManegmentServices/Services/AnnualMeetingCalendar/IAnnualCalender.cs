﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.AnnualMeetingCalendar
{
    public interface IAnnualCalender
    {
        AnnualMeetingVM CreateAnnualMeeting(AnnualMeetingVM _objAnnualMeeting, int userId, int customerId);
        List<AnnualMeetingSheduler> GetAnnualMeeting(int customerId,int UserId,string Role);
        List<EntityDetails> GetAllEntityDetails(int customerId, int userID, string role);
        List<FYearDetails> GetFYEntityWise(int entityID);
        List<AnnualMVM> GetAllAnnualMeeting(int customerId);
       
        AnnualMeetingSheduler UpdateAnnualMeeting(AnnualMeetingSheduler item, int customerId, int userId);
        AnnualMeetingSheduler GetSchedulerEditvalue(int iD);
        void DeleteAnnualCalenderMeeting(AnnualMeetingSheduler _objannualmeeting, ModelStateDictionary modelState,int userId);
        AnnualMVM AddAnnualMeeting(AnnualMVM item, int customerId, int userId, ModelStateDictionary modelState);
        bool CheckDuplicateAnnualMeetingDate(IEnumerable<AnnualMVM> _objMeeting, int customerId);
        string Checkvalidation(AnnualMVM _obj, int customerId);
        string CheckvalidationforSrnumber(AnnualMVM _obj, int customerId);
    }
}
