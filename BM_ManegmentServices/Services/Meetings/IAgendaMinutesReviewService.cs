﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BM_ManegmentServices.VM;

namespace BM_ManegmentServices.Services.Meetings
{
    public interface IAgendaMinutesReviewService
    {
        #region Agenda - Performer/Reviewer
        AgendaReviewVM GetAgendaReviewDetails(long mappingId, long taskId, long assignmentId, long agendaReviewId);
        AgendaReviewVM SaveAgendaReviewDeails(AgendaReviewVM obj, bool canAcceptChanges, int createdBy);
        Message MergeChanges(long taskId, long taskAssignmentId, string remark, int mergedBy);
        Message ReSendChangesForReview(long taskId, long taskAssignmentId, string remark, int updatedBy, out string mailFormat);
        #endregion
        PreviewAgendaVM PreviewAgenda(long MeetingId, int CustomerId, long? taskAssignmentId);
        PreviewAgendaVM PreviewMOM(long MeetingId, int CustomerId);
        #region Agenda, Documents review
        List<AgendaAndDocumentReviewVM> AgendaDocumentReview(long meetingId, long taskId, long assignmentId);
        Message SetDocumentOrder(List<FileSrNoVM> obj, long meetingId, int updatedBy);
        List<VM_AgendaDocument> GetAgendaDocument(long agendaMappingID, int customerId);
        #endregion
        #region AgendaMaster check format is valid or not
        List<AgendaReviewVM> GetAgenda(int c);
        #endregion

        #region Attendance
        List<MeetingAttendance_VM> GetPerticipenforAttendenceCS(long meetingID, int customerId, int? userId);
        List<MeetingAttendance_VM> GetOtherParticipentAttendence(long meetingID);
        #endregion

        #region Minutes Details
        MeetingMinutesDetailsVM GetMinutesDetails(long meetingId);
        List<AgendaAndDocumentReviewVM> DraftMinutesReview(long meetingId, long taskId, long assignmentId);
        #endregion
    }
}
