﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Meetings
{
    public class MeetingAgendaInterestedParty : IMeetingAgendaInterestedParty
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        public IEnumerable<MeetingAgendaInterestedPartyVM> GetAll(long meetingAgendaMappingID)
        {
            var result = entities.BM_MeetingAgendaInterestedParty.Where(template => template.MeetingAgendaMappingID == meetingAgendaMappingID && template.IsDeleted == false).Select(template => new MeetingAgendaInterestedPartyVM
            {
                InterestedPartyID = template.InterestedPartyID,
                MeetingAgendaMappingID = template.MeetingAgendaMappingID,
                Director_Id = template.Director_Id,

                Category = new DirectorMasterListVM()
                {
                    ID = template.Director_Id,
                    Name = entities.BM_DirectorMaster.Where(k => k.Id == template.Director_Id && k.Is_Deleted == false).Select(k => k.FirstName + " " + k.LastName).FirstOrDefault()
                }
             }).ToList();
            return result;
        }
        public MeetingAgendaInterestedPartyVM Create(MeetingAgendaInterestedPartyVM template, int createdBy)
        {
            try
            {
                var isExists = entities.BM_MeetingAgendaInterestedParty.Where(k => k.Director_Id == template.Director_Id && k.MeetingAgendaMappingID == template.MeetingAgendaMappingID && k.IsDeleted == false).FirstOrDefault();
                if(isExists == null)
                {
                    var entity = new BM_MeetingAgendaInterestedParty();

                    entity.InterestedPartyID = template.InterestedPartyID;
                    entity.MeetingAgendaMappingID = template.MeetingAgendaMappingID;
                    entity.IsDeleted = false;
                    entity.CreatedBy = createdBy;
                    entity.CreatedOn = DateTime.Now;

                    if (template.Category != null)
                    {
                        entity.Director_Id = template.Category.ID;
                    }

                    entities.BM_MeetingAgendaInterestedParty.Add(entity);
                    entities.SaveChanges();
                    template.InterestedPartyID = entity.InterestedPartyID;
                } 
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return template;
        }
        public MeetingAgendaInterestedPartyVM Update(MeetingAgendaInterestedPartyVM template, int updatedBy)
        {
            try
            {
                var isExists = entities.BM_MeetingAgendaInterestedParty.Where(k => k.InterestedPartyID != template.InterestedPartyID && k.Director_Id == template.Director_Id && k.MeetingAgendaMappingID == template.MeetingAgendaMappingID  && k.IsDeleted == false).FirstOrDefault();

                if(isExists == null)
                {
                    var interestedParty = entities.BM_MeetingAgendaInterestedParty.Where(k => k.InterestedPartyID == template.InterestedPartyID && k.IsDeleted == false).FirstOrDefault();

                    if (interestedParty != null)
                    {
                        interestedParty.IsDeleted = false;
                        interestedParty.UpdatedBy = updatedBy;
                        interestedParty.UpdatedOn = DateTime.Now;

                        if (template.Category != null)
                        {
                            interestedParty.Director_Id = template.Category.ID;
                        }

                        entities.Entry(interestedParty).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return template;
        }
        public MeetingAgendaInterestedPartyVM Delete(MeetingAgendaInterestedPartyVM template, int updatedBy)
        {
            try
            {
                var interestedParty = entities.BM_MeetingAgendaInterestedParty.Where(k => k.InterestedPartyID == template.InterestedPartyID && k.IsDeleted == false).FirstOrDefault();
                if (interestedParty != null)
                {
                    interestedParty.IsDeleted = true;
                    interestedParty.UpdatedBy = updatedBy;
                    interestedParty.UpdatedOn = DateTime.Now;

                    entities.Entry(interestedParty).State = System.Data.Entity.EntityState.Modified;
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return template;
        }
    }
}