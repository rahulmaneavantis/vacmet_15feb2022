﻿using BM_ManegmentServices.Services.Masters;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BM_ManegmentServices.Services.Meetings
{
    public class MeetingInvitee : IMeetingInvitee
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        public IEnumerable<MeetingInviteeVM> GetAll(long meetingID, bool loadAgendaItems)
        {
            var result = entities.BM_MeetingParticipant.Where(invitee => invitee.Meeting_ID == meetingID && invitee.IsInvited == true && invitee.IsDeleted == false).Select(invitee => new MeetingInviteeVM
            {
                MeetingParticipantId = invitee.MeetingParticipantId,
                ParticipantName = invitee.ParticipantName,
                ParticipantEmail = invitee.ParticipantEmail,
                Meeting_ID = invitee.Meeting_ID,
            }).ToList();

            if (result != null && loadAgendaItems == true)
            {
                foreach (var item in result)
                {
                    item.lstAgendaItems = (from row in entities.BM_MeetingParticipantAgenda
                                           join mapping in entities.BM_MeetingAgendaMapping on row.MeetingAgendaMappingId equals mapping.MeetingAgendaMappingID
                                           where row.ParticipantId == item.MeetingParticipantId &&
                                           row.IsDeleted == false
                                           select new MeetingInviteeAgendaItemVM
                                           {
                                               Meeting_Id = item.Meeting_ID,
                                               IsCheked = true,
                                               Agenda = "",
                                               AgendaHeading = mapping.AgendaItemTextNew != null ? mapping.AgendaItemTextNew : mapping.AgendaItemText,
                                               BM_AgendaMasterId = row.AgendaID
                                           }).ToList();
                }
            }
            return result;
        }

        public MeetingInviteeVM Create(MeetingInviteeVM obj, int createdBy)
        {
            try
            {
                var isExists = entities.BM_MeetingParticipant.Where(k => k.MeetingParticipantId != obj.MeetingParticipantId && k.ParticipantEmail == obj.ParticipantEmail && k.Meeting_ID == obj.Meeting_ID && k.IsDeleted == false).FirstOrDefault();
                if (isExists == null)
                {
                    var invitee = new BM_MeetingParticipant();

                    invitee.ParticipantName = obj.ParticipantName;
                    invitee.ParticipantEmail = obj.ParticipantEmail;
                    invitee.Meeting_ID = obj.Meeting_ID;
                    invitee.IsDeleted = false;
                    invitee.IsInvited = true;
                    invitee.CreatedBy = createdBy;
                    invitee.CreatedOn = DateTime.Now;
                    invitee.ParticipantType = SecretarialConst.MeetingParticipantsTypes.INVITEE;

                    entities.BM_MeetingParticipant.Add(invitee);
                    entities.SaveChanges();
                    obj.MeetingParticipantId = invitee.MeetingParticipantId;

                    if (obj.lstAgendaItems != null)
                    {
                        var lstParticipantAgenda = new List<BM_MeetingParticipantAgenda>();
                        foreach (var item in obj.lstAgendaItems)
                        {
                            lstParticipantAgenda.Add(new BM_MeetingParticipantAgenda()
                            {
                                ParticipantAgendaId = 0,
                                MeetingAgendaMappingId = item.MeetingAgendaMappingId,
                                AgendaID = item.BM_AgendaMasterId,
                                ParticipantId = obj.MeetingParticipantId,
                                IsDeleted = false,
                                CreatedBy = createdBy,
                                CreatedOn = DateTime.Now
                            });
                        }

                        if (lstParticipantAgenda.Count > 0)
                        {
                            entities.BM_MeetingParticipantAgenda.AddRange(lstParticipantAgenda);
                            entities.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }
        public MeetingInviteeVM Update(MeetingInviteeVM obj, int updatedBy)
        {
            try
            {
                var isExists = entities.BM_MeetingParticipant.Where(k => k.MeetingParticipantId != obj.MeetingParticipantId && k.ParticipantEmail == obj.ParticipantEmail && k.Meeting_ID == obj.Meeting_ID && k.IsDeleted == false).FirstOrDefault();

                if (isExists == null)
                {
                    var invitee = entities.BM_MeetingParticipant.Where(k => k.MeetingParticipantId == obj.MeetingParticipantId && k.IsDeleted == false).FirstOrDefault();

                    if (invitee != null)
                    {
                        invitee.ParticipantName = obj.ParticipantName;
                        invitee.ParticipantEmail = obj.ParticipantEmail;
                        invitee.IsDeleted = false;
                        invitee.UpdatedBy = updatedBy;
                        invitee.UpdatedOn = DateTime.Now;
                        invitee.ParticipantType = SecretarialConst.MeetingParticipantsTypes.INVITEE;

                        entities.Entry(invitee).State = System.Data.Entity.EntityState.Modified;
                        entities.SaveChanges();

                        #region Add/Update Agenda Items

                        entities.BM_MeetingParticipantAgenda.Where(k => k.ParticipantId == obj.MeetingParticipantId && k.IsDeleted == false).ToList().ForEach(k => k.IsDeleted = true);
                        entities.SaveChanges();

                        if (obj.lstAgendaItems != null)
                        {
                            var lstParticipantAgenda = new List<BM_MeetingParticipantAgenda>();
                            foreach (var item in obj.lstAgendaItems)
                            {
                                var _obj = entities.BM_MeetingParticipantAgenda.Where(k => k.ParticipantId == obj.MeetingParticipantId && k.AgendaID == item.BM_AgendaMasterId).FirstOrDefault();
                                if (_obj != null)
                                {
                                    _obj.IsDeleted = false;
                                    _obj.UpdatedBy = updatedBy;
                                    _obj.UpdatedOn = DateTime.Now;
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    lstParticipantAgenda.Add(new BM_MeetingParticipantAgenda()
                                    {
                                        ParticipantAgendaId = 0,
                                        MeetingAgendaMappingId = item.MeetingAgendaMappingId,
                                        ParticipantId = obj.MeetingParticipantId,
                                        AgendaID = item.BM_AgendaMasterId,
                                        IsDeleted = false,
                                        CreatedBy = updatedBy,
                                        CreatedOn = DateTime.Now
                                    });
                                }
                            }

                            if (lstParticipantAgenda.Count > 0)
                            {
                                entities.BM_MeetingParticipantAgenda.AddRange(lstParticipantAgenda);
                                entities.SaveChanges();
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public MeetingInviteeVM DestroyInvitee(MeetingInviteeVM template, int updatedBy)
        {
            MeetingInviteeVM _objinvitee = new MeetingInviteeVM();
            try
            {
                var checkInvitee = (from x in entities.BM_MeetingParticipant
                                    where x.MeetingParticipantId == template.MeetingParticipantId && x.IsDeleted == false
                                    select x).FirstOrDefault();
                if (checkInvitee != null)
                {
                    var checktransection = (from row in entities.BM_MeetingNoticeTransaction
                                            where row.MeetingParticipantId == checkInvitee.MeetingParticipantId
                                            select row).FirstOrDefault();
                    if (checktransection != null)
                    {
                        template.Error = true;
                        template.Message = "Invitee can't deleted";
                    }
                    else
                    {
                        checkInvitee.IsDeleted = true;
                        checkInvitee.IsInvitedActive = false;
                        checkInvitee.UpdatedBy = updatedBy;
                        checkInvitee.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();
                        template.Success = true;
                        template.Message = "Delete successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                template.Error = true;
                template.Message = "server error occured";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return template;
        }
    }
}