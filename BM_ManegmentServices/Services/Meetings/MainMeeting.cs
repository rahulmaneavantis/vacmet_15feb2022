﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
using BM_ManegmentServices.Data;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using BM_ManegmentServices.Services.Compliance;

namespace BM_ManegmentServices.Services.Meetings
{
    public class MainMeeting : IMainMeeting
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();
        IMeeting_Service objIMeeting_Service;
        IComplianceTransaction_Service objIComplianceTransaction_Service;

        public MainMeeting(IMeeting_Service obj, IComplianceTransaction_Service obj1)
        {
            objIMeeting_Service = obj;
            objIComplianceTransaction_Service = obj1;
        }

        public MainMeeting_VM GetAgendabyIDforMainContent(long meetingAgendaMappingID)
        {
            try
            {

                var _objgetagenda = (from row in entities.BM_SP_GetAgendaTemplate(meetingAgendaMappingID, null)
                                     select new MainMeeting_VM
                                     {
                                         MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                                         MeetingAgenda = row.AgendaItemText,
                                         AgendaFormate = row.AgendaFormat,
                                         DraftResolution = row.ResolutionFormat,
                                         DraftResolutionHeadine = row.ResolutionFormatHeading,
                                         MinutesFormatHeading = row.MinutesFormatHeading,
                                         MinutesFormat = row.MinutesFormat,
                                         AgendaId = row.BM_AgendaMasterId,
                                         MeetingId = row.MeetingID,
                                         IsVooting = row.Isvoting,
                                         SrNo = row.SrNo,
                                         EntityId = (from E in entities.BM_Meetings where E.MeetingID == row.MeetingID select E.EntityId).FirstOrDefault(),
                                         TemplateFieldEditable = row.TemplateFieldEditable
                                     }).FirstOrDefault();
                if (_objgetagenda != null)
                {
                    _objgetagenda.lstPreCommitteeAgendaResult = (from mapping in entities.BM_MeetingAgendaMapping
                                                                 join meeting in entities.BM_Meetings on mapping.MeetingID equals meeting.MeetingID
                                                                 join agenda in entities.BM_AgendaMaster on mapping.AgendaID equals agenda.BM_AgendaMasterId
                                                                 join preCommittee in entities.BM_CommitteeComp on agenda.MeetingTypeId equals preCommittee.Id
                                                                 where mapping.PostMeetingID == _objgetagenda.MeetingId &&
                                                                 mapping.PostAgendaID == _objgetagenda.AgendaId &&
                                                                 mapping.PostMappingId == _objgetagenda.MeetingAgendaMappingID &&
                                                                 mapping.IsDeleted == false && mapping.IsActive == true
                                                                 select new PreCommitteeAgendaResultVM
                                                                 {
                                                                     MeetingAgendaMappingID = mapping.MeetingAgendaMappingID,
                                                                     MeetingId = mapping.MeetingID,
                                                                     AgendaId = mapping.AgendaID,
                                                                     PreCommittee = preCommittee.Name,
                                                                     Result = meeting.IsCompleted != true ? null : mapping.Result,
                                                                     ResultRemark = mapping.ResultRemark,
                                                                     AgendaHeading = mapping.AgendaItemText
                                                                 }).ToList();
                }
                return _objgetagenda;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<MainMeeting_VM> GetMeetingAgenda(long meetingId)
        {
            try
            {
                var agendaItems = (from row in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, false, null)
                                 select new MainMeeting_VM
                                 {
                                     SrNo = row.SrNo,
                                     Result = row.Result,
                                     ResultRemark = row.ResultRemark,
                                     //Commented by Amit on 5 Mar 2020
                                     //AgendaId = row.MeetingAgendaMappingID,
                                     AgendaId = row.BM_AgendaMasterId,
                                     MeetingAgendaMappingID = row.MeetingAgendaMappingID,
                                     //End
                                     MeetingId = row.MeetingID,
                                     MeetingAgenda = row.AgendaItemText,
                                     AgendaPart = row.PartName,
                                     AgendaSrNo = "Item No. " + row.SrNo,
                                     //AgendaSrNo = "Item No. " + row.itemSrNo,
                                 }).ToList();
                                
                return agendaItems;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<AgendaVoting> GetMeetingPerticipent(long meetingId, long agendaId, long mappingId)
        {
            try
            {
                var _objgetParticipant = (from row in entities.BM_SP_MeetingAgendaResponse(meetingId, agendaId, mappingId)
                                          select new AgendaVoting
                                          {
                                              MeetingId = row.MeetingID,
                                              AgendaId = (long)row.AgendaID,
                                              MeetingAgendaMappingId = row.MeetingAgendaMappingID,
                                              ParticipantName = row.ParticipantName,
                                              ParticipentId = row.MeetingParticipantId,
                                              AgendaVotingResponse = row.Response,
                                              Remark = row.Remark,
                                              Castingvote = row.CastingVote == null ? "" : row.CastingVote.Trim(),
                                              IsDeleted = row.IsDeleted
                                          }).ToList();

                return _objgetParticipant;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<MeetingAttendance_VM> GetPerticipenforAttendence(long meetingID, int customerId, int? userId)
        {

            try
            {
                var getParticipant = (from row in entities.BM_SP_GetParticipantAttendance(meetingID)
                                      where row.UserId == userId
                                      select new MeetingAttendance_VM
                                      {
                                          ParticipantId = row.MeetingParticipantId,
                                          MeetingParticipant_Name = row.Participant_Name,
                                          MeetingId = row.Meeting_ID,
                                          Director_Id = row.Director_Id,
                                          ImagePath = row.Photo_Doc,
                                          Designation = row.Designation,
                                          Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                          Remark = row.Reason,
                                          userId = row.UserId,
                                          RSVP = row.RSVP,
                                          RSVP_Reason = row.RSVP_Reason
                                      }).ToList();


                return getParticipant;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public AgendaVoting UpdateMeetingAgendaResponse(AgendaVoting item, int userId)
        {
            try
            {
                var IsExists = entities.BM_Meetings_Agenda_Responses.Where(k => k.MeetingAgendaMappingId == item.MeetingAgendaMappingId && k.Meeting_ID == item.MeetingId &&
                                k.MeetingParticipantID == item.ParticipentId && k.AgendaId == item.AgendaId &&
                                k.IsDeleted == false).FirstOrDefault();
                if (IsExists == null)
                {
                    BM_Meetings_Agenda_Responses _obj = new BM_Meetings_Agenda_Responses()
                    {
                        Meeting_ID = item.MeetingId,
                        MeetingAgendaMappingId = item.MeetingAgendaMappingId,
                        AgendaId = (long)item.AgendaId,
                        MeetingParticipantID = item.ParticipentId,
                        Response = item.AgendaVotingResponse,
                        Remark = item.Remark,
                        IsDeleted = false,
                        CreatedOn = DateTime.Now,
                        CreatedBy = userId
                    };

                    entities.BM_Meetings_Agenda_Responses.Add(_obj);
                    entities.SaveChanges();
                }
                else
                {
                    var checkforCastingVote = (from casting in entities.BM_Meetings_Agenda_Responses
                                               where casting.MeetingAgendaMappingId == item.MeetingAgendaMappingId
                                               && casting.Meeting_ID == item.MeetingId && casting.AgendaId == item.AgendaId
                                               && casting.MeetingParticipantID == item.ParticipentId
                                               && casting.CastingVote == "C"
                                               select casting).FirstOrDefault();
                    if (checkforCastingVote == null)
                    {
                        IsExists.Response = item.AgendaVotingResponse;
                        IsExists.Remark = item.Remark;
                        IsExists.IsDeleted = false;
                        IsExists.UpdatedOn = DateTime.Now;
                        IsExists.UpdatedBy = userId;
                        entities.SaveChanges();
                    }
                    else
                    {
                        checkforCastingVote.MeetingAgendaMappingId = item.MeetingAgendaMappingId;
                        checkforCastingVote.Response = item.AgendaVotingResponse;
                        checkforCastingVote.Remark = item.Remark;
                        checkforCastingVote.IsDeleted = false;
                        checkforCastingVote.UpdatedOn = DateTime.Now;
                        checkforCastingVote.UpdatedBy = userId;
                        entities.SaveChanges();
                    }
                }
                //GetAllAgendaDetails(item.MeetingId, item.AgendaId, item.MeetingAgendaMappingId);
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return item;
        }

        public MeetingAttendance_VM UpdateMeetingAttendence(MeetingAttendance_VM item, int userId)
        {
            try
            {
                //var IsExist = (from row in entities.BM_MeetingAttendance where row.DirectorId == item.Director_Id && row.MeetingId == item.MeetingId && row.MeetingParticipantId == item.ParticipantId select row).FirstOrDefault();
                var IsExist = (from row in entities.BM_MeetingAttendance where row.MeetingParticipantId == item.ParticipantId select row).FirstOrDefault();
                if (IsExist == null)
                {
                    BM_MeetingAttendance _meetingattendence = new BM_MeetingAttendance();
                    _meetingattendence.MeetingId = item.MeetingId;
                    _meetingattendence.MeetingParticipantId = item.ParticipantId;
                    _meetingattendence.Attendance = item.Attendance.Trim();
                    _meetingattendence.CreatedOn = DateTime.Now;
                    _meetingattendence.Createdby = userId;
                    //_meetingattendence.DirectorId = (int)item.Director_Id;
                    _meetingattendence.Reason = item.Remark;
                    _meetingattendence.IsActive = true;

                    entities.BM_MeetingAttendance.Add(_meetingattendence);
                    entities.SaveChanges();
                }
                else
                {
                    IsExist.MeetingId = item.MeetingId;
                    IsExist.MeetingParticipantId = item.ParticipantId;
                    IsExist.Attendance = item.Attendance.Trim();
                    IsExist.CreatedOn = DateTime.Now;
                    IsExist.Createdby = userId;
                    //IsExist.DirectorId = (int)item.Director_Id;
                    IsExist.Reason = item.Remark;
                    IsExist.IsActive = true;

                    IsExist.Updatedby = userId;
                    IsExist.UpdatedOn = DateTime.Now;

                    entities.SaveChanges();
                }

                item.Success = true;
                item.Message = "Attendance Saved Successfully";
                return item;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public MeetingAttendance_VM UpdateMeetingAttendenceforAuditor(MeetingAttendance_VM item, int userId)
        {
            try
            {

                var IsExist = (from row in entities.BM_MeetingAttendance where row.MeetingParticipantId == item.ParticipantId select row).FirstOrDefault();
                if (IsExist == null)
                {
                    BM_MeetingAttendance _meetingattendence = new BM_MeetingAttendance();
                    _meetingattendence.MeetingId = item.MeetingId;
                    _meetingattendence.MeetingParticipantId = item.ParticipantId;
                    _meetingattendence.Attendance = item.Attendance.Trim();
                    _meetingattendence.CreatedOn = DateTime.Now;
                    _meetingattendence.Createdby = userId;
                    //_meetingattendence.DirectorId = (int)item.Director_Id;
                    //_meetingattendence.Reason = item.Remark;
                    _meetingattendence.IsActive = true;
                    entities.BM_MeetingAttendance.Add(_meetingattendence);
                    entities.SaveChanges();

                }
                else
                {
                    IsExist.MeetingId = item.MeetingId;
                    IsExist.MeetingParticipantId = item.ParticipantId;
                    IsExist.Attendance = item.Attendance.Trim();
                    IsExist.CreatedOn = DateTime.Now;
                    IsExist.Createdby = userId;
                    //IsExist.DirectorId = (int)item.Director_Id;
                    //IsExist.Reason = item.Remark;
                    IsExist.IsActive = true;
                    entities.SaveChanges();
                }
                if (item.PartnerName != null)
                {
                    var getParticipantdata = (from row in entities.BM_MeetingParticipant where row.MeetingParticipantId == item.ParticipantId select row).FirstOrDefault();
                    if (getParticipantdata != null)
                    {
                        getParticipantdata.ParticipantName = item.PartnerName;
                        entities.SaveChanges();
                    }
                }
                item.Success = true;
                item.Message = "Saved Successfully.";
                return item;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public MeetingAttendance_VM UpdateMeetingAttendenceforInvitee(MeetingAttendance_VM item, int userId)
        {
            try
            {

                var IsExist = (from row in entities.BM_MeetingAttendance where row.MeetingParticipantId == item.ParticipantId select row).FirstOrDefault();
                if (IsExist == null)
                {
                    BM_MeetingAttendance _meetingattendence = new BM_MeetingAttendance();
                    _meetingattendence.MeetingId = item.MeetingId;
                    _meetingattendence.MeetingParticipantId = item.ParticipantId;
                    _meetingattendence.Attendance = item.Attendance.Trim();
                    _meetingattendence.CreatedOn = DateTime.Now;
                    _meetingattendence.Createdby = userId;
                    //_meetingattendence.DirectorId = (int)item.Director_Id;
                    _meetingattendence.Reason = item.Remark;
                    _meetingattendence.IsActive = true;
                    entities.BM_MeetingAttendance.Add(_meetingattendence);
                    entities.SaveChanges();

                }
                else
                {
                    IsExist.MeetingId = item.MeetingId;
                    IsExist.MeetingParticipantId = item.ParticipantId;
                    IsExist.Attendance = item.Attendance.Trim();
                    IsExist.CreatedOn = DateTime.Now;
                    IsExist.Createdby = userId;
                    //IsExist.DirectorId = (int)item.Director_Id;
                    IsExist.Reason = item.Remark;
                    IsExist.IsActive = true;

                    IsExist.UpdatedOn = DateTime.Now;
                    IsExist.Updatedby = userId;

                    entities.SaveChanges();
                }

                item.Success = true;
                item.Message = "Attendance Saved Successfully";
                return item;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #region Start stop Meeting
        public startstopeDate StartMeeting(startstopeDate _objstartstopdate)
        {
            startstopeDate obj = new startstopeDate();

            var GetMeetings = (from M in entities.BM_Meetings where M.MeetingID == _objstartstopdate.MeetingId && M.IsDeleted == false select M).FirstOrDefault();
            if (GetMeetings != null)
            {
                GetMeetings.StartMeetingDate = _objstartstopdate.StartDate;
                GetMeetings.StartTime = _objstartstopdate.StartTime;
                GetMeetings.IsMeetingStarted = true;//added on 7th of July 2020 by Ruchi
                entities.SaveChanges();
            }

            DateTime d = (DateTime)GetMeetings.StartMeetingDate; // x,y,z are year.month and date respectivel
            DateTime t = Convert.ToDateTime(GetMeetings.StartTime);
            DateTime dtCombined = new DateTime(d.Year, d.Month, d.Day, t.Hour, t.Minute, t.Second);
            _objstartstopdate.MeetingStartDate = dtCombined.ToString("MM/dd/yyyy h:mm:tt");
            _objstartstopdate.StartDate = (DateTime)GetMeetings.StartMeetingDate;
            _objstartstopdate.StartTime = GetMeetings.StartTime;
            _objstartstopdate.MeetingDueDate = GetMeetings.MeetingDate;
            _objstartstopdate.MeetingDueTime = GetMeetings.MeetingTime;

            return _objstartstopdate;
        }

        public startstopeDate getMeetingstartstopDate(long meetingId,int CustomerId)
        {
            try
            {
                startstopeDate objdate = new startstopeDate();
                var getMeetingstartstopTiming = (from row in entities.BM_Meetings where row.MeetingID == meetingId && row.IsDeleted == false select row).FirstOrDefault();

                var GetChairManForMeeting = (from participant in entities.BM_MeetingParticipant
                                             join MA in entities.BM_MeetingAttendance on participant.MeetingParticipantId equals MA.MeetingParticipantId
                                             where MA.MeetingId == meetingId 
                                             && participant.Meeting_ID == meetingId
                                             && participant.Position == 1 
                                             && (MA.Attendance == "P" || MA.Attendance == "VC")
                                             select new
                                             {
                                                 DirectorId = participant.Director_Id
                                             }).FirstOrDefault();

                if (getMeetingstartstopTiming != null)
                {
                    objdate.MeetingId = meetingId;
                    objdate.MeetingTypeId = getMeetingstartstopTiming.MeetingTypeId;
                    objdate.IsEVoting = getMeetingstartstopTiming.IsEVoting;
                    objdate.IsAGMConclude = getMeetingstartstopTiming.IsAGMConclude;
                    objdate.IsCompleted = getMeetingstartstopTiming.IsCompleted;

                    if (objdate.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || objdate.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                    {
                        //objdate.TotalMemberForAgm = getMeetingstartstopTiming.TotalMemberInAGM;
                        //objdate.TotalMemberPresentForAgm = getMeetingstartstopTiming.TotalMemberPresentInAGM;
                        //objdate.TotalProxyPresentforAgm = getMeetingstartstopTiming.TotalProxyPresentInAGM;

                        var attandance = (from row in entities.BM_MeetingMinutesDetails
                                          where row.MeetingId == meetingId && row.IsDeleted == false
                                          select row).FirstOrDefault();
                        if (attandance != null)
                        {
                            objdate.TotalMemberForAgm = attandance.TotalMemberInAGM;
                            objdate.TotalMemberPresentForAgm = attandance.TotalMemberPresentInAGM;
                            objdate.TotalProxyPresentforAgm = attandance.TotalProxyPresentInAGM;

                            //objdate.TotalValidProxy = attandance.TotalValidProxy;
                            //objdate.TotalMemberProxy = attandance.TotalMemberProxy;
                            //objdate.TotalShareHeldByProxy = attandance.TotalShareHeldByProxy;
                            //objdate.FaceValueOfEnquityShare = attandance.FaceValueOfEnquityShare;
                            //objdate.PercentageOfIssuedCapital = attandance.PercentageOfIssuedCapital;
                        }
                    }
                    objdate.MeetingType = (from CC in entities.BM_CommitteeComp where CC.Id == getMeetingstartstopTiming.MeetingTypeId select CC.MeetingTypeName).FirstOrDefault() + " " + "Meeting";
                    objdate.StartDate = getMeetingstartstopTiming.StartMeetingDate;
                    objdate.StartTime = getMeetingstartstopTiming.StartTime;
                    objdate.EndDate = getMeetingstartstopTiming.EndMeetingDate;
                    objdate.IsPause = getMeetingstartstopTiming.IsPause;
                    objdate.MeetingDueDate = getMeetingstartstopTiming.MeetingDate;
                    objdate.MeetingDueTime = getMeetingstartstopTiming.MeetingTime;
                    objdate.EntityName = getMeetingstartstopTiming.BM_EntityMaster.CompanyName;
                    objdate.CIN = getMeetingstartstopTiming.BM_EntityMaster.CIN_LLPIN;
                    objdate.EntityId = getMeetingstartstopTiming.EntityId;
                    objdate.MeetingSrNo = getMeetingstartstopTiming.MeetingSrNo;
                    objdate.Venu = getMeetingstartstopTiming.MeetingVenue;
                    objdate.IsVideoMeeting = getMeetingstartstopTiming.IsVideoMeeting;
                    objdate.Startvideourl = (from vm in entities.BM_VideoMeeting where vm.MeetingID == meetingId select vm.Start_Url).FirstOrDefault();
                    objdate.joinvideourl = (from vm in entities.BM_VideoMeeting where vm.MeetingID == meetingId select vm.Join_Url).FirstOrDefault();
                    objdate.ProviderName= (from vm in entities.BM_CustomerConfiguration where vm.CustomerID == CustomerId && vm.IsActive==true select vm.ProviderName).FirstOrDefault();


                    objdate.MeetingStartDate = objdate.MeetingDueDate != null ? String.Format("{0:MMM dd,yyyy}", objdate.MeetingDueDate) : "";

                    if (GetChairManForMeeting != null)
                        objdate.ChairmanoftheMeeting = (from row in entities.BM_DirectorMaster where row.Id == GetChairManForMeeting.DirectorId select row.Salutation + " " + row.FirstName + " " + row.LastName).FirstOrDefault();
                }
                if (objdate.MeetingSrNo > 0)
                {
                    if (objdate.MeetingSrNo == 1)
                    {
                        objdate.Meetingno = "1st";
                    }
                    else if (objdate.MeetingSrNo == 2)
                    {
                        objdate.Meetingno = "2nd";
                    }
                    else if (objdate.MeetingSrNo == 3)
                    {
                        objdate.Meetingno = "3rd";
                    }
                    else
                    {
                        objdate.Meetingno = objdate.MeetingSrNo + "th";
                    }
                }

                if (getMeetingstartstopTiming.FY != null)
                    objdate.FYText = (from row in entities.BM_YearMaster where row.FYID == getMeetingstartstopTiming.FY select row.FYText).FirstOrDefault();

                return objdate;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public int? getRole(int userId)
        {
            var getrole = (from row in entities.Users
                           where row.ID == userId && row.IsActive == true
                           select row.SecretarialRoleID).FirstOrDefault();
            return getrole;
        }

        public startstopeDate ResumeMeeting(startstopeDate _objstartstopdate)
        {
            try
            {
                var getpausedate = (dynamic)null;
                var checkPause = (from row in entities.BM_Meetings where row.MeetingID == _objstartstopdate.MeetingId && row.StartMeetingDate == _objstartstopdate.StartDate select row).FirstOrDefault();

                checkPause.Resume = _objstartstopdate.ResumeTime;
                checkPause.IsPause = false;
                entities.SaveChanges();
                if (_objstartstopdate.StartMeetingId > 0)
                {
                    getpausedate = (from MD in entities.BM_Meeting_TimeTraking where MD.MeetingId == _objstartstopdate.MeetingId && MD.Id == _objstartstopdate.StartMeetingId select MD).FirstOrDefault();
                }
                else
                {
                    getpausedate = entities.BM_Meeting_TimeTraking.OrderByDescending(item => item.Id).First();
                }
                if (getpausedate.ResumeTime == null && getpausedate != null && getpausedate.PauseTime != null)
                {
                    getpausedate.ResumeTime = _objstartstopdate.ResumeTime;

                    entities.SaveChanges();
                }
                else
                {
                    BM_Meeting_TimeTraking objMeetingStartEndDatetime = new BM_Meeting_TimeTraking();
                    objMeetingStartEndDatetime.MeetingId = _objstartstopdate.MeetingId;

                    objMeetingStartEndDatetime.ResumeTime = _objstartstopdate.ResumeTime;
                    objMeetingStartEndDatetime.CreatedOn = DateTime.Now;
                    objMeetingStartEndDatetime.Createdby = 35;
                    //objMeetingStartEndDatetime.CustomerId = 35;
                    objMeetingStartEndDatetime.IsActive = true;
                    entities.BM_Meeting_TimeTraking.Add(objMeetingStartEndDatetime);
                    entities.SaveChanges();

                }
                _objstartstopdate.IsPause = checkPause.IsPause;
                //_objstartstopdate.StartTime= getpausedate.sta
                return _objstartstopdate;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public startstopeDate PauseMeeting(startstopeDate _objstartstopdate)
        {
            try
            {
                var getpausedate = (dynamic)null;
                //var checkPause = (from row in entities.BM_Meetings where row.MeetingID == _objstartstopdate.MeetingId && row.StartMeetingDate == _objstartstopdate.StartDate select row).FirstOrDefault();
                var checkPause = (from row in entities.BM_Meetings where row.MeetingID == _objstartstopdate.MeetingId select row).FirstOrDefault();
                int startMeetingId = 0;

                checkPause.Pause = _objstartstopdate.PauseTime;
                checkPause.IsPause = true;
                entities.SaveChanges();
                getpausedate = (from MD in entities.BM_Meeting_TimeTraking where MD.MeetingId == _objstartstopdate.MeetingId select MD).FirstOrDefault();
                if (getpausedate != null && getpausedate.PauseTime == null && getpausedate.PauseReason != null)
                {
                    getpausedate.PauseTime = _objstartstopdate.PauseTime;
                    getpausedate.PauseReason = _objstartstopdate.ReasonforPause;
                    entities.SaveChanges();
                    startMeetingId = getpausedate.Id;
                }
                else
                {
                    BM_Meeting_TimeTraking objMeetingStartEndDatetime = new BM_Meeting_TimeTraking();
                    objMeetingStartEndDatetime.MeetingId = _objstartstopdate.MeetingId;
                    objMeetingStartEndDatetime.PauseTime = _objstartstopdate.PauseTime;
                    objMeetingStartEndDatetime.ReasonForPause = _objstartstopdate.ReasonforPause;
                    objMeetingStartEndDatetime.CreatedOn = DateTime.Now;
                    objMeetingStartEndDatetime.Createdby = 35;
                    //objMeetingStartEndDatetime.CustomerId = 35;
                    objMeetingStartEndDatetime.IsActive = true;
                    entities.BM_Meeting_TimeTraking.Add(objMeetingStartEndDatetime);
                    entities.SaveChanges();
                    startMeetingId = objMeetingStartEndDatetime.Id;
                }
                if (startMeetingId > 0)
                {
                    _objstartstopdate.StartMeetingId = startMeetingId;
                }
                _objstartstopdate.IsPause = checkPause.IsPause;
                //_objstartstopdate.StartTime= getpausedate.sta
                return _objstartstopdate;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public MeetingAttendance_VM checkforQuorum(long meetingId, int CustomerId, int EntityId)
        {
            MeetingAttendance_VM obj = new MeetingAttendance_VM();
            obj.IsAttendenceDone = false;

            List<string> lstParticipantType = new List<string>() {
                SecretarialConst.MeetingParticipantsTypes.DIRECTOR,
                SecretarialConst.MeetingParticipantsTypes.MEMBER
            };

            var CheckAttendenceDoneornot = (from A in entities.BM_MeetingAttendance where A.MeetingId == meetingId && A.IsActive == true select A).ToList();
            if (CheckAttendenceDoneornot.Count > 0)
            {
                obj.IsAttendenceDone = true;
                var meeting = (from row in entities.BM_Meetings
                               join rows in entities.BM_CommitteeComp on row.MeetingTypeId equals rows.Id
                               where row.MeetingID == meetingId
                               select new
                               {
                                   row.Entity_Type,
                                   row.MeetingTypeId,
                                   rows.CompositionType,
                                   //attandance.TotalMemberInAGM,
                                   //attandance.TotalMemberPresentInAGM,
                                   //attandance.TotalProxyPresentInAGM,

                                   //attandance.TotalValidProxy,
                                   //attandance.TotalMemberProxy,
                                   //attandance.TotalShareHeldByProxy,
                                   //attandance.FaceValueOfEnquityShare,
                                   //attandance.PercentageOfIssuedCapital,
                               }).FirstOrDefault();

                if (meeting != null)
                {
                    BM_BoardDirector_Rule objboardrule = new BM_BoardDirector_Rule();
                    if (meeting.CompositionType == "B" && (meeting.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || meeting.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM))
                    {
                        try
                        {

                            var attendance = (from row in entities.BM_MeetingMinutesDetails
                                              where row.MeetingId == meetingId && row.IsDeleted == false
                                              select new
                                              {
                                                  row.TotalMemberInAGM,
                                                  row.TotalMemberPresentInAGM,
                                                  row.TotalProxyPresentInAGM,
                                              }).FirstOrDefault();

                            #region CheckForAGMMeeting 
                            if (attendance != null)
                            {
                                if (meeting.Entity_Type == SecretarialConst.EntityTypeID.PRIVATE)
                                {
                                    if (attendance.TotalMemberPresentInAGM >= 2)
                                    {
                                        obj.Success = true;
                                        obj.Message = "Quorum is fulfilled for AGM";
                                        return obj;
                                    }
                                    else
                                    {
                                        obj.Error = true;
                                        obj.Message = "Quorum is Not fulfilled for AGM";
                                        return obj;
                                    }
                                }
                                else if (meeting.Entity_Type == SecretarialConst.EntityTypeID.LISTED || meeting.Entity_Type == SecretarialConst.EntityTypeID.PUBLIC)
                                {
                                    if (attendance.TotalMemberInAGM <= 1000)
                                    {
                                        if (attendance.TotalMemberPresentInAGM >= 5)
                                        {
                                            obj.Success = true;
                                            obj.Message = "Quorum is fulfilled for AGM";
                                            return obj;
                                        }
                                        else
                                        {
                                            obj.Error = true;
                                            obj.Message = "Quorum is Not fulfilled for AGM";
                                            return obj;
                                        }
                                    }
                                    else if (attendance.TotalMemberInAGM > 1000 && attendance.TotalMemberInAGM <= 5000)
                                    {
                                        if (attendance.TotalMemberPresentInAGM >= 15)
                                        {
                                            obj.Success = true;
                                            obj.Message = "Quorum is fulfilled for AGM";
                                            return obj;
                                        }
                                        else
                                        {
                                            obj.Error = true;
                                            obj.Message = "Quorum is Not fulfilled for AGM";
                                            return obj;
                                        }
                                    }
                                    else if (attendance.TotalMemberInAGM >= 5000)
                                    {
                                        if (attendance.TotalMemberPresentInAGM >= 30)
                                        {
                                            obj.Success = true;
                                            obj.Message = "Quorum is fulfilled for AGM";
                                            return obj;
                                        }
                                        else
                                        {
                                            obj.Error = true;
                                            obj.Message = "Quorum is Not fulfilled for AGM";
                                            return obj;
                                        }
                                    }
                                    else
                                    {
                                        obj.Error = true;
                                        obj.Message = "Quorum is Not fulfilled for AGM";
                                        return obj;
                                    }
                                }
                            }
                            else
                            {
                                obj.Error = true;
                                obj.Message = "Quorum is Not fulfilled for AGM";
                                return obj;
                            }

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                        #endregion
                    }
                    if (meeting.CompositionType == "B" && meeting.MeetingTypeId == SecretarialConst.MeetingTypeID.BOARD)
                    {
                        #region CheckForBoardMeeting
                        //try
                        //{
                        //    var FindEntityId = (from row in entities.BM_Meetings join BE in entities.BM_EntityMaster on row.EntityId equals BE.Id where row.MeetingID == meetingId select BE).FirstOrDefault();
                        //    var getquramvalue = (from row in entities.BM_sp_getMeetingAttendenceQuarum(meetingId) select row).FirstOrDefault();
                        //    var getQuorumMasterDetails = (from QM in entities.BM_BoardDirector_Rule where QM.RuleType == "DD" && QM.DesignationId == 2 && QM.Entity_Type == FindEntityId.Entity_Type select QM).FirstOrDefault();
                        //    if ((FindEntityId.Entity_Type == 1) || (FindEntityId.Entity_Type == 2 && FindEntityId.IS_Listed == false))
                        //    {
                        //        if (getquramvalue != null && getquramvalue.TotalDirectorPresent > 0 && getquramvalue.TotalIntrestedDirectorPresent == 0 && getquramvalue.TotalIntrestedDirector == 0)
                        //        {

                        //            var gettotalDirector = (double)(getquramvalue.TotalDirectorPresent) / (double)(getquramvalue.TotalDirector);
                        //            var QuroumofDirector = ((double)(1) / (double)(3));
                        //            if (getquramvalue.TotalDirectorPresent >= 2)
                        //            {
                        //                obj.Success = true;
                        //                obj.Message = "Quorum is fulfilled for Board Meeting";
                        //            }
                        //            else if (gettotalDirector >= QuroumofDirector)
                        //            {
                        //                obj.Success = true;
                        //                obj.Message = "Quorum is fulfilled for Board Meeting";
                        //            }
                        //            else
                        //            {
                        //                obj.Error = true;
                        //                //obj.Message = "1/3rd of Total Board of Directors or 2 directors, whichever is higher is the quorum";
                        //                obj.Message = "Quorum is not fulfilled for Board Meeting";
                        //            }
                        //        }
                        //        else if (getquramvalue != null && getquramvalue.TotalDirectorPresent > 0 && getquramvalue.TotalIntrestedDirectorPresent > 0 && getquramvalue.TotalIntrestedDirector > 0)
                        //        {
                        //            var getTotalIntrestofDirectorPresent = (double)(getquramvalue.TotalDirectorPresent) / (double)(getquramvalue.TotalDirector);
                        //            var QuroumofIntrestedDirector = ((double)(2) / (double)(3));
                        //            if (getTotalIntrestofDirectorPresent >= QuroumofIntrestedDirector && getquramvalue.TotalDirectorPresent >= 2)
                        //            {
                        //                obj.Success = true;
                        //                //obj.Message = "Quorum is fulfilled for Board Meeting";
                        //                obj.Message = "Quorum is not fulfilled for Board Meeting";

                        //            }
                        //            else
                        //            {
                        //                obj.Error = true;
                        //                //obj.Message = "If interested directors exceeds or equals to 2/3rd of the BoD, then the remaining directors not being less than 2 shall be the quorum during such time";
                        //                obj.Message = "Quorum is not fulfilled for Board Meeting";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            obj.Error = true;
                        //            //obj.Message = "1/3rd of Total Board of Directors or 2 directors, whichever is higher is the quorum";
                        //            obj.Message = "Quorum is not fulfilled for Board Meeting";
                        //        }
                        //    }
                        //    else if ((FindEntityId.Entity_Type == 10))
                        //    {
                        //        if (getquramvalue != null && getquramvalue.TotalDirectorPresent > 0)
                        //        {

                        //            var gettotalDirector = (double)(getquramvalue.TotalDirectorPresent) / (double)(getquramvalue.TotalDirector);
                        //            var QuroumofDirector = ((double)(1) / (double)(3));
                        //            if (getquramvalue.TotalDirectorPresent >= 3)
                        //            {
                        //                obj.Success = true;
                        //                obj.Message = "Quorum is fullfill for Board Meeting";
                        //            }
                        //            else if (gettotalDirector >= QuroumofDirector)
                        //            {
                        //                obj.Success = true;
                        //                obj.Message = "Quorum is fullfill for Board Meeting";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            obj.Error = true;
                        //            //obj.Message = "1/3rd of Total Board of Directors or 3 directors, whichever is higher, including at least 1 Independent Director, is the quorum";
                        //            obj.Message = "Quorum is not fulfilled for Board Meeting";
                        //        }
                        //    }


                        //}
                        //catch (Exception ex)
                        //{
                        //    LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                        //}
                        //return obj;


                        try
                        {
                            var result = new List<Composition>();
                            int? CustomerIdTemp = null;
                            ICommitteeComp objICommitteeComp = new CommitteeComp();

                            int EntityType = 0;

                            var asPerStandard = objICommitteeComp.CheckAsPerStandard(meeting.MeetingTypeId, CustomerId, EntityId);

                            if (!asPerStandard)
                            {
                                //CustomerIdTemp = CustomerId;
                            }

                            #region Get Entity Type
                            EntityType = meeting.Entity_Type;
                            #endregion

                            var CompositionRules = objICommitteeComp.GetAllComposition(meeting.MeetingTypeId, EntityType, "Q", CustomerIdTemp, EntityId);

                            #region Check Rules
                            // BM_Directors_DetailsOfCommiteePosition   Committee_Type column contains Designation Id
                            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                            {
                                //Get total Members
                                var totalMember = (from p in entities.BM_MeetingParticipant
                                                   join a in entities.BM_MeetingAttendance on p.MeetingParticipantId equals a.MeetingParticipantId
                                                   where p.Meeting_ID == meetingId && p.IsDeleted == false && (a.Attendance == "P" || a.Attendance == "VC") && a.IsActive == true &&
                                                        lstParticipantType.Contains(p.ParticipantType)
                                                   select p.MeetingParticipantId).Count();

                                if (totalMember == 0)
                                {
                                    result = CompositionRules;
                                }
                                else
                                {
                                    foreach (var item in CompositionRules)
                                    {
                                        //Minimum Count
                                        if (item.DesignationId == null && item.DesignationCount > 0)
                                        {
                                            if (totalMember < item.DesignationCount)
                                            {
                                                result.Add(item);
                                            }
                                        }
                                        //Minumum Designation Count
                                        else if (item.DesignationId != null && item.DesignationCount > 0 && item.DesignationNumerator == null && item.DesignationDenominator == null)
                                        {
                                            var DesignationCount = (from p in entities.BM_MeetingParticipant
                                                                    join a in entities.BM_MeetingAttendance on p.MeetingParticipantId equals a.MeetingParticipantId
                                                                    where p.Meeting_ID == meetingId && p.IsDeleted == false && (a.Attendance == "P" || a.Attendance == "VC") && a.IsActive == true && p.DesignationId == item.DesignationId &&
                                                                        lstParticipantType.Contains(p.ParticipantType)
                                                                    select p.MeetingParticipantId).Count();
                                            //var DesignationCount = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.Committee_Id == CommitteeId && k.EntityId == EntityId && k.Committee_Type == item.DesignationId && k.IsDeleted == false).Count();
                                            if (DesignationCount < item.DesignationCount)
                                            {
                                                result.Add(item);
                                            }
                                        }
                                        //Minumum Designation Count with total strength
                                        else if (item.DesignationId != null && item.DesignationNumerator != null && item.DesignationDenominator != null)
                                        {
                                            decimal minimum = 0;
                                            int DesignationCount = 0;
                                            //var DesignationCount = entities.BM_MeetingAttendance.Where(k => k.MeetingId == meetingId && k.IsActive == true).Count();
                                            if (item.DesignationId > 0)
                                            {
                                                DesignationCount = (from p in entities.BM_MeetingParticipant
                                                                    join a in entities.BM_MeetingAttendance on p.MeetingParticipantId equals a.MeetingParticipantId
                                                                    where p.Meeting_ID == meetingId && p.IsDeleted == false && (a.Attendance == "P" || a.Attendance == "VC") && a.IsActive == true && p.DesignationId == item.DesignationId &&
                                                                        lstParticipantType.Contains(p.ParticipantType)
                                                                    select p.MeetingParticipantId).Count();

                                            }
                                            else
                                            {

                                                DesignationCount = (from p in entities.BM_MeetingParticipant
                                                                    join a in entities.BM_MeetingAttendance on p.MeetingParticipantId equals a.MeetingParticipantId
                                                                    where p.Meeting_ID == meetingId && p.IsDeleted == false && (a.Attendance == "P" || a.Attendance == "VC") && a.IsActive == true &&
                                                                        lstParticipantType.Contains(p.ParticipantType)
                                                                    select p.MeetingParticipantId).Count();
                                            }

                                            if (item.DesignationCount > minimum)
                                            {
                                                minimum = (int)item.DesignationCount;
                                            }

                                            if (item.DesignationDenominator > 0)
                                            {
                                                decimal n = Math.Ceiling(((decimal)item.DesignationNumerator / (decimal)item.DesignationDenominator) * totalMember);

                                                if (n > minimum)
                                                {
                                                    minimum = n;
                                                }

                                                if (DesignationCount < minimum)
                                                {
                                                    result.Add(item);
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            #endregion

                            if (result.Count > 0)
                            {
                                obj.Error = true;
                                obj.Message = "Quorum is not fulfilled for Meeting";
                            }
                            else
                            {
                                obj.Success = true;
                                obj.Message = "Quorum is fulfill for Meeting";
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                        #endregion
                    }
                    else if (meeting.CompositionType == "B" && (meeting.MeetingTypeId == SecretarialConst.MeetingTypeID.PARTNERS || meeting.MeetingTypeId == SecretarialConst.MeetingTypeID.DESIGNATED_PARTNERS))
                    {
                        //LLP - Atleast 1 Partner should present
                        var totalMember = (from p in entities.BM_MeetingParticipant
                                           join a in entities.BM_MeetingAttendance on p.MeetingParticipantId equals a.MeetingParticipantId
                                           where p.Meeting_ID == meetingId && p.IsDeleted == false && (a.Attendance == "P" || a.Attendance == "VC") && a.IsActive == true &&
                                                lstParticipantType.Contains(p.ParticipantType)
                                           select p.MeetingParticipantId).Count();

                        if (totalMember >= 1)
                        {
                            obj.Success = true;
                            obj.Message = "Quorum is fulfilled for Meeting";
                            return obj;
                        }
                        else
                        {
                            obj.Error = true;
                            obj.Message = "Quorum is not fulfilled for Meeting";
                            return obj;
                        }
                    }
                    else if (meeting.CompositionType == "C")
                    {
                        #region CheckForCommitee
                        try
                        {
                            var result = new List<Composition>();
                            int? CustomerIdTemp = null;
                            ICommitteeComp objICommitteeComp = new CommitteeComp();

                            int EntityType = 0;

                            var asPerStandard = objICommitteeComp.CheckAsPerStandard(meeting.MeetingTypeId, CustomerId, EntityId);

                            if (!asPerStandard)
                            {
                                CustomerIdTemp = CustomerId;
                            }

                            #region Get Entity Type
                            EntityType = meeting.Entity_Type;
                            #endregion

                            var CompositionRules = objICommitteeComp.GetAllComposition(meeting.MeetingTypeId, EntityType, "Q", CustomerIdTemp, EntityId);

                            #region Check Rules
                            // BM_Directors_DetailsOfCommiteePosition   Committee_Type column contains Designation Id
                            using (Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities())
                            {
                                //Get total Members
                                var totalMember = (from p in entities.BM_MeetingParticipant
                                                   join a in entities.BM_MeetingAttendance on p.MeetingParticipantId equals a.MeetingParticipantId
                                                   where p.Meeting_ID == meetingId && p.IsDeleted == false && (a.Attendance == "P" || a.Attendance == "VC") && a.IsActive == true &&
                                                        lstParticipantType.Contains(p.ParticipantType)
                                                   select p.MeetingParticipantId).Count();

                                if (totalMember == 0)
                                {
                                    result = CompositionRules;
                                }
                                else
                                {
                                    foreach (var item in CompositionRules)
                                    {
                                        //Minimum Count
                                        if (item.DesignationId == null && item.DesignationCount > 0)
                                        {
                                            if (totalMember < item.DesignationCount)
                                            {
                                                result.Add(item);
                                            }
                                        }
                                        //Minumum Designation Count
                                        else if (item.DesignationId != null && item.DesignationCount > 0 && item.DesignationNumerator == null && item.DesignationDenominator == null)
                                        {
                                            var DesignationCount = (from p in entities.BM_MeetingParticipant
                                                                    join a in entities.BM_MeetingAttendance on p.MeetingParticipantId equals a.MeetingParticipantId
                                                                    where p.Meeting_ID == meetingId && p.IsDeleted == false && (a.Attendance == "P" || a.Attendance == "VC") && a.IsActive == true && p.DesignationId == item.DesignationId &&
                                                                        lstParticipantType.Contains(p.ParticipantType)
                                                                    select p.MeetingParticipantId).Count();
                                            //var DesignationCount = entities.BM_Directors_DetailsOfCommiteePosition.Where(k => k.Committee_Id == CommitteeId && k.EntityId == EntityId && k.Committee_Type == item.DesignationId && k.IsDeleted == false).Count();
                                            if (DesignationCount < item.DesignationCount)
                                            {
                                                result.Add(item);
                                            }
                                        }
                                        //Minumum Designation Count with total strength
                                        else if (item.DesignationId != null && item.DesignationNumerator != null && item.DesignationDenominator != null)
                                        {
                                            decimal minimum = 0;
                                            //var DesignationCount = entities.BM_MeetingAttendance.Where(k => k.MeetingId == meetingId && k.IsActive == true).Count();
                                            var DesignationCount = (from p in entities.BM_MeetingParticipant
                                                                    join a in entities.BM_MeetingAttendance on p.MeetingParticipantId equals a.MeetingParticipantId
                                                                    where p.Meeting_ID == meetingId && p.IsDeleted == false && (a.Attendance == "P" || a.Attendance == "VC") && a.IsActive == true && p.DesignationId == item.DesignationId &&
                                                                        lstParticipantType.Contains(p.ParticipantType)
                                                                    select p.MeetingParticipantId).Count();

                                            if (item.DesignationCount > minimum)
                                            {
                                                minimum = (int)item.DesignationCount;
                                            }

                                            if (item.DesignationDenominator > 0)
                                            {
                                                decimal n = Math.Ceiling(((decimal)item.DesignationNumerator / (decimal)item.DesignationDenominator) * totalMember);

                                                if (n > minimum)
                                                {
                                                    minimum = n;
                                                }

                                                if (DesignationCount < minimum)
                                                {
                                                    result.Add(item);
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            #endregion

                            if (result.Count > 0)
                            {
                                obj.Error = true;
                                obj.Message = "Quorum is not fulfilled for Meeting";
                            }
                            else
                            {
                                obj.Success = true;
                                obj.Message = "Quorum is fulfill for Meeting";
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                        #endregion
                    }
                }
            }
            return obj;
        }

        public IEnumerable<ChairpersonElection> GetParticipant(long meetingId)
        {
            try
            {
                var getparticipantbyMeeting = (from MP in entities.BM_MeetingParticipant
                                               join MA in entities.BM_MeetingAttendance on MP.MeetingParticipantId equals MA.MeetingParticipantId
                                               join D in entities.BM_DirectorMaster on MP.Director_Id equals D.Id
                                               where MP.Meeting_ID == meetingId
                                               && (MA.Attendance == "P" || MA.Attendance == "VC")
                                               select new ChairpersonElection
                                               {
                                                   DirectorId = D.Id,
                                                   DirectorName = D.Salutation + " " + D.FirstName + " " + (D.MiddleName != null ? D.MiddleName + " " : "") + D.LastName
                                               }).ToList();

                return getparticipantbyMeeting;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public string GetChareManForMeeting(long meetingId)
        {
            try
            {
                var getparticipantbyMeeting = (from MP in entities.BM_MeetingParticipant
                                               join MA in entities.BM_MeetingAttendance on MP.MeetingParticipantId equals MA.MeetingParticipantId
                                               where MP.Meeting_ID == meetingId 
                                               && (MA.Attendance == "P" || MA.Attendance == "VC")
                                               && (MP.Position == 1 || MP.IschairmanElected == true)
                                               select MA).FirstOrDefault();

                if (getparticipantbyMeeting == null)
                { 
					return "false";
                    //return "Chairman not Present !! Please Elect Chairman for this meeting";
                }
                else
                {
					return "true";
                    //return "Chairman is present.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public dynamic checkNoofAgendadisscussed(long meetingId)
        {
            var checkagendadiscussion = (from row in entities.BM_MeetingAgendaMapping where row.MeetingID == meetingId && row.IsDeleted == false && row.IsActive == true && row.PartId != 0 && (row.Result == null || row.Result == SecretarialConst.AgendaResult.CASTING) select row).ToList();
            return checkagendadiscussion;
        }

        public startstopeDate ConcludeMeeting(startstopeDate objstartstopedate, int customerId, int concludedBy)
        {
            try
            {
                var setDefaultCirculateMinutes = false;
                var checkMeeting = (from row in entities.BM_Meetings where row.MeetingID == objstartstopedate.MeetingId select row).FirstOrDefault();
                if (checkMeeting != null)
                {
                    if ((checkMeeting.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || checkMeeting.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM) && checkMeeting.IsEVoting == true)
                    {
                        if (checkMeeting.IsAGMConclude == false)
                        {
                            checkMeeting.EndMeetingDate = objstartstopedate.EndDate;
                            checkMeeting.EndMeetingTime = objstartstopedate.EndTime;
                            checkMeeting.Stage = SecretarialConst.MeetingStages.COMPLETED;
                            checkMeeting.IsAGMConclude = true;

                            checkMeeting.UpdatedBy = concludedBy;
                            checkMeeting.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();

                            objIMeeting_Service.ConculdeAGM(objstartstopedate.MeetingId, concludedBy, customerId);
                        }
                        else
                        {
                            checkMeeting.IsCompleted = true;

                            checkMeeting.UpdatedBy = concludedBy;
                            checkMeeting.UpdatedOn = DateTime.Now;
                            entities.SaveChanges();

                            #region Minutes for Qurum Agenda Item
                            var lstLeaveOfAbsence = (from row in entities.BM_MeetingParticipant
                                                     join attendance in entities.BM_MeetingAttendance on row.MeetingParticipantId equals attendance.MeetingParticipantId
                                                     join director in entities.BM_DirectorMaster on row.Director_Id equals director.Id
                                                     where row.Meeting_ID == objstartstopedate.MeetingId 
                                                     && attendance.MeetingId == objstartstopedate.MeetingId 
                                                     && attendance.Attendance == "A"
                                                     select new
                                                     {
                                                         director.Salutation,
                                                         director.FirstName,
                                                         director.MiddleName,
                                                         director.LastName,
                                                         attendance.Reason
                                                     }).ToList();
                            if (lstLeaveOfAbsence.Count > 0)
                            {
                                var lstAbsence = new List<string>();
                                foreach (var director in lstLeaveOfAbsence)
                                {
                                    lstAbsence.Add((string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + director.FirstName + (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.LastName) + " - " + (string.IsNullOrEmpty(director.Reason) ? "" : director.Reason));
                                }
                                objIMeeting_Service.SetMinutesForDefaultAgendaItem(objstartstopedate.MeetingId, SecretarialConst.DefaultAgendaUsedFor.LEAVE_OF_ABSENCE, lstAbsence, concludedBy);
                            }
                            else
                            {
                                objIMeeting_Service.SetMinutesForDefaultAgendaItem(objstartstopedate.MeetingId, SecretarialConst.DefaultAgendaUsedFor.ALL_PRESENT, new List<string>(), concludedBy);
                            }
                            #endregion

                            objIMeeting_Service.ConculdeMeeting(objstartstopedate.MeetingId, concludedBy, customerId);

                            setDefaultCirculateMinutes = true;
                        }
                    }
                    else
                    {
                        checkMeeting.EndMeetingDate = objstartstopedate.EndDate;
                        checkMeeting.EndMeetingTime = objstartstopedate.EndTime;
                        checkMeeting.Stage = SecretarialConst.MeetingStages.COMPLETED;
                        checkMeeting.IsCompleted = true;

                        checkMeeting.UpdatedBy = concludedBy;
                        checkMeeting.UpdatedOn = DateTime.Now;
                        entities.SaveChanges();

                        #region Minutes for Qurum Agenda Item
                        var lstLeaveOfAbsence = (from row in entities.BM_MeetingParticipant
                                                 join attendance in entities.BM_MeetingAttendance on row.MeetingParticipantId equals attendance.MeetingParticipantId
                                                 join director in entities.BM_DirectorMaster on row.Director_Id equals director.Id
                                                 where row.Meeting_ID == objstartstopedate.MeetingId 
                                                 && attendance.MeetingId == objstartstopedate.MeetingId 
                                                 && attendance.Attendance == "A"
                                                 select new
                                                 {
                                                     director.Salutation,
                                                     director.FirstName,
                                                     director.MiddleName,
                                                     director.LastName,
                                                     attendance.Reason
                                                 }).ToList();
                        if (lstLeaveOfAbsence.Count > 0)
                        {
                            var lstAbsence = new List<string>();
                            foreach (var director in lstLeaveOfAbsence)
                            {
                                lstAbsence.Add((string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + director.FirstName + (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.LastName) + " - " + (string.IsNullOrEmpty(director.Reason) ? "" : director.Reason));
                            }
                            objIMeeting_Service.SetMinutesForDefaultAgendaItem(objstartstopedate.MeetingId, SecretarialConst.DefaultAgendaUsedFor.LEAVE_OF_ABSENCE, lstAbsence, concludedBy);
                        }
                        else
                        {
                            objIMeeting_Service.SetMinutesForDefaultAgendaItem(objstartstopedate.MeetingId, SecretarialConst.DefaultAgendaUsedFor.ALL_PRESENT, new List<string>(), concludedBy);
                        }
                        #endregion

                        objIMeeting_Service.ConculdeMeeting(objstartstopedate.MeetingId, concludedBy, customerId);

                        #region Set Default circulate minutes option
                        if(checkMeeting.MeetingTypeId == SecretarialConst.MeetingTypeID.AGM || checkMeeting.MeetingTypeId == SecretarialConst.MeetingTypeID.EGM)
                        {
                            setDefaultCirculateMinutes = true;
                        }
                        #endregion
                    }

                    #region AGM/EGM Draft minutes not circulate Added on 20 July 2021
                    if(setDefaultCirculateMinutes)
                    {
                        var _obj = (from row in entities.BM_MeetingMinutesDetails
                                    where row.MeetingId == objstartstopedate.MeetingId
                                    select row).FirstOrDefault();
                        if(_obj != null)
                        {
                            _obj.IsCirculate = true;
                            _obj.IsDeleted = false;
                            _obj.DateOfDraftCirculation = checkMeeting.MeetingDate;
                            _obj.DueDateOfDraftCirculation = checkMeeting.MeetingDate; // DateTime.Now.Date.AddDays(7);
                            _obj.UpdatedBy = concludedBy;
                            _obj.UpdatedOn = DateTime.Now;

                            entities.Entry(_obj).State = System.Data.Entity.EntityState.Modified;
                            entities.SaveChanges();
                        }
                        else
                        {
                            _obj = new BM_MeetingMinutesDetails();
                            _obj.MeetingId = objstartstopedate.MeetingId;
                            //_obj.DateOfEntering = obj.DateOfEntering;
                            //_obj.Place = obj.Place;
                            //_obj.SigningOfEntering = obj.DateOfSigning;
                            //_obj.IsFinalized = false;
                            _obj.IsCirculate = true;
                            _obj.IsDeleted = false;
                            _obj.DateOfDraftCirculation = checkMeeting.MeetingDate;
                            _obj.DueDateOfDraftCirculation = checkMeeting.MeetingDate;
                            _obj.CreatedBy = concludedBy;
                            _obj.CreatedOn = DateTime.Now;

                            entities.BM_MeetingMinutesDetails.Add(_obj);
                            entities.SaveChanges();
                        }
                    }
                    #endregion

                }
                return objstartstopedate;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region Calculating Result for voting
        public MeetingAgendaData GetAllAgendaDetails(long parentID, long agendaID, long meetingAgendaMappingId)
        {
            try
            {
                int TotalPresentperticipant = 0;
                int totalApproved = 0;
                int totaldisaproved = 0;
                int totaldiffer = 0;
                int totalAbstain = 0;
                int totalNoting = 0;
                BM_Meetings_Agenda_Responses _objMeeting = new BM_Meetings_Agenda_Responses();
                MeetingAgendaData obj = new MeetingAgendaData();
                var checkforcastingvote = (dynamic)null;
                obj.MeetingAgendaResultVM = new MeetingAgendaResult();
                var getdata = (from rows in entities.BM_MeetingAgendaMapping
                               where rows.MeetingAgendaMappingID == meetingAgendaMappingId 
                               && rows.MeetingID == parentID 
                               && rows.AgendaID == agendaID 
                               && rows.IsDeleted == false
                               select new MeetingAgendaData
                               {
                                   AgendaId = rows.AgendaID,
                                   MeetingId = parentID,
                                   AgendaMappingId = rows.MeetingAgendaMappingID,
                                   StartMeetingId = rows.StartMeetingId,
                                   StartAgendaId = rows.StartAgendaId,
                                   StartMappingId = rows.StartMappingId,
                                   SequenceNo = rows.SequenceNo
                               }).FirstOrDefault();

                if (getdata != null)
                {
                    string getagendaformate = (from x in entities.BM_SP_GetAgendaTemplate(getdata.AgendaMappingId, null) select x.AgendaItemText).FirstOrDefault();
                    if (getagendaformate != null)
                    {
                        getdata.AgendaName = getagendaformate;
                    }

                    var getresultlist = (from row in entities.BM_SP_MeetingAgendaCountResponse(getdata.MeetingId, getdata.AgendaId, getdata.AgendaMappingId) select row).FirstOrDefault();
                    if (getresultlist != null)
                    {
                        #region Calculate Agenda Response
                        var getMeetingagendaResponse = (from row in entities.BM_Meetings_Agenda_Responses
                                                        join MP in entities.BM_MeetingParticipant on row.MeetingParticipantID equals MP.MeetingParticipantId
                                                        where row.MeetingAgendaMappingId == getdata.AgendaMappingId && row.Meeting_ID == getdata.MeetingId && row.AgendaId == getdata.AgendaId
                                                        select row).FirstOrDefault();
                        var getMeetingagendaResponse1 = (from row in entities.BM_Meetings_Agenda_Responses
                                                         join MP in entities.BM_MeetingParticipant on row.MeetingParticipantID equals MP.MeetingParticipantId
                                                         where row.MeetingAgendaMappingId == getdata.AgendaMappingId && row.Meeting_ID == getdata.MeetingId && row.AgendaId == getdata.AgendaId && MP.DesignationId == 1
                                                         select row).FirstOrDefault();
                        if (getMeetingagendaResponse1 != null)
                        {
                            checkforcastingvote = (from row in entities.BM_Meetings_Agenda_Responses
                                                   where row.MeetingAgendaMappingId == getdata.AgendaMappingId && row.AgendaId == getdata.AgendaId && row.Meeting_ID == getdata.MeetingId &&
                                                   row.MeetingParticipantID == getMeetingagendaResponse.MeetingParticipantID && row.CastingVote == "C" && row.IsDeleted == false
                                                   select row).FirstOrDefault();
                        }

                        getdata.MeetingAgendaResultVM = new MeetingAgendaResult();


                        if (getresultlist != null && getMeetingagendaResponse != null)
                        {
                            totalApproved = (int)Math.Round((double)(100 * getresultlist.Approve) / getresultlist.Participant);
                            totaldisaproved = (int)Math.Round((double)(100 * getresultlist.Disapprove) / getresultlist.Participant);
                            totaldiffer = (int)Math.Round((double)(100 * getresultlist.Differ) / getresultlist.Participant);
                            totalAbstain = (int)Math.Round((double)(100 * getresultlist.Abstain) / getresultlist.Participant);
                            totalNoting = (int)Math.Round((double)(100 * getresultlist.ApprovebyNoting) / getresultlist.Participant);
                            if (totalApproved > 50)
                            {
                                if (totalApproved == 100)
                                {
                                    getdata.MeetingAgendaResultVM.ResultRemark = "Approved unanimously";
                                }
                                else
                                {
                                    getdata.MeetingAgendaResultVM.ResultRemark = "Approved by majority";
                                }

                                getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.APPROVED;
                                if (checkforcastingvote != null)
                                {
                                    checkforcastingvote.IsDeleted = true;
                                    entities.SaveChanges();
                                }
                            }
                            else if (totalNoting > 0)
                            {
                                getdata.MeetingAgendaResultVM.ResultRemark = "Noted";
                                getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.NOTED;
                            }
                            else if (totalAbstain > 50)
                            {
                                getdata.MeetingAgendaResultVM.ResultRemark = "Approved";
                                getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.APPROVED;
                                if (checkforcastingvote != null)
                                {
                                    checkforcastingvote.IsDeleted = true;
                                    entities.SaveChanges();
                                }
                            }
                            else if (totaldisaproved > 50)
                            {
                                getdata.MeetingAgendaResultVM.ResultRemark = "Disapproved";
                                getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.DISAPPROVED;

                                getdata.MeetingAgendaResultVM.ResultRemark = "Disapproved";
                                if (checkforcastingvote != null)
                                {
                                    checkforcastingvote.IsDeleted = true;
                                    entities.SaveChanges();
                                }

                            }
                            else if (((totalApproved == totaldisaproved) && (totalApproved > 0 && totaldisaproved > 0)) || ((totaldiffer == totaldisaproved) && (totaldisaproved > 0 && totaldiffer > 0)) || ((totaldiffer == totalApproved) && (totalApproved > 0 && totaldiffer > 0)) || ((totalAbstain == totaldisaproved) && (totalAbstain > 0 && totaldisaproved > 0)) || ((totalAbstain == totaldiffer) && (totalAbstain > 0 && totaldiffer > 0)))
                            {
                                if (checkforcastingvote != null)
                                {

                                    if (checkforcastingvote.Response == "A")
                                    {
                                        getdata.MeetingAgendaResultVM.ResultRemark = "Approved";
                                        getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.APPROVED;
                                    }
                                    else if (checkforcastingvote.Response == "D")
                                    {
                                        getdata.MeetingAgendaResultVM.ResultRemark = "Disapproved";
                                        getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.DISAPPROVED;
                                    }
                                }
                                else
                                {
                                    getdata.MeetingAgendaResultVM.ResultRemark = "Casting Vote need to be given by Chairman";
                                    getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.CASTING;
                                }
                            }
                            else if ((totalApproved == totalAbstain) && (totalApproved == 50 && totalAbstain == 50))
                            {
                                getdata.MeetingAgendaResultVM.ResultRemark = "Approved";
                                getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.APPROVED;
                            }

                            else if (totaldiffer > 50)
                            {
                                getdata.MeetingAgendaResultVM.ResultRemark = "Deferred";
                                getdata.MeetingAgendaResultVM.Result = SecretarialConst.AgendaResult.DIFFERED;

                            }
                            else if (totalApproved == 0 && totaldisaproved == 0 && totaldiffer == 0 || totalAbstain == 0 && totaldisaproved == 0 && totaldiffer == 0)
                            {
                                getdata.MeetingAgendaResultVM.ResultRemark = " ";

                            }
                        }
                        else
                        {
                            getdata.MeetingAgendaResultVM.ResultRemark = "";
                        }
                        var IsCompleted = (from row in entities.BM_Meetings
                                           where row.MeetingID == parentID
                                           select row.IsCompleted).FirstOrDefault();
                        if (IsCompleted != true)
                        {
                            long agendaId = (long)getdata.AgendaId;
                            if (getMeetingagendaResponse != null)
                            {
                                var checkforcaasting = (from row in entities.BM_Meetings_Agenda_Responses where row.MeetingAgendaMappingId == getdata.AgendaMappingId && row.MeetingParticipantID == getMeetingagendaResponse.MeetingParticipantID && row.AgendaId == agendaId && row.Meeting_ID == getdata.MeetingId && row.CastingVote == "C" && row.IsDeleted == false select row).FirstOrDefault();
                                if (getdata.MeetingAgendaResultVM.ResultRemark == "Casting Vote need to be given by Chairman" && checkforcastingvote == null)
                                {
                                    if (!(totalApproved > 50 || totaldisaproved > 50 || totalAbstain > 50 || totaldiffer > 50))
                                    {
                                        if (checkforcaasting == null)
                                        {
                                            _objMeeting.MeetingParticipantID = getMeetingagendaResponse.MeetingParticipantID;
                                            _objMeeting.AgendaId = (long)getdata.AgendaId;
                                            _objMeeting.Meeting_ID = getdata.MeetingId;
                                            _objMeeting.MeetingAgendaMappingId = getdata.AgendaMappingId;
                                            _objMeeting.CreatedOn = DateTime.Now;
                                            _objMeeting.CreatedBy = 35;
                                            _objMeeting.CastingVote = "C";
                                            _objMeeting.IsDeleted = false;
                                            entities.BM_Meetings_Agenda_Responses.Add(_objMeeting);
                                            entities.SaveChanges();
                                        }
                                        else
                                        {
                                            checkforcaasting.MeetingParticipantID = getMeetingagendaResponse.MeetingParticipantID;
                                            _objMeeting.MeetingAgendaMappingId = getdata.AgendaMappingId;
                                            checkforcaasting.AgendaId = (long)getdata.AgendaId;
                                            checkforcaasting.Meeting_ID = getdata.MeetingId;
                                            checkforcaasting.CreatedOn = DateTime.Now;
                                            checkforcaasting.CreatedBy = 35;
                                            checkforcaasting.CastingVote = "C";
                                            checkforcaasting.IsDeleted = false;
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                            }
                            var checkAgendasummary = (from row in entities.BM_MeetingAgendaMapping where row.MeetingAgendaMappingID == getdata.AgendaMappingId && row.MeetingID == parentID && row.AgendaID == agendaID && row.IsDeleted == false && row.IsActive == true select row).FirstOrDefault();
                            if (checkAgendasummary != null)
                            {
                                checkAgendasummary.Result = getdata.MeetingAgendaResultVM.Result;
                                checkAgendasummary.ResultRemark = getdata.MeetingAgendaResultVM.ResultRemark;
                                checkAgendasummary.UpdatedOn = DateTime.Now;
                                //checkAgendasummary.UpdatedBy = 5;
                                entities.SaveChanges();
                            }
                        }
                        #endregion Calculate Agenda Responses
                    }
                }

                return getdata;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region GetMeeting Result after voting created by ruchi on 8th of july 2020
        public MeetingAgendaData GetMeetingResult(long MeetingId, long agendaID, long meetingAgendaMappingId)
        {
            try
            {
                string Agendaname = (from x in entities.BM_SP_GetAgendaTemplate(meetingAgendaMappingId, null) select x.AgendaItemHeading).FirstOrDefault();
                var getvotingResult = (from row in entities.BM_MeetingAgendaMapping
                                       where row.MeetingID == MeetingId &&
                                       row.AgendaID == agendaID && row.MeetingAgendaMappingID == meetingAgendaMappingId
                                       && row.IsActive == true
                                       select new MeetingAgendaData

                                       {
                                           AgendaMappingId = meetingAgendaMappingId,
                                           MeetingId = MeetingId,
                                           AgendaId = agendaID,
                                           AgendaName = Agendaname,
                                           MeetingAgendaResultVM = new MeetingAgendaResult
                                           {
                                               Result = row.Result,
                                               ResultRemark = row.ResultRemark
                                           }

                                       }).FirstOrDefault();
                return getvotingResult;

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;

            }
        }
        #endregion

        public List<AvailabilityResponseChartData_ResultVM> GetAgendaResponseChartData(long MeetingId, long AgendaId, long MappingId, int UserId)
        {
            var lstResponse = GetCircularAgendaResponse(MeetingId, AgendaId, MappingId);

            var result = (from row in entities.BM_SP_MeetingAgendaResponseChartData(MeetingId, AgendaId, MappingId)
                          select new AvailabilityResponseChartData_ResultVM
                          {
                              Category = row.Category,
                              MappingId = MappingId,
                              Response_Count = row.Response_Count,
                              Color = row.Color,
                              MeetingId = MeetingId,
                              lstParticiapants = lstResponse.Where(k => k.Category == row.Category).ToList()
                          }).ToList();
            return result;
        }

        private List<AvailabilityResponseByAvailabilityId_ResultVM> GetCircularAgendaResponse(long meetingId, long agendaId, long MappingId)
        {
            var result = (from row in entities.BM_SP_MeetingAgendaResponsesCircular(meetingId, agendaId, MappingId)
                          select new AvailabilityResponseByAvailabilityId_ResultVM
                          {
                              ParticipantName = row.ParticipantName,
                              Email = row.EmailId_Official,
                              Responses = row.Response,
                              Category = row.Category
                          }).ToList();
            return result;
        }

        public ChairpersonElection UpdateChairmanPerson(ChairpersonElection _objchairmanelection)
        {
            try
            {
                var checkforCharman = (from row in entities.BM_MeetingParticipant
                                       where row.Director_Id == _objchairmanelection.DirectorId 
                                       && row.Meeting_ID == _objchairmanelection.MeetingId
                                       select row).FirstOrDefault();

                if (checkforCharman != null)
                {
                    checkforCharman.IschairmanElected = true;
                    //Add on 20 Jan 2020 By : Amit
                    checkforCharman.Position = 1;
                    entities.SaveChanges();

                    _objchairmanelection.Success = true;
                    _objchairmanelection.Message = "Chairman Elected Successfully";
                }
                else
                {
                    _objchairmanelection.Error = true;
                    _objchairmanelection.Message = "Somthing wents wrong";
                }

                return _objchairmanelection;
            }
            catch (Exception ex)
            {
                _objchairmanelection.Error = true;
                _objchairmanelection.Message = "Server error occured";
                return _objchairmanelection;
            }
        }

        public List<MeetingAttendance_VM> GetPerticipenforAttendenceCS(long meetingID, int customerId, int? userId)
        {
            try
            {
                var getParticipant = (from row in entities.BM_SP_GetParticipantAttendance(meetingID)
                                      select new MeetingAttendance_VM
                                      {
                                          ParticipantId = row.MeetingParticipantId,
                                          MeetingParticipant_Name = row.Participant_Name,
                                          MeetingId = row.Meeting_ID,
                                          Director_Id = row.Director_Id,
                                          ImagePath = row.Photo_Doc,
                                          Designation = row.Designation,
                                          Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                          Remark = row.Reason,
                                          Position = row.Position,
                                          RSVP = row.RSVP,
                                          RSVP_Reason = row.RSVP_Reason,
                                          ParticipantType = row.ParticipantType
                                      }).ToList();
                return getParticipant;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public List<MeetingAttendance_VM> GetOtherParticipentAttendence(long meetingID)
        {
            try
            {
                var getParticipant = (from row in entities.BM_SP_GetParticipantAttendanceAllType(meetingID)
                                      select new MeetingAttendance_VM
                                      {
                                          ParticipantId = row.MeetingParticipantId,
                                          MeetingParticipant_Name = row.ParticipantName,
                                          MeetingId = meetingID,
                                          Director_Id = row.MasterId,
                                          Designation = row.Designation,
                                          Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                          Remark = row.Reason,
                                          ParticipantType = row.ParticipantType,
                                          Representative = row.Representative
                                      }).ToList();
                return getParticipant;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public List<MeetingAttendance_VM> GetParticipantRSVP(long meetingID)
        {
            try
            {
                var getParticipant = (from row in entities.BM_SP_GetParticipantRSVP(meetingID)
                                      select new MeetingAttendance_VM
                                      {
                                          ParticipantId = row.MeetingParticipantId,
                                          MeetingParticipant_Name = row.Participant_Name,
                                          MeetingId = row.Meeting_ID,
                                          Director_Id = row.Director_Id,
                                          ImagePath = row.Photo_Doc,
                                          Designation = row.Designation,
                                          Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                          Remark = row.Reason,
                                          Position = row.Position,
                                          RSVP = row.RSVP,
                                          RSVP_Reason = row.RSVP_Reason,
                                          ParticipantType = row.ParticipantType
                                      }).ToList();
                return getParticipant;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<AgendaVoting> GetMeetingPerticipentbyuser(long meetingId, long agendaId, long mappingId, int userId)
        {
            try
            {
                var _objgetParticipant = (from row in entities.BM_SP_MeetingAgendaResponse(meetingId, agendaId, mappingId)
                                          where row.UserId == userId
                                          select new AgendaVoting
                                          {
                                              MeetingId = row.MeetingID,
                                              AgendaId = (long)row.AgendaID,
                                              MeetingAgendaMappingId = row.MeetingAgendaMappingID,
                                              ParticipantName = row.ParticipantName,
                                              ParticipentId = row.MeetingParticipantId,
                                              AgendaVotingResponse = row.Response,
                                              Remark = row.Remark,
                                              Castingvote = row.CastingVote == null ? "" : row.CastingVote.Trim()
                                          }).ToList();
                return _objgetParticipant;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<MeetingAgendaSummary> GetAgendaMeetingwise(long meetingId, int userID)
        {
            try
            {
                long participantID = 0;
                long directorID = (from dir in entities.BM_DirectorMaster where dir.UserID == userID select dir.Id).FirstOrDefault();
                
                if (directorID > 0)
                {
                    participantID = (from par in entities.BM_MeetingParticipant
                                     where par.Director_Id == directorID
                                     && par.Meeting_ID == meetingId
                                     && par.IsDeleted == false
                                     select par.MeetingParticipantId).FirstOrDefault();
                }
                else
                {
                    participantID = (from par in entities.BM_MeetingParticipant
                                     where par.UserId == userID
                                     && par.Meeting_ID == meetingId
                                     && par.IsDeleted == false
                                     select par.MeetingParticipantId).FirstOrDefault();
                }

                var getMeetingwiseAgenda = (from row in entities.BM_SP_MeetingAgendaTemplateList(meetingId, null, false, null)
                                            select new MeetingAgendaSummary
                                            {
                                                SrNo = row.SrNo,
                                                AgendaId = row.BM_AgendaMasterId,
                                                ParticipantId = participantID,
                                                AgendaResponse = (from res in entities.BM_Meetings_Agenda_Responses where res.Meeting_ID == meetingId && res.MeetingParticipantID == participantID && res.AgendaId == row.BM_AgendaMasterId && res.MeetingAgendaMappingId == row.MeetingAgendaMappingID select res.Response).FirstOrDefault(),
                                                MeetingId = row.MeetingID,
                                                AgendaFormate = row.AgendaItemText,
                                                //AgendaFormate = row.AgendaItemHeading,
                                                MeetingAgendaMappingId = row.MeetingAgendaMappingID,

                                                AgendaResponseResult = row.ResultRemark,
                                                MeetingTypeID = (from rows in entities.BM_Meetings where rows.MeetingID == meetingId select rows.MeetingTypeId).FirstOrDefault(),
                                                Result = (from y in entities.BM_MeetingAgendaMapping where y.MeetingAgendaMappingID == row.MeetingAgendaMappingID select y.ResultRemark).FirstOrDefault(),
                                                Isvooting = (from x in entities.BM_AgendaMaster where x.BM_AgendaMasterId == row.BM_AgendaMasterId select x.IsVoting).FirstOrDefault(),
                                                hasDocuments = (from x in entities.BM_FileData where x.AgendaMappingId == row.MeetingAgendaMappingID && x.IsDeleted == false select x).Any(),
                                            }).ToList();

                return getMeetingwiseAgenda;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public IEnumerable<MeetingParticipants> GetParticipantforNotes(long meetingId)
        {
            try
            {
                var getpaticipant = (from row in entities.BM_SP_GetMeetingParticipants(meetingId)
                                     select new MeetingParticipants
                                     {
                                         DirectorId = row.UserId,
                                         DirectorName = row.ParticipantName
                                     }).ToList();
                return getpaticipant;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<MeetingParticipants> GetAuthorisedDirector(long meetingId)
        {
            try
            {
                var getpaticipant = (from row in entities.BM_SP_GetMeetingParticipants(meetingId)
                                     select new MeetingParticipants
                                     {
                                         DirectorId = row.Director_Id,
                                         DirectorName = row.ParticipantName
                                     }).ToList();
                return getpaticipant;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public dynamic getTotalNotesUserwise(long meetingId, int userId)
        {
            try
            {
                var gettotalnotes = (from row in entities.BM_Notes
                                     join rows in entities.BM_NotesUserMapping on
                                     row.Id equals rows.NotesId
                                     where row.MeetingId == meetingId && rows.UserId == userId
                                     select rows).ToList();
                return gettotalnotes.Count();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #region Meeting Resolution
        public MeetingResolution GetMeetingAgendaResolution(long meetingId, long meetingAgendaMappingId)
        {
            try
            {
                var getResolutionFormate = (from x in entities.BM_SP_GetAgendaTemplate(meetingAgendaMappingId, null)
                                            select new MeetingResolution
                                            {
                                                MeetingId = x.MeetingID,
                                                AgendaId = x.BM_AgendaMasterId,
                                                AgendaMappingId = meetingAgendaMappingId,
                                                ResolutionFormatHeadingSub = x.ResolutionFormatHeading,
                                                ResolutionFormatSub = x.ResolutionFormat,
                                            }).FirstOrDefault();
                return getResolutionFormate;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public MeetingResolution updateMeetingResolution(MeetingResolution _objmeetingResolution)
        {
            try
            {
                var checkresolutionExist = (from row in entities.BM_MeetingAgendaMapping where row.MeetingAgendaMappingID == _objmeetingResolution.AgendaMappingId && row.MeetingID == _objmeetingResolution.MeetingId select row).FirstOrDefault();

                if (checkresolutionExist != null)
                {
                    checkresolutionExist.ResolutionFormatHeading = _objmeetingResolution.ResolutionFormatHeadingSub;
                    checkresolutionExist.ResolutionFormat = _objmeetingResolution.ResolutionFormatSub;
                    entities.SaveChanges();
                    _objmeetingResolution.Success = true;
                    _objmeetingResolution.Message = "Updated Successfully.";
                    return _objmeetingResolution;
                }
                else
                {
                    _objmeetingResolution.Error = true;
                    _objmeetingResolution.Message = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                _objmeetingResolution.Error = true;
                _objmeetingResolution.Message = "Server error occurred";
            }
            return _objmeetingResolution;
        }


        #endregion

        #region Generate CTC
        public MeetingCTC getCTCdetails(long meetingId, long agendaId)
        {
            MeetingCTC objmeetingctc = new MeetingCTC();

            var getDetailsofCTC = (from x in entities.BM_SP_GetCTCDetails(meetingId, agendaId) select x).FirstOrDefault();

            long AgendaMappingId = (from am in entities.BM_MeetingAgendaMapping where am.MeetingID == meetingId && am.AgendaID == agendaId select am.MeetingAgendaMappingID).FirstOrDefault();

            var getAgendadetails = (from AF in entities.BM_SP_GetAgendaTemplate(AgendaMappingId, null) select AF).FirstOrDefault();

            objmeetingctc.MeetingId = meetingId;
            objmeetingctc.AgendaId = agendaId;

            if (getDetailsofCTC != null)
            {
                objmeetingctc.MeetingNo = getDetailsofCTC.MeetingSrNo;
                objmeetingctc.EntityName = getDetailsofCTC.CompanyName;
                DateTime date = (DateTime)getDetailsofCTC.MeetingDate;
                objmeetingctc.MeetingDay = date.DayOfWeek.ToString();
                objmeetingctc.MeetingDate = (DateTime)getDetailsofCTC.MeetingDate;
                objmeetingctc.MeetingMinutesFormate = getAgendadetails.MinutesFormat;
                objmeetingctc.MeetingMinutesHeadingFormate = getAgendadetails.MinutesFormatHeading;
                objmeetingctc.MeetingResolutionFormate = getAgendadetails.ResolutionFormat;
                objmeetingctc.MeetingResolutionHeadingFormate = getAgendadetails.ResolutionFormatHeading;
                objmeetingctc.MeetingVenu = getDetailsofCTC.MeetingVenue;
                //objmeetingctc.EntityId = getDetailsofCTC.EntityId;
                objmeetingctc.DirectorName = getDetailsofCTC.DirectorName;
                objmeetingctc.DinNumberofDirector = getDetailsofCTC.DIN;
                objmeetingctc.DesignationofDirector = getDetailsofCTC.Name;
            }

            return objmeetingctc;
        }

        public List<AgendaListforCTC> getListofApprovedAgenda(long meetingId)
        {
            var ListofApprovedAgenda = (from row in entities.BM_MeetingAgendaMapping
                                        where row.MeetingID == meetingId && row.IsDeleted == false && row.IsActive == true
                                        && (row.Result == "A" || row.Result == "N")
                                        select new AgendaListforCTC
                                        {
                                            AgendaName = row.AgendaItemTextNew != null ? row.AgendaItemTextNew : row.AgendaItemText,
                                            MeetingId = row.MeetingID,
                                            AgendaId = row.AgendaID,
                                            MeetingAgendaId = row.MeetingAgendaMappingID,
                                            DirectorId = row.AuthorisedDirector
                                        }).ToList();
            return ListofApprovedAgenda;
        }

        public string GenerateCTCDocument(long meetingId, long agendaId)
        {
            throw new NotImplementedException();
        }

        public MeetingCTC saveCTCAuthorizedDirector(MeetingCTC obctc)
        {
            try
            {
                if (obctc != null)
                {
                    var chaekDirecto = (from row in entities.BM_MeetingAgendaMapping
                                        where row.MeetingID == obctc.MeetingId
                                        && row.AgendaID == obctc.AgendaId
                                        //&& row.AuthorisedDirector == null
                                        select row).FirstOrDefault();
                    if (chaekDirecto != null)
                    {
                        chaekDirecto.AuthorisedDirector = obctc.DirectorId;
                        entities.SaveChanges();
                    }
                }
                return obctc;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<AttendenceDetails> getAttendenceParticipantDetails(long meetingId)
        {
            try
            {
                return (from row in entities.BM_SP_GetParticipantAttendance(meetingId)
                        where row.Attendance != null
                        select new AttendenceDetails
                        {
                            Participant = row.Participant_Name,
                            Attendence = GetAttendanceText(row.Attendance.Trim())
                            //Attendence = (row.Attendance.Trim() == "P")||(row.Attendance.Trim() == "VC") ? "PRESENT" : "LEAVE OF ABSENCE"
                        }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string GetAttendanceText(string attdValue)
        {
            if (!string.IsNullOrEmpty(attdValue))
            {
                if (attdValue == "P")
                {
                    return "PRESENT";
                }
                else if (attdValue == "VC")
                {
                    return "PRESENT(VC/OAVM)";
                }
                else if (attdValue == "A")
                {
                    return "LEAVE OF ABSENCE";
                }
            }

            return string.Empty;
        }

        public List<AGMVoting> GetAgendaForAgm(long meetingId)
        {
            var result = new List<AGMVoting>();
            try
            {
                result = (from row in entities.BM_SP_MeetingAgendaItemsForVotingNoting(meetingId)
                          select new AGMVoting
                          {
                              MeetingID = meetingId,
                              AgendaId = row.AgendaId,
                              AgendaMappingID = row.MeetingAgendaMappingID,
                              Agenda = row.AgendaItem,
                              IsVoting = row.IsVoting,
                              AllowUpdate = row.AllowUpdate,
                              VotingResponse = row.Result,
                              Customer_Id = row.Customer_Id,
                              HasPreCommittee = row.HasPreCommittee,
                              PreCommitteResult = row.PreCommitteResult,

                              IsOrdinaryBusiness = row.IsOrdinaryBusiness,
                              IsSpecialResolution = row.IsSpecialResolution,

                              Business_ = row.Business,

                              PoposedBy = row.PoposedBy,
                              SecondedBy = row.SecondedBy
                          }).ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<AGMVoting> UpdateVirtualMeetingsAgendaResponse(List<AGMVoting> obj, int userId)
        {
            try
            {
                if (obj != null)
                {
                    foreach (var item in obj)
                    {
                        UpdateAGMAgendaResponse(item);
                    }
                    var first = obj.FirstOrDefault();
                    if (first.Customer_Id > 0 && first.MeetingID > 0)
                    {
                        #region Minutes for Qurum Agenda Item
                        var lstLeaveOfAbsence = (from row in entities.BM_MeetingParticipant
                                                 join attendance in entities.BM_MeetingAttendance on row.MeetingParticipantId equals attendance.MeetingParticipantId
                                                 join director in entities.BM_DirectorMaster on row.Director_Id equals director.Id
                                                 where row.Meeting_ID == first.MeetingID
                                                 && attendance.MeetingId == first.MeetingID
                                                 && attendance.Attendance == "A"
                                                 select new
                                                 {
                                                     director.Salutation,
                                                     director.FirstName,
                                                     director.MiddleName,
                                                     director.LastName,
                                                     attendance.Reason
                                                 }).ToList();
                        if (lstLeaveOfAbsence.Count > 0)
                        {
                            var lstAbsence = new List<string>();
                            foreach (var director in lstLeaveOfAbsence)
                            {
                                lstAbsence.Add((string.IsNullOrEmpty(director.Salutation) ? "" : director.Salutation + " ") + director.FirstName + (string.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.LastName) + " - " + (string.IsNullOrEmpty(director.Reason) ? "" : director.Reason));
                            }
                            objIMeeting_Service.SetMinutesForDefaultAgendaItem(first.MeetingID, SecretarialConst.DefaultAgendaUsedFor.LEAVE_OF_ABSENCE, lstAbsence, userId);
                        }
                        else
                        {
                            objIMeeting_Service.SetMinutesForDefaultAgendaItem(first.MeetingID, SecretarialConst.DefaultAgendaUsedFor.ALL_PRESENT, new List<string>(), userId);
                        }
                        #endregion

                        objIMeeting_Service.ConculdeMeeting(first.MeetingID, userId, (int)first.Customer_Id);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return obj;
        }

        public AGMVoting UpdateAGMAgendaResponse(AGMVoting agendaResponse)
        {
            try
            {
                var _obj = (from row in entities.BM_MeetingAgendaMapping
                            where row.MeetingAgendaMappingID == agendaResponse.AgendaMappingID && row.AgendaID == agendaResponse.AgendaId && row.MeetingID == row.MeetingID && row.IsActive == true
                            select row
                                     ).FirstOrDefault();
                if (_obj != null)
                {
                    if (_obj.Result == SecretarialConst.AgendaResult.DIFFERED && _obj.IsClosed == true)
                    {
                        //Defer agenda is used in another meeting
                    }
                    else
                    {
                        _obj.Result = agendaResponse.VotingResponse;
                        switch (agendaResponse.VotingResponse)
                        {
                            case SecretarialConst.AgendaResult.NOTED:
                                _obj.ResultRemark = "Noted";
                                break;
                            case SecretarialConst.AgendaResult.APPROVED:
                                _obj.ResultRemark = "Approved";
                                break;
                            case SecretarialConst.AgendaResult.DISAPPROVED:
                                _obj.ResultRemark = "Disapproved";
                                break;
                            case SecretarialConst.AgendaResult.DIFFERED:
                                _obj.ResultRemark = "Deferred";
                                break;
                            default:
                                break;
                        }
                        _obj.PoposedBy = agendaResponse.PoposedBy;
                        _obj.SecondedBy = agendaResponse.SecondedBy;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return agendaResponse;
        }

        public bool SaveTotalMemberPresentInAgm(MeetingAttendance_VM objAttendance, int userId)
        {
            bool success = false;
            try
            {
                //var checkMeetingPresent = (from row in entities.BM_Meetings where row.MeetingID == objAttendance.MeetingId select row).FirstOrDefault();
                //if (checkMeetingPresent != null)
                //{
                //    checkMeetingPresent.TotalMemberInAGM = objAttendance.TotalMemberForAgm;
                //    checkMeetingPresent.TotalMemberPresentInAGM = objAttendance.TotalMemberPresentForAgm;
                //    checkMeetingPresent.TotalProxyPresentInAGM = objAttendance.TotalproxyPresentForAgm;
                //    entities.SaveChanges();
                //    success = true;
                //}


                var _obj = (from row in entities.BM_MeetingMinutesDetails
                            where row.MeetingId == objAttendance.MeetingId && row.IsDeleted == false
                            select row).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.TotalMemberInAGM = objAttendance.TotalMemberForAgm;
                    _obj.TotalMemberPresentInAGM = objAttendance.TotalMemberPresentForAgm;
                    _obj.TotalProxyPresentInAGM = objAttendance.TotalproxyPresentForAgm;

                    //_obj.TotalValidProxy = objAttendance.TotalValidProxy;
                    //_obj.TotalMemberProxy = objAttendance.TotalMemberProxy;
                    //_obj.TotalShareHeldByProxy = objAttendance.TotalShareHeldByProxy;
                    //_obj.FaceValueOfEnquityShare = objAttendance.FaceValueOfEnquityShare;
                    //_obj.PercentageOfIssuedCapital = objAttendance.PercentageOfIssuedCapital;

                    _obj.UpdatedBy = userId;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();
                    success = true;
                }
                else
                {
                    _obj = new BM_MeetingMinutesDetails();
                    _obj.MeetingId = objAttendance.MeetingId;

                    _obj.TotalMemberInAGM = objAttendance.TotalMemberForAgm;
                    _obj.TotalMemberPresentInAGM = objAttendance.TotalMemberPresentForAgm;
                    _obj.TotalProxyPresentInAGM = objAttendance.TotalproxyPresentForAgm;

                    //_obj.TotalValidProxy = objAttendance.TotalValidProxy;
                    //_obj.TotalMemberProxy = objAttendance.TotalMemberProxy;
                    //_obj.TotalShareHeldByProxy = objAttendance.TotalShareHeldByProxy;
                    //_obj.FaceValueOfEnquityShare = objAttendance.FaceValueOfEnquityShare;
                    //_obj.PercentageOfIssuedCapital = objAttendance.PercentageOfIssuedCapital;

                    _obj.IsDeleted = false;
                    _obj.CreatedBy = userId;
                    _obj.CreatedOn = DateTime.Now;
                    entities.BM_MeetingMinutesDetails.Add(_obj);
                    entities.SaveChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                success = false;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return success;
        }

        public List<MeetingAttendance_VM> GetPerticipenforAuditorAttendenceCS(long meetingID, int customerId, int? userId)
        {
            try
            {
                var getParticipant = (from row in entities.BM_SP_GetAuditorParticipantAttendance(meetingID)
                                          //where row.UserId == userId
                                      select new MeetingAttendance_VM
                                      {
                                          ParticipantId = row.MeetingParticipantId,
                                          MeetingParticipant_Name = row.Participant_Name,
                                          MeetingId = row.Meeting_ID,
                                          PartnerName = row.PartnerName,
                                          //Director_Id = row.Director_Id,
                                          //ImagePath = row.Photo_Doc,
                                          Designation = row.Designation,
                                          Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                          Remark = row.Reason
                                      }).ToList();


                return getParticipant;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public List<MeetingAttendance_VM> GetPerticipenforInviteeAttendenceCS(long meetingID, int customerId, int? userId)
        {
            try
            {
                var getParticipant = (from row in entities.BM_SP_GetInviteeParticipantAttendance(meetingID)
                                          //where row.UserId == userId
                                      select new MeetingAttendance_VM
                                      {
                                          ParticipantId = row.MeetingParticipantId,
                                          MeetingParticipant_Name = row.ParticipantName,
                                          MeetingId = row.Meeting_ID,
                                          //Director_Id = row.Director_Id,
                                          //ImagePath = row.Photo_Doc,
                                          Designation = row.Designation,
                                          Attendance = row.Attendance != null ? row.Attendance.Trim() : "",
                                          Remark = row.Reason
                                      }).ToList();

                return getParticipant;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion


        #region Find User ParticipantId when user is Director (change by Ruchi on 15th of may 2020)
        public long getuserparticipantId(long meetingId, int userId)
        {
            //MeetingAttendance_VM obj = new MeetingAttendance_VM();
            try
            {
                var getParticipant = (from row in entities.BM_MeetingParticipant
                                      where row.Meeting_ID == meetingId
                                      && row.UserId == userId
                                      && row.IsDeleted == false
                                      select row.MeetingParticipantId).FirstOrDefault();
                if (getParticipant > 0)
                {
                    return getParticipant;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                return 0;
            }
        }

        public string MarkAttendenceofDirector(long meetingId, long participantId, int userId)
        {
            try
            {
                var markattendence = (from row in entities.BM_MeetingAttendance where row.MeetingParticipantId == participantId && row.MeetingId == meetingId select row).FirstOrDefault();
                if (markattendence != null)
                {
                    markattendence.Attendance = "P";
                    markattendence.UpdatedOn = DateTime.Now;
                    markattendence.Updatedby = userId;
                    entities.SaveChanges();
                }
                else
                {
                    BM_MeetingAttendance _objattendence = new BM_MeetingAttendance();
                    _objattendence.MeetingId = meetingId;
                    _objattendence.MeetingParticipantId = participantId;
                    _objattendence.Attendance = "P";
                    _objattendence.IsActive = true;
                    _objattendence.CreatedOn = DateTime.Now;
                    _objattendence.Createdby = userId;
                    entities.BM_MeetingAttendance.Add(_objattendence);
                    entities.SaveChanges();
                }

                return "Mark attendance successfully";

            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "Something went wrong";

            }
        }

        public void saveNotingVotingbyDir(MeetingAgendaSummary items)
        {
            try
            {
                var checkIsvotingNotingExist = (from row in entities.BM_Meetings_Agenda_Responses
                                                where row.Meeting_ID == items.MeetingId

&& row.MeetingParticipantID == items.ParticipantId
&& row.AgendaId == items.AgendaId
&& row.MeetingAgendaMappingId == items.MeetingAgendaMappingId
                                                select row).FirstOrDefault();
                if (checkIsvotingNotingExist != null)
                {
                    checkIsvotingNotingExist.Response = items.AgendaResponse;
                    checkIsvotingNotingExist.IsDeleted = false;
                    checkIsvotingNotingExist.UpdatedOn = DateTime.Now;
                    checkIsvotingNotingExist.UpdatedBy = 5;
                    entities.SaveChanges();
                }
                else
                {
                    BM_Meetings_Agenda_Responses objvotingnoting = new BM_Meetings_Agenda_Responses();
                    objvotingnoting.MeetingParticipantID = items.ParticipantId;
                    objvotingnoting.MeetingAgendaMappingId = (long)items.MeetingAgendaMappingId;
                    objvotingnoting.AgendaId = (long)items.AgendaId;
                    objvotingnoting.Meeting_ID = items.MeetingId;
                    objvotingnoting.Response = items.AgendaResponse;
                    objvotingnoting.IsDeleted = false;
                    objvotingnoting.CreatedOn = DateTime.Now;
                    objvotingnoting.CreatedBy = 5;
                    entities.BM_Meetings_Agenda_Responses.Add(objvotingnoting);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public List<MeetingAgendaSummary> AddCircularVotting(MeetingAgendaSummary vottings)
        {
            throw new NotImplementedException();
        }
        #endregion Find user participantId when user is Director(change by Ruchi on 15th of may 2020)

        #region Participant Details by UserId
        public ParticipantDetails GetParticipantDetails(long meetingId, int userId)
        {
            var result = new ParticipantDetails() { MeetingId = meetingId, MeetingParticipantId = 0, CanVoteNote = false };
            try
            {
                var details = (from row in entities.BM_MeetingParticipant
                               where row.Meeting_ID == meetingId && row.UserId == userId && row.IsDeleted == false
                               select new
                               {
                                   row.MeetingParticipantId,
                                   row.ParticipantType
                               }).FirstOrDefault();
                if (details != null)
                {
                    result.MeetingParticipantId = details.MeetingParticipantId;
                    result.ParticipantType = details.ParticipantType;
                    if (details.ParticipantType == SecretarialConst.MeetingParticipantsTypes.DIRECTOR || details.ParticipantType == SecretarialConst.MeetingParticipantsTypes.MEMBER)
                    {
                        result.CanVoteNote = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion
    }
}