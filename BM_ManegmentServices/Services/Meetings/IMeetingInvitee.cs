﻿using BM_ManegmentServices.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM_ManegmentServices.Services.Meetings
{
    public interface IMeetingInvitee
    {
        IEnumerable<MeetingInviteeVM> GetAll(long meetingID, bool loadAgendaItems);
        MeetingInviteeVM Create(MeetingInviteeVM template, int createdBy);
        MeetingInviteeVM Update(MeetingInviteeVM template, int updatedBy);
        MeetingInviteeVM DestroyInvitee(MeetingInviteeVM template, int updatedBy);
    }
}
