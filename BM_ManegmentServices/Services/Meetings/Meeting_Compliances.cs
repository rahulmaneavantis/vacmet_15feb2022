﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BM_ManegmentServices.VM;
//using com.VirtuosoITech.ComplianceManagement.Business.Data;
using BM_ManegmentServices.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using BM_ManegmentServices.Services.Masters;
using System.Reflection;
using BM_ManegmentServices.VM.Compliance;
using BM_ManegmentServices.Services.Compliance;
using System.Data.Entity.SqlServer;
using System.Configuration;

namespace BM_ManegmentServices.Services.Meetings
{
    public class Meeting_Compliances : IMeeting_Compliances
    {
        Compliance_SecretarialEntities entities = new Compliance_SecretarialEntities();

        ICompliance_Service objICompliance_Service;
        public Meeting_Compliances(ICompliance_Service objCompliance_Service)
        {
            objICompliance_Service = objCompliance_Service;
        }
        public IEnumerable<MeetingComplianceScheduleDetails_ResultVM> GetCompliances(long meetingAgendaMappingId, long meetingId)
        {
            //var result = from scheduleNew in entities.BM_ComplianceScheduleOnNew
            //             join com in entities.Compliances on scheduleNew.ComplianceID equals com.ID
            //             //join mapping in entities.BM_ComplianceMapping on scheduleNew.MCM_ID equals mapping.MCM_ID
            //             from status in entities.ComplianceStatus.Where(k => k.ID == scheduleNew.StatusId).DefaultIfEmpty()
            //            // from froms in entities.BM_MeetingFormMapping.Where(k => k.MeetingAgendaMappingId == meetingAgendaMappingId).Select(k=>k.MeetingAgendaMappingId).DefaultIfEmpty(0).FirstOrDefault()
            //             where scheduleNew.MeetingID == meetingId && scheduleNew.MeetingAgendaMappingId == meetingAgendaMappingId && scheduleNew.IsActive == true
            //             select new MeetingComplianceScheduleDetails_ResultVM
            //             {
            //                 MeetingID = (long)scheduleNew.MeetingID,
            //                 MeetingAgendaMappingID = scheduleNew .MeetingAgendaMappingId,
            //                 ComplianceID = (long)scheduleNew.ComplianceID,
            //                 MappingType = scheduleNew.MappingType,
            //                 ScheduleOnID = scheduleNew.ScheduleOnID,
            //                 ScheduleOn = scheduleNew.ScheduleOn,
            //                 //ScheduleOnTime = scheduleNew.ScheduleOn == null ? "" : scheduleNew.ToString(),
            //                 ClosedDate = scheduleNew.ClosedDate,
            //                 //ClosedDateTime = row.ClosedDateTime,
            //                 StatusId = scheduleNew.StatusId,
            //                 Status = status.Name,
            //                 AgendaItemHeading = "",
            //                 ShortDescription = com.ShortDescription,
            //                 Entity_ID = scheduleNew.EntityId,
            //                 //HasForms = froms
            //             };

            var result = (from row in entities.BM_SP_GetMeetingComplianceScheduleForAgenda(meetingId, meetingAgendaMappingId)
                          select new MeetingComplianceScheduleDetails_ResultVM
                          {
                              MappingType = row.MappingType,
                              ScheduleOnID = row.ScheduleOnID,
                              ScheduleOn = row.ScheduleOn,
                              ScheduleOnTime = row.ScheduleOnTime,
                              ClosedDate = row.ClosedDate,
                              ClosedDateTime = row.ClosedDateTime,
                              StatusId = row.StatusId,
                              Status = row.ComplianceStatus,
                              AgendaItemHeading = row.AgendaItemHeading,
                              ShortDescription = row.ShortDescription,
                              Entity_ID = row.EntityId,
                              MeetingID = row.MeetingID,
                              MeetingAgendaMappingID = row.MeetingAgendaMappingId,
                              ComplianceID = row.ComplianceId,
                              HasFormMapped = row.HasForms
                          }).ToList();

            return result.ToList();
        }


        #region Get Compliance Id for generate Meeting Level schedule
        public Message GenerateScheduleOn(long meetingId, string mappingType, int customerId, int createdBy)
        {
            var result = new Message();
            try
            {
                var entityId = entities.BM_Meetings.Where(k => k.MeetingID == meetingId && k.Customer_Id == customerId && k.IsDeleted == false).Select(k => k.EntityId).FirstOrDefault();
                return GenerateScheduleOn(meetingId, mappingType, entityId, customerId, createdBy, false);
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Something went wrong.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        public Message GenerateScheduleOn(long meetingId, string mappingType, int entityId, int customerId, int createdBy, bool evoting)
        {
            var result = new Message();
            try
            {
                List<BM_ComplianceScheduleAssignment> lstUsers = null;
                var customerBranchId = objICompliance_Service.GetCustomerBranchID(entityId, customerId);

                var lstComplianceIdForScheduleOn = (from row in entities.BM_SP_GetComplianceIdForSchedule(entityId, meetingId, mappingType, customerId)
                                                    select new ComplianceScheduleOnVM
                                                    {
                                                        ScheduleOnID = 0,
                                                        MCM_ID = row.MCM_ID,
                                                        AgendaMasterId = row.AgendaMasterID,
                                                        ComplianceId = row.ComplianceID,
                                                        MeetingAgendaMappingId = (long) row.MeetingAgendaMappingId,
                                                        AutoCompleteOn = row.AutoCompleteOn
                                                    }).ToList();

                if (lstComplianceIdForScheduleOn != null)
                {
                    foreach (var item in lstComplianceIdForScheduleOn)
                    {
                        if(evoting == false && item.AutoCompleteOn == SecretarialConst.ComplianceAutoCompleteOn.E_VOTING)
                        {
                            continue;
                        }
                        lstUsers = null;
                        var instanceId = objICompliance_Service.GetInstanceId(customerBranchId, item.ComplianceId);

                        if(instanceId>0)
                        {
                            //Added on 08 Oct 2021
                            var IsDocMan_NonMan = true;
                            using (Compliance_SecretarialEntities dbEntities = new Compliance_SecretarialEntities())
                            {
                                var complianceType = (from row in dbEntities.Compliances
                                                   where row.ID == item.ComplianceId
                                                   select row.ComplianceType).FirstOrDefault();

                                IsDocMan_NonMan = complianceType == 1 ? false : true;
                            }
                            //End

                            var complianceScheduleOnOld = new BM_ManegmentServices.Data.ComplianceScheduleOn
                            {
                                ComplianceInstanceID = instanceId,
                                ForMonth = "",
                                ForPeriod = null,
                                IsActive = true,
                                IsUpcomingNotDeleted = true,
                                ScheduleOn = DateTime.Now,
                                IsDocMan_NonMan = IsDocMan_NonMan
                            };

                            entities.ComplianceScheduleOns.Add(complianceScheduleOnOld);
                            entities.SaveChanges();

                            if(complianceScheduleOnOld.ID > 0)
                            {
                                var complianceScheduleOn = new BM_ComplianceScheduleOnNew
                                {
                                    ComplianceInstanceID = instanceId,
                                    ScheduleOnID = complianceScheduleOnOld.ID,
                                    ComplianceID = item.ComplianceId,
                                    MeetingID = meetingId,
                                    MappingType = mappingType,
                                    MCM_ID = item.MCM_ID,
                                    AgendaMasterID = item.AgendaMasterId,
                                    CreatedBy = createdBy,
                                    CreatedOn = DateTime.Now,
                                    IsActive = true,
                                    IsDeleted = false,
                                    EntityId = entityId,
                                    Customer_Id = customerId,
                                    MeetingAgendaMappingId = item.MeetingAgendaMappingId
                                };

                                entities.BM_ComplianceScheduleOnNew.Add(complianceScheduleOn);
                                entities.SaveChanges();

                                #region Meeting - Form mapping
                                var lstForms = (from mapping in entities.BM_ComplianceFormMapping
                                                join forms in entities.BM_FormMaster on mapping.FormID equals forms.FormID
                                                where mapping.ComplianceId == item.ComplianceId && mapping.IsDeleted == false &&
                                                      forms.IsActive == true && forms.IsDeleted == false
                                                select new
                                                {
                                                    forms.FormID,
                                                    forms.FormName,
                                                    forms.FormFormat
                                                }).ToList();
                                if(lstForms != null)
                                {
                                    var lstMeetingForms = (from row in lstForms
                                                           select new BM_MeetingFormMapping
                                                           {
                                                               MeetingId = meetingId,
                                                               MeetingAgendaMappingId = item.MeetingAgendaMappingId,
                                                               ComplianceId = item.ComplianceId,
                                                               FormID = row.FormID,
                                                               FormName = row.FormName,
                                                               Format_ = row.FormFormat,

                                                               IsDeleted = false,
                                                               CreatedBy = createdBy,
                                                               CreatedOn = DateTime.Now
                                                           }).ToList();

                                    entities.BM_MeetingFormMapping.AddRange(lstMeetingForms);
                                    entities.SaveChanges();
                                }
                                #endregion

                                //if (complianceScheduleOn.ScheduleOnID > 0)
                                //{
                                //    #region Get Performer/Reviewer list
                                //    var lstAssignment = objICompliance_Service.GetAssignment(entityId, item.ComplianceId);
                                //    if (lstAssignment != null)
                                //    {
                                //        lstUsers = lstAssignment.Select(row => new BM_ComplianceScheduleAssignment
                                //        {
                                //            ScheduleOnID = complianceScheduleOn.ScheduleOnID,
                                //            RoleID = row.RoleID,
                                //            UserID = row.UserID,
                                //            IsDeleted = false,
                                //            CreatedBy = createdBy,
                                //            CreatedOn = DateTime.Now
                                //        }).ToList();
                                //    }

                                //    #region Insert Performer / Reviewer Default
                                //    if (lstUsers == null)
                                //    {
                                //        lstUsers = new List<BM_ComplianceScheduleAssignment>();
                                //    }

                                //    if (lstUsers.Where(k => k.RoleID == SecretarialConst.RoleID.PERFORMER).Any() == false)
                                //    {
                                //        lstUsers.Add(new BM_ComplianceScheduleAssignment()
                                //        {
                                //            ScheduleOnID = complianceScheduleOn.ScheduleOnID,
                                //            RoleID = SecretarialConst.RoleID.PERFORMER,
                                //            IsDeleted = false,
                                //            CreatedBy = createdBy,
                                //            CreatedOn = DateTime.Now,
                                //        });
                                //    }

                                //    if (lstUsers.Where(k => k.RoleID == SecretarialConst.RoleID.REVIEWER).Any() == false)
                                //    {
                                //        lstUsers.Add(new BM_ComplianceScheduleAssignment()
                                //        {
                                //            ScheduleOnID = complianceScheduleOn.ScheduleOnID,
                                //            RoleID = SecretarialConst.RoleID.REVIEWER,
                                //            IsDeleted = false,
                                //            CreatedBy = createdBy,
                                //            CreatedOn = DateTime.Now,
                                //        });
                                //    }
                                //    #endregion

                                //    #endregion

                                //    entities.BM_ComplianceScheduleAssignment.AddRange(lstUsers);
                                //    entities.SaveChanges();
                                //}
                            }
                        }

                    }
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Something went wrong.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

            return result;
        }
        #endregion

        //Pending Conversion
        #region Get Meeting -> Compliance Performer / Reviewer
        public List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceScheduleAssignment(long meetingId, string MappingType)
        {
            try
            {
                //var result = (from row in entities.BM_SP_GetMeetingComplianceScheduleAssignment(meetingId)
                //              select new MeetingComplianceScheduleDetails_ResultVM
                //              {
                //                  AssignmentID = row.AssignmentID,
                //                  MeetingID = row.MeetingID,
                //                  MappingType = row.MappingType,
                //                  PartId = row.PartId,
                //                  SrNo = row.SrNo,
                //                  ScheduleOnID = row.ScheduleOnID,
                //                  ScheduleOn = row.ScheduleOn,
                //                  AgendaItemHeading = row.AgendaItemHeading,
                //                  ShortDescription = row.ShortDescription,
                //                  RoleID = row.RoleID,
                //                  Role = row.Role,
                //                  UserID = row.UserID,
                //                  UserName = row.UserName,
                //                  User = new PerformerReviewerViewModel()
                //                  {
                //                      UserID = row.UserID,
                //                      FullName = row.UserName
                //                  }
                //              });

                var result = (from row in entities.BM_SP_GetMeetingComplianceScheduleAssignment(meetingId)
                              select new MeetingComplianceScheduleDetails_ResultVM
                              {
                                  AssignmentID = row.AssignmentID,
                                  MeetingID = row.MeetingID,
                                  MappingType = row.MappingType,
                                  PartId = row.PartId,
                                  SrNo = row.SrNo,
                                  ScheduleOnID = row.ScheduleOnID,
                                  ScheduleOn = row.ScheduleOn,
                                  AgendaItemHeading = row.AgendaItemHeading,
                                  ShortDescription = row.ShortDescription,
                                  RoleID = row.RoleID,
                                  Role = row.Role,
                                  UserID = row.UserID,
                                  UserName = row.UserName,
                                  User = new PerformerReviewerViewModel()
                                  {
                                      UserID = row.UserID,
                                      FullName = row.UserName
                                  }
                              });

                if (MappingType == SecretarialConst.ComplianceMappingType.MEETING || MappingType == SecretarialConst.ComplianceMappingType.AGENDA)
                {
                    result = result.Where(k => k.MappingType == MappingType);
                }
                return result.ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion
        //Pending Conversion
        #region Meeting -> Compliance ReAssignment
        public List<MeetingComplianceScheduleDetails_ResultVM> MeetingComplianceScheduleReAssignment(List<MeetingComplianceScheduleDetails_ResultVM> lstUsers, int updatedBy)
        {
            try
            {
                foreach (var item in lstUsers)
                {
                    if (item.User != null)
                    {
                        if (item.User.UserID > 0)
                        {
                            var obj = entities.BM_ComplianceScheduleAssignment.Where(k => k.AssignmentID == item.AssignmentID).FirstOrDefault();
                            if (obj != null)
                            {
                                if (obj.UserID == 0)
                                {

                                }
                                obj.UserID = item.User.UserID;
                                obj.UpdatedBy = updatedBy;
                                obj.UpdatedOn = DateTime.Now;
                                entities.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return lstUsers;
        }
        #endregion

        #region Meeting -> Compliance schedule
        public List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceSchedule(long meetingId)
        {
            try
            {
                var result = (from row in entities.BM_SP_GetMeetingComplianceSchedule(meetingId)
                              select new MeetingComplianceScheduleDetails_ResultVM
                              {
                                  MappingType = row.MappingType,
                                  ScheduleOnID = row.ScheduleOnID,
                                  ScheduleOn = row.ScheduleOn,
                                  ScheduleOnTime = row.ScheduleOnTime,
                                  ClosedDate = row.ClosedDate,
                                  ClosedDateTime = row.ClosedDateTime,
                                  StatusId = row.StatusId,
                                  Status = row.ComplianceStatus,
                                  AgendaItemHeading = row.AgendaItemHeading,
                                  ShortDescription = row.ShortDescription,
                                  Entity_ID = row.EntityId,
                                  MeetingID = row.MeetingID,
                                  MeetingAgendaMappingID = row.MeetingAgendaMappingId,
                                  ComplianceID = row.ComplianceId,
                                  HasFormMapped = row.HasForms
                              }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public List<MeetingComplianceScheduleDetails_ResultVM> GetMeetingComplianceSchedule(long meetingId, string mappingType, bool showCalendarEvent = false)
        {
            var result = new List<MeetingComplianceScheduleDetails_ResultVM>();
            try
            {
                if(showCalendarEvent)
                {
                    result = (from row in entities.BM_SP_GetMeetingComplianceSchedule(meetingId)
                              where row.MappingType == mappingType && row.IsEvent == showCalendarEvent
                              select new MeetingComplianceScheduleDetails_ResultVM
                              {
                                  MappingType = row.MappingType,
                                  ScheduleOnID = row.ScheduleOnID,
                                  ScheduleOn = row.ScheduleOn,
                                  ScheduleOnTime = row.ScheduleOnTime,
                                  ClosedDate = row.ClosedDate,
                                  ClosedDateTime = row.ClosedDateTime,
                                  StatusId = row.StatusId,
                                  Status = row.ComplianceStatus,
                                  AgendaItemHeading = row.AgendaItemHeading,
                                  ShortDescription = row.ShortDescription,
                                  Entity_ID = row.EntityId,
                                  MeetingID = row.MeetingID,
                                  MeetingAgendaMappingID = row.MeetingAgendaMappingId,
                                  ComplianceID = row.ComplianceId,
                                  HasFormMapped = row.HasForms,
                                  IsCalendarEvent = row.IsEvent,
                                  AllowDelete = row.AllowDelete
                              }).ToList();
                }
                else
                {
                    result = (from row in entities.BM_SP_GetMeetingComplianceSchedule(meetingId)
                              where row.MappingType == mappingType
                              select new MeetingComplianceScheduleDetails_ResultVM
                              {
                                  MappingType = row.MappingType,
                                  ScheduleOnID = row.ScheduleOnID,
                                  ScheduleOn = row.ScheduleOn,
                                  ScheduleOnTime = row.ScheduleOnTime,
                                  ClosedDate = row.ClosedDate,
                                  ClosedDateTime = row.ClosedDateTime,
                                  StatusId = row.StatusId,
                                  Status = row.ComplianceStatus,
                                  AgendaItemHeading = row.AgendaItemHeading,
                                  ShortDescription = row.ShortDescription,
                                  Entity_ID = row.EntityId,
                                  MeetingID = row.MeetingID,
                                  MeetingAgendaMappingID = row.MeetingAgendaMappingId,
                                  ComplianceID = row.ComplianceId,
                                  HasFormMapped = row.HasForms,
                                  IsCalendarEvent = row.IsEvent,
                                  AllowDelete = row.AllowDelete
                              }).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public ComplianceScheduleDetailsVM GetScheduleDetails(long scheduleOnID, int entityID, int customerID)
        {
            try
            {
                //Commented & Added on 29 Dec 2021 
                //var result = (from schedule in entities.BM_ComplianceScheduleOnNew
                //              join mapping in entities.BM_ComplianceMapping on schedule.MCM_ID equals mapping.MCM_ID
                //              join compliance in entities.Compliances on schedule.ComplianceID equals compliance.ID
                //              where schedule.ScheduleOnID == scheduleOnID 
                //              && schedule.IsActive == true
                //              && schedule.EntityId == entityID 
                //              && schedule.Customer_Id == customerID
                //              select new ComplianceScheduleDetailsVM
                //              {
                //                  ScheduleOnID = schedule.ScheduleOnID,
                //                  ScheduleOn = schedule.ScheduleOn,
                //                  Meeting_ID = schedule.MeetingID,
                //                  MeetingAgendaMappingId = schedule.MeetingAgendaMappingId,
                //                  AgendaMasterId = schedule.AgendaMasterID,
                //                  MappingType = schedule.MappingType,
                //                  MCM_ID = schedule.MCM_ID,
                //                  DaysOrHours = mapping.DaysOrHours,
                //                  ComplianceId =(long)schedule.ComplianceID,
                //                  EntityId = entityID,

                //                  Compliance = new ComplianceDetailsVM()
                //                  {
                //                      ActID = compliance.ActID,
                //                      ComplianceId = compliance.ID,
                //                      Description = compliance.ShortDescription,
                //                      DetailedDescription = compliance.Description,
                //                      RiskType = compliance.RiskType,
                //                      SampleFormLink = compliance.SampleFormLink,
                //                  }
                //              }).FirstOrDefault();

                var customerBranchId = (from row in entities.BM_EntityMaster
                                        where row.Id == entityID
                                        select row.CustomerBranchId).FirstOrDefault();

                var result = (from schedule in entities.ComplianceScheduleOns
                              join ci in entities.ComplianceInstances on schedule.ComplianceInstanceID equals ci.ID
                              join compliance in entities.Compliances on ci.ComplianceId equals compliance.ID
                              where schedule.ID == scheduleOnID
                              && schedule.IsActive == true
                              && ci.CustomerBranchID == customerBranchId
                              select new ComplianceScheduleDetailsVM
                              {
                                  ScheduleOnID = schedule.ID,
                                  ScheduleOn = schedule.ScheduleOn,
                                  //Meeting_ID = schedule.MeetingID,
                                  //MeetingAgendaMappingId = schedule.MeetingAgendaMappingId,
                                  //AgendaMasterId = schedule.AgendaMasterID,
                                  //MappingType = schedule.MappingType,
                                  //MCM_ID = schedule.MCM_ID,
                                  DaysOrHours = "",
                                  ComplianceId = compliance.ID,
                                  EntityId = entityID,

                                  Compliance = new ComplianceDetailsVM()
                                  {
                                      ActID = compliance.ActID,
                                      ComplianceId = compliance.ID,
                                      Description = compliance.ShortDescription,
                                      DetailedDescription = compliance.Description,
                                      RiskType = compliance.RiskType,
                                      SampleFormLink = compliance.SampleFormLink,
                                  }
                              }).FirstOrDefault();

                //End

                if (result != null)
                {
                    //Added on 29 Dec 2021
                    var scheduleOnNew = (from schedule in entities.BM_ComplianceScheduleOnNew
                                         from mapping in entities.BM_ComplianceMapping.Where(k => k.MCM_ID == schedule.MCM_ID).DefaultIfEmpty()
                                         where schedule.ScheduleOnID == result.ScheduleOnID
                                         select new
                                         {
                                             schedule.MeetingID,
                                             schedule.MeetingAgendaMappingId,
                                             schedule.AgendaMasterID,
                                             schedule.MappingType,
                                             schedule.MCM_ID,
                                             mapping.DaysOrHours,
                                         }).FirstOrDefault();

                    if(scheduleOnNew != null)
                    {
                        result.Meeting_ID = scheduleOnNew.MeetingID;
                        result.MeetingAgendaMappingId = scheduleOnNew.MeetingAgendaMappingId;
                        result.AgendaMasterId = scheduleOnNew.AgendaMasterID;
                        result.MappingType = scheduleOnNew.MappingType;
                        result.MCM_ID = scheduleOnNew.MCM_ID;
                        result.DaysOrHours = scheduleOnNew.DaysOrHours;
                    }
                    //End

                    result.Act = (from act in entities.Acts
                                  where act.ID == result.Compliance.ActID
                                  select new ActVM
                                  {
                                      ActId = act.ID,
                                      ActName = act.Name
                                  }).FirstOrDefault();

                    result.TransactionLog = (from log in entities.ComplianceTransactions
                                             from status in entities.ComplianceStatus.Where(k => k.ID == log.StatusId).DefaultIfEmpty()
                                             where log.ComplianceScheduleOnID == result.ScheduleOnID
                                             orderby log.ID descending
                                             select new ComplianceTransactionLogVM
                                             {
                                                 TransactionID = log.ID,
                                                 StatusId = log.StatusId,
                                                 Status = status == null ? "" : status.ID == 1 ? result.ScheduleOn > DateTime.Now ? "Upcoming" : "Overdue" : status.Name,
                                                 Remarks = log.Remarks,
                                                 Dated = log.Dated,
                                                 CreatedBy = log.CreatedBy,
                                                 CreatedByText = log.CreatedByText,
                                                 StatusChangedOn = log.StatusChangedOn
                                             }).ToList();

                    result.MeetingTitle = (from row in entities.BM_Meetings
                                           where row.MeetingID == result.Meeting_ID
                                           select row.MeetingTitle).FirstOrDefault();
                    
                    if(result.MeetingAgendaMappingId > 0)
                    {
                        result.AgendaItem = (from row in entities.BM_MeetingAgendaMapping
                                               where row.MeetingAgendaMappingID == result.MeetingAgendaMappingId && row.MeetingID == result.Meeting_ID && row.AgendaID == result.AgendaMasterId
                                               select row.AgendaItemTextNew).FirstOrDefault();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public ComplianceScheduleDetailsVM GetScheduleDetailsNew(long scheduleOnID, int userID)
        {
            var result = new ComplianceScheduleDetailsVM();
            try
            {
                //Commented & Added on 29 Dec 2021 
                //result = (from schedule in entities.BM_ComplianceScheduleOnNew
                //          from mapping in entities.BM_ComplianceMapping.Where(mapping => mapping.MCM_ID == schedule.MCM_ID).DefaultIfEmpty()
                //          join compliance in entities.Compliances on schedule.ComplianceID equals compliance.ID
                //          join instance in entities.ComplianceInstances on schedule.ComplianceInstanceID equals instance.ID
                //          where schedule.ScheduleOnID == scheduleOnID && schedule.IsActive == true
                //          //&& schedule.EntityId == entityID && schedule.Customer_Id == customerID
                //          select new ComplianceScheduleDetailsVM
                //          {
                //              ScheduleOnID = schedule.ScheduleOnID,
                //              ScheduleOn = schedule.ScheduleOn,
                //              Meeting_ID = schedule.MeetingID,
                //              MeetingAgendaMappingId = schedule.MeetingAgendaMappingId,
                //              AgendaMasterId = schedule.AgendaMasterID,
                //              MappingType = schedule.MappingType,
                //              MCM_ID = schedule.MCM_ID,
                //              DaysOrHours = mapping.DaysOrHours,
                //              ComplianceId = (long)schedule.ComplianceID,
                //              EntityId = schedule.EntityId,
                //              DirectorId = instance.DirectorId,
                //              ComplianceInstanceId= schedule.ComplianceInstanceID,
                //              IsCheckList = (compliance.ComplianceType == 1 && compliance.Status == null && ( compliance.ComplinceVisible == true || compliance.ComplinceVisible == null)),
                //              Compliance = new ComplianceDetailsVM()
                //              {
                //                  ActID = compliance.ActID,
                //                  ComplianceId = compliance.ID,
                //                  Description = compliance.ShortDescription,
                //                  DetailedDescription = compliance.Description,
                //                  RiskType = compliance.RiskType,
                //                  SampleFormLink = compliance.SampleFormLink,
                //                  Sec = compliance.Sections
                //              }
                //          }).FirstOrDefault();

                result = (from schedule in entities.ComplianceScheduleOns
                          join ci in entities.ComplianceInstances on schedule.ComplianceInstanceID equals ci.ID
                          join compliance in entities.Compliances on ci.ComplianceId equals compliance.ID
                          where schedule.ID == scheduleOnID && schedule.IsActive == true
                          select new ComplianceScheduleDetailsVM
                          {
                              ScheduleOnID = schedule.ID,
                              ScheduleOn = schedule.ScheduleOn,
                              //Meeting_ID = schedule.MeetingID,
                              //MeetingAgendaMappingId = schedule.MeetingAgendaMappingId,
                              //AgendaMasterId = schedule.AgendaMasterID,
                              //MappingType = schedule.MappingType,
                              //MCM_ID = schedule.MCM_ID,
                              //DaysOrHours = mapping.DaysOrHours,
                              ComplianceId = compliance.ID,
                              //EntityId = schedule.EntityId,
                              DirectorId = ci.DirectorId,
                              ComplianceInstanceId = schedule.ComplianceInstanceID,
                              IsCheckList = (compliance.ComplianceType == 1 && compliance.Status == null && (compliance.ComplinceVisible == true || compliance.ComplinceVisible == null)),
                              Compliance = new ComplianceDetailsVM()
                              {
                                  ActID = compliance.ActID,
                                  ComplianceId = compliance.ID,
                                  Description = compliance.ShortDescription,
                                  DetailedDescription = compliance.Description,
                                  RiskType = compliance.RiskType,
                                  SampleFormLink = compliance.SampleFormLink,
                                  Sec = compliance.Sections
                              }
                          }).FirstOrDefault();

                //End

                if (result != null)
                {
                    //Added on 29 Dec 2021
                    var scheduleOnNew = (from schedule in entities.BM_ComplianceScheduleOnNew
                                         from mapping in entities.BM_ComplianceMapping.Where(k => k.MCM_ID == schedule.MCM_ID).DefaultIfEmpty()
                                         where schedule.ScheduleOnID == result.ScheduleOnID
                                         select new
                                         {
                                             schedule.MeetingID,
                                             schedule.MeetingAgendaMappingId,
                                             schedule.AgendaMasterID,
                                             schedule.MappingType,
                                             schedule.MCM_ID,
                                             mapping.DaysOrHours,
                                         }).FirstOrDefault();

                    if (scheduleOnNew != null)
                    {
                        result.Meeting_ID = scheduleOnNew.MeetingID;
                        result.MeetingAgendaMappingId = scheduleOnNew.MeetingAgendaMappingId;
                        result.AgendaMasterId = scheduleOnNew.AgendaMasterID;
                        result.MappingType = scheduleOnNew.MappingType;
                        result.MCM_ID = scheduleOnNew.MCM_ID;
                        result.DaysOrHours = scheduleOnNew.DaysOrHours;
                    }
                    //End
                    result.Act = (from act in entities.Acts
                                  where act.ID == result.Compliance.ActID
                                  select new ActVM
                                  {
                                      ActId = act.ID,
                                      ActName = act.Name
                                  }).FirstOrDefault();

                    result.TransactionLog = (from log in entities.ComplianceTransactions
                                             from status in entities.ComplianceStatus.Where(k => k.ID == log.StatusId).DefaultIfEmpty()
                                             where log.ComplianceScheduleOnID == result.ScheduleOnID
                                             orderby log.ID descending
                                             select new ComplianceTransactionLogVM
                                             {
                                                 TransactionID = log.ID,
                                                 StatusId = log.StatusId,
                                                 Status = status == null ? "" : status.ID == 1 ? result.ScheduleOn > DateTime.Now ? "Upcoming" : "Overdue" : status.Name,
                                                 Remarks = log.Remarks,
                                                 Dated = log.Dated,
                                                 CreatedBy = log.CreatedBy,
                                                 CreatedByText = log.CreatedByText,
                                                 StatusChangedOn = log.StatusChangedOn
                                             }).ToList();

                    if(result.Meeting_ID > 0)
                    {
                        result.MeetingTitle = (from row in entities.BM_Meetings
                                               where row.MeetingID == result.Meeting_ID
                                               select row.MeetingTitle).FirstOrDefault();
                    }

                    if (result.MeetingAgendaMappingId > 0)
                    {
                        result.AgendaItem = (from row in entities.BM_MeetingAgendaMapping
                                             where row.MeetingAgendaMappingID == result.MeetingAgendaMappingId && row.MeetingID == result.Meeting_ID && row.AgendaID == result.AgendaMasterId
                                             select row.AgendaItemTextNew).FirstOrDefault();
                    }

                    if(result.DirectorId > 0)
                    {
                        var director = (from row in entities.BM_DirectorMaster
                                        where row.Id == result.DirectorId
                                        select new
                                        {
                                            row.Salutation,
                                            row.FirstName,
                                            row.MiddleName,
                                            row.LastName
                                        }).FirstOrDefault();
                        if(director != null)
                        {
                            result.DirectorName = director.Salutation + " " + director.FirstName +
                                                    (String.IsNullOrEmpty(director.MiddleName) ? "" : " " + director.MiddleName) +
                                                    (String.IsNullOrEmpty(director.LastName) ? "" : " " + director.LastName);
                        }

                    }

                    #region check Peformer & Reviewer is same user
                    var lstUserDetails = (from instance in entities.ComplianceInstances
                                          join assingment in entities.ComplianceAssignments on instance.ID equals assingment.ComplianceInstanceID
                                          where instance.ID == result.ComplianceInstanceId && instance.IsDeleted == false
                                          select new
                                          {
                                              assingment.UserID,
                                              assingment.RoleID
                                          }).ToList();
                    if (lstUserDetails != null)
                    {
                        var isPerformer = (from assignment in lstUserDetails
                                           where assignment.RoleID == SecretarialConst.RoleID.PERFORMER
                                           && assignment.UserID == userID
                                           select assignment.RoleID).Any();

                        var isReviewer = (from assignment in lstUserDetails
                                          where assignment.RoleID == SecretarialConst.RoleID.REVIEWER
                                          && assignment.UserID == userID
                                          select assignment.RoleID).Any();

                        if(isPerformer && isReviewer)
                        {
                            result.IsPerformerReviewerSameUser = true;
                        }
                    }
                    #endregion

                    string IS_ICSI_MODE = ConfigurationManager.AppSettings["AVASEC_IS_ICSI_MODE"].ToString();
                    result.Is_ICSIMODE = IS_ICSI_MODE == "1" ? true : false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<FileDataDocumentVM> GetComplianceDocuments(long scheduleOnID)
        {
            try
            {
                var result = from row in entities.FileDataMappings
                             join fileData in entities.FileDatas on row.FileID equals fileData.ID
                             //from u in entities.Users.Where(k => k.ID == fileData.).DefaultIfEmpty()
                             where row.ScheduledOnID == scheduleOnID
                             select new FileDataDocumentVM
                             {
                                 FileID = fileData.ID,
                                 FileName = fileData.Name,
                                 Version = fileData.Version,
                                 //  UploadedBy = u == null ? "" : u.FirstName + " " + u.LastName
                                 FileType = row.FileType
                             };
                return result.ToList();
            }
            catch (Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion

        #region Meeting Compliance  Generate Schedule date
        public Message CalculateScheduleDate(long meetingID, int updatedBy)
        {
            var result = new Message();
            try
            {
                entities.BM_SP_CalculateScheduleOnForMeeting(meetingID, updatedBy);
                entities.SaveChanges();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = "Server Error occured.";
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region Update Event Calendar date
        public Message UpdateEventCalendarDate(MeetingComplianceScheduleDetails_ResultVM obj, int updatedBy)
        {
            var result = new Message();
            try
            {
                var _obj = (from row in entities.BM_ComplianceScheduleOnNew
                            where row.ScheduleOnID == obj.ScheduleOnID
                            select row).FirstOrDefault();
                if(_obj != null)
                {
                    _obj.ScheduleOn = obj.ScheduleOn;
                    entities.SaveChanges();

                    if(obj.ScheduleOn != null)
                    {
                        var _objSchedule = (from row in entities.ComplianceScheduleOns
                                    where row.ID == obj.ScheduleOnID
                                    select row).FirstOrDefault();
                        if(_objSchedule != null)
                        {
                            _objSchedule.ScheduleOn = (DateTime)obj.ScheduleOn;
                            entities.SaveChanges();
                        }
                        result.Success = true;
                        result.Message = SecretarialConst.Messages.updateSuccess;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }
        #endregion

        #region delete scheduleon
        public Message DeleteShceduleOnById(long id, int updatedBy)
        {
            var result = new Message();
            try
            {
                var _objSchedule = (from row in entities.ComplianceScheduleOns
                                   where row.ID == id
                                   select row).FirstOrDefault();
                if (_objSchedule != null)
                {
                    _objSchedule.IsActive = false;
                    _objSchedule.IsUpcomingNotDeleted = false;
                    entities.SaveChanges();
                }

                var _obj = (from row in entities.BM_ComplianceScheduleOnNew
                            where row.ScheduleOnID == id
                            select row).FirstOrDefault();
                if (_obj != null)
                {
                    _obj.IsDeleted = true;
                    _obj.IsActive = false;
                    _obj.UpdatedBy = updatedBy;
                    _obj.UpdatedOn = DateTime.Now;
                    entities.SaveChanges();

                    result.Success = true;
                    result.Message = SecretarialConst.Messages.deleteSuccess;
                }
            }
            catch (Exception ex)
            {
                result.Error = true;
                result.Message = SecretarialConst.Messages.serverError;
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return result;
        }

        public List<tbl_ChecklistRemark> GetAllRemark()
        {
            List<tbl_ChecklistRemark> _objremark = new List<tbl_ChecklistRemark>();
           try
            {
                _objremark = (from x in entities.tbl_ChecklistRemark
                              select x).ToList();
            }
            catch(Exception ex)
            {
                LoggerMessage_AVASEC.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return _objremark;
        }
        #endregion
    }
}