//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_EntityDocuments
    {
        public long Id { get; set; }
        public int CustomerId { get; set; }
        public int EntityId { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> BMDate_LastRenew { get; set; }
        public Nullable<System.DateTime> GMDate_UpcomingRenew { get; set; }
        public string License_RegNumber { get; set; }
        public string AlteredDoc { get; set; }
        public Nullable<int> FYID { get; set; }
        public string StartTimeAGM { get; set; }
        public string EndTimeAGM { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<long> ChargeHeaderId { get; set; }
        public Nullable<long> ChargeDetailId { get; set; }
        public Nullable<long> ParticularChargeId { get; set; }
        public Nullable<int> NatureOfTransactionId { get; set; }
    }
}
