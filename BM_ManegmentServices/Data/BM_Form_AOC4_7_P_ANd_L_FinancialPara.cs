//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_Form_AOC4_7_P_ANd_L_FinancialPara
    {
        public long Id { get; set; }
        public long AOC4Id { get; set; }
        public Nullable<decimal> PROPOSED_DIVIDND { get; set; }
        public Nullable<decimal> PROPOSED_DIV_PER { get; set; }
        public Nullable<decimal> BASIC_EARNING_PS { get; set; }
        public Nullable<decimal> DILUTED_EARN_PS { get; set; }
        public Nullable<decimal> INCOME_FOREIGN { get; set; }
        public Nullable<decimal> EXPENDIT_FOREIGN { get; set; }
        public Nullable<decimal> REVENUE_SUBSIDIE { get; set; }
        public Nullable<decimal> RENT_PAID { get; set; }
        public Nullable<decimal> CONSUMPTION_STOR { get; set; }
        public Nullable<decimal> GROSS_VALUE_TRAN { get; set; }
        public Nullable<decimal> BAD_DEBTS_RP { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
