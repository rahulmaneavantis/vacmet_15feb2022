//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_DirectorListForImportFromOtherCust_Result
    {
        public int CustId { get; set; }
        public long DirectorId { get; set; }
        public string DIN { get; set; }
        public string PAN { get; set; }
        public string DirectorName { get; set; }
        public string EmailId { get; set; }
    }
}
