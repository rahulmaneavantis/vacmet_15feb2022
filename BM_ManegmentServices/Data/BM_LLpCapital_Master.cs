//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_LLpCapital_Master
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public int Customer_Id { get; set; }
        public string DPIN_No { get; set; }
        public string Partner_Name { get; set; }
        public decimal Capital_Contribution { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public bool IsDPIN_No { get; set; }
        public string PAN { get; set; }
        public string Address { get; set; }
        public Nullable<int> DesignatedPartnerId { get; set; }
        public Nullable<decimal> DePCapital_Contribution { get; set; }
    }
}
