//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_GetAuditorParticipantAttendance_Result
    {
        public long Meeting_ID { get; set; }
        public long MeetingParticipantId { get; set; }
        public string Designation { get; set; }
        public string Participant_Name { get; set; }
        public string PartnerName { get; set; }
        public string Attendance { get; set; }
        public string Reason { get; set; }
    }
}
