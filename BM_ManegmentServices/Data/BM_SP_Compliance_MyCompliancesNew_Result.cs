//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_Compliance_MyCompliancesNew_Result
    {
        public int EntityId { get; set; }
        public Nullable<long> DirectorId { get; set; }
        public long ScheduleOnID { get; set; }
        public int RoleID { get; set; }
        public string MappingType { get; set; }
        public string CompanyName { get; set; }
        public long MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public long MeetingAgendaMappingID { get; set; }
        public string AgendaItem { get; set; }
        public long ComplianceId { get; set; }
        public int ActID { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string ShortForm { get; set; }
        public string Frequency { get; set; }
        public string Risk { get; set; }
        public Nullable<System.DateTime> ScheduleOn { get; set; }
        public string ScheduleOnTime { get; set; }
        public Nullable<System.DateTime> ClosedDate { get; set; }
        public string ClosedDateTime { get; set; }
        public string Status_ { get; set; }
        public string ForMonth { get; set; }
        public Nullable<long> ForPeriod { get; set; }
        public Nullable<bool> IsCheckList { get; set; }
        public Nullable<bool> HasForms { get; set; }
    }
}
