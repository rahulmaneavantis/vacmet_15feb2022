//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_BodyCorporateMaster
    {
        public int Id { get; set; }
        public Nullable<int> RefEntityId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string TypeOfBodyCorporate { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> Entity_Type { get; set; }
        public string CIN_LLPIN { get; set; }
        public string Email_Id { get; set; }
        public string PAN { get; set; }
        public Nullable<int> CountryId { get; set; }
        public string Registration_No { get; set; }
        public string Regi_Address_Line1 { get; set; }
        public string Regi_Address_Line2 { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
