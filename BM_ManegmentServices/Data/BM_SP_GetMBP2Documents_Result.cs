//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    
    public partial class BM_SP_GetMBP2Documents_Result
    {
        public long DocumentID { get; set; }
        public string Description { get; set; }
        public long FileId { get; set; }
        public string FileName { get; set; }
        public string EntitydocType { get; set; }
        public string DocType { get; set; }
    }
}
