//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_Share
    {
        public long Id { get; set; }
        public int CapitalMasterId { get; set; }
        public int CustomerId { get; set; }
        public string Sharetype { get; set; }
        public long Shares_Class { get; set; }
        public long No_AuhorisedCapital { get; set; }
        public long No_IssuedCapital { get; set; }
        public long No_SubscribedCapital { get; set; }
        public long No_Paid_upCapital { get; set; }
        public decimal Nominalper_AuhorisedCapital { get; set; }
        public decimal Nominalper_IssuedCapital { get; set; }
        public decimal Nominalper_SubscribedCapital { get; set; }
        public decimal Nominalper_Paid_upCapital { get; set; }
        public decimal totamt_AuhorisedCapital { get; set; }
        public decimal totamt_IssuedCapital { get; set; }
        public decimal totamt_SubscribedCapital { get; set; }
        public decimal totamt_upCapital { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Createdon { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<int> Pref_CapitalType { get; set; }
        public Nullable<decimal> Pref_CoupenRate { get; set; }
        public Nullable<long> RefIdRevised { get; set; }
    }
}
