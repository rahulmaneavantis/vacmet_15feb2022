//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_Entity_MBP2_Transaction
    {
        public long Id { get; set; }
        public int EntityId { get; set; }
        public Nullable<int> NatureOfTransactionId { get; set; }
        public Nullable<System.DateTime> DateOfMaking { get; set; }
        public string Name_ { get; set; }
        public string Address_ { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string TimePeriod { get; set; }
        public string Purpose { get; set; }
        public string PercentageOfReserve { get; set; }
        public Nullable<System.DateTime> DateOfBoardResolution { get; set; }
        public Nullable<System.DateTime> DateOfPassingResolution { get; set; }
        public Nullable<decimal> RateOfInterest { get; set; }
        public Nullable<System.DateTime> DateOfMaturity { get; set; }
        public string NoOfSecurity { get; set; }
        public string KindOfSecurity { get; set; }
        public Nullable<decimal> NominalValue { get; set; }
        public Nullable<decimal> PaidUp { get; set; }
        public Nullable<decimal> CostOfAuisition { get; set; }
        public Nullable<System.DateTime> DateOfSelling { get; set; }
        public Nullable<decimal> SellingPrice { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
