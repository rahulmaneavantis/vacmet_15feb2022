//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_Form_AOC4_1_SignedDetails
    {
        public long Id { get; set; }
        public long AOC4Id { get; set; }
        public string DINPAN_1 { get; set; }
        public string Name_1 { get; set; }
        public Nullable<System.DateTime> Date_1 { get; set; }
        public string DINPAN_2 { get; set; }
        public string Name_2 { get; set; }
        public Nullable<System.DateTime> Date_2 { get; set; }
        public string DINPAN_3 { get; set; }
        public string Name_3 { get; set; }
        public Nullable<System.DateTime> Date_3 { get; set; }
        public string DINPAN_4 { get; set; }
        public string Name_4 { get; set; }
        public Nullable<System.DateTime> Date_4 { get; set; }
        public string DINPAN_5 { get; set; }
        public string Name_5 { get; set; }
        public Nullable<System.DateTime> Date_5 { get; set; }
        public string DIN_1 { get; set; }
        public string DName_1 { get; set; }
        public Nullable<System.DateTime> DDate_1 { get; set; }
        public string DIN_2 { get; set; }
        public string DName_2 { get; set; }
        public Nullable<System.DateTime> DDate_2 { get; set; }
        public string DIN_3 { get; set; }
        public string DName_3 { get; set; }
        public Nullable<System.DateTime> DDate_3 { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
