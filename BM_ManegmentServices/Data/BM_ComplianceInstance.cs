//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_ComplianceInstance
    {
        public long ComplianceInstanceID { get; set; }
        public string MappingType { get; set; }
        public long ComplianceId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime ScheduledOn { get; set; }
        public int CustomerBranchID { get; set; }
        public int EntityID { get; set; }
        public int CustomerID { get; set; }
        public bool GenerateSchedule { get; set; }
        public bool IsActive { get; set; }
    }
}
