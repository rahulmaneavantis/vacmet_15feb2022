//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BM_ManegmentServices.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BM_Form_AOC4_4_ShareCapitalRaised
    {
        public long Id { get; set; }
        public long AOC4Id { get; set; }
        public Nullable<decimal> PublicIssue_E { get; set; }
        public Nullable<decimal> PublicIssue_P { get; set; }
        public Nullable<decimal> PublicIssue_T { get; set; }
        public Nullable<decimal> BonusIssue_E { get; set; }
        public Nullable<decimal> BonusIssue_P { get; set; }
        public Nullable<decimal> BonusIssue_T { get; set; }
        public Nullable<decimal> RightIssue_E { get; set; }
        public Nullable<decimal> RightIssue_P { get; set; }
        public Nullable<decimal> RightIssue_T { get; set; }
        public Nullable<decimal> PvtPlacementarising_E { get; set; }
        public Nullable<decimal> PvtPlacementarising_P { get; set; }
        public Nullable<decimal> PvtPlacementarising_T { get; set; }
        public Nullable<decimal> OtherPvtPlacement_E { get; set; }
        public Nullable<decimal> OtherPvtPlacement_P { get; set; }
        public Nullable<decimal> OtherPvtPlacement_T { get; set; }
        public Nullable<decimal> PrferentialAllotment_E { get; set; }
        public Nullable<decimal> PrferentialAllotment_P { get; set; }
        public Nullable<decimal> PrferentialAllotment_T { get; set; }
        public Nullable<decimal> OtherPreferentialallotment_E { get; set; }
        public Nullable<decimal> OtherPreferentialallotment_P { get; set; }
        public Nullable<decimal> OtherPreferentialallotment_T { get; set; }
        public Nullable<decimal> EmpStockOptionPlan_E { get; set; }
        public Nullable<decimal> EmpStockOptionPlan_P { get; set; }
        public Nullable<decimal> EmpStockOptionPlan_T { get; set; }
        public Nullable<decimal> Others_E { get; set; }
        public Nullable<decimal> Others_P { get; set; }
        public Nullable<decimal> Others_T { get; set; }
        public Nullable<decimal> TotalAmtShares_E { get; set; }
        public Nullable<decimal> TotalAmtShares_P { get; set; }
        public Nullable<decimal> TotalAmtShares_T { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
