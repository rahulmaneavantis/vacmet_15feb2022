﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMShareholding
    {
        public int? designationId { get; set; }
        public long DirectorId { get; set; }
        public int Count { get; set; }
        public long Id { get; set; }
        public string Follio_No { get; set; }
        public string DPIN_PAN { get; set; }
        public string DPIN { get; set; }
        public string PAN { get; set; }
        public decimal? Tot_SharesHeld { get; set; }
        public decimal? Contribution_Obligation { get; set; }
        public decimal? Contribution_Received { get; set; }        
        public decimal perofTotShareHolding { get; set; }
        public string Member_Name { get; set; }
        public string EmailId { get; set; }
        public string EntityName { get; set; }
        public decimal paidupCapital { get; set; }
        public ShareHoldings Shareholding { get; set; }
        //public List<ShareHoldingDetails> lstShareHoldingDetails { get; set; }
        public Response Message { get; set; }

        public decimal? equity_PaidUp { get; set; }
        public decimal? Prifrencepaidup { get; set; }
        public decimal? perofHolding { get; set; }

        public long PartnerId { get; set; }
        public long partnerShersID { get; set; }
        public int EntityID { get; set; }
        public string NatureofIntrest { get; set; }
        public string Designation { get; set; }
        public int? BCID { get; set; }
        public int? BodyCorpAsPartnerId { get; set; }
    }

    public class ShareHoldings
    {
        public long Id { get; set; }
        public int Entity_Id { get; set; }
        public decimal TotalPrefrenceCapital { get; set; }
        public int Customer_Id { get; set; }
        [Required(ErrorMessage = "Please Enter Folio Number")]
        public string Follio_No { get; set; }
        [Required(ErrorMessage = "Please Enter Class of shares.")]
        public string Shares_Class { get; set; }
        [Required, Range(1, int.MaxValue, ErrorMessage = "Please Select Type")]
        public int Type { get; set; }

        public long NominaVal_per_Shares { get; set; }
        [Required(ErrorMessage = "Please Enter Total shares held")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Total shares held can not be (-ve) value")]
        public long Tot_SharesHeld { get; set; }
        //[Required(ErrorMessage = "Please Enter Name of the member")]
        public string Member_Name { get; set; }
        public string JointHolder_Name { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string bodycorporate_Address { get; set; }
        public string CIN { get; set; }
        //[Required(ErrorMessage = "Please Enter Email Address")]
        [Display(Name = "Email_Id")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Please Enter Correct Email Address")]
        public string Email_Id { get; set; }
        public string Father_Mother_Spouse_Name { get; set; }

        public string Marital_Status { get; set; }

        public string Occupation { get; set; }
        [DisplayName("PAN Number")]
        //[Required(ErrorMessage = "Please Enter PAN No.")]
        [RegularExpression(@"[A-Za-z]{3}[A,a,B,b,C,c,F,f,G,g,H,h,L,l,J,j,P,p,T,t,e,E]{1}[A-Za-z]{1}\d{4}[A-Za-z]{1}", ErrorMessage = "Please Enter valid PAN Number")]
        public string PAN { get; set; }
        //[Required(ErrorMessage = "Please Select Nationality.")]
        public int Nationality { get; set; }
        //DataFormatString = @"{0:d}", 
        public string Guardian_Name { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DOB_minor { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? becomingmember_Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? declaration_Date_under89 { get; set; }
        public string Name_and_Address_ben { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Nomination_Date_Receipt { get; set; }
        public string NomineeName_Address { get; set; }
        public long? shares_abeyance { get; set; }
        public string lienonshares { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Cessation_Membership_Date { get; set; }
        public string Sendingnotices_Instruction { get; set; }
        public string PowerofAttorny { get; set; }
        public bool IsMemberMinnor { get; set; }
        public Response Message { get; set; }
        public List<ShareHoldingDetails> lstShareHoldingDetails { get; set; }

        public bool ISPramoter { get; set; }
        //public long? DirectorId { get; set; }
        public Response Response { get; set; }
    }

    public class ShareHoldingDetails : IMessage
    {
        public long parentID { get; set; }
        public int Id { get; set; }
        [MaxLength(100)]
        public string AllotmentNo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Dated { get; set; }
        public int? NumberOfShares { get; set; }
        public string DistinctiveFrom { get; set; }
        public string DistinctiveTo { get; set; }
        [MaxLength(10)]
        public string FolioOfTransferor { get; set; }
        [MaxLength(50)]
        public string NameOfTransferor { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateOfIssued { get; set; }
        [MaxLength(50)]
        public string CertificateNo { get; set; }
        [MaxLength(10)]
        public string LockInPeriod { get; set; }
        public decimal? PayableAmount { get; set; }
        public decimal? PaidOrToBePaidAmount { get; set; }
        public decimal? DueAmount { get; set; }
        [MaxLength(500)]
        public string Thereof { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateOfTransfer { get; set; }
        public int? NoOfShareTransfered { get; set; }
        [MaxLength(10)]
        public string DistinctiveFrom1 { get; set; }
        [MaxLength(10)]
        public string DistinctiveTo1 { get; set; }
        [MaxLength(10)]
        public string FolioOfTransferee { get; set; }
        [MaxLength(50)]
        public string NameOfTransferee { get; set; }
        public int? BalanceShare { get; set; }
        [MaxLength(100)]
        public string Remarks { get; set; }
        public string Authentication_ { get; set; }
    }

    public class ClassesOfShare
    {
        public int ClassesOfShareID { get; set; }

        public string ClassesOfShareName { get; set; }

        public decimal NominalValuePerShare { get; set; }
    }

}