﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_EntityuserAssignment
    {
        public long Id { get; set; }
        [Required (ErrorMessage = "Please Select User"), Range(1, int.MaxValue, ErrorMessage = "Please Select User")]
        public int user { get; set; }
        public string UserName { get; set; }
        [Required (ErrorMessage ="Please Select Entity")]
        public List<int> EntityId { get; set; }
       public int CompanyId { get; set; }
        public List<string> Entity { get; set; }
        public string CompanyName { get; set; }
        public List<string> MeetingTypeName { get; set; }
        public bool errorMessage { get; set; }
        public bool successMessage { get; set; }
        public string successerrorMessage { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
    }

    public class UserMeetingTypeAssignmentVM : IMessage
    {
        public int AssignmentId { get; set; }
        public int EntityId { get; set; }
        public List<int> ListMeetingTypeId { get; set; }

    }
   
}