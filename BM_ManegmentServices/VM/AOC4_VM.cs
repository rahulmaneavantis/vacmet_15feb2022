﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class AOC4_VM
    {
    }

    public class AOC4_List_VM : IMessage
    {
        public long AOC4Id { get; set; }
        public int EntityId { get; set; }
        public string Company { get; set; }
        public int FYId { get; set; }
        public string FY { get; set; }
        public int SrNo { get; set; }

        public bool Status_ { get; set; }
    }

    public class AOC4DetailsVM
    {
        public long Aoc4Id { get; set; }
        public int? EntityId { get; set; }
        public string Seg { get; set; }
        public string Details { get; set; }
        public bool? Status_ { get; set; }
        public string EditAction { get; set; }
        public string PrevAction { get; set; }
        public string wndTitle { get; set; }
    }

    #region Company Info
    public class AOC4_CompanyInfoVM : IMessage
    {
        public long CompanyInfoId { get; set; }
        public long CompanyInfoAOC4Id { get; set; }
        public string CompanyName { get; set; }
        public int EntityId { get; set; }
        [Required(ErrorMessage = "Please enter financial statements relates from date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FYStart { get; set; }
        [Required(ErrorMessage = "Please enter financial statements relates to date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FYEnd { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBM_FRApproved { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBM_FRSec134 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBM_FRSignAuditor { get; set; }
        public short? NoOfAuditors { get; set; }
        public bool? RecordsInEForm { get; set; }

        public AOC4_SignedDetailsVM objSignedDetails { get; set; }
        public bool? ShareCapital { get; set; }
        public bool? DetailedPL { get; set; }
        public bool? RPT { get; set; }
    }
    public class AOC4_SignedDetailsVM
    {
        public long SignedId { get; set; }
        public long SignedAOC4Id { get; set; }
        public string DINPAN_1 { get; set; }
        public string Name_1 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_1 { get; set; }

        public string DINPAN_2 { get; set; }
        public string Name_2 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_2 { get; set; }

        public string DINPAN_3 { get; set; }
        public string Name_3 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_3 { get; set; }

        public string DINPAN_4 { get; set; }
        public string Name_4 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_4 { get; set; }

        public string DINPAN_5 { get; set; }
        public string Name_5 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_5 { get; set; }

        public string DIN_1 { get; set; }
        public string DName_1 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DDate_1 { get; set; }

        public string DIN_2 { get; set; }
        public string DName_2 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DDate_2 { get; set; }

        public string DIN_3 { get; set; }
        public string DName_3 { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DDate_3 { get; set; }
    }
    #endregion

    #region Balance Sheet
    public class AOC4_BalanceSheetVM : IMessage
    {
        public long BalanceSheetId { get; set; }
        public long BalanceSheetAOC4Id { get; set; }
        public string ReportingPeriod { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Capital { get; set; }
        public decimal? Reserve { get; set; }
        public decimal? MoneyRecived { get; set; }
        public decimal? MoneyPending { get; set; }
        public decimal? LongTerm { get; set; }
        public decimal? Deferred { get; set; }
        public decimal? OtherLongTerm { get; set; }
        public decimal? LongTermProvision { get; set; }
        public decimal? ShortTerm { get; set; }
        public decimal? TradePayables { get; set; }
        public decimal? OtherCL { get; set; }
        public decimal? ShortTermProvision { get; set; }
        public decimal? Total_I { get; set; }
        public decimal? TangibleA { get; set; }
        public decimal? IntangibleA { get; set; }
        public decimal? CapitalWIP { get; set; }
        public decimal? IntangibleA_UD { get; set; }
        public decimal? NonCI { get; set; }
        public decimal? DeferredTA { get; set; }
        public decimal? Lt_LoanAndAdv { get; set; }
        public decimal? OtherNonCA { get; set; }
        public decimal? CurrentInvestment { get; set; }
        public decimal? Invetories { get; set; }
        public decimal? TradeReceivable { get; set; }
        public decimal? CashEquivalents { get; set; }
        public decimal? ShortTermLoan { get; set; }
        public decimal? OtherCurrentAsset { get; set; }
        public decimal? Total_II { get; set; }
    }

    public class AOC4_BalanceSheetPrevVM
    {
        public AOC4_BalanceSheetVM Current { get; set; }
        public AOC4_BalanceSheetVM Previous { get; set; }
    }
    #endregion

    #region Detailed Balance Sheet
    public class AOC4_DetailedBalanceSheetVM : IMessage
    {
        public long DetailedBalanceSheetId { get; set; }
        public long BalanceSheetAOC4Id { get; set; }
        public string LT_ReportingPeriod { get; set; }
        public decimal? LT_Bonds { get; set; }
        public decimal? LT_LoanBank { get; set; }
        public decimal? LT_LoanOtherParties { get; set; }
        public decimal? LT_Deferred_pay_liabilities { get; set; }
        public decimal? LT_Deposits { get; set; }
        public decimal? LT_Loans_adv_related_parties { get; set; }
        public decimal? LT_maturities { get; set; }
        public decimal? LT_Otherloans_advances { get; set; }
        public decimal? LT_Tot_borrowings { get; set; }
        public decimal? LT_Aggregate_Amt { get; set; }
        public decimal? ST_LoanBank { get; set; }
        public decimal? ST_LoanOtherParties { get; set; }
        public decimal? ST_Loans_advances_related_parties { get; set; }
        public decimal? ST_Deposits { get; set; }
        public decimal? ST_Loans_adv_related_parties { get; set; }
        public decimal? ST_Tot_borrowings { get; set; }
        public decimal? ST_Aggregate_Amt { get; set; }
        public decimal? LTA_Capital_adv { get; set; }
        public decimal? LTA_Security_Deposits { get; set; }
        public decimal? LTA_Loans_adv_related_parties { get; set; }
        public decimal? LTA_Otherloans_advances { get; set; }
        public decimal? LTA_TotLoan_Adv { get; set; }
        public decimal? LTA_FromRelated_Parties { get; set; }
        public decimal? LTA_FromOthers { get; set; }
        public decimal? LTA_Net_loan_Adv { get; set; }
        public decimal? LTA_Loans_Adv_dueByDir { get; set; }
        public decimal? LTAD_Capital_adv { get; set; }
        public decimal? LTAD_Security_Deposits { get; set; }
        public decimal? LTAD_Loans_adv_related_parties { get; set; }
        public decimal? LTAD_Otherloans_advances { get; set; }
        public decimal? LTAD_TotLoan_Adv { get; set; }
        public decimal? LTAD_FromRelated_Parties { get; set; }
        public decimal? LTAD_FromOthers { get; set; }
        public decimal? LTAD_Net_loan_Adv { get; set; }
        public decimal? LTAD_Loans_Adv_dueByDir { get; set; }

        public decimal? TR_Secured_ESM { get; set; }
        public decimal? TR_Secured_WSM { get; set; }
        public decimal? TR_Unsecured_ESM { get; set; }
        public decimal? TR_Unsecured_WSM { get; set; }
        public decimal? TR_Doubtful_ESM { get; set; }
        public decimal? TR_Doubtful_WSM { get; set; }
        public decimal? TR_TotalTrade_ESM { get; set; }
        public decimal? TR_TotalTrade_WSM { get; set; }
        public decimal? TR_Provision_ESM { get; set; }
        public decimal? TR_Provision_WSM { get; set; }
        public decimal? TR_NetTrade_ESM { get; set; }
        public decimal? TR_NetTrade_WSM { get; set; }
        public decimal? TR_DebtDue_ESM { get; set; }
        public decimal? TR_DebtDue_WSM { get; set; }

    }
    public class AOC4_DetailedBalanceSheetPrevVM
    {
        public AOC4_DetailedBalanceSheetVM Current { get; set; }
        public AOC4_DetailedBalanceSheetVM Previous { get; set; }
    }
    #endregion

    #region Finanacial Parameter
    public class AOC4_FinancialParameterVM : IMessage
    {
        public long FinancialParameterAOC4Id { get; set; }
        public decimal? Amt_Of_issue_Alloted { get; set; }
        public decimal? Share_app_Money_1 { get; set; }
        public decimal? Share_app_Money_2 { get; set; }
        public decimal? Share_app_Money_3 { get; set; }
        public decimal? Share_app_Money_4 { get; set; }
        public decimal? Paid_up_capital_1 { get; set; }
        public decimal? Paid_up_capital_2 { get; set; }
        public decimal? NoOfShares { get; set; }
        public decimal? Deposit_Accepted { get; set; }
        public decimal? Deposit_Matured_1 { get; set; }
        public decimal? Deposit_Matured_2 { get; set; }
        public decimal? Deposit_Matured_3 { get; set; }
        public decimal? Unclaimed_Debentures { get; set; }
        public decimal? Claimed_Debentures { get; set; }
        public decimal? InterestOnDepositAccrued { get; set; }
        public decimal? Unpaid_divedend { get; set; }
        public decimal? InvestmentInCompany_1 { get; set; }
        public decimal? InvestmentInCompany_2 { get; set; }
        public decimal? Capital_Reserves { get; set; }
        public decimal? Amt_TransferforIEPF { get; set; }
        public decimal? Inter_Corp_Deposit { get; set; }
        public decimal? GrossValueOfTransaction { get; set; }
        public decimal? CapitalSubsidies { get; set; }
        public decimal? CallsUnpaidByDirector { get; set; }
        public decimal? CallsUnpaidByOthers { get; set; }
        public decimal? Forfeited_Shares_1 { get; set; }
        public decimal? Forfeited_Shares_2 { get; set; }
        public decimal? Borrowing_foreign_1 { get; set; }
        public decimal? Borrowing_foreign_2 { get; set; }
        public decimal? InterCorporate_1 { get; set; }
        public decimal? InterCorporate_2 { get; set; }
        public decimal? CommercialPaper { get; set; }
        public decimal? ConversionOfWarrant_1 { get; set; }
        public decimal? ConversionOfWarrant_2 { get; set; }
        public decimal? ConversionOfWarrant_3 { get; set; }
        public decimal? WarrantsIssueInForeignCurrency { get; set; }
        public decimal? WarrantsIssueInRupees { get; set; }
        public decimal? DefaultInPayment_1 { get; set; }
        public decimal? DefaultInPayment_2 { get; set; }
        public bool? WheatheOperatingLease { get; set; }
        public string ProvideDetailsOfConversion { get; set; }
        public decimal? NetWorthOfComp { get; set; }
        public decimal? NoOfShareHolders { get; set; }
        public decimal? SecuredLoan { get; set; }
        public decimal? GrossFixedAssets { get; set; }
        public decimal? Depreciation_Amortization { get; set; }
        public decimal? Misc_Expenditure { get; set; }
        public decimal? UnhedgedForeign { get; set; }
        public int UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }

    }
    #endregion

    #region Share Capital
    public class ShareCapitalRaisedVM : IMessage
    {
        public long ShareCapitalRaiseId { get; set; }
        public long ShareCapitalAOC4Id { get; set; }
        public decimal? PublicIssue_E { get; set; }
        public decimal? PublicIssue_P { get; set; }
        public decimal? PublicIssue_T { get; set; }
        public decimal? BonusIssue_E { get; set; }
        public decimal? BonusIssue_P { get; set; }
        public decimal? BonusIssue_T { get; set; }
        public decimal? RightIssue_E { get; set; }
        public decimal? RightIssue_P { get; set; }
        public decimal? RightIssue_T { get; set; }
        public decimal? PvtPlacementarising_E { get; set; }
        public decimal? PvtPlacementarising_P { get; set; }
        public decimal? PvtPlacementarising_T { get; set; }
        public decimal? OtherPvtPlacement_E { get; set; }
        public decimal? OtherPvtPlacement_P { get; set; }
        public decimal? OtherPvtPlacement_T { get; set; }
        public decimal? PrferentialAllotment_E { get; set; }
        public decimal? PrferentialAllotment_P { get; set; }
        public decimal? PrferentialAllotment_T { get; set; }
        public decimal? OtherPreferentialallotment_E { get; set; }
        public decimal? OtherPreferentialallotment_P { get; set; }
        public decimal? OtherPreferentialallotment_T { get; set; }
        public decimal? EmpStockOptionPlan_E { get; set; }
        public decimal? EmpStockOptionPlan_P { get; set; }
        public decimal? EmpStockOptionPlan_T { get; set; }
        public decimal? Others_E { get; set; }
        public decimal? Others_P { get; set; }
        public decimal? Others_T { get; set; }
        public decimal? TotalAmtShares_E { get; set; }
        public decimal? TotalAmtShares_P { get; set; }
        public decimal? TotalAmtShares_T { get; set; }
    }
    #endregion

    #region Cost Records
    public class DetailsOfCostRecordsVM : IMessage
    {
        public long DetailsOfCostRecordsId { get; set; }
        public long DetailsOfCostRecordsAOC4Id { get; set; }
        public bool MaintenanceOfCR { get; set; }
        public string CentralExcise { get; set; }
        public bool AuditOfCR { get; set; }
        public string CentralExcise1 { get; set; }
    }
    #endregion

    #region Profit & Loss
    public class AOC4_ProfitLossVM : IMessage
    {

        public long ProfitLossId { get; set; }
        public long Aoc4Id { get; set; }
        public string ReportingPeriod { get; set; }
        public DateTime? FROM_DATE_CR { get; set; }
        public DateTime? TO_DATE_CR { get; set; }
        public decimal? SALES_GOODS_CR { get; set; }
        public decimal? SALES_GOODS_T_CR { get; set; }
        public decimal? SALES_SUPPLY_CR { get; set; }
        public decimal? SALES_GOODS1_CR { get; set; }
        public decimal? SALE_GOODS_T1_CR { get; set; }
        public decimal? SALES_SUPPLY1_CR { get; set; }
        public decimal? OTHER_INCOME_CR { get; set; }
        public decimal? TOTAL_REVENUE_CR { get; set; }
        public decimal? COST_MATERIAL_CR { get; set; }
        public decimal? PURCHASE_STOCK_C { get; set; }
        public decimal? FINISHED_GOODS_C { get; set; }
        public decimal? WORK_IN_PROG_CR { get; set; }
        public decimal? STOCK_IN_TRADE_C { get; set; }
        public decimal? EMP_BENEFIT_EX_C { get; set; }
        public decimal? MANGERIAL_REM_CR { get; set; }
        public decimal? PAYMENT_AUDTRS_C { get; set; }
        public decimal? INSURANCE_EXP_CR { get; set; }
        public decimal? POWER_FUEL_CR { get; set; }
        public decimal? FINANCE_COST_CR { get; set; }
        public decimal? DEPRECTN_AMORT_C { get; set; }
        public decimal? OTHER_EXPENSES_C { get; set; }
        public decimal? TOTAL_EXPENSES_C { get; set; }
        public decimal? PROFIT_BEFORE_CR { get; set; }
        public decimal? EXCEPTIONL_ITM_C { get; set; }
        public decimal? PROFIT_BEF_TAX_C { get; set; }
        public decimal? EXTRAORDINARY_CR { get; set; }
        public decimal? PROF_B_TAX_7_8_C { get; set; }
        public decimal? CURRENT_TAX_CR { get; set; }
        public decimal? DEFERRED_TAX_CR { get; set; }
        public decimal? PROF_LOSS_OPER_C { get; set; }
        public decimal? PROF_LOSS_DO_CR { get; set; }
        public decimal? TAX_EXPNS_DIS_CR { get; set; }
        public decimal? PROF_LOS_12_13_C { get; set; }
        public decimal? PROF_LOS_11_14_C { get; set; }
        public decimal? BASIC_BEFR_EI_CR { get; set; }
        public decimal? DILUTED_BEF_EI_C { get; set; }
        public decimal? BASIC_AFTR_EI_CR { get; set; }
        public decimal? DILUTED_AFT_EI_C { get; set; }
        public bool IsActive { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }

    public class AOC4_ProfitLossPrevVM
    {
        public AOC4_ProfitLossVM Current { get; set; }
        public AOC4_ProfitLossVM Previous { get; set; }
    }
    #endregion

    #region Detailed Profit and Loss items

    public class DetailedProfitLossPrevVM
    {
        public DetailedProfitLossVM Current { get; set; }
        public DetailedProfitLossVM Previous { get; set; }
    }
    public class DetailedProfitLossVM : IMessage
    {
        public long DetailedOfPLId { get; set; }
        public long DetailedOfPLAOC4Id { get; set; }
        public string ReportingPeriod { get; set; }
        public decimal? EXP_GOODS_FOB_CR { get; set; }
        public decimal? INTEREST_DIVD_CR { get; set; }
        public decimal? ROYALTY_CR { get; set; }
        public decimal? KNOW_HOW_CR { get; set; }
        public decimal? PROF_CONS_FEE_CR { get; set; }
        public decimal? OTHR_INCOME_E_CR { get; set; }
        public decimal? TOTAL_EARNG_FE_C { get; set; }
        public decimal? RAW_MATERIAL_CR { get; set; }
        public decimal? COMPONENT_SP_CR { get; set; }
        public decimal? CAPITAL_GOODS_CR { get; set; }
        public decimal? ROYALTY_EXP_CR { get; set; }
        public decimal? KNOW_HOW_EXP_CR { get; set; }
        public decimal? PROF_CON_FEE_E_C { get; set; }
        public decimal? INTEREST_EXP_CR { get; set; }
        public decimal? OTHER_MATTERS_CR { get; set; }
        public decimal? DIVIDEND_PAID_CR { get; set; }
        public decimal? TOT_EXP_FE_CR { get; set; }
        public bool IsActive { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
    #endregion

    #region Financial parameters - Profit and loss account items
    public class FinancialParameter_PLVM : IMessage
    {
        public long FinancialParameterPLId { get; set; }
        public HttpPostedFileBase File { get; set; }
        public long FinancialParameterPLAOC4Id { get; set; }
        public decimal? PROPOSED_DIVIDND { get; set; }
        public decimal? PROPOSED_DIV_PER { get; set; }
        public decimal? BASIC_EARNING_PS { get; set; }
        public decimal? DILUTED_EARN_PS { get; set; }
        public decimal? INCOME_FOREIGN { get; set; }
        public decimal? EXPENDIT_FOREIGN { get; set; }
        public decimal? REVENUE_SUBSIDIE { get; set; }
        public decimal? RENT_PAID { get; set; }
        public decimal? CONSUMPTION_STOR { get; set; }
        public decimal? GROSS_VALUE_TRAN { get; set; }
        public decimal? BAD_DEBTS_RP { get; set; }
        public bool IsActive { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
    #endregion

    public class ExcelFileAOC4UploadVM : IMessage
    {
        public long ExcelAOC4Id { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string FormName { get; set; }
    }

    #region Misellaneous_Authorization
    public class MiscellaneousAuthorizationVM : IMessage
    {        
        public long MiscId { get; set; }
        public long Aoc4Id { get; set; }
        public bool RB_SECRETARIAL_A { get; set; }
        public bool RB_AUDTR_REPORT { get; set; }
        public string CVRN { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DECLARATION_DATE { get; set; }
        public string DESIGNATION { get; set; }

        public string DIN_PAN_MEM_NUM { get; set; }
        public string RB_CA_COA_CS { get; set; }
        public bool RB_ASSOC_FELLOW { get; set; }
        public string MEMBERSHIP_NUM { get; set; }
        public string CERT_PRACTICE_NO { get; set; }
        public bool IsActive { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
    #endregion

    #region Related Party Transactions
    public class RPT_VM : IMessage
    {
        public long RPTAOC4ID { get; set; }
        public List<RPT_Contracts_VM> lstContracts { get; set; }
        public List<RPT_MaterialContracts_VM> lstMaterialContracts { get; set; }
    }
    public class RPT_Contracts_VM
    {
        public long Id { get; set; }
        public long AOC4Id { get; set; }
        public string NAME_RELATED_PAR { get; set; }
        public string NATURE_OF_RELATN { get; set; }
        public string NATURE_OF_CONTRA { get; set; }
        public string DURATION_OF_CONT { get; set; }
        public System.DateTime? DATE_OF_APPROVAL { get; set; }
        public decimal? AMOUNT_PAID { get; set; }
        public System.DateTime? DATE_SPCL_RESOLT { get; set; }
        public bool IsActive { get; set; }
        public int? CreatedBy { get; set; }
        public System.DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public System.DateTime? UpdatedOn { get; set; }
    }

    public class RPT_MaterialContracts_VM
    {
        public long Id { get; set; }
        public long AOC4Id { get; set; }
        public string NAME_RELATED_PAR { get; set; }
        public string NATURE_OF_RELATN { get; set; }
        public string NATURE_OF_CONTRA { get; set; }
        public string DURATION_OF_CONT { get; set; }
        public System.DateTime? DATE_OF_APPROVAL { get; set; }
        public decimal? AMOUNT_PAID { get; set; }
        public bool IsActive { get; set; }
        public int? CreatedBy { get; set; }
        public System.DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public System.DateTime? UpdatedOn { get; set; }
    }
    #endregion

    #region CSR
    public class CSR_VM : IMessage
    {
        public long CSRId { get; set; }
        public long CSRAOC4Id { get; set; }
        public bool RB_CSR_APPLICABL { get; set; }
        public Decimal? TURNOVER { get; set; }
        public Decimal? NET_WORTH { get; set; }
        public Decimal? PRESCRIBED_CSR_E { get; set; }
        public Decimal? TOTAL_AMT_SPENT { get; set; }
        public Decimal? AMOUNT_SPENT_LA { get; set; }
        public int? NUM_CSR_ACTIVITY { get; set; }
        public HttpPostedFileBase File { get; set; }
        public List<CSRDetails_VM> lstDetails { get; set; }
        public string DETAILS_IMPLEMEN {get; set;}
    }

    public class CSRDetails_VM : IMessage
    {
        public long CSRDetailsId { get; set; }
        public long CSRDetailsAOC4Id { get; set; }
        public string CSR_PROJECT_ACTV { get; set; }
        public int? SECTOR_PROJ_COVR { get; set; }
        public int? STATE_UT { get; set; }
        public decimal? AMOUNT_OUTLAY { get; set; }
        public decimal? AMOUNT_SPENT_PRJ { get; set; }
        public int? MODE_AMOUNT_SPNT { get; set; }


        public string SECTOR_PROJ_COVR_Str { get; set; }
        public string Sector_Code { get; set; }
        public string STATE_UT_Str { get; set; }
        public string State_UT_Code { get; set; }
        public string MODE_AMOUNT_SPNT_Str { get; set; }
        public string Mode_Amount_Code { get; set; }
        
       
    }

    public class CSR_ModeOfAmtVM : IMessage
    {
        public int CSRModeOfAmtId { get; set; }
        public int CSRModeOfAmtAOC4Id { get; set; }
        public string ModeCode { get; set; }
        public string ModeName { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
    #endregion

    #region  Auditor's Report
    public class AuditorsReport_VM : IMessage
    {
        public long AuditorReportId { get; set; }
        public long AuditorReportAOC4Id { get; set; }
        public bool RB_COMPTRL_AUDTR { get; set; }
        public bool RB_AUDTR_REPORT { get; set; }
        public bool RB_COMP_AUDT_REP { get; set; }
        public string FIXED_ASSETS { get; set; }
        public string INVENTORIES { get; set; }
        public string LOANS_GIVEN_COMP { get; set; }
        public string ACCEPTANCE_PUB_D { get; set; }
        public string MAINTENANCE_CR { get; set; }
        public string STATUTORY_DUES { get; set; }
        public string TERM_LOANS { get; set; }
        public string FRAUD_NOTICED { get; set; }
        public string OTHER_COMMENTS { get; set; }
        public long RB_SECRETARIAL_A { get; set; }
        public long RB_DETAILED_DISC { get; set; }
        public bool IsActive { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public HttpPostedFileBase File { get; set; }
    }
    #endregion

    public class AOC4FormUploadVM : IMessage
    {
        public HttpPostedFileBase File { get; set; }
        public string FormName { get; set; }
    }

    public class AOC4_CompanyHoldingVM
    {
        public string HoldingCompanyId { get; set; }
        public string CIN_HOLDING_COMP { get; set; }
    }

}