﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class ChargeTypeList_VM
    {
        public int Id { get; set; }
        public string ChargeTypeDesc { get; set; }
    }
}