﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BM_ManegmentServices.VM
{
    public class NotesMasterVM :IMessage
    {
        public long NotesMasterID { get; set; }
        public bool eVoting { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Please enter notes")]
        public string NotesFormat { get; set; }
    }
}