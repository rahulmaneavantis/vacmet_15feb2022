﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.Dashboard
{
    public class MyCompliance_Director
    {

    }

    public class MyCompliance_DirectorBarChartVM
    {
        public string CompanyName { get; set; }
        public string ComplianceStatus { get; set; }
    }

    public partial class MyCompliance_Director_ResultVM
    {
        public int EntityId { get; set; }
        public string CompanyName { get; set; }
        public long? DirectorId { get; set; }
        public string DirectorName { get; set; }
        public long ScheduleOnID { get; set; }
        public int RoleID { get; set; }
        public string MappingType { get; set; }
        public long MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public string ShortForm { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string RequiredForms { get; set; }
        public int? ScheduleOnInt { get; set; }
        public DateTime? ScheduleOn { get; set; }
        public string ScheduleOnTime { get; set; }
        public string ComplianceStatus { get; set; }
        public string Frequency { get; set; }
        public string ForMonth { get; set; }
        public long? ForPeriod { get; set; }
        public string Risk { get; set; }
        public string PerformerName { get; set; }
        //public string ReviewerName { get; set; }
    }

}