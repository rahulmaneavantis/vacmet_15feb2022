﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_CircularResolution
    {
        public long CircularID { get; set; }
        public long companyID { get; set; }
        public string companyName { get; set; }

        [Required(ErrorMessage = "Select Type")]
        public int meetingTypeID { get; set; }

        public string meetingTypeName { get; set; }
                
        [Required(ErrorMessage = "Required")]
        public Nullable<int> CircularNo { get; set; }
                
        [Required(ErrorMessage = "Required")]
        public Nullable<DateTime> CircularDate { get; set; }

        [Required(ErrorMessage = "Required")]
        public Nullable<DateTime> DueDate { get; set; }

        public string Type { get; set; }
        public int customerID { get; set; }
        public bool errorMessage { get; set; }
        public bool saveSuccess { get; set; }
        public string Message { get; set; }
        public List<long> lstAgendas { get; set; }
        public int loggedinUserID { get; set; }
    }

    public class CircularAgendaMapping
    {        
        public int agendaID { get; set; }
    }

    
}