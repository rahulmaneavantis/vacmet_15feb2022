﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace BM_ManegmentServices.VM
{
    public class DirectorRegisterVM
    {
        public Entity EntityDetails { get; set; }
        public Directors DirectorDetails { get; set; }
        public DirectorSecurity DirectorSeccurityDetails { get; set; }
        public RegistersLists RegisterList { get; set; }
        public RegisterListDetails RegisterListDetails { get; set; }
        public MemberRegister MemberRegisterDetails { get; set; }
    }
    public class Entity
    {
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public string Address { get; set; }
    }
    public class Directors
    {
        public long DirectorId { get; set; }
        public string DIN { get; set; }
        public string FullName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string SpouseName { get; set; }
        public DateTime DateofBirth { get; set; }
        public string PresentAddress { get; set; }
        public string ParmanentAddress { get; set; }
        public string Nationalality { get; set; }
        public string Occupation { get; set; }
        public DateTime? DateofBoardResolution { get; set; }
        public DateTime? DateofAppointmentandReappointment { get; set; }
        public string DateofCessation { get; set; }
        public string ICSNo { get; set; }
        public string PAN { get; set; }
        public string EmailId { get; set; }
        public bool IsDirector { get; set; }
        public bool IsMNGT { get; set; }
    }
    public class DirectorSecurity
    {
        public string CIN { get; set; }
        public string CompanyName { get; set; }
       public long? NoofSecurities { get; set; }
        public string DescriptionofSecurity { get; set; }
        public long? NominalvalueofSecurities { get; set; }
        public string DateofAccusation { get; set; }
        public decimal? PricePaidforaccusation { get; set; }
        public decimal? otherPricePaidforaccusation { get; set; }
        public string DateofDesposal { get; set; }
        public decimal? Pricerecivefordisposal { get; set; }
        public decimal? OtherPriceforDisposal { get; set; }

        public long? CumaltiveBalance { get; set; }
        public string ModeofAccusation { get; set; }
        public string ModeOfHolding { get; set; }
        public string SecurityHasbeenpledge { get; set; }
    }

    public class RegistersLists
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class RegisterListDetails
    {
        public int registerID { get; set; }
        public long EntityId { get; set; }
    }

    public class MemberRegister
    {
        public int MemberId { get; set; }
        public string EntityName { get; set; }
        public string EntityAddress { get; set; }
        public string FolioNumber { get; set; }
        public string ClassofShares { get; set; }
        public decimal Nominalvaluepershares { get; set; }
        public decimal? TotalSharesHeld { get; set; }
        public string MemberName { get; set; }
        public string JointHolderName { get; set; }
        public string Address { get; set; }
        public string EmailId { get; set; }
        public string CIN { get; set; }
        public string UIN { get; set; }
        public string Father_Mother_SpouseName { get; set; }
        public string Status { get; set; }
        public string PAN { get; set; }
        public string GuardianName { get; set; }
        public string DateofBirth { get; set; }
        public string DateofBecomingMember { get; set; }
        public string Dateunder89 { get; set; }
        public string BenificialNameandAddress { get; set; }
        public string DateofNomination { get; set; }
        public string NameandAddressofNomminee { get; set; }
        public long? sharesinAbayes { get; set; }
        public string DateofCessation { get; set; }
        public string Occupation { get; set; }
        public string Nationality { get; set; }

        public MemberSecurities MemberSecuritiesDetails { get; set; }
    }
    public class MemberSecurities
    {
        public string AllotmentNo { get; set; }
        public string DateofAllotment { get; set; }
        public int? NumberofShares { get; set; }
        public string DistinctiveNoofShares_From { get; set; }
        public string DistinctiveNoofShares_To { get; set; }
        public string TransferorFolioNo { get; set; }
        public string NameOfTransferror { get; set; }
        public string DateofIssues { get; set; }
        public string CertificateNumber { get; set; }
        public string LockinPeriod { get; set; }
        public decimal? AmountPayable { get; set; }
        public decimal? AmountPaid { get; set; }
        public decimal? AmountDue { get; set; }
        public string DescofConsidration { get; set; }
        public string DateofTransfer { get; set; }
        public int? NoofSharesTransfered { get; set; }
      
        public string DistinctiveNo_Form { get; set; }
        public string DistinctiveNo_To { get; set; }
        public string TransfreeFolio { get; set; }
        public string NameofTransfaree { get; set; }
        public int? BalanceofSharesHeld { get; set; }
        public string Remark { get; set; }
        public string Authentication { get; set; }
    }

    public class ChargeRegister
    {

        public int Entity_Id { get; set; }
        public string EntityName { get; set; }
        public int Charge_Id { get; set; }
        public string ChargeIdMannual { get; set; }
        public int ChargeTypeId { get; set; }
        public DateTime Charge_CreationDate { get; set; }
        public string ChargeType { get; set; }
        public string Charge_TypeDesc { get; set; }
        public string ChargeAmount { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreationDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RegistrationChargeCreateDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ModificationDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RegistrationModificationDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SatisfactionDate { get; set; }
        public string InFavour { get; set; }
        public int? CreatedBy { get; set; }
        public string ShortDescPropertyCharged { get; set; }
        public string Namesaddresseschargeholder { get; set; }
        public string TermsconditionsOfcharge { get; set; }
        public string Descinstrument { get; set; }
        public DateTime? RegistrationChargeModificationDate { get; set; }
        public string Descinstrumentchargemodify { get; set; }
        public string Particularsmodification { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RegistrationsatisfactionDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FactsDelaycondonationDate { get; set; }
        public string Reasonsdelayfiling { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool Status { get; set; }
        public bool IsActive { get; set; }

        public string ChargeTerm { get; set; }
        public string AddressOfChargeHolder { get; set; }
    }
}