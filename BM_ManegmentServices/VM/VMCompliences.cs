﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMCompliences
    {
        public long ID { get; set; }
        public int ActID { get; set; }
        public string ActName { get; set; }
        public byte ComplianceType { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Sections { get; set; }
        public bool? UploadDocument { get; set; }
        public byte? NatureOfCompliance { get; set; }
        public string RiskType { get; set; }
        public string Frequency { get; set; }
        public string Status { get; set; }
        public bool IsCheked { get; set; }
        public long? AgendaId { get; set; }
        public bool OnComplianceCompletion { get; set; }
        public bool New { get; set; }

        public int? MeetingTypeId { get; set; }
        public int? EntityTypeId { get; set; }
        public long? FormId { get; set; }

        public bool HasForms { get; set; }

    }
    public class FormDetails
    {
        public long? MappingID { get; set; }
        public long ComplianceId { get; set; }
        public long FormId { get; set; }
        public string FormName { get; set; }
        public string Compliance { get; set; }
        public bool New { get; set; }
        public bool IsCheked { get; set; }
    }
}