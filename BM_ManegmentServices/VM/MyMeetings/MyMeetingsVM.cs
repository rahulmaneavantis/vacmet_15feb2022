﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.MyMeetings
{
    public class MyMeetingsVM
    {

    }

    public class ConcludedMeetingAgendaItemVM : MeetingAgendaMappingVM
    {
        public long EntityId { get; set; }
        public long MeetingID { get; set; }
        public long? HID { get; set; }
        public long? HMappingID { get; set; }
        public long? RowNo { get; set; }
        public long FY { get; set; }
        public string CompanyName { get; set; }
        public string MeetingTypeName { get; set; }
        public string FYText { get; set; }
        public string MeetingTitle { get; set; }
        public DateTime? MeetingDate { get; set; }

        public string ResultRemark { get; set; }
    }
}