﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMRegisters
    {
        public int Id { get; set; }
        public List<int> EntityType { get; set; }
        public List<VMEntityType> EntityTypeName { get; set; }
        public string Register_Name { get; set; }
        public int CustomerId { get; set; }
        public string successErrorMessage { get; set; }
        public bool Errormessage { get; set; }
        public bool Successmessage { get; set; }
    }
}