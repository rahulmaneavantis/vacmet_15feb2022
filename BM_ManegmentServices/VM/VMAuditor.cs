﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMAuditor
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int CustomerId { get; set; }
        public int Entity_Id { get; set; }
        [Required(ErrorMessage = "Please select Auditor Type")]
        public int? Auditor_Type { get; set; }
        public string AuditorTypeName { get; set; }
        public string Entity { get; set; }
        [Required(ErrorMessage = "Please Enter Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid Email")]
        public string Email_Id { get; set; }
        [Required,Range(1, int.MaxValue, ErrorMessage = "Please select Audit Category")]
        public int Audit_CategoryId { get; set; }
        public int Auditor_Category { get; set; }
        [DisplayName("PAN Number")]
        //[Required(ErrorMessage = "Please Enter PAN")]
        [RegularExpression(@"[a-zA-Z]{3}[a,b,c,f,g,h,l,j,p,t,e,A,B,C,F,G,H,L,J,P,T,E]{1}[a-zA-Z]{1}\d{4}[a-zA-Z]{1}", ErrorMessage = "Please Enter valid PAN Number")]
        public string Pan_Number { get; set; }

        public string FirstName_Auditor { get; set; }

        public string LastName_Auditor { get; set; } 

        public string MiddleName_Auditor { get; set; }
        [Required(ErrorMessage = "Please Enter Full Auditor/Firm Name")]
        [RegularExpression(@"^[a-zA-Z\s-_,.&()]*$", ErrorMessage = "Please Enter valid Auditor/Firm Name")]
        public string Audit_FirmName_FullName { get; set; }
        [Required(ErrorMessage = "Please Enter Membership Number")]
      
        public string AuditorFirmRegistration_Number { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line1")]
        // [RegularExpression(@"^[a-zA-Z0-9\s-_,.&()]*$", ErrorMessage = "Please Enter valid Address Line 1")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid Address Line 1")]
        public string AddressLine1 { get; set; }
        // [Required(ErrorMessage = "Please Enter Address Line2")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid Address Line 2")]
        //[RegularExpression(@"^[a-zA-Z0-9\s-_,.&()]*$", ErrorMessage = "Please Enter valid Address Line 2")]
        public string AddressLine2 { get; set; }
        [Required(ErrorMessage = "Please select Country")]
        public int country { get; set; }
        //[Required(ErrorMessage = "Please select state")]
        public int state { get; set; }
        //[Required(ErrorMessage = "Please select city")]
        public int city { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter valid PIN")]
        public int? PinNumber { get; set; }
        public bool errorMessage { get; set; }
        public bool successMessage { get; set; }
        public string showMessage { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Please enter valid Mobile")]
        public long? MobileNo { get; set; }
        public int forStatutory { get; set; }
        public bool isStatutoryAuditor { get; set; }
        public int logedinuserId { get; set; }
        public string ViewName { get; set; }

        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
        public bool IsMapped { get; set; }
        public int? AuditorId { get; set; }
        public bool Isexisting { get; set; }
    }

    public class statutoryAuditor
    {
        public bool IsAuditor { get; set; }
        public int? AuditorId { get; set; }
        public string EmailID { get; set; }
        public string Designation { get; set; }
        public int MeetingId { get; set; }
        public string AuditorName { get; set; }
    }
}