﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class BankAccDetails_VM : IMessage
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        [Required(ErrorMessage = "Please Enter Name Of Your Bank ")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_]*$", ErrorMessage = "Please Enter Name Of Your Bank ")]
        public string BankName { get; set; }
        [Required(ErrorMessage = "Please Enter Account Number ")]
        [RegularExpression(@"^\d{9,18}$", ErrorMessage = "Please Enter valid Account Number ")]
        public string AccNo { get; set; }
        [Required(ErrorMessage = "Select Type Of Account")]
        public int AccTypeId { get; set; }
        //[Required(ErrorMessage = "Please Enter IFSC Code ")]
        [RegularExpression(@"^[A-Z]{4}0[A-Z0-9]{6}$", ErrorMessage = "Please Enter 11 digit IFSC Code ")]
        public string Ifsc { get; set; }
        //[Required(ErrorMessage = "Please Enter Address ")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,()&@{}/.#]*$", ErrorMessage = "Please Enter valid Address ")]
        public string Address { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Enter Opening Date")]
        public DateTime OpenDate { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Required(ErrorMessage = "Enter Closing Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CloseDate { get; set; }
        public bool Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool IsActive { get; set; }
        public string AccType { get; set; }
    }
    public class BankTypeList_VM
    {
        public int Id { get; set; }
        public string Acctype { get; set; }
    }
}