﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMllpCapital
    {
        public long Id { get; set; }
        public string EntityName { get; set; }
        public string DPIN_No { get; set; }
        public int Entity_Id { get; set; }
        public string Partner_Name { get; set; }
        public decimal Capital { get; set; }
        public decimal? Capital_Contribution { get; set; }
        public decimal Capital_ContributionforPartener { get; set; }
        [RegularExpression(@"[A-Z]{3}[A,B,C,F,G,H,L,J,P,T,E]{1}[A-Z]{1}\d{4}[A-Z]{1}", ErrorMessage = "Please Enter valid PAN Number")]
        public string PANNo { get; set; }
      
        [RegularExpression("^[0-9]{12}$", ErrorMessage = "Please Enter valid Aadhar Number")]
        public string Address { get; set; }
        public int DesignatedPartner_Id { get; set; }
        public bool IsDesiorPart { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string SuccessErrorMsg { get; set; }


    }
}