﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.Compliance
{
    public class MyCompliancesVM
    {
        public int RoleId { get; set; }
        public string Role { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string StatusFilter { get; set; }
    }
    public partial class MyComplianceListVM
    {
        public long ComplianceID { get; set; }
        public int EntityId { get; set; }
        public string CompanyName { get; set; }
        public long? DirectorId { get; set; }
        public string DirectorName { get; set; }
        public long ScheduleOnID { get; set; }
        public int RoleID { get; set; }
        public string MappingType { get; set; }        
        public long? MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public string ShortForm { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public DateTime? ScheduleOn { get; set; }
        public string ScheduleOnTime { get; set; }
        public string StatusName { get; set; }
        public string RequiredForms { get; set; }
        public string Frequency { get; set; }
        public string Period { get; set; }

        public bool? IsCheckList { get; set; }
        public bool? IsSelect { get; set; }
        public string ForMonth { get; set; }
    }
}