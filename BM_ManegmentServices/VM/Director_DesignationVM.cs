﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BM_ManegmentServices.VM
{
    public class Director_DesignationVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DirectorTypeOfMembershipVM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsChairman { get; set; }
    }
}