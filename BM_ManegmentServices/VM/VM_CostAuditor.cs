﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_CostAuditor
    {
        public long Id { get; set; }
        public long AuditorMappingId { get; set; }
        public int EntityId { get; set; }
        public int CustomerId { get; set; }
        public string CategoryName { get; set; }
        public string Auditor_Name { get; set; }
        public string AddressOfAuditor { get; set; }
        public string AuditorAppointedForFY { get; set; }

        [Required, Range(1, int.MaxValue, ErrorMessage = "Please select Nature of Intimation cost")]
        public int? Nature_of_intimation_cost { get; set; }
        public int categoryId { get; set; }
        public int AuditorId { get; set; }
        [Required (ErrorMessage = "Please Enter Cost Auditor firm Original")]
        public string Cost_auditor_firm_Original { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //Commented on 5 May 2020 by Amit
        //[Required(ErrorMessage = "Please select BordMeeting Date")]
        public DateTime? dateofBordMeeting { get; set; }
        //Commented on 5 May 2020 by Amit
        //[Required(ErrorMessage = "Please Enter Resulation Number")]
        public string ResulationNumber { get; set; }
        public string SuccesserrorMessage { get; set; }
        public bool errorMessage { get; set; }
        public bool successMessage { get; set; }

        public bool? IsMappingActive { get; set; }
        //Used in Template field
        public string RegistrationNo { get; set; }
        public string DateOfAppointment { get; set; }
        public string Appointed_FromDate { get; set; }
        public string Appointed_DueDate { get; set; }
        public long AgendaID { get; set; }
        public long MeetingAgengaMappingID { get; set; }
        public int? FYID { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? DueDate { get; set; }

        public bool IsTenureCompleted { get; set; }
    }

    public class VM_CostAuditorRatification : IMessage
    {
        public string Auditor_Name { get; set; }
        public string AddressOfAuditor { get; set; }
        public string AuditorAppointedForFY { get; set; }
        public string RegistrationNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please select BordMeeting Date")]
        public DateTime? YearEndDateOfAuditor { get; set; }

        public string YearEndDate { get; set; }
        public long AgendaID { get; set; }
        public long MeetingAgengaMappingID { get; set; }
    }
}