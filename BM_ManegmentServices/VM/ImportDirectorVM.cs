﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class ImportDirectorFromCustomerVM
    {
        public int CustId { get; set; }
        public long DirectorId { get; set; }
        public string Name { get; set; }
        public string DIN { get; set; }
        public string PAN { get; set; }
        public string Email { get; set; }
        public bool IsCheked { get; set; }
    }
}