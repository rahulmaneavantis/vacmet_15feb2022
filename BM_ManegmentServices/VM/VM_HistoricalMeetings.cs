﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VM_HistoricalMeetings:IMessage
    {
        public long H_ID { get; set; }
        public long ID { get; set; }
        [Required (ErrorMessage ="Please Select Entity")]
        public int EntityId { get; set; }
        public List<int?> MeetingTypeId { get; set; }
        public string EntityName { get; set; }
        public string MeetingTypeName { get; set; }
        public string Circular_Meeting { get; set; }
        public string Type { get; set; }
        public int? FYID { get; set; }
        public string FY { get; set; }
        public string status { get; set; }
        public bool IsOther { get; set; }
        public string AgendaTag { get; set; }
        public int? MeetingSRNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDate { get; set; }
        public string Quarter_ { get; set; }

        public int? MeetingNo { get; set; }
        public bool Isdefaultcheck { get; set; }

        public bool IsComcheck { get; set; }

        public string FileTags { get; set; }

        public IEnumerable<HttpPostedFileBase> files { get; set; }
        public string StartMeetingTime { get; set; }
        public string EndMeetingTime { get; set; }

    }
    public class FYearVM
    {
        public long Id { get; set; }
        public string FYText { get; set; }
    }
    public class HistoricalDataAgendaVM : IMessage
    {
        public long ID { get; set; }
        public long? HistoricalId { get; set; }
        public int? ItemNo { get; set; }
        public string AgendaItemText { get; set; }
        public string Result { get; set; }
        public int? t { get; set; }
    }
}