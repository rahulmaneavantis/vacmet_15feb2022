﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class VMTask
    {

        public long Id { get; set; }
        public string Tasklevel { get; set; }
       public string UserName { get; set; }
        public long TaskAssinmentId { get; set; }
        public long TaskId { get; set; }
        public long TaskTransactionId { get; set; }
        //[Required(ErrorMessage = "Please select Entity")]
        public int? EntityId { get; set; }
        public string CompanyName { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public int? Createby { get; set; }
        // [Required(ErrorMessage = "Please Enter Task Title")]
        public string TaskTitle { get; set; }
        // [Required(ErrorMessage = "Please select Task Type")]
        public int TaskType { get; set; }
        public string TaskTypes { get; set; }
        public int AssignTo { get; set; }
        public string Taskcreatedby { get; set; }
        public string TaskAssignTo { get; set; }
        public string RoleName { get; set; }
        public int Role { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$", ErrorMessage = "Please Enter correct Date")]
        public string AssignOnDate { get; set; }
        public string ReminderDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        //[Required]
        [RegularExpression(@"^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$", ErrorMessage = "Please Enter correct Date")]
        public string DueDate { get; set; }
        public DateTime? TaskDueDate { get; set; }
        public int? MeetingType { get; set; }
        public int? AgendaId { get; set; }
        public int StatusId { get; set; }
        public string status { get; set; }
        public string Description { get; set; }
        public string filesPath { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }

        public string successErrorMsg { get; set; }
        public bool errorMessage { get; set; }
        public bool successMessage { get; set; }
        public string View { get; set; }
        public string Remark { get; set; }
       
        public string TaskTypeDetails { get; set; }
        public TaskTypeVM TaskTypeDetail { get; set; }

        public TaskFile TaskFileDetailsVM { get; set; }

        public List<int> lstReviewer { get; set; }
        public bool IsExpire { get; set; }
    }
    public class ReviewerRemark : IMessage
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public long? MeetingId { get; set; }
        public string  TaskTittle { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
        public int PerformerId { get; set; }
        public string PerformerName { get; set; }
        public int ReviewerId { get; set; }
        public string ReviewerName { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        [AllowHtml]
        public string Remark { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool CanAcceptChanges { get; set; }
        public bool AllowFinalizeReview { get; set; }
        public bool AllowApproveReview { get; set; }
    }
    public class TaskAssignmentVM
    {
        public long TaskAssinmentId { get; set; }
        public long TaskId { get; set; }
        public int AssignTo { get; set; }
        public int Role { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }

    public class AgendaMinutesTaskVM
    {
        public long TaskMeetingId { get; set; }
        public int TaskTypeId { get; set; }
        public long TaskEnitytId { get; set; }
        public long? MaxTaskAssinmentId { get; set; }
    }
    public class TaskTypeVM
    {
        public long TaskId { get; set; }
        public string TaskTypeName { get; set; }

        public string TaskTypeNameforMeeting { get; set; }
        public string TaskTypeNameforAgenda { get; set; }
        public string MeetingName { get; set; }
        public string MeetingType { get; set; }
        public string MeetingPurpose { get; set; }
        public DateTime? MettingDate { get; set; }

        public string AgendaName { get; set; }
        public string ComplienceName { get; set; }
    }

public class TaskFile
{
    public long Id { get; set; }
    public long? TaskId { get; set; }
    public long? FileId { get; set; }
    public string FileName { get; set; }
    public string UploadedBy { get; set; }
    public DateTime UploadedOn { get; set; }
    public string Remark { get; set; }
}
    public class TaskRemarks
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public string Role { get; set; }
        public string User { get; set; }
   
        public DateTime UploadedOn { get; set; }
        public string Remark { get; set; }
    }
    public class TaskMeeting
    {
        public long MeetingId { get; set; }
        public string MeetingTitle { get; set; }
    }

    public class TaskAssignmentUserVM
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
    }

    public class ReviewTransactionVM
    {
        public long Id { get; set; }
        public long? TaskId { get; set; }
        public int? RoleId { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public bool IsSelfMessage { get; set; }
    }


    public class TimelineEventModel
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Description { get; set; }

        public DateTime EventDate { get; set; }

        public string AltField { get; set; }

        public List<TimelineEventModelImage> Images { get; set; }
        public List<TimelineEventModelAction> Actions { get; set; }
    }

    public class TimelineEventModelImage
    {
        public string src { get; set; }
    }
    public class TimelineEventModelAction
    {
        public string text { get; set; }
        public string url { get; set; }
    }
}
