﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class VMCountry
    {
        public int CountryId { get; set; }
        [Required(ErrorMessage ="Please enter Country Name")]
        public string CountryName { get; set; }
        public string Nationality { get; set; }
        public string Message { get; set; }
        public bool error { get; set; }
        public bool success { get; set; }
    }
}