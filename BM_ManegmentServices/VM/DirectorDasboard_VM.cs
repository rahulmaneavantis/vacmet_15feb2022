﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class DirectorDasboard_VM
    {
        public List<TodayMeetingDetails> TodayMeetingVM { get; set; }
    }

    public class TodayMeetingDetails
    {
        public long MeetingId { get; set; }
        public string MeetingType { get; set; }
        public string MeetingTitle { get; set; }
        public int? SrNo { get; set; }
    }

    public class MeetingDetailsforCal
    {
        public long MeetingId { get; set; }
        public string MeetingType { get; set; }
        public string venu { get; set; }
        public DateTime? MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingTitle { get; set; }
    }
}