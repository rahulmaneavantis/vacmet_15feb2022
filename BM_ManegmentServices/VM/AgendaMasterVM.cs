﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class AgendaMasterVM
    {
        public long BM_AgendaMasterId { get; set; }

        public string DefaultAgendaFor { get; set; }
        [UIHint("UIFormID")]
        public long? UIFormID { get; set; }
        public bool ShowDefaultAgendaCtrl { get; set; }
        public bool? IsApproved { get; set; }
        public bool? HasInfo { get; set; }
        public int? Customer_Id { get; set; }
        //[Required(ErrorMessage = "Please Enter Agenda Item Heading")]
        [Required(ErrorMessage = "Please Enter Agenda Item")]
        public string AgendaHeading { get; set; }
        //[Required(ErrorMessage = "Please Enter Agenda Item Text")]
        [Required(ErrorMessage = "Please Enter Agenda Item Heading")]
        public string Agenda { get; set; }
        [UIHint("EntityType")]
        [Required(ErrorMessage = "Please Select Applicability")]
        public int? EntityType { get; set; }
        [UIHint("MeetingTypeId")]
        [Required(ErrorMessage = "Please Select Applicable to(Meeting)")]
        public int? MeetingTypeId { get; set; }
        public string EntityTypeName { get; set; }
        public string MeetingTypeName { get; set; }
        public bool? IsForAllEntityType { get; set; }
        public List<EntityTypeVM> lstEntity { get; set; }
        [UIHint("FrequencyId")]
        public int? FrequencyId { get; set; }
        //public string Frequency { get; set; }
        [UIHint("PreCommitteeMeetingTypeId")]
        public int? PreCommitteeMeetingTypeId { get; set; }
        public bool? IsForAllMeetingType { get; set; }
        public List<CommitteeCompVM> lstMeetingType { get; set; }
        public bool IsVoting { get; set; }
        public bool IsNoting { get; set; }
        public bool IsLive { get; set; }
        public bool CanMultiple { get; set; }

        //public List<ComplianceVM> lstCompliance { get; set; }
        public int UserId { get; set; }

        [AllowHtml]
        public string AgendaFormat { get; set; }
        public string ResolutionFormatHeading { get; set; }
        [AllowHtml]
        public string ResolutionFormat { get; set; }
        public string MinutesFormatHeading { get; set; }
        [AllowHtml]
        public string MinutesFormat { get; set; }
        [AllowHtml]
        public string MinutesDisApproveFormat { get; set; }
        [AllowHtml]
        public string MinutesDifferFormat { get; set; }
        public long ? Parent_Id { get; set; }

        [Required(ErrorMessage = "Please Select Part")]
        [UIHint("PartId")]
        public int? PartId { get; set; }

        public bool? IsMultiStageAgenda { get; set; }

        public bool? IsPostBallotRequired { get; set; }

        public bool? CanPassedByCircularResolution { get; set; }
        public bool IsOrdinaryBusiness { get; set; }
        public bool IsSpecialResolution { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        //public List<AgendaMasterVM> BM_AgendaMaster2 { get; set; }
        //public List<SubAgendaMasterVM> BM_SubAgendaMaster { get; set; }

        //public SubAgendaMasterVM subAgendaMaster { get; set; }

        public Response Message { get; set; }

        public bool? IsSubAgenda { get; set; }

        //public AgendaMaster_SubAgendaMappging subAgendaMasterMapping { get; set; }

        //For Insert Agenda Item in Meeting when Created from Meeting Model
        public long? MeetingId { get; set; }

        public string AgendaInfomative { get; set; }


        [AllowHtml]
        public string SEBI_IntimationFormat { get; set; }
        [AllowHtml]
        public string SEBI_DisclosureFormat { get; set; }

	    public string Doctype { get; set; }

        [UIHint("DocumentType")]
        public DocumentTypeViewdata DocumentType { get; set; }
    }

    public class DocumentTypeViewdata
    {
        public string Type { get; set; }
        public string TypeName { get; set; }
    }
    public class AgendaInfoVM : IMessage
    {
        public long AgendaInfoId { get; set; }

        [Required(ErrorMessage = "Please Enter Agenda Information")]
        [AllowHtml]
        public string AgendaInfo { get; set; }
    }

    public class ChangeAgendaItemNumberVM : IMessage
    {
        public long ChangeMeetingAgendaMappingID { get; set; }
        public int? ItemNo { get; set; }
        [AllowHtml]
        public string ChangeAgendaItemText { get; set; }
        public bool ReadOnly { get; set; }
    }

    public class UIFormsVM
    {
        public long UIFormID { get; set; }
        public string UIFormName { get; set; }
    }
    public class StandardAgendaVM
    {
        public int? EntityType { get; set; }
        public int? MeetingTypeId { get; set; }
        public string EntityTypeName { get; set; }
        public string MeetingTypeName { get; set; }

        public string AgendaHeading { get; set; }
        public StandardAgendaFormatesVM formates { get; set; }
    }
    public class StandardAgendaFormatesVM : IMessage
    {
        public long AgendaMasterId { get; set; }
        public long? StandardAgendaMasterId { get; set; }
        public int CustomerID { get; set; }

        [Required(ErrorMessage = "Please Enter Agenda Item Text")]
        public string AgendaItemText { get; set; }

        [Required(ErrorMessage = "Please Enter Agenda Format")]
        [AllowHtml]
        public string AgendaFormat { get; set; }

        public string ResolutionFormatHeading { get; set; }
        [AllowHtml]
        public string ResolutionFormat { get; set; }

        public string MinutesFormatHeading { get; set; }
        [AllowHtml]
        public string MinutesFormat { get; set; }
        [AllowHtml]
        public string MinutesDisApproveFormat { get; set; }
        [AllowHtml]
        public string MinutesDifferFormat { get; set; }

        [AllowHtml]
        public string SEBI_IntimationFormat { get; set; }
        [AllowHtml]
        public string SEBI_DisclosureFormat { get; set; }
    }

    public class StandardAgendaFormates_SubVM : IMessage
    {
        public long AgendaMasterIdSub { get; set; }
        public long? StandardAgendaMasterIdSub { get; set; }
        public int CustomerID { get; set; }

        [Required(ErrorMessage = "Please Enter Agenda Item Text")]
        public string AgendaItemTextSub { get; set; }

        [Required(ErrorMessage = "Please Enter Agenda Format")]
        [AllowHtml]
        public string AgendaFormatSub { get; set; }

        public string ResolutionFormatHeadingSub { get; set; }
        [AllowHtml]
        public string ResolutionFormatSub { get; set; }

        public string MinutesFormatHeadingSub { get; set; }
        [AllowHtml]
        public string MinutesFormatSub { get; set; }
        [AllowHtml]
        public string MinutesDisApproveFormatSub { get; set; }
        [AllowHtml]
        public string MinutesDifferFormatSub { get; set; }

    }
    public class AgendaMaster_SubAgendaMappging
    {
        public long Agenda_SubAgendaMappgingID { get; set; }
        public string SubAgenda { get; set; }
        [Required(ErrorMessage = "Please Select Meeting")]
        public int? MeetingTypeIDSub { get; set; }
        public string MeetingTypeNameSub { get; set; }
        [Required(ErrorMessage = "Please Enter Sequence Number")]
        public int? Sequence { get; set; }
        [UIHint("StageId")]
        public int? StageId { get; set; }
        public string StageName { get; set; }

        public long Parent_AgendaMasterId { get; set; }
        [Required(ErrorMessage = "Please Select Sub Agenda")]
        public long SubAgendaMasterId { get; set; }
        
        public int UserId { get; set; }
        public Response Message { get; set; }

        public bool ShowCtrl { get; set; }

    }

    public class SubAgendaMasterVM
    {
        public long BM_SubAgendaMasterId { get; set; }
        public long BM_AgendaMasterId { get; set; }
        //[Required(ErrorMessage = "Please Enter Agenda Item Heading")]
        [Required(ErrorMessage = "Please Enter Agenda Item")]
        public string SubAgendaHeading { get; set; }
        //[Required(ErrorMessage = "Please Enter Agenda Item Text")]
        [Required(ErrorMessage = "Please Enter Agenda Item Heading")]
        public string SubAgenda { get; set; }
        public bool IsVotingSub { get; set; }
        [Required(ErrorMessage = "Please select Applicable to(Meeting)")]
        [UIHint("MeetingTypeIdSub")]
        public int? MeetingTypeIdSub { get; set; }
        public string MeetingTypeNameSub { get; set; }
        public int? Sequence { get; set; }
        [UIHint("StageId")]
        public int? StageId { get; set; }
        public string StageName { get; set; }
        public bool? IsPostBallotRequiredSub { get; set; }
        public bool? CanPassedByCircularResolutionSub { get; set; }

        [AllowHtml]
        public string AgendaFormatSub { get; set; }

        public string ResolutionFormatHeadingSub { get; set; }
        [AllowHtml]
        public string ResolutionFormatSub { get; set; }

        public string MinutesFormatHeadingSub { get; set; }
        [AllowHtml]
        public string MinutesFormatSub { get; set; }
        [AllowHtml]
        public string MinutesDisApproveFormatSub { get; set; }
        [AllowHtml]
        public string MinutesDifferFormatSub { get; set; }

        public Response Message { get; set; }
        public int UserId { get; set; }
        public bool? IsSubAgenda { get; set; }

        [AllowHtml]
        public string SubAgendaInfomative { get; set; }

        public bool? SubHasInfo { get; set; }

    }

    public class AgendaItemSelect
    {
        public long BM_AgendaMasterId { get; set; }
        public string AgendaHeading { get; set; }
        public string Agenda { get; set; }
        public int? PartID { get; set; }
        public bool IsCheked { get; set; }
        public long Meeting_Id { get; set; }

        public int? FrequencyID { get; set; }

        public long? RefPendingMappingID { get; set; }
        public string RefMeeting { get; set; }
        public string RefResult { get; set; }

        public bool? IsNewStage { get; set; }
        public long? StartAgendaId { get; set; }
        public long? StartMeetingId { get; set; }
        public long? StartMappingId { get; set; }
        public int? SequenceNo { get; set; }
        public bool? HasCompliance { get; set; }
        public bool? HasInfo { get; set; }

        public long? ScheduleOnID { get; set; }
        public long? RefMasterID { get; set; }
        public long? RefMasterIDOriginal { get; set; }

        public long? OpenAgendaId { get; set; }
        public string RefMasterName { get; set; }
        public long CopyAgendaMappingId { get; set; }
        public long? CopyMeetingId { get; set; }
    }

    public class AgendaFrequencyVM
    {
        public int FrequencyId { get; set; }

        public string FrequencyName { get; set; }
    }

    public class AgendaComplianceMappingPendingVM
    {
        public long? AgendaId { get; set; }
        public string Agenda { get; set; }
    }
    public class AgendaComplianceVM : IMessage
    {
        public long Agenda_ComplianceId { get; set; }
        public string MappingType { get; set; }
        public long? AgendaMasterId { get; set; }
       
        public long ComplianceId { get; set; }
        public string ComplianceShortDesc { get; set; }
        public string DateType { get; set; }
        public string DayType { get; set; }
        public string BeforeAfter { get; set; }
        [Required(ErrorMessage = "Please Enter Number")]
        public decimal? Numbers { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? startDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? endDate { get; set; }
        public decimal? decendingNo { get; set; }
        public string DaysOrHours { get; set; }
        public DateTime? ScheduleOn { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
        public int UserId { get; set; }
        public int MeetingType { get; set; }
        public int EntityTypeId { get; set; }
        public int? Compliancesrno { get; set; }
        public int? afterbefor { get; set; }
        public bool IssubindustrySpecific { get; set; }
    }

	public partial class OpenAgendaDetailsVM
    {
        public int? SrNo { get; set; }
        public long MeetingId { get; set; }
        public long? AgendaID { get; set; }
        public long? MeetingAgendaMappingID { get; set; }
        public string MeetingTypeName { get; set; }
        public string MeetingCircular { get; set; }
        public DateTime? MeetingDate { get; set; }
        public int? MeetingSrNo { get; set; }
        public string FYText { get; set; }
        public string AgendaItemText { get; set; }
        public string AgendaFormat { get; set; }
        public string ResultRemark { get; set; }
	}

    public class AgendaComplianceEditorVM : AgendaComplianceVM
    {
        [UIHint("TriggerOn")]
        public TriggeronViewdata TriggerOn { get; set; }
        [UIHint("Type")]
        public TypeViewModel Type { get; set; }
        public bool Autoclose { get; set; }

        [UIHint("DayTypeNames")]
        public DayTypeViewModel DayTypeNames
        {
            get;
            set;
        }

        [UIHint("_BeforeAfter")]
        public BeforeAfterViewModel BeforeAfterNames
        {
            get;
            set;
        }

        [UIHint("DaysOrHoursNames")]
        public DaysOrHoursViewModel DaysOrHoursNames
        {
            get;
            set;
        }

        [UIHint("MeetingLevel")]
        public MeetingViewModel MeetingLevel
        {
            get;
            set;
        }

        [UIHint("_Regulatory")]
        public SubIndustriesVM Regulatory
        {
            get;
            set;
        }
    }

    public class MeetingViewModel
    {
        public string MeetingLevelId { get; set; }
        public string MeetingLevelName { get; set; }
    }

    public class AutocloseViewModel
    {
       public string AutocloseName { get; set; }
       public string Autoclose { get; set; }

    }
    public class TypeViewModel
    {
        public string Type { get; set; }
        public string TypeName { get; set; }
    }
    public class TriggeronViewdata
    {
        public string Triggeron { get; set; }
        public string TriggerOnName { get; set; }
    }
    public class DaysOrHoursViewModel
    {
        public string DaysOrHours { get; set; }
        public string DaysOrHoursName { get; set; }
    }
    public class DayTypeViewModel
    {
        public string DayType { get; set; }
        public string DayTypeName { get; set; }
    }

    public class BeforeAfterViewModel
    {
        public string BeforeAfter { get; set; }
        public string BeforeAfterName { get; set; }
    }

    public class AgendaComlianceMapping:IMessage
    {
        public long ComplianceId { get; set; }
        public long ActId { get; set; }
        public string Compliance { get; set; }
        public string Act { get; set; }
    }

    public class NewCompliances_VM
    {
        public long Id { get; set; }
        public string SEBI { get; set; }
        public string NonCompliance { get; set; }
        public string ComplianceNumber { get; set; }
    }
}