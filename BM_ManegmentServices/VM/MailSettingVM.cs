﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class MailSettingVM : IMessage
    {
        public long MailSettingId { get; set; }
        [Required(ErrorMessage = "Please enter Email ID")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter valid Email ID")]
        public string Email_Entity { get; set; }
        [Required(ErrorMessage ="Please enter Password")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter SMTP Port")]
        public int Port { get; set; }
        [Required(ErrorMessage = "Please enter Mail Server Domain")]
        public string Host { get; set; }
        public bool? EnableSsl { get; set; }
        [UIHint("EntityId")]
        [Range(1,Int64.MaxValue, ErrorMessage ="Please select Entity Name")]
        public int? EntityID { get; set; }
        public int CustomerID { get; set; }

        public string EntityName { get; set; }
    }

    public class TestMailVM : IMessage
    {
        public long MailSettingId { get; set; }
        public int? EntityID { get; set; }
        public string FromMail { get; set; }
        [Required(ErrorMessage ="Please enter To Mail")]
        public string To { get; set; }
        public string Cc { get; set; }
        public string Subject { get; set; }
        [AllowHtml]
        public string Body { get; set; }
    }
}