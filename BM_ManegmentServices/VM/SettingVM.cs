﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class SettingVM
    {
    }

    public class PageHeaderFooterSettingVM : IMessage
    {
        public long  PageHeaderId { get; set; }
        public int? PageHeaderEntityId { get; set; }
        [AllowHtml]
        public string PageHeaderFormat { get; set; }
        [AllowHtml]
        public string PageFooterFormat { get; set; }
        public bool UsedInAgenda { get; set; }
        public bool UsedInNotice { get; set; }
        public bool UsedInCTC { get; set; }
        public bool UsedInComplianceDoc { get; set; }
    }

    public class PageHeaderFooterSettingListVM
    {
        public long PageHeaderId { get; set; }
        public int? PageHeaderEntityId { get; set; }
        public string EntityName { get; set; }
    }
}