﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class AddtionalAct_VM :IMessage
    {
        public long Id { get; set; }
        public long ActId { get; set; }    
        public string LSTAct { get; set; }
        public string ActName { get; set; }
        public long AdditionalActId { get; set; }
    }
}