﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class CommitteeMasterVM
    {
        public long Id { get; set; }
        [UIHint("EntityId")]
        public int Entity_Id { get; set; }
        public string EntityName { get; set; }
        [UIHint("CommitteeId")]
        public int Committee_Id { get; set; }
        public string CommitteeName { get; set; }
        public string NameOfOtherCommittee { get; set; }

        [UIHint("DirectorId")]
        public long Director_Id { get; set; }
        [UIHint("UserId")]
        public int? UserId { get; set; }
        public string DirectorName { get; set; }
        public string ImagePath { get; set; }
        public int Designation_Id { get; set; }
        public int DirectorDesignation_Id { get; set; }
        public string Designation_Name { get; set; }
        public int Committee_Type { get; set; }
        public string Committee_Type_Name { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? AppointmentFrom { get; set; }
        public DateTime? AppointmentTo { get; set; }
        public string ParticipantType { get; set; }
        public string ParticipantDesignation { get; set; }
        public string DIN_PAN { get; set; }
        public int DirectorshipIdForQuorum { get; set; }
        public string Directorship { get; set; }
        public int PositionId { get; set; }
        public string Position { get; set; }
        public bool IsNew { get; set; }
        public bool? IsStandard { get; set; }
        public bool? IsCurrentCustRecord { get; set; }

    }

    public class CM_Director
    {
        public long DirectorId { get; set; }
        public string DirectorName { get; set; }
        public string ImagePath { get; set; }
    }

    public class CM_Entity
    {
        public long EntityId { get; set; }
        public string EntityName { get; set; }
    }

    public class CM_Designation
    {
        public long DesignationId { get; set; }
        public string Designation { get; set; }
    }

    public class CM_Committee_Type
    {
        public long CommitteeTypeId { get; set; }
        public string CommitteeType { get; set; }
    }
}