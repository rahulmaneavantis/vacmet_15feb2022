﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.UIForms
{
    public class UIFormVM
    {
    }

    public class UIForm_KMPMasterVM : IMessage
    {
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        public long? RefMasterID { get; set; }
        public string Salutation { get; set; }
        public string FSalutations { get; set; }
        [Required(ErrorMessage = "Please enter First Name")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "Please enter Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter Father First Name")]
        public string Father { get; set; }
        public string FatherMiddleName { get; set; }
        public string FatherLastName { get; set; }
        [Required(ErrorMessage = "Please enter PAN Number")]
        [RegularExpression(@"[a-zA-Z]{3}[a,b,c,f,g,h,l,j,p,t,e,A,B,C,F,G,H,L,J,P,T,E]{1}[a-zA-Z]{1}\d{4}[a-zA-Z]{1}", ErrorMessage = "Please enter valid PAN")]
        public string PAN { get; set; }
        public string Permenant_Address_Line1 { get; set; }
        public string Permenant_Address_Line2 { get; set; }

        [Required(ErrorMessage = "Please enter Line 1 address")]
        public string Present_Address_Line1 { get; set; }
        [Required(ErrorMessage = "Please enter Line 2 address")]
        public string Present_Address_Line2 { get; set; }

        public string DateOfConsent_ { get; set; }
        public string Kmp_appointment_Date { get; set; }
        public string Kmp_appointment_Date_To { get; set; }
        [RegularExpression(@"[A|F][0-9]{5}|[A|F][0-9]{4}|[A|F][0-9]{3}", ErrorMessage = "Please Enter valid CS_Membersip Number")]
        public string CS_MemberNo { get; set; }
        public int DesignationId { get; set; }
        public string Renumeration { get; set; }
        public string StartingSalary { get; set; }
        public string UptoSalary { get; set; }
        public string NoOfLeaveDays { get; set; }

        public int Present_StateId { get; set; }
        public int Present_CityId { get; set; }
        public string Present_Address_State { get; set; }
        public string Present_Address_City { get; set; }
        public string Present_PINCode { get; set; }
    }

    public class UIForm_KMPMasterCessationVM : IMessage
    {
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        [Required(ErrorMessage = "Please select name")]
        public long RefMasterID { get; set; }
        public long? EntityId { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string Present_Address_Line1 { get; set; }
        public string Present_Address_Line2 { get; set; }
        public string PAN { get; set; }

        [Required(ErrorMessage = "Please enter cessation Date")]
        public string cessation_Date { get; set; }
        public int DesignationId { get; set; }
    }

    public class UIForm_DirectorMasterVM : IMessage
    {
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        [Required(ErrorMessage = "Please select name")]
        public long RefMasterID { get; set; }

        public string OriginalDirectorDIN { get; set; }
        public long OriginalDirector_Id { get; set; }
        public string OriginalDirectorName { get; set; }
        public string ClauseNoOfAOA { get; set; }

        public string DIN { get; set; }
        public long Director_Id { get; set; }
        public int EntityId { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string Father { get; set; }

        public string Present_Address_Line1 { get; set; }
        public string Present_Address_Line2 { get; set; }
        public string PAN { get; set; }

        [Required(ErrorMessage = "Please enter appointment Date")]
        public string DateOfAppointment { get; set; }
        public string TenureOfAppointment { get; set; }

        public int DirectorDesignationId { get; set; }
        public int TypeOfDirectorShipId { get; set; }

        public string CompanyOrInstitutionForNominee { get; set; }
        public string EmailIdOfDirectorForNominee { get; set; }

        public string TermOfAppointment { get; set; }
        //public int NatureOfInterest { get; set; }
        //public int IfDirector { get; set; }
        //public int IfInterestThroughRelatives { get; set; }
        //public decimal PercentagesOfShareHolding { get; set; }

        public string StartingSalary { get; set; }
        public string UptoSalary { get; set; }
        public string NoOfLeaveDays { get; set; }

        public string DateOfConsent_ { get; set; }
        public string CS_MemberNo { get; set; }
        public string Renumeration { get; set; }
    }

    public class UIForm_DirectorMasterCessationVM : IMessage
    {
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        //[Required(ErrorMessage = "Please select name")]
        public long RefMasterID { get; set; }
        public long? EntityId { get; set; }

        public long Director_Id { get; set; }
        public string DIN { get; set; }
        public string PAN { get; set; }
        public string FirstName { get; set; }

        public string Present_Address_Line1 { get; set; }

        [Required(ErrorMessage = "Please enter cessation Date")]
        public string CessationEffectFrom { get; set; }
        public string ResignedDate { get; set; }
        
        public int DirectorDesignationId { get; set; }

        public string FullNameOfDirector { get; set; }
    }

    public class UIForm_AuditorResignationVM : IMessage
    {
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        public long RefMasterID { get; set; }
        public long? EntityId { get; set; }

        public string AuditorName { get; set; }
        public string AddressOfAuditor { get; set; }
        public string RegistrationNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfResignation { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfResignationSubmit { get; set; }
        public string ResonOfResignation { get; set; }
        public long? FileDataId { get; set; }

        public string Date_Of_Resignation { get; set; }
        public string Date_Of_Resignation_Submit { get; set; }


    }


    public class UIForm_FinancialResult_ListedVM : IMessage
    {
        public long AgendaID { get; set; }
        public long MappingID { get; set; }

        public long? EntityId { get; set; }
        public int EntityTypeId { get; set; }

        public string NameOfAuthorisedDirector { get; set; }
        public string DesignationOfAuthorisedDirector { get; set; }
        public string DINOfAuthorisedDirector { get; set; }

        public string QuarterEndedForTheMeeting { get; set; }

        public string NameOfHoldingCompany { get; set; }
    }

    public class UIForm_FinancialResultYearEnded_PrivateVM : IMessage
    {
        public long AgendaID { get; set; }
        public long MappingID { get; set; }

        public long? EntityId { get; set; }
        public int EntityTypeId { get; set; }

        public string Id_of_1st_director { get; set; }
        public string Name_of_1st_director { get; set; }
        public string DIN_of_1st_director { get; set; }

        public string Id_of_2nd_director { get; set; }
        public string Name_of_2nd_director { get; set; }
        public string DIN_of_2nd_director { get; set; }

        public string QuarterEndedForTheMeeting { get; set; }
    }
    public class UIForm_FinancialResultYearEnded_ListedVM : IMessage
    {
        public long AgendaID { get; set; }
        public long MappingID { get; set; }

        public long? EntityId { get; set; }
        public int EntityTypeId { get; set; }

        public string Name_of_managing_director { get; set; }
        public string DIN_of_managing_director { get; set; }

        public string Name_of_CFO { get; set; }
        public string Name_of_CS { get; set; }

        public string QuarterEndedForTheMeeting { get; set; }
    }

    public class UIForm_Calling_GeneralMeetingVM : IMessage
    {
        public long Meeting_ID_ { get; set; }
        public long AgendaID { get; set; }
        public long MappingID { get; set; }

        public long? EntityId { get; set; }
        public int EntityTypeId { get; set; }
        public int GMTypeId { get; set; }

        //[Required(ErrorMessage = "Please enter Serial number")]
        public string SerialNo_ { get; set; }
        
        [Required(ErrorMessage = "Please select financial year")]
        public string FYID { get; set; }
        public string FYText { get; set; }

        [Required(ErrorMessage = "Please enter date of meeting")]
        public string DateOfGM { get; set; }

        [Required(ErrorMessage = "Please select start time")]
        public string StartTimeOfGM { get; set; }
        public string EndTimeOfGM { get; set; }
        [Required(ErrorMessage = "Please enter venue")]
        public string VenueOfGM { get; set; }

        public string IsEVoting { get; set; }
    }

    public class EntityMasterBooksOfAccountsVM : IMessage
    {
        public long Meeting_ID_ { get; set; }
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        public long MeetingAgengaMappingID { get; set; }

        public int BOA_Id { get; set; }
        public int BOA_EntityId { get; set; }
        public DateTime? DateOfBoardMeeting { get; set; }
        public string BOA_AddressLine1 { get; set; }
        public string BOA_AddressLine2 { get; set; }
        public string BOA_CityName { get; set; }
        public int? BOA_StateId { get; set; }
        public string BOA_StateName { get; set; }
        public int? BOA_CountyId { get; set; }
        public string BOA_Pincode { get; set; }
    }

    public class EntityMasterBooksOfAccountsTemplateVM
    {
        public string AddressAtWhichBooksToBeKept { get; set; }
    }

    #region Shifting Register Office
    public class ShiftingRegisterOfficeVM : IMessage
    {
        public long Meeting_ID_ { get; set; }
        public long AgendaID { get; set; }
        public long MappingID { get; set; }
        public long MeetingAgengaMappingID { get; set; }

        public int Shifting_Id { get; set; }
        public int Shifting_EntityId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EffectFrom { get; set; }

        public string Regi_Address_Line1 { get; set; }
        public string Regi_Address_Line2 { get; set; }
        public int? Regi_CityId { get; set; }
        public string Regi_CityName { get; set; }
        public int? Regi_StateId { get; set; }
        public string Regi_StateName { get; set; }
        public string Regi_PINCode { get; set; }

        public string Regi_Address_Line1New { get; set; }
        public string Regi_Address_Line2New { get; set; }
        public int? Regi_CityIdNew { get; set; }
        public string Regi_CityNameNew { get; set; }
        public int? Regi_StateIdNew { get; set; }
        public string Regi_StateNameNew { get; set; }
        public string Regi_PINCodeNew { get; set; }
        public string RegisteredOfficeAddress { get; set; }
    }

    public class ShiftingRegisterOfficeTemplateVM
    {
        public string RegisteredOfficeAddress { get; set; }
        public string NewRegisteredOfficeAddress { get; set; }
        public string DateOfShifting { get; set; }
    }
    #endregion

    #region Increase In Authorised Capital
    public class IncreaseInAuthCapitalTemplateVM
    {
        public decimal ExistingAuthorisedShareCapital { get; set; }
        public decimal IncreasedAuthorisedShareCapital { get; set; }
        public decimal NumberOfEquityShares { get; set; }
        public decimal FaceValueOfEquityShare { get; set; }
        public decimal EquityShareCapital { get; set; }
        public decimal IncreasedNumberOfEquityShares { get; set; }
        public decimal IncreasedEquityShareCapital { get; set; }

        public decimal NumberOfPreferenceShares { get; set; }
        public decimal FaceValueOfPreferenceShare { get; set; }
        public decimal PreferenceShareCapital { get; set; }
        public decimal IncreasedNumberOfPreferenceShare { get; set; }
        public decimal IncreasedPreferenceShareCapital { get; set; }
    }
    #endregion
}