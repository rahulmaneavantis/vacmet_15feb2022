﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM.Annotation
{
    public class AnnotationVM
    {
    }

    public class AgendaMinutesCommentsVM : IMessage
    {
        public long AgendaMinutesCommentsId { get; set; }
        public long MeetingId { get; set; }
        public string DocType { get; set; }
        public string FilePath { get; set; }
        public string CommentData { get; set; }
        public long MeetingParticipantId { get; set; }
        public bool IsCS { get; set; }
        public string DocURL { get; set; }
        public long? DraftCirculationID_ { get; set; }
        public bool IsDraftMinutesApproved { get; set; }
        public bool ViewOnly { get; set; }
    }

    public class AgendaMinutesCommentsTransactionVM
    {
        public long CommentsTransaction { get; set; }
        public long AgendaMinutesCommentsId { get; set; }
        public long MeetingParticipantId { get; set; }
        public string CommentData { get; set; }
    }
}