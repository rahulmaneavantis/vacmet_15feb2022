﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class FormsVM
    {
        public long MeetingFormMappingId { get; set; }
        public long MeetingId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public long ComplianceId { get; set; }

        public long FormId { get; set; }
        public string FormName { get; set; }
        public string EForm { get; set; }

        public bool IsEForm { get; set; }
        public bool CanGenerate { get; set; }
        public bool ForMultipleEvents { get; set; }
        public int EventCount { get; set; }
        public string EventMessage { get; set; }
    }

    public class FormsDocumentVM
    {
        public int EntityId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public long FormMappingId { get; set; }
        public string FormFormat { get; set; }
        public bool? LetterHead { get; set; }
    }
    public class MeetingFormsVM
    {
        public long MeetingId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public long ComplianceId { get; set; }
    }

    public class EFormVM : IMessage
    {
        public long MeetingFormMappingId { get; set; }
        public string FormName { get; set; }
        public byte[] FormData { get; set; }

        public string FullFileName { get; set; }
        public long DirectorID { get; set; }
    }

    public class EFormFieldVM
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class MultipleFormsVM
    {
        public long MeetingFormMappingId { get; set; }
        public long MeetingId { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public long? RefMasterID { get; set; }
        public long? UIFormID { get; set; }
        public int? ItemNo { get; set; }
    }
    public class ExcelFileFormUploadVM : IMessage
    {
        public long EntityId { get; set; }
        public int FYId { get; set; }
        public HttpPostedFileBase File { get; set; }
//        [Required(ErrorMessage = "Please select Form Type")]
        public string FormName { get; set; }
        public bool CanGenerateEform { get; set; }
    }

    #region DPT#
    public class DPT3EFormVM
    {
        public _08_NetWorthVM _08_NetWorth { get; set; }
        public _15_A_ReceiptOfMoneyVM _15_A_ReceiptOfMoney { get; set; }
        public _15_B_AmountReceivedFromVM _15_B_AmountReceivedFrom { get; set; }
        public _15_C_AnyAmountReceiptVM _15_C_AnyAmountReceipt { get; set; }
        public _15_D_AnyAmountReceiptAsLoanVM _15_D_AnyAmountReceiptAsLoan { get; set; }
        public _15_I_AnyAmountReceiptAsLoanVM _15_I_AnyAmountReceiptAsLoan { get; set; }
        public _15_M_AnyAmountReceiptAsLoanVM _15_M_AnyAmountReceiptAsLoan { get; set; }
        public _15_S_AnyAmountReceiptAsLoanVM _15_S_AnyAmountReceiptAsLoan { get; set; }
        public string _15_E_IssueOfCommercial { get; set; }

        public string _15_F_FromOtherCompany { get; set; }

        public string _15_G_IssueOfCommercial { get; set; }

        public string _15_H_FromPerson { get; set; }
        public string _15_J_NonConvertibleDebentures { get; set; }
        public string _15_K_EmployeeOfCompany { get; set; }
        public string _15_L_NonInterest { get; set; }

        public string _15_N_ByPromoters { get; set; }
        public string _15_O_ByNidhiCompnay { get; set; }
        public string _15_P_ByChitFund { get; set; }
        public string _15_Q_SEBI { get; set; }
        public string _15_R_ByStartUp { get; set; }
    }

    public class _08_NetWorthVM
    {
        public string PaidUpShareCapital { get; set; }
        public string FreeReserves { get; set; }
        public string AccumulatedLoss { get; set; }
        public string SecuritiesPremium { get; set; }
        public string BalanceOfDeferred { get; set; }
        public string AccumulatedUnprovidedDep { get; set; }
        public string MiscellaneousExpense { get; set; }
        public string OtherIntangible { get; set; }
        public string NetWorth { get; set; }

    }

    public class _15_A_ReceiptOfMoneyVM
    {
        public string CentralGovernment { get; set; }
        public string stateGovernment { get; set; }
        public string LocalAuthority { get; set; }
        public string StatutoryAuthority { get; set; }
    }

    public class _15_B_AmountReceivedFromVM
    {
        public string ForeignGovernments { get; set; }
        public string ForeignInternationalBanks { get; set; }
        public string MultilateralFI { get; set; }
        public string ForeignGODFI { get; set; }


        public string ForeignECA { get; set; }
        public string ForeignCO { get; set; }
        public string ForeignBodyCor { get; set; }
        public string ForeignCitizens { get; set; }
        public string ForeignAuthorities { get; set; }
        public string PersonsResidentsOutsideIndia { get; set; }
    }

    public class _15_C_AnyAmountReceiptVM
    {
        public string BankingCompany { get; set; }
        public string SBI { get; set; }
        public string NotifiedByTheCentralGovernment { get; set; }
        public string CorrespondingNewBank { get; set; }
        public string CooperativeBank { get; set; }
    }

    public class _15_D_AnyAmountReceiptAsLoanVM
    {
        public string PublicFinancialInstitutions { get; set; }
        public string Government { get; set; }
        public string AnyRegionalFI { get; set; }
        public string InsuranceCompanies { get; set; }
        public string ScheduledBanks { get; set; }
    }

    public class _15_I_AnyAmountReceiptAsLoanVM
    {
        public string AmountRaisedBy { get; set; }
        public string BondsOrDebentures { get; set; }
    }

    public class _15_M_AnyAmountReceiptAsLoanVM
    {
        public string AdvanceForSupply { get; set; }
        public string AdvanceAccounted { get; set; }
        public string SecurityDeposit { get; set; }
        public string LongTermProjects { get; set; }
        public string TowardsConsideration { get; set; }
        public string SectoralRegulator { get; set; }
        public string Publication { get; set; }
    }

    public class _15_S_AnyAmountReceiptAsLoanVM
    {
        public string InvestmentFund { get; set; }
        public string CapitalFund { get; set; }
        public string InfraTrust { get; set; }
        public string RealEstateTrust { get; set; }
        public string MutualFund { get; set; }
    }
    #endregion
}