﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class ActVM
    {
        public long ActId { get; set; }
        public string ActName { get; set; }
    }

    public class ActListViewVM
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }

    public class ComplianceVM
    {
        public long ComplianceId { get; set; }
        public string Description { get; set; }
    }
    public class ComplianceDetailsVM : ComplianceVM
    {
        public long ActID { get; set; }
        public byte? RiskType { get; set; }
        public string DetailedDescription { get; set; }
        public string SampleFormLink { get; set; }
        public string ReferenceText { get; set; }
        public string Sec { get; set; }
    }

    public class AgendaComplianceAssingVM : IMessage
    {
        public string Name { get; set; }
        public int PerformerID { get; set; }
        public int ReviewerID { get; set; }
        public string ScheduleOnDate { get; set; }
        public int EntityID { get; set; }
        public long DirectorID { get; set; }
        public int CustomerID { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string MappingType { get; set; }
        public List<AgendaComplianceListVM> lst { get; set; }
    }
    public class AgendaComplianceListVM
    {
        public long AgendaId { get; set; }
        public string AgendaItem { get; set; }
        public long ComplianceId { get; set; }
        public string Description { get; set; }

        public bool IsAgendaSelect { get; set; }
        public bool IsComplianceSelect { get; set; }

        public int PerformerID { get; set; }
        public int ReviewerID { get; set; }

        public PerformerReviewerViewModel Performer { get; set; }
        public PerformerReviewerViewModel Reviewer { get; set; }
        public int EntityID { get; set; }
        public long CustomerBranchID { get; set; }
        public int? MeetingTypeId { get; set; }
        public string MeetingTypeName { get; set; }

        public long? ComplianceInstanceIdNew { get; set; }

    }

    public class StatutoryComplianceListVM
    {
        public long ActId { get; set; }
        public string ActName { get; set; }

        public long ComplianceId { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Section { get; set; }
        public string Risk { get; set; }
        public string Frequency { get; set; }
        public Nullable<int> TagID { get; set; }

        public bool IsComplianceSelect { get; set; }
        public int PerformerID { get; set; }
        public int ReviewerID { get; set; }
        public PerformerReviewerViewModel Performer { get; set; }
        public PerformerReviewerViewModel Reviewer { get; set; }
        public long CustomerBranchID { get; set; }
        public int EntityID { get; set; }
        public string EntityName { get; set; }
        public long DirectorID { get; set; }
    }

    public class ComplianceAssignedListVM
    {
        public long ComplianceId { get; set; }
        public long InstanceId { get; set; }
        public string Description { get; set; }

        public PerformerReviewerViewModel Performer { get; set; }
        public PerformerReviewerViewModel Reviewer { get; set; }
        public int EntityID { get; set; }
        public string CompanyName { get; set; }
        public long? DirectorID { get; set; }
        public bool IsComplianceSelect { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ScheduleOnDate { get; set; }

    }

    public class ComplianceReAssingmentVM : IMessage
    {
        public int EntityID { get; set; }
        public int CustomerID { get; set; }
        public int UpdatedBy { get; set; }
        public List<ComplianceReAssignmentListVM> lst { get; set; }
    }
    public class ComplianceReAssignmentListVM
    {
        public long ComplianceAssignmentId { get; set; }
        public long ComplianceId { get; set; }
        public string Description { get; set; }
        public bool IsComplianceSelect { get; set; }
        public int RoleID { get; set; }
        public string UserRole { get; set; }
        public PerformerReviewerViewModel User { get; set; }
        public int EntityId { get; set; }
        public string CompanyName { get; set; }
        public int? CustId { get; set; }
        public string CustName { get; set; }
        public string MappingType { get; set; }

    }

    public class PerformerReviewerViewModel
    {
        public long UserID { get; set; }
        public string FullName { get; set; }
    }

    public class UploadComplianceAssignmentVM : IMessage
    {
        public string FilePath { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
        public bool isUploaded { get; set; }

        public List<UploadComplianceVM> lstCompliance { get; set; }
        
    }

    public class UploadComplianceVM
    {
        public int EntityID { get; set; }
        public string EntityName { get; set; }

        public long ComplianceId { get; set; }
        public string Description { get; set; }

        public bool IsValid { get; set; }

        public string PerformerMail { get; set; }
        public string ReviewerMail { get; set; }

        public string ScheduleOn { get; set; }
        public DateTime ScheduleOnDate { get; set; }

        public PerformerReviewerViewModel Performer { get; set; }
        public PerformerReviewerViewModel Reviewer { get; set; }

        public string ValidationMessage { get; set; }
    }

    public class SecretarialTag_VM
    {
        public int ID { get; set; }
        public string Tag { get; set; }
    }
}