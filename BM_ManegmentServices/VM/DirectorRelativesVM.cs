﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class DirectorRelativesVM
    {
            [Required(ErrorMessage = "Please select Relation")]
            public string Relation { get; set; }
            public long Id { get; set; }
            public long Director_ID { get; set; }
            
            [Required(ErrorMessage = "Please enter Name")]
            [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Please enter valid Name")]
            public string Name { get; set; }

            [Required(ErrorMessage = "Please select Minor status")]
            public bool? Minor_or_Adult { get; set; }

            public bool? MaritalStatus { get; set; }
            
            [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please enter valid Name")]
            public string Spouse { get; set; }
            public DateTime? ChangeDate { get; set; }
            public int? ChangeTypeId { get; set; }
            public bool? IsActive { get; set; }
            public Response Message { get; set; }
            public bool? IsDeleted { get; set; }
            public int CreatedBy { get; set; }
            public System.DateTime? CreatedOn { get; set; }
            public int? UpdatedBy { get; set; }
            public System.DateTime? UpdatedOn { get; set; }
    }

    public enum RelationEnum
    {
        HUF = 1,
        Spouse,
        Father,
        Mother,
        Son,
        Daughter,
        Brother,
        Sister
    }

    public class RelationVM
    {
        public string id { get; set; }
        public string text { get; set; }
    }
}