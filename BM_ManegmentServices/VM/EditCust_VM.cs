﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class EditCust_VM
    {
        public int? CustomerID { get; set; }
        public int ID { get; set; }
        public bool SentMailtoDirector { get; set; }
        public bool DefaultVirtualMeeting { get; set; }
        public bool IsVirtualMeeting { get; set; }
        public string Name { get; set; }

        public string VideoConferencingApplicable { get; set; }
        public string VideoConferencingProviderName { get; set; }
    }
}