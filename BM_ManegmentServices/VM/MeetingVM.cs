﻿using BM_ManegmentServices.VM.Compliance;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{
    public class Meeting_NewVM : IMessage
    {
        public long MeetingID { get; set; }
        public int? MeetingSrNo { get; set; }
        [Required(ErrorMessage = "Please Select Type")]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Type")]
        public int MeetingTypeId { get; set; }
        [Required(ErrorMessage = "Please Select Financial Year")]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Financial Year")]
        public long FYID { get; set; }
        public string Quarter_ { get; set; }
        public bool IsShorter { get; set; }
        public string Type { get; set; }
        public bool? IsAdjourned { get; set; }

        public int CustomerId { get; set; }
        [Required(ErrorMessage = "Please Select Entity")]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Entity")]
        public int Entityt_Id { get; set; }
        public int UserId { get; set; }

        //use in Adjourned meeting
        public DateTime? MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingAddressType { get; set; }
        public string MeetingVenue { get; set; }
        public bool? IsVirtualMeeting { get; set; }
        public bool IsEVoting { get; set; }

        public bool IsVideoconfrence { get; set; }
        public bool isVideomeetingCusto { get; set; }
    }

    public class MeetingFilter
    {
        public string Stage { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
    }

    public class CompletedMeetingsVM
    {
        public long MeetingID { get; set; }
        public string MeetingSrNo { get; set; }
        public DateTime? MeetingDate { get; set; }

        public string MeetingCircular { get; set; }
        public int? Day_ { get; set; }
        public string DName { get; set; }
        public string Month_ { get; set; }
        public string MeetingTime { get; set; }
        public long FY { get; set; }
        public string FYText { get; set; }
        public int MeetingTypeId { get; set; }
        public string MeetingTypeName { get; set; }
        public int? Customer_Id { get; set; }
        public int? SerialNo { get; set; }
        public string Quarter_ { get; set; }
    }
    public class MeetingVM : IMessage
    {
        public long MeetingID { get; set; }

        public string Stage { get; set; }
        public int? MeetingSrNo { get; set; }
        [Required]
        public int MeetingTypeId { get; set; }
        public int FYID { get; set; }
        public string FY_CY { get; set; }
        public string MeetingTypeName { get; set; }
        public string Quarter_ { get; set; }
        public bool IsShorter { get; set; }
        //[Required]
        public string MeetingTitle { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingAddressType { get; set; }
        public string MeetingVenue { get; set; }
        public string Type { get; set; }

        public string TypeName { get; set; }
        public bool? IsSeekAvailability { get; set; }
        public bool IsVideoMeeting { get; set; }
        public bool isVideomeetingCusto { get; set; }
        public string VMeetingId { get; set; }
        public bool? IsMeetingCompleted { get; set; }
        public bool IsComplianceClosed { get; set; }
        public bool? IsAdjourned { get; set; }

        public string Availability_ShortDesc { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SeekAvailability_DueDate { get; set; }
        public bool? IsSeekAvailabilitySent { get; set; }
        public bool? CanReSendSeekAvailability { get; set; }
        public bool? CanMarkSeekAvailability { get; set; }
        public bool? IsNoticeSent { get; set; }
        public bool? IsAgendaSent { get; set; }
        public bool? IsPostponded { get; set; }
        public bool CanPostpond { get; set; }

        public bool? IsCirculate { get; set; }
        public bool? IsFinalized { get; set; }
        public long? DraftCirculationID { get; set; }
        public bool? CanStart { get; set; }

        public DateTime? SendDateAvailability { get; set; }
        public DateTime? SendDateNotice { get; set; }
        public DateTime? SendDateAgenda { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int CustomerId { get; set; }
        [Required]
        public int Entityt_Id { get; set; }
        public int EntityTypeId_ { get; set; }
        public int UserId { get; set; }

        public string EntityName { get; set; }
        public string EntityAddressLine1 { get; set; }
        public string EntityAddressLine2 { get; set; }

        public long ParticipantId { get; set; }
        public string ParticipantType { get; set; }
        public DateTime? StartMeetingDate { get; set; }
        public string StartMeetingTime { get; set; }
        public DateTime? EndMeetingDate { get; set; }
        public string EndMeetingTime { get; set; }
        public DateTime? circularDuedate { get; set; }
        public MeetingAvailabilityMailVM AvailabilityMailFormat { get; set; }
        public MeetingNoticeMailVM NoticeMailFormat { get; set; }
        public MeetingInviteeMailVM InviteeMailFormat { get; set; }
        public CircularMeeting CircularMeetingDetails { get; set; }
        public Agenda_SummitResponse Agenda_SummitResponseDetails { get; set; }
        public FinalizeResolutionVM FinalizeResolutionDetails { get; set; }
        public List<Aviability> MeetingAviability { get; set; }
        public MeetingNotesVM NotesFormat { get; set; }

        public bool IsMeetingToday { get; set; }
        public bool? IsMeetingStarted { get; set; }

        public bool? IsVirtualMeeting { get; set; }
        public bool IsEVoting { get; set; }

        public MeetingAttendance_VM AttendanceNew { get; set; }
        public MeetingMinutesDetailsVM MinutesDetailsNew { get; set; }
        public bool IsConfigurationMasterSaved { get; set; }
        public string NoticeType { get; set; }

        public DateTime? MeetingDateTime { get; set; }

        public AgendaMinutesTaskVM AgendaTaskReview { get; set; }
        public bool IsRecording { get; set; }
    }
    public class Aviability
    {

        public DateTime SeekAviabiltyDate { get; set; }
        public long MeetingId { get; set; }
        public long ParticipantId { get; set; }
        public long meetingavaiId { get; set; }
        public string seekavadate { get; set; }
        public string seekavaTime { get; set; }
        public bool Isavialale { get; set; }
        public bool notAvialabel { get; set; }
        public bool? AviabilityResponses { get; set; }
        public long ResponseAviabilityId { get; set; }
    }
    public class CircularMeeting : IMessage
    {
        public long CircularMeetingId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please enter Circular Date")]
        public DateTime? CircularDate { get; set; }
        public string Title { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please enter Circular Due Date")]
        public DateTime? DueDate { get; set; }
        public string CircularDueTime { get; set; }
        public int EntityId { get; set; }
        public int MeetingTypeId { get; set; }
        public int? CircularNumber { get; set; }
    }
    public class FinalizeResolutionVM : IMessage
    {
        public long ResolutionFinalizeId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please enter Date")]
        public DateTime? ResolutionFinalizeDate { get; set; }
        public bool? IsResolutionFinalized { get; set; }
        public bool? IsVirtualResolution { get; set; }
    }
    public class MeetingAvailabilityVM : IMessage
    {
        public long MeetingAvailabilityId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime AvailabilityDate { get; set; }
        public string AvailabilityTime { get; set; }

        public string AvailabilityToTime { get; set; }
        public string AvailabilityAddressType { get; set; }
        public string AvailabilityVenue { get; set; }

        [Required(ErrorMessage = "Please Select Preference")]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Preference")]
        public int? PreferenceId { get; set; }

        public long MeetingID { get; set; }
        public int UserId { get; set; }

        public Boolean ShowCtrls { get; set; }

        public bool? IsSeekAvailabilitySent { get; set; }
        public bool? CanReSendSeekAvailability_ { get; set; }
        public bool? CanMarkSeekAvailability_ { get; set; }
        public bool? RefreshMailFormat { get; set; }

        public int AvailabilityEntityt_Id { get; set; }

        public int Participant_UserId { get; set; }
        public Boolean Participant_View { get; set; }
    }
    public class MeetingAvailabilityMailVM : IMessage
    {
        public long AvailabilityMailID { get; set; }

        //public DateTime? SeekAvailabilityDueDate { get; set; }
        [AllowHtml]
        public string AvailabilityMail { get; set; }
        public bool? IsSeekAvailabilitySent { get; set; }

        public bool? RefreshAvailability { get; set; }

        public int UserId { get; set; }
    }
    public class MeetingNoticeMailVM : IMessage
    {
        public long NoticeMailID { get; set; }
        public int MeetingType_Id { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? NoticeSendDate { get; set; }
        public string NoticeSendTime { get; set; }
        public string TemplateType { get; set; }
        [AllowHtml]
        public string NoticeMail { get; set; }
        [AllowHtml]
        public string NoticeMailParticipants { get; set; }
        public bool? IsNoticeSent { get; set; }
        public bool? IsQuarter_ { get; set; }
        public bool? IsShorter_ { get; set; }
        public bool? IsVirtualMeeting_ { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public string Type { get; set; }
        public bool IsSummit { get; set; }
        public bool FillPostpondedOptions { get; set; }

    }
    public class MeetingNotesVM : IMessage
    {
        public long NotesID { get; set; }
        [AllowHtml]
        public string Notes { get; set; }
    }

    public class MeetingNoticeLogResultVM
    {
        public long MeetingID { get; set; }
        public long MeetingNoticeLogID { get; set; }
        public string ParticipantName { get; set; }
        public string LogType { get; set; }
        public string LogTypeStr { get; set; }
        public DateTime? SendOn { get; set; }
        public DateTime? DeliveredOn { get; set; }
        public DateTime? ReadOn { get; set; }
    }

    public class MeetingInviteeMailVM : IMessage
    {
        public long InviteeMailID { get; set; }
        [AllowHtml]
        public string InviteeMail { get; set; }
        public bool? IsInviteeMailSent { get; set; }
    }

    public class MeetingParticipantVM : IMessage
    {
        public long MeetingParticipantId { get; set; }

        public long Meeting_ID { get; set; }
        public long? Director_Id { get; set; }
        public string Director_Name { get; set; }
        public int UserId { get; set; }
    }

    public class MeetingInviteeVM : IMessage
    {
        public long MeetingParticipantId { get; set; }
        public long Meeting_ID { get; set; }
        public string ParticipantName { get; set; }
        public string ParticipantEmail { get; set; }
        public List<MeetingInviteeAgendaItemVM> lstAgendaItems { get; set; }
    }

    public class MeetingInviteeAgendaItemVM
    {
        public long MeetingAgendaMappingId { get; set; }
        public long BM_AgendaMasterId { get; set; }
        public string AgendaHeading { get; set; }
        public string Agenda { get; set; }
        public int? PartID { get; set; }
        public bool IsCheked { get; set; }
        public long Meeting_Id { get; set; }

        public int? FrequencyID { get; set; }

        public long? RefPendingMappingID { get; set; }
        public string RefMeeting { get; set; }
        public string RefResult { get; set; }

        public bool? IsNewStage { get; set; }
        public long? StartAgendaId { get; set; }
        public long? StartMeetingId { get; set; }
        public int? SequenceNo { get; set; }
        public bool? HasCompliance { get; set; }
        public bool? HasInfo { get; set; }

    }

    public partial class MeetingParticipants_ResultVM
    {
        public long MeetingID { get; set; }
        public long MeetingParticipantId { get; set; }
        public long? Director_Id { get; set; }
        public long? UserId { get; set; }
        public string ParticipantName { get; set; }
        public string Email { get; set; }
    }

    public class OtherScheduledMeetings_ResultVM
    {
        public bool IsOtherMeetingScheduled { get; set; }
        public List<OtherScheduledMeetingList_ResultVM> lstMeetingList { get; set; }
    }
    public partial class OtherScheduledMeetingList_ResultVM
    {
        public long? MeetingID { get; set; }
        public long? Director_Id { get; set; }
        public int? EntityId { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingVenue { get; set; }
        public string MeetingTitle { get; set; }
        public string ParticipantName { get; set; }
        public string CompanyName { get; set; }
    }

    public class MeetingAvailabilityResponseVM : IMessage
    {
        public long MeetingAvailabilityId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime AvailabilityDate { get; set; }
        public string AvailabilityTime { get; set; }
        public string AvailabilityVenue { get; set; }

        public int? PreferenceId { get; set; }

        public long MeetingID { get; set; }
        public int UserId { get; set; }

        public bool? Response_ { get; set; }

        public int AvailabilityEntityt_Id { get; set; }
    }

    public class MarkSeekAvailabilityVM
    {
        public long MeetingID { get; set; }
        public string ViewType { get; set; }
        public bool? CanMarkAvailability_ { get; set; }
    }

    public partial class AvailabilityResponse_ResultVM
    {
        public long MeetingID { get; set; }
        public long MeetingAvailabilityId { get; set; }
        public long AvailabilityResponseID { get; set; }
        public long MeetingParticipantId { get; set; }
        public long? Director_Id { get; set; }
        public long? UserId { get; set; }
        public string ParticipantName { get; set; }
        public string Email { get; set; }
        public string AvailabilityDate { get; set; }
        public string AvailabilityTime { get; set; }
        public string AvailabilityVenue { get; set; }
        public int? PreferenceId { get; set; }
        public bool? Response { get; set; }
    }

    public partial class AvailabilityResponseChartData_ResultVM
    {
        public string Category { get; set; }
        public string Color { get; set; }
        public int Response_Count { get; set; }
        public List<AvailabilityResponseByAvailabilityId_ResultVM> lstParticiapants { get; set; }
        public long MappingId { get; set; }
        public long MeetingId { get; set; }
        public long MeetingAvailabilityId { get; set; }
    }

    public partial class AvailabilityResponseByAvailabilityId_ResultVM
    {
        public long AvailabilityResponseID { get; set; }
        public long MeetingParticipantId { get; set; }
        public long MeetingID { get; set; }
        public long MeetingAvailabilityId { get; set; }
        public long? Director_Id { get; set; }
        public long? UserId { get; set; }
        public string ParticipantName { get; set; }
        public string Email { get; set; }
        public bool? Response { get; set; }
        public string Responses { get; set; }
        public string Category { get; set; }
    }

    public class MeetingAgendaMappingVM : IMessage
    {
        public long MeetingAgendaMappingID { get; set; }
        public int SrNo { get; set; }

        public long? UIFormID { get; set; }

        public string AgendaItemHeading { get; set; }
        [AllowHtml]
        public string AgendaItemText { get; set; }
        [AllowHtml]
        public string AgendaFormat { get; set; }
        public string ResolutionFormatHeading { get; set; }
        [AllowHtml]
        public string ResolutionFormat { get; set; }
        public string MinutesFormatHeading { get; set; }
        [AllowHtml]
        public string MinutesFormat { get; set; }
        [AllowHtml]
        public string MinutesDisApproveFormat { get; set; }
        [AllowHtml]
        public string MinutesDifferFormat { get; set; }

        [AllowHtml]
        public string SEBI_IntimationFormat { get; set; }
        [AllowHtml]
        public string SEBI_DisclosureFormat { get; set; }

        [AllowHtml]
        public string ExplanatoryStatement { get; set; }

        public int? MeetingTypeId_ { get; set; }
        public int? EntityTypeId_ { get; set; }

        public long Meeting_Id { get; set; }
        public long AgendaID { get; set; }

        public int? PartId { get; set; }
        public bool? HasTemplate { get; set; }
        public bool? IsFillTemplateFields { get; set; }
        public bool? HasCompliance { get; set; }
        public bool? HasInfo { get; set; }
        public bool? Hasfoms { get; set; }
        public bool? MayCompliancePenalty { get; set; }

        public bool IsOrdinaryBusiness { get; set; }
        public bool IsSpecialResolution { get; set; }

    }

    public class MeetingAgendaMappingMOMVM : IMessage
    {
        public long MeetingAgendaMappingID { get; set; }

        public string ResolutionFormatHeading { get; set; }
        [AllowHtml]
        public string ResolutionFormat { get; set; }
        [Required(ErrorMessage = "Please enter Minutes Heading")]
        public string MinutesFormatHeading { get; set; }
        [AllowHtml]
        public string MinutesFormat { get; set; }

        public long Meeting_Id { get; set; }
        public long? AgendaID { get; set; }
        public string Result { get; set; }
        public string ResultRemark { get; set; }
    }

    #region Preview Agenda
    public class PreviewAgendaVM
    {
        public long MeetingID { get; set; }
        public int userID { get; set; }
        public bool? IsVirtual { get; set; }
        public bool IsEVoting { get; set; }
        public bool? GenerateMinutes { get; set; }
        public int? MeetingSrNo { get; set; }
        public int MeetingTypeId { get; set; }
        public string MeetingTypeName { get; set; }
        public string MeetingCircular { get; set; }
        public string Quarter_ { get; set; }
        public string MeetingTitle { get; set; }
        public DateTime? NoticeDate { get; set; }
        public DateTime? MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public DateTime? CircularDate { get; set; }
        public string MeetingAddressType { get; set; }
        public string MeetingVenue { get; set; }

        public DateTime? MeetingStartDate { get; set; }
        public string MeetingStartTime { get; set; }
        public DateTime? MeetingEndDate { get; set; }
        public string MeetingEndTime { get; set; }

        public bool IsAdjourned { get; set; }
        public int Entityt_Id { get; set; }
        public string EntityCIN_LLPIN { get; set; }
        public string EntityName { get; set; }
        public string EntityAddressLine1 { get; set; }
        public string EntityAddressLine2 { get; set; }
        public string EntityCity { get; set; }
        public string EntityState { get; set; }
        public string FYText { get; set; }
        [AllowHtml]
        public string GM_Notes { get; set; }
        public int? TotalMemberPresentInAGM { get; set; }
        public int? TotalProxyPresentInAGM { get; set; }
        public int? TotalMemberInAGM { get; set; }

        public int TotalValidProxy { get; set; }
        public int TotalMemberProxy { get; set; }
        public int TotalShareHeldByProxy { get; set; }
        public int FaceValueOfEnquityShare { get; set; }
        public decimal? PercentageOfIssuedCapital { get; set; }
        public bool HasConsolidatedFinancial { get; set; }
        public List<MeetingAgendaMappingVM> lstAgendaItems { get; set; }

        public List<MeetingAttendance_VM> lstMeetingAttendance { get; set; }
        public List<MeetingAttendance_VM> lstOtherParticipantAttendance { get; set; }

        public MeetingMinutesDetailsVM meetingMinutesDetails { get; set; }
        public MeetingSigningAuthorityForAGM_VM MeetingSigningAuthorityForAGM { get; set; }


        public long ParticipantID_ { get; set; }
        public long DraftCirculationID_ { get; set; }
        public bool IsDraftMinutesApproved { get; set; }
    }
    public class MeetingSigningAuthorityForAGM_VM
    {
        public string AuthorityName { get; set; }
        public string Designation { get; set; }
        public string DIN_PAN { get; set; }
        public string MembershipNo { get; set; }
        public bool? IsCS { get; set; }
        public bool? IsChairman { get; set; }
        public bool? IsAuthorisedSignotory { get; set; }
    }
    #endregion

    #region Pre Committee Agenda
    public class PreCommiitteeDraftMeetingVM
    {
        public long MeetingID { get; set; }
        public string MeetingTitle { get; set; }
    }
    public class MeetingPendingPreCommitteeVM
    {
        public long MeetingTypeID { get; set; }
        public string MeetingTypeName { get; set; }
        public long SourceMeetingID { get; set; }
        public long SourceMeetingAgendaMappingID { get; set; }
        public long? PostAgendaID { get; set; }
        public long TargetMeetingID { get; set; }
        public long TargetMeetingAgendaMappingID { get; set; }
        public int? EntityID { get; set; }

        public List<MeetingPreCommitteeAgendaItemVM> lstPreCommiteeAgenda { get; set; }
    }

    public class MeetingPreCommitteeAgendaItemVM
    {
        public long PreCommitteeAgendaID { get; set; }
        public string PreCommitteeAgendaItem { get; set; }
    }
    #endregion

    public class MeetingAgendaTemplateVM : IMessage
    {
        public List<MeetingAgendaTemplateListVM> lstControls { get; set; }
        public long MeetingAgendaMappingID { get; set; }
        public long Meeting_Id { get; set; }
        public long? AgendaID { get; set; }

        public int? EntityID_Ref { get; set; }
    }

    public class MeetingAgendaTemplateListVM : IMessage
    {
        public long TemplateListID { get; set; }
        public long MeetingAgendaMapping_ID { get; set; }
        public long TemplateID { get; set; }

        public string TemplateLabel { get; set; }
        public string TemplateName { get; set; }
        public string TemplateValue { get; set; }

        public int? CategoryID { get; set; }
    }
    public class Agenda_SummitResponse
    {
        public long Id { get; set; }
        public long? CircularResponseId { get; set; }
        public long MeetingId { get; set; }
        public string MeetingName { get; set; }
        public string Degignation { get; set; }
        public string DueDate { get; set; }
        public long? AgendaId { get; set; }
        public long logginUser { get; set; }
        public int? RoleId { get; set; }
        public long MeetingParticipantId { get; set; }
        [AllowHtml]
        public string AgendaName { get; set; }
        public string ViewType { get; set; }
        public bool CanMarkAgenda { get; set; }
        public string ParticipantName { get; set; }
        public int AvailabilityResponseID { get; set; }
        public int MeetingAvailabilityId { get; set; }
        public int PreferenceId { get; set; }
        public string Response { get; set; }
        public string Castingvote { get; set; }
        //[Required(ErrorMessage = "Please Enter Remark")]
        public string Remark { get; set; }
        public long MeetingAgendaMappingId { get; set; }

        public CircularResponseResult CircularResponseResultVM { get; set; }
    }
    public class CircularResponseResult
    {
        public long MeetingID { get; set; }
        public long? CircularResponseId { get; set; }

    }
    public class CircularAgendaData
    {
        public long MeetingAgendaMappingId { get; set; }
        public long? AgendaId { get; set; }
        public string AgendaName { get; set; }
        public long MeetingId { get; set; }
        public DateTime DueDate { get; set; }
        public CircularAgendaResult CircularAgendaResultVM { get; set; }
    }
    public class CircularAgendaResult
    {
        public int Approved_count { get; set; }
        public int DisApprove_count { get; set; }
        public int Abstrain_count { get; set; }
        public int Differed_count { get; set; }
        public int Participant_Count { get; set; }
        public string Result { get; set; }
        public string Remark { get; set; }
    }

    public class Meeting_AdjournVM : IMessage
    {
        public long MeetingAdjourn_ID { get; set; }
        public int? MeetingSrNo { get; set; }
        public bool? IsAdjourned { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDate_Adjourn { get; set; }
        public string MeetingTime_Adjourn { get; set; }
        public string MeetingVenue_Adjourn { get; set; }
        public string ReasonOfAdjourn { get; set; }

        public string MinutesDetails { get; set; }
        public bool? CanAdjourn { get; set; }

        public bool? CanCancel { get; set; }
        public string AdjournedTime { get; set; }
    }

    public class MeetingMinutesDetailsVM : IMessage
    {
        public long MeetingMinutesDetailsId { get; set; }
        public long MOM_MeetingId { get; set; }
        public string Place { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfEntering { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfSigning { get; set; }
        public bool IsCirculate { get; set; }
        public bool IsFinalized { get; set; }
        public long? ConfirmMeetingId { get; set; }
        public bool HasAgendaVotingNotingWarning { get; set; }
    }

    #region Meeting Compliances
    public class MeetingComplianceVM : AgendaComplianceVM
    {
        public long MeetingComplianceID { get; set; }
        public long Meeting_ID { get; set; }
    }
    #endregion

    #region Generate schedule on for Meeting Compliances
    public class ComplianceScheduleOnVM //: AgendaComplianceVM
    {
        public long ScheduleOnID { get; set; }
        public long? Meeting_ID { get; set; }
        public long MeetingAgendaMappingId { get; set; }
        public string MappingType { get; set; }
        public long? MCM_ID { get; set; }
        public string DaysOrHours { get; set; }
        public long ComplianceId { get; set; }
        public long? AgendaMasterId { get; set; }
        public DateTime? ScheduleOn { get; set; }
        public int? EntityId { get; set; }
        public long DirectorId { get; set; }
        public int Customer_Id { get; set; }

        public string BeforeAfter { get; set; }
        public decimal? Numbers { get; set; }
        public string AutoCompleteOn { get; set; }
    }

    public class ComplianceScheduleDetailsVM : ComplianceScheduleOnVM
    {
        public ActVM Act { get; set; }
        public ComplianceDetailsVM Compliance { get; set; }
        public List<FileDataDocumentVM> ComplianceDocumentList { get; set; }
        public List<ComplianceTransactionLogVM> TransactionLog { get; set; }
        public int? RoleID { get; set; }

        public string MeetingTitle { get; set; }
        public string AgendaItem { get; set; }
        public string DirectorName { get; set; }
        public bool Is_ICSIMODE { get; set; }
        public long? ComplianceInstanceId { get; set; }

        public DateTime ComplianceDueDate { get; set; }
        public bool IsPerformerReviewerSameUser { get; set; }
        public bool IsCheckList { get; set; }
    }

    public partial class MeetingComplianceScheduleDetails_ResultVM
    {
        public long AssignmentID { get; set; }
        public long MeetingID { get; set; }
        public long MeetingAgendaMappingID { get; set; }
        public long ComplianceID { get; set; }
        public string MappingType { get; set; }
        public int? PartId { get; set; }
        public int? SrNo { get; set; }
        public long ScheduleOnID { get; set; }
        public DateTime? ScheduleOn { get; set; }
        public string ScheduleOnTime { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string ClosedDateTime { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
        public string AgendaItemHeading { get; set; }
        public string ShortDescription { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string Role { get; set; }
        public string UserName { get; set; }

        [UIHint("User")]
        public PerformerReviewerViewModel User { get; set; }

        public int? Entity_ID { get; set; }

        public long? HasForms { get; set; }

        public bool? HasFormMapped { get; set; }
        public bool IsCalendarEvent { get; set; }
        public bool AllowDelete { get; set; }

    }

    public class ComplianceScheduleAssignmentVM
    {
        public long AssignmentID { get; set; }
        public long? ScheduleOnID { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public bool IsDeleted { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
    #endregion

    public class VM_AgendaDocument : IMessage
    {
        public long ID { get; set; }
        public long AgendaDocumentMeetingId { get; set; }
        public long MeetingAggendaMappingId { get; set; }
        public long FileUpload_MeetingId { get; set; }
        public string MeetingDocs { get; set; }
        public string FileName { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }
    }

    #region Meeting Agenda Interested Party
    public class MeetingAgendaInterestedPartyVM
    {
        public long InterestedPartyID { get; set; }
        public long MeetingAgendaMappingID { get; set; }
        public long Director_Id { get; set; }
        public bool IsDeleted { get; set; }

        [UIHint("InterestedParty")]
        public DirectorMasterListVM Category
        {
            get;
            set;
        }
    }
    #endregion

    #region Comments
    public class MeetingAgendaCommentVM : IMessage
    {
        public int UserId { get; set; }
        public long MeetingId { get; set; }
        public long ParticipantId { get; set; }
        public string CommentData { get; set; }
    }

    public class MeetingAgendaCommentListVM
    {
        public string Salutaion { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long MeetingId { get; set; }
        public long ParticipantId { get; set; }
        public string CommentData { get; set; }
    }

    public class MeetingMinutesCommentVM : IMessage
    {
        public long UserId { get; set; }
        public long MeetingId { get; set; }
        public long DraftCirculationID { get; set; }
        public string CommentData { get; set; }

        public List<MeetingMinutesCommentDetailsVM> lstData { get; set; }
    }

    public class MeetingMinutesCommentListVM
    {
        public string Salutaion { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long MeetingId { get; set; }
        public long DraftCirculationID { get; set; }
        public string CommentData { get; set; }
    }

    public class MeetingMinutesApproveVM : IMessage
    {
        public int userId { get; set; }
        public long MeetingId { get; set; }
        public long DraftCirculationID { get; set; }
        public string Comment { get; set; }
        public HttpPostedFileBase Files { get; set; }
    }

    public class MeetingMinutesCommentDetailsVM
    {
        public string style { get; set; }
        public string order { get; set; }
        public string title { get; set; }
        public string text { get; set; }
        public string uniqueName { get; set; }
    }
    #endregion

    #region Delete Agenda, Meeting VM
    public class DeleteAgendaVM : IMessage
    {
        public long MeetingId_del { get; set; }
        public long MeetingAgendaMappingId_del { get; set; }
        public bool? AllowDelete { get; set; }
        public string MessageDetails { get; set; }
        public string AgendaItem_ { get; set; }
    }

    public class DeleteMeetingVM : IMessage
    {
        public long DeleteMeetingId { get; set; }
        public bool? AllowDeleteMeeting { get; set; }
        public string MessageDetails { get; set; }
        public List<DeleteAgendaVM> lstAgendaItems_ { get; set; }
    }
    #endregion

    public class MeetingDetailsVM : IMessage
    {
        public long MeetingsDetailsId { get; set; }
        public long MeetingsDetails_MeetingId { get; set; }
        public int MeetingsDetails_EntityId { get; set; }
        public long? SigningAuthorityId { get; set; }
        public string SigningAuthorityDesignation { get; set; }
    }

    public class VMeetingVM : IMessage
    {
        public long MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDate { get; set; }

        public string MeetingDate1 { get; set; }
        public string MeetingTime { get; set; }
        public string EntityName { get; set; }

        public string Venue { get; set; }

        public string VMeetingId { get; set; }
    }

    public class VideoMeetingVM
    {
        public string Id { get; set; }
        public Nullable<long> MeetingID { get; set; }
        public string V_MeetingId { get; set; }
        public Nullable<int> MType { get; set; }
        public string Password { get; set; }
        public string encrypted_password { get; set; }
        public string Start_Url { get; set; }
        public string Join_Url { get; set; }
        public string host_video { get; set; }
        public string participant_video { get; set; }
        public string join_before_host { get; set; }
        public string mute_upon_entry { get; set; }
        public Nullable<int> approval_type { get; set; }
        public string audio { get; set; }
        public string auto_recording { get; set; }
        public string enforce_login { get; set; }
        public string close_registration { get; set; }
        public string allow_multiple_devices { get; set; }
        public string registrants_confirmation_email { get; set; }
        public string waiting_room { get; set; }
        public string request_permission_to_unmute_participants { get; set; }
        public string registrants_email_notification { get; set; }
        public string meeting_authentication { get; set; }
        public string Status { get; set; }
        public Nullable<int> CrBy { get; set; }
        public Nullable<System.DateTime> CrDate { get; set; }
        public Nullable<int> ModBy { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }

    }

    #region Webex Cisco Meeting
    public class Link
    {
        public string rel { get; set; }
        public string href { get; set; }
        public string method { get; set; }
    }

    public class Telephony
    {
        public string accessCode { get; set; }
        public List<object> callInNumbers { get; set; }
        public List<Link> links { get; set; }
    }

    public class CiscoWebexVM
    {
        public string id { get; set; }
        public string meetingNumber { get; set; }
        public string title { get; set; }
        public string agenda { get; set; }
        public string password { get; set; }
        public string phoneAndVideoSystemPassword { get; set; }
        public string meetingType { get; set; }
        public string state { get; set; }
        public string timezone { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string recurrence { get; set; }
        public string hostUserId { get; set; }
        public string hostDisplayName { get; set; }
        public string hostEmail { get; set; }
        public string hostKey { get; set; }
        public string siteUrl { get; set; }
        public string webLink { get; set; }
        public string sipAddress { get; set; }
        public string dialInIpAddress { get; set; }
        public bool enabledAutoRecordMeeting { get; set; }
        public bool allowAnyUserToBeCoHost { get; set; }
        public Telephony telephony { get; set; }
    }

    public class CiscoTokenVM
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public int refresh_token_expires_in { get; set; }
    }
    #endregion


    #region Cisco webex Recording
    public class TemporaryDirectDownloadLinks
    {
        public string recordingDownloadLink { get; set; }
        public string audioDownloadLink { get; set; }
        public DateTime expiration { get; set; }
    }

    public class CiscowebexRecVM
    {
        public string id { get; set; }
        public string meetingId { get; set; }
        public string scheduledMeetingId { get; set; }
        public string meetingSeriesId { get; set; }
        public string topic { get; set; }
        public DateTime createTime { get; set; }
        public string siteUrl { get; set; }
        public string downloadUrl { get; set; }
        public string playbackUrl { get; set; }
        public string password { get; set; }
        public TemporaryDirectDownloadLinks temporaryDirectDownloadLinks { get; set; }
        public string format { get; set; }
        public int durationSeconds { get; set; }
        public int sizeBytes { get; set; }
        public bool shareToMe { get; set; }
    }

    public class RecordingFile
    {
        public string id { get; set; }
        public string meeting_id { get; set; }
        public DateTime recording_start { get; set; }
        public DateTime recording_end { get; set; }
        public string file_type { get; set; }
        public string file_extension { get; set; }
        public int file_size { get; set; }
        public string play_url { get; set; }
        public string download_url { get; set; }
        public string status { get; set; }
        public string recording_type { get; set; }
    }

    public class RecordingDetlsVM
    {
        public string id { get; set; }
        public string meetingId { get; set; }
        public string scheduledMeetingId { get; set; }
        public string meetingSeriesId { get; set; }
        public string topic { get; set; }
        public DateTime createTime { get; set; }
        public string siteUrl { get; set; }
        public string downloadUrl { get; set; }
        public string playbackUrl { get; set; }
        public string password { get; set; }
        public string format { get; set; }
        public int durationSeconds { get; set; }
        public int sizeBytes { get; set; }
        public bool shareToMe { get; set; }
    }

    public class CiscoRecordingDetailsVM
    {
        public List<RecordingDetlsVM> items { get; set; }
    }
    public class ZoomRecordingDetailsVM
    {
        public string uuid { get; set; }
        public long id { get; set; }
        public string account_id { get; set; }
        public string host_id { get; set; }
        public string topic { get; set; }
        public int type { get; set; }
        public DateTime start_time { get; set; }
        public string timezone { get; set; }
        public string host_email { get; set; }
        public int duration { get; set; }
        public int total_size { get; set; }
        public int recording_count { get; set; }
        public string share_url { get; set; }
        public List<RecordingFile> recording_files { get; set; }
    }
    #endregion

    public class AgendaReviewVM : IMessage
    {
        public long MeetingAgendaMappingID { get; set; }
        public long Meeting_Id { get; set; }
        public long AgendaID { get; set; }

        public long AgendaReviewId { get; set; }
        public long? TaskId { get; set; }
        public long? TaskAssignmentId { get; set; }

        public string AgendaFormatHeading { get; set; }
        [AllowHtml]
        public string AgendaFormat { get; set; }
        [AllowHtml]
        public string ResolutionFormat { get; set; }
        public string MinutesFormatHeading { get; set; }
        [AllowHtml]
        public string MinutesApproveFormat { get; set; }
        [AllowHtml]
        public string MinutesDisApproveFormat { get; set; }
        [AllowHtml]
        public string MinutesDeferFormat { get; set; }
    }

    public class AgendaAndDocumentReviewVM : IMessage
    {
        public long? AgendaReviewId { get; set; }
        public long? ID { get; set; }
        public long? ParentID { get; set; }
        public long Meeting_Id { get; set; }
        public long? TaskId { get; set; }
        public long? TaskAssignmentId { get; set; }
        [AllowHtml]
        public string AgendaOrFileName { get; set; }
        
        public string Category { get; set; }
        public int? ItemNo { get; set; }
        public bool? HasParent { get; set; }
        public bool? hasChildren { get; set; }
        public bool HasChanges { get; set; }
        public bool HasComment { get; set; }
    }
    public class zoomtokenVM
    {
        public string token { get; set; }
        public DateTime? ExpiryDate { get; set; }
		public string ClientId { get; set; }
        public string ClientSecret { get; set; }
		public string refreshToken { get; set; }
        public DateTime? expiryDateofRefreshToken { get; set; }
    }

    public class AgendaVotingNotingMeetingDetailsVM
    {
        public long Meeting_Id { get; set; }
        public int MeetingType_Id { get; set; }
    }

    public class MinutesViewerVM
    {
        public long MeetingMinutesId { get; set; }
        public int MeetingTypeId { get; set; }
        public int MOMEntityTypeId { get; set; }
        public MeetingMinutesDetailsVM MeetingMinutesDetails { get; set; }
    }

    public class CopyMeetingSourceMeetingVM
    {
        public long MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public long EntityId { get; set; }
        public string EntityName { get; set; }
        public int EntityTypeId { get; set; }
        public DateTime? MeetingDate { get; set; }
        public string MeetingTime { get; set; }
    }

    public class CopyMeetingVM : IMessage
    {
        public long SourceMeetingID { get; set; }
        public int SourceMeetingEntityID { get; set; }
        public int EntityTypeId_ { get; set; }
        public string MeetingTitleTemp { get; set; }
        public List<int> lstEntityId { get; set; }
    }

    public class MeetingAgendaListForCopyMeeting_ResultVM
    {
        public int? PartId { get; set; }
        public int SrNo { get; set; }
        public int? ItemNo { get; set; }
        public long MeetingAgendaMappingID { get; set; }
        public long? AgendaID { get; set; }
        public string AgendaItemText { get; set; }
        public bool IsCheked { get; set; }
        public long? RefMasterId { get; set; }
    }
    public class CopyMeetingCheckComplianceAssignedVM : IMessage
    {
        public long SourceMeetingID { get; set; }
        public string MeetingDateStr { get; set; }
        public string MeetingTimeStr { get; set; }
        public List<int> lstEntityId { get; set; }
        public List<MeetingAgendaListForCopyMeeting_ResultVM> lstAgendaItems { get; set; }
    }

    public class CopyMeetingComplianceAssignementPendingVM
    {
        public string EntityName { get; set; }
        public List<MeetingAgendaListForCopyMeeting_ResultVM> lstAgendaItems { get; set; }
    }
    public class CopyMeetingMessageVM : IMessage
    {
        public List<CopyMeetingComplianceAssignementPendingVM> lstPendingAssignements { get; set; }
    }

    public class CopyMeetingLogVM
    {
        public long TargetMeetingId { get; set; }
        public string TargetMeetingTitle { get; set; }
        public string TargetMeetingEntityName { get; set; }
        public DateTime? Dated { get; set; }
    }
}