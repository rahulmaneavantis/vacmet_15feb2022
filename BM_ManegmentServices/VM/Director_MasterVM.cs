﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BM_ManegmentServices.VM
{

    public class BM_Rule
    {
        public int Entity_Id { get; set; }
        public int Entity_Type { get; set; }
        public string AmountInvariable { get; set; }
        public long? AmountIn { get; set; }
        public decimal? PaidupShareCapital { get; set; }
        public decimal? PaidupShareCapital_rule { get; set; }
        [RegularExpression(@"(^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$)", ErrorMessage = "Please enter valid Turnover")]
        public decimal? Turnover { get; set; }
        public decimal? NetWorth { get; set; }
        public decimal? NetProfit { get; set; }
        public bool BorrowingApplicable { get; set; }

        public decimal? Turnover_rule { get; set; }
        public decimal? NetWorth_rule { get; set; }
       public decimal? NetProfit_rule { get; set; }
        public decimal? Borrowings_rule { get; set; }
        public decimal? ShareHolders_rule { get; set; }
        [RegularExpression(@"(^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$)", ErrorMessage = "Please enter valid Borrowings")]
        public decimal? Borrowings { get; set; }
        public bool DepositApplicable { get; set; }
        [RegularExpression(@"(^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$)", ErrorMessage = "Please enter valid Deposit")]
        public decimal? Deposit { get; set; }
        public decimal? Deposit_rule { get; set; }

        [RegularExpression(@"(^[+]?([0-9]+)$)", ErrorMessage = "Please enter valid ShareHolders")]
        public long? ShareHolders { get; set; }

        public bool SMEListed { get; set; }
        public List<Rules> Rules { get; set; }

        public Response Messages { get; set; }
    }
    public class Rules
    {
        public long? SrNo { get; set; }
        public int Id { get; set; }
        public int ApplicabilityId { get; set; }
        public string Name { get; set; }
        [Required(ErrorMessage = "Please select")]
        public bool Answer { get; set; }
        public bool Editable { get; set; }

    }

    public class BM_ApplicabilityVM
    {
        public long? SrNo { get; set; }
        public int Id { get; set; }
        public string Applicability { get; set; }

        public bool ApplicabilityValue { get; set; }
        public bool Editable { get; set; }

        public decimal? PaidupCapital { get; set; }
        public decimal? Turnover { get; set; }
        public decimal? Turnover_rule { get; set; }
        public decimal? NetWorth_rule { get; set; }
        public decimal? NetProfit_rule { get; set; }
        public decimal? Borrowings_rule { get; set; }
        public decimal? ShareHolders_rule { get; set; }

        public decimal? NetWorth { get; set; }
        public decimal? NetProfit { get; set; }
        public decimal? Borrowings { get; set; }
        public decimal? DepositFromMember { get; set; }
        public long? ShareHolders { get; set; }
        public string Formula { get; set; }
        public long? AmountIn { get; set; }
    }

    public class ShareHolding
    {
        public int Count { get; set; }
        public List<ShareHoldingDetails> lstShareHoldingDetails { get; set; }

        [DataType(DataType.Text)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string DateString { get; set; }

        [DataType(DataType.Date)]
        public string DateString1 { get; set; }

        public string DateString2 { get; set; }

        [DataType(DataType.Text)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date_ { get; set; }
    }

    public class DirectorMasterListVM
    {
        public long ID { get; set; }
        public string Name { get; set; }
        //public string dropDownValueID { get; set; }
    }
    public class Director
    {
        public Director_MasterVM objDirector_MasterVM { get; set; }
        public Directors_Relatives objDirectors_Relatives { get; set; }
        public Directors_DetailsOfInterest objDirectors_DetailsOfInterest { get; set; }
        public Directors_DetailsOfCommiteePosition objDirectors_DetailsOfCommiteePosition { get; set; }

        public Director_MasterKMPUploadSecurity _objSecurities { get; set; }

        public bool CanDelete { get; set;}
        public bool CanAdd { get; set;}
        public bool CanEdit { get; set;}

    }
    public class Director_MasterVM
    {
        public string ViewName { get; set; }
        public long ID { get; set; }
       // [Required(ErrorMessage = "Please enter DIN"), StringLength(8, MinimumLength = 8, ErrorMessage = "Please enter valid DIN")]
        [RegularExpression(@"\d{8}", ErrorMessage = "Please enter valid DIN")]
        public string DIN { get; set; }
        [Required(ErrorMessage = "Please select Salutation")]
        public string Salutation { get; set; }

        [Required(ErrorMessage = "Please enter Name")]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please enter valid Name")]
        public string FirstName { get; set; }
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please enter valid Middle Name")]
        public string MiddleName { get; set; }
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please enter valid Last Name")]
        public string LastName { get; set; }

        public string FullName { get; set; }

        [Required(ErrorMessage = "Please enter Mobile Number"), DataType(DataType.PhoneNumber, ErrorMessage = "Please enter valid Mobile Number")]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Please enter valid Mobile Number")]
        [RegularExpression(@"\d{10}", ErrorMessage = "Please enter valid Mobile Number")]
        public string MobileNo { get; set; }

        [Required(ErrorMessage = "Please select whether resident in India")]
        public bool ResidentInIndia { get; set; }

        [Required(ErrorMessage = "Please select Nationality")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select Nationality")]
        public int? Nationality { get; set; }


        [Required(ErrorMessage = "Please select Occupation")]
        [Range(0, int.MaxValue, ErrorMessage = "Please select Occupation")]
        public int Occupation { get; set; }

        [Required(ErrorMessage = "Please select Area of Occupation")]
        public int AreaOfOccupation { get; set; }

        public string OtherOccupation { get; set; }

        [Required(ErrorMessage = "Please select Educational Qualification")]
        [Range(0, int.MaxValue, ErrorMessage = "Please select Educational Qualification")]
        public int EducationalQualification { get; set; }

        [RequiredIf("EducationalQualification", "-1")]
        public string OtherQualification { get; set; }


        [Required(ErrorMessage = "Please enter Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBirth { get; set; }

        public string DOB { get; set; }

        [Required(ErrorMessage = "Please select Gender")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select Gender")]
        public int? Gender { get; set; }

        [RequiredIf("ResidentInIndia", true, ErrorMessage = "Please enter PAN Number")]
        [RegularExpression(@"[a-zA-Z]{3}[a,b,c,f,g,h,l,j,p,t,e,A,B,C,F,G,H,L,J,P,T,E]{1}[a-zA-Z]{1}\d{4}[a-zA-Z]{1}", ErrorMessage = "Please enter valid PAN")]
        public string PAN { get; set; }
        [RegularExpression(@"\d{12}", ErrorMessage = "Please enter valid Aadhaar Number")]
        //[RequiredIf("ResidentInIndia", true, ErrorMessage = "Please enter Aadhaar Number")]
        public string Adhaar { get; set; }
        //[RegularExpression("^[A-PR-WYa-pr-wy][1-9]\\d\\s?\\d{4}[1-9]$", ErrorMessage = "Please enter valid Passport Number")]
        public string PassportNo { get; set; }
        [Required(ErrorMessage = "Please enter Personal Email Id")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid Email Id")]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "Please enter Official Email Id")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter valid Email Id")]
        public string EmailId_Official { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        //[Required(ErrorMessage = "Please enter DSC Expiry Date")]
        public Nullable<DateTime> DESC_ExpiryDate { get; set; }

        public string DESC_ExpiryDateprofile { get; set; }
        public string FSalutations { get; set; }
        public string MSalutations { get; set; }
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please enter valid Father First Name")]
        public string FatherFirstName { get; set; }
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please enter valid Father Middle Name")]
        public string FatherMiddleName { get; set; }
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please enter valid Father Last Name")]
        public string FatherLastName { get; set; }

        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please enter valid Father Last Name")]
        public string MotherName { get; set; }

        #region Address
        [Required(ErrorMessage = "Please enter Line 1 address")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,.&()/]*$", ErrorMessage = "Please Enter valid Address")]
        public string Present_Address_Line1 { get; set; }
        //[Required(ErrorMessage = "Please enter Line 2 address")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,.&()/]*$", ErrorMessage = "Please Enter valid Address")]

        public string Present_Address_Line2 { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select Country")]
        public int Present_CountryID { get; set; }

        //[Range(1, int.MaxValue, ErrorMessage = "Please select State")]
        public int? Present_StateId { get; set; }
        //[Range(1, int.MaxValue, ErrorMessage = "Please select City")]
        public int? Present_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter valid PIN Number")]
        public string Present_PINCode { get; set; }
        public bool IsSameAddress { get; set; }

        [Required(ErrorMessage = "Please enter Line 1 address")]
        [RegularExpression(@"^[a-zA-Z0-9\s-_,.&()/]*$", ErrorMessage = "Please Enter valid Address")]

        public string Permenant_Address_Line1 { get; set; }
        [RegularExpression(@"^[a-zA-Z0-9\s-_,.&()/]*$", ErrorMessage = "Please Enter valid Address")]
        public string Permenant_Address_Line2 { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please select Country")]
        public int Permenant_CountryID { get; set; }
        //[Range(1, int.MaxValue, ErrorMessage = "Please select State")]
        public int? Permenant_StateId { get; set; }
        //[Range(1, int.MaxValue, ErrorMessage = "Please select City")]
        public int? Permenant_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter valid PIN Number")]
        public string Permenant_PINCode { get; set; }
        #endregion

        public string Photo_Doc_Name { get; set; }
        public string PAN_Doc_Name { get; set; }
        public string Aadhaar_Doc_Name { get; set; }
        public string Passport_Doc_Name { get; set; }

        [ValidateFile(false, ErrorMessage = "Please upload File")]
        public HttpPostedFileBase Photo_Doc { get; set; }
        public HttpPostedFileBase PAN_Doc { get; set; }
        public HttpPostedFileBase Aadhaar_Doc { get; set; }
        public HttpPostedFileBase Passport_Doc { get; set; }
        public int UserID { get; set; }
        public string Approve_disapprove { get; set; }
        public int? NumberofTypeChange { get; set; }
        public bool IsApproved { get; set; }
        public bool IsDisapprove { get; set; }
        public string status { get; set; }
        public bool Ispending { get; set; }
        public List<string> MessageforDirector { get; set; }
        public DateTime? CreateonDate { get; set; }
        public string ChangedBy { get; set; }
        public long MappingID { get; set; }
        public bool? IsActive { get; set; }
        public string TypeofChange { get; set; }
        public string Designation { get; set; }
        public DateTime DateofAppointment { get; set; }
        public Response Response { get; set; }
        public bool IsCAmember { get; set; }

        //[RegularExpression(@"[ACS,FCS]{3}\d{5}", ErrorMessage = "Please Enter valid CS_Membersip Number")]
        [RegularExpression(@"[A|F][0-9]{5}|[A|F][0-9]{4}|[A|F][0-9]{3}", ErrorMessage = "Please Enter valid CS Membership Number")]
        public string CS_Membersip { get; set; }

        public bool IsDetailsOfIntrest { get; set; }

        public string Profiledetails { get; set; }

        public bool? CurrentCust { get; set; }
        public int? DisplayOrder { get; set; }
        public int CustomerId { get; set; }
    }
    public class Directors_Relatives
    {
        public long ID { get; set; }
        public long Director_ID { get; set; }
        public string HUF { get; set; }
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please enter valid Name")]
        public string Spouse { get; set; }
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please enter valid Name")]
        public string Father { get; set; }


        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please enter valid Name")]
        public string Mother { get; set; }

        public List<Relatives> lstSon { get; set; }
        public List<Relatives> lstDaughter { get; set; }
        public List<Relatives> lstBrother { get; set; }
        public List<Relatives> lstSister { get; set; }
        public Response Response { get; set; }

    }
    public class Relatives
    {
        public long Id { get; set; }
        public long Director_ID { get; set; }
        [Required(ErrorMessage = "Please enter Name")]
        [RegularExpression(@"^[a-zA-Z]*$", ErrorMessage = "Please enter valid Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please select Minor status")]
        public bool Minor_or_Adult { get; set; }

        //[RequiredIf("Minor_or_Adult", false)]
        public bool MaritalStatus { get; set; }
        [RequiredIf("MaritalStatus", true)]
        [RegularExpression(@"^[a-zA-Z]*$", ErrorMessage = "Please enter valid Name")]
        public string Spouse { get; set; }
    }
    public class Directors_DetailsOfInterest
    {
        public long Director_ID { get; set; }
        public int count { get; set; }
        public Response Response { get; set; }
        public List<DetailsOfInterest> lstDetailsOfInterest { get; set; }
    }
    public class DetailsOfInterest:Message
    {
        public long Id { get; set; }
        public long Director_ID { get; set; }
        public int SrNo { get; set; }
        [Required(ErrorMessage = "Please select Name of the Entity")]
        public int Entity_Id { get; set; }
        public bool IsOtherEntity { get; set; }
        public string EntityName { get; set; }

        public string EntityType { get; set; }
        public string NatureofIntrest { get; set; }

        [RequiredIf("Entity_Id", "-1")]
        public string NameOfOther { get; set; }
        [RequiredIf("Entity_Id", "-1", ErrorMessage = "Please enter CIN number")]

        public string CIN { get; set; }
        [RequiredIf("Entity_Id", "-1", ErrorMessage = "Please enter PAN")]
        [RegularExpression(@"[A-Za-z]{3}[A,B,C,F,G,H,L,J,P,T,E,a,b,c,f,g,h,l,j,p,t,e]{1}[A-Za-z]{1}\d{4}[A-Za-z]{1}", ErrorMessage = "Please enter valid PAN Number")]
        public string PAN { get; set; }
        public int TypeOfEntity { get; set; }
        [Required(ErrorMessage = "Please select Nature of Interest")]
        public int NatureOfInterest { get; set; }
        [RequiredIf("NatureOfInterest", "2", ErrorMessage = "Please select If Director")]
        public int IfDirector { get; set; }
        public int DirectorDesignationId { get; set; }

        public int DesignationtypeId { get; set; }
        public string BodyCorporateName { get; set; }
        public int? bodycorporateId { get; set; }
        public long? ObligationofContribution { get; set; }
      
        public string DirectorDesignationName { get; set; }
        [RequiredIf("NatureOfInterest", "7", ErrorMessage = "Please select If Interest Through Relatives")]
        public int IfInterestThroughRelatives { get; set; }

        public List<Relatives> IfRelatives { get; set; }
        public List<long> RelativeId { get; set; }
        public string IfDirectorName { get; set; }
        public string IfInterestThroughRelativesName { get; set; }
        public decimal? PercentagesOfShareHolding { get; set; }
        public long? NumberofShares { get; set; }
        [Required(ErrorMessage = "Please select date when the interest arosed or changed")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Arosed{ get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateofAppointment { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? BoardofResolution { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ChangeDate { get; set; }
        public bool? IsResigned { get; set; }
        public bool? IsActivated { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CessationEffectFrom { get; set; }
        public Response Response { get; set; }

        public bool? IsCurrentCustRecord { get; set; }

    }
    public class DetailsOfInterest_Grid
    {
        public long Id { get; set; }
        public long Director_ID { get; set; }
        public int SrNo { get; set; }
        public string Entity_Name { get; set; }
        public string Entity_Type { get; set; }
        public string CIN { get; set; }
        public string PAN { get; set; }
        public string NatureOfinterest { get; set; }
    }

    public class DirectorList_ForEntity
    {
        public string interestType { get; set; }
        public int Entity_Id { get; set; }
        public long Director_ID { get; set; }
        public long? UserID { get; set; }
        public string FullName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DIN { get; set; }
        public string Designation { get; set; }
        public int? DesignationId { get; set; }
        public int? PositionId { get; set; }
        public string ImagePath { get; set; }
        public DateTime? DSC_ExpiryDate { get; set; }
        public int itemlist { get; set; }

        public string DesignationName { get; set; }
        public int DirectorshipIdForQuorum { get; set; }
        public string Directorship { get; set; }
        public string Position { get; set; }
        public string ParticipantType { get; set; }
        public string ParticipantDesignation { get; set; }
        public DateTime? DateofAppointment { get; set; }
    }

    public class CommitteeList_ForEntity
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Please select Entity")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select Entity")]
        public int Entity_Id { get; set; }
        [Required(ErrorMessage = "Please select Committee")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select Committee")]
        public int Committee_Id { get; set; }
        public string CommitteeName { get; set; }
        public long Director_ID { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string DIN { get; set; }
        public string Designation { get; set; }
        public string ImagePath { get; set; }
        public DateTime DSC_ExpiryDate { get; set; }

        [Required(ErrorMessage = "Please select Designation")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select Designation")]
        public int Designation_Id { get; set; }

        [Required(ErrorMessage = "Please select Type")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select Type")]
        public int Committee_Type { get; set; }
        public DateTime? AppointmentFrom { get; set; }
    }
    public class Directors_DetailsOfCommiteePosition
    {
        public long Director_ID { get; set; }
        public bool IsPositionHeld { get; set; }

        public Response Response { get; set; }
        public int count { get; set; }

        public List<DetailsOfCommiteePosition> lstDetailsOfCommiteePosition { get; set; }
    }
    public class DetailsOfCommiteePosition
    {
        public long Id { get; set; }

        public string EntityName { get; set; }
        public string CommityName { get; set; }
        public string DesignationName { get; set; }
        public string Committee_TypeName { get; set; }
        public long Director_ID { get; set; }
        public int? UserId { get; set; }
        public int SrNo { get; set; }
        [Required(ErrorMessage = "Please select Name of the Company")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select Name of the Company")]
        public int Entity_Id_Committee { get; set; }
        [Required(ErrorMessage = "Please select Name of the Committee")]
        [Range(0, Int64.MaxValue, ErrorMessage = "Please select Name of the Committee")]
        public int Committee_Id { get; set; }
        [RequiredIf("Committee_Id", "0", ErrorMessage = "Please enter name of other committee")]
        public string NameOfOtherCommittee { get; set; }
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select Designation")]
        [Required(ErrorMessage = "Please select Designation")]
        public int Designation_Id { get; set; }

        //[Required(ErrorMessage = "Please select Type")]
        //[Range(1, Int64.MaxValue, ErrorMessage = "Please select Type")]
        public int Committee_Type { get; set; }

        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public Response Response { get; set; }
        public DateTime? AppointmentFrom { get; set; }
        public DateTime? AppointmentTo { get; set; }
    }

    public class DetailsOfCommiteePosition_Grid
    {
        public long Id { get; set; }
        public long Director_ID { get; set; }
        public int SrNo { get; set; }
        public string Entity_Name { get; set; }
        public string Committee_Name { get; set; }
        public string CommitteePosition { get; set; }
        public string Designation { get; set; }
    }
    #region KMP Master
    public class Director_KMP
    {
        public Director_MasterVM_KMP objDirector_MasterVM { get; set; }
        public Director_Master_KMPCompanySecurity objcompanysecurity { get; set; }

        public Director_MasterKMPUploadSecurity _objSecurities { get; set; }
    }

    public class Director_MasterVM_KMP
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please select Salutation")]
        public string Salutation { get; set; }
        [Required(ErrorMessage = "Please Select Company Name")]
        public int Entity_Id { get; set; }
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please Enter Father Name")]
        public string FatherName { get; set; }
        public string formerName { get; set; }
        public string MotherName { get; set; }
        //[Required (ErrorMessage ="Please Select Marital Status")]
        public int? Marital_Status { get; set; }
        public string SpouseName { get; set; }
        [Required(ErrorMessage = "Please Enter Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please Enter valid Mobile Number")]
        public string MobileNo { get; set; }
        public bool ResidentInIndia { get; set; }
        [Required(ErrorMessage = "Please Select Nationality")]
        public int Nationality { get; set; }
        [Required(ErrorMessage = "Please select  Occupation")]
        public int Occupation { get; set; }
        [Required (ErrorMessage ="Please enter Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date_BordResulaion { get; set; }

        [Required(ErrorMessage = "Please Enter date of appointment")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? appointment_Date { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_Cessation { get; set; }
        public string ReasonforCessation { get; set; }
        public bool IsCAmember { get; set; }

        [RegularExpression(@"[A|F][0-9]{5}|[A|F][0-9]{4}|[A|F][0-9]{3}", ErrorMessage = "Please Enter valid CS_Membersip Number")]
        public string CS_Membersip { get; set; }
        [Required]
        public int Gender { get; set; }
        [Required(ErrorMessage = "Please Enter PAN Number")]
        [RegularExpression(@"[a-zA-Z]{3}[a,b,c,f,g,h,l,j,p,t,e,A,B,C,F,G,H,L,J,P,T,E]{1}[a-zA-Z]{1}\d{4}[a-zA-Z]{1}", ErrorMessage = "Please enter valid PAN")]
        public string PAN { get; set; }

        [Required(ErrorMessage = "Please Enter Email-Id")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please Enter valid Email-Id")]
        public string EmailId { get; set; }

        [Required]
        public string Reg_officeAddress { get; set; }
        [Required]
        public string Permenant_Address_Line1 { get; set; }
        [Required]
        public string Permenant_Address_Line2 { get; set; }
        [Required]
        public int Permenant_StateId { get; set; }
        [Required]
        public int Permenant_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter valid PIN Number")]
        public string Permenant_PINCode { get; set; }
        public bool IsSameAddress { get; set; }
        [Required]
        public string Present_Address_Line1 { get; set; }
        [Required]
        public string Present_Address_Line2 { get; set; }
        [Required]
        public int Present_StateId { get; set; }
        [Required]
        public int Present_CityId { get; set; }
        [RegularExpression(@"[0-9]{6}", ErrorMessage = "Please Enter valid PIN Number")]
        public string Present_PINCode { get; set; }
        public bool Success { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }

    }

    public class Director_Master_KMPCompanySecurity
    {
        public long Id_sec { get; set; }
        public long KMP_Id { get; set; }
        [Required(ErrorMessage = "Please Enter Name of the Company")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Please Enter Number of securities")]
        public long? securities_No { get; set; }
        [Required(ErrorMessage = "Please Enter Description of Securities")]
        public string Dis_of_securities { get; set; }
        //[Required(ErrorMessage = "Please Enter Nominal Value of Securities")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Nominal Value of Securities must be Numeric")]
        public long? Nominam_valSecurities { get; set; }
        //[Required(ErrorMessage = "Please Enter Date of acquisition")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? acquisition_Date { get; set; }
        //[Required(ErrorMessage = "Please Enter Price paid for acquisition of securities")]
        public decimal? acquisition_pricePaid { get; set; }

        public decimal? Other_ConsiPaid_acquisition { get; set; }
        public decimal? otherconsideration_disposal_RescPrice { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date_disposal { get; set; }
        public decimal? Price_Received_disposal { get; set; }

        public long? Cbal_No_securities_afterTransaction { get; set; }
        public string Mode_of_acquisitionSecurities { get; set; }
        public string Isdematerialized { get; set; }
        public string Securities_encumbrance { get; set; }
        public Response Response { get; set; }

    }

    public class Director_MasterKMPUploadSecurity
    {
        public long Id { get; set; }
        public int EntityId { get; set; }
        public long KMP_ID { get; set; }
        //To change label title value  
        [DisplayName("Date")]
        public string Current_Date { get; set; }

        //To change label title value  
        [DisplayName("Upload File")]
        public string FilePath { get; set; }

        public HttpPostedFileBase File { get; set; }
    }

    #endregion
    public class ValidateFileAttribute : ValidationAttribute
    {
        private bool _IsMandatory { get; set; }
        public ValidateFileAttribute(bool IsMandatory)
        {
            _IsMandatory = IsMandatory;
        }
        public override bool IsValid(object value)
        {
            int MaxContentLength = 1024 * 1024 * 1; //1 MB
            string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".pdf" };

            var file = value as HttpPostedFileBase;
            if (_IsMandatory == false)
                return true;
            else if (file == null)
                return false;
            else if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
            {
                ErrorMessage = "Please upload Your Photo of type: " + string.Join(", ", AllowedFileExtensions);
                return false;
            }
            else if (file.ContentLength > MaxContentLength)
            {
                ErrorMessage = "Your Photo is too large, maximum allowed size is : " + (MaxContentLength / 1024).ToString() + "MB";
                return false;
            }
            else
                return true;
        }



    }
    public class OnEditingDirector:IMessage
    {
        public long Id { get; set; }
        public long DirectorID { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public bool Name { get; set; }
        public bool FatherName { get; set; }
        public bool Nationality { get; set; }
        public bool DateofBirth { get; set; }
        public bool Gender { get; set; }
        public bool PAN { get; set; }
        public bool PassportNumber { get; set; }
        public bool EmailId { get; set; }
        public bool MobileNumber { get; set; }
        public bool PermanentAddress { get; set; }
        public bool PresentAddress { get; set; }
        public bool Photograph { get; set; }
        public bool ResidentialStatus { get; set; }
        public bool Aadhaar { get; set; }
        public bool others { get; set; }
        public bool? IsChangedbyDirector { get; set; }

        public int userIdPerformerDIR6 { get; set; }
        public int userIdPerformerDIR3 { get; set; }
        public bool IsScheduleGenerated { get; set; }
    }

    public class ResignationofDirector:IMessage
    {
        public long IntrestedId { get; set; }
        public long? DirectorID { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ResignationDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CESSATION { get; set; }
        public long? EntityID { get; set; }
        public string EntityName { get; set; }
        public string DirectorName { get; set; }
        public bool isResign { get; set; }
    }

    public class Director_ResignationVM : IMessage
    {
        public long IntrestedId { get; set; }
        public long DirectorID { get; set; }
        public string DirectorName { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please Select Resignation Date")]
        public DateTime? DateOfResignation { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please Select Submission Date")]
        public DateTime? DateOfResignationSubmit { get; set; }
        public string ResignedTime { get; set; }
        public string ReasonOfResignation { get; set; }

        [ValidateFile(false, ErrorMessage = "Please upload File")]
        public HttpPostedFileBase Resignation_Doc { get; set; }
        public string Resignation_DocName { get; set; }
        public long? FileDataId { get; set; }

        public int EntityId { get; set; }
        public int Entity_TypeForResignation { get; set; }
        public long AgendaID { get; set; }
        public long MappingID { get; set; }

        public string Date_Of_Resignation { get; set; }
        public string Date_Of_Resignation_Submit { get; set; }

        public int? Performer_ID { get; set; }
        public int? Reviewer_ID { get; set; }
        public bool? IsResigned { get; set; }
    }

    public class CommitteeMemberVM
    {
        public int Entity_Id { get; set; }
        public long Director_ID { get; set; }
        public long? UserID { get; set; }
        public string FullName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DIN { get; set; }
        public string Designation { get; set; }
        public int? DesignationId { get; set; }
        public int? PositionId { get; set; }
        public string ImagePath { get; set; }

        public string DesignationName { get; set; }
        public string Directorship { get; set; }
        public string Position { get; set; }
        public string ParticipantType { get; set; }
        public DateTime? DateofAppointment { get; set; }
        public bool IsNew { get; set; }
    }

    public class TempMemberVM
    {
        public long Director_ID { get; set; }
        public long? UserID { get; set; }
        public string FullName { get; set; }
    }

    public class DirectorDir3anddir6VM
    {
        public long dirId { get; set; }
        public bool IsName_of_Director { get; set; }
        public bool IsFatherName { get; set; }
        public bool IsNationality { get; set; }
        public bool IsDateofBirth { get; set; }
        public bool IsGender { get; set; }
        public bool IsPAN { get; set; }
        public bool IsPassport { get; set; }
        public bool IsEmailID { get; set; }
        public bool IsMobile { get; set; }
        public bool IsParmanent_Address { get; set; }
        public bool IsPresent_Address { get; set; }
        public bool IsPhotoGraph_of_Director { get; set; }
        public bool IsRasidential_Status { get; set; }
        public bool IsAadharNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string din { get; set; }
        public string FatherFirstName { get; set; }
        public string FatherMiddleName { get; set; }
        public string FatherLastName { get; set; }
        public Nullable<int> CountryId { get; set; }
        public Nullable<int> DesignationId { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<int> Gender { get; set; }
        public string MobileNo { get; set; }
        public string EmailId_Personal { get; set; }
        public string EmailId_Official { get; set; }
        public Nullable<int> EducationalQualification { get; set; }
        public string OtherQualification { get; set; }
        public Nullable<int> Occupation { get; set; }
        public Nullable<int> AreaOfOccupation { get; set; }
        public string OtherOccupation { get; set; }
        public Nullable<bool> ResidentInIndia { get; set; }
        public Nullable<int> Nationality { get; set; }
        public string PAN { get; set; }
        public string Aadhaar { get; set; }
        public string PassportNo { get; set; }
        public string PAN_Doc { get; set; }
        public string Aadhar_Doc { get; set; }
        public string Passport_Doc { get; set; }
        public string Photo_Doc { get; set; }
        public Nullable<System.DateTime> IsDSC_ExpiryDate { get; set; }
    
        public string Permenant_Address_Line1 { get; set; }
        public string Permenant_Address_Line2 { get; set; }
        public int Permenant_StateId { get; set; }
        public Nullable<int> Permenant_CityId { get; set; }
        public string Permenant_PINCode { get; set; }
        public string Present_Address_Line1 { get; set; }
        public string Present_Address_Line2 { get; set; }
        public Nullable<int> Present_StateId { get; set; }
        public Nullable<int> Present_CityId { get; set; }
        public string Present_PINCode { get; set; }
        public Nullable<int> Customer_Id { get; set; }
        public bool Is_Deleted { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string Register_officeAddress { get; set; }
    }

    public class DirectorprofiledetailsVM:IMessage
    {
        public long DirectorId { get; set; }
        [AllowHtml]
        public string Profiledetails { get; set; }
    }

    public class VMDirectorFileUpload
    {
        public Response Response { get; set; }
        public long EntityId { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string FilePath { get; set; }
        [Required(ErrorMessage = "Please Select Form")]
        public HttpPostedFileBase File { get; set; }

        public string FileType { get; set; }

    }
}