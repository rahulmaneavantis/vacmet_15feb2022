﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BM_ManegmentServices.VM
{
    public class MBP2TransactionVM : IMessage
    {
        public long MBP2TransId { get; set; }
        public int EntityId { get; set; }
        public string NatureOfTransaction { get; set; }
        [Required(ErrorMessage = "Please select Nature of Transacction")]
        public int? NatureOfTransactionId { get; set; }
        [Required(ErrorMessage = "Please Date of Transacction")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? DateOfMaking { get; set; }
        [Required(ErrorMessage = "Please Enter Name of Company/Body Corporate/Person")]
        public string Name_ { get; set; }
        public string Address_ { get; set; }
        public decimal? Amount { get; set; }
        public string TimePeriod { get; set; }
        public string Purpose { get; set; }
        public string PercentageOfReserve { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? DateOfBoardResolution { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? DateOfPassingResolution { get; set; }
        public decimal? RateOfInterest { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? DateOfMaturity { get; set; }
        public string NoOfSecurity { get; set; }
        public string KindOfSecurity { get; set; }
        public decimal? NominalValue { get; set; }
        public decimal? PaidUp { get; set; }
        public decimal? CostOfAuisition { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? DateOfSelling { get; set; }
        public decimal? SellingPrice { get; set; }
        public string Remarks { get; set; }
        public DateTime? LastModified { get; set; }
    }

    public class MBP2TransactionNatureVM 
    {
        public int Id { get; set; }
        public string NatureOfTransaction { get; set; }
    }

    public class MBP2TransactionUploadVM : IMessage
    {
        public int EntityId { get; set; }
        public string FilePath { get; set; }
        [Required(ErrorMessage = "Please Select File")]
        public HttpPostedFileBase File { get; set; }
        public List<string> lstMessage { get; set; }
        public int NatureOfTransactionId { get; set; }

    }

    public class MBP2TransactionLimitVM : IMessage
    {
        public int EntityId { get; set; }
        public decimal? ApprovedLimit { get; set; }
        public decimal? UsedLimit { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? DateOfBoardResolution { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? DateOfPassingResolution { get; set; }

        public string DateOfBoardResolutionStr { get; set; }
        public string DateOfPassingResolutionStr { get; set; }
    }
}