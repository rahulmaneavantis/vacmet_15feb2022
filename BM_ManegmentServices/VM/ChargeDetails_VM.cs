﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BM_ManegmentServices.VM
{
    
    public class ChargeDetails_VM : IMessage
    {
        public int EntityId { get; set; }
        public int ChargeId { get; set; }
        public int ChargeTypeId { get; set; }
        public string ChargeType { get; set; }
        public string ChargeTypeDesc { get; set; }
        public string ChargeAmount { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Charge Creation date is required.")]
        public DateTime CreationDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Registration Charge Create date is required.")]
        public DateTime? RegistrationChargeCreateDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Charge Modification date is required.")]
        public DateTime? ModificationDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SatisfactionDate { get; set; }
        public string InFavour { get; set; }
        public int? CreatedBy { get; set; }
        public string ShortDescPropertyCharged { get; set; }
        public string Namesaddresseschargeholder { get; set; }
        public string TermsconditionsOfcharge { get; set; }
        public string Descinstrument { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Registration Charge Modification date is required.")]
        public DateTime? RegistrationChargeModificationDate { get; set; }
        public string Descinstrumentchargemodify { get; set; }
        public string Particularsmodification { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RegistrationsatisfactionDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FactsDelaycondonationDate { get; set; }
        public string Reasonsdelayfiling { get; set; }
        public int? UpdatedBy { get; set; }
        public int? UpdatedOn { get; set; }
        public bool Status { get; set; }
        public bool IsActive { get; set; }
        public bool CanAdd { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
        public string ChargeTerm { get; set; }
        public string AddressOfChargeHolder { get; set; }
        public long? ChargeHeaderId { get; set; }
        public long? ChargeDetailId { get; set; }
        public DateTime? Registration_ChargeCreateDate { get; set; }

        public long? PId { get; set; }
    }
    public class VM_ChargeUpload
    {
        public int EntityId_ChargeUpload { get; set; }
        public string FilePath { get; set; }
        [Required]
        public HttpPostedFileBase File { get; set; }
        public bool Message { get; set; }
        public bool errorMessage { get; set; }
        public string successErrorMessage { get; set; }
    }

    public class ChargeUploadEFormVM : IMessage
    {
        public long EntityId { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public string FilePath { get; set; }
        [Required(ErrorMessage = "Please Select Form")]
        public HttpPostedFileBase File { get; set; }
        [Required(ErrorMessage = "Please select Form Type")]
        public string fromTypeCharge { get; set; }
    }

    public class ChargeHeaderVM : IMessage
    {
        public long ChargeHeaderId { get; set; }
        public string ChargeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public DateTime? SatisficationDate { get; set; }
        public string ChargeHolder { get; set; }
        public string ChargeAmount { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
    }
    public partial class BM_SP_GetEntityChargeDocuments_VM
    {
        public long DocumentID { get; set; }
        public string Description { get; set; }
        public long FileId { get; set; }
        public string FileName { get; set; }
        public string EntitydocType { get; set; }
        public string DocType { get; set; }
    }
}